/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:12 PM
**        * FROM NATURAL PROGRAM : Iaap307c
************************************************************
**        * FILE NAME            : Iaap307c.java
**        * CLASS NAME           : Iaap307c
**        * INSTANCE NAME        : Iaap307c
************************************************************
************************************************************************
* PROGRAM:  IAAP307C
* FUNCTION: RETRIEVE NAME AND ADDRESS FROM MDM ADDRESS EXTRACT REPLACING
*           DIRECT ADABAS READ AS PART OF COR/NAAD SUNSET. THE FINAL
*           OUTPUT WILL BE SORTED BY SSN AND SENT TO SURVIVOR CENTRAL TO
*           BE MERGED WITH OMNI AND TCLIFE FILES RESULTING IN A SINGLE
*           FILE FOR BERWYN.
* CREATED:  08/06/14 : BY JUN TINIO
* HISTORY
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
* 10/2017 JT PIN EXPANSION FIX MARKED 10/2017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap307c extends BLNatBase
{
    // Data Areas
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Write_Rec;

    private DbsGroup pnd_Write_Rec__R_Field_1;
    private DbsField pnd_Write_Rec_Pnd_Wr_Ssn;

    private DbsGroup pnd_Write_Rec__R_Field_2;
    private DbsField pnd_Write_Rec_Pnd_Wr_Ssn_A;
    private DbsField pnd_Write_Rec_Pnd_Wr_Pin;
    private DbsField pnd_Write_Rec_Pnd_Wr_Last_Name;
    private DbsField pnd_Write_Rec_Pnd_Wr_First_Name;
    private DbsField pnd_Write_Rec_Pnd_Wr_Mddle_Name;
    private DbsField pnd_Write_Rec_Pnd_Wr_Gender;
    private DbsField pnd_Write_Rec_Pnd_Wr_Dob;

    private DbsGroup pnd_Write_Rec__R_Field_3;
    private DbsField pnd_Write_Rec_Pnd_Wr_Dob_Yyyy;
    private DbsField pnd_Write_Rec_Pnd_Wr_Dob_Mm;
    private DbsField pnd_Write_Rec_Pnd_Wr_Dob_Dd;
    private DbsField pnd_Write_Rec_Pnd_Wr_Addr_1;
    private DbsField pnd_Write_Rec_Pnd_Wr_Addr_2;
    private DbsField pnd_Write_Rec_Pnd_Wr_City;
    private DbsField pnd_Write_Rec_Pnd_Wr_State;
    private DbsField pnd_Write_Rec_Pnd_Wr_Zip;
    private DbsField pnd_W1_Rec;

    private DbsGroup pnd_W1_Rec__R_Field_4;
    private DbsField pnd_W1_Rec_Pnd_W1_Ssn;

    private DbsGroup pnd_W1_Rec__R_Field_5;
    private DbsField pnd_W1_Rec_Pnd_W1_Ssn_A;
    private DbsField pnd_W1_Rec_Pnd_W1_Last_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_First_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_Mddle_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_Dob;

    private DbsGroup pnd_W1_Rec__R_Field_6;
    private DbsField pnd_W1_Rec_Pnd_Dob_Yyyy;
    private DbsField pnd_W1_Rec_Pnd_Dob_Mm;
    private DbsField pnd_W1_Rec_Pnd_Dob_Dd;
    private DbsField pnd_W1_Rec_Pnd_W1_Cp;

    private DbsGroup pnd_W1_Rec__R_Field_7;
    private DbsField pnd_W1_Rec_Pnd_W1_Contract;
    private DbsField pnd_W1_Rec_Pnd_W1_Payee;
    private DbsField pnd_W1_Rec_Pnd_W1_Pend;
    private DbsField pnd_W1_Rec_Pnd_W1_Option_Code;

    private DbsGroup pnd_W1_Rec__R_Field_8;
    private DbsField pnd_W1_Rec_Pnd_W1_Option_Code_A;
    private DbsField pnd_W1_Rec_Pnd_W1_Pin;
    private DbsField pnd_W1_Rec_Pnd_W1_Orgn_Cde;
    private DbsField pnd_W1_Rec_Pnd_W1_Sex_Cde;

    private DbsGroup pnd_Addr_Rec;
    private DbsField pnd_Addr_Rec_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Addr_Rec_Cntrct_Py;

    private DbsGroup pnd_Addr_Rec__R_Field_9;
    private DbsField pnd_Addr_Rec_Cntrct_Nmbr;
    private DbsField pnd_Addr_Rec_Cntrct_Payee_Cde;
    private DbsField pnd_Addr_Rec_Pnd_Cntrct_Name_Add;

    private DbsGroup pnd_Addr_Rec__R_Field_10;
    private DbsField pnd_Addr_Rec_Cntrct_Name_Free;
    private DbsField pnd_Addr_Rec_Addrss_Lne_1;
    private DbsField pnd_Addr_Rec_Addrss_Lne_2;
    private DbsField pnd_Addr_Rec_Addrss_Lne_3;
    private DbsField pnd_Addr_Rec_Addrss_Lne_4;
    private DbsField pnd_Addr_Rec_Addrss_Lne_5;
    private DbsField pnd_Addr_Rec_Addrss_Lne_6;
    private DbsField pnd_Addr_Rec_Addrss_Postal_Data;

    private DbsGroup pnd_Addr_Rec__R_Field_11;
    private DbsField pnd_Addr_Rec_Addrss_Zip;
    private DbsField pnd_Addr_Rec_Addrss_Zip_4;
    private DbsField pnd_Addr_Rec_Addrss_Carrier_Rte;
    private DbsField pnd_Addr_Rec_Addrss_Walk_Rte;
    private DbsField pnd_Addr_Rec_Addrss_Usps_Future_Use;
    private DbsField pnd_Addr_Rec_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Addr_Rec__R_Field_12;
    private DbsField pnd_Addr_Rec_Addrss_Type_Cde;
    private DbsField pnd_Addr_Rec_Addrss_Last_Chnge_Dte;
    private DbsField pnd_Addr_Rec_Addrss_Last_Chnge_Time;
    private DbsField pnd_Addr_Rec_Permanent_Addrss_Ind;
    private DbsField pnd_Addr_Rec_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Addr_Rec_Eft_Status_Ind;
    private DbsField pnd_Addr_Rec_Checking_Saving_Cde;
    private DbsField pnd_Addr_Rec_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Addr_Rec_Pending_Addrss_Chnge_Dte;
    private DbsField pnd_Addr_Rec_Pending_Addrss_Restore_Dte;
    private DbsField pnd_Addr_Rec_Pending_Perm_Addrss_Chnge_Dte;
    private DbsField pnd_Addr_Rec_Correspondence_Addrss_Ind;
    private DbsField pnd_Addr_Rec_Addrss_Geographic_Cde;
    private DbsField pnd_W_Addr;

    private DbsGroup pnd_W_Addr__R_Field_13;
    private DbsField pnd_W_Addr_Pnd_W_A1;
    private DbsField pnd_New_Addr;

    private DbsGroup pnd_New_Addr__R_Field_14;
    private DbsField pnd_New_Addr_Pnd_N_A1;
    private DbsField pnd_Addr_Lines;

    private DbsGroup pnd_Addr_Lines__R_Field_15;
    private DbsField pnd_Addr_Lines_Pnd_Addr1;
    private DbsField pnd_Addr_Lines_Pnd_Addr2;
    private DbsField pnd_Addr_Lines_Pnd_Addr3;
    private DbsField pnd_Addr_Lines_Pnd_Addr4;
    private DbsField pnd_Addr_Lines_Pnd_Addr5;
    private DbsField pnd_City;
    private DbsField pnd_State;
    private DbsField pnd_Zip;
    private DbsField pnd_W_Names;
    private DbsField pnd_Zip_Found;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_S;
    private DbsField pnd_T;
    private DbsField pnd_Space;
    private DbsField pnd_Naad;
    private DbsField pnd_Naad_Eof;
    private DbsField pnd_Read_Recs;
    private DbsField pnd_Tape_Writes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);

        // Local Variables
        pnd_Write_Rec = localVariables.newFieldInRecord("pnd_Write_Rec", "#WRITE-REC", FieldType.STRING, 252);

        pnd_Write_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Write_Rec__R_Field_1", "REDEFINE", pnd_Write_Rec);
        pnd_Write_Rec_Pnd_Wr_Ssn = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Ssn", "#WR-SSN", FieldType.NUMERIC, 9);

        pnd_Write_Rec__R_Field_2 = pnd_Write_Rec__R_Field_1.newGroupInGroup("pnd_Write_Rec__R_Field_2", "REDEFINE", pnd_Write_Rec_Pnd_Wr_Ssn);
        pnd_Write_Rec_Pnd_Wr_Ssn_A = pnd_Write_Rec__R_Field_2.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Ssn_A", "#WR-SSN-A", FieldType.STRING, 9);
        pnd_Write_Rec_Pnd_Wr_Pin = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Pin", "#WR-PIN", FieldType.STRING, 12);
        pnd_Write_Rec_Pnd_Wr_Last_Name = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Last_Name", "#WR-LAST-NAME", FieldType.STRING, 
            20);
        pnd_Write_Rec_Pnd_Wr_First_Name = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_First_Name", "#WR-FIRST-NAME", FieldType.STRING, 
            15);
        pnd_Write_Rec_Pnd_Wr_Mddle_Name = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Mddle_Name", "#WR-MDDLE-NAME", FieldType.STRING, 
            15);
        pnd_Write_Rec_Pnd_Wr_Gender = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Gender", "#WR-GENDER", FieldType.STRING, 1);
        pnd_Write_Rec_Pnd_Wr_Dob = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Dob", "#WR-DOB", FieldType.STRING, 8);

        pnd_Write_Rec__R_Field_3 = pnd_Write_Rec__R_Field_1.newGroupInGroup("pnd_Write_Rec__R_Field_3", "REDEFINE", pnd_Write_Rec_Pnd_Wr_Dob);
        pnd_Write_Rec_Pnd_Wr_Dob_Yyyy = pnd_Write_Rec__R_Field_3.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Dob_Yyyy", "#WR-DOB-YYYY", FieldType.STRING, 4);
        pnd_Write_Rec_Pnd_Wr_Dob_Mm = pnd_Write_Rec__R_Field_3.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Dob_Mm", "#WR-DOB-MM", FieldType.STRING, 2);
        pnd_Write_Rec_Pnd_Wr_Dob_Dd = pnd_Write_Rec__R_Field_3.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Dob_Dd", "#WR-DOB-DD", FieldType.STRING, 2);
        pnd_Write_Rec_Pnd_Wr_Addr_1 = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Addr_1", "#WR-ADDR-1", FieldType.STRING, 65);
        pnd_Write_Rec_Pnd_Wr_Addr_2 = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Addr_2", "#WR-ADDR-2", FieldType.STRING, 65);
        pnd_Write_Rec_Pnd_Wr_City = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_City", "#WR-CITY", FieldType.STRING, 35);
        pnd_Write_Rec_Pnd_Wr_State = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_State", "#WR-STATE", FieldType.STRING, 2);
        pnd_Write_Rec_Pnd_Wr_Zip = pnd_Write_Rec__R_Field_1.newFieldInGroup("pnd_Write_Rec_Pnd_Wr_Zip", "#WR-ZIP", FieldType.STRING, 5);
        pnd_W1_Rec = localVariables.newFieldInRecord("pnd_W1_Rec", "#W1-REC", FieldType.STRING, 97);

        pnd_W1_Rec__R_Field_4 = localVariables.newGroupInRecord("pnd_W1_Rec__R_Field_4", "REDEFINE", pnd_W1_Rec);
        pnd_W1_Rec_Pnd_W1_Ssn = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Ssn", "#W1-SSN", FieldType.NUMERIC, 9);

        pnd_W1_Rec__R_Field_5 = pnd_W1_Rec__R_Field_4.newGroupInGroup("pnd_W1_Rec__R_Field_5", "REDEFINE", pnd_W1_Rec_Pnd_W1_Ssn);
        pnd_W1_Rec_Pnd_W1_Ssn_A = pnd_W1_Rec__R_Field_5.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Ssn_A", "#W1-SSN-A", FieldType.STRING, 9);
        pnd_W1_Rec_Pnd_W1_Last_Name = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Last_Name", "#W1-LAST-NAME", FieldType.STRING, 20);
        pnd_W1_Rec_Pnd_W1_First_Name = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_First_Name", "#W1-FIRST-NAME", FieldType.STRING, 15);
        pnd_W1_Rec_Pnd_W1_Mddle_Name = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Mddle_Name", "#W1-MDDLE-NAME", FieldType.STRING, 15);
        pnd_W1_Rec_Pnd_W1_Dob = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Dob", "#W1-DOB", FieldType.STRING, 8);

        pnd_W1_Rec__R_Field_6 = pnd_W1_Rec__R_Field_4.newGroupInGroup("pnd_W1_Rec__R_Field_6", "REDEFINE", pnd_W1_Rec_Pnd_W1_Dob);
        pnd_W1_Rec_Pnd_Dob_Yyyy = pnd_W1_Rec__R_Field_6.newFieldInGroup("pnd_W1_Rec_Pnd_Dob_Yyyy", "#DOB-YYYY", FieldType.STRING, 4);
        pnd_W1_Rec_Pnd_Dob_Mm = pnd_W1_Rec__R_Field_6.newFieldInGroup("pnd_W1_Rec_Pnd_Dob_Mm", "#DOB-MM", FieldType.STRING, 2);
        pnd_W1_Rec_Pnd_Dob_Dd = pnd_W1_Rec__R_Field_6.newFieldInGroup("pnd_W1_Rec_Pnd_Dob_Dd", "#DOB-DD", FieldType.STRING, 2);
        pnd_W1_Rec_Pnd_W1_Cp = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Cp", "#W1-CP", FieldType.STRING, 12);

        pnd_W1_Rec__R_Field_7 = pnd_W1_Rec__R_Field_4.newGroupInGroup("pnd_W1_Rec__R_Field_7", "REDEFINE", pnd_W1_Rec_Pnd_W1_Cp);
        pnd_W1_Rec_Pnd_W1_Contract = pnd_W1_Rec__R_Field_7.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Contract", "#W1-CONTRACT", FieldType.STRING, 10);
        pnd_W1_Rec_Pnd_W1_Payee = pnd_W1_Rec__R_Field_7.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Payee", "#W1-PAYEE", FieldType.NUMERIC, 2);
        pnd_W1_Rec_Pnd_W1_Pend = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Pend", "#W1-PEND", FieldType.STRING, 1);
        pnd_W1_Rec_Pnd_W1_Option_Code = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Option_Code", "#W1-OPTION-CODE", FieldType.NUMERIC, 2);

        pnd_W1_Rec__R_Field_8 = pnd_W1_Rec__R_Field_4.newGroupInGroup("pnd_W1_Rec__R_Field_8", "REDEFINE", pnd_W1_Rec_Pnd_W1_Option_Code);
        pnd_W1_Rec_Pnd_W1_Option_Code_A = pnd_W1_Rec__R_Field_8.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Option_Code_A", "#W1-OPTION-CODE-A", FieldType.STRING, 
            2);
        pnd_W1_Rec_Pnd_W1_Pin = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Pin", "#W1-PIN", FieldType.NUMERIC, 12);
        pnd_W1_Rec_Pnd_W1_Orgn_Cde = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", FieldType.STRING, 2);
        pnd_W1_Rec_Pnd_W1_Sex_Cde = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Sex_Cde", "#W1-SEX-CDE", FieldType.STRING, 1);

        pnd_Addr_Rec = localVariables.newGroupInRecord("pnd_Addr_Rec", "#ADDR-REC");
        pnd_Addr_Rec_Ph_Unque_Id_Nmbr = pnd_Addr_Rec.newFieldInGroup("pnd_Addr_Rec_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Addr_Rec_Cntrct_Py = pnd_Addr_Rec.newFieldInGroup("pnd_Addr_Rec_Cntrct_Py", "CNTRCT-PY", FieldType.STRING, 12);

        pnd_Addr_Rec__R_Field_9 = pnd_Addr_Rec.newGroupInGroup("pnd_Addr_Rec__R_Field_9", "REDEFINE", pnd_Addr_Rec_Cntrct_Py);
        pnd_Addr_Rec_Cntrct_Nmbr = pnd_Addr_Rec__R_Field_9.newFieldInGroup("pnd_Addr_Rec_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Addr_Rec_Cntrct_Payee_Cde = pnd_Addr_Rec__R_Field_9.newFieldInGroup("pnd_Addr_Rec_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Addr_Rec_Pnd_Cntrct_Name_Add = pnd_Addr_Rec.newFieldInGroup("pnd_Addr_Rec_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 245);

        pnd_Addr_Rec__R_Field_10 = pnd_Addr_Rec.newGroupInGroup("pnd_Addr_Rec__R_Field_10", "REDEFINE", pnd_Addr_Rec_Pnd_Cntrct_Name_Add);
        pnd_Addr_Rec_Cntrct_Name_Free = pnd_Addr_Rec__R_Field_10.newFieldInGroup("pnd_Addr_Rec_Cntrct_Name_Free", "CNTRCT-NAME-FREE", FieldType.STRING, 
            35);
        pnd_Addr_Rec_Addrss_Lne_1 = pnd_Addr_Rec__R_Field_10.newFieldInGroup("pnd_Addr_Rec_Addrss_Lne_1", "ADDRSS-LNE-1", FieldType.STRING, 35);
        pnd_Addr_Rec_Addrss_Lne_2 = pnd_Addr_Rec__R_Field_10.newFieldInGroup("pnd_Addr_Rec_Addrss_Lne_2", "ADDRSS-LNE-2", FieldType.STRING, 35);
        pnd_Addr_Rec_Addrss_Lne_3 = pnd_Addr_Rec__R_Field_10.newFieldInGroup("pnd_Addr_Rec_Addrss_Lne_3", "ADDRSS-LNE-3", FieldType.STRING, 35);
        pnd_Addr_Rec_Addrss_Lne_4 = pnd_Addr_Rec__R_Field_10.newFieldInGroup("pnd_Addr_Rec_Addrss_Lne_4", "ADDRSS-LNE-4", FieldType.STRING, 35);
        pnd_Addr_Rec_Addrss_Lne_5 = pnd_Addr_Rec__R_Field_10.newFieldInGroup("pnd_Addr_Rec_Addrss_Lne_5", "ADDRSS-LNE-5", FieldType.STRING, 35);
        pnd_Addr_Rec_Addrss_Lne_6 = pnd_Addr_Rec__R_Field_10.newFieldInGroup("pnd_Addr_Rec_Addrss_Lne_6", "ADDRSS-LNE-6", FieldType.STRING, 35);
        pnd_Addr_Rec_Addrss_Postal_Data = pnd_Addr_Rec.newFieldInGroup("pnd_Addr_Rec_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 32);

        pnd_Addr_Rec__R_Field_11 = pnd_Addr_Rec.newGroupInGroup("pnd_Addr_Rec__R_Field_11", "REDEFINE", pnd_Addr_Rec_Addrss_Postal_Data);
        pnd_Addr_Rec_Addrss_Zip = pnd_Addr_Rec__R_Field_11.newFieldInGroup("pnd_Addr_Rec_Addrss_Zip", "ADDRSS-ZIP", FieldType.STRING, 5);
        pnd_Addr_Rec_Addrss_Zip_4 = pnd_Addr_Rec__R_Field_11.newFieldInGroup("pnd_Addr_Rec_Addrss_Zip_4", "ADDRSS-ZIP-4", FieldType.STRING, 4);
        pnd_Addr_Rec_Addrss_Carrier_Rte = pnd_Addr_Rec__R_Field_11.newFieldInGroup("pnd_Addr_Rec_Addrss_Carrier_Rte", "ADDRSS-CARRIER-RTE", FieldType.STRING, 
            4);
        pnd_Addr_Rec_Addrss_Walk_Rte = pnd_Addr_Rec__R_Field_11.newFieldInGroup("pnd_Addr_Rec_Addrss_Walk_Rte", "ADDRSS-WALK-RTE", FieldType.STRING, 4);
        pnd_Addr_Rec_Addrss_Usps_Future_Use = pnd_Addr_Rec__R_Field_11.newFieldInGroup("pnd_Addr_Rec_Addrss_Usps_Future_Use", "ADDRSS-USPS-FUTURE-USE", 
            FieldType.STRING, 15);
        pnd_Addr_Rec_Pnd_Rest_Of_Record = pnd_Addr_Rec.newFieldInGroup("pnd_Addr_Rec_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 76);

        pnd_Addr_Rec__R_Field_12 = pnd_Addr_Rec.newGroupInGroup("pnd_Addr_Rec__R_Field_12", "REDEFINE", pnd_Addr_Rec_Pnd_Rest_Of_Record);
        pnd_Addr_Rec_Addrss_Type_Cde = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 1);
        pnd_Addr_Rec_Addrss_Last_Chnge_Dte = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Addrss_Last_Chnge_Dte", "ADDRSS-LAST-CHNGE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Addr_Rec_Addrss_Last_Chnge_Time = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Addrss_Last_Chnge_Time", "ADDRSS-LAST-CHNGE-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Addr_Rec_Permanent_Addrss_Ind = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Permanent_Addrss_Ind", "PERMANENT-ADDRSS-IND", FieldType.STRING, 
            1);
        pnd_Addr_Rec_Bank_Aba_Acct_Nmbr = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Bank_Aba_Acct_Nmbr", "BANK-ABA-ACCT-NMBR", FieldType.STRING, 
            9);
        pnd_Addr_Rec_Eft_Status_Ind = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Eft_Status_Ind", "EFT-STATUS-IND", FieldType.STRING, 1);
        pnd_Addr_Rec_Checking_Saving_Cde = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Checking_Saving_Cde", "CHECKING-SAVING-CDE", FieldType.STRING, 
            1);
        pnd_Addr_Rec_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Ph_Bank_Pymnt_Acct_Nmbr", "PH-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Addr_Rec_Pending_Addrss_Chnge_Dte = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Pending_Addrss_Chnge_Dte", "PENDING-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Addr_Rec_Pending_Addrss_Restore_Dte = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Pending_Addrss_Restore_Dte", "PENDING-ADDRSS-RESTORE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Addr_Rec_Pending_Perm_Addrss_Chnge_Dte = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Pending_Perm_Addrss_Chnge_Dte", "PENDING-PERM-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Addr_Rec_Correspondence_Addrss_Ind = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Correspondence_Addrss_Ind", "CORRESPONDENCE-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Addr_Rec_Addrss_Geographic_Cde = pnd_Addr_Rec__R_Field_12.newFieldInGroup("pnd_Addr_Rec_Addrss_Geographic_Cde", "ADDRSS-GEOGRAPHIC-CDE", FieldType.STRING, 
            2);
        pnd_W_Addr = localVariables.newFieldInRecord("pnd_W_Addr", "#W-ADDR", FieldType.STRING, 25);

        pnd_W_Addr__R_Field_13 = localVariables.newGroupInRecord("pnd_W_Addr__R_Field_13", "REDEFINE", pnd_W_Addr);
        pnd_W_Addr_Pnd_W_A1 = pnd_W_Addr__R_Field_13.newFieldArrayInGroup("pnd_W_Addr_Pnd_W_A1", "#W-A1", FieldType.STRING, 1, new DbsArrayController(1, 
            25));
        pnd_New_Addr = localVariables.newFieldInRecord("pnd_New_Addr", "#NEW-ADDR", FieldType.STRING, 25);

        pnd_New_Addr__R_Field_14 = localVariables.newGroupInRecord("pnd_New_Addr__R_Field_14", "REDEFINE", pnd_New_Addr);
        pnd_New_Addr_Pnd_N_A1 = pnd_New_Addr__R_Field_14.newFieldArrayInGroup("pnd_New_Addr_Pnd_N_A1", "#N-A1", FieldType.STRING, 1, new DbsArrayController(1, 
            25));
        pnd_Addr_Lines = localVariables.newFieldArrayInRecord("pnd_Addr_Lines", "#ADDR-LINES", FieldType.STRING, 25, new DbsArrayController(1, 5));

        pnd_Addr_Lines__R_Field_15 = localVariables.newGroupInRecord("pnd_Addr_Lines__R_Field_15", "REDEFINE", pnd_Addr_Lines);
        pnd_Addr_Lines_Pnd_Addr1 = pnd_Addr_Lines__R_Field_15.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr1", "#ADDR1", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr2 = pnd_Addr_Lines__R_Field_15.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr2", "#ADDR2", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr3 = pnd_Addr_Lines__R_Field_15.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr3", "#ADDR3", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr4 = pnd_Addr_Lines__R_Field_15.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr4", "#ADDR4", FieldType.STRING, 25);
        pnd_Addr_Lines_Pnd_Addr5 = pnd_Addr_Lines__R_Field_15.newFieldInGroup("pnd_Addr_Lines_Pnd_Addr5", "#ADDR5", FieldType.STRING, 25);
        pnd_City = localVariables.newFieldInRecord("pnd_City", "#CITY", FieldType.STRING, 25);
        pnd_State = localVariables.newFieldInRecord("pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Zip = localVariables.newFieldInRecord("pnd_Zip", "#ZIP", FieldType.STRING, 5);
        pnd_W_Names = localVariables.newFieldArrayInRecord("pnd_W_Names", "#W-NAMES", FieldType.STRING, 25, new DbsArrayController(1, 19));
        pnd_Zip_Found = localVariables.newFieldInRecord("pnd_Zip_Found", "#ZIP-FOUND", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.INTEGER, 2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.INTEGER, 2);
        pnd_Space = localVariables.newFieldInRecord("pnd_Space", "#SPACE", FieldType.INTEGER, 2);
        pnd_Naad = localVariables.newFieldInRecord("pnd_Naad", "#NAAD", FieldType.BOOLEAN, 1);
        pnd_Naad_Eof = localVariables.newFieldInRecord("pnd_Naad_Eof", "#NAAD-EOF", FieldType.BOOLEAN, 1);
        pnd_Read_Recs = localVariables.newFieldInRecord("pnd_Read_Recs", "#READ-RECS", FieldType.NUMERIC, 8);
        pnd_Tape_Writes = localVariables.newFieldInRecord("pnd_Tape_Writes", "#TAPE-WRITES", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap307c() throws Exception
    {
        super("Iaap307c");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 0
        //* *AT TOP OF PAGE(1)
        //*   WRITE(1) NOTITLE NOHDR *PROGRAM 20T
        //*     'BERWYN MISSING ADDRESS REPORT' 20X *DATE
        //* *END-TOPPAGE
        R1:                                                                                                                                                               //Natural: READ WORK FILE 1 #W1-REC
        while (condition(getWorkFiles().read(1, pnd_W1_Rec)))
        {
            pnd_Read_Recs.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #READ-RECS
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-TAPE
            sub_Pnd_Write_Out_Tape();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "IA READS ================================> ",pnd_Read_Recs);                                                                               //Natural: WRITE 'IA READS ================================> ' #READ-RECS
        if (Global.isEscape()) return;
        getReports().write(0, "RECORDS WRITTEN TO TAPE =================> ",pnd_Tape_Writes);                                                                             //Natural: WRITE 'RECORDS WRITTEN TO TAPE =================> ' #TAPE-WRITES
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-TAPE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CITY
    }
    private void sub_Pnd_Write_Out_Tape() throws Exception                                                                                                                //Natural: #WRITE-OUT-TAPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
                                                                                                                                                                          //Natural: PERFORM GET-ADDRESS
        sub_Get_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Write_Rec_Pnd_Wr_Ssn.setValue(pnd_W1_Rec_Pnd_W1_Ssn);                                                                                                         //Natural: ASSIGN #WR-SSN := #W1-SSN
        pnd_Write_Rec_Pnd_Wr_Last_Name.setValue(pnd_W1_Rec_Pnd_W1_Last_Name);                                                                                             //Natural: ASSIGN #WR-LAST-NAME := #W1-LAST-NAME
        pnd_Write_Rec_Pnd_Wr_First_Name.setValue(pnd_W1_Rec_Pnd_W1_First_Name);                                                                                           //Natural: ASSIGN #WR-FIRST-NAME := #W1-FIRST-NAME
        pnd_Write_Rec_Pnd_Wr_Mddle_Name.setValue(pnd_W1_Rec_Pnd_W1_Mddle_Name);                                                                                           //Natural: ASSIGN #WR-MDDLE-NAME := #W1-MDDLE-NAME
        pnd_Write_Rec_Pnd_Wr_Gender.setValue(pnd_W1_Rec_Pnd_W1_Sex_Cde);                                                                                                  //Natural: ASSIGN #WR-GENDER := #W1-SEX-CDE
        pnd_Write_Rec_Pnd_Wr_Dob_Yyyy.setValue(pnd_W1_Rec_Pnd_Dob_Yyyy);                                                                                                  //Natural: ASSIGN #WR-DOB-YYYY := #DOB-YYYY
        pnd_Write_Rec_Pnd_Wr_Dob_Mm.setValue(pnd_W1_Rec_Pnd_Dob_Mm);                                                                                                      //Natural: ASSIGN #WR-DOB-MM := #DOB-MM
        pnd_Write_Rec_Pnd_Wr_Dob_Dd.setValue(pnd_W1_Rec_Pnd_Dob_Dd);                                                                                                      //Natural: ASSIGN #WR-DOB-DD := #DOB-DD
        pnd_Write_Rec_Pnd_Wr_Pin.setValue(pnd_W1_Rec_Pnd_W1_Pin);                                                                                                         //Natural: ASSIGN #WR-PIN := #W1-PIN
        getWorkFiles().write(3, false, pnd_Write_Rec);                                                                                                                    //Natural: WRITE WORK FILE 3 #WRITE-REC
        pnd_Tape_Writes.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TAPE-WRITES
        pnd_Addr_Lines.getValue("*").reset();                                                                                                                             //Natural: RESET #ADDR-LINES ( * ) #CITY #STATE #ZIP #NAAD #WR-ADDR-1 #WR-ADDR-2 #WR-CITY #WR-STATE #WR-ZIP
        pnd_City.reset();
        pnd_State.reset();
        pnd_Zip.reset();
        pnd_Naad.reset();
        pnd_Write_Rec_Pnd_Wr_Addr_1.reset();
        pnd_Write_Rec_Pnd_Wr_Addr_2.reset();
        pnd_Write_Rec_Pnd_Wr_City.reset();
        pnd_Write_Rec_Pnd_Wr_State.reset();
        pnd_Write_Rec_Pnd_Wr_Zip.reset();
    }
    private void sub_Get_Address() throws Exception                                                                                                                       //Natural: GET-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Naad_Eof.getBoolean()))                                                                                                                         //Natural: IF #NAAD-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Addr_Rec_Cntrct_Py.greater(pnd_W1_Rec_Pnd_W1_Cp)))                                                                                          //Natural: IF CNTRCT-PY GT #W1-CP
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Addr_Rec_Cntrct_Py.equals(pnd_W1_Rec_Pnd_W1_Cp)))                                                                                           //Natural: IF CNTRCT-PY = #W1-CP
            {
                pnd_Naad.setValue(true);                                                                                                                                  //Natural: ASSIGN #NAAD := TRUE
                pnd_W_Names.getValue("*").reset();                                                                                                                        //Natural: RESET #W-NAMES ( * )
                pnd_Addr_Lines_Pnd_Addr1.setValue(pnd_Addr_Rec_Addrss_Lne_1);                                                                                             //Natural: ASSIGN #ADDR1 := ADDRSS-LNE-1
                pnd_Addr_Lines_Pnd_Addr2.setValue(pnd_Addr_Rec_Addrss_Lne_2);                                                                                             //Natural: ASSIGN #ADDR2 := ADDRSS-LNE-2
                pnd_Addr_Lines_Pnd_Addr3.setValue(pnd_Addr_Rec_Addrss_Lne_3);                                                                                             //Natural: ASSIGN #ADDR3 := ADDRSS-LNE-3
                pnd_Addr_Lines_Pnd_Addr4.setValue(pnd_Addr_Rec_Addrss_Lne_4);                                                                                             //Natural: ASSIGN #ADDR4 := ADDRSS-LNE-4
                pnd_Addr_Lines_Pnd_Addr5.setValue(pnd_Addr_Rec_Addrss_Lne_5);                                                                                             //Natural: ASSIGN #ADDR5 := ADDRSS-LNE-5
                pnd_Zip.setValue(pnd_Addr_Rec_Addrss_Zip);                                                                                                                //Natural: ASSIGN #ZIP := ADDRSS-ZIP
                if (condition(pnd_Addr_Rec_Addrss_Type_Cde.equals("U")))                                                                                                  //Natural: IF ADDRSS-TYPE-CDE = 'U'
                {
                    if (condition(pnd_Addr_Rec_Addrss_Geographic_Cde.notEquals(" ")))                                                                                     //Natural: IF ADDRSS-GEOGRAPHIC-CDE NE ' '
                    {
                        DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_State, pnd_Addr_Rec_Addrss_Geographic_Cde);          //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #STATE ADDRSS-GEOGRAPHIC-CDE
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CITY
                    sub_Get_City();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_City.reset();                                                                                                                                     //Natural: RESET #CITY #STATE
                    pnd_State.reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, pnd_Addr_Rec);                                                                                                                         //Natural: READ WORK 2 ONCE #ADDR-REC
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Naad_Eof.setValue(true);                                                                                                                              //Natural: ASSIGN #NAAD-EOF := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pnd_Naad.equals(false)))                                                                                                                            //Natural: IF #NAAD = FALSE
        {
            getReports().display(0, "SSN",                                                                                                                                //Natural: DISPLAY 'SSN' #W1-SSN 'PIN' #W1-PIN 'CONTRCT PY' #W1-CP
            		pnd_W1_Rec_Pnd_W1_Ssn,"PIN",
            		pnd_W1_Rec_Pnd_W1_Pin,"CONTRCT PY",
            		pnd_W1_Rec_Pnd_W1_Cp);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Write_Rec_Pnd_Wr_Addr_1.setValue(pnd_Addr_Lines_Pnd_Addr1);                                                                                               //Natural: ASSIGN #WR-ADDR-1 := #ADDR1
            pnd_Write_Rec_Pnd_Wr_Addr_2.setValue(pnd_Addr_Lines_Pnd_Addr2);                                                                                               //Natural: ASSIGN #WR-ADDR-2 := #ADDR2
            pnd_Write_Rec_Pnd_Wr_Addr_2.setValue(DbsUtil.compress(pnd_Write_Rec_Pnd_Wr_Addr_2, pnd_Addr_Lines_Pnd_Addr3, pnd_Addr_Lines_Pnd_Addr4, pnd_Addr_Lines_Pnd_Addr5)); //Natural: COMPRESS #WR-ADDR-2 #ADDR3 #ADDR4 #ADDR5 INTO #WR-ADDR-2
            pnd_Write_Rec_Pnd_Wr_City.setValue(pnd_City);                                                                                                                 //Natural: ASSIGN #WR-CITY := #CITY
            pnd_Write_Rec_Pnd_Wr_State.setValue(pnd_State);                                                                                                               //Natural: ASSIGN #WR-STATE := #STATE
            pnd_Write_Rec_Pnd_Wr_Zip.setValue(pnd_Zip);                                                                                                                   //Natural: ASSIGN #WR-ZIP := #ZIP
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_City() throws Exception                                                                                                                          //Natural: GET-CITY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Names.getValue("*").reset();                                                                                                                                //Natural: RESET #W-NAMES ( * ) #ZIP-FOUND #CITY #L
        pnd_Zip_Found.reset();
        pnd_City.reset();
        pnd_L.reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 5 TO 1 STEP -1
        for (pnd_I.setValue(5); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
        {
            if (condition(pnd_Addr_Lines.getValue(pnd_I).notEquals(" ")))                                                                                                 //Natural: IF #ADDR-LINES ( #I ) NE ' '
            {
                if (condition(pnd_L.equals(getZero())))                                                                                                                   //Natural: IF #L = 0
                {
                    pnd_L.setValue(pnd_I);                                                                                                                                //Natural: ASSIGN #L := #I
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Addr.setValue(pnd_Addr_Lines.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #W-ADDR := #ADDR-LINES ( #I )
                pnd_New_Addr.reset();                                                                                                                                     //Natural: RESET #NEW-ADDR #T
                pnd_T.reset();
                FOR02:                                                                                                                                                    //Natural: FOR #S = 1 TO 25
                for (pnd_S.setValue(1); condition(pnd_S.lessOrEqual(25)); pnd_S.nadd(1))
                {
                    if (condition(pnd_W_Addr_Pnd_W_A1.getValue(pnd_S).notEquals(" ")))                                                                                    //Natural: IF #W-A1 ( #S ) NE ' '
                    {
                        if (condition(pnd_Space.greater(getZero())))                                                                                                      //Natural: IF #SPACE GT 0
                        {
                            pnd_Space.reset();                                                                                                                            //Natural: RESET #SPACE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_T.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #T
                        pnd_New_Addr_Pnd_N_A1.getValue(pnd_T).setValue(pnd_W_Addr_Pnd_W_A1.getValue(pnd_S));                                                              //Natural: ASSIGN #N-A1 ( #T ) := #W-A1 ( #S )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Space.nadd(1);                                                                                                                                //Natural: ADD 1 TO #SPACE
                        if (condition(pnd_Space.equals(1)))                                                                                                               //Natural: IF #SPACE = 1
                        {
                            pnd_T.nadd(1);                                                                                                                                //Natural: ADD 1 TO #T
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_New_Addr.separate(SeparateOption.WithAnyDelimiters, " ", pnd_W_Names.getValue("*"));                                                                  //Natural: SEPARATE #NEW-ADDR INTO #W-NAMES ( * ) WITH DELIMITER ' '
                if (condition(pnd_W_Names.getValue(1).equals(pnd_Addr_Rec_Addrss_Zip)))                                                                                   //Natural: IF #W-NAMES ( 1 ) = ADDRSS-ZIP
                {
                    if (condition(pnd_W_Names.getValue(2,":",19).notEquals(" ")))                                                                                         //Natural: IF #W-NAMES ( 2:19 ) NE ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Addr_Lines.getValue(pnd_I).reset();                                                                                                           //Natural: RESET #ADDR-LINES ( #I )
                        pnd_Zip_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #ZIP-FOUND := TRUE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                FOR03:                                                                                                                                                    //Natural: FOR #J 19 TO 1 STEP -1
                for (pnd_J.setValue(19); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
                {
                    if (condition(pnd_W_Names.getValue(pnd_J).equals(" ")))                                                                                               //Natural: IF #W-NAMES ( #J ) = ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_W_Names.getValue(pnd_J).equals(pnd_Addr_Rec_Addrss_Zip)))                                                                           //Natural: IF #W-NAMES ( #J ) = ADDRSS-ZIP
                    {
                        if (condition(pnd_J.subtract(2).equals(getZero())))                                                                                               //Natural: IF #J - 2 = 0
                        {
                            pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_J.subtract(1));                                                                        //Natural: ASSIGN #K := #J -1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_J.subtract(2));                                                                        //Natural: ASSIGN #K := #J - 2
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_City.setValue(DbsUtil.compress(pnd_W_Names.getValue(1,":",pnd_K)));                                                                           //Natural: COMPRESS #W-NAMES ( 1:#K ) INTO #CITY
                        pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_J.subtract(1));                                                                            //Natural: ASSIGN #K := #J - 1
                        if (condition(pnd_K.equals(getZero())))                                                                                                           //Natural: IF #K = 0
                        {
                            pnd_K.setValue(1);                                                                                                                            //Natural: ASSIGN #K := 1
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Addr_Rec_Addrss_Geographic_Cde.equals(" ")))                                                                                    //Natural: IF ADDRSS-GEOGRAPHIC-CDE = ' '
                        {
                            pnd_State.setValue(pnd_W_Names.getValue(pnd_K));                                                                                              //Natural: ASSIGN #STATE := #W-NAMES ( #K )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Addr_Lines.getValue(pnd_I).reset();                                                                                                           //Natural: RESET #ADDR-LINES ( #I )
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Zip_Found.getBoolean()))                                                                                                        //Natural: IF #ZIP-FOUND
                        {
                            pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_J.subtract(1));                                                                        //Natural: ASSIGN #K := #J - 1
                            if (condition(pnd_K.equals(getZero())))                                                                                                       //Natural: IF #K = 0
                            {
                                pnd_City.reset();                                                                                                                         //Natural: RESET #CITY
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_City.setValue(DbsUtil.compress(pnd_W_Names.getValue(1,":",pnd_K)));                                                                   //Natural: COMPRESS #W-NAMES ( 1:#K ) INTO #CITY
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Addr_Rec_Addrss_Geographic_Cde.equals(" ")))                                                                                //Natural: IF ADDRSS-GEOGRAPHIC-CDE = ' '
                            {
                                pnd_State.setValue(pnd_W_Names.getValue(pnd_J));                                                                                          //Natural: ASSIGN #STATE := #W-NAMES ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Addr_Lines.getValue(pnd_I).reset();                                                                                                       //Natural: RESET #ADDR-LINES ( #I )
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Zip_Found.equals(false)))                                                                                                                       //Natural: IF #ZIP-FOUND = FALSE
        {
            pnd_W_Addr.setValue(pnd_Addr_Lines.getValue(pnd_L));                                                                                                          //Natural: ASSIGN #W-ADDR := #ADDR-LINES ( #L )
            pnd_New_Addr.reset();                                                                                                                                         //Natural: RESET #NEW-ADDR #T
            pnd_T.reset();
            FOR04:                                                                                                                                                        //Natural: FOR #S = 1 TO 25
            for (pnd_S.setValue(1); condition(pnd_S.lessOrEqual(25)); pnd_S.nadd(1))
            {
                if (condition(pnd_W_Addr_Pnd_W_A1.getValue(pnd_S).notEquals(" ")))                                                                                        //Natural: IF #W-A1 ( #S ) NE ' '
                {
                    if (condition(pnd_Space.greater(getZero())))                                                                                                          //Natural: IF #SPACE GT 0
                    {
                        pnd_Space.reset();                                                                                                                                //Natural: RESET #SPACE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_T.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #T
                    pnd_New_Addr_Pnd_N_A1.getValue(pnd_T).setValue(pnd_W_Addr_Pnd_W_A1.getValue(pnd_S));                                                                  //Natural: ASSIGN #N-A1 ( #T ) := #W-A1 ( #S )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Space.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #SPACE
                    if (condition(pnd_Space.equals(1)))                                                                                                                   //Natural: IF #SPACE = 1
                    {
                        pnd_T.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #T
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_New_Addr.separate(SeparateOption.WithAnyDelimiters, " ", pnd_W_Names.getValue("*"));                                                                      //Natural: SEPARATE #NEW-ADDR INTO #W-NAMES ( * ) WITH DELIMITER ' '
            FOR05:                                                                                                                                                        //Natural: FOR #J 19 TO 1 STEP -1
            for (pnd_J.setValue(19); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
            {
                if (condition(pnd_W_Names.getValue(pnd_J).equals(" ")))                                                                                                   //Natural: IF #W-NAMES ( #J ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_J.subtract(1));                                                                                    //Natural: ASSIGN #K := #J - 1
                if (condition(pnd_K.equals(getZero())))                                                                                                                   //Natural: IF #K = 0
                {
                    pnd_City.reset();                                                                                                                                     //Natural: RESET #CITY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_City.setValue(DbsUtil.compress(pnd_W_Names.getValue(1,":",pnd_K)));                                                                               //Natural: COMPRESS #W-NAMES ( 1:#K ) INTO #CITY
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Addr_Rec_Addrss_Geographic_Cde.equals(" ")))                                                                                            //Natural: IF ADDRSS-GEOGRAPHIC-CDE = ' '
                {
                    pnd_State.setValue(pnd_W_Names.getValue(pnd_J));                                                                                                      //Natural: ASSIGN #STATE := #W-NAMES ( #J )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Addr_Lines.getValue(pnd_L).reset();                                                                                                                   //Natural: RESET #ADDR-LINES ( #L )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=0");

        getReports().setDisplayColumns(0, "SSN",
        		pnd_W1_Rec_Pnd_W1_Ssn,"PIN",
        		pnd_W1_Rec_Pnd_W1_Pin,"CONTRCT PY",
        		pnd_W1_Rec_Pnd_W1_Cp);
    }
}
