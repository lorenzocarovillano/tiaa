/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:53 PM
**        * FROM NATURAL PROGRAM : Iaap3028
************************************************************
**        * FILE NAME            : Iaap3028.java
**        * CLASS NAME           : Iaap3028
**        * INSTANCE NAME        : Iaap3028
************************************************************
************************************************************************
*  PROGRAM NAME  :  IAAP3028                                           *
*  DESCRIPTION   :  NET CHANGE DATA EXTRACTION                         *
*  DATE          :  FEB 1998                                           *
*  FUNCTION      :  INPUT WORK FILES ARE SORTED BY PIN.                *
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* ---------+---------+----------------------------------------------
* 07/13/00 | JHH     | ADD PA SELECT AND SPIA
*          |         | - NEW FIELDS ON IAAA323
*          |         | - COMPANY-CDE CAN BE 'L'
*          |         | - NEW MESSAGE (20) AND TELEPHONE NUMBER
* 09/11/00 | RC/JHH  | CALLNAT FCPN199A FOR FUND DESCRIPTION
* 10/03/00 | JHH     | ADD BIN LOGIC TO 'DETERMINE-BIN-PULLS'
*          |         | CORRECT 'ASSIGN-MESSAGE-NUMBER' LOGIC
*          |         | REMOVE 'Account' FROM 'Stock Index' MESSAGE
* 10/17/00 | JHH     | PASS 'G' IN PAYMENT-METHOD FOR FIXED 'Graded'
*          |         | CHANGE PA-SELECT BIN FROM 5 TO 4 (PER M.A.P.
*          |         |   REQUEST: DO NOT EXCEED 4 WHEN INSERTING!!)
* 01/11/00 | TMM     | PASS 'G' TO BIN SELECT FOR MONTH 1 AS WELL AS
*          |         | MONTH 5 -- FORCE OUTREACH BIN 1 ALL EXCEPT
*          |         | PA SELECT WHICH WILL PULL BIN 4 (EMPTY)
* 03/01/01 | RCC     | CHANGE LOGIC IN 'DETERMINE-BIN-PULLS' TO
*          |         | PULL FROM BINS 1 AND 4 FOR COMBINATION OF
*          |         | REGULAR IA AND SPIA/PA-SELECT CONTRACTS
* 04/18/01 | MARIO   | ADD NEW MESSAGE NUMBERS FOR PRINCIPAL AND INT.
*          |         | AND INTEREST ONLY PAYMENTS.
*          |         | FOR INTEREST ONLY PAYMENTS, USE 'Interest only'
*          |         | FOR PAYMENT METHOD DESCRIPTION.
*          |         | FOR PRINCIPAL AND INTEREST PAYMENTS, USE
*          |         | 'Principal and Interest' FOR PAYMENT METHOD
*          |         | DESCRIPTION.
* 05/10/01 | MARIO   | UNDO CHANGES ABOVE (04/18/01) AND APPLY THE
*          |         | CHANGES BELOW:
*          |         | FOR FIXED ACCOUNTS,
*          |         |    - IF P&I, MOVE 'P' TO PAYMENT-METHOD
*          |         |    - IF IPRO, MOVE 'I' TO PAYMENT-METHOD
* 03/24/04 | SAMLAND | ADD LOGIC TO BYPASS TRANSATION WHICH WILL
*          |         | BLOW UP PROGRAM
*          |         | FIND 'JSAMLAND FOR ADDED LOGIC
* 03/07/11 | BREMER  | ADD CANADIAN TAX DEDUCTION DESCRIPTION
* 02/24/12 | MORRIT  | CHANGE LOGIC TO CHECK FOR #WS-PRIOR-PERCENT
*          |  (TM1)  | AND #WS-CURR-PERCENT > 0 INSTEAD OF > 1.
*          |  (RS1)  | IF CURRENT-PCT IS 0 DONT WRITE LINE '06'
* 07/30/15 |MANSOOR  | REMOVE BYPASS LOGIC FOR ALL SUM DEDUCTIONS AS
*          |         | NET CHANGE LETTER SHOULD GENERATE REGARDLESS
*          |         | OF AMOUNT.
* 05/10/17 |SAI K    | CHANGES FOR PIN EXPANSION
* ---------+---------+----------------------------------------------
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap3028 extends BLNatBase
{
    // Data Areas
    private PdaIaaa323 pdaIaaa323;
    private LdaIaaa029b ldaIaaa029b;
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Additional_Field;
    private DbsField pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent;
    private DbsField pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent;
    private DbsField pnd_Ws_Additional_Field_Pnd_Ws_Graded_Chg_Ind;

    private DbsGroup pnd_Annuity_Certain_Table;
    private DbsField pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr;

    private DbsGroup pnd_Annuity_Certain_Table__R_Field_1;
    private DbsField pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr_A;

    private DbsGroup pnd_Annuity_Certain_Table_Pnd_Ac_Contract_Payee;
    private DbsField pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Payee_Cde;
    private DbsField pnd_Ctrl_Record;

    private DbsGroup pnd_Ctrl_Record__R_Field_2;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Parm_Type;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Date;

    private DbsGroup pnd_Ctrl_Record__R_Field_3;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Ccyy;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Mm;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Dd;

    private DbsGroup pnd_Ctrl_Record__R_Field_4;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Date_Alpha;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Proc_Month;

    private DbsGroup constants;
    private DbsField constants_Pnd_Lo_Values;
    private DbsField constants_Pnd_Hi_Values;

    private DbsGroup pnd_Other_Variables;
    private DbsField pnd_Other_Variables_Pnd_Ws_Date;
    private DbsField pnd_Other_Variables_Pnd_Date;
    private DbsField pnd_Other_Variables_Pnd_I;
    private DbsField pnd_Other_Variables_Pnd_Match;
    private DbsField pnd_Other_Variables_Pnd_First;
    private DbsField pnd_Other_Variables_Pnd_Ws_Ac_Pin;
    private DbsField pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde;

    private DbsGroup pnd_Other_Variables__R_Field_5;
    private DbsField pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde_2;
    private DbsField pnd_Other_Variables_Pnd_Ws_Payee_Cde;

    private DbsGroup pnd_Other_Variables__R_Field_6;
    private DbsField pnd_Other_Variables_Pnd_Ws_Payee_Cde_2;
    private DbsField pnd_Other_Variables_Pnd_Ws_Annuity_Ind;
    private DbsField pnd_Other_Variables_Pnd_Hi_Values_Annuity;
    private DbsField pnd_Other_Variables_Pnd_Hi_Values_Temp;
    private DbsField pnd_Other_Variables_Pnd_Ws_Changed;
    private DbsField pnd_Other_Variables_Pnd_Ws_Save_Pin;
    private DbsField pnd_Other_Variables_Pnd_Ws_Save_Payee_Cde;
    private DbsField pnd_Other_Variables_Pnd_Ws_Hold_Code;

    private DbsGroup pnd_Other_Variables__R_Field_7;
    private DbsField pnd_Other_Variables_Pnd_Ws_Hold_Code_2;
    private DbsField pnd_Other_Variables_Pnd_Ws_Hold_Code_4;
    private DbsField pnd_Other_Variables_Pnd_Ws_No_Contracts;
    private DbsField pnd_Other_Variables_Pnd_Ws_Date_For_Label_Check;

    private DbsGroup pnd_Other_Variables__R_Field_8;
    private DbsField pnd_Other_Variables_Pnd_Ws_Date_Label_Ccyy;
    private DbsField pnd_Other_Variables_Pnd_Ws_Date_Label_Mm;
    private DbsField pnd_Other_Variables_Pnd_Ws_Date_Label_Dd;
    private DbsField pnd_Other_Variables_Pnd_Ws_Fund_Prev_Accum_Amt;
    private DbsField pnd_Other_Variables_Pnd_Ws_Fund_Curr_Accum_Amt;
    private DbsField pnd_Other_Variables_Pnd_Ws_Fund_Prev_Gross_Amt;
    private DbsField pnd_Other_Variables_Pnd_Ws_Fund_Curr_Gross_Amt;
    private DbsField pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt;
    private DbsField pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt;
    private DbsField pnd_Other_Variables_Pnd_Cref_Only;
    private DbsField pnd_Other_Variables_Pnd_Tiaa_Only;
    private DbsField pnd_Other_Variables_Pnd_Life_Only;
    private DbsField pnd_Other_Variables_Pnd_Spia_Paselect;
    private DbsField pnd_Other_Variables_Pnd_Ia_Reg;
    private DbsField pnd_Other_Variables_Pnd_Ws_Bypass;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zero_Value;
    private DbsField pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract;

    private DbsGroup pnd_Other_Variables_Pnd_Ws_Bin_Pulls;
    private DbsField pnd_Other_Variables_Pnd_Ws_Bin_1;
    private DbsField pnd_Other_Variables_Pnd_Ws_Bin_2;
    private DbsField pnd_Other_Variables_Pnd_Ws_Bin_3;
    private DbsField pnd_Other_Variables_Pnd_Ws_Bin_4;
    private DbsField pnd_Other_Variables_Pnd_Ws_Post_Barcode;
    private DbsField pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit;

    private DbsGroup pnd_Other_Variables__R_Field_9;
    private DbsField pnd_Other_Variables_Pnd_Ws_Post_Check_Digit1;
    private DbsField pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2;
    private DbsField pnd_Other_Variables_Pnd_Ws_Postal_Data;

    private DbsGroup pnd_Other_Variables__R_Field_10;
    private DbsField pnd_Other_Variables_Pnd_Ws_Post_Data;

    private DbsGroup pnd_Other_Variables__R_Field_11;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_Code_5;

    private DbsGroup pnd_Other_Variables__R_Field_12;
    private DbsField pnd_Other_Variables_Pnd_Ws_Post_Table;

    private DbsGroup pnd_Other_Variables__R_Field_13;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_1;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_2;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_3;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_4;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_5;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_6;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_7;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_8;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_9;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_10;
    private DbsField pnd_Other_Variables_Pnd_Ws_Zip_11;

    private DbsGroup pnd_Other_Variables_Pnd_Ws_Delivery_Point;
    private DbsField pnd_Other_Variables_Pnd_Ws_Delivery_1;
    private DbsField pnd_Other_Variables_Pnd_Ws_Delivery_2;
    private DbsField pnd_Other_Variables_Filler;

    private DbsGroup pnd_Accept_Pin_Set;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Pin;

    private DbsGroup pnd_Accept_Pin_Set__R_Field_14;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Pin_A;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Annuity_Ind;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Zero_Div_Amt;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_No_Contracts;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org;
    private DbsField pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia;
    private DbsField pnd_Accept_Pin_Set_Pnd_Filler;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa323 = new PdaIaaa323(localVariables);
        ldaIaaa029b = new LdaIaaa029b();
        registerRecord(ldaIaaa029b);
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // Local Variables

        pnd_Ws_Additional_Field = localVariables.newGroupInRecord("pnd_Ws_Additional_Field", "#WS-ADDITIONAL-FIELD");
        pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent = pnd_Ws_Additional_Field.newFieldInGroup("pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent", "#WS-PRIOR-PERCENT", 
            FieldType.NUMERIC, 5, 3);
        pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent = pnd_Ws_Additional_Field.newFieldInGroup("pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent", "#WS-CURR-PERCENT", 
            FieldType.NUMERIC, 5, 3);
        pnd_Ws_Additional_Field_Pnd_Ws_Graded_Chg_Ind = pnd_Ws_Additional_Field.newFieldInGroup("pnd_Ws_Additional_Field_Pnd_Ws_Graded_Chg_Ind", "#WS-GRADED-CHG-IND", 
            FieldType.STRING, 1);

        pnd_Annuity_Certain_Table = localVariables.newGroupInRecord("pnd_Annuity_Certain_Table", "#ANNUITY-CERTAIN-TABLE");
        pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr = pnd_Annuity_Certain_Table.newFieldInGroup("pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr", "#AC-PIN-NBR", 
            FieldType.NUMERIC, 12);

        pnd_Annuity_Certain_Table__R_Field_1 = pnd_Annuity_Certain_Table.newGroupInGroup("pnd_Annuity_Certain_Table__R_Field_1", "REDEFINE", pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr);
        pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr_A = pnd_Annuity_Certain_Table__R_Field_1.newFieldInGroup("pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr_A", 
            "#AC-PIN-NBR-A", FieldType.STRING, 12);

        pnd_Annuity_Certain_Table_Pnd_Ac_Contract_Payee = pnd_Annuity_Certain_Table.newGroupInGroup("pnd_Annuity_Certain_Table_Pnd_Ac_Contract_Payee", 
            "#AC-CONTRACT-PAYEE");
        pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Ppcn_Nbr = pnd_Annuity_Certain_Table_Pnd_Ac_Contract_Payee.newFieldInGroup("pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Ppcn_Nbr", 
            "#AC-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Payee_Cde = pnd_Annuity_Certain_Table_Pnd_Ac_Contract_Payee.newFieldInGroup("pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Payee_Cde", 
            "#AC-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Ctrl_Record = localVariables.newFieldInRecord("pnd_Ctrl_Record", "#CTRL-RECORD", FieldType.STRING, 10);

        pnd_Ctrl_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Ctrl_Record__R_Field_2", "REDEFINE", pnd_Ctrl_Record);
        pnd_Ctrl_Record_Pnd_Cr_Parm_Type = pnd_Ctrl_Record__R_Field_2.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Parm_Type", "#CR-PARM-TYPE", FieldType.STRING, 
            1);
        pnd_Ctrl_Record_Pnd_Cr_Run_Date = pnd_Ctrl_Record__R_Field_2.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Date", "#CR-RUN-DATE", FieldType.NUMERIC, 
            8);

        pnd_Ctrl_Record__R_Field_3 = pnd_Ctrl_Record__R_Field_2.newGroupInGroup("pnd_Ctrl_Record__R_Field_3", "REDEFINE", pnd_Ctrl_Record_Pnd_Cr_Run_Date);
        pnd_Ctrl_Record_Pnd_Cr_Run_Ccyy = pnd_Ctrl_Record__R_Field_3.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Ccyy", "#CR-RUN-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Ctrl_Record_Pnd_Cr_Run_Mm = pnd_Ctrl_Record__R_Field_3.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Mm", "#CR-RUN-MM", FieldType.NUMERIC, 2);
        pnd_Ctrl_Record_Pnd_Cr_Run_Dd = pnd_Ctrl_Record__R_Field_3.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Dd", "#CR-RUN-DD", FieldType.NUMERIC, 2);

        pnd_Ctrl_Record__R_Field_4 = pnd_Ctrl_Record__R_Field_2.newGroupInGroup("pnd_Ctrl_Record__R_Field_4", "REDEFINE", pnd_Ctrl_Record_Pnd_Cr_Run_Date);
        pnd_Ctrl_Record_Pnd_Cr_Run_Date_Alpha = pnd_Ctrl_Record__R_Field_4.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Date_Alpha", "#CR-RUN-DATE-ALPHA", 
            FieldType.STRING, 8);
        pnd_Ctrl_Record_Pnd_Cr_Proc_Month = pnd_Ctrl_Record__R_Field_2.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Proc_Month", "#CR-PROC-MONTH", FieldType.STRING, 
            1);

        constants = localVariables.newGroupInRecord("constants", "CONSTANTS");
        constants_Pnd_Lo_Values = constants.newFieldInGroup("constants_Pnd_Lo_Values", "#LO-VALUES", FieldType.STRING, 1);
        constants_Pnd_Hi_Values = constants.newFieldInGroup("constants_Pnd_Hi_Values", "#HI-VALUES", FieldType.STRING, 1);

        pnd_Other_Variables = localVariables.newGroupInRecord("pnd_Other_Variables", "#OTHER-VARIABLES");
        pnd_Other_Variables_Pnd_Ws_Date = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Date", "#WS-DATE", FieldType.STRING, 8);
        pnd_Other_Variables_Pnd_Date = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Date", "#DATE", FieldType.DATE);
        pnd_Other_Variables_Pnd_I = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Other_Variables_Pnd_Match = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Match", "#MATCH", FieldType.NUMERIC, 1);
        pnd_Other_Variables_Pnd_First = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_First", "#FIRST", FieldType.NUMERIC, 1);
        pnd_Other_Variables_Pnd_Ws_Ac_Pin = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Ac_Pin", "#WS-AC-PIN", FieldType.NUMERIC, 
            12);
        pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde", "#WS-AC-PAYEE-CDE", FieldType.STRING, 
            4);

        pnd_Other_Variables__R_Field_5 = pnd_Other_Variables.newGroupInGroup("pnd_Other_Variables__R_Field_5", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde);
        pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde_2 = pnd_Other_Variables__R_Field_5.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde_2", "#WS-AC-PAYEE-CDE-2", 
            FieldType.NUMERIC, 2);
        pnd_Other_Variables_Pnd_Ws_Payee_Cde = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Payee_Cde", "#WS-PAYEE-CDE", FieldType.STRING, 
            4);

        pnd_Other_Variables__R_Field_6 = pnd_Other_Variables.newGroupInGroup("pnd_Other_Variables__R_Field_6", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Payee_Cde);
        pnd_Other_Variables_Pnd_Ws_Payee_Cde_2 = pnd_Other_Variables__R_Field_6.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Payee_Cde_2", "#WS-PAYEE-CDE-2", 
            FieldType.NUMERIC, 2);
        pnd_Other_Variables_Pnd_Ws_Annuity_Ind = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Annuity_Ind", "#WS-ANNUITY-IND", FieldType.STRING, 
            1);
        pnd_Other_Variables_Pnd_Hi_Values_Annuity = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Hi_Values_Annuity", "#HI-VALUES-ANNUITY", 
            FieldType.NUMERIC, 1);
        pnd_Other_Variables_Pnd_Hi_Values_Temp = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Hi_Values_Temp", "#HI-VALUES-TEMP", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Changed = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Changed", "#WS-CHANGED", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Save_Pin = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Save_Pin", "#WS-SAVE-PIN", FieldType.NUMERIC, 
            12);
        pnd_Other_Variables_Pnd_Ws_Save_Payee_Cde = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Save_Payee_Cde", "#WS-SAVE-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Other_Variables_Pnd_Ws_Hold_Code = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Hold_Code", "#WS-HOLD-CODE", FieldType.STRING, 
            4);

        pnd_Other_Variables__R_Field_7 = pnd_Other_Variables.newGroupInGroup("pnd_Other_Variables__R_Field_7", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Hold_Code);
        pnd_Other_Variables_Pnd_Ws_Hold_Code_2 = pnd_Other_Variables__R_Field_7.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Hold_Code_2", "#WS-HOLD-CODE-2", 
            FieldType.STRING, 2);
        pnd_Other_Variables_Pnd_Ws_Hold_Code_4 = pnd_Other_Variables__R_Field_7.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Hold_Code_4", "#WS-HOLD-CODE-4", 
            FieldType.STRING, 2);
        pnd_Other_Variables_Pnd_Ws_No_Contracts = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_No_Contracts", "#WS-NO-CONTRACTS", FieldType.NUMERIC, 
            3);
        pnd_Other_Variables_Pnd_Ws_Date_For_Label_Check = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Date_For_Label_Check", "#WS-DATE-FOR-LABEL-CHECK", 
            FieldType.NUMERIC, 8);

        pnd_Other_Variables__R_Field_8 = pnd_Other_Variables.newGroupInGroup("pnd_Other_Variables__R_Field_8", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Date_For_Label_Check);
        pnd_Other_Variables_Pnd_Ws_Date_Label_Ccyy = pnd_Other_Variables__R_Field_8.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Date_Label_Ccyy", "#WS-DATE-LABEL-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Other_Variables_Pnd_Ws_Date_Label_Mm = pnd_Other_Variables__R_Field_8.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Date_Label_Mm", "#WS-DATE-LABEL-MM", 
            FieldType.NUMERIC, 2);
        pnd_Other_Variables_Pnd_Ws_Date_Label_Dd = pnd_Other_Variables__R_Field_8.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Date_Label_Dd", "#WS-DATE-LABEL-DD", 
            FieldType.NUMERIC, 2);
        pnd_Other_Variables_Pnd_Ws_Fund_Prev_Accum_Amt = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Fund_Prev_Accum_Amt", "#WS-FUND-PREV-ACCUM-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Other_Variables_Pnd_Ws_Fund_Curr_Accum_Amt = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Fund_Curr_Accum_Amt", "#WS-FUND-CURR-ACCUM-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Other_Variables_Pnd_Ws_Fund_Prev_Gross_Amt = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Fund_Prev_Gross_Amt", "#WS-FUND-PREV-GROSS-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Other_Variables_Pnd_Ws_Fund_Curr_Gross_Amt = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Fund_Curr_Gross_Amt", "#WS-FUND-CURR-GROSS-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt", "#WS-DED-CURR-AMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt", "#WS-DED-PREV-AMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Other_Variables_Pnd_Cref_Only = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Cref_Only", "#CREF-ONLY", FieldType.STRING, 1);
        pnd_Other_Variables_Pnd_Tiaa_Only = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Tiaa_Only", "#TIAA-ONLY", FieldType.STRING, 1);
        pnd_Other_Variables_Pnd_Life_Only = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Life_Only", "#LIFE-ONLY", FieldType.STRING, 1);
        pnd_Other_Variables_Pnd_Spia_Paselect = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Spia_Paselect", "#SPIA-PASELECT", FieldType.BOOLEAN, 
            1);
        pnd_Other_Variables_Pnd_Ia_Reg = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ia_Reg", "#IA-REG", FieldType.BOOLEAN, 1);
        pnd_Other_Variables_Pnd_Ws_Bypass = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Bypass", "#WS-BYPASS", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zero_Value = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zero_Value", "#WS-ZERO-VALUE", FieldType.STRING, 
            1);
        pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract", "#WS-TG-CHG-CONTRACT", 
            FieldType.STRING, 1);

        pnd_Other_Variables_Pnd_Ws_Bin_Pulls = pnd_Other_Variables.newGroupInGroup("pnd_Other_Variables_Pnd_Ws_Bin_Pulls", "#WS-BIN-PULLS");
        pnd_Other_Variables_Pnd_Ws_Bin_1 = pnd_Other_Variables_Pnd_Ws_Bin_Pulls.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Bin_1", "#WS-BIN-1", FieldType.STRING, 
            1);
        pnd_Other_Variables_Pnd_Ws_Bin_2 = pnd_Other_Variables_Pnd_Ws_Bin_Pulls.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Bin_2", "#WS-BIN-2", FieldType.STRING, 
            1);
        pnd_Other_Variables_Pnd_Ws_Bin_3 = pnd_Other_Variables_Pnd_Ws_Bin_Pulls.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Bin_3", "#WS-BIN-3", FieldType.STRING, 
            1);
        pnd_Other_Variables_Pnd_Ws_Bin_4 = pnd_Other_Variables_Pnd_Ws_Bin_Pulls.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Bin_4", "#WS-BIN-4", FieldType.STRING, 
            1);
        pnd_Other_Variables_Pnd_Ws_Post_Barcode = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Post_Barcode", "#WS-POST-BARCODE", FieldType.STRING, 
            14);
        pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit", "#WS-POST-ACCUM-DIGIT", 
            FieldType.NUMERIC, 2);

        pnd_Other_Variables__R_Field_9 = pnd_Other_Variables.newGroupInGroup("pnd_Other_Variables__R_Field_9", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit);
        pnd_Other_Variables_Pnd_Ws_Post_Check_Digit1 = pnd_Other_Variables__R_Field_9.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Post_Check_Digit1", 
            "#WS-POST-CHECK-DIGIT1", FieldType.NUMERIC, 1);
        pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2 = pnd_Other_Variables__R_Field_9.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2", 
            "#WS-POST-CHECK-DIGIT2", FieldType.NUMERIC, 1);
        pnd_Other_Variables_Pnd_Ws_Postal_Data = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Postal_Data", "#WS-POSTAL-DATA", FieldType.STRING, 
            32);

        pnd_Other_Variables__R_Field_10 = pnd_Other_Variables.newGroupInGroup("pnd_Other_Variables__R_Field_10", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Postal_Data);
        pnd_Other_Variables_Pnd_Ws_Post_Data = pnd_Other_Variables__R_Field_10.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Post_Data", "#WS-POST-DATA", 
            FieldType.NUMERIC, 13);

        pnd_Other_Variables__R_Field_11 = pnd_Other_Variables__R_Field_10.newGroupInGroup("pnd_Other_Variables__R_Field_11", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Post_Data);
        pnd_Other_Variables_Pnd_Ws_Zip_Code_5 = pnd_Other_Variables__R_Field_11.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_Code_5", "#WS-ZIP-CODE-5", 
            FieldType.STRING, 5);

        pnd_Other_Variables__R_Field_12 = pnd_Other_Variables__R_Field_10.newGroupInGroup("pnd_Other_Variables__R_Field_12", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Post_Data);
        pnd_Other_Variables_Pnd_Ws_Post_Table = pnd_Other_Variables__R_Field_12.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Post_Table", "#WS-POST-TABLE", 
            FieldType.STRING, 13);

        pnd_Other_Variables__R_Field_13 = pnd_Other_Variables__R_Field_10.newGroupInGroup("pnd_Other_Variables__R_Field_13", "REDEFINE", pnd_Other_Variables_Pnd_Ws_Post_Data);
        pnd_Other_Variables_Pnd_Ws_Zip_1 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_1", "#WS-ZIP-1", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_2 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_2", "#WS-ZIP-2", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_3 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_3", "#WS-ZIP-3", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_4 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_4", "#WS-ZIP-4", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_5 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_5", "#WS-ZIP-5", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_6 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_6", "#WS-ZIP-6", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_7 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_7", "#WS-ZIP-7", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_8 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_8", "#WS-ZIP-8", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_9 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_9", "#WS-ZIP-9", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_10 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_10", "#WS-ZIP-10", FieldType.NUMERIC, 
            1);
        pnd_Other_Variables_Pnd_Ws_Zip_11 = pnd_Other_Variables__R_Field_13.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Zip_11", "#WS-ZIP-11", FieldType.NUMERIC, 
            1);

        pnd_Other_Variables_Pnd_Ws_Delivery_Point = pnd_Other_Variables__R_Field_13.newGroupInGroup("pnd_Other_Variables_Pnd_Ws_Delivery_Point", "#WS-DELIVERY-POINT");
        pnd_Other_Variables_Pnd_Ws_Delivery_1 = pnd_Other_Variables_Pnd_Ws_Delivery_Point.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Delivery_1", "#WS-DELIVERY-1", 
            FieldType.NUMERIC, 1);
        pnd_Other_Variables_Pnd_Ws_Delivery_2 = pnd_Other_Variables_Pnd_Ws_Delivery_Point.newFieldInGroup("pnd_Other_Variables_Pnd_Ws_Delivery_2", "#WS-DELIVERY-2", 
            FieldType.NUMERIC, 1);
        pnd_Other_Variables_Filler = pnd_Other_Variables__R_Field_10.newFieldInGroup("pnd_Other_Variables_Filler", "FILLER", FieldType.STRING, 19);

        pnd_Accept_Pin_Set = localVariables.newGroupInRecord("pnd_Accept_Pin_Set", "#ACCEPT-PIN-SET");
        pnd_Accept_Pin_Set_Pnd_Temp_Pin = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Pin", "#TEMP-PIN", FieldType.NUMERIC, 12);

        pnd_Accept_Pin_Set__R_Field_14 = pnd_Accept_Pin_Set.newGroupInGroup("pnd_Accept_Pin_Set__R_Field_14", "REDEFINE", pnd_Accept_Pin_Set_Pnd_Temp_Pin);
        pnd_Accept_Pin_Set_Pnd_Temp_Pin_A = pnd_Accept_Pin_Set__R_Field_14.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Pin_A", "#TEMP-PIN-A", FieldType.STRING, 
            12);
        pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code", "#TEMP-PAYEE-CODE", FieldType.STRING, 
            4);
        pnd_Accept_Pin_Set_Pnd_Temp_Annuity_Ind = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Annuity_Ind", "#TEMP-ANNUITY-IND", FieldType.STRING, 
            1);
        pnd_Accept_Pin_Set_Pnd_Temp_Zero_Div_Amt = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Zero_Div_Amt", "#TEMP-ZERO-DIV-AMT", 
            FieldType.STRING, 1);
        pnd_Accept_Pin_Set_Pnd_Temp_No_Contracts = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_No_Contracts", "#TEMP-NO-CONTRACTS", 
            FieldType.NUMERIC, 3);
        pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind", "#TEMP-HOLD-IND", FieldType.STRING, 
            1);
        pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org", "#TEMP-HOLD-ORG", FieldType.STRING, 
            4);
        pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia", "#TEMP-SPIA-IA", FieldType.STRING, 
            1);
        pnd_Accept_Pin_Set_Pnd_Filler = pnd_Accept_Pin_Set.newFieldInGroup("pnd_Accept_Pin_Set_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaaa029b.initializeValues();

        localVariables.reset();
        pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent.setInitialValue(0);
        pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent.setInitialValue(0);
        constants_Pnd_Lo_Values.setInitialValue("H'00'");
        constants_Pnd_Hi_Values.setInitialValue("H'99'");
        pnd_Other_Variables_Pnd_First.setInitialValue(0);
        pnd_Other_Variables_Pnd_Cref_Only.setInitialValue(" ");
        pnd_Other_Variables_Pnd_Tiaa_Only.setInitialValue(" ");
        pnd_Other_Variables_Pnd_Life_Only.setInitialValue(" ");
        pnd_Other_Variables_Pnd_Ia_Reg.setInitialValue(false);
        pnd_Other_Variables_Pnd_Ws_Bypass.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap3028() throws Exception
    {
        super("Iaap3028");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Iaap3028|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  -------------------------------------------
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 151
                //* ***********************************************************************
                //*               >>>>>      GET PARAMETER CARD       <<<<<               *
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ctrl_Record);                                                                                      //Natural: INPUT #CTRL-RECORD
                //*  TMM 05/2009
                if (condition(pnd_Ctrl_Record.notEquals("**********")))                                                                                                   //Natural: IF #CTRL-RECORD NE '**********'
                {
                    if (condition(pnd_Ctrl_Record_Pnd_Cr_Parm_Type.equals("T") || pnd_Ctrl_Record_Pnd_Cr_Parm_Type.equals("C") || pnd_Ctrl_Record_Pnd_Cr_Parm_Type.equals("A")  //Natural: IF #CR-PARM-TYPE = 'T' OR = 'C' OR = 'A' OR = '0'
                        || pnd_Ctrl_Record_Pnd_Cr_Parm_Type.equals("0")))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "VALID VALUES ARE 'T' , 'C' , 'A', '0' ONLY ");                                                                             //Natural: WRITE 'VALID VALUES ARE "T" , "C" , "A", "0" ONLY '
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(99);  if (true) return;                                                                                                         //Natural: TERMINATE 99
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ctrl_Record_Pnd_Cr_Run_Date_Alpha.greater(" ")))                                                                                    //Natural: IF #CR-RUN-DATE-ALPHA > ' '
                    {
                        if (condition(DbsUtil.maskMatches(pnd_Ctrl_Record_Pnd_Cr_Run_Date_Alpha,"YYYYMMDD")))                                                             //Natural: IF #CR-RUN-DATE-ALPHA = MASK ( YYYYMMDD )
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, "NOT A VALID DATE");                                                                                                    //Natural: WRITE 'NOT A VALID DATE'
                            if (Global.isEscape()) return;
                            DbsUtil.terminate(99);  if (true) return;                                                                                                     //Natural: TERMINATE 99
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ctrl_Record_Pnd_Cr_Run_Date.equals(getZero())))                                                                                     //Natural: IF #CR-RUN-DATE = 0
                    {
                        pnd_Other_Variables_Pnd_Ws_Date.setValue(Global.getDATN());                                                                                       //Natural: MOVE *DATN TO #WS-DATE
                        pnd_Other_Variables_Pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Other_Variables_Pnd_Ws_Date);                                      //Natural: MOVE EDITED #WS-DATE TO #DATE ( EM = YYYYMMDD )
                        pnd_Ctrl_Record_Pnd_Cr_Run_Date_Alpha.setValueEdited(pnd_Other_Variables_Pnd_Date,new ReportEditMask("YYYYMMDD"));                                //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #CR-RUN-DATE-ALPHA
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("1") || pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("4") || pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("5")  //Natural: IF #CR-PROC-MONTH = '1' OR = '4' OR = '5' OR = '0'
                        || pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("0")))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, " VALID VALUES ARE '1', '4', '5', '0' ONLY");                                                                               //Natural: WRITE ' VALID VALUES ARE "1", "4", "5", "0" ONLY'
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(99);  if (true) return;                                                                                                         //Natural: TERMINATE 99
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* * /
                //* *  OVERRIDE MCGEE
                //* * /
                //*  USE *DATN - CREATE DATE
                if (condition(pnd_Ctrl_Record.equals("**********")))                                                                                                      //Natural: IF #CTRL-RECORD = '**********'
                {
                    pnd_Ctrl_Record_Pnd_Cr_Run_Date.setValue(Global.getDATN());                                                                                           //Natural: MOVE *DATN TO #CR-RUN-DATE
                    pnd_Ctrl_Record_Pnd_Cr_Run_Dd.setValue(20);                                                                                                           //Natural: MOVE 20 TO #CR-RUN-DD
                    pnd_Ctrl_Record_Pnd_Cr_Parm_Type.setValue(0);                                                                                                         //Natural: MOVE 0 TO #CR-PARM-TYPE
                    short decideConditionsMet514 = 0;                                                                                                                     //Natural: DECIDE ON FIRST #CR-RUN-MM;//Natural: VALUE 12
                    if (condition((pnd_Ctrl_Record_Pnd_Cr_Run_Mm.equals(12))))
                    {
                        decideConditionsMet514++;
                        pnd_Ctrl_Record_Pnd_Cr_Proc_Month.setValue("1");                                                                                                  //Natural: MOVE '1' TO #CR-PROC-MONTH
                    }                                                                                                                                                     //Natural: VALUE 03
                    else if (condition((pnd_Ctrl_Record_Pnd_Cr_Run_Mm.equals(3))))
                    {
                        decideConditionsMet514++;
                        pnd_Ctrl_Record_Pnd_Cr_Proc_Month.setValue("4");                                                                                                  //Natural: MOVE '4' TO #CR-PROC-MONTH
                    }                                                                                                                                                     //Natural: VALUE 04
                    else if (condition((pnd_Ctrl_Record_Pnd_Cr_Run_Mm.equals(4))))
                    {
                        decideConditionsMet514++;
                        pnd_Ctrl_Record_Pnd_Cr_Proc_Month.setValue("5");                                                                                                  //Natural: MOVE '5' TO #CR-PROC-MONTH
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Ctrl_Record_Pnd_Cr_Proc_Month.setValue("0");                                                                                                  //Natural: MOVE '0' TO #CR-PROC-MONTH
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*               >>>>> READ WKF1 TO CHECK IF PIN SET <<<<<               *
                //*               >>>>> IS TO BE BYPASSED             <<<<<               *
                //* ***********************************************************************
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 IAAA320 #WS-PRIOR-PERCENT #WS-CURR-PERCENT #WS-GRADED-CHG-IND
                while (condition(getWorkFiles().read(1, pdaIaaa323.getIaaa320(), pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent, pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent, 
                    pnd_Ws_Additional_Field_Pnd_Ws_Graded_Chg_Ind)))
                {
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                    if (condition(pdaIaaa323.getIaaa320_Pin_Nbr().equals(pnd_Other_Variables_Pnd_Ws_Save_Pin)))                                                           //Natural: IF PIN-NBR = #WS-SAVE-PIN
                    {
                        if (condition(pdaIaaa323.getIaaa320_Payee_Cde().notEquals(pnd_Other_Variables_Pnd_Ws_Save_Payee_Cde)))                                            //Natural: IF PAYEE-CDE NE #WS-SAVE-PAYEE-CDE
                        {
                                                                                                                                                                          //Natural: PERFORM DECIDE-ACCEPT-PIN
                            sub_Decide_Accept_Pin();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM DECIDE-ACCEPT-PIN
                        sub_Decide_Accept_Pin();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CHECK PARAMETER
                    //*  APRIL
                    //*  CONVERT TO 2 BYTES
                    if (condition(pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("4")))                                                                                         //Natural: IF #CR-PROC-MONTH = '4'
                    {
                        pnd_Other_Variables_Pnd_Ws_Payee_Cde.setValue(pdaIaaa323.getIaaa320_Payee_Cde());                                                                 //Natural: ASSIGN #WS-PAYEE-CDE := PAYEE-CDE
                        //*  WITH ANNUITY CERTAIN
                        if (condition(pdaIaaa323.getIaaa320_Pin_Nbr().equals(pnd_Other_Variables_Pnd_Ws_Ac_Pin)))                                                         //Natural: IF PIN-NBR = #WS-AC-PIN
                        {
                            if (condition(pnd_Other_Variables_Pnd_Ws_Payee_Cde.equals(pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde)))                                          //Natural: IF #WS-PAYEE-CDE = #WS-AC-PAYEE-CDE
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Other_Variables_Pnd_Ws_Annuity_Ind.setValue(" ");                                                                                     //Natural: MOVE ' ' TO #WS-ANNUITY-IND
                                                                                                                                                                          //Natural: PERFORM CHECK-ANNUITY-CERTAIN
                                sub_Check_Annuity_Certain();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                //*  ANNUITY CERTAIN MATCH
                                if (condition(pnd_Other_Variables_Pnd_Match.equals(getZero())))                                                                           //Natural: IF #MATCH = 0
                                {
                                    //*  SET INDICATOR TO TRUE
                                    pnd_Other_Variables_Pnd_Ws_Annuity_Ind.setValue("Y");                                                                                 //Natural: MOVE 'Y' TO #WS-ANNUITY-IND
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Other_Variables_Pnd_Ws_Annuity_Ind.setValue(" ");                                                                                         //Natural: MOVE ' ' TO #WS-ANNUITY-IND
                                                                                                                                                                          //Natural: PERFORM CHECK-ANNUITY-CERTAIN
                            sub_Check_Annuity_Certain();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  ANNUITY CERTAIN MATCH
                            if (condition(pnd_Other_Variables_Pnd_Match.equals(getZero())))                                                                               //Natural: IF #MATCH = 0
                            {
                                //*  SET INDICATOR TO TRUE
                                pnd_Other_Variables_Pnd_Ws_Annuity_Ind.setValue("Y");                                                                                     //Natural: MOVE 'Y' TO #WS-ANNUITY-IND
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FOR JAN  - GET ALL
                    //*  RECORDS FOR STATEMENT
                    if (condition(((pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("1") || pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("5")) && pdaIaaa323.getIaaa320_Mailing_Label_Ind().equals("1")))) //Natural: IF ( ( #CR-PROC-MONTH = '1' OR = '5' ) AND ( MAILING-LABEL-IND = '1' ) )
                    {
                        //*  PRINTING
                        pdaIaaa323.getIaaa320_Mailing_Label_Ind().setValue("2");                                                                                          //Natural: MOVE '2' TO MAILING-LABEL-IND
                    }                                                                                                                                                     //Natural: END-IF
                    //*  END CHECK PARAMETER
                    if (condition(pdaIaaa323.getIaaa320_Gtn_Return_Cde().greater("0000")))                                                                                //Natural: IF GTN-RETURN-CDE > '0000'
                    {
                        //* * FOR RPT-CODE 9, 10, 13 THRU 16 BYPASSS PIN SET                     **
                        FOR01:                                                                                                                                            //Natural: FOR #I = 1 TO 10
                        for (pnd_Other_Variables_Pnd_I.setValue(1); condition(pnd_Other_Variables_Pnd_I.lessOrEqual(10)); pnd_Other_Variables_Pnd_I.nadd(1))
                        {
                            if (condition(((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(9) || pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(10))  //Natural: IF ( GTN-REPORT-CDE ( #I ) = 9 OR = 10 ) OR ( GTN-REPORT-CDE ( #I ) = 13 THRU 16 )
                                || (pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).greaterOrEqual(13) && pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).lessOrEqual(16)))))
                            {
                                pnd_Other_Variables_Pnd_Ws_Bypass.setValue(1);                                                                                            //Natural: MOVE 1 TO #WS-BYPASS
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WHEN ONE CONTRACT HAS
                    if (condition(pnd_Other_Variables_Pnd_Ws_Bypass.equals(1)))                                                                                           //Natural: IF #WS-BYPASS = 1
                    {
                        //*  AN INVALID REPORT-CODE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  BYPASS PIN SET
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaIaaa323.getIaaa320_Pin_Nbr().equals(1089683)))                                                                                       //Natural: IF PIN-NBR = 1089683
                    {
                        pnd_Other_Variables_Pnd_Ws_Bypass.setValue(1);                                                                                                    //Natural: MOVE 1 TO #WS-BYPASS
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Other_Variables_Pnd_Ws_Hold_Code.setValue(pdaIaaa323.getIaaa320_Hold_Cde());                                                                      //Natural: MOVE HOLD-CDE TO #WS-HOLD-CODE
                    if (condition(((((pnd_Other_Variables_Pnd_Ws_Hold_Code_2.equals(" ") || pnd_Other_Variables_Pnd_Ws_Hold_Code_2.equals("00")) || pnd_Other_Variables_Pnd_Ws_Hold_Code_2.equals("C0"))  //Natural: IF ( #WS-HOLD-CODE-2 = ' ' OR = '00' OR = 'C0' OR = 'B0' ) AND ( HOLD-IND = '0' OR = ' ' )
                        || pnd_Other_Variables_Pnd_Ws_Hold_Code_2.equals("B0")) && (pdaIaaa323.getIaaa320_Hold_Ind().equals("0") || pdaIaaa323.getIaaa320_Hold_Ind().equals(" ")))))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ON HOLD
                        pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                               //Natural: MOVE 'H' TO #TEMP-HOLD-IND
                        pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue(pdaIaaa323.getIaaa320_Hold_Cde());                                                                  //Natural: ASSIGN #TEMP-HOLD-ORG := HOLD-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaIaaa323.getIaaa320_Gtn_Return_Cde().notEquals("0000") || pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue("*").notEquals(getZero()))) //Natural: IF GTN-RETURN-CDE NE '0000' OR GTN-REPORT-CDE ( * ) NE 0
                    {
                                                                                                                                                                          //Natural: PERFORM EVALUATE-GTN-RPT-CODE
                        sub_Evaluate_Gtn_Rpt_Code();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //* ***********************************************************************
                    //*   CHECK IF PIN HAS BOTH SPIA-PASELECT AND IA REG  - ROXAN 03/01/2001
                    //* ***********************************************************************
                    if (condition(pdaIaaa323.getIaaa320_Cntrct_Annty_Ins_Type().equals("S") || pdaIaaa323.getIaaa320_Cntrct_Annty_Ins_Type().equals("M")))                //Natural: IF IAAA320.CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
                    {
                        pnd_Other_Variables_Pnd_Spia_Paselect.setValue(true);                                                                                             //Natural: ASSIGN #SPIA-PASELECT = TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Other_Variables_Pnd_Ia_Reg.setValue(true);                                                                                                    //Natural: ASSIGN #IA-REG := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    //* ***********************************************************************
                    //* *         >>>>>      CHECK IF THERE's any change     <<<<<           **
                    //* ***********************************************************************
                    if (condition(pdaIaaa323.getIaaa320_Tax_Fed_Chg_Flag().equals(1) || pdaIaaa323.getIaaa320_Tax_St_Chg_Flag().equals(1) || pdaIaaa323.getIaaa320_Tax_Loc_Chg_Flag().equals(1))) //Natural: IF TAX-FED-CHG-FLAG = 1 OR TAX-ST-CHG-FLAG = 1 OR TAX-LOC-CHG-FLAG = 1
                    {
                        //*  CHANGED
                        pnd_Other_Variables_Pnd_Ws_Changed.setValue(1);                                                                                                   //Natural: MOVE 1 TO #WS-CHANGED
                    }                                                                                                                                                     //Natural: END-IF
                    FOR02:                                                                                                                                                //Natural: FOR #I 1 DEDUCTION-CNT
                    for (pnd_Other_Variables_Pnd_I.setValue(1); condition(pnd_Other_Variables_Pnd_I.lessOrEqual(pdaIaaa323.getIaaa320_Deduction_Cnt())); 
                        pnd_Other_Variables_Pnd_I.nadd(1))
                    {
                        if (condition(pdaIaaa323.getIaaa320_Ded_Amt_Chg_Flag().getValue(pnd_Other_Variables_Pnd_I).equals(1)))                                            //Natural: IF DED-AMT-CHG-FLAG ( #I ) EQ 1
                        {
                            //*  AT LEAST ONE CHANGED
                            pnd_Other_Variables_Pnd_Ws_Changed.setValue(1);                                                                                               //Natural: MOVE 1 TO #WS-CHANGED
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR03:                                                                                                                                                //Natural: FOR #I 1 FUND-CNT
                    for (pnd_Other_Variables_Pnd_I.setValue(1); condition(pnd_Other_Variables_Pnd_I.lessOrEqual(pdaIaaa323.getIaaa320_Fund_Cnt())); pnd_Other_Variables_Pnd_I.nadd(1))
                    {
                        if (condition(pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue(pnd_Other_Variables_Pnd_I).equals(1) || pdaIaaa323.getIaaa320_Fund_Unit_Value_Chg_Flag().getValue(pnd_Other_Variables_Pnd_I).equals(1)  //Natural: IF FUND-UNIT-QTY-CHG-FLAG ( #I ) = 1 OR FUND-UNIT-VALUE-CHG-FLAG ( #I ) = 1 OR FUND-SETTLEMENT-CHG-FLAG ( #I ) = 1 OR FUND-CONTRACT-CHG-FLAG ( #I ) = 1 OR FUND-DIVIDEND-CHG-FLAG ( #I ) = 1 OR FUND-NET-PYMNT-CHG-FLAG ( #I ) = 1
                            || pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue(pnd_Other_Variables_Pnd_I).equals(1) || pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue(pnd_Other_Variables_Pnd_I).equals(1) 
                            || pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue(pnd_Other_Variables_Pnd_I).equals(1) || pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue(pnd_Other_Variables_Pnd_I).equals(1)))
                        {
                            //*  AT LEAST ONE CHANGED
                            pnd_Other_Variables_Pnd_Ws_Changed.setValue(1);                                                                                               //Natural: MOVE 1 TO #WS-CHANGED
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* ***********************************************************************
                    //* *         >>>>>  CHECK IF ALL CONTRACTS OF A PIN     <<<<<           **
                    //* *         >>>>>  BELONG TO ONE COMPANY ONLY          <<<<<           **
                    //* ***********************************************************************
                    //*  CREF
                    if (condition(pdaIaaa323.getIaaa320_Company_Cde().equals("C")))                                                                                       //Natural: IF COMPANY-CDE = 'C'
                    {
                        pnd_Other_Variables_Pnd_Cref_Only.setValue("C");                                                                                                  //Natural: MOVE 'C' TO #CREF-ONLY
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TIAA
                    if (condition(pdaIaaa323.getIaaa320_Company_Cde().equals("T")))                                                                                       //Natural: IF COMPANY-CDE = 'T'
                    {
                        pnd_Other_Variables_Pnd_Tiaa_Only.setValue("T");                                                                                                  //Natural: MOVE 'T' TO #TIAA-ONLY
                    }                                                                                                                                                     //Natural: END-IF
                    //*  LIFE  JH 7/13/00
                    if (condition(pdaIaaa323.getIaaa320_Company_Cde().equals("L")))                                                                                       //Natural: IF COMPANY-CDE = 'L'
                    {
                        pnd_Other_Variables_Pnd_Life_Only.setValue("L");                                                                                                  //Natural: MOVE 'L' TO #LIFE-ONLY
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Other_Variables_Pnd_Ws_No_Contracts.nadd(1);                                                                                                      //Natural: ADD 1 TO #WS-NO-CONTRACTS
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    //*    MOVE 9999999 TO PIN-NBR
                    //*  PIN EXPANSION
                    pdaIaaa323.getIaaa320_Pin_Nbr().setValue(999999999999L);                                                                                              //Natural: MOVE 999999999999 TO PIN-NBR
                }                                                                                                                                                         //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //*  IB
                                                                                                                                                                          //Natural: PERFORM DECIDE-ACCEPT-PIN
                sub_Decide_Accept_Pin();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* ***********************************************************************
                //* *         >>>>>         WRITE HEADER RECORD          <<<<<           **
                //* ***********************************************************************
                pnd_Accept_Pin_Set_Pnd_Temp_Pin_A.setValue(constants_Pnd_Lo_Values);                                                                                      //Natural: MOVE #LO-VALUES TO #TEMP-PIN-A #AC-PIN-NBR-A
                pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr_A.setValue(constants_Pnd_Lo_Values);
                //* ***********************************************************************
                //* *        >>>>>  PROCESS WORKFILE 1 AGAIN THIS TIME WITH   <<<<<      **
                //* *               THE TEMP FILE. MATCH TO CREATE FINAL NET             **
                //* *               CHANGE RECORD.                                       **
                //* ***********************************************************************
                READWORK02:                                                                                                                                               //Natural: READ WORK FILE 1 IAAA320 #WS-PRIOR-PERCENT #WS-CURR-PERCENT #WS-GRADED-CHG-IND
                while (condition(getWorkFiles().read(1, pdaIaaa323.getIaaa320(), pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent, pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent, 
                    pnd_Ws_Additional_Field_Pnd_Ws_Graded_Chg_Ind)))
                {
                    pnd_Other_Variables_Pnd_Match.setValue(0);                                                                                                            //Natural: MOVE 0 TO #MATCH
                                                                                                                                                                          //Natural: PERFORM COMPARE-PINS
                    sub_Compare_Pins();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Other_Variables_Pnd_Match.equals(1)))                                                                                               //Natural: IF #MATCH = 1
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*      >>>>   ONLY #TEMP-PIN = PIN-NBR GOES BEYOND THIS LINE   <<<<     *
                    //* ***********************************************************************
                    //*  COMMENT THIS - ROXAN 03/01/2001
                    //*  ASSIGN #SPIA-PASELECT = FALSE                   /* JH 10/02/2000
                    //*  IF IAAA320.CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
                    //*    ASSIGN #SPIA-PASELECT = TRUE
                    //*  END-IF
                    //*  --------------------
                                                                                                                                                                          //Natural: PERFORM DETERMINE-POST-BARCODE
                    sub_Determine_Post_Barcode();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETERMINE-BIN-PULLS
                    sub_Determine_Bin_Pulls();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-NET-CHANGE
                    sub_Write_Net_Change();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-WORK
                READWORK02_Exit:
                if (Global.isEscape()) return;
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-TEMP-FILE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-PINS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-POST-BARCODE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-BIN-PULLS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NET-CHANGE
                //* *
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-MESSAGE-NUMBER
                //*  WHEN (#WS-PRIOR-PERCENT > 1 OR #WS-CURR-PERCENT > 1)
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DEDUCTION-DESC
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-INFO
                //*  -------------------------------------     RC/JHH 9/11/2000
                //*  TO COMPARE AMOUNTS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-UNIT-VALUE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TIAA-AMOUNTS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVALUATE-GTN-RPT-CODE
                //* *         >>>>>      CHECK GTN REPORT CODES          <<<<<           **
                //* * FOR RPT-CODE 7     DIVIDENDS ARE SENT TO COLLEGES                  **
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ANNUITY-CERTAIN
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-ANNUITY-CERTAIN
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECIDE-ACCEPT-PIN
                //*   JSAMLAND 3/24/04
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-BYPASS-RECORD
                //*   JSAMLAND 3/24/04
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Check_Temp_File() throws Exception                                                                                                                   //Natural: CHECK-TEMP-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().read(4, pnd_Accept_Pin_Set);                                                                                                                       //Natural: READ WORK FILE 4 ONCE #ACCEPT-PIN-SET
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Accept_Pin_Set_Pnd_Temp_Pin_A.setValue(constants_Pnd_Hi_Values);                                                                                          //Natural: MOVE #HI-VALUES TO #TEMP-PIN-A
            pnd_Other_Variables_Pnd_Hi_Values_Temp.setValue(1);                                                                                                           //Natural: ASSIGN #HI-VALUES-TEMP := 1
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Compare_Pins() throws Exception                                                                                                                      //Natural: COMPARE-PINS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  UNTIL THERE's a match
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Other_Variables_Pnd_Hi_Values_Temp.equals(1)))                                                                                              //Natural: IF #HI-VALUES-TEMP = 1
            {
                pnd_Other_Variables_Pnd_Match.setValue(1);                                                                                                                //Natural: MOVE 1 TO #MATCH
                //*  EOF TEMP FILE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Pin_A.equals(constants_Pnd_Hi_Values)))                                                                             //Natural: IF #TEMP-PIN-A = #HI-VALUES
            {
                pnd_Other_Variables_Pnd_Match.setValue(1);                                                                                                                //Natural: MOVE 1 TO #MATCH
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  OR TEMP > IAAA320.PIN
            if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Pin.less(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                                         //Natural: IF #TEMP-PIN < IAAA320.PIN-NBR
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-TEMP-FILE
                sub_Check_Temp_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Pin.greater(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                                      //Natural: IF #TEMP-PIN > IAAA320.PIN-NBR
            {
                pnd_Other_Variables_Pnd_Match.setValue(1);                                                                                                                //Natural: MOVE 1 TO #MATCH
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Pin.equals(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                                       //Natural: IF #TEMP-PIN = IAAA320.PIN-NBR
            {
                if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code.equals(pdaIaaa323.getIaaa320_Payee_Cde())))                                                          //Natural: IF #TEMP-PAYEE-CODE = IAAA320.PAYEE-CDE
                {
                    pnd_Other_Variables_Pnd_Match.setValue(0);                                                                                                            //Natural: MOVE 0 TO #MATCH
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code.less(pdaIaaa323.getIaaa320_Payee_Cde())))                                                            //Natural: IF #TEMP-PAYEE-CODE < IAAA320.PAYEE-CDE
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-TEMP-FILE
                    sub_Check_Temp_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code.greater(pdaIaaa323.getIaaa320_Payee_Cde())))                                                         //Natural: IF #TEMP-PAYEE-CODE > IAAA320.PAYEE-CDE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Post_Barcode() throws Exception                                                                                                            //Natural: DETERMINE-POST-BARCODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Other_Variables_Pnd_Ws_Post_Table.setValue(pdaIaaa323.getIaaa320_Postal_Data());                                                                              //Natural: MOVE POSTAL-DATA TO #WS-POST-TABLE
        pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit.reset();                                                                                                              //Natural: RESET #WS-POST-ACCUM-DIGIT
        if (condition(DbsUtil.maskMatches(pnd_Other_Variables_Pnd_Ws_Post_Data,"NNNNNNNNN..NN")))                                                                         //Natural: IF #WS-POST-DATA EQ MASK ( NNNNNNNNN..NN )
        {
            pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit.compute(new ComputeParameters(false, pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit), pnd_Other_Variables_Pnd_Ws_Zip_1.add(pnd_Other_Variables_Pnd_Ws_Zip_2).add(pnd_Other_Variables_Pnd_Ws_Zip_3).add(pnd_Other_Variables_Pnd_Ws_Zip_4).add(pnd_Other_Variables_Pnd_Ws_Zip_5).add(pnd_Other_Variables_Pnd_Ws_Zip_6).add(pnd_Other_Variables_Pnd_Ws_Zip_7).add(pnd_Other_Variables_Pnd_Ws_Zip_8).add(pnd_Other_Variables_Pnd_Ws_Zip_9).add(pnd_Other_Variables_Pnd_Ws_Delivery_1).add(pnd_Other_Variables_Pnd_Ws_Delivery_2)); //Natural: ASSIGN #WS-POST-ACCUM-DIGIT := #WS-ZIP-1 + #WS-ZIP-2 + #WS-ZIP-3 + #WS-ZIP-4 + #WS-ZIP-5 + #WS-ZIP-6 + #WS-ZIP-7 + #WS-ZIP-8 + #WS-ZIP-9 + #WS-DELIVERY-1 + #WS-DELIVERY-2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Other_Variables_Pnd_Ws_Post_Accum_Digit.greater(getZero()) && pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2.greater(getZero())))                 //Natural: IF #WS-POST-ACCUM-DIGIT > 0 AND #WS-POST-CHECK-DIGIT2 > 0
        {
            pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2.compute(new ComputeParameters(false, pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2), DbsField.subtract(10,        //Natural: ASSIGN #WS-POST-CHECK-DIGIT2 := 10 - #WS-POST-CHECK-DIGIT2
                pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2));
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Other_Variables_Pnd_Ws_Post_Barcode.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "B", pnd_Other_Variables_Pnd_Ws_Zip_1, pnd_Other_Variables_Pnd_Ws_Zip_2,  //Natural: COMPRESS 'B' #WS-ZIP-1 #WS-ZIP-2 #WS-ZIP-3 #WS-ZIP-4 #WS-ZIP-5 #WS-ZIP-6 #WS-ZIP-7 #WS-ZIP-8 #WS-ZIP-9 #WS-DELIVERY-1 #WS-DELIVERY-2 #WS-POST-CHECK-DIGIT2 'E' INTO #WS-POST-BARCODE LEAVING NO
            pnd_Other_Variables_Pnd_Ws_Zip_3, pnd_Other_Variables_Pnd_Ws_Zip_4, pnd_Other_Variables_Pnd_Ws_Zip_5, pnd_Other_Variables_Pnd_Ws_Zip_6, pnd_Other_Variables_Pnd_Ws_Zip_7, 
            pnd_Other_Variables_Pnd_Ws_Zip_8, pnd_Other_Variables_Pnd_Ws_Zip_9, pnd_Other_Variables_Pnd_Ws_Delivery_1, pnd_Other_Variables_Pnd_Ws_Delivery_2, 
            pnd_Other_Variables_Pnd_Ws_Post_Check_Digit2, "E"));
    }
    private void sub_Determine_Bin_Pulls() throws Exception                                                                                                               //Natural: DETERMINE-BIN-PULLS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WS-BIN-1 TO 4 DEFAULT
        pnd_Other_Variables_Pnd_Ws_Bin_1.setValue("0");                                                                                                                   //Natural: MOVE '0' TO #WS-BIN-1 #WS-BIN-2 #WS-BIN-3 #WS-BIN-4
        pnd_Other_Variables_Pnd_Ws_Bin_2.setValue("0");
        pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("0");
        pnd_Other_Variables_Pnd_Ws_Bin_4.setValue("0");
        if (condition(pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("4") && pnd_Accept_Pin_Set_Pnd_Temp_Annuity_Ind.equals("Y")))                                              //Natural: IF #CR-PROC-MONTH = '4' AND #TEMP-ANNUITY-IND = 'Y'
        {
            //*   ANNUITY CERTAIN
            pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("G");                                                                                                               //Natural: MOVE 'G' TO #WS-BIN-3
        }                                                                                                                                                                 //Natural: END-IF
        //*  OPEN BIN 4 FOR IPRO - ROXAN 03/14/2002
        //*  BIN 4
        if (condition((pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("4") && (pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(25) || pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(27))))) //Natural: IF #CR-PROC-MONTH = '4' AND ( CNTRCT-OPTION-CDE = 25 OR = 27 )
        {
            pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("2");                                                                                                               //Natural: ASSIGN #WS-BIN-3 := '2'
        }                                                                                                                                                                 //Natural: END-IF
        //*  END
        if (condition((! ((pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent.equals(getZero()) || pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent.equals(getZero())))              //Natural: IF NOT ( #WS-PRIOR-PERCENT = 0 OR #WS-CURR-PERCENT = 0 ) AND #CR-PROC-MONTH = '1'
            && pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("1"))))
        {
            //*   GRADED
            pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("G");                                                                                                               //Natural: MOVE 'G' TO #WS-BIN-3
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREFREVALUATION RUN
        //*  BIN 1
        if (condition(pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("5") || pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("1")))                                                    //Natural: IF #CR-PROC-MONTH = '5' OR #CR-PROC-MONTH = '1'
        {
            pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("G");                                                                                                               //Natural: ASSIGN #WS-BIN-3 := 'G'
            //*  BIN 4
            if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia.equals("1")))                                                                                               //Natural: IF #TEMP-SPIA-IA = '1'
            {
                pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("2");                                                                                                           //Natural: ASSIGN #WS-BIN-3 := '2'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  BINS 1 & 4
                if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia.equals("2")))                                                                                           //Natural: IF #TEMP-SPIA-IA = '2'
                {
                    pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("I");                                                                                                       //Natural: ASSIGN #WS-BIN-3 := 'I'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.equals("L") || pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.equals("H")))                                              //Natural: IF #TEMP-HOLD-IND = 'L' OR = 'H'
        {
            //*   LABELS AND HELD
            pnd_Other_Variables_Pnd_Ws_Bin_3.setValue("G");                                                                                                               //Natural: MOVE 'G' TO #WS-BIN-3
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Net_Change() throws Exception                                                                                                                  //Natural: WRITE-NET-CHANGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
        sub_Initialize_Fields();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().setValue(pnd_Accept_Pin_Set_Pnd_Temp_No_Contracts);                                                  //Natural: MOVE #TEMP-NO-CONTRACTS TO #NC-NBR-OF-CONTRACT
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr().setValue(pdaIaaa323.getIaaa320_Pin_Nbr());                                                                   //Natural: MOVE PIN-NBR TO #NC-PIN-NBR
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Nbr().setValue(pdaIaaa323.getIaaa320_Contract_Nbr());                                                         //Natural: MOVE CONTRACT-NBR TO #NC-CONTRACT-NBR
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Payee_Code().setValue(pdaIaaa323.getIaaa320_Payee_Cde());                                                              //Natural: MOVE PAYEE-CDE TO #NC-PAYEE-CODE
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Type().setValue(pdaIaaa323.getIaaa320_Company_Cde());                                                         //Natural: MOVE COMPANY-CDE TO #NC-CONTRACT-TYPE
        //*  --------------------------------------   JH 7/13/00 PA SELECT/SPIA
        //*  PA SELECT
        if (condition(pdaIaaa323.getIaaa320_Cntrct_Annty_Ins_Type().equals("S")))                                                                                         //Natural: IF IAAA320.CNTRCT-ANNTY-INS-TYPE = 'S'
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Type().setValue("P");                                                                                     //Natural: MOVE 'P' TO #NETCHANGE-RECORD.#NC-CONTRACT-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SPIA
        if (condition(pdaIaaa323.getIaaa320_Cntrct_Annty_Ins_Type().equals("M")))                                                                                         //Natural: IF IAAA320.CNTRCT-ANNTY-INS-TYPE = 'M'
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Type().setValue("S");                                                                                     //Natural: MOVE 'S' TO #NETCHANGE-RECORD.#NC-CONTRACT-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------------------------------   JH 7/13/00 END
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Hold_Code().setValue(pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org);                                                            //Natural: MOVE #TEMP-HOLD-ORG TO #NC-HOLD-CODE
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind().setValue(pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind);                                                       //Natural: MOVE #TEMP-HOLD-IND TO #NC-LABEL-HOLD-IND
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code().getValue("*").setValue(pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue("*"));                        //Natural: MOVE GTN-REPORT-CDE ( * ) TO #NC-GTN-REPORT-CODE ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Full_Name().setValue(pdaIaaa323.getIaaa320_Ph_Mailing_Nme());                                                          //Natural: MOVE PH-MAILING-NME TO #NC-FULL-NAME
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(1).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_1());                                           //Natural: MOVE PH-ADDRESS-LINE-1 TO #NC-ADDRESS-1 ( 1 )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(2).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_2());                                           //Natural: MOVE PH-ADDRESS-LINE-2 TO #NC-ADDRESS-1 ( 2 )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(3).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_3());                                           //Natural: MOVE PH-ADDRESS-LINE-3 TO #NC-ADDRESS-1 ( 3 )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(4).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_4());                                           //Natural: MOVE PH-ADDRESS-LINE-4 TO #NC-ADDRESS-1 ( 4 )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(5).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_5());                                           //Natural: MOVE PH-ADDRESS-LINE-5 TO #NC-ADDRESS-1 ( 5 )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(6).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_6());                                           //Natural: MOVE PH-ADDRESS-LINE-6 TO #NC-ADDRESS-1 ( 6 )
        if (condition(pdaIaaa323.getIaaa320_Mode_Cde().equals(100)))                                                                                                      //Natural: IF MODE-CDE = 100
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Date().setValue(pdaIaaa323.getIaaa320_Prior_Check_Dte());                                               //Natural: MOVE PRIOR-CHECK-DTE TO #NC-PREV-CHECK-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Date().reset();                                                                                         //Natural: RESET #NC-PREV-CHECK-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //* * JSAMLAND 3/25/04
        //* * TO PREVENT A PROGRAM ABEND FROM TO LARGE A FEDERAL TAX AMOUNT
        //* * ADDITIONAL LOGIC HAS BEEN ADDED TO CHECK AMOUNT AND BYPASS THE RECORD
        //* * NEED TO CHECK THE FIELDS FOR SIZE PROBLEM
        if (condition(pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt().greater(new DbsDecimal("99999.99")) || pdaIaaa323.getIaaa320_Tax_Prior_St_Amt().greater(new               //Natural: IF TAX-PRIOR-FED-AMT > 99999.99 OR TAX-PRIOR-ST-AMT > 99999.99 OR TAX-PRIOR-LOC-AMT > 99999.99 OR TAX-CURR-FED-AMT > 99999.99 OR TAX-CURR-ST-AMT > 99999.99 OR TAX-CURR-LOC-AMT > 99999.99
            DbsDecimal("99999.99")) || pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt().greater(new DbsDecimal("99999.99")) || pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt().greater(new 
            DbsDecimal("99999.99")) || pdaIaaa323.getIaaa320_Tax_Curr_St_Amt().greater(new DbsDecimal("99999.99")) || pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt().greater(new 
            DbsDecimal("99999.99"))))
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-BYPASS-RECORD
            sub_Display_Bypass_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *  JSAMLAND 03/25/04
        //* ***********************************************************************
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Date().setValue(pdaIaaa323.getIaaa320_Future_Pymnt_Dte());                                                  //Natural: MOVE FUTURE-PYMNT-DTE TO #NC-CURR-CHECK-DATE
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2().setValue(pnd_Other_Variables_Pnd_Ws_Bin_1);                                                                //Natural: MOVE #WS-BIN-1 TO #NC-BIN-NBR-2
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3().setValue(pnd_Other_Variables_Pnd_Ws_Bin_2);                                                                //Natural: MOVE #WS-BIN-2 TO #NC-BIN-NBR-3
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4().setValue(pnd_Other_Variables_Pnd_Ws_Bin_3);                                                                //Natural: MOVE #WS-BIN-3 TO #NC-BIN-NBR-4
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5().setValue(pnd_Other_Variables_Pnd_Ws_Bin_4);                                                                //Natural: MOVE #WS-BIN-4 TO #NC-BIN-NBR-5
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Postnet_Zip().setValue(pnd_Other_Variables_Pnd_Ws_Post_Barcode);                                                       //Natural: MOVE #WS-POST-BARCODE TO #NC-POSTNET-ZIP
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Zip_Code_5().setValue(pnd_Other_Variables_Pnd_Ws_Zip_Code_5);                                                          //Natural: MOVE #WS-ZIP-CODE-5 TO #NC-ZIP-CODE-5
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Count().setValue(pdaIaaa323.getIaaa320_Deduction_Cnt());                                                     //Natural: MOVE DEDUCTION-CNT TO #NC-DEDUCTION-COUNT
        //*  JS
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt().setValue(pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt());                                                    //Natural: MOVE TAX-PRIOR-FED-AMT TO #NC-PREV-FED-AMT
        //*  JS
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_St_Amt().setValue(pdaIaaa323.getIaaa320_Tax_Prior_St_Amt());                                                      //Natural: MOVE TAX-PRIOR-ST-AMT TO #NC-PREV-ST-AMT
        //*  JS
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt().setValue(pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt());                                                    //Natural: MOVE TAX-PRIOR-LOC-AMT TO #NC-PREV-LOC-AMT
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt().setValue(pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt());                                                     //Natural: MOVE TAX-CURR-FED-AMT TO #NC-CURR-FED-AMT
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_St_Amt().setValue(pdaIaaa323.getIaaa320_Tax_Curr_St_Amt());                                                       //Natural: MOVE TAX-CURR-ST-AMT TO #NC-CURR-ST-AMT
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt().setValue(pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt());                                                     //Natural: MOVE TAX-CURR-LOC-AMT TO #NC-CURR-LOC-AMT
        pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt.compute(new ComputeParameters(false, pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt), pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt().add(pdaIaaa323.getIaaa320_Tax_Prior_St_Amt()).add(pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt())); //Natural: ASSIGN #WS-DED-PREV-AMT := TAX-PRIOR-FED-AMT + TAX-PRIOR-ST-AMT + TAX-PRIOR-LOC-AMT
        pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt.compute(new ComputeParameters(false, pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt), pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt().add(pdaIaaa323.getIaaa320_Tax_Curr_St_Amt()).add(pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt())); //Natural: ASSIGN #WS-DED-CURR-AMT := TAX-CURR-FED-AMT + TAX-CURR-ST-AMT + TAX-CURR-LOC-AMT
                                                                                                                                                                          //Natural: PERFORM GET-DEDUCTION-DESC
        sub_Get_Deduction_Desc();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //* *  TOTAL DEDUCTIONS TAXES PLUS OTHER DED  **
        //* ***********************************************************************
        //* * JSAMLAND 3/24/04
        //* **** CHANGE MANSOOR START**********************************************
        //* *IF #WS-DED-PREV-AMT > 99999.99 OR #WS-DED-CURR-AMT > 99999.99
        //* *      PERFORM DISPLAY-BYPASS-RECORD
        //* *      ESCAPE ROUTINE IMMEDIATE
        //* *   END-IF
        //* **** CHANGE MANSOOR END  **********************************************
        //* * JSAMLAND 3/24/04
        //* ***********************************************************************
        //* *
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Total_Deduction().setValue(pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt);                                              //Natural: MOVE #WS-DED-PREV-AMT TO #NC-PREV-TOTAL-DEDUCTION
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Total_Deduction().setValue(pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt);                                              //Natural: MOVE #WS-DED-CURR-AMT TO #NC-CURR-TOTAL-DEDUCTION
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Count().setValue(pdaIaaa323.getIaaa320_Fund_Cnt());                                                               //Natural: MOVE FUND-CNT TO #NC-FUND-COUNT
                                                                                                                                                                          //Natural: PERFORM GET-FUND-INFO
        sub_Get_Fund_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ASSIGN-MESSAGE-NUMBER
        sub_Assign_Message_Number();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages_Ctr().setValue(pnd_Other_Variables_Pnd_I);                                                                    //Natural: MOVE #I TO #NC-MESSAGES-CTR
        getWorkFiles().write(5, false, ldaIaaa029b.getPnd_Netchange_Record());                                                                                            //Natural: WRITE WORK FILE 5 #NETCHANGE-RECORD
    }
    private void sub_Assign_Message_Number() throws Exception                                                                                                             //Natural: ASSIGN-MESSAGE-NUMBER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Other_Variables_Pnd_I.reset();                                                                                                                                //Natural: RESET #I #NC-MESSAGES ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue("*").reset();
        //*  ACCUM NET AMT
        short decideConditionsMet984 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #WS-FUND-PREV-ACCUM-AMT = #WS-FUND-CURR-ACCUM-AMT
        if (condition(pnd_Other_Variables_Pnd_Ws_Fund_Prev_Accum_Amt.equals(pnd_Other_Variables_Pnd_Ws_Fund_Curr_Accum_Amt)))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            //*  JHH
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("02");                                               //Natural: MOVE '02' TO #NC-MESSAGE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: WHEN ( #CR-PROC-MONTH = '1' OR = '5' ) AND NOT #SPIA-PASELECT
        if (condition(((pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("1") || pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("5")) && ! (pnd_Other_Variables_Pnd_Spia_Paselect.getBoolean()))))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            //*  OUTREACH
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("01");                                               //Natural: MOVE '01' TO #NC-MESSAGE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: WHEN #CR-PROC-MONTH = '4' AND ( IAAA320.CNTRCT-OPTION-CDE = 25 OR = 27 )
        if (condition((pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("4") && (pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(25) || pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(27)))))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            //*  NEWSBRIEF
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("1B");                                               //Natural: MOVE '1B' TO #NC-MESSAGE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: WHEN #WS-ZERO-VALUE = 'Y'
        if (condition(pnd_Other_Variables_Pnd_Ws_Zero_Value.equals("Y")))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("10");                                               //Natural: MOVE '10' TO #NC-MESSAGE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: WHEN TRANSFER-IND = '1'
        if (condition(pdaIaaa323.getIaaa320_Transfer_Ind().equals("1")))
        {
            decideConditionsMet984++;
            //*  JHH 10/2/00
            if (condition(pnd_Other_Variables_Pnd_Ws_Zero_Value.notEquals("Y") && ! (pnd_Other_Variables_Pnd_Spia_Paselect.getBoolean())))                                //Natural: IF #WS-ZERO-VALUE NE 'Y' AND NOT #SPIA-PASELECT
            {
                pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                        //Natural: ADD 1 TO #I
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                        //Natural: RESET #NC-MESSAGES ( #I )
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("09");                                           //Natural: MOVE '09' TO #NC-MESSAGE-NUMBER ( #I )
                //*  IB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN TAX-FED-CHG-FLAG = 1 OR TAX-ST-CHG-FLAG = 1 OR TAX-LOC-CHG-FLAG = 1 OR DED-AMT-CHG-FLAG ( * ) = 1
        if (condition(pdaIaaa323.getIaaa320_Tax_Fed_Chg_Flag().equals(1) || pdaIaaa323.getIaaa320_Tax_St_Chg_Flag().equals(1) || pdaIaaa323.getIaaa320_Tax_Loc_Chg_Flag().equals(1) 
            || pdaIaaa323.getIaaa320_Ded_Amt_Chg_Flag().getValue("*").equals(1)))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("04");                                               //Natural: MOVE '04' TO #NC-MESSAGE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: WHEN #WS-ZIP-CODE-5 = 'CANAD' OR = 'FORGN'
        if (condition(pnd_Other_Variables_Pnd_Ws_Zip_Code_5.equals("CANAD") || pnd_Other_Variables_Pnd_Ws_Zip_Code_5.equals("FORGN")))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("08");                                               //Natural: MOVE '08' TO #NC-MESSAGE-NUMBER ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Phone_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("1 800 842-2776");                                     //Natural: MOVE '1 800 842-2776' TO #NC-PHONE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: WHEN #CR-PROC-MONTH = '5' AND #WS-TG-CHG-CONTRACT = 'Y'
        if (condition(pnd_Ctrl_Record_Pnd_Cr_Proc_Month.equals("5") && pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract.equals("Y")))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("11");                                               //Natural: MOVE '11' TO #NC-MESSAGE-NUMBER ( #I )
            //*  TM1 NEW
            //*  JHH
            pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract.reset();                                                                                                           //Natural: RESET #WS-TG-CHG-CONTRACT
        }                                                                                                                                                                 //Natural: WHEN ( #WS-PRIOR-PERCENT > 0 OR #WS-CURR-PERCENT > 0 ) AND #WS-TG-CHG-CONTRACT = 'Y' AND NOT #SPIA-PASELECT
        if (condition((((pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent.greater(getZero()) || pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent.greater(getZero())) 
            && pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract.equals("Y")) && ! (pnd_Other_Variables_Pnd_Spia_Paselect.getBoolean()))))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("05");                                               //Natural: MOVE '05' TO #NC-MESSAGE-NUMBER ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Percent().getValue(pnd_Other_Variables_Pnd_I).setValue(pnd_Ws_Additional_Field_Pnd_Ws_Prior_Percent);      //Natural: MOVE #WS-PRIOR-PERCENT TO #NC-MESSAGE-PERCENT ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1().getValue(pnd_Other_Variables_Pnd_I).compute(new ComputeParameters(false, ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1().getValue(pnd_Other_Variables_Pnd_I)),  //Natural: ASSIGN #NC-MSG-DATE-CCYY-1 ( #I ) := #CR-RUN-CCYY + 1
                pnd_Ctrl_Record_Pnd_Cr_Run_Ccyy.add(1));
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2().getValue(pnd_Other_Variables_Pnd_I).setValue(pnd_Ctrl_Record_Pnd_Cr_Run_Ccyy);                   //Natural: ASSIGN #NC-MSG-DATE-CCYY-2 ( #I ) := #CR-RUN-CCYY
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_2().getValue(pnd_Other_Variables_Pnd_I).setValue(pnd_Ctrl_Record_Pnd_Cr_Run_Mm);                       //Natural: ASSIGN #NC-MSG-DATE-MM-2 ( #I ) := #CR-RUN-MM
            //*  RS1
            if (condition(pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent.greater(getZero())))                                                                                //Natural: IF #WS-CURR-PERCENT GT 0
            {
                pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                        //Natural: ADD 1 TO #I
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                        //Natural: RESET #NC-MESSAGES ( #I )
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("06");                                           //Natural: MOVE '06' TO #NC-MESSAGE-NUMBER ( #I )
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1().getValue(pnd_Other_Variables_Pnd_I).compute(new ComputeParameters(false,                     //Natural: ASSIGN #NC-MSG-DATE-CCYY-1 ( #I ) := #CR-RUN-CCYY + 1
                    ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1().getValue(pnd_Other_Variables_Pnd_I)), pnd_Ctrl_Record_Pnd_Cr_Run_Ccyy.add(1));
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2().getValue(pnd_Other_Variables_Pnd_I).compute(new ComputeParameters(false,                     //Natural: ASSIGN #NC-MSG-DATE-CCYY-2 ( #I ) := #CR-RUN-CCYY + 2
                    ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2().getValue(pnd_Other_Variables_Pnd_I)), pnd_Ctrl_Record_Pnd_Cr_Run_Ccyy.add(2));
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Percent().getValue(pnd_Other_Variables_Pnd_I).setValue(pnd_Ws_Additional_Field_Pnd_Ws_Curr_Percent);   //Natural: MOVE #WS-CURR-PERCENT TO #NC-MESSAGE-PERCENT ( #I )
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_2().getValue(pnd_Other_Variables_Pnd_I).setValue(0);                                               //Natural: MOVE 0 TO #NC-MSG-DATE-MM-2 ( #I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract.reset();                                                                                                           //Natural: RESET #WS-TG-CHG-CONTRACT
        }                                                                                                                                                                 //Natural: WHEN MODE-CDE NE 100
        if (condition(pdaIaaa323.getIaaa320_Mode_Cde().notEquals(100)))
        {
            decideConditionsMet984++;
            pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                            //Natural: ADD 1 TO #I
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                            //Natural: RESET #NC-MESSAGES ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("07");                                               //Natural: MOVE '07' TO #NC-MESSAGE-NUMBER ( #I )
            pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract.reset();                                                                                                           //Natural: RESET #WS-TG-CHG-CONTRACT
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet984 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  JHH 10/2/2000
        pnd_Other_Variables_Pnd_I.nadd(1);                                                                                                                                //Natural: ADD 1 TO #I
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages().getValue(pnd_Other_Variables_Pnd_I).reset();                                                                //Natural: RESET #NC-MESSAGES ( #I )
        if (condition(pnd_Other_Variables_Pnd_Spia_Paselect.getBoolean()))                                                                                                //Natural: IF #SPIA-PASELECT
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("20");                                               //Natural: MOVE '20' TO #NC-MESSAGE-NUMBER ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Phone_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("1 800 223-1200");                                     //Natural: MOVE '1 800 223-1200' TO #NC-PHONE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(pnd_Other_Variables_Pnd_I).setValue("03");                                               //Natural: MOVE '03' TO #NC-MESSAGE-NUMBER ( #I )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Deduction_Desc() throws Exception                                                                                                                //Natural: GET-DEDUCTION-DESC
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I 1 DEDUCTION-CNT
        for (pnd_Other_Variables_Pnd_I.setValue(1); condition(pnd_Other_Variables_Pnd_I.lessOrEqual(pdaIaaa323.getIaaa320_Deduction_Cnt())); pnd_Other_Variables_Pnd_I.nadd(1))
        {
            short decideConditionsMet1069 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF DED-CDE ( #I );//Natural: VALUE 001
            if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(1))))
            {
                decideConditionsMet1069++;
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Blue Cross");                                   //Natural: MOVE 'Blue Cross' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 002
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(2))))
            {
                decideConditionsMet1069++;
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Long Term Care");                               //Natural: MOVE 'Long Term Care' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 003
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(3))))
            {
                decideConditionsMet1069++;
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Major Medical");                                //Natural: MOVE 'Major Medical' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 004
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(4))))
            {
                decideConditionsMet1069++;
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Group Life    ");                               //Natural: MOVE 'Group Life    ' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 005
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(5))))
            {
                decideConditionsMet1069++;
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Overpayment   ");                               //Natural: MOVE 'Overpayment   ' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 006
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(6))))
            {
                decideConditionsMet1069++;
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("NYSUT         ");                               //Natural: MOVE 'NYSUT         ' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 007
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(7))))
            {
                decideConditionsMet1069++;
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Personal Annuity");                             //Natural: MOVE 'Personal Annuity' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 008
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(8))))
            {
                decideConditionsMet1069++;
                //*  (LB)
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Mutual Funds  ");                               //Natural: MOVE 'Mutual Funds  ' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 009
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(9))))
            {
                decideConditionsMet1069++;
                //*  JH 7/13/00
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("PA Select     ");                               //Natural: MOVE 'PA Select     ' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 010
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(10))))
            {
                decideConditionsMet1069++;
                //*  JH 7/13/00
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Universal Life");                               //Natural: MOVE 'Universal Life' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 011
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(11))))
            {
                decideConditionsMet1069++;
                //*  JWO 10/01/09
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Dental        ");                               //Natural: MOVE 'Dental        ' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: VALUE 012
            else if (condition((pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(12))))
            {
                decideConditionsMet1069++;
                //*  JB01 02/25/11
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue("Canadian Tax  ");                               //Natural: MOVE 'Canadian Tax  ' TO #NC-DEDUCTION-DESC ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Code().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Ded_Cde().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE DED-CDE ( #I ) TO #NC-DEDUCTION-CODE ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE DED-PRIOR-AMT ( #I ) TO #NC-PREV-DED-AMT ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE DED-CURR-AMT ( #I ) TO #NC-CURR-DED-AMT ( #I )
            pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt.nadd(pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue(pnd_Other_Variables_Pnd_I));                                      //Natural: ASSIGN #WS-DED-PREV-AMT := #WS-DED-PREV-AMT + DED-PRIOR-AMT ( #I )
            pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt.nadd(pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue(pnd_Other_Variables_Pnd_I));                                       //Natural: ASSIGN #WS-DED-CURR-AMT := #WS-DED-CURR-AMT + DED-CURR-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Fund_Info() throws Exception                                                                                                                     //Natural: GET-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //*          >>>>>          CHECK ALSO IF CREF/TIAA          <<<<<*
        //* ***********************************************************************
        pnd_Other_Variables_Pnd_Ws_Zero_Value.setValue("N");                                                                                                              //Natural: MOVE 'N' TO #WS-ZERO-VALUE
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Ins_Type().setValue(pdaIaaa323.getIaaa320_Cntrct_Annty_Ins_Type());                                                  //Natural: ASSIGN #CNTRCT-ANNTY-INS-TYPE := CNTRCT-ANNTY-INS-TYPE
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 FUND-CNT
        for (pnd_Other_Variables_Pnd_I.setValue(1); condition(pnd_Other_Variables_Pnd_I.lessOrEqual(pdaIaaa323.getIaaa320_Fund_Cnt())); pnd_Other_Variables_Pnd_I.nadd(1))
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method().getValue(pnd_Other_Variables_Pnd_I).reset();                                                 //Natural: RESET #NC-FUND-PAYMENT-METHOD ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue(" ");                                                     //Natural: MOVE ' ' TO #NC-FUND-DESC ( #I ) #NC-VALUATION-PERIOD ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Valuation_Period().getValue(pnd_Other_Variables_Pnd_I).setValue(" ");
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Code().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-ACCOUNT-CDE-N ( #I ) TO #NC-FUND-CODE ( #I )
            //*  DIV PAY TO COLLEGE
            if (condition(pnd_Accept_Pin_Set_Pnd_Temp_Zero_Div_Amt.equals("Y")))                                                                                          //Natural: IF #TEMP-ZERO-DIV-AMT = 'Y'
            {
                pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(0);                                                          //Natural: MOVE 0 TO FUND-PRIOR-DIVIDEND-AMT ( #I ) FUND-CURR-DIVIDEND-AMT ( #I )
                pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(0);
                pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-PRIOR-CONTRACT-AMT ( #I ) TO FUND-PRIOR-SETTLEMENT-AMT ( #I )
                pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-CURR-CONTRACT-AMT ( #I ) TO FUND-CURR-SETTLEMENT-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Other_Variables_Pnd_I));                    //Natural: ASSIGN #INV-ACCT-INPUT := FUND-ACCOUNT-CDE-A ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_Other_Variables_Pnd_I));            //Natural: ASSIGN #INV-ACCT-VALUAT-PERIOD := FUND-VALUAT-PERIOD ( #I )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' #FUND-PDA
            if (condition(Global.isEscape())) return;
            //*  --------------------                   JHH 10/03/2000
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1().equals("Stock Index")))                                                                          //Natural: IF #STMNT-LINE-1 = 'Stock Index'
            {
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2().reset();                                                                                                   //Natural: RESET #STMNT-LINE-2
            }                                                                                                                                                             //Natural: END-IF
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Desc().getValue(pnd_Other_Variables_Pnd_I).setValue(DbsUtil.compress(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1(),  //Natural: COMPRESS #STMNT-LINE-1 #STMNT-LINE-2 INTO #NC-FUND-DESC ( #I )
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2()));
            //*  -----------------------------------
            //*  TRADITIONAL STANDARD & GROUP
            short decideConditionsMet1136 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FUND-ACCOUNT-CDE-A ( #I );//Natural: VALUE 'T ','G '
            if (condition((pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Other_Variables_Pnd_I).equals("T ") || pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Other_Variables_Pnd_I).equals("G "))))
            {
                decideConditionsMet1136++;
                //*  FIXED
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Type().getValue(pnd_Other_Variables_Pnd_I).setValue("F");                                                 //Natural: MOVE 'F' TO #NC-FUND-TYPE ( #I )
                //*  STANDARD
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method().getValue(pnd_Other_Variables_Pnd_I).setValue("S");                                       //Natural: MOVE 'S' TO #NC-FUND-PAYMENT-METHOD ( #I )
                //*  TRADITIONAL GRADED
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-AMOUNTS
                sub_Move_Tiaa_Amounts();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'TG'
            else if (condition((pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Other_Variables_Pnd_I).equals("TG"))))
            {
                decideConditionsMet1136++;
                //*  FIXED
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Type().getValue(pnd_Other_Variables_Pnd_I).setValue("F");                                                 //Natural: MOVE 'F' TO #NC-FUND-TYPE ( #I )
                //*  GRADED JH 10/16/00
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method().getValue(pnd_Other_Variables_Pnd_I).setValue("G");                                       //Natural: MOVE 'G' TO #NC-FUND-PAYMENT-METHOD ( #I )
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-AMOUNTS
                sub_Move_Tiaa_Amounts();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I)))) //Natural: IF FUND-PRIOR-CONTRACT-AMT ( #I ) NOT = FUND-CURR-CONTRACT-AMT ( #I )
                {
                    pnd_Other_Variables_Pnd_Ws_Tg_Chg_Contract.setValue("Y");                                                                                             //Natural: MOVE 'Y' TO #WS-TG-CHG-CONTRACT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                //*  VARIABLE ..
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Type().getValue(pnd_Other_Variables_Pnd_I).setValue("V");                                                 //Natural: MOVE 'V' TO #NC-FUND-TYPE ( #I )
                //*  .. NON TIAA ONLY
                                                                                                                                                                          //Natural: PERFORM CHECK-UNIT-VALUE
                sub_Check_Unit_Value();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  -------------------------------------  END 9/11/2000
            //*  MM 05/10/01 BEGIN
            //*  FIXED
            if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Type().getValue(pnd_Other_Variables_Pnd_I).equals("F")))                                        //Natural: IF #NC-FUND-TYPE ( #I ) = 'F'
            {
                //*  PRINCIPAL AND INTEREST
                short decideConditionsMet1166 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF IAAA320.CNTRCT-OPTION-CDE;//Natural: VALUE 22
                if (condition((pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(22))))
                {
                    decideConditionsMet1166++;
                    //*  INTEREST ONLY
                    ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method().getValue(pnd_Other_Variables_Pnd_I).setValue("P");                                   //Natural: MOVE 'P' TO #NC-FUND-PAYMENT-METHOD ( #I )
                }                                                                                                                                                         //Natural: VALUE 25,27
                else if (condition((pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(25) || pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(27))))
                {
                    decideConditionsMet1166++;
                    ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method().getValue(pnd_Other_Variables_Pnd_I).setValue("I");                                   //Natural: MOVE 'I' TO #NC-FUND-PAYMENT-METHOD ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  MM 05/10/01 END
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt().nadd(pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue(pnd_Other_Variables_Pnd_I));    //Natural: ADD FUND-PRIOR-NET-PYMNT-AMT ( #I ) TO #NC-FUND-PREV-NET-AMT
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt().nadd(pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue(pnd_Other_Variables_Pnd_I));     //Natural: ADD FUND-CURR-NET-PYMNT-AMT ( #I ) TO #NC-FUND-CURR-NET-AMT
            pnd_Other_Variables_Pnd_Ws_Fund_Prev_Accum_Amt.nadd(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt());                                          //Natural: ASSIGN #WS-FUND-PREV-ACCUM-AMT := #WS-FUND-PREV-ACCUM-AMT + #NC-FUND-PREV-NET-AMT
            pnd_Other_Variables_Pnd_Ws_Fund_Curr_Accum_Amt.nadd(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt());                                          //Natural: ASSIGN #WS-FUND-CURR-ACCUM-AMT := #WS-FUND-CURR-ACCUM-AMT + #NC-FUND-CURR-NET-AMT
            pnd_Other_Variables_Pnd_Ws_Fund_Prev_Gross_Amt.nadd(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt().getValue(pnd_Other_Variables_Pnd_I));    //Natural: ASSIGN #WS-FUND-PREV-GROSS-AMT := #WS-FUND-PREV-GROSS-AMT + #NC-FUND-PREV-TOTAL-AMT ( #I )
            pnd_Other_Variables_Pnd_Ws_Fund_Curr_Gross_Amt.nadd(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt().getValue(pnd_Other_Variables_Pnd_I));    //Natural: ASSIGN #WS-FUND-CURR-GROSS-AMT := #WS-FUND-CURR-GROSS-AMT + #NC-FUND-CURR-TOTAL-AMT ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pnd_Other_Variables_Pnd_Ws_Fund_Prev_Gross_Amt); //Natural: MOVE #WS-FUND-PREV-GROSS-AMT TO #NC-FUND-PREV-GROSS-AMT ( #I )
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pnd_Other_Variables_Pnd_Ws_Fund_Curr_Gross_Amt); //Natural: MOVE #WS-FUND-CURR-GROSS-AMT TO #NC-FUND-CURR-GROSS-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Unit_Value() throws Exception                                                                                                                  //Natural: CHECK-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_Other_Variables_Pnd_I).equals("A")))                                                        //Natural: IF FUND-VALUAT-PERIOD ( #I ) = 'A'
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Valuation_Period().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-VALUAT-PERIOD ( #I ) TO #NC-VALUATION-PERIOD ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Valuation_Period().getValue(pnd_Other_Variables_Pnd_I).setValue("M");                                              //Natural: MOVE 'M' TO #NC-VALUATION-PERIOD ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-PRIOR-UNIT-QTY ( #I ) TO #NC-FUND-PREV-UNITS ( #I )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-PRIOR-SETTLEMENT-AMT ( #I ) TO #NC-FUND-PREV-TOTAL-AMT ( #I )
        if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Other_Variables_Pnd_I).greater(getZero()) && pdaIaaa323.getIaaa320_Fund_Prior_Unit_Value().getValue(pnd_Other_Variables_Pnd_I).equals(getZero()))) //Natural: IF FUND-PRIOR-UNIT-QTY ( #I ) > 0 AND FUND-PRIOR-UNIT-VALUE ( #I ) = 0
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value().getValue(pnd_Other_Variables_Pnd_I).setValue(0);                                            //Natural: MOVE 0 TO #NC-FUND-PREV-UNIT-VALUE ( #I )
            pnd_Other_Variables_Pnd_Ws_Zero_Value.setValue("Y");                                                                                                          //Natural: MOVE 'Y' TO #WS-ZERO-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Prior_Unit_Value().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-PRIOR-UNIT-VALUE ( #I ) TO #NC-FUND-PREV-UNIT-VALUE ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-CURR-UNIT-QTY ( #I ) TO #NC-FUND-CURR-UNITS ( #I )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-CURR-SETTLEMENT-AMT ( #I ) TO #NC-FUND-CURR-TOTAL-AMT ( #I )
        if (condition(pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Other_Variables_Pnd_I).greater(getZero()) && pdaIaaa323.getIaaa320_Fund_Curr_Unit_Value().getValue(pnd_Other_Variables_Pnd_I).equals(getZero()))) //Natural: IF FUND-CURR-UNIT-QTY ( #I ) > 0 AND FUND-CURR-UNIT-VALUE ( #I ) = 0
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value().getValue(pnd_Other_Variables_Pnd_I).setValue(0);                                            //Natural: MOVE 0 TO #NC-FUND-CURR-UNIT-VALUE ( #I )
            pnd_Other_Variables_Pnd_Ws_Zero_Value.setValue("Y");                                                                                                          //Natural: MOVE 'Y' TO #WS-ZERO-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Unit_Value().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-CURR-UNIT-VALUE ( #I ) TO #NC-FUND-CURR-UNIT-VALUE ( #I )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Tiaa_Amounts() throws Exception                                                                                                                 //Natural: MOVE-TIAA-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-PRIOR-CONTRACT-AMT ( #I ) TO #NC-FUND-PREV-CONTRACT-AMT ( #I )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-PRIOR-DIVIDEND-AMT ( #I ) TO #NC-FUND-PREV-DIVIDEND ( #I )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-PRIOR-SETTLEMENT-AMT ( #I ) TO #NC-FUND-PREV-TOTAL-AMT ( #I )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-CURR-CONTRACT-AMT ( #I ) TO #NC-FUND-CURR-CONTRACT-AMT ( #I )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-CURR-DIVIDEND-AMT ( #I ) TO #NC-FUND-CURR-DIVIDEND ( #I )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt().getValue(pnd_Other_Variables_Pnd_I).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Other_Variables_Pnd_I)); //Natural: MOVE FUND-CURR-SETTLEMENT-AMT ( #I ) TO #NC-FUND-CURR-TOTAL-AMT ( #I )
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().setValue(0);                                                                                         //Natural: MOVE 0 TO #NC-NBR-OF-CONTRACT #NC-PIN-NBR
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Nbr().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #NC-CONTRACT-NBR #NC-PAYEE-CODE #NC-CONTRACT-TYPE #NC-HOLD-CODE #NC-LABEL-HOLD-IND #NC-FULL-NAME #NC-ADDRESS-1 ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Payee_Code().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Type().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Hold_Code().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Full_Name().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue("*").setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code().getValue("*").setValue(0);                                                                           //Natural: MOVE 0 TO #NC-GTN-REPORT-CODE ( * ) #NC-PREV-CHECK-DATE #NC-CURR-CHECK-DATE
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Date().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Date().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #NC-BIN-NBR-2 #NC-BIN-NBR-3 #NC-BIN-NBR-4 #NC-BIN-NBR-5 #NC-POSTNET-ZIP
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Postnet_Zip().setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Count().setValue(0);                                                                                         //Natural: MOVE 0 TO #NC-DEDUCTION-COUNT #NC-DEDUCTION-CODE ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Code().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue("*").setValue(" ");                                                                          //Natural: MOVE ' ' TO #NC-DEDUCTION-DESC ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt().getValue("*").setValue(0);                                                                              //Natural: MOVE 0 TO #NC-PREV-DED-AMT ( * ) #NC-CURR-DED-AMT ( * ) #NC-PREV-FED-AMT #NC-PREV-ST-AMT #NC-PREV-LOC-AMT #NC-CURR-FED-AMT #NC-CURR-ST-AMT #NC-CURR-LOC-AMT #NC-PREV-TOTAL-DEDUCTION #NC-CURR-TOTAL-DEDUCTION #NC-FUND-PREV-NET-AMT #NC-FUND-CURR-NET-AMT #NC-FUND-CODE ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_St_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_St_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Total_Deduction().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Total_Deduction().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Code().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Desc().getValue("*").setValue(" ");                                                                               //Natural: MOVE ' ' TO #NC-FUND-DESC ( * ) #NC-VALUATION-PERIOD ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Valuation_Period().getValue("*").setValue(" ");
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units().getValue("*").setValue(0);                                                                           //Natural: MOVE 0 TO #NC-FUND-PREV-UNITS ( * ) #NC-FUND-PREV-UNIT-VALUE ( * ) #NC-FUND-CURR-UNIT-VALUE ( * ) #NC-FUND-CURR-UNITS ( * ) #NC-FUND-PREV-CONTRACT-AMT ( * ) #NC-FUND-PREV-DIVIDEND ( * ) #NC-FUND-PREV-TOTAL-AMT ( * ) #NC-FUND-PREV-GROSS-AMT ( * ) #NC-FUND-CURR-CONTRACT-AMT ( * ) #NC-FUND-CURR-DIVIDEND ( * ) #NC-FUND-CURR-TOTAL-AMT ( * ) #NC-FUND-CURR-GROSS-AMT ( * ) #NC-MESSAGES-CTR #NC-MESSAGE-NUMBER ( * ) #NC-MESSAGE-DATE-1 ( * ) #NC-MESSAGE-DATE-2 ( * ) #NC-MESSAGE-PERCENT ( * )
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages_Ctr().setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Date_1().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Date_2().getValue("*").setValue(0);
        ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Percent().getValue("*").setValue(0);
        pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt.reset();                                                                                                                  //Natural: RESET #WS-DED-PREV-AMT #WS-DED-CURR-AMT #WS-FUND-PREV-ACCUM-AMT #WS-FUND-CURR-ACCUM-AMT #WS-FUND-PREV-GROSS-AMT #WS-FUND-CURR-GROSS-AMT
        pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt.reset();
        pnd_Other_Variables_Pnd_Ws_Fund_Prev_Accum_Amt.reset();
        pnd_Other_Variables_Pnd_Ws_Fund_Curr_Accum_Amt.reset();
        pnd_Other_Variables_Pnd_Ws_Fund_Prev_Gross_Amt.reset();
        pnd_Other_Variables_Pnd_Ws_Fund_Curr_Gross_Amt.reset();
    }
    private void sub_Evaluate_Gtn_Rpt_Code() throws Exception                                                                                                             //Natural: EVALUATE-GTN-RPT-CODE
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR #I 1 10
        for (pnd_Other_Variables_Pnd_I.setValue(1); condition(pnd_Other_Variables_Pnd_I.lessOrEqual(10)); pnd_Other_Variables_Pnd_I.nadd(1))
        {
            //*  BLANK OR ZERO PIN
            short decideConditionsMet1247 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF GTN-REPORT-CDE ( #I );//Natural: VALUE 12
            if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(12))))
            {
                decideConditionsMet1247++;
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue("  PN");                                                                                                    //Natural: ASSIGN #TEMP-HOLD-ORG := '  PN'
                //*  DIVIDEND PAID TO COLLEGES
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                                       //Natural: MOVE 'H' TO #TEMP-HOLD-IND
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(7))))
            {
                decideConditionsMet1247++;
                if (condition(pdaIaaa323.getIaaa320_Combined_Pymnt_Ind().equals("D") || pdaIaaa323.getIaaa320_Combined_Pymnt_Ind().equals("B")))                          //Natural: IF COMBINED-PYMNT-IND = 'D' OR = 'B'
                {
                    pnd_Accept_Pin_Set_Pnd_Temp_Zero_Div_Amt.setValue("Y");                                                                                               //Natural: MOVE 'Y' TO #TEMP-ZERO-DIV-AMT
                    //*  CONTRACT WITH IRS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(6))))
            {
                decideConditionsMet1247++;
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue("  IR");                                                                                                    //Natural: ASSIGN #TEMP-HOLD-ORG := '  IR'
                //*  HOLD PIN SET
                //*  TAXES & DEDUCTIONS NOT WITHHELD
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                                       //Natural: MOVE 'H' TO #TEMP-HOLD-IND
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(5))))
            {
                decideConditionsMet1247++;
                pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt().setValue(0);                                                                                                    //Natural: MOVE 0 TO TAX-PRIOR-FED-AMT TAX-PRIOR-LOC-AMT TAX-PRIOR-ST-AMT TAX-CURR-FED-AMT TAX-CURR-LOC-AMT TAX-CURR-ST-AMT DEDUCTION-CNT DED-PRIOR-AMT ( * ) DED-CURR-AMT ( * )
                pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt().setValue(0);
                pdaIaaa323.getIaaa320_Tax_Prior_St_Amt().setValue(0);
                pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt().setValue(0);
                pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt().setValue(0);
                pdaIaaa323.getIaaa320_Tax_Curr_St_Amt().setValue(0);
                pdaIaaa323.getIaaa320_Deduction_Cnt().setValue(0);
                pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue("*").setValue(0);
                pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue("*").setValue(0);
                //*  HOLD PIN SET
                //*  DEDUCTIONS NOT WITHHELD
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                                       //Natural: MOVE 'H' TO #TEMP-HOLD-IND
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue("  TX");                                                                                                    //Natural: ASSIGN #TEMP-HOLD-ORG := '  TX'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(4))))
            {
                decideConditionsMet1247++;
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue("  DE");                                                                                                    //Natural: ASSIGN #TEMP-HOLD-ORG := '  DE'
                //*  HOLD PIN SET
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                                       //Natural: MOVE 'H' TO #TEMP-HOLD-IND
                //*  TAXES NOT WITHHELD
                pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue("*").setValue(0);                                                                                          //Natural: MOVE 0 TO DED-PRIOR-AMT ( * ) DED-CURR-AMT ( * ) DEDUCTION-CNT
                pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue("*").setValue(0);
                pdaIaaa323.getIaaa320_Deduction_Cnt().setValue(0);
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(3))))
            {
                decideConditionsMet1247++;
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue("  FS");                                                                                                    //Natural: ASSIGN #TEMP-HOLD-ORG := '  FS'
                //*  HOLD PIN SET
                //*  INTERNAL ROLLVER
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                                       //Natural: MOVE 'H' TO #TEMP-HOLD-IND
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(2))))
            {
                decideConditionsMet1247++;
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue("  RV");                                                                                                    //Natural: ASSIGN #TEMP-HOLD-ORG := '  RV'
                //*  HOLD PIN SET
                //*  ZERO CHECKS
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                                       //Natural: MOVE 'H' TO #TEMP-HOLD-IND
            }                                                                                                                                                             //Natural: VALUE 1
            else if (condition((pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue(pnd_Other_Variables_Pnd_I).equals(1))))
            {
                decideConditionsMet1247++;
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue("  ZR");                                                                                                    //Natural: ASSIGN #TEMP-HOLD-ORG := '  ZR'
                //*  HOLD PIN SET
                pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("H");                                                                                                       //Natural: MOVE 'H' TO #TEMP-HOLD-IND
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Annuity_Certain() throws Exception                                                                                                              //Natural: READ-ANNUITY-CERTAIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().read(3, pnd_Annuity_Certain_Table);                                                                                                                //Natural: READ WORK FILE 3 ONCE #ANNUITY-CERTAIN-TABLE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Other_Variables_Pnd_Match.setValue(1);                                                                                                                    //Natural: MOVE 1 TO #MATCH
            pnd_Other_Variables_Pnd_Hi_Values_Annuity.setValue(1);                                                                                                        //Natural: MOVE 1 TO #HI-VALUES-ANNUITY
            pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr_A.setValue(constants_Pnd_Hi_Values);                                                                                 //Natural: MOVE #HI-VALUES TO #AC-PIN-NBR-A
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Check_Annuity_Certain() throws Exception                                                                                                             //Natural: CHECK-ANNUITY-CERTAIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Other_Variables_Pnd_Match.setValue(1);                                                                                                                        //Natural: MOVE 1 TO #MATCH
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Other_Variables_Pnd_Hi_Values_Annuity.equals(1)))                                                                                           //Natural: IF #HI-VALUES-ANNUITY = 1
            {
                //*   ANNUITY CERTAIN EOF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr_A.equals(constants_Pnd_Hi_Values)))                                                                    //Natural: IF #AC-PIN-NBR-A = #HI-VALUES
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr.less(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                                //Natural: IF #AC-PIN-NBR < IAAA320.PIN-NBR
            {
                                                                                                                                                                          //Natural: PERFORM READ-ANNUITY-CERTAIN
                sub_Read_Annuity_Certain();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr.greater(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                             //Natural: IF #AC-PIN-NBR > IAAA320.PIN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr.equals(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                              //Natural: IF #AC-PIN-NBR = IAAA320.PIN-NBR
            {
                if (condition(pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Payee_Cde.equals(pnd_Other_Variables_Pnd_Ws_Payee_Cde_2)))                                          //Natural: IF #AC-CNTRCT-PAYEE-CDE = #WS-PAYEE-CDE-2
                {
                    pnd_Other_Variables_Pnd_Ws_Ac_Pin.setValue(pnd_Annuity_Certain_Table_Pnd_Ac_Pin_Nbr);                                                                 //Natural: ASSIGN #WS-AC-PIN := #AC-PIN-NBR
                    pnd_Other_Variables_Pnd_Ws_Ac_Payee_Cde_2.setValue(pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Payee_Cde);                                                //Natural: ASSIGN #WS-AC-PAYEE-CDE-2 := #AC-CNTRCT-PAYEE-CDE
                    pnd_Other_Variables_Pnd_Match.setValue(0);                                                                                                            //Natural: MOVE 0 TO #MATCH
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Payee_Cde.less(pnd_Other_Variables_Pnd_Ws_Payee_Cde_2)))                                            //Natural: IF #AC-CNTRCT-PAYEE-CDE < #WS-PAYEE-CDE-2
                {
                                                                                                                                                                          //Natural: PERFORM READ-ANNUITY-CERTAIN
                    sub_Read_Annuity_Certain();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Annuity_Certain_Table_Pnd_Ac_Cntrct_Payee_Cde.greater(pnd_Other_Variables_Pnd_Ws_Payee_Cde_2)))                                         //Natural: IF #AC-CNTRCT-PAYEE-CDE > #WS-PAYEE-CDE-2
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Decide_Accept_Pin() throws Exception                                                                                                                 //Natural: DECIDE-ACCEPT-PIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Other_Variables_Pnd_Ws_Changed.equals(getZero())))                                                                                              //Natural: IF #WS-CHANGED = 0
        {
            //*  CREF OR WITH CHANGES
            short decideConditionsMet1344 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #CR-PARM-TYPE;//Natural: VALUE 'C'
            if (condition((pnd_Ctrl_Record_Pnd_Cr_Parm_Type.equals("C"))))
            {
                decideConditionsMet1344++;
                //*  NO CREF BYPASS
                if (condition(pnd_Other_Variables_Pnd_Cref_Only.equals(" ")))                                                                                             //Natural: IF #CREF-ONLY = ' '
                {
                    pnd_Other_Variables_Pnd_Ws_Bypass.setValue(1);                                                                                                        //Natural: MOVE 1 TO #WS-BYPASS
                    //*  TIAA OR WITH CHANGES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Ctrl_Record_Pnd_Cr_Parm_Type.equals("T"))))
            {
                decideConditionsMet1344++;
                //*  NO TIAA BYPASS
                if (condition(pnd_Other_Variables_Pnd_Tiaa_Only.equals(" ")))                                                                                             //Natural: IF #TIAA-ONLY = ' '
                {
                    pnd_Other_Variables_Pnd_Ws_Bypass.setValue(1);                                                                                                        //Natural: MOVE 1 TO #WS-BYPASS
                    //*  NO CHANGE OR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '0'
            else if (condition((pnd_Ctrl_Record_Pnd_Cr_Parm_Type.equals("0"))))
            {
                decideConditionsMet1344++;
                //*  FIRST PASS
                if (condition(pnd_Other_Variables_Pnd_First.equals(getZero())))                                                                                           //Natural: IF #FIRST = 0
                {
                    pnd_Other_Variables_Pnd_First.setValue(1);                                                                                                            //Natural: MOVE 1 TO #FIRST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Other_Variables_Pnd_Ws_Date_For_Label_Check.setValue(pdaIaaa323.getIaaa320_Curr_Check_Dte());                                                     //Natural: MOVE CURR-CHECK-DTE TO #WS-DATE-FOR-LABEL-CHECK
                    //*  JAN & MAY
                    //*  NO CHANGE CONTRACTS
                    //*  GOES TO LABELS
                    if (condition((pnd_Other_Variables_Pnd_Ws_Date_Label_Mm.equals(1) || pnd_Other_Variables_Pnd_Ws_Date_Label_Mm.equals(5))))                            //Natural: IF ( #WS-DATE-LABEL-MM = 1 OR #WS-DATE-LABEL-MM = 5 )
                    {
                        pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue("L");                                                                                               //Natural: ASSIGN #TEMP-HOLD-IND := 'L'
                        pnd_Other_Variables_Pnd_Ws_Bypass.setValue(0);                                                                                                    //Natural: ASSIGN #WS-BYPASS := 0
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  BYPASS
                        pnd_Other_Variables_Pnd_Ws_Bypass.setValue(1);                                                                                                    //Natural: MOVE 1 TO #WS-BYPASS
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ACCEPT ALL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PIN SET OK
        if (condition(pnd_Other_Variables_Pnd_Ws_Bypass.equals(getZero())))                                                                                               //Natural: IF #WS-BYPASS = 0
        {
            //*  CHECK IF COMBO  - ROXAN 03/01/2001
            if (condition(pnd_Other_Variables_Pnd_Spia_Paselect.getBoolean()))                                                                                            //Natural: IF #SPIA-PASELECT
            {
                pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia.setValue("1");                                                                                                        //Natural: ASSIGN #TEMP-SPIA-IA := '1'
                if (condition(pnd_Other_Variables_Pnd_Ia_Reg.getBoolean()))                                                                                               //Natural: IF #IA-REG
                {
                    pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia.setValue("2");                                                                                                    //Natural: ASSIGN #TEMP-SPIA-IA := '2'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Accept_Pin_Set_Pnd_Temp_Pin.setValue(pnd_Other_Variables_Pnd_Ws_Save_Pin);                                                                                //Natural: MOVE #WS-SAVE-PIN TO #TEMP-PIN
            pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code.setValue(pnd_Other_Variables_Pnd_Ws_Save_Payee_Cde);                                                                   //Natural: MOVE #WS-SAVE-PAYEE-CDE TO #TEMP-PAYEE-CODE
            pnd_Accept_Pin_Set_Pnd_Temp_Annuity_Ind.setValue(pnd_Other_Variables_Pnd_Ws_Annuity_Ind);                                                                     //Natural: MOVE #WS-ANNUITY-IND TO #TEMP-ANNUITY-IND
            pnd_Accept_Pin_Set_Pnd_Temp_No_Contracts.setValue(pnd_Other_Variables_Pnd_Ws_No_Contracts);                                                                   //Natural: MOVE #WS-NO-CONTRACTS TO #TEMP-NO-CONTRACTS
            getWorkFiles().write(4, false, pnd_Accept_Pin_Set);                                                                                                           //Natural: WRITE WORK FILE 4 #ACCEPT-PIN-SET
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Other_Variables_Pnd_Ws_Save_Pin.setValue(pdaIaaa323.getIaaa320_Pin_Nbr());                                                                                    //Natural: MOVE PIN-NBR TO #WS-SAVE-PIN
        pnd_Other_Variables_Pnd_Ws_Save_Payee_Cde.setValue(pdaIaaa323.getIaaa320_Payee_Cde());                                                                            //Natural: MOVE PAYEE-CDE TO #WS-SAVE-PAYEE-CDE
        pnd_Accept_Pin_Set_Pnd_Temp_Annuity_Ind.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #TEMP-ANNUITY-IND #TEMP-ZERO-DIV-AMT #TEMP-HOLD-ORG #TEMP-HOLD-IND #TEMP-PAYEE-CODE #TEMP-SPIA-IA #WS-ANNUITY-IND
        pnd_Accept_Pin_Set_Pnd_Temp_Zero_Div_Amt.setValue(" ");
        pnd_Accept_Pin_Set_Pnd_Temp_Hold_Org.setValue(" ");
        pnd_Accept_Pin_Set_Pnd_Temp_Hold_Ind.setValue(" ");
        pnd_Accept_Pin_Set_Pnd_Temp_Payee_Code.setValue(" ");
        pnd_Accept_Pin_Set_Pnd_Temp_Spia_Ia.setValue(" ");
        pnd_Other_Variables_Pnd_Ws_Annuity_Ind.setValue(" ");
        pnd_Accept_Pin_Set_Pnd_Temp_Pin.setValue(0);                                                                                                                      //Natural: MOVE 0 TO #TEMP-PIN #WS-CHANGED #WS-BYPASS #TEMP-NO-CONTRACTS #WS-NO-CONTRACTS
        pnd_Other_Variables_Pnd_Ws_Changed.setValue(0);
        pnd_Other_Variables_Pnd_Ws_Bypass.setValue(0);
        pnd_Accept_Pin_Set_Pnd_Temp_No_Contracts.setValue(0);
        pnd_Other_Variables_Pnd_Ws_No_Contracts.setValue(0);
        //*  JH 7/13/00
        pnd_Other_Variables_Pnd_Cref_Only.reset();                                                                                                                        //Natural: RESET #CREF-ONLY #TIAA-ONLY #MATCH #LIFE-ONLY
        pnd_Other_Variables_Pnd_Tiaa_Only.reset();
        pnd_Other_Variables_Pnd_Match.reset();
        pnd_Other_Variables_Pnd_Life_Only.reset();
        pnd_Other_Variables_Pnd_Spia_Paselect.setValue(false);                                                                                                            //Natural: ASSIGN #SPIA-PASELECT := FALSE
        pnd_Other_Variables_Pnd_Ia_Reg.setValue(false);                                                                                                                   //Natural: ASSIGN #IA-REG := FALSE
    }
    private void sub_Display_Bypass_Record() throws Exception                                                                                                             //Natural: DISPLAY-BYPASS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* * THIS SUBROUTINE PURPOSE IS TO DISPLAY THE TRANSACTION WHICH WILL BE
        //* *  BYPASSED WITH DATA VALUE
        getReports().write(0, "**************************************************************");                                                                          //Natural: WRITE '**************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "-- PROGRAM IIAP3028 - ERROR PROCESSING INVOKED................");                                                                          //Natural: WRITE '-- PROGRAM IIAP3028 - ERROR PROCESSING INVOKED................'
        if (Global.isEscape()) return;
        getReports().write(0, "-- COUNTER OVERFLOW DETAILS OF RECORD BEING BYPASSED BELOW ----");                                                                         //Natural: WRITE '-- COUNTER OVERFLOW DETAILS OF RECORD BEING BYPASSED BELOW ----'
        if (Global.isEscape()) return;
        getReports().write(0, "BYPASSING RECORD FOR...............");                                                                                                     //Natural: WRITE 'BYPASSING RECORD FOR...............'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaIaaa323.getIaaa320_Ph_Mailing_Nme());                                                                                                //Natural: WRITE '=' PH-MAILING-NME
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaIaaa323.getIaaa320_Pin_Nbr(),"=",pdaIaaa323.getIaaa320_Contract_Nbr(),"=",pdaIaaa323.getIaaa320_Payee_Cde(),"=",pdaIaaa323.getIaaa320_Company_Cde()); //Natural: WRITE '=' PIN-NBR '=' CONTRACT-NBR '=' PAYEE-CDE '=' COMPANY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "FIELDS EXAMINED FOR > 99,999.99 OVERFLOW ARE.................");                                                                           //Natural: WRITE 'FIELDS EXAMINED FOR > 99,999.99 OVERFLOW ARE.................'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt(),"=",pdaIaaa323.getIaaa320_Tax_Prior_St_Amt());                                                //Natural: WRITE '=' TAX-PRIOR-FED-AMT '=' TAX-PRIOR-ST-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt(),"=",pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt());                                                //Natural: WRITE '=' TAX-PRIOR-LOC-AMT '=' TAX-CURR-FED-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaIaaa323.getIaaa320_Tax_Curr_St_Amt(),"=",pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt());                                                  //Natural: WRITE '=' TAX-CURR-ST-AMT '=' TAX-CURR-LOC-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Other_Variables_Pnd_Ws_Ded_Prev_Amt,"=",pnd_Other_Variables_Pnd_Ws_Ded_Curr_Amt);                                                   //Natural: WRITE '=' #WS-DED-PREV-AMT '=' #WS-DED-CURR-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "---------------------------------------------------------------");                                                                         //Natural: WRITE '---------------------------------------------------------------'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=151");
    }
}
