/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:10 PM
**        * FROM NATURAL PROGRAM : Iaap381
************************************************************
**        * FILE NAME            : Iaap381.java
**        * CLASS NAME           : Iaap381
**        * INSTANCE NAME        : Iaap381
************************************************************
************************************************************************
* PROGRAM  : RMLSTMD  LIST CNTRACTS THAT WERE ISSUED IN MARYLAND
* FOR SELECTED ISSU-COLLEGE-CODES FOR YEARS 1976 THRU 1995
* HISTORY
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
* 11/2017 JT FIX FOR INCIDENT XXXXXXXXXX. SCAN ON 11/2017 FOR CHANGES
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap381 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Work_Record_In;
    private DbsField pnd_Work_Record_In_Pnd_Contract_Number;
    private DbsField pnd_Work_Record_In_Pnd_Payee_Code;
    private DbsField pnd_Work_Record_In_Pnd_Id_Number;
    private DbsField pnd_Work_Record_In_Pnd_F;
    private DbsField pnd_Work_Record_In_Pnd_Tax_Id_Nbr;

    private DbsGroup pnd_Work_Record_In__R_Field_1;
    private DbsField pnd_Work_Record_In_Pnd_Tax_Nbr;
    private DbsField pnd_Work_Record_In_Pnd_F1;

    private DbsGroup print_Line;
    private DbsField print_Line_Pnd_Ppg;
    private DbsField print_Line_Pnd_Ppcn;
    private DbsField print_Line_Pnd_Ssn;
    private DbsField print_Line_Pnd_F;
    private DbsField print_Line_Pnd_Name;

    private DbsGroup record_Out;

    private DbsGroup record_Out_Ll_Record_Key;

    private DbsGroup record_Out_Ll_Full_Contract;
    private DbsField record_Out_Ll_Contract_No;
    private DbsField record_Out_Ll_Payee_Code;
    private DbsField record_Out_Ll_Sequence_No;
    private DbsField record_Out_Ll_Status_Code;
    private DbsField record_Out_Ll_Effective_Date;
    private DbsField record_Out_Ll_Entry_Date;
    private DbsField record_Out_Ll_Entry_Operator;
    private DbsField record_Out_Ll_Activity_Date;
    private DbsField record_Out_Ll_Activity_Operator;
    private DbsField record_Out_Ll_State_Code;
    private DbsField record_Out_Ll_Ia_State_Code;
    private DbsField record_Out_Ll_State_Name;
    private DbsField record_Out_Ll_Expiration_Date;
    private DbsField record_Out_Filler;

    private DbsGroup print_Line_1;
    private DbsField print_Line_1_Pnd_Ff;
    private DbsField print_Line_1_Pnd_Addr;
    private DbsField tax_Key;

    private DbsGroup tax_Key__R_Field_2;
    private DbsField tax_Key_Pnd_Ppcn1;
    private DbsField tax_Key_Pnd_Pay_Cd;
    private DbsField pnd_Cn;

    private DataAccessProgramView vw_twr_Form_Receipt;
    private DbsField twr_Form_Receipt_Tin_Type;
    private DbsField twr_Form_Receipt_Tin;
    private DbsField twr_Form_Receipt_Active_Ind;
    private DbsField twr_Form_Receipt_Lgo_Eff_Dte;
    private DbsField twr_Form_Receipt_Lgo_Res_Cde;

    private DbsGroup twr_Form_Receipt__R_Field_3;
    private DbsField twr_Form_Receipt_Pnd_Lgo_R1;
    private DbsField twr_Form_Receipt_Pnd_Lgo_R2;
    private DbsField twr_Form_Receipt_Pnd_Lgo_R3;

    private DbsGroup twr_Form_Receipt__R_Field_4;
    private DbsField twr_Form_Receipt__Filler1;
    private DbsField twr_Form_Receipt_Pnd_Lgo_R12;
    private DbsField twr_Form_Receipt_Lgo_Exp_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Work_Record_In = localVariables.newGroupInRecord("pnd_Work_Record_In", "#WORK-RECORD-IN");
        pnd_Work_Record_In_Pnd_Contract_Number = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_Contract_Number", "#CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Work_Record_In_Pnd_Payee_Code = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_Payee_Code", "#PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_In_Pnd_Id_Number = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_Id_Number", "#ID-NUMBER", FieldType.NUMERIC, 12);
        pnd_Work_Record_In_Pnd_F = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_F", "#F", FieldType.STRING, 6);
        pnd_Work_Record_In_Pnd_Tax_Id_Nbr = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 
            9);

        pnd_Work_Record_In__R_Field_1 = pnd_Work_Record_In.newGroupInGroup("pnd_Work_Record_In__R_Field_1", "REDEFINE", pnd_Work_Record_In_Pnd_Tax_Id_Nbr);
        pnd_Work_Record_In_Pnd_Tax_Nbr = pnd_Work_Record_In__R_Field_1.newFieldInGroup("pnd_Work_Record_In_Pnd_Tax_Nbr", "#TAX-NBR", FieldType.STRING, 
            9);
        pnd_Work_Record_In_Pnd_F1 = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_F1", "#F1", FieldType.STRING, 41);

        print_Line = localVariables.newGroupInRecord("print_Line", "PRINT-LINE");
        print_Line_Pnd_Ppg = print_Line.newFieldInGroup("print_Line_Pnd_Ppg", "#PPG", FieldType.STRING, 4);
        print_Line_Pnd_Ppcn = print_Line.newFieldInGroup("print_Line_Pnd_Ppcn", "#PPCN", FieldType.STRING, 10);
        print_Line_Pnd_Ssn = print_Line.newFieldInGroup("print_Line_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 9);
        print_Line_Pnd_F = print_Line.newFieldInGroup("print_Line_Pnd_F", "#F", FieldType.STRING, 1);
        print_Line_Pnd_Name = print_Line.newFieldInGroup("print_Line_Pnd_Name", "#NAME", FieldType.STRING, 25);

        record_Out = localVariables.newGroupInRecord("record_Out", "RECORD-OUT");

        record_Out_Ll_Record_Key = record_Out.newGroupInGroup("record_Out_Ll_Record_Key", "LL-RECORD-KEY");

        record_Out_Ll_Full_Contract = record_Out_Ll_Record_Key.newGroupInGroup("record_Out_Ll_Full_Contract", "LL-FULL-CONTRACT");
        record_Out_Ll_Contract_No = record_Out_Ll_Full_Contract.newFieldInGroup("record_Out_Ll_Contract_No", "LL-CONTRACT-NO", FieldType.STRING, 8);
        record_Out_Ll_Payee_Code = record_Out_Ll_Full_Contract.newFieldInGroup("record_Out_Ll_Payee_Code", "LL-PAYEE-CODE", FieldType.NUMERIC, 2);
        record_Out_Ll_Sequence_No = record_Out_Ll_Record_Key.newFieldInGroup("record_Out_Ll_Sequence_No", "LL-SEQUENCE-NO", FieldType.NUMERIC, 3);
        record_Out_Ll_Status_Code = record_Out.newFieldInGroup("record_Out_Ll_Status_Code", "LL-STATUS-CODE", FieldType.STRING, 1);
        record_Out_Ll_Effective_Date = record_Out.newFieldInGroup("record_Out_Ll_Effective_Date", "LL-EFFECTIVE-DATE", FieldType.STRING, 8);
        record_Out_Ll_Entry_Date = record_Out.newFieldInGroup("record_Out_Ll_Entry_Date", "LL-ENTRY-DATE", FieldType.STRING, 8);
        record_Out_Ll_Entry_Operator = record_Out.newFieldInGroup("record_Out_Ll_Entry_Operator", "LL-ENTRY-OPERATOR", FieldType.STRING, 4);
        record_Out_Ll_Activity_Date = record_Out.newFieldInGroup("record_Out_Ll_Activity_Date", "LL-ACTIVITY-DATE", FieldType.STRING, 8);
        record_Out_Ll_Activity_Operator = record_Out.newFieldInGroup("record_Out_Ll_Activity_Operator", "LL-ACTIVITY-OPERATOR", FieldType.STRING, 4);
        record_Out_Ll_State_Code = record_Out.newFieldInGroup("record_Out_Ll_State_Code", "LL-STATE-CODE", FieldType.STRING, 2);
        record_Out_Ll_Ia_State_Code = record_Out.newFieldInGroup("record_Out_Ll_Ia_State_Code", "LL-IA-STATE-CODE", FieldType.STRING, 2);
        record_Out_Ll_State_Name = record_Out.newFieldInGroup("record_Out_Ll_State_Name", "LL-STATE-NAME", FieldType.STRING, 17);
        record_Out_Ll_Expiration_Date = record_Out.newFieldInGroup("record_Out_Ll_Expiration_Date", "LL-EXPIRATION-DATE", FieldType.STRING, 6);
        record_Out_Filler = record_Out.newFieldInGroup("record_Out_Filler", "FILLER", FieldType.STRING, 7);

        print_Line_1 = localVariables.newGroupInRecord("print_Line_1", "PRINT-LINE-1");
        print_Line_1_Pnd_Ff = print_Line_1.newFieldInGroup("print_Line_1_Pnd_Ff", "#FF", FieldType.STRING, 53);
        print_Line_1_Pnd_Addr = print_Line_1.newFieldInGroup("print_Line_1_Pnd_Addr", "#ADDR", FieldType.STRING, 25);
        tax_Key = localVariables.newFieldInRecord("tax_Key", "TAX-KEY", FieldType.STRING, 10);

        tax_Key__R_Field_2 = localVariables.newGroupInRecord("tax_Key__R_Field_2", "REDEFINE", tax_Key);
        tax_Key_Pnd_Ppcn1 = tax_Key__R_Field_2.newFieldInGroup("tax_Key_Pnd_Ppcn1", "#PPCN1", FieldType.STRING, 8);
        tax_Key_Pnd_Pay_Cd = tax_Key__R_Field_2.newFieldInGroup("tax_Key_Pnd_Pay_Cd", "#PAY-CD", FieldType.NUMERIC, 2);
        pnd_Cn = localVariables.newFieldInRecord("pnd_Cn", "#CN", FieldType.NUMERIC, 9);

        vw_twr_Form_Receipt = new DataAccessProgramView(new NameInfo("vw_twr_Form_Receipt", "TWR-FORM-RECEIPT"), "TWR_FORM_RECEIPT", "TWR_FORM_RECEIPT");
        twr_Form_Receipt_Tin_Type = vw_twr_Form_Receipt.getRecord().newFieldInGroup("twr_Form_Receipt_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIN_TYPE");
        twr_Form_Receipt_Tin_Type.setDdmHeader("TIN/TYPE");
        twr_Form_Receipt_Tin = vw_twr_Form_Receipt.getRecord().newFieldInGroup("twr_Form_Receipt_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIN");
        twr_Form_Receipt_Tin.setDdmHeader("TAX/ID/NUMBER");
        twr_Form_Receipt_Active_Ind = vw_twr_Form_Receipt.getRecord().newFieldInGroup("twr_Form_Receipt_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ACTIVE_IND");
        twr_Form_Receipt_Active_Ind.setDdmHeader("ACT/IND");
        twr_Form_Receipt_Lgo_Eff_Dte = vw_twr_Form_Receipt.getRecord().newFieldInGroup("twr_Form_Receipt_Lgo_Eff_Dte", "LGO-EFF-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "LGO_EFF_DTE");
        twr_Form_Receipt_Lgo_Eff_Dte.setDdmHeader("LGO/EFF/DTE");
        twr_Form_Receipt_Lgo_Res_Cde = vw_twr_Form_Receipt.getRecord().newFieldInGroup("twr_Form_Receipt_Lgo_Res_Cde", "LGO-RES-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "LGO_RES_CDE");
        twr_Form_Receipt_Lgo_Res_Cde.setDdmHeader("LGO/RES/CDE");

        twr_Form_Receipt__R_Field_3 = vw_twr_Form_Receipt.getRecord().newGroupInGroup("twr_Form_Receipt__R_Field_3", "REDEFINE", twr_Form_Receipt_Lgo_Res_Cde);
        twr_Form_Receipt_Pnd_Lgo_R1 = twr_Form_Receipt__R_Field_3.newFieldInGroup("twr_Form_Receipt_Pnd_Lgo_R1", "#LGO-R1", FieldType.STRING, 1);
        twr_Form_Receipt_Pnd_Lgo_R2 = twr_Form_Receipt__R_Field_3.newFieldInGroup("twr_Form_Receipt_Pnd_Lgo_R2", "#LGO-R2", FieldType.STRING, 1);
        twr_Form_Receipt_Pnd_Lgo_R3 = twr_Form_Receipt__R_Field_3.newFieldInGroup("twr_Form_Receipt_Pnd_Lgo_R3", "#LGO-R3", FieldType.STRING, 1);

        twr_Form_Receipt__R_Field_4 = vw_twr_Form_Receipt.getRecord().newGroupInGroup("twr_Form_Receipt__R_Field_4", "REDEFINE", twr_Form_Receipt_Lgo_Res_Cde);
        twr_Form_Receipt__Filler1 = twr_Form_Receipt__R_Field_4.newFieldInGroup("twr_Form_Receipt__Filler1", "_FILLER1", FieldType.STRING, 1);
        twr_Form_Receipt_Pnd_Lgo_R12 = twr_Form_Receipt__R_Field_4.newFieldInGroup("twr_Form_Receipt_Pnd_Lgo_R12", "#LGO-R12", FieldType.STRING, 2);
        twr_Form_Receipt_Lgo_Exp_Dte = vw_twr_Form_Receipt.getRecord().newFieldInGroup("twr_Form_Receipt_Lgo_Exp_Dte", "LGO-EXP-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "LGO_EXP_DTE");
        twr_Form_Receipt_Lgo_Exp_Dte.setDdmHeader("LGO/EXP/DTE");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_twr_Form_Receipt.reset();

        localVariables.reset();
        record_Out_Ll_Sequence_No.setInitialValue(200);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap381() throws Exception
    {
        super("Iaap381");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT PS = 56 LS = 132;//Natural: READ WORK 1 #WORK-RECORD-IN
        while (condition(getWorkFiles().read(1, pnd_Work_Record_In)))
        {
                                                                                                                                                                          //Natural: PERFORM READ-TWR
            sub_Read_Twr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* **
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TWR
        //*   LL-ENTRY-DATE
        //*   LL-ENTRY-OPERATOR
        //*   LL-ACTIVITY-DATE
        //*   LL-ACTIVITY-OPERATOR
        //*   LL-STATE-CODE
        //*   LL-STATE-NAME
        //*  WRITE '='  LGO-RES-CDE
        //*  WRITE '='   LGO-EXP-DTE
    }
    private void sub_Read_Twr() throws Exception                                                                                                                          //Natural: READ-TWR
    {
        if (BLNatReinput.isReinput()) return;

        vw_twr_Form_Receipt.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) TWR-FORM-RECEIPT BY SUPER-1 STARTING FROM #TAX-NBR
        (
        "READ02",
        new Wc[] { new Wc("SUPER_1", ">=", pnd_Work_Record_In_Pnd_Tax_Nbr, WcType.BY) },
        new Oc[] { new Oc("SUPER_1", "ASC") },
        1
        );
        READ02:
        while (condition(vw_twr_Form_Receipt.readNextRow("READ02")))
        {
            if (condition(twr_Form_Receipt_Tin.notEquals(pnd_Work_Record_In_Pnd_Tax_Nbr)))                                                                                //Natural: IF TIN NE #TAX-NBR
            {
                tax_Key_Pnd_Ppcn1.setValue(pnd_Work_Record_In_Pnd_Contract_Number);                                                                                       //Natural: ASSIGN #PPCN1 := #CONTRACT-NUMBER
                tax_Key_Pnd_Pay_Cd.setValue(pnd_Work_Record_In_Pnd_Payee_Code);                                                                                           //Natural: ASSIGN #PAY-CD := #PAYEE-CODE
                vw_twr_Form_Receipt.startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) TWR-FORM-RECEIPT BY SUPER-1 STARTING FROM TAX-KEY
                (
                "READ03",
                new Wc[] { new Wc("SUPER_1", ">=", tax_Key, WcType.BY) },
                new Oc[] { new Oc("SUPER_1", "ASC") },
                1
                );
                READ03:
                while (condition(vw_twr_Form_Receipt.readNextRow("READ03")))
                {
                    if (condition(twr_Form_Receipt_Tin.notEquals(tax_Key)))                                                                                               //Natural: IF TIN NE TAX-KEY
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(twr_Form_Receipt_Active_Ind.notEquals("A")))                                                                                                        //Natural: IF ACTIVE-IND NE 'A'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(twr_Form_Receipt_Lgo_Res_Cde.equals(" ")))                                                                                                          //Natural: IF LGO-RES-CDE = ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        record_Out_Ll_Contract_No.setValue(pnd_Work_Record_In_Pnd_Contract_Number);                                                                                       //Natural: ASSIGN LL-CONTRACT-NO := #CONTRACT-NUMBER
        record_Out_Ll_Payee_Code.setValue(pnd_Work_Record_In_Pnd_Payee_Code);                                                                                             //Natural: ASSIGN LL-PAYEE-CODE := #PAYEE-CODE
        record_Out_Ll_Status_Code.setValue(twr_Form_Receipt_Active_Ind);                                                                                                  //Natural: ASSIGN LL-STATUS-CODE := ACTIVE-IND
        record_Out_Ll_Effective_Date.setValue(twr_Form_Receipt_Lgo_Eff_Dte);                                                                                              //Natural: ASSIGN LL-EFFECTIVE-DATE := LGO-EFF-DTE
        record_Out_Ll_Expiration_Date.setValue(twr_Form_Receipt_Lgo_Exp_Dte);                                                                                             //Natural: ASSIGN LL-EXPIRATION-DATE := LGO-EXP-DTE
        //*  11/2017 - START
        //*  LL-IA-STATE-CODE   := LGO-RES-CDE
        //*  FORMAT EXPECTED
        short decideConditionsMet129 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #LGO-R3 = ' '
        if (condition(twr_Form_Receipt_Pnd_Lgo_R3.equals(" ")))
        {
            decideConditionsMet129++;
            record_Out_Ll_Ia_State_Code.setValue(twr_Form_Receipt_Lgo_Res_Cde);                                                                                           //Natural: ASSIGN LL-IA-STATE-CODE := LGO-RES-CDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  USE LAST 2-BYTES
            if (condition(twr_Form_Receipt_Pnd_Lgo_R1.equals("0")))                                                                                                       //Natural: IF #LGO-R1 = '0'
            {
                record_Out_Ll_Ia_State_Code.setValue(twr_Form_Receipt_Pnd_Lgo_R12);                                                                                       //Natural: ASSIGN LL-IA-STATE-CODE := #LGO-R12
                //*  USE FIRST 2-BYTES
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                record_Out_Ll_Ia_State_Code.setValue(twr_Form_Receipt_Lgo_Res_Cde);                                                                                       //Natural: ASSIGN LL-IA-STATE-CODE := LGO-RES-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  11/2017 - END
        getWorkFiles().write(2, false, record_Out);                                                                                                                       //Natural: WRITE WORK FILE 2 RECORD-OUT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=132");
    }
}
