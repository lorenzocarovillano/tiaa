/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:54 PM
**        * FROM NATURAL PROGRAM : Iadp125
************************************************************
**        * FILE NAME            : Iadp125.java
**        * CLASS NAME           : Iadp125
**        * INSTANCE NAME        : Iadp125
************************************************************
************************************************************************
* PROGRAM : IADP125
* TITLE   : BLOCKED COUNTRIES REPORT
* FUNCTION: LIST PARTRICPANT CONTRACTS WITH PAYMENTS GOING TO BLOCK
*           COUNTRIES
*
* DATE    : 03/08/2000
*
* HISTORY :
*
* 08/13/15: JT COR/NAAD SUNSET. SCAN ON 08/15 FOR CHANGES.
* 04/2017 : OS PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadp125 extends BLNatBase
{
    // Data Areas
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Input_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Input_Pnd_1st_Xref_Ind;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input_Pnd_1st_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Input_Pnd_1st_Annt_Sex_Cde;
    private DbsField pnd_Input_Pnd_1st_Annt_Life_Cnt;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_2nd_Xref_Ind;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input_Pnd_2nd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Input_Pnd_2nd_Annt_Sex_Cde;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Input_Pnd_Scnd_Annt_Ssn;
    private DbsField pnd_Input_Pnd_Div_Payee_Cde;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Type;
    private DbsField pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Pnd_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;
    private DbsField pnd_Input_Pnd_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Cntrct_Nmbr;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_F17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_Final_Pay_Dte;
    private DbsField pnd_Input_Pnd_Bnfcry_Xref_Ind;
    private DbsField pnd_Input_Pnd_Bnfcry_Dod_Dte;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_F22;
    private DbsField pnd_Input_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Input_Pnd_F23;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_Cntrct_Name_Free;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);

        // Local Variables

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_5 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Joint_Cnvrt_Rcrcd_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Joint_Cnvrt_Rcrcd_Ind", "#JOINT-CNVRT-RCRCD-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Input_Pnd_Rsdncy_At_Issue_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Rsdncy_At_Issue_Cde", "#RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_1st_Xref_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Xref_Ind", "#1ST-XREF-IND", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_1st_Annt_Mrtlty_Yob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Annt_Mrtlty_Yob_Dte", "#1ST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Input_Pnd_1st_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Annt_Sex_Cde", "#1ST-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_1st_Annt_Life_Cnt = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Annt_Life_Cnt", "#1ST-ANNT-LIFE-CNT", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_2nd_Xref_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_2nd_Xref_Ind", "#2ND-XREF-IND", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_2nd_Annt_Mrtlty_Yob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_2nd_Annt_Mrtlty_Yob_Dte", "#2ND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Input_Pnd_2nd_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_2nd_Annt_Sex_Cde", "#2ND-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_Scnd_Annt_Life_Cnt = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Annt_Life_Cnt", "#SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_Scnd_Annt_Ssn = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Annt_Ssn", "#SCND-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_Div_Payee_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Payee_Cde", "#DIV-PAYEE-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Lst_Trans_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Pnd_Cntrct_Type = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re", "#CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3);
        pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte = pnd_Input__R_Field_4.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte", "#CNTRCT-FNL-PRM-DTE", FieldType.DATE, 
            new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Mtch_Ppcn = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Cntrct_Annty_Strt_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Annty_Strt_Dte", "#CNTRCT-ANNTY-STRT-DTE", FieldType.DATE);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd", "#CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd", "#CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Sub_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Sub_Plan_Nmbr", "#SUB-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr", "#ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Input_Pnd_Orgntng_Cntrct_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgntng_Cntrct_Nmbr", "#ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10);

        pnd_Input__R_Field_7 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_7.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_7.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_10 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_10.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_12 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 3);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F17 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Final_Pay_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Final_Pay_Dte", "#FINAL-PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Bnfcry_Xref_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Bnfcry_Xref_Ind", "#BNFCRY-XREF-IND", FieldType.STRING, 9);
        pnd_Input_Pnd_Bnfcry_Dod_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Bnfcry_Dod_Dte", "#BNFCRY-DOD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_13 = pnd_Input__R_Field_12.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_F22 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F22", "#F22", FieldType.STRING, 29);
        pnd_Input_Pnd_Cntrct_Local_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_F23 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F23", "#F23", FieldType.STRING, 19);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Cntrct_Name_Free = localVariables.newFieldInRecord("pnd_Cntrct_Name_Free", "#CNTRCT-NAME-FREE", FieldType.STRING, 35);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iadp125() throws Exception
    {
        super("Iadp125");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 55 LS = 132;//Natural: FORMAT ( 2 ) PS = 55 LS = 132;//Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
        //*  08/15 - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(20))))                                                                                                       //Natural: ACCEPT IF #RECORD-CODE = 20
            {
                continue;
            }
            //*  ACTIVE RECORDS
            if (condition(pnd_Input_Pnd_Status_Cde.notEquals(9)))                                                                                                         //Natural: IF #STATUS-CDE NE 9
            {
                if (condition(pnd_Input_Pnd_Rsdncy_Cde.equals("0AF") || pnd_Input_Pnd_Rsdncy_Cde.equals("0CU") || pnd_Input_Pnd_Rsdncy_Cde.equals("0IR")                  //Natural: IF #RSDNCY-CDE = '0AF' OR = '0CU' OR = '0IR' OR = '0IZ' OR = '0LY' OR = '0YO' OR = '0SR' OR = '0SU' OR = '0OC'
                    || pnd_Input_Pnd_Rsdncy_Cde.equals("0IZ") || pnd_Input_Pnd_Rsdncy_Cde.equals("0LY") || pnd_Input_Pnd_Rsdncy_Cde.equals("0YO") || pnd_Input_Pnd_Rsdncy_Cde.equals("0SR") 
                    || pnd_Input_Pnd_Rsdncy_Cde.equals("0SU") || pnd_Input_Pnd_Rsdncy_Cde.equals("0OC")))
                {
                                                                                                                                                                          //Natural: PERFORM READ-NAME-ADDR
                    sub_Read_Name_Addr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  08/15 - START
                    //*          IF   CNTRCT-NAME-FREE = ' '
                    //*          PERFORM  READ-COR
                    //*          END-IF
                    //*  08/15 - START
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),pnd_Input_Pnd_Ppcn_Nbr,new ColumnSpacing(4),pnd_Input_Pnd_Cpr_Id_Nbr,new              //Natural: WRITE ( 1 ) 1X #PPCN-NBR 4X #CPR-ID-NBR 4X #CNTRCT-NAME-FREE 1X #RSDNCY-CDE
                        ColumnSpacing(4),pnd_Cntrct_Name_Free,new ColumnSpacing(1),pnd_Input_Pnd_Rsdncy_Cde);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*          4X   CNTRCT-NAME-FREE
                    //*  08/15 - START
                    //*    IF ADDRSS-TYPE-CDE  = 'F' AND ADDRSS-GEOGRAPHIC-CDE = 'AF' OR = 'CU'
                    //*   OR = 'IR' OR = 'IZ' OR = 'LY' OR = 'YO' OR = 'SR' OR = 'SU' OR = 'OC'
                    if (condition((pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Type_Code().equals("F") && ((((((((pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("AF")  //Natural: IF #O-CM-ADDRESS-TYPE-CODE = 'F' AND #O-CM-ADDRESS-GEOGRAPHIC-CODE = 'AF' OR = 'CU' OR = 'IR' OR = 'IZ' OR = 'LY' OR = 'YO' OR = 'SR' OR = 'SU' OR = 'OC'
                        || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("CU")) || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("IR")) 
                        || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("IZ")) || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("LY")) 
                        || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("YO")) || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("SR")) 
                        || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("SU")) || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code().equals("OC")))))
                    {
                        //*  08/15 - END
                        if (condition(pnd_Input_Pnd_Rsdncy_Cde.greaterOrEqual("001") && pnd_Input_Pnd_Rsdncy_Cde.lessOrEqual("057")))                                     //Natural: IF #RSDNCY-CDE = '001' THRU '057'
                        {
                            getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(1),pnd_Input_Pnd_Ppcn_Nbr,new ColumnSpacing(4),pnd_Input_Pnd_Cpr_Id_Nbr,new      //Natural: WRITE ( 2 ) 1X #PPCN-NBR 4X #CPR-ID-NBR 1X #O-CM-ADDRESS-GEOGRAPHIC-CODE
                                ColumnSpacing(1),pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Geographic_Code());
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  08/15 - START
                            //*              1X  ADDRSS-GEOGRAPHIC-CDE
                            //*  08/15 - END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME-ADDR
            //* *#I-PIN := #CPR-ID-NBR
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  08/15 - CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*  08/15 - END
    }
    private void sub_Read_Name_Addr() throws Exception                                                                                                                    //Natural: READ-NAME-ADDR
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  08/15 - START
        //* *RESET #CNTRCT-NAME-FREE #MDMA210 #MDMA100      /* 082017
        //*  082017
        pnd_Cntrct_Name_Free.reset();                                                                                                                                     //Natural: RESET #CNTRCT-NAME-FREE #MDMA211 #MDMA101
        pdaMdma211.getPnd_Mdma211().reset();
        pdaMdma101.getPnd_Mdma101().reset();
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                               //Natural: ASSIGN #I-CONTRACT-NUMBER := #PPCN-NBR
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(pnd_Input_Pnd_Payee_Cde);                                                                                   //Natural: ASSIGN #I-PAYEE-CODE := #PAYEE-CDE
        //* *CALLNAT 'MDMN210A' #MDMA210                    /* 082017 START
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Pnd_Cpr_Id_Nbr);                                                                                     //Natural: ASSIGN #I-PIN-N12 := #CPR-ID-NBR
        //* *CALLNAT 'MDMN100A' #MDMA100
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //* * #MDMA100.#O-RETURN-CODE = '0000'
        //*  082017 END
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE = '0000'
        {
            pnd_Cntrct_Name_Free.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(),                   //Natural: COMPRESS #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME INTO #CNTRCT-NAME-FREE
                pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name()));
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(2),"PPCN",new ColumnSpacing(15),"PIN",new ColumnSpacing(17),"NAME",new           //Natural: WRITE ( 1 ) NOTITLE / 2X 'PPCN' 15X 'PIN' 17X 'NAME' 17X 'RESIDENCY CD'
                        ColumnSpacing(17),"RESIDENCY CD");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(2),"PPCN",new ColumnSpacing(15),"PIN",new ColumnSpacing(17),"NAME",new           //Natural: WRITE ( 2 ) NOTITLE / 2X 'PPCN' 15X 'PIN' 17X 'NAME' 17X 'RESIDENCY CD'
                        ColumnSpacing(17),"RESIDENCY CD");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55 LS=132");
        Global.format(2, "PS=55 LS=132");
    }
}
