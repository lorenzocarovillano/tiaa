/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:02 PM
**        * FROM NATURAL PROGRAM : Iaap599d
************************************************************
**        * FILE NAME            : Iaap599d.java
**        * CLASS NAME           : Iaap599d
**        * INSTANCE NAME        : Iaap599d
************************************************************
**********************************************************************
* PROGRAM     -  IAAP599D                                            *
* PURPOSE     -  CREATE MONTHLY IA DEATH REPORT. THIS IS BASED ON    *
*                IA2090D6 MOBIUS REPORT.                             *
* DATE        -  11/2017                                             *
* AUTHOR      -  JUN TINIO                                           *
* HISTORY                                                            *
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap599d extends BLNatBase
{
    // Data Areas
    private LdaIaal050 ldaIaal050;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField cntrl_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Prtcpnt_Ctznshp_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Payee_Key;

    private DbsGroup cpr_Cntrct_Company_Data;
    private DbsField cpr_Cntrct_Company_Cd;

    private DataAccessProgramView vw_sttlmnt;
    private DbsField sttlmnt_Sttlmnt_Id_Nbr;
    private DbsField sttlmnt_Sttlmnt_Tax_Id_Nbr;

    private DbsGroup sttlmnt__R_Field_1;
    private DbsField sttlmnt_Pnd_Sttlmnt_Tax_Id_Nbr_A;
    private DbsField sttlmnt_Sttlmnt_Req_Seq_Nbr;
    private DbsField sttlmnt_Sttlmnt_Process_Type;

    private DbsGroup sttlmnt__R_Field_2;
    private DbsField sttlmnt_Pnd_Sttlmnt_Id;
    private DbsField sttlmnt_Pnd_Sttlmnt_Dcdnt_Type;
    private DbsField sttlmnt_Pnd_Sttlmnt_Dcdnt_Filler;
    private DbsField sttlmnt_Sttlmnt_Timestamp;
    private DbsField sttlmnt_Sttlmnt_Status_Cde;
    private DbsField sttlmnt_Sttlmnt_Status_Timestamp;
    private DbsField sttlmnt_Sttlmnt_Cwf_Wpid;
    private DbsField sttlmnt_Sttlmnt_Decedent_Name;
    private DbsField sttlmnt_Sttlmnt_Decedent_Type;
    private DbsField sttlmnt_Sttlmnt_Dod_Dte;

    private DbsGroup sttlmnt__R_Field_3;
    private DbsField sttlmnt_Pnd_Sttlmnt_Dod_Dte;
    private DbsField sttlmnt_Sttlmnt_2nd_Dod_Dte;

    private DbsGroup sttlmnt__R_Field_4;
    private DbsField sttlmnt_Pnd_Sttlmnt_2nd_Dod_Dte;

    private DataAccessProgramView vw_dc_Cntrct;
    private DbsField dc_Cntrct_Cntrct_Id_Nbr;
    private DbsField dc_Cntrct_Cntrct_Tax_Id_Nbr;
    private DbsField dc_Cntrct_Cntrct_Process_Type;
    private DbsField dc_Cntrct_Cntrct_Req_Seq_Nbr;
    private DbsField dc_Cntrct_Cntrct_Timestamp;
    private DbsField dc_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField dc_Cntrct_Cntrct_Payee_Cde;
    private DbsField dc_Cntrct_Cntrct_Status_Cde;
    private DbsField dc_Cntrct_Cntrct_Status_Timestamp;
    private DbsField dc_Cntrct_Cntrct_Error_Msg;
    private DbsField dc_Cntrct_Cntrct_Annt_Type;
    private DbsField dc_Cntrct_Cntrct_Optn_Cde;
    private DbsField dc_Cntrct_Cntrct_Issue_Dte;
    private DbsGroup dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_CdeMuGroup;
    private DbsField dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde;
    private DbsGroup dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup;
    private DbsField dc_Cntrct_Cntrct_Sttlmnt_Info_Cde;
    private DbsField dc_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField dc_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField dc_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField dc_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField dc_Cntrct_Cntrct_Bnfcry_Xref_Ind;
    private DbsField dc_Cntrct_Cntrct_Bnfcry_Dod_Dte;
    private DbsField dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr;
    private DbsField dc_Cntrct_Cntrct_Bnfcry_Grp_Seq;
    private DbsField dc_Cntrct_Cntrct_Pmt_Data_Nmbr;
    private DbsField dc_Cntrct_Cntrct_Bnfcry_Name;
    private DbsField dc_Cntrct_Pin_Taxid_Seq_Cntrct_Key;

    private DataAccessProgramView vw_cntrct;
    private DbsField cntrct_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_Cntrct_Optn_Cde;
    private DbsField cntrct_Cntrct_Orgn_Cde;
    private DbsField cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField cntrct_Cntrct_Scnd_Annt_Dod_Dte;

    private DataAccessProgramView vw_fund;
    private DbsField fund_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup fund__R_Field_5;
    private DbsField fund_Tiaa_Cmpny_Cde;
    private DbsField fund_Tiaa_Fund_Cde;
    private DbsField fund_Tiaa_Per_Ivc_Amt;
    private DbsField fund_Tiaa_Rtb_Amt;
    private DbsField fund_Tiaa_Tot_Per_Amt;
    private DbsField fund_Tiaa_Tot_Div_Amt;
    private DbsField fund_Tiaa_Old_Per_Amt;
    private DbsField fund_Tiaa_Old_Div_Amt;
    private DbsField fund_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup fund_Tiaa_Rate_Data_Grp;
    private DbsField fund_Tiaa_Rate_Cde;
    private DbsField fund_Tiaa_Rate_Dte;
    private DbsField fund_Tiaa_Per_Pay_Amt;
    private DbsField fund_Tiaa_Per_Div_Amt;
    private DbsField fund_Tiaa_Units_Cnt;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_6;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde;
    private DbsField pnd_Tc_Life;
    private DbsField pnd_Start_Dte;
    private DbsField pnd_W_Start_Date;
    private DbsField pnd_B_Dte;
    private DbsField pnd_E_Dte;
    private DbsField pnd_Rc;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Begin_Date;

    private DbsGroup pnd_Begin_Date__R_Field_7;
    private DbsField pnd_Begin_Date_Pnd_Beg_Yyyy;
    private DbsField pnd_Begin_Date_Pnd_Beg_Mm;
    private DbsField pnd_Begin_Date_Pnd_Beg_Dd;
    private DbsField pnd_End_Date;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_8;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_9;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A;
    private DbsField pnd_I;
    private DbsField pnd_Contract_1_8;
    private DbsField pnd_Sttlmnt_Id_Nbr;
    private DbsField pnd_Sttlmnt_Seq_Nbr;
    private DbsField pnd_Sttlmnt_Decedent_Type;
    private DbsField pnd_Sttlmnt_Ssn;
    private DbsField pnd_Sttlmnt_Dod_Dte_1;
    private DbsField pnd_Sttlmnt_Dod_Dte_2;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Pnd_W_Pin;
    private DbsField pnd_Work_Record_Pnd_W_Ssn;
    private DbsField pnd_Work_Record_Pnd_W_Annu_Type;
    private DbsField pnd_Work_Record_Pnd_W_Cont_Stat;
    private DbsField pnd_Work_Record_Pnd_W_Contract_Mode;
    private DbsField pnd_Work_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Work_Record_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Work_Record_Pnd_W_Dte;

    private DbsGroup pnd_Work_Record__R_Field_10;
    private DbsField pnd_Work_Record_Pnd_W_Yyyy;
    private DbsField pnd_Work_Record_Pnd_W_Mm;
    private DbsField pnd_Work_Record_Pnd_W_Dd;
    private DbsField pnd_Work_Record_Pnd_W_Dod_Dte;

    private DbsGroup pnd_Work_Record__R_Field_11;
    private DbsField pnd_Work_Record_Pnd_Dod_Yyyy;
    private DbsField pnd_Work_Record_Pnd_Dod_Mm;
    private DbsField pnd_Work_Record_Pnd_Dod_Dd;
    private DbsField pnd_Work_Record_Pnd_Dth_Dte;
    private DbsField pnd_Work_Record_Pnd_1st_Dod;
    private DbsField pnd_Work_Record_Pnd_2nd_Dod;
    private DbsField pnd_Work_Record_Pnd_1st_Dob;
    private DbsField pnd_Work_Record_Pnd_2nd_Dob;
    private DbsField pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte;

    private DbsGroup pnd_Work_Record__R_Field_12;
    private DbsField pnd_Work_Record_Pnd_W_Iss_Yyyy;
    private DbsField pnd_Work_Record_Pnd_W_Iss_Mm;
    private DbsField pnd_Work_Record_Pnd_Iss_Dte;
    private DbsField pnd_Work_Record_Pnd_W_Cmpny;
    private DbsField pnd_Work_Record_Pnd_Guar_Amt;
    private DbsField pnd_Work_Record_Pnd_Div_Amt;
    private DbsField pnd_Work_Record_Pnd_W_Fund;
    private DbsField pnd_Work_Record_Pnd_W_Reval;
    private DbsField pnd_Work_Record_Pnd_Opt_Desc;
    private DbsField pnd_Work_Record_Pnd_Proc_Date;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S;

    private DbsGroup pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E;

    private DbsGroup pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        registerRecord(vw_cntrl);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Prtcpnt_Ctznshp_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_CTZNSHP_CDE");
        cpr_Prtcpnt_Rsdncy_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Payee_Key = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Payee_Key", "CNTRCT-PAYEE-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_KEY");
        cpr_Cntrct_Payee_Key.setSuperDescriptor(true);

        cpr_Cntrct_Company_Data = vw_cpr.getRecord().newGroupInGroup("cpr_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Company_Cd = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        registerRecord(vw_cpr);

        vw_sttlmnt = new DataAccessProgramView(new NameInfo("vw_sttlmnt", "STTLMNT"), "IAA_DC_STTLMNT_REQ", "IA_DEATH_CLAIMS");
        sttlmnt_Sttlmnt_Id_Nbr = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Id_Nbr", "STTLMNT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "STTLMNT_ID_NBR");
        sttlmnt_Sttlmnt_Tax_Id_Nbr = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Tax_Id_Nbr", "STTLMNT-TAX-ID-NBR", FieldType.NUMERIC, 9, 
            RepeatingFieldStrategy.None, "STTLMNT_TAX_ID_NBR");

        sttlmnt__R_Field_1 = vw_sttlmnt.getRecord().newGroupInGroup("sttlmnt__R_Field_1", "REDEFINE", sttlmnt_Sttlmnt_Tax_Id_Nbr);
        sttlmnt_Pnd_Sttlmnt_Tax_Id_Nbr_A = sttlmnt__R_Field_1.newFieldInGroup("sttlmnt_Pnd_Sttlmnt_Tax_Id_Nbr_A", "#STTLMNT-TAX-ID-NBR-A", FieldType.STRING, 
            9);
        sttlmnt_Sttlmnt_Req_Seq_Nbr = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Req_Seq_Nbr", "STTLMNT-REQ-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "STTLMNT_REQ_SEQ_NBR");
        sttlmnt_Sttlmnt_Process_Type = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Process_Type", "STTLMNT-PROCESS-TYPE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STTLMNT_PROCESS_TYPE");

        sttlmnt__R_Field_2 = vw_sttlmnt.getRecord().newGroupInGroup("sttlmnt__R_Field_2", "REDEFINE", sttlmnt_Sttlmnt_Process_Type);
        sttlmnt_Pnd_Sttlmnt_Id = sttlmnt__R_Field_2.newFieldInGroup("sttlmnt_Pnd_Sttlmnt_Id", "#STTLMNT-ID", FieldType.STRING, 2);
        sttlmnt_Pnd_Sttlmnt_Dcdnt_Type = sttlmnt__R_Field_2.newFieldInGroup("sttlmnt_Pnd_Sttlmnt_Dcdnt_Type", "#STTLMNT-DCDNT-TYPE", FieldType.STRING, 
            1);
        sttlmnt_Pnd_Sttlmnt_Dcdnt_Filler = sttlmnt__R_Field_2.newFieldInGroup("sttlmnt_Pnd_Sttlmnt_Dcdnt_Filler", "#STTLMNT-DCDNT-FILLER", FieldType.STRING, 
            1);
        sttlmnt_Sttlmnt_Timestamp = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Timestamp", "STTLMNT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STTLMNT_TIMESTAMP");
        sttlmnt_Sttlmnt_Status_Cde = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Status_Cde", "STTLMNT-STATUS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "STTLMNT_STATUS_CDE");
        sttlmnt_Sttlmnt_Status_Timestamp = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Status_Timestamp", "STTLMNT-STATUS-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STTLMNT_STATUS_TIMESTAMP");
        sttlmnt_Sttlmnt_Cwf_Wpid = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Cwf_Wpid", "STTLMNT-CWF-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STTLMNT_CWF_WPID");
        sttlmnt_Sttlmnt_Decedent_Name = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Decedent_Name", "STTLMNT-DECEDENT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_NAME");
        sttlmnt_Sttlmnt_Decedent_Type = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Decedent_Type", "STTLMNT-DECEDENT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_TYPE");
        sttlmnt_Sttlmnt_Dod_Dte = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_Dod_Dte", "STTLMNT-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "STTLMNT_DOD_DTE");

        sttlmnt__R_Field_3 = vw_sttlmnt.getRecord().newGroupInGroup("sttlmnt__R_Field_3", "REDEFINE", sttlmnt_Sttlmnt_Dod_Dte);
        sttlmnt_Pnd_Sttlmnt_Dod_Dte = sttlmnt__R_Field_3.newFieldInGroup("sttlmnt_Pnd_Sttlmnt_Dod_Dte", "#STTLMNT-DOD-DTE", FieldType.STRING, 8);
        sttlmnt_Sttlmnt_2nd_Dod_Dte = vw_sttlmnt.getRecord().newFieldInGroup("sttlmnt_Sttlmnt_2nd_Dod_Dte", "STTLMNT-2ND-DOD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "STTLMNT_2ND_DOD_DTE");

        sttlmnt__R_Field_4 = vw_sttlmnt.getRecord().newGroupInGroup("sttlmnt__R_Field_4", "REDEFINE", sttlmnt_Sttlmnt_2nd_Dod_Dte);
        sttlmnt_Pnd_Sttlmnt_2nd_Dod_Dte = sttlmnt__R_Field_4.newFieldInGroup("sttlmnt_Pnd_Sttlmnt_2nd_Dod_Dte", "#STTLMNT-2ND-DOD-DTE", FieldType.STRING, 
            8);
        registerRecord(vw_sttlmnt);

        vw_dc_Cntrct = new DataAccessProgramView(new NameInfo("vw_dc_Cntrct", "DC-CNTRCT"), "IAA_DC_CNTRCT", "IA_DEATH_CLAIMS");
        dc_Cntrct_Cntrct_Id_Nbr = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Id_Nbr", "CNTRCT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_ID_NBR");
        dc_Cntrct_Cntrct_Tax_Id_Nbr = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Tax_Id_Nbr", "CNTRCT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_TAX_ID_NBR");
        dc_Cntrct_Cntrct_Process_Type = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Process_Type", "CNTRCT-PROCESS-TYPE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PROCESS_TYPE");
        dc_Cntrct_Cntrct_Req_Seq_Nbr = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Req_Seq_Nbr", "CNTRCT-REQ-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_REQ_SEQ_NBR");
        dc_Cntrct_Cntrct_Timestamp = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Timestamp", "CNTRCT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CNTRCT_TIMESTAMP");
        dc_Cntrct_Cntrct_Ppcn_Nbr = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        dc_Cntrct_Cntrct_Payee_Cde = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        dc_Cntrct_Cntrct_Status_Cde = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Status_Cde", "CNTRCT-STATUS-CDE", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_STATUS_CDE");
        dc_Cntrct_Cntrct_Status_Timestamp = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Status_Timestamp", "CNTRCT-STATUS-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CNTRCT_STATUS_TIMESTAMP");
        dc_Cntrct_Cntrct_Error_Msg = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Error_Msg", "CNTRCT-ERROR-MSG", FieldType.STRING, 40, 
            RepeatingFieldStrategy.None, "CNTRCT_ERROR_MSG");
        dc_Cntrct_Cntrct_Annt_Type = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Annt_Type", "CNTRCT-ANNT-TYPE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_ANNT_TYPE");
        dc_Cntrct_Cntrct_Optn_Cde = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        dc_Cntrct_Cntrct_Issue_Dte = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6, 
            RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Count_Castcntrct_Sttlmnt_Info_Cde", "C*CNTRCT-STTLMNT-INFO-CDE", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_CNTRCT_STTLMNT_INFO_CDE");
        dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup = vw_dc_Cntrct.getRecord().newGroupInGroup("DC_CNTRCT_CNTRCT_STTLMNT_INFO_CDEMuGroup", "CNTRCT_STTLMNT_INFO_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_DEATH_CLAIMS_CNTRCT_STTLMNT_INFO_CDE");
        dc_Cntrct_Cntrct_Sttlmnt_Info_Cde = dc_Cntrct_Cntrct_Sttlmnt_Info_CdeMuGroup.newFieldArrayInGroup("dc_Cntrct_Cntrct_Sttlmnt_Info_Cde", "CNTRCT-STTLMNT-INFO-CDE", 
            FieldType.STRING, 4, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_STTLMNT_INFO_CDE");
        dc_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        dc_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        dc_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        dc_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        dc_Cntrct_Cntrct_Bnfcry_Xref_Ind = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Bnfcry_Xref_Ind", "CNTRCT-BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_XREF_IND");
        dc_Cntrct_Cntrct_Bnfcry_Dod_Dte = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Bnfcry_Dod_Dte", "CNTRCT-BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_DOD_DTE");
        dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Bnfcry_Grp_Nmbr", "CNTRCT-BNFCRY-GRP-NMBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_GRP_NMBR");
        dc_Cntrct_Cntrct_Bnfcry_Grp_Seq = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Bnfcry_Grp_Seq", "CNTRCT-BNFCRY-GRP-SEQ", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_GRP_SEQ");
        dc_Cntrct_Cntrct_Pmt_Data_Nmbr = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Pmt_Data_Nmbr", "CNTRCT-PMT-DATA-NMBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_PMT_DATA_NMBR");
        dc_Cntrct_Cntrct_Bnfcry_Name = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Cntrct_Bnfcry_Name", "CNTRCT-BNFCRY-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_NAME");
        dc_Cntrct_Pin_Taxid_Seq_Cntrct_Key = vw_dc_Cntrct.getRecord().newFieldInGroup("dc_Cntrct_Pin_Taxid_Seq_Cntrct_Key", "PIN-TAXID-SEQ-CNTRCT-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "PIN_TAXID_SEQ_CNTRCT_KEY");
        dc_Cntrct_Pin_Taxid_Seq_Cntrct_Key.setSuperDescriptor(true);
        registerRecord(vw_dc_Cntrct);

        vw_cntrct = new DataAccessProgramView(new NameInfo("vw_cntrct", "CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        cntrct_Cntrct_Ppcn_Nbr = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cntrct_Cntrct_Optn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        cntrct_Cntrct_Orgn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        cntrct_Cntrct_First_Annt_Dob_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        cntrct_Cntrct_First_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        registerRecord(vw_cntrct);

        vw_fund = new DataAccessProgramView(new NameInfo("vw_fund", "FUND"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        fund_Tiaa_Cntrct_Ppcn_Nbr = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CREF_CNTRCT_PPCN_NBR");
        fund_Tiaa_Cntrct_Payee_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        fund_Tiaa_Cmpny_Fund_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIAA_CMPNY_FUND_CDE");

        fund__R_Field_5 = vw_fund.getRecord().newGroupInGroup("fund__R_Field_5", "REDEFINE", fund_Tiaa_Cmpny_Fund_Cde);
        fund_Tiaa_Cmpny_Cde = fund__R_Field_5.newFieldInGroup("fund_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        fund_Tiaa_Fund_Cde = fund__R_Field_5.newFieldInGroup("fund_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 2);
        fund_Tiaa_Per_Ivc_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_PER_IVC_AMT");
        fund_Tiaa_Rtb_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_RTB_AMT");
        fund_Tiaa_Tot_Per_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_TOT_PER_AMT");
        fund_Tiaa_Tot_Div_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_TOT_DIV_AMT");
        fund_Tiaa_Old_Per_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "AJ");
        fund_Tiaa_Old_Div_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_OLD_UNIT_VAL");
        fund_Count_Casttiaa_Rate_Data_Grp = vw_fund.getRecord().newFieldInGroup("fund_Count_Casttiaa_Rate_Data_Grp", "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        fund_Tiaa_Rate_Data_Grp = vw_fund.getRecord().newGroupInGroup("fund_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Rate_Cde = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TIAA_RATE_CDE", "TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Rate_Dte = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TIAA_RATE_DTE", "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 
            250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Per_Pay_Amt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Per_Div_Amt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Units_Cnt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new 
            DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_fund);

        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_6", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr = pnd_Tiaa_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr", 
            "#TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde", 
            "#TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde", 
            "#TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3);
        pnd_Tc_Life = localVariables.newFieldArrayInRecord("pnd_Tc_Life", "#TC-LIFE", FieldType.NUMERIC, 2, new DbsArrayController(1, 9));
        pnd_Start_Dte = localVariables.newFieldInRecord("pnd_Start_Dte", "#START-DTE", FieldType.TIME);
        pnd_W_Start_Date = localVariables.newFieldInRecord("pnd_W_Start_Date", "#W-START-DATE", FieldType.DATE);
        pnd_B_Dte = localVariables.newFieldInRecord("pnd_B_Dte", "#B-DTE", FieldType.DATE);
        pnd_E_Dte = localVariables.newFieldInRecord("pnd_E_Dte", "#E-DTE", FieldType.DATE);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Begin_Date = localVariables.newFieldInRecord("pnd_Begin_Date", "#BEGIN-DATE", FieldType.STRING, 8);

        pnd_Begin_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Begin_Date__R_Field_7", "REDEFINE", pnd_Begin_Date);
        pnd_Begin_Date_Pnd_Beg_Yyyy = pnd_Begin_Date__R_Field_7.newFieldInGroup("pnd_Begin_Date_Pnd_Beg_Yyyy", "#BEG-YYYY", FieldType.NUMERIC, 4);
        pnd_Begin_Date_Pnd_Beg_Mm = pnd_Begin_Date__R_Field_7.newFieldInGroup("pnd_Begin_Date_Pnd_Beg_Mm", "#BEG-MM", FieldType.NUMERIC, 2);
        pnd_Begin_Date_Pnd_Beg_Dd = pnd_Begin_Date__R_Field_7.newFieldInGroup("pnd_Begin_Date_Pnd_Beg_Dd", "#BEG-DD", FieldType.NUMERIC, 2);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_8", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract = pnd_Cntrct_Payee_Key__R_Field_8.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract", "#CPK-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee = pnd_Cntrct_Payee_Key__R_Field_8.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee", "#CPK-PAYEE", FieldType.NUMERIC, 
            2);

        pnd_Cntrct_Payee_Key__R_Field_9 = pnd_Cntrct_Payee_Key__R_Field_8.newGroupInGroup("pnd_Cntrct_Payee_Key__R_Field_9", "REDEFINE", pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee);
        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A = pnd_Cntrct_Payee_Key__R_Field_9.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A", "#CPK-PAYEE-A", 
            FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Contract_1_8 = localVariables.newFieldInRecord("pnd_Contract_1_8", "#CONTRACT-1-8", FieldType.STRING, 8);
        pnd_Sttlmnt_Id_Nbr = localVariables.newFieldInRecord("pnd_Sttlmnt_Id_Nbr", "#STTLMNT-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Sttlmnt_Seq_Nbr = localVariables.newFieldInRecord("pnd_Sttlmnt_Seq_Nbr", "#STTLMNT-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Sttlmnt_Decedent_Type = localVariables.newFieldInRecord("pnd_Sttlmnt_Decedent_Type", "#STTLMNT-DECEDENT-TYPE", FieldType.STRING, 2);
        pnd_Sttlmnt_Ssn = localVariables.newFieldInRecord("pnd_Sttlmnt_Ssn", "#STTLMNT-SSN", FieldType.NUMERIC, 9);
        pnd_Sttlmnt_Dod_Dte_1 = localVariables.newFieldInRecord("pnd_Sttlmnt_Dod_Dte_1", "#STTLMNT-DOD-DTE-1", FieldType.STRING, 8);
        pnd_Sttlmnt_Dod_Dte_2 = localVariables.newFieldInRecord("pnd_Sttlmnt_Dod_Dte_2", "#STTLMNT-DOD-DTE-2", FieldType.STRING, 8);

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Pnd_W_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Pnd_W_Ssn = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Ssn", "#W-SSN", FieldType.STRING, 9);
        pnd_Work_Record_Pnd_W_Annu_Type = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Annu_Type", "#W-ANNU-TYPE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_W_Cont_Stat = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cont_Stat", "#W-CONT-STAT", FieldType.STRING, 11);
        pnd_Work_Record_Pnd_W_Contract_Mode = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Contract_Mode", "#W-CONTRACT-MODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Pnd_Cntrct_Optn_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_Cntrct_Orgn_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_W_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Dte", "#W-DTE", FieldType.STRING, 8);

        pnd_Work_Record__R_Field_10 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_10", "REDEFINE", pnd_Work_Record_Pnd_W_Dte);
        pnd_Work_Record_Pnd_W_Yyyy = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_W_Yyyy", "#W-YYYY", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_W_Mm = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_W_Mm", "#W-MM", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_W_Dd = pnd_Work_Record__R_Field_10.newFieldInGroup("pnd_Work_Record_Pnd_W_Dd", "#W-DD", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_W_Dod_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Dod_Dte", "#W-DOD-DTE", FieldType.STRING, 8);

        pnd_Work_Record__R_Field_11 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_11", "REDEFINE", pnd_Work_Record_Pnd_W_Dod_Dte);
        pnd_Work_Record_Pnd_Dod_Yyyy = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Dod_Yyyy", "#DOD-YYYY", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_Dod_Mm = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Dod_Mm", "#DOD-MM", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Dod_Dd = pnd_Work_Record__R_Field_11.newFieldInGroup("pnd_Work_Record_Pnd_Dod_Dd", "#DOD-DD", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Dth_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Dth_Dte", "#DTH-DTE", FieldType.STRING, 10);
        pnd_Work_Record_Pnd_1st_Dod = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_1st_Dod", "#1ST-DOD", FieldType.STRING, 7);
        pnd_Work_Record_Pnd_2nd_Dod = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_2nd_Dod", "#2ND-DOD", FieldType.STRING, 7);
        pnd_Work_Record_Pnd_1st_Dob = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_1st_Dob", "#1ST-DOB", FieldType.STRING, 10);
        pnd_Work_Record_Pnd_2nd_Dob = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_2nd_Dob", "#2ND-DOB", FieldType.STRING, 10);
        pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte", "#W-CNTRCT-ISSUE-DTE", FieldType.STRING, 
            6);

        pnd_Work_Record__R_Field_12 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_12", "REDEFINE", pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte);
        pnd_Work_Record_Pnd_W_Iss_Yyyy = pnd_Work_Record__R_Field_12.newFieldInGroup("pnd_Work_Record_Pnd_W_Iss_Yyyy", "#W-ISS-YYYY", FieldType.STRING, 
            4);
        pnd_Work_Record_Pnd_W_Iss_Mm = pnd_Work_Record__R_Field_12.newFieldInGroup("pnd_Work_Record_Pnd_W_Iss_Mm", "#W-ISS-MM", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Iss_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Iss_Dte", "#ISS-DTE", FieldType.STRING, 7);
        pnd_Work_Record_Pnd_W_Cmpny = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cmpny", "#W-CMPNY", FieldType.STRING, 6);
        pnd_Work_Record_Pnd_Guar_Amt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Guar_Amt", "#GUAR-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_Record_Pnd_Div_Amt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Div_Amt", "#DIV-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_Record_Pnd_W_Fund = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Fund", "#W-FUND", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_W_Reval = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Reval", "#W-REVAL", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Opt_Desc = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Opt_Desc", "#OPT-DESC", FieldType.STRING, 50);
        pnd_Work_Record_Pnd_Proc_Date = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Proc_Date", "#PROC-DATE", FieldType.STRING, 10);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S = localVariables.newFieldInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_S", "#PIN-TAXID-SEQ-CNTRCT-KEY-S", FieldType.STRING, 
            37);

        pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13 = localVariables.newGroupInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13", "REDEFINE", pnd_Pin_Taxid_Seq_Cntrct_Key_S);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S", 
            "#PTSCK-ID-NBR-S", FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S", 
            "#PTSCK-TAX-ID-NBR-S", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S", 
            "#PTSCK-REQ-SEQ-NBR-S", FieldType.NUMERIC, 3);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S", 
            "#PTSCK-PPCN-NBR-S", FieldType.STRING, 11);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S = pnd_Pin_Taxid_Seq_Cntrct_Key_S__R_Field_13.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S", 
            "#PTSCK-PAYEE-CDE-S", FieldType.NUMERIC, 2);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E = localVariables.newFieldInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_E", "#PIN-TAXID-SEQ-CNTRCT-KEY-E", FieldType.STRING, 
            37);

        pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14 = localVariables.newGroupInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14", "REDEFINE", pnd_Pin_Taxid_Seq_Cntrct_Key_E);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E", 
            "#PTSCK-ID-NBR-E", FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E", 
            "#PTSCK-TAX-ID-NBR-E", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E", 
            "#PTSCK-REQ-SEQ-NBR-E", FieldType.NUMERIC, 3);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E", 
            "#PTSCK-PPCN-NBR-E", FieldType.STRING, 11);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E = pnd_Pin_Taxid_Seq_Cntrct_Key_E__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E", 
            "#PTSCK-PAYEE-CDE-E", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();
        vw_cpr.reset();
        vw_sttlmnt.reset();
        vw_dc_Cntrct.reset();
        vw_cntrct.reset();
        vw_fund.reset();

        ldaIaal050.initializeValues();

        localVariables.reset();
        pnd_Tc_Life.getValue(1).setInitialValue(35);
        pnd_Tc_Life.getValue(2).setInitialValue(36);
        pnd_Tc_Life.getValue(3).setInitialValue(37);
        pnd_Tc_Life.getValue(4).setInitialValue(38);
        pnd_Tc_Life.getValue(5).setInitialValue(40);
        pnd_Tc_Life.getValue(6).setInitialValue(43);
        pnd_Tc_Life.getValue(7).setInitialValue(44);
        pnd_Tc_Life.getValue(8).setInitialValue(45);
        pnd_Tc_Life.getValue(9).setInitialValue(46);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap599d() throws Exception
    {
        super("Iaap599d");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 250
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                                  //Natural: ASSIGN IA-REVAL-METHD := 'M'
        ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue("L");                                                                                                    //Natural: ASSIGN IA-CALL-TYPE := 'L'
        ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(99999999);                                                                                              //Natural: ASSIGN IA-CHECK-DATE := 99999999
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde.reset();                                                                                                         //Natural: RESET #TIAA-CMPNY-FUND-CDE
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Iaan599d.class , getCurrentProcessState(), pnd_Begin_Date, pnd_End_Date);                                                                         //Natural: CALLNAT 'IAAN599D' #BEGIN-DATE #END-DATE
        if (condition(Global.isEscape())) return;
        getReports().write(0, "IAAN599D","=",pnd_Begin_Date,"=",pnd_End_Date);                                                                                            //Natural: WRITE 'IAAN599D' '=' #BEGIN-DATE '=' #END-DATE
        if (Global.isEscape()) return;
        if (condition(pnd_Begin_Date.greater(" ")))                                                                                                                       //Natural: IF #BEGIN-DATE GT ' '
        {
            pnd_Start_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Begin_Date);                                                                                  //Natural: MOVE EDITED #BEGIN-DATE TO #START-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_cntrl.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
            (
            "READ01",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
            new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
            1
            );
            READ01:
            while (condition(vw_cntrl.readNextRow("READ01")))
            {
                pnd_End_Date.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #END-DATE
                //*    #START-DTE := CNTRL-CHECK-DTE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            pnd_Begin_Date.setValue(pnd_End_Date);                                                                                                                        //Natural: ASSIGN #BEGIN-DATE := #END-DATE
            pnd_Begin_Date_Pnd_Beg_Mm.nsubtract(1);                                                                                                                       //Natural: SUBTRACT 1 FROM #BEG-MM
            if (condition(pnd_Begin_Date_Pnd_Beg_Mm.lessOrEqual(getZero())))                                                                                              //Natural: IF #BEG-MM LE 0
            {
                pnd_Begin_Date_Pnd_Beg_Mm.setValue(12);                                                                                                                   //Natural: ASSIGN #BEG-MM := 12
                pnd_Begin_Date_Pnd_Beg_Yyyy.nsubtract(1);                                                                                                                 //Natural: SUBTRACT 1 FROM #BEG-YYYY
            }                                                                                                                                                             //Natural: END-IF
            pnd_Begin_Date_Pnd_Beg_Dd.setValue(31);                                                                                                                       //Natural: ASSIGN #BEG-DD := 31
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(DbsUtil.maskMatches(pnd_Begin_Date,"YYYYMMDD")))                                                                                            //Natural: IF #BEGIN-DATE = MASK ( YYYYMMDD )
                {
                    pnd_Start_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Begin_Date);                                                                          //Natural: MOVE EDITED #BEGIN-DATE TO #START-DTE ( EM = YYYYMMDD )
                    pnd_W_Start_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Begin_Date);                                                                       //Natural: MOVE EDITED #BEGIN-DATE TO #W-START-DATE ( EM = YYYYMMDD )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Begin_Date_Pnd_Beg_Dd.nsubtract(1);                                                                                                                   //Natural: SUBTRACT 1 FROM #BEG-DD
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            DbsUtil.callnat(Adsn607.class , getCurrentProcessState(), pnd_W_Start_Date, pnd_Rc);                                                                          //Natural: CALLNAT 'ADSN607' #W-START-DATE #RC
            if (condition(Global.isEscape())) return;
            pnd_Start_Dte.setValue(pnd_W_Start_Date);                                                                                                                     //Natural: ASSIGN #START-DTE := #W-START-DATE
            pnd_Begin_Date.setValueEdited(pnd_Start_Dte,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED #START-DTE ( EM = YYYYMMDD ) TO #BEGIN-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *#BEGIN-DATE := '20160101'
        //* *#END-DATE := '20161231'
        pnd_B_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Begin_Date);                                                                                          //Natural: MOVE EDITED #BEGIN-DATE TO #B-DTE ( EM = YYYYMMDD )
        pnd_E_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_End_Date);                                                                                            //Natural: MOVE EDITED #END-DATE TO #E-DTE ( EM = YYYYMMDD )
        getReports().write(0, "=",pnd_Begin_Date,"=",pnd_End_Date,"=",pnd_Start_Dte, new ReportEditMask ("YYYYMMDD"));                                                    //Natural: WRITE '=' #BEGIN-DATE '=' #END-DATE '=' #START-DTE ( EM = YYYYMMDD )
        if (Global.isEscape()) return;
        vw_sttlmnt.startDatabaseRead                                                                                                                                      //Natural: READ STTLMNT BY STTLMNT-STATUS-TIMESTAMP STARTING FROM #START-DTE
        (
        "RD",
        new Wc[] { new Wc("STTLMNT_STATUS_TIMESTAMP", ">=", pnd_Start_Dte, WcType.BY) },
        new Oc[] { new Oc("STTLMNT_STATUS_TIMESTAMP", "ASC") }
        );
        RD:
        while (condition(vw_sttlmnt.readNextRow("RD")))
        {
            pnd_Trans_Dte.setValueEdited(sttlmnt_Sttlmnt_Status_Timestamp,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED STTLMNT-STATUS-TIMESTAMP ( EM = YYYYMMDD ) TO #TRANS-DTE
            pnd_Work_Record_Pnd_Proc_Date.setValueEdited(sttlmnt_Sttlmnt_Status_Timestamp,new ReportEditMask("MM/DD/YYYY"));                                              //Natural: MOVE EDITED STTLMNT-STATUS-TIMESTAMP ( EM = MM/DD/YYYY ) TO #PROC-DATE
            if (condition(pnd_Trans_Dte.greater(pnd_End_Date)))                                                                                                           //Natural: IF #TRANS-DTE GT #END-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition((sttlmnt_Sttlmnt_Process_Type.equals("ID1A") || sttlmnt_Sttlmnt_Process_Type.equals("ID2A") || sttlmnt_Sttlmnt_Process_Type.equals("ID3A"))     //Natural: IF ( STTLMNT-PROCESS-TYPE = 'ID1A' OR STTLMNT-PROCESS-TYPE = 'ID2A' OR STTLMNT-PROCESS-TYPE = 'ID3A' ) AND STTLMNT-STATUS-CDE = '99'
                && sttlmnt_Sttlmnt_Status_Cde.equals("99")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-SETTLEMENT-FIELDS
            sub_Move_Settlement_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ACCESS-CONTRACTS-AND-PROCESS
            sub_Access_Contracts_And_Process();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* **WRITE(1) '********** END OF REPORT **********'
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-SETTLEMENT-FIELDS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCESS-CONTRACTS-AND-PROCESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-AND-WRITE-REPORT
        //* *====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR
    }
    private void sub_Move_Settlement_Fields() throws Exception                                                                                                            //Natural: MOVE-SETTLEMENT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Sttlmnt_Dod_Dte_1.setValue(sttlmnt_Pnd_Sttlmnt_Dod_Dte);                                                                                                      //Natural: MOVE STTLMNT.#STTLMNT-DOD-DTE TO #STTLMNT-DOD-DTE-1
        pnd_Sttlmnt_Dod_Dte_2.setValue(sttlmnt_Pnd_Sttlmnt_2nd_Dod_Dte);                                                                                                  //Natural: MOVE STTLMNT.#STTLMNT-2ND-DOD-DTE TO #STTLMNT-DOD-DTE-2
        pnd_Sttlmnt_Decedent_Type.setValue(sttlmnt_Pnd_Sttlmnt_Dcdnt_Type);                                                                                               //Natural: MOVE STTLMNT.#STTLMNT-DCDNT-TYPE TO #STTLMNT-DECEDENT-TYPE
        pnd_Work_Record_Pnd_W_Pin.setValue(sttlmnt_Sttlmnt_Id_Nbr);                                                                                                       //Natural: MOVE STTLMNT.STTLMNT-ID-NBR TO #W-PIN #STTLMNT-ID-NBR
        pnd_Sttlmnt_Id_Nbr.setValue(sttlmnt_Sttlmnt_Id_Nbr);
        pnd_Work_Record_Pnd_W_Ssn.setValue(sttlmnt_Pnd_Sttlmnt_Tax_Id_Nbr_A);                                                                                             //Natural: MOVE STTLMNT.#STTLMNT-TAX-ID-NBR-A TO #W-SSN
        pnd_Sttlmnt_Ssn.setValue(sttlmnt_Sttlmnt_Tax_Id_Nbr);                                                                                                             //Natural: MOVE STTLMNT.STTLMNT-TAX-ID-NBR TO #STTLMNT-SSN
        pnd_Sttlmnt_Seq_Nbr.setValue(sttlmnt_Sttlmnt_Req_Seq_Nbr);                                                                                                        //Natural: MOVE STTLMNT.STTLMNT-REQ-SEQ-NBR TO #STTLMNT-SEQ-NBR
    }
    private void sub_Access_Contracts_And_Process() throws Exception                                                                                                      //Natural: ACCESS-CONTRACTS-AND-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S.setValue(pnd_Sttlmnt_Ssn);                                                                                  //Natural: MOVE #STTLMNT-SSN TO #PTSCK-TAX-ID-NBR-S #PTSCK-TAX-ID-NBR-E
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E.setValue(pnd_Sttlmnt_Ssn);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S.setValue(pnd_Sttlmnt_Id_Nbr);                                                                                   //Natural: MOVE #STTLMNT-ID-NBR TO #PTSCK-ID-NBR-S #PTSCK-ID-NBR-E
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E.setValue(pnd_Sttlmnt_Id_Nbr);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S.setValue(pnd_Sttlmnt_Seq_Nbr);                                                                             //Natural: MOVE #STTLMNT-SEQ-NBR TO #PTSCK-REQ-SEQ-NBR-S #PTSCK-REQ-SEQ-NBR-E
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E.setValue(pnd_Sttlmnt_Seq_Nbr);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S.setValue("H'00'");                                                                                            //Natural: MOVE H'00' TO #PTSCK-PPCN-NBR-S
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S.setValue(0);                                                                                                 //Natural: MOVE 0 TO #PTSCK-PAYEE-CDE-S
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E.setValue("H'FF'");                                                                                            //Natural: MOVE H'FF' TO #PTSCK-PPCN-NBR-E
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E.setValue(99);                                                                                                //Natural: MOVE 99 TO #PTSCK-PAYEE-CDE-E
        vw_dc_Cntrct.startDatabaseRead                                                                                                                                    //Natural: READ DC-CNTRCT BY PIN-TAXID-SEQ-CNTRCT-KEY STARTING FROM #PIN-TAXID-SEQ-CNTRCT-KEY-S ENDING AT #PIN-TAXID-SEQ-CNTRCT-KEY-E
        (
        "READ02",
        new Wc[] { new Wc("PIN_TAXID_SEQ_CNTRCT_KEY", ">=", pnd_Pin_Taxid_Seq_Cntrct_Key_S, "And", WcType.BY) ,
        new Wc("PIN_TAXID_SEQ_CNTRCT_KEY", "<=", pnd_Pin_Taxid_Seq_Cntrct_Key_E, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_CNTRCT_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_dc_Cntrct.readNextRow("READ02")))
        {
            if (condition(!(dc_Cntrct_Cntrct_Sttlmnt_Info_Cde.getValue("*").equals("D032") || dc_Cntrct_Cntrct_Sttlmnt_Info_Cde.getValue("*").equals("D033")              //Natural: ACCEPT IF CNTRCT-STTLMNT-INFO-CDE ( * ) = 'D032' OR CNTRCT-STTLMNT-INFO-CDE ( * ) = 'D033' OR CNTRCT-STTLMNT-INFO-CDE ( * ) = 'D038' OR CNTRCT-STTLMNT-INFO-CDE ( * ) = 'D039' OR CNTRCT-STTLMNT-INFO-CDE ( * ) = 'D046' OR CNTRCT-STTLMNT-INFO-CDE ( * ) = 'D053' OR CNTRCT-STTLMNT-INFO-CDE ( * ) = 'D054'
                || dc_Cntrct_Cntrct_Sttlmnt_Info_Cde.getValue("*").equals("D038") || dc_Cntrct_Cntrct_Sttlmnt_Info_Cde.getValue("*").equals("D039") || dc_Cntrct_Cntrct_Sttlmnt_Info_Cde.getValue("*").equals("D046") 
                || dc_Cntrct_Cntrct_Sttlmnt_Info_Cde.getValue("*").equals("D053") || dc_Cntrct_Cntrct_Sttlmnt_Info_Cde.getValue("*").equals("D054"))))
            {
                continue;
            }
            pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr.setValue(dc_Cntrct_Cntrct_Ppcn_Nbr);                                                                        //Natural: ASSIGN #TIAA-CNTRCT-PPCN-NBR := DC-CNTRCT.CNTRCT-PPCN-NBR
            pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(dc_Cntrct_Cntrct_Payee_Cde);                                                                      //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := DC-CNTRCT.CNTRCT-PAYEE-CDE
            pnd_Work_Record_Pnd_1st_Dob.reset();                                                                                                                          //Natural: RESET #1ST-DOB #2ND-DOB #1ST-DOD #2ND-DOD #OPT-DESC #W-CONTRACT-MODE
            pnd_Work_Record_Pnd_2nd_Dob.reset();
            pnd_Work_Record_Pnd_1st_Dod.reset();
            pnd_Work_Record_Pnd_2nd_Dod.reset();
            pnd_Work_Record_Pnd_Opt_Desc.reset();
            pnd_Work_Record_Pnd_W_Contract_Mode.reset();
            vw_cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND CNTRCT WITH CNTRCT-PPCN-NBR = DC-CNTRCT.CNTRCT-PPCN-NBR
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", dc_Cntrct_Cntrct_Ppcn_Nbr, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_cntrct.readNextRow("FIND01")))
            {
                vw_cntrct.setIfNotFoundControlFlag(false);
                DbsUtil.callnat(Iaan011.class , getCurrentProcessState(), cntrct_Cntrct_Optn_Cde, pnd_Work_Record_Pnd_Opt_Desc);                                          //Natural: CALLNAT 'IAAN011' CNTRCT-OPTN-CDE #OPT-DESC
                if (condition(Global.isEscape())) return;
                if (condition(cntrct_Cntrct_First_Annt_Dob_Dte.greater(getZero())))                                                                                       //Natural: IF CNTRCT-FIRST-ANNT-DOB-DTE GT 0
                {
                    pnd_Work_Record_Pnd_W_Dte.setValueEdited(cntrct_Cntrct_First_Annt_Dob_Dte,new ReportEditMask("99999999"));                                            //Natural: MOVE EDITED CNTRCT-FIRST-ANNT-DOB-DTE ( EM = 99999999 ) TO #W-DTE
                    pnd_Work_Record_Pnd_1st_Dob.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_Pnd_W_Mm, "/", pnd_Work_Record_Pnd_W_Dd,         //Natural: COMPRESS #W-MM '/' #W-DD '/' #W-YYYY INTO #1ST-DOB LEAVE NO
                        "/", pnd_Work_Record_Pnd_W_Yyyy));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cntrct_Cntrct_Scnd_Annt_Dob_Dte.greater(getZero())))                                                                                        //Natural: IF CNTRCT-SCND-ANNT-DOB-DTE GT 0
                {
                    pnd_Work_Record_Pnd_W_Dte.setValueEdited(cntrct_Cntrct_Scnd_Annt_Dob_Dte,new ReportEditMask("99999999"));                                             //Natural: MOVE EDITED CNTRCT-SCND-ANNT-DOB-DTE ( EM = 99999999 ) TO #W-DTE
                    pnd_Work_Record_Pnd_2nd_Dob.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_Pnd_W_Mm, "/", pnd_Work_Record_Pnd_W_Dd,         //Natural: COMPRESS #W-MM '/' #W-DD '/' #W-YYYY INTO #2ND-DOB LEAVE NO
                        "/", pnd_Work_Record_Pnd_W_Yyyy));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                                                                                       //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE GT 0
                {
                    pnd_Work_Record_Pnd_W_Dte.setValueEdited(cntrct_Cntrct_First_Annt_Dod_Dte,new ReportEditMask("999999"));                                              //Natural: MOVE EDITED CNTRCT-FIRST-ANNT-DOD-DTE ( EM = 999999 ) TO #W-DTE
                    pnd_Work_Record_Pnd_1st_Dod.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_Pnd_W_Mm, "/", pnd_Work_Record_Pnd_W_Yyyy));     //Natural: COMPRESS #W-MM '/' #W-YYYY INTO #1ST-DOD LEAVE NO
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                        //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    pnd_Work_Record_Pnd_W_Dte.setValueEdited(cntrct_Cntrct_Scnd_Annt_Dod_Dte,new ReportEditMask("999999"));                                               //Natural: MOVE EDITED CNTRCT-SCND-ANNT-DOD-DTE ( EM = 999999 ) TO #W-DTE
                    pnd_Work_Record_Pnd_2nd_Dod.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_Pnd_W_Mm, "/", pnd_Work_Record_Pnd_W_Yyyy));     //Natural: COMPRESS #W-MM '/' #W-YYYY INTO #2ND-DOD LEAVE NO
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet410 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STTLMNT-DECEDENT-TYPE = '1'
            if (condition(pnd_Sttlmnt_Decedent_Type.equals("1")))
            {
                decideConditionsMet410++;
                //*  REVERSAL WAS DONE
                if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(getZero())))                                                                                        //Natural: IF CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Record_Pnd_W_Dod_Dte.setValue(pnd_Sttlmnt_Dod_Dte_1);                                                                                            //Natural: MOVE #STTLMNT-DOD-DTE-1 TO #W-DOD-DTE
                pnd_Work_Record_Pnd_W_Annu_Type.setValue("01");                                                                                                           //Natural: MOVE '01' TO #W-ANNU-TYPE
            }                                                                                                                                                             //Natural: WHEN #STTLMNT-DECEDENT-TYPE = '2'
            else if (condition(pnd_Sttlmnt_Decedent_Type.equals("2")))
            {
                decideConditionsMet410++;
                //*  REVERSAL WAS DONE
                if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero())))                                                                                         //Natural: IF CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Record_Pnd_W_Annu_Type.setValue("02");                                                                                                           //Natural: MOVE '02' TO #W-ANNU-TYPE
                pnd_Work_Record_Pnd_W_Dod_Dte.setValue(pnd_Sttlmnt_Dod_Dte_2);                                                                                            //Natural: MOVE #STTLMNT-DOD-DTE-2 TO #W-DOD-DTE
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet410 > 0))
            {
                pnd_Work_Record_Pnd_Cntrct_Optn_Cde.setValue(dc_Cntrct_Cntrct_Optn_Cde);                                                                                  //Natural: MOVE CNTRCT-OPTN-CDE TO #CNTRCT-OPTN-CDE
                pnd_Work_Record_Pnd_Cntrct_Orgn_Cde.setValue(cntrct_Cntrct_Orgn_Cde);                                                                                     //Natural: MOVE CNTRCT-ORGN-CDE TO #CNTRCT-ORGN-CDE
                pnd_Contract_1_8.setValue(dc_Cntrct_Cntrct_Ppcn_Nbr);                                                                                                     //Natural: MOVE CNTRCT-PPCN-NBR TO #CONTRACT-1-8 #CPK-CONTRACT
                pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract.setValue(dc_Cntrct_Cntrct_Ppcn_Nbr);
                pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee.setValue(dc_Cntrct_Cntrct_Payee_Cde);                                                                                  //Natural: MOVE CNTRCT-PAYEE-CDE TO #CPK-PAYEE
                pnd_Work_Record_Pnd_W_Cont_Stat.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_1_8, "-", pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A));   //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
                pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte.setValue(dc_Cntrct_Cntrct_Issue_Dte);                                                                              //Natural: MOVE CNTRCT-ISSUE-DTE TO #W-CNTRCT-ISSUE-DTE
                pnd_Work_Record_Pnd_Iss_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_Pnd_W_Iss_Mm, "/", pnd_Work_Record_Pnd_W_Iss_Yyyy)); //Natural: COMPRESS #W-ISS-MM '/' #W-ISS-YYYY INTO #ISS-DTE LEAVE NO
                pnd_Work_Record_Pnd_Dth_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_Pnd_Dod_Mm, "/", pnd_Work_Record_Pnd_Dod_Dd,         //Natural: COMPRESS #DOD-MM '/' #DOD-DD '/' #DOD-YYYY INTO #DTH-DTE LEAVE NO
                    "/", pnd_Work_Record_Pnd_Dod_Yyyy));
                                                                                                                                                                          //Natural: PERFORM GET-CPR
                sub_Get_Cpr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-FUND-AND-WRITE-REPORT
                sub_Get_Fund_And_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                if (condition(pnd_Sttlmnt_Decedent_Type.equals("3")))                                                                                                     //Natural: IF #STTLMNT-DECEDENT-TYPE = '3'
                {
                    //*  REVERSAL WAS DONE
                    if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(getZero()) || cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero())))                               //Natural: IF CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0 OR CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Record_Pnd_W_Annu_Type.setValue("01");                                                                                                       //Natural: MOVE '01' TO #W-ANNU-TYPE
                    pnd_Work_Record_Pnd_W_Dod_Dte.setValue(sttlmnt_Pnd_Sttlmnt_Dod_Dte);                                                                                  //Natural: MOVE #STTLMNT-DOD-DTE TO #W-DOD-DTE
                    if (condition(sttlmnt_Pnd_Sttlmnt_Dod_Dte.lessOrEqual(pnd_Sttlmnt_Dod_Dte_2)))                                                                        //Natural: IF #STTLMNT-DOD-DTE NOT > #STTLMNT-DOD-DTE-2
                    {
                        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A.setValue("01");                                                                                              //Natural: MOVE '01' TO #CPK-PAYEE-A
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A.setValue("02");                                                                                              //Natural: MOVE '02' TO #CPK-PAYEE-A
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Contract_1_8.setValue(dc_Cntrct_Cntrct_Ppcn_Nbr);                                                                                                 //Natural: MOVE CNTRCT-PPCN-NBR TO #CONTRACT-1-8 #CPK-CONTRACT
                    pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract.setValue(dc_Cntrct_Cntrct_Ppcn_Nbr);
                    pnd_Work_Record_Pnd_W_Cont_Stat.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_1_8, "-", pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A)); //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
                    pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte.setValue(dc_Cntrct_Cntrct_Issue_Dte);                                                                          //Natural: MOVE CNTRCT-ISSUE-DTE TO #W-CNTRCT-ISSUE-DTE
                    pnd_Work_Record_Pnd_Iss_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_Pnd_W_Iss_Mm, "/", pnd_Work_Record_Pnd_W_Iss_Yyyy)); //Natural: COMPRESS #W-ISS-MM '/' #W-ISS-YYYY INTO #ISS-DTE LEAVE NO
                                                                                                                                                                          //Natural: PERFORM GET-CPR
                    sub_Get_Cpr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee);                                                      //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := #CPK-PAYEE
                                                                                                                                                                          //Natural: PERFORM GET-FUND-AND-WRITE-REPORT
                    sub_Get_Fund_And_Write_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Work_Record_Pnd_W_Annu_Type.setValue("02");                                                                                                       //Natural: MOVE '02' TO #W-ANNU-TYPE
                    pnd_Work_Record_Pnd_W_Dod_Dte.setValue(pnd_Sttlmnt_Dod_Dte_2);                                                                                        //Natural: MOVE #STTLMNT-DOD-DTE-2 TO #W-DOD-DTE
                    if (condition(sttlmnt_Pnd_Sttlmnt_Dod_Dte.lessOrEqual(pnd_Sttlmnt_Dod_Dte_2)))                                                                        //Natural: IF #STTLMNT-DOD-DTE NOT > #STTLMNT-DOD-DTE-2
                    {
                        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A.setValue("02");                                                                                              //Natural: MOVE '02' TO #CPK-PAYEE-A
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A.setValue("01");                                                                                              //Natural: MOVE '01' TO #CPK-PAYEE-A
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Record_Pnd_W_Cont_Stat.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_1_8, "-", pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A)); //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
                                                                                                                                                                          //Natural: PERFORM GET-CPR
                    sub_Get_Cpr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee);                                                      //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := #CPK-PAYEE
                                                                                                                                                                          //Natural: PERFORM GET-FUND-AND-WRITE-REPORT
                    sub_Get_Fund_And_Write_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Fund_And_Write_Report() throws Exception                                                                                                         //Natural: GET-FUND-AND-WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_fund.startDatabaseRead                                                                                                                                         //Natural: READ FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #TIAA-CNTRCT-FUND-KEY
        (
        "READ03",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Tiaa_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_fund.readNextRow("READ03")))
        {
            if (condition(fund_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr) || fund_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde))) //Natural: IF TIAA-CNTRCT-PPCN-NBR NE #TIAA-CNTRCT-PPCN-NBR OR TIAA-CNTRCT-PAYEE-CDE NE #TIAA-CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(fund_Tiaa_Cmpny_Cde.equals("T")))                                                                                                               //Natural: IF TIAA-CMPNY-CDE = 'T'
            {
                if (condition(fund_Tiaa_Fund_Cde.equals("1") || fund_Tiaa_Fund_Cde.equals("1S")))                                                                         //Natural: IF TIAA-FUND-CDE = '1' OR = '1S'
                {
                    pnd_Work_Record_Pnd_W_Fund.setValue("T");                                                                                                             //Natural: ASSIGN #W-FUND := 'T'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_W_Fund.setValue("G");                                                                                                             //Natural: ASSIGN #W-FUND := 'G'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Record_Pnd_W_Reval.setValue(" ");                                                                                                                //Natural: ASSIGN #W-REVAL := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                fund_Tiaa_Tot_Div_Amt.reset();                                                                                                                            //Natural: RESET TIAA-TOT-DIV-AMT
                DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"),true), new ExamineSearch(fund_Tiaa_Fund_Cde,                   //Natural: EXAMINE FULL IAAA051Z.#IA-STD-NM-CD ( * ) FOR FULL TIAA-FUND-CDE GIVING INDEX #I
                    true), new ExamineGivingIndex(pnd_I));
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();                                                                                              //Natural: RESET AUV-RETURNED RETURN-CODE AUV-DATE-RETURN
                    ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();
                    ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
                    ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                              //Natural: ASSIGN IA-FUND-CODE := #W-FUND := IAAA051Z.#IA-STD-ALPHA-CD ( #I )
                    pnd_Work_Record_Pnd_W_Fund.setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "ERROR ERROR ERROR -- INVALID FUND FOR: ",pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract,pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee,             //Natural: WRITE 'ERROR ERROR ERROR -- INVALID FUND FOR: ' #CPK-CONTRACT #CPK-PAYEE / TIAA-FUND-CDE
                        NEWLINE,fund_Tiaa_Fund_Cde);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(64);  if (true) return;                                                                                                             //Natural: TERMINATE 64
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Record_Pnd_W_Reval.setValue("A");                                                                                                                //Natural: ASSIGN #W-REVAL := 'A'
                if (condition(fund_Tiaa_Cmpny_Cde.equals("W") || fund_Tiaa_Cmpny_Cde.equals("4")))                                                                        //Natural: IF TIAA-CMPNY-CDE = 'W' OR = '4'
                {
                    pnd_Work_Record_Pnd_W_Reval.setValue("M");                                                                                                            //Natural: ASSIGN #W-REVAL := 'M'
                                                                                                                                                                          //Natural: PERFORM GET-AUV
                    sub_Get_Auv();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_Pnd_Guar_Amt.setValue(fund_Tiaa_Tot_Per_Amt);                                                                                                 //Natural: ASSIGN #GUAR-AMT := TIAA-TOT-PER-AMT
            pnd_Work_Record_Pnd_Div_Amt.setValue(fund_Tiaa_Tot_Div_Amt);                                                                                                  //Natural: ASSIGN #DIV-AMT := TIAA-TOT-DIV-AMT
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Auv() throws Exception                                                                                                                           //Natural: GET-AUV
    {
        if (BLNatReinput.isReinput()) return;

        //* *====================================================================
        ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();                                                                                                          //Natural: RESET AUV-RETURNED RETURN-CODE AUV-DATE-RETURN
        ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();
        ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                                    //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(ldaIaal050.getIa_Aian026_Linkage_Return_Code().equals(getZero())))                                                                                  //Natural: IF RETURN-CODE = 0
        {
            fund_Tiaa_Tot_Per_Amt.compute(new ComputeParameters(true, fund_Tiaa_Tot_Per_Amt), fund_Tiaa_Units_Cnt.getValue(1).multiply(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned())); //Natural: COMPUTE ROUNDED TIAA-TOT-PER-AMT = TIAA-UNITS-CNT ( 1 ) * AUV-RETURNED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "AIAN026 RETURN CODE NE 0 FOR: ",pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract,pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee,NEWLINE,"=",                  //Natural: WRITE 'AIAN026 RETURN CODE NE 0 FOR: ' #CPK-CONTRACT #CPK-PAYEE / '=' IA-FUND-CODE / '=' IA-CALL-TYPE / '=' IA-REVAL-METHD / '=' IA-CHECK-DATE / '=' RETURN-CODE
                ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code(),NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type(),NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd(),
                NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date(),NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Return_Code());
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, ReportOption.NOTITLE,"/PIN",                                                                                                              //Natural: DISPLAY ( 1 ) NOTITLE '/PIN' #W-PIN 'SSN/EIN' #STTLMNT-SSN ( EM = 999'-'99'-'9999 ZP = OFF ) '/COMPANY' #W-CMPNY '/CONT STAT' #W-CONT-STAT '/PY' #W-ANNU-TYPE '/MODE' #W-CONTRACT-MODE 'DATE OF/ISSUE' #ISS-DTE '/OPTN' #CNTRCT-OPTN-CDE ( EM = 99 ) '/ORGN' #CNTRCT-ORGN-CDE ( EM = 99 ) 'DATE OF/DEATH' #DTH-DTE 'TRANS/DATE' #PROC-DATE '1ST ANNT/DOB' #1ST-DOB '2ND ANNT/DOB' #2ND-DOB '1ST ANNT/DOD' #1ST-DOD '2ND ANNT/DOD' #2ND-DOD '/FUND' #W-FUND 'REVAL/MTHD' #W-REVAL 'GUAR/AMOUNT' #GUAR-AMT ( EM = ZZZZ,ZZ9.99 ZP = OFF ) 'DIV/AMOUNT' #DIV-AMT ( EM = ZZZZ,ZZ9.99 ZP = OFF ) '/ANNUITY OPTION' #OPT-DESC
        		pnd_Work_Record_Pnd_W_Pin,"SSN/EIN",
        		pnd_Sttlmnt_Ssn, new ReportEditMask ("999'-'99'-'9999"), new ReportZeroPrint (false),"/COMPANY",
        		pnd_Work_Record_Pnd_W_Cmpny,"/CONT STAT",
        		pnd_Work_Record_Pnd_W_Cont_Stat,"/PY",
        		pnd_Work_Record_Pnd_W_Annu_Type,"/MODE",
        		pnd_Work_Record_Pnd_W_Contract_Mode,"DATE OF/ISSUE",
        		pnd_Work_Record_Pnd_Iss_Dte,"/OPTN",
        		pnd_Work_Record_Pnd_Cntrct_Optn_Cde, new ReportEditMask ("99"),"/ORGN",
        		pnd_Work_Record_Pnd_Cntrct_Orgn_Cde, new ReportEditMask ("99"),"DATE OF/DEATH",
        		pnd_Work_Record_Pnd_Dth_Dte,"TRANS/DATE",
        		pnd_Work_Record_Pnd_Proc_Date,"1ST ANNT/DOB",
        		pnd_Work_Record_Pnd_1st_Dob,"2ND ANNT/DOB",
        		pnd_Work_Record_Pnd_2nd_Dob,"1ST ANNT/DOD",
        		pnd_Work_Record_Pnd_1st_Dod,"2ND ANNT/DOD",
        		pnd_Work_Record_Pnd_2nd_Dod,"/FUND",
        		pnd_Work_Record_Pnd_W_Fund,"REVAL/MTHD",
        		pnd_Work_Record_Pnd_W_Reval,"GUAR/AMOUNT",
        		pnd_Work_Record_Pnd_Guar_Amt, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"DIV/AMOUNT",
        		pnd_Work_Record_Pnd_Div_Amt, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"/ANNUITY OPTION",
        		pnd_Work_Record_Pnd_Opt_Desc);
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cpr() throws Exception                                                                                                                           //Natural: GET-CPR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_cpr.startDatabaseFind                                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_cpr.readNextRow("FIND02")))
        {
            vw_cpr.setIfNotFoundControlFlag(false);
            pnd_Work_Record_Pnd_W_Contract_Mode.setValue(cpr_Cntrct_Mode_Ind);                                                                                            //Natural: MOVE CNTRCT-MODE-IND TO #W-CONTRACT-MODE
            if (condition(cpr_Cntrct_Company_Cd.getValue(1).equals("T")))                                                                                                 //Natural: IF CNTRCT-COMPANY-CD ( 1 ) = 'T'
            {
                pnd_Work_Record_Pnd_W_Cmpny.setValue("TIAA");                                                                                                             //Natural: ASSIGN #W-CMPNY := 'TIAA'
                if (condition(cntrct_Cntrct_Orgn_Cde.equals(pnd_Tc_Life.getValue("*"))))                                                                                  //Natural: IF CNTRCT.CNTRCT-ORGN-CDE = #TC-LIFE ( * )
                {
                    pnd_Work_Record_Pnd_W_Cmpny.setValue("TCLIFE");                                                                                                       //Natural: ASSIGN #W-CMPNY := 'TCLIFE'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Record_Pnd_W_Cmpny.setValue("CREF");                                                                                                             //Natural: ASSIGN #W-CMPNY := 'CREF'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new ColumnSpacing(75),"IA DEATH TRANSACTIONS REPORT FOR",pnd_B_Dte, new ReportEditMask ("MM/DD/YYYY"),"THRU",pnd_E_Dte,         //Natural: WRITE ( 1 ) 75X 'IA DEATH TRANSACTIONS REPORT FOR' #B-DTE ( EM = MM/DD/YYYY ) 'THRU' #E-DTE ( EM = MM/DD/YYYY ) / 90X 'PROCESSING DATE:' *DATX ( EM = MM/DD/YYYY )
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new ColumnSpacing(90),"PROCESSING DATE:",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=250");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"/PIN",
        		pnd_Work_Record_Pnd_W_Pin,"SSN/EIN",
        		pnd_Sttlmnt_Ssn, new ReportEditMask ("999'-'99'-'9999"), new ReportZeroPrint (false),"/COMPANY",
        		pnd_Work_Record_Pnd_W_Cmpny,"/CONT STAT",
        		pnd_Work_Record_Pnd_W_Cont_Stat,"/PY",
        		pnd_Work_Record_Pnd_W_Annu_Type,"/MODE",
        		pnd_Work_Record_Pnd_W_Contract_Mode,"DATE OF/ISSUE",
        		pnd_Work_Record_Pnd_Iss_Dte,"/OPTN",
        		pnd_Work_Record_Pnd_Cntrct_Optn_Cde, new ReportEditMask ("99"),"/ORGN",
        		pnd_Work_Record_Pnd_Cntrct_Orgn_Cde, new ReportEditMask ("99"),"DATE OF/DEATH",
        		pnd_Work_Record_Pnd_Dth_Dte,"TRANS/DATE",
        		pnd_Work_Record_Pnd_Proc_Date,"1ST ANNT/DOB",
        		pnd_Work_Record_Pnd_1st_Dob,"2ND ANNT/DOB",
        		pnd_Work_Record_Pnd_2nd_Dob,"1ST ANNT/DOD",
        		pnd_Work_Record_Pnd_1st_Dod,"2ND ANNT/DOD",
        		pnd_Work_Record_Pnd_2nd_Dod,"/FUND",
        		pnd_Work_Record_Pnd_W_Fund,"REVAL/MTHD",
        		pnd_Work_Record_Pnd_W_Reval,"GUAR/AMOUNT",
        		pnd_Work_Record_Pnd_Guar_Amt, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"DIV/AMOUNT",
        		pnd_Work_Record_Pnd_Div_Amt, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"/ANNUITY OPTION",
        		pnd_Work_Record_Pnd_Opt_Desc);
    }
}
