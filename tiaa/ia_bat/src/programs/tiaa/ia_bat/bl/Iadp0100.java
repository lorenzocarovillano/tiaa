/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:51 PM
**        * FROM NATURAL PROGRAM : Iadp0100
************************************************************
**        * FILE NAME            : Iadp0100.java
**        * CLASS NAME           : Iadp0100
**        * INSTANCE NAME        : Iadp0100
************************************************************
************************************************************************
* PROGRAM  : IADP0100
* SYSTEM   : IAS
* TITLE    : IADP0100
* GENERATED: JAN 19,95 AT 02:03 PM
* FUNCTION : THIS PROGRAM ... INITIAL SELECT OF COR ACTIVE IA CONTRACTS
*                             FOR IA COMBINE CHECK CHECK PROCESSING
*
**SAG PRIMARY-FILE: COR-XREF-CNTRCT
**    PRIMARY-KEY:  COR-SUPER-LOB-CNTRCT-PAYEE
*
* HISTORY  CHANGED 7/10/95 ADDED IF STATEMENT TO ESCAPE BOTTOM
*                          WHEN LINE OF BUSINESS NOT AN IA CONTRACT
*          CHANGED 8/97    ADDED NEW PREFIXES Y & Z
*                          DO SCAN ON 8/97 FOR CHANGE
*          CHANGED 05/2015 READ ETL FILE INSTEAD OF COR AS PRIMARY FILE
*          04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
*          05/2017 RC FIX COMBINE CONTRACT PROCESS TO INCLUDE MODE-IND
*                  RC082017
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadp0100 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_iaa_Contract;
    private DbsField iaa_Contract_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Contract_Cntrct_Optn_Cde;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField pnd_Read_Cpr;

    private DbsGroup pnd_Read_Cpr__R_Field_1;
    private DbsField pnd_Read_Cpr_Pnd_Read_Ppcn;
    private DbsField pnd_Read_Cpr_Pnd_Read_Payee;

    private DbsGroup pnd_Etl;
    private DbsField pnd_Etl_Pnd_Etl_Cp;

    private DbsGroup pnd_Etl__R_Field_2;
    private DbsField pnd_Etl_Pnd_Cntrct_Nbr;
    private DbsField pnd_Etl_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Etl_Pnd_Ph_Unique_Id_Nbr;
    private DbsField pnd_Etl_Pnd_Ph_Rcd_Type_Cde;
    private DbsField pnd_Etl_Pnd_Ph_Social_Security_No;
    private DbsField pnd_Etl_Pnd_Ph_Dob_Dte;
    private DbsField pnd_Etl_Pnd_Ph_Dod_Dte;
    private DbsField pnd_Etl_Pnd_Ph_Last_Nme;
    private DbsField pnd_Etl_Pnd_Ph_First_Nme;
    private DbsField pnd_Etl_Pnd_Ph_Mddle_Nme;

    private DbsGroup pnd_Etl__R_Field_3;
    private DbsField pnd_Etl_Pnd_Ph_Mddle_Init;
    private DbsField pnd_Etl_Pnd_Cntrct_Status_Cde;
    private DbsField pnd_Etl_Pnd_Cor_Sex_Cde;

    private DbsGroup pnd_Pin_Out_Rec;
    private DbsField pnd_Pin_Out_Rec_Pnd_Pin_Pin_Nbr;
    private DbsField pnd_Pin_Out_Rec_Pnd_Pin_Contract_No;

    private DbsGroup pnd_Pin_Out_Rec__R_Field_4;
    private DbsField pnd_Pin_Out_Rec_Pnd_Pin_Contract_Pfx;
    private DbsField pnd_Pin_Out_Rec_Pnd_Pin_Nbr;
    private DbsField pnd_Pin_Out_Rec_Pnd_Pin_Payee_Cde;
    private DbsField pnd_Pin_Out_Rec_Pnd_Mode_Ind;
    private DbsField pnd_Pin_Out_Rec_Pnd_Pin_Fill;
    private DbsField pnd_Tot_Rec_Out;
    private DbsField pnd_Tot_Tiaa_Contr;
    private DbsField pnd_Tot_Cref_Contr;
    private DbsField pnd_Tot_Payee_01;
    private DbsField pnd_Tot_Payee_02;
    private DbsField pnd_Tot_Payee_Bene;
    private DbsField pnd_Save_Low_Contr;
    private DbsField pnd_Cntrct_Cnt;
    private DbsField pnd_First_Tme;
    private DbsField pnd_Accept_Ppcn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_iaa_Contract = new DataAccessProgramView(new NameInfo("vw_iaa_Contract", "IAA-CONTRACT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Contract_Cntrct_Ppcn_Nbr = vw_iaa_Contract.getRecord().newFieldInGroup("iaa_Contract_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Contract_Cntrct_Optn_Cde = vw_iaa_Contract.getRecord().newFieldInGroup("iaa_Contract_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        registerRecord(vw_iaa_Contract);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        registerRecord(vw_cpr);

        pnd_Read_Cpr = localVariables.newFieldInRecord("pnd_Read_Cpr", "#READ-CPR", FieldType.STRING, 12);

        pnd_Read_Cpr__R_Field_1 = localVariables.newGroupInRecord("pnd_Read_Cpr__R_Field_1", "REDEFINE", pnd_Read_Cpr);
        pnd_Read_Cpr_Pnd_Read_Ppcn = pnd_Read_Cpr__R_Field_1.newFieldInGroup("pnd_Read_Cpr_Pnd_Read_Ppcn", "#READ-PPCN", FieldType.STRING, 10);
        pnd_Read_Cpr_Pnd_Read_Payee = pnd_Read_Cpr__R_Field_1.newFieldInGroup("pnd_Read_Cpr_Pnd_Read_Payee", "#READ-PAYEE", FieldType.STRING, 2);

        pnd_Etl = localVariables.newGroupInRecord("pnd_Etl", "#ETL");
        pnd_Etl_Pnd_Etl_Cp = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Etl_Cp", "#ETL-CP", FieldType.STRING, 12);

        pnd_Etl__R_Field_2 = pnd_Etl.newGroupInGroup("pnd_Etl__R_Field_2", "REDEFINE", pnd_Etl_Pnd_Etl_Cp);
        pnd_Etl_Pnd_Cntrct_Nbr = pnd_Etl__R_Field_2.newFieldInGroup("pnd_Etl_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Etl_Pnd_Cntrct_Payee_Cde = pnd_Etl__R_Field_2.newFieldInGroup("pnd_Etl_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Etl_Pnd_Ph_Unique_Id_Nbr = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_Unique_Id_Nbr", "#PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Etl_Pnd_Ph_Rcd_Type_Cde = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_Rcd_Type_Cde", "#PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Etl_Pnd_Ph_Social_Security_No = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_Social_Security_No", "#PH-SOCIAL-SECURITY-NO", FieldType.NUMERIC, 
            9);
        pnd_Etl_Pnd_Ph_Dob_Dte = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_Dob_Dte", "#PH-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Etl_Pnd_Ph_Dod_Dte = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_Dod_Dte", "#PH-DOD-DTE", FieldType.NUMERIC, 8);
        pnd_Etl_Pnd_Ph_Last_Nme = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_Last_Nme", "#PH-LAST-NME", FieldType.STRING, 30);
        pnd_Etl_Pnd_Ph_First_Nme = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_First_Nme", "#PH-FIRST-NME", FieldType.STRING, 30);
        pnd_Etl_Pnd_Ph_Mddle_Nme = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Ph_Mddle_Nme", "#PH-MDDLE-NME", FieldType.STRING, 30);

        pnd_Etl__R_Field_3 = pnd_Etl.newGroupInGroup("pnd_Etl__R_Field_3", "REDEFINE", pnd_Etl_Pnd_Ph_Mddle_Nme);
        pnd_Etl_Pnd_Ph_Mddle_Init = pnd_Etl__R_Field_3.newFieldInGroup("pnd_Etl_Pnd_Ph_Mddle_Init", "#PH-MDDLE-INIT", FieldType.STRING, 1);
        pnd_Etl_Pnd_Cntrct_Status_Cde = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Cntrct_Status_Cde", "#CNTRCT-STATUS-CDE", FieldType.STRING, 1);
        pnd_Etl_Pnd_Cor_Sex_Cde = pnd_Etl.newFieldInGroup("pnd_Etl_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);

        pnd_Pin_Out_Rec = localVariables.newGroupInRecord("pnd_Pin_Out_Rec", "#PIN-OUT-REC");
        pnd_Pin_Out_Rec_Pnd_Pin_Pin_Nbr = pnd_Pin_Out_Rec.newFieldInGroup("pnd_Pin_Out_Rec_Pnd_Pin_Pin_Nbr", "#PIN-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Pin_Out_Rec_Pnd_Pin_Contract_No = pnd_Pin_Out_Rec.newFieldInGroup("pnd_Pin_Out_Rec_Pnd_Pin_Contract_No", "#PIN-CONTRACT-NO", FieldType.STRING, 
            8);

        pnd_Pin_Out_Rec__R_Field_4 = pnd_Pin_Out_Rec.newGroupInGroup("pnd_Pin_Out_Rec__R_Field_4", "REDEFINE", pnd_Pin_Out_Rec_Pnd_Pin_Contract_No);
        pnd_Pin_Out_Rec_Pnd_Pin_Contract_Pfx = pnd_Pin_Out_Rec__R_Field_4.newFieldInGroup("pnd_Pin_Out_Rec_Pnd_Pin_Contract_Pfx", "#PIN-CONTRACT-PFX", 
            FieldType.STRING, 2);
        pnd_Pin_Out_Rec_Pnd_Pin_Nbr = pnd_Pin_Out_Rec__R_Field_4.newFieldInGroup("pnd_Pin_Out_Rec_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 6);
        pnd_Pin_Out_Rec_Pnd_Pin_Payee_Cde = pnd_Pin_Out_Rec.newFieldInGroup("pnd_Pin_Out_Rec_Pnd_Pin_Payee_Cde", "#PIN-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Pin_Out_Rec_Pnd_Mode_Ind = pnd_Pin_Out_Rec.newFieldInGroup("pnd_Pin_Out_Rec_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Pin_Out_Rec_Pnd_Pin_Fill = pnd_Pin_Out_Rec.newFieldInGroup("pnd_Pin_Out_Rec_Pnd_Pin_Fill", "#PIN-FILL", FieldType.STRING, 1);
        pnd_Tot_Rec_Out = localVariables.newFieldInRecord("pnd_Tot_Rec_Out", "#TOT-REC-OUT", FieldType.NUMERIC, 9);
        pnd_Tot_Tiaa_Contr = localVariables.newFieldInRecord("pnd_Tot_Tiaa_Contr", "#TOT-TIAA-CONTR", FieldType.NUMERIC, 7);
        pnd_Tot_Cref_Contr = localVariables.newFieldInRecord("pnd_Tot_Cref_Contr", "#TOT-CREF-CONTR", FieldType.NUMERIC, 7);
        pnd_Tot_Payee_01 = localVariables.newFieldInRecord("pnd_Tot_Payee_01", "#TOT-PAYEE-01", FieldType.NUMERIC, 7);
        pnd_Tot_Payee_02 = localVariables.newFieldInRecord("pnd_Tot_Payee_02", "#TOT-PAYEE-02", FieldType.NUMERIC, 7);
        pnd_Tot_Payee_Bene = localVariables.newFieldInRecord("pnd_Tot_Payee_Bene", "#TOT-PAYEE-BENE", FieldType.NUMERIC, 7);
        pnd_Save_Low_Contr = localVariables.newFieldInRecord("pnd_Save_Low_Contr", "#SAVE-LOW-CONTR", FieldType.STRING, 8);
        pnd_Cntrct_Cnt = localVariables.newFieldInRecord("pnd_Cntrct_Cnt", "#CNTRCT-CNT", FieldType.NUMERIC, 4);
        pnd_First_Tme = localVariables.newFieldInRecord("pnd_First_Tme", "#FIRST-TME", FieldType.BOOLEAN, 1);
        pnd_Accept_Ppcn = localVariables.newFieldInRecord("pnd_Accept_Ppcn", "#ACCEPT-PPCN", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Contract.reset();
        vw_cpr.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Tot_Rec_Out.setInitialValue(0);
        pnd_Tot_Tiaa_Contr.setInitialValue(0);
        pnd_Tot_Cref_Contr.setInitialValue(0);
        pnd_Tot_Payee_01.setInitialValue(0);
        pnd_Tot_Payee_02.setInitialValue(0);
        pnd_Tot_Payee_Bene.setInitialValue(0);
        pnd_Save_Low_Contr.setInitialValue("99999999");
        pnd_Cntrct_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iadp0100() throws Exception
    {
        super("Iadp0100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 56
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        pnd_First_Tme.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #FIRST-TME
        READ_PRIME:                                                                                                                                                       //Natural: READ WORK FILE 2 #ETL
        while (condition(getWorkFiles().read(2, pnd_Etl)))
        {
            if (condition(pnd_Etl_Pnd_Etl_Cp.less("GA000000  00")))                                                                                                       //Natural: IF #ETL-CP < 'GA000000  00'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Etl_Pnd_Cntrct_Status_Cde.equals(" ") && pnd_Etl_Pnd_Ph_Unique_Id_Nbr.greater(getZero()))))                                               //Natural: ACCEPT IF #CNTRCT-STATUS-CDE = ' ' AND #PH-UNIQUE-ID-NBR > 0
            {
                continue;
            }
            if (condition(pnd_Etl_Pnd_Cntrct_Nbr.getSubstring(1,7).compareTo("W024999") > 0 && pnd_Etl_Pnd_Cntrct_Nbr.getSubstring(1,7).compareTo("W090000")              //Natural: IF SUBSTR ( #CNTRCT-NBR,1,7 ) GT 'W024999' AND SUBSTR ( #CNTRCT-NBR,1,7 ) LT 'W090000'
                < 0))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'GA'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'GW'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'IA'")  //Natural: IF #CNTRCT-NBR = MASK ( 'GA' ) OR = MASK ( 'GW' ) OR = MASK ( 'IA' ) OR = MASK ( 'IB' ) OR = MASK ( 'IC' ) OR = MASK ( 'ID' ) OR = MASK ( 'IE' ) OR = MASK ( 'IF' ) OR = MASK ( 'Y' ) OR = MASK ( 'IG' ) OR = MASK ( 'IH' ) OR = MASK ( 'S0' ) OR = MASK ( 'W0' ) OR = MASK ( '0L' ) OR = MASK ( '0M' ) OR = MASK ( '0N' ) OR = MASK ( '0U' ) OR = MASK ( '0T' ) OR = MASK ( 'Z' ) OR = MASK ( '6L' ) OR = MASK ( '6M' ) OR = MASK ( '6N' )
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'IB'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'IC'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'ID'") 
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'IE'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'IF'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'Y'") 
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'IG'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'IH'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'S0'") 
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'W0'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0L'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0M'") 
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0N'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0U'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0T'") 
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'Z'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'6L'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'6M'") 
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'6N'")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*     READ IAA-CNTRCT WITH #CNTRCT-NBR
            pnd_Accept_Ppcn.setValue(false);                                                                                                                              //Natural: ASSIGN #ACCEPT-PPCN := FALSE
            vw_iaa_Contract.startDatabaseFind                                                                                                                             //Natural: FIND IAA-CONTRACT WITH CNTRCT-PPCN-NBR = #CNTRCT-NBR
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Etl_Pnd_Cntrct_Nbr, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_iaa_Contract.readNextRow("FIND01")))
            {
                vw_iaa_Contract.setIfNotFoundControlFlag(false);
                if (condition(iaa_Contract_Cntrct_Optn_Cde.equals(21) || iaa_Contract_Cntrct_Optn_Cde.equals(22) || iaa_Contract_Cntrct_Optn_Cde.equals(23)               //Natural: IF CNTRCT-OPTN-CDE = 21 OR = 22 OR = 23 OR = 25 OR = 26 OR = 27 OR = 28 OR = 30
                    || iaa_Contract_Cntrct_Optn_Cde.equals(25) || iaa_Contract_Cntrct_Optn_Cde.equals(26) || iaa_Contract_Cntrct_Optn_Cde.equals(27) || 
                    iaa_Contract_Cntrct_Optn_Cde.equals(28) || iaa_Contract_Cntrct_Optn_Cde.equals(30)))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Accept_Ppcn.setValue(true);                                                                                                                       //Natural: ASSIGN #ACCEPT-PPCN := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_Accept_Ppcn.getBoolean())))                                                                                                              //Natural: IF NOT #ACCEPT-PPCN
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  RC082017
                                                                                                                                                                          //Natural: PERFORM GET-MODE-IND-FROM-CPR
            sub_Get_Mode_Ind_From_Cpr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_Accept_Ppcn.getBoolean())))                                                                                                              //Natural: IF NOT #ACCEPT-PPCN
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTRACT-RECS-FOR-PIN
            sub_Write_Contract_Recs_For_Pin();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Etl_Pnd_Cntrct_Payee_Cde.equals("01")))                                                                                                     //Natural: IF #CNTRCT-PAYEE-CDE = '01'
            {
                pnd_Tot_Payee_01.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-PAYEE-01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Etl_Pnd_Cntrct_Payee_Cde.equals("02")))                                                                                                 //Natural: IF #CNTRCT-PAYEE-CDE = '02'
                {
                    pnd_Tot_Payee_02.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-PAYEE-02
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tot_Payee_Bene.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOT-PAYEE-BENE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ADD Z 8/97
            if (condition(DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0L'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0M'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0N'")  //Natural: IF #CNTRCT-NBR = MASK ( '0L' ) OR = MASK ( '0M' ) OR = MASK ( '0N' ) OR = MASK ( '0U' ) OR = MASK ( '0T' ) OR = MASK ( 'Z' ) OR = MASK ( '6L' ) OR = MASK ( '6M' ) OR = MASK ( '6N' )
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0U'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'0T'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'Z'") 
                || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'6L'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,"'6M'") || DbsUtil.maskMatches(pnd_Etl_Pnd_Cntrct_Nbr,
                "'6N'")))
            {
                pnd_Tot_Cref_Contr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-CREF-CONTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Tiaa_Contr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-TIAA-CONTR
            }                                                                                                                                                             //Natural: END-IF
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-WORK
        READ_PRIME_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL RECORDS OUT ..........",pnd_Tot_Rec_Out);                                                              //Natural: WRITE ( 1 ) / ' TOTAL RECORDS OUT ..........' #TOT-REC-OUT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL TIAA CONTRACTS........",pnd_Tot_Tiaa_Contr);                                                           //Natural: WRITE ( 1 ) / ' TOTAL TIAA CONTRACTS........' #TOT-TIAA-CONTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL CREF CONTRACTS........",pnd_Tot_Cref_Contr);                                                           //Natural: WRITE ( 1 ) / ' TOTAL CREF CONTRACTS........' #TOT-CREF-CONTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL PAYEE 01 CONTRACTS ...",pnd_Tot_Payee_01);                                                             //Natural: WRITE ( 1 ) / ' TOTAL PAYEE 01 CONTRACTS ...' #TOT-PAYEE-01
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL PAYEE 02 CONTRACTS ...",pnd_Tot_Payee_02);                                                             //Natural: WRITE ( 1 ) / ' TOTAL PAYEE 02 CONTRACTS ...' #TOT-PAYEE-02
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL OTHR PAYEE CONTRACTS .",pnd_Tot_Payee_Bene);                                                           //Natural: WRITE ( 1 ) / ' TOTAL OTHR PAYEE CONTRACTS .' #TOT-PAYEE-BENE
        if (Global.isEscape()) return;
        //*  *************************************************
        //*  FORMAT OUTPUT RECORDS
        //*  *************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTRACT-RECS-FOR-PIN
        //*  ------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MODE-IND-FROM-CPR
        //*  ------------------------------------------------------------------
        //* *
    }
    private void sub_Write_Contract_Recs_For_Pin() throws Exception                                                                                                       //Natural: WRITE-CONTRACT-RECS-FOR-PIN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pin_Out_Rec_Pnd_Pin_Contract_No.setValue(pnd_Etl_Pnd_Cntrct_Nbr);                                                                                             //Natural: ASSIGN #PIN-CONTRACT-NO := #CNTRCT-NBR
        pnd_Pin_Out_Rec_Pnd_Pin_Payee_Cde.setValue(pnd_Etl_Pnd_Cntrct_Payee_Cde);                                                                                         //Natural: ASSIGN #PIN-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        pnd_Pin_Out_Rec_Pnd_Pin_Pin_Nbr.setValue(pnd_Etl_Pnd_Ph_Unique_Id_Nbr);                                                                                           //Natural: ASSIGN #PIN-PIN-NBR := #PH-UNIQUE-ID-NBR
        pnd_Tot_Rec_Out.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOT-REC-OUT
        getWorkFiles().write(1, false, pnd_Pin_Out_Rec);                                                                                                                  //Natural: WRITE WORK FILE 1 #PIN-OUT-REC
    }
    //*  RC082017
    private void sub_Get_Mode_Ind_From_Cpr() throws Exception                                                                                                             //Natural: GET-MODE-IND-FROM-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Read_Cpr_Pnd_Read_Ppcn.setValue(pnd_Etl_Pnd_Cntrct_Nbr);                                                                                                      //Natural: ASSIGN #READ-PPCN := #CNTRCT-NBR
        pnd_Read_Cpr_Pnd_Read_Payee.setValue(pnd_Etl_Pnd_Cntrct_Payee_Cde);                                                                                               //Natural: ASSIGN #READ-PAYEE := #CNTRCT-PAYEE-CDE
        pnd_Pin_Out_Rec_Pnd_Mode_Ind.reset();                                                                                                                             //Natural: RESET #MODE-IND #ACCEPT-PPCN
        pnd_Accept_Ppcn.reset();
        vw_cpr.startDatabaseFind                                                                                                                                          //Natural: FIND ( 1 ) CPR WITH CNTRCT-PAYEE-KEY = #READ-CPR
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Read_Cpr, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_cpr.readNextRow("FIND02", true)))
        {
            vw_cpr.setIfNotFoundControlFlag(false);
            if (condition(vw_cpr.getAstCOUNTER().equals(0)))                                                                                                              //Natural: IF NO RECORD FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(cpr_Cntrct_Actvty_Cde.notEquals(1)))                                                                                                            //Natural: IF CNTRCT-ACTVTY-CDE NE 1
            {
                getReports().write(0, "TERMINATED ",pnd_Read_Cpr);                                                                                                        //Natural: WRITE 'TERMINATED ' #READ-CPR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pin_Out_Rec_Pnd_Mode_Ind.setValue(cpr_Cntrct_Mode_Ind);                                                                                                   //Natural: ASSIGN #MODE-IND := CNTRCT-MODE-IND
            pnd_Accept_Ppcn.setValue(true);                                                                                                                               //Natural: ASSIGN #ACCEPT-PPCN := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(22),"EXTRACT OF IA CONTRACTS OFF COR FILE",new TabSetting(72),"Page",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 22T 'EXTRACT OF IA CONTRACTS OFF COR FILE' 72T 'Page' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 21T 'NEW ISSUES 304 COMBINE CHECK PROCESSING' 72T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(21),"NEW ISSUES 304 COMBINE CHECK PROCESSING",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=56");
    }
}
