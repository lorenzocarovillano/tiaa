/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:17 PM
**        * FROM NATURAL PROGRAM : Iaap385
************************************************************
**        * FILE NAME            : Iaap385.java
**        * CLASS NAME           : Iaap385
**        * INSTANCE NAME        : Iaap385
************************************************************
************************************************************************
*  PROGRAM: IAAP385
*   AUTHOR: ARI GROSSMAN
*     DATE: AUG 08, 1998
*  PURPOSE: READ FILE OF CONTRACTS AND CHANGE THE PERIODIC PAY AMT
*           AND THE ASSOCIATED BEFORE AND AFTER IMAGES, HISTORICAL
*           FILE AND CREATE A 50 TRANSACTION.
* MODIFICATION HISTORY
*
* 12/04/2007:CHANGED LAYOUT TO REMOVE FILLERS AND CONVERTED PARM INTO
* JUN TINIO :TSO DATA SET PIA.ANN.IA.GRP.ADJUSTMT
*
* 03/24/2012:RECATALOGED DUE TO CHANGES IN IAAL201E.
* 04/2017 OS RE-STOWED FOR IAAL200B, IAAL202B, AND IAAL202G PIN EXP.
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap385 extends BLNatBase
{
    // Data Areas
    private LdaIaal200a ldaIaal200a;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal201e ldaIaal201e;
    private LdaIaal202a ldaIaal202a;
    private LdaIaal202b ldaIaal202b;
    private LdaIaal202e ldaIaal202e;
    private LdaIaal202g ldaIaal202g;
    private LdaIaal202h ldaIaal202h;
    private LdaIaal205a ldaIaal205a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_1;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_2;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_3;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr_1;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde_1;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte_1;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde_1;
    private DbsField pnd_Check_Date_A;

    private DbsGroup pnd_Check_Date_A__R_Field_4;
    private DbsField pnd_Check_Date_A_Pnd_Check_Date;
    private DbsField pnd_Todays_Date_A;

    private DbsGroup pnd_Todays_Date_A__R_Field_5;
    private DbsField pnd_Todays_Date_A_Pnd_Todays_Date;
    private DbsField pnd_Time;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_6;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_I;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_Work_File_1;

    private DbsGroup pnd_Work_File_1__R_Field_7;
    private DbsField pnd_Work_File_1_Pnd_W1_Cntrct;
    private DbsField pnd_Work_File_1_Pnd_W1_Payee_A;

    private DbsGroup pnd_Work_File_1__R_Field_8;
    private DbsField pnd_Work_File_1_Pnd_W1_Payee;
    private DbsField pnd_Work_File_1_Pnd_W1_Before_Pay;
    private DbsField pnd_Work_File_1_Pnd_W1_After_Pay;
    private DbsField pnd_Work_File_1_Pnd_W1_Filler4;
    private DbsField pnd_Tot_Before_Pay;
    private DbsField pnd_Tot_After_Pay;
    private DbsField pnd_Tot_Diff;
    private DbsField pnd_Count_Reads;
    private DbsField pnd_Count_Writes;
    private DbsField pnd_Diff;
    private DbsField pnd_T1_Found;
    private DbsField pnd_Record_Updated_Already;
    private DbsField pnd_Before_Update_Error;
    private DbsField pnd_Last_Chk_Dt;

    private DbsGroup pnd_Last_Chk_Dt__R_Field_9;
    private DbsField pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N;
    private DbsField pnd_Last_Dt;

    private DbsGroup pnd_Last_Dt__R_Field_10;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Mm;
    private DbsField pnd_Last_Dt_Pnd_Filler_1;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Dd;
    private DbsField pnd_Last_Dt_Pnd_Filler_2;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Yyyy;
    private DbsField pnd_Next_Dt;
    private DbsField pnd_W_Cnt_Mode;
    private DbsField pnd_Page_Ctr;
    private DbsField pnd_Page_Ctr_2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal201e = new LdaIaal201e();
        registerRecord(ldaIaal201e);
        registerRecord(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());
        ldaIaal202a = new LdaIaal202a();
        registerRecord(ldaIaal202a);
        registerRecord(ldaIaal202a.getVw_iaa_Cntrct_Trans());
        ldaIaal202b = new LdaIaal202b();
        registerRecord(ldaIaal202b);
        registerRecord(ldaIaal202b.getVw_iaa_Cpr_Trans());
        ldaIaal202e = new LdaIaal202e();
        registerRecord(ldaIaal202e);
        registerRecord(ldaIaal202e.getVw_iaa_Tiaa_Fund_Trans());
        ldaIaal202g = new LdaIaal202g();
        registerRecord(ldaIaal202g);
        registerRecord(ldaIaal202g.getVw_iaa_Trans_Rcrd());
        ldaIaal202h = new LdaIaal202h();
        registerRecord(ldaIaal202h);
        registerRecord(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1());
        ldaIaal205a = new LdaIaal205a();
        registerRecord(ldaIaal205a);
        registerRecord(ldaIaal205a.getVw_old_Tiaa_Rates());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_1", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_1.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_1.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr_1 = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr_1", 
            "#CNTRCT-PPCN-NBR-1", FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde_1 = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde_1", 
            "#CNTRCT-PAYEE-CDE-1", FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte_1 = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte_1", 
            "#FUND-INVRSE-LST-PD-DTE-1", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde_1 = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde_1", "#CMPNY-FUND-CDE-1", 
            FieldType.STRING, 3);
        pnd_Check_Date_A = localVariables.newFieldInRecord("pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 8);

        pnd_Check_Date_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Check_Date_A__R_Field_4", "REDEFINE", pnd_Check_Date_A);
        pnd_Check_Date_A_Pnd_Check_Date = pnd_Check_Date_A__R_Field_4.newFieldInGroup("pnd_Check_Date_A_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Todays_Date_A = localVariables.newFieldInRecord("pnd_Todays_Date_A", "#TODAYS-DATE-A", FieldType.STRING, 8);

        pnd_Todays_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Todays_Date_A__R_Field_5", "REDEFINE", pnd_Todays_Date_A);
        pnd_Todays_Date_A_Pnd_Todays_Date = pnd_Todays_Date_A__R_Field_5.newFieldInGroup("pnd_Todays_Date_A_Pnd_Todays_Date", "#TODAYS-DATE", FieldType.NUMERIC, 
            8);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_6", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_File_1 = localVariables.newFieldInRecord("pnd_Work_File_1", "#WORK-FILE-1", FieldType.STRING, 70);

        pnd_Work_File_1__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_File_1__R_Field_7", "REDEFINE", pnd_Work_File_1);
        pnd_Work_File_1_Pnd_W1_Cntrct = pnd_Work_File_1__R_Field_7.newFieldInGroup("pnd_Work_File_1_Pnd_W1_Cntrct", "#W1-CNTRCT", FieldType.STRING, 10);
        pnd_Work_File_1_Pnd_W1_Payee_A = pnd_Work_File_1__R_Field_7.newFieldInGroup("pnd_Work_File_1_Pnd_W1_Payee_A", "#W1-PAYEE-A", FieldType.STRING, 
            2);

        pnd_Work_File_1__R_Field_8 = pnd_Work_File_1__R_Field_7.newGroupInGroup("pnd_Work_File_1__R_Field_8", "REDEFINE", pnd_Work_File_1_Pnd_W1_Payee_A);
        pnd_Work_File_1_Pnd_W1_Payee = pnd_Work_File_1__R_Field_8.newFieldInGroup("pnd_Work_File_1_Pnd_W1_Payee", "#W1-PAYEE", FieldType.NUMERIC, 2);
        pnd_Work_File_1_Pnd_W1_Before_Pay = pnd_Work_File_1__R_Field_7.newFieldInGroup("pnd_Work_File_1_Pnd_W1_Before_Pay", "#W1-BEFORE-PAY", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_File_1_Pnd_W1_After_Pay = pnd_Work_File_1__R_Field_7.newFieldInGroup("pnd_Work_File_1_Pnd_W1_After_Pay", "#W1-AFTER-PAY", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_File_1_Pnd_W1_Filler4 = pnd_Work_File_1__R_Field_7.newFieldInGroup("pnd_Work_File_1_Pnd_W1_Filler4", "#W1-FILLER4", FieldType.STRING, 
            20);
        pnd_Tot_Before_Pay = localVariables.newFieldInRecord("pnd_Tot_Before_Pay", "#TOT-BEFORE-PAY", FieldType.NUMERIC, 11, 2);
        pnd_Tot_After_Pay = localVariables.newFieldInRecord("pnd_Tot_After_Pay", "#TOT-AFTER-PAY", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Diff = localVariables.newFieldInRecord("pnd_Tot_Diff", "#TOT-DIFF", FieldType.NUMERIC, 11, 2);
        pnd_Count_Reads = localVariables.newFieldInRecord("pnd_Count_Reads", "#COUNT-READS", FieldType.NUMERIC, 6);
        pnd_Count_Writes = localVariables.newFieldInRecord("pnd_Count_Writes", "#COUNT-WRITES", FieldType.NUMERIC, 6);
        pnd_Diff = localVariables.newFieldInRecord("pnd_Diff", "#DIFF", FieldType.NUMERIC, 9, 2);
        pnd_T1_Found = localVariables.newFieldInRecord("pnd_T1_Found", "#T1-FOUND", FieldType.STRING, 1);
        pnd_Record_Updated_Already = localVariables.newFieldInRecord("pnd_Record_Updated_Already", "#RECORD-UPDATED-ALREADY", FieldType.STRING, 1);
        pnd_Before_Update_Error = localVariables.newFieldInRecord("pnd_Before_Update_Error", "#BEFORE-UPDATE-ERROR", FieldType.STRING, 1);
        pnd_Last_Chk_Dt = localVariables.newFieldInRecord("pnd_Last_Chk_Dt", "#LAST-CHK-DT", FieldType.STRING, 8);

        pnd_Last_Chk_Dt__R_Field_9 = localVariables.newGroupInRecord("pnd_Last_Chk_Dt__R_Field_9", "REDEFINE", pnd_Last_Chk_Dt);
        pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N = pnd_Last_Chk_Dt__R_Field_9.newFieldInGroup("pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N", "#LAST-CHK-DT-N", FieldType.NUMERIC, 
            8);
        pnd_Last_Dt = localVariables.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 10);

        pnd_Last_Dt__R_Field_10 = localVariables.newGroupInRecord("pnd_Last_Dt__R_Field_10", "REDEFINE", pnd_Last_Dt);
        pnd_Last_Dt_Pnd_Last_Dt_Mm = pnd_Last_Dt__R_Field_10.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Mm", "#LAST-DT-MM", FieldType.STRING, 2);
        pnd_Last_Dt_Pnd_Filler_1 = pnd_Last_Dt__R_Field_10.newFieldInGroup("pnd_Last_Dt_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Last_Dt_Pnd_Last_Dt_Dd = pnd_Last_Dt__R_Field_10.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Dd", "#LAST-DT-DD", FieldType.STRING, 2);
        pnd_Last_Dt_Pnd_Filler_2 = pnd_Last_Dt__R_Field_10.newFieldInGroup("pnd_Last_Dt_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Last_Dt_Pnd_Last_Dt_Yyyy = pnd_Last_Dt__R_Field_10.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Yyyy", "#LAST-DT-YYYY", FieldType.STRING, 4);
        pnd_Next_Dt = localVariables.newFieldInRecord("pnd_Next_Dt", "#NEXT-DT", FieldType.STRING, 10);
        pnd_W_Cnt_Mode = localVariables.newFieldInRecord("pnd_W_Cnt_Mode", "#W-CNT-MODE", FieldType.NUMERIC, 3);
        pnd_Page_Ctr = localVariables.newFieldInRecord("pnd_Page_Ctr", "#PAGE-CTR", FieldType.NUMERIC, 3);
        pnd_Page_Ctr_2 = localVariables.newFieldInRecord("pnd_Page_Ctr_2", "#PAGE-CTR-2", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal200a.initializeValues();
        ldaIaal200b.initializeValues();
        ldaIaal201e.initializeValues();
        ldaIaal202a.initializeValues();
        ldaIaal202b.initializeValues();
        ldaIaal202e.initializeValues();
        ldaIaal202g.initializeValues();
        ldaIaal202h.initializeValues();
        ldaIaal205a.initializeValues();

        localVariables.reset();
        pnd_Cntrct_Py_Dte_Key.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap385() throws Exception
    {
        super("Iaap385");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP385", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *********
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
                                                                                                                                                                          //Natural: ON ERROR;//Natural: PERFORM #GET-CONTROL-INFO
        sub_Pnd_Get_Control_Info();
        if (condition(Global.isEscape())) {return;}
        RW:                                                                                                                                                               //Natural: READ WORK FILE 1 #WORK-FILE-1
        while (condition(getWorkFiles().read(1, pnd_Work_File_1)))
        {
            pnd_Count_Reads.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #COUNT-READS
            pnd_Time.setValue(Global.getTIMX());                                                                                                                          //Natural: MOVE *TIMX TO #TIME
                                                                                                                                                                          //Natural: PERFORM #READ-FUND
            sub_Pnd_Read_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHECKING FOR ERROR CONDITIONS
            //*                                                                                                                                                           //Natural: DECIDE FOR FIRST CONDITION
            short decideConditionsMet668 = 0;                                                                                                                             //Natural: WHEN #T1-FOUND NOT = 'Y'
            if (condition(pnd_T1_Found.notEquals("Y")))
            {
                decideConditionsMet668++;
                getReports().write(2, ReportOption.NOTITLE,pnd_Work_File_1_Pnd_W1_Cntrct,pnd_Work_File_1_Pnd_W1_Payee,new ColumnSpacing(9),"RECORD MISSING");             //Natural: WRITE ( 2 ) #W1-CNTRCT #W1-PAYEE 9X 'RECORD MISSING'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #RECORD-UPDATED-ALREADY = 'Y'
            else if (condition(pnd_Record_Updated_Already.equals("Y")))
            {
                decideConditionsMet668++;
                getReports().write(2, ReportOption.NOTITLE,pnd_Work_File_1_Pnd_W1_Cntrct,pnd_Work_File_1_Pnd_W1_Payee,new ColumnSpacing(9),"ALREADY PROCESSED");          //Natural: WRITE ( 2 ) #W1-CNTRCT #W1-PAYEE 9X 'ALREADY PROCESSED'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #BEFORE-UPDATE-ERROR = 'Y'
            else if (condition(pnd_Before_Update_Error.equals("Y")))
            {
                decideConditionsMet668++;
                getReports().write(2, ReportOption.NOTITLE,pnd_Work_File_1_Pnd_W1_Cntrct,pnd_Work_File_1_Pnd_W1_Payee,new ColumnSpacing(9),"OLD PAYMENT DOES NOT MATCH CURRENT"); //Natural: WRITE ( 2 ) #W1-CNTRCT #W1-PAYEE 9X 'OLD PAYMENT DOES NOT MATCH CURRENT'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-THE-RECORD
                sub_Pnd_Process_The_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (Global.isEscape()) return;
        pnd_Tot_Diff.compute(new ComputeParameters(false, pnd_Tot_Diff), pnd_Tot_After_Pay.subtract(pnd_Tot_Before_Pay));                                                 //Natural: COMPUTE #TOT-DIFF = #TOT-AFTER-PAY - #TOT-BEFORE-PAY
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"<TOTALS>",new TabSetting(24),pnd_Tot_Before_Pay,new TabSetting(43),pnd_Tot_After_Pay,new            //Natural: WRITE ( 1 ) 001T '<TOTALS>' 024T #TOT-BEFORE-PAY 043T #TOT-AFTER-PAY 060T #TOT-DIFF
            TabSetting(60),pnd_Tot_Diff);
        if (Global.isEscape()) return;
        getReports().write(0, "INPUT RECORDS READ ===>",pnd_Count_Reads);                                                                                                 //Natural: WRITE 'INPUT RECORDS READ ===>' #COUNT-READS
        if (Global.isEscape()) return;
        getReports().write(0, "DATABASE WRITES ======>",pnd_Count_Writes);                                                                                                //Natural: WRITE 'DATABASE WRITES ======>' #COUNT-WRITES
        if (Global.isEscape()) return;
        getReports().write(0, "-----------  END OF PROGRAM - IAAP385  -----------");                                                                                      //Natural: WRITE '-----------  END OF PROGRAM - IAAP385  -----------'
        if (Global.isEscape()) return;
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-CONTRACT
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-CPR
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-TIAA-FUND
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CREATE-TRANSACTION
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RECORD-CHECKS
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-FUND
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-THE-RECORD
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-CONTROL-INFO
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-MASTER
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CALC-AND-WRITE-REPORT
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CONTRACT
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CPR
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-TIAA-FUND
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CREATE-HISTORICAL-REC
        //* **********************************************************************
    }
    private void sub_Pnd_Bi_Contract() throws Exception                                                                                                                   //Natural: #BI-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W1-CNTRCT
        (
        "FNR",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Work_File_1_Pnd_W1_Cntrct, WcType.WITH) },
        1
        );
        FNR:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FNR")))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal202a.getVw_iaa_Cntrct_Trans().reset();                                                                                                                 //Natural: RESET IAA-CNTRCT-TRANS
            ldaIaal202a.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal200a.getVw_iaa_Cntrct());                                                                         //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
            ldaIaal202a.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Time);                                                                                               //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE = #TIME
            pnd_Date_Time_P.setValue(ldaIaal202a.getIaa_Cntrct_Trans_Trans_Dte());                                                                                        //Natural: ASSIGN #DATE-TIME-P = IAA-CNTRCT-TRANS.TRANS-DTE
            ldaIaal202a.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202a.getIaa_Cntrct_Trans_Invrse_Trans_Dte()),                  //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
            ldaIaal202a.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE = #TIME
            ldaIaal202a.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue("1");                                                                                                 //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID = '1'
            ldaIaal202a.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
            ldaIaal202a.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CNTRCT-TRANS
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Bi_Cpr() throws Exception                                                                                                                        //Natural: #BI-CPR
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                 //Natural: ASSIGN #CNTRCT-PPCN-NBR = #W1-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                     //Natural: ASSIGN #CNTRCT-PAYEE = #W1-PAYEE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseRead                                                                                                     //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPR",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPR:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("CPR")))
        {
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal202b.getVw_iaa_Cpr_Trans().reset();                                                                                                                //Natural: RESET IAA-CPR-TRANS
                ldaIaal202b.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());                                                           //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
                ldaIaal202b.getIaa_Cpr_Trans_Trans_Dte().setValue(pnd_Time);                                                                                              //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal202b.getIaa_Cpr_Trans_Trans_Dte());                                                                                       //Natural: ASSIGN #DATE-TIME-P = IAA-CPR-TRANS.TRANS-DTE
                ldaIaal202b.getIaa_Cpr_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202b.getIaa_Cpr_Trans_Invrse_Trans_Dte()),                    //Natural: COMPUTE IAA-CPR-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal202b.getIaa_Cpr_Trans_Bfre_Imge_Id().setValue("1");                                                                                                //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID = '1'
                ldaIaal202b.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                                 //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal202b.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                          //Natural: STORE IAA-CPR-TRANS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Bi_Tiaa_Fund() throws Exception                                                                                                                  //Natural: #BI-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #W1-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                    //Natural: ASSIGN #W-CNTRCT-PAYEE = #W1-PAYEE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE = ' '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1A",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1A:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1A")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Work_File_1_Pnd_W1_Cntrct) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Work_File_1_Pnd_W1_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W1-CNTRCT AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W1-PAYEE
            {
                ldaIaal202e.getVw_iaa_Tiaa_Fund_Trans().reset();                                                                                                          //Natural: RESET IAA-TIAA-FUND-TRANS
                ldaIaal202e.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());                                                          //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                        //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal202e.getIaa_Tiaa_Fund_Trans_Trans_Dte());                                                                                 //Natural: ASSIGN #DATE-TIME-P = IAA-TIAA-FUND-TRANS.TRANS-DTE
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202e.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte()),        //Natural: COMPUTE IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                          //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID = '1'
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                           //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal202e.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                    //Natural: STORE IAA-TIAA-FUND-TRANS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1A;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1A. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Create_Transaction() throws Exception                                                                                                            //Natural: #CREATE-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaIaal202g.getVw_iaa_Trans_Rcrd().reset();                                                                                                                       //Natural: RESET IAA-TRANS-RCRD
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Time);                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TIME
        ldaIaal202g.getIaa_Trans_Rcrd_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202g.getIaa_Trans_Rcrd_Invrse_Trans_Dte()), new DbsDecimal("1000000000000").subtract(ldaIaal202g.getIaa_Trans_Rcrd_Trans_Dte())); //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := 1000000000000 - IAA-TRANS-RCRD.TRANS-DTE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #W1-CNTRCT
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #W1-PAYEE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Actvty_Cde().setValue("A");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                                        //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #CHECK-DATE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Todays_Dte().setValue(pnd_Todays_Date_A_Pnd_Todays_Date);                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #TODAYS-DATE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_User_Id().setValue("IAAP385");                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP385'
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue(" ");                                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_User_Area().setValue("GROUP");                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'GROUP'
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Verify_Cde().setValue(" ");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-CDE := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Verify_Id().setValue(" ");                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-ID := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cwf_Wpid().setValue(" ");                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-WPID := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := #TIME
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cwf_Id_Nbr().setValue(0);                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-ID-NBR := 0
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cde().setValue(50);                                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 050
        ldaIaal202g.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                 //Natural: STORE IAA-TRANS-RCRD
    }
    private void sub_Pnd_Record_Checks() throws Exception                                                                                                                 //Natural: #RECORD-CHECKS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_T1_Found.setValue("Y");                                                                                                                                       //Natural: MOVE 'Y' TO #T1-FOUND
        if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1).equals(pnd_Work_File_1_Pnd_W1_After_Pay)))                                         //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1 ) = #W1-AFTER-PAY
        {
            pnd_Record_Updated_Already.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #RECORD-UPDATED-ALREADY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1).notEquals(pnd_Work_File_1_Pnd_W1_Before_Pay)))                                 //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1 ) NOT = #W1-BEFORE-PAY
            {
                pnd_Before_Update_Error.setValue("Y");                                                                                                                    //Natural: MOVE 'Y' TO #BEFORE-UPDATE-ERROR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Read_Fund() throws Exception                                                                                                                     //Natural: #READ-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Record_Updated_Already.reset();                                                                                                                               //Natural: RESET #RECORD-UPDATED-ALREADY #T1-FOUND #BEFORE-UPDATE-ERROR
        pnd_T1_Found.reset();
        pnd_Before_Update_Error.reset();
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #W1-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                    //Natural: ASSIGN #W-CNTRCT-PAYEE := #W1-PAYEE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "RD",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        RD:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("RD")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Work_File_1_Pnd_W1_Cntrct) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Work_File_1_Pnd_W1_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W1-CNTRCT AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W1-PAYEE
            {
                if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1 ")))                                                                     //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'T1 '
                {
                                                                                                                                                                          //Natural: PERFORM #RECORD-CHECKS
                    sub_Pnd_Record_Checks();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "ERROR - FUND OTHER THAN T1 FOR CONTRACT",pnd_Work_File_1_Pnd_W1_Cntrct,pnd_Work_File_1_Pnd_W1_Payee);                          //Natural: WRITE 'ERROR - FUND OTHER THAN T1 FOR CONTRACT' #W1-CNTRCT #W1-PAYEE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RD;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RD. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Process_The_Record() throws Exception                                                                                                            //Natural: #PROCESS-THE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
                                                                                                                                                                          //Natural: PERFORM #BI-CONTRACT
        sub_Pnd_Bi_Contract();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #BI-CPR
        sub_Pnd_Bi_Cpr();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #BI-TIAA-FUND
        sub_Pnd_Bi_Tiaa_Fund();
        if (condition(Global.isEscape())) {return;}
        pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr_1.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                              //Natural: ASSIGN #CNTRCT-PPCN-NBR-1 = #W1-CNTRCT
        pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde_1.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                              //Natural: ASSIGN #CNTRCT-PAYEE-CDE-1 = #W1-PAYEE
        pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde_1.setValue("T1 ");                                                                                                       //Natural: ASSIGN #CMPNY-FUND-CDE-1 = 'T1 '
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte_1.compute(new ComputeParameters(false, pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte_1), DbsField.subtract(100000000, //Natural: COMPUTE #FUND-INVRSE-LST-PD-DTE-1 = 100000000 - #LAST-CHK-DT-N
            pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N));
        ldaIaal205a.getVw_old_Tiaa_Rates().startDatabaseFind                                                                                                              //Natural: FIND OLD-TIAA-RATES WITH CNTRCT-PY-DTE-KEY = #CNTRCT-PY-DTE-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", "=", pnd_Cntrct_Py_Dte_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaIaal205a.getVw_old_Tiaa_Rates().readNextRow("FIND01", true)))
        {
            ldaIaal205a.getVw_old_Tiaa_Rates().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal205a.getVw_old_Tiaa_Rates().getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORDS FOUND
            {
                                                                                                                                                                          //Natural: PERFORM #CREATE-HISTORICAL-REC
                sub_Pnd_Create_Historical_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #PROCESS-MASTER
        sub_Pnd_Process_Master();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #CREATE-TRANSACTION
        sub_Pnd_Create_Transaction();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #AI-CONTRACT
        sub_Pnd_Ai_Contract();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #AI-CPR
        sub_Pnd_Ai_Cpr();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #AI-TIAA-FUND
        sub_Pnd_Ai_Tiaa_Fund();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *BACKOUT TRANSACTION /* TEST ONLY
                                                                                                                                                                          //Natural: PERFORM #CALC-AND-WRITE-REPORT
        sub_Pnd_Calc_And_Write_Report();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Pnd_Get_Control_Info() throws Exception                                                                                                              //Natural: #GET-CONTROL-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "RD1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        RD1:
        while (condition(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().readNextRow("RD1")))
        {
            pnd_Check_Date_A.setValueEdited(ldaIaal202h.getIaa_Cntrl_Rcrd_1_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte.reset();                                                                                                                  //Natural: RESET #CNTRL-INVRSE-DTE
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("DC");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'DC'
        ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "RD2",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        RD2:
        while (condition(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().readNextRow("RD2")))
        {
            pnd_Todays_Date_A.setValueEdited(ldaIaal202h.getIaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DATE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Last_Dt.reset();                                                                                                                                              //Natural: RESET #LAST-DT #NEXT-DT
        pnd_Next_Dt.reset();
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Dt, pnd_Next_Dt);                                                                             //Natural: CALLNAT 'IAAN0020' #LAST-DT #NEXT-DT
        if (condition(Global.isEscape())) return;
        pnd_Last_Chk_Dt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Last_Dt_Pnd_Last_Dt_Yyyy, pnd_Last_Dt_Pnd_Last_Dt_Mm, pnd_Last_Dt_Pnd_Last_Dt_Dd));  //Natural: COMPRESS #LAST-DT-YYYY #LAST-DT-MM #LAST-DT-DD INTO #LAST-CHK-DT LEAVING NO
        //*  WRITE 'CHECK DATE =====>' #CHECK-DATE
        //*  WRITE 'BUSINESS DATE ==>' #TODAYS-DATE
        //*  WRITE 'LAST CHECK DATE >' #LAST-CHK-DT
    }
    private void sub_Pnd_Process_Master() throws Exception                                                                                                                //Natural: #PROCESS-MASTER
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #W1-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                    //Natural: ASSIGN #W-CNTRCT-PAYEE = #W1-PAYEE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue("T1 ");                                                                                                              //Natural: ASSIGN #W-FUND-CODE = 'T1 '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ ( 1 ) IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R3A",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R3A:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R3A")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Work_File_1_Pnd_W1_Cntrct) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Work_File_1_Pnd_W1_Payee)  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W1-CNTRCT AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W1-PAYEE AND IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'T1 '
                && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1 ")))
            {
                ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                      //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                      //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().setValue(pnd_Work_File_1_Pnd_W1_After_Pay);                                                          //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := #W1-AFTER-PAY
                ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().setValue(0);                                                                                         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := 0
                ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1).setValue(pnd_Work_File_1_Pnd_W1_After_Pay);                                              //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1 ) := #W1-AFTER-PAY
                ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1).setValue(0);                                                                             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( 1 ) := 0
                ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TIME
                ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().updateDBRow("R3A");                                                                                                //Natural: UPDATE ( R3A. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R3A;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R3A. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W1-CNTRCT
        (
        "FW",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Work_File_1_Pnd_W1_Cntrct, WcType.WITH) },
        1
        );
        FW:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FW")))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal200a.getIaa_Cntrct_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                 //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE = #TIME
            ldaIaal200a.getVw_iaa_Cntrct().updateDBRow("FW");                                                                                                             //Natural: UPDATE ( FW. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                 //Natural: ASSIGN #CNTRCT-PPCN-NBR = #W1-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                     //Natural: ASSIGN #CNTRCT-PAYEE = #W1-PAYEE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseRead                                                                                                     //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CZP",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CZP:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("CZP")))
        {
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                pnd_W_Cnt_Mode.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                                                        //Natural: ASSIGN #W-CNT-MODE = IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
                ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte().setValue(pnd_Time);                                                                                //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.LST-TRANS-DTE = #TIME
                ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().updateDBRow("CZP");                                                                                           //Natural: UPDATE ( CZP. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Calc_And_Write_Report() throws Exception                                                                                                         //Natural: #CALC-AND-WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Diff.compute(new ComputeParameters(false, pnd_Diff), pnd_Work_File_1_Pnd_W1_After_Pay.subtract(pnd_Work_File_1_Pnd_W1_Before_Pay));                           //Natural: COMPUTE #DIFF = #W1-AFTER-PAY - #W1-BEFORE-PAY
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Work_File_1_Pnd_W1_Cntrct,new TabSetting(12),pnd_Work_File_1_Pnd_W1_Payee,new                    //Natural: WRITE ( 1 ) 001T #W1-CNTRCT 012T #W1-PAYEE 026T #W1-BEFORE-PAY 045T #W1-AFTER-PAY 062T #DIFF
            TabSetting(26),pnd_Work_File_1_Pnd_W1_Before_Pay,new TabSetting(45),pnd_Work_File_1_Pnd_W1_After_Pay,new TabSetting(62),pnd_Diff);
        if (Global.isEscape()) return;
        pnd_Count_Writes.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #COUNT-WRITES
        pnd_Tot_After_Pay.nadd(pnd_Work_File_1_Pnd_W1_After_Pay);                                                                                                         //Natural: COMPUTE #TOT-AFTER-PAY = #TOT-AFTER-PAY + #W1-AFTER-PAY
        pnd_Tot_Before_Pay.nadd(pnd_Work_File_1_Pnd_W1_Before_Pay);                                                                                                       //Natural: COMPUTE #TOT-BEFORE-PAY = #TOT-BEFORE-PAY + #W1-BEFORE-PAY
    }
    private void sub_Pnd_Ai_Contract() throws Exception                                                                                                                   //Natural: #AI-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W1-CNTRCT
        (
        "FQ",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Work_File_1_Pnd_W1_Cntrct, WcType.WITH) },
        1
        );
        FQ:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FQ")))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal202a.getVw_iaa_Cntrct_Trans().reset();                                                                                                                 //Natural: RESET IAA-CNTRCT-TRANS
            ldaIaal202a.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal200a.getVw_iaa_Cntrct());                                                                         //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
            ldaIaal202a.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Time);                                                                                               //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE = #TIME
            pnd_Date_Time_P.setValue(ldaIaal202a.getIaa_Cntrct_Trans_Trans_Dte());                                                                                        //Natural: ASSIGN #DATE-TIME-P = IAA-CNTRCT-TRANS.TRANS-DTE
            ldaIaal202a.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202a.getIaa_Cntrct_Trans_Invrse_Trans_Dte()),                  //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
            ldaIaal202a.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE = #TIME
            ldaIaal202a.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID = '2'
            ldaIaal202a.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
            ldaIaal202a.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CNTRCT-TRANS
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Ai_Cpr() throws Exception                                                                                                                        //Natural: #AI-CPR
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                 //Natural: ASSIGN #CNTRCT-PPCN-NBR = #W1-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                     //Natural: ASSIGN #CNTRCT-PAYEE = #W1-PAYEE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseRead                                                                                                     //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CP",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CP:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("CP")))
        {
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal202b.getVw_iaa_Cpr_Trans().reset();                                                                                                                //Natural: RESET IAA-CPR-TRANS
                ldaIaal202b.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());                                                           //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
                ldaIaal202b.getIaa_Cpr_Trans_Trans_Dte().setValue(pnd_Time);                                                                                              //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal202b.getIaa_Cpr_Trans_Trans_Dte());                                                                                       //Natural: ASSIGN #DATE-TIME-P = IAA-CPR-TRANS.TRANS-DTE
                ldaIaal202b.getIaa_Cpr_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202b.getIaa_Cpr_Trans_Invrse_Trans_Dte()),                    //Natural: COMPUTE IAA-CPR-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal202b.getIaa_Cpr_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                          //Natural: ASSIGN IAA-CPR-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal202b.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal202b.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                                 //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal202b.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                          //Natural: STORE IAA-CPR-TRANS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Ai_Tiaa_Fund() throws Exception                                                                                                                  //Natural: #AI-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #W1-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                    //Natural: ASSIGN #W-CNTRCT-PAYEE = #W1-PAYEE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue("T1 ");                                                                                                              //Natural: ASSIGN #W-FUND-CODE = 'T1 '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ ( 1 ) IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1G",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1G:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1G")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'T1 '
                && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1 ")))
            {
                ldaIaal202e.getVw_iaa_Tiaa_Fund_Trans().reset();                                                                                                          //Natural: RESET IAA-TIAA-FUND-TRANS
                ldaIaal202e.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());                                                          //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                        //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal202e.getIaa_Tiaa_Fund_Trans_Trans_Dte());                                                                                 //Natural: ASSIGN #DATE-TIME-P = IAA-TIAA-FUND-TRANS.TRANS-DTE
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202e.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte()),        //Natural: COMPUTE IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                          //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                    //Natural: ASSIGN IAA-TIAA-FUND-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal202e.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                           //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal202e.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                    //Natural: STORE IAA-TIAA-FUND-TRANS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1G;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1G. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Create_Historical_Rec() throws Exception                                                                                                         //Natural: #CREATE-HISTORICAL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_1_Pnd_W1_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #W1-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Work_File_1_Pnd_W1_Payee);                                                                                    //Natural: ASSIGN #W-CNTRCT-PAYEE = #W1-PAYEE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue("T1 ");                                                                                                              //Natural: ASSIGN #W-FUND-CODE = 'T1 '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "RA",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        RA:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("RA")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'T1'
                && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1")))
            {
                ldaIaal205a.getVw_old_Tiaa_Rates().reset();                                                                                                               //Natural: RESET OLD-TIAA-RATES
                ldaIaal205a.getVw_old_Tiaa_Rates().setValuesByName(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());                                                               //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO OLD-TIAA-RATES
                ldaIaal205a.getOld_Tiaa_Rates_Fund_Lst_Pd_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Chk_Dt);                                           //Natural: MOVE EDITED #LAST-CHK-DT TO OLD-TIAA-RATES.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().compute(new ComputeParameters(false, ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte()),      //Natural: COMPUTE OLD-TIAA-RATES.FUND-INVRSE-LST-PD-DTE = 100000000 - #LAST-CHK-DT-N
                    DbsField.subtract(100000000,pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N));
                ldaIaal205a.getOld_Tiaa_Rates_Trans_Dte().setValue(pnd_Time);                                                                                             //Natural: ASSIGN OLD-TIAA-RATES.TRANS-DTE = #TIME
                ldaIaal205a.getOld_Tiaa_Rates_Trans_User_Area().setValue(" ");                                                                                            //Natural: ASSIGN OLD-TIAA-RATES.TRANS-USER-AREA = ' '
                ldaIaal205a.getOld_Tiaa_Rates_Trans_User_Id().setValue(" ");                                                                                              //Natural: ASSIGN OLD-TIAA-RATES.TRANS-USER-ID = ' '
                ldaIaal205a.getOld_Tiaa_Rates_Trans_Verify_Id().setValue(" ");                                                                                            //Natural: ASSIGN OLD-TIAA-RATES.TRANS-VERIFY-ID = ' '
                ldaIaal205a.getOld_Tiaa_Rates_Trans_Verify_Dte().setValue(0);                                                                                             //Natural: ASSIGN OLD-TIAA-RATES.TRANS-VERIFY-DTE = 0
                ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr());                                       //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-PPCN-NBR = IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR
                ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Payee_Cde().setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde());                                     //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE
                ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Mode_Ind().setValue(pnd_W_Cnt_Mode);                                                                                 //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-MODE-IND = #W-CNT-MODE
                ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde().setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                         //Natural: ASSIGN OLD-TIAA-RATES.CMPNY-FUND-CDE = IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE
                ldaIaal205a.getOld_Tiaa_Rates_Rcrd_Srce().setValue("RE");                                                                                                 //Natural: ASSIGN OLD-TIAA-RATES.RCRD-SRCE = 'RE'
                ldaIaal205a.getOld_Tiaa_Rates_Rcrd_Status().setValue("A");                                                                                                //Natural: ASSIGN OLD-TIAA-RATES.RCRD-STATUS = 'A'
                ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt().setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                        //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-TOT-PER-AMT = IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt().setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                        //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-TOT-DIV-AMT = IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                ldaIaal205a.getOld_Tiaa_Rates_Lst_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN OLD-TIAA-RATES.LST-TRANS-DTE = #TIME
                ldaIaal205a.getVw_old_Tiaa_Rates().insertDBRow();                                                                                                         //Natural: STORE OLD-TIAA-RATES
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RA;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RA. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    pnd_Page_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ","IAAP385",new TabSetting(54),"GROUP ANNUITY PAYMENT UPDATE",new TabSetting(119),                //Natural: WRITE ( 1 ) 'PROGRAM ' 'IAAP385' 54T 'GROUP ANNUITY PAYMENT UPDATE' 119T 'PAGE ' #PAGE-CTR
                        "PAGE ",pnd_Page_Ctr);
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 1 ) 2
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT",new TabSetting(12),"PAYEE",new TabSetting(24),"BEFORE AMOUNT",new             //Natural: WRITE ( 1 ) 001T 'CONTRACT' 012T 'PAYEE' 024T 'BEFORE AMOUNT' 044T 'AFTER AMOUNT' 061T '  DIFFERENCE'
                        TabSetting(44),"AFTER AMOUNT",new TabSetting(61),"  DIFFERENCE");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(12),"-----",new TabSetting(24),"-------------",new             //Natural: WRITE ( 1 ) 001T '--------' 012T '-----' 024T '-------------' 044T '------------' 061T '  ----------'
                        TabSetting(44),"------------",new TabSetting(61),"  ----------");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    pnd_Page_Ctr_2.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PAGE-CTR-2
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ","IAAP385",new TabSetting(54),"GROUP ANNUITY PAYMENT UPDATE ERROR REPORT",new                    //Natural: WRITE ( 2 ) 'PROGRAM ' 'IAAP385' 54T 'GROUP ANNUITY PAYMENT UPDATE ERROR REPORT' 119T 'PAGE ' #PAGE-CTR-2
                        TabSetting(119),"PAGE ",pnd_Page_Ctr_2);
                    getReports().skip(2, 2);                                                                                                                              //Natural: SKIP ( 2 ) 2
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT",new TabSetting(12),"PAYEE",new TabSetting(24),"        ERROR DESCRIPTION        "); //Natural: WRITE ( 2 ) 001T 'CONTRACT' 012T 'PAYEE' 024T '        ERROR DESCRIPTION        '
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(12),"-----",new TabSetting(24),"---------------------------------"); //Natural: WRITE ( 2 ) 001T '--------' 012T '-----' 024T '---------------------------------'
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
