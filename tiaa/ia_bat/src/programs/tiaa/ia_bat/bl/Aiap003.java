/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:01 PM
**        * FROM NATURAL PROGRAM : Aiap003
************************************************************
**        * FILE NAME            : Aiap003.java
**        * CLASS NAME           : Aiap003
**        * INSTANCE NAME        : Aiap003
************************************************************
************************************************************************
* AIAP003 - TIAA IA GRADED TO STANDARD TRANSFER SUMMARY
*  VERSION 1.1 - 4/7/97
* UPDATED FOR RATE EXPANSION - 2/16/05 L.W.
* STARTING FROM 02/02/05, THE TRANSFER RECORD IN IAA-XFR-ACTRL-RCRD-VIEW
* IS WRITTEN ON NEW FORMAT WITH RATE EXPANSION.
*
* 07/10/08  KCD-1  ROTH/99 RATES IMPLEMENTATION
* 11/12/08  KCD-2 EXPAND FILE IAA-XFR-ACTRL-RCRD
* 10-30-12   DY  RBE PHASE2; REFERENCE AS RBE2
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aiap003 extends BLNatBase
{
    // Data Areas
    private LdaAial0131 ldaAial0131;
    private LdaAial0140 ldaAial0140;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Next_Bus_Dte;
    private DbsField pnd_Temp_Rate_Code_G;
    private DbsField pnd_Temp_Gtd_Pmt_G;
    private DbsField pnd_Temp_Dvd_Pmt_G;
    private DbsField pnd_Temp_Gtd_Pmt_L;
    private DbsField pnd_Temp_Dvd_Pmt_L;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Input_Contract_Payee_Begin;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Input_Contract_Payee_End;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_2;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Date;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Date_A;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Year;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Month;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Day;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_3;
    private DbsField pnd_Input_Record_Pnd_Input_End_Date;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_4;
    private DbsField pnd_Rate_Code;
    private DbsField pnd_Contract_Payee;

    private DbsGroup pnd_Contract_Payee__R_Field_4;
    private DbsField pnd_Contract_Payee_Pnd_Contract_Number_1_7;
    private DbsField pnd_Contract_Payee_Pnd_Contract_Number_8;
    private DbsField pnd_Contract_Payee_Pnd_Payee_Code;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Print_Ppcn;

    private DbsGroup pnd_Print_Ppcn__R_Field_5;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Ppcn_1_7;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Dash;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Ppcn_8;
    private DbsField pnd_Prt_Switch;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Temp_Contract_Payee;
    private DbsField pnd_Temp_Sequence_Nbr;
    private DbsField pnd_First_Time;
    private DbsField pnd_Skip_Routine;
    private DbsField pnd_Max_Rates;
    private DbsField pnd_Curr_Date;
    private DbsField pnd_Curr_Time;
    private DbsField pnd_Num_Lines;
    private DbsField pnd_Max_Lines;
    private DbsField pnd_Gtd_Total_Pmt_G;
    private DbsField pnd_Dvd_Total_Pmt_G;
    private DbsField pnd_Gtd_Total_Pmt_L;
    private DbsField pnd_Dvd_Total_Pmt_L;
    private DbsField pnd_Gtd_Grand_Pmt_G;
    private DbsField pnd_Dvd_Grand_Pmt_G;
    private DbsField pnd_Gtd_Grand_Pmt_L;
    private DbsField pnd_Dvd_Grand_Pmt_L;
    private DbsField pnd_Temp_Final_Per_Pay_Date;

    private DbsGroup pnd_Temp_Final_Per_Pay_Date__R_Field_6;
    private DbsField pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Year;
    private DbsField pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Month;
    private DbsField pnd_Final_Per_Pay_Date;
    private DbsField pnd_Tiaa_Check;

    private DbsGroup pnd_Tiaa_Check__R_Field_7;
    private DbsField pnd_Tiaa_Check_Pnd_Tiaa_Check_Ind;
    private DbsField pnd_Tiaa_Check_Pnd_Tiaa_Check_Rest;
    private DbsField pnd_Cntrl_Sd_Cde;
    private DbsField pnd_Date1;
    private DbsField pnd_Date2;
    private DbsField pnd_Transfer_Date;

    private DbsGroup pnd_Transfer_Date__R_Field_8;
    private DbsField pnd_Transfer_Date_Pnd_Transfer_Year;
    private DbsField pnd_Transfer_Date_Pnd_Transfer_Month;
    private DbsField pnd_Transfer_Date_Pnd_Transfer_Day;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py;

    private DbsGroup pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_9;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number;

    private DbsGroup pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_10;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0131 = new LdaAial0131();
        registerRecord(ldaAial0131);
        registerRecord(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2());
        ldaAial0140 = new LdaAial0140();
        registerRecord(ldaAial0140);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Next_Bus_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_cntrl);

        pnd_Temp_Rate_Code_G = localVariables.newFieldInRecord("pnd_Temp_Rate_Code_G", "#TEMP-RATE-CODE-G", FieldType.STRING, 2);
        pnd_Temp_Gtd_Pmt_G = localVariables.newFieldInRecord("pnd_Temp_Gtd_Pmt_G", "#TEMP-GTD-PMT-G", FieldType.NUMERIC, 9, 2);
        pnd_Temp_Dvd_Pmt_G = localVariables.newFieldInRecord("pnd_Temp_Dvd_Pmt_G", "#TEMP-DVD-PMT-G", FieldType.NUMERIC, 9, 2);
        pnd_Temp_Gtd_Pmt_L = localVariables.newFieldInRecord("pnd_Temp_Gtd_Pmt_L", "#TEMP-GTD-PMT-L", FieldType.NUMERIC, 9, 2);
        pnd_Temp_Dvd_Pmt_L = localVariables.newFieldInRecord("pnd_Temp_Dvd_Pmt_L", "#TEMP-DVD-PMT-L", FieldType.NUMERIC, 9, 2);
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 80);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Filler_1 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Input_Contract_Payee_Begin = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Contract_Payee_Begin", 
            "#INPUT-CONTRACT-PAYEE-BEGIN", FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Input_Filler_1 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_1", "#INPUT-FILLER-1", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_Contract_Payee_End = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Contract_Payee_End", "#INPUT-CONTRACT-PAYEE-END", 
            FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Input_Filler_2 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_2", "#INPUT-FILLER-2", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_Begin_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Date", "#INPUT-BEGIN-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Input_Begin_Date);
        pnd_Input_Record_Pnd_Input_Begin_Date_A = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Date_A", "#INPUT-BEGIN-DATE-A", 
            FieldType.STRING, 8);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Input_Begin_Date);
        pnd_Input_Record_Pnd_Input_Begin_Year = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Year", "#INPUT-BEGIN-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Input_Record_Pnd_Input_Begin_Month = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Month", "#INPUT-BEGIN-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Input_Begin_Day = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Day", "#INPUT-BEGIN-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Input_Filler_3 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_3", "#INPUT-FILLER-3", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_End_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_End_Date", "#INPUT-END-DATE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Input_Filler_4 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_4", "#INPUT-FILLER-4", FieldType.STRING, 
            33);
        pnd_Rate_Code = localVariables.newFieldInRecord("pnd_Rate_Code", "#RATE-CODE", FieldType.STRING, 2);
        pnd_Contract_Payee = localVariables.newFieldInRecord("pnd_Contract_Payee", "#CONTRACT-PAYEE", FieldType.STRING, 10);

        pnd_Contract_Payee__R_Field_4 = localVariables.newGroupInRecord("pnd_Contract_Payee__R_Field_4", "REDEFINE", pnd_Contract_Payee);
        pnd_Contract_Payee_Pnd_Contract_Number_1_7 = pnd_Contract_Payee__R_Field_4.newFieldInGroup("pnd_Contract_Payee_Pnd_Contract_Number_1_7", "#CONTRACT-NUMBER-1-7", 
            FieldType.STRING, 7);
        pnd_Contract_Payee_Pnd_Contract_Number_8 = pnd_Contract_Payee__R_Field_4.newFieldInGroup("pnd_Contract_Payee_Pnd_Contract_Number_8", "#CONTRACT-NUMBER-8", 
            FieldType.STRING, 1);
        pnd_Contract_Payee_Pnd_Payee_Code = pnd_Contract_Payee__R_Field_4.newFieldInGroup("pnd_Contract_Payee_Pnd_Payee_Code", "#PAYEE-CODE", FieldType.STRING, 
            2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Print_Ppcn = localVariables.newFieldInRecord("pnd_Print_Ppcn", "#PRINT-PPCN", FieldType.STRING, 9);

        pnd_Print_Ppcn__R_Field_5 = localVariables.newGroupInRecord("pnd_Print_Ppcn__R_Field_5", "REDEFINE", pnd_Print_Ppcn);
        pnd_Print_Ppcn_Pnd_Print_Ppcn_1_7 = pnd_Print_Ppcn__R_Field_5.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Ppcn_1_7", "#PRINT-PPCN-1-7", FieldType.STRING, 
            7);
        pnd_Print_Ppcn_Pnd_Print_Dash = pnd_Print_Ppcn__R_Field_5.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Dash", "#PRINT-DASH", FieldType.STRING, 1);
        pnd_Print_Ppcn_Pnd_Print_Ppcn_8 = pnd_Print_Ppcn__R_Field_5.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Ppcn_8", "#PRINT-PPCN-8", FieldType.STRING, 
            1);
        pnd_Prt_Switch = localVariables.newFieldInRecord("pnd_Prt_Switch", "#PRT-SWITCH", FieldType.STRING, 1);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 3);
        pnd_Temp_Contract_Payee = localVariables.newFieldInRecord("pnd_Temp_Contract_Payee", "#TEMP-CONTRACT-PAYEE", FieldType.STRING, 10);
        pnd_Temp_Sequence_Nbr = localVariables.newFieldInRecord("pnd_Temp_Sequence_Nbr", "#TEMP-SEQUENCE-NBR", FieldType.NUMERIC, 2);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.STRING, 1);
        pnd_Skip_Routine = localVariables.newFieldInRecord("pnd_Skip_Routine", "#SKIP-ROUTINE", FieldType.STRING, 1);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.NUMERIC, 3);
        pnd_Curr_Date = localVariables.newFieldInRecord("pnd_Curr_Date", "#CURR-DATE", FieldType.STRING, 8);
        pnd_Curr_Time = localVariables.newFieldInRecord("pnd_Curr_Time", "#CURR-TIME", FieldType.STRING, 10);
        pnd_Num_Lines = localVariables.newFieldInRecord("pnd_Num_Lines", "#NUM-LINES", FieldType.NUMERIC, 2);
        pnd_Max_Lines = localVariables.newFieldInRecord("pnd_Max_Lines", "#MAX-LINES", FieldType.NUMERIC, 2);
        pnd_Gtd_Total_Pmt_G = localVariables.newFieldInRecord("pnd_Gtd_Total_Pmt_G", "#GTD-TOTAL-PMT-G", FieldType.NUMERIC, 9, 2);
        pnd_Dvd_Total_Pmt_G = localVariables.newFieldInRecord("pnd_Dvd_Total_Pmt_G", "#DVD-TOTAL-PMT-G", FieldType.NUMERIC, 9, 2);
        pnd_Gtd_Total_Pmt_L = localVariables.newFieldInRecord("pnd_Gtd_Total_Pmt_L", "#GTD-TOTAL-PMT-L", FieldType.NUMERIC, 9, 2);
        pnd_Dvd_Total_Pmt_L = localVariables.newFieldInRecord("pnd_Dvd_Total_Pmt_L", "#DVD-TOTAL-PMT-L", FieldType.NUMERIC, 9, 2);
        pnd_Gtd_Grand_Pmt_G = localVariables.newFieldInRecord("pnd_Gtd_Grand_Pmt_G", "#GTD-GRAND-PMT-G", FieldType.NUMERIC, 13, 2);
        pnd_Dvd_Grand_Pmt_G = localVariables.newFieldInRecord("pnd_Dvd_Grand_Pmt_G", "#DVD-GRAND-PMT-G", FieldType.NUMERIC, 13, 2);
        pnd_Gtd_Grand_Pmt_L = localVariables.newFieldInRecord("pnd_Gtd_Grand_Pmt_L", "#GTD-GRAND-PMT-L", FieldType.NUMERIC, 13, 2);
        pnd_Dvd_Grand_Pmt_L = localVariables.newFieldInRecord("pnd_Dvd_Grand_Pmt_L", "#DVD-GRAND-PMT-L", FieldType.NUMERIC, 13, 2);
        pnd_Temp_Final_Per_Pay_Date = localVariables.newFieldInRecord("pnd_Temp_Final_Per_Pay_Date", "#TEMP-FINAL-PER-PAY-DATE", FieldType.NUMERIC, 6);

        pnd_Temp_Final_Per_Pay_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Temp_Final_Per_Pay_Date__R_Field_6", "REDEFINE", pnd_Temp_Final_Per_Pay_Date);
        pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Year = pnd_Temp_Final_Per_Pay_Date__R_Field_6.newFieldInGroup("pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Year", 
            "#TEMP-FINAL-PER-PAY-YEAR", FieldType.NUMERIC, 4);
        pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Month = pnd_Temp_Final_Per_Pay_Date__R_Field_6.newFieldInGroup("pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Month", 
            "#TEMP-FINAL-PER-PAY-MONTH", FieldType.NUMERIC, 2);
        pnd_Final_Per_Pay_Date = localVariables.newFieldInRecord("pnd_Final_Per_Pay_Date", "#FINAL-PER-PAY-DATE", FieldType.STRING, 7);
        pnd_Tiaa_Check = localVariables.newFieldInRecord("pnd_Tiaa_Check", "#TIAA-CHECK", FieldType.STRING, 250);

        pnd_Tiaa_Check__R_Field_7 = localVariables.newGroupInRecord("pnd_Tiaa_Check__R_Field_7", "REDEFINE", pnd_Tiaa_Check);
        pnd_Tiaa_Check_Pnd_Tiaa_Check_Ind = pnd_Tiaa_Check__R_Field_7.newFieldInGroup("pnd_Tiaa_Check_Pnd_Tiaa_Check_Ind", "#TIAA-CHECK-IND", FieldType.STRING, 
            1);
        pnd_Tiaa_Check_Pnd_Tiaa_Check_Rest = pnd_Tiaa_Check__R_Field_7.newFieldInGroup("pnd_Tiaa_Check_Pnd_Tiaa_Check_Rest", "#TIAA-CHECK-REST", FieldType.STRING, 
            249);
        pnd_Cntrl_Sd_Cde = localVariables.newFieldInRecord("pnd_Cntrl_Sd_Cde", "#CNTRL-SD-CDE", FieldType.STRING, 2);
        pnd_Date1 = localVariables.newFieldInRecord("pnd_Date1", "#DATE1", FieldType.STRING, 10);
        pnd_Date2 = localVariables.newFieldInRecord("pnd_Date2", "#DATE2", FieldType.STRING, 10);
        pnd_Transfer_Date = localVariables.newFieldInRecord("pnd_Transfer_Date", "#TRANSFER-DATE", FieldType.NUMERIC, 8);

        pnd_Transfer_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Transfer_Date__R_Field_8", "REDEFINE", pnd_Transfer_Date);
        pnd_Transfer_Date_Pnd_Transfer_Year = pnd_Transfer_Date__R_Field_8.newFieldInGroup("pnd_Transfer_Date_Pnd_Transfer_Year", "#TRANSFER-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Transfer_Date_Pnd_Transfer_Month = pnd_Transfer_Date__R_Field_8.newFieldInGroup("pnd_Transfer_Date_Pnd_Transfer_Month", "#TRANSFER-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Transfer_Date_Pnd_Transfer_Day = pnd_Transfer_Date__R_Field_8.newFieldInGroup("pnd_Transfer_Date_Pnd_Transfer_Day", "#TRANSFER-DAY", FieldType.NUMERIC, 
            2);
        pnd_Iaxfr_Actrl_Cntrct_Py = localVariables.newFieldInRecord("pnd_Iaxfr_Actrl_Cntrct_Py", "#IAXFR-ACTRL-CNTRCT-PY", FieldType.STRING, 12);

        pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_9 = localVariables.newGroupInRecord("pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_9", "REDEFINE", pnd_Iaxfr_Actrl_Cntrct_Py);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_9.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10", 
            "#IAXFR-ACTRL-CNTRCT-PY-A10", FieldType.STRING, 10);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_9.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number", 
            "#IAXFR-ACTRL-RECORD-NUMBER", FieldType.NUMERIC, 2);

        pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_10 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_9.newGroupInGroup("pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_10", "REDEFINE", 
            pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_10.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2", 
            "#IAXFR-ACTRL-RECORD-NUMBER-A2", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        ldaAial0131.initializeValues();
        ldaAial0140.initializeValues();

        localVariables.reset();
        pnd_Page_Number.setInitialValue(0);
        pnd_Temp_Sequence_Nbr.setInitialValue(0);
        pnd_First_Time.setInitialValue("Y");
        pnd_Skip_Routine.setInitialValue("N");
        pnd_Max_Rates.setInitialValue(100);
        pnd_Num_Lines.setInitialValue(59);
        pnd_Max_Lines.setInitialValue(59);
        pnd_Gtd_Total_Pmt_G.setInitialValue(0);
        pnd_Dvd_Total_Pmt_G.setInitialValue(0);
        pnd_Gtd_Total_Pmt_L.setInitialValue(0);
        pnd_Dvd_Total_Pmt_L.setInitialValue(0);
        pnd_Gtd_Grand_Pmt_G.setInitialValue(0);
        pnd_Dvd_Grand_Pmt_G.setInitialValue(0);
        pnd_Gtd_Grand_Pmt_L.setInitialValue(0);
        pnd_Dvd_Grand_Pmt_L.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Aiap003() throws Exception
    {
        super("Aiap003");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *******************************************************************
        getReports().write(0, "***** AIAP003 ****");                                                                                                                      //Natural: WRITE '***** AIAP003 ****'
        if (Global.isEscape()) return;
        //*  RBE2                                                                                                                                                         //Natural: FORMAT LS = 132 PS = 0 ES = ON
        //*  RBE2                                                                                                                                                         //Natural: FORMAT ( 1 ) LS = 132 PS = 0 ES = ON
        pnd_Curr_Date.setValue(Global.getDATU());                                                                                                                         //Natural: MOVE *DATU TO #CURR-DATE
        //*  RBE2
        pnd_Curr_Time.setValue(Global.getTIME());                                                                                                                         //Natural: MOVE *TIME TO #CURR-TIME
        pnd_Print_Ppcn_Pnd_Print_Dash.setValue("-");                                                                                                                      //Natural: MOVE '-' TO #PRINT-DASH
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATE
        sub_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
        //* *READ WORK 1 #INPUT-RECORD
        //*  RBE2
        pnd_Temp_Contract_Payee.reset();                                                                                                                                  //Natural: RESET #TEMP-CONTRACT-PAYEE
        //*  MOVE '            'TO  #INPUT-CONTRACT-PAYEE-BEGIN /* RBE2
        //*  ---------------------------------------------
        //*  #INPUT-BEGIN-DATE := 20121018  /* REMOVE BFORE PROD
        //*  #INPUT-END-DATE := 20121018    /* REMOVE BFORE PROD
        //*  ---------------------------------------------
        getReports().write(0, "=",pnd_Input_Record_Pnd_Input_Begin_Date,"=",pnd_Input_Record_Pnd_Input_End_Date);                                                         //Natural: WRITE '=' #INPUT-BEGIN-DATE '=' #INPUT-END-DATE
        if (Global.isEscape()) return;
        //*  IF #INPUT-CONTRACT-PAYEE-BEGIN = '            '    /* RBE2
        //*  READ IAA-XFR-ACTRL-RCRD-VIEW                       /* RBE2
        //*  RBE2
        ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().startDatabaseRead                                                                                                    //Natural: READ IAA-XFR-ACTRL-RCRD-VIEW2 BY IAXFR-ACTRL-PRCSS-DTE STARTING FROM #INPUT-BEGIN-DATE THRU #INPUT-END-DATE
        (
        "READ01",
        new Wc[] { new Wc("IAXFR_ACTRL_PRCSS_DTE", ">=", pnd_Input_Record_Pnd_Input_Begin_Date, "And", WcType.BY) ,
        new Wc("IAXFR_ACTRL_PRCSS_DTE", "<=", pnd_Input_Record_Pnd_Input_End_Date, WcType.BY) },
        new Oc[] { new Oc("IAXFR_ACTRL_PRCSS_DTE", "ASC") }
        );
        READ01:
        while (condition(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().readNextRow("READ01")))
        {
            if (condition(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Prcss_Dte().greater(pnd_Input_Record_Pnd_Input_End_Date)))                                  //Natural: IF IAXFR-ACTRL-PRCSS-DTE > #INPUT-END-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MAIN-ROUTINE
                sub_Main_Routine();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  RBE2 FIX BEGINS >>
        //*  ELSE
        //*  READ IAA-XFR-ACTRL-RCRD-VIEW                      /* RBE2
        //* *  READ IAA-XFR-ACTRL-RCRD-VIEW2                   /* RBE2
        //*       BY IAXFR-ACTRL-CNTRCT-PY
        //*       STARTING FROM #INPUT-CONTRACT-PAYEE-BEGIN
        //*       THRU          #INPUT-CONTRACT-PAYEE-END
        //*     IF IAXFR-ACTRL-CNTRCT-PY > #INPUT-CONTRACT-PAYEE-END
        //*       ESCAPE BOTTOM
        //*     ELSE
        //*       PERFORM MAIN-ROUTINE
        //*     END-IF
        //*   END-READ
        //*  END-IF
        //*  RBE2 FIX ENDS   <<
        //* *END-WORK
        //*  RBE2
                                                                                                                                                                          //Natural: PERFORM SUBTOTAL-PRINTOUT
        sub_Subtotal_Printout();
        if (condition(Global.isEscape())) {return;}
        //*  RBE2
        pnd_Gtd_Grand_Pmt_G.nadd(pnd_Gtd_Total_Pmt_G);                                                                                                                    //Natural: ADD #GTD-TOTAL-PMT-G TO #GTD-GRAND-PMT-G
        //*  RBE2
        pnd_Dvd_Grand_Pmt_G.nadd(pnd_Dvd_Total_Pmt_G);                                                                                                                    //Natural: ADD #DVD-TOTAL-PMT-G TO #DVD-GRAND-PMT-G
        //*  RBE2
        pnd_Gtd_Grand_Pmt_L.nadd(pnd_Gtd_Total_Pmt_L);                                                                                                                    //Natural: ADD #GTD-TOTAL-PMT-L TO #GTD-GRAND-PMT-L
        //*  RBE2
        pnd_Dvd_Grand_Pmt_L.nadd(pnd_Dvd_Total_Pmt_L);                                                                                                                    //Natural: ADD #DVD-TOTAL-PMT-L TO #DVD-GRAND-PMT-L
                                                                                                                                                                          //Natural: PERFORM FINAL-PRINTOUT
        sub_Final_Printout();
        if (condition(Global.isEscape())) {return;}
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MAIN-ROUTINE
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADINGS
        //*  #S1-FINAL-PER-PAY-DATE            11X
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RATES-RECORD
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSITION-PRINTOUT
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUBTOTAL-PRINTOUT
        //*    #CURR-DATE 2X #CURR-TIME /
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FINAL-PRINTOUT
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TRANSFER-DATE
        //* **********************************************************************
    }
    private void sub_Main_Routine() throws Exception                                                                                                                      //Natural: MAIN-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Tiaa_Check.setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_1());                                                                             //Natural: MOVE IAXFR-ACTRL-FLD-1 TO #TIAA-CHECK
        //*  RBE2
        pnd_Iaxfr_Actrl_Cntrct_Py.setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Cntrct_Py());                                                              //Natural: MOVE IAXFR-ACTRL-CNTRCT-PY TO #IAXFR-ACTRL-CNTRCT-PY
        //*  WRITE '=' #IAXFR-ACTRL-RECORD-NUMBER-A2
        //*  IF IAXFR-ACTRL-RECORD-NUMBER-A2 �= '  '                  /* RBE2
        //*  IF #IAXFR-ACTRL-RECORD-NUMBER-A2 �= '  '                 /* RBE2
        //*  RBE2
        if (condition(pnd_Tiaa_Check_Pnd_Tiaa_Check_Ind.notEquals("T")))                                                                                                  //Natural: IF #TIAA-CHECK-IND NOT = 'T'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_Tiaa_Check_Pnd_Tiaa_Check_Ind);                                                                                                     //Natural: WRITE '=' #TIAA-CHECK-IND
        if (Global.isEscape()) return;
        if (condition(pnd_Tiaa_Check_Pnd_Tiaa_Check_Ind.equals("T")))                                                                                                     //Natural: IF #TIAA-CHECK-IND = 'T'
        {
            ldaAial0140.getPnd_Store_Actrl_Fld_1().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_1());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-1 TO #STORE-ACTRL-FLD-1
            ldaAial0140.getPnd_Store_Actrl_Fld_2().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_2());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-2 TO #STORE-ACTRL-FLD-2
            ldaAial0140.getPnd_Store_Actrl_Fld_3().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_3());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-3 TO #STORE-ACTRL-FLD-3
            ldaAial0140.getPnd_Store_Actrl_Fld_4().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_4());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-4 TO #STORE-ACTRL-FLD-4
            ldaAial0140.getPnd_Store_Actrl_Fld_5().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_5());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-5 TO #STORE-ACTRL-FLD-5
            ldaAial0140.getPnd_Store_Actrl_Fld_6().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_6());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-6 TO #STORE-ACTRL-FLD-6
            ldaAial0140.getPnd_Store_Actrl_Fld_7().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_7());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-7 TO #STORE-ACTRL-FLD-7
            ldaAial0140.getPnd_Store_Actrl_Fld_8().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_8());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-8 TO #STORE-ACTRL-FLD-8
            ldaAial0140.getPnd_Store_Actrl_Fld_9().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_9());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-9 TO #STORE-ACTRL-FLD-9
            ldaAial0140.getPnd_Store_Actrl_Fld_10().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_10());                                               //Natural: MOVE IAXFR-ACTRL-FLD-10 TO #STORE-ACTRL-FLD-10
            ldaAial0140.getPnd_Store_Actrl_Fld_11().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_11());                                               //Natural: MOVE IAXFR-ACTRL-FLD-11 TO #STORE-ACTRL-FLD-11
            ldaAial0140.getPnd_Store_Actrl_Fld_12().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_12());                                               //Natural: MOVE IAXFR-ACTRL-FLD-12 TO #STORE-ACTRL-FLD-12
            ldaAial0140.getPnd_Store_Actrl_Fld_13().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_13());                                               //Natural: MOVE IAXFR-ACTRL-FLD-13 TO #STORE-ACTRL-FLD-13
            ldaAial0140.getPnd_Store_Actrl_Fld_14().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_14());                                               //Natural: MOVE IAXFR-ACTRL-FLD-14 TO #STORE-ACTRL-FLD-14
            ldaAial0140.getPnd_Store_Actrl_Fld_15().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_15());                                               //Natural: MOVE IAXFR-ACTRL-FLD-15 TO #STORE-ACTRL-FLD-15
            ldaAial0140.getPnd_Store_Actrl_Fld_16().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_16());                                               //Natural: MOVE IAXFR-ACTRL-FLD-16 TO #STORE-ACTRL-FLD-16
            ldaAial0140.getPnd_Store_Actrl_Fld_17().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_17());                                               //Natural: MOVE IAXFR-ACTRL-FLD-17 TO #STORE-ACTRL-FLD-17
            ldaAial0140.getPnd_Store_Actrl_Fld_18().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_18());                                               //Natural: MOVE IAXFR-ACTRL-FLD-18 TO #STORE-ACTRL-FLD-18
            //*  KCD-2
            ldaAial0140.getPnd_Store_Actrl_Fld_19().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_19());                                               //Natural: MOVE IAXFR-ACTRL-FLD-19 TO #STORE-ACTRL-FLD-19
            ldaAial0140.getPnd_Store_Actrl_Fld_20().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_20());                                               //Natural: MOVE IAXFR-ACTRL-FLD-20 TO #STORE-ACTRL-FLD-20
                                                                                                                                                                          //Natural: PERFORM PRINT-TRANSFER-DATE
            sub_Print_Transfer_Date();
            if (condition(Global.isEscape())) {return;}
            //* *FOR #I = 1 TO 7   /* EOP 09/06/01 FOR NAT CONVERSION EMPTY LOOP >> <<
            //* *  WRITE `S1-RATE-CODE-G (#I) #S1-GTD-PMT-G (#I) #S1-DVD-PMT-G (#I)
            //* *  WRITE #S4-RATE-CODE-L (#I) #S4-GTD-PMT-L (#I) #S4-GTD-PMT-L (#I)
            //* *END-FOR           /* EOP 09/06/01 FOR NAT CONVERSION EMPTY LOOP >> <<
            //* * D WORK FILE 1 RECORD #AIAN014B-RECORD
            //*  IF #NUM-LINES NOT < 59                              RBE2
            //*    PERFORM WRITE-HEADINGS                            RBE2
            //*  END-IF                                              RBE2
            pnd_Contract_Payee.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Contract_Payee());                                                                    //Natural: MOVE #S1-CONTRACT-PAYEE TO #CONTRACT-PAYEE
            //*    #TEMP-CONTRACT-PAYEE                                  /* RBE2
            pnd_Print_Ppcn_Pnd_Print_Ppcn_1_7.setValue(pnd_Contract_Payee_Pnd_Contract_Number_1_7);                                                                       //Natural: MOVE #CONTRACT-NUMBER-1-7 TO #PRINT-PPCN-1-7
            pnd_Print_Ppcn_Pnd_Print_Ppcn_8.setValue(pnd_Contract_Payee_Pnd_Contract_Number_8);                                                                           //Natural: MOVE #CONTRACT-NUMBER-8 TO #PRINT-PPCN-8
            //*  RBE2 FIX BEGINS  >>
            //*  WRITE / 2X #PRINT-PPCN 5X #S1-MODE 6X #S1-OPTION
            //*    5X #S1-FINAL-PER-PAY-DATE 12X
            //*    #DATE2 3X
            //* *   #S1-RATE-CODE-G (1) 1X #S1-GTD-PMT-G (1) (EM=Z,ZZZ,ZZZ.99) 3X
            //* *   #S1-DVD-PMT-G (1) (EM=Z,ZZZ,ZZZ.99) 3X
            //*     #S2-RATE-CODE-G (1) 1X #S2-GTD-PMT-G (1) (EM=Z,ZZZ,ZZZ.99) 3X
            //*     #S2-DVD-PMT-G (1) (EM=Z,ZZZ,ZZZ.99) 3X
            //* *   #S4-GTD-PMT-L (1) (EM=Z,ZZZ,ZZZ.99) 3X
            //* *   #S4-DVD-PMT-L (1) (EM=Z,ZZZ,ZZZ.99)
            //*     #S12-GTD-PMT-L (1) (EM=Z,ZZZ,ZZZ.99) 3X
            //*     #S12-DVD-PMT-L (1) (EM=Z,ZZZ,ZZZ.99)
            //*   ADD 2 TO #NUM-LINES
            //*  ---- NEXT 4 COMMENTED LINES ARE OLD COMMENTS
            //*  MOVE #S1-GTD-PMT-G (1) TO #GTD-TOTAL-PMT-G
            //* * MOVE #S1-DVD-PMT-G (1) TO #DVD-TOTAL-PMT-G
            //* * MOVE #S4-GTD-PMT-L (1) TO #GTD-TOTAL-PMT-L
            //*  MOVE #S4-DVD-PMT-L (1) TO #DVD-TOTAL-PMT-L
            //*  MOVE #S2-GTD-PMT-G (1) TO #GTD-TOTAL-PMT-G
            //*  MOVE #S2-DVD-PMT-G (1) TO #DVD-TOTAL-PMT-G
            //*  MOVE #S12-GTD-PMT-L (1) TO #GTD-TOTAL-PMT-L
            //*  MOVE #S12-DVD-PMT-L (1) TO #DVD-TOTAL-PMT-L
            pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Rate_Code_G().getValue(1));                                                         //Natural: MOVE #S2-RATE-CODE-G ( 1 ) TO #TEMP-RATE-CODE-G #RATE-CODE
            pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Rate_Code_G().getValue(1));
            pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Gtd_Pmt_G().getValue(1));                                                             //Natural: MOVE #S2-GTD-PMT-G ( 1 ) TO #TEMP-GTD-PMT-G
            pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Dvd_Pmt_G().getValue(1));                                                             //Natural: MOVE #S2-DVD-PMT-G ( 1 ) TO #TEMP-DVD-PMT-G
            pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_12_Pnd_S12_Gtd_Pmt_L().getValue(1));                                                           //Natural: MOVE #S12-GTD-PMT-L ( 1 ) TO #TEMP-GTD-PMT-L
            pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_12_Pnd_S12_Dvd_Pmt_L().getValue(1));                                                           //Natural: MOVE #S12-DVD-PMT-L ( 1 ) TO #TEMP-DVD-PMT-L
            getReports().write(0, "BB: ---------------------------------------");                                                                                         //Natural: WRITE 'BB: ---------------------------------------'
            if (Global.isEscape()) return;
            getReports().write(0, "BB:",ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Contract_Payee(),"=?",pnd_Temp_Contract_Payee," AND ",ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Sequence_Number(), //Natural: WRITE 'BB:' #S1-CONTRACT-PAYEE '=?' #TEMP-CONTRACT-PAYEE ' AND ' SEQUENCE-NUMBER '=?' #TEMP-SEQUENCE-NBR
                "=?",pnd_Temp_Sequence_Nbr);
            if (Global.isEscape()) return;
            getReports().write(0, "BB: ","=",pnd_First_Time,"=",pnd_Skip_Routine);                                                                                        //Natural: WRITE 'BB: ' '=' #FIRST-TIME '=' #SKIP-ROUTINE
            if (Global.isEscape()) return;
            getReports().write(0, "BB: ///////////////////////////////////////");                                                                                         //Natural: WRITE 'BB: ///////////////////////////////////////'
            if (Global.isEscape()) return;
            if (condition(pnd_First_Time.equals("Y")))                                                                                                                    //Natural: IF #FIRST-TIME = 'Y'
            {
                pnd_First_Time.setValue("N");                                                                                                                             //Natural: MOVE 'N' TO #FIRST-TIME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition((ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Contract_Payee().equals(pnd_Temp_Contract_Payee)) && (ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Sequence_Number().equals(pnd_Temp_Sequence_Nbr)))) //Natural: IF ( #S1-CONTRACT-PAYEE = #TEMP-CONTRACT-PAYEE ) AND ( SEQUENCE-NUMBER = #TEMP-SEQUENCE-NBR )
                {
                    pnd_Skip_Routine.setValue("Y");                                                                                                                       //Natural: ASSIGN #SKIP-ROUTINE := 'Y'
                                                                                                                                                                          //Natural: PERFORM TRANSITION-PRINTOUT
                    sub_Transition_Printout();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition((ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Contract_Payee().notEquals(pnd_Temp_Contract_Payee))))                                     //Natural: IF ( #S1-CONTRACT-PAYEE NOT = #TEMP-CONTRACT-PAYEE )
                    {
                        pnd_Skip_Routine.setValue("Y");                                                                                                                   //Natural: ASSIGN #SKIP-ROUTINE := 'Y'
                                                                                                                                                                          //Natural: PERFORM TRANSITION-PRINTOUT
                        sub_Transition_Printout();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Gtd_Total_Pmt_G.nadd(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Gtd_Pmt_G().getValue(1));                                                                //Natural: ADD #S2-GTD-PMT-G ( 1 ) TO #GTD-TOTAL-PMT-G
            pnd_Dvd_Total_Pmt_G.nadd(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Dvd_Pmt_G().getValue(1));                                                                //Natural: ADD #S2-DVD-PMT-G ( 1 ) TO #DVD-TOTAL-PMT-G
            pnd_Gtd_Total_Pmt_L.nadd(ldaAial0140.getPnd_Store_Actrl_Fld_12_Pnd_S12_Gtd_Pmt_L().getValue(1));                                                              //Natural: ADD #S12-GTD-PMT-L ( 1 ) TO #GTD-TOTAL-PMT-L
            pnd_Dvd_Total_Pmt_L.nadd(ldaAial0140.getPnd_Store_Actrl_Fld_12_Pnd_S12_Dvd_Pmt_L().getValue(1));                                                              //Natural: ADD #S12-DVD-PMT-L ( 1 ) TO #DVD-TOTAL-PMT-L
            //*  RBE2
            pnd_Temp_Sequence_Nbr.setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Sequence_Number());                                                                    //Natural: MOVE SEQUENCE-NUMBER TO #TEMP-SEQUENCE-NBR
            //*  RBE2
            pnd_Temp_Contract_Payee.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Contract_Payee());                                                               //Natural: MOVE #S1-CONTRACT-PAYEE TO #TEMP-CONTRACT-PAYEE
            //*  RBE2 FIX ENDS    <<
            pnd_I.setValue(2);                                                                                                                                            //Natural: MOVE 2 TO #I
            //*  RBE2 FIX BEGINS  >>
            getReports().write(0, "=",pnd_Num_Lines,"=",pnd_Rate_Code);                                                                                                   //Natural: WRITE '=' #NUM-LINES '=' #RATE-CODE
            if (Global.isEscape()) return;
            if (condition(pnd_Num_Lines.greaterOrEqual(pnd_Max_Lines)))                                                                                                   //Natural: IF #NUM-LINES NOT < #MAX-LINES
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                sub_Write_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Skip_Routine.equals("N")))                                                                                                              //Natural: IF #SKIP-ROUTINE = 'N'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-RATES-RECORD
                    sub_Write_Rates_Record();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Skip_Routine.setValue("N");                                                                                                                       //Natural: MOVE 'N' TO #SKIP-ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  RBE2 FIX ENDS    <<
            getReports().write(0, "==================================");                                                                                                  //Natural: WRITE '=================================='
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Contract_Payee(),"=",ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Sequence_Number());        //Natural: WRITE '=' #S1-CONTRACT-PAYEE '=' SEQUENCE-NUMBER
            if (Global.isEscape()) return;
            //*  REPEAT UNTIL #I > 90 OR #RATE-CODE = '  ' OR #RATE-CODE = '00'
            //*  EPEAT UNTIL #I > 108 OR #RATE-CODE = '  ' OR #RATE-CODE = '00' /*KCD2
            //*  RBE2
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_I.greater(pnd_Max_Rates) || pnd_Rate_Code.equals("00") || pnd_Rate_Code.equals("  "))) {break;}                                         //Natural: UNTIL #I > #MAX-RATES OR #RATE-CODE = '00' OR = '  '
                //*  ---------------------------
                //*  THESE ARE TIAA GRADED RATES
                //*  ---------------------------
                if (condition(pnd_I.less(11)))                                                                                                                            //Natural: IF #I < 11
                {
                    //*      MOVE  #S2-RATE-CODE-G (#I) TO #TEMP-RATE-CODE-G      /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Rate_Code_G().getValue(pnd_I));                                             //Natural: MOVE #S2-RATE-CODE-G ( #I ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Rate_Code_G().getValue(pnd_I));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Gtd_Pmt_G().getValue(pnd_I));                                                 //Natural: MOVE #S2-GTD-PMT-G ( #I ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_2_Pnd_S2_Dvd_Pmt_G().getValue(pnd_I));                                                 //Natural: MOVE #S2-DVD-PMT-G ( #I ) TO #TEMP-DVD-PMT-G
                    //*  RITE 'RR:' #TEMP-RATE-CODE-G '**' #TEMP-GTD-PMT-G '**' #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(10) && pnd_I.less(21)))                                                                                                       //Natural: IF #I > 10 AND #I < 21
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(10));                                                                               //Natural: COMPUTE #J = #I - 10
                    //*      MOVE  #S3-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G      /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_3_Pnd_S3_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S3-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_3_Pnd_S3_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_3_Pnd_S3_Gtd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S3-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_3_Pnd_S3_Dvd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S3-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(20) && pnd_I.less(31)))                                                                                                       //Natural: IF #I > 20 AND #I < 31
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(20));                                                                               //Natural: COMPUTE #J = #I - 20
                    //*  RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_4_Pnd_S4_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S4-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_4_Pnd_S4_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S4-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_4_Pnd_S4_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_4_Pnd_S4_Gtd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S4-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_4_Pnd_S4_Dvd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S4-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(30) && pnd_I.less(41)))                                                                                                       //Natural: IF #I > 30 AND #I < 41
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(30));                                                                               //Natural: COMPUTE #J = #I - 30
                    //*      MOVE  #S5-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G       /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_5_Pnd_S5_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S5-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_5_Pnd_S5_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_5_Pnd_S5_Gtd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S5-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_5_Pnd_S5_Dvd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S5-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(40) && pnd_I.less(51)))                                                                                                       //Natural: IF #I > 40 AND #I < 51
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(40));                                                                               //Natural: COMPUTE #J = #I - 40
                    //*      MOVE  #S6-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G       /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_6_Pnd_S6_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S6-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_6_Pnd_S6_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_6_Pnd_S6_Gtd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S6-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_6_Pnd_S6_Dvd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S6-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(50) && pnd_I.less(61)))                                                                                                       //Natural: IF #I > 50 AND #I < 61
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(50));                                                                               //Natural: COMPUTE #J = #I - 50
                    //*      MOVE  #S7-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G       /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_7_Pnd_S7_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S7-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_7_Pnd_S7_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_7_Pnd_S7_Gtd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S7-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_7_Pnd_S7_Dvd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S7-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(60) && pnd_I.less(71)))                                                                                                       //Natural: IF #I > 60 AND #I < 71
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(60));                                                                               //Natural: COMPUTE #J = #I - 60
                    //*      MOVE  #S8-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G      /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_8_Pnd_S8_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S8-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_8_Pnd_S8_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_8_Pnd_S8_Gtd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S8-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_8_Pnd_S8_Dvd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S8-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(70) && pnd_I.less(81)))                                                                                                       //Natural: IF #I > 70 AND #I < 81
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(70));                                                                               //Natural: COMPUTE #J = #I - 70
                    //*      MOVE  #S9-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G      /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_9_Pnd_S9_Rate_Code_G().getValue(pnd_J));                                             //Natural: MOVE #S9-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_9_Pnd_S9_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_9_Pnd_S9_Gtd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S9-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_9_Pnd_S9_Dvd_Pmt_G().getValue(pnd_J));                                                 //Natural: MOVE #S9-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(80) && pnd_I.less(91)))                                                                                                       //Natural: IF #I > 80 AND #I < 91
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(80));                                                                               //Natural: COMPUTE #J = #I - 80
                    //*      MOVE  #S10-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G      /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_10_Pnd_S10_Rate_Code_G().getValue(pnd_J));                                           //Natural: MOVE #S10-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_10_Pnd_S10_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_10_Pnd_S10_Gtd_Pmt_G().getValue(pnd_J));                                               //Natural: MOVE #S10-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_10_Pnd_S10_Dvd_Pmt_G().getValue(pnd_J));                                               //Natural: MOVE #S10-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                //*  KCD-2
                if (condition(pnd_I.greater(90) && pnd_I.less(101)))                                                                                                      //Natural: IF #I > 90 AND #I < 101
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(90));                                                                               //Natural: COMPUTE #J = #I - 90
                    //*      MOVE  #S11-RATE-CODE-G (#J) TO #TEMP-RATE-CODE-G      /* RBE2
                    pnd_Temp_Rate_Code_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_11_Pnd_S11_Rate_Code_G().getValue(pnd_J));                                           //Natural: MOVE #S11-RATE-CODE-G ( #J ) TO #TEMP-RATE-CODE-G #RATE-CODE
                    pnd_Rate_Code.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_11_Pnd_S11_Rate_Code_G().getValue(pnd_J));
                    pnd_Temp_Gtd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_11_Pnd_S11_Gtd_Pmt_G().getValue(pnd_J));                                               //Natural: MOVE #S11-GTD-PMT-G ( #J ) TO #TEMP-GTD-PMT-G
                    pnd_Temp_Dvd_Pmt_G.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_11_Pnd_S11_Dvd_Pmt_G().getValue(pnd_J));                                               //Natural: MOVE #S11-DVD-PMT-G ( #J ) TO #TEMP-DVD-PMT-G
                }                                                                                                                                                         //Natural: END-IF
                //*  -----------------------------
                //*  THESE ARE TIAA STANDARD RATES
                //*  -----------------------------
                if (condition(pnd_I.less(13)))                                                                                                                            //Natural: IF #I < 13
                {
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_12_Pnd_S12_Gtd_Pmt_L().getValue(pnd_I));                                               //Natural: MOVE #S12-GTD-PMT-L ( #I ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_12_Pnd_S12_Dvd_Pmt_L().getValue(pnd_I));                                               //Natural: MOVE #S12-DVD-PMT-L ( #I ) TO #TEMP-DVD-PMT-L
                    getReports().write(0, "RR:",pnd_Temp_Rate_Code_G,"**",pnd_Temp_Gtd_Pmt_L,"**",pnd_Temp_Dvd_Pmt_L);                                                    //Natural: WRITE 'RR:' #TEMP-RATE-CODE-G '**' #TEMP-GTD-PMT-L '**' #TEMP-DVD-PMT-L
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(12) && pnd_I.less(25)))                                                                                                       //Natural: IF #I > 12 AND #I < 25
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(12));                                                                               //Natural: COMPUTE #J = #I - 12
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_13_Pnd_S13_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S13-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_13_Pnd_S13_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S13-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(24) && pnd_I.less(37)))                                                                                                       //Natural: IF #I > 24 AND #I < 37
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(24));                                                                               //Natural: COMPUTE #J = #I - 24
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_14_Pnd_S14_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S14-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_14_Pnd_S14_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S14-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(36) && pnd_I.less(49)))                                                                                                       //Natural: IF #I > 36 AND #I < 49
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(36));                                                                               //Natural: COMPUTE #J = #I - 36
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_15_Pnd_S15_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S15-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_15_Pnd_S15_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S15-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(48) && pnd_I.less(61)))                                                                                                       //Natural: IF #I > 48 AND #I < 61
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(48));                                                                               //Natural: COMPUTE #J = #I - 48
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_16_Pnd_S16_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S16-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_16_Pnd_S16_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S16-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(60) && pnd_I.less(73)))                                                                                                       //Natural: IF #I > 60 AND #I < 73
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(60));                                                                               //Natural: COMPUTE #J = #I - 60
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_17_Pnd_S17_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S17-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_17_Pnd_S17_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S17-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(72) && pnd_I.less(85)))                                                                                                       //Natural: IF #I > 72 AND #I < 85
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(72));                                                                               //Natural: COMPUTE #J = #I - 72
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_18_Pnd_S18_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S18-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_18_Pnd_S18_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S18-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(84) && pnd_I.less(97)))                                                                                                       //Natural: IF #I > 84 AND #I < 97
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(84));                                                                               //Natural: COMPUTE #J = #I - 84
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_19_Pnd_S19_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S19-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_19_Pnd_S19_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S19-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.greater(96) && pnd_I.less(108)))                                                                                                      //Natural: IF #I > 96 AND #I < 108
                {
                    pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(96));                                                                               //Natural: COMPUTE #J = #I - 96
                    pnd_Temp_Gtd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_20_Pnd_S20_Gtd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S20-GTD-PMT-L ( #J ) TO #TEMP-GTD-PMT-L
                    pnd_Temp_Dvd_Pmt_L.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_20_Pnd_S20_Dvd_Pmt_L().getValue(pnd_J));                                               //Natural: MOVE #S20-DVD-PMT-L ( #J ) TO #TEMP-DVD-PMT-L
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #NUM-LINES NOT < 59                            /* RBE2
                //*  RBE2
                if (condition(pnd_Num_Lines.greaterOrEqual(pnd_Max_Lines)))                                                                                               //Natural: IF #NUM-LINES NOT < #MAX-LINES
                {
                    if (condition(pnd_Temp_Rate_Code_G.notEquals("  ") && pnd_Temp_Rate_Code_G.notEquals("00")))                                                          //Natural: IF #TEMP-RATE-CODE-G NOT = '  ' AND #TEMP-RATE-CODE-G NOT = '00'
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                        sub_Write_Headings();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  RBE2 FIX BEGINS >>
                        //*        WRITE / 3X #PRINT-PPCN 5X #S1-MODE 6X #S1-OPTION
                        //*          5X #S1-FINAL-PER-PAY-DATE 4X
                        //*          #DATE2 3X
                        //*          #TEMP-RATE-CODE-G  6X
                        //*          #TEMP-GTD-PMT-G  (EM=Z,ZZZ,ZZZ.99) 3X
                        //*          #TEMP-DVD-PMT-G  (EM=Z,ZZZ,ZZZ.99) 3X
                        //*          #TEMP-GTD-PMT-L  (EM=Z,ZZZ,ZZZ.99) 3X
                        //*          #TEMP-DVD-PMT-L  (EM=Z,ZZZ,ZZZ.99)
                        //*        ADD 2 TO #NUM-LINES
                        //*  RBE2 FIX ENDS   <<
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  RBE2 FIX BEGINS >>
                                                                                                                                                                          //Natural: PERFORM WRITE-RATES-RECORD
                    sub_Write_Rates_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      IF #TEMP-RATE-CODE-G NOT = '  '
                    //*          AND #TEMP-RATE-CODE-G NOT = '00'
                    //*        WRITE 66X
                    //*          #TEMP-RATE-CODE-G  1X
                    //*          #TEMP-GTD-PMT-G  (EM=Z,ZZZ,ZZZ.99) 3X
                    //*          #TEMP-DVD-PMT-G  (EM=Z,ZZZ,ZZZ.99) 3X
                    //*          #TEMP-GTD-PMT-L  (EM=Z,ZZZ,ZZZ.99) 3X
                    //*          #TEMP-DVD-PMT-L  (EM=Z,ZZZ,ZZZ.99)
                    //*        ADD 1 TO #NUM-LINES
                    //*      END-IF
                    //*  RBE2 FIX ENDS   <<
                }                                                                                                                                                         //Natural: END-IF
                pnd_Gtd_Total_Pmt_G.nadd(pnd_Temp_Gtd_Pmt_G);                                                                                                             //Natural: COMPUTE ROUNDED #GTD-TOTAL-PMT-G = #GTD-TOTAL-PMT-G + #TEMP-GTD-PMT-G
                pnd_Dvd_Total_Pmt_G.nadd(pnd_Temp_Dvd_Pmt_G);                                                                                                             //Natural: COMPUTE ROUNDED #DVD-TOTAL-PMT-G = #DVD-TOTAL-PMT-G + #TEMP-DVD-PMT-G
                pnd_Gtd_Total_Pmt_L.nadd(pnd_Temp_Gtd_Pmt_L);                                                                                                             //Natural: COMPUTE ROUNDED #GTD-TOTAL-PMT-L = #GTD-TOTAL-PMT-L + #TEMP-GTD-PMT-L
                pnd_Dvd_Total_Pmt_L.nadd(pnd_Temp_Dvd_Pmt_L);                                                                                                             //Natural: COMPUTE ROUNDED #DVD-TOTAL-PMT-L = #DVD-TOTAL-PMT-L + #TEMP-DVD-PMT-L
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            //*  RBE2 FIX BEGINS >>
            //*  IF #NUM-LINES NOT < 59
            //*    PERFORM WRITE-HEADINGS
            //*  END-IF
            //*  WRITE 66X 'ALL' 1X #GTD-TOTAL-PMT-G (EM=Z,ZZZ,ZZZ.99) 3X
            //*    #DVD-TOTAL-PMT-G (EM=Z,ZZZ,ZZZ.99) 3X
            //*    #GTD-TOTAL-PMT-L (EM=Z,ZZZ,ZZZ.99) 3X
            //*    #DVD-TOTAL-PMT-L (EM=Z,ZZZ,ZZZ.99)
            //*  ADD 1 TO #NUM-LINES
            //*  SUBTRACT #GTD-TOTAL-PMT-G FROM #GTD-GRAND-PMT-G
            //*  SUBTRACT #DVD-TOTAL-PMT-G FROM #DVD-GRAND-PMT-G
            //* *  ADD #GTD-TOTAL-PMT-G TO #GTD-GRAND-PMT-G
            //* *  ADD #DVD-TOTAL-PMT-G TO #DVD-GRAND-PMT-G
            //*  RBE2 FIX ENDS   <<
            //*   ADD #GTD-TOTAL-PMT-L TO #GTD-GRAND-PMT-L
            //*   ADD #DVD-TOTAL-PMT-L TO #DVD-GRAND-PMT-L
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Headings() throws Exception                                                                                                                    //Natural: WRITE-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        //*  ITE (1) #DATE1 'ACTUARIAL IA GRADED TO STANDARD TRANSFER SUMMARY'60X/
        getReports().write(1, ReportOption.NOTITLE,pnd_Date1,"ACTUARIAL IA GRADED TO STANDARD TRANSFER SUMMARY",new ColumnSpacing(30),pnd_Curr_Date,new                   //Natural: WRITE ( 1 ) #DATE1 'ACTUARIAL IA GRADED TO STANDARD TRANSFER SUMMARY'30X #CURR-DATE 2X #CURR-TIME 4X 'PAGE:' #PAGE-NUMBER /
            ColumnSpacing(2),pnd_Curr_Time,new ColumnSpacing(4),"PAGE:",pnd_Page_Number,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"1) SERIATIM DETAIL",NEWLINE);                                                                                         //Natural: WRITE ( 1 ) '1) SERIATIM DETAIL' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(34),"FINAL PERIODIC",new ColumnSpacing(18),"RATE",new ColumnSpacing(13),"GRADED",new                 //Natural: WRITE ( 1 ) 34X 'FINAL PERIODIC' 18X 'RATE' 13X 'GRADED' 23X 'STANDARD'
            ColumnSpacing(23),"STANDARD");
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(3),"CONTRACT",new ColumnSpacing(5),"MODE",new ColumnSpacing(5),"OPTION",new ColumnSpacing(5),"PAYMENT DATE",new  //Natural: WRITE ( 1 ) 3X 'CONTRACT' 5X 'MODE' 5X 'OPTION' 5X 'PAYMENT DATE' 6X 'TRAN DATE' 3X 'CODE' 5X 'GUARANTEE' 6X 'DIVIDEND' 7X 'GUARANTEE' 6X 'DIVIDEND'
            ColumnSpacing(6),"TRAN DATE",new ColumnSpacing(3),"CODE",new ColumnSpacing(5),"GUARANTEE",new ColumnSpacing(6),"DIVIDEND",new ColumnSpacing(7),"GUARANTEE",new 
            ColumnSpacing(6),"DIVIDEND");
        if (Global.isEscape()) return;
        //*  RBE2 FIX BEGINS  >>
        //*  MOVE 6 TO #NUM-LINES
        pnd_Temp_Final_Per_Pay_Date.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Final_Per_Pay_Date());                                                           //Natural: MOVE #S1-FINAL-PER-PAY-DATE TO #TEMP-FINAL-PER-PAY-DATE
        pnd_Final_Per_Pay_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Month, "/",                    //Natural: COMPRESS #TEMP-FINAL-PER-PAY-MONTH '/' #TEMP-FINAL-PER-PAY-YEAR INTO #FINAL-PER-PAY-DATE LEAVING NO SPACE
            pnd_Temp_Final_Per_Pay_Date_Pnd_Temp_Final_Per_Pay_Year));
        //*  RBE2
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(3),pnd_Print_Ppcn,new ColumnSpacing(3),ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Mode(),new  //Natural: WRITE ( 1 ) / 3X #PRINT-PPCN 3X #S1-MODE 6X #S1-OPTION 9X #FINAL-PER-PAY-DATE 9X #DATE2 3X #TEMP-RATE-CODE-G 1X #TEMP-GTD-PMT-G ( EM = Z,ZZZ,ZZ9.99 ) 3X #TEMP-DVD-PMT-G ( EM = -Z,ZZZ,ZZ9.99 ) 3X #TEMP-GTD-PMT-L ( EM = Z,ZZZ,ZZ9.99 ) 3X #TEMP-DVD-PMT-L ( EM = -Z,ZZZ,ZZ9.99 )
            ColumnSpacing(6),ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Option(),new ColumnSpacing(9),pnd_Final_Per_Pay_Date,new ColumnSpacing(9),pnd_Date2,new 
            ColumnSpacing(3),pnd_Temp_Rate_Code_G,new ColumnSpacing(1),pnd_Temp_Gtd_Pmt_G, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Temp_Dvd_Pmt_G, 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Temp_Gtd_Pmt_L, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Temp_Dvd_Pmt_L, 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Num_Lines.setValue(8);                                                                                                                                        //Natural: MOVE 8 TO #NUM-LINES
        //*  RBE2 FIX ENDS    <<
    }
    private void sub_Write_Rates_Record() throws Exception                                                                                                                //Natural: WRITE-RATES-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Temp_Rate_Code_G.notEquals("00") && pnd_Temp_Rate_Code_G.notEquals("  ")))                                                                      //Natural: IF #TEMP-RATE-CODE-G NOT = '00' AND #TEMP-RATE-CODE-G NOT = '  '
        {
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(66),pnd_Temp_Rate_Code_G,new ColumnSpacing(1),pnd_Temp_Gtd_Pmt_G, new ReportEditMask             //Natural: WRITE ( 1 ) 66X #TEMP-RATE-CODE-G 1X #TEMP-GTD-PMT-G ( EM = Z,ZZZ,ZZ9.99 ) 3X #TEMP-DVD-PMT-G ( EM = -Z,ZZZ,ZZ9.99 ) 3X #TEMP-GTD-PMT-L ( EM = Z,ZZZ,ZZ9.99 ) 3X #TEMP-DVD-PMT-L ( EM = -Z,ZZZ,ZZ9.99 )
                ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Temp_Dvd_Pmt_G, new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Temp_Gtd_Pmt_L, 
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Temp_Dvd_Pmt_L, new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Num_Lines.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #NUM-LINES
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Transition_Printout() throws Exception                                                                                                               //Natural: TRANSITION-PRINTOUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
                                                                                                                                                                          //Natural: PERFORM SUBTOTAL-PRINTOUT
        sub_Subtotal_Printout();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
        sub_Write_Headings();
        if (condition(Global.isEscape())) {return;}
        pnd_Gtd_Grand_Pmt_G.nadd(pnd_Gtd_Total_Pmt_G);                                                                                                                    //Natural: ADD #GTD-TOTAL-PMT-G TO #GTD-GRAND-PMT-G
        pnd_Dvd_Grand_Pmt_G.nadd(pnd_Dvd_Total_Pmt_G);                                                                                                                    //Natural: ADD #DVD-TOTAL-PMT-G TO #DVD-GRAND-PMT-G
        pnd_Gtd_Grand_Pmt_L.nadd(pnd_Gtd_Total_Pmt_L);                                                                                                                    //Natural: ADD #GTD-TOTAL-PMT-L TO #GTD-GRAND-PMT-L
        pnd_Dvd_Grand_Pmt_L.nadd(pnd_Dvd_Total_Pmt_L);                                                                                                                    //Natural: ADD #DVD-TOTAL-PMT-L TO #DVD-GRAND-PMT-L
        pnd_Temp_Gtd_Pmt_G.reset();                                                                                                                                       //Natural: RESET #TEMP-GTD-PMT-G #TEMP-DVD-PMT-G #TEMP-GTD-PMT-L #TEMP-DVD-PMT-L #GTD-TOTAL-PMT-G #DVD-TOTAL-PMT-G #GTD-TOTAL-PMT-L #DVD-TOTAL-PMT-L
        pnd_Temp_Dvd_Pmt_G.reset();
        pnd_Temp_Gtd_Pmt_L.reset();
        pnd_Temp_Dvd_Pmt_L.reset();
        pnd_Gtd_Total_Pmt_G.reset();
        pnd_Dvd_Total_Pmt_G.reset();
        pnd_Gtd_Total_Pmt_L.reset();
        pnd_Dvd_Total_Pmt_L.reset();
    }
    private void sub_Subtotal_Printout() throws Exception                                                                                                                 //Natural: SUBTOTAL-PRINTOUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  WRITE '***** SUBTOTAL-PRINTOUT *******'
        //*  WRITE '=' #NUM-LINES '=' #MAX-LINES
        if (condition(pnd_Num_Lines.greaterOrEqual(pnd_Max_Lines)))                                                                                                       //Natural: IF #NUM-LINES NOT < #MAX-LINES
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Page_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #PAGE-NUMBER
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(10),"ACTUARIAL IA GRADED TO STANDARD TRANSFER SUMMARY",new ColumnSpacing(30),pnd_Curr_Date,new   //Natural: WRITE ( 1 ) NOTITLE 10X 'ACTUARIAL IA GRADED TO STANDARD TRANSFER SUMMARY'30X #CURR-DATE 2X #CURR-TIME 4X 'PAGE:' #PAGE-NUMBER
                ColumnSpacing(2),pnd_Curr_Time,new ColumnSpacing(4),"PAGE:",pnd_Page_Number);
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"1) SERIATIM DETAIL",NEWLINE);                                                                                     //Natural: WRITE ( 1 ) '1) SERIATIM DETAIL' /
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(34),"FINAL PERIODIC",new ColumnSpacing(18),"RATE",new ColumnSpacing(13),"GRADED",new             //Natural: WRITE ( 1 ) 34X 'FINAL PERIODIC' 18X 'RATE' 13X 'GRADED' 23X 'STANDARD'
                ColumnSpacing(23),"STANDARD");
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(3),"CONTRACT",new ColumnSpacing(5),"MODE",new ColumnSpacing(5),"OPTION",new ColumnSpacing(5),"PAYMENT DATE",new  //Natural: WRITE ( 1 ) 3X 'CONTRACT' 5X 'MODE' 5X 'OPTION' 5X 'PAYMENT DATE' 6X 'TRAN DATE' 3X 'CODE' 5X 'GUARANTEE' 6X 'DIVIDEND' 7X 'GUARANTEE' 6X 'DIVIDEND'
                ColumnSpacing(6),"TRAN DATE",new ColumnSpacing(3),"CODE",new ColumnSpacing(5),"GUARANTEE",new ColumnSpacing(6),"DIVIDEND",new ColumnSpacing(7),"GUARANTEE",new 
                ColumnSpacing(6),"DIVIDEND");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(65),"ALL",new ColumnSpacing(1),pnd_Gtd_Total_Pmt_G, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new  //Natural: WRITE ( 1 ) / 65X 'ALL' 1X #GTD-TOTAL-PMT-G ( EM = Z,ZZZ,ZZZ.99 ) 4X #DVD-TOTAL-PMT-G ( EM = Z,ZZZ,ZZZ.99 ) 3X #GTD-TOTAL-PMT-L ( EM = Z,ZZZ,ZZZ.99 ) 3X #DVD-TOTAL-PMT-L ( EM = -Z,ZZZ,ZZZ.99 )
            ColumnSpacing(4),pnd_Dvd_Total_Pmt_G, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new ColumnSpacing(3),pnd_Gtd_Total_Pmt_L, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new 
            ColumnSpacing(3),pnd_Dvd_Total_Pmt_L, new ReportEditMask ("-Z,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Final_Printout() throws Exception                                                                                                                    //Natural: FINAL-PRINTOUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        getReports().write(0, "***** FINAL-PRINTOUT *******");                                                                                                            //Natural: WRITE '***** FINAL-PRINTOUT *******'
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  RBE2 FIX BEGINS >>
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        //*  WRITE #DATE1 'ACTUARIAL IA GRADED TO STANDARD TRANSFER SUMMARY'60X/
        getReports().write(1, ReportOption.NOTITLE,pnd_Date1,"ACTUARIAL IA GRADED TO STANDARD TRANSFER GRAND TOTALS SUMMARY",new ColumnSpacing(15),pnd_Curr_Date,new      //Natural: WRITE ( 1 ) #DATE1 'ACTUARIAL IA GRADED TO STANDARD TRANSFER GRAND TOTALS SUMMARY' 15X #CURR-DATE 2X #CURR-TIME 4X 'PAGE: ' #PAGE-NUMBER /
            ColumnSpacing(2),pnd_Curr_Time,new ColumnSpacing(4),"PAGE: ",pnd_Page_Number,NEWLINE);
        if (Global.isEscape()) return;
        //*  RBE2 FIX ENDS   <<
        getReports().write(1, ReportOption.NOTITLE,"2) NET PERIODIC PAYMENT TRANSFER SUMMARY:",NEWLINE);                                                                  //Natural: WRITE ( 1 ) '2) NET PERIODIC PAYMENT TRANSFER SUMMARY:'/
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(45),"GUAR",new ColumnSpacing(21),"DIV");                                                             //Natural: WRITE ( 1 ) 45X 'GUAR' 21X 'DIV'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(34),"---------------",new ColumnSpacing(10),"---------------");                                      //Natural: WRITE ( 1 ) 34X '---------------' 10X '---------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(10),"TIAA GRADED",new ColumnSpacing(10),pnd_Gtd_Grand_Pmt_G, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZZ.99"),new  //Natural: WRITE ( 1 ) 10X 'TIAA GRADED' 10X #GTD-GRAND-PMT-G ( EM = -ZZ,ZZZ,ZZZ,ZZZ.99 ) 7X #DVD-GRAND-PMT-G ( EM = -ZZ,ZZZ,ZZZ,ZZZ.99 )
            ColumnSpacing(7),pnd_Dvd_Grand_Pmt_G, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(10),"TIAA STANDARD",new ColumnSpacing(8),pnd_Gtd_Grand_Pmt_L, new ReportEditMask                     //Natural: WRITE ( 1 ) 10X 'TIAA STANDARD' 8X #GTD-GRAND-PMT-L ( EM = -ZZ,ZZZ,ZZZ,ZZZ.99 ) 7X #DVD-GRAND-PMT-L ( EM = -ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("-ZZ,ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(7),pnd_Dvd_Grand_Pmt_L, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Get_Control_Date() throws Exception                                                                                                                  //Natural: GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Cntrl_Sd_Cde.setValue("DC");                                                                                                                                  //Natural: MOVE 'DC' TO #CNTRL-SD-CDE
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-SD-CDE
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Sd_Cde, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_cntrl.readNextRow("READ02")))
        {
            pnd_Input_Record_Pnd_Input_Begin_Date_A.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #INPUT-BEGIN-DATE-A
            pnd_Input_Record_Pnd_Input_End_Date.setValue(pnd_Input_Record_Pnd_Input_Begin_Date);                                                                          //Natural: MOVE #INPUT-BEGIN-DATE TO #INPUT-END-DATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "BEGIN-DATE",pnd_Input_Record_Pnd_Input_Begin_Date);                                                                                        //Natural: WRITE 'BEGIN-DATE' #INPUT-BEGIN-DATE
        if (Global.isEscape()) return;
        pnd_Date1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Record_Pnd_Input_Begin_Month, "/", pnd_Input_Record_Pnd_Input_Begin_Day,             //Natural: COMPRESS #INPUT-BEGIN-MONTH '/' #INPUT-BEGIN-DAY '/' #INPUT-BEGIN-YEAR INTO #DATE1 LEAVING NO SPACE
            "/", pnd_Input_Record_Pnd_Input_Begin_Year));
    }
    private void sub_Print_Transfer_Date() throws Exception                                                                                                               //Natural: PRINT-TRANSFER-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Transfer_Date.setValue(ldaAial0140.getPnd_Store_Actrl_Fld_1_Pnd_S1_Transfer_Effective_Date());                                                                //Natural: MOVE #S1-TRANSFER-EFFECTIVE-DATE TO #TRANSFER-DATE
        pnd_Date2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Transfer_Date_Pnd_Transfer_Month, "/", pnd_Transfer_Date_Pnd_Transfer_Day,                 //Natural: COMPRESS #TRANSFER-MONTH '/' #TRANSFER-DAY '/' #TRANSFER-YEAR INTO #DATE2 LEAVING NO SPACE
            "/", pnd_Transfer_Date_Pnd_Transfer_Year));
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=0 ES=ON");
        Global.format(1, "LS=132 PS=0 ES=ON");
    }
}
