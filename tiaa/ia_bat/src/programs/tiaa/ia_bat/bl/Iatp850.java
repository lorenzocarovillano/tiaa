/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:53 PM
**        * FROM NATURAL PROGRAM : Iatp850
************************************************************
**        * FILE NAME            : Iatp850.java
**        * CLASS NAME           : Iatp850
**        * INSTANCE NAME        : Iatp850
************************************************************
**********************************************************************
*   PROGRAM     -  IATP850                                           *
*      DATE     -  03/02/2000                                        *
*    AUTHOR     -  JERRY KREISCHER / JUN TINIO                       *
*  FUNCTION     -  PRINT REMINDER LETTER FOR SUBSEQUENT REPETITIVE
*                  REQUESTS 45 DAYS BEFORE THEIR EFFECTIVE DATE.
*
*  HISTORY
*  04/13/2000   -  MODIFY SUPERDESCRIPTOR USED FOR ACCESSING KDO AND
*                  USE REJECT LOGIC INSTEAD OF MULTIPLE NESTED IFS.
*
*  10/09/2000   -  ADD LOGIC TO PRODUCE LETTERS ONLY FOR REQUESTS
*                  WITH EFFECTIVE DATES GREATER THAN 10 DAYS FROM
*                  TODAY's date.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp850 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_Msg;
    private DbsField pls_Pnd_Debug_Ctr;
    private DbsField pnd_Rqst_Type;
    private DbsField pnd_Request_Id;
    private DbsField pnd_Printer_Id;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Msg1;
    private DbsField pnd_Mit_Status;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Srce;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Ssn;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde;

    private DbsGroup iaa_Trnsfr_Sw_Rqst__R_Field_1;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde_R;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee;

    private DbsGroup iaa_Trnsfr_Sw_Rqst__R_Field_2;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee_N;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_New_Issue;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField pnd_Top_Date;
    private DbsField pnd_Payee_Key;

    private DbsGroup pnd_Payee_Key__R_Field_3;
    private DbsField pnd_Payee_Key_Pnd_Contract;
    private DbsField pnd_Payee_Key_Pnd_Payee;
    private DbsField pnd_Mail_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // Local Variables
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.STRING, 1);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 79);
        pls_Pnd_Debug_Ctr = WsIndependent.getInstance().newFieldInRecord("pls_Pnd_Debug_Ctr", "+#DEBUG-CTR", FieldType.NUMERIC, 2);
        pnd_Rqst_Type = localVariables.newFieldInRecord("pnd_Rqst_Type", "#RQST-TYPE", FieldType.STRING, 1);
        pnd_Request_Id = localVariables.newFieldInRecord("pnd_Request_Id", "#REQUEST-ID", FieldType.STRING, 34);
        pnd_Printer_Id = localVariables.newFieldInRecord("pnd_Printer_Id", "#PRINTER-ID", FieldType.STRING, 4);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Msg1 = localVariables.newFieldInRecord("pnd_Msg1", "#MSG1", FieldType.STRING, 79);
        pnd_Mit_Status = localVariables.newFieldInRecord("pnd_Mit_Status", "#MIT-STATUS", FieldType.STRING, 4);

        vw_iaa_Trnsfr_Sw_Rqst = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst", "IAA-TRNSFR-SW-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_ENTRY_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_RCVD_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_CNTCT_MDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Srce = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_SRCE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "RQST_REP_NME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_STTMNT_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RQST_RCPRCL_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Ssn = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RQST_SSN");
        iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");

        iaa_Trnsfr_Sw_Rqst__R_Field_1 = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst__R_Field_1", "REDEFINE", iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde);
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde_R = iaa_Trnsfr_Sw_Rqst__R_Field_1.newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde_R", "XFR-STTS-CDE-R", FieldType.STRING, 
            1);
        iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_RJCTN_CDE");
        iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "XFR_RETRY_CNT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");

        iaa_Trnsfr_Sw_Rqst__R_Field_2 = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst__R_Field_2", "REDEFINE", iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee);
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee_N = iaa_Trnsfr_Sw_Rqst__R_Field_2.newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee_N", "IA-FRM-PAYEE-N", FieldType.NUMERIC, 
            2);
        iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "IA_HOLD_CDE");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta", 
            "C*XFR-FRM-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta", 
            "C*XFR-TO-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IA_APPL_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_Ia_New_Issue = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISSUE");
        iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_RSN_FOR_OVRRDE");
        iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "IA_OVRRDE_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_GNRL_PEND_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Top_Date = localVariables.newFieldInRecord("pnd_Top_Date", "#TOP-DATE", FieldType.DATE);
        pnd_Payee_Key = localVariables.newFieldInRecord("pnd_Payee_Key", "#PAYEE-KEY", FieldType.STRING, 12);

        pnd_Payee_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Payee_Key__R_Field_3", "REDEFINE", pnd_Payee_Key);
        pnd_Payee_Key_Pnd_Contract = pnd_Payee_Key__R_Field_3.newFieldInGroup("pnd_Payee_Key_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Payee_Key_Pnd_Payee = pnd_Payee_Key__R_Field_3.newFieldInGroup("pnd_Payee_Key_Pnd_Payee", "#PAYEE", FieldType.NUMERIC, 2);
        pnd_Mail_Date = localVariables.newFieldInRecord("pnd_Mail_Date", "#MAIL-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();

        localVariables.reset();
        pls_Pnd_Debug_Ctr.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp850() throws Exception
    {
        super("Iatp850");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            if (condition(iaa_Cntrl_Rcrd_1_Cntrl_Cde.equals("DC")))                                                                                                       //Natural: IF CNTRL-CDE = 'DC'
            {
                pnd_Top_Date.compute(new ComputeParameters(false, pnd_Top_Date), iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte.add(45));                                              //Natural: COMPUTE #TOP-DATE = IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE + 45
                pnd_Mail_Date.compute(new ComputeParameters(false, pnd_Mail_Date), iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte.add(10));                                            //Natural: COMPUTE #MAIL-DATE = IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE + 10
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Trnsfr_Sw_Rqst.startDatabaseRead                                                                                                                           //Natural: READ IAA-TRNSFR-SW-RQST BY IA-SUPER-DE-02 STARTING FROM '1F'
        (
        "R1",
        new Wc[] { new Wc("IA_SUPER_DE_02", ">=", "1F", WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_02", "ASC") }
        );
        R1:
        while (condition(vw_iaa_Trnsfr_Sw_Rqst.readNextRow("R1")))
        {
            if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.notEquals("1") || ! (DbsUtil.maskMatches(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde,"'F'"))))                             //Natural: IF RCRD-TYPE-CDE NE '1' OR XFR-STTS-CDE NE MASK ( 'F' )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition((iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte.greater(pnd_Top_Date) || (iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte.less(pnd_Top_Date) && iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme.notEquals(" "))))) //Natural: REJECT IF RQST-EFFCTV-DTE GT #TOP-DATE OR ( RQST-EFFCTV-DTE LT #TOP-DATE AND IAA-TRNSFR-SW-RQST.XFR-MIT-LOG-DTE-TME NE ' ' )
            {
                continue;
            }
            if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.greater("1")))                                                                                                 //Natural: IF RQST-XFR-TYPE > '1'
            {
                pnd_Payee_Key_Pnd_Contract.setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                                    //Natural: MOVE IA-FRM-CNTRCT TO #CONTRACT
                pnd_Payee_Key_Pnd_Payee.setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee_N);                                                                                      //Natural: MOVE IA-FRM-PAYEE-N TO #PAYEE
                vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseFind                                                                                                              //Natural: FIND IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #PAYEE-KEY
                (
                "FIND01",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Payee_Key, WcType.WITH) }
                );
                FIND01:
                while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("FIND01", true)))
                {
                    vw_iaa_Cntrct_Prtcpnt_Role.setIfNotFoundControlFlag(false);
                    if (condition(vw_iaa_Cntrct_Prtcpnt_Role.getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORDS FOUND
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9)))                                                                                   //Natural: REJECT IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
                    {
                        continue;
                    }
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals("0") || iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals(" ")))                            //Natural: IF CNTRCT-PEND-CDE = '0' OR = ' '
                    {
                                                                                                                                                                          //Natural: PERFORM ADD-A-REQUEST
                        sub_Add_A_Request();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Rtrn_Cde.equals("E")))                                                                                                          //Natural: IF #RTRN-CDE = 'E'
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Mit_Status.setValue("7555");                                                                                                              //Natural: ASSIGN #MIT-STATUS := '7555'
                            //*          #PRINTER-ID := 'TZUH'
                            pnd_Request_Id.setValue(iaa_Trnsfr_Sw_Rqst_Rqst_Id);                                                                                          //Natural: MOVE IAA-TRNSFR-SW-RQST.RQST-ID TO #REQUEST-ID
                            //*  TO PREVENT REMINDER LETTERS 10 DAYS OR LESS FROM TODAY's date
                            //*  FROM PRINTING. /* JFT 10/9/2000
                            if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte.greater(pnd_Mail_Date)))                                                                     //Natural: IF RQST-EFFCTV-DTE GT #MAIL-DATE
                            {
                                DbsUtil.callnat(Iatn580.class , getCurrentProcessState(), pnd_Rqst_Type, pnd_Request_Id, pnd_Printer_Id, pnd_Return_Code,                 //Natural: CALLNAT 'IATN580' USING #RQST-TYPE #REQUEST-ID #PRINTER-ID #RETURN-CODE #MSG1 #MIT-STATUS EFSA9120.RETURN-DATE-TIME ( 1 )
                                    pnd_Msg1, pnd_Mit_Status, pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1));
                                if (condition(Global.isEscape())) return;
                            }                                                                                                                                             //Natural: END-IF
                            //*  END OF CODE /* JFT 10/9/200
                            if (condition(pnd_Return_Code.equals(" ") || pnd_Return_Code.equals("0")))                                                                    //Natural: IF #RETURN-CODE = ' ' OR = '0'
                            {
                                G1:                                                                                                                                       //Natural: GET IAA-TRNSFR-SW-RQST *ISN ( R1. )
                                vw_iaa_Trnsfr_Sw_Rqst.readByID(vw_iaa_Trnsfr_Sw_Rqst.getAstISN("Find01"), "G1");
                                iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme.setValue(pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1));                                  //Natural: MOVE EFSA9120.RETURN-DATE-TIME ( 1 ) TO IAA-TRNSFR-SW-RQST.XFR-MIT-LOG-DTE-TME
                                vw_iaa_Trnsfr_Sw_Rqst.updateDBRow("G1");                                                                                                  //Natural: UPDATE ( G1. )
                                getCurrentProcessState().getDbConv().dbCommit();                                                                                          //Natural: END TRANSACTION
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RC-LOG
                        sub_Write_Rc_Log();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-A-REQUEST
        //* ****************************************************************
        //*  PERFORM GET-UNIT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RC-LOG
        //* ***********************************************************************
    }
    private void sub_Add_A_Request() throws Exception                                                                                                                     //Natural: ADD-A-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa9120.getEfsa9120_Action().setValue("AR");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AR'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id);                                                                                      //Natural: ASSIGN EFSA9120.PIN-NBR := IA-UNIQUE-ID
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue("TAITY");                                                                                                     //Natural: ASSIGN EFSA9120.WPID ( 1 ) := 'TAITY'
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := *DATN
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde);                                                                        //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := RQST-UNIT-CDE
        pdaEfsa9120.getEfsa9120_Contract_Nbr().getValue(1,1).setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                  //Natural: ASSIGN EFSA9120.CONTRACT-NBR ( 1,1 ) := IA-FRM-CNTRCT
        pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue("7554");                                                                                                //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := '7554'
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                            //Natural: ASSIGN EFSA9120.WPID-VALIDATE-IND ( 1 ) := 'Y'
        if (condition(pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).greater("7999")))                                                                                  //Natural: IF EFSA9120.STATUS-CDE ( 1 ) > '7999'
        {
            pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue("9");                                                                                              //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) = '9'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue("0");                                                                                              //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) = '0'
        }                                                                                                                                                                 //Natural: END-IF
        pdaEfsa9120.getEfsa9120_Work_Rqst_Prty_Cde().getValue(1).setValue("2");                                                                                           //Natural: ASSIGN EFSA9120.WORK-RQST-PRTY-CDE ( 1 ) := '2'
        pdaEfsa9120.getEfsa9120_Mj_Pull_Ind().getValue(1).setValue("N");                                                                                                  //Natural: ASSIGN EFSA9120.MJ-PULL-IND ( 1 ) := 'N'
        pdaEfsa9120.getEfsa9120_Check_Ind().getValue(1).setValue("N");                                                                                                    //Natural: ASSIGN EFSA9120.CHECK-IND ( 1 ) := 'N'
        pdaEfsa9120.getEfsa9120_Gen_Fldr_Ind().getValue(1).setValue("N");                                                                                                 //Natural: ASSIGN EFSA9120.GEN-FLDR-IND ( 1 ) := 'N'
        pdaEfsa9120.getEfsa9120_Effective_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: ASSIGN EFSA9120.EFFECTIVE-DTE ( 1 ) := *DATN
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("C");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'C'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde);                                                                        //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := RQST-UNIT-CDE
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue("IAIQ");                                                                                                     //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := 'IAIQ'
        pdaEfsa9120.getEfsa9120_System().setValue("IAIQ");                                                                                                                //Natural: ASSIGN EFSA9120.SYSTEM := 'IAIQ'
        pdaEfsa9120.getEfsa9120_Batch_Nbr().setValue(0);                                                                                                                  //Natural: ASSIGN EFSA9120.BATCH-NBR := 0
        pdaEfsa9120.getEfsa9120_Id().setValue(pdaEfsa9120.getEfsa9120_Cabinet_Name());                                                                                    //Natural: ASSIGN EFSA9120-ID := EFSA9120.CABINET-NAME
        //*  11/22/2001
        pdaEfsa9120.getEfsa9120_Case_Id().getValue("*").reset();                                                                                                          //Natural: RESET EFSA9120.CASE-ID ( * ) EFSA9120.SUB-RQST-CDE ( * )
        pdaEfsa9120.getEfsa9120_Sub_Rqst_Cde().getValue("*").reset();
        //*  FOR DEBUGGING PURPOSES ONLE WHEN YOU GET A ERROR 163'
        //*  FOR DEBUGGING PURPOSES ONLE WHEN YOU GET A ERROR 163'
        if (condition(pls_Pnd_Debug_Ctr.less(5)))                                                                                                                         //Natural: IF +#DEBUG-CTR < 5
        {
            pls_Pnd_Debug_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO +#DEBUG-CTR
            getReports().write(0, "EFSA9120.ACTION ",pdaEfsa9120.getEfsa9120_Action());                                                                                   //Natural: WRITE 'EFSA9120.ACTION ' EFSA9120.ACTION
            if (Global.isEscape()) return;
            getReports().write(0, "EFSA9120.SYSTEM ",pdaEfsa9120.getEfsa9120_System());                                                                                   //Natural: WRITE 'EFSA9120.SYSTEM ' EFSA9120.SYSTEM
            if (Global.isEscape()) return;
            getReports().write(0, "EFSA9120.CASE-ID(*) ",pdaEfsa9120.getEfsa9120_Case_Id().getValue("*"));                                                                //Natural: WRITE 'EFSA9120.CASE-ID(*) 'EFSA9120.CASE-ID ( * )
            if (Global.isEscape()) return;
            getReports().write(0, "EFSA9120.SUB-RQST-CDE(*) ",pdaEfsa9120.getEfsa9120_Sub_Rqst_Cde().getValue("*"));                                                      //Natural: WRITE 'EFSA9120.SUB-RQST-CDE(*) 'EFSA9120.SUB-RQST-CDE ( * )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition((pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(getZero()) && (pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(" ")                   //Natural: IF MSG-INFO-SUB.##MSG-NR = 0 AND ( MSG-INFO-SUB.##RETURN-CODE = ' ' OR = 'W' )
            || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))))
        {
            pnd_Rtrn_Cde.setValue(" ");                                                                                                                                   //Natural: ASSIGN #RTRN-CDE := ' '
            pnd_Return_Code.setValue(" ");                                                                                                                                //Natural: ASSIGN #RETURN-CODE := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rtrn_Cde.setValue("E");                                                                                                                                   //Natural: ASSIGN #RTRN-CDE := 'E'
            pnd_Return_Code.setValue("E");                                                                                                                                //Natural: ASSIGN #RETURN-CODE := 'E'
            getReports().write(0, "MSG-INFO-SUB.##MSG-NR      = ",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr());                                                          //Natural: WRITE 'MSG-INFO-SUB.##MSG-NR      = ' MSG-INFO-SUB.##MSG-NR
            if (Global.isEscape()) return;
            getReports().write(0, "MSG-INFO-SUB.##RETURN-CODE = ",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                     //Natural: WRITE 'MSG-INFO-SUB.##RETURN-CODE = ' MSG-INFO-SUB.##RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "MSG-INFO-SUB.##MSG =",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                      //Natural: WRITE 'MSG-INFO-SUB.##MSG =' MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            pnd_Msg.setValue("Error from EFSN9120 when adding an MIT request");                                                                                           //Natural: ASSIGN #MSG := 'Error from EFSN9120 when adding an MIT request'
            getReports().write(0, "=",pnd_Msg);                                                                                                                           //Natural: WRITE '=' #MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Rc_Log() throws Exception                                                                                                                      //Natural: WRITE-RC-LOG
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "Contract",                                                                                                                               //Natural: DISPLAY ( 1 ) 'Contract' #CONTRACT 'Payee' #PAYEE 'Return Code' #RETURN-CODE
        		pnd_Payee_Key_Pnd_Contract,"Payee",
        		pnd_Payee_Key_Pnd_Payee,"Return Code",
        		pnd_Return_Code);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(20),"Reminder Letter Processing Report -",Global.getDATX(),  //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 20T 'Reminder Letter Processing Report -' *DATX ( EM = MM/DD/YYYY ) 20X 'PAGE: ' *PAGE-NUMBER ( 1 )
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(20),"PAGE: ",getReports().getPageNumberDbs(1));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");

        getReports().setDisplayColumns(1, "Contract",
        		pnd_Payee_Key_Pnd_Contract,"Payee",
        		pnd_Payee_Key_Pnd_Payee,"Return Code",
        		pnd_Return_Code);
    }
}
