/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:08 PM
**        * FROM NATURAL PROGRAM : Iaap380
************************************************************
**        * FILE NAME            : Iaap380.java
**        * CLASS NAME           : Iaap380
**        * INSTANCE NAME        : Iaap380
************************************************************
**********************************
*
* DATE WRITTEN : 04/15/96
* WRITTEN BY JEFF BERINGER
* HISTORY
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
**********************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap380 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Payee_Key;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_Contract_Number;
    private DbsField pnd_Work_Record_1_Pnd_Payee_Code;
    private DbsField pnd_Work_Record_1_Pnd_Id_Number;
    private DbsField pnd_Work_Record_1_Pnd_Ctznshp_Code;
    private DbsField pnd_Work_Record_1_Pnd_Rsdncy_Code;
    private DbsField pnd_Work_Record_1_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_Ben_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_Cntrct_First_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_Cntrct_Hold_Cde;
    private DbsField pnd_Work_Record_1_Pnd_Cntrct_State_Cde;
    private DbsField pnd_Work_Record_1_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Work_Record_1_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Work_Record_1_Pnd_Cntrct_Filler;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_1;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Date_Pnd_Date_Ccyy;
    private DbsField pnd_Good_Record;
    private DbsField pnd_Search_Key_Rcrd;

    private DbsGroup pnd_Search_Key_Rcrd__R_Field_2;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte;
    private DbsField pnd_Search_Trans_Dte_A;
    private DbsField pnd_Func;
    private DbsField pnd_Num_Read;
    private DbsField pnd_Num_Read_Accepted;
    private DbsField pnd_Num_Read_1_2;
    private DbsField pnd_Num_Read_2;
    private DbsField pnd_Cntrl_Check_Dte;

    private DbsGroup pnd_Cntrl_Check_Dte__R_Field_3;
    private DbsField pnd_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A;
    private DbsField pnd_Mode_Code;
    private DbsField pnd_Prod_Code;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Payee_Key = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Payee_Key", 
            "CNTRCT-PAYEE-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_KEY");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Payee_Key.setSuperDescriptor(true);
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        registerRecord(vw_iaa_Cntrct);

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_Contract_Number = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Contract_Number", "#CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Work_Record_1_Pnd_Payee_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Payee_Code", "#PAYEE-CODE", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_Id_Number = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Id_Number", "#ID-NUMBER", FieldType.NUMERIC, 12);
        pnd_Work_Record_1_Pnd_Ctznshp_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Ctznshp_Code", "#CTZNSHP-CODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_1_Pnd_Rsdncy_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Rsdncy_Code", "#RSDNCY-CODE", FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_Tax_Id_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Work_Record_1_Pnd_Ben_Xref_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Ben_Xref_Ind", "#BEN-XREF-IND", FieldType.STRING, 
            9);
        pnd_Work_Record_1_Pnd_Cntrct_First_Annt_Xref_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Cntrct_First_Annt_Xref_Ind", "#CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_Cntrct_Scnd_Annt_Xref_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Cntrct_Scnd_Annt_Xref_Ind", "#CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_Cntrct_Hold_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Cntrct_Hold_Cde", "#CNTRCT-HOLD-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_Cntrct_State_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Cntrct_State_Cde", "#CNTRCT-STATE-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_1_Pnd_Cntrct_Crrncy_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_1_Pnd_Cntrct_Local_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_1_Pnd_Cntrct_Filler = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Cntrct_Filler", "#CNTRCT-FILLER", FieldType.STRING, 
            6);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Date__R_Field_1", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Ccyy = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Ccyy", "#DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Good_Record = localVariables.newFieldInRecord("pnd_Good_Record", "#GOOD-RECORD", FieldType.NUMERIC, 1);
        pnd_Search_Key_Rcrd = localVariables.newFieldInRecord("pnd_Search_Key_Rcrd", "#SEARCH-KEY-RCRD", FieldType.STRING, 10);

        pnd_Search_Key_Rcrd__R_Field_2 = localVariables.newGroupInRecord("pnd_Search_Key_Rcrd__R_Field_2", "REDEFINE", pnd_Search_Key_Rcrd);
        pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde = pnd_Search_Key_Rcrd__R_Field_2.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde", "#SEARCH-CNTRL-CDE", 
            FieldType.STRING, 2);
        pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte = pnd_Search_Key_Rcrd__R_Field_2.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte", "#SEARCH-TRANS-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Search_Trans_Dte_A = localVariables.newFieldInRecord("pnd_Search_Trans_Dte_A", "#SEARCH-TRANS-DTE-A", FieldType.NUMERIC, 8);
        pnd_Func = localVariables.newFieldInRecord("pnd_Func", "#FUNC", FieldType.STRING, 2);
        pnd_Num_Read = localVariables.newFieldInRecord("pnd_Num_Read", "#NUM-READ", FieldType.NUMERIC, 11);
        pnd_Num_Read_Accepted = localVariables.newFieldInRecord("pnd_Num_Read_Accepted", "#NUM-READ-ACCEPTED", FieldType.NUMERIC, 11);
        pnd_Num_Read_1_2 = localVariables.newFieldInRecord("pnd_Num_Read_1_2", "#NUM-READ-1-2", FieldType.NUMERIC, 11);
        pnd_Num_Read_2 = localVariables.newFieldInRecord("pnd_Num_Read_2", "#NUM-READ-2", FieldType.NUMERIC, 11);
        pnd_Cntrl_Check_Dte = localVariables.newFieldInRecord("pnd_Cntrl_Check_Dte", "#CNTRL-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Cntrl_Check_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrl_Check_Dte__R_Field_3", "REDEFINE", pnd_Cntrl_Check_Dte);
        pnd_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A = pnd_Cntrl_Check_Dte__R_Field_3.newFieldInGroup("pnd_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A", "#CNTRL-CHECK-DTE-A", 
            FieldType.STRING, 8);
        pnd_Mode_Code = localVariables.newFieldArrayInRecord("pnd_Mode_Code", "#MODE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 22));
        pnd_Prod_Code = localVariables.newFieldArrayInRecord("pnd_Prod_Code", "#PROD-CODE", FieldType.STRING, 2, new DbsArrayController(1, 20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct.reset();

        localVariables.reset();
        pnd_Good_Record.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap380() throws Exception
    {
        super("Iaap380");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60;//Natural: FORMAT ( 3 ) LS = 132 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
                                                                                                                                                                          //Natural: PERFORM READ-IAA-CNTRACT-PRT
        sub_Read_Iaa_Cntract_Prt();
        if (condition(Global.isEscape())) {return;}
        getReports().skip(0, 2);                                                                                                                                          //Natural: SKIP 2
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECORDS READ ",pnd_Num_Read, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));                                            //Natural: WRITE 'TOTAL RECORDS READ ' #NUM-READ ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECORDS ACCPT",pnd_Num_Read_Accepted, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));                                   //Natural: WRITE 'TOTAL RECORDS ACCPT' #NUM-READ-ACCEPTED ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECORDS PAYEE 1 OR 2",pnd_Num_Read_1_2, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE 'TOTAL RECORDS PAYEE 1 OR 2' #NUM-READ-1-2 ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECORDS PAYEE 2     ",pnd_Num_Read_2, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));                                   //Natural: WRITE 'TOTAL RECORDS PAYEE 2     ' #NUM-READ-2 ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //*  =================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-CNTRACT-PRT
        //*  =================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-CNTRCT
        //*  =================================================================
        getReports().skip(0, 2);                                                                                                                                          //Natural: SKIP 2
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECORDS READ ",pnd_Num_Read);                                                                                   //Natural: WRITE 'TOTAL RECORDS READ ' #NUM-READ
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECORDS ACCPT",pnd_Num_Read_Accepted);                                                                          //Natural: WRITE 'TOTAL RECORDS ACCPT' #NUM-READ-ACCEPTED
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Cntract_Prt() throws Exception                                                                                                              //Natural: READ-IAA-CNTRACT-PRT
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = '          '
        (
        "READ01",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", "          ", WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ01")))
        {
            pnd_Num_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NUM-READ
            if (condition(!(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.notEquals(9))))                                                                                     //Natural: ACCEPT IF CNTRCT-ACTVTY-CDE NE 9
            {
                continue;
            }
            pnd_Num_Read_Accepted.nadd(1);                                                                                                                                //Natural: ADD 1 TO #NUM-READ-ACCEPTED
            pnd_Work_Record_1_Pnd_Contract_Number.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                 //Natural: MOVE CNTRCT-PART-PPCN-NBR TO #CONTRACT-NUMBER
            pnd_Work_Record_1_Pnd_Payee_Code.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                     //Natural: MOVE CNTRCT-PART-PAYEE-CDE TO #PAYEE-CODE
            pnd_Work_Record_1_Pnd_Id_Number.setValue(iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr);                                                                                 //Natural: MOVE CPR-ID-NBR TO #ID-NUMBER
            pnd_Work_Record_1_Pnd_Ctznshp_Code.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde);                                                                     //Natural: MOVE PRTCPNT-CTZNSHP-CDE TO #CTZNSHP-CODE
            pnd_Work_Record_1_Pnd_Rsdncy_Code.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde);                                                                       //Natural: MOVE PRTCPNT-RSDNCY-CDE TO #RSDNCY-CODE
            pnd_Work_Record_1_Pnd_Tax_Id_Nbr.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr);                                                                        //Natural: MOVE PRTCPNT-TAX-ID-NBR TO #TAX-ID-NBR
            //*  CURRENCY-CODE
            //*  PENDING CODE
            pnd_Work_Record_1_Pnd_Cntrct_Hold_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde);                                                                      //Natural: MOVE CNTRCT-HOLD-CDE TO #CNTRCT-HOLD-CDE
            pnd_Work_Record_1_Pnd_Cntrct_State_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde);                                                                    //Natural: MOVE CNTRCT-STATE-CDE TO #CNTRCT-STATE-CDE
            pnd_Work_Record_1_Pnd_Cntrct_Local_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde);                                                                    //Natural: MOVE CNTRCT-LOCAL-CDE TO #CNTRCT-LOCAL-CDE
            //*  DELETE CODE
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.greater(2)))                                                                                      //Natural: IF CNTRCT-PART-PAYEE-CDE > 2
            {
                pnd_Num_Read_2.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NUM-READ-2
                pnd_Work_Record_1_Pnd_Ben_Xref_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind);                                                                     //Natural: MOVE BNFCRY-XREF-IND TO #BEN-XREF-IND
                pnd_Work_Record_1_Pnd_Cntrct_First_Annt_Xref_Ind.setValue(" ");                                                                                           //Natural: MOVE ' ' TO #CNTRCT-FIRST-ANNT-XREF-IND
                pnd_Work_Record_1_Pnd_Cntrct_Scnd_Annt_Xref_Ind.setValue(" ");                                                                                            //Natural: MOVE ' ' TO #CNTRCT-SCND-ANNT-XREF-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Num_Read_1_2.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NUM-READ-1-2
                                                                                                                                                                          //Natural: PERFORM READ-IAA-CNTRCT
                sub_Read_Iaa_Cntrct();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Work_Record_1_Pnd_Ben_Xref_Ind.setValue(" ");                                                                                                         //Natural: MOVE ' ' TO #BEN-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Work_Record_1);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-RECORD-1
            //*  WRITE 02X #CONTRACT-NUMBER
            //*    12X #ID-NUMBER
            //*    09X #PAYEE-CODE
            //*    06X #CTZNSHP-CODE
            //*    07X #RSDNCY-CODE
            //*    06X #TAX-ID-NBR (EM=999-99-9999)
            //*    05X #BEN-XREF-IND
            //*    09X #CNTRCT-FIRST-ANNT-XREF-IND
            //*    06X #CNTRCT-SCND-ANNT-XREF-IND
            //*    02X #CNTRCT-LOCAL-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: READ-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CONTRACT-NUMBER
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Work_Record_1_Pnd_Contract_Number, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND01")))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.equals(pnd_Work_Record_1_Pnd_Contract_Number)))                                                                      //Natural: IF CNTRCT-PPCN-NBR = #CONTRACT-NUMBER
            {
                pnd_Work_Record_1_Pnd_Cntrct_First_Annt_Xref_Ind.setValue(iaa_Cntrct_Cntrct_First_Annt_Xref_Ind);                                                         //Natural: MOVE CNTRCT-FIRST-ANNT-XREF-IND TO #CNTRCT-FIRST-ANNT-XREF-IND
                pnd_Work_Record_1_Pnd_Cntrct_Scnd_Annt_Xref_Ind.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind);                                                           //Natural: MOVE CNTRCT-SCND-ANNT-XREF-IND TO #CNTRCT-SCND-ANNT-XREF-IND
                pnd_Work_Record_1_Pnd_Cntrct_Crrncy_Cde.setValue(iaa_Cntrct_Cntrct_Crrncy_Cde);                                                                           //Natural: MOVE CNTRCT-CRRNCY-CDE TO #CNTRCT-CRRNCY-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(22),"IA ADMINISTRATION MODE CONTROL RECORD CREATE    ",new  //Natural: WRITE NOTITLE NOHDR 01X 'PROGRAM ' *PROGRAM 22X 'IA ADMINISTRATION MODE CONTROL RECORD CREATE    ' 29X 'PAGE :' *PAGE-NUMBER / 04X 'RUN DATE : ' *DATU //
                        ColumnSpacing(29),"PAGE :",getReports().getPageNumberDbs(0),NEWLINE,new ColumnSpacing(4),"RUN DATE : ",Global.getDATU(),NEWLINE,
                        NEWLINE);
                    //*    02X 'CONTRACT-NUMBER'
                    //*    02X 'UNIQUE NUMBER'
                    //*    02X 'PAYEE-CODE'
                    //*    02X 'CIT-CODE'
                    //*    02X 'RES-CODE'
                    //*    02X 'SOCIAL-SECURITY'
                    //*    05X 'BEN-X-REF'
                    //*    05X '1ST-ANT-X-REF'
                    //*    02X 'SEC-ANT-X-REF' ///
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
        Global.format(3, "LS=132 PS=60");
    }
}
