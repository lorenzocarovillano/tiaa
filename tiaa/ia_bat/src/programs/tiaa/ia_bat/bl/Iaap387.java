/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:23 PM
**        * FROM NATURAL PROGRAM : Iaap387
************************************************************
**        * FILE NAME            : Iaap387.java
**        * CLASS NAME           : Iaap387
**        * INSTANCE NAME        : Iaap387
************************************************************
*********************************************************************
*
* THE FUNCTION OF THIS PROGRAM IS TO GENERATE IAA-OLD-TIAA-RATES
* AND IAA-OLD-CREF-RATES RECORDS FROM IAA-TIAA-FUND-RCRD(S) FOR ALL
* THE APPROPRIATE TIAA CONTRACTS FOR APRIL DIVIDEND CHANGE
* THIS IS DONE AT THE END OF THE MARCH CHECK CYCLE.
* THE RECORDS WILL BE WRITTEN TO A WORK FILE WHICH WILL BE READ BY
* MODULE IAAP386.
* THE PROGRAM ALSO WILL GENERATE A TOTALS REPORT SHOWING TOTAL COUNTS
* AND AMOUNTS.
*
*********************  MAINTENANCE LOG ******************************
*
* DATE   PROGRAMMER   DESCRIPTION
*                     DO SCAN 2/98
*
* 01/19/01  TCD       ADDED OPTION CODES 25,27,28,29 & 30 FOR TPA & IPRO
* 02/99     KN        REMOVED ORIGIN CHECKS
* 11/98     JFT       POPULATED SRCE-CDE, STATUS-CDE, VERIFY-ID
*                     FIELDS FOR CREF HISTORICAL RATE RECORDS.
*  3/98     RM        REMOVED FIX TO FORCE CHECK DATE
*  2/98     RM        FIX TO FORCE CHECK DATE
*                     DO SCAN ON 2/98
*
* 10/97     RM        ADDED LOGIC TO MOVE CREF PMT-AMT & UNIT VAL
*                     INTO CREF RECORD & W09 NEW FND CODE FOR REA
*                     MONTHLY UNIT VALUE CONTRACTS
*
* 03/12     JFT       RATE BASES EXPANSION - LAYOUT CHANGE DUE TO
*                     INCREASE IN OCCURRENCES TO 250 AND UNITS-CNT
*                     FIELD EXPANSION TO P6.3 FROM P4.3.
*********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap387 extends BLNatBase
{
    // Data Areas
    private LdaIaal384 ldaIaal384;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_View;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role_View;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind;

    private DataAccessProgramView vw_iaa_Cntrct_View;
    private DbsField iaa_Cntrct_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_View_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Issue_Dte;

    private DbsGroup iaa_Old_Tiaa_Rates_Rec;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Trans_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Trans_User_Area;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Trans_User_Id;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Id;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Cmpny_Fund_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Rcrd_Srce;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Rcrd_Status;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Gic;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Mode_Ind;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Old_Tiaa_Rates_Rec_Lst_Trans_Dte;

    private DbsGroup iaa_Old_Cref_Rates_Rec;
    private DbsField iaa_Old_Cref_Rates_Rec_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_Rec_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_Rec_Trans_Dte;
    private DbsField iaa_Old_Cref_Rates_Rec_Trans_User_Area;
    private DbsField iaa_Old_Cref_Rates_Rec_Trans_User_Id;
    private DbsField iaa_Old_Cref_Rates_Rec_Trans_Verify_Id;
    private DbsField iaa_Old_Cref_Rates_Rec_Trans_Verify_Dte;
    private DbsField iaa_Old_Cref_Rates_Rec_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Cref_Rates_Rec_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Cref_Rates_Rec_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Cref_Rates_Rec_Cmpny_Fund_Cde;
    private DbsField iaa_Old_Cref_Rates_Rec_Rcrd_Srce;
    private DbsField iaa_Old_Cref_Rates_Rec_Rcrd_Status;
    private DbsField iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val;

    private DbsGroup iaa_Old_Cref_Rates_Rec__R_Field_1;
    private DbsField iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val_Redf;
    private DbsField iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Units;
    private DbsField iaa_Old_Cref_Rates_Rec_Cref_Rate_Cde;
    private DbsField iaa_Old_Cref_Rates_Rec_Cref_Rate_Dte;
    private DbsField iaa_Old_Cref_Rates_Rec_Cref_Per_Pay_Amt;
    private DbsField iaa_Old_Cref_Rates_Rec_Cref_No_Units;
    private DbsField iaa_Old_Cref_Rates_Rec_Cref_Mode_Ind;
    private DbsField iaa_Old_Cref_Rates_Rec_Cref_Old_Cmpny_Fund;
    private DbsField iaa_Old_Cref_Rates_Rec_Lst_Trans_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_2;

    private DbsGroup pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Check_Dte;

    private DbsGroup pnd_Check_Dte__R_Field_3;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_N;

    private DbsGroup pnd_Check_Dte__R_Field_4;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_N_Ccyymm;
    private DbsField pnd_Check_Dte__Filler1;
    private DbsField pnd_Prior_Year_End;

    private DbsGroup pnd_Prior_Year_End__R_Field_5;
    private DbsField pnd_Prior_Year_End_Pnd_Prior_Year_End_Ccyy;
    private DbsField pnd_Prior_Year_End_Pnd_Prior_Year_End_Mm;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Written;
    private DbsField pnd_Total_Written_Cref;
    private DbsField pnd_Total_Bypassed_Active;
    private DbsField pnd_Total_Active;
    private DbsField pnd_Total_Active_Per_Amt;
    private DbsField pnd_Total_Active_Div_Amt;
    private DbsField pnd_Total_Bypassed_Active_Per_Amt;
    private DbsField pnd_Total_Bypassed_Active_Div_Amt;
    private DbsField pnd_Count;
    private DbsField pnd_I;
    private DbsField pnd_Time;
    private DbsField pnd_Fund_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Tiaa_Record_Written;
    private DbsField pnd_Valid_Option_Codes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal384 = new LdaIaal384();
        registerRecord(ldaIaal384);
        registerRecord(ldaIaal384.getVw_iaa_Tiaa_Fund_Rcrd_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_View", "IAA-CNTRL-RCRD-VIEW"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_View);

        vw_iaa_Cntrct_Prtcpnt_Role_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role_View", "IAA-CNTRCT-PRTCPNT-ROLE-VIEW"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind", 
            "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role_View);

        vw_iaa_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_View", "IAA-CNTRCT-VIEW"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_View_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_View_Cntrct_Optn_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_View_Cntrct_Orgn_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_View_Cntrct_Issue_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        registerRecord(vw_iaa_Cntrct_View);

        iaa_Old_Tiaa_Rates_Rec = localVariables.newGroupInRecord("iaa_Old_Tiaa_Rates_Rec", "IAA-OLD-TIAA-RATES-REC");
        iaa_Old_Tiaa_Rates_Rec_Fund_Lst_Pd_Dte = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", FieldType.DATE);
        iaa_Old_Tiaa_Rates_Rec_Fund_Invrse_Lst_Pd_Dte = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8);
        iaa_Old_Tiaa_Rates_Rec_Trans_Dte = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Trans_Dte", "TRANS-DTE", FieldType.TIME);
        iaa_Old_Tiaa_Rates_Rec_Trans_User_Area = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6);
        iaa_Old_Tiaa_Rates_Rec_Trans_User_Id = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8);
        iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Id = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8);
        iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Dte = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Dte", "TRANS-VERIFY-DTE", 
            FieldType.TIME);
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Ppcn_Nbr = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Payee_Cde = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Mode_Ind = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        iaa_Old_Tiaa_Rates_Rec_Cmpny_Fund_Cde = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3);
        iaa_Old_Tiaa_Rates_Rec_Rcrd_Srce = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 2);
        iaa_Old_Tiaa_Rates_Rec_Rcrd_Status = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 
            1);
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Per_Amt = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Div_Amt = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Div_Amt", "CNTRCT-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Cde = iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Cde", "TIAA-RATE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 250));
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Dte = iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Dte", "TIAA-RATE-DTE", FieldType.DATE, 
            new DbsArrayController(1, 250));
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Pay_Amt = iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Div_Amt = iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Pay_Amt = iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Div_Amt = iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Gic = iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Gic", "TIAA-RATE-GIC", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 250));
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Mode_Ind = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3);
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Old_Cmpny_Fund = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3);
        iaa_Old_Tiaa_Rates_Rec_Lst_Trans_Dte = iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Old_Tiaa_Rates_Rec_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME);

        iaa_Old_Cref_Rates_Rec = localVariables.newGroupInRecord("iaa_Old_Cref_Rates_Rec", "IAA-OLD-CREF-RATES-REC");
        iaa_Old_Cref_Rates_Rec_Fund_Lst_Pd_Dte = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", FieldType.DATE);
        iaa_Old_Cref_Rates_Rec_Fund_Invrse_Lst_Pd_Dte = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8);
        iaa_Old_Cref_Rates_Rec_Trans_Dte = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Trans_Dte", "TRANS-DTE", FieldType.TIME);
        iaa_Old_Cref_Rates_Rec_Trans_User_Area = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6);
        iaa_Old_Cref_Rates_Rec_Trans_User_Id = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8);
        iaa_Old_Cref_Rates_Rec_Trans_Verify_Id = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8);
        iaa_Old_Cref_Rates_Rec_Trans_Verify_Dte = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Trans_Verify_Dte", "TRANS-VERIFY-DTE", 
            FieldType.TIME);
        iaa_Old_Cref_Rates_Rec_Cntrct_Ppcn_Nbr = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        iaa_Old_Cref_Rates_Rec_Cntrct_Payee_Cde = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        iaa_Old_Cref_Rates_Rec_Cntrct_Mode_Ind = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        iaa_Old_Cref_Rates_Rec_Cmpny_Fund_Cde = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3);
        iaa_Old_Cref_Rates_Rec_Rcrd_Srce = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 2);
        iaa_Old_Cref_Rates_Rec_Rcrd_Status = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 
            1);
        iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Per_Amt = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val", "CNTRCT-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4);

        iaa_Old_Cref_Rates_Rec__R_Field_1 = iaa_Old_Cref_Rates_Rec.newGroupInGroup("iaa_Old_Cref_Rates_Rec__R_Field_1", "REDEFINE", iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val);
        iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val_Redf = iaa_Old_Cref_Rates_Rec__R_Field_1.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val_Redf", 
            "CNTRCT-UNIT-VAL-REDF", FieldType.PACKED_DECIMAL, 9, 2);
        iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Units = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Units", "CNTRCT-TOT-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3);
        iaa_Old_Cref_Rates_Rec_Cref_Rate_Cde = iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Old_Cref_Rates_Rec_Cref_Rate_Cde", "CREF-RATE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 15));
        iaa_Old_Cref_Rates_Rec_Cref_Rate_Dte = iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Old_Cref_Rates_Rec_Cref_Rate_Dte", "CREF-RATE-DTE", FieldType.DATE, 
            new DbsArrayController(1, 15));
        iaa_Old_Cref_Rates_Rec_Cref_Per_Pay_Amt = iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Old_Cref_Rates_Rec_Cref_Per_Pay_Amt", "CREF-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 15));
        iaa_Old_Cref_Rates_Rec_Cref_No_Units = iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Old_Cref_Rates_Rec_Cref_No_Units", "CREF-NO-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 15));
        iaa_Old_Cref_Rates_Rec_Cref_Mode_Ind = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cref_Mode_Ind", "CREF-MODE-IND", FieldType.NUMERIC, 
            3);
        iaa_Old_Cref_Rates_Rec_Cref_Old_Cmpny_Fund = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Cref_Old_Cmpny_Fund", "CREF-OLD-CMPNY-FUND", 
            FieldType.STRING, 3);
        iaa_Old_Cref_Rates_Rec_Lst_Trans_Dte = iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Old_Cref_Rates_Rec_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Payee_Key);

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R = pnd_Cntrct_Payee_Key__R_Field_2.newGroupInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R", "#CNTRCT-PAYEE-KEY-R");
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 8);

        pnd_Check_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Check_Dte__R_Field_3", "REDEFINE", pnd_Check_Dte);
        pnd_Check_Dte_Pnd_Check_Dte_N = pnd_Check_Dte__R_Field_3.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_N", "#CHECK-DTE-N", FieldType.NUMERIC, 8);

        pnd_Check_Dte__R_Field_4 = pnd_Check_Dte__R_Field_3.newGroupInGroup("pnd_Check_Dte__R_Field_4", "REDEFINE", pnd_Check_Dte_Pnd_Check_Dte_N);
        pnd_Check_Dte_Pnd_Check_Dte_N_Ccyymm = pnd_Check_Dte__R_Field_4.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_N_Ccyymm", "#CHECK-DTE-N-CCYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Check_Dte__Filler1 = pnd_Check_Dte__R_Field_4.newFieldInGroup("pnd_Check_Dte__Filler1", "_FILLER1", FieldType.STRING, 2);
        pnd_Prior_Year_End = localVariables.newFieldInRecord("pnd_Prior_Year_End", "#PRIOR-YEAR-END", FieldType.NUMERIC, 6);

        pnd_Prior_Year_End__R_Field_5 = localVariables.newGroupInRecord("pnd_Prior_Year_End__R_Field_5", "REDEFINE", pnd_Prior_Year_End);
        pnd_Prior_Year_End_Pnd_Prior_Year_End_Ccyy = pnd_Prior_Year_End__R_Field_5.newFieldInGroup("pnd_Prior_Year_End_Pnd_Prior_Year_End_Ccyy", "#PRIOR-YEAR-END-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Prior_Year_End_Pnd_Prior_Year_End_Mm = pnd_Prior_Year_End__R_Field_5.newFieldInGroup("pnd_Prior_Year_End_Pnd_Prior_Year_End_Mm", "#PRIOR-YEAR-END-MM", 
            FieldType.NUMERIC, 2);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.INTEGER, 4);
        pnd_Total_Written = localVariables.newFieldInRecord("pnd_Total_Written", "#TOTAL-WRITTEN", FieldType.INTEGER, 4);
        pnd_Total_Written_Cref = localVariables.newFieldInRecord("pnd_Total_Written_Cref", "#TOTAL-WRITTEN-CREF", FieldType.INTEGER, 4);
        pnd_Total_Bypassed_Active = localVariables.newFieldInRecord("pnd_Total_Bypassed_Active", "#TOTAL-BYPASSED-ACTIVE", FieldType.INTEGER, 4);
        pnd_Total_Active = localVariables.newFieldInRecord("pnd_Total_Active", "#TOTAL-ACTIVE", FieldType.INTEGER, 4);
        pnd_Total_Active_Per_Amt = localVariables.newFieldInRecord("pnd_Total_Active_Per_Amt", "#TOTAL-ACTIVE-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Active_Div_Amt = localVariables.newFieldInRecord("pnd_Total_Active_Div_Amt", "#TOTAL-ACTIVE-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Bypassed_Active_Per_Amt = localVariables.newFieldInRecord("pnd_Total_Bypassed_Active_Per_Amt", "#TOTAL-BYPASSED-ACTIVE-PER-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_Bypassed_Active_Div_Amt = localVariables.newFieldInRecord("pnd_Total_Bypassed_Active_Div_Amt", "#TOTAL-BYPASSED-ACTIVE-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.INTEGER, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Fund_Invrse_Lst_Pd_Dte = localVariables.newFieldInRecord("pnd_Fund_Invrse_Lst_Pd_Dte", "#FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Tiaa_Record_Written = localVariables.newFieldInRecord("pnd_Tiaa_Record_Written", "#TIAA-RECORD-WRITTEN", FieldType.BOOLEAN, 1);
        pnd_Valid_Option_Codes = localVariables.newFieldArrayInRecord("pnd_Valid_Option_Codes", "#VALID-OPTION-CODES", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            10));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_View.reset();
        vw_iaa_Cntrct_Prtcpnt_Role_View.reset();
        vw_iaa_Cntrct_View.reset();

        ldaIaal384.initializeValues();

        localVariables.reset();
        pnd_Cntrct_Payee_Key.setInitialValue(" ");
        pnd_Tiaa_Record_Written.setInitialValue(false);
        pnd_Valid_Option_Codes.getValue(1).setInitialValue(21);
        pnd_Valid_Option_Codes.getValue(2).setInitialValue(25);
        pnd_Valid_Option_Codes.getValue(3).setInitialValue(27);
        pnd_Valid_Option_Codes.getValue(4).setInitialValue(28);
        pnd_Valid_Option_Codes.getValue(5).setInitialValue(29);
        pnd_Valid_Option_Codes.getValue(6).setInitialValue(30);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap387() throws Exception
    {
        super("Iaap387");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132
        pnd_Time.setValue(Global.getTIMX());                                                                                                                              //Natural: MOVE *TIMX TO #TIME
        vw_iaa_Cntrl_Rcrd_View.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-CNTRL-RCRD-VIEW WITH CNTRL-RCRD-KEY = 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_View.readNextRow("READ01")))
        {
            pnd_Check_Dte.setValueEdited(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DTE
            //*  FOLLOWING FOR FIX ONLY REMOVE AFTER RUN FEB 1998 2/98
            //*  #CHECK-DTE-N := 20120301
            //*  MOVE EDITED '20120301' TO CNTRL-CHECK-DTE (EM=YYYYMMDD)
            //*  END OF REMOVE   2/98
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Check_Dte,5,2), new ExamineSearch("03"), new ExamineGivingNumber(pnd_Count));                                               //Natural: EXAMINE SUBSTR ( #CHECK-DTE,5,2 ) FOR '03' GIVING NUMBER #COUNT
        if (condition(pnd_Count.equals(getZero())))                                                                                                                       //Natural: IF #COUNT = 0
        {
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,"***********************************",NEWLINE,"LATEST CHECK DATE ON CONTROL RECORD",            //Natural: WRITE NOTITLE NOHDR'***********************************' / 'LATEST CHECK DATE ON CONTROL RECORD' / 'IS NOT IN MARCH   ' / 'THIS JOB WILL RUN ONLY AT THE END' / 'OF THE MARCH CHECK CYCLE   ' / '***********************************' /
                NEWLINE,"IS NOT IN MARCH   ",NEWLINE,"THIS JOB WILL RUN ONLY AT THE END",NEWLINE,"OF THE MARCH CHECK CYCLE   ",NEWLINE,"***********************************",
                NEWLINE);
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prior_Year_End.setValue(pnd_Check_Dte_Pnd_Check_Dte_N_Ccyymm);                                                                                                //Natural: ASSIGN #PRIOR-YEAR-END = #CHECK-DTE-N-CCYYMM
        pnd_Prior_Year_End_Pnd_Prior_Year_End_Ccyy.nsubtract(1);                                                                                                          //Natural: ASSIGN #PRIOR-YEAR-END-CCYY = #PRIOR-YEAR-END-CCYY - 1
        pnd_Fund_Invrse_Lst_Pd_Dte.compute(new ComputeParameters(false, pnd_Fund_Invrse_Lst_Pd_Dte), DbsField.subtract(100000000,pnd_Check_Dte_Pnd_Check_Dte_N));         //Natural: ASSIGN #FUND-INVRSE-LST-PD-DTE = 100000000 - #CHECK-DTE-N
        ldaIaal384.getVw_iaa_Tiaa_Fund_Rcrd_View().startDatabaseRead                                                                                                      //Natural: READ IAA-TIAA-FUND-RCRD-VIEW LOGICAL BY TIAA-CNTRCT-FUND-KEY
        (
        "READ02",
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ02:
        while (condition(ldaIaal384.getVw_iaa_Tiaa_Fund_Rcrd_View().readNextRow("READ02")))
        {
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            if (condition(!(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde().equals("T") || ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde().equals("U")      //Natural: ACCEPT IF TIAA-CMPNY-CDE = 'T' OR = 'U' OR = 'W'
                || ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde().equals("W"))))
            {
                continue;
            }
            //*  ADDED W09 10/97
            if (condition(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde().equals("U") || ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde().equals("W")))      //Natural: IF TIAA-CMPNY-CDE = 'U' OR = 'W'
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-WRITE-CREF-RECS
                sub_Check_Write_Cref_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Record_Written.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #TIAA-RECORD-WRITTEN
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr());                                         //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR = TIAA-CNTRCT-PPCN-NBR
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde());                                       //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE = TIAA-CNTRCT-PAYEE-CDE
            vw_iaa_Cntrct_Prtcpnt_Role_View.startDatabaseFind                                                                                                             //Natural: FIND IAA-CNTRCT-PRTCPNT-ROLE-VIEW WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_iaa_Cntrct_Prtcpnt_Role_View.readNextRow("FIND01", true)))
            {
                vw_iaa_Cntrct_Prtcpnt_Role_View.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrct_Prtcpnt_Role_View.getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, ReportOption.NOTITLE,"**********************************************",NEWLINE,"NO CNTRCT PRTCPNT RECORD FOUND FOR FUND RECORD", //Natural: WRITE '**********************************************' / 'NO CNTRCT PRTCPNT RECORD FOUND FOR FUND RECORD' / 'PPCN NUMBER = ' TIAA-CNTRCT-PPCN-NBR / 'PAYEE CODE  = ' TIAA-CNTRCT-PAYEE-CDE / '**********************************************' /
                        NEWLINE,"PPCN NUMBER = ",ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr(),NEWLINE,"PAYEE CODE  = ",ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde(),
                        NEWLINE,"**********************************************",NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde.equals(9) && iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Lst_Chnge_Dte.lessOrEqual(pnd_Prior_Year_End))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-ACTVTY-CDE = 9 AND IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-LST-CHNGE-DTE LE #PRIOR-YEAR-END
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            vw_iaa_Cntrct_View.startDatabaseFind                                                                                                                          //Natural: FIND IAA-CNTRCT-VIEW WITH CNTRCT-PPCN-NBR = #CNTRCT-PART-PPCN-NBR
            (
            "FIND02",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_iaa_Cntrct_View.readNextRow("FIND02", true)))
            {
                vw_iaa_Cntrct_View.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrct_View.getAstCOUNTER().equals(0)))                                                                                              //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, ReportOption.NOTITLE,"**************************************",NEWLINE,"NO CNTRCT RECORD FOUND FOR FUND RECORD",                 //Natural: WRITE '**************************************' / 'NO CNTRCT RECORD FOUND FOR FUND RECORD' / 'PPCN NUMBER = ' TIAA-CNTRCT-PPCN-NBR / 'PAYEE CODE  = ' TIAA-CNTRCT-PAYEE-CDE / '**************************************' /
                        NEWLINE,"PPCN NUMBER = ",ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr(),NEWLINE,"PAYEE CODE  = ",ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde(),
                        NEWLINE,"**************************************",NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  01/2001
            if (condition((! (iaa_Cntrct_View_Cntrct_Optn_Cde.equals(pnd_Valid_Option_Codes.getValue("*"))) || (iaa_Cntrct_View_Cntrct_Optn_Cde.equals(21)                //Natural: IF NOT ( IAA-CNTRCT-VIEW.CNTRCT-OPTN-CDE EQ #VALID-OPTION-CODES ( * ) ) OR ( IAA-CNTRCT-VIEW.CNTRCT-OPTN-CDE = 21 AND IAA-CNTRCT-VIEW.CNTRCT-ISSUE-DTE LT 199104 )
                && iaa_Cntrct_View_Cntrct_Issue_Dte.less(199104)))))
            {
                //*      OR                                        /* ADDED 1/97
                //*      (IAA-CNTRCT-VIEW.CNTRCT-ORGN-CDE = 3 AND
                //*      IAA-CNTRCT-VIEW.CNTRCT-ISSUE-DTE  GT 199312)
                //*      OR
                //*      (IAA-CNTRCT-VIEW.CNTRCT-ORGN-CDE = 17 OR = 18) /* END OF ADD 1/97
                if (condition(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde.notEquals(9)))                                                                               //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-ACTVTY-CDE NE 9
                {
                    pnd_Total_Bypassed_Active.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL-BYPASSED-ACTIVE
                    pnd_Total_Bypassed_Active_Per_Amt.nadd(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt());                                                     //Natural: ASSIGN #TOTAL-BYPASSED-ACTIVE-PER-AMT = #TOTAL-BYPASSED-ACTIVE-PER-AMT + IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-PER-AMT
                    pnd_Total_Bypassed_Active_Div_Amt.nadd(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt());                                                     //Natural: ASSIGN #TOTAL-BYPASSED-ACTIVE-DIV-AMT = #TOTAL-BYPASSED-ACTIVE-DIV-AMT + IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-DIV-AMT
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde.notEquals(9)))                                                                                   //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-ACTVTY-CDE NE 9
            {
                pnd_Total_Active.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-ACTIVE
                pnd_Total_Active_Per_Amt.nadd(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt());                                                                  //Natural: ASSIGN #TOTAL-ACTIVE-PER-AMT = #TOTAL-ACTIVE-PER-AMT + IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-PER-AMT
                pnd_Total_Active_Div_Amt.nadd(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt());                                                                  //Natural: ASSIGN #TOTAL-ACTIVE-DIV-AMT = #TOTAL-ACTIVE-DIV-AMT + IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-IAA-OLD-TIAA-RATES-RECS
            sub_Write_Iaa_Old_Tiaa_Rates_Recs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Tiaa_Record_Written.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #TIAA-RECORD-WRITTEN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* .01D
        //* .01
        getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(60),"IAA SYSTEM",new TabSetting(114),"DATE:", Color.blue,                //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 060T 'IAA SYSTEM' 114T 'DATE:' ( BLI ) 120T *DATX ( AD = OD EM = MM/DD/YYYY CD = NE ) / 057T 'GRAND TOTAL REPORT' ( BLI ) 114T 'TIME:' ( BLI ) 120T *TIMX ( AD = OD EM = HH.MM.SS CD = NE ) / 114T 'PAGE:' ( BLI ) 120T *PAGE-NUMBER ( 1 ) ( AD = OD CD = NE ZP = ON ) / / / / / 001T 'TOTAL READ RECORDS' ( BLI ) 033T ':' ( BLI ) 038T #TOTAL-READ ( AD = ODL EM = Z,ZZZ,ZZZ,ZZ9 CD = NE ZP = ON ) / / 001T 'TOTAL BYPASSED ACTIVE RECORDS' ( BLI ) 033T ':' ( BLI ) 038T #TOTAL-BYPASSED-ACTIVE ( AD = ODL EM = Z,ZZZ,ZZZ,ZZ9 CD = NE ZP = ON ) / / 001T 'TOTAL BYPASSED ACTIVE PER AMOUNT:' ( BLI ) 036T #TOTAL-BYPASSED-ACTIVE-PER-AMT ( AD = ODL EM = ' '$ZZ,ZZZ,ZZZ,ZZ9.99 CD = NE ZP = ON ) / / 001T 'TOTAL BYPASSED ACTIVE DIV AMOUNT:' ( BLI ) 036T #TOTAL-BYPASSED-ACTIVE-DIV-AMT ( AD = ODL EM = ' '$ZZ,ZZZ,ZZZ,ZZ9.99 CD = NE ZP = ON ) / / 001T 'TOTAL ACTIVE RECORDS' ( BLI ) 033T ':' ( BLI ) 038T #TOTAL-ACTIVE ( AD = ODL EM = Z,ZZZ,ZZZ,ZZ9 CD = NE ZP = ON ) / / 001T 'TOTAL ACTIVE PER AMOUNT' ( BLI ) 033T ':' ( BLI ) 036T #TOTAL-ACTIVE-PER-AMT ( AD = ODL EM = ' '$ZZ,ZZZ,ZZZ,ZZ9.99 CD = NE ZP = ON ) / / 001T 'TOTAL ACTIVE DIV AMOUNT' ( BLI ) 033T ':' ( BLI ) 36T #TOTAL-ACTIVE-DIV-AMT ( AD = ODL EM = ' '$ZZ,ZZZ,ZZZ,ZZ9.99 CD = NE ZP = ON ) / / 001T 'TOTAL WRITTEN RECORDS:' ( BLI ) 033T ':' ( BLI ) 038T #TOTAL-WRITTEN ( AD = ODL EM = Z,ZZZ,ZZZ,ZZ9 CD = NE ZP = ON ) / / 001T 'TOTAL WRITTEN CREF RECORDS' ( BLI ) 033T ':' ( BLI ) 038T #TOTAL-WRITTEN-CREF ( AD = ODL EM = Z,ZZZ,ZZZ,ZZ9 CD = NE ZP = ON )
            new FieldAttributes ("AD=I"),new TabSetting(120),Global.getDATX(), new FieldAttributes ("AD=OD"), new ReportEditMask ("MM/DD/YYYY"), Color.white,NEWLINE,new 
            TabSetting(57),"GRAND TOTAL REPORT", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(114),"TIME:", Color.blue, new FieldAttributes ("AD=I"),new 
            TabSetting(120),Global.getTIMX(), new FieldAttributes ("AD=OD"), new ReportEditMask ("HH.MM.SS"), Color.white,NEWLINE,new TabSetting(114),"PAGE:", 
            Color.blue, new FieldAttributes ("AD=I"),new TabSetting(120),getReports().getPageNumberDbs(1), new FieldAttributes ("AD=OD"), Color.white, new 
            ReportZeroPrint (true),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"TOTAL READ RECORDS", Color.blue, new FieldAttributes ("AD=I"),new 
            TabSetting(33),":", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(38),pnd_Total_Read, new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL BYPASSED ACTIVE RECORDS", Color.blue, new 
            FieldAttributes ("AD=I"),new TabSetting(33),":", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(38),pnd_Total_Bypassed_Active, new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL BYPASSED ACTIVE PER AMOUNT:", 
            Color.blue, new FieldAttributes ("AD=I"),new TabSetting(36),pnd_Total_Bypassed_Active_Per_Amt, new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("' '$ZZ,ZZZ,ZZZ,ZZ9.99"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL BYPASSED ACTIVE DIV AMOUNT:", Color.blue, 
            new FieldAttributes ("AD=I"),new TabSetting(36),pnd_Total_Bypassed_Active_Div_Amt, new FieldAttributes ("AD=ODL"), new ReportEditMask ("' '$ZZ,ZZZ,ZZZ,ZZ9.99"), 
            Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL ACTIVE RECORDS", Color.blue, new FieldAttributes ("AD=I"),new 
            TabSetting(33),":", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(38),pnd_Total_Active, new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL ACTIVE PER AMOUNT", Color.blue, new FieldAttributes 
            ("AD=I"),new TabSetting(33),":", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(36),pnd_Total_Active_Per_Amt, new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("' '$ZZ,ZZZ,ZZZ,ZZ9.99"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL ACTIVE DIV AMOUNT", 
            Color.blue, new FieldAttributes ("AD=I"),new TabSetting(33),":", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(36),pnd_Total_Active_Div_Amt, 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("' '$ZZ,ZZZ,ZZZ,ZZ9.99"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL WRITTEN RECORDS:", 
            Color.blue, new FieldAttributes ("AD=I"),new TabSetting(33),":", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(38),pnd_Total_Written, 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(1),"TOTAL WRITTEN CREF RECORDS", 
            Color.blue, new FieldAttributes ("AD=I"),new TabSetting(33),":", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(38),pnd_Total_Written_Cref, 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"), Color.white, new ReportZeroPrint (true));
        if (Global.isEscape()) return;
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IAA-OLD-TIAA-RATES-RECS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-WRITE-CREF-RECS
        //*  ADDED FOLLOWING  10/97
        //*  END OF ADD 10/97
        //*  ADDED BY JFT - SEE COMMENTS ON TOP : 11/98
        //*  END OF JFT CODE                      11/98
    }
    private void sub_Write_Iaa_Old_Tiaa_Rates_Recs() throws Exception                                                                                                     //Natural: WRITE-IAA-OLD-TIAA-RATES-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        iaa_Old_Tiaa_Rates_Rec_Fund_Lst_Pd_Dte.setValue(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte);                                                                             //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.FUND-LST-PD-DTE = CNTRL-CHECK-DTE
        iaa_Old_Tiaa_Rates_Rec_Fund_Invrse_Lst_Pd_Dte.setValue(pnd_Fund_Invrse_Lst_Pd_Dte);                                                                               //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.FUND-INVRSE-LST-PD-DTE = #FUND-INVRSE-LST-PD-DTE
        iaa_Old_Tiaa_Rates_Rec_Trans_Dte.setValue(pnd_Time);                                                                                                              //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TRANS-DTE = #TIME
        iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Id.setValue("REVAL");                                                                                                         //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TRANS-VERIFY-ID = 'REVAL'
        iaa_Old_Tiaa_Rates_Rec_Trans_Verify_Dte.setValue(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte);                                                                            //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TRANS-VERIFY-DTE = CNTRL-CHECK-DTE
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Ppcn_Nbr.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr());                                                    //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.CNTRCT-PPCN-NBR = IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PPCN-NBR
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Payee_Cde.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde());                                                  //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.CNTRCT-PAYEE-CDE = IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PAYEE-CDE
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Mode_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind);                                                                    //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.CNTRCT-MODE-IND = IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-MODE-IND
        iaa_Old_Tiaa_Rates_Rec_Cmpny_Fund_Cde.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde());                                                      //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.CMPNY-FUND-CDE = IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE
        iaa_Old_Tiaa_Rates_Rec_Rcrd_Srce.setValue("RE");                                                                                                                  //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.RCRD-SRCE = 'RE'
        iaa_Old_Tiaa_Rates_Rec_Rcrd_Status.setValue("A");                                                                                                                 //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.RCRD-STATUS = 'A'
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Per_Amt.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt());                                                     //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.CNTRCT-TOT-PER-AMT = IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-PER-AMT
        iaa_Old_Tiaa_Rates_Rec_Cntrct_Tot_Div_Amt.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt());                                                     //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.CNTRCT-TOT-DIV-AMT = IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-DIV-AMT
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Cde.getValue("*").setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"));                                 //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-RATE-CDE ( * ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * )
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Dte.getValue("*").setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte().getValue("*"));                                 //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-RATE-DTE ( * ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-DTE ( * )
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Pay_Amt.getValue("*").setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue("*"));                           //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-PER-PAY-AMT ( * ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( * )
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Per_Div_Amt.getValue("*").setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue("*"));                           //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-PER-DIV-AMT ( * ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( * )
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Pay_Amt.getValue("*").setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt().getValue("*"));             //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-RATE-FINAL-PAY-AMT ( * ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-FINAL-PAY-AMT ( * )
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Final_Div_Amt.getValue("*").setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt().getValue("*"));             //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-RATE-FINAL-DIV-AMT ( * ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-FINAL-DIV-AMT ( * )
        //*  3/12
        //*  3/12
        //*  3/12
        //*  3/12
        //*  3/12
        //*  3/12
        iaa_Old_Tiaa_Rates_Rec_Lst_Trans_Dte.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte());                                                             //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.LST-TRANS-DTE = IAA-TIAA-FUND-RCRD-VIEW.LST-TRANS-DTE
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Rate_Gic.getValue("*").setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic().getValue("*"));                                 //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-RATE-GIC ( * ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-GIC ( * )
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Mode_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind);                                                                      //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-MODE-IND := IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-MODE-IND
        iaa_Old_Tiaa_Rates_Rec_Tiaa_Old_Cmpny_Fund.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund());                                                 //Natural: ASSIGN IAA-OLD-TIAA-RATES-REC.TIAA-OLD-CMPNY-FUND := IAA-TIAA-FUND-RCRD-VIEW.TIAA-OLD-CMPNY-FUND
        getWorkFiles().write(1, true, iaa_Old_Tiaa_Rates_Rec);                                                                                                            //Natural: WRITE WORK FILE 1 VARIABLE IAA-OLD-TIAA-RATES-REC
        pnd_Total_Written.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOTAL-WRITTEN
    }
    private void sub_Check_Write_Cref_Recs() throws Exception                                                                                                             //Natural: CHECK-WRITE-CREF-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition((ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr)) && (ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde)))) //Natural: IF ( IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PPCN-NBR = #CNTRCT-PART-PPCN-NBR ) AND ( IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PAYEE-CDE = #CNTRCT-PART-PAYEE-CDE )
        {
            if (condition(pnd_Tiaa_Record_Written.getBoolean()))                                                                                                          //Natural: IF #TIAA-RECORD-WRITTEN
            {
                iaa_Old_Cref_Rates_Rec_Fund_Lst_Pd_Dte.setValue(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte);                                                                     //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.FUND-LST-PD-DTE = CNTRL-CHECK-DTE
                iaa_Old_Cref_Rates_Rec_Fund_Invrse_Lst_Pd_Dte.setValue(pnd_Fund_Invrse_Lst_Pd_Dte);                                                                       //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.FUND-INVRSE-LST-PD-DTE = #FUND-INVRSE-LST-PD-DTE
                iaa_Old_Cref_Rates_Rec_Trans_Dte.setValue(pnd_Time);                                                                                                      //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.TRANS-DTE = #TIME
                iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Units.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt().getValue(1));                                     //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CNTRCT-TOT-UNITS = IAA-TIAA-FUND-RCRD-VIEW.TIAA-UNITS-CNT ( 1 )
                iaa_Old_Cref_Rates_Rec_Cref_Rate_Cde.getValue(1).setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue(1));                             //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CREF-RATE-CDE ( 1 ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( 1 )
                iaa_Old_Cref_Rates_Rec_Cref_Rate_Dte.getValue(1).setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte().getValue(1));                             //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CREF-RATE-DTE ( 1 ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-DTE ( 1 )
                iaa_Old_Cref_Rates_Rec_Cref_No_Units.getValue(1).setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt().getValue(1));                            //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CREF-NO-UNITS ( 1 ) = IAA-TIAA-FUND-RCRD-VIEW.TIAA-UNITS-CNT ( 1 )
                iaa_Old_Cref_Rates_Rec_Cntrct_Ppcn_Nbr.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr());                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CNTRCT-PPCN-NBR = IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PPCN-NBR
                iaa_Old_Cref_Rates_Rec_Cntrct_Payee_Cde.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde());                                          //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CNTRCT-PAYEE-CDE = IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PAYEE-CDE
                //*  3/12
                //*  3/12
                //*  3/12
                //*  3/12
                iaa_Old_Cref_Rates_Rec_Cmpny_Fund_Cde.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde());                                              //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CMPNY-FUND-CDE = IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE
                iaa_Old_Cref_Rates_Rec_Cntrct_Tot_Per_Amt.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt());                                             //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CNTRCT-TOT-PER-AMT := IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-PER-AMT
                iaa_Old_Cref_Rates_Rec_Cntrct_Unit_Val_Redf.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt());                                           //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CNTRCT-UNIT-VAL-REDF := IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-DIV-AMT
                iaa_Old_Cref_Rates_Rec_Trans_Verify_Id.setValue("REVAL");                                                                                                 //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.TRANS-VERIFY-ID := 'REVAL'
                iaa_Old_Cref_Rates_Rec_Rcrd_Srce.setValue("RE");                                                                                                          //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.RCRD-SRCE := 'RE'
                iaa_Old_Cref_Rates_Rec_Rcrd_Status.setValue("A");                                                                                                         //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.RCRD-STATUS := 'A'
                iaa_Old_Cref_Rates_Rec_Cntrct_Mode_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind);                                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CNTRCT-MODE-IND := IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-MODE-IND
                iaa_Old_Cref_Rates_Rec_Trans_Verify_Dte.setValue(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte);                                                                    //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.TRANS-VERIFY-DTE := CNTRL-CHECK-DTE
                iaa_Old_Cref_Rates_Rec_Lst_Trans_Dte.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte());                                                     //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.LST-TRANS-DTE := IAA-TIAA-FUND-RCRD-VIEW.LST-TRANS-DTE
                iaa_Old_Cref_Rates_Rec_Cref_Mode_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind);                                                              //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CREF-MODE-IND := IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-MODE-IND
                iaa_Old_Cref_Rates_Rec_Cref_Old_Cmpny_Fund.setValue(ldaIaal384.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund());                                         //Natural: ASSIGN IAA-OLD-CREF-RATES-REC.CREF-OLD-CMPNY-FUND := IAA-TIAA-FUND-RCRD-VIEW.TIAA-OLD-CMPNY-FUND
                getWorkFiles().write(1, true, iaa_Old_Cref_Rates_Rec);                                                                                                    //Natural: WRITE WORK FILE 1 VARIABLE IAA-OLD-CREF-RATES-REC
                pnd_Total_Written_Cref.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-WRITTEN-CREF
                pnd_Total_Written.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-WRITTEN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
    }
}
