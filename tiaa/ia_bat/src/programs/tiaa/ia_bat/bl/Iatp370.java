/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:05 PM
**        * FROM NATURAL PROGRAM : Iatp370
************************************************************
**        * FILE NAME            : Iatp370.java
**        * CLASS NAME           : Iatp370
**        * INSTANCE NAME        : Iatp370
************************************************************
************************************************************************
* PROGRAM  : IATP370
* SYSTEM   : IA
* TITLE    : DRIVER FOR REPORT MODULES IATN370,IATN371,IATN375 & IATN376
* DATE     : 05/25/99
* FUNCTION : THIS PROGRAM CALLS VARIOUS REPORTING MODULES ONE TO MANY
*            TIMES TO REPORT ALL USER-IDS PASSED BACK BY IATN36X.
*            THE MODULE TO CALL IS DETERMINED BY PARM READ
*            BY THIS MODULE.
*
* HISTORY
*
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp370 extends BLNatBase
{
    // Data Areas
    private PdaIata36x pdaIata36x;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIata36x = new PdaIata36x(localVariables);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp370() throws Exception
    {
        super("Iatp370");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  GET MODULE TO EXECUTE
                                                                                                                                                                          //Natural: PERFORM GET-MODULE-TO-CALL
        sub_Get_Module_To_Call();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "** Program called-->",pnd_Program," by Program-->",Global.getPROGRAM());                                                                   //Natural: WRITE '** Program called-->' #PROGRAM ' by Program-->' *PROGRAM
        if (Global.isEscape()) return;
        //*  GET LIST OF UNIT-CDES TO REPORT
        DbsUtil.callnat(Iatn36x.class , getCurrentProcessState(), pdaIata36x.getIata36x());                                                                               //Natural: CALLNAT 'IATN36X' IATA36X
        if (condition(Global.isEscape())) return;
        //* ****
        //* *  DO UNTIL END OF ALL UNIT-CDES TO REPORT
        //* ****
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO IATA36X-NBR-OF-UNIT-IDS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaIata36x.getIata36x_Iata36x_Nbr_Of_Unit_Ids())); pnd_I.nadd(1))
        {
            pdaIata36x.getIata36x_Iata36x_Unt_Cde_Nme().setValue(pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I).getSubstring(1,pdaIata36x.getIata36x_Iata36x_Lngth().getInt())); //Natural: MOVE SUBSTR ( IATA36X-UNIT-CDE ( #I ) ,1,IATA36X-LNGTH ) TO IATA36X-UNT-CDE-NME
            pdaIata36x.getIata36x_Iata36x_Indx().setValue(pnd_I);                                                                                                         //Natural: ASSIGN IATA36X-INDX := #I
            DbsUtil.callnat(DbsUtil.getBlType(pnd_Program), getCurrentProcessState(), pdaIata36x.getIata36x());                                                           //Natural: CALLNAT #PROGRAM IATA36X
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "Number of unit ids to process-->",pdaIata36x.getIata36x_Iata36x_Nbr_Of_Unit_Ids());                                                        //Natural: WRITE 'Number of unit ids to process-->' IATA36X-NBR-OF-UNIT-IDS
        if (Global.isEscape()) return;
        //*   READ PARM TO GET PROGRAM TO CALL FOR REPORTING
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MODULE-TO-CALL
    }
    private void sub_Get_Module_To_Call() throws Exception                                                                                                                //Natural: GET-MODULE-TO-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 9 #PROGRAM
        while (condition(getWorkFiles().read(9, pnd_Program)))
        {
            if (condition(! (DbsUtil.maskMatches(pnd_Program,"A"))))                                                                                                      //Natural: IF #PROGRAM NE MASK ( A )
            {
                getReports().write(0, "** ERROR PROGRAM NAME READ IN IS INVALID");                                                                                        //Natural: WRITE '** ERROR PROGRAM NAME READ IN IS INVALID'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "** PROGRM NAME READ-IN--->",pnd_Program);                                                                                          //Natural: WRITE '** PROGRM NAME READ-IN--->' #PROGRAM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "** JOB TERMINATED FIX MODULE NAME IN PARMLIB");                                                                                    //Natural: WRITE '** JOB TERMINATED FIX MODULE NAME IN PARMLIB'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "** AND RERUN JOB                            ");                                                                                    //Natural: WRITE '** AND RERUN JOB                            '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(16);  if (true) return;                                                                                                                 //Natural: TERMINATE 16
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
