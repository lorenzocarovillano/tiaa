/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:15 PM
**        * FROM NATURAL PROGRAM : Iaap910r
************************************************************
**        * FILE NAME            : Iaap910r.java
**        * CLASS NAME           : Iaap910r
**        * INSTANCE NAME        : Iaap910r
************************************************************
************************************************************************
* PROGRAM : IAAP910R
* DATE    : 10/09/2015
* PURPOSE : UPDATE ROTH CONTRIBUTION DATE FROM A FILE FEED. JOB TO
*           EXECUTE IS PIA2340D. INPUT FILE WILL COME FROM OMNI -
*           DSN=PDA.ANN.ROTHEXT.POMA390D.EXTRSEQY(0)
* HISTORY:
* 08/25/16  J BREMER  PIN EXPANSION   MAKE 082516
* 04/2017   O SOTTO   PIN EXPANSION - SC 082017 FOR CHANGES.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap910r extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_In_Pin_A;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_In_Pin;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_In_Da_Nbr;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_In_Roth_Contrib_Dte;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_In_Proc_Dte;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Pin_Cpr_Key;

    private DbsGroup pnd_Pin_Cpr_Key__R_Field_2;
    private DbsField pnd_Pin_Cpr_Key_Pnd_P_Pin;
    private DbsField pnd_Pin_Cpr_Key_Pnd_P_Cntrct;
    private DbsField pnd_Pin_Cpr_Key_Pnd_P_Payee;
    private DbsField pnd_S_Cntrct;
    private DbsField pnd_Updt_Cnt;
    private DbsField pnd_W_Roth_Dte;
    private DbsField pnd_O_Roth_Dte;
    private DbsField pnd_R_Roth_Dte;
    private DbsField pnd_Parm_Invrse_Dte;
    private DbsField pnd_Parm_Trans_Dte;
    private DbsField pnd_Parm_Check_Dte;
    private DbsField pnd_Parm_Todays_Dte;
    private DbsField pnd_Parm_Sub_Cde;
    private DbsField pnd_Parm_Trans_Cde;
    private DbsField pnd_Parm_Pgm;
    private DbsField pnd_Roth_Orgn;
    private DbsField pls_S_Invrse_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_In_Pin_A = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Pin_A", "#IN-PIN-A", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_In_Pin_A);
        pnd_Input_Pnd_In_Pin = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_In_Pin", "#IN-PIN", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F1 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Input_Pnd_In_Da_Nbr = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Da_Nbr", "#IN-DA-NBR", FieldType.STRING, 8);
        pnd_Input_Pnd_F2 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_In_Roth_Contrib_Dte = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Roth_Contrib_Dte", "#IN-ROTH-CONTRIB-DTE", FieldType.STRING, 8);
        pnd_Input_Pnd_F3 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Input_Pnd_In_Proc_Dte = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Proc_Dte", "#IN-PROC-DTE", FieldType.STRING, 8);
        pnd_Input_Pnd_F4 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 41);
        pnd_Pin_Cpr_Key = localVariables.newFieldInRecord("pnd_Pin_Cpr_Key", "#PIN-CPR-KEY", FieldType.STRING, 24);

        pnd_Pin_Cpr_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Pin_Cpr_Key__R_Field_2", "REDEFINE", pnd_Pin_Cpr_Key);
        pnd_Pin_Cpr_Key_Pnd_P_Pin = pnd_Pin_Cpr_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cpr_Key_Pnd_P_Pin", "#P-PIN", FieldType.NUMERIC, 12);
        pnd_Pin_Cpr_Key_Pnd_P_Cntrct = pnd_Pin_Cpr_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cpr_Key_Pnd_P_Cntrct", "#P-CNTRCT", FieldType.STRING, 10);
        pnd_Pin_Cpr_Key_Pnd_P_Payee = pnd_Pin_Cpr_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cpr_Key_Pnd_P_Payee", "#P-PAYEE", FieldType.NUMERIC, 2);
        pnd_S_Cntrct = localVariables.newFieldInRecord("pnd_S_Cntrct", "#S-CNTRCT", FieldType.STRING, 8);
        pnd_Updt_Cnt = localVariables.newFieldInRecord("pnd_Updt_Cnt", "#UPDT-CNT", FieldType.INTEGER, 2);
        pnd_W_Roth_Dte = localVariables.newFieldInRecord("pnd_W_Roth_Dte", "#W-ROTH-DTE", FieldType.DATE);
        pnd_O_Roth_Dte = localVariables.newFieldInRecord("pnd_O_Roth_Dte", "#O-ROTH-DTE", FieldType.DATE);
        pnd_R_Roth_Dte = localVariables.newFieldInRecord("pnd_R_Roth_Dte", "#R-ROTH-DTE", FieldType.STRING, 8);
        pnd_Parm_Invrse_Dte = localVariables.newFieldInRecord("pnd_Parm_Invrse_Dte", "#PARM-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Parm_Trans_Dte = localVariables.newFieldInRecord("pnd_Parm_Trans_Dte", "#PARM-TRANS-DTE", FieldType.TIME);
        pnd_Parm_Check_Dte = localVariables.newFieldInRecord("pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", FieldType.NUMERIC, 8);
        pnd_Parm_Todays_Dte = localVariables.newFieldInRecord("pnd_Parm_Todays_Dte", "#PARM-TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Parm_Sub_Cde = localVariables.newFieldInRecord("pnd_Parm_Sub_Cde", "#PARM-SUB-CDE", FieldType.STRING, 3);
        pnd_Parm_Trans_Cde = localVariables.newFieldInRecord("pnd_Parm_Trans_Cde", "#PARM-TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Parm_Pgm = localVariables.newFieldInRecord("pnd_Parm_Pgm", "#PARM-PGM", FieldType.STRING, 8);
        pnd_Roth_Orgn = localVariables.newFieldArrayInRecord("pnd_Roth_Orgn", "#ROTH-ORGN", FieldType.NUMERIC, 2, new DbsArrayController(1, 25));
        pls_S_Invrse_Dte = WsIndependent.getInstance().newFieldInRecord("pls_S_Invrse_Dte", "+S-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal999.initializeValues();

        localVariables.reset();
        pnd_Parm_Trans_Cde.setInitialValue(50);
        pnd_Parm_Pgm.setInitialValue("PIA2340D");
        pnd_Roth_Orgn.getValue(1).setInitialValue(12);
        pnd_Roth_Orgn.getValue(2).setInitialValue(20);
        pnd_Roth_Orgn.getValue(3).setInitialValue(21);
        pnd_Roth_Orgn.getValue(4).setInitialValue(22);
        pnd_Roth_Orgn.getValue(5).setInitialValue(23);
        pnd_Roth_Orgn.getValue(6).setInitialValue(24);
        pnd_Roth_Orgn.getValue(7).setInitialValue(25);
        pnd_Roth_Orgn.getValue(8).setInitialValue(26);
        pnd_Roth_Orgn.getValue(9).setInitialValue(27);
        pnd_Roth_Orgn.getValue(10).setInitialValue(51);
        pnd_Roth_Orgn.getValue(11).setInitialValue(52);
        pnd_Roth_Orgn.getValue(12).setInitialValue(53);
        pnd_Roth_Orgn.getValue(13).setInitialValue(54);
        pnd_Roth_Orgn.getValue(14).setInitialValue(55);
        pnd_Roth_Orgn.getValue(15).setInitialValue(56);
        pnd_Roth_Orgn.getValue(16).setInitialValue(57);
        pnd_Roth_Orgn.getValue(17).setInitialValue(58);
        pnd_Roth_Orgn.getValue(18).setInitialValue(71);
        pnd_Roth_Orgn.getValue(19).setInitialValue(72);
        pnd_Roth_Orgn.getValue(20).setInitialValue(73);
        pnd_Roth_Orgn.getValue(21).setInitialValue(74);
        pnd_Roth_Orgn.getValue(22).setInitialValue(75);
        pnd_Roth_Orgn.getValue(23).setInitialValue(76);
        pnd_Roth_Orgn.getValue(24).setInitialValue(77);
        pnd_Roth_Orgn.getValue(25).setInitialValue(78);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap910r() throws Exception
    {
        super("Iaap910r");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 133 ZP = ON
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            //*  IF #IN-PIN-A IS (N7)  /* 082516
            //*  082516
            if (condition(DbsUtil.is(pnd_Input_Pnd_In_Pin_A.getText(),"N12")))                                                                                            //Natural: IF #IN-PIN-A IS ( N12 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #IN-PIN = 0 OR = 9999999 /* 082516
            //*  082516
            if (condition(pnd_Input_Pnd_In_Pin.equals(getZero()) || pnd_Input_Pnd_In_Pin.equals(new DbsDecimal("999999999999"))))                                         //Natural: IF #IN-PIN = 0 OR = 999999999999
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pin_Cpr_Key_Pnd_P_Pin.setValue(pnd_Input_Pnd_In_Pin);                                                                                                     //Natural: ASSIGN #P-PIN := #IN-PIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-PIN
            sub_Process_Pin();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Updt_Cnt.equals(getZero())))                                                                                                                    //Natural: IF #UPDT-CNT = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"*************************************************************",NEWLINE,"*                                                           *", //Natural: WRITE ( 1 ) ///// '*************************************************************' / '*                                                           *' / '*                                                           *' / '*                                                           *' / '*                No records processed                       *' / '*                                                           *' / '*                                                           *' / '*                                                           *' / '*************************************************************' /
                NEWLINE,"*                                                           *",NEWLINE,"*                                                           *",
                NEWLINE,"*                No records processed                       *",NEWLINE,"*                                                           *",
                NEWLINE,"*                                                           *",NEWLINE,"*                                                           *",
                NEWLINE,"*************************************************************",NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTRACT-BEFORE-IMAGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTRACT-AFTER-IMAGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
    }
    private void sub_Process_Pin() throws Exception                                                                                                                       //Natural: PROCESS-PIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_S_Cntrct.reset();                                                                                                                                             //Natural: RESET #S-CNTRCT
        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ CPR BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #PIN-CPR-KEY
        (
        "RCPR",
        new Wc[] { new Wc("PIN_CNTRCT_PAYEE_KEY", ">=", pnd_Pin_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_PAYEE_KEY", "ASC") }
        );
        RCPR:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("RCPR")))
        {
            if (condition(ldaIaal999.getCpr_Cpr_Id_Nbr().notEquals(pnd_Pin_Cpr_Key_Pnd_P_Pin)))                                                                           //Natural: IF CPR-ID-NBR NE #P-PIN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal999.getCpr_Cntrct_Actvty_Cde().notEquals(1)))                                                                                            //Natural: IF CNTRCT-ACTVTY-CDE NE 1
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().notEquals(pnd_S_Cntrct)))                                                                              //Natural: IF CNTRCT-PART-PPCN-NBR NE #S-CNTRCT
            {
                pnd_S_Cntrct.setValue(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr());                                                                                          //Natural: ASSIGN #S-CNTRCT := CNTRCT-PART-PPCN-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                               //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = CNTRCT-PART-PPCN-NBR
            (
            "FC",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(), WcType.WITH) }
            );
            FC:
            while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FC")))
            {
                ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal999.getIaa_Cntrct_Cntrct_Orgn_Cde().equals(pnd_Roth_Orgn.getValue("*")) && ldaIaal999.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr().equals(pnd_Input_Pnd_In_Da_Nbr))) //Natural: IF CNTRCT-ORGN-CDE = #ROTH-ORGN ( * ) AND CNTRCT-ORIG-DA-CNTRCT-NBR = #IN-DA-NBR
                {
                    pnd_W_Roth_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Pnd_In_Roth_Contrib_Dte);                                                      //Natural: MOVE EDITED #IN-ROTH-CONTRIB-DTE TO #W-ROTH-DTE ( EM = YYYYMMDD )
                    if (condition(ldaIaal999.getIaa_Cntrct_Roth_Frst_Cntrb_Dte().notEquals(pnd_W_Roth_Dte)))                                                              //Natural: IF ROTH-FRST-CNTRB-DTE NE #W-ROTH-DTE
                    {
                        pnd_O_Roth_Dte.setValue(ldaIaal999.getIaa_Cntrct_Roth_Frst_Cntrb_Dte());                                                                          //Natural: ASSIGN #O-ROTH-DTE := ROTH-FRST-CNTRB-DTE
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTRACT-BEFORE-IMAGE
                        sub_Write_Contract_Before_Image();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FC"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FC"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        GC:                                                                                                                                               //Natural: GET IAA-CNTRCT *ISN ( FC. )
                        ldaIaal999.getVw_iaa_Cntrct().readByID(ldaIaal999.getVw_iaa_Cntrct().getAstISN("FC"), "GC");
                        ldaIaal999.getIaa_Cntrct_Roth_Frst_Cntrb_Dte().setValue(pnd_W_Roth_Dte);                                                                          //Natural: ASSIGN ROTH-FRST-CNTRB-DTE := #W-ROTH-DTE
                        ldaIaal999.getVw_iaa_Cntrct().updateDBRow("GC");                                                                                                  //Natural: UPDATE ( GC. )
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTRACT-AFTER-IMAGE
                        sub_Write_Contract_After_Image();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FC"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FC"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                        sub_Write_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FC"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FC"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Updt_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #UPDT-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RCPR"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RCPR"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Contract_Before_Image() throws Exception                                                                                                       //Natural: WRITE-CONTRACT-BEFORE-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaan060z.class , getCurrentProcessState(), ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(), ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),                   //Natural: CALLNAT 'IAAN060Z' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE #PARM-INVRSE-DTE #PARM-TRANS-DTE #PARM-CHECK-DTE #PARM-TODAYS-DTE #PARM-SUB-CDE #PARM-TRANS-CDE #PARM-PGM
            pnd_Parm_Invrse_Dte, pnd_Parm_Trans_Dte, pnd_Parm_Check_Dte, pnd_Parm_Todays_Dte, pnd_Parm_Sub_Cde, pnd_Parm_Trans_Cde, pnd_Parm_Pgm);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Contract_After_Image() throws Exception                                                                                                        //Natural: WRITE-CONTRACT-AFTER-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaan060y.class , getCurrentProcessState(), ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(), ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),                   //Natural: CALLNAT 'IAAN060Y' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE #PARM-INVRSE-DTE #PARM-TRANS-DTE #PARM-CHECK-DTE
            pnd_Parm_Invrse_Dte, pnd_Parm_Trans_Dte, pnd_Parm_Check_Dte);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_R_Roth_Dte.reset();                                                                                                                                           //Natural: RESET #R-ROTH-DTE
        if (condition(pnd_O_Roth_Dte.greater(getZero())))                                                                                                                 //Natural: IF #O-ROTH-DTE GT 0
        {
            pnd_R_Roth_Dte.setValueEdited(pnd_O_Roth_Dte,new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED #O-ROTH-DTE ( EM = YYYYMMDD ) TO #R-ROTH-DTE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "//Pin",                                                                                                                                  //Natural: DISPLAY ( 1 ) '//Pin' #IN-PIN '/Original/DA Number' #IN-DA-NBR '//Contract' IAA-CNTRCT.CNTRCT-PPCN-NBR 'Old Roth/Contribution/Date' #R-ROTH-DTE 'New Roth/Contribution/Date' #W-ROTH-DTE ( EM = YYYYMMDD )
        		pnd_Input_Pnd_In_Pin,"/Original/DA Number",
        		pnd_Input_Pnd_In_Da_Nbr,"//Contract",
        		ldaIaal999.getIaa_Cntrct_Cntrct_Ppcn_Nbr(),"Old Roth/Contribution/Date",
        		pnd_R_Roth_Dte,"New Roth/Contribution/Date",
        		pnd_W_Roth_Dte, new ReportEditMask ("YYYYMMDD"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(5),"IA ROTH CONTRIBUTION DATE UPDATE REPORT",new ColumnSpacing(5),Global.getDATX());      //Natural: WRITE ( 1 ) *PROGRAM 5X 'IA ROTH CONTRIBUTION DATE UPDATE REPORT' 5X *DATX
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=133 ZP=ON");

        getReports().setDisplayColumns(1, "//Pin",
        		pnd_Input_Pnd_In_Pin,"/Original/DA Number",
        		pnd_Input_Pnd_In_Da_Nbr,"//Contract",
        		ldaIaal999.getIaa_Cntrct_Cntrct_Ppcn_Nbr(),"Old Roth/Contribution/Date",
        		pnd_R_Roth_Dte,"New Roth/Contribution/Date",
        		pnd_W_Roth_Dte, new ReportEditMask ("YYYYMMDD"));
    }
}
