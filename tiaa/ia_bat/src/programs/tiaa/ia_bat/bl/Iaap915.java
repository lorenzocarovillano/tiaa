/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:17 PM
**        * FROM NATURAL PROGRAM : Iaap915
************************************************************
**        * FILE NAME            : Iaap915.java
**        * CLASS NAME           : Iaap915
**        * INSTANCE NAME        : Iaap915
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP915    READS IAA TRANS RECORD                 *
*      DATE     -  10/94      CREATES SELECTION RECORD LAYOUT        *
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap915 extends BLNatBase
{
    // Data Areas
    private LdaIaal915 ldaIaal915;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Trans_Effective_Dte_A;

    private DbsGroup pnd_Trans_Effective_Dte_A__R_Field_1;
    private DbsField pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal915 = new LdaIaal915();
        registerRecord(ldaIaal915);
        registerRecord(ldaIaal915.getVw_iaa_Cntrct());
        registerRecord(ldaIaal915.getVw_iaa_Cntrl_Rcrd());
        registerRecord(ldaIaal915.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal915.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal915.getVw_iaa_Cpr_Trans());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Trans_Effective_Dte_A = localVariables.newFieldInRecord("pnd_Trans_Effective_Dte_A", "#TRANS-EFFECTIVE-DTE-A", FieldType.STRING, 8);

        pnd_Trans_Effective_Dte_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Trans_Effective_Dte_A__R_Field_1", "REDEFINE", pnd_Trans_Effective_Dte_A);
        pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte = pnd_Trans_Effective_Dte_A__R_Field_1.newFieldInGroup("pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte", 
            "#TRANS-EFFECTIVE-DTE", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal915.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap915() throws Exception
    {
        super("Iaap915");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
                                                                                                                                                                          //Natural: PERFORM READ-PARM-CARD
        sub_Read_Parm_Card();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-CNTRL-RCRD
        sub_Read_Cntrl_Rcrd();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915.getVw_iaa_Trans_Rcrd().startDatabaseRead                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #PARM-CHECK-DTE-N
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N(), WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaIaal915.getVw_iaa_Trans_Rcrd().readNextRow("READ01")))
        {
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte.reset();                                                                                                    //Natural: RESET #TRANS-EFFECTIVE-DTE
                                                                                                                                                                          //Natural: PERFORM CHECK-PARM-CARD
            sub_Check_Parm_Card();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915.getPnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr().greater(getZero())))                                                              //Natural: IF #304-TRANS-TO-PROCESS-CTR > 0
        {
                                                                                                                                                                          //Natural: PERFORM COMBINE-CHECK-TRANS
            sub_Combine_Check_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "NO 304 TRANSACTIONS TO PROCESS");                                                                                                      //Natural: WRITE 'NO 304 TRANSACTIONS TO PROCESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ                : ",ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Ctr());                                           //Natural: WRITE 'NUMBER OF RECORDS READ                : ' #RECORDS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED           : ",ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr());                                 //Natural: WRITE 'NUMBER OF RECORDS PROCESSED           : ' #RECORDS-PROCESSED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr());                                  //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #RECORDS-BYPASSED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED (304)      : ",ldaIaal915.getPnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr());                              //Natural: WRITE 'NUMBER OF RECORDS BYPASSED (304)      : ' #304-TRANS-TO-PROCESS-CTR
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TRANS-RCRD
        //*      IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PARM-CARD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CNTRL-RCRD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PARM-CARD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MISC-NON-TAX-TRANS
        //*  IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MANUAL-NEW-ISSUE-TRANS
        //*  IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TAX-TRANS
        //*   IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMBINE-CHECK-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MULTIPLE-DED-TRANS
        //*  IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
    }
    private void sub_Process_Trans_Rcrd() throws Exception                                                                                                                //Natural: PROCESS-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal915.getPnd_Seq_Nbr().greater(9900)))                                                                                                         //Natural: IF #SEQ-NBR > 9900
        {
            ldaIaal915.getPnd_Seq_Nbr().reset();                                                                                                                          //Natural: RESET #SEQ-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal915.getPnd_Seq_Nbr().nadd(1);                                                                                                                          //Natural: ADD 1 TO #SEQ-NBR
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet248 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TRANS-CDE;//Natural: VALUE 020
        if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(20))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 033
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(33))))
        {
            decideConditionsMet248++;
            ldaIaal915.getPnd_Mode().setValue(ldaIaal915.getIaa_Cpr_Trans_Cntrct_Mode_Ind());                                                                             //Natural: ASSIGN #MODE := IAA-CPR-TRANS.CNTRCT-MODE-IND
                                                                                                                                                                          //Natural: PERFORM MANUAL-NEW-ISSUE-TRANS
            sub_Manual_New_Issue_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
            ldaIaal915.getPnd_Logical_Variables_Pnd_Records_Processed().setValue(true);                                                                                   //Natural: ASSIGN #RECORDS-PROCESSED := TRUE
        }                                                                                                                                                                 //Natural: VALUE 035
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(35))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MANUAL-NEW-ISSUE-TRANS
            sub_Manual_New_Issue_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 037
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(37))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 040
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(40))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 050 : 59
        else if (condition(((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().greaterOrEqual(50) && ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().lessOrEqual(59)))))
        {
            decideConditionsMet248++;
            ldaIaal915.getPnd_Mode().setValue(ldaIaal915.getIaa_Cpr_Trans_Cntrct_Mode_Ind());                                                                             //Natural: ASSIGN #MODE := IAA-CPR-TRANS.CNTRCT-MODE-IND
                                                                                                                                                                          //Natural: PERFORM MANUAL-NEW-ISSUE-TRANS
            sub_Manual_New_Issue_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 066
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(66))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(102))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 104
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(104))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(106))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 304
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(304))))
        {
            decideConditionsMet248++;
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte().setValue(ldaIaal915.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte());                                 //Natural: ASSIGN #WS-TRANS-304-RCRD.#CNTRL-FRST-TRANS-DTE := IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Dte().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Dte());                                                       //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte().setValue(ldaIaal915.getIaa_Trans_Rcrd_Lst_Trans_Dte());                                               //Natural: ASSIGN #WS-TRANS-304-RCRD.#LST-TRANS-DTE := IAA-TRANS-RCRD.LST-TRANS-DTE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                             //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                           //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Sub_Cde());                                               //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-SUB-CDE := IAA-TRANS-RCRD.TRANS-SUB-CDE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cde().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde());                                                       //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-CDE := IAA-TRANS-RCRD.TRANS-CDE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Actvty_Cde());                                         //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-ACTVTY-CDE := IAA-TRANS-RCRD.TRANS-ACTVTY-CDE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Check_Dte());                                           //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-CHECK-DTE := IAA-TRANS-RCRD.TRANS-CHECK-DTE
            if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte().notEquals(getZero())))                                                                       //Natural: IF IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE NE 0
            {
                pnd_Trans_Effective_Dte_A.setValueEdited(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #TRANS-EFFECTIVE-DTE-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte.setValue(0);                                                                                            //Natural: MOVE 00000000 TO #TRANS-EFFECTIVE-DTE
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte().setValue(pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte);                                      //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-EFFCTVE-DTE := #TRANS-EFFECTIVE-DTE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_Cmbne_Cde());                                           //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE := IAA-TRANS-RCRD.TRANS-CMBNE-CDE
            ldaIaal915.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Area().setValue(ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Area());                                           //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-USER-AREA := IAA-TRANS-RCRD.TRANS-USER-AREA
            //*  ===============================================================
            //*  WRITE 'IN IAAP915 WRITE BEFORE WRITTING RECORD'
            //*  WRITE '=' #CNTRL-FRST-TRANS-DTE
            //*  WRITE '=' #LST-TRANS-DTE
            //*  WRITE '=' #TRANS-PPCN-NBR
            //*  WRITE '=' #TRANS-PAYEE-CDE
            //*  WRITE '=' #TRANS-SUB-CDE
            //*  WRITE '=' #TRANS-CDE
            //*  WRITE '=' #TRANS-ACTVTY-CDE
            //*  WRITE '=' #TRANS-CHECK-DTE
            //*  WRITE '=' #TRANS-EFFCTVE-DTE
            //*  WRITE '=' #TRANS-CMBNE-CDE
            //*  WRITE '=' #TRANS-USER-AREA
            //*  WRITE '=' IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE(EM=CCYYMMDD)
            //*  WRITE '=' IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
            //*  ===============================================================
            getWorkFiles().write(2, false, ldaIaal915.getPnd_Ws_Trans_304_Rcrd());                                                                                        //Natural: WRITE WORK FILE 2 #WS-TRANS-304-RCRD
                                                                                                                                                                          //Natural: PERFORM COMBINE-CHECK-TRANS
            sub_Combine_Check_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
            ldaIaal915.getPnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr().nadd(1);                                                                                    //Natural: ADD 1 TO #304-TRANS-TO-PROCESS-CTR
        }                                                                                                                                                                 //Natural: VALUE 724
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(724))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM TAX-TRANS
            sub_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 902
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(902))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MULTIPLE-DED-TRANS
            sub_Multiple_Ded_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: VALUE 906
        else if (condition((ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(906))))
        {
            decideConditionsMet248++;
                                                                                                                                                                          //Natural: PERFORM MULTIPLE-DED-TRANS
            sub_Multiple_Ded_Trans();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Read_Parm_Card() throws Exception                                                                                                                    //Natural: READ-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        READWORK02:                                                                                                                                                       //Natural: READ WORK 4 #IAA-PARM-CARD
        while (condition(getWorkFiles().read(4, ldaIaal915.getPnd_Iaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaIaal915.getPnd_Iaa_Parm_Card());                                                                                                     //Natural: WRITE '=' #IAA-PARM-CARD
        if (Global.isEscape()) return;
    }
    private void sub_Read_Cntrl_Rcrd() throws Exception                                                                                                                   //Natural: READ-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Program_Id().equals("NO DATA")))                                                                                //Natural: IF #IAA-PARM-CARD.#PROGRAM-ID = 'NO DATA'
        {
            ldaIaal915.getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde().setValue("AA");                                                                                              //Natural: ASSIGN #CNTRL-CDE := 'AA'
            ldaIaal915.getVw_iaa_Cntrl_Rcrd().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
            (
            "READ03",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", ldaIaal915.getPnd_Cntrl_Rcrd_Key(), WcType.BY) },
            new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
            1
            );
            READ03:
            while (condition(ldaIaal915.getVw_iaa_Cntrl_Rcrd().readNextRow("READ03")))
            {
                ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte().setValueEdited(ldaIaal915.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));       //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #PARM-CHECK-DTE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Parm_Card() throws Exception                                                                                                                   //Natural: CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().reset();                                                                                                         //Natural: RESET #BYPASS
        short decideConditionsMet357 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PARM-CHECK-DTE NE ' '
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte().notEquals(" ")))
        {
            decideConditionsMet357++;
            if (condition(DbsUtil.maskMatches(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte(),"YYYYMMDD") && ldaIaal915.getIaa_Trans_Rcrd_Trans_Check_Dte().equals(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N()))) //Natural: IF #PARM-CHECK-DTE = MASK ( YYYYMMDD ) AND TRANS-CHECK-DTE = #PARM-CHECK-DTE-N
            {
                ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Ctr().nadd(1);                                                                                             //Natural: ADD 1 TO #RECORDS-CTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-FROM-DTE NE ' '
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte().notEquals(" ")))
        {
            decideConditionsMet357++;
            ldaIaal915.getPnd_Comp_Dte().setValueEdited(ldaIaal915.getIaa_Trans_Rcrd_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                         //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-DTE ( EM = YYYYMMDD ) TO #COMP-DTE
            if (condition(DbsUtil.maskMatches(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte(),"YYYYMMDD") && ldaIaal915.getPnd_Comp_Dte_Pnd_Comp_Dte_N().greaterOrEqual(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_N()))) //Natural: IF #PARM-FROM-DTE = MASK ( YYYYMMDD ) AND #COMP-DTE-N GE #PARM-FROM-DTE-N
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-TO-DTE NE ' '
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte().notEquals(" ")))
        {
            decideConditionsMet357++;
            ldaIaal915.getPnd_Comp_Dte().setValueEdited(ldaIaal915.getIaa_Trans_Rcrd_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                         //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-DTE ( EM = YYYYMMDD ) TO #COMP-DTE
            if (condition(DbsUtil.maskMatches(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte(),"YYYYMMDD") && ldaIaal915.getPnd_Comp_Dte_Pnd_Comp_Dte_N().lessOrEqual(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_N())  //Natural: IF #PARM-TO-DTE = MASK ( YYYYMMDD ) AND #COMP-DTE-N LE #PARM-TO-DTE-N AND #COMP-DTE-N GE #PARM-FROM-DTE-N
                && ldaIaal915.getPnd_Comp_Dte_Pnd_Comp_Dte_N().greaterOrEqual(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_N())))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-USER-AREA NE ' '
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_User_Area().notEquals(" ")))
        {
            decideConditionsMet357++;
            if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Area().equals(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_User_Area())))                                   //Natural: IF TRANS-USER-AREA = #PARM-USER-AREA
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-USER-ID NE ' '
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_User_Id().notEquals(" ")))
        {
            decideConditionsMet357++;
            if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Id().equals(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_User_Id())))                                       //Natural: IF TRANS-USER-ID = #PARM-USER-ID
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-VERIFY-ID NE ' '
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Verify_Id().notEquals(" ")))
        {
            decideConditionsMet357++;
            if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Verify_Id().equals(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Verify_Id())))                                   //Natural: IF TRANS-VERIFY-ID = #PARM-VERIFY-ID
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-VERIFY-CDE = 'U'
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde().equals("U")))
        {
            decideConditionsMet357++;
            if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Verify_Cde().equals("V")))                                                                                   //Natural: IF TRANS-VERIFY-CDE = 'V'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-VERIFY-CDE = 'A' OR = ' '
        if (condition(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde().equals("A") || ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde().equals(" ")))
        {
            decideConditionsMet357++;
            if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Actvty_Cde().equals("A")))                                                                                   //Natural: IF TRANS-ACTVTY-CDE = 'A'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().setValue(true);                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet357 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(33) && ldaIaal915.getIaa_Trans_Rcrd_Trans_Sub_Cde().equals("066")))                                 //Natural: IF TRANS-CDE = 033 AND TRANS-SUB-CDE = '066'
        {
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr().nadd(1);                                                                                        //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(33) && ldaIaal915.getIaa_Trans_Rcrd_Trans_Payee_Cde().equals(2)))                                   //Natural: IF TRANS-CDE = 033 AND TRANS-PAYEE-CDE = 02
        {
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr().nadd(1);                                                                                        //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde().equals(33) && ldaIaal915.getIaa_Trans_Rcrd_Trans_Payee_Cde().equals(3)))                                   //Natural: IF TRANS-CDE = 033 AND TRANS-PAYEE-CDE = 03
        {
            ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr().nadd(1);                                                                                        //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915.getPnd_Logical_Variables_Pnd_Bypass().getBoolean()))                                                                                     //Natural: IF #BYPASS
        {
            if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Check_Dte().equals(ldaIaal915.getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N())))                                 //Natural: IF TRANS-CHECK-DTE = #PARM-CHECK-DTE-N
            {
                ldaIaal915.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr().nadd(1);                                                                                    //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRANS-RCRD
            sub_Process_Trans_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Misc_Non_Tax_Trans() throws Exception                                                                                                                //Natural: MISC-NON-TAX-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte().notEquals(getZero())))                                                                           //Natural: IF IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE NE 0
        {
            pnd_Trans_Effective_Dte_A.setValueEdited(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #TRANS-EFFECTIVE-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte.setValue(0);                                                                                                //Natural: MOVE 00000000 TO #TRANS-EFFECTIVE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //*     #WS-TRANS-304-RCRD.#TRANS-EFFCTVE-DTE :=
        //*       #TRANS-EFFECTIVE-DTE
        DbsUtil.callnat(Iaan915a.class , getCurrentProcessState(), ldaIaal915.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN915A' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-SUB-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA IAA-TRANS-RCRD.TRANS-USER-ID IAA-TRANS-RCRD.INVRSE-TRANS-DTE #SEQ-NBR
            ldaIaal915.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Sub_Cde(), 
            ldaIaal915.getIaa_Trans_Rcrd_Trans_Check_Dte(), pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Area(), 
            ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Id(), ldaIaal915.getIaa_Trans_Rcrd_Invrse_Trans_Dte(), ldaIaal915.getPnd_Seq_Nbr());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Manual_New_Issue_Trans() throws Exception                                                                                                            //Natural: MANUAL-NEW-ISSUE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte().notEquals(getZero())))                                                                           //Natural: IF IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE NE 0
        {
            pnd_Trans_Effective_Dte_A.setValueEdited(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #TRANS-EFFECTIVE-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte.setValue(0);                                                                                                //Natural: MOVE 00000000 TO #TRANS-EFFECTIVE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Iaan915b.class , getCurrentProcessState(), ldaIaal915.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN915B' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA IAA-TRANS-RCRD.TRANS-USER-ID IAA-TRANS-RCRD.INVRSE-TRANS-DTE #SEQ-NBR #MODE
            ldaIaal915.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Check_Dte(), 
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Area(), ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Id(), 
            ldaIaal915.getIaa_Trans_Rcrd_Invrse_Trans_Dte(), ldaIaal915.getPnd_Seq_Nbr(), ldaIaal915.getPnd_Mode());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Tax_Trans() throws Exception                                                                                                                         //Natural: TAX-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte().notEquals(getZero())))                                                                           //Natural: IF IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE NE 0
        {
            pnd_Trans_Effective_Dte_A.setValueEdited(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #TRANS-EFFECTIVE-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte.setValue(0);                                                                                                //Natural: MOVE 00000000 TO #TRANS-EFFECTIVE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Iaan915c.class , getCurrentProcessState(), ldaIaal915.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN915C' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA IAA-TRANS-RCRD.TRANS-USER-ID IAA-TRANS-RCRD.INVRSE-TRANS-DTE #SEQ-NBR
            ldaIaal915.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Check_Dte(), 
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Area(), ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Id(), 
            ldaIaal915.getIaa_Trans_Rcrd_Invrse_Trans_Dte(), ldaIaal915.getPnd_Seq_Nbr());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Combine_Check_Trans() throws Exception                                                                                                               //Natural: COMBINE-CHECK-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.callnat(Iaan915d.class , getCurrentProcessState(), ldaIaal915.getPnd_Seq_Nbr());                                                                          //Natural: CALLNAT 'IAAN915D' #SEQ-NBR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Multiple_Ded_Trans() throws Exception                                                                                                                //Natural: MULTIPLE-DED-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte().notEquals(getZero())))                                                                           //Natural: IF IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE NE 0
        {
            pnd_Trans_Effective_Dte_A.setValueEdited(ldaIaal915.getIaa_Trans_Rcrd_Trans_Effective_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #TRANS-EFFECTIVE-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte.setValue(0);                                                                                                //Natural: MOVE 00000000 TO #TRANS-EFFECTIVE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Iaan915e.class , getCurrentProcessState(), ldaIaal915.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN915E' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA IAA-TRANS-RCRD.TRANS-USER-ID IAA-TRANS-RCRD.INVRSE-TRANS-DTE IAA-TRANS-RCRD.TRANS-CMBNE-CDE #SEQ-NBR
            ldaIaal915.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Check_Dte(), 
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Area(), ldaIaal915.getIaa_Trans_Rcrd_Trans_User_Id(), 
            ldaIaal915.getIaa_Trans_Rcrd_Invrse_Trans_Dte(), ldaIaal915.getIaa_Trans_Rcrd_Trans_Cmbne_Cde(), ldaIaal915.getPnd_Seq_Nbr());
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
