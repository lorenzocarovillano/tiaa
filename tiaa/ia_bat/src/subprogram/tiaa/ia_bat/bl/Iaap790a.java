/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:58 AM
**        * FROM NATURAL SUBPROGRAM : Iaap790a
************************************************************
**        * FILE NAME            : Iaap790a.java
**        * CLASS NAME           : Iaap790a
**        * INSTANCE NAME        : Iaap790a
************************************************************
************************************************************************
*
* PROGRAM NAME :- IAAP790A
* DATE         :- 05/30/00
* AUTHOR       :-
* DESCRIPTION  :- THIS PROGRAM UPDATES THE MODE CONTROL RECORDS
*              :- FOR PA SELECT VARIABLE FUNDS CALLED BY IAAP790
*              :- AND PRODUCES PA SELECT REPORT BY MODE FOR VARIABLE
*              :- FUNDS, TIAA PA SELECT FUNDS ARE HANDLED BY IAAP790
*
* HISTORY
* 7/02       ADDED NEW TITLES FOR NEW PA SELECT FUNDS
*                DO SCAN ON 7/02
* 3/12       RATE BASE EXPANSION
*                DO SCAN ON 3/12
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap790a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Number;
    private DbsField pnd_Work_Record_1_Pnd_C_Payee;
    private DbsField pnd_Work_Record_1_Pnd_C_Record_Code;
    private DbsField pnd_Work_Record_1_Pnd_Rest_Of_Record_250;

    private DbsGroup pnd_Work_Record_1__R_Field_1;
    private DbsField pnd_Work_Record_1_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Work_Record_1__R_Field_2;
    private DbsField pnd_Work_Record_1_Pnd_Header_Chk_Dte_A;

    private DbsGroup pnd_Work_Record_1__R_Field_3;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Option;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Origin;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Issue_Date;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Due_Dt;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Pd_Dt;
    private DbsField pnd_Work_Record_1_Pnd_C_Currency_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Type_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Payement_Meth;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Pension_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Joint_Cnv_Rcrd;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Original_Da_No;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Rsd_Issue_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_1st_Ann_Xref;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annuitant_Dob;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annt_Mrt_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annuitant_Sex;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annuitant_Life_Ct;
    private DbsField pnd_Work_Record_1_Pnd_C_Annuitant_Dod;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_2nd_Ann_Xref;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dob;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Mrt_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Sex;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dod;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Life_Ct;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Ss_No;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Dividend_Payee;
    private DbsField pnd_Work_Record_1_Pnd_C_Dividend_College_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Inst_Issue_Cde;
    private DbsField pnd_Work_Record_1_Pnd_C_Last_Transaction_Date;

    private DbsGroup pnd_Work_Record_1__R_Field_4;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_1__Filler1;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Ctznshp_Cde;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Rsdncy_Cde;
    private DbsField pnd_Work_Record_1__Filler2;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_1__Filler3;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Status_Code;
    private DbsField pnd_Work_Record_1__Filler4;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_1__Filler5;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Payment_Mode;

    private DbsGroup pnd_Work_Record_1__R_Field_5;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Company_Code_A3;

    private DbsGroup pnd_Work_Record_1__R_Field_6;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Company_Code;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Fund_Code;

    private DbsGroup pnd_Work_Record_1__R_Field_7;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Fund_Code_N;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Ivc_Amt;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Rtb_Amont;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Payment;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Old_Periodic_Payment;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Old_Dividend_Payment;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Rate_Code;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Rate_Date;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Amount;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend_2;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units;
    private DbsField pnd_Mode_Sub;
    private DbsField pnd_Updt_Cntrl;
    private DbsField pnd_Frst_Tme_Sw;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Next_Bus_Dte;
    private DbsField pnd_Func;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Prod_Cnt;
    private DbsField pnd_Max_Prd_Cde;
    private DbsField pnd_Rept_Sub;
    private DbsField pnd_Prod_Sub;
    private DbsField pnd_Prod_Sub_Monthly;
    private DbsField pls_Mode_Code;
    private DbsField pls_Prod_Code;
    private DbsField pls_Rept_Tot_Periodic_Payment;
    private DbsField pls_Rept_Tot_Periodic_Dividend;
    private DbsField pls_Rept_Tot_Mode_Units_Prod;
    private DbsField pls_Rept_Tot_Mode_Amts_Prod;
    private DbsField pls_Rept_Prod_Titles;
    private DbsField pls_Rept_Amt_Titles;
    private DbsField pls_Store_Cntrl_Cref_Payees;
    private DbsField pls_Search_Key_Rcrd;

    private DbsGroup pls_Search_Key_Rcrd__R_Field_8;
    private DbsField pls_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde;
    private DbsField pls_Search_Key_Rcrd_Pnd_Search_Trans_Dte;
    private DbsField pls_Search_Trans_Dte_A;
    private DbsField pls_Head_Check_Dte;
    private DbsField pls_Cntrl_Check_Dte;

    private DbsGroup pls_Cntrl_Check_Dte__R_Field_9;
    private DbsField pls_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Work_Record_1 = parameters.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1.setParameterOption(ParameterOption.ByReference);
        pnd_Work_Record_1_Pnd_C_Contract_Number = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Number", "#C-CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Work_Record_1_Pnd_C_Payee = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Payee", "#C-PAYEE", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Record_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Record_Code", "#C-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_Rest_Of_Record_250 = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Rest_Of_Record_250", "#REST-OF-RECORD-250", 
            FieldType.STRING, 250);

        pnd_Work_Record_1__R_Field_1 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_1", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_Header_Chk_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_Record_1__R_Field_2 = pnd_Work_Record_1__R_Field_1.newGroupInGroup("pnd_Work_Record_1__R_Field_2", "REDEFINE", pnd_Work_Record_1_Pnd_Header_Chk_Dte);
        pnd_Work_Record_1_Pnd_Header_Chk_Dte_A = pnd_Work_Record_1__R_Field_2.newFieldInGroup("pnd_Work_Record_1_Pnd_Header_Chk_Dte_A", "#HEADER-CHK-DTE-A", 
            FieldType.STRING, 8);

        pnd_Work_Record_1__R_Field_3 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_3", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_C_Contract_Option = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Option", "#C-CONTRACT-OPTION", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Origin = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Origin", "#C-CONTRACT-ORIGIN", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Issue_Date = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Issue_Date", "#C-CONTRACT-ISSUE-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Due_Dt = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Due_Dt", 
            "#C-CONTRACT-1ST-PAY-DUE-DT", FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Pd_Dt = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Pd_Dt", 
            "#C-CONTRACT-1ST-PAY-PD-DT", FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_C_Currency_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Currency_Code", "#C-CURRENCY-CODE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Contract_Type_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Type_Code", "#C-CONTRACT-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_C_Contract_Payement_Meth = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Payement_Meth", 
            "#C-CONTRACT-PAYEMENT-METH", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Pension_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Pension_Code", 
            "#C-CONTRACT-PENSION-CODE", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_C_Contract_Joint_Cnv_Rcrd = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Joint_Cnv_Rcrd", 
            "#C-CONTRACT-JOINT-CNV-RCRD", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Original_Da_No = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Original_Da_No", 
            "#C-CONTRACT-ORIGINAL-DA-NO", FieldType.STRING, 8);
        pnd_Work_Record_1_Pnd_C_Contract_Rsd_Issue_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Rsd_Issue_Code", 
            "#C-CONTRACT-RSD-ISSUE-CODE", FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_C_Contract_1st_Ann_Xref = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_1st_Ann_Xref", 
            "#C-CONTRACT-1ST-ANN-XREF", FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_C_First_Annuitant_Dob = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annuitant_Dob", "#C-FIRST-ANNUITANT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_C_First_Annt_Mrt_Yob_Dte = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annt_Mrt_Yob_Dte", 
            "#C-FIRST-ANNT-MRT-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_C_First_Annuitant_Sex = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annuitant_Sex", "#C-FIRST-ANNUITANT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_First_Annuitant_Life_Ct = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annuitant_Life_Ct", 
            "#C-FIRST-ANNUITANT-LIFE-CT", FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Annuitant_Dod = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Annuitant_Dod", "#C-ANNUITANT-DOD", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_C_Contract_2nd_Ann_Xref = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_2nd_Ann_Xref", 
            "#C-CONTRACT-2ND-ANN-XREF", FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dob = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dob", "#C-SECOND-ANNUITANT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_C_Second_Mrt_Yob_Dte = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Mrt_Yob_Dte", "#C-SECOND-MRT-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Sex = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Sex", "#C-SECOND-ANNUITANT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dod = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dod", "#C-SECOND-ANNUITANT-DOD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Life_Ct = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Life_Ct", 
            "#C-SECOND-ANNUITANT-LIFE-CT", FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Ss_No = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Ss_No", 
            "#C-SECOND-ANNUITANT-SS-NO", FieldType.NUMERIC, 9);
        pnd_Work_Record_1_Pnd_C_Contract_Dividend_Payee = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Dividend_Payee", 
            "#C-CONTRACT-DIVIDEND-PAYEE", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Dividend_College_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Dividend_College_Code", 
            "#C-DIVIDEND-COLLEGE-CODE", FieldType.STRING, 5);
        pnd_Work_Record_1_Pnd_C_Contract_Inst_Issue_Cde = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Inst_Issue_Cde", 
            "#C-CONTRACT-INST-ISSUE-CDE", FieldType.STRING, 5);
        pnd_Work_Record_1_Pnd_C_Last_Transaction_Date = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Last_Transaction_Date", 
            "#C-LAST-TRANSACTION-DATE", FieldType.TIME);

        pnd_Work_Record_1__R_Field_4 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_4", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_Cpr_Id_Nbr = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Work_Record_1__Filler1 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler1", "_FILLER1", FieldType.STRING, 7);
        pnd_Work_Record_1_Pnd_Cpr_Ctznshp_Cde = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Ctznshp_Cde", "#CPR-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_1_Pnd_Cpr_Rsdncy_Cde = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Rsdncy_Cde", "#CPR-RSDNCY-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_1__Filler2 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Cpr_Tax_Id_Nbr = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Tax_Id_Nbr", "#CPR-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record_1__Filler3 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Cpr_Status_Code = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Status_Code", "#CPR-STATUS-CODE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1__Filler4 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler4", "_FILLER4", FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_Cpr_Rcvry_Type_Ind = pnd_Work_Record_1__R_Field_4.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_Cpr_Rcvry_Type_Ind", "#CPR-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Work_Record_1_Pnd_Cpr_Cntrct_Per_Ivc_Amt = pnd_Work_Record_1__R_Field_4.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_Cpr_Cntrct_Per_Ivc_Amt", 
            "#CPR-CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_1__Filler5 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler5", "_FILLER5", FieldType.STRING, 120);
        pnd_Work_Record_1_Pnd_Cpr_Payment_Mode = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Payment_Mode", "#CPR-PAYMENT-MODE", 
            FieldType.NUMERIC, 3);

        pnd_Work_Record_1__R_Field_5 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_5", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_Fund_Company_Code_A3 = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Company_Code_A3", "#FUND-COMPANY-CODE-A3", 
            FieldType.STRING, 3);

        pnd_Work_Record_1__R_Field_6 = pnd_Work_Record_1__R_Field_5.newGroupInGroup("pnd_Work_Record_1__R_Field_6", "REDEFINE", pnd_Work_Record_1_Pnd_Fund_Company_Code_A3);
        pnd_Work_Record_1_Pnd_Fund_Company_Code = pnd_Work_Record_1__R_Field_6.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Company_Code", "#FUND-COMPANY-CODE", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Fund_Fund_Code = pnd_Work_Record_1__R_Field_6.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Fund_Code", "#FUND-FUND-CODE", 
            FieldType.STRING, 2);

        pnd_Work_Record_1__R_Field_7 = pnd_Work_Record_1__R_Field_6.newGroupInGroup("pnd_Work_Record_1__R_Field_7", "REDEFINE", pnd_Work_Record_1_Pnd_Fund_Fund_Code);
        pnd_Work_Record_1_Pnd_Fund_Fund_Code_N = pnd_Work_Record_1__R_Field_7.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Fund_Code_N", "#FUND-FUND-CODE-N", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Ivc_Amt = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Ivc_Amt", "#FUND-PERIODIC-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Rtb_Amont = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Rtb_Amont", "#FUND-RTB-AMONT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Payment = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Payment", "#FUND-PERIODIC-PAYMENT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend", "#FUND-PERIODIC-DIVIDEND", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Old_Periodic_Payment = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Old_Periodic_Payment", 
            "#FUND-OLD-PERIODIC-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Old_Dividend_Payment = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Old_Dividend_Payment", 
            "#FUND-OLD-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Rate_Code = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Rate_Code", "#FUND-RATE-CODE", 
            FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_Fund_Rate_Date = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Rate_Date", "#FUND-RATE-DATE", 
            FieldType.DATE);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Amount = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Amount", "#FUND-PERIODIC-AMOUNT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend_2 = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend_2", 
            "#FUND-PERIODIC-DIVIDEND-2", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units", "#FUND-TIAA-CREF-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Mode_Sub = parameters.newFieldInRecord("pnd_Mode_Sub", "#MODE-SUB", FieldType.NUMERIC, 2);
        pnd_Mode_Sub.setParameterOption(ParameterOption.ByReference);
        pnd_Updt_Cntrl = parameters.newFieldInRecord("pnd_Updt_Cntrl", "#UPDT-CNTRL", FieldType.STRING, 1);
        pnd_Updt_Cntrl.setParameterOption(ParameterOption.ByReference);
        pnd_Frst_Tme_Sw = parameters.newFieldInRecord("pnd_Frst_Tme_Sw", "#FRST-TME-SW", FieldType.STRING, 1);
        pnd_Frst_Tme_Sw.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte", 
            "CNTRL-FRST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_VIEW_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt", 
            "CNTRL-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt", 
            "CNTRL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts", 
            "C*CNTRL-FUND-CNTS", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", 
            FieldType.STRING, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees", 
            "CNTRL-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", 
            "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Units = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Units", "CNTRL-UNITS", 
            FieldType.PACKED_DECIMAL, 13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Amt = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys", 
            "CNTRL-ACTVE-TIAA-PYS", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys", 
            "CNTRL-ACTVE-CREF-PYS", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees", 
            "CNTRL-INACTVE-PAYEES", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Next_Bus_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        pnd_Func = localVariables.newFieldInRecord("pnd_Func", "#FUNC", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Prod_Cnt = localVariables.newFieldInRecord("pnd_Prod_Cnt", "#PROD-CNT", FieldType.NUMERIC, 2);
        pnd_Max_Prd_Cde = localVariables.newFieldInRecord("pnd_Max_Prd_Cde", "#MAX-PRD-CDE", FieldType.NUMERIC, 2);
        pnd_Rept_Sub = localVariables.newFieldInRecord("pnd_Rept_Sub", "#REPT-SUB", FieldType.NUMERIC, 2);
        pnd_Prod_Sub = localVariables.newFieldInRecord("pnd_Prod_Sub", "#PROD-SUB", FieldType.NUMERIC, 2);
        pnd_Prod_Sub_Monthly = localVariables.newFieldInRecord("pnd_Prod_Sub_Monthly", "#PROD-SUB-MONTHLY", FieldType.NUMERIC, 2);
        pls_Mode_Code = WsIndependent.getInstance().newFieldArrayInRecord("pls_Mode_Code", "+MODE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            22));
        pls_Prod_Code = WsIndependent.getInstance().newFieldArrayInRecord("pls_Prod_Code", "+PROD-CODE", FieldType.STRING, 3, new DbsArrayController(1, 
            80));
        pls_Rept_Tot_Periodic_Payment = WsIndependent.getInstance().newFieldArrayInRecord("pls_Rept_Tot_Periodic_Payment", "+REPT-TOT-PERIODIC-PAYMENT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 23));
        pls_Rept_Tot_Periodic_Dividend = WsIndependent.getInstance().newFieldArrayInRecord("pls_Rept_Tot_Periodic_Dividend", "+REPT-TOT-PERIODIC-DIVIDEND", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 23));
        pls_Rept_Tot_Mode_Units_Prod = WsIndependent.getInstance().newFieldArrayInRecord("pls_Rept_Tot_Mode_Units_Prod", "+REPT-TOT-MODE-UNITS-PROD", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 23, 41, 80));
        pls_Rept_Tot_Mode_Amts_Prod = WsIndependent.getInstance().newFieldArrayInRecord("pls_Rept_Tot_Mode_Amts_Prod", "+REPT-TOT-MODE-AMTS-PROD", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 23, 41, 80));
        pls_Rept_Prod_Titles = WsIndependent.getInstance().newFieldArrayInRecord("pls_Rept_Prod_Titles", "+REPT-PROD-TITLES", FieldType.STRING, 23, new 
            DbsArrayController(41, 80));
        pls_Rept_Amt_Titles = WsIndependent.getInstance().newFieldArrayInRecord("pls_Rept_Amt_Titles", "+REPT-AMT-TITLES", FieldType.STRING, 23, new DbsArrayController(41, 
            80));
        pls_Store_Cntrl_Cref_Payees = WsIndependent.getInstance().newFieldArrayInRecord("pls_Store_Cntrl_Cref_Payees", "+STORE-CNTRL-CREF-PAYEES", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 22, 41, 80));
        pls_Search_Key_Rcrd = WsIndependent.getInstance().newFieldInRecord("pls_Search_Key_Rcrd", "+SEARCH-KEY-RCRD", FieldType.STRING, 10);

        pls_Search_Key_Rcrd__R_Field_8 = localVariables.newGroupInRecord("pls_Search_Key_Rcrd__R_Field_8", "REDEFINE", pls_Search_Key_Rcrd);
        pls_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde = pls_Search_Key_Rcrd__R_Field_8.newFieldInGroup("pls_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde", "#SEARCH-CNTRL-CDE", 
            FieldType.STRING, 2);
        pls_Search_Key_Rcrd_Pnd_Search_Trans_Dte = pls_Search_Key_Rcrd__R_Field_8.newFieldInGroup("pls_Search_Key_Rcrd_Pnd_Search_Trans_Dte", "#SEARCH-TRANS-DTE", 
            FieldType.NUMERIC, 8);
        pls_Search_Trans_Dte_A = WsIndependent.getInstance().newFieldInRecord("pls_Search_Trans_Dte_A", "+SEARCH-TRANS-DTE-A", FieldType.NUMERIC, 8);
        pls_Head_Check_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Head_Check_Dte", "+HEAD-CHECK-DTE", FieldType.DATE);
        pls_Cntrl_Check_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Cntrl_Check_Dte", "+CNTRL-CHECK-DTE", FieldType.NUMERIC, 8);

        pls_Cntrl_Check_Dte__R_Field_9 = localVariables.newGroupInRecord("pls_Cntrl_Check_Dte__R_Field_9", "REDEFINE", pls_Cntrl_Check_Dte);
        pls_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A = pls_Cntrl_Check_Dte__R_Field_9.newFieldInGroup("pls_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A", "#CNTRL-CHECK-DTE-A", 
            FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1_View.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Prod_Cnt.setInitialValue(0);
        pnd_Max_Prd_Cde.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaap790a() throws Exception
    {
        super("Iaap790a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(pnd_Frst_Tme_Sw.equals("Y")))                                                                                                                       //Natural: IF #FRST-TME-SW = 'Y'
        {
            iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Record_1_Pnd_Header_Chk_Dte_A);                                  //Natural: MOVE EDITED #HEADER-CHK-DTE-A TO CNTRL-CHECK-DTE ( EM = YYYYMMDD )
            pls_Head_Check_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Record_1_Pnd_Header_Chk_Dte_A);                                                     //Natural: MOVE EDITED #HEADER-CHK-DTE-A TO +HEAD-CHECK-DTE ( EM = YYYYMMDD )
            pls_Cntrl_Check_Dte.setValue(pnd_Work_Record_1_Pnd_Header_Chk_Dte);                                                                                           //Natural: MOVE #HEADER-CHK-DTE TO +CNTRL-CHECK-DTE
            iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte.compute(new ComputeParameters(false, iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte), DbsField.subtract(100000000,             //Natural: COMPUTE CNTRL-INVRSE-DTE = 100000000 - #HEADER-CHK-DTE
                pnd_Work_Record_1_Pnd_Header_Chk_Dte));
            pls_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pls_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000000,         //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000000 - #HEADER-CHK-DTE
                pnd_Work_Record_1_Pnd_Header_Chk_Dte));
            pls_Search_Trans_Dte_A.setValue(pls_Search_Key_Rcrd_Pnd_Search_Trans_Dte);                                                                                    //Natural: MOVE #SEARCH-TRANS-DTE TO +SEARCH-TRANS-DTE-A
            pls_Search_Key_Rcrd_Pnd_Search_Trans_Dte.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte);                                                                    //Natural: MOVE CNTRL-INVRSE-DTE TO #SEARCH-TRANS-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Updt_Cntrl.notEquals("Y") && pnd_Frst_Tme_Sw.notEquals("Y")))                                                                                   //Natural: IF #UPDT-CNTRL NE 'Y' AND #FRST-TME-SW NE 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM FUND-EXTRACT
            sub_Fund_Extract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE THE REPORT AND CHANGE THE CONTROL RECORD
        if (condition(pnd_Updt_Cntrl.equals("Y")))                                                                                                                        //Natural: IF #UPDT-CNTRL = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-1
            sub_Write_Report_1();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-RECORD
            sub_Write_Control_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FUND-EXTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-1
        //*   +REPT-MODE-SUB = DETERMINES WHICH MODE TO PRINT
        //* ****************************
        //*  PAGE 1  MODES 100 THRU 603
        //* ****************************
        //*  ANNUAL FUND TOTAL
        //*  ANNUAL FUND TOTAL
        //*  ANNUAL FUND TOTAL
        //*   PRINT GRAND TOTAL FOR ALL MODES
        //*  WRITE (1) +REPT-PROD-TITLES (#REPT-SUB)
        //*   03X +REPT-TOT-PERIODIC-PAYMENT(#MODE-SUB) (EM=ZZZ,ZZZ,ZZZ.99)
        //*  ADD  1 TO #REPT-SUB
        //*  WRITE (1) +REPT-PROD-TITLES (#REPT-SUB)
        //*   03X +REPT-TOT-PERIODIC-DIVIDEND(#MODE-SUB) (EM=ZZZ,ZZZ,ZZZ.99)
        //*  ADD  1 TO #REPT-SUB
        //*  ANNUAL TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-RECORD
        //*    COMMNETED FOLLOWING  DOES NOT ACCUM FIXED TIAA-PERIODICS 7/00
        //*    THIS IS CURRENTLY DONE BY IAAP790
        //*    MOVE +MODE-CODE(#MODE-SUB) TO CNTRL-CDE
        //*    MOVE +REPT-TOT-PERIODIC-PAYMENT(#MODE-SUB) TO CNTRL-PER-PAY-AMT
        //*    MOVE +REPT-TOT-PERIODIC-DIVIDEND(#MODE-SUB) TO CNTRL-PER-DIV-AMT
        //*    END OF COMMENTED  7/00
    }
    private void sub_Fund_Extract() throws Exception                                                                                                                      //Natural: FUND-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1 ") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1G") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1S"))) //Natural: IF #FUND-FUND-CODE = '1 ' OR = '1G' OR = '1S'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Record_1_Pnd_Fund_Fund_Code.setValue(pnd_Work_Record_1_Pnd_Fund_Fund_Code, MoveOption.RightJustified);                                               //Natural: MOVE RIGHT #FUND-FUND-CODE TO #FUND-FUND-CODE
            DbsUtil.examine(new ExamineSource(pnd_Work_Record_1_Pnd_Fund_Fund_Code), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE #FUND-FUND-CODE FOR ' ' REPLACE '0'
        }                                                                                                                                                                 //Natural: END-IF
        //*  LB 10/97
        short decideConditionsMet295 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-FUND-CODE = '1 ' OR = '1G' OR = '1S'
        if (condition(pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1 ") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1G") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1S")))
        {
            decideConditionsMet295++;
            //*  ANNUAL FUNDS
            pnd_Prod_Sub.setValue(0);                                                                                                                                     //Natural: MOVE 0 TO #PROD-SUB
        }                                                                                                                                                                 //Natural: WHEN #FUND-COMPANY-CODE = '2' OR = 'U'
        else if (condition(pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("2") || pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("U")))
        {
            decideConditionsMet295++;
            pnd_Prod_Sub.setValue(pnd_Work_Record_1_Pnd_Fund_Fund_Code_N);                                                                                                //Natural: MOVE #FUND-FUND-CODE-N TO #PROD-SUB
            //*  MONTHLY FUNDS
            pls_Prod_Code.getValue(pnd_Prod_Sub).setValue(pnd_Work_Record_1_Pnd_Fund_Company_Code_A3);                                                                    //Natural: MOVE #FUND-COMPANY-CODE-A3 TO +PROD-CODE ( #PROD-SUB )
        }                                                                                                                                                                 //Natural: WHEN #FUND-COMPANY-CODE = '4' OR = 'W'
        else if (condition(pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("4") || pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("W")))
        {
            decideConditionsMet295++;
            pnd_Prod_Sub.compute(new ComputeParameters(false, pnd_Prod_Sub), pnd_Work_Record_1_Pnd_Fund_Fund_Code_N.add(20));                                             //Natural: ASSIGN #PROD-SUB = #FUND-FUND-CODE-N + 20
            pls_Prod_Code.getValue(pnd_Prod_Sub).setValue(pnd_Work_Record_1_Pnd_Fund_Company_Code_A3);                                                                    //Natural: MOVE #FUND-COMPANY-CODE-A3 TO +PROD-CODE ( #PROD-SUB )
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOHDR,"=============================");                                                                                    //Natural: WRITE NOHDR '============================='
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"| INVALID PROD CODE  ",pnd_Work_Record_1_Pnd_Fund_Fund_Code);                                                       //Natural: WRITE NOHDR '| INVALID PROD CODE  ' #FUND-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"| OR INVALID COMPANY CODE  ",pnd_Work_Record_1_Pnd_Fund_Company_Code);                                              //Natural: WRITE NOHDR '| OR INVALID COMPANY CODE  ' #FUND-COMPANY-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"=============================");                                                                                    //Natural: WRITE NOHDR '============================='
            if (Global.isEscape()) return;
            //*  LB 10/97
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ADD TOTALS
        if (condition(pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1 ") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1G") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1S"))) //Natural: IF #FUND-FUND-CODE = '1 ' OR = '1G' OR = '1S'
        {
            pls_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                                                       //Natural: ADD #FUND-PERIODIC-PAYMENT TO +REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB )
            pls_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend);                                                     //Natural: ADD #FUND-PERIODIC-DIVIDEND TO +REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB )
            //*  ADD 1 TO  #STORE-CNTRL-TIAA-PAYEES(#MODE-SUB)
            //*  ACCUMULATE TOTALS
            pls_Rept_Tot_Periodic_Payment.getValue(23).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                                                                 //Natural: ADD #FUND-PERIODIC-PAYMENT TO +REPT-TOT-PERIODIC-PAYMENT ( 23 )
            pls_Rept_Tot_Periodic_Dividend.getValue(23).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend);                                                               //Natural: ADD #FUND-PERIODIC-DIVIDEND TO +REPT-TOT-PERIODIC-DIVIDEND ( 23 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pls_Rept_Tot_Mode_Units_Prod.getValue(pnd_Mode_Sub,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units);                                            //Natural: ADD #FUND-TIAA-CREF-UNITS TO +REPT-TOT-MODE-UNITS-PROD ( #MODE-SUB, #PROD-SUB )
            pls_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units);                                                      //Natural: ADD #FUND-TIAA-CREF-UNITS TO +REPT-TOT-MODE-UNITS-PROD ( 23, #PROD-SUB )
            pls_Store_Cntrl_Cref_Payees.getValue(pnd_Mode_Sub,pnd_Prod_Sub).nadd(1);                                                                                      //Natural: ADD 1 TO +STORE-CNTRL-CREF-PAYEES ( #MODE-SUB, #PROD-SUB )
            //*  ANNUAL FUNDS
            if (condition(pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("2") || pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("U")))                                    //Natural: IF #FUND-COMPANY-CODE = '2' OR = 'U'
            {
                pls_Rept_Tot_Mode_Amts_Prod.getValue(pnd_Mode_Sub,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                                        //Natural: ADD #FUND-PERIODIC-PAYMENT TO +REPT-TOT-MODE-AMTS-PROD ( #MODE-SUB,#PROD-SUB )
                pls_Rept_Tot_Mode_Amts_Prod.getValue(23,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                                                  //Natural: ADD #FUND-PERIODIC-PAYMENT TO +REPT-TOT-MODE-AMTS-PROD ( 23,#PROD-SUB )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FUND-EXTRACT
    }
    private void sub_Write_Report_1() throws Exception                                                                                                                    //Natural: WRITE-REPORT-1
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE 'TIAA PERIODIC PAYMENT ' TO +REPT-PROD-TITLES(1)
        //*  MOVE 'TIAA PERIODIC DIVIDEND' TO +REPT-PROD-TITLES(41)
        pls_Rept_Prod_Titles.getValue(41).setValue("T/L FREE LOOK    UNITS");                                                                                             //Natural: MOVE 'T/L FREE LOOK    UNITS' TO +REPT-PROD-TITLES ( 41 )
        pls_Rept_Prod_Titles.getValue(42).setValue("T/L STOCK INDEX  UNITS");                                                                                             //Natural: MOVE 'T/L STOCK INDEX  UNITS' TO +REPT-PROD-TITLES ( 42 )
        pls_Rept_Prod_Titles.getValue(43).setValue("T/L GROWTH EQTY  UNITS");                                                                                             //Natural: MOVE 'T/L GROWTH EQTY  UNITS' TO +REPT-PROD-TITLES ( 43 )
        pls_Rept_Prod_Titles.getValue(44).setValue("T/L GROWTH INC   UNITS");                                                                                             //Natural: MOVE 'T/L GROWTH INC   UNITS' TO +REPT-PROD-TITLES ( 44 )
        pls_Rept_Prod_Titles.getValue(45).setValue("T/L INTEEQUITY   UNITS");                                                                                             //Natural: MOVE 'T/L INTEEQUITY   UNITS' TO +REPT-PROD-TITLES ( 45 )
        pls_Rept_Prod_Titles.getValue(46).setValue("T/L SOCIAL CHC   UNITS");                                                                                             //Natural: MOVE 'T/L SOCIAL CHC   UNITS' TO +REPT-PROD-TITLES ( 46 )
        //*  ADDED 7/02
        pls_Rept_Prod_Titles.getValue(47).setValue("T/L LGCAPV ACT   UNITS");                                                                                             //Natural: MOVE 'T/L LGCAPV ACT   UNITS' TO +REPT-PROD-TITLES ( 47 )
        //*  ADDED 7/02
        pls_Rept_Prod_Titles.getValue(48).setValue("T/L SMCAPE ACT   UNITS");                                                                                             //Natural: MOVE 'T/L SMCAPE ACT   UNITS' TO +REPT-PROD-TITLES ( 48 )
        //*  ADDED 7/02
        pls_Rept_Prod_Titles.getValue(49).setValue("T/L RESECS ACT   UNITS");                                                                                             //Natural: MOVE 'T/L RESECS ACT   UNITS' TO +REPT-PROD-TITLES ( 49 )
        //*  DOLLAR AMOUNT REPORT TITLES ADDED 3/97
        pls_Rept_Amt_Titles.getValue(41).setValue("T/L FREE LOOK   AMOUNT");                                                                                              //Natural: MOVE 'T/L FREE LOOK   AMOUNT' TO +REPT-AMT-TITLES ( 41 )
        pls_Rept_Amt_Titles.getValue(42).setValue("T/L STOCK INDEX AMOUNT");                                                                                              //Natural: MOVE 'T/L STOCK INDEX AMOUNT' TO +REPT-AMT-TITLES ( 42 )
        pls_Rept_Amt_Titles.getValue(43).setValue("T/L GROWTH EQTY AMOUNT");                                                                                              //Natural: MOVE 'T/L GROWTH EQTY AMOUNT' TO +REPT-AMT-TITLES ( 43 )
        pls_Rept_Amt_Titles.getValue(44).setValue("T/L GROWTH INC  AMOUNT");                                                                                              //Natural: MOVE 'T/L GROWTH INC  AMOUNT' TO +REPT-AMT-TITLES ( 44 )
        pls_Rept_Amt_Titles.getValue(45).setValue("T/L INTEEQUITY  AMOUNT");                                                                                              //Natural: MOVE 'T/L INTEEQUITY  AMOUNT' TO +REPT-AMT-TITLES ( 45 )
        pls_Rept_Amt_Titles.getValue(46).setValue("T/L SOCIAL CHC  AMOUNT");                                                                                              //Natural: MOVE 'T/L SOCIAL CHC  AMOUNT' TO +REPT-AMT-TITLES ( 46 )
        //*  ADDED 7/02
        pls_Rept_Amt_Titles.getValue(47).setValue("T/L LGCAPV ACT  AMOUNT");                                                                                              //Natural: MOVE 'T/L LGCAPV ACT  AMOUNT' TO +REPT-AMT-TITLES ( 47 )
        //*  ADDED 7/02
        pls_Rept_Amt_Titles.getValue(48).setValue("T/L SMCAPE ACT  AMOUNT");                                                                                              //Natural: MOVE 'T/L SMCAPE ACT  AMOUNT' TO +REPT-AMT-TITLES ( 48 )
        //*  ADDED 7/02
        pls_Rept_Amt_Titles.getValue(49).setValue("T/L RESECS ACT  AMOUNT");                                                                                              //Natural: MOVE 'T/L RESECS ACT  AMOUNT' TO +REPT-AMT-TITLES ( 49 )
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Mode_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #MODE-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        //*  -->PRINT MODES 100 THRU 603
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"100",new ColumnSpacing(51),"601",new ColumnSpacing(15),"602",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '100' 51X '601' 15X '602' 15X '603'
            ColumnSpacing(15),"603");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE (#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE (#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                //*  ADD NEW PRODUCT DESCRIPTION
                if (condition(pnd_Prod_Sub.greater(60)))                                                                                                                  //Natural: IF #PROD-SUB > 60
                {
                    pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).reset();                                                                                                  //Natural: RESET +REPT-PROD-TITLES ( #PROD-SUB )
                    pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).setValue(DbsUtil.compress("CODE ANN=", pls_Prod_Code.getValue(pnd_Prod_Sub), "MNTHLY=",                   //Natural: COMPRESS 'CODE ANN=' +PROD-CODE ( #PROD-SUB ) 'MNTHLY=' +PROD-CODE ( #PROD-SUB + 60 ) INTO +REPT-PROD-TITLES ( #PROD-SUB )
                        pls_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(60))));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(1,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 1, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 40X +REPT-TOT-MODE-UNITS-PROD ( 2, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 3, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 4, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(40),pls_Rept_Tot_Mode_Units_Prod.getValue(2,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(3,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(4,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  3/97
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Amts_Prod.getValue(1,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-AMT-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-AMTS-PROD ( 1, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 40X +REPT-TOT-MODE-AMTS-PROD ( 2, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 3, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 4, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(40),pls_Rept_Tot_Mode_Amts_Prod.getValue(2,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(3,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(4,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE(#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE(#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(1,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 1, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 40X +REPT-TOT-MODE-UNITS-PROD ( 2, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 3, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 4, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(40),pls_Rept_Tot_Mode_Units_Prod.getValue(2,pnd_Prod_Sub_Monthly), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(3,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(4,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************
        //*  PAGE 2  PAYMENT MODES 701 THRU 706
        //* ***********************************
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Mode_Sub.setValue(5);                                                                                                                                         //Natural: MOVE 5 TO #MODE-SUB
        //*   PRINT MODES 701 THRU 706
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"701",new ColumnSpacing(15),"702",new ColumnSpacing(15),"703",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '701' 15X '702' 15X '703' 15X '704' 15X '705' 15X '706'
            ColumnSpacing(15),"704",new ColumnSpacing(15),"705",new ColumnSpacing(15),"706");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE (#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE (#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(5,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 5, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 6, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 7, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 8, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 9, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 10, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(6,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(7,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(8,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(9,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(10,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT LINES ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Amts_Prod.getValue(5,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-AMT-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-AMTS-PROD ( 5, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 6, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 7, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 8, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 9, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 10,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(6,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(7,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(8,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(9,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(10,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE(#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE(#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(5,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 5, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 6, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 7, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 8, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 9, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 10, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(6,pnd_Prod_Sub_Monthly), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(7,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(8,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(9,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(10,pnd_Prod_Sub_Monthly), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *****************************
        //*  PAGE 3  MODES 801  THRU 806
        //* *****************************
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Mode_Sub.setValue(11);                                                                                                                                        //Natural: MOVE 11 TO #MODE-SUB
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"801",new ColumnSpacing(15),"802",new ColumnSpacing(15),"803",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '801' 15X '802' 15X '803' 15X '804' 15X '805' 15X '806'
            ColumnSpacing(15),"804",new ColumnSpacing(15),"805",new ColumnSpacing(15),"806");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE(#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE(#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(11,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 11, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 12, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 13, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 14, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 15, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 16, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(12,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(13,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(14,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(15,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(16,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT LINES ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Amts_Prod.getValue(11,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-AMT-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-AMTS-PROD ( 11,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 12,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 13,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 14,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 15,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 16,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(12,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(13,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(14,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(15,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(16,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE(#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE(#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(11,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 11, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 12, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 13, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 14, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 15, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 16, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(12,pnd_Prod_Sub_Monthly), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(13,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(14,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(15,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(16,pnd_Prod_Sub_Monthly), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* **************************
        //*  PAGE 4 MODES 807 THRU 812
        //* **************************
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Mode_Sub.setValue(17);                                                                                                                                        //Natural: MOVE 17 TO #MODE-SUB
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"807",new ColumnSpacing(15),"808",new ColumnSpacing(15),"809",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '807' 15X '808' 15X '809' 15X '810' 15X '811' 15X '812'
            ColumnSpacing(15),"810",new ColumnSpacing(15),"811",new ColumnSpacing(15),"812");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE(#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE(#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(17,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 17, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 18, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 19, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 20, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 21, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 22, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(18,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(19,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(20,pnd_Prod_Sub), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(21,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(22,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT LINES ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Amts_Prod.getValue(17,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-AMT-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-AMTS-PROD ( 17,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 18,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 19,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 20,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 21,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X +REPT-TOT-MODE-AMTS-PROD ( 22,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(18,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(19,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(20,pnd_Prod_Sub), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(21,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Amts_Prod.getValue(22,pnd_Prod_Sub), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE(#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE(#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(17,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 17, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 18, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 19, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 20, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 21, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X +REPT-TOT-MODE-UNITS-PROD ( 22, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(18,pnd_Prod_Sub_Monthly), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(19,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(20,pnd_Prod_Sub_Monthly), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(21,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pls_Rept_Tot_Mode_Units_Prod.getValue(22,pnd_Prod_Sub_Monthly), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ************************************************
        //*  PAGE 5 GRAND TOTAL OF ALL MODES
        //* ************************************************
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Mode_Sub.setValue(23);                                                                                                                                        //Natural: MOVE 23 TO #MODE-SUB
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,"GRAND TOTAL ALL MODES");                                                                                              //Natural: WRITE ( 1 ) 'GRAND TOTAL ALL MODES'
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR09:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            //*  IF +PROD-CODE(#PROD-SUB) NE ' ' OR
            //*      +PROD-CODE(#PROD-SUB + 20) NE ' '
            if (condition(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))     //Natural: IF +REPT-PROD-TITLES ( #PROD-SUB ) NE ' ' OR +REPT-PROD-TITLES ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 23, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT TOTALS ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Amts_Prod.getValue(23,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) +REPT-AMT-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-AMTS-PROD ( 23, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY TOTALS                                             /* LB 10/97
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #PROD-SUB = 41 TO 60
        for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pls_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pls_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF +PROD-CODE ( #PROD-SUB ) NE ' ' OR +PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub).setValue(DbsUtil.compress(pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub), pls_Prod_Code.getValue(pnd_Prod_Sub_Monthly))); //Natural: COMPRESS +REPT-PROD-TITLES ( #PROD-SUB ) +PROD-CODE ( #PROD-SUB-MONTHLY ) INTO +REPT-PROD-TITLES ( #PROD-SUB )
                getReports().write(1, ReportOption.NOTITLE,pls_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pls_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) +REPT-PROD-TITLES ( #PROD-SUB ) 03X +REPT-TOT-MODE-UNITS-PROD ( 23, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-REPORT-1
    }
    private void sub_Write_Control_Record() throws Exception                                                                                                              //Natural: WRITE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pls_Mode_Code.getValue(1).setValue("MA");                                                                                                                         //Natural: MOVE 'MA' TO +MODE-CODE ( 1 )
        pls_Mode_Code.getValue(2).setValue("MB");                                                                                                                         //Natural: MOVE 'MB' TO +MODE-CODE ( 2 )
        pls_Mode_Code.getValue(3).setValue("MC");                                                                                                                         //Natural: MOVE 'MC' TO +MODE-CODE ( 3 )
        pls_Mode_Code.getValue(4).setValue("MD");                                                                                                                         //Natural: MOVE 'MD' TO +MODE-CODE ( 4 )
        pls_Mode_Code.getValue(5).setValue("ME");                                                                                                                         //Natural: MOVE 'ME' TO +MODE-CODE ( 5 )
        pls_Mode_Code.getValue(6).setValue("MF");                                                                                                                         //Natural: MOVE 'MF' TO +MODE-CODE ( 6 )
        pls_Mode_Code.getValue(7).setValue("MG");                                                                                                                         //Natural: MOVE 'MG' TO +MODE-CODE ( 7 )
        pls_Mode_Code.getValue(8).setValue("MH");                                                                                                                         //Natural: MOVE 'MH' TO +MODE-CODE ( 8 )
        pls_Mode_Code.getValue(9).setValue("MI");                                                                                                                         //Natural: MOVE 'MI' TO +MODE-CODE ( 9 )
        pls_Mode_Code.getValue(10).setValue("MJ");                                                                                                                        //Natural: MOVE 'MJ' TO +MODE-CODE ( 10 )
        pls_Mode_Code.getValue(11).setValue("MK");                                                                                                                        //Natural: MOVE 'MK' TO +MODE-CODE ( 11 )
        pls_Mode_Code.getValue(12).setValue("ML");                                                                                                                        //Natural: MOVE 'ML' TO +MODE-CODE ( 12 )
        pls_Mode_Code.getValue(13).setValue("MM");                                                                                                                        //Natural: MOVE 'MM' TO +MODE-CODE ( 13 )
        pls_Mode_Code.getValue(14).setValue("MN");                                                                                                                        //Natural: MOVE 'MN' TO +MODE-CODE ( 14 )
        pls_Mode_Code.getValue(15).setValue("MO");                                                                                                                        //Natural: MOVE 'MO' TO +MODE-CODE ( 15 )
        pls_Mode_Code.getValue(16).setValue("MP");                                                                                                                        //Natural: MOVE 'MP' TO +MODE-CODE ( 16 )
        pls_Mode_Code.getValue(17).setValue("MQ");                                                                                                                        //Natural: MOVE 'MQ' TO +MODE-CODE ( 17 )
        pls_Mode_Code.getValue(18).setValue("MR");                                                                                                                        //Natural: MOVE 'MR' TO +MODE-CODE ( 18 )
        pls_Mode_Code.getValue(19).setValue("MS");                                                                                                                        //Natural: MOVE 'MS' TO +MODE-CODE ( 19 )
        pls_Mode_Code.getValue(20).setValue("MT");                                                                                                                        //Natural: MOVE 'MT' TO +MODE-CODE ( 20 )
        pls_Mode_Code.getValue(21).setValue("MU");                                                                                                                        //Natural: MOVE 'MU' TO +MODE-CODE ( 21 )
        pls_Mode_Code.getValue(22).setValue("MV");                                                                                                                        //Natural: MOVE 'MV' TO +MODE-CODE ( 22 )
        getReports().write(0, " ** IAAP790A IN UPDATE CONTROL RECORD *****");                                                                                             //Natural: WRITE ' ** IAAP790A IN UPDATE CONTROL RECORD *****'
        if (Global.isEscape()) return;
        pnd_Mode_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #MODE-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Mode_Sub.greater(22))) {break;}                                                                                                             //Natural: UNTIL #MODE-SUB > 22
            pls_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde.setValue(pls_Mode_Code.getValue(pnd_Mode_Sub));                                                                      //Natural: MOVE +MODE-CODE ( #MODE-SUB ) TO #SEARCH-CNTRL-CDE
            pnd_Func.setValue("  ");                                                                                                                                      //Natural: MOVE '  ' TO #FUNC
            vw_iaa_Cntrl_Rcrd_1_View.startDatabaseFind                                                                                                                    //Natural: FIND IAA-CNTRL-RCRD-1-VIEW WITH CNTRL-RCRD-KEY = +SEARCH-KEY-RCRD
            (
            "FINDC",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", "=", pls_Search_Key_Rcrd, WcType.WITH) }
            );
            FINDC:
            while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("FINDC", true)))
            {
                vw_iaa_Cntrl_Rcrd_1_View.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrl_Rcrd_1_View.getAstCOUNTER().equals(0)))                                                                                        //Natural: IF NO RECORDS FOUND
                {
                    pnd_Func.setValue("ST");                                                                                                                              //Natural: MOVE 'ST' TO #FUNC
                }                                                                                                                                                         //Natural: END-NOREC
                iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pls_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A);                           //Natural: MOVE EDITED #CNTRL-CHECK-DTE-A TO CNTRL-CHECK-DTE ( EM = YYYYMMDD )
                iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte.setValue(pls_Search_Trans_Dte_A);                                                                                  //Natural: MOVE +SEARCH-TRANS-DTE-A TO CNTRL-INVRSE-DTE
                iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte.setValue(Global.getDATX());                                                                                        //Natural: ASSIGN CNTRL-TODAYS-DTE = *DATX
                FOR11:                                                                                                                                                    //Natural: FOR #PROD-SUB = 41 TO 60
                for (pnd_Prod_Sub.setValue(41); condition(pnd_Prod_Sub.lessOrEqual(60)); pnd_Prod_Sub.nadd(1))
                {
                    //*  FILL ANNUAL BUCKETS
                    iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_Prod_Sub).setValue(pls_Prod_Code.getValue(pnd_Prod_Sub));                                           //Natural: MOVE +PROD-CODE ( #PROD-SUB ) TO CNTRL-FUND-CDE ( #PROD-SUB )
                    iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees.getValue(pnd_Prod_Sub).setValue(pls_Store_Cntrl_Cref_Payees.getValue(pnd_Mode_Sub,pnd_Prod_Sub));             //Natural: MOVE +STORE-CNTRL-CREF-PAYEES ( #MODE-SUB, #PROD-SUB ) TO CNTRL-FUND-PAYEES ( #PROD-SUB )
                    iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_Prod_Sub).setValue(pls_Rept_Tot_Mode_Units_Prod.getValue(pnd_Mode_Sub,pnd_Prod_Sub));                  //Natural: MOVE +REPT-TOT-MODE-UNITS-PROD ( #MODE-SUB, #PROD-SUB ) TO CNTRL-UNITS ( #PROD-SUB )
                    iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_Prod_Sub).setValue(pls_Rept_Tot_Mode_Amts_Prod.getValue(pnd_Mode_Sub,pnd_Prod_Sub));                     //Natural: MOVE +REPT-TOT-MODE-AMTS-PROD ( #MODE-SUB, #PROD-SUB ) TO CNTRL-AMT ( #PROD-SUB )
                    //*  FILL MONTHLY BUCKETS
                    pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                               //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                    iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_Prod_Sub_Monthly).setValue(pls_Prod_Code.getValue(pnd_Prod_Sub_Monthly));                           //Natural: MOVE +PROD-CODE ( #PROD-SUB-MONTHLY ) TO CNTRL-FUND-CDE ( #PROD-SUB-MONTHLY )
                    iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees.getValue(pnd_Prod_Sub_Monthly).setValue(pls_Store_Cntrl_Cref_Payees.getValue(pnd_Mode_Sub,                    //Natural: MOVE +STORE-CNTRL-CREF-PAYEES ( #MODE-SUB, #PROD-SUB-MONTHLY ) TO CNTRL-FUND-PAYEES ( #PROD-SUB-MONTHLY )
                        pnd_Prod_Sub_Monthly));
                    iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_Prod_Sub_Monthly).setValue(pls_Rept_Tot_Mode_Units_Prod.getValue(pnd_Mode_Sub,pnd_Prod_Sub_Monthly));  //Natural: MOVE +REPT-TOT-MODE-UNITS-PROD ( #MODE-SUB, #PROD-SUB-MONTHLY ) TO CNTRL-UNITS ( #PROD-SUB-MONTHLY )
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  STORE OR UPDATE THE CONTROL RECORD
                if (condition(pnd_Func.equals("ST")))                                                                                                                     //Natural: IF #FUNC = 'ST'
                {
                    vw_iaa_Cntrl_Rcrd_1_View.insertDBRow();                                                                                                               //Natural: STORE IAA-CNTRL-RCRD-1-VIEW
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    vw_iaa_Cntrl_Rcrd_1_View.updateDBRow("FINDC");                                                                                                        //Natural: UPDATE ( FINDC. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Prod_Sub.setValue(1);                                                                                                                                     //Natural: MOVE 1 TO #PROD-SUB
            pnd_Mode_Sub.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #MODE-SUB
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  WRITE-CONTROL-RECORD
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"PROGRAM    :",Global.getPROGRAM(),new ColumnSpacing(22),"IA ADMINISTRATION MODE CONTROL RECORD CREATE    ",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 01X 'PROGRAM    :' *PROGRAM 22X 'IA ADMINISTRATION MODE CONTROL RECORD CREATE    ' 115T 'Page :' *PAGE-NUMBER ( 1 ) ( AD = L ) / 01X 'CHECK DATE :' +HEAD-CHECK-DTE ( EM = MM/DD/YYYY ) 20X ' FOR PA SELECT VARIABLE FUNDS' / 01X 'RUN DATE   :' *DATX ( EM = MM/DD/YYYY ) //
                        TabSetting(115),"Page :",getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"),NEWLINE,new ColumnSpacing(1),"CHECK DATE :",pls_Head_Check_Dte, 
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(20)," FOR PA SELECT VARIABLE FUNDS",NEWLINE,new ColumnSpacing(1),"RUN DATE   :",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
    }
}
