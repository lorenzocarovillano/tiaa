/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:42:35 AM
**        * FROM NATURAL SUBPROGRAM : Iatn100a
************************************************************
**        * FILE NAME            : Iatn100a.java
**        * CLASS NAME           : Iatn100a
**        * INSTANCE NAME        : Iatn100a
************************************************************
************************************************************************
*  PROGRAM: IATN100A
*   AUTHOR: ARI GROSSMAN
*     DATE: MAY 18, 1999
*  PURPOSE: FILL AIAN019 LINKAGE FOR TEACHER TO CREF.
*
*  02/10  : SVSAA, PCPOP, INDEXED RATE GUARANTEE
*  O. SOTTO 04/05/12 RECOMPILED FOR RATE BASE EXPANSION CHANGE
*                    TO AIAL0190.
*  04/2017  OS RE-STOWED FOR IATA100 AND IAAL200B PIN EXPANSION.
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn100a extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIata100 pdaIata100;
    private PdaAial0190 pdaAial0190;
    private LdaIaal200a ldaIaal200a;
    private LdaIaal200b ldaIaal200b;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_2;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_3;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_1;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_2;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_File_Mode;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_To_Cntrct_Fund;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_T;
    private DbsField pnd_S;
    private DbsField pnd_W_Pct;
    private DbsField pnd_I;
    private DbsField pnd_Wrk_Dte;

    private DbsGroup pnd_Wrk_Dte__R_Field_4;
    private DbsField pnd_Wrk_Dte_Pnd_Wrk_Yyyymm;
    private DbsField pnd_Wrk_Dte_Pnd_Wrk_Dd;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_5;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Cc;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Yy;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Dd;

    private DbsGroup pnd_Eff_Date__R_Field_6;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_N;
    private DbsField pnd_Lst_Updte_Dte;
    private DbsField pnd_Nxt_Updte_Dte;

    private DbsGroup pnd_Nxt_Updte_Dte__R_Field_7;
    private DbsField pnd_Nxt_Updte_Dte_Pnd_Mm;
    private DbsField pnd_Nxt_Updte_Dte__Filler1;
    private DbsField pnd_Nxt_Updte_Dte_Pnd_Dd;
    private DbsField pnd_Nxt_Updte_Dte__Filler2;
    private DbsField pnd_Nxt_Updte_Dte_Pnd_Yyyy;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());

        // parameters
        parameters = new DbsRecord();
        pdaIata100 = new PdaIata100(parameters);
        pdaAial0190 = new PdaAial0190(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);

        pnd_Cntrct_Fund_Key__R_Field_3 = pnd_Cntrct_Fund_Key__R_Field_2.newGroupInGroup("pnd_Cntrct_Fund_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_1 = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_1", "#W-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_2 = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_2", "#W-FUND-CODE-2", 
            FieldType.STRING, 2);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_To_Cntrct_Fund = localVariables.newFieldInRecord("pnd_To_Cntrct_Fund", "#TO-CNTRCT-FUND", FieldType.STRING, 1);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.STRING, 1);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 4);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.PACKED_DECIMAL, 4);
        pnd_W_Pct = localVariables.newFieldInRecord("pnd_W_Pct", "#W-PCT", FieldType.PACKED_DECIMAL, 6, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_Wrk_Dte = localVariables.newFieldInRecord("pnd_Wrk_Dte", "#WRK-DTE", FieldType.STRING, 8);

        pnd_Wrk_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Wrk_Dte__R_Field_4", "REDEFINE", pnd_Wrk_Dte);
        pnd_Wrk_Dte_Pnd_Wrk_Yyyymm = pnd_Wrk_Dte__R_Field_4.newFieldInGroup("pnd_Wrk_Dte_Pnd_Wrk_Yyyymm", "#WRK-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Wrk_Dte_Pnd_Wrk_Dd = pnd_Wrk_Dte__R_Field_4.newFieldInGroup("pnd_Wrk_Dte_Pnd_Wrk_Dd", "#WRK-DD", FieldType.NUMERIC, 2);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_5", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Cc = pnd_Eff_Date__R_Field_5.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Cc", "#EFF-DATE-CC", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Yy = pnd_Eff_Date__R_Field_5.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Yy", "#EFF-DATE-YY", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Mm = pnd_Eff_Date__R_Field_5.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Mm", "#EFF-DATE-MM", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Dd = pnd_Eff_Date__R_Field_5.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Dd", "#EFF-DATE-DD", FieldType.STRING, 2);

        pnd_Eff_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_6", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_N = pnd_Eff_Date__R_Field_6.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 8);
        pnd_Lst_Updte_Dte = localVariables.newFieldInRecord("pnd_Lst_Updte_Dte", "#LST-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Nxt_Updte_Dte = localVariables.newFieldInRecord("pnd_Nxt_Updte_Dte", "#NXT-UPDTE-DTE", FieldType.STRING, 10);

        pnd_Nxt_Updte_Dte__R_Field_7 = localVariables.newGroupInRecord("pnd_Nxt_Updte_Dte__R_Field_7", "REDEFINE", pnd_Nxt_Updte_Dte);
        pnd_Nxt_Updte_Dte_Pnd_Mm = pnd_Nxt_Updte_Dte__R_Field_7.newFieldInGroup("pnd_Nxt_Updte_Dte_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Nxt_Updte_Dte__Filler1 = pnd_Nxt_Updte_Dte__R_Field_7.newFieldInGroup("pnd_Nxt_Updte_Dte__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Nxt_Updte_Dte_Pnd_Dd = pnd_Nxt_Updte_Dte__R_Field_7.newFieldInGroup("pnd_Nxt_Updte_Dte_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Nxt_Updte_Dte__Filler2 = pnd_Nxt_Updte_Dte__R_Field_7.newFieldInGroup("pnd_Nxt_Updte_Dte__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Nxt_Updte_Dte_Pnd_Yyyy = pnd_Nxt_Updte_Dte__R_Field_7.newFieldInGroup("pnd_Nxt_Updte_Dte_Pnd_Yyyy", "#YYYY", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal200a.initializeValues();
        ldaIaal200b.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn100a() throws Exception
    {
        super("Iatn100a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN100A", onError);
        //* *********
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number().setValue(pdaIata100.getIata100_Ia_Frm_Cntrct());                                                 //Natural: ON ERROR;//Natural: ASSIGN #AIAN019-CONTRACT-NUMBER := IATA100.IA-FRM-CNTRCT
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code().compute(new ComputeParameters(false, pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code()),    //Natural: ASSIGN #AIAN019-PAYEE-CODE := VAL ( IATA100.IA-FRM-PAYEE )
            pdaIata100.getIata100_Ia_Frm_Payee().val());
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #AIAN019-CONTRACT-NUMBER
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number(), WcType.WITH) },
        1
        );
        F1:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("F1")))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Option().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde());                                                //Natural: ASSIGN #AIAN019-OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                //Natural: ASSIGN #AIAN019-ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
            //*      #AIAN019-PAYMENT-METHOD := IAA-CNTRCT.CNTRCT-PYMNT-MTHD
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Yyyy_Mm().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte());                                        //Natural: ASSIGN #AIAN019-ISSUE-YYYY-MM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Day().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                         //Natural: ASSIGN #AIAN019-ISSUE-DAY := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dob().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                               //Natural: ASSIGN #AIAN019-FIRST-ANN-DOB := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Sex().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                               //Natural: ASSIGN #AIAN019-FIRST-ANN-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dod().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte());                               //Natural: ASSIGN #AIAN019-FIRST-ANN-DOD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dob().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte());                               //Natural: ASSIGN #AIAN019-SECOND-ANN-DOB := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Sex().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                               //Natural: ASSIGN #AIAN019-SECOND-ANN-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dod().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte());                               //Natural: ASSIGN #AIAN019-SECOND-ANN-DOD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
            //*  TPA FROM IPRO
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(54) || pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(62)        //Natural: IF #AIAN019-ORIGIN = 54 OR = 62 OR = 74
                || pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(74)))
            {
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Tpa_Ipro_Issue_Date().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Ssnng_Dte());                              //Natural: ASSIGN #AIAN019-TPA-IPRO-ISSUE-DATE := IAA-CNTRCT.CNTRCT-SSNNG-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number());                                                     //Natural: ASSIGN #PPCN-NBR := #AIAN019-CONTRACT-NUMBER
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code());                                                         //Natural: ASSIGN #PAYEE-CDE := #AIAN019-PAYEE-CODE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseRead                                                                                                     //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R1:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("R1")))
        {
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().notEquals(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number())       //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE #AIAN019-CONTRACT-NUMBER OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE #AIAN019-PAYEE-CODE
                || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().notEquals(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code())))
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9)))                                                                          //Natural: REJECT IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
            {
                continue;
            }
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Mode().setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                     //Natural: ASSIGN #AIAN019-MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Final_Per_Pay_Date().setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());              //Natural: ASSIGN #AIAN019-FINAL-PER-PAY-DATE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Eff_Date.setValueEdited(pdaIata100.getIata100_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED IATA100.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Effective_Date().setValue(pnd_Eff_Date_Pnd_Eff_Date_N);                                                   //Natural: ASSIGN #AIAN019-TRANSFER-EFFECTIVE-DATE := #EFF-DATE-N
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Type_Of_Run().setValue("I");                                                                                       //Natural: ASSIGN #AIAN019-TYPE-OF-RUN := 'I'
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Processing_Date().setValue(0);                                                                                     //Natural: ASSIGN #AIAN019-PROCESSING-DATE := 0
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Pct().setValue(0);                                                                                            //Natural: ASSIGN #AIAN019-GBPM-PCT := 0
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Rate().setValue(0);                                                                                           //Natural: ASSIGN #AIAN019-GBPM-RATE := 0
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Prorate_Ind().setValue(0);                                                                                    //Natural: ASSIGN #AIAN019-GBPM-PRORATE-IND := 0
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Lst_Updte_Dte, pnd_Nxt_Updte_Dte);                                                                 //Natural: CALLNAT 'IAAN0020' #LST-UPDTE-DTE #NXT-UPDTE-DTE
        if (condition(Global.isEscape())) return;
        pnd_Wrk_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Nxt_Updte_Dte_Pnd_Yyyy, pnd_Nxt_Updte_Dte_Pnd_Mm, pnd_Nxt_Updte_Dte_Pnd_Dd));            //Natural: COMPRESS #YYYY #MM #DD INTO #WRK-DTE LEAVING NO SPACE
        DbsUtil.callnat(Nazn6032.class , getCurrentProcessState(), pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Next_Pymnt_Dte(), pnd_Wrk_Dte, pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Mode()); //Natural: CALLNAT 'NAZN6032' #AIAN019-NEXT-PYMNT-DTE #WRK-DTE #AIAN019-MODE
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
