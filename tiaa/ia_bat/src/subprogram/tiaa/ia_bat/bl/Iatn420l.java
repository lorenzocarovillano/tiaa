/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:50 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420l
************************************************************
**        * FILE NAME            : Iatn420l.java
**        * CLASS NAME           : Iatn420l
**        * INSTANCE NAME        : Iatn420l
************************************************************
************************************************************************
*  PROGRAM: IATN420L
*   AUTHOR: ARI GROSSMAN
*     DATE: MAY 27, 1998
*  PURPOSE: EDIT CHECK ON CROSS CONTRACT TRANSFER
*
* HISTORY :
* 04/12 JT: RATE BASE EXPANSION - SCAN ON 041212 FOR CHANGES
* 04/2017   O SOTTO  RE-STOWED FOR IAAL999 PIN EXPANSION.
*
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420l extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ia_Frm_Cntrct;
    private DbsField pnd_Ia_Frm_Payee;

    private DbsGroup pnd_Ia_Frm_Payee__R_Field_1;
    private DbsField pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N;
    private DbsField pnd_Ia_To_Cntrct;

    private DbsGroup pnd_Ia_To_Cntrct__R_Field_2;
    private DbsField pnd_Ia_To_Cntrct_Pnd_Ia_To_Cntrct_1;
    private DbsField pnd_Ia_To_Payee;

    private DbsGroup pnd_Ia_To_Payee__R_Field_3;
    private DbsField pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Option;
    private DbsField pnd_Origin;
    private DbsField pnd_First_Ann_Dob;
    private DbsField pnd_First_Ann_Sex;
    private DbsField pnd_Second_Ann_Dob;
    private DbsField pnd_Second_Ann_Sex;
    private DbsField pnd_Cntrct_Mode_Ind;
    private DbsField pnd_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Found_Cnt;
    private DbsField pnd_Cntrct_Nbr;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_4;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_5;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_File_Mode;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();
        pnd_Ia_Frm_Cntrct = parameters.newFieldInRecord("pnd_Ia_Frm_Cntrct", "#IA-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_Ia_Frm_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Frm_Payee = parameters.newFieldInRecord("pnd_Ia_Frm_Payee", "#IA-FRM-PAYEE", FieldType.STRING, 2);
        pnd_Ia_Frm_Payee.setParameterOption(ParameterOption.ByReference);

        pnd_Ia_Frm_Payee__R_Field_1 = parameters.newGroupInRecord("pnd_Ia_Frm_Payee__R_Field_1", "REDEFINE", pnd_Ia_Frm_Payee);
        pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N = pnd_Ia_Frm_Payee__R_Field_1.newFieldInGroup("pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N", "#IA-FRM-PYEE-N", FieldType.NUMERIC, 
            2);
        pnd_Ia_To_Cntrct = parameters.newFieldInRecord("pnd_Ia_To_Cntrct", "#IA-TO-CNTRCT", FieldType.STRING, 10);
        pnd_Ia_To_Cntrct.setParameterOption(ParameterOption.ByReference);

        pnd_Ia_To_Cntrct__R_Field_2 = parameters.newGroupInRecord("pnd_Ia_To_Cntrct__R_Field_2", "REDEFINE", pnd_Ia_To_Cntrct);
        pnd_Ia_To_Cntrct_Pnd_Ia_To_Cntrct_1 = pnd_Ia_To_Cntrct__R_Field_2.newFieldInGroup("pnd_Ia_To_Cntrct_Pnd_Ia_To_Cntrct_1", "#IA-TO-CNTRCT-1", FieldType.STRING, 
            1);
        pnd_Ia_To_Payee = parameters.newFieldInRecord("pnd_Ia_To_Payee", "#IA-TO-PAYEE", FieldType.STRING, 2);
        pnd_Ia_To_Payee.setParameterOption(ParameterOption.ByReference);

        pnd_Ia_To_Payee__R_Field_3 = parameters.newGroupInRecord("pnd_Ia_To_Payee__R_Field_3", "REDEFINE", pnd_Ia_To_Payee);
        pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N = pnd_Ia_To_Payee__R_Field_3.newFieldInGroup("pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N", "#IA-TO-PAYEE-N", FieldType.NUMERIC, 
            2);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Option = localVariables.newFieldInRecord("pnd_Option", "#OPTION", FieldType.NUMERIC, 2);
        pnd_Origin = localVariables.newFieldInRecord("pnd_Origin", "#ORIGIN", FieldType.NUMERIC, 2);
        pnd_First_Ann_Dob = localVariables.newFieldInRecord("pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_First_Ann_Sex = localVariables.newFieldInRecord("pnd_First_Ann_Sex", "#FIRST-ANN-SEX", FieldType.NUMERIC, 1);
        pnd_Second_Ann_Dob = localVariables.newFieldInRecord("pnd_Second_Ann_Dob", "#SECOND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Second_Ann_Sex = localVariables.newFieldInRecord("pnd_Second_Ann_Sex", "#SECOND-ANN-SEX", FieldType.NUMERIC, 1);
        pnd_Cntrct_Mode_Ind = localVariables.newFieldInRecord("pnd_Cntrct_Mode_Ind", "#CNTRCT-MODE-IND", FieldType.NUMERIC, 3);
        pnd_Cntrct_Final_Pay_Dte = localVariables.newFieldInRecord("pnd_Cntrct_Final_Pay_Dte", "#CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Found_Cnt = localVariables.newFieldInRecord("pnd_Found_Cnt", "#FOUND-CNT", FieldType.STRING, 1);
        pnd_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal999.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420l() throws Exception
    {
        super("Iatn420l");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420L", onError);
        //* *********
        //*  WRITE 'IATN420L'
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
                                                                                                                                                                          //Natural: ON ERROR;//Natural: PERFORM #CHECK-CONTRACT
        sub_Pnd_Check_Contract();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.equals(" ")))                                                                                                                       //Natural: IF #RETURN-CODE = ' '
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-CPR
            sub_Pnd_Check_Cpr();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-CONTRACT
        //* ******************************************************************
        //*  USE NEW VIEW PREFIX FROM IAAL999                  041212 START
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-CPR
        //* ******************************************************************
    }
    private void sub_Pnd_Check_Contract() throws Exception                                                                                                                //Natural: #CHECK-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IA-FRM-CNTRCT
        (
        "U1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Ia_Frm_Cntrct, WcType.WITH) },
        1
        );
        U1:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("U1", true)))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal999.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                       //Natural: IF NO RECORDS FOUND
            {
                pnd_Return_Code.setValue("M1");                                                                                                                           //Natural: MOVE 'M1' TO #RETURN-CODE
                if (true) break U1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( U1. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Option.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Optn_Cde());                                                                                              //Natural: ASSIGN #OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
            pnd_Origin.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                              //Natural: ASSIGN #ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
            pnd_First_Ann_Dob.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                                                                             //Natural: ASSIGN #FIRST-ANN-DOB := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            pnd_First_Ann_Sex.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                                                             //Natural: ASSIGN #FIRST-ANN-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
            pnd_Second_Ann_Dob.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte());                                                                             //Natural: ASSIGN #SECOND-ANN-DOB := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
            pnd_Second_Ann_Sex.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                                                             //Natural: ASSIGN #SECOND-ANN-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IA-TO-CNTRCT
        (
        "V1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Ia_To_Cntrct, WcType.WITH) },
        1
        );
        V1:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("V1", true)))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal999.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                       //Natural: IF NO RECORDS FOUND
            {
                if (true) break V1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( V1. )
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pnd_Option.notEquals(ldaIaal999.getIaa_Cntrct_Cntrct_Optn_Cde()) || pnd_Origin.notEquals(ldaIaal999.getIaa_Cntrct_Cntrct_Orgn_Cde())            //Natural: IF #OPTION NE IAA-CNTRCT.CNTRCT-OPTN-CDE OR #ORIGIN NE IAA-CNTRCT.CNTRCT-ORGN-CDE OR #FIRST-ANN-DOB NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE OR #FIRST-ANN-SEX NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE OR #SECOND-ANN-DOB NE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE OR #SECOND-ANN-SEX NE IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
                || pnd_First_Ann_Dob.notEquals(ldaIaal999.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte()) || pnd_First_Ann_Sex.notEquals(ldaIaal999.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde()) 
                || pnd_Second_Ann_Dob.notEquals(ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte()) || pnd_Second_Ann_Sex.notEquals(ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde())))
            {
                pnd_Return_Code.setValue("V8");                                                                                                                           //Natural: MOVE 'V8' TO #RETURN-CODE
                if (true) break V1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( V1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Cpr() throws Exception                                                                                                                     //Natural: #CHECK-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(pnd_Ia_Frm_Cntrct);                                                                                                    //Natural: ASSIGN #PPCN-NBR := #IA-FRM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N);                                                                                  //Natural: ASSIGN #PAYEE-CDE := #IA-FRM-PYEE-N
        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R1:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("R1")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().notEquals(pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr) || ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().notEquals(pnd_Cntrct_Payee_Key_Pnd_Payee_Cde))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR NE #PPCN-NBR OR CPR.CNTRCT-PART-PAYEE-CDE NE #PAYEE-CDE
            {
                pnd_Return_Code.setValue("M2");                                                                                                                           //Natural: MOVE 'M2' TO #RETURN-CODE
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrct_Mode_Ind.setValue(ldaIaal999.getCpr_Cntrct_Mode_Ind());                                                                                            //Natural: ASSIGN #CNTRCT-MODE-IND := CPR.CNTRCT-MODE-IND
            pnd_Cntrct_Final_Pay_Dte.setValue(ldaIaal999.getCpr_Cntrct_Final_Pay_Dte());                                                                                  //Natural: ASSIGN #CNTRCT-FINAL-PAY-DTE := CPR.CNTRCT-FINAL-PAY-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(pnd_Ia_To_Cntrct);                                                                                                     //Natural: ASSIGN #PPCN-NBR := #IA-TO-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N);                                                                                   //Natural: ASSIGN #PAYEE-CDE := #IA-TO-PAYEE-N
        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R2",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R2:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("R2")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().notEquals(pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr) || ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().notEquals(pnd_Cntrct_Payee_Key_Pnd_Payee_Cde))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR NE #PPCN-NBR OR CPR.CNTRCT-PART-PAYEE-CDE NE #PAYEE-CDE
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cntrct_Mode_Ind.notEquals(ldaIaal999.getCpr_Cntrct_Mode_Ind()) || pnd_Cntrct_Final_Pay_Dte.notEquals(ldaIaal999.getCpr_Cntrct_Final_Pay_Dte()))) //Natural: IF #CNTRCT-MODE-IND NE CPR.CNTRCT-MODE-IND OR #CNTRCT-FINAL-PAY-DTE NE CPR.CNTRCT-FINAL-PAY-DTE
            {
                pnd_Return_Code.setValue("V8");                                                                                                                           //Natural: MOVE 'V8' TO #RETURN-CODE
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  USE NEW VIEW PREFIX FROM IAAL999                  041212 END
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
