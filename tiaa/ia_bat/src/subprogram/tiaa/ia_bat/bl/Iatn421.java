/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:56 AM
**        * FROM NATURAL SUBPROGRAM : Iatn421
************************************************************
**        * FILE NAME            : Iatn421.java
**        * CLASS NAME           : Iatn421
**        * INSTANCE NAME        : Iatn421
************************************************************
************************************************************************
* PROGRAM : IATN421
* FUNCTION: RETRIEVE RATE CODE FOR A FUND
* HISTORY:
* 03/04/09  OS CREATED AS PART OF TIAA ACCESS PROJECT.
*
* 11/05/2013 J TINIO TIAA ACCESS CHANGE - CALL NECN4000 FOR CREF/REA
*                    REDESIGN PROJECT.
* 09/08/2014 O.SOTTO REVISED SO THAT NECN4000 IS CALLED ONLY ONCE.
*                    CHANGES MARKED 090814.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn421 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Num_Cde;
    private DbsField pnd_Rate_Cde;

    private DataAccessProgramView vw_ia_Rte;
    private DbsField ia_Rte_Nec_Table_Cde;
    private DbsField ia_Rte_Nec_Product_Cde;
    private DbsField ia_Rte_Nec_Ticker_Symbol;
    private DbsField ia_Rte_Nec_Rate_Cde;
    private DbsField ia_Rte_Nec_Effective_Dte;
    private DbsField ia_Rte_Nec_Inception_Dte;
    private DbsField pnd_Nec_Rte_Super1;

    private DbsGroup pnd_Nec_Rte_Super1__R_Field_1;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Product_Cde;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Ticker;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Rate_Cde;
    private DbsField pnd_Tkr;
    private DbsField pnd_Fnd;

    private DbsGroup pnd_Fnd__R_Field_2;
    private DbsField pnd_Fnd_Pnd_Fnd_N;
    private DbsField pnd_A;
    private DbsField pls_Iatn421_Not_First_Time;
    private DbsField pls_Iatn421_Tkr;
    private DbsField pls_Iatn421_Fnd;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Num_Cde = parameters.newFieldInRecord("pnd_Num_Cde", "#NUM-CDE", FieldType.STRING, 2);
        pnd_Num_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Rate_Cde = parameters.newFieldInRecord("pnd_Rate_Cde", "#RATE-CDE", FieldType.STRING, 2);
        pnd_Rate_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_ia_Rte = new DataAccessProgramView(new NameInfo("vw_ia_Rte", "IA-RTE"), "NEW_EXT_CNTRL_RTE", "NEW_EXT_CNTRL");
        ia_Rte_Nec_Table_Cde = vw_ia_Rte.getRecord().newFieldInGroup("ia_Rte_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NEC_TABLE_CDE");
        ia_Rte_Nec_Product_Cde = vw_ia_Rte.getRecord().newFieldInGroup("ia_Rte_Nec_Product_Cde", "NEC-PRODUCT-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "NEC_PRODUCT_CDE");
        ia_Rte_Nec_Product_Cde.setDdmHeader("PRODUCT CODE");
        ia_Rte_Nec_Ticker_Symbol = vw_ia_Rte.getRecord().newFieldInGroup("ia_Rte_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "NEC_TICKER_SYMBOL");
        ia_Rte_Nec_Rate_Cde = vw_ia_Rte.getRecord().newFieldInGroup("ia_Rte_Nec_Rate_Cde", "NEC-RATE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NEC_RATE_CDE");
        ia_Rte_Nec_Effective_Dte = vw_ia_Rte.getRecord().newFieldInGroup("ia_Rte_Nec_Effective_Dte", "NEC-EFFECTIVE-DTE", FieldType.PACKED_DECIMAL, 8, 
            RepeatingFieldStrategy.None, "NEC_EFFECTIVE_DTE");
        ia_Rte_Nec_Inception_Dte = vw_ia_Rte.getRecord().newFieldInGroup("ia_Rte_Nec_Inception_Dte", "NEC-INCEPTION-DTE", FieldType.PACKED_DECIMAL, 8, 
            RepeatingFieldStrategy.None, "NEC_INCEPTION_DTE");
        registerRecord(vw_ia_Rte);

        pnd_Nec_Rte_Super1 = localVariables.newFieldInRecord("pnd_Nec_Rte_Super1", "#NEC-RTE-SUPER1", FieldType.STRING, 31);

        pnd_Nec_Rte_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Nec_Rte_Super1__R_Field_1", "REDEFINE", pnd_Nec_Rte_Super1);
        pnd_Nec_Rte_Super1_Pnd_Nec_Table_Cde = pnd_Nec_Rte_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Rte_Super1_Pnd_Nec_Product_Cde = pnd_Nec_Rte_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Product_Cde", "#NEC-PRODUCT-CDE", 
            FieldType.STRING, 10);
        pnd_Nec_Rte_Super1_Pnd_Nec_Ticker = pnd_Nec_Rte_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Ticker", "#NEC-TICKER", FieldType.STRING, 
            10);
        pnd_Nec_Rte_Super1_Pnd_Nec_Rate_Cde = pnd_Nec_Rte_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Rate_Cde", "#NEC-RATE-CDE", FieldType.STRING, 
            8);
        pnd_Tkr = localVariables.newFieldArrayInRecord("pnd_Tkr", "#TKR", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pnd_Fnd = localVariables.newFieldArrayInRecord("pnd_Fnd", "#FND", FieldType.STRING, 2, new DbsArrayController(1, 100));

        pnd_Fnd__R_Field_2 = localVariables.newGroupInRecord("pnd_Fnd__R_Field_2", "REDEFINE", pnd_Fnd);
        pnd_Fnd_Pnd_Fnd_N = pnd_Fnd__R_Field_2.newFieldArrayInGroup("pnd_Fnd_Pnd_Fnd_N", "#FND-N", FieldType.NUMERIC, 2, new DbsArrayController(1, 100));
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pls_Iatn421_Not_First_Time = WsIndependent.getInstance().newFieldInRecord("pls_Iatn421_Not_First_Time", "+IATN421-NOT-FIRST-TIME", FieldType.BOOLEAN, 
            1);
        pls_Iatn421_Tkr = WsIndependent.getInstance().newFieldArrayInRecord("pls_Iatn421_Tkr", "+IATN421-TKR", FieldType.STRING, 10, new DbsArrayController(1, 
            100));
        pls_Iatn421_Fnd = WsIndependent.getInstance().newFieldArrayInRecord("pls_Iatn421_Fnd", "+IATN421-FND", FieldType.STRING, 2, new DbsArrayController(1, 
            100));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ia_Rte.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Nec_Rte_Super1.setInitialValue("RTEIA");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn421() throws Exception
    {
        super("Iatn421");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  090814
        //*  090814
        if (condition(! (pls_Iatn421_Not_First_Time.getBoolean())))                                                                                                       //Natural: IF NOT +IATN421-NOT-FIRST-TIME
        {
            pls_Iatn421_Not_First_Time.setValue(true);                                                                                                                    //Natural: ASSIGN +IATN421-NOT-FIRST-TIME := TRUE
            pdaNeca4000.getNeca4000_Function_Cde().setValue("CFN");                                                                                                       //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CFN'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                 //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
            pdaNeca4000.getNeca4000_Cfn_Key_Company_Cde().setValue("001");                                                                                                //Natural: ASSIGN NECA4000.CFN-KEY-COMPANY-CDE := '001'
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())) return;
            FOR01:                                                                                                                                                        //Natural: FOR #A 1 100
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(100)); pnd_A.nadd(1))
            {
                if (condition(pdaNeca4000.getNeca4000_Cfn_Ticker_Symbol().getValue(pnd_A).equals(" ")))                                                                   //Natural: IF CFN-TICKER-SYMBOL ( #A ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tkr.getValue(pnd_A).setValue(pdaNeca4000.getNeca4000_Cfn_Ticker_Symbol().getValue(pnd_A));                                                            //Natural: ASSIGN #TKR ( #A ) := CFN-TICKER-SYMBOL ( #A )
                if (condition(DbsUtil.is(pdaNeca4000.getNeca4000_Cfn_Fnd_Ia_Fund_Cde().getValue(pnd_A).getText(),"N2")))                                                  //Natural: IF CFN-FND-IA-FUND-CDE ( #A ) IS ( N2 )
                {
                    pnd_Fnd_Pnd_Fnd_N.getValue(pnd_A).compute(new ComputeParameters(false, pnd_Fnd_Pnd_Fnd_N.getValue(pnd_A)), pdaNeca4000.getNeca4000_Cfn_Fnd_Ia_Fund_Cde().getValue(pnd_A).val()); //Natural: ASSIGN #FND-N ( #A ) := VAL ( CFN-FND-IA-FUND-CDE ( #A ) )
                }                                                                                                                                                         //Natural: END-IF
                //*  090814 START
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pls_Iatn421_Tkr.getValue("*").setValue(pnd_Tkr.getValue("*"));                                                                                                //Natural: ASSIGN +IATN421-TKR ( * ) := #TKR ( * )
            pls_Iatn421_Fnd.getValue("*").setValue(pnd_Fnd.getValue("*"));                                                                                                //Natural: ASSIGN +IATN421-FND ( * ) := #FND ( * )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tkr.getValue("*").setValue(pls_Iatn421_Tkr.getValue("*"));                                                                                                //Natural: ASSIGN #TKR ( * ) := +IATN421-TKR ( * )
            pnd_Fnd.getValue("*").setValue(pls_Iatn421_Fnd.getValue("*"));                                                                                                //Natural: ASSIGN #FND ( * ) := +IATN421-FND ( * )
            //*  090814 END
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Fnd.getValue("*")), new ExamineSearch(pnd_Num_Cde), new ExamineGivingIndex(pnd_A));                                         //Natural: EXAMINE #FND ( * ) FOR #NUM-CDE GIVING INDEX #A
        if (condition(pnd_A.greater(getZero())))                                                                                                                          //Natural: IF #A GT 0
        {
            pnd_Nec_Rte_Super1_Pnd_Nec_Ticker.setValue(pnd_Tkr.getValue(pnd_A));                                                                                          //Natural: ASSIGN #NEC-TICKER := #TKR ( #A )
            vw_ia_Rte.startDatabaseRead                                                                                                                                   //Natural: READ IA-RTE BY NEC-RTE-SUPER1 STARTING FROM #NEC-RTE-SUPER1
            (
            "READ01",
            new Wc[] { new Wc("NEC_RTE_SUPER1", ">=", pnd_Nec_Rte_Super1, WcType.BY) },
            new Oc[] { new Oc("NEC_RTE_SUPER1", "ASC") }
            );
            READ01:
            while (condition(vw_ia_Rte.readNextRow("READ01")))
            {
                if (condition(ia_Rte_Nec_Table_Cde.notEquals("RTE") || ia_Rte_Nec_Product_Cde.notEquals("IA") || ia_Rte_Nec_Ticker_Symbol.notEquals(pnd_Nec_Rte_Super1_Pnd_Nec_Ticker))) //Natural: IF NEC-TABLE-CDE NE 'RTE' OR NEC-PRODUCT-CDE NE 'IA' OR NEC-TICKER-SYMBOL NE #NEC-TICKER
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rate_Cde.setValue(ia_Rte_Nec_Rate_Cde);                                                                                                               //Natural: ASSIGN #RATE-CDE := NEC-RATE-CDE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
