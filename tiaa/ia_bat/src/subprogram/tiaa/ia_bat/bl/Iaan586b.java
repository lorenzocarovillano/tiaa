/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:25 AM
**        * FROM NATURAL SUBPROGRAM : Iaan586b
************************************************************
**        * FILE NAME            : Iaan586b.java
**        * CLASS NAME           : Iaan586b
**        * INSTANCE NAME        : Iaan586b
************************************************************
************************************************************************
* PROGRAM: IAAN586B
* DATE   : 01/29/99
* AUTHOR : ARI G
* DESC   : THIS SUBPROGRAM WILL WRITE OUT OVERPAYMENT RECORDS IN A WORK
*          FILE.
* HISTORY:
*  05/26/00 A.G.      ADDED PA SELECT CHANGES
* 04/02/12 J BREMER  RATE BASIS EXPANSION - JB01
* 04/15/14 O SOTTO INCLUDE PIN IN THE EXTRACT.  CHANGES MARKED 041514.
* 04/2017  RE-STOWED FOR IAAL200B PIN EXPANSION.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan586b extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIaaa586a pdaIaaa586a;
    private PdaIaaa299a pdaIaaa299a;
    private LdaIaal202h ldaIaal202h;
    private PdaIaapda_M pdaIaapda_M;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal200a ldaIaal200a;
    private LdaIaal205a ldaIaal205a;
    private LdaIaal201e ldaIaal201e;
    private LdaIaal201f ldaIaal201f;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Overpay_Id_Nbr;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_2;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_3;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_4;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Fund_Code_1_3;

    private DbsGroup pnd_Fund_Code_1_3__R_Field_5;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_1;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3;

    private DbsGroup pnd_Count;
    private DbsField pnd_Count_Pnd_Err_Cnt;
    private DbsField pnd_Count_Pnd_Rec_Cnt;
    private DbsField pnd_Count_Pnd_Install_Cnt;
    private DbsField pnd_Ws_Date;

    private DbsGroup pnd_Ws_Date__R_Field_6;
    private DbsField pnd_Ws_Date_Pnd_Ws_Year;
    private DbsField pnd_Ws_Date_Pnd_Ws_Mm;
    private DbsField pnd_Ws_Date_Pnd_Ws_Dd;
    private DbsField pnd_Skip_Month;
    private DbsField pnd_W_Issue_Date;
    private DbsField pnd_W_Option;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_A;

    private DbsGroup pnd_A__R_Field_7;
    private DbsField pnd_A_Pnd_A_A;
    private DbsField pnd_E;
    private DbsField pnd_B;
    private DbsField pnd_Cref;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Annu;
    private DbsField pnd_Unit;
    private DbsField pnd_Num;
    private DbsField pnd_Tab_Funds;
    private DbsField pnd_Tab_Units;
    private DbsField pnd_W_Tab_Funds;

    private DbsGroup pnd_W_Tab_Funds__R_Field_8;
    private DbsField pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1;
    private DbsField pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3;
    private DbsField pnd_Cref_Funds;
    private DbsField pnd_Tiaa_Funds;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_9;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N;

    private DbsGroup pnd_Issue_Date_A__R_Field_10;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Dd;

    private DbsGroup pnd_Issue_Date_A__R_Field_11;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Mm;

    private DbsGroup pnd_Issue_Date_A__R_Field_12;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd;
    private DbsField pnd_Xfr_Issue_Date_A;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_13;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_14;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_15;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_16;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd;
    private DbsField pnd_Install_Date_A;

    private DbsGroup pnd_Install_Date_A__R_Field_17;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_N;

    private DbsGroup pnd_Install_Date_A__R_Field_18;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Yyyymm;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Dd;

    private DbsGroup pnd_Install_Date_A__R_Field_19;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Yyyy;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Mm;

    private DbsGroup pnd_Install_Date_A__R_Field_20;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Mm;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Dd;

    private DbsGroup pnd_A26_Fields;
    private DbsField pnd_A26_Fields_Pnd_A26_Call_Type;
    private DbsField pnd_A26_Fields_Pnd_A26_Fnd_Cde;
    private DbsField pnd_A26_Fields_Pnd_A26_Reval_Meth;
    private DbsField pnd_A26_Fields_Pnd_A26_Req_Dte;
    private DbsField pnd_A26_Fields_Pnd_A26_Prtc_Dte;
    private DbsField pnd_A26_Fields_Pnd_Rc_Pgm;

    private DbsGroup pnd_A26_Fields__R_Field_21;
    private DbsField pnd_A26_Fields_Pnd_Pgm;
    private DbsField pnd_A26_Fields_Pnd_Rc;
    private DbsField pnd_A26_Fields_Pnd_Auv;
    private DbsField pnd_A26_Fields_Pnd_Auv_Ret_Dte;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Request_Month;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Particip_Month;
    private DbsField pnd_W_Option_Cde_Desc;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Rtn_Cde;
    private DbsField pnd_W_Amt;
    private DbsField pnd_Fund_Lst_Pd_Dte_Next;
    private DbsField pnd_W_Fund_Inverse_Lst_Pd_Dte_Next;
    private DbsField pnd_Historical_Last_Pd_Dte_Hold;
    private DbsField pnd_W_Install_Date;
    private DbsField pnd_W_File;
    private DbsField pnd_W_Inverse_Date;
    private DbsField pnd_Change_In_Record;
    private DbsField pnd_Finish_Processing;
    private DbsField pnd_Valid_Date;
    private DbsField pnd_Val_Meth;
    private DbsField pnd_Error;
    private DbsField pnd_W_Fund_Dte;
    private DbsField pnd_Present_Year;

    private DbsGroup pnd_Present_Year__R_Field_22;
    private DbsField pnd_Present_Year_Pnd_Present_Year_Yyyy;
    private DbsField pnd_Present_Year_Pnd_Present_Year_Mm;
    private DbsField pnd_Tiaa_No_Units;
    private DbsField pnd_W_Bottom_Year;

    private DbsGroup pnd_W_Bottom_Year__R_Field_23;
    private DbsField pnd_W_Bottom_Year_Pnd_W_Date_Yyyy;
    private DbsField pnd_W_Bottom_Year_Pnd_W_Date_Mm;
    private DbsField pnd_T_Fund;
    private DbsField pnd_T_Per_Amt_1;
    private DbsField pnd_T_Div_Amt_1;
    private DbsField pnd_T_Unt_Amt_1;
    private DbsField pnd_T_Per_Amt_2;
    private DbsField pnd_T_Div_Amt_2;
    private DbsField pnd_T_Unt_Amt_2;
    private DbsField pnd_W_Fund_3;

    private DbsGroup pnd_W_Fund_3__R_Field_24;
    private DbsField pnd_W_Fund_3_Pnd_W_Fund_1;
    private DbsField pnd_W_Fund_3_Pnd_W_Fund_2;
    private DbsField pnd_Sub_Fund_1;
    private DbsField pnd_Sub_Fund_2;
    private DbsField pnd_Sub_Tot;
    private DbsField pnd_W_Contract;
    private DbsField pnd_W_Payee_Cde;
    private DbsField pnd_W_Fund;
    private DbsField pnd_W_Desc;
    private DbsField pnd_W_Mode;
    private DbsField pnd_W_Date;
    private DbsField pnd_W_Guar_Payment;
    private DbsField pnd_W_Divd_Payment;
    private DbsField pnd_W_Unit_Payment;
    private DbsField pnd_W_Guar_Overpayment;
    private DbsField pnd_W_Divd_Overpayment;
    private DbsField pnd_W_Unit_Overpayment;
    private DbsField pnd_W_Msg;
    private DbsField pnd_Meth_Fund;
    private DbsField pnd_Desc;
    private DbsField pnd_Tc;
    private DbsField pnd_Len;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_25;
    private DbsField pnd_Date_A_Pnd_Date_N;

    private DbsGroup pnd_Date_A__R_Field_26;
    private DbsField pnd_Date_A_Pnd_Date_Yyyymm;
    private DbsField pnd_Date_A_Pnd_Date_Dd;

    private DbsGroup pnd_Date_A__R_Field_27;
    private DbsField pnd_Date_A_Pnd_Date_Yyyy;
    private DbsField pnd_Date_A_Pnd_Date_Mm;

    private DbsGroup pnd_Date_A__R_Field_28;
    private DbsField pnd_Date_A_Pnd_Date_Mm_A;

    private DbsGroup pnd_Date_A__R_Field_29;
    private DbsField pnd_Date_A_Pnd_Date_A8_Ccyy;
    private DbsField pnd_Date_A_Pnd_Date_A8_Mm;
    private DbsField pnd_Date_A_Pnd_Date_A8_Dd;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa299a = new PdaIaaa299a(localVariables);
        ldaIaal202h = new LdaIaal202h();
        registerRecord(ldaIaal202h);
        registerRecord(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1());
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaIaal205a = new LdaIaal205a();
        registerRecord(ldaIaal205a);
        registerRecord(ldaIaal205a.getVw_old_Tiaa_Rates());
        ldaIaal201e = new LdaIaal201e();
        registerRecord(ldaIaal201e);
        registerRecord(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());
        ldaIaal201f = new LdaIaal201f();
        registerRecord(ldaIaal201f);
        registerRecord(ldaIaal201f.getVw_iaa_Cref_Fund_Rcrd_1());

        // parameters
        parameters = new DbsRecord();
        pdaIaaa586a = new PdaIaaa586a(parameters);
        pnd_Overpay_Id_Nbr = parameters.newFieldInRecord("pnd_Overpay_Id_Nbr", "#OVERPAY-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Overpay_Id_Nbr.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_2", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_2.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_2.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr", 
            "#OLD-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee", "#OLD-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte", 
            "#OLD-INVERSE-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code", "#OLD-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_Fund_Code_1_3", "#FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_Fund_Code_1_3__R_Field_5 = localVariables.newGroupInRecord("pnd_Fund_Code_1_3__R_Field_5", "REDEFINE", pnd_Fund_Code_1_3);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_1 = pnd_Fund_Code_1_3__R_Field_5.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_1", "#FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3 = pnd_Fund_Code_1_3__R_Field_5.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3", "#FUND-CODE-2-3", FieldType.STRING, 
            2);

        pnd_Count = localVariables.newGroupInRecord("pnd_Count", "#COUNT");
        pnd_Count_Pnd_Err_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Err_Cnt", "#ERR-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Rec_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Install_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Install_Cnt", "#INSTALL-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Ws_Date = localVariables.newFieldInRecord("pnd_Ws_Date", "#WS-DATE", FieldType.NUMERIC, 8);

        pnd_Ws_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Date__R_Field_6", "REDEFINE", pnd_Ws_Date);
        pnd_Ws_Date_Pnd_Ws_Year = pnd_Ws_Date__R_Field_6.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Year", "#WS-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Date_Pnd_Ws_Mm = pnd_Ws_Date__R_Field_6.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Mm", "#WS-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Date_Pnd_Ws_Dd = pnd_Ws_Date__R_Field_6.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Dd", "#WS-DD", FieldType.NUMERIC, 2);
        pnd_Skip_Month = localVariables.newFieldInRecord("pnd_Skip_Month", "#SKIP-MONTH", FieldType.STRING, 1);
        pnd_W_Issue_Date = localVariables.newFieldInRecord("pnd_W_Issue_Date", "#W-ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_W_Option = localVariables.newFieldInRecord("pnd_W_Option", "#W-OPTION", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 6);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);

        pnd_A__R_Field_7 = localVariables.newGroupInRecord("pnd_A__R_Field_7", "REDEFINE", pnd_A);
        pnd_A_Pnd_A_A = pnd_A__R_Field_7.newFieldInGroup("pnd_A_Pnd_A_A", "#A-A", FieldType.STRING, 2);
        pnd_E = localVariables.newFieldInRecord("pnd_E", "#E", FieldType.NUMERIC, 2);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);
        pnd_Cref = localVariables.newFieldInRecord("pnd_Cref", "#CREF", FieldType.STRING, 1);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.STRING, 1);
        pnd_Annu = localVariables.newFieldInRecord("pnd_Annu", "#ANNU", FieldType.STRING, 1);
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 1);
        pnd_Num = localVariables.newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 2);
        pnd_Tab_Funds = localVariables.newFieldArrayInRecord("pnd_Tab_Funds", "#TAB-FUNDS", FieldType.STRING, 3, new DbsArrayController(1, 20));
        pnd_Tab_Units = localVariables.newFieldArrayInRecord("pnd_Tab_Units", "#TAB-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20));
        pnd_W_Tab_Funds = localVariables.newFieldInRecord("pnd_W_Tab_Funds", "#W-TAB-FUNDS", FieldType.STRING, 3);

        pnd_W_Tab_Funds__R_Field_8 = localVariables.newGroupInRecord("pnd_W_Tab_Funds__R_Field_8", "REDEFINE", pnd_W_Tab_Funds);
        pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1 = pnd_W_Tab_Funds__R_Field_8.newFieldInGroup("pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1", "#W-TAB-FUNDS-1", FieldType.STRING, 
            1);
        pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3 = pnd_W_Tab_Funds__R_Field_8.newFieldInGroup("pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3", "#W-TAB-FUNDS-2-3", FieldType.STRING, 
            2);
        pnd_Cref_Funds = localVariables.newFieldInRecord("pnd_Cref_Funds", "#CREF-FUNDS", FieldType.STRING, 1);
        pnd_Tiaa_Funds = localVariables.newFieldInRecord("pnd_Tiaa_Funds", "#TIAA-FUNDS", FieldType.STRING, 1);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Issue_Date_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_9", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N = pnd_Issue_Date_A__R_Field_9.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A__R_Field_10 = pnd_Issue_Date_A__R_Field_9.newGroupInGroup("pnd_Issue_Date_A__R_Field_10", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm = pnd_Issue_Date_A__R_Field_10.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm", "#ISSUE-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Issue_Date_A_Pnd_Issue_Date_Dd = pnd_Issue_Date_A__R_Field_10.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_11 = pnd_Issue_Date_A__R_Field_9.newGroupInGroup("pnd_Issue_Date_A__R_Field_11", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy = pnd_Issue_Date_A__R_Field_11.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_Mm = pnd_Issue_Date_A__R_Field_11.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_12 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_12", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy = pnd_Issue_Date_A__R_Field_12.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy", "#ISSUE-DATE-A8-CCYY", 
            FieldType.STRING, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm = pnd_Issue_Date_A__R_Field_12.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm", "#ISSUE-DATE-A8-MM", 
            FieldType.STRING, 2);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd = pnd_Issue_Date_A__R_Field_12.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd", "#ISSUE-DATE-A8-DD", 
            FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A = localVariables.newFieldInRecord("pnd_Xfr_Issue_Date_A", "#XFR-ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Xfr_Issue_Date_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_13", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N = pnd_Xfr_Issue_Date_A__R_Field_13.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N", "#XFR-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Xfr_Issue_Date_A__R_Field_14 = pnd_Xfr_Issue_Date_A__R_Field_13.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_14", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm = pnd_Xfr_Issue_Date_A__R_Field_14.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm", 
            "#XFR-ISSUE-DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd = pnd_Xfr_Issue_Date_A__R_Field_14.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd", "#XFR-ISSUE-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_15 = pnd_Xfr_Issue_Date_A__R_Field_13.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_15", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy = pnd_Xfr_Issue_Date_A__R_Field_15.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy", 
            "#XFR-ISSUE-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm = pnd_Xfr_Issue_Date_A__R_Field_15.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm", "#XFR-ISSUE-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_16 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_16", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy = pnd_Xfr_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy", 
            "#XFR-ISSUE-DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm = pnd_Xfr_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm", 
            "#XFR-ISSUE-DATE-A8-MM", FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd = pnd_Xfr_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd", 
            "#XFR-ISSUE-DATE-A8-DD", FieldType.STRING, 2);
        pnd_Install_Date_A = localVariables.newFieldInRecord("pnd_Install_Date_A", "#INSTALL-DATE-A", FieldType.STRING, 8);

        pnd_Install_Date_A__R_Field_17 = localVariables.newGroupInRecord("pnd_Install_Date_A__R_Field_17", "REDEFINE", pnd_Install_Date_A);
        pnd_Install_Date_A_Pnd_Install_Date_N = pnd_Install_Date_A__R_Field_17.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_N", "#INSTALL-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Install_Date_A__R_Field_18 = pnd_Install_Date_A__R_Field_17.newGroupInGroup("pnd_Install_Date_A__R_Field_18", "REDEFINE", pnd_Install_Date_A_Pnd_Install_Date_N);
        pnd_Install_Date_A_Pnd_Install_Date_Yyyymm = pnd_Install_Date_A__R_Field_18.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Yyyymm", "#INSTALL-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Install_Date_A_Pnd_Install_Date_Dd = pnd_Install_Date_A__R_Field_18.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Dd", "#INSTALL-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Install_Date_A__R_Field_19 = pnd_Install_Date_A__R_Field_17.newGroupInGroup("pnd_Install_Date_A__R_Field_19", "REDEFINE", pnd_Install_Date_A_Pnd_Install_Date_N);
        pnd_Install_Date_A_Pnd_Install_Date_Yyyy = pnd_Install_Date_A__R_Field_19.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Yyyy", "#INSTALL-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Install_Date_A_Pnd_Install_Date_Mm = pnd_Install_Date_A__R_Field_19.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Mm", "#INSTALL-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Install_Date_A__R_Field_20 = localVariables.newGroupInRecord("pnd_Install_Date_A__R_Field_20", "REDEFINE", pnd_Install_Date_A);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy = pnd_Install_Date_A__R_Field_20.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy", "#INSTALL-DATE-A8-CCYY", 
            FieldType.STRING, 4);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Mm = pnd_Install_Date_A__R_Field_20.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Mm", "#INSTALL-DATE-A8-MM", 
            FieldType.STRING, 2);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Dd = pnd_Install_Date_A__R_Field_20.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Dd", "#INSTALL-DATE-A8-DD", 
            FieldType.STRING, 2);

        pnd_A26_Fields = localVariables.newGroupInRecord("pnd_A26_Fields", "#A26-FIELDS");
        pnd_A26_Fields_Pnd_A26_Call_Type = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Call_Type", "#A26-CALL-TYPE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Fnd_Cde = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Fnd_Cde", "#A26-FND-CDE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Reval_Meth = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Reval_Meth", "#A26-REVAL-METH", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Req_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Req_Dte", "#A26-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_A26_Prtc_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Prtc_Dte", "#A26-PRTC-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Rc_Pgm = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Rc_Pgm", "#RC-PGM", FieldType.STRING, 11);

        pnd_A26_Fields__R_Field_21 = pnd_A26_Fields.newGroupInGroup("pnd_A26_Fields__R_Field_21", "REDEFINE", pnd_A26_Fields_Pnd_Rc_Pgm);
        pnd_A26_Fields_Pnd_Pgm = pnd_A26_Fields__R_Field_21.newFieldInGroup("pnd_A26_Fields_Pnd_Pgm", "#PGM", FieldType.STRING, 8);
        pnd_A26_Fields_Pnd_Rc = pnd_A26_Fields__R_Field_21.newFieldInGroup("pnd_A26_Fields_Pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_A26_Fields_Pnd_Auv = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_A26_Fields_Pnd_Auv_Ret_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Days_In_Request_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_A26_Fields_Pnd_Days_In_Particip_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_W_Option_Cde_Desc = localVariables.newFieldInRecord("pnd_W_Option_Cde_Desc", "#W-OPTION-CDE-DESC", FieldType.STRING, 20);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Rtn_Cde = localVariables.newFieldInRecord("pnd_Rtn_Cde", "#RTN-CDE", FieldType.NUMERIC, 2);
        pnd_W_Amt = localVariables.newFieldInRecord("pnd_W_Amt", "#W-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Fund_Lst_Pd_Dte_Next = localVariables.newFieldInRecord("pnd_Fund_Lst_Pd_Dte_Next", "#FUND-LST-PD-DTE-NEXT", FieldType.NUMERIC, 8);
        pnd_W_Fund_Inverse_Lst_Pd_Dte_Next = localVariables.newFieldInRecord("pnd_W_Fund_Inverse_Lst_Pd_Dte_Next", "#W-FUND-INVERSE-LST-PD-DTE-NEXT", 
            FieldType.NUMERIC, 8);
        pnd_Historical_Last_Pd_Dte_Hold = localVariables.newFieldInRecord("pnd_Historical_Last_Pd_Dte_Hold", "#HISTORICAL-LAST-PD-DTE-HOLD", FieldType.NUMERIC, 
            8);
        pnd_W_Install_Date = localVariables.newFieldInRecord("pnd_W_Install_Date", "#W-INSTALL-DATE", FieldType.STRING, 8);
        pnd_W_File = localVariables.newFieldInRecord("pnd_W_File", "#W-FILE", FieldType.STRING, 1);
        pnd_W_Inverse_Date = localVariables.newFieldInRecord("pnd_W_Inverse_Date", "#W-INVERSE-DATE", FieldType.NUMERIC, 8);
        pnd_Change_In_Record = localVariables.newFieldInRecord("pnd_Change_In_Record", "#CHANGE-IN-RECORD", FieldType.STRING, 1);
        pnd_Finish_Processing = localVariables.newFieldInRecord("pnd_Finish_Processing", "#FINISH-PROCESSING", FieldType.STRING, 1);
        pnd_Valid_Date = localVariables.newFieldInRecord("pnd_Valid_Date", "#VALID-DATE", FieldType.STRING, 1);
        pnd_Val_Meth = localVariables.newFieldInRecord("pnd_Val_Meth", "#VAL-METH", FieldType.STRING, 1);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 1);
        pnd_W_Fund_Dte = localVariables.newFieldInRecord("pnd_W_Fund_Dte", "#W-FUND-DTE", FieldType.NUMERIC, 8);
        pnd_Present_Year = localVariables.newFieldInRecord("pnd_Present_Year", "#PRESENT-YEAR", FieldType.NUMERIC, 8);

        pnd_Present_Year__R_Field_22 = localVariables.newGroupInRecord("pnd_Present_Year__R_Field_22", "REDEFINE", pnd_Present_Year);
        pnd_Present_Year_Pnd_Present_Year_Yyyy = pnd_Present_Year__R_Field_22.newFieldInGroup("pnd_Present_Year_Pnd_Present_Year_Yyyy", "#PRESENT-YEAR-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Present_Year_Pnd_Present_Year_Mm = pnd_Present_Year__R_Field_22.newFieldInGroup("pnd_Present_Year_Pnd_Present_Year_Mm", "#PRESENT-YEAR-MM", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_No_Units = localVariables.newFieldInRecord("pnd_Tiaa_No_Units", "#TIAA-NO-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_W_Bottom_Year = localVariables.newFieldInRecord("pnd_W_Bottom_Year", "#W-BOTTOM-YEAR", FieldType.NUMERIC, 8);

        pnd_W_Bottom_Year__R_Field_23 = localVariables.newGroupInRecord("pnd_W_Bottom_Year__R_Field_23", "REDEFINE", pnd_W_Bottom_Year);
        pnd_W_Bottom_Year_Pnd_W_Date_Yyyy = pnd_W_Bottom_Year__R_Field_23.newFieldInGroup("pnd_W_Bottom_Year_Pnd_W_Date_Yyyy", "#W-DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_W_Bottom_Year_Pnd_W_Date_Mm = pnd_W_Bottom_Year__R_Field_23.newFieldInGroup("pnd_W_Bottom_Year_Pnd_W_Date_Mm", "#W-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_T_Fund = localVariables.newFieldArrayInRecord("pnd_T_Fund", "#T-FUND", FieldType.STRING, 3, new DbsArrayController(1, 40));
        pnd_T_Per_Amt_1 = localVariables.newFieldArrayInRecord("pnd_T_Per_Amt_1", "#T-PER-AMT-1", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            40));
        pnd_T_Div_Amt_1 = localVariables.newFieldArrayInRecord("pnd_T_Div_Amt_1", "#T-DIV-AMT-1", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            40));
        pnd_T_Unt_Amt_1 = localVariables.newFieldArrayInRecord("pnd_T_Unt_Amt_1", "#T-UNT-AMT-1", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            40));
        pnd_T_Per_Amt_2 = localVariables.newFieldArrayInRecord("pnd_T_Per_Amt_2", "#T-PER-AMT-2", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            40));
        pnd_T_Div_Amt_2 = localVariables.newFieldArrayInRecord("pnd_T_Div_Amt_2", "#T-DIV-AMT-2", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            40));
        pnd_T_Unt_Amt_2 = localVariables.newFieldArrayInRecord("pnd_T_Unt_Amt_2", "#T-UNT-AMT-2", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            40));
        pnd_W_Fund_3 = localVariables.newFieldInRecord("pnd_W_Fund_3", "#W-FUND-3", FieldType.STRING, 3);

        pnd_W_Fund_3__R_Field_24 = localVariables.newGroupInRecord("pnd_W_Fund_3__R_Field_24", "REDEFINE", pnd_W_Fund_3);
        pnd_W_Fund_3_Pnd_W_Fund_1 = pnd_W_Fund_3__R_Field_24.newFieldInGroup("pnd_W_Fund_3_Pnd_W_Fund_1", "#W-FUND-1", FieldType.STRING, 1);
        pnd_W_Fund_3_Pnd_W_Fund_2 = pnd_W_Fund_3__R_Field_24.newFieldInGroup("pnd_W_Fund_3_Pnd_W_Fund_2", "#W-FUND-2", FieldType.STRING, 2);
        pnd_Sub_Fund_1 = localVariables.newFieldInRecord("pnd_Sub_Fund_1", "#SUB-FUND-1", FieldType.NUMERIC, 3);
        pnd_Sub_Fund_2 = localVariables.newFieldInRecord("pnd_Sub_Fund_2", "#SUB-FUND-2", FieldType.NUMERIC, 3);
        pnd_Sub_Tot = localVariables.newFieldInRecord("pnd_Sub_Tot", "#SUB-TOT", FieldType.NUMERIC, 3);
        pnd_W_Contract = localVariables.newFieldInRecord("pnd_W_Contract", "#W-CONTRACT", FieldType.STRING, 10);
        pnd_W_Payee_Cde = localVariables.newFieldInRecord("pnd_W_Payee_Cde", "#W-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_W_Fund = localVariables.newFieldInRecord("pnd_W_Fund", "#W-FUND", FieldType.STRING, 3);
        pnd_W_Desc = localVariables.newFieldInRecord("pnd_W_Desc", "#W-DESC", FieldType.STRING, 20);
        pnd_W_Mode = localVariables.newFieldInRecord("pnd_W_Mode", "#W-MODE", FieldType.NUMERIC, 3);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.NUMERIC, 8);
        pnd_W_Guar_Payment = localVariables.newFieldInRecord("pnd_W_Guar_Payment", "#W-GUAR-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Divd_Payment = localVariables.newFieldInRecord("pnd_W_Divd_Payment", "#W-DIVD-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Unit_Payment = localVariables.newFieldInRecord("pnd_W_Unit_Payment", "#W-UNIT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Guar_Overpayment = localVariables.newFieldInRecord("pnd_W_Guar_Overpayment", "#W-GUAR-OVERPAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Divd_Overpayment = localVariables.newFieldInRecord("pnd_W_Divd_Overpayment", "#W-DIVD-OVERPAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Unit_Overpayment = localVariables.newFieldInRecord("pnd_W_Unit_Overpayment", "#W-UNIT-OVERPAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Msg = localVariables.newFieldInRecord("pnd_W_Msg", "#W-MSG", FieldType.STRING, 3);
        pnd_Meth_Fund = localVariables.newFieldInRecord("pnd_Meth_Fund", "#METH-FUND", FieldType.STRING, 5);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 35);
        pnd_Tc = localVariables.newFieldInRecord("pnd_Tc", "#TC", FieldType.STRING, 4);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_25 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_25", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_25.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);

        pnd_Date_A__R_Field_26 = pnd_Date_A__R_Field_25.newGroupInGroup("pnd_Date_A__R_Field_26", "REDEFINE", pnd_Date_A_Pnd_Date_N);
        pnd_Date_A_Pnd_Date_Yyyymm = pnd_Date_A__R_Field_26.newFieldInGroup("pnd_Date_A_Pnd_Date_Yyyymm", "#DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Date_A_Pnd_Date_Dd = pnd_Date_A__R_Field_26.newFieldInGroup("pnd_Date_A_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        pnd_Date_A__R_Field_27 = pnd_Date_A__R_Field_25.newGroupInGroup("pnd_Date_A__R_Field_27", "REDEFINE", pnd_Date_A_Pnd_Date_N);
        pnd_Date_A_Pnd_Date_Yyyy = pnd_Date_A__R_Field_27.newFieldInGroup("pnd_Date_A_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Date_A_Pnd_Date_Mm = pnd_Date_A__R_Field_27.newFieldInGroup("pnd_Date_A_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);

        pnd_Date_A__R_Field_28 = pnd_Date_A__R_Field_27.newGroupInGroup("pnd_Date_A__R_Field_28", "REDEFINE", pnd_Date_A_Pnd_Date_Mm);
        pnd_Date_A_Pnd_Date_Mm_A = pnd_Date_A__R_Field_28.newFieldInGroup("pnd_Date_A_Pnd_Date_Mm_A", "#DATE-MM-A", FieldType.STRING, 2);

        pnd_Date_A__R_Field_29 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_29", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_A8_Ccyy = pnd_Date_A__R_Field_29.newFieldInGroup("pnd_Date_A_Pnd_Date_A8_Ccyy", "#DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Date_A_Pnd_Date_A8_Mm = pnd_Date_A__R_Field_29.newFieldInGroup("pnd_Date_A_Pnd_Date_A8_Mm", "#DATE-A8-MM", FieldType.STRING, 2);
        pnd_Date_A_Pnd_Date_A8_Dd = pnd_Date_A__R_Field_29.newFieldInGroup("pnd_Date_A_Pnd_Date_A8_Dd", "#DATE-A8-DD", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal202h.initializeValues();
        ldaIaal200b.initializeValues();
        ldaIaal200a.initializeValues();
        ldaIaal205a.initializeValues();
        ldaIaal201e.initializeValues();
        ldaIaal201f.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan586b() throws Exception
    {
        super("Iaan586b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
                                                                                                                                                                          //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: PERFORM #PROCESS-CNTRCT
        sub_Pnd_Process_Cntrct();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Iaan004.class , getCurrentProcessState(), pdaIaaa586a.getIaaa460a_Pnd_Option_Cde(), pnd_W_Option_Cde_Desc);                                       //Natural: CALLNAT 'IAAN004' #OPTION-CDE #W-OPTION-CDE-DESC
        if (condition(Global.isEscape())) return;
        F1:                                                                                                                                                               //Natural: FOR #I = 1 TO #INSTLMNTS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaIaaa586a.getIaaa460a_Pnd_Instlmnts())); pnd_I.nadd(1))
        {
            //*  WRITE 'IN IAAN586B' '=' #INSTLMNTS
            //* *F1. FOR #I = #INSTLMNTS TO 1 STEP -1
            //* *  WRITE '=' #TABLE-INSTL-DATE(#I)
            if (condition(pdaIaaa586a.getIaaa460a_Pnd_Table_File().getValue(pnd_I).equals("F")))                                                                          //Natural: IF #TABLE-FILE ( #I ) = 'F'
            {
                                                                                                                                                                          //Natural: PERFORM #READ-FUND
                sub_Pnd_Read_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaIaaa586a.getIaaa460a_Pnd_Table_File().getValue(pnd_I).equals("H")))                                                                      //Natural: IF #TABLE-FILE ( #I ) = 'H'
                {
                                                                                                                                                                          //Natural: PERFORM #READ-HISTORY
                    sub_Pnd_Read_History();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "ERROR IN IAAN460 TABLE-FILE FIELD",pdaIaaa586a.getIaaa460a_Pnd_Table_File().getValue(pnd_I));                                  //Natural: WRITE 'ERROR IN IAAN460 TABLE-FILE FIELD' #TABLE-FILE ( #I )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
            sub_Pnd_Write_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-FUND
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-HISTORY
        //*        COMPUTE #W-FUND-DTE = 100000000 - FUND-INVRSE-LST-PD-DTE
        //*        COMPUTE #W-FUND-DTE = 100000000 - FUND-INVRSE-LST-PD-DTE
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-SINGLE-BYTE-FUND
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-UNIT-VALUE
        //* ******************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-RECORD
        //* ************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CNTRCT
        //* ***********************************************************************
    }
    private void sub_Pnd_Read_Fund() throws Exception                                                                                                                     //Natural: #READ-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Sub_Fund_1.reset();                                                                                                                                           //Natural: RESET #SUB-FUND-1
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Payee_Cde_1());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-PAYEE-CDE-1
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                pnd_Sub_Fund_1.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #SUB-FUND-1
                pnd_Fund_Code_1_3.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                                      //Natural: MOVE TIAA-CMPNY-FUND-CDE TO #FUND-CODE-1-3
                pnd_T_Fund.getValue(pnd_Sub_Fund_1).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                    //Natural: MOVE TIAA-CMPNY-FUND-CDE TO #T-FUND ( #SUB-FUND-1 )
                //*    WRITE '=' #FUND-CODE-1-3 '=' TIAA-TOT-PER-AMT '=' TIAA-TOT-DIV-AMT
                if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                             //Natural: IF #FUND-CODE-1 = 'T'
                {
                    pnd_T_Per_Amt_1.getValue(pnd_Sub_Fund_1).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                              //Natural: MOVE TIAA-TOT-PER-AMT TO #T-PER-AMT-1 ( #SUB-FUND-1 )
                    pnd_T_Div_Amt_1.getValue(pnd_Sub_Fund_1).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                              //Natural: MOVE TIAA-TOT-DIV-AMT TO #T-DIV-AMT-1 ( #SUB-FUND-1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Instl_Date().getValue(pnd_I));                                                              //Natural: ASSIGN #W-FUND-DTE := #TABLE-INSTL-DATE ( #I )
                    if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                        //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                    {
                        pnd_T_Unt_Amt_1.getValue(pnd_Sub_Fund_1).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                          //Natural: MOVE TIAA-TOT-PER-AMT TO #T-UNT-AMT-1 ( #SUB-FUND-1 )
                        pnd_T_Div_Amt_1.getValue(pnd_Sub_Fund_1).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-1 ( #SUB-FUND-1 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3);                                                                                         //Natural: ASSIGN #FUND-2 := #FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                    //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-UNITS-CNT ( 1 ) * #AUV
                        pnd_T_Unt_Amt_1.getValue(pnd_Sub_Fund_1).setValue(pnd_W_Amt);                                                                                     //Natural: MOVE #W-AMT TO #T-UNT-AMT-1 ( #SUB-FUND-1 )
                        pnd_T_Div_Amt_1.getValue(pnd_Sub_Fund_1).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-1 ( #SUB-FUND-1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Sub_Fund_2.reset();                                                                                                                                           //Natural: RESET #SUB-FUND-2
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Payee_Cde_2());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-PAYEE-CDE-2
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R2",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R2:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R2")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                pnd_Sub_Fund_2.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #SUB-FUND-2
                pnd_Fund_Code_1_3.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                                      //Natural: MOVE TIAA-CMPNY-FUND-CDE TO #FUND-CODE-1-3
                //*    WRITE '=' #FUND-CODE-1-3 '=' TIAA-TOT-PER-AMT '=' TIAA-TOT-DIV-AMT
                if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                             //Natural: IF #FUND-CODE-1 = 'T'
                {
                    pnd_T_Per_Amt_2.getValue(pnd_Sub_Fund_2).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                              //Natural: MOVE TIAA-TOT-PER-AMT TO #T-PER-AMT-2 ( #SUB-FUND-2 )
                    pnd_T_Div_Amt_2.getValue(pnd_Sub_Fund_2).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                              //Natural: MOVE TIAA-TOT-DIV-AMT TO #T-DIV-AMT-2 ( #SUB-FUND-2 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Instl_Date().getValue(pnd_I));                                                              //Natural: ASSIGN #W-FUND-DTE := #TABLE-INSTL-DATE ( #I )
                    if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                        //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                    {
                        pnd_T_Unt_Amt_2.getValue(pnd_Sub_Fund_2).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                          //Natural: MOVE TIAA-TOT-PER-AMT TO #T-UNT-AMT-2 ( #SUB-FUND-2 )
                        pnd_T_Div_Amt_2.getValue(pnd_Sub_Fund_2).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-2 ( #SUB-FUND-2 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3);                                                                                         //Natural: ASSIGN #FUND-2 := #FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                    //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-UNITS-CNT ( 1 ) * #AUV
                        pnd_T_Unt_Amt_2.getValue(pnd_Sub_Fund_2).setValue(pnd_W_Amt);                                                                                     //Natural: MOVE #W-AMT TO #T-UNT-AMT-2 ( #SUB-FUND-2 )
                        pnd_T_Div_Amt_2.getValue(pnd_Sub_Fund_2).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-2 ( #SUB-FUND-2 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_History() throws Exception                                                                                                                  //Natural: #READ-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Sub_Fund_1.reset();                                                                                                                                           //Natural: RESET #SUB-FUND-1
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct());                                                                   //Natural: ASSIGN #OLD-CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Payee_Cde_1());                                                                 //Natural: ASSIGN #OLD-CNTRCT-PAYEE := #W-PAYEE-CDE-1
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Inv_Date().getValue(pnd_I));                                              //Natural: ASSIGN #OLD-INVERSE-PD-DTE := #TABLE-INV-DATE ( #I )
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code.setValue(" ");                                                                                                            //Natural: ASSIGN #OLD-FUND-CODE := ' '
        ldaIaal205a.getVw_old_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ OLD-TIAA-RATES BY CNTRCT-PY-DTE-KEY STARTING FROM #CNTRCT-PY-DTE-KEY
        (
        "R3",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_Cntrct_Py_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") }
        );
        R3:
        while (condition(ldaIaal205a.getVw_old_Tiaa_Rates().readNextRow("R3")))
        {
            if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr) && ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Payee_Cde().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee)  //Natural: IF OLD-TIAA-RATES.CNTRCT-PPCN-NBR = #OLD-CNTRCT-PPCN-NBR AND OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = #OLD-CNTRCT-PAYEE AND OLD-TIAA-RATES.FUND-INVRSE-LST-PD-DTE = #OLD-INVERSE-PD-DTE
                && ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte)))
            {
                pnd_Sub_Fund_1.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #SUB-FUND-1
                pnd_Fund_Code_1_3.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde());                                                                               //Natural: MOVE OLD-TIAA-RATES.CMPNY-FUND-CDE TO #FUND-CODE-1-3
                pnd_T_Fund.getValue(pnd_Sub_Fund_1).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde());                                                             //Natural: MOVE OLD-TIAA-RATES.CMPNY-FUND-CDE TO #T-FUND ( #SUB-FUND-1 )
                if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                             //Natural: IF #FUND-CODE-1 = 'T'
                {
                    pnd_T_Per_Amt_1.getValue(pnd_Sub_Fund_1).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                                //Natural: MOVE CNTRCT-TOT-PER-AMT TO #T-PER-AMT-1 ( #SUB-FUND-1 )
                    pnd_T_Div_Amt_1.getValue(pnd_Sub_Fund_1).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt());                                                //Natural: MOVE CNTRCT-TOT-DIV-AMT TO #T-DIV-AMT-1 ( #SUB-FUND-1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Instl_Date().getValue(pnd_I));                                                              //Natural: ASSIGN #W-FUND-DTE := #TABLE-INSTL-DATE ( #I )
                    if (condition(((pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")) && pnd_W_Fund_Dte.greater(19970401)))) //Natural: IF ( #FUND-CODE-1 = 'U' OR = '2' ) AND #W-FUND-DTE > 19970401
                    {
                        pnd_T_Unt_Amt_1.getValue(pnd_Sub_Fund_1).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                            //Natural: MOVE CNTRCT-TOT-PER-AMT TO #T-UNT-AMT-1 ( #SUB-FUND-1 )
                        pnd_T_Div_Amt_1.getValue(pnd_Sub_Fund_1).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-1 ( #SUB-FUND-1 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3);                                                                                         //Natural: ASSIGN #FUND-2 := #FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                    //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal205a.getOld_Tiaa_Rates_Tiaa_No_Units().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-NO-UNITS ( 1 ) * #AUV
                        //*           WRITE '=' #W-AMT '=' TIAA-NO-UNITS(1) '=' #AUV
                        pnd_T_Unt_Amt_1.getValue(pnd_Sub_Fund_1).setValue(pnd_W_Amt);                                                                                     //Natural: MOVE #W-AMT TO #T-UNT-AMT-1 ( #SUB-FUND-1 )
                        pnd_T_Div_Amt_1.getValue(pnd_Sub_Fund_1).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-1 ( #SUB-FUND-1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Sub_Fund_2.reset();                                                                                                                                           //Natural: RESET #SUB-FUND-2
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct());                                                                   //Natural: ASSIGN #OLD-CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Payee_Cde_2());                                                                 //Natural: ASSIGN #OLD-CNTRCT-PAYEE := #W-PAYEE-CDE-2
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Inv_Date().getValue(pnd_I));                                              //Natural: ASSIGN #OLD-INVERSE-PD-DTE := #TABLE-INV-DATE ( #I )
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code.setValue(" ");                                                                                                            //Natural: ASSIGN #OLD-FUND-CODE := ' '
        ldaIaal205a.getVw_old_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ OLD-TIAA-RATES BY CNTRCT-PY-DTE-KEY STARTING FROM #CNTRCT-PY-DTE-KEY
        (
        "R4",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_Cntrct_Py_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") }
        );
        R4:
        while (condition(ldaIaal205a.getVw_old_Tiaa_Rates().readNextRow("R4")))
        {
            if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr) && ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Payee_Cde().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee)  //Natural: IF OLD-TIAA-RATES.CNTRCT-PPCN-NBR = #OLD-CNTRCT-PPCN-NBR AND OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = #OLD-CNTRCT-PAYEE AND OLD-TIAA-RATES.FUND-INVRSE-LST-PD-DTE = #OLD-INVERSE-PD-DTE
                && ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte)))
            {
                pnd_Sub_Fund_2.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #SUB-FUND-2
                pnd_Fund_Code_1_3.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde());                                                                               //Natural: MOVE OLD-TIAA-RATES.CMPNY-FUND-CDE TO #FUND-CODE-1-3
                if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                             //Natural: IF #FUND-CODE-1 = 'T'
                {
                    pnd_T_Per_Amt_2.getValue(pnd_Sub_Fund_2).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                                //Natural: MOVE CNTRCT-TOT-PER-AMT TO #T-PER-AMT-2 ( #SUB-FUND-2 )
                    pnd_T_Div_Amt_2.getValue(pnd_Sub_Fund_2).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt());                                                //Natural: MOVE CNTRCT-TOT-DIV-AMT TO #T-DIV-AMT-2 ( #SUB-FUND-2 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Instl_Date().getValue(pnd_I));                                                              //Natural: ASSIGN #W-FUND-DTE := #TABLE-INSTL-DATE ( #I )
                    if (condition(((pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")) && pnd_W_Fund_Dte.greater(19970401)))) //Natural: IF #FUND-CODE-1 = 'U' OR = '2' AND #W-FUND-DTE > 19970401
                    {
                        pnd_T_Unt_Amt_2.getValue(pnd_Sub_Fund_2).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                            //Natural: MOVE CNTRCT-TOT-PER-AMT TO #T-UNT-AMT-2 ( #SUB-FUND-2 )
                        pnd_T_Div_Amt_2.getValue(pnd_Sub_Fund_2).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-2 ( #SUB-FUND-2 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3);                                                                                         //Natural: ASSIGN #FUND-2 := #FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R4"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                    //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R4"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal205a.getOld_Tiaa_Rates_Tiaa_No_Units().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-NO-UNITS ( 1 ) * #AUV
                        pnd_T_Unt_Amt_2.getValue(pnd_Sub_Fund_2).setValue(pnd_W_Amt);                                                                                     //Natural: MOVE #W-AMT TO #T-UNT-AMT-2 ( #SUB-FUND-2 )
                        pnd_T_Div_Amt_2.getValue(pnd_Sub_Fund_2).setValue(0);                                                                                             //Natural: MOVE 0 TO #T-DIV-AMT-2 ( #SUB-FUND-2 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R4;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R4. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Get_Single_Byte_Fund() throws Exception                                                                                                          //Natural: #GET-SINGLE-BYTE-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_1.reset();                                                                                                                                               //Natural: RESET #FUND-1
        //* * CALLNAT 'IAAN0501' #FUND-1 #FUND-2 #RTN-CDE
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtn_Cde);                                                                  //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTN-CDE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Get_Unit_Value() throws Exception                                                                                                                //Natural: #GET-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_A26_Fields_Pnd_Auv.reset();                                                                                                                                   //Natural: RESET #AUV #AUV-RET-DTE #RC #A26-PRTC-DTE
        pnd_A26_Fields_Pnd_Auv_Ret_Dte.reset();
        pnd_A26_Fields_Pnd_Rc.reset();
        pnd_A26_Fields_Pnd_A26_Prtc_Dte.reset();
        pnd_A26_Fields_Pnd_A26_Call_Type.setValue("P");                                                                                                                   //Natural: ASSIGN #A26-CALL-TYPE := 'P'
        pnd_A26_Fields_Pnd_A26_Fnd_Cde.setValue(pnd_Fund_1);                                                                                                              //Natural: ASSIGN #A26-FND-CDE := #FUND-1
        pnd_A26_Fields_Pnd_A26_Reval_Meth.setValue(pnd_Val_Meth);                                                                                                         //Natural: ASSIGN #A26-REVAL-METH := #VAL-METH
        pnd_A26_Fields_Pnd_A26_Req_Dte.setValue(pnd_W_Fund_Dte);                                                                                                          //Natural: ASSIGN #A26-REQ-DTE := #W-FUND-DTE
        pnd_A26_Fields_Pnd_A26_Prtc_Dte.setValue(pnd_W_Issue_Date);                                                                                                       //Natural: ASSIGN #A26-PRTC-DTE := #W-ISSUE-DATE
        //*  WRITE #A26-FIELDS
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pnd_A26_Fields);                                                                                        //Natural: CALLNAT 'AIAN026' #A26-FIELDS
        if (condition(Global.isEscape())) return;
        //*  #RETURN-FLDS                                        /* JB01
        //*  #RC #AUV #AUV-RET-DTE
        //*  #DAYS-IN-REQUEST-MONTH
        //*  #DAYS-IN-PARTICIP-MONTH
        //*  WRITE '=' #A26-FIELDS
        //*  WRITE '=' #RC #AUV #AUV-RET-DTE
        if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero())))                                                                                                          //Natural: IF #RC > 0
        {
            getReports().write(0, NEWLINE,"ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ",pnd_A26_Fields_Pnd_Rc_Pgm);                                                       //Natural: WRITE / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RC-PGM
            if (Global.isEscape()) return;
            getReports().write(0, "CONTRACT NUMBER =>",pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct(),pdaIaaa586a.getIaaa460a_Pnd_W_Payee_Cde_1());                               //Natural: WRITE 'CONTRACT NUMBER =>' #W-CNTRCT #W-PAYEE-CDE-1
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE #AUV TO #W-AUV
    }
    private void sub_Pnd_Write_Record() throws Exception                                                                                                                  //Natural: #WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE '=' #SUB-FUND-1 '=' #SUB-FUND-2
        //*  IF #SUB-FUND-1 NE #SUB-FUND-2
        //*   MOVE 'Y' TO #ERROR
        //*   WRITE 'MISMATCH IN FUNDS IN 01 AND 02 RECORDS FOR CONTRACT' #W-CNTRCT
        //*   ESCAPE ROUTINE
        //*  END-IF
        pnd_Ws_Date.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Instl_Date().getValue(pnd_I));                                                                             //Natural: MOVE #TABLE-INSTL-DATE ( #I ) TO #WS-DATE
        pnd_Skip_Month.reset();                                                                                                                                           //Natural: RESET #SKIP-MONTH
        FQ:                                                                                                                                                               //Natural: FOR #E = 1 TO 12
        for (pnd_E.setValue(1); condition(pnd_E.lessOrEqual(12)); pnd_E.nadd(1))
        {
            if (condition(pdaIaaa586a.getIaaa460a_Pnd_Month_Field().getValue(pdaIaaa586a.getIaaa460a_Pnd_Year_Num(),pnd_E).equals(pnd_Ws_Date_Pnd_Ws_Mm)))                //Natural: IF #MONTH-FIELD ( #YEAR-NUM,#E ) = #WS-MM
            {
                if (condition(pdaIaaa586a.getIaaa460a_Pnd_Delete_Field().getValue(pdaIaaa586a.getIaaa460a_Pnd_Year_Num(),pnd_E).equals("D")))                             //Natural: IF #DELETE-FIELD ( #YEAR-NUM,#E ) = 'D'
                {
                    pnd_Skip_Month.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #SKIP-MONTH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Skip_Month.equals("Y")))                                                                                                                        //Natural: IF #SKIP-MONTH = 'Y'
        {
            pnd_W_Msg.setValue("DEL");                                                                                                                                    //Natural: MOVE 'DEL' TO #W-MSG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Msg.setValue(" ");                                                                                                                                      //Natural: MOVE ' ' TO #W-MSG
        }                                                                                                                                                                 //Natural: END-IF
        F8:                                                                                                                                                               //Natural: FOR #J = 1 TO #SUB-FUND-1
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Sub_Fund_1)); pnd_J.nadd(1))
        {
            pnd_Sub_Tot.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #SUB-TOT
            //*      WRITE  '=' #SUB-TOT
            pdaIaaa586a.getIaaa460a_Pnd_Ov_Per_Amt().getValue(pnd_Sub_Tot).compute(new ComputeParameters(false, pdaIaaa586a.getIaaa460a_Pnd_Ov_Per_Amt().getValue(pnd_Sub_Tot)),  //Natural: COMPUTE #OV-PER-AMT ( #SUB-TOT ) = #T-PER-AMT-1 ( #J ) - #T-PER-AMT-2 ( #J )
                pnd_T_Per_Amt_1.getValue(pnd_J).subtract(pnd_T_Per_Amt_2.getValue(pnd_J)));
            pdaIaaa586a.getIaaa460a_Pnd_Ov_Div_Amt().getValue(pnd_Sub_Tot).compute(new ComputeParameters(false, pdaIaaa586a.getIaaa460a_Pnd_Ov_Div_Amt().getValue(pnd_Sub_Tot)),  //Natural: COMPUTE #OV-DIV-AMT ( #SUB-TOT ) = #T-DIV-AMT-1 ( #J ) - #T-DIV-AMT-2 ( #J )
                pnd_T_Div_Amt_1.getValue(pnd_J).subtract(pnd_T_Div_Amt_2.getValue(pnd_J)));
            //*  KEEP AS #I
            pdaIaaa586a.getIaaa460a_Pnd_Ov_Unt_Amt().getValue(pnd_Sub_Tot).compute(new ComputeParameters(false, pdaIaaa586a.getIaaa460a_Pnd_Ov_Unt_Amt().getValue(pnd_Sub_Tot)),  //Natural: COMPUTE #OV-UNT-AMT ( #SUB-TOT ) = #T-UNT-AMT-1 ( #J ) - #T-UNT-AMT-2 ( #J )
                pnd_T_Unt_Amt_1.getValue(pnd_J).subtract(pnd_T_Unt_Amt_2.getValue(pnd_J)));
            pnd_W_Contract.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct());                                                                                              //Natural: ASSIGN #W-CONTRACT := #W-CNTRCT
            pnd_W_Payee_Cde.setValue(pdaIaaa586a.getIaaa460a_Pnd_W_Payee_Cde_1());                                                                                        //Natural: ASSIGN #W-PAYEE-CDE := #W-PAYEE-CDE-1
            pnd_W_Date.setValue(pdaIaaa586a.getIaaa460a_Pnd_Table_Instl_Date().getValue(pnd_I));                                                                          //Natural: ASSIGN #W-DATE := #TABLE-INSTL-DATE ( #I )
            pnd_W_Desc.setValue(pnd_W_Option_Cde_Desc);                                                                                                                   //Natural: ASSIGN #W-DESC := #W-OPTION-CDE-DESC
            pnd_W_Mode.setValue(pdaIaaa586a.getIaaa460a_Pnd_Mode());                                                                                                      //Natural: ASSIGN #W-MODE := #MODE
            pnd_W_Fund.setValue(pnd_T_Fund.getValue(pnd_J));                                                                                                              //Natural: ASSIGN #W-FUND := #T-FUND ( #J )
            pnd_W_Guar_Payment.setValue(pnd_T_Per_Amt_1.getValue(pnd_J));                                                                                                 //Natural: ASSIGN #W-GUAR-PAYMENT := #T-PER-AMT-1 ( #J )
            pnd_W_Divd_Payment.setValue(pnd_T_Div_Amt_1.getValue(pnd_J));                                                                                                 //Natural: ASSIGN #W-DIVD-PAYMENT := #T-DIV-AMT-1 ( #J )
            pnd_W_Unit_Payment.setValue(pnd_T_Unt_Amt_1.getValue(pnd_J));                                                                                                 //Natural: ASSIGN #W-UNIT-PAYMENT := #T-UNT-AMT-1 ( #J )
            pnd_W_Guar_Overpayment.setValue(pdaIaaa586a.getIaaa460a_Pnd_Ov_Per_Amt().getValue(pnd_Sub_Tot));                                                              //Natural: ASSIGN #W-GUAR-OVERPAYMENT := #OV-PER-AMT ( #SUB-TOT )
            pnd_W_Divd_Overpayment.setValue(pdaIaaa586a.getIaaa460a_Pnd_Ov_Div_Amt().getValue(pnd_Sub_Tot));                                                              //Natural: ASSIGN #W-DIVD-OVERPAYMENT := #OV-DIV-AMT ( #SUB-TOT )
            pnd_W_Unit_Overpayment.setValue(pdaIaaa586a.getIaaa460a_Pnd_Ov_Unt_Amt().getValue(pnd_Sub_Tot));                                                              //Natural: ASSIGN #W-UNIT-OVERPAYMENT := #OV-UNT-AMT ( #SUB-TOT )
            //*  USER REQUESTS NOT SEEING DELETED RECORDS ON REPORT - 06/99
            if (condition(pnd_W_Msg.equals(" ")))                                                                                                                         //Natural: IF #W-MSG = ' '
            {
                //*  041514
                getWorkFiles().write(2, false, pnd_W_Contract, pnd_W_Payee_Cde, pnd_W_Fund, pnd_W_Desc, pnd_W_Mode, pnd_W_Date, pnd_W_Guar_Payment, pnd_W_Divd_Payment,   //Natural: WRITE WORK FILE 2 #W-CONTRACT #W-PAYEE-CDE #W-FUND #W-DESC #W-MODE #W-DATE #W-GUAR-PAYMENT #W-DIVD-PAYMENT #W-UNIT-PAYMENT #W-GUAR-OVERPAYMENT #W-DIVD-OVERPAYMENT #W-UNIT-OVERPAYMENT #W-MSG #OVERPAY-ID-NBR
                    pnd_W_Unit_Payment, pnd_W_Guar_Overpayment, pnd_W_Divd_Overpayment, pnd_W_Unit_Overpayment, pnd_W_Msg, pnd_Overpay_Id_Nbr);
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Contract.reset();                                                                                                                                       //Natural: RESET #W-CONTRACT #W-PAYEE-CDE #W-FUND #W-DESC #W-MODE #W-DATE #W-GUAR-PAYMENT #W-DIVD-PAYMENT #W-UNIT-PAYMENT #W-GUAR-OVERPAYMENT #W-DIVD-OVERPAYMENT #W-UNIT-OVERPAYMENT
            pnd_W_Payee_Cde.reset();
            pnd_W_Fund.reset();
            pnd_W_Desc.reset();
            pnd_W_Mode.reset();
            pnd_W_Date.reset();
            pnd_W_Guar_Payment.reset();
            pnd_W_Divd_Payment.reset();
            pnd_W_Unit_Payment.reset();
            pnd_W_Guar_Overpayment.reset();
            pnd_W_Divd_Overpayment.reset();
            pnd_W_Unit_Overpayment.reset();
            pnd_T_Per_Amt_1.getValue(pnd_J).reset();                                                                                                                      //Natural: RESET #T-PER-AMT-1 ( #J ) #T-PER-AMT-2 ( #J ) #T-DIV-AMT-1 ( #J ) #T-DIV-AMT-2 ( #J ) #T-UNT-AMT-1 ( #J ) #T-UNT-AMT-2 ( #J ) #T-FUND ( #J )
            pnd_T_Per_Amt_2.getValue(pnd_J).reset();
            pnd_T_Div_Amt_1.getValue(pnd_J).reset();
            pnd_T_Div_Amt_2.getValue(pnd_J).reset();
            pnd_T_Unt_Amt_1.getValue(pnd_J).reset();
            pnd_T_Unt_Amt_2.getValue(pnd_J).reset();
            pnd_T_Fund.getValue(pnd_J).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Process_Cntrct() throws Exception                                                                                                                //Natural: #PROCESS-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W-CNTRCT
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct(), WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT FOUND FOR ",pdaIaaa586a.getIaaa460a_Pnd_W_Cntrct());                                                                   //Natural: WRITE 'NO CONTRACT FOUND FOR ' #W-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD = 0
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(1);                                                                                                           //Natural: ASSIGN #ISSUE-DATE-DD := 01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                             //Natural: ASSIGN #ISSUE-DATE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte());                                                                //Natural: ASSIGN #ISSUE-DATE-YYYYMM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_W_Issue_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                                                     //Natural: ASSIGN #W-ISSUE-DATE := #ISSUE-DATE-N
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
    }
}
