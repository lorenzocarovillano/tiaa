/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:56:31 PM
**        * FROM NATURAL SUBPROGRAM : Aian013r
************************************************************
**        * FILE NAME            : Aian013r.java
**        * CLASS NAME           : Aian013r
**        * INSTANCE NAME        : Aian013r
************************************************************
*
******* REMEMBER TO PUT BACK THE STORE TRANSACTION **********
*************************************************************
*************************************************************
* AIAN013 - CREF POST-RETIREMENT TRANSFER ACTUARIAL CALCULATION MODULE
*  VERSION 2.7 - 12/16/98
*  08/14/08  DY  SUPPORT TIAA ACCESS FUND; REF AS DY1
*********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aian013r extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAial0130 pdaAial0130;
    private LdaAial0131 ldaAial0131;
    private PdaAiaa026 pdaAiaa026;
    private LdaAial013b ldaAial013b;
    private PdaAial0210 pdaAial0210;
    private LdaAial0270 ldaAial0270;
    private LdaAial0271 ldaAial0271;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Compare_Rate_Date;

    private DbsGroup pnd_Compare_Rate_Date__R_Field_1;
    private DbsField pnd_Compare_Rate_Date_Pnd_Compare_Rate_Century;
    private DbsField pnd_Compare_Rate_Date_Pnd_Compare_Rate_Year;
    private DbsField pnd_Compare_Rate_Date_Pnd_Compare_Rate_Month;
    private DbsField pnd_Compare_Auv_Date;

    private DbsGroup pnd_Compare_Auv_Date__R_Field_2;
    private DbsField pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year;

    private DbsGroup pnd_Compare_Auv_Date__R_Field_3;
    private DbsField pnd_Compare_Auv_Date_Pnd_Compare_Auv_Century;
    private DbsField pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year_2;
    private DbsField pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month;

    private DbsGroup pnd_Aian027_Linkage;

    private DbsGroup pnd_Aian027_Linkage_Pnd_Aian027_Input;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Da_Rate;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Fund_Indicator;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Effective_Date;

    private DbsGroup pnd_Aian027_Linkage__R_Field_4;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Effective_Date_Without_Day;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Effective_Day;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Type_Of_Call;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Option_Code;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Gsra_Ira_Tpa_Mdo_Ind;

    private DbsGroup pnd_Aian027_Linkage_Pnd_Aian027_Output;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Return_Code;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table;

    private DbsGroup pnd_Aian027_Linkage__R_Field_5;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table_1_3;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table_4;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Total_Setback;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table;

    private DbsGroup pnd_Aian027_Linkage__R_Field_6;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table_1_3;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table_4;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Guara_Setback;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table;

    private DbsGroup pnd_Aian027_Linkage__R_Field_7;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table_1_3;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table_4;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Cref_Interest_Rate;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Cref_Setback;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Modal_Interest_Rate;
    private DbsField pnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate;

    private DbsGroup pnd_Aian030_Linkage;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_D;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_End_Date_D;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days;

    private DbsGroup pnd_Ws_Date;
    private DbsField pnd_Ws_Date_Pnd_Ws_Year;
    private DbsField pnd_Ws_Date_Pnd_Ws_Month;
    private DbsField pnd_Ws_Date_Pnd_Ws_Day;

    private DbsGroup pnd_Ws_Date__R_Field_8;
    private DbsField pnd_Ws_Date_Pnd_Temp_Date;

    private DbsGroup pnd_Ws_Date__R_Field_9;
    private DbsField pnd_Ws_Date_Pnd_Temp_Date_A8;

    private DbsGroup pnd_Ws_Date__R_Field_10;
    private DbsField pnd_Ws_Date_Pnd_Temp_Year_Month;

    private DbsGroup pnd_Ws_Date__R_Field_11;
    private DbsField pnd_Ws_Date_Pnd_Temp_Year;
    private DbsField pnd_Ws_Date_Pnd_Temp_Month;
    private DbsField pnd_Ws_Date_Pnd_Temp_Day;
    private DbsField pnd_Tiaa_Sub;
    private DbsField pnd_Cref_Sub;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Pmt_Out;
    private DbsField pnd_Pmt_New;
    private DbsField pnd_Pmt_Old;
    private DbsField pnd_Auv_Old_1;
    private DbsField pnd_Auv_Date;

    private DbsGroup pnd_Auv_Date__R_Field_12;
    private DbsField pnd_Auv_Date_Pnd_Auv_Year;
    private DbsField pnd_Auv_Date_Pnd_Auv_Month;
    private DbsField pnd_Auv_Date_Pnd_Auv_Day;

    private DbsGroup pnd_Auv_Date__R_Field_13;
    private DbsField pnd_Auv_Date_Pnd_Auv_Date_A8;
    private DbsField pnd_Temp_Auv_Date;
    private DbsField pnd_Temp_Iartmg_Date;
    private DbsField pnd_Auv_Date_Plus_1;

    private DbsGroup pnd_Auv_Date_Plus_1__R_Field_14;
    private DbsField pnd_Auv_Date_Plus_1_Pnd_Auv_Year_Plus_1;
    private DbsField pnd_Auv_Date_Plus_1_Pnd_Auv_Month_Plus_1;
    private DbsField pnd_Auv_Date_Plus_1_Pnd_Auv_Day_Plus_1;

    private DbsGroup pnd_Auv_Date_Plus_1__R_Field_15;
    private DbsField pnd_Auv_Date_Plus_1_Pnd_Auv_Date_Plus_1_A8;
    private DbsField pnd_Iartmg_Date_Plus_1;

    private DbsGroup pnd_Iartmg_Date_Plus_1__R_Field_16;
    private DbsField pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Year_Plus_1;
    private DbsField pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Month_Plus_1;
    private DbsField pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Day_Plus_1;

    private DbsGroup pnd_Iartmg_Date_Plus_1__R_Field_17;
    private DbsField pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Date_Plus_1_A8;
    private DbsField pnd_Age_As_Of_Date;
    private DbsField pnd_Gbpm_Int_Rate;
    private DbsField pnd_Months;
    private DbsField pnd_Months_Between_Pmts;
    private DbsField pnd_V_4;
    private DbsField pnd_V_I;
    private DbsField pnd_Modal_Factor;
    private DbsField pnd_Modal_Num;
    private DbsField pnd_Modal_Den;
    private DbsField pnd_Gtd_Pmt;
    private DbsField pnd_Test;
    private DbsField pnd_One;
    private DbsField pnd_One_Over_Months;
    private DbsField pnd_V_To_Power;
    private DbsField pnd_One_Minus_V;
    private DbsField pnd_One_Minus_V_To_Power;
    private DbsField pnd_Months_Times_Power;
    private DbsField pnd_Num_Pmts;
    private DbsField pnd_Ann_Factor_Switch;
    private DbsField pnd_Next_Pymnt_Dte_A8;

    private DbsGroup pnd_Next_Pymnt_Dte_A8__R_Field_18;
    private DbsField pnd_Next_Pymnt_Dte_A8_Pnd_Next_Pymnt_Dte_N8;
    private DbsField pnd_Cref_Summary_End_Date;

    private DbsGroup pnd_Cref_Summary_End_Date__R_Field_19;
    private DbsField pnd_Cref_Summary_End_Date_Pnd_Cref_End_Century;
    private DbsField pnd_Cref_Summary_End_Date_Pnd_Cref_End_Year;
    private DbsField pnd_Cref_Summary_End_Date_Pnd_Cref_End_Month;
    private DbsField pnd_Cref_Summary_End_Date_Pnd_Cref_End_Day;
    private DbsField pnd_T_Cref_Ann_Factora;
    private DbsField pnd_T_Cref_Ann_Factora_1;
    private DbsField pnd_T_Cref_Ann_Factora_2;
    private DbsField pnd_T_Cref_Ann_Factora_To_Fiscal;
    private DbsField pnd_T_Rea_Ann_Factora;
    private DbsField pnd_T_Rea_Ann_Factora_1;
    private DbsField pnd_T_Rea_Ann_Factora_2;
    private DbsField pnd_T_Rea_Ann_Factora_To_Fiscal;
    private DbsField pnd_T_Gbpm_Ann_Factor;
    private DbsField pnd_T_Std_Ann_Factor;
    private DbsField pnd_T_Guar_Ann_Factor;
    private DbsField pnd_Temp_Fund_Code;
    private DbsField pnd_Tiaa_Switch;

    private DbsGroup pnd_Tiaa_Mortality_Arraysa;

    private DbsGroup pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays;
    private DbsField pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Std_Ann_Factora;
    private DbsField pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Grd_Ann_Factora;
    private DbsField pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Std_Ann_Factora;
    private DbsField pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Grd_Ann_Factora;

    private DbsGroup pnd_Tiaa_Mortality_Arraysa__R_Field_20;

    private DbsGroup pnd_Tiaa_Mortality_Arraysa_Pnd_Filler4;
    private DbsField pnd_Tiaa_Mortality_Arraysa_Pnd_T_Ann_Factora;

    private DbsGroup pnd_Days_In_Month_Array;
    private DbsField pnd_Days_In_Month_Array_Pnd_Jan;
    private DbsField pnd_Days_In_Month_Array_Pnd_Feb;
    private DbsField pnd_Days_In_Month_Array_Pnd_Mar;
    private DbsField pnd_Days_In_Month_Array_Pnd_Apr;
    private DbsField pnd_Days_In_Month_Array_Pnd_May;
    private DbsField pnd_Days_In_Month_Array_Pnd_Jun;
    private DbsField pnd_Days_In_Month_Array_Pnd_Jul;
    private DbsField pnd_Days_In_Month_Array_Pnd_Aug;
    private DbsField pnd_Days_In_Month_Array_Pnd_Sep;
    private DbsField pnd_Days_In_Month_Array_Pnd_Oct;
    private DbsField pnd_Days_In_Month_Array_Pnd_Nov;
    private DbsField pnd_Days_In_Month_Array_Pnd_Dec;

    private DbsGroup pnd_Days_In_Month_Array__R_Field_21;

    private DbsGroup pnd_Days_In_Month_Array_Pnd_Filler5;
    private DbsField pnd_Days_In_Month_Array_Pnd_Days_In_Month;
    private DbsField pnd_Result_Year_Dec;
    private DbsField pnd_Auv_Year_Dec;
    private DbsField pnd_Result_Rem;
    private DbsField pnd_Tiaa_Current_Ia_Rb;
    private DbsField pnd_Fund_Code_Temp;
    private DbsField pnd_Pmt_Month_1;
    private DbsField pnd_Pmt_Month_2;
    private DbsField pnd_Pmt_Month_3;
    private DbsField pnd_Pmt_Month_4;
    private DbsField pnd_Pmt_Switch;
    private DbsField pnd_Annuity_Factor_1;
    private DbsField pnd_Annuity_Factor_2;
    private DbsField pnd_Annuity_Factor_To_Fiscal;
    private DbsField pnd_Auv_Revaluation;
    private DbsField pnd_Auv_Revaluation_From;
    private DbsField pnd_Temp_In_Mortality_Table;
    private DbsField pnd_Temp_In_Setback;
    private DbsField pnd_Temp_In_Interest_Rate;
    private DbsField pnd_Temp_In_Def_Interest_Rate;
    private DbsField pnd_Deferred_Period_Save;
    private DbsField pnd_Total_Units_To_Receive;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0131 = new LdaAial0131();
        registerRecord(ldaAial0131);
        registerRecord(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2());
        localVariables = new DbsRecord();
        pdaAiaa026 = new PdaAiaa026(localVariables);
        ldaAial013b = new LdaAial013b();
        registerRecord(ldaAial013b);
        pdaAial0210 = new PdaAial0210(localVariables);
        ldaAial0270 = new LdaAial0270();
        registerRecord(ldaAial0270);
        registerRecord(ldaAial0270.getVw_cratemgr_M_View());
        ldaAial0271 = new LdaAial0271();
        registerRecord(ldaAial0271);

        // parameters
        parameters = new DbsRecord();
        pdaAial0130 = new PdaAial0130(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Compare_Rate_Date = localVariables.newFieldInRecord("pnd_Compare_Rate_Date", "#COMPARE-RATE-DATE", FieldType.NUMERIC, 6);

        pnd_Compare_Rate_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Compare_Rate_Date__R_Field_1", "REDEFINE", pnd_Compare_Rate_Date);
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Century = pnd_Compare_Rate_Date__R_Field_1.newFieldInGroup("pnd_Compare_Rate_Date_Pnd_Compare_Rate_Century", 
            "#COMPARE-RATE-CENTURY", FieldType.NUMERIC, 2);
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Year = pnd_Compare_Rate_Date__R_Field_1.newFieldInGroup("pnd_Compare_Rate_Date_Pnd_Compare_Rate_Year", 
            "#COMPARE-RATE-YEAR", FieldType.NUMERIC, 2);
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Month = pnd_Compare_Rate_Date__R_Field_1.newFieldInGroup("pnd_Compare_Rate_Date_Pnd_Compare_Rate_Month", 
            "#COMPARE-RATE-MONTH", FieldType.NUMERIC, 2);
        pnd_Compare_Auv_Date = localVariables.newFieldInRecord("pnd_Compare_Auv_Date", "#COMPARE-AUV-DATE", FieldType.NUMERIC, 6);

        pnd_Compare_Auv_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Compare_Auv_Date__R_Field_2", "REDEFINE", pnd_Compare_Auv_Date);
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year = pnd_Compare_Auv_Date__R_Field_2.newFieldInGroup("pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year", "#COMPARE-AUV-YEAR", 
            FieldType.NUMERIC, 4);

        pnd_Compare_Auv_Date__R_Field_3 = pnd_Compare_Auv_Date__R_Field_2.newGroupInGroup("pnd_Compare_Auv_Date__R_Field_3", "REDEFINE", pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year);
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Century = pnd_Compare_Auv_Date__R_Field_3.newFieldInGroup("pnd_Compare_Auv_Date_Pnd_Compare_Auv_Century", 
            "#COMPARE-AUV-CENTURY", FieldType.NUMERIC, 2);
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year_2 = pnd_Compare_Auv_Date__R_Field_3.newFieldInGroup("pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year_2", "#COMPARE-AUV-YEAR-2", 
            FieldType.NUMERIC, 2);
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month = pnd_Compare_Auv_Date__R_Field_2.newFieldInGroup("pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month", "#COMPARE-AUV-MONTH", 
            FieldType.NUMERIC, 2);

        pnd_Aian027_Linkage = localVariables.newGroupInRecord("pnd_Aian027_Linkage", "#AIAN027-LINKAGE");

        pnd_Aian027_Linkage_Pnd_Aian027_Input = pnd_Aian027_Linkage.newGroupInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Input", "#AIAN027-INPUT");
        pnd_Aian027_Linkage_Pnd_Aian027_Da_Rate = pnd_Aian027_Linkage_Pnd_Aian027_Input.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Da_Rate", "#AIAN027-DA-RATE", 
            FieldType.NUMERIC, 2);
        pnd_Aian027_Linkage_Pnd_Aian027_Fund_Indicator = pnd_Aian027_Linkage_Pnd_Aian027_Input.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Fund_Indicator", 
            "#AIAN027-FUND-INDICATOR", FieldType.STRING, 1);
        pnd_Aian027_Linkage_Pnd_Aian027_Effective_Date = pnd_Aian027_Linkage_Pnd_Aian027_Input.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Effective_Date", 
            "#AIAN027-EFFECTIVE-DATE", FieldType.NUMERIC, 8);

        pnd_Aian027_Linkage__R_Field_4 = pnd_Aian027_Linkage_Pnd_Aian027_Input.newGroupInGroup("pnd_Aian027_Linkage__R_Field_4", "REDEFINE", pnd_Aian027_Linkage_Pnd_Aian027_Effective_Date);
        pnd_Aian027_Linkage_Pnd_Aian027_Effective_Date_Without_Day = pnd_Aian027_Linkage__R_Field_4.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Effective_Date_Without_Day", 
            "#AIAN027-EFFECTIVE-DATE-WITHOUT-DAY", FieldType.NUMERIC, 6);
        pnd_Aian027_Linkage_Pnd_Aian027_Effective_Day = pnd_Aian027_Linkage__R_Field_4.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Effective_Day", 
            "#AIAN027-EFFECTIVE-DAY", FieldType.NUMERIC, 2);
        pnd_Aian027_Linkage_Pnd_Aian027_Type_Of_Call = pnd_Aian027_Linkage_Pnd_Aian027_Input.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Type_Of_Call", 
            "#AIAN027-TYPE-OF-CALL", FieldType.STRING, 2);
        pnd_Aian027_Linkage_Pnd_Aian027_Option_Code = pnd_Aian027_Linkage_Pnd_Aian027_Input.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Option_Code", 
            "#AIAN027-OPTION-CODE", FieldType.NUMERIC, 2);
        pnd_Aian027_Linkage_Pnd_Aian027_Gsra_Ira_Tpa_Mdo_Ind = pnd_Aian027_Linkage_Pnd_Aian027_Input.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Gsra_Ira_Tpa_Mdo_Ind", 
            "#AIAN027-GSRA-IRA-TPA-MDO-IND", FieldType.STRING, 1);

        pnd_Aian027_Linkage_Pnd_Aian027_Output = pnd_Aian027_Linkage.newGroupInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Output", "#AIAN027-OUTPUT");
        pnd_Aian027_Linkage_Pnd_Aian027_Return_Code = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Return_Code", 
            "#AIAN027-RETURN-CODE", FieldType.NUMERIC, 3);
        pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table", 
            "#AIAN027-TOTAL-MORTALITY-TABLE", FieldType.NUMERIC, 4);

        pnd_Aian027_Linkage__R_Field_5 = pnd_Aian027_Linkage_Pnd_Aian027_Output.newGroupInGroup("pnd_Aian027_Linkage__R_Field_5", "REDEFINE", pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table);
        pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table_1_3 = pnd_Aian027_Linkage__R_Field_5.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table_1_3", 
            "#AIAN027-TOTAL-MORTALITY-TABLE-1-3", FieldType.NUMERIC, 3);
        pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table_4 = pnd_Aian027_Linkage__R_Field_5.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table_4", 
            "#AIAN027-TOTAL-MORTALITY-TABLE-4", FieldType.NUMERIC, 1);
        pnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate", 
            "#AIAN027-TOTAL-INTEREST-RATE", FieldType.NUMERIC, 5, 5);
        pnd_Aian027_Linkage_Pnd_Aian027_Total_Setback = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Total_Setback", 
            "#AIAN027-TOTAL-SETBACK", FieldType.NUMERIC, 7, 5);
        pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table", 
            "#AIAN027-GUARA-MORTALITY-TABLE", FieldType.NUMERIC, 4);

        pnd_Aian027_Linkage__R_Field_6 = pnd_Aian027_Linkage_Pnd_Aian027_Output.newGroupInGroup("pnd_Aian027_Linkage__R_Field_6", "REDEFINE", pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table);
        pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table_1_3 = pnd_Aian027_Linkage__R_Field_6.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table_1_3", 
            "#AIAN027-GUARA-MORTALITY-TABLE-1-3", FieldType.NUMERIC, 3);
        pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table_4 = pnd_Aian027_Linkage__R_Field_6.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table_4", 
            "#AIAN027-GUARA-MORTALITY-TABLE-4", FieldType.NUMERIC, 1);
        pnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate", 
            "#AIAN027-GUARA-INTEREST-RATE", FieldType.NUMERIC, 5, 5);
        pnd_Aian027_Linkage_Pnd_Aian027_Guara_Setback = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Guara_Setback", 
            "#AIAN027-GUARA-SETBACK", FieldType.NUMERIC, 7, 5);
        pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table", 
            "#AIAN027-CREF-MORTALITY-TABLE", FieldType.NUMERIC, 4);

        pnd_Aian027_Linkage__R_Field_7 = pnd_Aian027_Linkage_Pnd_Aian027_Output.newGroupInGroup("pnd_Aian027_Linkage__R_Field_7", "REDEFINE", pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table);
        pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table_1_3 = pnd_Aian027_Linkage__R_Field_7.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table_1_3", 
            "#AIAN027-CREF-MORTALITY-TABLE-1-3", FieldType.NUMERIC, 3);
        pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table_4 = pnd_Aian027_Linkage__R_Field_7.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table_4", 
            "#AIAN027-CREF-MORTALITY-TABLE-4", FieldType.NUMERIC, 1);
        pnd_Aian027_Linkage_Pnd_Aian027_Cref_Interest_Rate = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Cref_Interest_Rate", 
            "#AIAN027-CREF-INTEREST-RATE", FieldType.NUMERIC, 5, 5);
        pnd_Aian027_Linkage_Pnd_Aian027_Cref_Setback = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Cref_Setback", 
            "#AIAN027-CREF-SETBACK", FieldType.NUMERIC, 7, 5);
        pnd_Aian027_Linkage_Pnd_Aian027_Modal_Interest_Rate = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Modal_Interest_Rate", 
            "#AIAN027-MODAL-INTEREST-RATE", FieldType.NUMERIC, 5, 5);
        pnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate = pnd_Aian027_Linkage_Pnd_Aian027_Output.newFieldInGroup("pnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate", "#AIAN027-IA-RATE", 
            FieldType.NUMERIC, 2);

        pnd_Aian030_Linkage = localVariables.newGroupInRecord("pnd_Aian030_Linkage", "#AIAN030-LINKAGE");
        pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N", "#AIAN030-BEGIN-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N", "#AIAN030-END-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_D = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_D", "#AIAN030-BEGIN-DATE-D", 
            FieldType.DATE);
        pnd_Aian030_Linkage_Pnd_Aian030_End_Date_D = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_End_Date_D", "#AIAN030-END-DATE-D", 
            FieldType.DATE);
        pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days", 
            "#AIAN030-DAYS-BETWEEN-TWO-DAYS", FieldType.NUMERIC, 8);

        pnd_Ws_Date = localVariables.newGroupInRecord("pnd_Ws_Date", "#WS-DATE");
        pnd_Ws_Date_Pnd_Ws_Year = pnd_Ws_Date.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Year", "#WS-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Date_Pnd_Ws_Month = pnd_Ws_Date.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Month", "#WS-MONTH", FieldType.NUMERIC, 2);
        pnd_Ws_Date_Pnd_Ws_Day = pnd_Ws_Date.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Day", "#WS-DAY", FieldType.NUMERIC, 2);

        pnd_Ws_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Ws_Date__R_Field_8", "REDEFINE", pnd_Ws_Date);
        pnd_Ws_Date_Pnd_Temp_Date = pnd_Ws_Date__R_Field_8.newFieldInGroup("pnd_Ws_Date_Pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 8);

        pnd_Ws_Date__R_Field_9 = pnd_Ws_Date__R_Field_8.newGroupInGroup("pnd_Ws_Date__R_Field_9", "REDEFINE", pnd_Ws_Date_Pnd_Temp_Date);
        pnd_Ws_Date_Pnd_Temp_Date_A8 = pnd_Ws_Date__R_Field_9.newFieldInGroup("pnd_Ws_Date_Pnd_Temp_Date_A8", "#TEMP-DATE-A8", FieldType.STRING, 8);

        pnd_Ws_Date__R_Field_10 = pnd_Ws_Date__R_Field_8.newGroupInGroup("pnd_Ws_Date__R_Field_10", "REDEFINE", pnd_Ws_Date_Pnd_Temp_Date);
        pnd_Ws_Date_Pnd_Temp_Year_Month = pnd_Ws_Date__R_Field_10.newFieldInGroup("pnd_Ws_Date_Pnd_Temp_Year_Month", "#TEMP-YEAR-MONTH", FieldType.NUMERIC, 
            6);

        pnd_Ws_Date__R_Field_11 = pnd_Ws_Date__R_Field_10.newGroupInGroup("pnd_Ws_Date__R_Field_11", "REDEFINE", pnd_Ws_Date_Pnd_Temp_Year_Month);
        pnd_Ws_Date_Pnd_Temp_Year = pnd_Ws_Date__R_Field_11.newFieldInGroup("pnd_Ws_Date_Pnd_Temp_Year", "#TEMP-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Date_Pnd_Temp_Month = pnd_Ws_Date__R_Field_11.newFieldInGroup("pnd_Ws_Date_Pnd_Temp_Month", "#TEMP-MONTH", FieldType.NUMERIC, 2);
        pnd_Ws_Date_Pnd_Temp_Day = pnd_Ws_Date__R_Field_10.newFieldInGroup("pnd_Ws_Date_Pnd_Temp_Day", "#TEMP-DAY", FieldType.NUMERIC, 2);
        pnd_Tiaa_Sub = localVariables.newFieldInRecord("pnd_Tiaa_Sub", "#TIAA-SUB", FieldType.NUMERIC, 1);
        pnd_Cref_Sub = localVariables.newFieldInRecord("pnd_Cref_Sub", "#CREF-SUB", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Pmt_Out = localVariables.newFieldInRecord("pnd_Pmt_Out", "#PMT-OUT", FieldType.NUMERIC, 9, 2);
        pnd_Pmt_New = localVariables.newFieldInRecord("pnd_Pmt_New", "#PMT-NEW", FieldType.NUMERIC, 9, 2);
        pnd_Pmt_Old = localVariables.newFieldInRecord("pnd_Pmt_Old", "#PMT-OLD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Auv_Old_1 = localVariables.newFieldInRecord("pnd_Auv_Old_1", "#AUV-OLD-1", FieldType.NUMERIC, 7, 4);
        pnd_Auv_Date = localVariables.newFieldInRecord("pnd_Auv_Date", "#AUV-DATE", FieldType.NUMERIC, 8);

        pnd_Auv_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_Auv_Date__R_Field_12", "REDEFINE", pnd_Auv_Date);
        pnd_Auv_Date_Pnd_Auv_Year = pnd_Auv_Date__R_Field_12.newFieldInGroup("pnd_Auv_Date_Pnd_Auv_Year", "#AUV-YEAR", FieldType.NUMERIC, 4);
        pnd_Auv_Date_Pnd_Auv_Month = pnd_Auv_Date__R_Field_12.newFieldInGroup("pnd_Auv_Date_Pnd_Auv_Month", "#AUV-MONTH", FieldType.NUMERIC, 2);
        pnd_Auv_Date_Pnd_Auv_Day = pnd_Auv_Date__R_Field_12.newFieldInGroup("pnd_Auv_Date_Pnd_Auv_Day", "#AUV-DAY", FieldType.NUMERIC, 2);

        pnd_Auv_Date__R_Field_13 = localVariables.newGroupInRecord("pnd_Auv_Date__R_Field_13", "REDEFINE", pnd_Auv_Date);
        pnd_Auv_Date_Pnd_Auv_Date_A8 = pnd_Auv_Date__R_Field_13.newFieldInGroup("pnd_Auv_Date_Pnd_Auv_Date_A8", "#AUV-DATE-A8", FieldType.STRING, 8);
        pnd_Temp_Auv_Date = localVariables.newFieldInRecord("pnd_Temp_Auv_Date", "#TEMP-AUV-DATE", FieldType.DATE);
        pnd_Temp_Iartmg_Date = localVariables.newFieldInRecord("pnd_Temp_Iartmg_Date", "#TEMP-IARTMG-DATE", FieldType.DATE);
        pnd_Auv_Date_Plus_1 = localVariables.newFieldInRecord("pnd_Auv_Date_Plus_1", "#AUV-DATE-PLUS-1", FieldType.NUMERIC, 8);

        pnd_Auv_Date_Plus_1__R_Field_14 = localVariables.newGroupInRecord("pnd_Auv_Date_Plus_1__R_Field_14", "REDEFINE", pnd_Auv_Date_Plus_1);
        pnd_Auv_Date_Plus_1_Pnd_Auv_Year_Plus_1 = pnd_Auv_Date_Plus_1__R_Field_14.newFieldInGroup("pnd_Auv_Date_Plus_1_Pnd_Auv_Year_Plus_1", "#AUV-YEAR-PLUS-1", 
            FieldType.NUMERIC, 4);
        pnd_Auv_Date_Plus_1_Pnd_Auv_Month_Plus_1 = pnd_Auv_Date_Plus_1__R_Field_14.newFieldInGroup("pnd_Auv_Date_Plus_1_Pnd_Auv_Month_Plus_1", "#AUV-MONTH-PLUS-1", 
            FieldType.NUMERIC, 2);
        pnd_Auv_Date_Plus_1_Pnd_Auv_Day_Plus_1 = pnd_Auv_Date_Plus_1__R_Field_14.newFieldInGroup("pnd_Auv_Date_Plus_1_Pnd_Auv_Day_Plus_1", "#AUV-DAY-PLUS-1", 
            FieldType.NUMERIC, 2);

        pnd_Auv_Date_Plus_1__R_Field_15 = localVariables.newGroupInRecord("pnd_Auv_Date_Plus_1__R_Field_15", "REDEFINE", pnd_Auv_Date_Plus_1);
        pnd_Auv_Date_Plus_1_Pnd_Auv_Date_Plus_1_A8 = pnd_Auv_Date_Plus_1__R_Field_15.newFieldInGroup("pnd_Auv_Date_Plus_1_Pnd_Auv_Date_Plus_1_A8", "#AUV-DATE-PLUS-1-A8", 
            FieldType.STRING, 8);
        pnd_Iartmg_Date_Plus_1 = localVariables.newFieldInRecord("pnd_Iartmg_Date_Plus_1", "#IARTMG-DATE-PLUS-1", FieldType.NUMERIC, 8);

        pnd_Iartmg_Date_Plus_1__R_Field_16 = localVariables.newGroupInRecord("pnd_Iartmg_Date_Plus_1__R_Field_16", "REDEFINE", pnd_Iartmg_Date_Plus_1);
        pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Year_Plus_1 = pnd_Iartmg_Date_Plus_1__R_Field_16.newFieldInGroup("pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Year_Plus_1", 
            "#IARTMG-YEAR-PLUS-1", FieldType.NUMERIC, 4);
        pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Month_Plus_1 = pnd_Iartmg_Date_Plus_1__R_Field_16.newFieldInGroup("pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Month_Plus_1", 
            "#IARTMG-MONTH-PLUS-1", FieldType.NUMERIC, 2);
        pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Day_Plus_1 = pnd_Iartmg_Date_Plus_1__R_Field_16.newFieldInGroup("pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Day_Plus_1", 
            "#IARTMG-DAY-PLUS-1", FieldType.NUMERIC, 2);

        pnd_Iartmg_Date_Plus_1__R_Field_17 = localVariables.newGroupInRecord("pnd_Iartmg_Date_Plus_1__R_Field_17", "REDEFINE", pnd_Iartmg_Date_Plus_1);
        pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Date_Plus_1_A8 = pnd_Iartmg_Date_Plus_1__R_Field_17.newFieldInGroup("pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Date_Plus_1_A8", 
            "#IARTMG-DATE-PLUS-1-A8", FieldType.STRING, 8);
        pnd_Age_As_Of_Date = localVariables.newFieldInRecord("pnd_Age_As_Of_Date", "#AGE-AS-OF-DATE", FieldType.NUMERIC, 8);
        pnd_Gbpm_Int_Rate = localVariables.newFieldInRecord("pnd_Gbpm_Int_Rate", "#GBPM-INT-RATE", FieldType.NUMERIC, 5, 5);
        pnd_Months = localVariables.newFieldInRecord("pnd_Months", "#MONTHS", FieldType.FLOAT, 8);
        pnd_Months_Between_Pmts = localVariables.newFieldInRecord("pnd_Months_Between_Pmts", "#MONTHS-BETWEEN-PMTS", FieldType.FLOAT, 8);
        pnd_V_4 = localVariables.newFieldInRecord("pnd_V_4", "#V-4", FieldType.FLOAT, 8);
        pnd_V_I = localVariables.newFieldInRecord("pnd_V_I", "#V-I", FieldType.FLOAT, 8);
        pnd_Modal_Factor = localVariables.newFieldInRecord("pnd_Modal_Factor", "#MODAL-FACTOR", FieldType.FLOAT, 8);
        pnd_Modal_Num = localVariables.newFieldInRecord("pnd_Modal_Num", "#MODAL-NUM", FieldType.FLOAT, 8);
        pnd_Modal_Den = localVariables.newFieldInRecord("pnd_Modal_Den", "#MODAL-DEN", FieldType.FLOAT, 8);
        pnd_Gtd_Pmt = localVariables.newFieldInRecord("pnd_Gtd_Pmt", "#GTD-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Test = localVariables.newFieldInRecord("pnd_Test", "#TEST", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_One = localVariables.newFieldInRecord("pnd_One", "#ONE", FieldType.FLOAT, 8);
        pnd_One_Over_Months = localVariables.newFieldInRecord("pnd_One_Over_Months", "#ONE-OVER-MONTHS", FieldType.FLOAT, 8);
        pnd_V_To_Power = localVariables.newFieldInRecord("pnd_V_To_Power", "#V-TO-POWER", FieldType.FLOAT, 8);
        pnd_One_Minus_V = localVariables.newFieldInRecord("pnd_One_Minus_V", "#ONE-MINUS-V", FieldType.FLOAT, 8);
        pnd_One_Minus_V_To_Power = localVariables.newFieldInRecord("pnd_One_Minus_V_To_Power", "#ONE-MINUS-V-TO-POWER", FieldType.FLOAT, 8);
        pnd_Months_Times_Power = localVariables.newFieldInRecord("pnd_Months_Times_Power", "#MONTHS-TIMES-POWER", FieldType.FLOAT, 8);
        pnd_Num_Pmts = localVariables.newFieldInRecord("pnd_Num_Pmts", "#NUM-PMTS", FieldType.NUMERIC, 8);
        pnd_Ann_Factor_Switch = localVariables.newFieldInRecord("pnd_Ann_Factor_Switch", "#ANN-FACTOR-SWITCH", FieldType.STRING, 1);
        pnd_Next_Pymnt_Dte_A8 = localVariables.newFieldInRecord("pnd_Next_Pymnt_Dte_A8", "#NEXT-PYMNT-DTE-A8", FieldType.STRING, 8);

        pnd_Next_Pymnt_Dte_A8__R_Field_18 = localVariables.newGroupInRecord("pnd_Next_Pymnt_Dte_A8__R_Field_18", "REDEFINE", pnd_Next_Pymnt_Dte_A8);
        pnd_Next_Pymnt_Dte_A8_Pnd_Next_Pymnt_Dte_N8 = pnd_Next_Pymnt_Dte_A8__R_Field_18.newFieldInGroup("pnd_Next_Pymnt_Dte_A8_Pnd_Next_Pymnt_Dte_N8", 
            "#NEXT-PYMNT-DTE-N8", FieldType.NUMERIC, 8);
        pnd_Cref_Summary_End_Date = localVariables.newFieldInRecord("pnd_Cref_Summary_End_Date", "#CREF-SUMMARY-END-DATE", FieldType.NUMERIC, 8);

        pnd_Cref_Summary_End_Date__R_Field_19 = localVariables.newGroupInRecord("pnd_Cref_Summary_End_Date__R_Field_19", "REDEFINE", pnd_Cref_Summary_End_Date);
        pnd_Cref_Summary_End_Date_Pnd_Cref_End_Century = pnd_Cref_Summary_End_Date__R_Field_19.newFieldInGroup("pnd_Cref_Summary_End_Date_Pnd_Cref_End_Century", 
            "#CREF-END-CENTURY", FieldType.NUMERIC, 2);
        pnd_Cref_Summary_End_Date_Pnd_Cref_End_Year = pnd_Cref_Summary_End_Date__R_Field_19.newFieldInGroup("pnd_Cref_Summary_End_Date_Pnd_Cref_End_Year", 
            "#CREF-END-YEAR", FieldType.NUMERIC, 2);
        pnd_Cref_Summary_End_Date_Pnd_Cref_End_Month = pnd_Cref_Summary_End_Date__R_Field_19.newFieldInGroup("pnd_Cref_Summary_End_Date_Pnd_Cref_End_Month", 
            "#CREF-END-MONTH", FieldType.NUMERIC, 2);
        pnd_Cref_Summary_End_Date_Pnd_Cref_End_Day = pnd_Cref_Summary_End_Date__R_Field_19.newFieldInGroup("pnd_Cref_Summary_End_Date_Pnd_Cref_End_Day", 
            "#CREF-END-DAY", FieldType.NUMERIC, 2);
        pnd_T_Cref_Ann_Factora = localVariables.newFieldInRecord("pnd_T_Cref_Ann_Factora", "#T-CREF-ANN-FACTORA", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Cref_Ann_Factora_1 = localVariables.newFieldInRecord("pnd_T_Cref_Ann_Factora_1", "#T-CREF-ANN-FACTORA-1", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Cref_Ann_Factora_2 = localVariables.newFieldInRecord("pnd_T_Cref_Ann_Factora_2", "#T-CREF-ANN-FACTORA-2", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Cref_Ann_Factora_To_Fiscal = localVariables.newFieldInRecord("pnd_T_Cref_Ann_Factora_To_Fiscal", "#T-CREF-ANN-FACTORA-TO-FISCAL", FieldType.PACKED_DECIMAL, 
            8, 5);
        pnd_T_Rea_Ann_Factora = localVariables.newFieldInRecord("pnd_T_Rea_Ann_Factora", "#T-REA-ANN-FACTORA", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Rea_Ann_Factora_1 = localVariables.newFieldInRecord("pnd_T_Rea_Ann_Factora_1", "#T-REA-ANN-FACTORA-1", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Rea_Ann_Factora_2 = localVariables.newFieldInRecord("pnd_T_Rea_Ann_Factora_2", "#T-REA-ANN-FACTORA-2", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Rea_Ann_Factora_To_Fiscal = localVariables.newFieldInRecord("pnd_T_Rea_Ann_Factora_To_Fiscal", "#T-REA-ANN-FACTORA-TO-FISCAL", FieldType.PACKED_DECIMAL, 
            8, 5);
        pnd_T_Gbpm_Ann_Factor = localVariables.newFieldInRecord("pnd_T_Gbpm_Ann_Factor", "#T-GBPM-ANN-FACTOR", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Std_Ann_Factor = localVariables.newFieldInRecord("pnd_T_Std_Ann_Factor", "#T-STD-ANN-FACTOR", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_T_Guar_Ann_Factor = localVariables.newFieldInRecord("pnd_T_Guar_Ann_Factor", "#T-GUAR-ANN-FACTOR", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_Temp_Fund_Code = localVariables.newFieldInRecord("pnd_Temp_Fund_Code", "#TEMP-FUND-CODE", FieldType.STRING, 1);
        pnd_Tiaa_Switch = localVariables.newFieldInRecord("pnd_Tiaa_Switch", "#TIAA-SWITCH", FieldType.STRING, 1);

        pnd_Tiaa_Mortality_Arraysa = localVariables.newGroupInRecord("pnd_Tiaa_Mortality_Arraysa", "#TIAA-MORTALITY-ARRAYSA");

        pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays = pnd_Tiaa_Mortality_Arraysa.newGroupInGroup("pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays", 
            "#T-MORT-ANN-FACTORA-ARRAYS");
        pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Std_Ann_Factora = pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays.newFieldInGroup("pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Std_Ann_Factora", 
            "#T-GUAR-STD-ANN-FACTORA", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Grd_Ann_Factora = pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays.newFieldInGroup("pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Grd_Ann_Factora", 
            "#T-GUAR-GRD-ANN-FACTORA", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Std_Ann_Factora = pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays.newFieldInGroup("pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Std_Ann_Factora", 
            "#T-TOT-STD-ANN-FACTORA", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Grd_Ann_Factora = pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays.newFieldInGroup("pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Grd_Ann_Factora", 
            "#T-TOT-GRD-ANN-FACTORA", FieldType.PACKED_DECIMAL, 8, 5);

        pnd_Tiaa_Mortality_Arraysa__R_Field_20 = pnd_Tiaa_Mortality_Arraysa.newGroupInGroup("pnd_Tiaa_Mortality_Arraysa__R_Field_20", "REDEFINE", pnd_Tiaa_Mortality_Arraysa_Pnd_T_Mort_Ann_Factora_Arrays);

        pnd_Tiaa_Mortality_Arraysa_Pnd_Filler4 = pnd_Tiaa_Mortality_Arraysa__R_Field_20.newGroupArrayInGroup("pnd_Tiaa_Mortality_Arraysa_Pnd_Filler4", 
            "#FILLER4", new DbsArrayController(1, 4));
        pnd_Tiaa_Mortality_Arraysa_Pnd_T_Ann_Factora = pnd_Tiaa_Mortality_Arraysa_Pnd_Filler4.newFieldInGroup("pnd_Tiaa_Mortality_Arraysa_Pnd_T_Ann_Factora", 
            "#T-ANN-FACTORA", FieldType.PACKED_DECIMAL, 8, 5);

        pnd_Days_In_Month_Array = localVariables.newGroupInRecord("pnd_Days_In_Month_Array", "#DAYS-IN-MONTH-ARRAY");
        pnd_Days_In_Month_Array_Pnd_Jan = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Jan", "#JAN", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Feb = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Feb", "#FEB", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Mar = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Mar", "#MAR", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Apr = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Apr", "#APR", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_May = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_May", "#MAY", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Jun = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Jun", "#JUN", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Jul = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Jul", "#JUL", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Aug = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Aug", "#AUG", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Sep = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Sep", "#SEP", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Oct = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Oct", "#OCT", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Nov = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Nov", "#NOV", FieldType.NUMERIC, 2);
        pnd_Days_In_Month_Array_Pnd_Dec = pnd_Days_In_Month_Array.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Dec", "#DEC", FieldType.NUMERIC, 2);

        pnd_Days_In_Month_Array__R_Field_21 = localVariables.newGroupInRecord("pnd_Days_In_Month_Array__R_Field_21", "REDEFINE", pnd_Days_In_Month_Array);

        pnd_Days_In_Month_Array_Pnd_Filler5 = pnd_Days_In_Month_Array__R_Field_21.newGroupArrayInGroup("pnd_Days_In_Month_Array_Pnd_Filler5", "#FILLER5", 
            new DbsArrayController(1, 12));
        pnd_Days_In_Month_Array_Pnd_Days_In_Month = pnd_Days_In_Month_Array_Pnd_Filler5.newFieldInGroup("pnd_Days_In_Month_Array_Pnd_Days_In_Month", "#DAYS-IN-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Result_Year_Dec = localVariables.newFieldInRecord("pnd_Result_Year_Dec", "#RESULT-YEAR-DEC", FieldType.NUMERIC, 9, 5);
        pnd_Auv_Year_Dec = localVariables.newFieldInRecord("pnd_Auv_Year_Dec", "#AUV-YEAR-DEC", FieldType.NUMERIC, 9, 5);
        pnd_Result_Rem = localVariables.newFieldInRecord("pnd_Result_Rem", "#RESULT-REM", FieldType.NUMERIC, 5, 5);
        pnd_Tiaa_Current_Ia_Rb = localVariables.newFieldInRecord("pnd_Tiaa_Current_Ia_Rb", "#TIAA-CURRENT-IA-RB", FieldType.NUMERIC, 2);
        pnd_Fund_Code_Temp = localVariables.newFieldInRecord("pnd_Fund_Code_Temp", "#FUND-CODE-TEMP", FieldType.STRING, 1);
        pnd_Pmt_Month_1 = localVariables.newFieldInRecord("pnd_Pmt_Month_1", "#PMT-MONTH-1", FieldType.NUMERIC, 2);
        pnd_Pmt_Month_2 = localVariables.newFieldInRecord("pnd_Pmt_Month_2", "#PMT-MONTH-2", FieldType.NUMERIC, 2);
        pnd_Pmt_Month_3 = localVariables.newFieldInRecord("pnd_Pmt_Month_3", "#PMT-MONTH-3", FieldType.NUMERIC, 2);
        pnd_Pmt_Month_4 = localVariables.newFieldInRecord("pnd_Pmt_Month_4", "#PMT-MONTH-4", FieldType.NUMERIC, 2);
        pnd_Pmt_Switch = localVariables.newFieldInRecord("pnd_Pmt_Switch", "#PMT-SWITCH", FieldType.NUMERIC, 1);
        pnd_Annuity_Factor_1 = localVariables.newFieldInRecord("pnd_Annuity_Factor_1", "#ANNUITY-FACTOR-1", FieldType.NUMERIC, 8, 5);
        pnd_Annuity_Factor_2 = localVariables.newFieldInRecord("pnd_Annuity_Factor_2", "#ANNUITY-FACTOR-2", FieldType.NUMERIC, 8, 5);
        pnd_Annuity_Factor_To_Fiscal = localVariables.newFieldInRecord("pnd_Annuity_Factor_To_Fiscal", "#ANNUITY-FACTOR-TO-FISCAL", FieldType.NUMERIC, 
            8, 5);
        pnd_Auv_Revaluation = localVariables.newFieldInRecord("pnd_Auv_Revaluation", "#AUV-REVALUATION", FieldType.NUMERIC, 7, 4);
        pnd_Auv_Revaluation_From = localVariables.newFieldInRecord("pnd_Auv_Revaluation_From", "#AUV-REVALUATION-FROM", FieldType.NUMERIC, 7, 4);
        pnd_Temp_In_Mortality_Table = localVariables.newFieldInRecord("pnd_Temp_In_Mortality_Table", "#TEMP-IN-MORTALITY-TABLE", FieldType.NUMERIC, 4);
        pnd_Temp_In_Setback = localVariables.newFieldInRecord("pnd_Temp_In_Setback", "#TEMP-IN-SETBACK", FieldType.NUMERIC, 7, 5);
        pnd_Temp_In_Interest_Rate = localVariables.newFieldInRecord("pnd_Temp_In_Interest_Rate", "#TEMP-IN-INTEREST-RATE", FieldType.NUMERIC, 4, 4);
        pnd_Temp_In_Def_Interest_Rate = localVariables.newFieldInRecord("pnd_Temp_In_Def_Interest_Rate", "#TEMP-IN-DEF-INTEREST-RATE", FieldType.NUMERIC, 
            4, 4);
        pnd_Deferred_Period_Save = localVariables.newFieldInRecord("pnd_Deferred_Period_Save", "#DEFERRED-PERIOD-SAVE", FieldType.NUMERIC, 3);
        pnd_Total_Units_To_Receive = localVariables.newFieldInRecord("pnd_Total_Units_To_Receive", "#TOTAL-UNITS-TO-RECEIVE", FieldType.NUMERIC, 8, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAial0131.initializeValues();
        ldaAial013b.initializeValues();
        ldaAial0270.initializeValues();
        ldaAial0271.initializeValues();

        localVariables.reset();
        pnd_Tiaa_Sub.setInitialValue(0);
        pnd_Cref_Sub.setInitialValue(0);
        pnd_Gbpm_Int_Rate.setInitialValue(0);
        pnd_One.setInitialValue(1.0E0);
        pnd_Num_Pmts.setInitialValue(0);
        pnd_Ann_Factor_Switch.setInitialValue("N");
        pnd_Days_In_Month_Array_Pnd_Jan.setInitialValue(31);
        pnd_Days_In_Month_Array_Pnd_Feb.setInitialValue(28);
        pnd_Days_In_Month_Array_Pnd_Mar.setInitialValue(31);
        pnd_Days_In_Month_Array_Pnd_Apr.setInitialValue(30);
        pnd_Days_In_Month_Array_Pnd_May.setInitialValue(31);
        pnd_Days_In_Month_Array_Pnd_Jun.setInitialValue(30);
        pnd_Days_In_Month_Array_Pnd_Jul.setInitialValue(31);
        pnd_Days_In_Month_Array_Pnd_Aug.setInitialValue(31);
        pnd_Days_In_Month_Array_Pnd_Sep.setInitialValue(30);
        pnd_Days_In_Month_Array_Pnd_Oct.setInitialValue(31);
        pnd_Days_In_Month_Array_Pnd_Nov.setInitialValue(30);
        pnd_Days_In_Month_Array_Pnd_Dec.setInitialValue(31);
        pnd_Total_Units_To_Receive.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Aian013r() throws Exception
    {
        super("Aian013r");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* *RMAT (2) LS=133                                                                                                                                              //Natural: FORMAT LS = 132 PS = 66 ES = ON
        //*  WRITE '#ILLUSTRATION-EFF-DATE = ' #ILLUSTRATION-EFF-DATE
        //*  WRITE '#CONTRACT-PAYEE = ' #CONTRACT-PAYEE
        //*  WRITE '#ILLUSTRATION-EFF-DATE = ' #ILLUSTRATION-EFF-DATE
        //*  WRITE '#NEXT-PYMNT-DTE = ' #NEXT-PYMNT-DTE (EM=YYYYMMDD)
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("A") && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().less(19980331))) //Natural: IF #TYPE-OF-RUN = 'A' AND #ILLUSTRATION-EFF-DATE < 19980331
        {
            //*  WRITE '#CONTRACT-PAYEE = ' #CONTRACT-PAYEE
            //*  WRITE '#ILLUSTRATION-EFF-DATE = ' #ILLUSTRATION-EFF-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(11);                                                                                            //Natural: MOVE 11 TO #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            pnd_Total_Units_To_Receive.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I));                                                   //Natural: COMPUTE #TOTAL-UNITS-TO-RECEIVE = #TOTAL-UNITS-TO-RECEIVE + #UNITS-TO-RECEIVE ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().notEquals(pnd_Total_Units_To_Receive)))                                                     //Natural: IF #TRANSFER-UNITS <> #TOTAL-UNITS-TO-RECEIVE
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(13);                                                                                            //Natural: MOVE 13 TO #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 19
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(19)); pnd_I.nadd(1))
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_I).setValue(" ");                                                                         //Natural: MOVE ' ' TO #FUND-CODE-OUT ( #I )
            //*  WRITE '#FUND-CODE-OUT (' #I ') = ' #FUND-CODE-OUT (#I)
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_I).setValue(0);                                                                               //Natural: MOVE 0 TO #UNITS-OUT ( #I ) #AUV-OUT ( #I ) #TRANSFER-AMT-OUT-CREF ( #I )
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_I).setValue(0);
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_I).setValue(0);
            //*  WRITE '#UNITS-OUT (' #I ') = ' #UNITS-OUT (#I)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(pnd_I).setValue(" ");                                                                   //Natural: MOVE ' ' TO #PMT-METHOD-CODE-OUT ( #I )
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(pnd_I).setValue(0);                                                                           //Natural: MOVE 0 TO #RATE-CODE-OUT ( #I ) #GTD-PMT-OUT ( #I ) #DVD-PMT-OUT ( #I ) #TRANSFER-AMT-OUT-TIAA ( #I )
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_I).setValue(0);
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_I).setValue(0);
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_I).setValue(0);
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().setValue(0);                                                                                      //Natural: MOVE 0 TO #TOTAL-TRANSFER-AMT-OUT #TRANSFER-EFF-DATE-OUT #RETURN-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Eff_Date_Out().setValue(0);
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(0);
        pnd_Pmt_Month_1.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Month());                                                                                   //Natural: MOVE #ISSUE-MONTH TO #PMT-MONTH-1
        short decideConditionsMet731 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #MODE-1;//Natural: VALUE 1
        if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode_1().equals(1))))
        {
            decideConditionsMet731++;
            pnd_Months.setValue(12);                                                                                                                                      //Natural: MOVE 12 TO #MONTHS
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode_1().equals(6))))
        {
            decideConditionsMet731++;
            pnd_Months.setValue(4);                                                                                                                                       //Natural: MOVE 4 TO #MONTHS
            pnd_Pmt_Month_2.compute(new ComputeParameters(false, pnd_Pmt_Month_2), pnd_Pmt_Month_1.add(3));                                                               //Natural: COMPUTE #PMT-MONTH-2 = #PMT-MONTH-1 + 3
            if (condition(pnd_Pmt_Month_2.greater(12)))                                                                                                                   //Natural: IF #PMT-MONTH-2 > 12
            {
                pnd_Pmt_Month_2.nsubtract(12);                                                                                                                            //Natural: SUBTRACT 12 FROM #PMT-MONTH-2
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pmt_Month_3.compute(new ComputeParameters(false, pnd_Pmt_Month_3), pnd_Pmt_Month_2.add(3));                                                               //Natural: COMPUTE #PMT-MONTH-3 = #PMT-MONTH-2 + 3
            if (condition(pnd_Pmt_Month_3.greater(12)))                                                                                                                   //Natural: IF #PMT-MONTH-3 > 12
            {
                pnd_Pmt_Month_3.nsubtract(12);                                                                                                                            //Natural: SUBTRACT 12 FROM #PMT-MONTH-3
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pmt_Month_4.compute(new ComputeParameters(false, pnd_Pmt_Month_4), pnd_Pmt_Month_3.add(3));                                                               //Natural: COMPUTE #PMT-MONTH-4 = #PMT-MONTH-3 + 3
            if (condition(pnd_Pmt_Month_4.greater(12)))                                                                                                                   //Natural: IF #PMT-MONTH-4 > 12
            {
                pnd_Pmt_Month_4.nsubtract(12);                                                                                                                            //Natural: SUBTRACT 12 FROM #PMT-MONTH-4
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode_1().equals(7))))
        {
            decideConditionsMet731++;
            pnd_Months.setValue(2);                                                                                                                                       //Natural: MOVE 2 TO #MONTHS
            pnd_Pmt_Month_2.compute(new ComputeParameters(false, pnd_Pmt_Month_2), pnd_Pmt_Month_1.add(6));                                                               //Natural: COMPUTE #PMT-MONTH-2 = #PMT-MONTH-1 + 6
            if (condition(pnd_Pmt_Month_2.greater(12)))                                                                                                                   //Natural: IF #PMT-MONTH-2 > 12
            {
                pnd_Pmt_Month_2.nsubtract(12);                                                                                                                            //Natural: SUBTRACT 12 FROM #PMT-MONTH-2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode_1().equals(8))))
        {
            decideConditionsMet731++;
            pnd_Months.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #MONTHS
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(1);                                                                                             //Natural: MOVE 1 TO #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Months_Between_Pmts.compute(new ComputeParameters(false, pnd_Months_Between_Pmts), DbsField.divide(12,pnd_Months));                                           //Natural: COMPUTE #MONTHS-BETWEEN-PMTS = 12 / #MONTHS
        //*  IF #TYPE-OF-RUN = 'A'
        //*    MOVE #FUND-CODE TO #FUND-CODE-TEMP
        //*    PERFORM GET-LATEST-REVALUED-AUV
        //*    MOVE #AUV-REVALUATION TO #AUV-REVALUATION-FROM
        //*  END-IF
                                                                                                                                                                          //Natural: PERFORM GET-AUV-DATE
        sub_Get_Auv_Date();
        if (condition(Global.isEscape())) {return;}
        //*  IF #RETURN-CODE �= 0
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                                     //Natural: IF #RETURN-CODE-NBR <> 0
        {
            //*  WRITE '*** AFTER GET-AUV-DATE *** #RETURN-CODE = ' #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().notEquals(99999999) && (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("A")  //Natural: IF #ILLUSTRATION-EFF-DATE NOT = 99999999 AND ( #TYPE-OF-RUN = 'A' OR ( #TYPE-OF-RUN = 'I' AND #ILLUSTRATION-EFF-DATE NOT > #AUV-DATE ) )
            || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("I") && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().lessOrEqual(pnd_Auv_Date)))))
        {
            pnd_Auv_Date.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date());                                                                        //Natural: MOVE #ILLUSTRATION-EFF-DATE TO #AUV-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Age_As_Of_Date.setValue(pnd_Auv_Date);                                                                                                                        //Natural: MOVE #AUV-DATE TO #AGE-AS-OF-DATE #TRANSFER-EFF-DATE-OUT
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Eff_Date_Out().setValue(pnd_Auv_Date);
        //*  WRITE '#AUV-DATE = ' #AUV-DATE
        pnd_Auv_Year_Dec.setValue(pnd_Auv_Date_Pnd_Auv_Year);                                                                                                             //Natural: MOVE #AUV-YEAR TO #AUV-YEAR-DEC
        pnd_Result_Rem.compute(new ComputeParameters(false, pnd_Result_Rem), pnd_Auv_Year_Dec.mod(4));                                                                    //Natural: DIVIDE 4 INTO #AUV-YEAR-DEC GIVING #RESULT-YEAR-DEC REMAINDER #RESULT-REM
        pnd_Result_Year_Dec.compute(new ComputeParameters(false, pnd_Result_Year_Dec), pnd_Auv_Year_Dec.divide(4));
        if (condition(pnd_Result_Rem.equals(getZero())))                                                                                                                  //Natural: IF #RESULT-REM = 0
        {
            pnd_Days_In_Month_Array_Pnd_Days_In_Month.getValue(2).setValue(29);                                                                                           //Natural: MOVE 29 TO #DAYS-IN-MONTH ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("F");                                                                                          //Natural: MOVE 'F' TO #AIAN026-CALL-TYPE
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(pnd_Auv_Date);                                                                         //Natural: MOVE #AUV-DATE TO #AIAN026-IAUV-REQUEST-DATE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' USING #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        //*  IF #AIAN026-RETURN-CODE NOT = 0
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            getReports().write(0, "AIAN026 RETURN CODE = ",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                                                  //Natural: WRITE 'AIAN026 RETURN CODE = ' #AIAN026-RETURN-CODE
            if (Global.isEscape()) return;
            //*   WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(3);                                                                                             //Natural: MOVE 3 TO #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Auv_Old_1.setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());                                                              //Natural: MOVE #AIAN026-INTERIM-ANNUITY-UNIT-VA TO #AUV-OLD-1
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
        //*  WRITE '#AUV-DATE = ' #AUV-DATE
        //*  WRITE '#AUV-OLD-1 = ' #AUV-OLD-1
        //*  WRITE '#AUV-DATE-PLUS-1 = ' #AUV-DATE-PLUS-1
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().notEquals(99999999)))                                                                //Natural: IF #ILLUSTRATION-EFF-DATE NOT = 99999999
        {
            pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Date_Plus_1_A8.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date());                                    //Natural: MOVE #ILLUSTRATION-EFF-DATE TO #IARTMG-DATE-PLUS-1-A8
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Date_Plus_1_A8.setValue(pnd_Auv_Date);                                                                                      //Natural: MOVE #AUV-DATE TO #IARTMG-DATE-PLUS-1-A8
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Temp_Iartmg_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Date_Plus_1_A8);                                             //Natural: MOVE EDITED #IARTMG-DATE-PLUS-1-A8 TO #TEMP-IARTMG-DATE ( EM = YYYYMMDD )
        pnd_Temp_Iartmg_Date.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TEMP-IARTMG-DATE
        pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Date_Plus_1_A8.setValueEdited(pnd_Temp_Iartmg_Date,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED #TEMP-IARTMG-DATE ( EM = YYYYMMDD ) TO #IARTMG-DATE-PLUS-1-A8
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("I") && pnd_Age_As_Of_Date.less(19980401)))                                             //Natural: IF #TYPE-OF-RUN = 'I' AND #AGE-AS-OF-DATE < 19980401
        {
            pnd_Auv_Date.setValue(19980401);                                                                                                                              //Natural: MOVE 19980401 TO #AUV-DATE
            pnd_Age_As_Of_Date.setValue(19980331);                                                                                                                        //Natural: MOVE 19980331 TO #AGE-AS-OF-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Age_As_Of_Date.setValue(pnd_Auv_Date);                                                                                                                    //Natural: MOVE #AUV-DATE TO #AGE-AS-OF-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VE #AUV-DATE TO #AUV-DATE-PLUS-1
        pnd_Auv_Date_Plus_1_Pnd_Auv_Date_Plus_1_A8.setValue(pnd_Age_As_Of_Date);                                                                                          //Natural: MOVE #AGE-AS-OF-DATE TO #AUV-DATE-PLUS-1-A8
        //*  WRITE '#AGE-AS-OF-DATE = ' #AGE-AS-OF-DATE
        pnd_Temp_Auv_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Auv_Date_Plus_1_Pnd_Auv_Date_Plus_1_A8);                                                      //Natural: MOVE EDITED #AUV-DATE-PLUS-1-A8 TO #TEMP-AUV-DATE ( EM = YYYYMMDD )
        pnd_Temp_Auv_Date.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TEMP-AUV-DATE
        pnd_Auv_Date_Plus_1_Pnd_Auv_Date_Plus_1_A8.setValueEdited(pnd_Temp_Auv_Date,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #TEMP-AUV-DATE ( EM = YYYYMMDD ) TO #AUV-DATE-PLUS-1-A8
        //*  WRITE 'AFTER ADDING 1 DAY'
        //*  WRITE '#AUV-DATE-PLUS-1 = ' #AUV-DATE-PLUS-1
        //*  ADD 1 TO #AUV-DAY-PLUS-1
        //*  IF #AUV-DAY-PLUS-1 > #DAYS-IN-MONTH (#AUV-MONTH)
        //*    ADD 1 TO #AUV-MONTH-PLUS-1
        //*    MOVE 1 TO #AUV-DAY-PLUS-1
        //*    IF #AUV-MONTH-PLUS-1 = 13
        //*      ADD 1 TO #AUV-YEAR-PLUS-1
        //*      MOVE 1 TO #AUV-MONTH-PLUS-1
        //*    END-IF
        //*  END-IF
        //*  WRITE '#AUV-DATE = ' #AUV-DATE ' #AGE-AS-OF-DATE = ' #AGE-AS-OF-DATE
        pnd_Temp_Fund_Code.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code());                                                                                  //Natural: MOVE #FUND-CODE TO #TEMP-FUND-CODE
                                                                                                                                                                          //Natural: PERFORM GET-FACTOR-DATE
        sub_Get_Factor_Date();
        if (condition(Global.isEscape())) {return;}
        //*  IF #RETURN-CODE �= 0
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                                     //Natural: IF #RETURN-CODE-NBR <> 0
        {
            //*  WRITE '*** AFTER GET-FACTOR-DATE #RETURN-CODE = ' #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CREF-MORTALITY
        sub_Get_Cref_Mortality();
        if (condition(Global.isEscape())) {return;}
        //*  IF #RETURN-CODE �= 0
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                                     //Natural: IF #RETURN-CODE-NBR <> 0
        {
            //*  WRITE '*** AFTER GET-CREF-MORTALITY #RETURN-CODE = ' #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Date_Pnd_Temp_Date_A8.setValueEdited(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED #NEXT-PYMNT-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-A8
        pnd_Num_Pmts.setValue(0);                                                                                                                                         //Natural: MOVE 0 TO #NUM-PMTS
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CERTAIN-PERIOD
        sub_Determine_Certain_Period();
        if (condition(Global.isEscape())) {return;}
        pnd_Next_Pymnt_Dte_A8.setValueEdited(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #NEXT-PYMNT-DTE ( EM = YYYYMMDD ) TO #NEXT-PYMNT-DTE-A8
        pnd_Ws_Date_Pnd_Temp_Date.setValue(pnd_Auv_Date_Plus_1);                                                                                                          //Natural: MOVE #AUV-DATE-PLUS-1 TO #TEMP-DATE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DEFERRED-PERIOD
        sub_Determine_Deferred_Period();
        if (condition(Global.isEscape())) {return;}
        pnd_Deferred_Period_Save.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period());                                                           //Natural: MOVE #AIAN021-IN-DEFERRED-PERIOD TO #DEFERRED-PERIOD-SAVE
        pnd_Temp_In_Mortality_Table.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Code());                                                               //Natural: MOVE #FUND-FROM-MORT-CODE TO #TEMP-IN-MORTALITY-TABLE
        pnd_Temp_In_Setback.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Setback());                                                                    //Natural: MOVE #FUND-FROM-MORT-SETBACK TO #TEMP-IN-SETBACK
        pnd_Temp_In_Interest_Rate.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Interest());                                                             //Natural: MOVE #FUND-FROM-MORT-INTEREST TO #TEMP-IN-INTEREST-RATE
        pnd_Temp_In_Def_Interest_Rate.setValue(0);                                                                                                                        //Natural: MOVE .04 TO #TEMP-IN-DEF-INTEREST-RATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-FISCAL-FACTORS
        sub_Calculate_Fiscal_Factors();
        if (condition(Global.isEscape())) {return;}
        //*  PERFORM MOVE-TO-ANN-FACT-LINKAGE
        //*  IF #RETURN-CODE �= 0
        //*    ESCAPE ROUTINE
        //*  END-IF
        //*  MOVE #FUND-FROM-MORT-CODE TO #AIAN021-IN-MORTALITY-TABLE
        //*  MOVE #FUND-FROM-MORT-SETBACK TO #AIAN021-IN-SETBACK
        //*  MOVE #FUND-FROM-MORT-INTEREST TO #AIAN021-IN-INTEREST-RATE
        //*  MOVE .04 TO #AIAN021-IN-DEF-INTEREST-RATE
        //*  WRITE '#AIAN021-LINKAGE = ' #AIAN021-LINKAGE
        //*  CALLNAT 'AIAN021' USING #AIAN021-LINKAGE
        //*   WRITE '#AIAN021-OUT-RETURN-CODE    = ' #AIAN021-OUT-RETURN-CODE
        //*  WRITE '#AIAN021-OUT-ANNUITY-FACTOR = ' #AIAN021-OUT-ANNUITY-FACTOR
        //*  IF #RETURN-CODE �= 0
        //*    MOVE 4 TO #RETURN-CODE
        //*    ESCAPE ROUTINE
        //*  ELSE IF #FUND-CODE = 'R'
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("R")))                                                                                    //Natural: IF #FUND-CODE = 'R'
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1().setValue(pnd_Annuity_Factor_1);                                                                   //Natural: MOVE #ANNUITY-FACTOR-1 TO #T-REA-ANN-FACTOR-1
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2().setValue(pnd_Annuity_Factor_2);                                                                   //Natural: MOVE #ANNUITY-FACTOR-2 TO #T-REA-ANN-FACTOR-2
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal().setValue(pnd_Annuity_Factor_To_Fiscal);                                                   //Natural: MOVE #ANNUITY-FACTOR-TO-FISCAL TO #T-REA-ANN-FACTOR-TO-FISCAL
            //*  MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #T-REA-ANN-FACTOR
            //*  WRITE '#T-REA-ANN-FACTOR = ' #T-REA-ANN-FACTOR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1().setValue(pnd_Annuity_Factor_1);                                                                  //Natural: MOVE #ANNUITY-FACTOR-1 TO #T-CREF-ANN-FACTOR-1
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2().setValue(pnd_Annuity_Factor_2);                                                                  //Natural: MOVE #ANNUITY-FACTOR-2 TO #T-CREF-ANN-FACTOR-2
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal().setValue(pnd_Annuity_Factor_To_Fiscal);                                                  //Natural: MOVE #ANNUITY-FACTOR-TO-FISCAL TO #T-CREF-ANN-FACTOR-TO-FISCAL
            //*  MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #T-CREF-ANN-FACTOR
            //*  WRITE '#T-CREF-ANN-FACTOR = ' #T-CREF-ANN-FACTOR
        }                                                                                                                                                                 //Natural: END-IF
        //*  END-IF
        //*  WRITE '#AIAN021-OUT-ANNUITY-FACTOR = ' #AIAN021-OUT-ANNUITY-FACTOR
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().setValue(0);                                                                                      //Natural: MOVE 0 TO #TOTAL-TRANSFER-AMT-OUT
        pnd_Tiaa_Switch.setValue("N");                                                                                                                                    //Natural: MOVE 'N' TO #TIAA-SWITCH
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("1") || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("2"))) //Natural: IF #FUND-TO-RECEIVE ( #I ) = '1' OR = '2'
            {
                pnd_Tiaa_Switch.setValue("Y");                                                                                                                            //Natural: MOVE 'Y' TO #TIAA-SWITCH
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Tiaa_Switch.equals("Y")))                                                                                                                       //Natural: IF #TIAA-SWITCH = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM GET-TIAA-MORTALITY
            sub_Get_Tiaa_Mortality();
            if (condition(Global.isEscape())) {return;}
            //*  IF #RETURN-CODE �= 0
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                                 //Natural: IF #RETURN-CODE-NBR <> 0
            {
                //*  WRITE '*** AFTER GET-TIAA-MORTALITY #RETURN-CODE = ' #RETURN-CODE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-ANN-FACT-LINKAGE
            sub_Move_To_Ann_Fact_Linkage();
            if (condition(Global.isEscape())) {return;}
            //*  IF #RETURN-CODE �= 0
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                                 //Natural: IF #RETURN-CODE-NBR <> 0
            {
                //*  WRITE '*** AFTER MOVE-TO-ANN-FACT-LINKAG #RETURN-CODE = ' #RETURN-CODE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().setValue(pnd_Deferred_Period_Save);                                                       //Natural: MOVE #DEFERRED-PERIOD-SAVE TO #AIAN021-IN-DEFERRED-PERIOD
            FOR05:                                                                                                                                                        //Natural: FOR #I = 1 TO 4
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
            {
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Mort_Tbl_Code().getValue(pnd_I));   //Natural: MOVE #T-MORT-TBL-CODE ( #I ) TO #AIAN021-IN-MORTALITY-TABLE
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Mort_Tbl_Setback().getValue(pnd_I));        //Natural: MOVE #T-MORT-TBL-SETBACK ( #I ) TO #AIAN021-IN-SETBACK
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Mort_Tbl_Interest().getValue(pnd_I)); //Natural: MOVE #T-MORT-TBL-INTEREST ( #I ) TO #AIAN021-IN-INTEREST-RATE
                //*    ******************************************
                //*    IF #I = 3
                //*      MOVE .0675 TO #AIAN021-IN-INTEREST-RATE
                //*    END-IF
                //*    ******************************************
                if (condition(pnd_I.equals(4)))                                                                                                                           //Natural: IF #I = 4
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Std_Mort_Tbl_Interest()); //Natural: MOVE #T-TOT-STD-MORT-TBL-INTEREST TO #AIAN021-IN-DEF-INTEREST-RATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Mort_Tbl_Interest().getValue(pnd_I)); //Natural: MOVE #T-MORT-TBL-INTEREST ( #I ) TO #AIAN021-IN-DEF-INTEREST-RATE
                }                                                                                                                                                         //Natural: END-IF
                //*    *********************************************************
                //*    IF #I = 3 OR = 4
                //*      MOVE .0675 TO #AIAN021-IN-DEF-INTEREST-RATE
                //*    END-IF
                //*    *********************************************************
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DEFERRED-IN-FISCAL
                sub_Determine_Deferred_In_Fiscal();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Date_Pnd_Temp_Date_A8.setValueEdited(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte(),new ReportEditMask("YYYYMMDD"));                      //Natural: MOVE EDITED #NEXT-PYMNT-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-A8
                pnd_Num_Pmts.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #NUM-PMTS
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CERTAIN-PERIOD
                sub_Determine_Certain_Period();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period().compute(new ComputeParameters(true, pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period()),  //Natural: COMPUTE ROUNDED #AIAN021-IN-CERTAIN-PERIOD = #NUM-PMTS / #MONTHS
                    pnd_Num_Pmts.divide(pnd_Months));
                DbsUtil.callnat(Aian021.class , getCurrentProcessState(), pdaAial0210.getPnd_Aian021_Linkage());                                                          //Natural: CALLNAT 'AIAN021' USING #AIAN021-LINKAGE
                if (condition(Global.isEscape())) return;
                //*    WRITE '#AIAN021-LINKAGE = ' #AIAN021-LINKAGE
                if (condition(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code_Nbr().notEquals(getZero())))                                                 //Natural: IF #AIAN021-OUT-RETURN-CODE-NBR <> 0
                {
                    //*  WRITE '*** AFTER CALL TO AIAN021 #RETURN-CODE = ' #RETURN-CODE
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(8);                                                                                     //Natural: MOVE 8 TO #RETURN-CODE
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tiaa_Mortality_Arraysa_Pnd_T_Ann_Factora.getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());           //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #T-ANN-FACTORA ( #I )
                    //*      WRITE '#AIAN021-OUT-ANNUITY-FACTOR = ' #AIAN021-OUT-ANNUITY-FACTOR
                    //*      WRITE 'TIAA ANNUITY FACTOR ' #I ':'
                    //*      WRITE '#AIAN021-LINKAGE = ' #AIAN021-LINKAGE
                    //*      WRITE '#T-ANN-FACTOR (' #I ') = ' #T-ANN-FACTOR (#I)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet948 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANSFER-REVAL-SWITCH;//Natural: VALUE '0'
        if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().equals("0"))))
        {
            decideConditionsMet948++;
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator());                  //Natural: MOVE #REVALUATION-INDICATOR TO #AIAN026-REVALUATION-METHOD
        }                                                                                                                                                                 //Natural: VALUE '1'
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().equals("1"))))
        {
            decideConditionsMet948++;
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("A")))                                                                    //Natural: IF #REVALUATION-INDICATOR = 'A'
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(9);                                                                                         //Natural: MOVE 9 TO #RETURN-CODE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("A");                                                                         //Natural: MOVE 'A' TO #AIAN026-REVALUATION-METHOD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().equals("2"))))
        {
            decideConditionsMet948++;
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                    //Natural: IF #REVALUATION-INDICATOR = 'M'
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(10);                                                                                        //Natural: MOVE 10 TO #RETURN-CODE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("M");                                                                         //Natural: MOVE 'M' TO #AIAN026-REVALUATION-METHOD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(4);                                                                                             //Natural: MOVE 4 TO #RETURN-CODE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  WRITE '#TYPE-OF-RUN = ' #TYPE-OF-RUN
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            //*    WRITE '#CONTRACT-PAYEE = ' #CONTRACT-PAYEE
            short decideConditionsMet972 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF #FUND-TO-RECEIVE ( #I );//Natural: VALUE ' '
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals(" ")))
            {
                decideConditionsMet972++;
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("A")))                                                                          //Natural: IF #TYPE-OF-RUN = 'A'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-ADABAS-RECORD
                    sub_Write_Adabas_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  DY1
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
                //*    VALUE 'C', 'M', 'S', 'B', 'W', 'L', 'E','I','F'
            }                                                                                                                                                             //Natural: VALUE 'C', 'M', 'S', 'B', 'W', 'L', 'E','I','F','D'
            if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("C")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("M")) 
                || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("S")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("B")) 
                || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("W")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("L")) 
                || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("E")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("I")) 
                || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("F")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("D"))))
            {
                decideConditionsMet972++;
                //*  DY1
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("C") || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("M")            //Natural: IF #FUND-CODE = 'C' OR = 'M' OR = 'S' OR = 'B' OR = 'W' OR = 'L' OR = 'E' OR = 'I' OR = 'F' OR = 'D'
                    || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("S") || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("B") 
                    || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("W") || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("L") 
                    || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("E") || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("I") 
                    || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("F") || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("D")))
                {
                    //*          OR = 'E' OR = 'I' OR = 'F'
                                                                                                                                                                          //Natural: PERFORM FROM-CREF-TO-CREF
                    sub_From_Cref_To_Cref();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("R")))                                                                        //Natural: IF #FUND-CODE = 'R'
                    {
                                                                                                                                                                          //Natural: PERFORM FROM-REA-TO-CREF
                        sub_From_Rea_To_Cref();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'R'
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("R")))
            {
                decideConditionsMet972++;
                                                                                                                                                                          //Natural: PERFORM TO-REA
                sub_To_Rea();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE '1', '2'
            if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("1")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("2"))))
            {
                decideConditionsMet972++;
                                                                                                                                                                          //Natural: PERFORM TO-TIAA
                sub_To_Tiaa();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'V', 'N', 'O', 'P', 'Q', 'J', 'K', 'X'
            if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("V")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("N")) 
                || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("O")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("P")) 
                || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("Q")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("J")) 
                || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("K")) || (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("X"))))
            {
                decideConditionsMet972++;
                                                                                                                                                                          //Natural: PERFORM FROM-CREF-TO-CREF
                sub_From_Cref_To_Cref();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet972 == 0))
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(6);                                                                                         //Natural: MOVE 6 TO #RETURN-CODE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("A")))                                                                                  //Natural: IF #TYPE-OF-RUN = 'A'
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-ADABAS-RECORD
            sub_Write_Adabas_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CERTAIN-PERIOD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FACTOR-DATE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CREF-MORTALITY
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TIAA-MORTALITY
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV-DATE
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-LATEST-REVALUED-AUV
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-ANN-FACT-LINKAGE
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-DEFERRED-PERIOD
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-DEFERRED-IN-FISCAL
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-DEFERRED-AFTER-FISCAL
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-FISCAL-FACTORS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FROM-CREF-TO-CREF
        //*  (#PMT-NEW * #ANNUITY-FACTOR-TO-FISCAL)
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TO-REA
        //*  WRITE '#AIAN026-INTERIM-ANNUITY-UNIT-VALUE = '
        //* **** (#PMTS-TO-RECEIVE (#I) * #T-REA-ANN-FACTOR-TO-FISCAL)
        //* *********************
        //*  CHANGED FOR AIAP040
        //*      (#PMTS-TO-RECEIVE (#I) * #T-CREF-ANN-FACTOR-TO-FISCAL)
        //*      CHANGED FOR AIAP040
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FROM-REA-TO-CREF
        //*    (#PMTS-TO-RECEIVE (#I) * #T-REA-ANN-FACTOR-TO-FISCAL)
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TO-TIAA
        //*        (#UNITS-TO-RECEIVE (#I) * #AUV-OLD-1 * #T-REA-ANN-FACTOR-1)
        //*        + (#UNITS-TO-RECEIVE (#I) * #AUV-OLD-1 * #T-REA-ANN-FACTOR-2))
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ADABAS-RECORD
    }
    private void sub_Determine_Certain_Period() throws Exception                                                                                                          //Natural: DETERMINE-CERTAIN-PERIOD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //*  WRITE 'DETERMINE-CERTAIN-PERIOD'
        //*  WRITE '#TEMP-YEAR-MONTH = ' #TEMP-YEAR-MONTH
        //*  WRITE '#FINAL-PER-PAY-DATE = ' #FINAL-PER-PAY-DATE
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Ws_Date_Pnd_Temp_Year_Month.greater(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date()))) {break;}                                 //Natural: UNTIL #TEMP-YEAR-MONTH > #FINAL-PER-PAY-DATE
            pnd_Num_Pmts.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NUM-PMTS
            pnd_Ws_Date_Pnd_Ws_Month.nadd(pnd_Months_Between_Pmts);                                                                                                       //Natural: ADD #MONTHS-BETWEEN-PMTS TO #WS-MONTH
            if (condition(pnd_Ws_Date_Pnd_Ws_Month.greater(12)))                                                                                                          //Natural: IF #WS-MONTH > 12
            {
                pnd_Ws_Date_Pnd_Ws_Month.nsubtract(12);                                                                                                                   //Natural: SUBTRACT 12 FROM #WS-MONTH
                pnd_Ws_Date_Pnd_Ws_Year.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WS-YEAR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  WRITE '#NUM-PMTS = ' #NUM-PMTS
    }
    private void sub_Get_Factor_Date() throws Exception                                                                                                                   //Natural: GET-FACTOR-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  WRITE 'GET-FACTOR-DATE'
        if (condition(pnd_Temp_Fund_Code.equals("I")))                                                                                                                    //Natural: IF #TEMP-FUND-CODE = 'I'
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator().setValue("F");                                                                                 //Natural: MOVE 'F' TO WTC-FUND-INDICATOR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator().setValue(pnd_Temp_Fund_Code);                                                                  //Natural: MOVE #TEMP-FUND-CODE TO WTC-FUND-INDICATOR
        }                                                                                                                                                                 //Natural: END-IF
        ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Date().setValue(99999999);                                                                                          //Natural: MOVE 99999999 TO WTC-DATE
        ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Da_Rate().setValue(99);                                                                                             //Natural: MOVE 99 TO WTC-DA-RATE
        ldaAial0270.getVw_cratemgr_M_View().startDatabaseFind                                                                                                             //Natural: FIND CRATEMGR-M-VIEW WITH WTC-RECORD-KEY = WTC-RECORD-KEYS
        (
        "FIND01",
        new Wc[] { new Wc("WTC_RECORD_KEY", "=", ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Record_Keys(), WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAial0270.getVw_cratemgr_M_View().readNextRow("FIND01", true)))
        {
            ldaAial0270.getVw_cratemgr_M_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0270.getVw_cratemgr_M_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(5);                                                                                         //Natural: MOVE 5 TO #RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas().setValue(ldaAial0270.getCratemgr_M_View_Wtc_Tiaacref_Payout_Area());                    //Natural: MOVE WTC-TIAACREF-PAYOUT-AREA TO WTC-TIAACREF-PAYOUT-AREAS
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE 'WTC-TIAACREF-PAYOUT-RECORD = ' WTC-TIAACREF-PAYOUT-RECORD
    }
    private void sub_Get_Cref_Mortality() throws Exception                                                                                                                //Natural: GET-CREF-MORTALITY
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //* *****************************************************************
        //* ***   ADDED THE FOLLOWING CODE ON 12/30/97    GLENN FRIEDMAN
        //* *****************************************************************
        //*  WRITE 'GET-CREF-MORTALITY'
        //*  WRITE '#AUV-DATE = ' #AUV-DATE
        //*  MOVE EDITED #AUV-DATE-A8 TO #TEMP-AUV-DATE (EM=YYYYMMDD)
        //*  ADD 1 TO #TEMP-AUV-DATE
        //*  MOVE EDITED #TEMP-AUV-DATE (EM=YYYYMMDD) TO #AUV-DATE-A8
        //*  WRITE '#AUV-DATE = ' #AUV-DATE
        //*  WRITE '#AUV-DATE-PLUS-1-A8 = ' #AUV-DATE-PLUS-1-A8
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Century.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                                        //Natural: MOVE WTC-CREF-END-CENTURY TO #COMPARE-RATE-CENTURY
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Year.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                              //Natural: MOVE WTC-CREF-END-YEAR TO #COMPARE-RATE-YEAR
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Month.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                                            //Natural: MOVE WTC-CREF-END-MONTH TO #COMPARE-RATE-MONTH
        //*  MOVE #AUV-YEAR TO #COMPARE-AUV-YEAR
        //*  MOVE #AUV-MONTH TO #COMPARE-AUV-MONTH
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Year_Plus_1);                                                                //Natural: MOVE #IARTMG-YEAR-PLUS-1 TO #COMPARE-AUV-YEAR
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Month_Plus_1);                                                              //Natural: MOVE #IARTMG-MONTH-PLUS-1 TO #COMPARE-AUV-MONTH
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("A") && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().equals(19980331))) //Natural: IF #TYPE-OF-RUN = 'A' AND #TRANSFER-EFFECTIVE-DATE = 19980331
        {
            pnd_Compare_Auv_Date.setValue(199804);                                                                                                                        //Natural: MOVE 199804 TO #COMPARE-AUV-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *****
        if (condition(pnd_Compare_Auv_Date.less(pnd_Compare_Rate_Date)))                                                                                                  //Natural: IF #COMPARE-AUV-DATE < #COMPARE-RATE-DATE
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Century);                                               //Natural: MOVE #COMPARE-AUV-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year_2);                                                   //Natural: MOVE #COMPARE-AUV-YEAR-2 TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month);                                                   //Natural: MOVE #COMPARE-AUV-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                           //Natural: MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                 //Natural: MOVE WTC-CREF-END-YEAR TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                               //Natural: MOVE WTC-CREF-END-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************************************************
        //* ***   COMMENTED OUT THE FOLLOWING FOUR LINES OF CODE  G.F  12/30/97
        //* *******************************************************************
        //* * MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
        //* * MOVE WTC-CREF-END-YEAR    TO WTC-YEAR
        //* * MOVE WTC-CREF-END-MONTH   TO WTC-MONTH
        //* * MOVE 0                    TO WTC-DAY
        //*  WRITE 'WTC-RECORD-KEY = 'WTC-RECORD-KEY
        //*  WRITE 'WTC-RECORD-KEYS = 'WTC-RECORD-KEYS
        ldaAial0270.getVw_cratemgr_M_View().startDatabaseFind                                                                                                             //Natural: FIND CRATEMGR-M-VIEW WITH WTC-RECORD-KEY = WTC-RECORD-KEYS
        (
        "FIND02",
        new Wc[] { new Wc("WTC_RECORD_KEY", "=", ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Record_Keys(), WcType.WITH) }
        );
        FIND02:
        while (condition(ldaAial0270.getVw_cratemgr_M_View().readNextRow("FIND02", true)))
        {
            ldaAial0270.getVw_cratemgr_M_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0270.getVw_cratemgr_M_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                //*     WRITE 'NO RECORD FOUND'
                //*     WRITE 'WTC-RECORD-KEYS = 'WTC-RECORD-KEYS
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(5);                                                                                         //Natural: MOVE 5 TO #RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*   WRITE 'RECORD FOUND'
            //*  WRITE 'WTC-RECORD-KEYS = 'WTC-RECORD-KEYS
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas().setValue(ldaAial0270.getCratemgr_M_View_Wtc_Tiaacref_Payout_Area());                    //Natural: MOVE WTC-TIAACREF-PAYOUT-AREA TO WTC-TIAACREF-PAYOUT-AREAS
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Code().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code());                  //Natural: MOVE WTC-CREF-MORT-TABLE-CODE TO #FUND-FROM-MORT-CODE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Setback().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback());            //Natural: MOVE WTC-CREF-MORT-TABLE-SETBACK TO #FUND-FROM-MORT-SETBACK
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Interest().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest());          //Natural: MOVE WTC-CREF-MORT-TABLE-INTEREST TO #FUND-FROM-MORT-INTEREST
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE 'AFTER FIND'
        //*  WRITE 'WTC-CREF-MORT-TABLE-CODE     = ' WTC-CREF-MORT-TABLE-CODE
        //*  WRITE 'WTC-CREF-MORT-TABLE-SETBACK  = ' WTC-CREF-MORT-TABLE-SETBACK
        //*  WRITE 'WTC-CREF-MORT-TABLE-INTEREST = ' WTC-CREF-MORT-TABLE-INTEREST
    }
    private void sub_Get_Tiaa_Mortality() throws Exception                                                                                                                //Natural: GET-TIAA-MORTALITY
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Temp_Fund_Code.setValue("T");                                                                                                                                 //Natural: MOVE 'T' TO #TEMP-FUND-CODE
                                                                                                                                                                          //Natural: PERFORM GET-FACTOR-DATE
        sub_Get_Factor_Date();
        if (condition(Global.isEscape())) {return;}
        ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Da_Rate().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DA-RATE
        //* *****************************************************************
        //* ***   ADDED THE FOLLOWING CODE ON 12/30/97    GLENN FRIEDMAN
        //* *****************************************************************
        //*  WRITE 'GET-CREF-MORTALITY'
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Century.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                                        //Natural: MOVE WTC-CREF-END-CENTURY TO #COMPARE-RATE-CENTURY
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Year.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                              //Natural: MOVE WTC-CREF-END-YEAR TO #COMPARE-RATE-YEAR
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Month.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                                            //Natural: MOVE WTC-CREF-END-MONTH TO #COMPARE-RATE-MONTH
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Year_Plus_1);                                                                //Natural: MOVE #IARTMG-YEAR-PLUS-1 TO #COMPARE-AUV-YEAR
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Month_Plus_1);                                                              //Natural: MOVE #IARTMG-MONTH-PLUS-1 TO #COMPARE-AUV-MONTH
        //* *****
        if (condition(pnd_Compare_Auv_Date.less(pnd_Compare_Rate_Date)))                                                                                                  //Natural: IF #COMPARE-AUV-DATE < #COMPARE-RATE-DATE
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Century);                                               //Natural: MOVE #COMPARE-AUV-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year_2);                                                   //Natural: MOVE #COMPARE-AUV-YEAR-2 TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month);                                                   //Natural: MOVE #COMPARE-AUV-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                           //Natural: MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                 //Natural: MOVE WTC-CREF-END-YEAR TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                               //Natural: MOVE WTC-CREF-END-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************************************************************
        //* ***   COMMENTED OUT THE FOLLOWING FOUR LINES OF CODE  G.F  12/30/97
        //* *****************************************************************
        //* * MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
        //* * MOVE WTC-CREF-END-YEAR    TO WTC-YEAR
        //* * MOVE WTC-CREF-END-MONTH   TO WTC-MONTH
        //* * MOVE WTC-CREF-END-DAY     TO WTC-DAY
        ldaAial0270.getVw_cratemgr_M_View().startDatabaseFind                                                                                                             //Natural: FIND CRATEMGR-M-VIEW WITH WTC-RECORD-KEY = WTC-RECORD-KEYS
        (
        "FIND03",
        new Wc[] { new Wc("WTC_RECORD_KEY", "=", ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Record_Keys(), WcType.WITH) }
        );
        FIND03:
        while (condition(ldaAial0270.getVw_cratemgr_M_View().readNextRow("FIND03", true)))
        {
            ldaAial0270.getVw_cratemgr_M_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0270.getVw_cratemgr_M_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(5);                                                                                         //Natural: MOVE 5 TO #RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas().setValue(ldaAial0270.getCratemgr_M_View_Wtc_Tiaacref_Payout_Area());                    //Natural: MOVE WTC-TIAACREF-PAYOUT-AREA TO WTC-TIAACREF-PAYOUT-AREAS
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Da_Rate().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Current_Ia_Rate());                           //Natural: MOVE WTC-TIAA-CURRENT-IA-RATE TO WTC-DA-RATE
        pnd_Tiaa_Current_Ia_Rb.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Current_Ia_Rate());                                                            //Natural: MOVE WTC-TIAA-CURRENT-IA-RATE TO #TIAA-CURRENT-IA-RB
        ldaAial0270.getVw_cratemgr_M_View().startDatabaseFind                                                                                                             //Natural: FIND CRATEMGR-M-VIEW WITH WTC-RECORD-KEY = WTC-RECORD-KEYS
        (
        "FIND04",
        new Wc[] { new Wc("WTC_RECORD_KEY", "=", ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Record_Keys(), WcType.WITH) }
        );
        FIND04:
        while (condition(ldaAial0270.getVw_cratemgr_M_View().readNextRow("FIND04", true)))
        {
            ldaAial0270.getVw_cratemgr_M_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0270.getVw_cratemgr_M_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(5);                                                                                         //Natural: MOVE 5 TO #RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas().setValue(ldaAial0270.getCratemgr_M_View_Wtc_Tiaacref_Payout_Area());                    //Natural: MOVE WTC-TIAACREF-PAYOUT-AREA TO WTC-TIAACREF-PAYOUT-AREAS
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Guar_Std_Mort_Tbl_Code().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Code());                //Natural: MOVE WTC-SG-TIAA-MORT-TBL-CODE TO #T-GUAR-STD-MORT-TBL-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Guar_Std_Mort_Tbl_Setback().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Setback());          //Natural: MOVE WTC-SG-TIAA-MORT-TBL-SETBACK TO #T-GUAR-STD-MORT-TBL-SETBACK
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Guar_Std_Mort_Tbl_Interest().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Interest());        //Natural: MOVE WTC-SG-TIAA-MORT-TBL-INTEREST TO #T-GUAR-STD-MORT-TBL-INTEREST
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Guar_Grd_Mort_Tbl_Code().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Code());                //Natural: MOVE WTC-GG-TIAA-MORT-TBL-CODE TO #T-GUAR-GRD-MORT-TBL-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Guar_Grd_Mort_Tbl_Setback().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Setback());          //Natural: MOVE WTC-GG-TIAA-MORT-TBL-SETBACK TO #T-GUAR-GRD-MORT-TBL-SETBACK
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Guar_Grd_Mort_Tbl_Interest().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Interest());        //Natural: MOVE WTC-GG-TIAA-MORT-TBL-INTEREST TO #T-GUAR-GRD-MORT-TBL-INTEREST
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Std_Mort_Tbl_Code().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Code());                 //Natural: MOVE WTC-ST-TIAA-MORT-TBL-CODE TO #T-TOT-STD-MORT-TBL-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Std_Mort_Tbl_Setback().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Setback());           //Natural: MOVE WTC-ST-TIAA-MORT-TBL-SETBACK TO #T-TOT-STD-MORT-TBL-SETBACK
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(21) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().greater(2)))                      //Natural: IF #OPTION = 21 OR #PAYEE-CODE > 2
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Std_Mort_Tbl_Interest().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Ac_Tiaa_Interest());              //Natural: MOVE WTC-AC-TIAA-INTEREST TO #T-TOT-STD-MORT-TBL-INTEREST
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Std_Mort_Tbl_Interest().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Interest());     //Natural: MOVE WTC-ST-TIAA-MORT-TBL-INTEREST TO #T-TOT-STD-MORT-TBL-INTEREST
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Grd_Mort_Tbl_Code().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Code());                 //Natural: MOVE WTC-GT-TIAA-MORT-TBL-CODE TO #T-TOT-GRD-MORT-TBL-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Grd_Mort_Tbl_Setback().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Setback());           //Natural: MOVE WTC-GT-TIAA-MORT-TBL-SETBACK TO #T-TOT-GRD-MORT-TBL-SETBACK
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Grd_Mort_Tbl_Interest().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Interest());         //Natural: MOVE WTC-GT-TIAA-MORT-TBL-INTEREST TO #T-TOT-GRD-MORT-TBL-INTEREST
    }
    private void sub_Get_Auv_Date() throws Exception                                                                                                                      //Natural: GET-AUV-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  WRITE 'GET-AUV-DATE'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("L");                                                                                          //Natural: MOVE 'L' TO #AIAN026-CALL-TYPE
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code());                                        //Natural: MOVE #FUND-CODE TO #AIAN026-IA-FUND-CODE
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator());                      //Natural: MOVE #REVALUATION-INDICATOR TO #AIAN026-REVALUATION-METHOD
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date());                       //Natural: MOVE #ILLUSTRATION-EFF-DATE TO #AIAN026-IAUV-REQUEST-DATE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' USING #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            getReports().write(0, "AIAN026 RETURN CODE = ",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                                                  //Natural: WRITE 'AIAN026 RETURN CODE = ' #AIAN026-RETURN-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Auv_Date.setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Date_Returned());                                                                        //Natural: MOVE #AIAN026-IAUV-DATE-RETURNED TO #AUV-DATE
        //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
    }
    private void sub_Get_Latest_Revalued_Auv() throws Exception                                                                                                           //Natural: GET-LATEST-REVALUED-AUV
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        //*  WRITE 'GET-LATEST-REVALUED-AUV'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("F");                                                                                          //Natural: MOVE 'F' TO #AIAN026-CALL-TYPE
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(pnd_Fund_Code_Temp);                                                                        //Natural: MOVE #FUND-CODE-TEMP TO #AIAN026-IA-FUND-CODE
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator());                      //Natural: MOVE #REVALUATION-INDICATOR TO #AIAN026-REVALUATION-METHOD
        pnd_Ws_Date_Pnd_Temp_Date.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date());                                                               //Natural: MOVE #ILLUSTRATION-EFF-DATE TO #TEMP-DATE
        if (condition(pnd_Ws_Date_Pnd_Temp_Date.less(19980401)))                                                                                                          //Natural: IF #TEMP-DATE < 19980401
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Date_Pnd_Temp_Month.less(3) || (pnd_Ws_Date_Pnd_Temp_Month.equals(3) && pnd_Ws_Date_Pnd_Temp_Month.less(31))))                               //Natural: IF #TEMP-MONTH < 3 OR ( #TEMP-MONTH = 3 AND #TEMP-MONTH < 31 )
        {
            pnd_Ws_Date_Pnd_Temp_Year.nsubtract(1);                                                                                                                       //Natural: SUBTRACT 1 FROM #TEMP-YEAR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Date_Pnd_Temp_Month.setValue(3);                                                                                                                           //Natural: MOVE 3 TO #TEMP-MONTH
        pnd_Ws_Date_Pnd_Temp_Day.setValue(31);                                                                                                                            //Natural: MOVE 31 TO #TEMP-DAY
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(pnd_Ws_Date_Pnd_Temp_Date);                                                            //Natural: MOVE #TEMP-DATE TO #AIAN026-IAUV-REQUEST-DATE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' USING #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            getReports().write(0, "AIAN026 RETURN CODE = ",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                                                  //Natural: WRITE 'AIAN026 RETURN CODE = ' #AIAN026-RETURN-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
        pnd_Auv_Revaluation.setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());                                                            //Natural: MOVE #AIAN026-INTERIM-ANNUITY-UNIT-VA TO #AUV-REVALUATION
    }
    private void sub_Move_To_Ann_Fact_Linkage() throws Exception                                                                                                          //Natural: MOVE-TO-ANN-FACT-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //*  MOVE #ISSUE-YEAR     TO #AIAN021-IN-ISSUE-YEAR
        //*  MOVE #ISSUE-MONTH    TO #AIAN021-IN-ISSUE-MONTH
        //*  MOVE 1               TO #AIAN021-IN-ISSUE-DAY
        //*  MOVE #AUV-DATE TO #AIAN021-IN-ISSUE-DATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Issue_Date().setValue(pnd_Auv_Date_Plus_1);                                                                     //Natural: MOVE #AUV-DATE-PLUS-1 TO #AIAN021-IN-ISSUE-DATE
        //*  MOVE #AGE-AS-OF-DATE TO #AIAN021-IN-ISSUE-DATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Mode().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode());                                         //Natural: MOVE #MODE TO #AIAN021-IN-PAYMENT-MODE
        //* **********************************************************************
        //* **   MOVED THE FOLLOWING LINES OF CODE   12/31/97   G.F
        //* **********************************************************************
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Sex().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex());                                 //Natural: MOVE #FIRST-ANN-SEX TO #AIAN021-IN-1ST-ANN-SEX
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Birth_Date().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob());                          //Natural: MOVE #FIRST-ANN-DOB TO #AIAN021-IN-1ST-ANN-BIRTH-DATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_2nd_Ann_Sex().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex());                                //Natural: MOVE #SECOND-ANN-SEX TO #AIAN021-IN-2ND-ANN-SEX
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_2nd_Ann_Birth_Date().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob());                         //Natural: MOVE #SECOND-ANN-DOB TO #AIAN021-IN-2ND-ANN-BIRTH-DATE
        //* **********************************************************************
        short decideConditionsMet1267 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #OPTION;//Natural: VALUE 1, 5, 6, 9, 32, 50
        if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(1) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(5) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(6) 
            || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(9) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(32) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(50))))
        {
            decideConditionsMet1267++;
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("SL");                                                                            //Natural: MOVE 'SL' TO #AIAN021-IN-PAYMENT-OPTION
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().greater(2)))                                                                                //Natural: IF #PAYEE-CODE > 2
            {
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("AC");                                                                        //Natural: MOVE 'AC' TO #AIAN021-IN-PAYMENT-OPTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 3, 4, 11, 12, 14, 15, 16, 17, 51, 52, 53, 54, 55, 56, 57
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(3) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(4) || 
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(11) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(12) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(14) 
            || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(15) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(16) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(17) 
            || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(51) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(52) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(53) 
            || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(54) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(55) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(56) 
            || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(57))))
        {
            decideConditionsMet1267++;
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().equals(2)))                                                                                 //Natural: IF #PAYEE-CODE = 2
            {
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("SL");                                                                        //Natural: MOVE 'SL' TO #AIAN021-IN-PAYMENT-OPTION
                //* **********************************************************************
                //* **   ADDED THE FOLLOWING LINES OF CODE   12/31/97   G.F
                //* **********************************************************************
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod().greater(getZero())))                                                                 //Natural: IF #FIRST-ANN-DOD > 0
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Sex().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex());                    //Natural: MOVE #SECOND-ANN-SEX TO #AIAN021-IN-1ST-ANN-SEX
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Birth_Date().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob());             //Natural: MOVE #SECOND-ANN-DOB TO #AIAN021-IN-1ST-ANN-BIRTH-DATE
                }                                                                                                                                                         //Natural: END-IF
                //* **********************************************************************
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().greater(2)))                                                                            //Natural: IF #PAYEE-CODE > 2
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("AC");                                                                    //Natural: MOVE 'AC' TO #AIAN021-IN-PAYMENT-OPTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("LS");                                                                    //Natural: MOVE 'LS' TO #AIAN021-IN-PAYMENT-OPTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 7, 8, 10, 13
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(7) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(8) || 
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(10) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(13))))
        {
            decideConditionsMet1267++;
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().equals(2)))                                                                                 //Natural: IF #PAYEE-CODE = 2
            {
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("SL");                                                                        //Natural: MOVE 'SL' TO #AIAN021-IN-PAYMENT-OPTION
                //* **********************************************************************
                //* **   ADDED THE FOLLOWING LINES OF CODE   12/31/97   G.F
                //* **********************************************************************
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod().greater(getZero())))                                                                 //Natural: IF #FIRST-ANN-DOD > 0
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Sex().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex());                    //Natural: MOVE #SECOND-ANN-SEX TO #AIAN021-IN-1ST-ANN-SEX
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Birth_Date().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob());             //Natural: MOVE #SECOND-ANN-DOB TO #AIAN021-IN-1ST-ANN-BIRTH-DATE
                }                                                                                                                                                         //Natural: END-IF
                //* **********************************************************************
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().greater(2)))                                                                            //Natural: IF #PAYEE-CODE > 2
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("AC");                                                                    //Natural: MOVE 'AC' TO #AIAN021-IN-PAYMENT-OPTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("JT");                                                                    //Natural: MOVE 'JT' TO #AIAN021-IN-PAYMENT-OPTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(2))))
        {
            decideConditionsMet1267++;
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("IR");                                                                            //Natural: MOVE 'IR' TO #AIAN021-IN-PAYMENT-OPTION
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().greater(2)))                                                                                //Natural: IF #PAYEE-CODE > 2
            {
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("AC");                                                                        //Natural: MOVE 'AC' TO #AIAN021-IN-PAYMENT-OPTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 21
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(21))))
        {
            decideConditionsMet1267++;
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("AC");                                                                            //Natural: MOVE 'AC' TO #AIAN021-IN-PAYMENT-OPTION
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(2);                                                                                             //Natural: MOVE 2 TO #RETURN-CODE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  MOVE EDITED #NEXT-PYMNT-DTE (EM=YYYYMMDD) TO #TEMP-DATE-A8
        //*  REPEAT UNTIL #TEMP-YEAR-MONTH > #FINAL-PER-PAY-DATE
        //*   ADD 1 TO #NUM-PMTS
        //*   ADD #MONTHS-BETWEEN-PMTS TO #WS-MONTH
        //*   IF #WS-MONTH > 12
        //*     SUBTRACT 12 FROM #WS-MONTH
        //*     ADD 1 TO #WS-YEAR
        //*   END-IF
        //*  WRITE '#TEMP-DATE = ' #TEMP-DATE
        //*  END-REPEAT
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period().compute(new ComputeParameters(true, pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period()),  //Natural: COMPUTE ROUNDED #AIAN021-IN-CERTAIN-PERIOD = #NUM-PMTS / #MONTHS
            pnd_Num_Pmts.divide(pnd_Months));
        //*  MOVE #AGE-AS-OF-DATE TO #TEMP-DATE
        pnd_Ws_Date_Pnd_Temp_Date.setValue(pnd_Auv_Date);                                                                                                                 //Natural: MOVE #AUV-DATE TO #TEMP-DATE
        //*  MOVE 0 TO #NUM-PMTS
        //*  MOVE EDITED #NEXT-PYMNT-DTE (EM=YYYYMMDD) TO #NEXT-PYMNT-DTE-A8
        //*  PERFORM DETERMINE-DEFERRED-PERIOD
        short decideConditionsMet1335 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #OPTION;//Natural: VALUE 4, 11, 14, 16, 51
        if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(4) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(11) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(14) 
            || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(16) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(51))))
        {
            decideConditionsMet1335++;
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Pct_To_Second_Ann().setValue(100);                                                                          //Natural: MOVE 100 TO #AIAN021-IN-PCT-TO-SECOND-ANN
        }                                                                                                                                                                 //Natural: VALUE 3, 12, 15, 17, 52
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(3) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(12) || 
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(15) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(17) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(52))))
        {
            decideConditionsMet1335++;
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Pct_To_Second_Ann().setValue(50);                                                                           //Natural: MOVE 50 TO #AIAN021-IN-PCT-TO-SECOND-ANN
        }                                                                                                                                                                 //Natural: VALUE 7, 8, 10, 13
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(7) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(8) || 
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(10) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(13))))
        {
            decideConditionsMet1335++;
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Pct_To_Second_Ann().setValue(66);                                                                           //Natural: MOVE 66.66667 TO #AIAN021-IN-PCT-TO-SECOND-ANN
        }                                                                                                                                                                 //Natural: VALUE 53, 54, 55, 56, 57
        else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(53) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(54) || 
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(55) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(56) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(57))))
        {
            decideConditionsMet1335++;
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Pct_To_Second_Ann().setValue(75);                                                                           //Natural: MOVE 75 TO #AIAN021-IN-PCT-TO-SECOND-ANN
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Pct_To_Second_Ann().setValue(0);                                                                            //Natural: MOVE 0 TO #AIAN021-IN-PCT-TO-SECOND-ANN
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Due_Or_Immediate().setValue(0);                                                                                 //Natural: MOVE 0 TO #AIAN021-IN-DUE-OR-IMMEDIATE
    }
    private void sub_Determine_Deferred_Period() throws Exception                                                                                                         //Natural: DETERMINE-DEFERRED-PERIOD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        //*  WRITE 'DETERMINE-DEFERRED-PERIOD'
        pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N.setValue(pnd_Ws_Date_Pnd_Temp_Date);                                                                                 //Natural: MOVE #TEMP-DATE TO #AIAN030-BEGIN-DATE-N
        pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N.setValue(pnd_Next_Pymnt_Dte_A8_Pnd_Next_Pymnt_Dte_N8);                                                                 //Natural: MOVE #NEXT-PYMNT-DTE-N8 TO #AIAN030-END-DATE-N
        //*  WRITE '#AIAN030-BEGIN-DATE-N = ' #AIAN030-BEGIN-DATE-N
        //*  WRITE '#AIAN030-END-DATE-N   = ' #AIAN030-END-DATE-N
        DbsUtil.callnat(Aian030.class , getCurrentProcessState(), pnd_Aian030_Linkage);                                                                                   //Natural: CALLNAT 'AIAN030' USING #AIAN030-LINKAGE
        if (condition(Global.isEscape())) return;
        //*  ITE '#AIAN030-DAYS-BETWEEN-TWO-DAYS = ' #AIAN030-DAYS-BETWEEN-TWO-DAYS
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().setValue(pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days);                              //Natural: MOVE #AIAN030-DAYS-BETWEEN-TWO-DAYS TO #AIAN021-IN-DEFERRED-PERIOD
    }
    private void sub_Determine_Deferred_In_Fiscal() throws Exception                                                                                                      //Natural: DETERMINE-DEFERRED-IN-FISCAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        //*  WRITE 'DETERMINE-DEFERRED-IN-FISCAL'
        //*  MOVE #ILLUSTRATION-EFF-DATE TO #AIAN030-BEGIN-DATE-N
        pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N.setValue(pnd_Auv_Date_Plus_1);                                                                                       //Natural: MOVE #AUV-DATE-PLUS-1 TO #AIAN030-BEGIN-DATE-N #TEMP-DATE
        pnd_Ws_Date_Pnd_Temp_Date.setValue(pnd_Auv_Date_Plus_1);
        pnd_Ws_Date_Pnd_Temp_Month.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TEMP-MONTH
        pnd_Pmt_Switch.setValue(0);                                                                                                                                       //Natural: MOVE 0 TO #PMT-SWITCH
        //*  REPEAT UNTIL #PMT-SWITCH = 1
        //*    DECIDE ON FIRST VALUE OF #MODE-1
        //*     VALUE 6
        //*       IF #TEMP-MONTH = #PMT-MONTH-1 OR = #PMT-MONTH-2 OR = #PMT-MONTH-3
        //*         OR = #PMT-MONTH-4
        //*         MOVE 1 TO #PMT-SWITCH
        //*       END-IF
        //*     VALUE 7
        //*       IF #TEMP-MONTH = #PMT-MONTH-1 OR = #PMT-MONTH-2
        //*         MOVE 1 TO #PMT-SWITCH
        //*       END-IF
        //*     VALUE 8
        //*       IF #TEMP-MONTH = #PMT-MONTH-1
        //*         MOVE 1 TO #PMT-SWITCH
        //*       END-IF
        //*     NONE
        //*       MOVE 1 TO #PMT-SWITCH
        //*   END-DECIDE
        //*   ADD 1 TO #TEMP-MONTH
        //*   IF #TEMP-MONTH > 12
        //*     MOVE 1 TO #TEMP-MONTH
        //*   END-IF
        //*  END-REPEAT
        //*  SUBTRACT 1 FROM #TEMP-MONTH
        //*  IF #TEMP-MONTH = 0
        //*   MOVE 12 TO #TEMP-MONTH
        //*  END-IF
        //*  MOVE #DAYS-IN-MONTH (#TEMP-MONTH) TO #TEMP-DAY
        //*  MOVE #TEMP-DATE TO #AIAN030-END-DATE-N
        pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N.setValue(pnd_Next_Pymnt_Dte_A8_Pnd_Next_Pymnt_Dte_N8);                                                                 //Natural: MOVE #NEXT-PYMNT-DTE-N8 TO #AIAN030-END-DATE-N
        //*  WRITE '#AIAN030-BEGIN-DATE-N = ' #AIAN030-BEGIN-DATE-N
        //*  WRITE '#AIAN030-END-DATE-N   = ' #AIAN030-END-DATE-N
        DbsUtil.callnat(Aian030.class , getCurrentProcessState(), pnd_Aian030_Linkage);                                                                                   //Natural: CALLNAT 'AIAN030' USING #AIAN030-LINKAGE
        if (condition(Global.isEscape())) return;
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().setValue(pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days);                              //Natural: MOVE #AIAN030-DAYS-BETWEEN-TWO-DAYS TO #AIAN021-IN-DEFERRED-PERIOD
        //*  WRITE '#AIAN021-IN-DEFERRED-PERIOD = ' #AIAN021-IN-DEFERRED-PERIOD
    }
    private void sub_Determine_Deferred_After_Fiscal() throws Exception                                                                                                   //Natural: DETERMINE-DEFERRED-AFTER-FISCAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        //*  WRITE 'DETERMINE-DEFERRED-AFTER-FISCAL'
        //*  MOVE #ILLUSTRATION-EFF-DATE TO #AIAN030-BEGIN-DATE-N
        pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N.setValue(pnd_Auv_Date_Plus_1);                                                                                       //Natural: MOVE #AUV-DATE-PLUS-1 TO #AIAN030-BEGIN-DATE-N
        pnd_Ws_Date_Pnd_Temp_Date.setValue(pnd_Next_Pymnt_Dte_A8_Pnd_Next_Pymnt_Dte_N8);                                                                                  //Natural: MOVE #NEXT-PYMNT-DTE-N8 TO #TEMP-DATE
        //*  WRITE '#AIAN030-BEGIN-DATE-N = ' #AIAN030-BEGIN-DATE-N
        //*  WRITE 'TEMP-DATE = ' #TEMP-DATE
        if (condition(pnd_Age_As_Of_Date.greater(19980331)))                                                                                                              //Natural: IF #AGE-AS-OF-DATE > 19980331
        {
            if (condition(pnd_Ws_Date_Pnd_Temp_Month.greater(4)))                                                                                                         //Natural: IF #TEMP-MONTH > 4
            {
                pnd_Ws_Date_Pnd_Temp_Year.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TEMP-YEAR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Date_Pnd_Temp_Month.setValue(5);                                                                                                                       //Natural: MOVE 5 TO #TEMP-MONTH
            pnd_Pmt_Switch.setValue(0);                                                                                                                                   //Natural: MOVE 0 TO #PMT-SWITCH
            REPEAT02:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Pmt_Switch.equals(1))) {break;}                                                                                                         //Natural: UNTIL #PMT-SWITCH = 1
                short decideConditionsMet1419 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #MODE-1;//Natural: VALUE 6
                if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode_1().equals(6))))
                {
                    decideConditionsMet1419++;
                    if (condition(pnd_Ws_Date_Pnd_Temp_Month.equals(pnd_Pmt_Month_1) || pnd_Ws_Date_Pnd_Temp_Month.equals(pnd_Pmt_Month_2) || pnd_Ws_Date_Pnd_Temp_Month.equals(pnd_Pmt_Month_3)  //Natural: IF #TEMP-MONTH = #PMT-MONTH-1 OR = #PMT-MONTH-2 OR = #PMT-MONTH-3 OR = #PMT-MONTH-4
                        || pnd_Ws_Date_Pnd_Temp_Month.equals(pnd_Pmt_Month_4)))
                    {
                        pnd_Pmt_Switch.setValue(1);                                                                                                                       //Natural: MOVE 1 TO #PMT-SWITCH
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 7
                else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode_1().equals(7))))
                {
                    decideConditionsMet1419++;
                    if (condition(pnd_Ws_Date_Pnd_Temp_Month.equals(pnd_Pmt_Month_1) || pnd_Ws_Date_Pnd_Temp_Month.equals(pnd_Pmt_Month_2)))                              //Natural: IF #TEMP-MONTH = #PMT-MONTH-1 OR = #PMT-MONTH-2
                    {
                        pnd_Pmt_Switch.setValue(1);                                                                                                                       //Natural: MOVE 1 TO #PMT-SWITCH
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 8
                else if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode_1().equals(8))))
                {
                    decideConditionsMet1419++;
                    if (condition(pnd_Ws_Date_Pnd_Temp_Month.equals(pnd_Pmt_Month_1)))                                                                                    //Natural: IF #TEMP-MONTH = #PMT-MONTH-1
                    {
                        pnd_Pmt_Switch.setValue(1);                                                                                                                       //Natural: MOVE 1 TO #PMT-SWITCH
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Pmt_Switch.setValue(1);                                                                                                                           //Natural: MOVE 1 TO #PMT-SWITCH
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Pmt_Switch.equals(getZero())))                                                                                                          //Natural: IF #PMT-SWITCH = 0
                {
                    pnd_Ws_Date_Pnd_Temp_Month.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TEMP-MONTH
                    if (condition(pnd_Ws_Date_Pnd_Temp_Month.greater(12)))                                                                                                //Natural: IF #TEMP-MONTH > 12
                    {
                        pnd_Ws_Date_Pnd_Temp_Month.setValue(1);                                                                                                           //Natural: MOVE 1 TO #TEMP-MONTH
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUBTRACT 1 FROM #TEMP-MONTH
        //*  MOVE #DAYS-IN-MONTH (#TEMP-MONTH) TO #TEMP-DAY
        pnd_Ws_Date_Pnd_Temp_Day.setValue(1);                                                                                                                             //Natural: MOVE 1 TO #TEMP-DAY
        pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N.setValue(pnd_Ws_Date_Pnd_Temp_Date);                                                                                   //Natural: MOVE #TEMP-DATE TO #AIAN030-END-DATE-N
        //*  WRITE '#AIAN030-BEGIN-DATE-N = ' #AIAN030-BEGIN-DATE-N
        //*  WRITE '#AIAN030-END-DATE-N   = ' #AIAN030-END-DATE-N
        DbsUtil.callnat(Aian030.class , getCurrentProcessState(), pnd_Aian030_Linkage);                                                                                   //Natural: CALLNAT 'AIAN030' USING #AIAN030-LINKAGE
        if (condition(Global.isEscape())) return;
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().setValue(pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days);                              //Natural: MOVE #AIAN030-DAYS-BETWEEN-TWO-DAYS TO #AIAN021-IN-DEFERRED-PERIOD
        //*  WRITE '#AIAN021-IN-DEFERRED-PERIOD = ' #AIAN021-IN-DEFERRED-PERIOD
        //* ************
        //*  SUBTRACT 1 FROM #AIAN021-IN-DEFERRED-PERIOD
        //* ************
        //*  WRITE '#AIAN021-IN-DEFERRED-PERIOD = ' #AIAN021-IN-DEFERRED-PERIOD
    }
    private void sub_Calculate_Fiscal_Factors() throws Exception                                                                                                          //Natural: CALCULATE-FISCAL-FACTORS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //*  WRITE 'CALCULATE-FISCAL-FACTORS'
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DEFERRED-IN-FISCAL
        sub_Determine_Deferred_In_Fiscal();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Date_Pnd_Temp_Date.setValue(pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N);                                                                                   //Natural: MOVE #AIAN030-END-DATE-N TO #TEMP-DATE
        pnd_Num_Pmts.setValue(0);                                                                                                                                         //Natural: MOVE 0 TO #NUM-PMTS
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CERTAIN-PERIOD
        sub_Determine_Certain_Period();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-ANN-FACT-LINKAGE
        sub_Move_To_Ann_Fact_Linkage();
        if (condition(Global.isEscape())) {return;}
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(pnd_Temp_In_Mortality_Table);                                                        //Natural: MOVE #TEMP-IN-MORTALITY-TABLE TO #AIAN021-IN-MORTALITY-TABLE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(pnd_Temp_In_Setback);                                                                        //Natural: MOVE #TEMP-IN-SETBACK TO #AIAN021-IN-SETBACK
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(pnd_Temp_In_Interest_Rate);                                                            //Natural: MOVE #TEMP-IN-INTEREST-RATE TO #AIAN021-IN-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pnd_Temp_In_Def_Interest_Rate);                                                    //Natural: MOVE #TEMP-IN-DEF-INTEREST-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
        DbsUtil.callnat(Aian021.class , getCurrentProcessState(), pdaAial0210.getPnd_Aian021_Linkage());                                                                  //Natural: CALLNAT 'AIAN021' USING #AIAN021-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code_Nbr().notEquals(getZero())))                                                         //Natural: IF #AIAN021-OUT-RETURN-CODE-NBR <> 0
        {
            //*  WRITE '*** AFTER FIRST CALL TO AIAN021 #RETURN-CODE = ' #RETURN-CODE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code());                              //Natural: MOVE #AIAN021-OUT-RETURN-CODE TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#AIAN021-LINKAGE = ' #AIAN021-LINKAGE
        pnd_Annuity_Factor_1.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());                                                               //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #ANNUITY-FACTOR-1
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DEFERRED-AFTER-FISCAL
        sub_Determine_Deferred_After_Fiscal();
        if (condition(Global.isEscape())) {return;}
        pnd_Num_Pmts.setValue(0);                                                                                                                                         //Natural: MOVE 0 TO #NUM-PMTS
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CERTAIN-PERIOD
        sub_Determine_Certain_Period();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-ANN-FACT-LINKAGE
        sub_Move_To_Ann_Fact_Linkage();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Aian021.class , getCurrentProcessState(), pdaAial0210.getPnd_Aian021_Linkage());                                                                  //Natural: CALLNAT 'AIAN021' USING #AIAN021-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code_Nbr().notEquals(getZero())))                                                         //Natural: IF #AIAN021-OUT-RETURN-CODE-NBR <> 0
        {
            //*  WRITE '*** AFTER SECOND CALL TO AIAN021 #RETURN-CODE = ' #RETURN-CODE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code());                              //Natural: MOVE #AIAN021-OUT-RETURN-CODE TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#AIAN021-LINKAGE = ' #AIAN021-LINKAGE
        pnd_Annuity_Factor_2.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());                                                               //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #ANNUITY-FACTOR-2
        pnd_Annuity_Factor_To_Fiscal.compute(new ComputeParameters(true, pnd_Annuity_Factor_To_Fiscal), pnd_Annuity_Factor_1.subtract(pnd_Annuity_Factor_2));             //Natural: COMPUTE ROUNDED #ANNUITY-FACTOR-TO-FISCAL = #ANNUITY-FACTOR-1 - #ANNUITY-FACTOR-2
        //*  WRITE '#ANNUITY-FACTOR-1         = ' #ANNUITY-FACTOR-1
        //*  WRITE '#ANNUITY-FACTOR-2         = ' #ANNUITY-FACTOR-2
        //*  WRITE '#ANNUITY-FACTOR-TO-FISCAL = ' #ANNUITY-FACTOR-TO-FISCAL
        //*   WRITE '#AUV-OLD-1                = ' #AUV-OLD-1
    }
    private void sub_From_Cref_To_Cref() throws Exception                                                                                                                 //Natural: FROM-CREF-TO-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  WRITE 'FROM-CREF-TO-CREF'
        pnd_Cref_Sub.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CREF-SUB
        //*  IF #TYPE-OF-RUN = 'A'
        //*   MOVE #FUND-TO-RECEIVE (#I) TO #FUND-CODE-TEMP
        //*   PERFORM GET-LATEST-REVALUED-AUV
        //*  END-IF
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I));                  //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO #AIAN026-IA-FUND-CODE
        //*  WRITE '#TRANSFER-REVAL-SWITCH = ' #TRANSFER-REVAL-SWITCH
        //*  WRITE '#FUND-CODE             = ' #FUND-CODE
        //*  WRITE '#FUND-TO-RECEIVE (' #I ') = ' #FUND-TO-RECEIVE (#I)
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().notEquals("0") && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().notEquals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I)))) //Natural: IF #TRANSFER-REVAL-SWITCH <> '0' AND #FUND-CODE <> #FUND-TO-RECEIVE ( #I )
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(12);                                                                                            //Natural: MOVE 12 TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE #ILLUSTRATION-EFF-DATE TO #AIAN026-IAUV-REQUEST-DATE
        //*  DECIDE ON FIRST VALUE OF #TRANSFER-REVAL-SWITCH
        //*   VALUE '0'
        //*     MOVE #REVALUATION-INDICATOR TO #AIAN026-REVALUATION-METHOD
        //*   VALUE '1'
        //*     MOVE 'A' TO #AIAN026-REVALUATION-METHOD
        //*   VALUE '2'
        //*     MOVE 'M' TO #AIAN026-REVALUATION-METHOD
        //*   NONE
        //*     MOVE 4 TO #RETURN-CODE
        //*     ESCAPE ROUTINE
        //*  END-DECIDE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' USING #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            getReports().write(0, "AIAN026 RETURN CODE = ",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                                                  //Natural: WRITE 'AIAN026 RETURN CODE = ' #AIAN026-RETURN-CODE
            if (Global.isEscape()) return;
            //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(3);                                                                                             //Natural: MOVE 3 TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());    //Natural: MOVE #AIAN026-INTERIM-ANNUITY-UNIT-VA TO #AUV-OUT ( #CREF-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_Cref_Sub).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I)); //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO #FUND-CODE-OUT ( #CREF-SUB )
        //*   WRITE '#FUND-CODE-OUT (' #CREF-SUB ') = ' #FUND-CODE-OUT (#CREF-SUB)
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #UNITS-OUT ( #CREF-SUB ) = #UNITS-TO-RECEIVE ( #I ) * #AUV-OLD-1 / #AUV-OUT ( #CREF-SUB )
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).multiply(pnd_Auv_Old_1).divide(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub)));
        pnd_Temp_In_Mortality_Table.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Code());                                                               //Natural: MOVE #FUND-FROM-MORT-CODE TO #TEMP-IN-MORTALITY-TABLE
        pnd_Temp_In_Setback.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Setback());                                                                    //Natural: MOVE #FUND-FROM-MORT-SETBACK TO #TEMP-IN-SETBACK
        pnd_Temp_In_Interest_Rate.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_From_Mort_Interest());                                                             //Natural: MOVE #FUND-FROM-MORT-INTEREST TO #TEMP-IN-INTEREST-RATE
        pnd_Temp_In_Def_Interest_Rate.setValue(0);                                                                                                                        //Natural: MOVE .04 TO #TEMP-IN-DEF-INTEREST-RATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-FISCAL-FACTORS
        sub_Calculate_Fiscal_Factors();
        if (condition(Global.isEscape())) {return;}
        pnd_Pmt_New.compute(new ComputeParameters(true, pnd_Pmt_New), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).multiply(pnd_Auv_Old_1)); //Natural: COMPUTE ROUNDED #PMT-NEW = #UNITS-TO-RECEIVE ( #I ) * #AUV-OLD-1
        //*   WRITE '#UNITS-TO-RECEIVE (' #I ') = ' #UNITS-TO-RECEIVE (#I)
        //*   WRITE '#AUV-OLD-1 = ' #AUV-OLD-1 ' #PMT-NEW = ' #PMT-NEW
        //* *************************************************************
        //*  ADDED FOR AIAP040
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).compute(new ComputeParameters(false, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE #PMTS-TO-RECEIVE ( #I ) = #UNITS-TO-RECEIVE ( #I ) * #AUV-OLD-1
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).multiply(pnd_Auv_Old_1));
        //* *************************************************************
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                        //Natural: IF #REVALUATION-INDICATOR = 'M'
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = #PMT-NEW * #ANNUITY-FACTOR-1
                pnd_Pmt_New.multiply(pnd_Annuity_Factor_1));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = ( #PMTS-TO-RECEIVE ( #I ) * #ANNUITY-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #ANNUITY-FACTOR-2 )
                (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pnd_Annuity_Factor_To_Fiscal)).add((pnd_Pmt_New.multiply(pnd_Annuity_Factor_2))));
            //*    WRITE '#PMTS-TO-RECEIVE (' #I ') = ' #PMTS-TO-RECEIVE (#I)
            //*    WRITE '#PMT-NEW                 = ' #PMT-NEW
            //*    WRITE '#ANNUITY-FACTOR-TO-FISCAL = ' #ANNUITY-FACTOR-TO-FISCAL
            //*    WRITE '#ANNUITY-FACTOR-2         = ' #ANNUITY-FACTOR-2
            //*    WRITE '#TRANSFER-AMT-OUT-CREF (' #CREF-SUB ') = '
            //*           #TRANSFER-AMT-OUT-CREF (#CREF-SUB)
        }                                                                                                                                                                 //Natural: END-IF
        //*   WRITE '#PMTS-TO-RECEIVE (' #I ') = ' #PMTS-TO-RECEIVE (#I)
        //*   WRITE '#ANNUITY-FACTOR-TO-FISCAL = ' #ANNUITY-FACTOR-TO-FISCAL
        //*  WRITE '#PMT-NEW = ' #PMT-NEW ' #ANNUITY-FACTOR-2 = ' #ANNUITY-FACTOR-2
        //*  WRITE '#TRANSFER-AMT-OUT-CREF (' #CREF-SUB ') = '
        //*  #TRANSFER-AMT-OUT-CREF (#CREF-SUB)
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub));      //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) TO #TOTAL-TRANSFER-AMT-OUT
        //*   WRITE '#UNITS-TO-RECEIVE (' #I ') = ' #UNITS-TO-RECEIVE (#I)
        //*   WRITE '#AUV-OLD-1 = ' #AUV-OLD-1
        //*   WRITE '#AUV-OUT (' #CREF-SUB ') = ' #AUV-OUT (#CREF-SUB)
        //*   WRITE '#UNITS-OUT (' #CREF-SUB ') = ' #UNITS-OUT (#CREF-SUB)
        //*   WRITE '#PMT-OLD = ' #PMT-OLD
        //*   WRITE 'T-CREF-ANN-FACTOR = ' T-CREF-ANN-FACTOR
        //*   WRITE '#TRANSFER-AMT-OUT-CREF (' #CREF-SUB ') = '
        //*   #TRANSFER-AMT-OUT-CREF (#CREF-SUB)
    }
    private void sub_To_Rea() throws Exception                                                                                                                            //Natural: TO-REA
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  WRITE 'TO-REA'
        pnd_Cref_Sub.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CREF-SUB
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().notEquals("0") && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().notEquals("R"))) //Natural: IF #TRANSFER-REVAL-SWITCH <> '0' AND #FUND-CODE <> 'R'
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(12);                                                                                            //Natural: MOVE 12 TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I));                  //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO #AIAN026-IA-FUND-CODE
        //*   DECIDE ON FIRST VALUE OF #TRANSFER-REVAL-SWITCH
        //*     VALUE '0'
        //*       MOVE #REVALUATION-INDICATOR TO #AIAN026-REVALUATION-METHOD
        //*     VALUE '1'
        //*       MOVE 'A' TO #AIAN026-REVALUATION-METHOD
        //*     VALUE '2'
        //*       MOVE 'M' TO #AIAN026-REVALUATION-METHOD
        //*     NONE
        //*       MOVE 4 TO #RETURN-CODE
        //*       ESCAPE ROUTINE
        //*   END-DECIDE
        //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' USING #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            getReports().write(0, "AIAN026 RETURN CODE = ",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                                                  //Natural: WRITE 'AIAN026 RETURN CODE = ' #AIAN026-RETURN-CODE
            if (Global.isEscape()) return;
            //*    WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(3);                                                                                             //Natural: MOVE 3 TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());    //Natural: MOVE #AIAN026-INTERIM-ANNUITY-UNIT-VA TO #AUV-OUT ( #CREF-SUB ) #AIAN026-INTERIM-ANNUITY-UNIT-VA
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va().setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
        //*  MOVE #FUND-TO-RECEIVE (#I) TO #FUND-CODE-OUT (#CREF-SUB)
        pnd_Pmt_Old.compute(new ComputeParameters(true, pnd_Pmt_Old), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).multiply(pnd_Auv_Old_1)); //Natural: COMPUTE ROUNDED #PMT-OLD = #UNITS-TO-RECEIVE ( #I ) * #AUV-OLD-1
        ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I));                //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO WTC-FUND-INDICATOR #TEMP-FUND-CODE #FUND-CODE-OUT ( #CREF-SUB )
        pnd_Temp_Fund_Code.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I));
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_Cref_Sub).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I));
                                                                                                                                                                          //Natural: PERFORM GET-FACTOR-DATE
        sub_Get_Factor_Date();
        if (condition(Global.isEscape())) {return;}
        //* *****************************************************************
        //* ***   ADDED THE FOLLOWING CODE ON 12/30/97    GLENN FRIEDMAN
        //* *****************************************************************
        //*  WRITE 'GET-CREF-MORTALITY'
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Century.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                                        //Natural: MOVE WTC-CREF-END-CENTURY TO #COMPARE-RATE-CENTURY
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Year.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                              //Natural: MOVE WTC-CREF-END-YEAR TO #COMPARE-RATE-YEAR
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Month.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                                            //Natural: MOVE WTC-CREF-END-MONTH TO #COMPARE-RATE-MONTH
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Year_Plus_1);                                                                //Natural: MOVE #IARTMG-YEAR-PLUS-1 TO #COMPARE-AUV-YEAR
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Month_Plus_1);                                                              //Natural: MOVE #IARTMG-MONTH-PLUS-1 TO #COMPARE-AUV-MONTH
        //* *****
        if (condition(pnd_Compare_Auv_Date.less(pnd_Compare_Rate_Date)))                                                                                                  //Natural: IF #COMPARE-AUV-DATE < #COMPARE-RATE-DATE
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Century);                                               //Natural: MOVE #COMPARE-AUV-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year_2);                                                   //Natural: MOVE #COMPARE-AUV-YEAR-2 TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month);                                                   //Natural: MOVE #COMPARE-AUV-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                           //Natural: MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                 //Natural: MOVE WTC-CREF-END-YEAR TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                               //Natural: MOVE WTC-CREF-END-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************************************************************
        //* ***   COMMENTED OUT THE FOLLOWING FOUR LINES OF CODE  G.F  12/30/97
        //* *****************************************************************
        //* * MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
        //* * MOVE WTC-CREF-END-YEAR    TO WTC-YEAR
        //* * MOVE WTC-CREF-END-MONTH   TO WTC-MONTH
        //* * MOVE WTC-CREF-END-DAY     TO WTC-DAY
        //*  WRITE 'WTC-FUND-INDICATOR = ' WTC-FUND-INDICATOR
        //*  WRITE 'WTC-DATE           = ' WTC-DATE
        ldaAial0270.getVw_cratemgr_M_View().startDatabaseFind                                                                                                             //Natural: FIND CRATEMGR-M-VIEW WITH WTC-RECORD-KEY = WTC-RECORD-KEYS
        (
        "FIND05",
        new Wc[] { new Wc("WTC_RECORD_KEY", "=", ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Record_Keys(), WcType.WITH) }
        );
        FIND05:
        while (condition(ldaAial0270.getVw_cratemgr_M_View().readNextRow("FIND05", true)))
        {
            ldaAial0270.getVw_cratemgr_M_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0270.getVw_cratemgr_M_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(5);                                                                                         //Natural: MOVE 5 TO #RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas().setValue(ldaAial0270.getCratemgr_M_View_Wtc_Tiaacref_Payout_Area());                    //Natural: MOVE WTC-TIAACREF-PAYOUT-AREA TO WTC-TIAACREF-PAYOUT-AREAS
            pnd_Temp_In_Mortality_Table.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code());                                                   //Natural: MOVE WTC-CREF-MORT-TABLE-CODE TO #TEMP-IN-MORTALITY-TABLE
            pnd_Temp_In_Setback.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback());                                                        //Natural: MOVE WTC-CREF-MORT-TABLE-SETBACK TO #TEMP-IN-SETBACK
            pnd_Temp_In_Interest_Rate.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest());                                                 //Natural: MOVE WTC-CREF-MORT-TABLE-INTEREST TO #TEMP-IN-INTEREST-RATE
            pnd_Temp_In_Def_Interest_Rate.setValue(0);                                                                                                                    //Natural: MOVE .04 TO #TEMP-IN-DEF-INTEREST-RATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-FISCAL-FACTORS
            sub_Calculate_Fiscal_Factors();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MOVE WTC-CREF-MORT-TABLE-CODE TO #AIAN021-IN-MORTALITY-TABLE
            //*  MOVE WTC-CREF-MORT-TABLE-SETBACK TO #AIAN021-IN-SETBACK
            //*  MOVE WTC-CREF-MORT-TABLE-INTEREST TO #AIAN021-IN-INTEREST-RATE
            //*  MOVE .04 TO #AIAN021-IN-DEF-INTEREST-RATE
            //*  CALLNAT 'AIAN021' USING #AIAN021-LINKAGE
            //*  IF #AIAN021-OUT-RETURN-CODE �= 0
            //*    MOVE 4 TO #RETURN-CODE
            //*    ESCAPE ROUTINE
            //*  ELSE
            //*    MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #T-REA-ANN-FACTOR
            //*      WRITE '#T-REA-ANN-FACTOR = ' #T-REA-ANN-FACTOR
            //*  END-IF
            //*  WRITE '#AIAN021-LINKAGE = ' #AIAN021-LINKAGE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  COMPUTE ROUNDED #PMT-OUT =
        //*    #PMT-OLD * #T-CREF-ANN-FACTOR / #T-REA-ANN-FACTOR
        //*  COMPUTE ROUNDED #UNITS-OUT (#CREF-SUB) =
        //*    #PMT-OUT / #AUV-OUT (#CREF-SUB)
        //*  COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF (#CREF-SUB) =
        //*    #PMT-OLD * #T-CREF-ANN-FACTOR
        pnd_Pmt_New.compute(new ComputeParameters(true, pnd_Pmt_New), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).multiply(pnd_Auv_Old_1)); //Natural: COMPUTE ROUNDED #PMT-NEW = #UNITS-TO-RECEIVE ( #I ) * #AUV-OLD-1
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("R")))                                                                                    //Natural: IF #FUND-CODE = 'R'
        {
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                    //Natural: IF #REVALUATION-INDICATOR = 'M'
            {
                //*    WRITE 'INC746216'  #AUV-OUT (#CREF-SUB) #ANNUITY-FACTOR-1
                if (condition(pnd_Annuity_Factor_1.notEquals(getZero())))                                                                                                 //Natural: IF #ANNUITY-FACTOR-1 NOT = 0
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #UNITS-OUT ( #CREF-SUB ) = ( #PMT-NEW * #T-REA-ANN-FACTOR-1 ) / ( #AUV-OUT ( #CREF-SUB ) * #ANNUITY-FACTOR-1 )
                        (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1())).divide((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).multiply(pnd_Annuity_Factor_1))));
                }                                                                                                                                                         //Natural: END-IF
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = #PMT-NEW * #T-REA-ANN-FACTOR-1
                    pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    WRITE 'AUV-OUT' #AUV-OUT (#CREF-SUB) #ANNUITY-FACTOR-2
                if (condition(pnd_Annuity_Factor_2.notEquals(getZero())))                                                                                                 //Natural: IF #ANNUITY-FACTOR-2 NOT = 0
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #UNITS-OUT ( #CREF-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 ) - ( #PMTS-TO-RECEIVE ( #I ) * #ANNUITY-FACTOR-TO-FISCAL ) ) / ( #AUV-OUT ( #CREF-SUB ) * #ANNUITY-FACTOR-2 )
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2()))).subtract((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pnd_Annuity_Factor_To_Fiscal)))).divide((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).multiply(pnd_Annuity_Factor_2))));
                }                                                                                                                                                         //Natural: END-IF
                //*  MOVE #AUV-OLD-1 TO #AUV-OUT (#CREF-SUB)
                //*  WRITE '#UNITS-OUT (' #CREF-SUB ') = ' #UNITS-OUT (#CREF-SUB)
                //*  WRITE '#AUV-OUT   (' #CREF-SUB ') = ' #AUV-OUT   (#CREF-SUB)
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = ( #PMT-NEW * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 )
                    (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2()))));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                    //Natural: IF #REVALUATION-INDICATOR = 'M'
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #UNITS-OUT ( #CREF-SUB ) = ( #PMT-NEW * #T-CREF-ANN-FACTOR-1 ) / ( #AUV-OUT ( #CREF-SUB ) * #ANNUITY-FACTOR-1 )
                    (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1())).divide((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).multiply(pnd_Annuity_Factor_1))));
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = #PMT-NEW * #T-CREF-ANN-FACTOR-1
                    pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  IF #ANNUITY-FACTOR-2 NOT = 0
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #UNITS-OUT ( #CREF-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-CREF-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-CREF-ANN-FACTOR-2 ) - ( #PMTS-TO-RECEIVE ( #I ) * #ANNUITY-FACTOR-TO-FISCAL ) ) / ( #AUV-OUT ( #CREF-SUB ) * #ANNUITY-FACTOR-2 )
                    ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2()))).subtract((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pnd_Annuity_Factor_To_Fiscal)))).divide((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).multiply(pnd_Annuity_Factor_2))));
                //*  END-IF
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = ( #PMT-NEW * #T-CREF-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-CREF-ANN-FACTOR-2 )
                    (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2()))));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub));      //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) TO #TOTAL-TRANSFER-AMT-OUT
        //*  WRITE '#UNITS-TO-RECEIVE (' #I ') = ' #UNITS-TO-RECEIVE (#I)
        //*  WRITE '#AUV-OLD-1 = ' #AUV-OLD-1
        //*  WRITE '#T-CREF-ANN-FACTOR-TO-FISCAL = ' #T-CREF-ANN-FACTOR-TO-FISCAL
        //*  WRITE '#T-CREF-ANN-FACTOR-2         = ' #T-CREF-ANN-FACTOR-2
        //*  WRITE '#TRANSFER-AMT-OUT-CREF (' #CREF-SUB ') = '
        //*  WRITE #TRANSFER-AMT-OUT-CREF (#CREF-SUB)
    }
    private void sub_From_Rea_To_Cref() throws Exception                                                                                                                  //Natural: FROM-REA-TO-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  WRITE 'FROM-REA-TO-CREF'
        pnd_Cref_Sub.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CREF-SUB
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().notEquals("0") && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().notEquals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I)))) //Natural: IF #TRANSFER-REVAL-SWITCH <> '0' AND #FUND-CODE <> #FUND-TO-RECEIVE ( #I )
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(12);                                                                                            //Natural: MOVE 12 TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I));                  //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO #AIAN026-IA-FUND-CODE
        //*  DECIDE ON FIRST VALUE OF #TRANSFER-REVAL-SWITCH
        //*   VALUE '0'
        //*     MOVE #REVALUATION-INDICATOR TO #AIAN026-REVALUATION-METHOD
        //*   VALUE '1'
        //*     MOVE 'A' TO #AIAN026-REVALUATION-METHOD
        //*   VALUE '2'
        //*     MOVE 'M' TO #AIAN026-REVALUATION-METHOD
        //*   NONE
        //*     MOVE 4 TO #RETURN-CODE
        //*     ESCAPE ROUTINE
        //*  END-DECIDE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' USING #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            getReports().write(0, "AIAN026 RETURN CODE = ",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                                                  //Natural: WRITE 'AIAN026 RETURN CODE = ' #AIAN026-RETURN-CODE
            if (Global.isEscape()) return;
            //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(3);                                                                                             //Natural: MOVE 3 TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());    //Natural: MOVE #AIAN026-INTERIM-ANNUITY-UNIT-VA TO #AUV-OUT ( #CREF-SUB )
            //*   WRITE '#AIAN026-INTERIM-ANNUITY-UNIT-VALUE = '
            //*     #AIAN026-INTERIM-ANNUITY-UNIT-VALUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#AIAN026-LINKAGE = ' #AIAN026-LINKAGE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_Cref_Sub).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I)); //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO #FUND-CODE-OUT ( #CREF-SUB )
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("I")))                                                              //Natural: IF #FUND-TO-RECEIVE ( #I ) = 'I'
        {
            pnd_Temp_Fund_Code.setValue("F");                                                                                                                             //Natural: MOVE 'F' TO #TEMP-FUND-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Temp_Fund_Code.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I));                                                        //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO #TEMP-FUND-CODE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-FACTOR-DATE
        sub_Get_Factor_Date();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().equals("A") && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().equals(19980331))) //Natural: IF #TYPE-OF-RUN = 'A' AND #TRANSFER-EFFECTIVE-DATE = 19980331
        {
            pnd_Compare_Auv_Date.setValue(199804);                                                                                                                        //Natural: MOVE 199804 TO #COMPARE-AUV-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************************************************************
        //* ***   ADDED THE FOLLOWING CODE ON 12/30/97    GLENN FRIEDMAN
        //* *****************************************************************
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Century.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                                        //Natural: MOVE WTC-CREF-END-CENTURY TO #COMPARE-RATE-CENTURY
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Year.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                              //Natural: MOVE WTC-CREF-END-YEAR TO #COMPARE-RATE-YEAR
        pnd_Compare_Rate_Date_Pnd_Compare_Rate_Month.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                                            //Natural: MOVE WTC-CREF-END-MONTH TO #COMPARE-RATE-MONTH
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Year_Plus_1);                                                                //Natural: MOVE #IARTMG-YEAR-PLUS-1 TO #COMPARE-AUV-YEAR
        pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month.setValue(pnd_Iartmg_Date_Plus_1_Pnd_Iartmg_Month_Plus_1);                                                              //Natural: MOVE #IARTMG-MONTH-PLUS-1 TO #COMPARE-AUV-MONTH
        //*  WRITE '#COMPARE-AUV-DATE  = ' #COMPARE-AUV-DATE
        //*  WRITE '#COMPARE-RATE-DATE = ' #COMPARE-RATE-DATE
        //* *****
        if (condition(pnd_Compare_Auv_Date.less(pnd_Compare_Rate_Date)))                                                                                                  //Natural: IF #COMPARE-AUV-DATE < #COMPARE-RATE-DATE
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Century);                                               //Natural: MOVE #COMPARE-AUV-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Year_2);                                                   //Natural: MOVE #COMPARE-AUV-YEAR-2 TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(pnd_Compare_Auv_Date_Pnd_Compare_Auv_Month);                                                   //Natural: MOVE #COMPARE-AUV-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Century().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century());                           //Natural: MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Year().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year());                                 //Natural: MOVE WTC-CREF-END-YEAR TO WTC-YEAR
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Month().setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month());                               //Natural: MOVE WTC-CREF-END-MONTH TO WTC-MONTH
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Day().setValue(0);                                                                                              //Natural: MOVE 0 TO WTC-DAY
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************************************************************
        //* ***   COMMENTED OUT THE FOLLOWING FOUR LINES OF CODE  G.F  12/30/97
        //* *****************************************************************
        //* * MOVE WTC-CREF-END-CENTURY TO WTC-CENTURY
        //* * MOVE WTC-CREF-END-YEAR    TO WTC-YEAR
        //* * MOVE WTC-CREF-END-MONTH   TO WTC-MONTH
        //* * MOVE WTC-CREF-END-DAY     TO WTC-DAY
        //*  WRITE 'WTC-FUND-INDICATOR = ' WTC-FUND-INDICATOR
        //*  WRITE 'WTC-DATE           = ' WTC-DATE
        //*  WRITE 'WTC-RECORD-KEYS = ' WTC-RECORD-KEYS
        ldaAial0270.getVw_cratemgr_M_View().startDatabaseFind                                                                                                             //Natural: FIND CRATEMGR-M-VIEW WITH WTC-RECORD-KEY = WTC-RECORD-KEYS
        (
        "FIND06",
        new Wc[] { new Wc("WTC_RECORD_KEY", "=", ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Record_Keys(), WcType.WITH) }
        );
        FIND06:
        while (condition(ldaAial0270.getVw_cratemgr_M_View().readNextRow("FIND06", true)))
        {
            ldaAial0270.getVw_cratemgr_M_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0270.getVw_cratemgr_M_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(5);                                                                                         //Natural: MOVE 5 TO #RETURN-CODE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  WRITE 'WTC-TIAACREF-PAYOUT-AREA = ' WTC-TIAACREF-PAYOUT-AREA
            ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas().setValue(ldaAial0270.getCratemgr_M_View_Wtc_Tiaacref_Payout_Area());                    //Natural: MOVE WTC-TIAACREF-PAYOUT-AREA TO WTC-TIAACREF-PAYOUT-AREAS
            pnd_Temp_In_Mortality_Table.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code());                                                   //Natural: MOVE WTC-CREF-MORT-TABLE-CODE TO #TEMP-IN-MORTALITY-TABLE
            pnd_Temp_In_Setback.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback());                                                        //Natural: MOVE WTC-CREF-MORT-TABLE-SETBACK TO #TEMP-IN-SETBACK
            pnd_Temp_In_Interest_Rate.setValue(ldaAial0271.getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest());                                                 //Natural: MOVE WTC-CREF-MORT-TABLE-INTEREST TO #TEMP-IN-INTEREST-RATE
            pnd_Temp_In_Def_Interest_Rate.setValue(0);                                                                                                                    //Natural: MOVE .04 TO #TEMP-IN-DEF-INTEREST-RATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-FISCAL-FACTORS
            sub_Calculate_Fiscal_Factors();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MOVE WTC-CREF-MORT-TABLE-CODE TO #AIAN021-IN-MORTALITY-TABLE
            //*  MOVE WTC-CREF-MORT-TABLE-SETBACK TO #AIAN021-IN-SETBACK
            //*  MOVE WTC-CREF-MORT-TABLE-INTEREST TO #AIAN021-IN-INTEREST-RATE
            //*  MOVE .04 TO #AIAN021-IN-DEF-INTEREST-RATE
            //*  CALLNAT 'AIAN021' USING #AIAN021-LINKAGE
            //*  IF #AIAN021-OUT-RETURN-CODE �= 0
            //*    MOVE 4 TO #RETURN-CODE
            //*    ESCAPE ROUTINE
            //*  ELSE
            //*    MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #T-CREF-ANN-FACTOR
            //*    WRITE '#AIAN021-LINKAGE = ' #AIAN021-LINKAGE
            //*    WRITE '#T-CREF-ANN-FACTOR = ' #T-CREF-ANN-FACTOR
            //*  END-IF
            //*  ITE '#FUND-CODE-OUT (' #CREF-SUB ') = ' #FUND-CODE-OUT (#CREF-SUB)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  COMPUTE ROUNDED #PMT-OLD = #UNITS-TO-RECEIVE (#I) * #AUV-OLD-1
        //*  COMPUTE ROUNDED #PMT-OUT =
        //*    #PMT-OLD * #T-REA-ANN-FACTOR / #T-CREF-ANN-FACTOR
        //*  COMPUTE ROUNDED #UNITS-OUT (#CREF-SUB) =
        //*    #PMT-OUT / #AUV-OUT (#CREF-SUB)
        //*  COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF (#CREF-SUB) =
        //*    #PMT-OLD * #T-REA-ANN-FACTOR
        pnd_Pmt_New.compute(new ComputeParameters(true, pnd_Pmt_New), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).multiply(pnd_Auv_Old_1)); //Natural: COMPUTE ROUNDED #PMT-NEW = #UNITS-TO-RECEIVE ( #I ) * #AUV-OLD-1
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                        //Natural: IF #REVALUATION-INDICATOR = 'M'
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #UNITS-OUT ( #CREF-SUB ) = ( #PMT-NEW * #T-REA-ANN-FACTOR-1 ) / ( #AUV-OUT ( #CREF-SUB ) * #ANNUITY-FACTOR-1 )
                (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1())).divide((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).multiply(pnd_Annuity_Factor_1))));
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = #PMT-NEW * #T-REA-ANN-FACTOR-1
                pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IF #ANNUITY-FACTOR-2 NOT = 0
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #UNITS-OUT ( #CREF-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 ) - ( #PMTS-TO-RECEIVE ( #I ) * #ANNUITY-FACTOR-TO-FISCAL ) ) / ( #AUV-OUT ( #CREF-SUB ) * #ANNUITY-FACTOR-2 )
                ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2()))).subtract((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pnd_Annuity_Factor_To_Fiscal)))).divide((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_Cref_Sub).multiply(pnd_Annuity_Factor_2))));
            //*   END-IF
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) = ( #PMT-NEW * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 )
                (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2()))));
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_Cref_Sub));      //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-SUB ) TO #TOTAL-TRANSFER-AMT-OUT
        //*  WRITE '#PMTS-TO-RECEIVE  (' #I ') = ' #PMTS-TO-RECEIVE  (#I)
        //*  WRITE '#UNITS-TO-RECEIVE (' #I ') = ' #UNITS-TO-RECEIVE (#I)
        //*  WRITE '#AUV-OLD-1 = ' #AUV-OLD-1
        //*  WRITE '#AUV-OUT (' #CREF-SUB ') = ' #AUV-OUT (#CREF-SUB)
        //*  WRITE '#T-REA-ANN-FACTOR-TO-FISCAL = ' #T-REA-ANN-FACTOR-TO-FISCAL
        //*  WRITE '#T-REA-ANN-FACTOR-2         = ' #T-REA-ANN-FACTOR-2
        //*  WRITE '#ANNUITY-FACTOR-TO-FISCAL   = ' #ANNUITY-FACTOR-TO-FISCAL
        //*  WRITE '#ANNUITY-FACTOR-2           = ' #ANNUITY-FACTOR-2
        //*  WRITE '#UNITS-OUT (' #CREF-SUB ') = ' #UNITS-OUT (#CREF-SUB)
        //*  WRITE '#TRANSFER-AMT-OUT-CREF (' #CREF-SUB ') = '
        //*  #TRANSFER-AMT-OUT-CREF (#CREF-SUB)
    }
    private void sub_To_Tiaa() throws Exception                                                                                                                           //Natural: TO-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  WRITE 'TO-TIAA'
        if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().greater(2) || pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().equals(21)) &&                   //Natural: IF ( #PAYEE-CODE > 2 OR #OPTION = 21 ) AND #FUND-TO-RECEIVE ( #I ) = '2'
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("2")))
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().setValue(7);                                                                                             //Natural: MOVE 7 TO #RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Sub.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #TIAA-SUB
        if (condition((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date().greater(196902) && pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date().less(197703))           //Natural: IF ( #ISSUE-DATE > 196902 AND #ISSUE-DATE < 197703 ) AND #TIAA-CURRENT-IA-RB = 74
            && pnd_Tiaa_Current_Ia_Rb.equals(74)))
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(pnd_Tiaa_Sub).setValue(24);                                                                   //Natural: MOVE 24 TO #RATE-CODE-OUT ( #TIAA-SUB )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(pnd_Tiaa_Sub).setValue(pnd_Tiaa_Current_Ia_Rb);                                               //Natural: MOVE #TIAA-CURRENT-IA-RB TO #RATE-CODE-OUT ( #TIAA-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(pnd_Tiaa_Sub).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I)); //Natural: MOVE #FUND-TO-RECEIVE ( #I ) TO #PMT-METHOD-CODE-OUT ( #TIAA-SUB )
        //*  COMPUTE ROUNDED #PMT-OLD = #UNITS-TO-RECEIVE (#I) * #AUV-OLD-1
        //*  WRITE '#UNITS-TO-RECEIVE (' #I ') = ' #UNITS-TO-RECEIVE (#I)
        //*  WRITE '#AUV-OLD-1 = ' #AUV-OLD-1 ' #PMT-OLD = ' #PMT-OLD
        pnd_Pmt_New.compute(new ComputeParameters(true, pnd_Pmt_New), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).multiply(pnd_Auv_Old_1)); //Natural: COMPUTE ROUNDED #PMT-NEW = #UNITS-TO-RECEIVE ( #I ) * #AUV-OLD-1
        //*  WRITE '#UNITS-TO-RECEIVE (' #I ') = ' #UNITS-TO-RECEIVE (#I)
        //*  WRITE '#AUV-OLD-1 = ' #AUV-OLD-1 ' #PMT-NEW = ' #PMT-NEW
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().equals("R")))                                                                                    //Natural: IF #FUND-CODE = 'R'
        {
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                    //Natural: IF #REVALUATION-INDICATOR = 'M'
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-TIAA ( #TIAA-SUB ) = #PMT-NEW * #T-REA-ANN-FACTOR-1
                    pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-TIAA ( #TIAA-SUB ) = ( #PMTS-TO-RECEIVE ( #I ) * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 )
                    (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2()))));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("1")))                                                          //Natural: IF #FUND-TO-RECEIVE ( #I ) = '1'
            {
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                //Natural: IF #REVALUATION-INDICATOR = 'M'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = #PMT-NEW * #T-REA-ANN-FACTOR-1 / #T-GUAR-STD-ANN-FACTORA
                        pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1()).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Std_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = ( #PMT-NEW * #T-REA-ANN-FACTOR-1 / #T-TOT-STD-ANN-FACTORA ) - #GTD-PMT-OUT ( #TIAA-SUB )
                        (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1()).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Std_Ann_Factora)).subtract(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 ) ) / #T-GUAR-STD-ANN-FACTORA
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Std_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 ) ) / #T-TOT-STD-ANN-FACTORA - #GTD-PMT-OUT ( #TIAA-SUB )
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Std_Ann_Factora).subtract(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                //Natural: IF #REVALUATION-INDICATOR = 'M'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = ( #PMT-NEW * #T-REA-ANN-FACTOR-1 ) / #T-GUAR-GRD-ANN-FACTORA
                        (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1())).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Grd_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = #PMT-NEW * #T-REA-ANN-FACTOR-1 / #T-TOT-GRD-ANN-FACTORA
                        pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_1()).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Grd_Ann_Factora));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 ) ) / #T-GUAR-GRD-ANN-FACTORA
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Grd_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-REA-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-REA-ANN-FACTOR-2 ) ) / #T-TOT-GRD-ANN-FACTORA
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Rea_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Grd_Ann_Factora));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                    //Natural: IF #REVALUATION-INDICATOR = 'M'
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-TIAA ( #TIAA-SUB ) = #PMT-NEW * #T-CREF-ANN-FACTOR-1
                    pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #TRANSFER-AMT-OUT-TIAA ( #TIAA-SUB ) = ( #PMTS-TO-RECEIVE ( #I ) * #T-CREF-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-CREF-ANN-FACTOR-2 )
                    (pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2()))));
                //*    WRITE '#PMTS-TO-RECEIVE (' #I ') = ' #PMTS-TO-RECEIVE (#I)
                //*    WRITE '#T-CREF-ANN-FACTOR-TO-FISCAL = '
                //*      #T-CREF-ANN-FACTOR-TO-FISCAL
                //*    WRITE '#PMT-NEW = ' #PMT-NEW
                //*    WRITE '#T-CREF-ANN-FACTOR-2   = ' #T-CREF-ANN-FACTOR-2
                //*    WRITE '#TRANSFER-AMT-OUT-TIAA (' #TIAA-SUB ') = '
                //*      #TRANSFER-AMT-OUT-TIAA (#TIAA-SUB)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("1")))                                                          //Natural: IF #FUND-TO-RECEIVE ( #I ) = '1'
            {
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                //Natural: IF #REVALUATION-INDICATOR = 'M'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = #PMT-NEW * #T-CREF-ANN-FACTOR-1 / #T-GUAR-STD-ANN-FACTORA
                        pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1()).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Std_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = ( #PMT-NEW * #T-CREF-ANN-FACTOR-1 / #T-TOT-STD-ANN-FACTORA ) - #GTD-PMT-OUT ( #TIAA-SUB )
                        (pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1()).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Std_Ann_Factora)).subtract(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)));
                    //*      WRITE '#PMT-NEW = ' #PMT-NEW
                    //*      WRITE '#T-CREF-ANN-FACTOR-1   = ' #T-CREF-ANN-FACTOR-1
                    //*      WRITE '#T-GUAR-STD-ANN-FACTORA = ' #T-GUAR-STD-ANN-FACTORA
                    //*      WRITE '#T-TOT-STD-ANN-FACTORA  = ' #T-TOT-STD-ANN-FACTORA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-CREF-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-CREF-ANN-FACTOR-2 ) ) / #T-GUAR-STD-ANN-FACTORA
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Std_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-CREF-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-CREF-ANN-FACTOR-2 ) ) / #T-TOT-STD-ANN-FACTORA - #GTD-PMT-OUT ( #TIAA-SUB )
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Std_Ann_Factora).subtract(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)));
                    //*      WRITE '#PMTS-TO-RECEIVE (' #I ' ) = ' #PMTS-TO-RECEIVE (#I)
                    //*      WRITE '#PMT-NEW = ' #PMT-NEW
                    //*      WRITE '#T-CREF-ANN-FACTOR-TO-FISCAL = '
                    //*             #T-CREF-ANN-FACTOR-TO-FISCAL
                    //*      WRITE '#T-CREF-ANN-FACTOR-2 = ' #T-CREF-ANN-FACTOR-2
                    //*      WRITE '#T-GUAR-STD-ANN-FACTORA = ' #T-GUAR-STD-ANN-FACTORA
                    //*      WRITE '#T-TOT-STD-ANN-FACTORA  = ' #T-TOT-STD-ANN-FACTORA
                    //*      WRITE '#GTD-PMT-OUT (' #TIAA-SUB ') = ' #GTD-PMT-OUT (#TIAA-SUB)
                    //*      WRITE '#DVD-PMT-OUT (' #TIAA-SUB ') = ' #DVD-PMT-OUT (#TIAA-SUB)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().equals("M")))                                                                //Natural: IF #REVALUATION-INDICATOR = 'M'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = #PMT-NEW * #T-CREF-ANN-FACTOR-1 / #T-GUAR-GRD-ANN-FACTORA
                        pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1()).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Grd_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = #PMT-NEW * #T-CREF-ANN-FACTOR-1 / #T-TOT-GRD-ANN-FACTORA
                        pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_1()).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Grd_Ann_Factora));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #GTD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-CREF-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-CREF-ANN-FACTOR-2 ) ) / #T-GUAR-GRD-ANN-FACTORA
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Guar_Grd_Ann_Factora));
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = ( ( #PMTS-TO-RECEIVE ( #I ) * #T-CREF-ANN-FACTOR-TO-FISCAL ) + ( #PMT-NEW * #T-CREF-ANN-FACTOR-2 ) ) / #T-TOT-GRD-ANN-FACTORA
                        ((pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_To_Fiscal())).add((pnd_Pmt_New.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Cref_Ann_Factor_2())))).divide(pnd_Tiaa_Mortality_Arraysa_Pnd_T_Tot_Grd_Ann_Factora));
                    //*    WRITE '#T-GUAR-GRD-ANN-FACTORA = ' #T-GUAR-GRD-ANN-FACTORA
                    //*    WRITE '#T-TOT-GRD-ANN-FACTORA  = ' #T-TOT-GRD-ANN-FACTORA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).equals("2")))                                                              //Natural: IF #FUND-TO-RECEIVE ( #I ) = '2'
        {
            pnd_V_4.compute(new ComputeParameters(true, pnd_V_4), new DbsDecimal("1.0E0").divide(new DbsDecimal("1.04E0")));                                              //Natural: COMPUTE ROUNDED #V-4 = 1.0E0 / 1.04E0
            pnd_V_I.compute(new ComputeParameters(true, pnd_V_I), new DbsDecimal("1.0E0").divide((new DbsDecimal("1.0E0").add(pdaAial0130.getPnd_Aian013_Linkage_Pnd_T_Tot_Std_Mort_Tbl_Interest())))); //Natural: COMPUTE ROUNDED #V-I = 1.0E0 / ( 1.0E0 + #T-TOT-STD-MORT-TBL-INTEREST )
            //*  COMPUTE ROUNDED #MODAL-FACTOR =
            //*    ((#ONE - #V-4) / (#MONTHS* (#ONE - (#V-4 ** (1.0E0 / #MONTHS)))))
            //*    / ((#ONE - #V-I) / (#MONTHS* (#ONE - (#V-I ** (#ONE / #MONTHS)))))
            //*  COMPUTE ROUNDED #ONE-MINUS-V  = #ONE - #V-4
            //*  COMPUTE ROUNDED #ONE-OVER-MONTHS  = #ONE / #MONTHS
            //*  COMPUTE ROUNDED #V-TO-POWER  = #V-4 ** #ONE-OVER-MONTHS
            //*  COMPUTE ROUNDED #ONE-MINUS-V-TO-POWER  = #ONE - #V-TO-POWER
            //*  COMPUTE ROUNDED #MONTHS-TIMES-POWER  = #MONTHS * #ONE-MINUS-V-TO-POWER
            //*  COMPUTE ROUNDED #MODAL-NUM = #ONE-MINUS-V / #MONTHS-TIMES-POWER
            pnd_Modal_Num.compute(new ComputeParameters(true, pnd_Modal_Num), ((pnd_One.subtract(pnd_V_4)).divide((pnd_Months.multiply((pnd_One.subtract(NMath.pow        //Natural: COMPUTE ROUNDED #MODAL-NUM = ( #ONE - #V-4 ) / ( #MONTHS* ( #ONE - ( #V-4 ** ( #ONE / #MONTHS ) ) ) )
                (pnd_V_4 ,(pnd_One.divide(pnd_Months))))))))));
            pnd_Modal_Den.compute(new ComputeParameters(true, pnd_Modal_Den), ((pnd_One.subtract(pnd_V_I)).divide((pnd_Months.multiply((pnd_One.subtract(NMath.pow        //Natural: COMPUTE ROUNDED #MODAL-DEN = ( #ONE - #V-I ) / ( #MONTHS* ( #ONE - ( #V-I ** ( #ONE / #MONTHS ) ) ) )
                (pnd_V_I ,(pnd_One.divide(pnd_Months))))))))));
            pnd_Modal_Factor.compute(new ComputeParameters(true, pnd_Modal_Factor), pnd_Modal_Num.divide(pnd_Modal_Den));                                                 //Natural: COMPUTE ROUNDED #MODAL-FACTOR = #MODAL-NUM / #MODAL-DEN
            //*  WRITE '#V-4                  = ' #V-4
            //*  WRITE '#V-I                  = ' #V-I
            //*  WRITE '#MODAL-NUM            = ' #MODAL-NUM
            //*  WRITE '#MODAL-DEN          = ' #MODAL-DEN
            //*  WRITE '#MODAL-FACTOR       = ' #MODAL-FACTOR
            //*  WRITE 'BEFORE MODAL'
            //*  WRITE '#GTD-PMT-OUT (' #TIAA-SUB ') = ' #GTD-PMT-OUT (#TIAA-SUB)
            //*  WRITE '#DVD-PMT-OUT (' #TIAA-SUB ') = ' #DVD-PMT-OUT (#TIAA-SUB)
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub)),  //Natural: COMPUTE ROUNDED #DVD-PMT-OUT ( #TIAA-SUB ) = #DVD-PMT-OUT ( #TIAA-SUB ) * #MODAL-FACTOR - #GTD-PMT-OUT ( #TIAA-SUB )
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_Tiaa_Sub).multiply(pnd_Modal_Factor).subtract(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_Tiaa_Sub)));
            //*  WRITE 'AFTER  MODAL'
            //*  WRITE '#GTD-PMT-OUT (' #TIAA-SUB ') = ' #GTD-PMT-OUT (#TIAA-SUB)
            //*  WRITE '#DVD-PMT-OUT (' #TIAA-SUB ') = ' #DVD-PMT-OUT (#TIAA-SUB)
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_Tiaa_Sub));      //Natural: ADD #TRANSFER-AMT-OUT-TIAA ( #TIAA-SUB ) TO #TOTAL-TRANSFER-AMT-OUT
    }
    private void sub_Write_Adabas_Record() throws Exception                                                                                                               //Natural: WRITE-ADABAS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        //*  WRITE 'WRITE-ADABAS-RECORD'
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1().setValue("C");                                                                                   //Natural: MOVE 'C' TO #CREF-REA-INDICATOR-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch());                     //Natural: MOVE #TRANSFER-REVAL-SWITCH TO #TRANSFER-REVAL-SWITCH-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Contract_Payee_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Payee());                                   //Natural: MOVE #CONTRACT-PAYEE TO #CONTRACT-PAYEE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Fund_Code_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code());                                             //Natural: MOVE #FUND-CODE TO #FUND-CODE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Mode_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode());                                                       //Natural: MOVE #MODE TO #MODE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Option_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option());                                                   //Natural: MOVE #OPTION TO #OPTION-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Issue_Date_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date());                                           //Natural: MOVE #ISSUE-DATE TO #ISSUE-DATE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Final_Per_Pay_Date_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date());                           //Natural: MOVE #FINAL-PER-PAY-DATE TO #FINAL-PER-PAY-DATE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dob_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob());                                     //Natural: MOVE #FIRST-ANN-DOB TO #FIRST-ANN-DOB-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_First_Ann_Sex_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex());                                     //Natural: MOVE #FIRST-ANN-SEX TO #FIRST-ANN-SEX-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dod_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod());                                     //Natural: MOVE #FIRST-ANN-DOD TO #FIRST-ANN-DOD-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dob_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob());                                   //Natural: MOVE #SECOND-ANN-DOB TO #SECOND-ANN-DOB-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Sex_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex());                                   //Natural: MOVE #SECOND-ANN-SEX TO #SECOND-ANN-SEX-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dod_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dod());                                   //Natural: MOVE #SECOND-ANN-DOD TO #SECOND-ANN-DOD-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Effective_Date_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date());                 //Natural: MOVE #TRANSFER-EFFECTIVE-DATE TO #TRANSFER-EFFECTIVE-DATE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Type_Of_Run_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run());                                         //Natural: MOVE #TYPE-OF-RUN TO #TYPE-OF-RUN-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator());                     //Natural: MOVE #REVALUATION-INDICATOR TO #REVALUATION-INDICATOR-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Processing_Date_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date());                                 //Natural: MOVE #PROCESSING-DATE TO #PROCESSING-DATE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Illustration_Eff_Date_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date());                     //Natural: MOVE #ILLUSTRATION-EFF-DATE TO #ILLUSTRATION-EFF-DATE-S1
        ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units());                                   //Natural: MOVE #TRANSFER-UNITS TO #TRANSFER-UNITS-S1
        pnd_J.setValue(0);                                                                                                                                                //Natural: MOVE 0 TO #J
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO 8
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(8)); pnd_I.nadd(1))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Fund_To_Receive_S1().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_J)); //Natural: MOVE #FUND-TO-RECEIVE ( #J ) TO #FUND-TO-RECEIVE-S1 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Units_To_Receive_S1().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_J)); //Natural: MOVE #UNITS-TO-RECEIVE ( #J ) TO #UNITS-TO-RECEIVE-S1 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_1_Pnd_Pmts_To_Receive_S1().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_J)); //Natural: MOVE #PMTS-TO-RECEIVE ( #J ) TO #PMTS-TO-RECEIVE-S1 ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 13
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(13)); pnd_I.nadd(1))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            ldaAial013b.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_J)); //Natural: MOVE #FUND-TO-RECEIVE ( #J ) TO #FUND-TO-RECEIVE-S2 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_2_Pnd_Units_To_Receive_S2().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_J)); //Natural: MOVE #UNITS-TO-RECEIVE ( #J ) TO #UNITS-TO-RECEIVE-S2 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_2_Pnd_Pmts_To_Receive_S2().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_J)); //Natural: MOVE #PMTS-TO-RECEIVE ( #J ) TO #PMTS-TO-RECEIVE-S2 ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAial013b.getPnd_Store_Actrl_Fld_2_Pnd_Next_Pymnt_Dte_S2().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte());                                   //Natural: MOVE #NEXT-PYMNT-DTE TO #NEXT-PYMNT-DTE-S2
        ldaAial013b.getPnd_Store_Actrl_Fld_2_Pnd_Return_Code_S2().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr());                                     //Natural: MOVE #RETURN-CODE-NBR TO #RETURN-CODE-S2
        ldaAial013b.getPnd_Store_Actrl_Fld_2_Pnd_Transfer_Eff_Date_Out_S2().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Eff_Date_Out());                     //Natural: MOVE #TRANSFER-EFF-DATE-OUT TO #TRANSFER-EFF-DATE-OUT-S2
        pnd_J.setValue(0);                                                                                                                                                //Natural: MOVE 0 TO #J
        FOR09:                                                                                                                                                            //Natural: FOR #I = 1 TO 9
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            ldaAial013b.getPnd_Store_Actrl_Fld_3_Pnd_Fund_Code_Out_S3().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_J)); //Natural: MOVE #FUND-CODE-OUT ( #J ) TO #FUND-CODE-OUT-S3 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_3_Pnd_Units_Out_S3().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_J));         //Natural: MOVE #UNITS-OUT ( #J ) TO #UNITS-OUT-S3 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_3_Pnd_Auv_Out_S3().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_J));             //Natural: MOVE #AUV-OUT ( #J ) TO #AUV-OUT-S3 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_3_Pnd_Transfer_Amt_Out_Cref_S3().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_J)); //Natural: MOVE #TRANSFER-AMT-OUT-CREF ( #J ) TO #TRANSFER-AMT-OUT-CREF-S3 ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #I = 1 TO 9
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            ldaAial013b.getPnd_Store_Actrl_Fld_4_Pnd_Fund_Code_Out_S4().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_J)); //Natural: MOVE #FUND-CODE-OUT ( #J ) TO #FUND-CODE-OUT-S4 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_4_Pnd_Units_Out_S4().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_J));         //Natural: MOVE #UNITS-OUT ( #J ) TO #UNITS-OUT-S4 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_4_Pnd_Auv_Out_S4().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_J));             //Natural: MOVE #AUV-OUT ( #J ) TO #AUV-OUT-S4 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_4_Pnd_Transfer_Amt_Out_Cref_S4().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_J)); //Natural: MOVE #TRANSFER-AMT-OUT-CREF ( #J ) TO #TRANSFER-AMT-OUT-CREF-S4 ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Fund_Code_Out_S5().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(19));                        //Natural: MOVE #FUND-CODE-OUT ( 19 ) TO #FUND-CODE-OUT-S5
        ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Units_Out_S5().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(19));                                //Natural: MOVE #UNITS-OUT ( 19 ) TO #UNITS-OUT-S5
        ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Auv_Out_S5().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(19));                                    //Natural: MOVE #AUV-OUT ( 19 ) TO #AUV-OUT-S5
        ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Cref_S5().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(19));        //Natural: MOVE #TRANSFER-AMT-OUT-CREF ( 19 ) TO #TRANSFER-AMT-OUT-CREF-S5
        FOR11:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Pmt_Method_Code_Out_S5().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(pnd_I)); //Natural: MOVE #PMT-METHOD-CODE-OUT ( #I ) TO #PMT-METHOD-CODE-OUT-S5 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Rate_Code_Out_S5().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(pnd_I)); //Natural: MOVE #RATE-CODE-OUT ( #I ) TO #RATE-CODE-OUT-S5 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Gtd_Pmt_Out_S5().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_I));     //Natural: MOVE #GTD-PMT-OUT ( #I ) TO #GTD-PMT-OUT-S5 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Dvd_Pmt_Out_S5().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_I));     //Natural: MOVE #DVD-PMT-OUT ( #I ) TO #DVD-PMT-OUT-S5 ( #I )
            ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Tiaa_S5().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_I)); //Natural: MOVE #TRANSFER-AMT-OUT-TIAA ( #I ) TO #TRANSFER-AMT-OUT-TIAA-S5 ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAial013b.getPnd_Store_Actrl_Fld_5_Pnd_Total_Transfer_Amt_Out_S5().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out());                   //Natural: MOVE #TOTAL-TRANSFER-AMT-OUT TO #TOTAL-TRANSFER-AMT-OUT-S5
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_1().setValue(ldaAial013b.getPnd_Store_Actrl_Fld_1());                                                     //Natural: MOVE #STORE-ACTRL-FLD-1 TO IAXFR-ACTRL-FLD-1
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_2().setValue(ldaAial013b.getPnd_Store_Actrl_Fld_2());                                                     //Natural: MOVE #STORE-ACTRL-FLD-2 TO IAXFR-ACTRL-FLD-2
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_3().setValue(ldaAial013b.getPnd_Store_Actrl_Fld_3());                                                     //Natural: MOVE #STORE-ACTRL-FLD-3 TO IAXFR-ACTRL-FLD-3
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_4().setValue(ldaAial013b.getPnd_Store_Actrl_Fld_4());                                                     //Natural: MOVE #STORE-ACTRL-FLD-4 TO IAXFR-ACTRL-FLD-4
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_5().setValue(ldaAial013b.getPnd_Store_Actrl_Fld_5());                                                     //Natural: MOVE #STORE-ACTRL-FLD-5 TO IAXFR-ACTRL-FLD-5
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Lst_Chnge_Dte().setValue(Global.getTIMX());                                                                               //Natural: MOVE *TIMX TO LST-CHNGE-DTE
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Cntrct_Py().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Payee());                                //Natural: MOVE #CONTRACT-PAYEE TO IAXFR-ACTRL-CNTRCT-PY
        ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Prcss_Dte().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date());                               //Natural: MOVE #PROCESSING-DATE TO IAXFR-ACTRL-PRCSS-DTE
        ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().insertDBRow();                                                                                                       //Natural: STORE IAA-XFR-ACTRL-RCRD-VIEW2
        //*  END TRANSACTION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=66 ES=ON");
    }
}
