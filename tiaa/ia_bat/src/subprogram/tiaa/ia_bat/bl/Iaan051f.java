/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:34:14 AM
**        * FROM NATURAL SUBPROGRAM : Iaan051f
************************************************************
**        * FILE NAME            : Iaan051f.java
**        * CLASS NAME           : Iaan051f
**        * INSTANCE NAME        : Iaan051f
************************************************************
************************************************************************
* PROGRAM  : IAAN051F
* FUNCTION : THIS IS A BRIDGE MODULE THAT CALLS THE NEW STANDARD
*            ROUTINE (IAAN0510) THAT READS THE EXTERNALIZATION FILE.
* AUTHOR   : ARI G. 05/00
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan051f extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_W_Ia_Rpt_Nm_Cd;
    private DbsField pnd_W_Ia_Rpt_Cmpny_Cd_A;
    private DbsField pnd_Q;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_W_Ia_Rpt_Nm_Cd = parameters.newFieldArrayInRecord("pnd_W_Ia_Rpt_Nm_Cd", "#W-IA-RPT-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            80));
        pnd_W_Ia_Rpt_Nm_Cd.setParameterOption(ParameterOption.ByReference);
        pnd_W_Ia_Rpt_Cmpny_Cd_A = parameters.newFieldArrayInRecord("pnd_W_Ia_Rpt_Cmpny_Cd_A", "#W-IA-RPT-CMPNY-CD-A", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_W_Ia_Rpt_Cmpny_Cd_A.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Q = localVariables.newFieldInRecord("pnd_Q", "#Q", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan051f() throws Exception
    {
        super("Iaan051f");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Nm_Cd().getValue(1,":",80).reset();                                                                                            //Natural: RESET #IA-RPT-NM-CD ( 1:80 ) #IA-RPT-CMPNY-CD-A ( 1:80 )
        pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Cmpny_Cd_A().getValue(1,":",80).reset();
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_W_Ia_Rpt_Nm_Cd.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Nm_Cd().getValue("*"));                                                              //Natural: ASSIGN #W-IA-RPT-NM-CD ( * ) := IAAA051Z.#IA-RPT-NM-CD ( * )
        pnd_W_Ia_Rpt_Cmpny_Cd_A.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Cmpny_Cd_A().getValue("*"));                                                    //Natural: ASSIGN #W-IA-RPT-CMPNY-CD-A ( * ) := IAAA051Z.#IA-RPT-CMPNY-CD-A ( * )
    }

    //
}
