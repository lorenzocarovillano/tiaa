/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:39:25 AM
**        * FROM NATURAL SUBPROGRAM : Nazn6033
************************************************************
**        * FILE NAME            : Nazn6033.java
**        * CLASS NAME           : Nazn6033
**        * INSTANCE NAME        : Nazn6033
************************************************************
************************************************************************
*
* PROGRAM:  NAZN6033
* DATE   :  05/01/1997
* FUNCTION: THIS ROUTINE CALCULATES THE IA DATE FOR RATE BASIS
*
*
*
*
*
*
*
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nazn6033 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Rate_Dte;

    private DbsGroup pnd_Rate_Dte__R_Field_1;
    private DbsField pnd_Rate_Dte_Pnd_Rate_Dte_P;
    private DbsField pnd_Frst_Pymnt_Curr_Dte;

    private DbsGroup pnd_Frst_Pymnt_Curr_Dte__R_Field_2;
    private DbsField pnd_Frst_Pymnt_Curr_Dte_Pnd_Frst_Pymnt_Curr_Dte_P;
    private DbsField pnd_Date_1;

    private DbsGroup pnd_Date_1__R_Field_3;
    private DbsField pnd_Date_1_Pnd_Date_1_P;
    private DbsField pnd_Old_Per_Pymnt;
    private DbsField pnd_Old_Div_Pymnt;
    private DbsField pnd_New_Per_Pymnt;
    private DbsField pnd_New_Div_Pymnt;
    private DbsField pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Cntrct_Issue_Dte;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Rate_Dte = parameters.newFieldInRecord("pnd_Rate_Dte", "#RATE-DTE", FieldType.DATE);
        pnd_Rate_Dte.setParameterOption(ParameterOption.ByReference);

        pnd_Rate_Dte__R_Field_1 = parameters.newGroupInRecord("pnd_Rate_Dte__R_Field_1", "REDEFINE", pnd_Rate_Dte);
        pnd_Rate_Dte_Pnd_Rate_Dte_P = pnd_Rate_Dte__R_Field_1.newFieldInGroup("pnd_Rate_Dte_Pnd_Rate_Dte_P", "#RATE-DTE-P", FieldType.PACKED_DECIMAL, 7);
        pnd_Frst_Pymnt_Curr_Dte = parameters.newFieldInRecord("pnd_Frst_Pymnt_Curr_Dte", "#FRST-PYMNT-CURR-DTE", FieldType.DATE);
        pnd_Frst_Pymnt_Curr_Dte.setParameterOption(ParameterOption.ByReference);

        pnd_Frst_Pymnt_Curr_Dte__R_Field_2 = parameters.newGroupInRecord("pnd_Frst_Pymnt_Curr_Dte__R_Field_2", "REDEFINE", pnd_Frst_Pymnt_Curr_Dte);
        pnd_Frst_Pymnt_Curr_Dte_Pnd_Frst_Pymnt_Curr_Dte_P = pnd_Frst_Pymnt_Curr_Dte__R_Field_2.newFieldInGroup("pnd_Frst_Pymnt_Curr_Dte_Pnd_Frst_Pymnt_Curr_Dte_P", 
            "#FRST-PYMNT-CURR-DTE-P", FieldType.PACKED_DECIMAL, 7);
        pnd_Date_1 = parameters.newFieldInRecord("pnd_Date_1", "#DATE-1", FieldType.DATE);
        pnd_Date_1.setParameterOption(ParameterOption.ByReference);

        pnd_Date_1__R_Field_3 = parameters.newGroupInRecord("pnd_Date_1__R_Field_3", "REDEFINE", pnd_Date_1);
        pnd_Date_1_Pnd_Date_1_P = pnd_Date_1__R_Field_3.newFieldInGroup("pnd_Date_1_Pnd_Date_1_P", "#DATE-1-P", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Per_Pymnt = parameters.newFieldInRecord("pnd_Old_Per_Pymnt", "#OLD-PER-PYMNT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Old_Per_Pymnt.setParameterOption(ParameterOption.ByReference);
        pnd_Old_Div_Pymnt = parameters.newFieldInRecord("pnd_Old_Div_Pymnt", "#OLD-DIV-PYMNT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Old_Div_Pymnt.setParameterOption(ParameterOption.ByReference);
        pnd_New_Per_Pymnt = parameters.newFieldInRecord("pnd_New_Per_Pymnt", "#NEW-PER-PYMNT", FieldType.NUMERIC, 9, 2);
        pnd_New_Per_Pymnt.setParameterOption(ParameterOption.ByReference);
        pnd_New_Div_Pymnt = parameters.newFieldInRecord("pnd_New_Div_Pymnt", "#NEW-DIV-PYMNT", FieldType.NUMERIC, 9, 2);
        pnd_New_Div_Pymnt.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Optn_Cde = parameters.newFieldInRecord("pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Optn_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Issue_Dte = parameters.newFieldInRecord("pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Cntrct_Issue_Dte.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Nazn6033() throws Exception
    {
        super("Nazn6033");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Rate_Dte.reset();                                                                                                                                             //Natural: RESET #RATE-DTE
        if (condition(pnd_Cntrct_Optn_Cde.equals(21) && pnd_Cntrct_Issue_Dte.less(199104)))                                                                               //Natural: IF #CNTRCT-OPTN-CDE = 21 AND #CNTRCT-ISSUE-DTE LT 199104
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rate_Dte_Pnd_Rate_Dte_P.compute(new ComputeParameters(true, pnd_Rate_Dte_Pnd_Rate_Dte_P), ((pnd_Frst_Pymnt_Curr_Dte_Pnd_Frst_Pymnt_Curr_Dte_P.multiply((pnd_Old_Per_Pymnt.add(pnd_Old_Div_Pymnt)))).add((pnd_Date_1_Pnd_Date_1_P.multiply((pnd_New_Per_Pymnt.add(pnd_New_Div_Pymnt)))))).divide((pnd_Old_Per_Pymnt.add(pnd_Old_Div_Pymnt).add(pnd_New_Per_Pymnt).add(pnd_New_Div_Pymnt)))); //Natural: COMPUTE ROUNDED #RATE-DTE-P = ( ( #FRST-PYMNT-CURR-DTE-P * ( #OLD-PER-PYMNT + #OLD-DIV-PYMNT ) ) + ( #DATE-1-P * ( #NEW-PER-PYMNT + #NEW-DIV-PYMNT ) ) ) / ( #OLD-PER-PYMNT + #OLD-DIV-PYMNT + #NEW-PER-PYMNT + #NEW-DIV-PYMNT )
    }

    //
}
