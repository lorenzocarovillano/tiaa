/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:35:14 AM
**        * FROM NATURAL SUBPROGRAM : Iaan390
************************************************************
**        * FILE NAME            : Iaan390.java
**        * CLASS NAME           : Iaan390
**        * INSTANCE NAME        : Iaan390
************************************************************
************************************************************************
* PROGRAM  : IAAN390
* DATE     : JANUARY 22, 1998
* FUNCTION : THIS PROGRAM WILL CALL THE ACTUARIAL MODULE IAAN026 TO GET
*            THE UNIT VALUE OF A PARTICULAR FUND FOR A GIVEN DATE.
*            THIS WILL HANDLE BOTH MONTHLY AND ANNUAL BASED UNITS.
*
* MAINTENANCE LOG:
*
* PROGRAMMER        DATE        DESCRIPTION
*
* 1. JUN F. TINIO   01/22/1998  ORIGINAL CODE
* 2. J BREMER       04/04/2012  RATE BASIS EXPANSION - JB01
*
*
*
*
*
*
*
*
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan390 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Call_Type;
    private DbsField pnd_Parm_Fund_Code;
    private DbsField pnd_Parm_Reval_Meth;
    private DbsField pnd_Parm_Req_Dte;
    private DbsField pnd_Parm_Issue_Dte;
    private DbsField pnd_Parm_Iuv;

    private DbsGroup aian026;

    private DbsGroup aian026_Pnd_Input;
    private DbsField aian026_Pnd_Call_Type;
    private DbsField aian026_Pnd_Ia_Fund_Code;
    private DbsField aian026_Pnd_Revaluation_Method;
    private DbsField aian026_Pnd_Uv_Req_Dte;
    private DbsField aian026_Pnd_Prtcptn_Dte;

    private DbsGroup aian026_Pnd_Output;
    private DbsField aian026_Pnd_Rtrn_Cde_Pgm;

    private DbsGroup aian026__R_Field_1;
    private DbsField aian026_Pnd_Rtrn_Pgm;
    private DbsField aian026_Pnd_Rtrn_Cde;
    private DbsField aian026_Pnd_Iuv;
    private DbsField aian026_Pnd_Iuv_Dte;
    private DbsField aian026_Pnd_Days_In_Request_Month;
    private DbsField aian026_Pnd_Days_In_Particip_Month;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Call_Type = parameters.newFieldInRecord("pnd_Parm_Call_Type", "#PARM-CALL-TYPE", FieldType.STRING, 1);
        pnd_Parm_Call_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Fund_Code = parameters.newFieldInRecord("pnd_Parm_Fund_Code", "#PARM-FUND-CODE", FieldType.STRING, 1);
        pnd_Parm_Fund_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Reval_Meth = parameters.newFieldInRecord("pnd_Parm_Reval_Meth", "#PARM-REVAL-METH", FieldType.STRING, 1);
        pnd_Parm_Reval_Meth.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Req_Dte = parameters.newFieldInRecord("pnd_Parm_Req_Dte", "#PARM-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_Parm_Req_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Issue_Dte = parameters.newFieldInRecord("pnd_Parm_Issue_Dte", "#PARM-ISSUE-DTE", FieldType.NUMERIC, 8);
        pnd_Parm_Issue_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Iuv = parameters.newFieldInRecord("pnd_Parm_Iuv", "#PARM-IUV", FieldType.PACKED_DECIMAL, 8, 4);
        pnd_Parm_Iuv.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        aian026 = localVariables.newGroupInRecord("aian026", "AIAN026");

        aian026_Pnd_Input = aian026.newGroupInGroup("aian026_Pnd_Input", "#INPUT");
        aian026_Pnd_Call_Type = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        aian026_Pnd_Ia_Fund_Code = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        aian026_Pnd_Revaluation_Method = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Revaluation_Method", "#REVALUATION-METHOD", FieldType.STRING, 
            1);
        aian026_Pnd_Uv_Req_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);
        aian026_Pnd_Prtcptn_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Prtcptn_Dte", "#PRTCPTN-DTE", FieldType.NUMERIC, 8);

        aian026_Pnd_Output = aian026.newGroupInGroup("aian026_Pnd_Output", "#OUTPUT");
        aian026_Pnd_Rtrn_Cde_Pgm = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Rtrn_Cde_Pgm", "#RTRN-CDE-PGM", FieldType.STRING, 11);

        aian026__R_Field_1 = aian026_Pnd_Output.newGroupInGroup("aian026__R_Field_1", "REDEFINE", aian026_Pnd_Rtrn_Cde_Pgm);
        aian026_Pnd_Rtrn_Pgm = aian026__R_Field_1.newFieldInGroup("aian026_Pnd_Rtrn_Pgm", "#RTRN-PGM", FieldType.STRING, 8);
        aian026_Pnd_Rtrn_Cde = aian026__R_Field_1.newFieldInGroup("aian026_Pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 3);
        aian026_Pnd_Iuv = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv", "#IUV", FieldType.NUMERIC, 8, 4);
        aian026_Pnd_Iuv_Dte = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv_Dte", "#IUV-DTE", FieldType.NUMERIC, 8);
        aian026_Pnd_Days_In_Request_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", FieldType.NUMERIC, 
            2);
        aian026_Pnd_Days_In_Particip_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", FieldType.NUMERIC, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan390() throws Exception
    {
        super("Iaan390");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        aian026_Pnd_Call_Type.setValue(pnd_Parm_Call_Type);                                                                                                               //Natural: ASSIGN #CALL-TYPE := #PARM-CALL-TYPE
        aian026_Pnd_Revaluation_Method.setValue(pnd_Parm_Reval_Meth);                                                                                                     //Natural: ASSIGN #REVALUATION-METHOD := #PARM-REVAL-METH
        aian026_Pnd_Uv_Req_Dte.setValue(pnd_Parm_Req_Dte);                                                                                                                //Natural: ASSIGN #UV-REQ-DTE := #PARM-REQ-DTE
        aian026_Pnd_Prtcptn_Dte.setValue(pnd_Parm_Issue_Dte);                                                                                                             //Natural: ASSIGN #PRTCPTN-DTE := #PARM-ISSUE-DTE
        aian026_Pnd_Ia_Fund_Code.setValue(pnd_Parm_Fund_Code);                                                                                                            //Natural: ASSIGN #IA-FUND-CODE := #PARM-FUND-CODE
        pnd_Parm_Iuv.reset();                                                                                                                                             //Natural: RESET #PARM-IUV
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), aian026);                                                                                               //Natural: CALLNAT 'AIAN026' AIAN026
        if (condition(Global.isEscape())) return;
        if (condition(aian026_Pnd_Rtrn_Cde.equals(getZero())))                                                                                                            //Natural: IF #RTRN-CDE = 0
        {
            pnd_Parm_Iuv.setValue(aian026_Pnd_Iuv);                                                                                                                       //Natural: ASSIGN #PARM-IUV := #IUV
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
