/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:35 AM
**        * FROM NATURAL SUBPROGRAM : Iaanhp02
************************************************************
**        * FILE NAME            : Iaanhp02.java
**        * CLASS NAME           : Iaanhp02
**        * INSTANCE NAME        : Iaanhp02
************************************************************
***********************************************************************
* PROGRAM  : IAANHP02
* SYSTEM   : IA
* TITLE    : CALLED BY IADP170 QTRLY MODULE TO READ TIAA-HISTORY
* CREATED  : JUL 12, 2004
* FUNCTION : READ TIAA RATE HISTORY FOR PMT-AMT FOR MARCH FOR QTRLY
*            REPORTING
*
* CHANGED HISTORY
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaanhp02 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Optn_Cde;
    private DbsField pnd_Issue_Dte;

    private DbsGroup pnd_Issue_Dte__R_Field_1;
    private DbsField pnd_Issue_Dte__Filler1;
    private DbsField pnd_Issue_Dte_Pnd_Issue_Mm;
    private DbsField pnd_Wtrans_Dte;
    private DbsField pnd_Pymnt_Amt;

    private DataAccessProgramView vw_hp;
    private DbsField hp_Fund_Lst_Pd_Dte;
    private DbsField hp_Fund_Invrse_Lst_Pd_Dte;
    private DbsField hp_Cntrct_Ppcn_Nbr;
    private DbsField hp_Cntrct_Payee_Cde;
    private DbsField hp_Cmpny_Fund_Cde;
    private DbsField hp_Cntrct_Tot_Per_Amt;
    private DbsField hp_Cntrct_Tot_Div_Amt;
    private DbsField hp_Rcrd_Srce;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_2;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Payee_Cde;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde;
    private DbsField pnd_W_Dte;
    private DbsField pnd_Iss_Dte;
    private DbsField pnd_Pd_Dte;

    private DbsGroup pnd_Pd_Dte__R_Field_3;
    private DbsField pnd_Pd_Dte_Pnd_Pd_Yyyy;
    private DbsField pnd_Pd_Dte_Pnd_Pd_Mm;
    private DbsField pnd_C_Pd_Dte;

    private DbsGroup pnd_C_Pd_Dte__R_Field_4;
    private DbsField pnd_C_Pd_Dte_Pnd_C_Pd_Yyyy;
    private DbsField pnd_C_Pd_Dte_Pnd_C_Pd_Mm;
    private DbsField pnd_Reval_Dte;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Ppcn_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Payee_Cde = parameters.newFieldInRecord("pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Optn_Cde = parameters.newFieldInRecord("pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Optn_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Issue_Dte = parameters.newFieldInRecord("pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Issue_Dte.setParameterOption(ParameterOption.ByReference);

        pnd_Issue_Dte__R_Field_1 = parameters.newGroupInRecord("pnd_Issue_Dte__R_Field_1", "REDEFINE", pnd_Issue_Dte);
        pnd_Issue_Dte__Filler1 = pnd_Issue_Dte__R_Field_1.newFieldInGroup("pnd_Issue_Dte__Filler1", "_FILLER1", FieldType.STRING, 4);
        pnd_Issue_Dte_Pnd_Issue_Mm = pnd_Issue_Dte__R_Field_1.newFieldInGroup("pnd_Issue_Dte_Pnd_Issue_Mm", "#ISSUE-MM", FieldType.STRING, 2);
        pnd_Wtrans_Dte = parameters.newFieldInRecord("pnd_Wtrans_Dte", "#WTRANS-DTE", FieldType.STRING, 6);
        pnd_Wtrans_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Pymnt_Amt = parameters.newFieldInRecord("pnd_Pymnt_Amt", "#PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Pymnt_Amt.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_hp = new DataAccessProgramView(new NameInfo("vw_hp", "HP"), "IAA_OLD_PAYMENT", "IA_OLD_RATES");
        hp_Fund_Lst_Pd_Dte = vw_hp.getRecord().newFieldInGroup("hp_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        hp_Fund_Invrse_Lst_Pd_Dte = vw_hp.getRecord().newFieldInGroup("hp_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "FUND_INVRSE_LST_PD_DTE");
        hp_Cntrct_Ppcn_Nbr = vw_hp.getRecord().newFieldInGroup("hp_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        hp_Cntrct_Payee_Cde = vw_hp.getRecord().newFieldInGroup("hp_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        hp_Cmpny_Fund_Cde = vw_hp.getRecord().newFieldInGroup("hp_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CMPNY_FUND_CDE");
        hp_Cntrct_Tot_Per_Amt = vw_hp.getRecord().newFieldInGroup("hp_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TOT_PER_AMT");
        hp_Cntrct_Tot_Div_Amt = vw_hp.getRecord().newFieldInGroup("hp_Cntrct_Tot_Div_Amt", "CNTRCT-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TOT_DIV_AMT");
        hp_Rcrd_Srce = vw_hp.getRecord().newFieldInGroup("hp_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RCRD_SRCE");
        registerRecord(vw_hp);

        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Payee_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Invrse_Lst_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Invrse_Lst_Pd_Dte", 
            "#INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 
            3);
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.STRING, 6);
        pnd_Iss_Dte = localVariables.newFieldInRecord("pnd_Iss_Dte", "#ISS-DTE", FieldType.STRING, 6);
        pnd_Pd_Dte = localVariables.newFieldInRecord("pnd_Pd_Dte", "#PD-DTE", FieldType.STRING, 6);

        pnd_Pd_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Pd_Dte__R_Field_3", "REDEFINE", pnd_Pd_Dte);
        pnd_Pd_Dte_Pnd_Pd_Yyyy = pnd_Pd_Dte__R_Field_3.newFieldInGroup("pnd_Pd_Dte_Pnd_Pd_Yyyy", "#PD-YYYY", FieldType.NUMERIC, 4);
        pnd_Pd_Dte_Pnd_Pd_Mm = pnd_Pd_Dte__R_Field_3.newFieldInGroup("pnd_Pd_Dte_Pnd_Pd_Mm", "#PD-MM", FieldType.STRING, 2);
        pnd_C_Pd_Dte = localVariables.newFieldInRecord("pnd_C_Pd_Dte", "#C-PD-DTE", FieldType.STRING, 6);

        pnd_C_Pd_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_C_Pd_Dte__R_Field_4", "REDEFINE", pnd_C_Pd_Dte);
        pnd_C_Pd_Dte_Pnd_C_Pd_Yyyy = pnd_C_Pd_Dte__R_Field_4.newFieldInGroup("pnd_C_Pd_Dte_Pnd_C_Pd_Yyyy", "#C-PD-YYYY", FieldType.NUMERIC, 4);
        pnd_C_Pd_Dte_Pnd_C_Pd_Mm = pnd_C_Pd_Dte__R_Field_4.newFieldInGroup("pnd_C_Pd_Dte_Pnd_C_Pd_Mm", "#C-PD-MM", FieldType.STRING, 2);
        pnd_Reval_Dte = localVariables.newFieldInRecord("pnd_Reval_Dte", "#REVAL-DTE", FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_hp.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaanhp02() throws Exception
    {
        super("Iaanhp02");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Cntrct_Py_Dte_Key_Pnd_Ppcn_Nbr.setValue(pnd_Cntrct_Ppcn_Nbr);                                                                                                 //Natural: ASSIGN #PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Cntrct_Py_Dte_Key_Pnd_Payee_Cde.setValue(pnd_Cntrct_Payee_Cde);                                                                                               //Natural: ASSIGN #PAYEE-CDE := #CNTRCT-PAYEE-CDE
        pnd_Cntrct_Py_Dte_Key_Pnd_Invrse_Lst_Pd_Dte.setValue(0);                                                                                                          //Natural: ASSIGN #INVRSE-LST-PD-DTE := 0
        pnd_Pd_Dte.setValueEdited(pnd_Issue_Dte,new ReportEditMask("999999"));                                                                                            //Natural: MOVE EDITED #ISSUE-DTE ( EM = 999999 ) TO #PD-DTE
        pnd_Pymnt_Amt.setValue(0);                                                                                                                                        //Natural: ASSIGN #PYMNT-AMT := 0
        vw_hp.startDatabaseRead                                                                                                                                           //Natural: READ HP BY CNTRCT-PY-DTE-KEY STARTING FROM #CNTRCT-PY-DTE-KEY
        (
        "READ01",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_Cntrct_Py_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_hp.readNextRow("READ01")))
        {
            if (condition(hp_Cntrct_Ppcn_Nbr.notEquals(pnd_Cntrct_Ppcn_Nbr) || hp_Cntrct_Payee_Cde.notEquals(pnd_Cntrct_Payee_Cde)))                                      //Natural: IF CNTRCT-PPCN-NBR NE #CNTRCT-PPCN-NBR OR CNTRCT-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Reval_Dte.setValueEdited(hp_Fund_Lst_Pd_Dte,new ReportEditMask("YYYYMM"));                                                                                //Natural: MOVE EDITED FUND-LST-PD-DTE ( EM = YYYYMM ) TO #REVAL-DTE
            if (condition(DbsUtil.maskMatches(hp_Cmpny_Fund_Cde,"'T'")))                                                                                                  //Natural: IF CMPNY-FUND-CDE = MASK ( 'T' )
            {
                pnd_C_Pd_Dte.reset();                                                                                                                                     //Natural: RESET #C-PD-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_C_Pd_Dte.setValueEdited(pnd_Issue_Dte,new ReportEditMask("999999"));                                                                                  //Natural: MOVE EDITED #ISSUE-DTE ( EM = 999999 ) TO #C-PD-DTE
                if (condition(pnd_Issue_Dte_Pnd_Issue_Mm.greaterOrEqual("04")))                                                                                           //Natural: IF #ISSUE-MM GE '04'
                {
                    pnd_C_Pd_Dte_Pnd_C_Pd_Yyyy.nadd(1);                                                                                                                   //Natural: ADD 1 TO #C-PD-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ACCEPT IF #REVAL-DTE = #PD-DTE OR = #C-PD-DTE
            if (condition(!(pnd_Reval_Dte.equals(pnd_Wtrans_Dte))))                                                                                                       //Natural: ACCEPT IF #REVAL-DTE = #WTRANS-DTE
            {
                continue;
            }
            if (condition(DbsUtil.maskMatches(hp_Cmpny_Fund_Cde,"'U'") || DbsUtil.maskMatches(hp_Cmpny_Fund_Cde,"'W'") || DbsUtil.maskMatches(hp_Cmpny_Fund_Cde,"'2'")    //Natural: IF CMPNY-FUND-CDE = MASK ( 'U' ) OR = MASK ( 'W' ) OR = MASK ( '2' ) OR = MASK ( '4' )
                || DbsUtil.maskMatches(hp_Cmpny_Fund_Cde,"'4'")))
            {
                pnd_Pymnt_Amt.nadd(hp_Cntrct_Tot_Per_Amt);                                                                                                                //Natural: ADD CNTRCT-TOT-PER-AMT TO #PYMNT-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Amt.nadd(hp_Cntrct_Tot_Per_Amt);                                                                                                                //Natural: ADD CNTRCT-TOT-PER-AMT TO #PYMNT-AMT
                pnd_Pymnt_Amt.nadd(hp_Cntrct_Tot_Div_Amt);                                                                                                                //Natural: ADD CNTRCT-TOT-DIV-AMT TO #PYMNT-AMT
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
