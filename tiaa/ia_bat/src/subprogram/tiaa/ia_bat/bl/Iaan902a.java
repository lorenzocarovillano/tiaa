/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:55 AM
**        * FROM NATURAL SUBPROGRAM : Iaan902a
************************************************************
**        * FILE NAME            : Iaan902a.java
**        * CLASS NAME           : Iaan902a
**        * INSTANCE NAME        : Iaan902a
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN902A CREATES MISC NON TAX TRANS CODING SHEETS *
*      DATE     -  8/94                                              *
*                                                                    *
**********************************************************************
* CHANGE HISTORY
* CHANGED ON APRIL 10, 1995 BY D ROBINSON
* >   CORRECTED TRANSACTION HISTORY (BEFORE IMAGE) ACCESS
* CHANGED ON APRIL 21, 1995 BY D ROBINSON
* >   ENHANCED FOR DEATH CLAIMS PROCESSING
* CHANGED ON MAY 10, 1995 BY D ROBINSON
* >   CORRECTED REVERSAL OF DEATH TRANSACTION PROCESSING (037)
* CHANGED ON JUNE 13, 1995 BY D ROBINSON
* >   CORRECTED PROCESSING OF XREF
* CHANGED ON AUGUST 9, 1995 BY D ROBINSON
* >   CHANGED TO READ BEFORE/AFT IMAGES OF CONTRACT TRANSACTION
* >   PROCESSING FOR DEATH PROCESSING.
* CHANGED ON SEPT 1, 1995  BY D ROBINSON
* >   CHANGED TO PROCESS CONTRACTUAL CORRECTIONS AND SUSPEND PAYMENT
* >   TRANSACTIONS DIFFERENTLY IF CONTRACT HAS REVERSAL
* CHANGED ON SEPT 14, 1995  BY D ROBINSON
* >   CHANGED TO PROCESS TERMINATION TRANSACTIONS DIFFERENTLY IF
* >   CONTRACT HAS REVERSAL
* >
* 04/2017 OS RE-STOWED FOR IAAL902A PIN EXPANSION.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan902a extends BLNatBase
{
    // Data Areas
    private LdaIaal902a ldaIaal902a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Passed_Data;
    private DbsField pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Passed_Data__R_Field_1;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8;
    private DbsField pnd_Passed_Data_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Sub_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_2;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_3;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Area;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Id;
    private DbsField pnd_Passed_Data_Pnd_Trans_Verify_Id;
    private DbsField pnd_Passed_Data_Pnd_Last_Batch_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Reason;
    private DbsField pnd_Passed_Data_Pnd_Records_Bypassed_Ctr;
    private DbsField pnd_Passed_Data_Pnd_Records_Processed_Ctr;
    private DbsField pnd_Passed_Data_Pnd_Save_33_Ppcn_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Save_33_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Save_37_Payee_Cde;

    private DbsGroup iaa_Cntrct_B;
    private DbsField iaa_Cntrct_B_Trans_Dte;
    private DbsField iaa_Cntrct_B_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_B_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_B_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_B_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_B_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_B_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Dod_Dte;

    private DbsGroup iaa_Cntrct_B__R_Field_4;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_B_Cntrct_First_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Dte;

    private DbsGroup iaa_Cntrct_B__R_Field_5;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_B_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_B_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_B_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_B_Trans_Check_Dte;
    private DbsField iaa_Cntrct_B_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_B_Aftr_Imge_Id;

    private DbsGroup iaa_Cntrct_A;
    private DbsField iaa_Cntrct_A_Trans_Dte;
    private DbsField iaa_Cntrct_A_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_A_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_A_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_A_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_A_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_A_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Dod_Dte;

    private DbsGroup iaa_Cntrct_A__R_Field_6;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_A_Cntrct_First_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Dte;

    private DbsGroup iaa_Cntrct_A__R_Field_7;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_A_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_A_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_A_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_A_Trans_Check_Dte;
    private DbsField iaa_Cntrct_A_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_A_Aftr_Imge_Id;
    private DbsField pnd_Intent_L;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal902a = new LdaIaal902a();
        registerRecord(ldaIaal902a);
        registerRecord(ldaIaal902a.getVw_iaa_Cntrct());
        registerRecord(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal902a.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal902a.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();

        pnd_Passed_Data = parameters.newGroupInRecord("pnd_Passed_Data", "#PASSED-DATA");
        pnd_Passed_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte", "#CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Passed_Data__R_Field_1 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_1", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8 = pnd_Passed_Data__R_Field_1.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8", "#TRANS-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Passed_Data_Pnd_Trans_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Passed_Data_Pnd_Trans_Sub_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Sub_Cde", "#TRANS-SUB-CDE", FieldType.STRING, 3);
        pnd_Passed_Data_Pnd_Trans_Check_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_2 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_2", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Check_Dte);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc", "#TRANS-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy", "#TRANS-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm", "#TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd", "#TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_3 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_3", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Effctve_Dte);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc", "#TRANS-EFFCTVE-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy", "#TRANS-EFFCTVE-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm", "#TRANS-EFFCTVE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd", "#TRANS-EFFCTVE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_User_Area = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Area", "#TRANS-USER-AREA", FieldType.STRING, 
            6);
        pnd_Passed_Data_Pnd_Trans_User_Id = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Id", "#TRANS-USER-ID", FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Verify_Id = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Verify_Id", "#TRANS-VERIFY-ID", FieldType.STRING, 
            8);
        pnd_Passed_Data_Pnd_Last_Batch_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Passed_Data_Pnd_Reason = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Reason", "#REASON", FieldType.STRING, 2);
        pnd_Passed_Data_Pnd_Records_Bypassed_Ctr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Records_Bypassed_Ctr", "#RECORDS-BYPASSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Passed_Data_Pnd_Records_Processed_Ctr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Records_Processed_Ctr", "#RECORDS-PROCESSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Passed_Data_Pnd_Save_33_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_33_Ppcn_Nbr", "#SAVE-33-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Passed_Data_Pnd_Save_33_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_33_Payee_Cde", "#SAVE-33-PAYEE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr", "#SAVE-37-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Passed_Data_Pnd_Save_37_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_37_Payee_Cde", "#SAVE-37-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        iaa_Cntrct_B = localVariables.newGroupInRecord("iaa_Cntrct_B", "IAA-CNTRCT-B");
        iaa_Cntrct_B_Trans_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Trans_Dte", "TRANS-DTE", FieldType.TIME);
        iaa_Cntrct_B_Invrse_Trans_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        iaa_Cntrct_B_Lst_Trans_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME);
        iaa_Cntrct_B_Cntrct_Ppcn_Nbr = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        iaa_Cntrct_B_Cntrct_Optn_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_Orgn_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_Acctng_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 2);
        iaa_Cntrct_B_Cntrct_Issue_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6);
        iaa_Cntrct_B_Cntrct_First_Pymnt_Due_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        iaa_Cntrct_B_Cntrct_First_Pymnt_Pd_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 
            6);
        iaa_Cntrct_B_Cntrct_Crrncy_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 1);
        iaa_Cntrct_B_Cntrct_Type_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1);
        iaa_Cntrct_B_Cntrct_Pymnt_Mthd = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 1);
        iaa_Cntrct_B_Cntrct_Pnsn_Pln_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 1);
        iaa_Cntrct_B_Cntrct_Joint_Cnvrt_Rcrd_Ind = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1);
        iaa_Cntrct_B_Cntrct_Orig_Da_Cntrct_Nbr = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        iaa_Cntrct_B_Cntrct_Rsdncy_At_Issue_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3);
        iaa_Cntrct_B_Cntrct_First_Annt_Xref_Ind = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        iaa_Cntrct_B_Cntrct_First_Annt_Dob_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8);
        iaa_Cntrct_B_Cntrct_First_Annt_Mrtlty_Yob_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        iaa_Cntrct_B_Cntrct_First_Annt_Sex_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_B_Cntrct_First_Annt_Lfe_Cnt = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_B_Cntrct_First_Annt_Dod_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6);

        iaa_Cntrct_B__R_Field_4 = iaa_Cntrct_B.newGroupInGroup("iaa_Cntrct_B__R_Field_4", "REDEFINE", iaa_Cntrct_B_Cntrct_First_Annt_Dod_Dte);
        iaa_Cntrct_B_Cntrct_First_Annt_Dod_Cc = iaa_Cntrct_B__R_Field_4.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Dod_Cc", "CNTRCT-FIRST-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_First_Annt_Dod_Yy = iaa_Cntrct_B__R_Field_4.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Dod_Yy", "CNTRCT-FIRST-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_First_Annt_Dod_Mm = iaa_Cntrct_B__R_Field_4.newFieldInGroup("iaa_Cntrct_B_Cntrct_First_Annt_Dod_Mm", "CNTRCT-FIRST-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Xref_Ind = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", FieldType.STRING, 
            9);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Dob_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Sex_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6);

        iaa_Cntrct_B__R_Field_5 = iaa_Cntrct_B.newGroupInGroup("iaa_Cntrct_B__R_Field_5", "REDEFINE", iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Dte);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Cc = iaa_Cntrct_B__R_Field_5.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Cc", "CNTRCT-SCND-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Yy = iaa_Cntrct_B__R_Field_5.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Yy", "CNTRCT-SCND-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Mm = iaa_Cntrct_B__R_Field_5.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Mm", "CNTRCT-SCND-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Life_Cnt = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_B_Cntrct_Scnd_Annt_Ssn = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        iaa_Cntrct_B_Cntrct_Div_Payee_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_B_Cntrct_Div_Coll_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 5);
        iaa_Cntrct_B_Cntrct_Inst_Iss_Cde = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 5);
        iaa_Cntrct_B_Trans_Check_Dte = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8);
        iaa_Cntrct_B_Bfre_Imge_Id = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1);
        iaa_Cntrct_B_Aftr_Imge_Id = iaa_Cntrct_B.newFieldInGroup("iaa_Cntrct_B_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1);

        iaa_Cntrct_A = localVariables.newGroupInRecord("iaa_Cntrct_A", "IAA-CNTRCT-A");
        iaa_Cntrct_A_Trans_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Trans_Dte", "TRANS-DTE", FieldType.TIME);
        iaa_Cntrct_A_Invrse_Trans_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        iaa_Cntrct_A_Lst_Trans_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME);
        iaa_Cntrct_A_Cntrct_Ppcn_Nbr = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        iaa_Cntrct_A_Cntrct_Optn_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_Orgn_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_Acctng_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 2);
        iaa_Cntrct_A_Cntrct_Issue_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6);
        iaa_Cntrct_A_Cntrct_First_Pymnt_Due_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        iaa_Cntrct_A_Cntrct_First_Pymnt_Pd_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 
            6);
        iaa_Cntrct_A_Cntrct_Crrncy_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 1);
        iaa_Cntrct_A_Cntrct_Type_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1);
        iaa_Cntrct_A_Cntrct_Pymnt_Mthd = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 1);
        iaa_Cntrct_A_Cntrct_Pnsn_Pln_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 1);
        iaa_Cntrct_A_Cntrct_Joint_Cnvrt_Rcrd_Ind = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1);
        iaa_Cntrct_A_Cntrct_Orig_Da_Cntrct_Nbr = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        iaa_Cntrct_A_Cntrct_Rsdncy_At_Issue_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3);
        iaa_Cntrct_A_Cntrct_First_Annt_Xref_Ind = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        iaa_Cntrct_A_Cntrct_First_Annt_Dob_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8);
        iaa_Cntrct_A_Cntrct_First_Annt_Mrtlty_Yob_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        iaa_Cntrct_A_Cntrct_First_Annt_Sex_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_A_Cntrct_First_Annt_Lfe_Cnt = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_A_Cntrct_First_Annt_Dod_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6);

        iaa_Cntrct_A__R_Field_6 = iaa_Cntrct_A.newGroupInGroup("iaa_Cntrct_A__R_Field_6", "REDEFINE", iaa_Cntrct_A_Cntrct_First_Annt_Dod_Dte);
        iaa_Cntrct_A_Cntrct_First_Annt_Dod_Cc = iaa_Cntrct_A__R_Field_6.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Dod_Cc", "CNTRCT-FIRST-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_First_Annt_Dod_Yy = iaa_Cntrct_A__R_Field_6.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Dod_Yy", "CNTRCT-FIRST-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_First_Annt_Dod_Mm = iaa_Cntrct_A__R_Field_6.newFieldInGroup("iaa_Cntrct_A_Cntrct_First_Annt_Dod_Mm", "CNTRCT-FIRST-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Xref_Ind = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", FieldType.STRING, 
            9);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Dob_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Sex_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6);

        iaa_Cntrct_A__R_Field_7 = iaa_Cntrct_A.newGroupInGroup("iaa_Cntrct_A__R_Field_7", "REDEFINE", iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Dte);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Cc = iaa_Cntrct_A__R_Field_7.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Cc", "CNTRCT-SCND-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Yy = iaa_Cntrct_A__R_Field_7.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Yy", "CNTRCT-SCND-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Mm = iaa_Cntrct_A__R_Field_7.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Mm", "CNTRCT-SCND-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Life_Cnt = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_A_Cntrct_Scnd_Annt_Ssn = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        iaa_Cntrct_A_Cntrct_Div_Payee_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_A_Cntrct_Div_Coll_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 5);
        iaa_Cntrct_A_Cntrct_Inst_Iss_Cde = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 5);
        iaa_Cntrct_A_Trans_Check_Dte = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8);
        iaa_Cntrct_A_Bfre_Imge_Id = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1);
        iaa_Cntrct_A_Aftr_Imge_Id = iaa_Cntrct_A.newFieldInGroup("iaa_Cntrct_A_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1);
        pnd_Intent_L = localVariables.newFieldInRecord("pnd_Intent_L", "#INTENT-L", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal902a.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan902a() throws Exception
    {
        super("Iaan902a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: DECIDE ON FIRST VALUE OF #TRANS-CDE
        short decideConditionsMet449 = 0;                                                                                                                                 //Natural: VALUE 037
        if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(37))))
        {
            decideConditionsMet449++;
                                                                                                                                                                          //Natural: PERFORM GET-ANNT-INFO
            sub_Get_Annt_Info();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-037
            sub_Determine_Intent_Cde_037();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals(" ")))                                                                           //Natural: IF #INTENT-CODE = ' '
            {
                pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                                //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                            //Natural: ASSIGN #SAVE-37-PPCN-NBR := #TRANS-PPCN-NBR
            pnd_Passed_Data_Pnd_Save_37_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                          //Natural: ASSIGN #SAVE-37-PAYEE-CDE := #TRANS-PAYEE-CDE
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals("1")))                                                                           //Natural: IF #INTENT-CODE = '1'
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-SEX-A := ' '
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-DOB-A := ' '
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-DOD-A := ' '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals("2")))                                                                           //Natural: IF #INTENT-CODE = '2'
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-SEX-A := ' '
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-DOB-A := ' '
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-DOD-A := ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 064
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(64))))
        {
            decideConditionsMet449++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-064-066-070
            sub_Determine_Intent_Cde_064_066_070();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals(" ")))                                                                           //Natural: IF #INTENT-CODE = ' '
            {
                pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                                //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 066
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(66))))
        {
            decideConditionsMet449++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-064-066-070
            sub_Determine_Intent_Cde_064_066_070();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals(" ")))                                                                           //Natural: IF #INTENT-CODE = ' '
            {
                pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                                //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 070
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(70))))
        {
            decideConditionsMet449++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-064-066-070
            sub_Determine_Intent_Cde_064_066_070();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals(" ")))                                                                           //Natural: IF #INTENT-CODE = ' '
            {
                pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                                //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(102))))
        {
            decideConditionsMet449++;
            if (condition(pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr.notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                            //Natural: IF #SAVE-37-PPCN-NBR NE #TRANS-PPCN-NBR
            {
                pnd_Passed_Data_Pnd_Save_37_Payee_Cde.reset();                                                                                                            //Natural: RESET #SAVE-37-PAYEE-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.equals("066") || pnd_Passed_Data_Pnd_Trans_Sub_Cde.equals("66U")))                                            //Natural: IF #TRANS-SUB-CDE = '066' OR = '66U'
            {
                if (condition(pnd_Passed_Data_Pnd_Save_33_Ppcn_Nbr.equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && pnd_Passed_Data_Pnd_Save_33_Payee_Cde.equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF #SAVE-33-PPCN-NBR = #TRANS-PPCN-NBR AND #SAVE-33-PAYEE-CDE = #TRANS-PAYEE-CDE
                {
                    pnd_Passed_Data_Pnd_Reason.setValue("M ");                                                                                                            //Natural: ASSIGN #REASON := 'M '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.equals("066")))                                                                                               //Natural: IF #TRANS-SUB-CDE = '066'
            {
                ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                        //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                      //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
                (
                "FIND01",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
                1
                );
                FIND01:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND01", true)))
                {
                    ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        getReports().write(0, "NO CONTRACT PARTICIPANT RECORD FOUND FOR: ",ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                  //Natural: WRITE 'NO CONTRACT PARTICIPANT RECORD FOUND FOR: ' #IAA-CNTRCT-PRTCPNT-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-NOREC
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
                if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().equals("0")))                                                                      //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE = '0'
                {
                    pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                            //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                //Natural: ASSIGN #INTENT-CODE := '1'
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde().setValue(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde());                     //Natural: ASSIGN #SUS-PYMNT-RSN-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.equals("66U")))                                                                                               //Natural: IF #TRANS-SUB-CDE = '66U'
            {
                ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                         //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                  //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                       //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                       //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
                (
                "READ01",
                new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
                1
                );
                READ01:
                while (condition(ldaIaal902a.getVw_iaa_Cpr_Trans().readNextRow("READ01")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Pend_Dte_Mm().equals(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm()) && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Pend_Dte_Yy().equals(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy()))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PEND-DTE-MM = #CHECK-DTE-MM AND IAA-CPR-TRANS.CNTRCT-PEND-DTE-YY = #CHECK-DTE-YY
                {
                    pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                            //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                //Natural: ASSIGN #INTENT-CODE := '3'
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde().setValue(" ");                                                                          //Natural: ASSIGN #SUS-PYMNT-RSN-CDE := ' '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.notEquals("066") && pnd_Passed_Data_Pnd_Trans_Sub_Cde.notEquals("66U")))                                      //Natural: IF #TRANS-SUB-CDE NE '066' AND #TRANS-SUB-CDE NE '66U'
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-102
                sub_Determine_Intent_Cde_102();
                if (condition(Global.isEscape())) {return;}
                if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals(" ")))                                                                       //Natural: IF #INTENT-CODE = ' '
                {
                    pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                            //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals("3")))                                                                       //Natural: IF #INTENT-CODE = '3'
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde().setValue(" ");                                                                          //Natural: ASSIGN #SUS-PYMNT-RSN-CDE := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde().setValue(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde());                     //Natural: ASSIGN #SUS-PYMNT-RSN-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 104
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(104))))
        {
            decideConditionsMet449++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-104
            sub_Determine_Intent_Cde_104();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals(" ")))                                                                           //Natural: IF #INTENT-CODE = ' '
            {
                pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                                //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals("3")))                                                                           //Natural: IF #INTENT-CODE = '3'
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde().setValue(" ");                                                                             //Natural: ASSIGN #HOLD-CHECK-RSN-CDE := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde().setValue(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde());                        //Natural: ASSIGN #HOLD-CHECK-RSN-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(106))))
        {
            decideConditionsMet449++;
            ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                            //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                          //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
            (
            "FIND02",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
            1
            );
            FIND02:
            while (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
            {
                ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res().setValue(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde());                        //Natural: ASSIGN #STATE-CNTRY-RES := PRTCPNT-RSDNCY-CDE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Passed_Data_Pnd_Records_Processed_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        getWorkFiles().write(1, false, ldaIaal902a.getPnd_Misc_Non_Tax_Trans());                                                                                          //Natural: WRITE WORK FILE 1 #MISC-NON-TAX-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-037
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-064-066-070
        //*  #IAA-CNTRCT-TRANS-KEY.#TRANS-DTE := #PASSED-DATA.#CNTRL-FRST-TRANS-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-102
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-104
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ANNT-INFO
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASS-TRANSACTION
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902a.getPnd_Misc_Non_Tax_Trans().reset();                                                                                                                  //Natural: RESET #MISC-NON-TAX-TRANS #NO-CNTRCT-REC
        ldaIaal902a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().reset();
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr().compute(new ComputeParameters(false, ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr()),                //Natural: COMPUTE #BATCH-NBR = #LAST-BATCH-NBR + 1
            pnd_Passed_Data_Pnd_Last_Batch_Nbr.add(1));
        pnd_Passed_Data_Pnd_Last_Batch_Nbr.setValue(ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr());                                                               //Natural: ASSIGN #LAST-BATCH-NBR := #BATCH-NBR
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm);                                                        //Natural: ASSIGN #CHECK-DTE-MM := #TRANS-CHECK-DTE-MM
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd);                                                        //Natural: ASSIGN #CHECK-DTE-DD := #TRANS-CHECK-DTE-DD
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy);                                                        //Natural: ASSIGN #CHECK-DTE-YY := #TRANS-CHECK-DTE-YY
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8);                                                            //Natural: ASSIGN #CNTRCT-NBR := #TRANS-PPCN-NBR-8
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Record_Status().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                          //Natural: ASSIGN #RECORD-STATUS := #TRANS-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT
        sub_Find_Iaa_Cntrct();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR
        sub_Get_Cross_Reference_Nbr();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                    //Natural: ASSIGN #TRANS-NBR := #TRANS-CDE
        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_User_Area().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                              //Natural: ASSIGN #USER-AREA := #TRANS-USER-AREA
    }
    private void sub_Find_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: FIND-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902a.getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                             //Natural: ASSIGN #IAA-CNTRCT-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND03:
        while (condition(ldaIaal902a.getVw_iaa_Cntrct().readNextRow("FIND03", true)))
        {
            ldaIaal902a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA CONTRACT RECORD FOR : ",pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                //Natural: WRITE 'NO IAA CONTRACT RECORD FOR : ' #TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal902a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                  //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cross_Reference_Nbr() throws Exception                                                                                                           //Natural: GET-CROSS-REFERENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet639 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(1))))
        {
            decideConditionsMet639++;
            if (condition(ldaIaal902a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ02",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ02:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ02")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && pnd_Passed_Data_Pnd_Trans_Cde.lessOrEqual(50))) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR AND #TRANS-CDE <= 50
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                     //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                           //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(2))))
        {
            decideConditionsMet639++;
            if (condition(ldaIaal902a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ03",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ03:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ03")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && pnd_Passed_Data_Pnd_Trans_Cde.lessOrEqual(50))) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR AND #TRANS-CDE <= 50
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                     //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                           //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ04",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ04:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ04")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && pnd_Passed_Data_Pnd_Trans_Cde.lessOrEqual(50))) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR AND #TRANS-CDE <= 50
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                      //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                            //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ05",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ05:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ05")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && pnd_Passed_Data_Pnd_Trans_Cde.lessOrEqual(50)))   //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR AND #TRANS-CDE <= 50
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                     //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                           //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ06",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ06:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ06")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && pnd_Passed_Data_Pnd_Trans_Cde.lessOrEqual(50)))   //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR AND #TRANS-CDE <= 50
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                      //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                            //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((pnd_Passed_Data_Pnd_Trans_Payee_Cde.greaterOrEqual(3) && pnd_Passed_Data_Pnd_Trans_Payee_Cde.lessOrEqual(99)))))
        {
            decideConditionsMet639++;
            if (condition((pnd_Passed_Data_Pnd_Trans_Cde.greater(50)) || (pnd_Passed_Data_Pnd_Save_33_Ppcn_Nbr.equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)                 //Natural: IF ( #TRANS-CDE > 50 ) OR ( #SAVE-33-PPCN-NBR = #TRANS-PPCN-NBR AND #SAVE-33-PAYEE-CDE = #TRANS-PAYEE-CDE )
                && pnd_Passed_Data_Pnd_Save_33_Payee_Cde.equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))))
            {
                ldaIaal902a.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                   //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                            //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
                (
                "FIND04",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902a.getPnd_Cntrct_Payee_Key(), WcType.WITH) },
                1
                );
                FIND04:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND04", true)))
                {
                    ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := BNFCRY-XREF-IND
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                             //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGE-ID := '1'
            ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                      //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal902a.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                           //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
            ldaIaal902a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
            (
            "READ07",
            new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
            new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
            1
            );
            READ07:
            while (condition(ldaIaal902a.getVw_iaa_Cpr_Trans().readNextRow("READ07")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR = #TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = #TRANS-PAYEE-CDE
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cpr_Trans_Bnfcry_Xref_Ind());                                       //Natural: ASSIGN #CROSS-REF-NBR := IAA-CPR-TRANS.BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                   //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902a.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                            //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
                (
                "FIND05",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902a.getPnd_Cntrct_Payee_Key(), WcType.WITH) },
                1
                );
                FIND05:
                while (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND05", true)))
                {
                    ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := BNFCRY-XREF-IND
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Intent_Cde_037() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-037
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                              //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Trans_Dte);                                                                       //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #PASSED-DATA.#TRANS-DTE
        ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
        (
        "READ08",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902a.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        READ08:
        while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ08")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        iaa_Cntrct_B.setValuesByName(ldaIaal902a.getVw_iaa_Cntrct_Trans());                                                                                               //Natural: MOVE BY NAME IAA-CNTRCT-TRANS TO IAA-CNTRCT-B
        ldaIaal902a.getPnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                              //Natural: ASSIGN #CNTRCT-AFTR-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal902a.getPnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(iaa_Cntrct_B_Invrse_Trans_Dte);                                                                //Natural: ASSIGN #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE := IAA-CNTRCT-B.INVRSE-TRANS-DTE
        ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #CNTRCT-AFTR-KEY
        (
        "READ09",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", ldaIaal902a.getPnd_Cntrct_Aftr_Key(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
        1
        );
        READ09:
        while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ09")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        iaa_Cntrct_A.setValuesByName(ldaIaal902a.getVw_iaa_Cntrct_Trans());                                                                                               //Natural: MOVE BY NAME IAA-CNTRCT-TRANS TO IAA-CNTRCT-A
        pnd_Intent_L.setValue(false);                                                                                                                                     //Natural: ASSIGN #INTENT-L := FALSE
        if (condition(iaa_Cntrct_B_Cntrct_First_Annt_Dob_Dte.equals(iaa_Cntrct_A_Cntrct_First_Annt_Dob_Dte) && iaa_Cntrct_B_Cntrct_First_Annt_Sex_Cde.equals(iaa_Cntrct_A_Cntrct_First_Annt_Sex_Cde)  //Natural: IF IAA-CNTRCT-B.CNTRCT-FIRST-ANNT-DOB-DTE = IAA-CNTRCT-A.CNTRCT-FIRST-ANNT-DOB-DTE AND IAA-CNTRCT-B.CNTRCT-FIRST-ANNT-SEX-CDE = IAA-CNTRCT-A.CNTRCT-FIRST-ANNT-SEX-CDE AND IAA-CNTRCT-A.CNTRCT-FIRST-ANNT-DOD-DTE NE IAA-CNTRCT-B.CNTRCT-FIRST-ANNT-DOD-DTE
            && iaa_Cntrct_A_Cntrct_First_Annt_Dod_Dte.notEquals(iaa_Cntrct_B_Cntrct_First_Annt_Dod_Dte)))
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm().setValue(iaa_Cntrct_A_Cntrct_First_Annt_Dod_Mm);                                                  //Natural: ASSIGN #1ST-ANNT-DOD-MM := IAA-CNTRCT-A.CNTRCT-FIRST-ANNT-DOD-MM
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy().setValue(iaa_Cntrct_A_Cntrct_First_Annt_Dod_Yy);                                                  //Natural: ASSIGN #1ST-ANNT-DOD-YY := IAA-CNTRCT-A.CNTRCT-FIRST-ANNT-DOD-YY
            pnd_Intent_L.setValue(true);                                                                                                                                  //Natural: ASSIGN #INTENT-L := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(iaa_Cntrct_B_Cntrct_Scnd_Annt_Dob_Dte.equals(iaa_Cntrct_A_Cntrct_Scnd_Annt_Dob_Dte) && iaa_Cntrct_B_Cntrct_Scnd_Annt_Sex_Cde.equals(iaa_Cntrct_A_Cntrct_Scnd_Annt_Sex_Cde)  //Natural: IF IAA-CNTRCT-B.CNTRCT-SCND-ANNT-DOB-DTE = IAA-CNTRCT-A.CNTRCT-SCND-ANNT-DOB-DTE AND IAA-CNTRCT-B.CNTRCT-SCND-ANNT-SEX-CDE = IAA-CNTRCT-A.CNTRCT-SCND-ANNT-SEX-CDE AND IAA-CNTRCT-B.CNTRCT-SCND-ANNT-DOD-DTE NE IAA-CNTRCT-A.CNTRCT-SCND-ANNT-DOD-DTE
            && iaa_Cntrct_B_Cntrct_Scnd_Annt_Dod_Dte.notEquals(iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Dte)))
        {
            if (condition(pnd_Intent_L.getBoolean()))                                                                                                                     //Natural: IF #INTENT-L
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                    //Natural: ASSIGN #INTENT-CODE := '3'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                    //Natural: ASSIGN #INTENT-CODE := '2'
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm().setValue(iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Mm);                                                   //Natural: ASSIGN #2ND-ANNT-DOD-MM := IAA-CNTRCT-A.CNTRCT-SCND-ANNT-DOD-MM
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy().setValue(iaa_Cntrct_A_Cntrct_Scnd_Annt_Dod_Yy);                                                   //Natural: ASSIGN #2ND-ANNT-DOD-YY := IAA-CNTRCT-A.CNTRCT-SCND-ANNT-DOD-YY
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Intent_Cde_064_066_070() throws Exception                                                                                                  //Natural: DETERMINE-INTENT-CDE-064-066-070
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902a.getPnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id().setValue("1");                                                                                         //Natural: ASSIGN #IAA-CNTRCT-TRANS-KEY.#BFRE-IMGR-ID := '1'
        ldaIaal902a.getPnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                       //Natural: ASSIGN #IAA-CNTRCT-TRANS-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Trans_Dte);                                                                  //Natural: ASSIGN #IAA-CNTRCT-TRANS-KEY.#TRANS-DTE := #PASSED-DATA.#TRANS-DTE
        ldaIaal902a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #IAA-CNTRCT-TRANS-KEY
        (
        "READ10",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902a.getPnd_Iaa_Cntrct_Trans_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        READ10:
        while (condition(ldaIaal902a.getVw_iaa_Cntrct_Trans().readNextRow("READ10")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                              //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
        (
        "FIND06",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
        1
        );
        FIND06:
        while (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND06", true)))
        {
            ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO PRTCPNT ROLE FOUND FOR : ",ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                                        //Natural: WRITE 'NO PRTCPNT ROLE FOUND FOR : ' #IAA-CNTRCT-PRTCPNT-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().greater(2)))                                                                         //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE > 02
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            ldaIaal902a.getPnd_Bnfcry_Dod().setValue(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte());                                                            //Natural: ASSIGN #BNFCRY-DOD := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-DOD-DTE
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm().setValue(ldaIaal902a.getPnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Mm());                                      //Natural: ASSIGN #1ST-ANNT-DOD-MM := #BNFCRY-DOD-MM
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy().setValue(ldaIaal902a.getPnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Yy());                                      //Natural: ASSIGN #1ST-ANNT-DOD-YY := #BNFCRY-DOD-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.equals("002")))                                                                                                   //Natural: IF #TRANS-SUB-CDE = '002'
        {
            if (condition(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero()) && ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greater(getZero()))) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE = 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GT 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                    //Natural: ASSIGN #INTENT-CODE := '2'
                if (condition(ldaIaal902a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                     //Natural: IF #NO-CNTRCT-REC
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde().equals(getZero())))                                                                //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE = 0
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A().setValue(" ");                                                                         //Natural: ASSIGN #2ND-ANNT-SEX-A := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                          //Natural: ASSIGN #2ND-ANNT-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte().equals(getZero())))                                                                //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE = 0
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A().setValue(" ");                                                                         //Natural: ASSIGN #2ND-ANNT-DOB-A := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm());                        //Natural: ASSIGN #2ND-ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-MM
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                        //Natural: ASSIGN #2ND-ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DD
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy());                        //Natural: ASSIGN #2ND-ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-YY
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A().setValue(" ");                                                                         //Natural: ASSIGN #2ND-ANNT-DOD-A := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm());                        //Natural: ASSIGN #2ND-ANNT-DOD-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-MM
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy());                        //Natural: ASSIGN #2ND-ANNT-DOD-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-YY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.equals("001")))                                                                                                   //Natural: IF #TRANS-SUB-CDE = '001'
        {
            if (condition(ldaIaal902a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().equals(getZero()) && ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero()))) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GT 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                    //Natural: ASSIGN #INTENT-CODE := '1'
                if (condition(ldaIaal902a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                     //Natural: IF #NO-CNTRCT-REC
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde().equals(getZero())))                                                               //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE = 0
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A().setValue(" ");                                                                         //Natural: ASSIGN #1ST-ANNT-SEX-A := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                         //Natural: ASSIGN #1ST-ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte().equals(getZero())))                                                               //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE = 0
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A().setValue(" ");                                                                         //Natural: ASSIGN #1ST-ANNT-DOB-A := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                       //Natural: ASSIGN #1ST-ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-MM
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                       //Natural: ASSIGN #1ST-ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DD
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy());                       //Natural: ASSIGN #1ST-ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-YY
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                               //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A().setValue(" ");                                                                         //Natural: ASSIGN #1ST-ANNT-DOD-A := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Mm());                       //Natural: ASSIGN #1ST-ANNT-DOD-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-MM
                        ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Yy());                       //Natural: ASSIGN #1ST-ANNT-DOD-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-YY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Intent_Cde_102() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-102
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                              //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
        (
        "FIND07",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
        1
        );
        FIND07:
        while (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND07", true)))
        {
            ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO PRTCPNT ROLE FOUND FOR : ",ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                                        //Natural: WRITE 'NO PRTCPNT ROLE FOUND FOR : ' #IAA-CNTRCT-PRTCPNT-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Image_Id().setValue("1");                                                                                           //Natural: ASSIGN #BFRE-IMAGE-ID := '1'
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                     //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                   //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                          //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
        ldaIaal902a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #IAA-CPR-TRANS-KEY
        (
        "READ11",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ11:
        while (condition(ldaIaal902a.getVw_iaa_Cpr_Trans().readNextRow("READ11")))
        {
            if (condition(ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR EQ #TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE EQ #TRANS-PAYEE-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "NO CPR TRANS FOUND FOR : ",ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key());                                                                //Natural: WRITE 'NO CPR TRANS FOUND FOR : ' #IAA-CPR-TRANS-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Passed_Data_Pnd_Save_37_Payee_Cde.equals(1) || pnd_Passed_Data_Pnd_Save_37_Payee_Cde.equals(2)))                                                //Natural: IF #SAVE-37-PAYEE-CDE = 1 OR = 2
        {
            ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Pend_Cde().setValue("0");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.CNTRCT-PEND-CDE := '0'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().notEquals("0") && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Pend_Cde().equals("0")))             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE NE '0' AND IAA-CPR-TRANS.CNTRCT-PEND-CDE = '0'
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().equals("0") && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Pend_Cde().notEquals("0")))             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE = '0' AND IAA-CPR-TRANS.CNTRCT-PEND-CDE NE '0'
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                        //Natural: ASSIGN #INTENT-CODE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().notEquals(ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Pend_Cde())))                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE NE IAA-CPR-TRANS.CNTRCT-PEND-CDE
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                        //Natural: ASSIGN #INTENT-CODE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Intent_Cde_104() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-104
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                              //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
        (
        "FIND08",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
        1
        );
        FIND08:
        while (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND08", true)))
        {
            ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO PRTCPNT ROLE FOUND FOR : ",ldaIaal902a.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                                        //Natural: WRITE 'NO PRTCPNT ROLE FOUND FOR : ' #IAA-CNTRCT-PRTCPNT-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Image_Id().setValue("1");                                                                                           //Natural: ASSIGN #BFRE-IMAGE-ID := '1'
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                     //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                   //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                          //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
        ldaIaal902a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #IAA-CPR-TRANS-KEY
        (
        "READ12",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ12:
        while (condition(ldaIaal902a.getVw_iaa_Cpr_Trans().readNextRow("READ12")))
        {
            if (condition(ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR EQ #TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE EQ #TRANS-PAYEE-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "NO CPR TRANS FOUND FOR : ",ldaIaal902a.getPnd_Iaa_Cpr_Trans_Key());                                                                //Natural: WRITE 'NO CPR TRANS FOUND FOR : ' #IAA-CPR-TRANS-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde().notEquals("0") && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Hold_Cde().equals("0")))             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE NE '0' AND IAA-CPR-TRANS.CNTRCT-HOLD-CDE = '0'
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde().equals("0") && ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Hold_Cde().notEquals("0")))             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE = '0' AND IAA-CPR-TRANS.CNTRCT-HOLD-CDE NE '0'
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                        //Natural: ASSIGN #INTENT-CODE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde().notEquals(ldaIaal902a.getIaa_Cpr_Trans_Cntrct_Hold_Cde())))                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE NE IAA-CPR-TRANS.CNTRCT-HOLD-CDE
        {
            ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                        //Natural: ASSIGN #INTENT-CODE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Annt_Info() throws Exception                                                                                                                     //Natural: GET-ANNT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal902a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                             //Natural: IF #NO-CNTRCT-REC
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE = 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-SEX-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                 //Natural: ASSIGN #1ST-ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE = 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-DOB-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                               //Natural: ASSIGN #1ST-ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-MM
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                               //Natural: ASSIGN #1ST-ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DD
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy());                               //Natural: ASSIGN #1ST-ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-DOD-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Mm());                               //Natural: ASSIGN #1ST-ANNT-DOD-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-MM
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Yy());                               //Natural: ASSIGN #1ST-ANNT-DOD-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE = 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-SEX-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                  //Natural: ASSIGN #2ND-ANNT-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE = 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-DOB-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm());                                //Natural: ASSIGN #2ND-ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-MM
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                                //Natural: ASSIGN #2ND-ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DD
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy());                                //Natural: ASSIGN #2ND-ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-DOD-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm());                                //Natural: ASSIGN #2ND-ANNT-DOD-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-MM
                ldaIaal902a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy().setValue(ldaIaal902a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy());                                //Natural: ASSIGN #2ND-ANNT-DOD-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-YY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bypass_Transaction() throws Exception                                                                                                                //Natural: BYPASS-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
        getWorkFiles().write(9, false, pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr, pnd_Passed_Data_Pnd_Trans_Payee_Cde, pnd_Passed_Data_Pnd_Trans_Cde, pnd_Passed_Data_Pnd_Trans_User_Area,  //Natural: WRITE WORK FILE 9 #PASSED-DATA.#TRANS-PPCN-NBR #PASSED-DATA.#TRANS-PAYEE-CDE #PASSED-DATA.#TRANS-CDE #PASSED-DATA.#TRANS-USER-AREA #PASSED-DATA.#TRANS-USER-ID #PASSED-DATA.#TRANS-VERIFY-ID #PASSED-DATA.#TRANS-DTE #PASSED-DATA.#REASON
            pnd_Passed_Data_Pnd_Trans_User_Id, pnd_Passed_Data_Pnd_Trans_Verify_Id, pnd_Passed_Data_Pnd_Trans_Dte, pnd_Passed_Data_Pnd_Reason);
    }

    //
}
