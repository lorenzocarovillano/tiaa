/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:44 AM
**        * FROM NATURAL SUBPROGRAM : Iaan700b
************************************************************
**        * FILE NAME            : Iaan700b.java
**        * CLASS NAME           : Iaan700b
**        * INSTANCE NAME        : Iaan700b
************************************************************
**********************************************************************
* PROGRAM : IAAN700B                                                 *
* PURPOSE : GET NAME FROM KOR USING SSN                              *
*                                                                    *
* HISTORY                                                            *
*   03/15 : JUN TINIO - ORIGINAL CODE                                *
*
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan700b extends BLNatBase
{
    // Data Areas
    private LdaIaalpsgm ldaIaalpsgm;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ph_Last_Name;
    private DbsField pnd_Ph_Middle_Name;
    private DbsField pnd_Ph_First_Name;
    private DbsField pnd_Ph_Ssn;
    private int psgm001ReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaalpsgm = new LdaIaalpsgm();
        registerRecord(ldaIaalpsgm);

        // parameters
        parameters = new DbsRecord();
        pnd_Ph_Last_Name = parameters.newFieldInRecord("pnd_Ph_Last_Name", "#PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Ph_Last_Name.setParameterOption(ParameterOption.ByReference);
        pnd_Ph_Middle_Name = parameters.newFieldInRecord("pnd_Ph_Middle_Name", "#PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Ph_Middle_Name.setParameterOption(ParameterOption.ByReference);
        pnd_Ph_First_Name = parameters.newFieldInRecord("pnd_Ph_First_Name", "#PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Ph_First_Name.setParameterOption(ParameterOption.ByReference);
        pnd_Ph_Ssn = parameters.newFieldInRecord("pnd_Ph_Ssn", "#PH-SSN", FieldType.NUMERIC, 9);
        pnd_Ph_Ssn.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaalpsgm.initializeValues();

        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan700b() throws Exception
    {
        super("Iaan700b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Ph_Last_Name.reset();                                                                                                                                         //Natural: RESET #PH-LAST-NAME #PH-MIDDLE-NAME #PH-FIRST-NAME
        pnd_Ph_Middle_Name.reset();
        pnd_Ph_First_Name.reset();
        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function().setValue("PR");                                                                                          //Natural: ASSIGN #IN-FUNCTION := 'PR'
        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ().setValue("RS");                                                                                      //Natural: ASSIGN #IN-ADDR-USG-TYP := 'RS'
        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type().setValue("SSN");                                                                                             //Natural: ASSIGN #IN-TYPE := 'SSN'
        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn().setValueEdited(pnd_Ph_Ssn,new ReportEditMask("999999999"));                                               //Natural: MOVE EDITED #PH-SSN ( EM = 999999999 ) TO #IN-PIN-SSN
        psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ(), //Natural: CALL 'PSGM001' #PSGM001-COMMON-AREA
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Input_String(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Pref(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_1(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_2(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_3(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_4(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_5(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_City(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Country(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_State_Nm(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status());
        if (condition(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code().equals("00")))                                                                             //Natural: IF #RT-RET-CODE = '00'
        {
            pnd_Ph_Last_Name.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name());                                                                         //Natural: ASSIGN #PH-LAST-NAME := #RT-LAST-NAME
            pnd_Ph_Middle_Name.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name());                                                                     //Natural: ASSIGN #PH-MIDDLE-NAME := #RT-SECOND-NAME
            pnd_Ph_First_Name.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name());                                                                       //Natural: ASSIGN #PH-FIRST-NAME := #RT-FIRST-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "TIN NOT FOUND ",ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn());                                                              //Natural: WRITE ( 0 ) 'TIN NOT FOUND ' #IN-PIN-SSN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  #IN-CNTRCT-NBR-TYP:= 'T'
        //*  #IN-CNTRCT        := ' '
        //*  #IN-PY-A          := '01'
        //*  #IN-FUNCTION := 'PR'
        //*  #IN-TYPE := 'SSN'
        //*  MOVE EDITED #PH-SSN(EM=999999999) TO #IN-PIN-SSN
        //*  CALL 'PSGM003' #PSGM003-COMMON-AREA
        //*  IF #RET-CDE = '00'
        //*    #PH-LAST-NAME := #OUT-LAST-NAME
        //*    #PH-MIDDLE-NAME := #OUT-MIDDLE-NAME
        //*    #PH-FIRST-NAME := #OUT-FIRST-NAME
        //*  END-IF
    }

    //
}
