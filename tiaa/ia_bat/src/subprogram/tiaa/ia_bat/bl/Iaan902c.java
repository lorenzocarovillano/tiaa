/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:03 AM
**        * FROM NATURAL SUBPROGRAM : Iaan902c
************************************************************
**        * FILE NAME            : Iaan902c.java
**        * CLASS NAME           : Iaan902c
**        * INSTANCE NAME        : Iaan902c
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN902C CREATES TAX TRANSACTION CODING SHEETS    *
*      DATE     -  8/94                                              *
*                                                                    *
**********************************************************************
* CHANGE HISTORY
* CHANGED ON APRIL 10, 1995 BY D ROBINSON
* >  CORRECT TRANSACTION HISTORY (BEFORE IMAGE) RECORD ACCESS
* >
* >
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan902c extends BLNatBase
{
    // Data Areas
    private LdaIaal902c ldaIaal902c;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Passed_Data;
    private DbsField pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Passed_Data__R_Field_1;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8;
    private DbsField pnd_Passed_Data_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_2;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_3;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Area;
    private DbsField pnd_Passed_Data_Pnd_Last_Batch_Nbr;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal902c = new LdaIaal902c();
        registerRecord(ldaIaal902c);
        registerRecord(ldaIaal902c.getVw_iaa_Cntrct());
        registerRecord(ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal902c.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal902c.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();

        pnd_Passed_Data = parameters.newGroupInRecord("pnd_Passed_Data", "#PASSED-DATA");
        pnd_Passed_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte", "#CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Passed_Data__R_Field_1 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_1", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8 = pnd_Passed_Data__R_Field_1.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8", "#TRANS-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Passed_Data_Pnd_Trans_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Passed_Data_Pnd_Trans_Check_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_2 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_2", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Check_Dte);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc", "#TRANS-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy", "#TRANS-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm", "#TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd", "#TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_3 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_3", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Effctve_Dte);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc", "#TRANS-EFFCTVE-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy", "#TRANS-EFFCTVE-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm", "#TRANS-EFFCTVE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd", "#TRANS-EFFCTVE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_User_Area = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Area", "#TRANS-USER-AREA", FieldType.STRING, 
            6);
        pnd_Passed_Data_Pnd_Last_Batch_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.PACKED_DECIMAL, 
            7);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal902c.initializeValues();

        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan902c() throws Exception
    {
        super("Iaan902c");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        ldaIaal902c.getPnd_Ddctn_From_Net().reset();                                                                                                                      //Natural: RESET #DDCTN-FROM-NET #NO-CNTRCT-REC
        ldaIaal902c.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().reset();
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Batch_Nbr().compute(new ComputeParameters(false, ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Batch_Nbr()), pnd_Passed_Data_Pnd_Last_Batch_Nbr.add(1)); //Natural: COMPUTE #BATCH-NBR = #LAST-BATCH-NBR + 1
        pnd_Passed_Data_Pnd_Last_Batch_Nbr.setValue(ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Batch_Nbr());                                                                   //Natural: ASSIGN #LAST-BATCH-NBR := #BATCH-NBR
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Check_Dte_Mm().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm);                                                            //Natural: ASSIGN #CHECK-DTE-MM := #TRANS-CHECK-DTE-MM
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Check_Dte_Dd().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd);                                                            //Natural: ASSIGN #CHECK-DTE-DD := #TRANS-CHECK-DTE-DD
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Check_Dte_Yy().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy);                                                            //Natural: ASSIGN #CHECK-DTE-YY := #TRANS-CHECK-DTE-YY
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cntrct_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8);                                                                //Natural: ASSIGN #CNTRCT-NBR := #TRANS-PPCN-NBR-8
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Record_Status().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                              //Natural: ASSIGN #RECORD-STATUS := #TRANS-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-RECORD
        sub_Get_Cntrct_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR
        sub_Get_Cross_Reference_Nbr();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-PRTCPNT-RECORD
        sub_Get_Prtcpnt_Record();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Trans_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                        //Natural: ASSIGN #TRANS-NBR := #TRANS-CDE
        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_User_Area().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                                  //Natural: ASSIGN #USER-AREA := #TRANS-USER-AREA
        getWorkFiles().write(1, false, ldaIaal902c.getPnd_Ddctn_From_Net());                                                                                              //Natural: WRITE WORK FILE 1 #DDCTN-FROM-NET
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CNTRCT-RECORD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PRTCPNT-RECORD
    }
    private void sub_Get_Cntrct_Record() throws Exception                                                                                                                 //Natural: GET-CNTRCT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902c.getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                             //Natural: ASSIGN #IAA-CNTRCT-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902c.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal902c.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal902c.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902c.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA CONTRACT RECORD FOR: ",pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                 //Natural: WRITE 'NO IAA CONTRACT RECORD FOR: ' #TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal902c.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                  //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cross_Reference_Nbr() throws Exception                                                                                                           //Natural: GET-CROSS-REFERENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet173 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(1))))
        {
            decideConditionsMet173++;
            if (condition(ldaIaal902c.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902c.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ01",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902c.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ01:
                while (condition(ldaIaal902c.getVw_iaa_Cntrct_Trans().readNextRow("READ01")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                              //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                               //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(2))))
        {
            decideConditionsMet173++;
            if (condition(ldaIaal902c.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902c.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ02",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902c.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ02:
                while (condition(ldaIaal902c.getVw_iaa_Cntrct_Trans().readNextRow("READ02")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                              //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                               //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902c.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902c.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ03",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902c.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ03:
                while (condition(ldaIaal902c.getVw_iaa_Cntrct_Trans().readNextRow("READ03")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                              //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                          //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902c.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal902c.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902c.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ04",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902c.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ04:
                while (condition(ldaIaal902c.getVw_iaa_Cntrct_Trans().readNextRow("READ04")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902c.getIaa_Cntrct_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                    //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                               //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902c.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal902c.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal902c.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902c.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902c.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ05",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902c.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ05:
                while (condition(ldaIaal902c.getVw_iaa_Cntrct_Trans().readNextRow("READ05")))
                {
                    if (condition(ldaIaal902c.getIaa_Cntrct_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                    {
                        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                      //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                            //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((pnd_Passed_Data_Pnd_Trans_Payee_Cde.greaterOrEqual(3) && pnd_Passed_Data_Pnd_Trans_Payee_Cde.lessOrEqual(99)))))
        {
            decideConditionsMet173++;
            ldaIaal902c.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                             //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGE-ID := '1'
            ldaIaal902c.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                      //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal902c.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal902c.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                           //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
            ldaIaal902c.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
            (
            "READ06",
            new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902c.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
            new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
            1
            );
            READ06:
            while (condition(ldaIaal902c.getVw_iaa_Cpr_Trans().readNextRow("READ06")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(ldaIaal902c.getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && ldaIaal902c.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR = #TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = #TRANS-PAYEE-CDE
            {
                ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cpr_Trans_Bnfcry_Xref_Ind());                                           //Natural: ASSIGN #CROSS-REF-NBR := IAA-CPR-TRANS.BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902c.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                        //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902c.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                      //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
                (
                "FIND02",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902c.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
                1
                );
                FIND02:
                while (condition(ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
                {
                    ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902c.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                             //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Prtcpnt_Record() throws Exception                                                                                                                //Natural: GET-PRTCPNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902c.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902c.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                              //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902c.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
        1
        );
        FIND03:
        while (condition(ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND03", true)))
        {
            ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902c.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaIaal902c.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals(" ")))                                                            //Natural: IF CNTRCT-COMPANY-CD ( 1 ) = ' '
            {
                ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_N().setValue(ldaIaal902c.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue(2));                 //Natural: ASSIGN #INVEST-IN-CNTRCT-N := CNTRCT-IVC-AMT ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902c.getPnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_N().setValue(ldaIaal902c.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue(1));                 //Natural: ASSIGN #INVEST-IN-CNTRCT-N := CNTRCT-IVC-AMT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
