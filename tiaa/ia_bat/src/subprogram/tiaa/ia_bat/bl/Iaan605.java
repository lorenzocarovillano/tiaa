/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:41 AM
**        * FROM NATURAL SUBPROGRAM : Iaan605
************************************************************
**        * FILE NAME            : Iaan605.java
**        * CLASS NAME           : Iaan605
**        * INSTANCE NAME        : Iaan605
************************************************************
************************************************************************
* PROGRAM: IAAN605
* PURPOSE: TO BE USED FOR DOUBLE DEATH TO DETERMINE IF THE FIRST
*          ANNUITANT IS DECEASED BY LOOKING AT THE PAYEE 02 AFTER
*          IMAGE FOR TRANS-CDE = 66 AND SUB-CDE = 001
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan605 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ppcn_Nbr;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_First_Dod;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_1;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Cde;
    private DbsField pnd_Cntrct_Aftr_Key;

    private DbsGroup pnd_Cntrct_Aftr_Key__R_Field_2;
    private DbsField pnd_Cntrct_Aftr_Key__Filler1;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Invrse_Dte;
    private DbsField pnd_Payee_Cde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Ppcn_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Todays_Dte = parameters.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Todays_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_First_Dod = parameters.newFieldInRecord("pnd_First_Dod", "#FIRST-DOD", FieldType.NUMERIC, 6);
        pnd_First_Dod.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        registerRecord(vw_iaa_Trans_Rcrd);

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cntrct_Trans);

        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Trans_Cntrct_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_1", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Cntrct_Aftr_Key = localVariables.newFieldInRecord("pnd_Cntrct_Aftr_Key", "#CNTRCT-AFTR-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Aftr_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Aftr_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Aftr_Key);
        pnd_Cntrct_Aftr_Key__Filler1 = pnd_Cntrct_Aftr_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Aftr_Key__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Aftr_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Aftr_Key_Pnd_Invrse_Dte = pnd_Cntrct_Aftr_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.NUMERIC, 
            12);
        pnd_Payee_Cde = localVariables.newFieldInRecord("pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct_Trans.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Cntrct_Aftr_Key.setInitialValue("2");
        pnd_Payee_Cde.setInitialValue(2);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan605() throws Exception
    {
        super("Iaan605");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Cntrct_Aftr_Key_Pnd_Invrse_Dte.reset();                                                                                                                       //Natural: RESET #INVRSE-DTE #INVRSE-TRANS-DTE #TRANS-CDE #FIRST-DOD
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte.reset();
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde.reset();
        pnd_First_Dod.reset();
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(pnd_Ppcn_Nbr);                                                                                                   //Natural: ASSIGN #TRANS-PPCN-NBR := #PPCN-NBR
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde.setValue(pnd_Payee_Cde);                                                                                                 //Natural: ASSIGN #TRANS-PAYEE-CDE := #PAYEE-CDE
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CNTRCT-KEY STARTING FROM #TRANS-CNTRCT-KEY
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trans_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ01")))
        {
            if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.notEquals(pnd_Ppcn_Nbr) || iaa_Trans_Rcrd_Trans_Payee_Cde.notEquals(pnd_Payee_Cde)))                              //Natural: IF TRANS-PPCN-NBR NE #PPCN-NBR OR TRANS-PAYEE-CDE NE #PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(iaa_Trans_Rcrd_Trans_Cde.equals(66) && iaa_Trans_Rcrd_Trans_Sub_Cde.equals("001") && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("A")              //Natural: ACCEPT IF TRANS-CDE = 66 AND TRANS-SUB-CDE = '001' AND TRANS-ACTVTY-CDE = 'A' AND TRANS-TODAYS-DTE = #TODAYS-DTE
                && iaa_Trans_Rcrd_Trans_Todays_Dte.equals(pnd_Todays_Dte))))
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM GET-AFTER-IMAGE
            sub_Get_After_Image();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AFTER-IMAGE
        //* ***********************************************************************
    }
    private void sub_Get_After_Image() throws Exception                                                                                                                   //Natural: GET-AFTER-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                  //Natural: ASSIGN #CNTRCT-PPCN-NBR := TRANS-PPCN-NBR
        pnd_Cntrct_Aftr_Key_Pnd_Invrse_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                                     //Natural: ASSIGN #INVRSE-DTE := IAA-TRANS-RCRD.INVRSE-TRANS-DTE
        vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #CNTRCT-AFTR-KEY
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", pnd_Cntrct_Aftr_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrct_Trans.readNextRow("READ02")))
        {
            if (condition(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr.notEquals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) || iaa_Cntrct_Trans_Invrse_Trans_Dte.notEquals(pnd_Cntrct_Aftr_Key_Pnd_Invrse_Dte))) //Natural: IF CNTRCT-PPCN-NBR NE TRANS-PPCN-NBR OR IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE NE #INVRSE-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_First_Dod.setValue(iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte);                                                                                           //Natural: ASSIGN #FIRST-DOD := CNTRCT-FIRST-ANNT-DOD-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
