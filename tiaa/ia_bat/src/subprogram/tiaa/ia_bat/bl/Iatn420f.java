/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:30 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420f
************************************************************
**        * FILE NAME            : Iatn420f.java
**        * CLASS NAME           : Iatn420f
**        * INSTANCE NAME        : Iatn420f
************************************************************
************************************************************************
*  PROGRAM: IATN420F
*   AUTHOR: ARI GROSSMAN
*     DATE: MAR 08, 1996
*  PURPOSE: UPDATE AND ADD FUND RECS FOR GRADED TO STANDARD TRANSFER
*         : UPDATE CPR AND STORE AFTER IMAGES FOR EVERYTHING
*  HISTORY: 01/16/97 : ADDED MULTI FUND FUNCTIONALITY/ CONTRACT TRANS
*           01/12/98 : TRANSFER PROCESSING - 1998 (FORMERLY IATN170F)
*           01/16/09 OS TIAA ACCESS CHANGE. SC 011609.
*           04/06/12 OS RATE BASE EXPANSION CHANGES. SC 040612.
*           04/2017 OS RE-STOWED ONLY FOR IAAL420 AND IAAL162G PIN EXP.
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420f extends BLNatBase
{
    // Data Areas
    private LdaIaal420 ldaIaal420;
    private LdaIaal162g ldaIaal162g;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Iaa_From_Cntrct;
    private DbsField pnd_Iaa_From_Pyee;

    private DbsGroup pnd_Iaa_From_Pyee__R_Field_1;
    private DbsField pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N;
    private DbsField pnd_From_Fund;
    private DbsField pnd_Rate_Code_Grd;
    private DbsField pnd_Gtd_Pmt_Grd;
    private DbsField pnd_Dvd_Pmt_Grd;
    private DbsField pnd_Rate_Code_Std;
    private DbsField pnd_Gtd_Pmt_Std;
    private DbsField pnd_Dvd_Pmt_Std;
    private DbsField pnd_From_Aftr_Xfr_Guar;
    private DbsField pnd_From_Aftr_Xfr_Divid;
    private DbsField pnd_To_Aftr_Xfr_Guar;
    private DbsField pnd_To_Aftr_Xfr_Divid;
    private DbsField pnd_Check_Date;

    private DbsGroup pnd_Check_Date__R_Field_2;
    private DbsField pnd_Check_Date_Pnd_Check_Date_A;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Effective_Date;
    private DbsField pnd_Next_Bus_Dte;
    private DbsField pnd_Time;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Datd;
    private DbsField pnd_Frst_Pymnt_Curr_Dte;
    private DbsField pnd_On_File_Already;
    private DbsField pnd_Rate_Code_Breakdown;

    private DbsGroup pnd_Rate_Code_Breakdown__R_Field_3;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2;
    private DbsField pnd_From_Fund_H;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_4;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_5;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_File_Mode;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_To_Cntrct_Fund;
    private DbsField pnd_I;
    private DbsField pnd_Partial_Transfer;
    private DbsField pnd_Mode;

    private DbsGroup pnd_Mode__R_Field_6;
    private DbsField pnd_Mode_Pnd_Mode_A;

    private DbsGroup pnd_Mode__R_Field_7;
    private DbsField pnd_Mode_Pnd_Mode_1;
    private DbsField pnd_Mode_Pnd_Mode_2_3;
    private DbsField pnd_Transfer_96;
    private DbsField pnd_Xfr_Out_Date;
    private DbsField pnd_W_Tiaa_Rate_Cde;
    private DbsField pnd_W_Tiaa_Rate_Dte;
    private DbsField pnd_W_Tiaa_Per_Pay_Amt;
    private DbsField pnd_W_Tiaa_Per_Div_Amt;
    private DbsField pnd_K;
    private DbsField pnd_Negative_Amount;
    private DbsField pnd_Sub;
    private DbsField pnd_Per_Pay_Amt;
    private DbsField pnd_Per_Div_Amt;
    private DbsField pnd_Rate_Code;

    private DbsGroup pnd_Rate_Code__R_Field_8;
    private DbsField pnd_Rate_Code_Pnd_Rate_Code_A;
    private DbsField pnd_Tiaa_Rate_Code;
    private DbsField pnd_Found_Rate;
    private DbsField pnd_C;
    private DbsField pnd_Total;
    private DbsField pnd_Total_1;
    private DbsField pnd_Total_2;
    private DbsField pnd_T1;
    private DbsField pnd_T2;
    private DbsField pnd_Trans_Date;
    private DbsField pnd_Trans_Date_1;
    private DbsField pnd_Trans_Date_3;

    private DbsGroup pnd_Trans_Date_3__R_Field_9;
    private DbsField pnd_Trans_Date_3_Pnd_Trans_Date_3_A;
    private DbsField pnd_Trans_Date_2;

    private DataAccessProgramView vw_iaa_Cpr_1;
    private DbsField iaa_Cpr_1_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_1_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_1_Cntrct_Mode_Ind;
    private DbsField pnd_T_Dte;

    private DbsGroup pnd_T_Dte__R_Field_10;
    private DbsField pnd_T_Dte_Pnd_T_Dte_A;

    private DbsGroup pnd_T_Dte__R_Field_11;
    private DbsField pnd_T_Dte_Pnd_T_Yy;
    private DbsField pnd_T_Dte_Pnd_T_Mm;

    private DbsGroup pnd_Tr_Date_A;
    private DbsField pnd_Tr_Date_A_Pnd_Tr_Date_Yy;
    private DbsField pnd_Tr_Date_A_Pnd_Tr_Date_Mm;

    private DbsGroup pnd_Tr_Date_A__R_Field_12;
    private DbsField pnd_Tr_Date_A_Pnd_Tr_Date_N2;
    private DbsField pnd_Tr_Date_N;
    private DbsField pnd_Mth_Nmb;
    private DbsField pnd_Max_Rate;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal420 = new LdaIaal420();
        registerRecord(ldaIaal420);
        registerRecord(ldaIaal420.getVw_iaa_Cref_Fund());
        registerRecord(ldaIaal420.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal420.getVw_iaa_Tiaa_Fund());
        registerRecord(ldaIaal420.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal420.getVw_iaa_Cpr());
        registerRecord(ldaIaal420.getVw_iaa_Cpr_Trans());
        ldaIaal162g = new LdaIaal162g();
        registerRecord(ldaIaal162g);
        registerRecord(ldaIaal162g.getVw_iaa_Cntrct_1());
        registerRecord(ldaIaal162g.getVw_iaa_Cntrct_2());
        registerRecord(ldaIaal162g.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal162g.getVw_iaa_Cpr_2());

        // parameters
        parameters = new DbsRecord();
        pnd_Iaa_From_Cntrct = parameters.newFieldInRecord("pnd_Iaa_From_Cntrct", "#IAA-FROM-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_From_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_From_Pyee = parameters.newFieldInRecord("pnd_Iaa_From_Pyee", "#IAA-FROM-PYEE", FieldType.STRING, 2);
        pnd_Iaa_From_Pyee.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_From_Pyee__R_Field_1 = parameters.newGroupInRecord("pnd_Iaa_From_Pyee__R_Field_1", "REDEFINE", pnd_Iaa_From_Pyee);
        pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N = pnd_Iaa_From_Pyee__R_Field_1.newFieldInGroup("pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N", "#IAA-FROM-PYEE-N", 
            FieldType.NUMERIC, 2);
        pnd_From_Fund = parameters.newFieldInRecord("pnd_From_Fund", "#FROM-FUND", FieldType.STRING, 1);
        pnd_From_Fund.setParameterOption(ParameterOption.ByReference);
        pnd_Rate_Code_Grd = parameters.newFieldArrayInRecord("pnd_Rate_Code_Grd", "#RATE-CODE-GRD", FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_Rate_Code_Grd.setParameterOption(ParameterOption.ByReference);
        pnd_Gtd_Pmt_Grd = parameters.newFieldArrayInRecord("pnd_Gtd_Pmt_Grd", "#GTD-PMT-GRD", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Gtd_Pmt_Grd.setParameterOption(ParameterOption.ByReference);
        pnd_Dvd_Pmt_Grd = parameters.newFieldArrayInRecord("pnd_Dvd_Pmt_Grd", "#DVD-PMT-GRD", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Dvd_Pmt_Grd.setParameterOption(ParameterOption.ByReference);
        pnd_Rate_Code_Std = parameters.newFieldArrayInRecord("pnd_Rate_Code_Std", "#RATE-CODE-STD", FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_Rate_Code_Std.setParameterOption(ParameterOption.ByReference);
        pnd_Gtd_Pmt_Std = parameters.newFieldArrayInRecord("pnd_Gtd_Pmt_Std", "#GTD-PMT-STD", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Gtd_Pmt_Std.setParameterOption(ParameterOption.ByReference);
        pnd_Dvd_Pmt_Std = parameters.newFieldArrayInRecord("pnd_Dvd_Pmt_Std", "#DVD-PMT-STD", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Dvd_Pmt_Std.setParameterOption(ParameterOption.ByReference);
        pnd_From_Aftr_Xfr_Guar = parameters.newFieldArrayInRecord("pnd_From_Aftr_Xfr_Guar", "#FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 20));
        pnd_From_Aftr_Xfr_Guar.setParameterOption(ParameterOption.ByReference);
        pnd_From_Aftr_Xfr_Divid = parameters.newFieldArrayInRecord("pnd_From_Aftr_Xfr_Divid", "#FROM-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 20));
        pnd_From_Aftr_Xfr_Divid.setParameterOption(ParameterOption.ByReference);
        pnd_To_Aftr_Xfr_Guar = parameters.newFieldArrayInRecord("pnd_To_Aftr_Xfr_Guar", "#TO-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_To_Aftr_Xfr_Guar.setParameterOption(ParameterOption.ByReference);
        pnd_To_Aftr_Xfr_Divid = parameters.newFieldArrayInRecord("pnd_To_Aftr_Xfr_Divid", "#TO-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_To_Aftr_Xfr_Divid.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Date = parameters.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Check_Date.setParameterOption(ParameterOption.ByReference);

        pnd_Check_Date__R_Field_2 = parameters.newGroupInRecord("pnd_Check_Date__R_Field_2", "REDEFINE", pnd_Check_Date);
        pnd_Check_Date_Pnd_Check_Date_A = pnd_Check_Date__R_Field_2.newFieldInGroup("pnd_Check_Date_Pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 
            8);
        pnd_Todays_Dte = parameters.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.DATE);
        pnd_Todays_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Effective_Date = parameters.newFieldInRecord("pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.DATE);
        pnd_Effective_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Next_Bus_Dte = parameters.newFieldInRecord("pnd_Next_Bus_Dte", "#NEXT-BUS-DTE", FieldType.DATE);
        pnd_Next_Bus_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Time = parameters.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Time.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Frst_Pymnt_Curr_Dte = localVariables.newFieldInRecord("pnd_Frst_Pymnt_Curr_Dte", "#FRST-PYMNT-CURR-DTE", FieldType.DATE);
        pnd_On_File_Already = localVariables.newFieldInRecord("pnd_On_File_Already", "#ON-FILE-ALREADY", FieldType.STRING, 1);
        pnd_Rate_Code_Breakdown = localVariables.newFieldInRecord("pnd_Rate_Code_Breakdown", "#RATE-CODE-BREAKDOWN", FieldType.STRING, 3);

        pnd_Rate_Code_Breakdown__R_Field_3 = localVariables.newGroupInRecord("pnd_Rate_Code_Breakdown__R_Field_3", "REDEFINE", pnd_Rate_Code_Breakdown);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1 = pnd_Rate_Code_Breakdown__R_Field_3.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1", "#RATE-CODE-1", 
            FieldType.STRING, 1);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2 = pnd_Rate_Code_Breakdown__R_Field_3.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2", "#RATE-CODE-2", 
            FieldType.STRING, 2);
        pnd_From_Fund_H = localVariables.newFieldInRecord("pnd_From_Fund_H", "#FROM-FUND-H", FieldType.STRING, 1);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_To_Cntrct_Fund = localVariables.newFieldInRecord("pnd_To_Cntrct_Fund", "#TO-CNTRCT-FUND", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_Partial_Transfer = localVariables.newFieldInRecord("pnd_Partial_Transfer", "#PARTIAL-TRANSFER", FieldType.STRING, 1);
        pnd_Mode = localVariables.newFieldInRecord("pnd_Mode", "#MODE", FieldType.NUMERIC, 3);

        pnd_Mode__R_Field_6 = localVariables.newGroupInRecord("pnd_Mode__R_Field_6", "REDEFINE", pnd_Mode);
        pnd_Mode_Pnd_Mode_A = pnd_Mode__R_Field_6.newFieldInGroup("pnd_Mode_Pnd_Mode_A", "#MODE-A", FieldType.STRING, 3);

        pnd_Mode__R_Field_7 = pnd_Mode__R_Field_6.newGroupInGroup("pnd_Mode__R_Field_7", "REDEFINE", pnd_Mode_Pnd_Mode_A);
        pnd_Mode_Pnd_Mode_1 = pnd_Mode__R_Field_7.newFieldInGroup("pnd_Mode_Pnd_Mode_1", "#MODE-1", FieldType.STRING, 1);
        pnd_Mode_Pnd_Mode_2_3 = pnd_Mode__R_Field_7.newFieldInGroup("pnd_Mode_Pnd_Mode_2_3", "#MODE-2-3", FieldType.STRING, 2);
        pnd_Transfer_96 = localVariables.newFieldInRecord("pnd_Transfer_96", "#TRANSFER-96", FieldType.STRING, 1);
        pnd_Xfr_Out_Date = localVariables.newFieldInRecord("pnd_Xfr_Out_Date", "#XFR-OUT-DATE", FieldType.STRING, 8);
        pnd_W_Tiaa_Rate_Cde = localVariables.newFieldArrayInRecord("pnd_W_Tiaa_Rate_Cde", "#W-TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            250));
        pnd_W_Tiaa_Rate_Dte = localVariables.newFieldArrayInRecord("pnd_W_Tiaa_Rate_Dte", "#W-TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 
            250));
        pnd_W_Tiaa_Per_Pay_Amt = localVariables.newFieldArrayInRecord("pnd_W_Tiaa_Per_Pay_Amt", "#W-TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 250));
        pnd_W_Tiaa_Per_Div_Amt = localVariables.newFieldArrayInRecord("pnd_W_Tiaa_Per_Div_Amt", "#W-TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 250));
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Negative_Amount = localVariables.newFieldInRecord("pnd_Negative_Amount", "#NEGATIVE-AMOUNT", FieldType.STRING, 1);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Per_Pay_Amt = localVariables.newFieldInRecord("pnd_Per_Pay_Amt", "#PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Div_Amt = localVariables.newFieldInRecord("pnd_Per_Div_Amt", "#PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Rate_Code = localVariables.newFieldInRecord("pnd_Rate_Code", "#RATE-CODE", FieldType.STRING, 2);

        pnd_Rate_Code__R_Field_8 = localVariables.newGroupInRecord("pnd_Rate_Code__R_Field_8", "REDEFINE", pnd_Rate_Code);
        pnd_Rate_Code_Pnd_Rate_Code_A = pnd_Rate_Code__R_Field_8.newFieldInGroup("pnd_Rate_Code_Pnd_Rate_Code_A", "#RATE-CODE-A", FieldType.STRING, 2);
        pnd_Tiaa_Rate_Code = localVariables.newFieldInRecord("pnd_Tiaa_Rate_Code", "#TIAA-RATE-CODE", FieldType.STRING, 2);
        pnd_Found_Rate = localVariables.newFieldInRecord("pnd_Found_Rate", "#FOUND-RATE", FieldType.STRING, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.NUMERIC, 15, 7);
        pnd_Total_1 = localVariables.newFieldInRecord("pnd_Total_1", "#TOTAL-1", FieldType.NUMERIC, 15, 7);
        pnd_Total_2 = localVariables.newFieldInRecord("pnd_Total_2", "#TOTAL-2", FieldType.NUMERIC, 15, 7);
        pnd_T1 = localVariables.newFieldInRecord("pnd_T1", "#T1", FieldType.NUMERIC, 15, 7);
        pnd_T2 = localVariables.newFieldInRecord("pnd_T2", "#T2", FieldType.NUMERIC, 15, 7);
        pnd_Trans_Date = localVariables.newFieldInRecord("pnd_Trans_Date", "#TRANS-DATE", FieldType.NUMERIC, 15, 7);
        pnd_Trans_Date_1 = localVariables.newFieldInRecord("pnd_Trans_Date_1", "#TRANS-DATE-1", FieldType.NUMERIC, 8);
        pnd_Trans_Date_3 = localVariables.newFieldInRecord("pnd_Trans_Date_3", "#TRANS-DATE-3", FieldType.NUMERIC, 8);

        pnd_Trans_Date_3__R_Field_9 = localVariables.newGroupInRecord("pnd_Trans_Date_3__R_Field_9", "REDEFINE", pnd_Trans_Date_3);
        pnd_Trans_Date_3_Pnd_Trans_Date_3_A = pnd_Trans_Date_3__R_Field_9.newFieldInGroup("pnd_Trans_Date_3_Pnd_Trans_Date_3_A", "#TRANS-DATE-3-A", FieldType.STRING, 
            8);
        pnd_Trans_Date_2 = localVariables.newFieldInRecord("pnd_Trans_Date_2", "#TRANS-DATE-2", FieldType.NUMERIC, 7, 7);

        vw_iaa_Cpr_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_1", "IAA-CPR-1"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        iaa_Cpr_1_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_1.getRecord().newFieldInGroup("iaa_Cpr_1_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_1_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_1.getRecord().newFieldInGroup("iaa_Cpr_1_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_1_Cntrct_Mode_Ind = vw_iaa_Cpr_1.getRecord().newFieldInGroup("iaa_Cpr_1_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        registerRecord(vw_iaa_Cpr_1);

        pnd_T_Dte = localVariables.newFieldInRecord("pnd_T_Dte", "#T-DTE", FieldType.NUMERIC, 8);

        pnd_T_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_T_Dte__R_Field_10", "REDEFINE", pnd_T_Dte);
        pnd_T_Dte_Pnd_T_Dte_A = pnd_T_Dte__R_Field_10.newFieldInGroup("pnd_T_Dte_Pnd_T_Dte_A", "#T-DTE-A", FieldType.STRING, 8);

        pnd_T_Dte__R_Field_11 = pnd_T_Dte__R_Field_10.newGroupInGroup("pnd_T_Dte__R_Field_11", "REDEFINE", pnd_T_Dte_Pnd_T_Dte_A);
        pnd_T_Dte_Pnd_T_Yy = pnd_T_Dte__R_Field_11.newFieldInGroup("pnd_T_Dte_Pnd_T_Yy", "#T-YY", FieldType.STRING, 6);
        pnd_T_Dte_Pnd_T_Mm = pnd_T_Dte__R_Field_11.newFieldInGroup("pnd_T_Dte_Pnd_T_Mm", "#T-MM", FieldType.STRING, 2);

        pnd_Tr_Date_A = localVariables.newGroupInRecord("pnd_Tr_Date_A", "#TR-DATE-A");
        pnd_Tr_Date_A_Pnd_Tr_Date_Yy = pnd_Tr_Date_A.newFieldInGroup("pnd_Tr_Date_A_Pnd_Tr_Date_Yy", "#TR-DATE-YY", FieldType.STRING, 4);
        pnd_Tr_Date_A_Pnd_Tr_Date_Mm = pnd_Tr_Date_A.newFieldInGroup("pnd_Tr_Date_A_Pnd_Tr_Date_Mm", "#TR-DATE-MM", FieldType.STRING, 2);

        pnd_Tr_Date_A__R_Field_12 = localVariables.newGroupInRecord("pnd_Tr_Date_A__R_Field_12", "REDEFINE", pnd_Tr_Date_A);
        pnd_Tr_Date_A_Pnd_Tr_Date_N2 = pnd_Tr_Date_A__R_Field_12.newFieldInGroup("pnd_Tr_Date_A_Pnd_Tr_Date_N2", "#TR-DATE-N2", FieldType.NUMERIC, 6);
        pnd_Tr_Date_N = localVariables.newFieldInRecord("pnd_Tr_Date_N", "#TR-DATE-N", FieldType.NUMERIC, 8, 2);
        pnd_Mth_Nmb = localVariables.newFieldInRecord("pnd_Mth_Nmb", "#MTH-NMB", FieldType.NUMERIC, 2);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cpr_1.reset();

        ldaIaal420.initializeValues();
        ldaIaal162g.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Tr_Date_A_Pnd_Tr_Date_Yy.setInitialValue("1997");
        pnd_Max_Rate.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420f() throws Exception
    {
        super("Iatn420f");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420F", onError);
        //* *********
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ON ERROR;//Natural: ASSIGN #CNTRCT-PPCN-NBR := #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                            //Natural: ASSIGN #CNTRCT-PAYEE := #IAA-FROM-PYEE-N
        ldaIaal420.getVw_iaa_Cpr().startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) IAA-CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R1R",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R1R:
        while (condition(ldaIaal420.getVw_iaa_Cpr().readNextRow("R1R")))
        {
            if (condition(ldaIaal420.getIaa_Cpr_Cntrct_Part_Ppcn_Nbr().notEquals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) || ldaIaal420.getIaa_Cpr_Cntrct_Part_Payee_Cde().notEquals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF IAA-CPR.CNTRCT-PART-PPCN-NBR NE #CNTRCT-PPCN-NBR OR IAA-CPR.CNTRCT-PART-PAYEE-CDE NE #CNTRCT-PAYEE
            {
                if (true) break R1R;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1R. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Mode.setValue(ldaIaal420.getIaa_Cpr_Cntrct_Mode_Ind());                                                                                                   //Natural: ASSIGN #MODE := IAA-CPR.CNTRCT-MODE-IND
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Return_Code.setValue("T1");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T1'
                                                                                                                                                                          //Natural: PERFORM #AI-CONTRACTS
        sub_Pnd_Ai_Contracts();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("M2");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'M2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                            //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CPR
        sub_Pnd_Update_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("T2");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                            //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #AI-CPR
        sub_Pnd_Ai_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Nazn6031.class , getCurrentProcessState(), pnd_Datd, ldaIaal162g.getIaa_Cntrct_1_Cntrct_Issue_Dte(), pnd_Check_Date_Pnd_Check_Date_A,             //Natural: CALLNAT 'NAZN6031' #DATD IAA-CNTRCT-1.CNTRCT-ISSUE-DTE #CHECK-DATE-A IAA-CNTRCT-1.CNTRCT-FIRST-PYMNT-DUE-DTE IAA-CNTRCT-1.CNTRCT-FP-DUE-DTE-DD #MODE IAA-CNTRCT-1.CNTRCT-OPTN-CDE
            ldaIaal162g.getIaa_Cntrct_1_Cntrct_First_Pymnt_Due_Dte(), ldaIaal162g.getIaa_Cntrct_1_Cntrct_Fp_Due_Dte_Dd(), pnd_Mode, ldaIaal162g.getIaa_Cntrct_1_Cntrct_Optn_Cde());
        if (condition(Global.isEscape())) return;
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                           //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue("T1G");                                                                                                              //Natural: ASSIGN #W-FUND-CODE = 'T1G'
        if (condition(pnd_From_Aftr_Xfr_Guar.getValue(1).greater(getZero())))                                                                                             //Natural: IF #FROM-AFTR-XFR-GUAR ( 1 ) > 0
        {
            pnd_Partial_Transfer.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #PARTIAL-TRANSFER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Partial_Transfer.setValue(" ");                                                                                                                           //Natural: MOVE ' ' TO #PARTIAL-TRANSFER
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Partial_Transfer.equals("Y")))                                                                                                                  //Natural: IF #PARTIAL-TRANSFER = 'Y'
        {
            pnd_Return_Code.setValue("M3");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M3'
                                                                                                                                                                          //Natural: PERFORM #UPDATE-TIAA-FUND-FROM
            sub_Pnd_Update_Tiaa_Fund_From();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Return_Code.setValue("M3");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M3'
                                                                                                                                                                          //Natural: PERFORM #DELETE-TIAA-FUND-FROM
            sub_Pnd_Delete_Tiaa_Fund_From();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-ON-FILE
        sub_Pnd_Check_If_On_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_On_File_Already.equals("Y")))                                                                                                                   //Natural: IF #ON-FILE-ALREADY = 'Y'
        {
            pnd_Return_Code.setValue("M3");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M3'
                                                                                                                                                                          //Natural: PERFORM #UPDATE-TIAA-FUND-TO
            sub_Pnd_Update_Tiaa_Fund_To();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Return_Code.setValue("M3");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M3'
                                                                                                                                                                          //Natural: PERFORM #STORE-TIAA-FUND-TO
            sub_Pnd_Store_Tiaa_Fund_To();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("T3");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T3'
                                                                                                                                                                          //Natural: PERFORM #STORE-TIAA-FUND-TO-AI
        sub_Pnd_Store_Tiaa_Fund_To_Ai();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-TIAA-FUND-FROM
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DELETE-TIAA-FUND-FROM
        //* **********************************************************************
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-IF-ON-FILE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-TIAA-FUND-TO-AI
        //* **********************************************************************
        //*       #W-FUND-CODE        := #FROM-FUND-TOT
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-TIAA-FUND-TO
        //* *FN. FOR #I = 1 TO 99
        //*       MOVE EDITED '19970401' TO
        //*              IAA-TIAA-FUND.TIAA-RATE-DTE(#I)(EM=YYYYMMDD)
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CONTRACTS
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CPR
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CPR
        //* **********************************************************************
        //* **********************************************************************
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RATE-CODE-PARA
        //*  040612 CHANGED THE FOLLOWING FROM 1:99 TO *
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-TIAA-FUND-TO
        //* **********************************************************************
        //*    FQ. FOR #I = 1 TO 99
        //*       MOVE C*TIAA-RATE-DATA-GRP TO #NUM
        //*        FM.    FOR #C = 1 TO 99
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-RE-AI
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WEIGHTED-AVERAGE-98
        //* **********************************************************************
    }
    private void sub_Pnd_Update_Tiaa_Fund_From() throws Exception                                                                                                         //Natural: #UPDATE-TIAA-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1B",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1B:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1B")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal420.getIaa_Tiaa_Fund_Lst_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN IAA-TIAA-FUND.LST-TRANS-DTE = #TIME
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Old_Per_Amt().setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt());                                                  //Natural: ASSIGN IAA-TIAA-FUND.TIAA-OLD-PER-AMT := IAA-TIAA-FUND.TIAA-TOT-PER-AMT
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Old_Div_Amt().setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt());                                                  //Natural: ASSIGN IAA-TIAA-FUND.TIAA-OLD-DIV-AMT := IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
                                                                                                                                                                          //Natural: PERFORM #RATE-CODE-PARA
                sub_Pnd_Rate_Code_Para();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1B"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1B"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Lst_Xfr_Out_Dte().setValue(pnd_Todays_Dte);                                                                              //Natural: ASSIGN IAA-TIAA-FUND.TIAA-LST-XFR-OUT-DTE := #TODAYS-DTE
                ldaIaal420.getVw_iaa_Tiaa_Fund().updateDBRow("R1B");                                                                                                      //Natural: UPDATE
                if (condition(pnd_Negative_Amount.equals(" ")))                                                                                                           //Natural: IF #NEGATIVE-AMOUNT = ' '
                {
                    pnd_Return_Code.reset();                                                                                                                              //Natural: RESET #RETURN-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1B;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1B. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Delete_Tiaa_Fund_From() throws Exception                                                                                                         //Natural: #DELETE-TIAA-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1D",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1D:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1D")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal420.getVw_iaa_Tiaa_Fund().deleteDBRow("R1D");                                                                                                      //Natural: DELETE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1D;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1D. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_If_On_File() throws Exception                                                                                                              //Natural: #CHECK-IF-ON-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File_Already.reset();                                                                                                                                      //Natural: RESET #ON-FILE-ALREADY
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                           //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue("T1S");                                                                                                              //Natural: ASSIGN #W-FUND-CODE = 'T1S'
        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Z",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1Z:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1Z")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_On_File_Already.setValue("Y");                                                                                                                        //Natural: MOVE 'Y' TO #ON-FILE-ALREADY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Tiaa_Fund_To_Ai() throws Exception                                                                                                         //Natural: #STORE-TIAA-FUND-TO-AI
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                           //Natural: ASSIGN #W-CNTRCT-PAYEE := #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1G",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1G:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1G")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*  011609
                if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("U09") || ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("W09")          //Natural: IF IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = 'U09' OR = 'W09' OR = 'U11' OR = 'W11'
                    || ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("U11") || ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("W11")))
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde());                                                      //Natural: MOVE IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE TO #W-FUND-CODE
                                                                                                                                                                          //Natural: PERFORM #STORE-RE-AI
                    sub_Pnd_Store_Re_Ai();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1G"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1G"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal420.getVw_iaa_Tiaa_Fund_Trans().reset();                                                                                                       //Natural: RESET IAA-TIAA-FUND-TRANS
                    ldaIaal420.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal420.getVw_iaa_Tiaa_Fund());                                                             //Natural: MOVE BY NAME IAA-TIAA-FUND TO IAA-TIAA-FUND-TRANS
                    ldaIaal420.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE = #TIME
                    pnd_Date_Time_P.setValue(ldaIaal420.getIaa_Tiaa_Fund_Trans_Trans_Dte());                                                                              //Natural: ASSIGN #DATE-TIME-P = IAA-TIAA-FUND-TRANS.TRANS-DTE
                    ldaIaal420.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal420.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte()),      //Natural: COMPUTE IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                        new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                    ldaIaal420.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID = '2'
                    ldaIaal420.getIaa_Tiaa_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                 //Natural: ASSIGN IAA-TIAA-FUND-TRANS.LST-TRANS-DTE = #TIME
                    ldaIaal420.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                         //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                    ldaIaal420.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                 //Natural: STORE IAA-TIAA-FUND-TRANS
                    pnd_Return_Code.reset();                                                                                                                              //Natural: RESET #RETURN-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1G;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1G. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Tiaa_Fund_To() throws Exception                                                                                                            //Natural: #STORE-TIAA-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  WRITE 'STORE TIAA'
        ldaIaal420.getVw_iaa_Tiaa_Fund().reset();                                                                                                                         //Natural: RESET IAA-TIAA-FUND
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().setValue(pnd_Iaa_From_Cntrct);                                                                                 //Natural: ASSIGN IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR := #IAA-FROM-CNTRCT
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                              //Natural: ASSIGN IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE := #IAA-FROM-PYEE-N
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().setValue("T1S");                                                                                                //Natural: ASSIGN IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE := 'T1S'
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt().nadd(pnd_Gtd_Pmt_Std.getValue("*"));                                                                               //Natural: ADD #GTD-PMT-STD ( * ) TO IAA-TIAA-FUND.TIAA-TOT-PER-AMT
        //*  040612
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt().nadd(pnd_Dvd_Pmt_Std.getValue("*"));                                                                               //Natural: ADD #DVD-PMT-STD ( * ) TO IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
        FN:                                                                                                                                                               //Natural: FOR #I = 1 TO #MAX-RATE
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rate)); pnd_I.nadd(1))
        {
            if (condition(pnd_Rate_Code_Std.getValue(pnd_I).greater(" ")))                                                                                                //Natural: IF #RATE-CODE-STD ( #I ) > ' '
            {
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Cde().getValue(pnd_I).setValue(pnd_Rate_Code_Std.getValue(pnd_I));                                                  //Natural: ASSIGN IAA-TIAA-FUND.TIAA-RATE-CDE ( #I ) := #RATE-CODE-STD ( #I )
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Dte().getValue(pnd_I).setValue(pnd_Effective_Date);                                                                 //Natural: ASSIGN IAA-TIAA-FUND.TIAA-RATE-DTE ( #I ) := #EFFECTIVE-DATE
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue(pnd_I).setValue(pnd_Gtd_Pmt_Std.getValue(pnd_I));                                                 //Natural: ASSIGN IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( #I ) := #GTD-PMT-STD ( #I )
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue(pnd_I).setValue(pnd_Dvd_Pmt_Std.getValue(pnd_I));                                                 //Natural: ASSIGN IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( #I ) := #DVD-PMT-STD ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  040612
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaal420.getIaa_Tiaa_Fund_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                   //Natural: ASSIGN IAA-TIAA-FUND.LST-TRANS-DTE := #TIME
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                                       //Natural: ASSIGN IAA-TIAA-FUND.TIAA-LST-XFR-IN-DTE := #TODAYS-DTE
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Xfr_Iss_Dte().setValue(pnd_Todays_Dte);                                                                                          //Natural: ASSIGN IAA-TIAA-FUND.TIAA-XFR-ISS-DTE := #TODAYS-DTE
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Old_Cmpny_Fund().setValue("T1G");                                                                                                //Natural: ASSIGN IAA-TIAA-FUND.TIAA-OLD-CMPNY-FUND := 'T1G'
        ldaIaal420.getVw_iaa_Tiaa_Fund().insertDBRow();                                                                                                                   //Natural: STORE IAA-TIAA-FUND
        pnd_Return_Code.reset();                                                                                                                                          //Natural: RESET #RETURN-CODE
    }
    private void sub_Pnd_Ai_Contracts() throws Exception                                                                                                                  //Natural: #AI-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal162g.getVw_iaa_Cntrct_1().startDatabaseFind                                                                                                                //Natural: FIND ( 1 ) IAA-CNTRCT-1 WITH CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        (
        "FNR",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_From_Cntrct, WcType.WITH) },
        1
        );
        FNR:
        while (condition(ldaIaal162g.getVw_iaa_Cntrct_1().readNextRow("FNR")))
        {
            ldaIaal162g.getVw_iaa_Cntrct_1().setIfNotFoundControlFlag(false);
            ldaIaal162g.getVw_iaa_Cntrct_Trans().reset();                                                                                                                 //Natural: RESET IAA-CNTRCT-TRANS
            ldaIaal162g.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal162g.getVw_iaa_Cntrct_1());                                                                       //Natural: MOVE BY NAME IAA-CNTRCT-1 TO IAA-CNTRCT-TRANS
            ldaIaal162g.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Time);                                                                                               //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE = #TIME
            pnd_Date_Time_P.setValue(ldaIaal162g.getIaa_Cntrct_Trans_Trans_Dte());                                                                                        //Natural: ASSIGN #DATE-TIME-P = IAA-CNTRCT-TRANS.TRANS-DTE
            ldaIaal162g.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal162g.getIaa_Cntrct_Trans_Invrse_Trans_Dte()),                  //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
            ldaIaal162g.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE = #TIME
            ldaIaal162g.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID = '2'
            ldaIaal162g.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                   //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
            ldaIaal162g.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CNTRCT-TRANS
            pnd_Return_Code.reset();                                                                                                                                      //Natural: RESET #RETURN-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Ai_Cpr() throws Exception                                                                                                                        //Natural: #AI-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal420.getVw_iaa_Cpr().startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) IAA-CPR BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPR",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPR:
        while (condition(ldaIaal420.getVw_iaa_Cpr().readNextRow("CPR")))
        {
            if (condition(ldaIaal420.getIaa_Cpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Cpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF IAA-CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND IAA-CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal420.getVw_iaa_Cpr_Trans().reset();                                                                                                                 //Natural: RESET IAA-CPR-TRANS
                ldaIaal420.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal420.getVw_iaa_Cpr());                                                                             //Natural: MOVE BY NAME IAA-CPR TO IAA-CPR-TRANS
                ldaIaal420.getIaa_Cpr_Trans_Trans_Dte().setValue(pnd_Time);                                                                                               //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal420.getIaa_Cpr_Trans_Trans_Dte());                                                                                        //Natural: ASSIGN #DATE-TIME-P = IAA-CPR-TRANS.TRANS-DTE
                ldaIaal420.getIaa_Cpr_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal420.getIaa_Cpr_Trans_Invrse_Trans_Dte()), new                  //Natural: COMPUTE IAA-CPR-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal420.getIaa_Cpr_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN IAA-CPR-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal420.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal420.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                   //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal420.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CPR-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Update_Cpr() throws Exception                                                                                                                    //Natural: #UPDATE-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal420.getVw_iaa_Cpr().startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) IAA-CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "CP",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CP:
        while (condition(ldaIaal420.getVw_iaa_Cpr().readNextRow("CP")))
        {
            if (condition(ldaIaal420.getIaa_Cpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Cpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF IAA-CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND IAA-CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal420.getIaa_Cpr_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                 //Natural: ASSIGN IAA-CPR.LST-TRANS-DTE = #TIME
                ldaIaal420.getVw_iaa_Cpr().updateDBRow("CP");                                                                                                             //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Rate_Code_Para() throws Exception                                                                                                                //Natural: #RATE-CODE-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  040612 CHANGED THE FOLLOWING FROM 1:99 TO *
        pnd_W_Tiaa_Rate_Cde.getValue("*").reset();                                                                                                                        //Natural: RESET #W-TIAA-RATE-CDE ( * ) #K #NEGATIVE-AMOUNT #W-TIAA-RATE-DTE ( * ) #W-TIAA-PER-PAY-AMT ( * ) #W-TIAA-PER-DIV-AMT ( * )
        pnd_K.reset();
        pnd_Negative_Amount.reset();
        pnd_W_Tiaa_Rate_Dte.getValue("*").reset();
        pnd_W_Tiaa_Per_Pay_Amt.getValue("*").reset();
        pnd_W_Tiaa_Per_Div_Amt.getValue("*").reset();
        //*  040612 END
        pnd_Sub.setValue(ldaIaal420.getIaa_Tiaa_Fund_Count_Casttiaa_Rate_Data_Grp());                                                                                     //Natural: MOVE C*IAA-TIAA-FUND.TIAA-RATE-DATA-GRP TO #SUB
        FAS:                                                                                                                                                              //Natural: FOR #I = 1 TO #SUB
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Sub)); pnd_I.nadd(1))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Cde().getValue(pnd_I).equals(" ")))                                                                       //Natural: IF IAA-TIAA-FUND.TIAA-RATE-CDE ( #I ) EQ ' '
            {
                if (true) break FAS;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FAS. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Per_Pay_Amt), ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue(pnd_I).subtract(pnd_Gtd_Pmt_Grd.getValue(pnd_I))); //Natural: COMPUTE #PER-PAY-AMT = IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( #I ) - #GTD-PMT-GRD ( #I )
                pnd_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Per_Div_Amt), ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue(pnd_I).subtract(pnd_Dvd_Pmt_Grd.getValue(pnd_I))); //Natural: COMPUTE #PER-DIV-AMT = IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( #I ) - #DVD-PMT-GRD ( #I )
                short decideConditionsMet1008 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PER-PAY-AMT > 0 AND ( #PER-DIV-AMT > 0 OR #PER-DIV-AMT = 0 )
                if (condition((pnd_Per_Pay_Amt.greater(getZero()) && (pnd_Per_Div_Amt.greater(getZero()) || pnd_Per_Div_Amt.equals(getZero())))))
                {
                    decideConditionsMet1008++;
                    pnd_K.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #K
                    pnd_W_Tiaa_Rate_Cde.getValue(pnd_K).setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Cde().getValue(pnd_I));                                            //Natural: ASSIGN #W-TIAA-RATE-CDE ( #K ) := IAA-TIAA-FUND.TIAA-RATE-CDE ( #I )
                    pnd_W_Tiaa_Rate_Dte.getValue(pnd_K).setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Dte().getValue(pnd_I));                                            //Natural: ASSIGN #W-TIAA-RATE-DTE ( #K ) := IAA-TIAA-FUND.TIAA-RATE-DTE ( #I )
                    pnd_W_Tiaa_Per_Pay_Amt.getValue(pnd_K).setValue(pnd_Per_Pay_Amt);                                                                                     //Natural: ASSIGN #W-TIAA-PER-PAY-AMT ( #K ) := #PER-PAY-AMT
                    pnd_W_Tiaa_Per_Div_Amt.getValue(pnd_K).setValue(pnd_Per_Div_Amt);                                                                                     //Natural: ASSIGN #W-TIAA-PER-DIV-AMT ( #K ) := #PER-DIV-AMT
                }                                                                                                                                                         //Natural: WHEN #PER-PAY-AMT = 0 AND #PER-DIV-AMT = 0
                else if (condition(pnd_Per_Pay_Amt.equals(getZero()) && pnd_Per_Div_Amt.equals(getZero())))
                {
                    decideConditionsMet1008++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN #PER-PAY-AMT < 0 OR #PER-DIV-AMT < 0
                else if (condition(pnd_Per_Pay_Amt.less(getZero()) || pnd_Per_Div_Amt.less(getZero())))
                {
                    decideConditionsMet1008++;
                    pnd_Negative_Amount.setValue("Y");                                                                                                                    //Natural: MOVE 'Y' TO #NEGATIVE-AMOUNT
                }                                                                                                                                                         //Natural: WHEN #PER-PAY-AMT NOT > 0 AND #PER-DIV-AMT > 0
                else if (condition(pnd_Per_Pay_Amt.lessOrEqual(getZero()) && pnd_Per_Div_Amt.greater(getZero())))
                {
                    decideConditionsMet1008++;
                    pnd_Negative_Amount.setValue("Y");                                                                                                                    //Natural: MOVE 'Y' TO #NEGATIVE-AMOUNT
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Cde().getValue("*").setValue(pnd_W_Tiaa_Rate_Cde.getValue("*"));                                                            //Natural: ASSIGN IAA-TIAA-FUND.TIAA-RATE-CDE ( * ) := #W-TIAA-RATE-CDE ( * )
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Dte().getValue("*").setValue(pnd_W_Tiaa_Rate_Dte.getValue("*"));                                                            //Natural: ASSIGN IAA-TIAA-FUND.TIAA-RATE-DTE ( * ) := #W-TIAA-RATE-DTE ( * )
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue("*").setValue(pnd_W_Tiaa_Per_Pay_Amt.getValue("*"));                                                      //Natural: ASSIGN IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( * ) := #W-TIAA-PER-PAY-AMT ( * )
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue("*").setValue(pnd_W_Tiaa_Per_Div_Amt.getValue("*"));                                                      //Natural: ASSIGN IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( * ) := #W-TIAA-PER-DIV-AMT ( * )
        //*  040612 END
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt().reset();                                                                                                           //Natural: RESET IAA-TIAA-FUND.TIAA-TOT-PER-AMT IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt().reset();
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt().nadd(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue("*"));                                                //Natural: ADD IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( * ) TO IAA-TIAA-FUND.TIAA-TOT-PER-AMT
        ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt().nadd(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue("*"));                                                //Natural: ADD IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( * ) TO IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
    }
    private void sub_Pnd_Update_Tiaa_Fund_To() throws Exception                                                                                                           //Natural: #UPDATE-TIAA-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1W",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1W:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1W")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Lst_Xfr_In_Dte().equals(pnd_Todays_Dte) || ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Lst_Xfr_Out_Dte().equals(pnd_Todays_Dte))) //Natural: IF IAA-TIAA-FUND.TIAA-LST-XFR-IN-DTE = #TODAYS-DTE OR IAA-TIAA-FUND.TIAA-LST-XFR-OUT-DTE = #TODAYS-DTE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Old_Per_Amt().setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt());                                              //Natural: ASSIGN IAA-TIAA-FUND.TIAA-OLD-PER-AMT := IAA-TIAA-FUND.TIAA-TOT-PER-AMT
                    ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Old_Div_Amt().setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt());                                              //Natural: ASSIGN IAA-TIAA-FUND.TIAA-OLD-DIV-AMT := IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt().nadd(pnd_Gtd_Pmt_Std.getValue("*"));                                                                       //Natural: ADD #GTD-PMT-STD ( * ) TO IAA-TIAA-FUND.TIAA-TOT-PER-AMT
                //*  040612
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt().nadd(pnd_Dvd_Pmt_Std.getValue("*"));                                                                       //Natural: ADD #DVD-PMT-STD ( * ) TO IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
                FQ:                                                                                                                                                       //Natural: FOR #I = 1 TO #MAX-RATE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rate)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Rate_Code_Std.getValue(pnd_I).greater(" ")))                                                                                        //Natural: IF #RATE-CODE-STD ( #I ) > ' '
                    {
                        pnd_Found_Rate.reset();                                                                                                                           //Natural: RESET #FOUND-RATE
                        //*  040612
                        pnd_Rate_Code.setValue(pnd_Rate_Code_Std.getValue(pnd_I));                                                                                        //Natural: MOVE #RATE-CODE-STD ( #I ) TO #RATE-CODE
                        FM:                                                                                                                                               //Natural: FOR #C = 1 TO #MAX-RATE
                        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_Max_Rate)); pnd_C.nadd(1))
                        {
                            pnd_Tiaa_Rate_Code.setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Cde().getValue(pnd_C));                                                     //Natural: MOVE IAA-TIAA-FUND.TIAA-RATE-CDE ( #C ) TO #TIAA-RATE-CODE
                            if (condition(pnd_Tiaa_Rate_Code.equals(" ")))                                                                                                //Natural: IF #TIAA-RATE-CODE = ' '
                            {
                                if (true) break FM;                                                                                                                       //Natural: ESCAPE BOTTOM ( FM. )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Tiaa_Rate_Code.equals(pnd_Rate_Code_Pnd_Rate_Code_A)))                                                                      //Natural: IF #TIAA-RATE-CODE = #RATE-CODE-A
                            {
                                                                                                                                                                          //Natural: PERFORM #WEIGHTED-AVERAGE-98
                                sub_Pnd_Weighted_Average_98();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FM"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FM"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue(pnd_C).nadd(pnd_Gtd_Pmt_Std.getValue(pnd_I));                                     //Natural: ADD #GTD-PMT-STD ( #I ) TO IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( #C )
                                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue(pnd_C).nadd(pnd_Dvd_Pmt_Std.getValue(pnd_I));                                     //Natural: ADD #DVD-PMT-STD ( #I ) TO IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( #C )
                                pnd_Found_Rate.setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO #FOUND-RATE
                                if (true) break FM;                                                                                                                       //Natural: ESCAPE BOTTOM ( FM. )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FQ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FQ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Found_Rate.equals(" ")))                                                                                                        //Natural: IF #FOUND-RATE = ' '
                        {
                            ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Cde().getValue(pnd_C).setValue(pnd_Rate_Code_Std.getValue(pnd_I));                                      //Natural: ASSIGN IAA-TIAA-FUND.TIAA-RATE-CDE ( #C ) := #RATE-CODE-STD ( #I )
                                                                                                                                                                          //Natural: PERFORM #WEIGHTED-AVERAGE-98
                            sub_Pnd_Weighted_Average_98();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FQ"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FQ"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue(pnd_C).setValue(pnd_Gtd_Pmt_Std.getValue(pnd_I));                                     //Natural: ASSIGN IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( #C ) := #GTD-PMT-STD ( #I )
                            ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue(pnd_C).setValue(pnd_Dvd_Pmt_Std.getValue(pnd_I));                                     //Natural: ASSIGN IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( #C ) := #DVD-PMT-STD ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  040612
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1W"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1W"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal420.getIaa_Tiaa_Fund_Lst_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN IAA-TIAA-FUND.LST-TRANS-DTE := #TIME
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                               //Natural: ASSIGN IAA-TIAA-FUND.TIAA-LST-XFR-IN-DTE := #TODAYS-DTE
                ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Old_Cmpny_Fund().setValue("T1G");                                                                                        //Natural: ASSIGN IAA-TIAA-FUND.TIAA-OLD-CMPNY-FUND := 'T1G'
                ldaIaal420.getVw_iaa_Tiaa_Fund().updateDBRow("R1W");                                                                                                      //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Re_Ai() throws Exception                                                                                                                   //Natural: #STORE-RE-AI
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal420.getVw_iaa_Cref_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-CREF-FUND BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1O",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1O:
        while (condition(ldaIaal420.getVw_iaa_Cref_Fund().readNextRow("R1O")))
        {
            if (condition(ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Cref_Fund_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal420.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal420.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal420.getVw_iaa_Cref_Fund());                                                                 //Natural: MOVE BY NAME IAA-CREF-FUND TO IAA-CREF-FUND-TRANS
                ldaIaal420.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal420.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal420.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal420.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal420.getIaa_Cref_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal420.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal420.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal420.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1O;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1O. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Weighted_Average_98() throws Exception                                                                                                           //Natural: #WEIGHTED-AVERAGE-98
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Dte().getValue(pnd_C).greater(getZero())))                                                                    //Natural: IF IAA-TIAA-FUND.TIAA-RATE-DTE ( #C ) GT 0
        {
            pnd_Frst_Pymnt_Curr_Dte.setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Dte().getValue(pnd_C));                                                                //Natural: ASSIGN #FRST-PYMNT-CURR-DTE := IAA-TIAA-FUND.TIAA-RATE-DTE ( #C )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Frst_Pymnt_Curr_Dte.setValue(pnd_Datd);                                                                                                                   //Natural: ASSIGN #FRST-PYMNT-CURR-DTE := #DATD
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Nazn6033.class , getCurrentProcessState(), ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Dte().getValue(pnd_C), pnd_Frst_Pymnt_Curr_Dte,                  //Natural: CALLNAT 'NAZN6033' IAA-TIAA-FUND.TIAA-RATE-DTE ( #C ) #FRST-PYMNT-CURR-DTE #EFFECTIVE-DATE IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( #C ) IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( #C ) #GTD-PMT-STD ( #I ) #DVD-PMT-STD ( #I ) IAA-CNTRCT-1.CNTRCT-OPTN-CDE IAA-CNTRCT-1.CNTRCT-ISSUE-DTE
            pnd_Effective_Date, ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue(pnd_C), ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue(pnd_C), 
            pnd_Gtd_Pmt_Std.getValue(pnd_I), pnd_Dvd_Pmt_Std.getValue(pnd_I), ldaIaal162g.getIaa_Cntrct_1_Cntrct_Optn_Cde(), ldaIaal162g.getIaa_Cntrct_1_Cntrct_Issue_Dte());
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
