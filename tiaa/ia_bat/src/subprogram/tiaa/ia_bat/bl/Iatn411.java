/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:44:46 AM
**        * FROM NATURAL SUBPROGRAM : Iatn411
************************************************************
**        * FILE NAME            : Iatn411.java
**        * CLASS NAME           : Iatn411
**        * INSTANCE NAME        : Iatn411
************************************************************
************************************************************************
*                  SWITCH REQUEST CONFIRMATION LETTER
*                  -----------------------------------
*  SUBPROG :- IATN411
*  AUTHOR  :- LEN B
*  DATE    :- OCT '97
*  DESC    :- THIS MODULE INTERFACES WITH THE POST SYSTEM TO PRODUCE
*             CONFIRMATION LETTERS FOR THE IA TRANSFER REQUESTS.
*             THIS SUBPROG IS CALLED BY IATN402.
*             THIS SUBPROG CALLS FOLLOWING MODULES;
*
*          1. IATN216  - PRODUCT INFORMATION
*          2. NAZN553  - CONVERSION OF NAME TO LOWER CASE
*          3. PSTN9610 - POST OPEN
*          4. PSTN9502 - POST WRITE
*          5. PSTN9680 - POST CLOSE
*
*  NOTE    :- THIS SUBPROG PERFORMS AN ET OR BT.
*
*  HISTORY :-
*  -------
* 3/25/99  MADE CHANGE IN WRITE-FYI-REC (ROUTINE)
*          & COMMENTED CODE TO PREVENT FYI'S FROM BEING USED
*          DO SCAN ON 3/99 FOR CHANGES
* 8/01/99  KN CHANGED MIT  STATUS CODE
*             CHANGE CONTACT METHOD TO ALWAYS BE "V"
* 05/01/02 TD ADDED #ACCT-TCKR IN IATN216 PARM
* 02/20/09 OS TIAA ACCESS CHANGES. SC 022009.
* 04/22/09    PUT A DASH (-) BETEWEEN CONTRACT AND 'REA' AS
*             REQUESTED BY LIZETTE.
* 08/29/10    POPULATED CN-PROCESSING-DATE. GGG-08/10
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/20170
* 04/2017  O SOTTO PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn411 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl410p pdaIatl410p;
    private PdaIatl400p pdaIatl400p;
    private LdaPstl6165 ldaPstl6165;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Xfr_Audit;
    private DbsField iaa_Xfr_Audit_Rcrd_Type_Cde;
    private DbsField iaa_Xfr_Audit_Rqst_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Wpid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_In_Progress_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Retry_Cnt;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Asset_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cycle_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Acctng_Dte;
    private DbsField iaa_Xfr_Audit_Lst_Chnge_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;

    private DbsGroup pnd_Iatn0201_Out;
    private DbsField pnd_Iatn0201_Out_Pnd_Post_Fund_N2;

    private DbsGroup pnd_Nazn6032_Io;
    private DbsField pnd_Nazn6032_Io_Pnd_Next_Pymnt_Dte;
    private DbsField pnd_Nazn6032_Io_Pnd_Ia_Updte_Dte;
    private DbsField pnd_Nazn6032_Io_Pnd_Mode;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Tiaa_Std;
    private DbsField pnd_Const_Pnd_Tiaa_Grad;
    private DbsField pnd_Const_Pnd_Tiaa_Re;
    private DbsField pnd_Const_Pnd_Tiaa_Acc;
    private DbsField pnd_Const_Pnd_Annual;
    private DbsField pnd_Const_Pnd_Yearly;
    private DbsField pnd_Const_Pnd_Monthly;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Sw;
    private DbsField pnd_Const_Pnd_Percent;
    private DbsField pnd_Const_Pnd_Dollars;
    private DbsField pnd_Const_Pnd_Units;
    private DbsField pnd_Const_Pnd_Mit_Status;
    private DbsField pnd_Const_Pnd_Fund_Cde_Std;
    private DbsField pnd_Const_Pnd_Fund_Cde_Grded;
    private DbsField pnd_Const_Pnd_Fund_Cde_Stock;
    private DbsField pnd_Const_Pnd_Fund_Cde_Mma;
    private DbsField pnd_Const_Pnd_Fund_Cde_Social;
    private DbsField pnd_Const_Pnd_Fund_Cde_Bond;
    private DbsField pnd_Const_Pnd_Fund_Cde_Global;
    private DbsField pnd_Const_Pnd_Fund_Cde_Equity;
    private DbsField pnd_Const_Pnd_Fund_Cde_Growth;
    private DbsField pnd_Const_Pnd_Fund_Cde_Rea;
    private DbsField pnd_Const_Pnd_Fund_Cde_Infl;
    private DbsField pnd_Const_Pnd_Fund_Cde_Acc;
    private DbsField pnd_Trans_Type;

    private DbsGroup pnd_Trans_Type__R_Field_2;
    private DbsField pnd_Trans_Type_Pnd_From_Grad;
    private DbsField pnd_Trans_Type_Pnd_From_Std;
    private DbsField pnd_Trans_Type_Pnd_From_Re;
    private DbsField pnd_Trans_Type_Pnd_From_Acc;
    private DbsField pnd_Trans_Type_Pnd_From_Cref;
    private DbsField pnd_Trans_Type_Pnd_To_Grad;
    private DbsField pnd_Trans_Type_Pnd_To_Std;
    private DbsField pnd_Trans_Type_Pnd_To_Re;
    private DbsField pnd_Trans_Type_Pnd_To_Acc;
    private DbsField pnd_Trans_Type_Pnd_To_Cref;
    private DbsField pnd_Ws_Todays_Dte;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_3;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Date_Yyyymmdd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_4;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyy;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Dd;
    private DbsField pnd_Issue_Date;

    private DbsGroup pnd_Issue_Date__R_Field_5;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Yyyy_Mm;

    private DbsGroup pnd_Issue_Date__R_Field_6;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Mm;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Dd;
    private DbsField pnd_Mod_Ts;

    private DbsGroup pnd_Mod_Ts__R_Field_7;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Mm;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Dd;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Yyyy;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Time;

    private DbsGroup pnd_Mod_Ts__R_Field_8;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Hrs;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Mins;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Secs;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Am;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Appl_Key_Counter;
    private DbsField pnd_Prod_Code_N;
    private DbsField pnd_Name;
    private DbsField pnd_Post_Error;
    private DbsField pnd_Var_Trad_Both;

    private DbsGroup iatn216_Data_Area;
    private DbsField iatn216_Data_Area_Pnd_Prod_Ct;
    private DbsField iatn216_Data_Area_Pnd_Prod_Cd;
    private DbsField iatn216_Data_Area_Pnd_Acct_Nme_5;
    private DbsField iatn216_Data_Area_Pnd_Acct_Tckr;
    private DbsField iatn216_Data_Area_Pnd_Acct_Effctve_Dte;
    private DbsField iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind;
    private DbsField iatn216_Data_Area_Pnd_Prod_Cd_N;
    private DbsField iatn216_Data_Area_Pnd_Acct_Rpt_Seq;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIatl400p = new PdaIatl400p(localVariables);
        ldaPstl6165 = new LdaPstl6165();
        registerRecord(ldaPstl6165);
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl410p = new PdaIatl410p(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_iaa_Xfr_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Audit", "IAA-XFR-AUDIT"), "IAA_XFR_AUDIT", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_AUDIT"));
        iaa_Xfr_Audit_Rcrd_Type_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Xfr_Audit_Rqst_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id", "IAXFR-CALC-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIQUE_ID");
        iaa_Xfr_Audit_Iaxfr_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Effctve_Dte", "IAXFR-EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte", "IAXFR-RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme", "IAXFR-RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte", "IAXFR-INVRSE-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "IAXFR_INVRSE_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme", "IAXFR-INVRSE-RCVD-TME", 
            FieldType.NUMERIC, 14, RepeatingFieldStrategy.None, "IAXFR_INVRSE_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Entry_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Dte", "IAXFR-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ENTRY_DTE");
        iaa_Xfr_Audit_Iaxfr_Entry_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Tme", "IAXFR-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_ENTRY_TME");
        iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr", "IAXFR-CALC-STTMNT-INDCTR", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_STTMNT_INDCTR");
        iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde", "IAXFR-CALC-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_STATUS_CDE");
        iaa_Xfr_Audit_Iaxfr_Cwf_Wpid = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Wpid", "IAXFR-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IAXFR_CWF_WPID");
        iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time", "IAXFR-CWF-LOG-DTE-TIME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "IAXFR_CWF_LOG_DTE_TIME");
        iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr", "IAXFR-FROM-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IAXFR_FROM_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_From_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Payee_Cde", "IAXFR-FROM-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_FROM_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde", "IAXFR-CALC-REJECT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_REJECT_CDE");
        iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind", "IAXFR-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IAXFR_OPN_CLSD_IND");
        iaa_Xfr_Audit_Iaxfr_In_Progress_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_In_Progress_Ind", "IAXFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "IAXFR_IN_PROGRESS_IND");
        iaa_Xfr_Audit_Iaxfr_Retry_Cnt = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Retry_Cnt", "IAXFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "IAXFR_RETRY_CNT");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data", 
            "C*IAXFR-CALC-FROM-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data", "IAXFR-CALC-FROM-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Fund_Cde", "IAXFR-FROM-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd", "IAXFR-FRM-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ", "IAXFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Typ", "IAXFR-FROM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Qty = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Qty", "IAXFR-FROM-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_QTY", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar", 
            "IAXFR-FROM-CURRENT-PMT-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid", 
            "IAXFR-FROM-CURRENT-PMT-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units", 
            "IAXFR-FROM-CURRENT-PMT-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val", 
            "IAXFR-FROM-CURRENT-PMT-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val", 
            "IAXFR-FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt", 
            "IAXFR-FROM-RQSTD-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units", 
            "IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar", 
            "IAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid", 
            "IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt", 
            "IAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_ASSET_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr", 
            "IAXFR-FROM-PMT-AFTR-XFR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_PMT_AFTR_XFR", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units", 
            "IAXFR-FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar", 
            "IAXFR-FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid", 
            "IAXFR-FROM-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data", 
            "C*IAXFR-CALC-TO-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Fund_Cde", "IAXFR-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Unit_Typ", "IAXFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec", "IAXFR-TO-NEW-FUND-REC", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_FUND_REC", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue", 
            "IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_PHYS_CNTRCT_ISSUE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Typ", "IAXFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Qty = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Qty", "IAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_QTY", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar", "IAXFR-TO-BFR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid", "IAXFR-TO-BFR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units", "IAXFR-TO-BFR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val", "IAXFR-TO-REVAL-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Units", "IAXFR-TO-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar", "IAXFR-TO-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid", "IAXFR-TO-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Asset_Amt = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Asset_Amt", "IAXFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units", "IAXFR-TO-AFTR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar", "IAXFR-TO-AFTR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid", "IAXFR-TO-AFTR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr", "IAXFR-CALC-TO-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde", "IAXFR-CALC-TO-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind", 
            "IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NEW_ISSUE_IND");
        iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte", "IAXFR-CALC-UNIT-VAL-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIT_VAL_DTE");
        iaa_Xfr_Audit_Iaxfr_Cycle_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cycle_Dte", "IAXFR-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CYCLE_DTE");
        iaa_Xfr_Audit_Iaxfr_Acctng_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Acctng_Dte", "IAXFR-ACCTNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ACCTNG_DTE");
        iaa_Xfr_Audit_Lst_Chnge_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_CHNGE_DTE");
        registerRecord(vw_iaa_Xfr_Audit);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        registerRecord(vw_iaa_Cntrct);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Iatn0201_Out = localVariables.newGroupInRecord("pnd_Iatn0201_Out", "#IATN0201-OUT");
        pnd_Iatn0201_Out_Pnd_Post_Fund_N2 = pnd_Iatn0201_Out.newFieldInGroup("pnd_Iatn0201_Out_Pnd_Post_Fund_N2", "#POST-FUND-N2", FieldType.NUMERIC, 
            2);

        pnd_Nazn6032_Io = localVariables.newGroupInRecord("pnd_Nazn6032_Io", "#NAZN6032-IO");
        pnd_Nazn6032_Io_Pnd_Next_Pymnt_Dte = pnd_Nazn6032_Io.newFieldInGroup("pnd_Nazn6032_Io_Pnd_Next_Pymnt_Dte", "#NEXT-PYMNT-DTE", FieldType.DATE);
        pnd_Nazn6032_Io_Pnd_Ia_Updte_Dte = pnd_Nazn6032_Io.newFieldInGroup("pnd_Nazn6032_Io_Pnd_Ia_Updte_Dte", "#IA-UPDTE-DTE", FieldType.STRING, 8);
        pnd_Nazn6032_Io_Pnd_Mode = pnd_Nazn6032_Io.newFieldInGroup("pnd_Nazn6032_Io_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Tiaa_Std = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Std", "#TIAA-STD", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Grad = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Grad", "#TIAA-GRAD", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Re = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Re", "#TIAA-RE", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Acc = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Acc", "#TIAA-ACC", FieldType.STRING, 1);
        pnd_Const_Pnd_Annual = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Annual", "#ANNUAL", FieldType.STRING, 1);
        pnd_Const_Pnd_Yearly = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Yearly", "#YEARLY", FieldType.STRING, 1);
        pnd_Const_Pnd_Monthly = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Monthly", "#MONTHLY", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Sw = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Sw", "#RCRD-TYPE-SW", FieldType.STRING, 1);
        pnd_Const_Pnd_Percent = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Percent", "#PERCENT", FieldType.STRING, 1);
        pnd_Const_Pnd_Dollars = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Dollars", "#DOLLARS", FieldType.STRING, 1);
        pnd_Const_Pnd_Units = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Units", "#UNITS", FieldType.STRING, 1);
        pnd_Const_Pnd_Mit_Status = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Mit_Status", "#MIT-STATUS", FieldType.STRING, 4);
        pnd_Const_Pnd_Fund_Cde_Std = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Std", "#FUND-CDE-STD", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Grded = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Grded", "#FUND-CDE-GRDED", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Stock = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Stock", "#FUND-CDE-STOCK", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Mma = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Mma", "#FUND-CDE-MMA", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Social = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Social", "#FUND-CDE-SOCIAL", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Bond = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Bond", "#FUND-CDE-BOND", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Global = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Global", "#FUND-CDE-GLOBAL", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Equity = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Equity", "#FUND-CDE-EQUITY", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Growth = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Growth", "#FUND-CDE-GROWTH", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Rea = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Rea", "#FUND-CDE-REA", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Infl = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Infl", "#FUND-CDE-INFL", FieldType.STRING, 2);
        pnd_Const_Pnd_Fund_Cde_Acc = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Fund_Cde_Acc", "#FUND-CDE-ACC", FieldType.STRING, 2);
        pnd_Trans_Type = localVariables.newFieldInRecord("pnd_Trans_Type", "#TRANS-TYPE", FieldType.STRING, 10);

        pnd_Trans_Type__R_Field_2 = localVariables.newGroupInRecord("pnd_Trans_Type__R_Field_2", "REDEFINE", pnd_Trans_Type);
        pnd_Trans_Type_Pnd_From_Grad = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_From_Grad", "#FROM-GRAD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Std = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_From_Std", "#FROM-STD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Re = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_From_Re", "#FROM-RE", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Acc = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_From_Acc", "#FROM-ACC", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Cref = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_From_Cref", "#FROM-CREF", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Grad = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_To_Grad", "#TO-GRAD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Std = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_To_Std", "#TO-STD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Re = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_To_Re", "#TO-RE", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Acc = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_To_Acc", "#TO-ACC", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Cref = pnd_Trans_Type__R_Field_2.newFieldInGroup("pnd_Trans_Type_Pnd_To_Cref", "#TO-CREF", FieldType.STRING, 1);
        pnd_Ws_Todays_Dte = localVariables.newFieldInRecord("pnd_Ws_Todays_Dte", "#WS-TODAYS-DTE", FieldType.DATE);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_3", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_3.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Date_Yyyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_4", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyy = pnd_Date_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Yyyymmdd_Pnd_Date_Mm = pnd_Date_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Yyyymmdd_Pnd_Date_Dd = pnd_Date_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Issue_Date = localVariables.newFieldInRecord("pnd_Issue_Date", "#ISSUE-DATE", FieldType.NUMERIC, 8);

        pnd_Issue_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Issue_Date__R_Field_5", "REDEFINE", pnd_Issue_Date);
        pnd_Issue_Date_Pnd_Issue_Date_Yyyy_Mm = pnd_Issue_Date__R_Field_5.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Yyyy_Mm", "#ISSUE-DATE-YYYY-MM", 
            FieldType.NUMERIC, 6);

        pnd_Issue_Date__R_Field_6 = pnd_Issue_Date__R_Field_5.newGroupInGroup("pnd_Issue_Date__R_Field_6", "REDEFINE", pnd_Issue_Date_Pnd_Issue_Date_Yyyy_Mm);
        pnd_Issue_Date_Pnd_Issue_Date_Yyyy = pnd_Issue_Date__R_Field_6.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Issue_Date_Pnd_Issue_Date_Mm = pnd_Issue_Date__R_Field_6.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_Pnd_Issue_Date_Dd = pnd_Issue_Date__R_Field_5.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Mod_Ts = localVariables.newFieldInRecord("pnd_Mod_Ts", "#MOD-TS", FieldType.NUMERIC, 15);

        pnd_Mod_Ts__R_Field_7 = localVariables.newGroupInRecord("pnd_Mod_Ts__R_Field_7", "REDEFINE", pnd_Mod_Ts);
        pnd_Mod_Ts_Pnd_Mod_Date_Mm = pnd_Mod_Ts__R_Field_7.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Mm", "#MOD-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Date_Dd = pnd_Mod_Ts__R_Field_7.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Dd", "#MOD-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Date_Yyyy = pnd_Mod_Ts__R_Field_7.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Yyyy", "#MOD-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Mod_Ts_Pnd_Mod_Time = pnd_Mod_Ts__R_Field_7.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Time", "#MOD-TIME", FieldType.NUMERIC, 7);

        pnd_Mod_Ts__R_Field_8 = pnd_Mod_Ts__R_Field_7.newGroupInGroup("pnd_Mod_Ts__R_Field_8", "REDEFINE", pnd_Mod_Ts_Pnd_Mod_Time);
        pnd_Mod_Ts_Pnd_Mod_Hrs = pnd_Mod_Ts__R_Field_8.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Hrs", "#MOD-HRS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Mins = pnd_Mod_Ts__R_Field_8.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Mins", "#MOD-MINS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Secs = pnd_Mod_Ts__R_Field_8.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Secs", "#MOD-SECS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Am = pnd_Mod_Ts__R_Field_8.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Am", "#MOD-AM", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 2);
        pnd_Appl_Key_Counter = localVariables.newFieldInRecord("pnd_Appl_Key_Counter", "#APPL-KEY-COUNTER", FieldType.NUMERIC, 5);
        pnd_Prod_Code_N = localVariables.newFieldArrayInRecord("pnd_Prod_Code_N", "#PROD-CODE-N", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 50);
        pnd_Post_Error = localVariables.newFieldInRecord("pnd_Post_Error", "#POST-ERROR", FieldType.STRING, 1);
        pnd_Var_Trad_Both = localVariables.newFieldInRecord("pnd_Var_Trad_Both", "#VAR-TRAD-BOTH", FieldType.STRING, 1);

        iatn216_Data_Area = localVariables.newGroupInRecord("iatn216_Data_Area", "IATN216-DATA-AREA");
        iatn216_Data_Area_Pnd_Prod_Ct = iatn216_Data_Area.newFieldInGroup("iatn216_Data_Area_Pnd_Prod_Ct", "#PROD-CT", FieldType.PACKED_DECIMAL, 3);
        iatn216_Data_Area_Pnd_Prod_Cd = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Prod_Cd", "#PROD-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        iatn216_Data_Area_Pnd_Acct_Nme_5 = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 
            6, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Tckr = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Tckr", "#ACCT-TCKR", FieldType.STRING, 10, 
            new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Effctve_Dte = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", 
            FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Prod_Cd_N = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Prod_Cd_N", "#PROD-CD-N", FieldType.NUMERIC, 2, 
            new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Rpt_Seq = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Xfr_Audit.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct.reset();

        ldaPstl6165.initializeValues();

        localVariables.reset();
        pnd_Const_Pnd_Tiaa_Std.setInitialValue("T");
        pnd_Const_Pnd_Tiaa_Grad.setInitialValue("G");
        pnd_Const_Pnd_Tiaa_Re.setInitialValue("R");
        pnd_Const_Pnd_Tiaa_Acc.setInitialValue("D");
        pnd_Const_Pnd_Annual.setInitialValue("A");
        pnd_Const_Pnd_Yearly.setInitialValue("Y");
        pnd_Const_Pnd_Monthly.setInitialValue("M");
        pnd_Const_Pnd_Rcrd_Type_Sw.setInitialValue("2");
        pnd_Const_Pnd_Percent.setInitialValue("P");
        pnd_Const_Pnd_Dollars.setInitialValue("D");
        pnd_Const_Pnd_Units.setInitialValue("U");
        pnd_Const_Pnd_Mit_Status.setInitialValue("8022");
        pnd_Const_Pnd_Fund_Cde_Std.setInitialValue("1S");
        pnd_Const_Pnd_Fund_Cde_Grded.setInitialValue("1G");
        pnd_Const_Pnd_Fund_Cde_Stock.setInitialValue("02");
        pnd_Const_Pnd_Fund_Cde_Mma.setInitialValue("03");
        pnd_Const_Pnd_Fund_Cde_Social.setInitialValue("04");
        pnd_Const_Pnd_Fund_Cde_Bond.setInitialValue("05");
        pnd_Const_Pnd_Fund_Cde_Global.setInitialValue("06");
        pnd_Const_Pnd_Fund_Cde_Equity.setInitialValue("07");
        pnd_Const_Pnd_Fund_Cde_Growth.setInitialValue("08");
        pnd_Const_Pnd_Fund_Cde_Rea.setInitialValue("09");
        pnd_Const_Pnd_Fund_Cde_Infl.setInitialValue("10");
        pnd_Const_Pnd_Fund_Cde_Acc.setInitialValue("11");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn411() throws Exception
    {
        super("Iatn411");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ======================================================================
        //*                        START OF SUB-PROGRAM
        //* ======================================================================
        pdaIatl410p.getPnd_Iatn410_Out().reset();                                                                                                                         //Natural: RESET #IATN410-OUT
        //*  ==================================
        //*  VALIDATE THE REQUIRED INPUT FIELDS
        //*  ==================================
        if (condition(pdaIatl410p.getPnd_Iatn410_In_Rqst_Id().equals(" ")))                                                                                               //Natural: IF #IATN410-IN.RQST-ID = ' '
        {
            pdaIatl410p.getPnd_Iatn410_Out_Pnd_Return_Code().setValue("E");                                                                                               //Natural: MOVE 'E' TO #IATN410-OUT.#RETURN-CODE
            pdaIatl410p.getPnd_Iatn410_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Request id is required. (", Global.getPROGRAM(),           //Natural: COMPRESS 'Request id is required. (' *PROGRAM ')' INTO #IATN410-OUT.#MSG LEAVING NO
                ")"));
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
            //*  FOR POST & EFM CONSTRUCT
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET CONTACT MOD:'C'=PHONE 'M'=MAIL 'V'=PERSONAL VISIT  'A'=ATS RQSTS
        //*   NOT (#IATN410-IN.RQST-CNTCT-MDE = 'C' OR = 'M' OR= 'V' OR= 'A' )
        //*  MOVE 'E'   TO #IATN410-OUT.#RETURN-CODE
        //*  COMPRESS 'Contract mode invalid. Valid values are "C","M","V" or "A".'
        //*    ' ('  *PROGRAM ')' INTO #IATN410-OUT.#MSG LEAVING NO
        //*  ESCAPE ROUTINE
        //*  D-IF
        //*  FOR PHONE REQUESTS THE REPS NAME IS REQUIRED. IE:- WHEN CNTCT-MDE="C"
        //*   #IATN410-IN.RQST-REP-NME = ' ' AND #IATN410-IN.RQST-CNTCT-MDE = 'C'
        //*  MOVE 'E'   TO #IATN410-OUT.#RETURN-CODE
        //*  COMPRESS 'Service Rep"s name is required when contact mode'
        //*    ' is Phone(C). (' *PROGRAM ')' INTO #IATN410-OUT.#MSG LEAVING NO
        //*  ESCAPE ROUTINE
        //*  D-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Iatn216.class , getCurrentProcessState(), iatn216_Data_Area);                                                                                     //Natural: CALLNAT 'IATN216' IATN216-DATA-AREA
        if (condition(Global.isEscape())) return;
        //*  ======================
        //*  START THE POST PROCESS
        //*  ======================
        vw_iaa_Xfr_Audit.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) IAA-XFR-AUDIT WITH IAA-XFR-AUDIT.RQST-ID = #IATN410-IN.RQST-ID
        (
        "FND1",
        new Wc[] { new Wc("RQST_ID", "=", pdaIatl410p.getPnd_Iatn410_In_Rqst_Id(), WcType.WITH) },
        1
        );
        FND1:
        while (condition(vw_iaa_Xfr_Audit.readNextRow("FND1", true)))
        {
            vw_iaa_Xfr_Audit.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Xfr_Audit.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORD FOUND
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RQST Id (", pdaIatl410p.getPnd_Iatn410_In_Rqst_Id(),  //Natural: COMPRESS 'RQST Id (' #IATN410-IN.RQST-ID ') not on Audit file. ' INTO ##MSG LEAVING NO SPACE
                    ") not on Audit file. "));
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(iaa_Xfr_Audit_Rcrd_Type_Cde.notEquals(pnd_Const_Pnd_Rcrd_Type_Sw)))                                                                             //Natural: IF IAA-XFR-AUDIT.RCRD-TYPE-CDE NE #RCRD-TYPE-SW
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Invalid Record Type. RCRD-TYPE-CDE must be Transfer (1)."));                         //Natural: COMPRESS 'Invalid Record Type. RCRD-TYPE-CDE must be Transfer (1).' INTO ##MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-OPEN
            sub_Call_Post_For_Open();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM GET-FROM-TO-TIAA-REA-CREF
            sub_Get_From_To_Tiaa_Rea_Cref();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CN-CONTRACT-STRUCT
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTRACT-REC
            sub_Write_Contract_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CN-FRM-REC-STRUCT
                                                                                                                                                                          //Natural: PERFORM WRITE-FROM-REC
            sub_Write_From_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHANGE
            //*   PERFORM WRITE-TO-REC                   /* CN-TO-ACCT-STRUCT
            //*  CN-ETF-STRUCT
                                                                                                                                                                          //Natural: PERFORM WRITE-EFFECT-OF-TRNSFR-FROM
            sub_Write_Effect_Of_Trnsfr_From();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CN-ETT-STRUCT
                                                                                                                                                                          //Natural: PERFORM WRITE-EFFECT-OF-TRNSFR-TO
            sub_Write_Effect_Of_Trnsfr_To();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CN-ANV-STRUCT
                                                                                                                                                                          //Natural: PERFORM WRITE-CONFIRM-ANNUITY-VAL
            sub_Write_Confirm_Annuity_Val();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CN-FYI-STRUCT
                                                                                                                                                                          //Natural: PERFORM WRITE-FYI-REC
            sub_Write_Fyi_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
            sub_Call_Post_For_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ET
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //* ********************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
            //*  FND1
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* ======================================================================
        //*                         START OF SUBROUTINES
        //* ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-OPEN
        //* *PSTA9611.PIN-NBR         := IAXFR-CALC-UNIQUE-ID
        //*  PSTA9611.PCKGE-CDE       := 'PFIAIL'
        //*  PSTA9611
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTRACT-REC
        //*  SET CONTACT MOD:'C'=PHONE 'M'=MAIL 'V'=PERSONAL VISIT  'A'=ATS REQUEST
        //*  CN-REQ-SOURCE :=  #IATN410-IN.RQST-CNTCT-MDE
        //*  CONVERT THE SERVICE REPS NAME TO LOWER CASE
        //*  MOVE #IATN410-IN.RQST-REP-NME TO #NAME
        //*  CALLNAT 'NAZN553' #NAME
        //*  MOVE #NAME TO CN-SERVICE-REP
        //*  CHANGE
        //*  TRANS TYPE P=PERCENT D=DOLLARS U=UNITS
        //*  CALL POST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FROM-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EFFECT-OF-TRNSFR-FROM
        //*  SET VARIABLE/TRADITIONAL/BOTH FIELD  (ALWAYS VARIABLE FOR SWITCHES)
        //*  SETTING THE ACCOUNT VALUES TO -1 MEANS WE DO NOT PULL THAT ACCOUNT
        //*  SET MONTHLY/YEARLY FIELD
        //*  SET THE "FROM" AND "TO" PAYMENTS AND UNITS
        //*      VALUE #FUND-CDE-GRDED
        //*        CN-ETF-ACCNT-GRDED-P(#J) := IAXFR-FROM-CURRENT-PMT-GUAR(#I) +
        //*          IAXFR-FROM-CURRENT-PMT-DIVID(#I)
        //*        CN-ETF-ACCNT-GRD-CON(#J) := IAXFR-FROM-CURRENT-PMT-GUAR(#I)
        //*        CN-ETF-ACCNT-GRD-DIV(#J) := IAXFR-FROM-CURRENT-PMT-DIVID(#I)
        //*      VALUE #FUND-CDE-STD
        //*        CN-ETF-ACCNT-STD-P(#J) := IAXFR-FROM-CURRENT-PMT-GUAR(#I)
        //*          + IAXFR-FROM-CURRENT-PMT-DIVID(#I)
        //*        CN-ETF-ACCNT-STD-CON(#J) := IAXFR-FROM-CURRENT-PMT-GUAR(#I)
        //*        CN-ETF-ACCNT-STD-DIV(#J) := IAXFR-FROM-CURRENT-PMT-DIVID(#I)
        //*      VALUE #FUND-CDE-GRDED
        //*        CN-ETF-ACCNT-GRDED-P(#J) := IAXFR-TO-BFR-XFR-GUAR(#I) +
        //*          IAXFR-TO-BFR-XFR-DIVID(#I)
        //*        CN-ETF-ACCNT-GRD-CON(#J) := IAXFR-TO-BFR-XFR-GUAR(#I)
        //*        CN-ETF-ACCNT-GRD-DIV(#J) := IAXFR-TO-BFR-XFR-DIVID(#I)
        //*      VALUE #FUND-CDE-STD
        //*        CN-ETF-ACCNT-STD-P(#J) := IAXFR-TO-BFR-XFR-GUAR(#I)
        //*          + IAXFR-TO-BFR-XFR-DIVID(#I)
        //*        CN-ETF-ACCNT-STD-CON(#J) := IAXFR-TO-BFR-XFR-GUAR(#I)
        //*        CN-ETF-ACCNT-STD-DIV(#J) := IAXFR-TO-BFR-XFR-DIVID(#I)
        //*  SET UP SORT KEY LEVEL 4
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EFFECT-OF-TRNSFR-TO
        //*  SET VARIABLE/TRADITIONAL/BOTH FIELD (ALWAYS VARIABLE FOR SWITCHES)
        //*  "999" IN A DATE FIELD MEANS THAT WE DO NOT PULL THAT PARAGRAPH
        //*  SETTING THE ACCOUNT VALUES TO -1 MEANS WE DO NOT PULL THAT ACCOUNT
        //*  SET MONTHLY/YEARLY FIELD
        //*  SET THE "TO" PAYMENTS AND UNITS
        //*      VALUE #FUND-CDE-GRDED
        //*        CN-ETT-ACCNT-GRDED-P(#J) := IAXFR-TO-AFTR-XFR-GUAR(#I) +
        //*          IAXFR-TO-AFTR-XFR-DIVID(#I)
        //*        CN-ETT-ACCNT-GRD-CON(#J) := IAXFR-TO-AFTR-XFR-GUAR(#I)
        //*        CN-ETT-ACCNT-GRD-DIV(#J) := IAXFR-TO-AFTR-XFR-DIVID(#I)
        //*      VALUE #FUND-CDE-STD
        //*        CN-ETT-ACCNT-STD-P(#J) := IAXFR-TO-AFTR-XFR-GUAR(#I)
        //*          + IAXFR-TO-AFTR-XFR-DIVID(#I)
        //*        CN-ETT-ACCNT-STD-CON(#J) := IAXFR-TO-AFTR-XFR-GUAR(#I)
        //*        CN-ETT-ACCNT-STD-DIV(#J) := IAXFR-TO-AFTR-XFR-DIVID(#I)
        //*      VALUE #FUND-CDE-GRDED
        //*        CN-ETT-ACCNT-GRDED-P(#J) := IAXFR-FROM-AFTR-XFR-GUAR(#I) +
        //*          IAXFR-FROM-AFTR-XFR-DIVID(#I)
        //*        CN-ETT-ACCNT-GRD-CON(#J) := IAXFR-FROM-AFTR-XFR-GUAR(#I)
        //*        CN-ETT-ACCNT-GRD-DIV(#J) := IAXFR-FROM-AFTR-XFR-DIVID(#I)
        //*      VALUE #FUND-CDE-STD
        //*        CN-ETT-ACCNT-STD-P(#J) := IAXFR-FROM-AFTR-XFR-GUAR(#I)
        //*          + IAXFR-FROM-AFTR-XFR-DIVID(#I)
        //*        CN-ETT-ACCNT-STD-CON(#J) := IAXFR-FROM-AFTR-XFR-GUAR(#I)
        //*        CN-ETT-ACCNT-STD-DIV(#J) := IAXFR-FROM-AFTR-XFR-DIVID(#I)
        //*  SET UP SORT KEY LEVEL 4
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONFIRM-ANNUITY-VAL
        //*  SETTING THE ACCOUNT VALUES TO -1 MEANS WE DO NOT PULL THAT ACCOUNT
        //*  SET THE ANNUITY VALUES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FYI-REC
        //*  ======================================================================
        //*  MSG = 05
        //*  CONDITION :- ALL
        //*  IF YOU HAVE ANY QUESTIONS, PLEASE CALL OUR TELEPHONE COUNSELING CEN...
        //*  ======================================================================
        //*  ======================================================================
        //*  MSG = 20
        //*  CONDITIONS:- 1. TRANSFER MONTHLY TO MONTHLY
        //*               2. TRANSFER AFTER 20TH AND BEFORE 1ST OF NEXT MONTH
        //*  ANNUITY PAYMENTS ARE CALCULATED ON THE 20TH OF EACH MONTH.............
        //*  ======================================================================
        //*  BEGIN OF COMMENTED LINES              3/99
        //*  IF IAXFR-FRM-UNIT-TYP(1) EQ #MONTHLY
        //*   MOVE EDITED IAXFR-EFFCTVE-DTE (EM=YYYYMMDD) TO #DATE-A
        //*   #DATE-YYYYMMDD := #DATE-N
        //*   IF #DATE-DD GT 20
        //*     #J := #J + 1
        //*     MOVE 20  TO CN-FYI-MSG-NUMBER(#J)
        //*     MOVE 020 TO CN-FYI-SRT-NUM(#J)
        //*   END-IF
        //*  END-IF
        //*  ======================================================================
        //*  MSG = 30
        //*  CONDITION:- TRANSFER ANNUAL TO ANNUAL
        //*  WHEN YOU TRANSFER AMONG VARIABLE ACCOUNTS THAT ARE REVALUED ANNUALLY..
        //*  ======================================================================
        //*  IF IAXFR-FRM-UNIT-TYP(1) EQ #ANNUAL
        //*   #J := #J + 1
        //*   MOVE 30  TO CN-FYI-MSG-NUMBER(#J)
        //*   MOVE 030 TO CN-FYI-SRT-NUM(#J)
        //*   MOVE EDITED IAXFR-EFFCTVE-DTE (EM=YYYYMMDD) TO #DATE-A
        //*   #DATE-YYYYMMDD := #DATE-N
        //*   IF #DATE-MM LT 04
        //*     CN-FYI-DATE1-MM(#J) := 04
        //*     CN-FYI-DATE1-DD(#J) := 01
        //*     CN-FYI-DATE1-YY(#J) := #DATE-YYYY
        //*   CN-FYI-DATE2-MM(#J) := 05
        //*    CN-FYI-DATE2-DD(#J) := 01
        //*    CN-FYI-DATE2-YY(#J) := #DATE-YYYY
        //*    CN-FYI-DATE3-MM(#J) := 04
        //*    CN-FYI-DATE3-DD(#J) := 01
        //*    COMPUTE CN-FYI-DATE3-YY(#J)  = #DATE-YYYY - 1
        //*    CN-FYI-DATE4-MM(#J) := 03
        //*    CN-FYI-DATE4-DD(#J) := 31
        //*    CN-FYI-DATE4-YY(#J) := #DATE-YYYY
        //*  ELSE
        //*    CN-FYI-DATE1-MM(#J) := 04
        //*    CN-FYI-DATE1-DD(#J) := 01
        //*    COMPUTE  CN-FYI-DATE1-YY(#J) = #DATE-YYYY + 1
        //*    CN-FYI-DATE2-MM(#J) := 05
        //*    CN-FYI-DATE2-DD(#J) := 01
        //*    COMPUTE CN-FYI-DATE2-YY(#J)  = #DATE-YYYY + 1
        //*    CN-FYI-DATE3-MM(#J) := 04
        //*    CN-FYI-DATE3-DD(#J) := 01
        //*    CN-FYI-DATE3-YY(#J) := #DATE-YYYY
        //*    CN-FYI-DATE4-MM(#J) := 03
        //*    CN-FYI-DATE4-DD(#J) := 31
        //*    COMPUTE CN-FYI-DATE4-YY(#J)  = #DATE-YYYY + 1
        //*  END-IF
        //*  FIND(1) IAA-CNTRCT WITH
        //*    CNTRCT-PPCN-NBR = IAXFR-FROM-PPCN-NBR
        //*    IF NO RECORD FOUND
        //*      ESCAPE BOTTOM
        //*    END-NOREC
        //*    #ISSUE-DATE-YYYY-MM := CNTRCT-ISSUE-DTE
        //*    IF CNTRCT-ISSUE-DTE-DD = 0
        //*      #ISSUE-DATE-DD := 01
        //*    ELSE
        //*      #ISSUE-DATE-DD := CNTRCT-ISSUE-DTE-DD
        //*    END-IF
        //*  END-FIND
        //*  IF #ISSUE-DATE-YYYY GE CN-FYI-DATE3-YY(#J)
        //*      AND #ISSUE-DATE-MM GE CN-FYI-DATE3-MM(#J)
        //*      AND #ISSUE-DATE-DD GT CN-FYI-DATE3-DD(#J)
        //*    CN-FYI-DATE3-MM(#J) := #ISSUE-DATE-MM
        //*    CN-FYI-DATE3-DD(#J) := #ISSUE-DATE-DD
        //*    CN-FYI-DATE3-YY(#J) := #ISSUE-DATE-YYYY
        //*  END-IF
        //*  END-IF
        //*  ======================================================================
        //*  MSG = 35
        //*  CONDITION:-TRANSFER FROM ANY VARIABLE ACCOUNT TO TIAA TRADITIONAL
        //*  WHEN YOU TRANSFER FROM A VARIABLE ACCOUNT TO TIAA TRADITIONAL, YOU...
        //*  ======================================================================
        //*  IF (#FROM-RE = 'Y' OR #FROM-CREF = 'Y')
        //*     AND ( #TO-STD = 'Y' OR #TO-GRAD = 'Y')
        //*   #J := #J + 1
        //*   MOVE 35  TO CN-FYI-MSG-NUMBER(#J)
        //*   MOVE 035 TO CN-FYI-SRT-NUM(#J)
        //*  END-IF
        //*  ======================================================================
        //*  MSG = 40
        //*  CONDITION:-TRANSFER TO TIAA STANDARD
        //*  TRANSFERS TO TIAA TRADITIONAL USING THE STANDARD PAYMENT METHOD ARE...
        //*  ======================================================================
        //*  IF #TO-STD = 'Y'
        //*   #J := #J + 1
        //*   MOVE 40  TO CN-FYI-MSG-NUMBER(#J)
        //*   MOVE 040 TO CN-FYI-SRT-NUM(#J)
        //*  END-IF
        //*  ======================================================================
        //*  MSG = 45
        //*  CONDITION:- TRANSFER TO TIAA GRADED
        //*  I YOU TRANSFER FROM A VARIABLE ACCOUNT TO TIAA TRADITIONAL, YOU...
        //*  ======================================================================
        //*  IF #TO-GRAD = 'Y'
        //*   #J := #J + 1
        //*   MOVE 45  TO CN-FYI-MSG-NUMBER(#J)
        //*   MOVE 045 TO CN-FYI-SRT-NUM(#J)
        //*  END-IF
        //*  END OF COMMENTED LINES  3/99
        //*  ======================================================================
        //*  MSG = 95
        //*  CONDITION:- ALL
        //*  THIS IS AN IMPORTANT DOCUMENT. PLEASE KEEP IT WITH YOUR CONTRACT.
        //*  ======================================================================
        //*  ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-CLOSE
        //*  PSTA9611
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FROM-TO-TIAA-REA-CREF
        //* ***********************************************************************
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pdaIatl410p.getPnd_Iatn410_Out_Pnd_Return_Code().setValue("E");                                                                                                   //Natural: MOVE 'E' TO #IATN410-OUT.#RETURN-CODE
        pdaIatl410p.getPnd_Iatn410_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), " (",                //Natural: COMPRESS ##MSG ' (' *PROGRAM ')' INTO #IATN410-OUT.#MSG LEAVING NO
            Global.getPROGRAM(), ")"));
        //*  BT
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( FND1. )
        Global.setEscapeCode(EscapeType.Bottom, "FND1");
        if (true) return;
    }
    private void sub_Call_Post_For_Open() throws Exception                                                                                                                //Natural: CALL-POST-FOR-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  082017 START - REPLACED PSTA9611 WITH PSTA9612
        //*  082017 NEW FIELD
        //*  082017 NEW FIELD
        pdaPsta9612.getPsta9612().reset();                                                                                                                                //Natural: RESET PSTA9612
        pdaPsta9612.getPsta9612_Univ_Id().setValue(iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id);                                                                                   //Natural: ASSIGN PSTA9612.UNIV-ID := IAXFR-CALC-UNIQUE-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("IAIQ");                                                                                                          //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'IAIQ'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTIACNF");                                                                                                          //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PTIACNF'
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                        //Natural: ASSIGN PSTA9612.PCKGE-DLVRY-TYP := '   '
        pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue(pdaIatl410p.getPnd_Iatn410_In_Pnd_Printer_Id());                                                                  //Natural: ASSIGN PSTA9612.PRNTR-ID-CDE := #IATN410-IN.#PRINTER-ID
        pdaPsta9612.getPsta9612_Tiaa_Cntrct_Nbr().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                            //Natural: ASSIGN PSTA9612.TIAA-CNTRCT-NBR := IAXFR-FROM-PPCN-NBR
        pdaPsta9612.getPsta9612_Payee_Cde().setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                                 //Natural: ASSIGN PSTA9612.PAYEE-CDE := IAXFR-FROM-PAYEE-CDE
        pdaPsta9612.getPsta9612_Dont_Use_Finalist().setValue(false);                                                                                                      //Natural: ASSIGN DONT-USE-FINALIST := FALSE
        pdaPsta9612.getPsta9612_Addrss_Lines_Rule().setValue(false);                                                                                                      //Natural: ASSIGN ADDRSS-LINES-RULE := FALSE
        pdaPsta9612.getPsta9612_Last_Nme().setValue(" ");                                                                                                                 //Natural: ASSIGN LAST-NME := ' '
        pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(" ");                                                                                                           //Natural: ASSIGN ADDRSS-TYP-CDE := ' '
        DbsUtil.callnat(Pstn9525.class , getCurrentProcessState(), pdaPsta9612.getPsta9612_Letter_Dte(), pdaCwfpda_M.getMsg_Info_Sub());                                  //Natural: CALLNAT 'PSTN9525' LETTER-DTE MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaPsta9612.getPsta9612_Next_Bsnss_Day_Ind().setValue(true);                                                                                                      //Natural: ASSIGN NEXT-BSNSS-DAY-IND := TRUE
        pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(" ");                                                                                                           //Natural: ASSIGN LTTR-SLTTN-TXT := ' '
        pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue(" ");                                                                                                          //Natural: ASSIGN EMPL-SGNTRY-NME := ' '
        pdaPsta9612.getPsta9612_Empl_Unit_Work_Nme().setValue(" ");                                                                                                       //Natural: ASSIGN EMPL-UNIT-WORK-NME := ' '
        pdaPsta9612.getPsta9612_Print_Fclty_Cde().setValue("S");                                                                                                          //Natural: ASSIGN PRINT-FCLTY-CDE := 'S'
        pdaPsta9612.getPsta9612_Form_Lbrry_Id().setValue(" ");                                                                                                            //Natural: ASSIGN FORM-LBRRY-ID := ' '
        pdaPsta9612.getPsta9612_Fnshng_Cde().setValue(" ");                                                                                                               //Natural: ASSIGN FNSHNG-CDE := ' '
        pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time);                                                            //Natural: ASSIGN PSTA9612.RQST-LOG-DTE-TME ( 1 ) := IAXFR-CWF-LOG-DTE-TIME
        pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(1).setValue(iaa_Xfr_Audit_Iaxfr_Cwf_Wpid);                                                                       //Natural: ASSIGN PSTA9612.WORK-PRCSS-ID ( 1 ) := IAXFR-CWF-WPID
        pdaPsta9612.getPsta9612_Mit_Contract_Nbr().getValue(1,1).setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                             //Natural: ASSIGN PSTA9612.MIT-CONTRACT-NBR ( 1,1 ) := IAXFR-FROM-PPCN-NBR
        pdaPsta9612.getPsta9612_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                               //Natural: ASSIGN PSTA9612.WPID-VLDTE-IND ( 1 ) := 'Y'
        pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue(pnd_Const_Pnd_Mit_Status);                                                                              //Natural: ASSIGN PSTA9612.STATUS-CDE ( 1 ) := #MIT-STATUS
        if (condition(iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time.equals(" ")))                                                                                                  //Natural: IF IAXFR-CWF-LOG-DTE-TIME = ' '
        {
            pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                 //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := TRUE
            pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                 //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(false);                                                                                                //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := FALSE
            pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(false);                                                                                                //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  082017 END REPLACED
        //*  CALL POST OPEN
        //* *CALLNAT 'PSTN9610'                             /* 082017 START
        //*  082017 END
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST-FOR-OPEN
    }
    private void sub_Write_Contract_Rec() throws Exception                                                                                                                //Natural: WRITE-CONTRACT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO CN-CONTRACT-STRUCT
        //*  SET THE FROM/TO CONTRACT NUMBERS
        if (condition(pnd_Trans_Type_Pnd_From_Grad.equals("Y")))                                                                                                          //Natural: IF #FROM-GRAD = 'Y'
        {
            ldaPstl6165.getCn_Contract_Struct_Cn_Tiaa_Cont_Num().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                             //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-TIAA-CONT-NUM := IAXFR-FROM-PPCN-NBR
            ldaPstl6165.getCn_Contract_Struct_Cn_Tiaa_Frmto_Ind().setValue("1");                                                                                          //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-TIAA-FRMTO-IND := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  FROM REAL ESTATE
            //*  022009
            if (condition(pnd_Trans_Type_Pnd_From_Re.equals("Y") || pnd_Trans_Type_Pnd_From_Acc.equals("Y")))                                                             //Natural: IF #FROM-RE = 'Y' OR #FROM-ACC = 'Y'
            {
                ldaPstl6165.getCn_Contract_Struct_Cn_Real_Estate_Cont().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                      //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-REAL-ESTATE-CONT := IAXFR-FROM-PPCN-NBR
                ldaPstl6165.getCn_Contract_Struct_Cn_Real_Frmto_Ind().setValue("1");                                                                                      //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-REAL-FRMTO-IND := '1'
                //*  042209
                ldaPstl6165.getCn_Contract_Struct_Cn_Real_Estate_Cont().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaPstl6165.getCn_Contract_Struct_Cn_Real_Estate_Cont(),  //Natural: COMPRESS CN-CONTRACT-STRUCT.CN-REAL-ESTATE-CONT '-REA' INTO CN-CONTRACT-STRUCT.CN-REAL-ESTATE-CONT LEAVING NO
                    "-REA"));
                if (condition(pnd_Trans_Type_Pnd_To_Cref.equals("Y")))                                                                                                    //Natural: IF #TO-CREF = 'Y'
                {
                    ldaPstl6165.getCn_Contract_Struct_Cn_Cref_Cont_Num().setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                                  //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-CREF-CONT-NUM := IAXFR-CALC-TO-PPCN-NBR
                    ldaPstl6165.getCn_Contract_Struct_Cn_Cref_Frmto_Ind().setValue("2");                                                                                  //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-CREF-FRMTO-IND := '2'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Trans_Type_Pnd_To_Std.equals("Y") || pnd_Trans_Type_Pnd_To_Grad.equals("Y")))                                                           //Natural: IF #TO-STD = 'Y' OR #TO-GRAD = 'Y'
                {
                    ldaPstl6165.getCn_Contract_Struct_Cn_Tiaa_Cont_Num().setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                                  //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-TIAA-CONT-NUM := IAXFR-CALC-TO-PPCN-NBR
                    ldaPstl6165.getCn_Contract_Struct_Cn_Tiaa_Frmto_Ind().setValue("2");                                                                                  //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-TIAA-FRMTO-IND := '2'
                }                                                                                                                                                         //Natural: END-IF
                //*  FROM CREF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6165.getCn_Contract_Struct_Cn_Cref_Cont_Num().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                         //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-CREF-CONT-NUM := IAXFR-FROM-PPCN-NBR
                ldaPstl6165.getCn_Contract_Struct_Cn_Cref_Frmto_Ind().setValue("1");                                                                                      //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-CREF-FRMTO-IND := '1'
                //*  022009
                if (condition(pnd_Trans_Type_Pnd_To_Re.equals("Y") || pnd_Trans_Type_Pnd_To_Acc.equals("Y")))                                                             //Natural: IF #TO-RE = 'Y' OR #TO-ACC = 'Y'
                {
                    ldaPstl6165.getCn_Contract_Struct_Cn_Real_Estate_Cont().setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                               //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-REAL-ESTATE-CONT := IAXFR-CALC-TO-PPCN-NBR
                    ldaPstl6165.getCn_Contract_Struct_Cn_Real_Frmto_Ind().setValue("2");                                                                                  //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-REAL-FRMTO-IND := '2'
                    //*  042209
                    ldaPstl6165.getCn_Contract_Struct_Cn_Real_Estate_Cont().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaPstl6165.getCn_Contract_Struct_Cn_Real_Estate_Cont(),  //Natural: COMPRESS CN-CONTRACT-STRUCT.CN-REAL-ESTATE-CONT '-REA' INTO CN-CONTRACT-STRUCT.CN-REAL-ESTATE-CONT LEAVING NO
                        "-REA"));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Trans_Type_Pnd_To_Std.equals("Y") || pnd_Trans_Type_Pnd_To_Grad.equals("Y")))                                                           //Natural: IF #TO-STD = 'Y' OR #TO-GRAD = 'Y'
                {
                    ldaPstl6165.getCn_Contract_Struct_Cn_Tiaa_Cont_Num().setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                                  //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-TIAA-CONT-NUM := IAXFR-CALC-TO-PPCN-NBR
                    ldaPstl6165.getCn_Contract_Struct_Cn_Tiaa_Frmto_Ind().setValue("2");                                                                                  //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-TIAA-FRMTO-IND := '2'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6165.getCn_Contract_Struct_Cn_Req_Source().setValue("V");                                                                                                  //Natural: ASSIGN CN-REQ-SOURCE := 'V'
        ldaPstl6165.getCn_Contract_Struct_Cn_Switch_Ind().setValue("S");                                                                                                  //Natural: ASSIGN CN-SWITCH-IND := 'S'
        pnd_Date_A.setValueEdited(iaa_Xfr_Audit_Iaxfr_Effctve_Dte,new ReportEditMask("MMDDYYYY"));                                                                        //Natural: MOVE EDITED IAXFR-EFFCTVE-DTE ( EM = MMDDYYYY ) TO #DATE-A
        ldaPstl6165.getCn_Contract_Struct_Cn_Effective_Date_F().setValue(pnd_Date_A_Pnd_Date_N);                                                                          //Natural: ASSIGN CN-EFFECTIVE-DATE-F := #DATE-N
        //*                                                          /* GGG-08/10
        pnd_Ws_Todays_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaIatl410p.getPnd_Iatn410_In_Pnd_Todays_Dte());                                                  //Natural: MOVE EDITED #IATN410-IN.#TODAYS-DTE TO #WS-TODAYS-DTE ( EM = YYYYMMDD )
        ldaPstl6165.getCn_Contract_Struct_Cn_Processing_Date().setValueEdited(pnd_Ws_Todays_Dte,new ReportEditMask("MM/DD/YYYY"));                                        //Natural: MOVE EDITED #WS-TODAYS-DTE ( EM = MM/DD/YYYY ) TO CN-PROCESSING-DATE
        ldaPstl6165.getCn_Contract_Struct_Cn_From_Tran_Type().setValue(iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(1));                                                         //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-FROM-TRAN-TYPE := IAXFR-FROM-TYP ( 1 )
        ldaPstl6165.getCn_Contract_Struct_Cn_To_Tran_Type().setValue(iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(1));                                                             //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-TO-TRAN-TYPE := IAXFR-TO-TYP ( 1 )
        //*  GET VALUES FOR AC-MOD-DATE-TIME
        pnd_Date_A.setValueEdited(pdaIatl410p.getPnd_Iatn410_In_Rqst_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED RQST-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Date_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                                //Natural: MOVE #DATE-N TO #DATE-YYYYMMDD
        pnd_Mod_Ts_Pnd_Mod_Date_Mm.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Mm);                                                                                               //Natural: MOVE #DATE-MM TO #MOD-DATE-MM
        pnd_Mod_Ts_Pnd_Mod_Date_Dd.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Dd);                                                                                               //Natural: MOVE #DATE-DD TO #MOD-DATE-DD
        pnd_Mod_Ts_Pnd_Mod_Date_Yyyy.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Yyyy);                                                                                           //Natural: MOVE #DATE-YYYY TO #MOD-DATE-YYYY
        pnd_Mod_Ts_Pnd_Mod_Time.setValue(pdaIatl410p.getPnd_Iatn410_In_Rqst_Entry_Tme());                                                                                 //Natural: MOVE RQST-ENTRY-TME TO #MOD-TIME
        if (condition(pnd_Mod_Ts_Pnd_Mod_Hrs.greater(12)))                                                                                                                //Natural: IF #MOD-HRS GT 12
        {
            pnd_Mod_Ts_Pnd_Mod_Hrs.nsubtract(12);                                                                                                                         //Natural: SUBTRACT 12 FROM #MOD-HRS
            //*  PM
            pnd_Mod_Ts_Pnd_Mod_Am.setValue(1);                                                                                                                            //Natural: MOVE 1 TO #MOD-AM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  AM
            pnd_Mod_Ts_Pnd_Mod_Am.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #MOD-AM
            //*   VALUES?????
            //*   ?????
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6165.getCn_Contract_Struct_Cn_Mod_Date_Time_F().setValue(pnd_Mod_Ts);                                                                                      //Natural: ASSIGN CN-MOD-DATE-TIME-F := #MOD-TS
        ldaPstl6165.getCn_Contract_Struct_Cn_Mach_Func_1().setValue("2");                                                                                                 //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-MACH-FUNC-1 := '2'
        ldaPstl6165.getCn_Contract_Struct_Cn_Image_Ind().setValue(" ");                                                                                                   //Natural: ASSIGN CN-CONTRACT-STRUCT.CN-IMAGE-IND := ' '
        //*   SET UP SORT KEY
        ldaPstl6165.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6165.getSort_Key_Struct_Key_L3_Req_Date_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                     //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO SORT-KEY-STRUCT.KEY-L3-REQ-DATE-TIME
        ldaPstl6165.getSort_Key_Struct_Key_L3_From_Cont_Number().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                             //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L3-FROM-CONT-NUMBER := IAXFR-FROM-PPCN-NBR
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        //*  SORT KEY
        //*  SPECIFIC RECORD TO BE WRITTEN
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CN-CONTRACT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6165.getSort_Key_Struct(), ldaPstl6165.getCn_Contract_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-CONTRACT-REC
    }
    private void sub_Write_From_Rec() throws Exception                                                                                                                    //Natural: WRITE-FROM-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO CN-FRM-REC-STRUCT
        ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Rec().getValue("*").reset();                                                                                              //Natural: RESET CN-FRM-REC-STRUCT.CN-FRM-REC ( * ) CN-FRM-PERCENT ( * ) CN-FRM-DOLLARS ( * ) CN-FRM-UNITS ( * ) CN-FRM-REC-STRUCT.CN-DATA-OCCURS
        ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Percent().getValue("*").reset();
        ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Dollars().getValue("*").reset();
        ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Units().getValue("*").reset();
        ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Data_Occurs().reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  SET AC-DATA-OCCURS
            if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals(" ") && iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(pnd_I).equals(" ")))                       //Natural: IF IAXFR-FRM-ACCT-CD ( #I ) = ' ' AND IAXFR-FROM-TYP ( #I ) = ' '
            {
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I,":",20).notEquals(" ") && iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(pnd_I,":",                   //Natural: IF IAXFR-FRM-ACCT-CD ( #I:20 ) NE ' ' AND IAXFR-FROM-TYP ( #I:20 ) NE ' '
                    20).notEquals(" ")))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Data_Occurs().nadd(1);                                                                                                //Natural: ADD 1 TO CN-FRM-REC-STRUCT.CN-DATA-OCCURS
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-IF
            //*  CONVERT ONE BYTE FUND CODE TO THE (N2) FUND CODE USED BY POST.
            if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF IAXFR-FRM-ACCT-CD ( #I ) NE ' '
            {
                DbsUtil.callnat(Iatn0201.class , getCurrentProcessState(), iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I), pnd_Iatn0201_Out_Pnd_Post_Fund_N2);           //Natural: CALLNAT 'IATN0201' IAXFR-FRM-ACCT-CD ( #I ) #IATN0201-OUT.#POST-FUND-N2
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Account_Type().getValue(pnd_K).setValue(pnd_Iatn0201_Out_Pnd_Post_Fund_N2);                                       //Natural: ASSIGN CN-FRM-ACCOUNT-TYPE ( #K ) := #IATN0201-OUT.#POST-FUND-N2
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Myi().getValue(pnd_K).setValue(pnd_Const_Pnd_Yearly);                                                         //Natural: ASSIGN CN-FRM-MYI ( #K ) := #YEARLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Myi().getValue(pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I));                             //Natural: ASSIGN CN-FRM-MYI ( #K ) := IAXFR-FRM-UNIT-TYP ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Trn_Typ().getValue(pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(pnd_I));                                     //Natural: ASSIGN CN-FRM-TRN-TYP ( #K ) := IAXFR-FROM-TYP ( #I )
            short decideConditionsMet1876 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF IAXFR-FROM-TYP ( #I );//Natural: VALUE #PERCENT
            if (condition((iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Percent))))
            {
                decideConditionsMet1876++;
                ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Percent().getValue(pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Qty.getValue(pnd_I));                                 //Natural: ASSIGN CN-FRM-PERCENT ( #K ) := IAXFR-FROM-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #DOLLARS
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Dollars))))
            {
                decideConditionsMet1876++;
                ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Dollars().getValue(pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Qty.getValue(pnd_I));                                 //Natural: ASSIGN CN-FRM-DOLLARS ( #K ) := IAXFR-FROM-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #UNITS
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Units))))
            {
                decideConditionsMet1876++;
                ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Units().getValue(pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Qty.getValue(pnd_I));                                   //Natural: ASSIGN CN-FRM-UNITS ( #K ) := IAXFR-FROM-QTY ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6165.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6165.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Account_Type().getValue(1));                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE := CN-FRM-ACCOUNT-TYPE ( 1 )
        ldaPstl6165.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM := 0
        ldaPstl6165.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM := 0
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CN-FRM-REC-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6165.getSort_Key_Struct(), ldaPstl6165.getCn_Frm_Rec_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-FROM-REC
    }
    private void sub_Write_To_Rec() throws Exception                                                                                                                      //Natural: WRITE-TO-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO CN-TO-ACCT-STRUCT
        ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Acct().getValue("*").reset();                                                                                              //Natural: RESET CN-TO-ACCT-STRUCT.CN-TO-ACCT ( * ) CN-TO-PERCENT ( * ) CN-TO-DOLLARS ( * ) CN-TO-UNITS ( * )
        ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Percent().getValue("*").reset();
        ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Dollars().getValue("*").reset();
        ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Units().getValue("*").reset();
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  SET AC-DATA-OCCURS
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(" ") && iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(pnd_I).equals(" ")))                          //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = ' ' AND IAXFR-TO-TYP ( #I ) = ' '
            {
                ldaPstl6165.getCn_To_Acct_Struct_Cn_Data_Occurs().compute(new ComputeParameters(false, ldaPstl6165.getCn_To_Acct_Struct_Cn_Data_Occurs()),                //Natural: ASSIGN CN-TO-ACCT-STRUCT.CN-DATA-OCCURS := #I - 1
                    pnd_I.subtract(1));
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6165.getCn_To_Acct_Struct_Cn_Data_Occurs().setValue(pnd_I);                                                                                        //Natural: ASSIGN CN-TO-ACCT-STRUCT.CN-DATA-OCCURS := #I
            }                                                                                                                                                             //Natural: END-IF
            //*  CONVERT ONE BYTE FUND CODE TO THE (N2) FUND CODE USED BY POST.
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).notEquals(" ")))                                                                                 //Natural: IF IAXFR-TO-ACCT-CD ( #I ) NE ' '
            {
                DbsUtil.callnat(Iatn0201.class , getCurrentProcessState(), iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I), pnd_Iatn0201_Out_Pnd_Post_Fund_N2);            //Natural: CALLNAT 'IATN0201' IAXFR-TO-ACCT-CD ( #I ) #IATN0201-OUT.#POST-FUND-N2
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Account_Type().getValue(pnd_I).setValue(pnd_Iatn0201_Out_Pnd_Post_Fund_N2);                                        //Natural: ASSIGN CN-TO-ACCOUNT-TYPE ( #I ) := #IATN0201-OUT.#POST-FUND-N2
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Myi().getValue(pnd_I).setValue(pnd_Const_Pnd_Yearly);                                                          //Natural: ASSIGN CN-TO-MYI ( #I ) := #YEARLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Myi().getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I));                               //Natural: ASSIGN CN-TO-MYI ( #I ) := IAXFR-TO-UNIT-TYP ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Trn_Typ().getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(pnd_I));                                        //Natural: ASSIGN CN-TO-TRN-TYP ( #I ) := IAXFR-TO-TYP ( #I )
            short decideConditionsMet1934 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF IAXFR-TO-TYP ( #I );//Natural: VALUE #PERCENT
            if (condition((iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Percent))))
            {
                decideConditionsMet1934++;
                ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Percent().getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Qty.getValue(pnd_I));                                    //Natural: ASSIGN CN-TO-PERCENT ( #I ) := IAXFR-TO-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #DOLLARS
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Dollars))))
            {
                decideConditionsMet1934++;
                ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Dollars().getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Qty.getValue(pnd_I));                                    //Natural: ASSIGN CN-TO-DOLLARS ( #I ) := IAXFR-TO-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #UNITS
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Units))))
            {
                decideConditionsMet1934++;
                ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Units().getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Qty.getValue(pnd_I));                                      //Natural: ASSIGN CN-TO-UNITS ( #I ) := IAXFR-TO-QTY ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6165.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6165.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6165.getCn_To_Acct_Struct_Cn_To_Account_Type().getValue(1));                                 //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE := CN-TO-ACCOUNT-TYPE ( 1 )
        ldaPstl6165.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM := 0
        ldaPstl6165.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM := 0
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CN-TO-ACCT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6165.getSort_Key_Struct(), ldaPstl6165.getCn_To_Acct_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-TO-REC
    }
    private void sub_Write_Effect_Of_Trnsfr_From() throws Exception                                                                                                       //Natural: WRITE-EFFECT-OF-TRNSFR-FROM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO CN-ETF-STRUCT
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf().getValue("*").reset();                                                                                                      //Natural: RESET CN-ETF-STRUCT.CN-ETF ( * )
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Vtb().getValue("*").setValue("V");                                                                                            //Natural: ASSIGN CN-ETF-VTB ( * ) := 'V'
        //*  MAY BE USED LATER
        //*  MAY BE USED LATER
        //*  MAY BE USED LATER
        //*  022009
        //*  022009
        //*  MAY BE USED LATER
        //*  "
        pnd_Date_A.setValueEdited(iaa_Xfr_Audit_Iaxfr_Effctve_Dte,new ReportEditMask("DDMMYYYY"));                                                                        //Natural: MOVE EDITED IAXFR-EFFCTVE-DTE ( EM = DDMMYYYY ) TO #DATE-A
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Datvp1f().getValue(1).setValue(pnd_Date_A_Pnd_Date_N);                                                                        //Natural: ASSIGN CN-ETF-DATVP1F ( 1 ) := #DATE-N
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Datvp1f().getValue(2).setValue(9999);                                                                                         //Natural: ASSIGN CN-ETF-DATVP1F ( 2 ) := 9999
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Var_P1v().getValue("*").setValue(" ");                                                                                        //Natural: ASSIGN CN-ETF-VAR-P1V ( * ) := ' '
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Datvp2f().getValue(1).setValue(ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Datvp1f().getValue(1));                                    //Natural: ASSIGN CN-ETF-DATVP2F ( 1 ) := CN-ETF-DATVP1F ( 1 )
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Datvp2f().getValue(2).setValue(9999);                                                                                         //Natural: ASSIGN CN-ETF-DATVP2F ( 2 ) := 9999
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Var_P2v().getValue("*").setValue(" ");                                                                                        //Natural: ASSIGN CN-ETF-VAR-P2V ( * ) := ' '
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Datvp3f().getValue("*").setValue(9999);                                                                                       //Natural: ASSIGN CN-ETF-DATVP3F ( * ) := 9999
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Var_P3v().getValue("*").setValue(" ");                                                                                        //Natural: ASSIGN CN-ETF-VAR-P3V ( * ) := ' '
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Grded_P().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-GRDED-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Grd_Con().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-GRD-CON ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Grd_Div().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-GRD-DIV ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Std_P().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETF-ACCNT-STD-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Std_Con().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-STD-CON ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Std_Div().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-STD-DIV ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Reale_P().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-REALE-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Reale_U().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-REALE-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Access_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-ACCESS-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Access_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-ACCESS-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Stock_P().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-STOCK-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Stock_U().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETF-ACCNT-STOCK-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mma_P().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETF-ACCNT-MMA-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mma_U().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETF-ACCNT-MMA-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Bond_P().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETF-ACCNT-BOND-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Bond_U().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETF-ACCNT-BOND-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Social_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-SOCIAL-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Social_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-SOCIAL-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Global_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-GLOBAL-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Global_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-GLOBAL-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_P().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETF-ACCNT-EQI-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_U().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETF-ACCNT-EQI-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Growth_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-GROWTH-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Growth_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-GROWTH-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Infb_P().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETF-ACCNT-INFB-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Infb_U().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETF-ACCNT-INFB-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mutual_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-MUTUAL-P ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mutual_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETF-ACCNT-MUTUAL-U ( * ) := -1
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Myi().getValue(1).setValue(pnd_Const_Pnd_Yearly);                                                                             //Natural: ASSIGN CN-ETF-MYI ( 1 ) := #YEARLY
        ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Myi().getValue(2).setValue(pnd_Const_Pnd_Monthly);                                                                            //Natural: ASSIGN CN-ETF-MYI ( 2 ) := #MONTHLY
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).notEquals(" ")))                                                                               //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) NE ' '
            {
                //*  ANNUAL
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) EQ #ANNUAL
                {
                    pnd_J.setValue(1);                                                                                                                                    //Natural: ASSIGN #J := 1
                    //*  MONTHLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J.setValue(2);                                                                                                                                    //Natural: ASSIGN #J := 2
                }                                                                                                                                                         //Natural: END-IF
                //*  022009 START
                short decideConditionsMet2027 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF IAXFR-FROM-FUND-CDE ( #I );//Natural: VALUE #FUND-CDE-REA
                if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Rea))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Reale_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));              //Natural: ASSIGN CN-ETF-ACCNT-REALE-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Reale_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));             //Natural: ASSIGN CN-ETF-ACCNT-REALE-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-ACC
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Acc))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Access_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));             //Natural: ASSIGN CN-ETF-ACCNT-ACCESS-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Access_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));            //Natural: ASSIGN CN-ETF-ACCNT-ACCESS-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-STOCK
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Stock))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Stock_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));              //Natural: ASSIGN CN-ETF-ACCNT-STOCK-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Stock_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));             //Natural: ASSIGN CN-ETF-ACCNT-STOCK-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-MMA
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Mma))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mma_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));                //Natural: ASSIGN CN-ETF-ACCNT-MMA-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mma_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));               //Natural: ASSIGN CN-ETF-ACCNT-MMA-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-BOND
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Bond))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Bond_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));               //Natural: ASSIGN CN-ETF-ACCNT-BOND-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Bond_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));              //Natural: ASSIGN CN-ETF-ACCNT-BOND-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-SOCIAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Social))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Social_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));             //Natural: ASSIGN CN-ETF-ACCNT-SOCIAL-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Social_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));            //Natural: ASSIGN CN-ETF-ACCNT-SOCIAL-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GLOBAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Global))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Global_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));             //Natural: ASSIGN CN-ETF-ACCNT-GLOBAL-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Global_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));            //Natural: ASSIGN CN-ETF-ACCNT-GLOBAL-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-EQUITY
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Equity))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));                //Natural: ASSIGN CN-ETF-ACCNT-EQI-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));               //Natural: ASSIGN CN-ETF-ACCNT-EQI-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GROWTH
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Growth))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Growth_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));             //Natural: ASSIGN CN-ETF-ACCNT-GROWTH-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Growth_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));            //Natural: ASSIGN CN-ETF-ACCNT-GROWTH-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-INFL
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Infl))))
                {
                    decideConditionsMet2027++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Infb_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I));               //Natural: ASSIGN CN-ETF-ACCNT-INFB-P ( #J ) := IAXFR-FROM-CURRENT-PMT-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Infb_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));              //Natural: ASSIGN CN-ETF-ACCNT-INFB-U ( #J ) := IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  SET THE BEFORE VALUES OF THE TO FUNDS
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) NE ' '
            {
                //*  ANNUAL
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) EQ #ANNUAL
                {
                    pnd_J.setValue(1);                                                                                                                                    //Natural: ASSIGN #J := 1
                    //*  MONTHLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J.setValue(2);                                                                                                                                    //Natural: ASSIGN #J := 2
                }                                                                                                                                                         //Natural: END-IF
                //*  022009 START
                short decideConditionsMet2075 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF IAXFR-TO-FUND-CDE ( #I );//Natural: VALUE #FUND-CDE-REA
                if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Rea))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Reale_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                    //Natural: ASSIGN CN-ETF-ACCNT-REALE-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Reale_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                   //Natural: ASSIGN CN-ETF-ACCNT-REALE-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-ACC
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Acc))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Access_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETF-ACCNT-ACCESS-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Access_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETF-ACCNT-ACCESS-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-STOCK
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Stock))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Stock_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                    //Natural: ASSIGN CN-ETF-ACCNT-STOCK-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Stock_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                   //Natural: ASSIGN CN-ETF-ACCNT-STOCK-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-MMA
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Mma))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mma_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                      //Natural: ASSIGN CN-ETF-ACCNT-MMA-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Mma_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                     //Natural: ASSIGN CN-ETF-ACCNT-MMA-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-BOND
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Bond))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Bond_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                     //Natural: ASSIGN CN-ETF-ACCNT-BOND-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Bond_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                    //Natural: ASSIGN CN-ETF-ACCNT-BOND-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-SOCIAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Social))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Social_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETF-ACCNT-SOCIAL-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Social_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETF-ACCNT-SOCIAL-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GLOBAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Global))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Global_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETF-ACCNT-GLOBAL-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Global_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETF-ACCNT-GLOBAL-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-EQUITY
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Equity))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                      //Natural: ASSIGN CN-ETF-ACCNT-EQI-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                     //Natural: ASSIGN CN-ETF-ACCNT-EQI-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GROWTH
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Growth))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Growth_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETF-ACCNT-GROWTH-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Growth_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETF-ACCNT-GROWTH-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-INFL
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Infl))))
                {
                    decideConditionsMet2075++;
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Infb_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I));                     //Natural: ASSIGN CN-ETF-ACCNT-INFB-P ( #J ) := IAXFR-TO-BFR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Etf_Struct_Cn_Etf_Accnt_Infb_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I));                    //Natural: ASSIGN CN-ETF-ACCNT-INFB-U ( #J ) := IAXFR-TO-BFR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaPstl6165.getCn_Etf_Struct_Cn_Data_Occurs().setValue(2);                                                                                                        //Natural: ASSIGN CN-ETF-STRUCT.CN-DATA-OCCURS := 2
        ldaPstl6165.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6165.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Account_Type().getValue(1));                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE := CN-FRM-ACCOUNT-TYPE ( 1 )
        ldaPstl6165.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM := 0
        ldaPstl6165.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM := 0
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CN-ETF-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6165.getSort_Key_Struct(), ldaPstl6165.getCn_Etf_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-EFFECT-OF-TRNSFR-FROM
    }
    private void sub_Write_Effect_Of_Trnsfr_To() throws Exception                                                                                                         //Natural: WRITE-EFFECT-OF-TRNSFR-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO CN-ETT-STRUCT
        //*  CONFIRM EFFECT ANNUAL SWITCH FUNDS (TO)
        //*  MAY BE USED LATER
        //*  MAY BE USED LATER
        //*  MAY BE USED LATER
        //*  022009
        //*  022009
        //*  MAY BE USED LATER
        //*  "
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett().getValue("*").reset();                                                                                                      //Natural: RESET CN-ETT-STRUCT.CN-ETT ( * )
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Vtb().getValue("*").setValue("V");                                                                                            //Natural: ASSIGN CN-ETT-VTB ( * ) := 'V'
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Datvp1f().getValue("*").setValue(9999);                                                                                       //Natural: ASSIGN CN-ETT-DATVP1F ( * ) := 9999
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Var_P1v().getValue("*").setValue(" ");                                                                                        //Natural: ASSIGN CN-ETT-VAR-P1V ( * ) := ' '
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Datvp2f().getValue("*").setValue(9999);                                                                                       //Natural: ASSIGN CN-ETT-DATVP2F ( * ) := 9999
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Var_P2v().getValue("*").setValue(" ");                                                                                        //Natural: ASSIGN CN-ETT-VAR-P2V ( * ) := ' '
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Datvp3f().getValue("*").setValue(9999);                                                                                       //Natural: ASSIGN CN-ETT-DATVP3F ( * ) := 9999
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Var_P3v().getValue("*").setValue(" ");                                                                                        //Natural: ASSIGN CN-ETT-VAR-P3V ( * ) := ' '
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Grded_P().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-GRDED-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Grd_Con().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-GRD-CON ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Grd_Div().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-GRD-DIV ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Std_P().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETT-ACCNT-STD-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Std_Con().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-STD-CON ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Std_Div().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-STD-DIV ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Reale_P().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-REALE-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Reale_U().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-REALE-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Access_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-ACCESS-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Access_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-ACCESS-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Stock_P().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-STOCK-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Stock_U().getValue("*").setValue(-1);                                                                                   //Natural: ASSIGN CN-ETT-ACCNT-STOCK-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mma_P().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETT-ACCNT-MMA-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mma_U().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETT-ACCNT-MMA-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Bond_P().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETT-ACCNT-BOND-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Bond_U().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETT-ACCNT-BOND-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Social_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-SOCIAL-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Social_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-SOCIAL-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Global_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-GLOBAL-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Global_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-GLOBAL-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_P().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETT-ACCNT-EQI-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_U().getValue("*").setValue(-1);                                                                                     //Natural: ASSIGN CN-ETT-ACCNT-EQI-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Growth_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-GROWTH-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Growth_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-GROWTH-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Infb_P().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETT-ACCNT-INFB-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Infb_U().getValue("*").setValue(-1);                                                                                    //Natural: ASSIGN CN-ETT-ACCNT-INFB-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mutual_P().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-MUTUAL-P ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mutual_U().getValue("*").setValue(-1);                                                                                  //Natural: ASSIGN CN-ETT-ACCNT-MUTUAL-U ( * ) := -1
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Myi().getValue(1).setValue(pnd_Const_Pnd_Yearly);                                                                             //Natural: ASSIGN CN-ETT-MYI ( 1 ) := #YEARLY
        ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Myi().getValue(2).setValue(pnd_Const_Pnd_Monthly);                                                                            //Natural: ASSIGN CN-ETT-MYI ( 2 ) := #MONTHLY
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) NE ' '
            {
                //*  ANNUAL
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) EQ #ANNUAL
                {
                    pnd_J.setValue(1);                                                                                                                                    //Natural: ASSIGN #J := 1
                    //*  MONTHLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J.setValue(2);                                                                                                                                    //Natural: ASSIGN #J := 2
                }                                                                                                                                                         //Natural: END-IF
                //*  022009 START
                short decideConditionsMet2189 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF IAXFR-TO-FUND-CDE ( #I );//Natural: VALUE #FUND-CDE-REA
                if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Rea))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Reale_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETT-ACCNT-REALE-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Reale_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-REALE-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-ACC
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Acc))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Access_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-ACCESS-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Access_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-ACCESS-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-STOCK
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Stock))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Stock_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETT-ACCNT-STOCK-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Stock_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-STOCK-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-MMA
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Mma))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mma_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                     //Natural: ASSIGN CN-ETT-ACCNT-MMA-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mma_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                    //Natural: ASSIGN CN-ETT-ACCNT-MMA-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-BOND
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Bond))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Bond_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                    //Natural: ASSIGN CN-ETT-ACCNT-BOND-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Bond_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                   //Natural: ASSIGN CN-ETT-ACCNT-BOND-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-SOCIAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Social))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Social_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-SOCIAL-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Social_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-SOCIAL-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GLOBAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Global))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Global_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-GLOBAL-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Global_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-GLOBAL-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-EQUITY
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Equity))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                     //Natural: ASSIGN CN-ETT-ACCNT-EQI-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                    //Natural: ASSIGN CN-ETT-ACCNT-EQI-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GROWTH
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Growth))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Growth_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-GROWTH-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Growth_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-GROWTH-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-INFL
                else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Infl))))
                {
                    decideConditionsMet2189++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Infb_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                    //Natural: ASSIGN CN-ETT-ACCNT-INFB-P ( #J ) := IAXFR-TO-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Infb_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                   //Natural: ASSIGN CN-ETT-ACCNT-INFB-U ( #J ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  SET THE NEW VALUES OF THE FROM FUNDS
            if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).notEquals(" ")))                                                                               //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) NE ' '
            {
                //*  ANNAUL
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) EQ #ANNUAL
                {
                    pnd_J.setValue(1);                                                                                                                                    //Natural: ASSIGN #J := 1
                    //*  MONTHLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J.setValue(2);                                                                                                                                    //Natural: ASSIGN #J := 2
                }                                                                                                                                                         //Natural: END-IF
                //*  022009 START
                short decideConditionsMet2237 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF IAXFR-FROM-FUND-CDE ( #I );//Natural: VALUE #FUND-CDE-REA
                if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Rea))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Reale_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-REALE-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Reale_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));                //Natural: ASSIGN CN-ETT-ACCNT-REALE-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-ACC
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Acc))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Access_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                //Natural: ASSIGN CN-ETT-ACCNT-ACCESS-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Access_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));               //Natural: ASSIGN CN-ETT-ACCNT-ACCESS-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-STOCK
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Stock))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Stock_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-STOCK-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Stock_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));                //Natural: ASSIGN CN-ETT-ACCNT-STOCK-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-MMA
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Mma))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mma_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETT-ACCNT-MMA-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Mma_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-MMA-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-BOND
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Bond))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Bond_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-BOND-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Bond_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-BOND-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-SOCIAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Social))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Social_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                //Natural: ASSIGN CN-ETT-ACCNT-SOCIAL-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Social_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));               //Natural: ASSIGN CN-ETT-ACCNT-SOCIAL-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GLOBAL
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Global))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Global_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                //Natural: ASSIGN CN-ETT-ACCNT-GLOBAL-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Global_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));               //Natural: ASSIGN CN-ETT-ACCNT-GLOBAL-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-EQUITY
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Equity))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                   //Natural: ASSIGN CN-ETT-ACCNT-EQI-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-EQI-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-GROWTH
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Growth))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Growth_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                //Natural: ASSIGN CN-ETT-ACCNT-GROWTH-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Growth_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));               //Natural: ASSIGN CN-ETT-ACCNT-GROWTH-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: VALUE #FUND-CDE-INFL
                else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Infl))))
                {
                    decideConditionsMet2237++;
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Infb_P().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I));                  //Natural: ASSIGN CN-ETT-ACCNT-INFB-P ( #J ) := IAXFR-FROM-AFTR-XFR-GUAR ( #I )
                    ldaPstl6165.getCn_Ett_Struct_Cn_Ett_Accnt_Infb_U().getValue(pnd_J).setValue(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units.getValue(pnd_I));                 //Natural: ASSIGN CN-ETT-ACCNT-INFB-U ( #J ) := IAXFR-FROM-AFTR-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaPstl6165.getCn_Ett_Struct_Cn_Data_Occurs().setValue(2);                                                                                                        //Natural: ASSIGN CN-ETT-STRUCT.CN-DATA-OCCURS := 2
        ldaPstl6165.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6165.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Account_Type().getValue(1));                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE := CN-FRM-ACCOUNT-TYPE ( 1 )
        ldaPstl6165.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM := 0
        ldaPstl6165.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM := 0
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CN-ETT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6165.getSort_Key_Struct(), ldaPstl6165.getCn_Ett_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-EFFECT-OF-TRNSFR-TO
    }
    private void sub_Write_Confirm_Annuity_Val() throws Exception                                                                                                         //Natural: WRITE-CONFIRM-ANNUITY-VAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO CN-ANV-STRUCT
        //*  "The following are the annuity units values in effect on the date "
        //*  " your transaction took place: "
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv().reset();                                                                                                                    //Natural: RESET CN-ANV-STRUCT.CN-ANV
        //*  SET MONTHLY/YEARLY FIELD
        if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(1).equals(pnd_Const_Pnd_Annual)))                                                                         //Natural: IF IAXFR-FRM-UNIT-TYP ( 1 ) = #ANNUAL
        {
            ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Myi().setValue(pnd_Const_Pnd_Yearly);                                                                                     //Natural: ASSIGN CN-ANV-MYI := #YEARLY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Myi().setValue(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(1));                                                             //Natural: ASSIGN CN-ANV-MYI := IAXFR-FRM-UNIT-TYP ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK IF THE TRANSFER WAS 1. VARIABLE TO VARIABLE
        //*                            2. VARIABLE TO TIAA TRADITIONAL ONLY
        //*  FROM VARIABLE
        //*  022009
        if (condition(pnd_Trans_Type_Pnd_From_Re.equals("Y") || pnd_Trans_Type_Pnd_From_Cref.equals("Y") || pnd_Trans_Type_Pnd_From_Acc.equals("Y")))                     //Natural: IF #FROM-RE = 'Y' OR #FROM-CREF = 'Y' OR #FROM-ACC = 'Y'
        {
            //*  TO VARIABLE
            //*  022009
            if (condition(pnd_Trans_Type_Pnd_To_Re.equals("Y") || pnd_Trans_Type_Pnd_To_Cref.equals("Y") || pnd_Trans_Type_Pnd_To_Acc.equals("Y")))                       //Natural: IF #TO-RE = 'Y' OR #TO-CREF = 'Y' OR #TO-ACC = 'Y'
            {
                pnd_Date_A.setValueEdited(iaa_Xfr_Audit_Iaxfr_Effctve_Dte,new ReportEditMask("DDMMYYYY"));                                                                //Natural: MOVE EDITED IAXFR-EFFCTVE-DTE ( EM = DDMMYYYY ) TO #DATE-A
                ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Datvp1f().setValue(pnd_Date_A_Pnd_Date_N);                                                                            //Natural: ASSIGN CN-ANV-DATVP1F := #DATE-N
                ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Datvp2f().setValue(9999);                                                                                             //Natural: ASSIGN CN-ANV-DATVP2F := 9999
                //*  TO TIAA TRAD ONLY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Datvp2f().setValue(pnd_Date_A_Pnd_Date_N);                                                                            //Natural: ASSIGN CN-ANV-DATVP2F := #DATE-N
                ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Datvp1f().setValue(9999);                                                                                             //Natural: ASSIGN CN-ANV-DATVP1F := 9999
            }                                                                                                                                                             //Natural: END-IF
            //*  TRANSFER FROM TIAA TRADITIONAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  DON't write this paragraph
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  MAY BE USED LATER
            //*  MAY BE USED LATER
            //*  MAY BE USED LATER
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Var_P1v().setValue(" ");                                                                                                      //Natural: ASSIGN CN-ANV-VAR-P1V := ' '
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Var_P2v().setValue(" ");                                                                                                      //Natural: ASSIGN CN-ANV-VAR-P2V := ' '
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Datvp3f().setValue(9999);                                                                                                     //Natural: ASSIGN CN-ANV-DATVP3F := 9999
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Var_P3v().setValue(" ");                                                                                                      //Natural: ASSIGN CN-ANV-VAR-P3V := ' '
        //*  SET THE DATE OF THE ANNUITY UNIT VALUE
        //*  022009
        //*  022009
        //*  MAY BE USED LATER
        //*  "
        pnd_Date_A.setValueEdited(iaa_Xfr_Audit_Iaxfr_Effctve_Dte,new ReportEditMask("DDMMYYYY"));                                                                        //Natural: MOVE EDITED IAXFR-EFFCTVE-DTE ( EM = DDMMYYYY ) TO #DATE-A
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Payhddt().setValue(pnd_Date_A_Pnd_Date_N);                                                                                    //Natural: ASSIGN CN-ANV-PAYHDDT := #DATE-N
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Reale_M().setValue(-1);                                                                                                 //Natural: ASSIGN CN-ANV-ACCNT-REALE-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Reale_A().setValue(-1);                                                                                                 //Natural: ASSIGN CN-ANV-ACCNT-REALE-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Access_M().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-ACCESS-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Access_A().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-ACCESS-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Stock_M().setValue(-1);                                                                                                 //Natural: ASSIGN CN-ANV-ACCNT-STOCK-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Stock_A().setValue(-1);                                                                                                 //Natural: ASSIGN CN-ANV-ACCNT-STOCK-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mma_M().setValue(-1);                                                                                                   //Natural: ASSIGN CN-ANV-ACCNT-MMA-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mma_A().setValue(-1);                                                                                                   //Natural: ASSIGN CN-ANV-ACCNT-MMA-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Bond_M().setValue(-1);                                                                                                  //Natural: ASSIGN CN-ANV-ACCNT-BOND-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Bond_A().setValue(-1);                                                                                                  //Natural: ASSIGN CN-ANV-ACCNT-BOND-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Social_M().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-SOCIAL-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Social_A().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-SOCIAL-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Global_M().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-GLOBAL-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Global_A().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-GLOBAL-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_M().setValue(-1);                                                                                                   //Natural: ASSIGN CN-ANV-ACCNT-EQI-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_A().setValue(-1);                                                                                                   //Natural: ASSIGN CN-ANV-ACCNT-EQI-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Growth_M().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-GROWTH-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Growth_A().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-GROWTH-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Infb_M().setValue(-1);                                                                                                  //Natural: ASSIGN CN-ANV-ACCNT-INFB-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Infb_A().setValue(-1);                                                                                                  //Natural: ASSIGN CN-ANV-ACCNT-INFB-A := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mutual_M().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-MUTUAL-M := -1
        ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mutual_A().setValue(-1);                                                                                                //Natural: ASSIGN CN-ANV-ACCNT-MUTUAL-A := -1
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  SET THE ANNUITY VALUES OF THE FROM FUNDS
            short decideConditionsMet2369 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF IAXFR-FROM-FUND-CDE ( #I );//Natural: VALUE #FUND-CDE-REA
            if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Rea))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Reale_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                //Natural: ASSIGN CN-ANV-ACCNT-REALE-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Reale_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                //Natural: ASSIGN CN-ANV-ACCNT-REALE-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                    //*  022009 START
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-ACC
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Acc))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Access_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-ACCESS-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Access_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-ACCESS-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-STOCK
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Stock))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Stock_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                //Natural: ASSIGN CN-ANV-ACCNT-STOCK-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Stock_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                //Natural: ASSIGN CN-ANV-ACCNT-STOCK-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-MMA
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Mma))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mma_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-MMA-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mma_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-MMA-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-BOND
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Bond))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Bond_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-BOND-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Bond_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-BOND-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-SOCIAL
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Social))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Social_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-SOCIAL-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Social_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-SOCIAL-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-GLOBAL
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Global))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Global_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-GLOBAL-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Global_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-GLOBAL-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-EQUITY
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Equity))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-EQI-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-EQI-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-GROWTH
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Growth))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Growth_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-GROWTH-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Growth_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                               //Natural: ASSIGN CN-ANV-ACCNT-GROWTH-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-INFL
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Infl))))
            {
                decideConditionsMet2369++;
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                             //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Infb_A().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-INFB-A := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Infb_M().setValue(iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-INFB-M := IAXFR-FROM-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  SET THE ANNUITY VALUES OF THE TO FUNDS
            short decideConditionsMet2437 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF IAXFR-TO-FUND-CDE ( #I );//Natural: VALUE #FUND-CDE-REA
            if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Rea))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Reale_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-REALE-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Reale_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-REALE-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                    //*  022009 START
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-ACC
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Acc))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Access_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-ACCESS-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Access_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-ACCESS-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-STOCK
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Stock))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Stock_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-STOCK-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Stock_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                  //Natural: ASSIGN CN-ANV-ACCNT-STOCK-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-MMA
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Mma))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mma_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                    //Natural: ASSIGN CN-ANV-ACCNT-MMA-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Mma_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                    //Natural: ASSIGN CN-ANV-ACCNT-MMA-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-BOND
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Bond))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Bond_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                   //Natural: ASSIGN CN-ANV-ACCNT-BOND-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Bond_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                   //Natural: ASSIGN CN-ANV-ACCNT-BOND-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-SOCIAL
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Social))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Social_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-SOCIAL-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Social_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-SOCIAL-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-GLOBAL
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Global))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Global_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-GLOBAL-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Global_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-GLOBAL-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-EQUITY
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Equity))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                    //Natural: ASSIGN CN-ANV-ACCNT-EQI-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                    //Natural: ASSIGN CN-ANV-ACCNT-EQI-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-GROWTH
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Growth))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Growth_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-GROWTH-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Growth_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                 //Natural: ASSIGN CN-ANV-ACCNT-GROWTH-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #FUND-CDE-INFL
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Fund_Cde_Infl))))
            {
                decideConditionsMet2437++;
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                              //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Infb_A().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                   //Natural: ASSIGN CN-ANV-ACCNT-INFB-A := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6165.getCn_Anv_Struct_Cn_Anv_Accnt_Infb_M().setValue(iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val.getValue(pnd_I));                                   //Natural: ASSIGN CN-ANV-ACCNT-INFB-M := IAXFR-TO-REVAL-UNIT-VAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6165.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6165.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6165.getCn_Frm_Rec_Struct_Cn_Frm_Account_Type().getValue(1));                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE := CN-FRM-ACCOUNT-TYPE ( 1 )
        ldaPstl6165.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM := 0
        ldaPstl6165.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM := 0
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CN-ANV-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6165.getSort_Key_Struct(), ldaPstl6165.getCn_Anv_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-CONFIRM-ANNUITY-VAL
    }
    private void sub_Write_Fyi_Rec() throws Exception                                                                                                                     //Natural: WRITE-FYI-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO CN-FYI-STRUCT
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        pnd_J.nadd(1);                                                                                                                                                    //Natural: ASSIGN #J := #J + 1
        ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Msg_Number().getValue(pnd_J).setValue(5);                                                                                     //Natural: MOVE 05 TO CN-FYI-MSG-NUMBER ( #J )
        ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Srt_Num().getValue(pnd_J).setValue(5);                                                                                        //Natural: MOVE 005 TO CN-FYI-SRT-NUM ( #J )
        //*  ======================================================================
        //*  MSG = 15
        //*  CONDITIONAL :- TRANSFER MONTHLY TO MONTHLY
        //*  VARIABLE INCOME THAT CHANGES MONTHLY WILL BE REVALUED ON MM/20/YYYY ..
        //*  ======================================================================
        //*  IF IAXFR-FRM-UNIT-TYP(1) EQ #MONTHLY
        //*  CHANGED ABOVE COMMENTED TO FOLLOWING USE TO FIELD          3/99
        if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue("*").equals(pnd_Const_Pnd_Monthly)))                                                                       //Natural: IF IAXFR-TO-UNIT-TYP ( * ) EQ #MONTHLY
        {
            pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("AA");                                                                                                     //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'AA'
            //*  FETCH THE LATEST CNTRL REC FOR CNTRL CDE AA
            DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                 //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
            if (condition(Global.isEscape())) return;
            if (condition(pdaIatl400p.getPnd_Iatn400_Out_Pnd_Return_Code().notEquals(" ")))                                                                               //Natural: IF #IATN400-OUT.#RETURN-CODE NE ' '
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Could not find IA Control record (AA)."));                                           //Natural: COMPRESS 'Could not find IA Control record (AA).' INTO ##MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Nazn6032_Io_Pnd_Ia_Updte_Dte.setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #NAZN6032-IO.#IA-UPDTE-DTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                    //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PPCN-NBR := IAXFR-FROM-PPCN-NBR
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                  //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PAYEE-CDE := IAXFR-FROM-PAYEE-CDE
            vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("FIND01", true)))
            {
                vw_iaa_Cntrct_Prtcpnt_Role.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrct_Prtcpnt_Role.getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
                {
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Could not find IA Contract Particpant Role for",  //Natural: COMPRESS 'Could not find IA Contract Particpant Role for' ' IAXFR-FROM-PPCN-NBR = (' IAXFR-FROM-PPCN-NBR ') and' ' IAXFR-FROM-PAYEE-CDE = (' IAXFR-FROM-PAYEE-CDE ')' INTO ##MSG LEAVING NO SPACE
                        " IAXFR-FROM-PPCN-NBR = (", iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr, ") and", " IAXFR-FROM-PAYEE-CDE = (", iaa_Xfr_Audit_Iaxfr_From_Payee_Cde, 
                        ")"));
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                    sub_Process_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Nazn6032_Io_Pnd_Mode.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind);                                                                               //Natural: ASSIGN #NAZN6032-IO.#MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  FETCH THE  NEXT PAYMENT DATE USING THE CHECK-DATE AND THE MODE
            DbsUtil.callnat(Nazn6032.class , getCurrentProcessState(), pnd_Nazn6032_Io);                                                                                  //Natural: CALLNAT 'NAZN6032' #NAZN6032-IO
            if (condition(Global.isEscape())) return;
            pnd_J.nadd(1);                                                                                                                                                //Natural: ASSIGN #J := #J + 1
            ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Msg_Number().getValue(pnd_J).setValue(15);                                                                                //Natural: MOVE 15 TO CN-FYI-MSG-NUMBER ( #J )
            ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Srt_Num().getValue(pnd_J).setValue(15);                                                                                   //Natural: MOVE 015 TO CN-FYI-SRT-NUM ( #J )
            //*   THE REVALUED DATE IS THE 20TH DAY IN THE MONTH PRIOR TO NXT PYMNT
            pnd_Date_A.setValueEdited(pnd_Nazn6032_Io_Pnd_Next_Pymnt_Dte,new ReportEditMask("YYYYMMDD"));                                                                 //Natural: MOVE EDITED #NAZN6032-IO.#NEXT-PYMNT-DTE ( EM = YYYYMMDD ) TO #DATE-A
            pnd_Date_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                            //Natural: ASSIGN #DATE-YYYYMMDD := #DATE-N
            if (condition(pnd_Date_Yyyymmdd_Pnd_Date_Mm.equals(1)))                                                                                                       //Natural: IF #DATE-MM = 1
            {
                pnd_Date_Yyyymmdd_Pnd_Date_Yyyy.nsubtract(1);                                                                                                             //Natural: ASSIGN #DATE-YYYY := #DATE-YYYY - 1
                pnd_Date_Yyyymmdd_Pnd_Date_Mm.setValue(12);                                                                                                               //Natural: ASSIGN #DATE-MM := 12
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Date_Yyyymmdd_Pnd_Date_Mm.nsubtract(1);                                                                                                               //Natural: ASSIGN #DATE-MM := #DATE-MM -1
            }                                                                                                                                                             //Natural: END-IF
            ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Date1_Mm().getValue(pnd_J).setValue(pnd_Date_Yyyymmdd_Pnd_Date_Mm);                                                       //Natural: ASSIGN CN-FYI-DATE1-MM ( #J ) := #DATE-MM
            ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Date1_Dd().getValue(pnd_J).setValue(20);                                                                                  //Natural: ASSIGN CN-FYI-DATE1-DD ( #J ) := 20
            ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Date1_Yy().getValue(pnd_J).setValue(pnd_Date_Yyyymmdd_Pnd_Date_Yyyy);                                                     //Natural: ASSIGN CN-FYI-DATE1-YY ( #J ) := #DATE-YYYY
            //*  SET THE NEXT PAYMENT DATE
            pnd_Date_A.setValueEdited(pnd_Nazn6032_Io_Pnd_Next_Pymnt_Dte,new ReportEditMask("MMDDYYYY"));                                                                 //Natural: MOVE EDITED #NAZN6032-IO.#NEXT-PYMNT-DTE ( EM = MMDDYYYY ) TO #DATE-A
            ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Date2_F().getValue(pnd_J).setValue(pnd_Date_A_Pnd_Date_N);                                                                //Natural: MOVE #DATE-N TO CN-FYI-DATE2-F ( #J )
            //*  END MSG = 15
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.nadd(1);                                                                                                                                                    //Natural: ASSIGN #J := #J + 1
        ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Msg_Number().getValue(pnd_J).setValue(95);                                                                                    //Natural: MOVE 95 TO CN-FYI-MSG-NUMBER ( #J )
        ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Srt_Num().getValue(pnd_J).setValue(95);                                                                                       //Natural: MOVE 095 TO CN-FYI-SRT-NUM ( #J )
        ldaPstl6165.getCn_Fyi_Struct_Cn_Data_Occurs().setValue(pnd_J);                                                                                                    //Natural: ASSIGN CN-FYI-STRUCT.CN-DATA-OCCURS := #J
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6165.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6165.getSort_Key_Struct_Key_L4_Account_Type().setValue(99);                                                                                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE := 99
        ldaPstl6165.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(ldaPstl6165.getCn_Fyi_Struct_Cn_Fyi_Srt_Num().getValue(1));                                          //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM := CN-FYI-SRT-NUM ( 1 )
        ldaPstl6165.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM := 0
        pdaPsta9500.getPsta9500_Application_Key().reset();                                                                                                                //Natural: RESET PSTA9500.APPLICATION-KEY
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CN-FYI-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6165.getSort_Key_Struct(), ldaPstl6165.getCn_Fyi_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   WRITE-FYI-REC
    }
    private void sub_Call_Post_For_Close() throws Exception                                                                                                               //Natural: CALL-POST-FOR-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CALL POST CLOSE TO FINALISE THE REQUEST
        //* *CALLNAT 'PSTN9680'                             /* 082017 START
        //*  082017 END
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST-FOR-CLOSE
    }
    private void sub_Get_From_To_Tiaa_Rea_Cref() throws Exception                                                                                                         //Natural: GET-FROM-TO-TIAA-REA-CREF
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  CHECK THE FROM FUNDS
            //*  022009
            //*  022009
            //*  THIS IS A CREF FUND
            short decideConditionsMet2634 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF IAXFR-FRM-ACCT-CD ( #I );//Natural: VALUE ' '
            if (condition((iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet2634++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-STD
            else if (condition((iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Std))))
            {
                decideConditionsMet2634++;
                pnd_Trans_Type_Pnd_From_Std.setValue("Y");                                                                                                                //Natural: ASSIGN #FROM-STD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-GRAD
            else if (condition((iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Grad))))
            {
                decideConditionsMet2634++;
                pnd_Trans_Type_Pnd_From_Grad.setValue("Y");                                                                                                               //Natural: ASSIGN #FROM-GRAD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-RE
            else if (condition((iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Re))))
            {
                decideConditionsMet2634++;
                pnd_Trans_Type_Pnd_From_Re.setValue("Y");                                                                                                                 //Natural: ASSIGN #FROM-RE := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-ACC
            else if (condition((iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Acc))))
            {
                decideConditionsMet2634++;
                pnd_Trans_Type_Pnd_From_Acc.setValue("Y");                                                                                                                //Natural: ASSIGN #FROM-ACC := 'Y'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Trans_Type_Pnd_From_Cref.setValue("Y");                                                                                                               //Natural: ASSIGN #FROM-CREF := 'Y'
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CHECK THE TO FUNDS
            //*  022009
            //*  022009
            //*  THIS IS A CREF FUND
            short decideConditionsMet2654 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF IAXFR-TO-ACCT-CD ( #I );//Natural: VALUE ' '
            if (condition((iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet2654++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-STD
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Std))))
            {
                decideConditionsMet2654++;
                pnd_Trans_Type_Pnd_To_Std.setValue("Y");                                                                                                                  //Natural: ASSIGN #TO-STD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-GRAD
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Grad))))
            {
                decideConditionsMet2654++;
                pnd_Trans_Type_Pnd_To_Grad.setValue("Y");                                                                                                                 //Natural: ASSIGN #TO-GRAD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #TIAA-RE
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Re))))
            {
                decideConditionsMet2654++;
                pnd_Trans_Type_Pnd_To_Re.setValue("Y");                                                                                                                   //Natural: ASSIGN #TO-RE := 'Y'
            }                                                                                                                                                             //Natural: VALUE #TIAA-ACC
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Acc))))
            {
                decideConditionsMet2654++;
                pnd_Trans_Type_Pnd_To_Acc.setValue("Y");                                                                                                                  //Natural: ASSIGN #TO-ACC := 'Y'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Trans_Type_Pnd_To_Cref.setValue("Y");                                                                                                                 //Natural: ASSIGN #TO-CREF := 'Y'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  022009
        if (condition(pnd_Trans_Type_Pnd_From_Re.equals("Y") || pnd_Trans_Type_Pnd_From_Cref.equals("Y") || pnd_Trans_Type_Pnd_To_Re.equals("Y") || pnd_Trans_Type_Pnd_To_Cref.equals("Y")  //Natural: IF #FROM-RE = 'Y' OR #FROM-CREF = 'Y' OR #TO-RE = 'Y' OR #TO-CREF = 'Y' OR #FROM-ACC = 'Y' OR #TO-ACC = 'Y'
            || pnd_Trans_Type_Pnd_From_Acc.equals("Y") || pnd_Trans_Type_Pnd_To_Acc.equals("Y")))
        {
            if (condition(pnd_Trans_Type_Pnd_From_Std.equals("Y") || pnd_Trans_Type_Pnd_From_Grad.equals("Y") || pnd_Trans_Type_Pnd_To_Std.equals("Y")                    //Natural: IF #FROM-STD = 'Y' OR #FROM-GRAD = 'Y' OR #TO-STD = 'Y' OR #TO-GRAD = 'Y'
                || pnd_Trans_Type_Pnd_To_Grad.equals("Y")))
            {
                pnd_Var_Trad_Both.setValue("B");                                                                                                                          //Natural: ASSIGN #VAR-TRAD-BOTH := 'B'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Var_Trad_Both.setValue("V");                                                                                                                          //Natural: ASSIGN #VAR-TRAD-BOTH := 'V'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Trans_Type_Pnd_From_Std.equals("Y") || pnd_Trans_Type_Pnd_From_Grad.equals("Y") || pnd_Trans_Type_Pnd_To_Std.equals("Y")                    //Natural: IF #FROM-STD = 'Y' OR #FROM-GRAD = 'Y' OR #TO-STD = 'Y' OR #TO-GRAD = 'Y'
                || pnd_Trans_Type_Pnd_To_Grad.equals("Y")))
            {
                pnd_Var_Trad_Both.setValue("T");                                                                                                                          //Natural: ASSIGN #VAR-TRAD-BOTH := 'T'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Could not establish whether this transfer 'TO' is ", "Variable, Traditional or Both.")); //Natural: COMPRESS 'Could not establish whether this transfer "TO" is ' 'Variable, Traditional or Both.' INTO ##MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-FROM-TO-TIAA-REA-CREF
    }

    //
}
