/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:35:05 AM
**        * FROM NATURAL SUBPROGRAM : Iaan3300
************************************************************
**        * FILE NAME            : Iaan3300.java
**        * CLASS NAME           : Iaan3300
**        * INSTANCE NAME        : Iaan3300
************************************************************
************************************************************************
* PROGRAM: IAAN3300 (CLONE OF IAAN460)
* DATE   : 09/2010
* AUTHOR : OSCAR SOTTO
* MODE   : BATCH
* DESC   : THIS SUBPROGRAM WILL GO BACK UP TO 240 PAYMENTS
*          FOR A SPECIFIC MULTI FUNDED CONTRACT.
*
* HISTORY:
* 3/12   JUN TINIO : RATE BASE EXPANSION - SCAN ON 3/12
* 04/2017 O SOTTO  RE-STOWED ONLY FOR IAAL200B PIN EXPANSION.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan3300 extends BLNatBase
{
    // Data Areas
    private PdaIaapda_M pdaIaapda_M;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal200a ldaIaal200a;
    private LdaIaal205a ldaIaal205a;
    private LdaIaal201e ldaIaal201e;
    private LdaIaal201f ldaIaal201f;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_W_Cntrct;

    private DbsGroup pnd_W_Cntrct__R_Field_1;
    private DbsField pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3;

    private DbsGroup pnd_W_Cntrct__R_Field_2;
    private DbsField pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1;
    private DbsField pnd_W_Payee_Cde;

    private DbsGroup pnd_Tab_Fund_Table;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Fund;

    private DbsGroup pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Install_Date;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Auv;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Reval;
    private DbsField pnd_Tab_Fund_Table_Pnd_Table_Cnt;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Bottom_Date;
    private DbsField pnd_Ret_Cde;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_3;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_4;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_5;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_6;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Fund_Code_1_3;

    private DbsGroup pnd_Fund_Code_1_3__R_Field_7;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_1;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3;

    private DbsGroup pnd_Count;
    private DbsField pnd_Count_Pnd_Err_Cnt;
    private DbsField pnd_Count_Pnd_Rec_Cnt;
    private DbsField pnd_Count_Pnd_Install_Cnt;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_8;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_9;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_10;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_11;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_12;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_13;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_14;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_W_Install_Date_N;

    private DbsGroup pnd_W_Install_Date_N__R_Field_15;
    private DbsField pnd_W_Install_Date_N_Pnd_W_Install_Date_A;
    private DbsField pnd_Install_Date_A;

    private DbsGroup pnd_Install_Date_A__R_Field_16;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_N;

    private DbsGroup pnd_Install_Date_A__R_Field_17;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Yyyymm;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Dd;

    private DbsGroup pnd_Install_Date_A__R_Field_18;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Yyyy;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Mm;

    private DbsGroup pnd_Install_Date_A__R_Field_19;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Mm;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Dd;
    private DbsField pnd_Dated;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_20;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N;

    private DbsGroup pnd_Issue_Date_A__R_Field_21;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Dd;

    private DbsGroup pnd_Issue_Date_A__R_Field_22;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Mm;

    private DbsGroup pnd_Issue_Date_A__R_Field_23;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd;
    private DbsField pnd_Xfr_Issue_Date_A;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_24;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_25;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_26;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_27;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd;
    private DbsField pnd_W_Issue_Date;
    private DbsField pnd_W_Option;
    private DbsField pnd_I;
    private DbsField pnd_A;

    private DbsGroup pnd_A__R_Field_28;
    private DbsField pnd_A_Pnd_A_A;
    private DbsField pnd_B;
    private DbsField pnd_C;
    private DbsField pnd_Tbl_Cnt;
    private DbsField pnd_Cref;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Annu;
    private DbsField pnd_Unit;
    private DbsField pnd_Num;
    private DbsField pnd_Num_C;
    private DbsField pnd_W_Num;
    private DbsField pnd_Tab_Funds;
    private DbsField pnd_Tab_Units;
    private DbsField pnd_W_Tab_Funds;

    private DbsGroup pnd_W_Tab_Funds__R_Field_29;
    private DbsField pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1;
    private DbsField pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3;
    private DbsField pnd_Cref_Funds;
    private DbsField pnd_Tiaa_Funds;
    private DbsField pnd_Tab_Mode;
    private DbsField pnd_W_Mode;

    private DbsGroup pnd_W_Mode__R_Field_30;
    private DbsField pnd_W_Mode_Pnd_W_Mode_A;

    private DbsGroup pnd_A26_Fields;
    private DbsField pnd_A26_Fields_Pnd_A26_Call_Type;
    private DbsField pnd_A26_Fields_Pnd_A26_Fnd_Cde;
    private DbsField pnd_A26_Fields_Pnd_A26_Reval_Meth;
    private DbsField pnd_A26_Fields_Pnd_A26_Req_Dte;
    private DbsField pnd_A26_Fields_Pnd_A26_Prtc_Dte;
    private DbsField pnd_A26_Fields_Pnd_Rc_Pgm;

    private DbsGroup pnd_A26_Fields__R_Field_31;
    private DbsField pnd_A26_Fields_Pnd_Pgm;
    private DbsField pnd_A26_Fields_Pnd_Rc;
    private DbsField pnd_A26_Fields_Pnd_Auv;
    private DbsField pnd_A26_Fields_Pnd_Auv_Ret_Dte;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Request_Month;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Particip_Month;
    private DbsField pnd_Max_Fnds;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Rtn_Cde;
    private DbsField pnd_W_Amt;

    private DbsGroup pnd_W_Amt__R_Field_32;
    private DbsField pnd_W_Amt_Pnd_W_Amt_R;
    private DbsField pnd_Fund_Lst_Pd_Dte_Next;
    private DbsField pnd_W_Fund_Inverse_Lst_Pd_Dte_Next;
    private DbsField pnd_Historical_Last_Pd_Dte_Hold;
    private DbsField pnd_W_Per_Amt;
    private DbsField pnd_W_Per_Auv;
    private DbsField pnd_W_Div_Amt;
    private DbsField pnd_W_File;
    private DbsField pnd_W_Inverse_Date;
    private DbsField pnd_Change_In_Record;
    private DbsField pnd_Finish_Processing;
    private DbsField pnd_Valid_Date;
    private DbsField pnd_Val_Meth;
    private DbsField pnd_Error;
    private DbsField pnd_W_Fund_Dte;
    private DbsField pnd_Present_Year;

    private DbsGroup pnd_Present_Year__R_Field_33;
    private DbsField pnd_Present_Year_Pnd_Present_Year_Yyyy;
    private DbsField pnd_Present_Year_Pnd_Present_Year_Mm;
    private DbsField pnd_Tiaa_No_Units;
    private DbsField pnd_W_Bottom_Year;

    private DbsGroup pnd_W_Bottom_Year__R_Field_34;
    private DbsField pnd_W_Bottom_Year_Pnd_W_Date_Yyyy;
    private DbsField pnd_W_Bottom_Year_Pnd_W_Date_Mm;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaIaal205a = new LdaIaal205a();
        registerRecord(ldaIaal205a);
        registerRecord(ldaIaal205a.getVw_old_Tiaa_Rates());
        ldaIaal201e = new LdaIaal201e();
        registerRecord(ldaIaal201e);
        registerRecord(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());
        ldaIaal201f = new LdaIaal201f();
        registerRecord(ldaIaal201f);
        registerRecord(ldaIaal201f.getVw_iaa_Cref_Fund_Rcrd_1());

        // parameters
        parameters = new DbsRecord();
        pnd_W_Cntrct = parameters.newFieldInRecord("pnd_W_Cntrct", "#W-CNTRCT", FieldType.STRING, 10);
        pnd_W_Cntrct.setParameterOption(ParameterOption.ByReference);

        pnd_W_Cntrct__R_Field_1 = parameters.newGroupInRecord("pnd_W_Cntrct__R_Field_1", "REDEFINE", pnd_W_Cntrct);
        pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3 = pnd_W_Cntrct__R_Field_1.newFieldInGroup("pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3", "#PPCN-NBR-A3", FieldType.STRING, 3);

        pnd_W_Cntrct__R_Field_2 = pnd_W_Cntrct__R_Field_1.newGroupInGroup("pnd_W_Cntrct__R_Field_2", "REDEFINE", pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3);
        pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1 = pnd_W_Cntrct__R_Field_2.newFieldInGroup("pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1", "#PPCN-NBR-A1", FieldType.STRING, 1);
        pnd_W_Payee_Cde = parameters.newFieldInRecord("pnd_W_Payee_Cde", "#W-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_W_Payee_Cde.setParameterOption(ParameterOption.ByReference);

        pnd_Tab_Fund_Table = parameters.newGroupArrayInRecord("pnd_Tab_Fund_Table", "#TAB-FUND-TABLE", new DbsArrayController(1, 40));
        pnd_Tab_Fund_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Tab_Fund_Table_Pnd_Tab_Fund = pnd_Tab_Fund_Table.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Fund", "#TAB-FUND", FieldType.STRING, 1);

        pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info = pnd_Tab_Fund_Table.newGroupArrayInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info", "#TAB-FUND-INFO", new DbsArrayController(1, 
            240));
        pnd_Tab_Fund_Table_Pnd_Tab_Install_Date = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Install_Date", "#TAB-INSTALL-DATE", 
            FieldType.STRING, 8);
        pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt", "#TAB-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt", "#TAB-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units", "#TAB-FUND-UNITS", 
            FieldType.NUMERIC, 9, 3);
        pnd_Tab_Fund_Table_Pnd_Tab_Auv = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Auv", "#TAB-AUV", FieldType.NUMERIC, 
            9, 4);
        pnd_Tab_Fund_Table_Pnd_Tab_Reval = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Reval", "#TAB-REVAL", FieldType.STRING, 
            1);
        pnd_Tab_Fund_Table_Pnd_Table_Cnt = pnd_Tab_Fund_Table.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Table_Cnt", "#TABLE-CNT", FieldType.NUMERIC, 3);
        pnd_Check_Date = parameters.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Check_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Bottom_Date = parameters.newFieldInRecord("pnd_Bottom_Date", "#BOTTOM-DATE", FieldType.NUMERIC, 8);
        pnd_Bottom_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Ret_Cde = parameters.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.STRING, 2);
        pnd_Ret_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_4", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_4.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_4.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr", 
            "#OLD-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee", "#OLD-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte", 
            "#OLD-INVERSE-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code", "#OLD-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_6", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_Fund_Code_1_3", "#FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_Fund_Code_1_3__R_Field_7 = localVariables.newGroupInRecord("pnd_Fund_Code_1_3__R_Field_7", "REDEFINE", pnd_Fund_Code_1_3);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_1 = pnd_Fund_Code_1_3__R_Field_7.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_1", "#FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3 = pnd_Fund_Code_1_3__R_Field_7.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3", "#FUND-CODE-2-3", FieldType.STRING, 
            2);

        pnd_Count = localVariables.newGroupInRecord("pnd_Count", "#COUNT");
        pnd_Count_Pnd_Err_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Err_Cnt", "#ERR-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Rec_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Install_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Install_Cnt", "#INSTALL-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_8", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_9 = pnd_Save_Check_Dte_A__R_Field_8.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_9", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);

        pnd_Save_Check_Dte_A__R_Field_10 = pnd_Save_Check_Dte_A__R_Field_9.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_10", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_10.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_10.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_11 = pnd_Save_Check_Dte_A__R_Field_10.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_11", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yy);
        pnd_Save_Check_Dte_A_Pnd_Yy_A = pnd_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy_A", "#YY-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_12 = pnd_Save_Check_Dte_A__R_Field_9.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_12", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_13 = pnd_Save_Check_Dte_A__R_Field_9.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_13", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Dd);
        pnd_Save_Check_Dte_A_Pnd_Dd_A = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd_A", "#DD-A", FieldType.STRING, 2);

        pnd_Save_Check_Dte_A__R_Field_14 = pnd_Save_Check_Dte_A__R_Field_8.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_14", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_W_Install_Date_N = localVariables.newFieldInRecord("pnd_W_Install_Date_N", "#W-INSTALL-DATE-N", FieldType.NUMERIC, 8);

        pnd_W_Install_Date_N__R_Field_15 = localVariables.newGroupInRecord("pnd_W_Install_Date_N__R_Field_15", "REDEFINE", pnd_W_Install_Date_N);
        pnd_W_Install_Date_N_Pnd_W_Install_Date_A = pnd_W_Install_Date_N__R_Field_15.newFieldInGroup("pnd_W_Install_Date_N_Pnd_W_Install_Date_A", "#W-INSTALL-DATE-A", 
            FieldType.STRING, 8);
        pnd_Install_Date_A = localVariables.newFieldInRecord("pnd_Install_Date_A", "#INSTALL-DATE-A", FieldType.STRING, 8);

        pnd_Install_Date_A__R_Field_16 = localVariables.newGroupInRecord("pnd_Install_Date_A__R_Field_16", "REDEFINE", pnd_Install_Date_A);
        pnd_Install_Date_A_Pnd_Install_Date_N = pnd_Install_Date_A__R_Field_16.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_N", "#INSTALL-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Install_Date_A__R_Field_17 = pnd_Install_Date_A__R_Field_16.newGroupInGroup("pnd_Install_Date_A__R_Field_17", "REDEFINE", pnd_Install_Date_A_Pnd_Install_Date_N);
        pnd_Install_Date_A_Pnd_Install_Date_Yyyymm = pnd_Install_Date_A__R_Field_17.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Yyyymm", "#INSTALL-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Install_Date_A_Pnd_Install_Date_Dd = pnd_Install_Date_A__R_Field_17.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Dd", "#INSTALL-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Install_Date_A__R_Field_18 = pnd_Install_Date_A__R_Field_16.newGroupInGroup("pnd_Install_Date_A__R_Field_18", "REDEFINE", pnd_Install_Date_A_Pnd_Install_Date_N);
        pnd_Install_Date_A_Pnd_Install_Date_Yyyy = pnd_Install_Date_A__R_Field_18.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Yyyy", "#INSTALL-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Install_Date_A_Pnd_Install_Date_Mm = pnd_Install_Date_A__R_Field_18.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Mm", "#INSTALL-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Install_Date_A__R_Field_19 = localVariables.newGroupInRecord("pnd_Install_Date_A__R_Field_19", "REDEFINE", pnd_Install_Date_A);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy = pnd_Install_Date_A__R_Field_19.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy", "#INSTALL-DATE-A8-CCYY", 
            FieldType.STRING, 4);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Mm = pnd_Install_Date_A__R_Field_19.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Mm", "#INSTALL-DATE-A8-MM", 
            FieldType.STRING, 2);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Dd = pnd_Install_Date_A__R_Field_19.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Dd", "#INSTALL-DATE-A8-DD", 
            FieldType.STRING, 2);
        pnd_Dated = localVariables.newFieldInRecord("pnd_Dated", "#DATED", FieldType.DATE);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Issue_Date_A__R_Field_20 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_20", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N = pnd_Issue_Date_A__R_Field_20.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A__R_Field_21 = pnd_Issue_Date_A__R_Field_20.newGroupInGroup("pnd_Issue_Date_A__R_Field_21", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm = pnd_Issue_Date_A__R_Field_21.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm", "#ISSUE-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Issue_Date_A_Pnd_Issue_Date_Dd = pnd_Issue_Date_A__R_Field_21.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_22 = pnd_Issue_Date_A__R_Field_20.newGroupInGroup("pnd_Issue_Date_A__R_Field_22", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy = pnd_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_Mm = pnd_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_23 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_23", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy = pnd_Issue_Date_A__R_Field_23.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy", "#ISSUE-DATE-A8-CCYY", 
            FieldType.STRING, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm = pnd_Issue_Date_A__R_Field_23.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm", "#ISSUE-DATE-A8-MM", 
            FieldType.STRING, 2);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd = pnd_Issue_Date_A__R_Field_23.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd", "#ISSUE-DATE-A8-DD", 
            FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A = localVariables.newFieldInRecord("pnd_Xfr_Issue_Date_A", "#XFR-ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Xfr_Issue_Date_A__R_Field_24 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_24", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N = pnd_Xfr_Issue_Date_A__R_Field_24.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N", "#XFR-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Xfr_Issue_Date_A__R_Field_25 = pnd_Xfr_Issue_Date_A__R_Field_24.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_25", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm = pnd_Xfr_Issue_Date_A__R_Field_25.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm", 
            "#XFR-ISSUE-DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd = pnd_Xfr_Issue_Date_A__R_Field_25.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd", "#XFR-ISSUE-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_26 = pnd_Xfr_Issue_Date_A__R_Field_24.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_26", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy = pnd_Xfr_Issue_Date_A__R_Field_26.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy", 
            "#XFR-ISSUE-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm = pnd_Xfr_Issue_Date_A__R_Field_26.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm", "#XFR-ISSUE-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_27 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_27", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy = pnd_Xfr_Issue_Date_A__R_Field_27.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy", 
            "#XFR-ISSUE-DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm = pnd_Xfr_Issue_Date_A__R_Field_27.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm", 
            "#XFR-ISSUE-DATE-A8-MM", FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd = pnd_Xfr_Issue_Date_A__R_Field_27.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd", 
            "#XFR-ISSUE-DATE-A8-DD", FieldType.STRING, 2);
        pnd_W_Issue_Date = localVariables.newFieldInRecord("pnd_W_Issue_Date", "#W-ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_W_Option = localVariables.newFieldInRecord("pnd_W_Option", "#W-OPTION", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 6);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);

        pnd_A__R_Field_28 = localVariables.newGroupInRecord("pnd_A__R_Field_28", "REDEFINE", pnd_A);
        pnd_A_Pnd_A_A = pnd_A__R_Field_28.newFieldInGroup("pnd_A_Pnd_A_A", "#A-A", FieldType.STRING, 2);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_Tbl_Cnt = localVariables.newFieldInRecord("pnd_Tbl_Cnt", "#TBL-CNT", FieldType.NUMERIC, 3);
        pnd_Cref = localVariables.newFieldInRecord("pnd_Cref", "#CREF", FieldType.STRING, 1);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.STRING, 1);
        pnd_Annu = localVariables.newFieldInRecord("pnd_Annu", "#ANNU", FieldType.STRING, 1);
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 1);
        pnd_Num = localVariables.newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 2);
        pnd_Num_C = localVariables.newFieldInRecord("pnd_Num_C", "#NUM-C", FieldType.NUMERIC, 2);
        pnd_W_Num = localVariables.newFieldInRecord("pnd_W_Num", "#W-NUM", FieldType.NUMERIC, 2);
        pnd_Tab_Funds = localVariables.newFieldArrayInRecord("pnd_Tab_Funds", "#TAB-FUNDS", FieldType.STRING, 3, new DbsArrayController(1, 40));
        pnd_Tab_Units = localVariables.newFieldArrayInRecord("pnd_Tab_Units", "#TAB-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 40));
        pnd_W_Tab_Funds = localVariables.newFieldInRecord("pnd_W_Tab_Funds", "#W-TAB-FUNDS", FieldType.STRING, 3);

        pnd_W_Tab_Funds__R_Field_29 = localVariables.newGroupInRecord("pnd_W_Tab_Funds__R_Field_29", "REDEFINE", pnd_W_Tab_Funds);
        pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1 = pnd_W_Tab_Funds__R_Field_29.newFieldInGroup("pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1", "#W-TAB-FUNDS-1", FieldType.STRING, 
            1);
        pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3 = pnd_W_Tab_Funds__R_Field_29.newFieldInGroup("pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3", "#W-TAB-FUNDS-2-3", FieldType.STRING, 
            2);
        pnd_Cref_Funds = localVariables.newFieldInRecord("pnd_Cref_Funds", "#CREF-FUNDS", FieldType.STRING, 1);
        pnd_Tiaa_Funds = localVariables.newFieldInRecord("pnd_Tiaa_Funds", "#TIAA-FUNDS", FieldType.STRING, 1);
        pnd_Tab_Mode = localVariables.newFieldArrayInRecord("pnd_Tab_Mode", "#TAB-MODE", FieldType.NUMERIC, 3, new DbsArrayController(1, 12, 1, 3));
        pnd_W_Mode = localVariables.newFieldInRecord("pnd_W_Mode", "#W-MODE", FieldType.NUMERIC, 3);

        pnd_W_Mode__R_Field_30 = localVariables.newGroupInRecord("pnd_W_Mode__R_Field_30", "REDEFINE", pnd_W_Mode);
        pnd_W_Mode_Pnd_W_Mode_A = pnd_W_Mode__R_Field_30.newFieldInGroup("pnd_W_Mode_Pnd_W_Mode_A", "#W-MODE-A", FieldType.STRING, 3);

        pnd_A26_Fields = localVariables.newGroupInRecord("pnd_A26_Fields", "#A26-FIELDS");
        pnd_A26_Fields_Pnd_A26_Call_Type = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Call_Type", "#A26-CALL-TYPE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Fnd_Cde = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Fnd_Cde", "#A26-FND-CDE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Reval_Meth = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Reval_Meth", "#A26-REVAL-METH", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Req_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Req_Dte", "#A26-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_A26_Prtc_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Prtc_Dte", "#A26-PRTC-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Rc_Pgm = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Rc_Pgm", "#RC-PGM", FieldType.STRING, 11);

        pnd_A26_Fields__R_Field_31 = pnd_A26_Fields.newGroupInGroup("pnd_A26_Fields__R_Field_31", "REDEFINE", pnd_A26_Fields_Pnd_Rc_Pgm);
        pnd_A26_Fields_Pnd_Pgm = pnd_A26_Fields__R_Field_31.newFieldInGroup("pnd_A26_Fields_Pnd_Pgm", "#PGM", FieldType.STRING, 8);
        pnd_A26_Fields_Pnd_Rc = pnd_A26_Fields__R_Field_31.newFieldInGroup("pnd_A26_Fields_Pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_A26_Fields_Pnd_Auv = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_A26_Fields_Pnd_Auv_Ret_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Days_In_Request_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_A26_Fields_Pnd_Days_In_Particip_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Max_Fnds = localVariables.newFieldInRecord("pnd_Max_Fnds", "#MAX-FNDS", FieldType.PACKED_DECIMAL, 3);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Rtn_Cde = localVariables.newFieldInRecord("pnd_Rtn_Cde", "#RTN-CDE", FieldType.NUMERIC, 2);
        pnd_W_Amt = localVariables.newFieldInRecord("pnd_W_Amt", "#W-AMT", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_W_Amt__R_Field_32 = localVariables.newGroupInRecord("pnd_W_Amt__R_Field_32", "REDEFINE", pnd_W_Amt);
        pnd_W_Amt_Pnd_W_Amt_R = pnd_W_Amt__R_Field_32.newFieldInGroup("pnd_W_Amt_Pnd_W_Amt_R", "#W-AMT-R", FieldType.PACKED_DECIMAL, 9, 4);
        pnd_Fund_Lst_Pd_Dte_Next = localVariables.newFieldInRecord("pnd_Fund_Lst_Pd_Dte_Next", "#FUND-LST-PD-DTE-NEXT", FieldType.NUMERIC, 8);
        pnd_W_Fund_Inverse_Lst_Pd_Dte_Next = localVariables.newFieldInRecord("pnd_W_Fund_Inverse_Lst_Pd_Dte_Next", "#W-FUND-INVERSE-LST-PD-DTE-NEXT", 
            FieldType.NUMERIC, 8);
        pnd_Historical_Last_Pd_Dte_Hold = localVariables.newFieldInRecord("pnd_Historical_Last_Pd_Dte_Hold", "#HISTORICAL-LAST-PD-DTE-HOLD", FieldType.NUMERIC, 
            8);
        pnd_W_Per_Amt = localVariables.newFieldArrayInRecord("pnd_W_Per_Amt", "#W-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 40));
        pnd_W_Per_Auv = localVariables.newFieldArrayInRecord("pnd_W_Per_Auv", "#W-PER-AUV", FieldType.NUMERIC, 9, 4, new DbsArrayController(1, 40));
        pnd_W_Div_Amt = localVariables.newFieldArrayInRecord("pnd_W_Div_Amt", "#W-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 40));
        pnd_W_File = localVariables.newFieldInRecord("pnd_W_File", "#W-FILE", FieldType.STRING, 1);
        pnd_W_Inverse_Date = localVariables.newFieldInRecord("pnd_W_Inverse_Date", "#W-INVERSE-DATE", FieldType.NUMERIC, 8);
        pnd_Change_In_Record = localVariables.newFieldInRecord("pnd_Change_In_Record", "#CHANGE-IN-RECORD", FieldType.STRING, 1);
        pnd_Finish_Processing = localVariables.newFieldInRecord("pnd_Finish_Processing", "#FINISH-PROCESSING", FieldType.STRING, 1);
        pnd_Valid_Date = localVariables.newFieldInRecord("pnd_Valid_Date", "#VALID-DATE", FieldType.STRING, 1);
        pnd_Val_Meth = localVariables.newFieldInRecord("pnd_Val_Meth", "#VAL-METH", FieldType.STRING, 1);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 1);
        pnd_W_Fund_Dte = localVariables.newFieldInRecord("pnd_W_Fund_Dte", "#W-FUND-DTE", FieldType.NUMERIC, 8);
        pnd_Present_Year = localVariables.newFieldInRecord("pnd_Present_Year", "#PRESENT-YEAR", FieldType.NUMERIC, 8);

        pnd_Present_Year__R_Field_33 = localVariables.newGroupInRecord("pnd_Present_Year__R_Field_33", "REDEFINE", pnd_Present_Year);
        pnd_Present_Year_Pnd_Present_Year_Yyyy = pnd_Present_Year__R_Field_33.newFieldInGroup("pnd_Present_Year_Pnd_Present_Year_Yyyy", "#PRESENT-YEAR-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Present_Year_Pnd_Present_Year_Mm = pnd_Present_Year__R_Field_33.newFieldInGroup("pnd_Present_Year_Pnd_Present_Year_Mm", "#PRESENT-YEAR-MM", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_No_Units = localVariables.newFieldInRecord("pnd_Tiaa_No_Units", "#TIAA-NO-UNITS", FieldType.PACKED_DECIMAL, 7, 3);
        pnd_W_Bottom_Year = localVariables.newFieldInRecord("pnd_W_Bottom_Year", "#W-BOTTOM-YEAR", FieldType.NUMERIC, 8);

        pnd_W_Bottom_Year__R_Field_34 = localVariables.newGroupInRecord("pnd_W_Bottom_Year__R_Field_34", "REDEFINE", pnd_W_Bottom_Year);
        pnd_W_Bottom_Year_Pnd_W_Date_Yyyy = pnd_W_Bottom_Year__R_Field_34.newFieldInGroup("pnd_W_Bottom_Year_Pnd_W_Date_Yyyy", "#W-DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_W_Bottom_Year_Pnd_W_Date_Mm = pnd_W_Bottom_Year__R_Field_34.newFieldInGroup("pnd_W_Bottom_Year_Pnd_W_Date_Mm", "#W-DATE-MM", FieldType.NUMERIC, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal200b.initializeValues();
        ldaIaal200a.initializeValues();
        ldaIaal205a.initializeValues();
        ldaIaal201e.initializeValues();
        ldaIaal201f.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Fnds.setInitialValue(40);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan3300() throws Exception
    {
        super("Iaan3300");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Tab_Fund_Table.getValue("*").reset();                                                                                                                         //Natural: RESET #TAB-FUND-TABLE ( * )
                                                                                                                                                                          //Natural: PERFORM #SETUP-MODE-TABLE
        sub_Pnd_Setup_Mode_Table();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CNTRCT
        sub_Pnd_Process_Cntrct();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error.equals("Y")))                                                                                                                             //Natural: IF #ERROR = 'Y'
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CPR
        sub_Pnd_Process_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error.equals("Y")))                                                                                                                             //Natural: IF #ERROR = 'Y'
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #SET-START-DATE
        sub_Pnd_Set_Start_Date();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND
        sub_Pnd_Process_Fund();
        if (condition(Global.isEscape())) {return;}
        pnd_W_Num.setValue(pnd_Num);                                                                                                                                      //Natural: ASSIGN #W-NUM := #NUM
                                                                                                                                                                          //Natural: PERFORM #READ-HISTORICAL-ONCE
        sub_Pnd_Read_Historical_Once();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #CHECK-TEACHERS
        sub_Pnd_Check_Teachers();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error.equals("Y")))                                                                                                                             //Natural: IF #ERROR = 'Y'
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #CHECK-CREF
        sub_Pnd_Check_Cref();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error.equals("Y")))                                                                                                                             //Natural: IF #ERROR = 'Y'
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
            //*  FUND LOOP
        }                                                                                                                                                                 //Natural: END-IF
        RP:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Finish_Processing.equals("Y"))) {break;}                                                                                                    //Natural: UNTIL #FINISH-PROCESSING = 'Y'
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.greater(pnd_Fund_Lst_Pd_Dte_Next)))                                                                       //Natural: IF #INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT
            {
                if (condition(pnd_Valid_Date.equals("Y")))                                                                                                                //Natural: IF #VALID-DATE = 'Y'
                {
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO #NUM
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Num)); pnd_I.nadd(1))
                    {
                        pnd_W_Tab_Funds.setValue(pnd_Tab_Funds.getValue(pnd_I));                                                                                          //Natural: ASSIGN #W-TAB-FUNDS := #TAB-FUNDS ( #I )
                        pnd_Fund_2.setValue(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3);                                                                                         //Natural: ASSIGN #FUND-2 := #W-TAB-FUNDS-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue(pnd_I).setValue(pnd_Fund_1);                                                                             //Natural: ASSIGN #TAB-FUND ( #I ) := #FUND-1
                        pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_I).nadd(1);                                                                                         //Natural: ADD 1 TO #TABLE-CNT ( #I )
                        pnd_Tbl_Cnt.setValue(pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_I));                                                                           //Natural: ASSIGN #TBL-CNT := #TABLE-CNT ( #I )
                        pnd_Tab_Fund_Table_Pnd_Tab_Install_Date.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_Install_Date_A);                                                 //Natural: ASSIGN #TAB-INSTALL-DATE ( #I,#TBL-CNT ) := #INSTALL-DATE-A
                        short decideConditionsMet596 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W-TAB-FUNDS-1 = 'W' OR = '4'
                        if (condition(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("W") || pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("4")))
                        {
                            decideConditionsMet596++;
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                            sub_Pnd_Get_Unit_Value();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  HAPPENS WHEN INSTALL DATE = CHECK DATE
                            if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero())))                                                                                      //Natural: IF #RC GT 0
                            {
                                pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_I).nsubtract(1);                                                                            //Natural: SUBTRACT 1 FROM #TABLE-CNT ( #I )
                                if (condition(pnd_Tbl_Cnt.greater(1)))                                                                                                    //Natural: IF #TBL-CNT GT 1
                                {
                                    pnd_Tbl_Cnt.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #TBL-CNT
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Tab_Fund_Table_Pnd_Tab_Install_Date.getValue(pnd_I,pnd_Tbl_Cnt).reset();                                                              //Natural: RESET #TAB-INSTALL-DATE ( #I,#TBL-CNT )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_I,pnd_Tbl_Cnt).compute(new ComputeParameters(true, pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_I,pnd_Tbl_Cnt)),  //Natural: COMPUTE ROUNDED #TAB-PER-AMT ( #I,#TBL-CNT ) = ( #TAB-UNITS ( #I ) * #AUV )
                                    (pnd_Tab_Units.getValue(pnd_I).multiply(pnd_A26_Fields_Pnd_Auv)));
                                pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_Tab_Units.getValue(pnd_I));                                //Natural: ASSIGN #TAB-FUND-UNITS ( #I,#TBL-CNT ) := #TAB-UNITS ( #I )
                                pnd_Tab_Fund_Table_Pnd_Tab_Auv.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_A26_Fields_Pnd_Auv);                                              //Natural: ASSIGN #TAB-AUV ( #I,#TBL-CNT ) := #AUV
                                pnd_Tab_Fund_Table_Pnd_Tab_Reval.getValue(pnd_I,pnd_Tbl_Cnt).setValue("M");                                                               //Natural: ASSIGN #TAB-REVAL ( #I,#TBL-CNT ) := 'M'
                                //*  TIAA
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: WHEN #W-TAB-FUNDS-1 = 'U' OR = '2'
                        else if (condition(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("U") || pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("2")))
                        {
                            decideConditionsMet596++;
                            pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_W_Per_Amt.getValue(pnd_I));                                       //Natural: ASSIGN #TAB-PER-AMT ( #I,#TBL-CNT ) := #W-PER-AMT ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Auv.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_W_Per_Auv.getValue(pnd_I));                                           //Natural: ASSIGN #TAB-AUV ( #I,#TBL-CNT ) := #W-PER-AUV ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_Tab_Units.getValue(pnd_I));                                    //Natural: ASSIGN #TAB-FUND-UNITS ( #I,#TBL-CNT ) := #TAB-UNITS ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Reval.getValue(pnd_I,pnd_Tbl_Cnt).setValue("A");                                                                   //Natural: ASSIGN #TAB-REVAL ( #I,#TBL-CNT ) := 'A'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_W_Per_Amt.getValue(pnd_I));                                       //Natural: ASSIGN #TAB-PER-AMT ( #I,#TBL-CNT ) := #W-PER-AMT ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt.getValue(pnd_I,pnd_Tbl_Cnt).setValue(pnd_W_Div_Amt.getValue(pnd_I));                                       //Natural: ASSIGN #TAB-DIV-AMT ( #I,#TBL-CNT ) := #W-DIV-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RP"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RP"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #SUBTRACT-INSTALL-DATE
                sub_Pnd_Subtract_Install_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #CHECK-LOWER-BOUNDARY
                sub_Pnd_Check_Lower_Boundary();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Finish_Processing.equals("Y")))                                                                                                         //Natural: IF #FINISH-PROCESSING = 'Y'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #CHECK-TEACHERS
                    sub_Pnd_Check_Teachers();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RP"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RP"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Error.equals("Y")))                                                                                                                 //Natural: IF #ERROR = 'Y'
                    {
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #CHECK-CREF
                    sub_Pnd_Check_Cref();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RP"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RP"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Error.equals("Y")))                                                                                                                 //Natural: IF #ERROR = 'Y'
                    {
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Change_In_Record.setValue("Y");                                                                                                                       //Natural: MOVE 'Y' TO #CHANGE-IN-RECORD
                if (true) break RP;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RP. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pnd_Change_In_Record.equals("Y") && pnd_Finish_Processing.notEquals("Y")))                                                                          //Natural: IF #CHANGE-IN-RECORD = 'Y' AND #FINISH-PROCESSING NE 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM #CHANGE-IN-RECORD
            sub_Pnd_Change_In_Record();
            if (condition(Global.isEscape())) {return;}
            pnd_W_Num.setValue(pnd_Num_C);                                                                                                                                //Natural: ASSIGN #W-NUM := #NUM-C
            //*  HISTORICAL LOOP
        }                                                                                                                                                                 //Natural: END-IF
        RP2:                                                                                                                                                              //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Finish_Processing.equals("Y"))) {break;}                                                                                                    //Natural: UNTIL #FINISH-PROCESSING = 'Y'
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.greater(pnd_Fund_Lst_Pd_Dte_Next)))                                                                       //Natural: IF #INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT
            {
                if (condition(pnd_Valid_Date.equals("Y")))                                                                                                                //Natural: IF #VALID-DATE = 'Y'
                {
                    FOR02:                                                                                                                                                //Natural: FOR #I = 1 TO #NUM-C
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Num_C)); pnd_I.nadd(1))
                    {
                        pnd_W_Tab_Funds.setValue(pnd_Tab_Funds.getValue(pnd_I));                                                                                          //Natural: ASSIGN #W-TAB-FUNDS := #TAB-FUNDS ( #I )
                        pnd_Fund_2.setValue(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3);                                                                                         //Natural: ASSIGN #FUND-2 := #W-TAB-FUNDS-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Fund_1.equals(pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue("*"))))                                                                  //Natural: IF #FUND-1 = #TAB-FUND ( * )
                        {
                            DbsUtil.examine(new ExamineSource(pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue("*")), new ExamineSearch(pnd_Fund_1), new ExamineGivingIndex(pnd_C)); //Natural: EXAMINE #TAB-FUND ( * ) FOR #FUND-1 GIVING INDEX #C
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            FOR03:                                                                                                                                        //Natural: FOR #C 1 #MAX-FNDS
                            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_Max_Fnds)); pnd_C.nadd(1))
                            {
                                if (condition(pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue(pnd_C).equals(" ")))                                                               //Natural: IF #TAB-FUND ( #C ) = ' '
                                {
                                    pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue(pnd_C).setValue(pnd_Fund_1);                                                                 //Natural: ASSIGN #TAB-FUND ( #C ) := #FUND-1
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_C.equals(getZero())))                                                                                                           //Natural: IF #C EQ 0
                        {
                            getReports().write(0, "FUND NOT FOUND. FUND= ",pnd_Fund_1);                                                                                   //Natural: WRITE 'FUND NOT FOUND. FUND= ' #FUND-1
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "FUNDS TABLE",pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue("*"));                                                           //Natural: WRITE 'FUNDS TABLE' #TAB-FUND ( * )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_C.greater(pnd_Max_Fnds)))                                                                                                       //Natural: IF #C GT #MAX-FNDS
                        {
                            getReports().write(0, "FUND TABLE OVERFLOW!");                                                                                                //Natural: WRITE 'FUND TABLE OVERFLOW!'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "FUNDS TABLE",pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue("*"));                                                           //Natural: WRITE 'FUNDS TABLE' #TAB-FUND ( * )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_C).nadd(1);                                                                                         //Natural: ADD 1 TO #TABLE-CNT ( #C )
                        if (condition(pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_C).greater(240)))                                                                     //Natural: IF #TABLE-CNT ( #C ) > 240
                        {
                            pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_C).setValue(240);                                                                               //Natural: ASSIGN #TABLE-CNT ( #C ) := 240
                            getReports().write(0, "TABLE OVERFLOW");                                                                                                      //Natural: WRITE 'TABLE OVERFLOW'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Tbl_Cnt.setValue(pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_C));                                                                           //Natural: ASSIGN #TBL-CNT := #TABLE-CNT ( #C )
                        pnd_Tab_Fund_Table_Pnd_Tab_Install_Date.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_Install_Date_A);                                                 //Natural: ASSIGN #TAB-INSTALL-DATE ( #C,#TBL-CNT ) := #INSTALL-DATE-A
                        short decideConditionsMet687 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W-TAB-FUNDS-1 = 'W' OR = '4'
                        if (condition(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("W") || pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("4")))
                        {
                            decideConditionsMet687++;
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                            sub_Pnd_Get_Unit_Value();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  TIAA
                            pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_C,pnd_Tbl_Cnt).compute(new ComputeParameters(true, pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_C,pnd_Tbl_Cnt)),  //Natural: COMPUTE ROUNDED #TAB-PER-AMT ( #C,#TBL-CNT ) = ( #TAB-UNITS ( #I ) * #AUV )
                                (pnd_Tab_Units.getValue(pnd_I).multiply(pnd_A26_Fields_Pnd_Auv)));
                            pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_Tab_Units.getValue(pnd_I));                                    //Natural: ASSIGN #TAB-FUND-UNITS ( #C,#TBL-CNT ) := #TAB-UNITS ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Auv.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_A26_Fields_Pnd_Auv);                                                  //Natural: ASSIGN #TAB-AUV ( #C,#TBL-CNT ) := #AUV
                            pnd_Tab_Fund_Table_Pnd_Tab_Reval.getValue(pnd_C,pnd_Tbl_Cnt).setValue("M");                                                                   //Natural: ASSIGN #TAB-REVAL ( #C,#TBL-CNT ) := 'M'
                        }                                                                                                                                                 //Natural: WHEN #W-TAB-FUNDS-1 = 'U' OR = '2'
                        else if (condition(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("U") || pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("2")))
                        {
                            decideConditionsMet687++;
                            pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_W_Per_Amt.getValue(pnd_I));                                       //Natural: ASSIGN #TAB-PER-AMT ( #C,#TBL-CNT ) := #W-PER-AMT ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Auv.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_W_Per_Auv.getValue(pnd_I));                                           //Natural: ASSIGN #TAB-AUV ( #C,#TBL-CNT ) := #W-PER-AUV ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_Tab_Units.getValue(pnd_I));                                    //Natural: ASSIGN #TAB-FUND-UNITS ( #C,#TBL-CNT ) := #TAB-UNITS ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Reval.getValue(pnd_C,pnd_Tbl_Cnt).setValue("A");                                                                   //Natural: ASSIGN #TAB-REVAL ( #C,#TBL-CNT ) := 'A'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_W_Per_Amt.getValue(pnd_I));                                       //Natural: ASSIGN #TAB-PER-AMT ( #C,#TBL-CNT ) := #W-PER-AMT ( #I )
                            pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt.getValue(pnd_C,pnd_Tbl_Cnt).setValue(pnd_W_Div_Amt.getValue(pnd_I));                                       //Natural: ASSIGN #TAB-DIV-AMT ( #C,#TBL-CNT ) := #W-DIV-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RP2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RP2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #SUBTRACT-INSTALL-DATE
                sub_Pnd_Subtract_Install_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #CHECK-LOWER-BOUNDARY
                sub_Pnd_Check_Lower_Boundary();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Finish_Processing.equals("Y")))                                                                                                         //Natural: IF #FINISH-PROCESSING = 'Y'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #CHECK-TEACHERS
                    sub_Pnd_Check_Teachers();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RP2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RP2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Error.equals("Y")))                                                                                                                 //Natural: IF #ERROR = 'Y'
                    {
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #CHECK-CREF
                    sub_Pnd_Check_Cref();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RP2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RP2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Error.equals("Y")))                                                                                                                 //Natural: IF #ERROR = 'Y'
                    {
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #CHANGE-IN-RECORD
                sub_Pnd_Change_In_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_W_Num.setValue(pnd_Num_C);                                                                                                                            //Natural: ASSIGN #W-NUM := #NUM-C
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  COMMENT OUT AFTER TEST
        //* *IF #W-CNTRCT = 'Z0016304'
        //* *  WRITE 'CONTRACT' #W-CNTRCT #W-PAYEE-CDE
        //* *FOR #I 1 40
        //* *  IF #TAB-FUND (#I) = ' '
        //* *    ESCAPE BOTTOM
        //* *  END-IF
        //* *  FOR #C =  1 TO 240
        //* *    IF #TAB-INSTALL-DATE(#I,#C) = ' '
        //* *      ESCAPE BOTTOM
        //* *    END-IF
        //* *    DISPLAY
        //* *      'F' #TAB-FUND (#I) #I
        //* *      'CNT'#TABLE-CNT (#I)
        //* *      'DATE' #TAB-INSTALL-DATE(#I,#C)
        //* *      'AMT'  #TAB-PER-AMT(#I,#C)
        //* *      'DIV'   #TAB-DIV-AMT(#I,#C)
        //* *      'AUV'  #TAB-AUV(#I,#C)
        //* *      'REVL'  #TAB-REVAL (#I,#C)
        //* *  END-FOR
        //* *END-FOR
        //* *END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SET-START-DATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SUBTRACT-INSTALL-DATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-MODE
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CNTRCT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CPR
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHANGE-IN-RECORD
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-HISTORICAL-ONCE
        //* ***********************************************************************
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-SINGLE-BYTE-FUND
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-LOWER-BOUNDARY
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-TEACHERS
        //*          MOVE 'H1' TO #RET-CDE
        //*          MOVE 'Y' TO #ERROR
        //*          ESCAPE ROUTINE
        //*          MOVE 'H1' TO #RET-CDE
        //*          MOVE 'Y' TO #ERROR
        //*          ESCAPE ROUTINE
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-ANNU
        //*        MOVE 'H1' TO #RET-CDE
        //*        MOVE 'Y' TO #ERROR
        //*        ESCAPE ROUTINE
        //*        MOVE 'H1' TO #RET-CDE
        //*        MOVE 'Y' TO #ERROR
        //*        ESCAPE ROUTINE
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-CREF
        //*          MOVE 'H1' TO #RET-CDE
        //*          MOVE 'Y' TO #ERROR
        //*          ESCAPE ROUTINE
        //*          MOVE 'H1' TO #RET-CDE
        //*          MOVE 'Y' TO #ERROR
        //*          ESCAPE ROUTINE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-UNIT-VALUE
        //* **********************************************************************
        //* ******************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-MODE-TABLE
        //* ******************************************************************
    }
    private void sub_Pnd_Set_Start_Date() throws Exception                                                                                                                //Natural: #SET-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte.setValue(pnd_Check_Date);                                                                                                 //Natural: ASSIGN #SAVE-CHECK-DTE := #CHECK-DATE
        pnd_Install_Date_A.setValue(pnd_Save_Check_Dte_A);                                                                                                                //Natural: ASSIGN #INSTALL-DATE-A := #SAVE-CHECK-DTE-A
        pnd_Present_Year.setValue(pnd_Install_Date_A_Pnd_Install_Date_N);                                                                                                 //Natural: ASSIGN #PRESENT-YEAR := #INSTALL-DATE-N
        pnd_W_Bottom_Year.setValue(pnd_Install_Date_A_Pnd_Install_Date_N);                                                                                                //Natural: ASSIGN #W-BOTTOM-YEAR := #INSTALL-DATE-N
        if (condition(pnd_W_Bottom_Year_Pnd_W_Date_Mm.equals(1)))                                                                                                         //Natural: IF #W-DATE-MM = 01
        {
            pnd_W_Bottom_Year_Pnd_W_Date_Yyyy.nsubtract(1);                                                                                                               //Natural: SUBTRACT 1 FROM #W-DATE-YYYY
            pnd_W_Bottom_Year_Pnd_W_Date_Mm.setValue(12);                                                                                                                 //Natural: MOVE 12 TO #W-DATE-MM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Bottom_Year_Pnd_W_Date_Mm.nsubtract(1);                                                                                                                 //Natural: SUBTRACT 1 FROM #W-DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Bottom_Year_Pnd_W_Date_Yyyy.nsubtract(20);                                                                                                                  //Natural: SUBTRACT 20 FROM #W-DATE-YYYY
        if (condition(pnd_W_Mode.equals(100)))                                                                                                                            //Natural: IF #W-MODE = 100
        {
            pnd_Valid_Date.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #VALID-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-MODE
            sub_Pnd_Check_Mode();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CONTROL-DATES
    }
    private void sub_Pnd_Subtract_Install_Date() throws Exception                                                                                                         //Natural: #SUBTRACT-INSTALL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Valid_Date.reset();                                                                                                                                           //Natural: RESET #VALID-DATE
        if (condition(pnd_Install_Date_A_Pnd_Install_Date_Mm.equals(1)))                                                                                                  //Natural: IF #INSTALL-DATE-MM = 01
        {
            pnd_Install_Date_A_Pnd_Install_Date_Yyyy.nsubtract(1);                                                                                                        //Natural: SUBTRACT 1 FROM #INSTALL-DATE-YYYY
            pnd_Install_Date_A_Pnd_Install_Date_Mm.setValue(12);                                                                                                          //Natural: MOVE 12 TO #INSTALL-DATE-MM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Install_Date_A_Pnd_Install_Date_Mm.nsubtract(1);                                                                                                          //Natural: SUBTRACT 1 FROM #INSTALL-DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Mode.equals(100)))                                                                                                                            //Natural: IF #W-MODE = 100
        {
            pnd_Valid_Date.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #VALID-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-MODE
            sub_Pnd_Check_Mode();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_Mode() throws Exception                                                                                                                    //Natural: #CHECK-MODE
    {
        if (BLNatReinput.isReinput()) return;

        F5:                                                                                                                                                               //Natural: FOR #A = 1 TO 12
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(12)); pnd_A.nadd(1))
        {
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_A8_Mm.equals(pnd_A_Pnd_A_A)))                                                                               //Natural: IF #INSTALL-DATE-A8-MM = #A-A
            {
                F6:                                                                                                                                                       //Natural: FOR #B = 1 TO 3
                for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(3)); pnd_B.nadd(1))
                {
                    if (condition(pnd_W_Mode.equals(pnd_Tab_Mode.getValue(pnd_A,pnd_B))))                                                                                 //Natural: IF #W-MODE = #TAB-MODE ( #A,#B )
                    {
                        pnd_Valid_Date.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #VALID-DATE
                        if (true) break F5;                                                                                                                               //Natural: ESCAPE BOTTOM ( F5. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break F5;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F5. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Process_Cntrct() throws Exception                                                                                                                //Natural: #PROCESS-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W-CNTRCT
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_W_Cntrct, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT FOUND FOR ",pnd_W_Cntrct);                                                                                             //Natural: WRITE 'NO CONTRACT FOUND FOR ' #W-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error.setValue("Y");                                                                                                                                  //Natural: MOVE 'Y' TO #ERROR
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD = 0
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(1);                                                                                                           //Natural: ASSIGN #ISSUE-DATE-DD := 01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                             //Natural: ASSIGN #ISSUE-DATE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte());                                                                //Natural: ASSIGN #ISSUE-DATE-YYYYMM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            pnd_W_Option.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde());                                                                                           //Natural: ASSIGN #W-OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Process_Cpr() throws Exception                                                                                                                   //Natural: #PROCESS-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_W_Cntrct);                                                                                                  //Natural: ASSIGN #CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_W_Payee_Cde);                                                                                              //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #W-PAYEE-CDE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02")))
        {
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            //*   IF NO RECORD FOUND
            //*     WRITE 'NO CPR RECORD FOUND FOR ' #W-CNTRCT #W-PAYEE-CDE
            //*     MOVE 'Y' TO #ERROR
            //*     ESCAPE ROUTINE
            //*   END-NOREC
            pnd_W_Mode.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                                                                //Natural: MOVE CNTRCT-MODE-IND TO #W-MODE
            pnd_Xfr_Issue_Date_A.setValueEdited(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte(),new ReportEditMask("YYYYMMDD"));                                 //Natural: MOVE EDITED CPR-XFR-ISS-DTE ( EM = YYYYMMDD ) TO #XFR-ISSUE-DATE-A
            //*  WRITE '=' #XFR-ISSUE-DATE-A
            if (condition(pnd_Xfr_Issue_Date_A.equals(" ")))                                                                                                              //Natural: IF #XFR-ISSUE-DATE-A = ' '
            {
                pnd_W_Issue_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                                             //Natural: ASSIGN #W-ISSUE-DATE := #ISSUE-DATE-N
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Issue_Date.setValue(pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);                                                                                     //Natural: ASSIGN #W-ISSUE-DATE := #XFR-ISSUE-DATE-N
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_W_Option.equals(21) && pnd_W_Issue_Date.greater(19910301)))                                                                                     //Natural: IF #W-OPTION = 21 AND #W-ISSUE-DATE > 19910301
        {
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T")))                                                            //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T'
            {
                pnd_Annu.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #ANNU
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Annu.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #ANNU
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Process_Fund() throws Exception                                                                                                                  //Natural: #PROCESS-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Per_Amt.getValue("*").reset();                                                                                                                              //Natural: RESET #W-PER-AMT ( * ) #W-DIV-AMT ( * )
        pnd_W_Div_Amt.getValue("*").reset();
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_W_Cntrct);                                                                                                 //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_W_Payee_Cde);                                                                                                 //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-PAYEE-CDE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                pnd_Fund_Code_1_3.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                                      //Natural: MOVE TIAA-CMPNY-FUND-CDE TO #FUND-CODE-1-3
                pnd_Num.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #NUM
                pnd_Tab_Funds.getValue(pnd_Num).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                        //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE TO #TAB-FUNDS ( #NUM )
                if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                             //Natural: IF #FUND-CODE-1 = 'T'
                {
                    pnd_Tiaa.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #TIAA
                    pnd_W_Per_Amt.getValue(pnd_Num).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                                           //Natural: ADD TIAA-TOT-PER-AMT TO #W-PER-AMT ( #NUM )
                    pnd_W_Div_Amt.getValue(pnd_Num).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                                           //Natural: ADD TIAA-TOT-DIV-AMT TO #W-DIV-AMT ( #NUM )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Unit.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #UNIT
                    pnd_Tab_Units.getValue(pnd_Num).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt().getValue(1));                                             //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-UNITS-CNT ( 1 ) TO #TAB-UNITS ( #NUM )
                    pnd_W_Amt.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                                                             //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT TO #W-AMT
                    if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                        //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                    {
                        pnd_W_Per_Amt.getValue(pnd_Num).setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                                   //Natural: ASSIGN #W-PER-AMT ( #NUM ) := TIAA-TOT-PER-AMT
                        pnd_W_Per_Auv.getValue(pnd_Num).setValue(pnd_W_Amt_Pnd_W_Amt_R);                                                                                  //Natural: ASSIGN #W-PER-AUV ( #NUM ) := #W-AMT-R
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Change_In_Record() throws Exception                                                                                                              //Natural: #CHANGE-IN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Tiaa.reset();                                                                                                                                                 //Natural: RESET #TIAA #UNIT #W-PER-AMT ( * ) #W-DIV-AMT ( * ) #TAB-FUNDS ( * ) #TAB-UNITS ( * ) #NUM-C
        pnd_Unit.reset();
        pnd_W_Per_Amt.getValue("*").reset();
        pnd_W_Div_Amt.getValue("*").reset();
        pnd_Tab_Funds.getValue("*").reset();
        pnd_Tab_Units.getValue("*").reset();
        pnd_Num_C.reset();
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr.setValue(pnd_W_Cntrct);                                                                                             //Natural: ASSIGN #OLD-CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee.setValue(pnd_W_Payee_Cde);                                                                                             //Natural: ASSIGN #OLD-CNTRCT-PAYEE := #W-PAYEE-CDE
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte.setValue(pnd_W_Fund_Inverse_Lst_Pd_Dte_Next);                                                                        //Natural: ASSIGN #OLD-INVERSE-PD-DTE := #W-FUND-INVERSE-LST-PD-DTE-NEXT
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code.setValue(" ");                                                                                                            //Natural: ASSIGN #OLD-FUND-CODE := ' '
        ldaIaal205a.getVw_old_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ OLD-TIAA-RATES BY CNTRCT-PY-DTE-KEY STARTING FROM #CNTRCT-PY-DTE-KEY
        (
        "R3",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_Cntrct_Py_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") }
        );
        R3:
        while (condition(ldaIaal205a.getVw_old_Tiaa_Rates().readNextRow("R3")))
        {
            if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF OLD-TIAA-RATES.CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                if (condition(ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().equals(pnd_W_Fund_Inverse_Lst_Pd_Dte_Next)))                                         //Natural: IF FUND-INVRSE-LST-PD-DTE = #W-FUND-INVERSE-LST-PD-DTE-NEXT
                {
                    pnd_W_Inverse_Date.setValue(ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte());                                                                  //Natural: MOVE FUND-INVRSE-LST-PD-DTE TO #W-INVERSE-DATE
                    pnd_Fund_Code_1_3.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde());                                                                           //Natural: MOVE OLD-TIAA-RATES.CMPNY-FUND-CDE TO #FUND-CODE-1-3
                    pnd_Num_C.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NUM-C
                    pnd_Tab_Funds.getValue(pnd_Num_C).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde());                                                           //Natural: MOVE OLD-TIAA-RATES.CMPNY-FUND-CDE TO #TAB-FUNDS ( #NUM-C )
                    if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                         //Natural: IF #FUND-CODE-1 = 'T'
                    {
                        pnd_Tiaa.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #TIAA
                        pnd_W_Per_Amt.getValue(pnd_Num_C).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                                   //Natural: ASSIGN #W-PER-AMT ( #NUM-C ) := CNTRCT-TOT-PER-AMT
                        pnd_W_Div_Amt.getValue(pnd_Num_C).setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt());                                                   //Natural: ASSIGN #W-DIV-AMT ( #NUM-C ) := CNTRCT-TOT-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Unit.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #UNIT
                        pnd_Tab_Units.getValue(pnd_Num_C).setValue(ldaIaal205a.getOld_Tiaa_Rates_Tiaa_No_Units().getValue(1));                                            //Natural: MOVE TIAA-NO-UNITS ( 1 ) TO #TAB-UNITS ( #NUM-C )
                        pnd_W_Amt.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt());                                                                           //Natural: MOVE OLD-TIAA-RATES.CNTRCT-TOT-DIV-AMT TO #W-AMT
                        pnd_W_Fund_Dte.compute(new ComputeParameters(false, pnd_W_Fund_Dte), DbsField.subtract(100000000,ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte())); //Natural: COMPUTE #W-FUND-DTE = 100000000 - FUND-INVRSE-LST-PD-DTE
                        if (condition(((pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")) && pnd_W_Fund_Dte.greater(19970401)))) //Natural: IF #FUND-CODE-1 = 'U' OR = '2' AND #W-FUND-DTE > 19970401
                        {
                            pnd_W_Per_Amt.getValue(pnd_Num_C).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                                   //Natural: ADD CNTRCT-TOT-PER-AMT TO #W-PER-AMT ( #NUM-C )
                            pnd_W_Per_Auv.getValue(pnd_Num_C).setValue(pnd_W_Amt_Pnd_W_Amt_R);                                                                            //Natural: ASSIGN #W-PER-AUV ( #NUM-C ) := #W-AMT-R
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Fund_2.setValue(pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3);                                                                                     //Natural: ASSIGN #FUND-2 := #FUND-CODE-2-3
                            if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2")))                                //Natural: IF #FUND-CODE-1 = 'U' OR = '2'
                            {
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                                sub_Pnd_Get_Single_Byte_Fund();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("R3"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Val_Meth.setValue("A");                                                                                                               //Natural: MOVE 'A' TO #VAL-METH
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                                sub_Pnd_Get_Unit_Value();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("R3"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_W_Per_Amt.getValue(pnd_Num_C).compute(new ComputeParameters(true, pnd_W_Per_Amt.getValue(pnd_Num_C)), ldaIaal205a.getOld_Tiaa_Rates_Tiaa_No_Units().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-PER-AMT ( #NUM-C ) = TIAA-NO-UNITS ( 1 ) * #AUV
                                pnd_W_Per_Auv.getValue(pnd_Num_C).setValue(pnd_A26_Fields_Pnd_Auv);                                                                       //Natural: ASSIGN #W-PER-AUV ( #NUM-C ) := #AUV
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fund_Lst_Pd_Dte_Next.compute(new ComputeParameters(false, pnd_Fund_Lst_Pd_Dte_Next), DbsField.subtract(100000000,ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte())); //Natural: COMPUTE #FUND-LST-PD-DTE-NEXT = 100000000 - FUND-INVRSE-LST-PD-DTE
                    pnd_W_Fund_Inverse_Lst_Pd_Dte_Next.setValue(ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte());                                                  //Natural: ASSIGN #W-FUND-INVERSE-LST-PD-DTE-NEXT := FUND-INVRSE-LST-PD-DTE
                    if (true) break R3;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R3. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fund_Lst_Pd_Dte_Next.setValue(0);                                                                                                                     //Natural: MOVE 0 TO #FUND-LST-PD-DTE-NEXT
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_Historical_Once() throws Exception                                                                                                          //Natural: #READ-HISTORICAL-ONCE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr.setValue(pnd_W_Cntrct);                                                                                             //Natural: ASSIGN #OLD-CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee.setValue(pnd_W_Payee_Cde);                                                                                             //Natural: ASSIGN #OLD-CNTRCT-PAYEE := #W-PAYEE-CDE
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte.setValue(0);                                                                                                         //Natural: ASSIGN #OLD-INVERSE-PD-DTE := 0
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code.setValue(" ");                                                                                                            //Natural: ASSIGN #OLD-FUND-CODE := ' '
        ldaIaal205a.getVw_old_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) OLD-TIAA-RATES BY CNTRCT-PY-DTE-KEY STARTING FROM #CNTRCT-PY-DTE-KEY
        (
        "R2",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_Cntrct_Py_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") },
        1
        );
        R2:
        while (condition(ldaIaal205a.getVw_old_Tiaa_Rates().readNextRow("R2")))
        {
            if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF OLD-TIAA-RATES.CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                pnd_Fund_Lst_Pd_Dte_Next.compute(new ComputeParameters(false, pnd_Fund_Lst_Pd_Dte_Next), DbsField.subtract(100000000,ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte())); //Natural: COMPUTE #FUND-LST-PD-DTE-NEXT = 100000000 - FUND-INVRSE-LST-PD-DTE
                pnd_W_Fund_Inverse_Lst_Pd_Dte_Next.setValue(ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte());                                                      //Natural: ASSIGN #W-FUND-INVERSE-LST-PD-DTE-NEXT := FUND-INVRSE-LST-PD-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fund_Lst_Pd_Dte_Next.setValue(0);                                                                                                                     //Natural: MOVE 0 TO #FUND-LST-PD-DTE-NEXT #W-FUND-INVERSE-LST-PD-DTE-NEXT
                pnd_W_Fund_Inverse_Lst_Pd_Dte_Next.setValue(0);
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Get_Single_Byte_Fund() throws Exception                                                                                                          //Natural: #GET-SINGLE-BYTE-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_1.reset();                                                                                                                                               //Natural: RESET #FUND-1
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtn_Cde);                                                                  //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTN-CDE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Check_Lower_Boundary() throws Exception                                                                                                          //Natural: #CHECK-LOWER-BOUNDARY
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition((pnd_I.greater(getZero()) && pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_I).greaterOrEqual(240)) || (pnd_Install_Date_A_Pnd_Install_Date_N.less(pnd_W_Issue_Date))  //Natural: IF ( #I > 0 AND #TABLE-CNT ( #I ) >= 240 ) OR ( #INSTALL-DATE-N < #W-ISSUE-DATE ) OR ( #INSTALL-DATE-N < #W-BOTTOM-YEAR ) OR ( #INSTALL-DATE-N < #BOTTOM-DATE )
            || (pnd_Install_Date_A_Pnd_Install_Date_N.less(pnd_W_Bottom_Year)) || (pnd_Install_Date_A_Pnd_Install_Date_N.less(pnd_Bottom_Date))))
        {
            pnd_Finish_Processing.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #FINISH-PROCESSING
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_Teachers() throws Exception                                                                                                                //Natural: #CHECK-TEACHERS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(pnd_Annu.equals("Y") && pnd_Tiaa.equals("Y")))                                                                                                      //Natural: IF #ANNU = 'Y' AND #TIAA = 'Y'
        {
            pnd_Tiaa.setValue(" ");                                                                                                                                       //Natural: MOVE ' ' TO #TIAA
                                                                                                                                                                          //Natural: PERFORM #CHECK-ANNU
            sub_Pnd_Check_Annu();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa.equals("Y")))                                                                                                                              //Natural: IF #TIAA = 'Y'
        {
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_Yyyy.equals(pnd_Present_Year_Pnd_Present_Year_Yyyy.subtract(1))))                                           //Natural: IF #INSTALL-DATE-YYYY = #PRESENT-YEAR-YYYY - 1
            {
                if (condition(pnd_Install_Date_A_Pnd_Install_Date_Mm.equals(12)))                                                                                         //Natural: IF #INSTALL-DATE-MM = 12
                {
                    short decideConditionsMet1058 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT
                    if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.greater(pnd_Fund_Lst_Pd_Dte_Next)))
                    {
                        decideConditionsMet1058++;
                        //*            AND #FUND-LST-PD-DTE-NEXT >= #BOTTOM-DATE
                        getReports().write(0, "IN #CHECK-TEACHERS");                                                                                                      //Natural: WRITE 'IN #CHECK-TEACHERS'
                        if (Global.isEscape()) return;
                        getReports().write(0, "ERROR - THERE SHOULD HAVE BEEN A 12 DATE IN HISTORY");                                                                     //Natural: WRITE 'ERROR - THERE SHOULD HAVE BEEN A 12 DATE IN HISTORY'
                        if (Global.isEscape()) return;
                        getReports().write(0, "#INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT");                                                                                 //Natural: WRITE '#INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT'
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",pnd_Install_Date_A_Pnd_Install_Date_N,"=",pnd_Fund_Lst_Pd_Dte_Next);                                                    //Natural: WRITE '=' #INSTALL-DATE-N '=' #FUND-LST-PD-DTE-NEXT
                        if (Global.isEscape()) return;
                        getReports().write(0, "Contract"," payee",pnd_W_Cntrct);                                                                                          //Natural: WRITE 'Contract' ' payee' #W-CNTRCT
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: WHEN #INSTALL-DATE-N = #FUND-LST-PD-DTE-NEXT
                    else if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.equals(pnd_Fund_Lst_Pd_Dte_Next)))
                    {
                        decideConditionsMet1058++;
                        pnd_Present_Year_Pnd_Present_Year_Yyyy.nsubtract(1);                                                                                              //Natural: COMPUTE #PRESENT-YEAR-YYYY = #PRESENT-YEAR-YYYY -1
                    }                                                                                                                                                     //Natural: WHEN #INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT
                    else if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.less(pnd_Fund_Lst_Pd_Dte_Next)))
                    {
                        decideConditionsMet1058++;
                        getReports().write(0, "IN #CHECK-TEACHERS");                                                                                                      //Natural: WRITE 'IN #CHECK-TEACHERS'
                        if (Global.isEscape()) return;
                        getReports().write(0, "ERROR - THERE SHOULD HAVE BEEN A 12 DATE IN HISTORY");                                                                     //Natural: WRITE 'ERROR - THERE SHOULD HAVE BEEN A 12 DATE IN HISTORY'
                        if (Global.isEscape()) return;
                        getReports().write(0, "#INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT");                                                                                 //Natural: WRITE '#INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT'
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",pnd_Install_Date_A_Pnd_Install_Date_N,"=",pnd_Fund_Lst_Pd_Dte_Next);                                                    //Natural: WRITE '=' #INSTALL-DATE-N '=' #FUND-LST-PD-DTE-NEXT
                        if (Global.isEscape()) return;
                        getReports().write(0, "Contract"," payee",pnd_W_Cntrct);                                                                                          //Natural: WRITE 'Contract' ' payee' #W-CNTRCT
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_Yyyy.equals(pnd_Present_Year_Pnd_Present_Year_Yyyy.subtract(1))))                                           //Natural: IF #INSTALL-DATE-YYYY = #PRESENT-YEAR-YYYY - 1
            {
                if (condition(pnd_Install_Date_A_Pnd_Install_Date_Mm.equals(12)))                                                                                         //Natural: IF #INSTALL-DATE-MM = 12
                {
                    pnd_Present_Year_Pnd_Present_Year_Yyyy.nsubtract(1);                                                                                                  //Natural: COMPUTE #PRESENT-YEAR-YYYY = #PRESENT-YEAR-YYYY - 1
                    //*       WRITE '=' #PRESENT-YEAR 'SUBTRACT IN TEACHER PARA'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_Annu() throws Exception                                                                                                                    //Natural: #CHECK-ANNU
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(pnd_Install_Date_A_Pnd_Install_Date_Yyyy.equals(pnd_Present_Year_Pnd_Present_Year_Yyyy)))                                                           //Natural: IF #INSTALL-DATE-YYYY = #PRESENT-YEAR-YYYY
        {
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_Mm.equals(3)))                                                                                              //Natural: IF #INSTALL-DATE-MM = 03
            {
                short decideConditionsMet1092 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT
                if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.greater(pnd_Fund_Lst_Pd_Dte_Next)))
                {
                    decideConditionsMet1092++;
                    //*  TEMPOS
                    getReports().write(0, "IN #CHECK-ANNU");                                                                                                              //Natural: WRITE 'IN #CHECK-ANNU'
                    if (Global.isEscape()) return;
                    getReports().write(0, "ERROR - THERE SHOULD HAVE BEEN A 03 DATE IN HISTORY");                                                                         //Natural: WRITE 'ERROR - THERE SHOULD HAVE BEEN A 03 DATE IN HISTORY'
                    if (Global.isEscape()) return;
                    getReports().write(0, "#INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT");                                                                                     //Natural: WRITE '#INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT'
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Install_Date_A_Pnd_Install_Date_N,"=",pnd_Fund_Lst_Pd_Dte_Next);                                                        //Natural: WRITE '=' #INSTALL-DATE-N '=' #FUND-LST-PD-DTE-NEXT
                    if (Global.isEscape()) return;
                    getReports().write(0, "Contract"," payee",pnd_W_Cntrct);                                                                                              //Natural: WRITE 'Contract' ' payee' #W-CNTRCT
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN #INSTALL-DATE-N = #FUND-LST-PD-DTE-NEXT
                else if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.equals(pnd_Fund_Lst_Pd_Dte_Next)))
                {
                    decideConditionsMet1092++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN #INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT
                else if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.less(pnd_Fund_Lst_Pd_Dte_Next)))
                {
                    decideConditionsMet1092++;
                    getReports().write(0, "IN #CHECK-ANNU");                                                                                                              //Natural: WRITE 'IN #CHECK-ANNU'
                    if (Global.isEscape()) return;
                    getReports().write(0, "ERROR - THERE SHOULD HAVE BEEN A 03 DATE IN HISTORY");                                                                         //Natural: WRITE 'ERROR - THERE SHOULD HAVE BEEN A 03 DATE IN HISTORY'
                    if (Global.isEscape()) return;
                    getReports().write(0, "#INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT");                                                                                     //Natural: WRITE '#INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT'
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Install_Date_A_Pnd_Install_Date_N,"=",pnd_Fund_Lst_Pd_Dte_Next);                                                        //Natural: WRITE '=' #INSTALL-DATE-N '=' #FUND-LST-PD-DTE-NEXT
                    if (Global.isEscape()) return;
                    getReports().write(0, "Contract"," payee",pnd_W_Cntrct);                                                                                              //Natural: WRITE 'Contract' ' payee' #W-CNTRCT
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_Cref() throws Exception                                                                                                                    //Natural: #CHECK-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(pnd_Unit.equals("Y")))                                                                                                                              //Natural: IF #UNIT = 'Y'
        {
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_Yyyy.equals(pnd_Present_Year_Pnd_Present_Year_Yyyy) && pnd_Install_Date_A_Pnd_Install_Date_Mm.equals(4)))   //Natural: IF #INSTALL-DATE-YYYY = #PRESENT-YEAR-YYYY AND #INSTALL-DATE-MM = 04
            {
                if (condition(pnd_Install_Date_A_Pnd_Install_Date_Yyyy.greaterOrEqual(1998)))                                                                             //Natural: IF #INSTALL-DATE-YYYY >= 1998
                {
                    short decideConditionsMet1119 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT
                    if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.greater(pnd_Fund_Lst_Pd_Dte_Next)))
                    {
                        decideConditionsMet1119++;
                        getReports().write(0, "IN #CHECK-CREF");                                                                                                          //Natural: WRITE 'IN #CHECK-CREF'
                        if (Global.isEscape()) return;
                        getReports().write(0, "ERROR - THERE SHOULD HAVE BEEN A 04 DATE IN HISTORY");                                                                     //Natural: WRITE 'ERROR - THERE SHOULD HAVE BEEN A 04 DATE IN HISTORY'
                        if (Global.isEscape()) return;
                        getReports().write(0, "#INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT");                                                                                 //Natural: WRITE '#INSTALL-DATE-N > #FUND-LST-PD-DTE-NEXT'
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",pnd_Install_Date_A_Pnd_Install_Date_N,"=",pnd_Fund_Lst_Pd_Dte_Next);                                                    //Natural: WRITE '=' #INSTALL-DATE-N '=' #FUND-LST-PD-DTE-NEXT
                        if (Global.isEscape()) return;
                        getReports().write(0, "Contract"," payee",pnd_W_Cntrct);                                                                                          //Natural: WRITE 'Contract' ' payee' #W-CNTRCT
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: WHEN #INSTALL-DATE-N = #FUND-LST-PD-DTE-NEXT
                    else if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.equals(pnd_Fund_Lst_Pd_Dte_Next)))
                    {
                        decideConditionsMet1119++;
                        ignore();
                    }                                                                                                                                                     //Natural: WHEN #INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT
                    else if (condition(pnd_Install_Date_A_Pnd_Install_Date_N.less(pnd_Fund_Lst_Pd_Dte_Next)))
                    {
                        decideConditionsMet1119++;
                        getReports().write(0, "IN #CHECK-CREF");                                                                                                          //Natural: WRITE 'IN #CHECK-CREF'
                        if (Global.isEscape()) return;
                        getReports().write(0, "ERROR - THERE SHOULD HAVE BEEN A 04 DATE IN HISTORY");                                                                     //Natural: WRITE 'ERROR - THERE SHOULD HAVE BEEN A 04 DATE IN HISTORY'
                        if (Global.isEscape()) return;
                        getReports().write(0, "#INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT");                                                                                 //Natural: WRITE '#INSTALL-DATE-N < #FUND-LST-PD-DTE-NEXT'
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",pnd_Install_Date_A_Pnd_Install_Date_N,"=",pnd_Fund_Lst_Pd_Dte_Next);                                                    //Natural: WRITE '=' #INSTALL-DATE-N '=' #FUND-LST-PD-DTE-NEXT
                        if (Global.isEscape()) return;
                        getReports().write(0, "Contract"," payee",pnd_W_Cntrct);                                                                                          //Natural: WRITE 'Contract' ' payee' #W-CNTRCT
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Install_Date_A_Pnd_Install_Date_Mm.equals(4) && pnd_Install_Date_A_Pnd_Install_Date_N.less(19970501)))                                      //Natural: IF #INSTALL-DATE-MM = 04 AND #INSTALL-DATE-N < 19970501
            {
                FOR04:                                                                                                                                                    //Natural: FOR #I = 1 TO #W-NUM
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_W_Num)); pnd_I.nadd(1))
                {
                    pnd_W_Tab_Funds.setValue(pnd_Tab_Funds.getValue(pnd_I));                                                                                              //Natural: ASSIGN #W-TAB-FUNDS := #TAB-FUNDS ( #I )
                    pnd_Fund_2.setValue(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_2_3);                                                                                             //Natural: ASSIGN #FUND-2 := #W-TAB-FUNDS-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                    sub_Pnd_Get_Single_Byte_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("U") || pnd_W_Tab_Funds_Pnd_W_Tab_Funds_1.equals("2")))                                        //Natural: IF #W-TAB-FUNDS-1 = 'U' OR = '2'
                    {
                        pnd_Val_Meth.setValue("A");                                                                                                                       //Natural: MOVE 'A' TO #VAL-METH
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Per_Amt.getValue(pnd_I).compute(new ComputeParameters(true, pnd_W_Per_Amt.getValue(pnd_I)), pnd_Tab_Units.getValue(pnd_I).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-PER-AMT ( #I ) = #TAB-UNITS ( #I ) * #AUV
                        pnd_W_Per_Auv.getValue(pnd_I).setValue(pnd_A26_Fields_Pnd_Auv);                                                                                   //Natural: ASSIGN #W-PER-AUV ( #I ) := #AUV
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Get_Unit_Value() throws Exception                                                                                                                //Natural: #GET-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Install_Date_N.setValue(pnd_Install_Date_A_Pnd_Install_Date_N);                                                                                             //Natural: ASSIGN #W-INSTALL-DATE-N := #INSTALL-DATE-N
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_A26_Fields_Pnd_Auv.reset();                                                                                                                               //Natural: RESET #AUV #AUV-RET-DTE #RC #A26-PRTC-DTE
            pnd_A26_Fields_Pnd_Auv_Ret_Dte.reset();
            pnd_A26_Fields_Pnd_Rc.reset();
            pnd_A26_Fields_Pnd_A26_Prtc_Dte.reset();
            pnd_A26_Fields_Pnd_A26_Call_Type.setValue("P");                                                                                                               //Natural: ASSIGN #A26-CALL-TYPE := 'P'
            pnd_A26_Fields_Pnd_A26_Req_Dte.setValue(pnd_W_Install_Date_N);                                                                                                //Natural: ASSIGN #A26-REQ-DTE := #W-INSTALL-DATE-N
            pnd_A26_Fields_Pnd_A26_Prtc_Dte.setValue(pnd_W_Issue_Date);                                                                                                   //Natural: ASSIGN #A26-PRTC-DTE := #W-ISSUE-DATE
            pnd_A26_Fields_Pnd_A26_Fnd_Cde.setValue(pnd_Fund_1);                                                                                                          //Natural: ASSIGN #A26-FND-CDE := #FUND-1
            pnd_A26_Fields_Pnd_A26_Reval_Meth.setValue(pnd_Val_Meth);                                                                                                     //Natural: ASSIGN #A26-REVAL-METH := #VAL-METH
            DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pnd_A26_Fields);                                                                                    //Natural: CALLNAT 'AIAN026' #A26-FIELDS
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            //*    #RC #AUV #AUV-RET-DTE   /* 3/12
            //*    #DAYS-IN-REQUEST-MONTH  /* 3/12
            //*    #DAYS-IN-PARTICIP-MONTH /* 3/12
            if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero()) && pnd_A26_Fields_Pnd_Rc.notEquals(615)))                                                              //Natural: IF #RC > 0 AND #RC NE 615
            {
                getReports().write(0, NEWLINE,"ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ",pnd_A26_Fields_Pnd_Rc);                                                       //Natural: WRITE / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RC
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "CONTRACT NUMBER =>",pnd_W_Cntrct,pnd_W_Payee_Cde);                                                                                 //Natural: WRITE 'CONTRACT NUMBER =>' #W-CNTRCT #W-PAYEE-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  GET THE LATEST AVAILABLE UV, USUALLY THE CURRENT
            if (condition(pnd_A26_Fields_Pnd_Rc.equals(615)))                                                                                                             //Natural: IF #RC = 615
            {
                //*              /* DATE
                if (condition(pnd_W_Install_Date_N.equals(pnd_Install_Date_A_Pnd_Install_Date_N)))                                                                        //Natural: IF #W-INSTALL-DATE-N = #INSTALL-DATE-N
                {
                    pnd_W_Install_Date_N.setValue(Global.getDATN());                                                                                                      //Natural: ASSIGN #W-INSTALL-DATE-N := *DATN
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Dated.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Install_Date_N_Pnd_W_Install_Date_A);                                                   //Natural: MOVE EDITED #W-INSTALL-DATE-A TO #DATED ( EM = YYYYMMDD )
                    pnd_Dated.nsubtract(1);                                                                                                                               //Natural: SUBTRACT 1 FROM #DATED
                    pnd_W_Install_Date_N_Pnd_W_Install_Date_A.setValueEdited(pnd_Dated,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #DATED ( EM = YYYYMMDD ) TO #W-INSTALL-DATE-A
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Setup_Mode_Table() throws Exception                                                                                                              //Natural: #SETUP-MODE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Tab_Mode.getValue(1,1).setValue(601);                                                                                                                         //Natural: MOVE 601 TO #TAB-MODE ( 1,1 )
        pnd_Tab_Mode.getValue(1,2).setValue(701);                                                                                                                         //Natural: MOVE 701 TO #TAB-MODE ( 1,2 )
        pnd_Tab_Mode.getValue(1,3).setValue(801);                                                                                                                         //Natural: MOVE 801 TO #TAB-MODE ( 1,3 )
        pnd_Tab_Mode.getValue(2,1).setValue(602);                                                                                                                         //Natural: MOVE 602 TO #TAB-MODE ( 2,1 )
        pnd_Tab_Mode.getValue(2,2).setValue(702);                                                                                                                         //Natural: MOVE 702 TO #TAB-MODE ( 2,2 )
        pnd_Tab_Mode.getValue(2,3).setValue(802);                                                                                                                         //Natural: MOVE 802 TO #TAB-MODE ( 2,3 )
        pnd_Tab_Mode.getValue(3,1).setValue(603);                                                                                                                         //Natural: MOVE 603 TO #TAB-MODE ( 3,1 )
        pnd_Tab_Mode.getValue(3,2).setValue(703);                                                                                                                         //Natural: MOVE 703 TO #TAB-MODE ( 3,2 )
        pnd_Tab_Mode.getValue(3,3).setValue(803);                                                                                                                         //Natural: MOVE 803 TO #TAB-MODE ( 3,3 )
        pnd_Tab_Mode.getValue(4,1).setValue(601);                                                                                                                         //Natural: MOVE 601 TO #TAB-MODE ( 4,1 )
        pnd_Tab_Mode.getValue(4,2).setValue(704);                                                                                                                         //Natural: MOVE 704 TO #TAB-MODE ( 4,2 )
        pnd_Tab_Mode.getValue(4,3).setValue(804);                                                                                                                         //Natural: MOVE 804 TO #TAB-MODE ( 4,3 )
        pnd_Tab_Mode.getValue(5,1).setValue(602);                                                                                                                         //Natural: MOVE 602 TO #TAB-MODE ( 5,1 )
        pnd_Tab_Mode.getValue(5,2).setValue(705);                                                                                                                         //Natural: MOVE 705 TO #TAB-MODE ( 5,2 )
        pnd_Tab_Mode.getValue(5,3).setValue(805);                                                                                                                         //Natural: MOVE 805 TO #TAB-MODE ( 5,3 )
        pnd_Tab_Mode.getValue(6,1).setValue(603);                                                                                                                         //Natural: MOVE 603 TO #TAB-MODE ( 6,1 )
        pnd_Tab_Mode.getValue(6,2).setValue(706);                                                                                                                         //Natural: MOVE 706 TO #TAB-MODE ( 6,2 )
        pnd_Tab_Mode.getValue(6,3).setValue(806);                                                                                                                         //Natural: MOVE 806 TO #TAB-MODE ( 6,3 )
        pnd_Tab_Mode.getValue(7,1).setValue(601);                                                                                                                         //Natural: MOVE 601 TO #TAB-MODE ( 7,1 )
        pnd_Tab_Mode.getValue(7,2).setValue(701);                                                                                                                         //Natural: MOVE 701 TO #TAB-MODE ( 7,2 )
        pnd_Tab_Mode.getValue(7,3).setValue(807);                                                                                                                         //Natural: MOVE 807 TO #TAB-MODE ( 7,3 )
        pnd_Tab_Mode.getValue(8,1).setValue(602);                                                                                                                         //Natural: MOVE 602 TO #TAB-MODE ( 8,1 )
        pnd_Tab_Mode.getValue(8,2).setValue(702);                                                                                                                         //Natural: MOVE 702 TO #TAB-MODE ( 8,2 )
        pnd_Tab_Mode.getValue(8,3).setValue(808);                                                                                                                         //Natural: MOVE 808 TO #TAB-MODE ( 8,3 )
        pnd_Tab_Mode.getValue(9,1).setValue(603);                                                                                                                         //Natural: MOVE 603 TO #TAB-MODE ( 9,1 )
        pnd_Tab_Mode.getValue(9,2).setValue(703);                                                                                                                         //Natural: MOVE 703 TO #TAB-MODE ( 9,2 )
        pnd_Tab_Mode.getValue(9,3).setValue(809);                                                                                                                         //Natural: MOVE 809 TO #TAB-MODE ( 9,3 )
        pnd_Tab_Mode.getValue(10,1).setValue(601);                                                                                                                        //Natural: MOVE 601 TO #TAB-MODE ( 10,1 )
        pnd_Tab_Mode.getValue(10,2).setValue(704);                                                                                                                        //Natural: MOVE 704 TO #TAB-MODE ( 10,2 )
        pnd_Tab_Mode.getValue(10,3).setValue(810);                                                                                                                        //Natural: MOVE 810 TO #TAB-MODE ( 10,3 )
        pnd_Tab_Mode.getValue(11,1).setValue(602);                                                                                                                        //Natural: MOVE 602 TO #TAB-MODE ( 11,1 )
        pnd_Tab_Mode.getValue(11,2).setValue(705);                                                                                                                        //Natural: MOVE 705 TO #TAB-MODE ( 11,2 )
        pnd_Tab_Mode.getValue(11,3).setValue(811);                                                                                                                        //Natural: MOVE 811 TO #TAB-MODE ( 11,3 )
        pnd_Tab_Mode.getValue(12,1).setValue(603);                                                                                                                        //Natural: MOVE 603 TO #TAB-MODE ( 12,1 )
        pnd_Tab_Mode.getValue(12,2).setValue(706);                                                                                                                        //Natural: MOVE 706 TO #TAB-MODE ( 12,2 )
        pnd_Tab_Mode.getValue(12,3).setValue(812);                                                                                                                        //Natural: MOVE 812 TO #TAB-MODE ( 12,3 )
    }

    //
}
