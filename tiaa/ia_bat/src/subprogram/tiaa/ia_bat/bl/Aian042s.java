/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:57:42 PM
**        * FROM NATURAL SUBPROGRAM : Aian042s
************************************************************
**        * FILE NAME            : Aian042s.java
**        * CLASS NAME           : Aian042s
**        * INSTANCE NAME        : Aian042s
************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aian042s extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAial0421 pdaAial0421;
    private LdaAial0391 ldaAial0391;
    private LdaAial0393 ldaAial0393;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaview;
    private DbsField iaview_Cntrct_Ppcn_Nbr;
    private DbsField iaview_Cntrct_Optn_Cde;
    private DbsField iaview_Cntrct_Orgn_Cde;
    private DbsField iaview_Cntrct_Issue_Dte;
    private DbsField iaview_Cntrct_Mode_Ind;
    private DbsField iaview_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaview_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaview_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaview_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaview_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaview_Cntrct_Scnd_Annt_Dod_Dte;

    private DataAccessProgramView vw_iapart;
    private DbsField iapart_Cntrct_Mode_Ind;
    private DbsField iapart_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Record_Key;

    private DbsGroup pnd_Record_Key__R_Field_1;
    private DbsField pnd_Record_Key_Pnd_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Record_Key_Pnd_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_2;
    private DbsField pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_Meth;
    private DbsField pnd_Record_Key_1;

    private DbsGroup pnd_Record_Key_1__R_Field_2;
    private DbsField pnd_Record_Key_1_Pnd_Key1_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Record_Key_1_Pnd_Key1_Cntrct_Payee_Cde;
    private DbsField pnd_I;
    private DbsField pnd_Temp_Rate_A;

    private DbsGroup pnd_Temp_Rate_A__R_Field_3;
    private DbsField pnd_Temp_Rate_A_Pnd_Temp_Rate_N;
    private DbsField pnd_Temp_Date_A;

    private DbsGroup pnd_Temp_Date_A__R_Field_4;
    private DbsField pnd_Temp_Date_A_Pnd_Temp_Date_N;
    private DbsField pnd_Found_Flag;
    private DbsField pnd_Temp_Payee_N;

    private DbsGroup pnd_Temp_Payee_N__R_Field_5;
    private DbsField pnd_Temp_Payee_N_Pnd_Temp_Payee_A;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0391 = new LdaAial0391();
        registerRecord(ldaAial0391);
        registerRecord(ldaAial0391.getVw_iaa_Tiaa_Fund_Rcrd_View());
        ldaAial0393 = new LdaAial0393();
        registerRecord(ldaAial0393);
        registerRecord(ldaAial0393.getVw_iaa_Cref_Fund_Rcrd_1_View());

        // parameters
        parameters = new DbsRecord();
        pdaAial0421 = new PdaAial0421(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaview = new DataAccessProgramView(new NameInfo("vw_iaview", "IAVIEW"), "IAA_MASTER_FILE", "IA_CONTRACT_PART");
        iaview_Cntrct_Ppcn_Nbr = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        iaview_Cntrct_Optn_Cde = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        iaview_Cntrct_Orgn_Cde = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        iaview_Cntrct_Issue_Dte = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_ISSUE_DTE");
        iaview_Cntrct_Mode_Ind = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        iaview_Cntrct_First_Annt_Dob_Dte = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaview_Cntrct_First_Annt_Sex_Cde = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaview_Cntrct_First_Annt_Dod_Dte = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaview_Cntrct_Scnd_Annt_Dob_Dte = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaview_Cntrct_Scnd_Annt_Sex_Cde = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaview_Cntrct_Scnd_Annt_Dod_Dte = vw_iaview.getRecord().newFieldInGroup("iaview_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        registerRecord(vw_iaview);

        vw_iapart = new DataAccessProgramView(new NameInfo("vw_iapart", "IAPART"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        iapart_Cntrct_Mode_Ind = vw_iapart.getRecord().newFieldInGroup("iapart_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        iapart_Cntrct_Final_Per_Pay_Dte = vw_iapart.getRecord().newFieldInGroup("iapart_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_iapart);

        pnd_Record_Key = localVariables.newFieldInRecord("pnd_Record_Key", "#RECORD-KEY", FieldType.STRING, 15);

        pnd_Record_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Record_Key__R_Field_1", "REDEFINE", pnd_Record_Key);
        pnd_Record_Key_Pnd_Key_Cntrct_Ppcn_Nbr = pnd_Record_Key__R_Field_1.newFieldInGroup("pnd_Record_Key_Pnd_Key_Cntrct_Ppcn_Nbr", "#KEY-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Record_Key_Pnd_Key_Cntrct_Payee_Cde = pnd_Record_Key__R_Field_1.newFieldInGroup("pnd_Record_Key_Pnd_Key_Cntrct_Payee_Cde", "#KEY-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_2 = pnd_Record_Key__R_Field_1.newFieldInGroup("pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_2", "#KEY-CMPNY-FUND-CDE-2", 
            FieldType.STRING, 2);
        pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_Meth = pnd_Record_Key__R_Field_1.newFieldInGroup("pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_Meth", "#KEY-CMPNY-FUND-CDE-METH", 
            FieldType.STRING, 1);
        pnd_Record_Key_1 = localVariables.newFieldInRecord("pnd_Record_Key_1", "#RECORD-KEY-1", FieldType.STRING, 12);

        pnd_Record_Key_1__R_Field_2 = localVariables.newGroupInRecord("pnd_Record_Key_1__R_Field_2", "REDEFINE", pnd_Record_Key_1);
        pnd_Record_Key_1_Pnd_Key1_Cntrct_Ppcn_Nbr = pnd_Record_Key_1__R_Field_2.newFieldInGroup("pnd_Record_Key_1_Pnd_Key1_Cntrct_Ppcn_Nbr", "#KEY1-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Record_Key_1_Pnd_Key1_Cntrct_Payee_Cde = pnd_Record_Key_1__R_Field_2.newFieldInGroup("pnd_Record_Key_1_Pnd_Key1_Cntrct_Payee_Cde", "#KEY1-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Temp_Rate_A = localVariables.newFieldInRecord("pnd_Temp_Rate_A", "#TEMP-RATE-A", FieldType.STRING, 2);

        pnd_Temp_Rate_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Temp_Rate_A__R_Field_3", "REDEFINE", pnd_Temp_Rate_A);
        pnd_Temp_Rate_A_Pnd_Temp_Rate_N = pnd_Temp_Rate_A__R_Field_3.newFieldInGroup("pnd_Temp_Rate_A_Pnd_Temp_Rate_N", "#TEMP-RATE-N", FieldType.NUMERIC, 
            2);
        pnd_Temp_Date_A = localVariables.newFieldInRecord("pnd_Temp_Date_A", "#TEMP-DATE-A", FieldType.STRING, 8);

        pnd_Temp_Date_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Temp_Date_A__R_Field_4", "REDEFINE", pnd_Temp_Date_A);
        pnd_Temp_Date_A_Pnd_Temp_Date_N = pnd_Temp_Date_A__R_Field_4.newFieldInGroup("pnd_Temp_Date_A_Pnd_Temp_Date_N", "#TEMP-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Found_Flag = localVariables.newFieldInRecord("pnd_Found_Flag", "#FOUND-FLAG", FieldType.STRING, 1);
        pnd_Temp_Payee_N = localVariables.newFieldInRecord("pnd_Temp_Payee_N", "#TEMP-PAYEE-N", FieldType.NUMERIC, 2);

        pnd_Temp_Payee_N__R_Field_5 = localVariables.newGroupInRecord("pnd_Temp_Payee_N__R_Field_5", "REDEFINE", pnd_Temp_Payee_N);
        pnd_Temp_Payee_N_Pnd_Temp_Payee_A = pnd_Temp_Payee_N__R_Field_5.newFieldInGroup("pnd_Temp_Payee_N_Pnd_Temp_Payee_A", "#TEMP-PAYEE-A", FieldType.STRING, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaview.reset();
        vw_iapart.reset();

        ldaAial0391.initializeValues();
        ldaAial0393.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Aian042s() throws Exception
    {
        super("Aian042s");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ******************************************************************
        pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Error().setValue(0);                                                                                               //Natural: MOVE 0 TO #AIAN042-ERROR
        pnd_Record_Key_Pnd_Key_Cntrct_Ppcn_Nbr.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr());                                                //Natural: MOVE #AIAN042-CNTRCT-PPCN-NBR TO #KEY-CNTRCT-PPCN-NBR
        pnd_Record_Key_1_Pnd_Key1_Cntrct_Ppcn_Nbr.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr());                                             //Natural: MOVE #AIAN042-CNTRCT-PPCN-NBR TO #KEY1-CNTRCT-PPCN-NBR
        pnd_Record_Key_Pnd_Key_Cntrct_Payee_Cde.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde());                                              //Natural: MOVE #AIAN042-CNTRCT-PAYEE-CDE TO #KEY-CNTRCT-PAYEE-CDE
        pnd_Record_Key_1_Pnd_Key1_Cntrct_Payee_Cde.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde());                                           //Natural: MOVE #AIAN042-CNTRCT-PAYEE-CDE TO #KEY1-CNTRCT-PAYEE-CDE
        pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_Meth.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cmpny_Fund_Cde_Meth());                                        //Natural: MOVE #AIAN042-CMPNY-FUND-CDE-METH TO #KEY-CMPNY-FUND-CDE-METH
        pnd_Record_Key_Pnd_Key_Cmpny_Fund_Cde_2.setValue("C1");                                                                                                           //Natural: MOVE 'C1' TO #KEY-CMPNY-FUND-CDE-2
        vw_iapart.startDatabaseFind                                                                                                                                       //Natural: FIND IAPART WITH CNTRCT-PAYEE-KEY = #RECORD-KEY-1
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Record_Key_1, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iapart.readNextRow("FIND01")))
        {
            vw_iapart.setIfNotFoundControlFlag(false);
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Mode().setValue(iapart_Cntrct_Mode_Ind);                                                                       //Natural: MOVE CNTRCT-MODE-IND TO #AIAN042-MODE
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date().setValue(iapart_Cntrct_Final_Per_Pay_Dte);                                                //Natural: MOVE CNTRCT-FINAL-PER-PAY-DTE TO #AIAN042-FINAL-PER-PAY-DATE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_iaview.startDatabaseFind                                                                                                                                       //Natural: FIND IAVIEW WITH CNTRCT-PPCN-NBR = #KEY-CNTRCT-PPCN-NBR
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Record_Key_Pnd_Key_Cntrct_Ppcn_Nbr, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_iaview.readNextRow("FIND02", true)))
        {
            vw_iaview.setIfNotFoundControlFlag(false);
            if (condition(vw_iaview.getAstCOUNTER().equals(0)))                                                                                                           //Natural: IF NO RECORDS FOUND
            {
                pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Error().setValue(1);                                                                                       //Natural: MOVE 1 TO #AIAN042-ERROR
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Option().setValue(iaview_Cntrct_Optn_Cde);                                                                     //Natural: MOVE CNTRCT-OPTN-CDE TO #AIAN042-OPTION
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Contract_Origin().setValue(iaview_Cntrct_Orgn_Cde);                                                            //Natural: MOVE CNTRCT-ORGN-CDE TO #AIAN042-CONTRACT-ORIGIN
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Issue_Date().setValue(iaview_Cntrct_Issue_Dte);                                                                //Natural: MOVE CNTRCT-ISSUE-DTE TO #AIAN042-ISSUE-DATE
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob().setValue(iaview_Cntrct_First_Annt_Dob_Dte);                                                    //Natural: MOVE CNTRCT-FIRST-ANNT-DOB-DTE TO #AIAN042-FIRST-ANN-DOB
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob().setValue(iaview_Cntrct_Scnd_Annt_Dob_Dte);                                                    //Natural: MOVE CNTRCT-SCND-ANNT-DOB-DTE TO #AIAN042-SECOND-ANN-DOB
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex().setValue(iaview_Cntrct_First_Annt_Sex_Cde);                                                    //Natural: MOVE CNTRCT-FIRST-ANNT-SEX-CDE TO #AIAN042-FIRST-ANN-SEX
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex().setValue(iaview_Cntrct_Scnd_Annt_Sex_Cde);                                                    //Natural: MOVE CNTRCT-SCND-ANNT-SEX-CDE TO #AIAN042-SECOND-ANN-SEX
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dod().setValue(iaview_Cntrct_First_Annt_Dod_Dte);                                                    //Natural: MOVE CNTRCT-FIRST-ANNT-DOD-DTE TO #AIAN042-FIRST-ANN-DOD
            pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dod().setValue(iaview_Cntrct_Scnd_Annt_Dod_Dte);                                                    //Natural: MOVE CNTRCT-SCND-ANNT-DOD-DTE TO #AIAN042-SECOND-ANN-DOD
            //*  IF NO RECORDS FOUND
            //*    MOVE 1 TO #AIAN042-ERROR ESCAPE BOTTOM
            //*  END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* * MOVE  'G' TO #KEY-CMPNY-FUND-CDE-METH
        //* * FIND IAA-TIAA-FUND-RCRD-VIEW WITH TIAA-CNTRCT-FUND-KEY = #RECORD-KEY
        //* * FOR #I = 1 TO 40
        //* *    MOVE TIAA-RATE-CDE (#I) TO #TEMP-RATE-A
        //* *    MOVE #TEMP-RATE-N TO #AIAN042-TG-RATE-CODE (#I)
        //* *    MOVE EDITED TIAA-RATE-DTE (#I) (EM=YYYYMMDD) TO #TEMP-DATE-A
        //* *    MOVE #TEMP-DATE-N TO #AIAN042-TG-RATE-TRANSFER-EFF-DT (#I)
        //* *    MOVE  TIAA-PER-PAY-AMT (#I) TO #AIAN042-TG-GUARANTEED-PAYMENT (#I)
        //* *    MOVE  TIAA-PER-DIV-AMT (#I) TO #AIAN042-TG-DIVIDEND-PAYMENT (#I)
        //* *    MOVE TIAA-RATE-FINAL-PAY-AMT (#I) TO
        //* *                                #AIAN042-TG-FINAL-GUARANTEED-PAY (#I)
        //* *    MOVE TIAA-RATE-FINAL-DIV-AMT (#I) TO
        //*                                  #AIAN042-TG-FINAL-DIVIDEND-PAY (#I)
        //*   END-FOR
        //*  END-FIND
        //*  MOVE  'S' TO #KEY-CMPNY-FUND-CDE-METH
        //*  FIND IAA-TIAA-FUND-RCRD-VIEW WITH TIAA-CNTRCT-FUND-KEY = #RECORD-KEY
        //*  FOR #I = 1 TO 40
        //*       MOVE TIAA-RATE-CDE (#I) TO #TEMP-RATE-A
        //*       MOVE #TEMP-RATE-N TO #AIAN042-TS-RATE-CODE (#I)
        //*      MOVE EDITED TIAA-RATE-DTE (#I) (EM=YYYYMMDD) TO #TEMP-DATE-A
        //*      MOVE #TEMP-DATE-N TO #AIAN042-TS-RATE-TRANSFER-EFF-DT (#I)
        //*      MOVE  TIAA-PER-PAY-AMT (#I) TO #AIAN042-TS-GUARANTEED-PAYMENT (#I)
        //*      MOVE  TIAA-PER-DIV-AMT (#I) TO #AIAN042-TS-DIVIDEND-PAYMENT (#I)
        //*      MOVE TIAA-RATE-FINAL-PAY-AMT (#I) TO
        //*                                  #AIAN042-TS-FINAL-GUARANTEED-PAY (#I)
        //*      MOVE TIAA-RATE-FINAL-DIV-AMT (#I) TO
        //*                                  #AIAN042-TS-FINAL-DIVIDEND-PAY (#I)
        //*  END-FOR
        //*  END-FIND
        //*  MOVE 'C1' TO #KEY-CMPNY-FUND-CDE-2
        //*  MOVE 'A' TO #KEY-CMPNY-FUND-CDE-METH
        //*  READ IAA-CREF-TIAA-FUND-02-VIEW
        //*  IF CREF-CNTRCT-PPCN-NBR> 'IF00000000'
        //* *AD IAA-CREF-FUND-RCRD-VIEW
        //* *AD IAA-CREF-FUND-RCRD-1-VIEW BY CREF-CNTRCT-FUND-KEY
        //* *TARTING FROM 'IF00000001   '
        //* * CREF-RATE-CDE (1) > '00'
        //* **WRITE (2) IAA-CREF-TIAA-FUND-02-VIEW
        //* *WRITE (2) CREF-CNTRCT-FUND-KEY
        //* *          CREF-RATE-CDE (*)
        //* *          CREF-RATE-DTE (*)
        //* *          CREF-UNITS-CNT (*)
        //* *IND IAA-CREF-FUND-RCRD-VIEW WITH CREF-CNTRCT-FUND-KEY = #RECORD-KEY
        //* *OR #I = 1 TO 5
        //* *    MOVE CREF-RATE-CDE (#I) TO #TEMP-RATE-A
        //* *    MOVE #TEMP-RATE-N TO #AIAN042-CA-RATE-CODE (#I)
        //* *    MOVE EDITED CREF-RATE-DTE (#I) (EM=YYYYMMDD) TO #TEMP-DATE-A
        //* *    MOVE #TEMP-DATE-N TO #AIAN042-CA-RATE-TRANSFER-EFF-DT (#I)
        //* *    MOVE CREF-PER-PAY-AMT (#I) TO #AIAN042-CA-GUARANTEED-PAYMENT (#I)
        //* *    MOVE CREF-PER-DIV-AMT (#I) TO #AIAN042-CA-DIVIDEND-PAYMENT (#I)
        //* *    MOVE CREF-RATE-FINAL-PAY-AMT (#I) TO
        //* *                                #AIAN042-CA-FINAL-GUARANTEED-PAY (#I)
        //* *    MOVE CREF-RATE-FINAL-DIV-AMT (#I) TO
        //* *                                #AIAN042-CA-FINAL-DIVIDEND-PAY (#I)
        //* *ND-FOR
        //*  END-IF
        //* *D-FIND
        //*  END-READ
    }

    //
}
