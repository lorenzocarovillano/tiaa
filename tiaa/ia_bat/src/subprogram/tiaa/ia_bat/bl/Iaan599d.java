/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:39 AM
**        * FROM NATURAL SUBPROGRAM : Iaan599d
************************************************************
**        * FILE NAME            : Iaan599d.java
**        * CLASS NAME           : Iaan599d
**        * INSTANCE NAME        : Iaan599d
************************************************************
************************************************************************
* PROGRAM  : IAAN599D
* SYSTEM   : IAD
* TITLE    : RETRIEVE BEGIN AND END DATE OVERRIDE
* CREATED  : NOV 3, 2017
* FUNCTION : THIS SUBPROGRAM IS USED TO RETRIEVE BEGIN AND END DATE
*            OVERRIDE FOR IA DEATH REPORT IAAP599D.
*
*
* HISTORY
* 11/03/2017 - JFT - ORIGINAL CODE
*
************************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan599d extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Begin_Date;
    private DbsField pnd_End_Date;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_1;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_Date_Range;

    private DbsGroup pnd_Date_Range__R_Field_2;
    private DbsField pnd_Date_Range_Pnd_B_Date;
    private DbsField pnd_Date_Range__Filler1;
    private DbsField pnd_Date_Range_Pnd_E_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Begin_Date = parameters.newFieldInRecord("pnd_Begin_Date", "#BEGIN-DATE", FieldType.STRING, 8);
        pnd_Begin_Date.setParameterOption(ParameterOption.ByReference);
        pnd_End_Date = parameters.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_End_Date.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_1", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_Date_Range = localVariables.newFieldInRecord("pnd_Date_Range", "#DATE-RANGE", FieldType.STRING, 20);

        pnd_Date_Range__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Range__R_Field_2", "REDEFINE", pnd_Date_Range);
        pnd_Date_Range_Pnd_B_Date = pnd_Date_Range__R_Field_2.newFieldInGroup("pnd_Date_Range_Pnd_B_Date", "#B-DATE", FieldType.STRING, 8);
        pnd_Date_Range__Filler1 = pnd_Date_Range__R_Field_2.newFieldInGroup("pnd_Date_Range__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Date_Range_Pnd_E_Date = pnd_Date_Range__R_Field_2.newFieldInGroup("pnd_Date_Range_Pnd_E_Date", "#E-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan599d() throws Exception
    {
        super("Iaan599d");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Begin_Date.reset();                                                                                                                                           //Natural: RESET #BEGIN-DATE #END-DATE
        pnd_End_Date.reset();
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ074");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ074'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("DTE");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'DTE'
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ01:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ01")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id)  //Natural: IF NAZ-TBL-RCRD-TYP-IND NE #NAZ-TBL-RCRD-TYP-IND OR NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
                || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.equals("DOVERRIDE"))))                                                                                     //Natural: ACCEPT IF NAZ-TBL-RCRD-LVL3-ID = 'DOVERRIDE'
            {
                continue;
            }
            pnd_Date_Range.setValue(naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.getValue(1));                                                                               //Natural: ASSIGN #DATE-RANGE := NAZ-TBL-SECNDRY-DSCRPTN-TXT ( 1 )
            if (condition(! (DbsUtil.maskMatches(pnd_Date_Range_Pnd_B_Date,"YYYYMMDD")) || ! (DbsUtil.maskMatches(pnd_Date_Range_Pnd_E_Date,"YYYYMMDD"))))                //Natural: IF #B-DATE NE MASK ( YYYYMMDD ) OR #E-DATE NE MASK ( YYYYMMDD )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Begin_Date.setValue(pnd_Date_Range_Pnd_B_Date);                                                                                                           //Natural: ASSIGN #BEGIN-DATE := #B-DATE
            pnd_End_Date.setValue(pnd_Date_Range_Pnd_E_Date);                                                                                                             //Natural: ASSIGN #END-DATE := #E-DATE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
