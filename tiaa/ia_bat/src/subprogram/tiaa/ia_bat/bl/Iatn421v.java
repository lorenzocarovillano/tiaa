/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:58 AM
**        * FROM NATURAL SUBPROGRAM : Iatn421v
************************************************************
**        * FILE NAME            : Iatn421v.java
**        * CLASS NAME           : Iatn421v
**        * INSTANCE NAME        : Iatn421v
************************************************************
************************************************************************
*
* CALLS "CALM" SYSTEM MODULE LAMN1400, WHICH WRITES RECORDS TO
* THE COMMON LEDGER TRANSACTION FILE (DB 003, FILE 150).
*
* MAINTENANCE:
*
* NAME          DATE       DESCRIPTION
* ----          ----       -----------
* O. SOTTO     12/23/08    NEW MODULE TO REPLACE IATN420V.
* J. TINIO     01/22/09    ADDED OMNI TRADE INTERFACE. THE RECORDS
*                          WILL BE STORED IN NAZ TABLE AND DELETED ONCE
*                          DATA ARE SENT TO OMNI TRADE.
* O. SOTTO     03/06/09    READ ADAT TO GET THE INVESTMENT ID FOR
*                          THE FUND REQUESTED. THIS IS PASSED INSTEAD
*                          OF TICKER.
* O. SOTTO     05/13/09    FIX THE PROBLEM WITH SUB-LOB. SC 051309.
* O. SOTTO     05/19/09    JOURNAL DATE SHOULD BE ASSIGNED THE CURRENT
*                          DATE ALWAYS AS AGREED UPON DURING THE MEETING
*                          WITH ROXANE AND OTHERS. SC 051909.
* O. SOTTO     05/20/09    FIXED DEPOSIT ACCOUNTING LOGIC FOR AC's.
*                          SC 052009.
* O. SOTTO     06/08/09    FILL HDR-STATE-CODE. SC 060809.
* J. TINIO     05/28/11    DELETE OMNI TRADE INTERFACE.
* O. SOTTO     03/06/14    CREF REA CHANGES MARKED REA2014.
* O. SOTTO     03/25/15    CALM RETIREMENT MARKED CALM2015.
* O. SOTTO     04/2017     PIN EXPANSION - SC 082017 FOR CHANGES.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn421v extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIata422 pdaIata422;
    private LdaIatlcalm ldaIatlcalm;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Unique_Id;
    private DbsField pnd_Xfr_Frm_Acct_Cde;
    private DbsField pnd_Xfr_Frm_Unit_Typ;
    private DbsField pnd_Xfr_To_Acct_Cde;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;

    private DataAccessProgramView vw_pnd_Ext_Fund;
    private DbsField pnd_Ext_Fund_Nec_Table_Cde;
    private DbsField pnd_Ext_Fund_Nec_Ticker_Symbol;
    private DbsField pnd_Ext_Fund_Nec_Act_Class_Cde;
    private DbsField pnd_Ext_Fund_Nec_Act_Invesment_Typ;
    private DbsField pnd_Ext_Fund_Nec_Investment_Grouping_Id;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_1;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol;
    private DbsField pnd_Pin_Cntrct_Payee_Key;

    private DbsGroup pnd_Pin_Cntrct_Payee_Key__R_Field_2;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Switch;
    private DbsField pnd_Debug;
    private DbsField pnd_End_Of_Month;
    private DbsField pnd_Grd_To_Std;
    private DbsField pnd_Fr_Tiaa;
    private DbsField pnd_To_Tiaa;
    private DbsField pnd_First_Call;
    private DbsField pnd_No_Fr_Contract_Pair;
    private DbsField pnd_No_To_Contract_Pair;
    private DbsField pnd_Annty_Certain;
    private DbsField pnd_Deposit_Acctg;
    private DbsField pnd_Leap_Year;
    private DbsField pnd_Tot_Trans_Amt;
    private DbsField pnd_Actl_Amt;
    private DbsField pnd_Dx;
    private DbsField pnd_Fund_Cnt;
    private DbsField pnd_Stts_Cde;
    private DbsField pnd_Res_Cde;
    private DbsField pnd_Payee_Code;
    private DbsField pnd_Settl_Ind;
    private DbsField pnd_Work_Date_X;
    private DbsField pnd_Work_Date;

    private DbsGroup pnd_Work_Date__R_Field_3;
    private DbsField pnd_Work_Date_Pnd_W_Yyyy;
    private DbsField pnd_Work_Date_Pnd_W_Mm;
    private DbsField pnd_Work_Date_Pnd_W_Dd;

    private DbsGroup pnd_Work_Date__R_Field_4;
    private DbsField pnd_Work_Date_Pnd_W_Yy;
    private DbsField pnd_Work_Date_Pnd_W_Mmdd;
    private DbsField pnd_Dte_Alpha;

    private DbsGroup pnd_Dte_Alpha__R_Field_5;
    private DbsField pnd_Dte_Alpha_Pnd_Dte;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_Day;
    private DbsField pnd_Fr_Ticker;
    private DbsField pnd_To_Ticker;
    private DbsField pnd_Ws_Ticker;
    private DbsField pnd_Contract_Cert;
    private DbsField pnd_Fr_Contract_Pair;
    private DbsField pnd_To_Contract_Pair;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Tckr;

    private DbsGroup pnd_Stable_Value_Funds;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_403b;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_401k;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc;
    private DbsField pnd_Stable_Value_Funds_Pnd_Rc;
    private DbsField pnd_Stable_Value_Funds_Pnd_Rafslash_Gra;

    private DbsGroup pnd_Stable_Value_Funds__R_Field_6;
    private DbsField pnd_Stable_Value_Funds_Pnd_Stable_Org_Cdes;
    private DbsField pnd_Grdd_Sub_Lob;
    private DbsField pnd_Stdd_Sub_Lob;
    private DbsField pnd_Tiaa_And_Access;
    private DbsField pnd_Tiaa_Fnd_Cdes;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_W_Date;

    private DbsGroup pnd_W_Date__R_Field_7;
    private DbsField pnd_W_Date_Pnd_W_Date_A;
    private DbsField pnd_Datd;
    private DbsField pnd_Naz_Tbl_Super1;

    private DbsGroup pnd_Naz_Tbl_Super1__R_Field_8;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField pnd_From_Tckr;
    private DbsField pnd_To_Acct;
    private DbsField pnd_To_Tckr;

    private DbsGroup pnd_T_Hdr1;
    private DbsField pnd_T_Hdr1_Pnd_Tprog;
    private DbsField pnd_T_Hdr1_Pnd_Tfill1;
    private DbsField pnd_T_Hdr1_Pnd_Thdr1;
    private DbsField pnd_T_Hdr1_Pnd_Tfill2;
    private DbsField pnd_T_Hdr1_Pnd_Thdr2;
    private DbsField pnd_T_Hdr1_Pnd_Thdr_Dte;

    private DbsGroup pnd_S_Hdr1;
    private DbsField pnd_S_Hdr1_Pnd_Sprog;
    private DbsField pnd_S_Hdr1_Pnd_Sfill1;
    private DbsField pnd_S_Hdr1_Pnd_Shdr1;
    private DbsField pnd_S_Hdr1_Pnd_Sfill2;
    private DbsField pnd_S_Hdr1_Pnd_Shdr2;
    private DbsField pnd_S_Hdr1_Pnd_Shdr_Dte;

    private DbsGroup pnd_T_Hdr2;
    private DbsField pnd_T_Hdr2_Pnd_Tfill;
    private DbsField pnd_T_Hdr2_Pnd_Thdr;
    private DbsField pnd_T_Hdr2_Pnd_Thdr_Jdate;
    private DbsField pnd_T_Hdr2_Pnd_Tfill2;
    private DbsField pnd_T_Hdr2_Pnd_Tpage_Lit;
    private DbsField pnd_T_Hdr2_Pnd_Thdr_Page;

    private DbsGroup pnd_S_Hdr2;
    private DbsField pnd_S_Hdr2_Pnd_Sfill;
    private DbsField pnd_S_Hdr2_Pnd_Shdr;
    private DbsField pnd_S_Hdr2_Pnd_Shdr_Jdate;
    private DbsField pnd_S_Hdr2_Pnd_Sfill2;
    private DbsField pnd_S_Hdr2_Pnd_Spage_Lit;
    private DbsField pnd_S_Hdr2_Pnd_Shdr_Page;

    private DbsGroup pnd_Hdr3;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld1;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld2;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld3;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld4;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld5;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld6;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld7;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld8;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld9;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld10;

    private DbsGroup pnd_Shdr3;
    private DbsField pnd_Shdr3_Pnd_Shdr3_Fld1;
    private DbsField pnd_Shdr3_Pnd_Shdr3_Fld2;
    private DbsField pnd_Shdr3_Pnd_Shdr3_Fld3;
    private DbsField pnd_Shdr3_Pnd_Shdr3_Fld4;
    private DbsField pnd_Shdr3_Pnd_Shdr3_Fld5;
    private DbsField pnd_S_Dashes;

    private DbsGroup pnd_S_Dashes__R_Field_9;
    private DbsField pnd_S_Dashes_Pnd_Sfill1;
    private DbsField pnd_S_Dashes_Pnd_Sfr;
    private DbsField pnd_S_Dashes_Pnd_Sfill2;
    private DbsField pnd_S_Dashes_Pnd_Sto;

    private DbsGroup pnd_Dashes;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld1;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld2;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld3;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld4;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld5;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld6;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld7;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld8;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld9;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld10;

    private DbsGroup pnd_Report_Detail;
    private DbsField pnd_Report_Detail_Pnd_From_Contract;
    private DbsField pnd_Report_Detail_Pnd_F1;
    private DbsField pnd_Report_Detail_Pnd_Fr_Tkr;
    private DbsField pnd_Report_Detail_Pnd_F2;
    private DbsField pnd_Report_Detail_Pnd_Ctls_Fr_Ticker;
    private DbsField pnd_Report_Detail_Pnd_F3;
    private DbsField pnd_Report_Detail_Pnd_To_Contract;
    private DbsField pnd_Report_Detail_Pnd_F4;
    private DbsField pnd_Report_Detail_Pnd_To_Tkr;
    private DbsField pnd_Report_Detail_Pnd_F5;
    private DbsField pnd_Report_Detail_Pnd_Ctls_To_Ticker;
    private DbsField pnd_Report_Detail_Pnd_F6;
    private DbsField pnd_Report_Detail_Pnd_Trans_Amt;
    private DbsField pnd_Report_Detail_Pnd_F7;
    private DbsField pnd_Report_Detail_Pnd_Ldgr_Ind;
    private DbsField pnd_Report_Detail_Pnd_F8;
    private DbsField pnd_Report_Detail_Pnd_Ind1;
    private DbsField pnd_Report_Detail_Pnd_F9;
    private DbsField pnd_Report_Detail_Pnd_Ind2;

    private DbsGroup pnd_S_Report_Detail;
    private DbsField pnd_S_Report_Detail_Pnd_Contract;
    private DbsField pnd_S_Report_Detail_Pnd_F1;
    private DbsField pnd_S_Report_Detail_Pnd_Tkr;
    private DbsField pnd_S_Report_Detail_Pnd_F2;
    private DbsField pnd_S_Report_Detail_Pnd_Ctls_Ticker;
    private DbsField pnd_S_Report_Detail_Pnd_F3;
    private DbsField pnd_S_Report_Detail_Pnd_Fr_Reval;
    private DbsField pnd_S_Report_Detail_Pnd_F4;
    private DbsField pnd_S_Report_Detail_Pnd_To_Reval;
    private DbsField pnd_S_Report_Detail_Pnd_F5;
    private DbsField pnd_S_Report_Detail_Pnd_Switch_Amt;
    private DbsField pls_Tot_Trans_Amt;
    private DbsField pls_Tot_Switch_Amt;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIatlcalm = new LdaIatlcalm();
        registerRecord(ldaIatlcalm);
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIata422 = new PdaIata422(parameters);
        pnd_Unique_Id = parameters.newFieldInRecord("pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Unique_Id.setParameterOption(ParameterOption.ByReference);
        pnd_Xfr_Frm_Acct_Cde = parameters.newFieldInRecord("pnd_Xfr_Frm_Acct_Cde", "#XFR-FRM-ACCT-CDE", FieldType.STRING, 1);
        pnd_Xfr_Frm_Acct_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Xfr_Frm_Unit_Typ = parameters.newFieldInRecord("pnd_Xfr_Frm_Unit_Typ", "#XFR-FRM-UNIT-TYP", FieldType.STRING, 1);
        pnd_Xfr_Frm_Unit_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_Xfr_To_Acct_Cde = parameters.newFieldArrayInRecord("pnd_Xfr_To_Acct_Cde", "#XFR-TO-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Xfr_To_Acct_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        registerRecord(vw_iaa_Cntrct);

        vw_pnd_Ext_Fund = new DataAccessProgramView(new NameInfo("vw_pnd_Ext_Fund", "#EXT-FUND"), "NEW_EXT_CNTRL_FND", "NEW_EXT_CNTRL");
        pnd_Ext_Fund_Nec_Table_Cde = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NEC_TABLE_CDE");
        pnd_Ext_Fund_Nec_Ticker_Symbol = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        pnd_Ext_Fund_Nec_Act_Class_Cde = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Act_Class_Cde", "NEC-ACT-CLASS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NEC_ACT_CLASS_CDE");
        pnd_Ext_Fund_Nec_Act_Invesment_Typ = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Act_Invesment_Typ", "NEC-ACT-INVESMENT-TYP", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ACT_INVESMENT_TYP");
        pnd_Ext_Fund_Nec_Investment_Grouping_Id = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Investment_Grouping_Id", "NEC-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "NEC_INVESTMENT_GROUPING_ID");
        registerRecord(vw_pnd_Ext_Fund);

        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_1", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde = pnd_Nec_Fnd_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol = pnd_Nec_Fnd_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol", "#NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10);
        pnd_Pin_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Payee_Key", "#PIN-CNTRCT-PAYEE-KEY", FieldType.STRING, 24);

        pnd_Pin_Cntrct_Payee_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Payee_Key__R_Field_2", "REDEFINE", pnd_Pin_Cntrct_Payee_Key);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr = pnd_Pin_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr", "#PIN-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Pin_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Pin_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Switch = localVariables.newFieldInRecord("pnd_Switch", "#SWITCH", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_End_Of_Month = localVariables.newFieldInRecord("pnd_End_Of_Month", "#END-OF-MONTH", FieldType.BOOLEAN, 1);
        pnd_Grd_To_Std = localVariables.newFieldInRecord("pnd_Grd_To_Std", "#GRD-TO-STD", FieldType.BOOLEAN, 1);
        pnd_Fr_Tiaa = localVariables.newFieldInRecord("pnd_Fr_Tiaa", "#FR-TIAA", FieldType.BOOLEAN, 1);
        pnd_To_Tiaa = localVariables.newFieldInRecord("pnd_To_Tiaa", "#TO-TIAA", FieldType.BOOLEAN, 1);
        pnd_First_Call = localVariables.newFieldInRecord("pnd_First_Call", "#FIRST-CALL", FieldType.BOOLEAN, 1);
        pnd_No_Fr_Contract_Pair = localVariables.newFieldInRecord("pnd_No_Fr_Contract_Pair", "#NO-FR-CONTRACT-PAIR", FieldType.BOOLEAN, 1);
        pnd_No_To_Contract_Pair = localVariables.newFieldInRecord("pnd_No_To_Contract_Pair", "#NO-TO-CONTRACT-PAIR", FieldType.BOOLEAN, 1);
        pnd_Annty_Certain = localVariables.newFieldInRecord("pnd_Annty_Certain", "#ANNTY-CERTAIN", FieldType.BOOLEAN, 1);
        pnd_Deposit_Acctg = localVariables.newFieldInRecord("pnd_Deposit_Acctg", "#DEPOSIT-ACCTG", FieldType.BOOLEAN, 1);
        pnd_Leap_Year = localVariables.newFieldInRecord("pnd_Leap_Year", "#LEAP-YEAR", FieldType.BOOLEAN, 1);
        pnd_Tot_Trans_Amt = localVariables.newFieldInRecord("pnd_Tot_Trans_Amt", "#TOT-TRANS-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Actl_Amt = localVariables.newFieldInRecord("pnd_Actl_Amt", "#ACTL-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Dx = localVariables.newFieldInRecord("pnd_Dx", "#DX", FieldType.INTEGER, 4);
        pnd_Fund_Cnt = localVariables.newFieldInRecord("pnd_Fund_Cnt", "#FUND-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Stts_Cde = localVariables.newFieldInRecord("pnd_Stts_Cde", "#STTS-CDE", FieldType.STRING, 3);
        pnd_Res_Cde = localVariables.newFieldInRecord("pnd_Res_Cde", "#RES-CDE", FieldType.STRING, 2);
        pnd_Payee_Code = localVariables.newFieldInRecord("pnd_Payee_Code", "#PAYEE-CODE", FieldType.NUMERIC, 2);
        pnd_Settl_Ind = localVariables.newFieldInRecord("pnd_Settl_Ind", "#SETTL-IND", FieldType.STRING, 1);
        pnd_Work_Date_X = localVariables.newFieldInRecord("pnd_Work_Date_X", "#WORK-DATE-X", FieldType.STRING, 8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 8);

        pnd_Work_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Date__R_Field_3", "REDEFINE", pnd_Work_Date);
        pnd_Work_Date_Pnd_W_Yyyy = pnd_Work_Date__R_Field_3.newFieldInGroup("pnd_Work_Date_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_Work_Date_Pnd_W_Mm = pnd_Work_Date__R_Field_3.newFieldInGroup("pnd_Work_Date_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_Work_Date_Pnd_W_Dd = pnd_Work_Date__R_Field_3.newFieldInGroup("pnd_Work_Date_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 2);

        pnd_Work_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Date__R_Field_4", "REDEFINE", pnd_Work_Date);
        pnd_Work_Date_Pnd_W_Yy = pnd_Work_Date__R_Field_4.newFieldInGroup("pnd_Work_Date_Pnd_W_Yy", "#W-YY", FieldType.NUMERIC, 4);
        pnd_Work_Date_Pnd_W_Mmdd = pnd_Work_Date__R_Field_4.newFieldInGroup("pnd_Work_Date_Pnd_W_Mmdd", "#W-MMDD", FieldType.NUMERIC, 4);
        pnd_Dte_Alpha = localVariables.newFieldInRecord("pnd_Dte_Alpha", "#DTE-ALPHA", FieldType.STRING, 8);

        pnd_Dte_Alpha__R_Field_5 = localVariables.newGroupInRecord("pnd_Dte_Alpha__R_Field_5", "REDEFINE", pnd_Dte_Alpha);
        pnd_Dte_Alpha_Pnd_Dte = pnd_Dte_Alpha__R_Field_5.newFieldInGroup("pnd_Dte_Alpha_Pnd_Dte", "#DTE", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.INTEGER, 4);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 9);
        pnd_Fr_Ticker = localVariables.newFieldInRecord("pnd_Fr_Ticker", "#FR-TICKER", FieldType.STRING, 10);
        pnd_To_Ticker = localVariables.newFieldInRecord("pnd_To_Ticker", "#TO-TICKER", FieldType.STRING, 10);
        pnd_Ws_Ticker = localVariables.newFieldInRecord("pnd_Ws_Ticker", "#WS-TICKER", FieldType.STRING, 10);
        pnd_Contract_Cert = localVariables.newFieldInRecord("pnd_Contract_Cert", "#CONTRACT-CERT", FieldType.STRING, 10);
        pnd_Fr_Contract_Pair = localVariables.newFieldInRecord("pnd_Fr_Contract_Pair", "#FR-CONTRACT-PAIR", FieldType.STRING, 10);
        pnd_To_Contract_Pair = localVariables.newFieldInRecord("pnd_To_Contract_Pair", "#TO-CONTRACT-PAIR", FieldType.STRING, 10);
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 1);
        pnd_Tckr = localVariables.newFieldInRecord("pnd_Tckr", "#TCKR", FieldType.STRING, 10);

        pnd_Stable_Value_Funds = localVariables.newGroupInRecord("pnd_Stable_Value_Funds", "#STABLE-VALUE-FUNDS");
        pnd_Stable_Value_Funds_Pnd_Roth_403b = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_403b", "#ROTH-403B", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc", "#ROTH-403B-RC", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Roth_401k = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_401k", "#ROTH-401K", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc", "#ROTH-401K-RC", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Rc = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Stable_Value_Funds_Pnd_Rafslash_Gra = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Rafslash_Gra", "#RA/GRA", FieldType.NUMERIC, 
            2);

        pnd_Stable_Value_Funds__R_Field_6 = localVariables.newGroupInRecord("pnd_Stable_Value_Funds__R_Field_6", "REDEFINE", pnd_Stable_Value_Funds);
        pnd_Stable_Value_Funds_Pnd_Stable_Org_Cdes = pnd_Stable_Value_Funds__R_Field_6.newFieldArrayInGroup("pnd_Stable_Value_Funds_Pnd_Stable_Org_Cdes", 
            "#STABLE-ORG-CDES", FieldType.NUMERIC, 2, new DbsArrayController(1, 6));
        pnd_Grdd_Sub_Lob = localVariables.newFieldInRecord("pnd_Grdd_Sub_Lob", "#GRDD-SUB-LOB", FieldType.STRING, 12);
        pnd_Stdd_Sub_Lob = localVariables.newFieldInRecord("pnd_Stdd_Sub_Lob", "#STDD-SUB-LOB", FieldType.STRING, 12);
        pnd_Tiaa_And_Access = localVariables.newFieldArrayInRecord("pnd_Tiaa_And_Access", "#TIAA-AND-ACCESS", FieldType.STRING, 1, new DbsArrayController(1, 
            3));
        pnd_Tiaa_Fnd_Cdes = localVariables.newFieldArrayInRecord("pnd_Tiaa_Fnd_Cdes", "#TIAA-FND-CDES", FieldType.STRING, 1, new DbsArrayController(1, 
            4));

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.NUMERIC, 8);

        pnd_W_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_W_Date__R_Field_7", "REDEFINE", pnd_W_Date);
        pnd_W_Date_Pnd_W_Date_A = pnd_W_Date__R_Field_7.newFieldInGroup("pnd_W_Date_Pnd_W_Date_A", "#W-DATE-A", FieldType.STRING, 8);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Naz_Tbl_Super1 = localVariables.newFieldInRecord("pnd_Naz_Tbl_Super1", "#NAZ-TBL-SUPER1", FieldType.STRING, 29);

        pnd_Naz_Tbl_Super1__R_Field_8 = localVariables.newGroupInRecord("pnd_Naz_Tbl_Super1__R_Field_8", "REDEFINE", pnd_Naz_Tbl_Super1);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id = pnd_Naz_Tbl_Super1__R_Field_8.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id", "#NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id = pnd_Naz_Tbl_Super1__R_Field_8.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id", "#NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id = pnd_Naz_Tbl_Super1__R_Field_8.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id", "#NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_From_Tckr = localVariables.newFieldInRecord("pnd_From_Tckr", "#FROM-TCKR", FieldType.STRING, 10);
        pnd_To_Acct = localVariables.newFieldArrayInRecord("pnd_To_Acct", "#TO-ACCT", FieldType.STRING, 1, new DbsArrayController(1, 50));
        pnd_To_Tckr = localVariables.newFieldArrayInRecord("pnd_To_Tckr", "#TO-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 50));

        pnd_T_Hdr1 = localVariables.newGroupInRecord("pnd_T_Hdr1", "#T-HDR1");
        pnd_T_Hdr1_Pnd_Tprog = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Tprog", "#TPROG", FieldType.STRING, 8);
        pnd_T_Hdr1_Pnd_Tfill1 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Tfill1", "#TFILL1", FieldType.STRING, 18);
        pnd_T_Hdr1_Pnd_Thdr1 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Thdr1", "#THDR1", FieldType.STRING, 34);
        pnd_T_Hdr1_Pnd_Tfill2 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Tfill2", "#TFILL2", FieldType.STRING, 10);
        pnd_T_Hdr1_Pnd_Thdr2 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Thdr2", "#THDR2", FieldType.STRING, 10);
        pnd_T_Hdr1_Pnd_Thdr_Dte = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Thdr_Dte", "#THDR-DTE", FieldType.STRING, 10);

        pnd_S_Hdr1 = localVariables.newGroupInRecord("pnd_S_Hdr1", "#S-HDR1");
        pnd_S_Hdr1_Pnd_Sprog = pnd_S_Hdr1.newFieldInGroup("pnd_S_Hdr1_Pnd_Sprog", "#SPROG", FieldType.STRING, 8);
        pnd_S_Hdr1_Pnd_Sfill1 = pnd_S_Hdr1.newFieldInGroup("pnd_S_Hdr1_Pnd_Sfill1", "#SFILL1", FieldType.STRING, 18);
        pnd_S_Hdr1_Pnd_Shdr1 = pnd_S_Hdr1.newFieldInGroup("pnd_S_Hdr1_Pnd_Shdr1", "#SHDR1", FieldType.STRING, 34);
        pnd_S_Hdr1_Pnd_Sfill2 = pnd_S_Hdr1.newFieldInGroup("pnd_S_Hdr1_Pnd_Sfill2", "#SFILL2", FieldType.STRING, 10);
        pnd_S_Hdr1_Pnd_Shdr2 = pnd_S_Hdr1.newFieldInGroup("pnd_S_Hdr1_Pnd_Shdr2", "#SHDR2", FieldType.STRING, 10);
        pnd_S_Hdr1_Pnd_Shdr_Dte = pnd_S_Hdr1.newFieldInGroup("pnd_S_Hdr1_Pnd_Shdr_Dte", "#SHDR-DTE", FieldType.STRING, 10);

        pnd_T_Hdr2 = localVariables.newGroupInRecord("pnd_T_Hdr2", "#T-HDR2");
        pnd_T_Hdr2_Pnd_Tfill = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Tfill", "#TFILL", FieldType.STRING, 29);
        pnd_T_Hdr2_Pnd_Thdr = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Thdr", "#THDR", FieldType.STRING, 14);
        pnd_T_Hdr2_Pnd_Thdr_Jdate = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Thdr_Jdate", "#THDR-JDATE", FieldType.STRING, 10);
        pnd_T_Hdr2_Pnd_Tfill2 = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Tfill2", "#TFILL2", FieldType.STRING, 17);
        pnd_T_Hdr2_Pnd_Tpage_Lit = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Tpage_Lit", "#TPAGE-LIT", FieldType.STRING, 6);
        pnd_T_Hdr2_Pnd_Thdr_Page = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Thdr_Page", "#THDR-PAGE", FieldType.NUMERIC, 7);

        pnd_S_Hdr2 = localVariables.newGroupInRecord("pnd_S_Hdr2", "#S-HDR2");
        pnd_S_Hdr2_Pnd_Sfill = pnd_S_Hdr2.newFieldInGroup("pnd_S_Hdr2_Pnd_Sfill", "#SFILL", FieldType.STRING, 29);
        pnd_S_Hdr2_Pnd_Shdr = pnd_S_Hdr2.newFieldInGroup("pnd_S_Hdr2_Pnd_Shdr", "#SHDR", FieldType.STRING, 14);
        pnd_S_Hdr2_Pnd_Shdr_Jdate = pnd_S_Hdr2.newFieldInGroup("pnd_S_Hdr2_Pnd_Shdr_Jdate", "#SHDR-JDATE", FieldType.STRING, 10);
        pnd_S_Hdr2_Pnd_Sfill2 = pnd_S_Hdr2.newFieldInGroup("pnd_S_Hdr2_Pnd_Sfill2", "#SFILL2", FieldType.STRING, 17);
        pnd_S_Hdr2_Pnd_Spage_Lit = pnd_S_Hdr2.newFieldInGroup("pnd_S_Hdr2_Pnd_Spage_Lit", "#SPAGE-LIT", FieldType.STRING, 6);
        pnd_S_Hdr2_Pnd_Shdr_Page = pnd_S_Hdr2.newFieldInGroup("pnd_S_Hdr2_Pnd_Shdr_Page", "#SHDR-PAGE", FieldType.NUMERIC, 7);

        pnd_Hdr3 = localVariables.newGroupInRecord("pnd_Hdr3", "#HDR3");
        pnd_Hdr3_Pnd_Hdr3_Fld1 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld1", "#HDR3-FLD1", FieldType.STRING, 12);
        pnd_Hdr3_Pnd_Hdr3_Fld2 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld2", "#HDR3-FLD2", FieldType.STRING, 12);
        pnd_Hdr3_Pnd_Hdr3_Fld3 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld3", "#HDR3-FLD3", FieldType.STRING, 12);
        pnd_Hdr3_Pnd_Hdr3_Fld4 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld4", "#HDR3-FLD4", FieldType.STRING, 12);
        pnd_Hdr3_Pnd_Hdr3_Fld5 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld5", "#HDR3-FLD5", FieldType.STRING, 12);
        pnd_Hdr3_Pnd_Hdr3_Fld6 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld6", "#HDR3-FLD6", FieldType.STRING, 13);
        pnd_Hdr3_Pnd_Hdr3_Fld7 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld7", "#HDR3-FLD7", FieldType.STRING, 14);
        pnd_Hdr3_Pnd_Hdr3_Fld8 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld8", "#HDR3-FLD8", FieldType.STRING, 11);
        pnd_Hdr3_Pnd_Hdr3_Fld9 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld9", "#HDR3-FLD9", FieldType.STRING, 6);
        pnd_Hdr3_Pnd_Hdr3_Fld10 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld10", "#HDR3-FLD10", FieldType.STRING, 5);

        pnd_Shdr3 = localVariables.newGroupInRecord("pnd_Shdr3", "#SHDR3");
        pnd_Shdr3_Pnd_Shdr3_Fld1 = pnd_Shdr3.newFieldInGroup("pnd_Shdr3_Pnd_Shdr3_Fld1", "#SHDR3-FLD1", FieldType.STRING, 12);
        pnd_Shdr3_Pnd_Shdr3_Fld2 = pnd_Shdr3.newFieldInGroup("pnd_Shdr3_Pnd_Shdr3_Fld2", "#SHDR3-FLD2", FieldType.STRING, 12);
        pnd_Shdr3_Pnd_Shdr3_Fld3 = pnd_Shdr3.newFieldInGroup("pnd_Shdr3_Pnd_Shdr3_Fld3", "#SHDR3-FLD3", FieldType.STRING, 12);
        pnd_Shdr3_Pnd_Shdr3_Fld4 = pnd_Shdr3.newFieldInGroup("pnd_Shdr3_Pnd_Shdr3_Fld4", "#SHDR3-FLD4", FieldType.STRING, 30);
        pnd_Shdr3_Pnd_Shdr3_Fld5 = pnd_Shdr3.newFieldInGroup("pnd_Shdr3_Pnd_Shdr3_Fld5", "#SHDR3-FLD5", FieldType.STRING, 12);
        pnd_S_Dashes = localVariables.newFieldInRecord("pnd_S_Dashes", "#S-DASHES", FieldType.STRING, 79);

        pnd_S_Dashes__R_Field_9 = localVariables.newGroupInRecord("pnd_S_Dashes__R_Field_9", "REDEFINE", pnd_S_Dashes);
        pnd_S_Dashes_Pnd_Sfill1 = pnd_S_Dashes__R_Field_9.newFieldInGroup("pnd_S_Dashes_Pnd_Sfill1", "#SFILL1", FieldType.STRING, 42);
        pnd_S_Dashes_Pnd_Sfr = pnd_S_Dashes__R_Field_9.newFieldInGroup("pnd_S_Dashes_Pnd_Sfr", "#SFR", FieldType.STRING, 4);
        pnd_S_Dashes_Pnd_Sfill2 = pnd_S_Dashes__R_Field_9.newFieldInGroup("pnd_S_Dashes_Pnd_Sfill2", "#SFILL2", FieldType.STRING, 8);
        pnd_S_Dashes_Pnd_Sto = pnd_S_Dashes__R_Field_9.newFieldInGroup("pnd_S_Dashes_Pnd_Sto", "#STO", FieldType.STRING, 2);

        pnd_Dashes = localVariables.newGroupInRecord("pnd_Dashes", "#DASHES");
        pnd_Dashes_Pnd_Dsh_Fld1 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld1", "#DSH-FLD1", FieldType.STRING, 12);
        pnd_Dashes_Pnd_Dsh_Fld2 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld2", "#DSH-FLD2", FieldType.STRING, 12);
        pnd_Dashes_Pnd_Dsh_Fld3 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld3", "#DSH-FLD3", FieldType.STRING, 12);
        pnd_Dashes_Pnd_Dsh_Fld4 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld4", "#DSH-FLD4", FieldType.STRING, 12);
        pnd_Dashes_Pnd_Dsh_Fld5 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld5", "#DSH-FLD5", FieldType.STRING, 12);
        pnd_Dashes_Pnd_Dsh_Fld6 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld6", "#DSH-FLD6", FieldType.STRING, 13);
        pnd_Dashes_Pnd_Dsh_Fld7 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld7", "#DSH-FLD7", FieldType.STRING, 14);
        pnd_Dashes_Pnd_Dsh_Fld8 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld8", "#DSH-FLD8", FieldType.STRING, 11);
        pnd_Dashes_Pnd_Dsh_Fld9 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld9", "#DSH-FLD9", FieldType.STRING, 6);
        pnd_Dashes_Pnd_Dsh_Fld10 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld10", "#DSH-FLD10", FieldType.STRING, 4);

        pnd_Report_Detail = localVariables.newGroupInRecord("pnd_Report_Detail", "#REPORT-DETAIL");
        pnd_Report_Detail_Pnd_From_Contract = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_From_Contract", "#FROM-CONTRACT", FieldType.STRING, 
            10);
        pnd_Report_Detail_Pnd_F1 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_Fr_Tkr = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Fr_Tkr", "#FR-TKR", FieldType.STRING, 10);
        pnd_Report_Detail_Pnd_F2 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_Ctls_Fr_Ticker = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ctls_Fr_Ticker", "#CTLS-FR-TICKER", FieldType.STRING, 
            10);
        pnd_Report_Detail_Pnd_F3 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_To_Contract = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_To_Contract", "#TO-CONTRACT", FieldType.STRING, 10);
        pnd_Report_Detail_Pnd_F4 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_To_Tkr = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_To_Tkr", "#TO-TKR", FieldType.STRING, 10);
        pnd_Report_Detail_Pnd_F5 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_Ctls_To_Ticker = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ctls_To_Ticker", "#CTLS-TO-TICKER", FieldType.STRING, 
            10);
        pnd_Report_Detail_Pnd_F6 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F6", "#F6", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_Trans_Amt = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Trans_Amt", "#TRANS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Report_Detail_Pnd_F7 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F7", "#F7", FieldType.STRING, 4);
        pnd_Report_Detail_Pnd_Ldgr_Ind = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ldgr_Ind", "#LDGR-IND", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_F8 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F8", "#F8", FieldType.STRING, 7);
        pnd_Report_Detail_Pnd_Ind1 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ind1", "#IND1", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_F9 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F9", "#F9", FieldType.STRING, 4);
        pnd_Report_Detail_Pnd_Ind2 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ind2", "#IND2", FieldType.STRING, 1);

        pnd_S_Report_Detail = localVariables.newGroupInRecord("pnd_S_Report_Detail", "#S-REPORT-DETAIL");
        pnd_S_Report_Detail_Pnd_Contract = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_S_Report_Detail_Pnd_F1 = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_S_Report_Detail_Pnd_Tkr = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_Tkr", "#TKR", FieldType.STRING, 10);
        pnd_S_Report_Detail_Pnd_F2 = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_S_Report_Detail_Pnd_Ctls_Ticker = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_Ctls_Ticker", "#CTLS-TICKER", FieldType.STRING, 
            10);
        pnd_S_Report_Detail_Pnd_F3 = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_F3", "#F3", FieldType.STRING, 4);
        pnd_S_Report_Detail_Pnd_Fr_Reval = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_Fr_Reval", "#FR-REVAL", FieldType.STRING, 7);
        pnd_S_Report_Detail_Pnd_F4 = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_F4", "#F4", FieldType.STRING, 3);
        pnd_S_Report_Detail_Pnd_To_Reval = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_To_Reval", "#TO-REVAL", FieldType.STRING, 7);
        pnd_S_Report_Detail_Pnd_F5 = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_F5", "#F5", FieldType.STRING, 3);
        pnd_S_Report_Detail_Pnd_Switch_Amt = pnd_S_Report_Detail.newFieldInGroup("pnd_S_Report_Detail_Pnd_Switch_Amt", "#SWITCH-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pls_Tot_Trans_Amt = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Trans_Amt", "+TOT-TRANS-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pls_Tot_Switch_Amt = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Switch_Amt", "+TOT-SWITCH-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct.reset();
        vw_pnd_Ext_Fund.reset();
        vw_naz_Table_Ddm.reset();

        ldaIatlcalm.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_First_Call.setInitialValue(true);
        pnd_Dx.setInitialValue(1);
        pnd_Fund_Cnt.setInitialValue(20);
        pnd_Stable_Value_Funds_Pnd_Roth_403b.setInitialValue(24);
        pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc.setInitialValue(25);
        pnd_Stable_Value_Funds_Pnd_Roth_401k.setInitialValue(26);
        pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc.setInitialValue(27);
        pnd_Stable_Value_Funds_Pnd_Rc.setInitialValue(28);
        pnd_Stable_Value_Funds_Pnd_Rafslash_Gra.setInitialValue(29);
        pnd_Grdd_Sub_Lob.setInitialValue("GBPM-IA");
        pnd_Stdd_Sub_Lob.setInitialValue("IA");
        pnd_Tiaa_And_Access.getValue(1).setInitialValue("T");
        pnd_Tiaa_And_Access.getValue(2).setInitialValue("G");
        pnd_Tiaa_And_Access.getValue(3).setInitialValue("D");
        pnd_Tiaa_Fnd_Cdes.getValue(1).setInitialValue("T");
        pnd_Tiaa_Fnd_Cdes.getValue(2).setInitialValue("G");
        pnd_Tiaa_Fnd_Cdes.getValue(3).setInitialValue("R");
        pnd_Tiaa_Fnd_Cdes.getValue(4).setInitialValue("D");
        pnd_T_Hdr1_Pnd_Thdr1.setInitialValue("IA Transfer Interface to CALM/CTLS");
        pnd_T_Hdr1_Pnd_Thdr2.setInitialValue("Run Date: ");
        pnd_S_Hdr1_Pnd_Shdr1.setInitialValue("IA Switch Interface to CALM/CTLS  ");
        pnd_S_Hdr1_Pnd_Shdr2.setInitialValue("Run Date: ");
        pnd_T_Hdr2_Pnd_Thdr.setInitialValue("Journal Date: ");
        pnd_T_Hdr2_Pnd_Tpage_Lit.setInitialValue("Page: ");
        pnd_S_Hdr2_Pnd_Shdr.setInitialValue("Journal Date: ");
        pnd_S_Hdr2_Pnd_Spage_Lit.setInitialValue("Page: ");
        pnd_Hdr3_Pnd_Hdr3_Fld1.setInitialValue("From");
        pnd_Hdr3_Pnd_Hdr3_Fld2.setInitialValue("Ticker");
        pnd_Hdr3_Pnd_Hdr3_Fld3.setInitialValue("CTLS Tkr");
        pnd_Hdr3_Pnd_Hdr3_Fld4.setInitialValue("To");
        pnd_Hdr3_Pnd_Hdr3_Fld5.setInitialValue("Ticker");
        pnd_Hdr3_Pnd_Hdr3_Fld6.setInitialValue("CTLS Tkr");
        pnd_Hdr3_Pnd_Hdr3_Fld7.setInitialValue("Trans Amt");
        pnd_Hdr3_Pnd_Hdr3_Fld8.setInitialValue("Ledgered");
        pnd_Hdr3_Pnd_Hdr3_Fld9.setInitialValue("Ind1");
        pnd_Hdr3_Pnd_Hdr3_Fld10.setInitialValue("Ind2");
        pnd_Shdr3_Pnd_Shdr3_Fld1.setInitialValue("Contract");
        pnd_Shdr3_Pnd_Shdr3_Fld2.setInitialValue("Ticker");
        pnd_Shdr3_Pnd_Shdr3_Fld3.setInitialValue("CTLS Tkr");
        pnd_Shdr3_Pnd_Shdr3_Fld4.setInitialValue("----Revaluation----");
        pnd_Shdr3_Pnd_Shdr3_Fld5.setInitialValue("Trans Amt");
        pnd_Dashes_Pnd_Dsh_Fld1.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld2.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld3.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld4.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld5.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld6.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld7.setInitialValue("------------");
        pnd_Dashes_Pnd_Dsh_Fld8.setInitialValue("--------  ");
        pnd_Dashes_Pnd_Dsh_Fld9.setInitialValue("---- ");
        pnd_Dashes_Pnd_Dsh_Fld10.setInitialValue("----");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn421v() throws Exception
    {
        super("Iatn421v");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        setupReports();
        pnd_T_Hdr1_Pnd_Thdr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                        //Natural: FORMAT ( 4 ) LS = 133 PS = 60;//Natural: FORMAT ( 5 ) LS = 133 PS = 60;//Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #THDR-DTE
        pnd_S_Hdr1_Pnd_Shdr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                        //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #SHDR-DTE
        pnd_S_Dashes.moveAll("-");                                                                                                                                        //Natural: MOVE ALL '-' TO #S-DASHES
        pnd_S_Dashes_Pnd_Sfr.setValue("From");                                                                                                                            //Natural: ASSIGN #SFR := 'From'
        pnd_S_Dashes_Pnd_Sto.setValue("To");                                                                                                                              //Natural: ASSIGN #STO := 'To'
        pnd_Debug.reset();                                                                                                                                                //Natural: AT TOP OF PAGE ( 4 );//Natural: AT TOP OF PAGE ( 5 );//Natural: RESET #DEBUG
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = 'NAZ021UTLUTL3'
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", "NAZ021UTLUTL3", WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND01")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                            //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y'
            {
                continue;
            }
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.equals("TEST")))                                                                                         //Natural: IF NAZ-TBL-RCRD-DSCRPTN-TXT = 'TEST'
            {
                pnd_Debug.setValue(true);                                                                                                                                 //Natural: ASSIGN #DEBUG := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Debug.setValue(false);                                                                                                                                //Natural: ASSIGN #DEBUG := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIatlcalm.getLama1001_Group_Pda().reset();                                                                                                                      //Natural: RESET LAMA1001.GROUP-PDA #IATA422-RETURN-CD
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().reset();
        pnd_Fund_Cde.setValue(pnd_Xfr_Frm_Acct_Cde);                                                                                                                      //Natural: ASSIGN #FUND-CDE := #XFR-FRM-ACCT-CDE
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYMBOL
        sub_Get_Ticker_Symbol();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Tckr.notEquals(" ")))                                                                                                                           //Natural: IF #TCKR NE ' '
        {
            pnd_From_Tckr.setValue(pnd_Tckr);                                                                                                                             //Natural: ASSIGN #FROM-TCKR := #TCKR
            pnd_Fr_Ticker.setValue(pnd_Tckr);                                                                                                                             //Natural: ASSIGN #FR-TICKER := #TCKR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("E1");                                                                                             //Natural: ASSIGN #IATA422-RETURN-CD := 'E1'
            getReports().write(0, Global.getPROGRAM());                                                                                                                   //Natural: WRITE *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "INVALID FROM ACCT CODE",pnd_Xfr_Frm_Acct_Cde);                                                                                         //Natural: WRITE 'INVALID FROM ACCT CODE' #XFR-FRM-ACCT-CDE
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  060809
        pnd_Annty_Certain.reset();                                                                                                                                        //Natural: RESET #ANNTY-CERTAIN #RES-CDE
        pnd_Res_Cde.reset();
        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND IAA-CNTRCT WITH IAA-CNTRCT.CNTRCT-PPCN-NBR = #IATA422-FROM-PPCN-NBR
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr(), WcType.WITH) }
        );
        FIND02:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND02")))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            //*  WRITE '=' #IATA422-FROM-PPCN-NBR '=' IAA-CNTRCT.CNTRCT-OPTN-CDE
            if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(21)))                                                                                                         //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
            {
                pnd_Annty_Certain.setValue(true);                                                                                                                         //Natural: ASSIGN #ANNTY-CERTAIN := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* *IF #ANNTY-CERTAIN
        //* *  IF #XFR-FRM-ACCT-CDE = #TIAA-AND-ACCESS (*)
        //* *    #DEPOSIT-ACCTG := TRUE
        //* *  ELSE
        //* *    #DEPOSIT-ACCTG := FALSE
        //* *  END-IF
        //* *END-IF
        pnd_Contract_Cert.setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr());                                                                                //Natural: ASSIGN #CONTRACT-CERT := #IATA422-FROM-PPCN-NBR
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT-OR-CERT
        sub_Get_Contract_Or_Cert();
        if (condition(Global.isEscape())) {return;}
        pnd_First_Call.setValue(false);                                                                                                                                   //Natural: ASSIGN #FIRST-CALL := FALSE
        pnd_Contract_Cert.setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr());                                                                                  //Natural: ASSIGN #CONTRACT-CERT := #IATA422-TO-PPCN-NBR
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT-OR-CERT
        sub_Get_Contract_Or_Cert();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Fr_Contract_Pair.equals(" ")))                                                                                                                  //Natural: IF #FR-CONTRACT-PAIR = ' '
        {
            pnd_No_Fr_Contract_Pair.setValue(true);                                                                                                                       //Natural: ASSIGN #NO-FR-CONTRACT-PAIR := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_No_Fr_Contract_Pair.setValue(false);                                                                                                                      //Natural: ASSIGN #NO-FR-CONTRACT-PAIR := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_To_Contract_Pair.equals(" ")))                                                                                                                  //Natural: IF #TO-CONTRACT-PAIR = ' '
        {
            pnd_No_To_Contract_Pair.setValue(true);                                                                                                                       //Natural: ASSIGN #NO-TO-CONTRACT-PAIR := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_No_To_Contract_Pair.setValue(false);                                                                                                                      //Natural: ASSIGN #NO-TO-CONTRACT-PAIR := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //* *WRITE 'FROM CONTRACT SET' #IATA422-FROM-PPCN-NBR #FR-CONTRACT-PAIR
        //* *WRITE 'TO CONTRACT SET' #IATA422-TO-PPCN-NBR #TO-CONTRACT-PAIR
        //* *IF #NO-FR-CONTRACT-PAIR                             /* 051309
        if (condition(pnd_Xfr_Frm_Acct_Cde.equals(pnd_Tiaa_Fnd_Cdes.getValue("*"))))                                                                                      //Natural: IF #XFR-FRM-ACCT-CDE = #TIAA-FND-CDES ( * )
        {
            pnd_Fr_Tiaa.setValue(true);                                                                                                                                   //Natural: ASSIGN #FR-TIAA := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fr_Tiaa.setValue(false);                                                                                                                                  //Natural: ASSIGN #FR-TIAA := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //* *END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO #FUND-CNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Fund_Cnt)); pnd_X.nadd(1))
        {
            if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_X).equals(" ")))                                                                                               //Natural: IF #XFR-TO-ACCT-CDE ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF #NO-TO-CONTRACT-PAIR                           /* 051309
                if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_X).equals(pnd_Tiaa_Fnd_Cdes.getValue("*"))))                                                               //Natural: IF #XFR-TO-ACCT-CDE ( #X ) = #TIAA-FND-CDES ( * )
                {
                    pnd_To_Tiaa.setValue(true);                                                                                                                           //Natural: ASSIGN #TO-TIAA := TRUE
                    //*  051309
                    //*  051309
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_To_Tiaa.setValue(false);                                                                                                                          //Natural: ASSIGN #TO-TIAA := FALSE
                }                                                                                                                                                         //Natural: END-IF
                //*    END-IF                                            /* 051309
                //*    IF #ANNTY-CERTAIN AND NOT #DEPOSIT-ACCTG          /* 052009
                //*  052009
                if (condition(pnd_Annty_Certain.getBoolean()))                                                                                                            //Natural: IF #ANNTY-CERTAIN
                {
                    if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_X).equals(pnd_Tiaa_And_Access.getValue("*"))))                                                         //Natural: IF #XFR-TO-ACCT-CDE ( #X ) = #TIAA-AND-ACCESS ( * )
                    {
                        pnd_Deposit_Acctg.setValue(true);                                                                                                                 //Natural: ASSIGN #DEPOSIT-ACCTG := TRUE
                        //*  052009
                        //*  052009
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Deposit_Acctg.setValue(false);                                                                                                                //Natural: ASSIGN #DEPOSIT-ACCTG := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Fund_Cde.setValue(pnd_Xfr_To_Acct_Cde.getValue(pnd_X));                                                                                               //Natural: ASSIGN #FUND-CDE := #XFR-TO-ACCT-CDE ( #X )
                pnd_Tckr.reset();                                                                                                                                         //Natural: RESET #TCKR #SETTL-IND
                pnd_Settl_Ind.reset();
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYMBOL
                sub_Get_Ticker_Symbol();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Tckr.notEquals(" ")))                                                                                                                   //Natural: IF #TCKR NE ' '
                {
                    pnd_To_Ticker.setValue(pnd_Tckr);                                                                                                                     //Natural: ASSIGN #TO-TICKER := #TCKR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("E1");                                                                                     //Natural: ASSIGN #IATA422-RETURN-CD := 'E1'
                    getReports().write(0, Global.getPROGRAM());                                                                                                           //Natural: WRITE *PROGRAM
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "INVALID TO ACCT CODE",pnd_Xfr_To_Acct_Cde.getValue(pnd_X),pnd_X);                                                              //Natural: WRITE 'INVALID TO ACCT CODE' #XFR-TO-ACCT-CDE ( #X ) #X
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(((pdaIata422.getPnd_Iata422_Pnd_Iata422_Switch_Ind().notEquals(" ") && pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().equals(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(pnd_X)))  //Natural: IF #IATA422-SWITCH-IND NE ' ' AND #IATA422-LDGR-FROM-FUND-CDE = #IATA422-LDGR-TO-FUND-CDE ( #X ) AND NOT ( #IATA422-LDGR-FROM-FUND-CDE = '1G' OR = '1S' )
                    && ! ((pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().equals("1G") || pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().equals("1S"))))))
                {
                    pnd_Switch.setValue(true);                                                                                                                            //Natural: ASSIGN #SWITCH := TRUE
                    pnd_Settl_Ind.setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd());                                                                         //Natural: ASSIGN #SETTL-IND := #IATA422-LDGR-ACCT-CD
                    //*  CTLS EXPECTS THE CURRENT REVALUATION METHOD.
                    //*      IF #IATA422-LDGR-ACCT-CD = 'M'
                    //*        #SETTL-IND := 'A' /* MONTHLY TO ANNUAL SWITCH
                    //*      ELSE
                    //*        #SETTL-IND := 'M' /* ANNUAL TO MONTHLY SWITCH
                    //*      END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Switch.setValue(false);                                                                                                                           //Natural: ASSIGN #SWITCH := FALSE
                    if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().equals("1G") && pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(pnd_X).equals("1S"))) //Natural: IF #IATA422-LDGR-FROM-FUND-CDE = '1G' AND #IATA422-LDGR-TO-FUND-CDE ( #X ) = '1S'
                    {
                        pnd_Grd_To_Std.setValue(true);                                                                                                                    //Natural: ASSIGN #GRD-TO-STD := TRUE
                        pnd_Settl_Ind.setValue(" ");                                                                                                                      //Natural: ASSIGN #SETTL-IND := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        short decideConditionsMet1563 = 0;                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #IATA422-LDGR-TO-FUND-CDE ( #X ) = '1S' OR = '1G'
                        if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(pnd_X).equals("1S") || pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(pnd_X).equals("1G")))
                        {
                            decideConditionsMet1563++;
                            pnd_Settl_Ind.setValue(pnd_Xfr_Frm_Unit_Typ);                                                                                                 //Natural: ASSIGN #SETTL-IND := #XFR-FRM-UNIT-TYP
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            pnd_Settl_Ind.setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ().getValue(pnd_X));                                             //Natural: ASSIGN #SETTL-IND := #IATA422-LDGR-TO-UNIT-TYP ( #X )
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Actl_Amt.setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Amt().getValue(pnd_X));                                                                  //Natural: ASSIGN #ACTL-AMT := #IATA422-LDGR-AMT ( #X )
                                                                                                                                                                          //Natural: PERFORM MOVE-DETAIL-DATA
                sub_Move_Detail_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().notEquals(" ")))                                                                          //Natural: IF #IATA422-RETURN-CD NE ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().equals(" ")))                                                                                     //Natural: IF #IATA422-RETURN-CD EQ ' '
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-HDR-RCRD-FIELDS
            sub_Write_Hdr_Rcrd_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK FOR TIAA ACCESS
        //* *IF #XFR-FRM-ACCT-CDE = 'D'
        //*   #FROM-LOAD := TRUE
        //*   PERFORM FORMAT-OMNI-RECORD
        //* *END-IF
        //* *IF #XFR-TO-ACCT-CDE (*) = 'D'
        //*   RESET #FROM-LOAD
        //*   FOR #X 1 TO #FUND-CNT
        //*     IF #XFR-TO-ACCT-CDE (#X) = 'D'
        //*       PERFORM FORMAT-OMNI-RECORD
        //*     END-IF
        //*   END-FOR
        //* *END-IF
        //* *IF #OMNI-CNT GT 0
        //*   PERFORM WRITE-OMNI-RECORD
        //* *END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-EXT-MODULE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTRACT-OR-CERT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INVSTMNT-ID
        //* ***********************************************************************
        //*  REA2014 START
        //* *#NAZ-TBL-RCRD-LVL1-ID := 'NAZ040'
        //* *#NAZ-TBL-RCRD-LVL2-ID := 'INV'
        //* *#NAZ-TBL-RCRD-LVL3-ID := #WS-TICKER
        //* *FIND (1) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = #NAZ-TBL-SUPER1
        //* *  #WS-TICKER := NAZ-TBL-RCRD-DSCRPTN-TXT
        //* *END-FIND
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TICKER-SYMBOL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DETAIL-DATA
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HDR-RCRD-FIELDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HDR-DATA
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-STTS-CDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FIELDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-OMNI-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OMNI-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORTS
    }
    //*  GET PRODUCT/CONTRACT TYPE
    //*  BY ACCT
    private void sub_Call_Ext_Module() throws Exception                                                                                                                   //Natural: CALL-EXT-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Contract_Or_Cert() throws Exception                                                                                                              //Natural: GET-CONTRACT-OR-CERT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr.setValue(pnd_Unique_Id);                                                                                                     //Natural: ASSIGN #PIN-NBR := #UNIQUE-ID
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #PIN-CNTRCT-PAYEE-KEY
        (
        "READ01",
        new Wc[] { new Wc("PIN_CNTRCT_PAYEE_KEY", ">=", pnd_Pin_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ01")))
        {
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr.notEquals(pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr)))                                                            //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR NE #PIN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1664 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR EQ #CONTRACT-CERT
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.equals(pnd_Contract_Cert)))
            {
                decideConditionsMet1664++;
                //*  060809
                if (condition(pnd_Res_Cde.equals(" ")))                                                                                                                   //Natural: IF #RES-CDE = ' '
                {
                    //*  060809
                    pnd_Res_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT PRTCPNT-RSDNCY-CDE TO #RES-CDE
                    //*  060809
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_First_Call.getBoolean()))                                                                                                               //Natural: IF #FIRST-CALL
                {
                    //*  TIAA
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).notEquals(" ")))                                                                  //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) NE ' '
                    {
                        pnd_Fr_Tiaa.setValue(true);                                                                                                                       //Natural: ASSIGN #FR-TIAA := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CREF
                        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(2).notEquals(" ")))                                                              //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 2 ) NE ' '
                        {
                            pnd_Fr_Tiaa.setValue(false);                                                                                                                  //Natural: ASSIGN #FR-TIAA := FALSE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("E2");                                                                             //Natural: ASSIGN #IATA422-RETURN-CD := 'E2'
                            getReports().write(0, Global.getPROGRAM());                                                                                                   //Natural: WRITE *PROGRAM
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "NO COMPANY CODE FOR",iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                    //Natural: WRITE 'NO COMPANY CODE FOR' IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SECOND CALL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  TIAA
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).notEquals(" ")))                                                                  //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) NE ' '
                    {
                        pnd_To_Tiaa.setValue(true);                                                                                                                       //Natural: ASSIGN #TO-TIAA := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CREF
                        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(2).notEquals(" ")))                                                              //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 2 ) NE ' '
                        {
                            pnd_To_Tiaa.setValue(false);                                                                                                                  //Natural: ASSIGN #TO-TIAA := FALSE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("E3");                                                                             //Natural: ASSIGN #IATA422-RETURN-CD := 'E3'
                            getReports().write(0, Global.getPROGRAM());                                                                                                   //Natural: WRITE *PROGRAM
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "NO COMPANY CODE FOR",iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                    //Natural: WRITE 'NO COMPANY CODE FOR' IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR,2,6 ) EQ SUBSTR ( #CONTRACT-CERT,2,6 )
            else if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.getSubstring(2,6).equals(pnd_Contract_Cert.getSubstring(2,6))))
            {
                decideConditionsMet1664++;
                //*  060809
                if (condition(pnd_Res_Cde.equals(" ")))                                                                                                                   //Natural: IF #RES-CDE = ' '
                {
                    //*  060809
                    pnd_Res_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT PRTCPNT-RSDNCY-CDE TO #RES-CDE
                    //*  060809
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_First_Call.getBoolean()))                                                                                                               //Natural: IF #FIRST-CALL
                {
                    pnd_Fr_Contract_Pair.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                          //Natural: ASSIGN #FR-CONTRACT-PAIR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_To_Contract_Pair.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                          //Natural: ASSIGN #TO-CONTRACT-PAIR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Invstmnt_Id() throws Exception                                                                                                                   //Natural: GET-INVSTMNT-ID
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde.setValue("FND");                                                                                                             //Natural: ASSIGN #NEC-TABLE-CDE := 'FND'
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol.setValue(pnd_Ws_Ticker);                                                                                                 //Natural: ASSIGN #NEC-TICKER-SYMBOL := #WS-TICKER
        vw_pnd_Ext_Fund.startDatabaseFind                                                                                                                                 //Natural: FIND #EXT-FUND WITH NEC-FND-SUPER1 = #NEC-FND-SUPER1
        (
        "FIND03",
        new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_pnd_Ext_Fund.readNextRow("FIND03")))
        {
            vw_pnd_Ext_Fund.setIfNotFoundControlFlag(false);
            pnd_Ws_Ticker.setValue(pnd_Ext_Fund_Nec_Investment_Grouping_Id);                                                                                              //Natural: ASSIGN #WS-TICKER := NEC-INVESTMENT-GROUPING-ID
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ084");                                                                                                   //Natural: ASSIGN #NAZ-TBL-RCRD-LVL1-ID := 'NAZ084'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id.setValue("ADS");                                                                                                      //Natural: ASSIGN #NAZ-TBL-RCRD-LVL2-ID := 'ADS'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pnd_Ws_Ticker);                                                                                              //Natural: ASSIGN #NAZ-TBL-RCRD-LVL3-ID := #WS-TICKER
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = #NAZ-TBL-SUPER1
        (
        "FIND04",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Naz_Tbl_Super1, WcType.WITH) },
        1
        );
        FIND04:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND04")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            pnd_Ws_Ticker.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                               //Natural: ASSIGN #WS-TICKER := NAZ-TBL-RCRD-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  REA2014 END
    }
    private void sub_Get_Ticker_Symbol() throws Exception                                                                                                                 //Natural: GET-TICKER-SYMBOL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaan021x.class , getCurrentProcessState(), pnd_Fund_Cde, pnd_Tckr);                                                                               //Natural: CALLNAT 'IAAN021X' #FUND-CDE #TCKR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Move_Detail_Data() throws Exception                                                                                                                  //Natural: MOVE-DETAIL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  START WITH 1 ON SECOND CALL
        pnd_Tot_Trans_Amt.nadd(pnd_Actl_Amt);                                                                                                                             //Natural: ADD #ACTL-AMT TO #TOT-TRANS-AMT
        ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Dx).setValue(pnd_Actl_Amt);                                                                            //Natural: ASSIGN DTL-TRANSACTION-AMT ( #DX ) := #ACTL-AMT
        ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Dx).setValue("PS");                                                                                   //Natural: ASSIGN DTL-TRANSACTION-TYPE ( #DX ) := 'PS'
        ldaIatlcalm.getLama1001_Dtl_Type_Record().getValue(pnd_Dx).setValue(" ");                                                                                         //Natural: ASSIGN DTL-TYPE-RECORD ( #DX ) := ' '
        ldaIatlcalm.getLama1001_Dtl_Occurrence_Nbr().getValue(pnd_Dx).setValue(pnd_Dx);                                                                                   //Natural: ASSIGN DTL-OCCURRENCE-NBR ( #DX ) := #DX
        if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                          //Natural: IF #FR-TIAA
        {
            ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx).setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr());                            //Natural: ASSIGN DTL-FROM-TIAA-CONTRACT ( #DX ) := #IATA422-FROM-PPCN-NBR
            ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx).setValue(pnd_Fr_Contract_Pair);                                                             //Natural: ASSIGN DTL-FROM-CREF-CONTRACT ( #DX ) := #FR-CONTRACT-PAIR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx).setValue(pnd_Fr_Contract_Pair);                                                             //Natural: ASSIGN DTL-FROM-TIAA-CONTRACT ( #DX ) := #FR-CONTRACT-PAIR
            ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx).setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr());                            //Natural: ASSIGN DTL-FROM-CREF-CONTRACT ( #DX ) := #IATA422-FROM-PPCN-NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Fr_Ticker.notEquals("TIAA#")))                                                                                                                  //Natural: IF #FR-TICKER NE 'TIAA#'
        {
            pnd_Ws_Ticker.setValue(pnd_Fr_Ticker);                                                                                                                        //Natural: ASSIGN #WS-TICKER := #FR-TICKER
                                                                                                                                                                          //Natural: PERFORM GET-INVSTMNT-ID
            sub_Get_Invstmnt_Id();
            if (condition(Global.isEscape())) {return;}
            //*  REA2014
            if (condition(pnd_Ws_Ticker.notEquals(" ")))                                                                                                                  //Natural: IF #WS-TICKER NE ' '
            {
                pnd_Fr_Ticker.setValue(pnd_Ws_Ticker);                                                                                                                    //Natural: ASSIGN #FR-TICKER := #WS-TICKER
                //*  REA2014
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Dtl_From_Fund_Ticker_Symbol().getValue(pnd_Dx).setValue(pnd_Fr_Ticker);                                                                   //Natural: ASSIGN DTL-FROM-FUND-TICKER-SYMBOL ( #DX ) := #FR-TICKER
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        //* *IF DTL-FROM-TIAA-CONTRACT (#DX) = ' '               /* 051309 START
        //* *  NECA4000.PRD-KEY4-ACCT-NBR     := #IATA422-FROM-PPCN-NBR
        //* *ELSE
        //*  051309 END
        if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                          //Natural: IF #FR-TIAA
        {
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx));                                      //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := DTL-FROM-TIAA-CONTRACT ( #DX )
            //*  051309 START
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx));                                      //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := DTL-FROM-CREF-CONTRACT ( #DX )
            //*  051309 END
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-EXT-MODULE
        sub_Call_Ext_Module();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                             //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
        {
            ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                      //Natural: ASSIGN DTL-FROM-LOB ( #DX ) := PRD-PRODUCT-CDE ( 1 )
            ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                              //Natural: ASSIGN DTL-FROM-SUB-LOB ( #DX ) := PRD-SUB-PRODUCT-CDE ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("I1");                                                                                             //Natural: ASSIGN #IATA422-RETURN-CD := 'I1'
            getReports().write(0, Global.getPROGRAM());                                                                                                                   //Natural: WRITE *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "ERROR FROM EXTERNALIZATION FOR",pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr());                                                          //Natural: WRITE 'ERROR FROM EXTERNALIZATION FOR' NECA4000.PRD-KEY4-ACCT-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "RC=",pdaNeca4000.getNeca4000_Return_Cde(),pdaNeca4000.getNeca4000_Return_Msg());                                                       //Natural: WRITE 'RC=' NECA4000.RETURN-CDE NECA4000.RETURN-MSG
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1788 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #IATA422-LDGR-FROM-FUND-CDE = '1G'
        if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().equals("1G")))
        {
            decideConditionsMet1788++;
            ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Grdd_Sub_Lob);                                                                       //Natural: ASSIGN DTL-FROM-SUB-LOB ( #DX ) := #GRDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN #IATA422-LDGR-FROM-FUND-CDE = '1S'
        else if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().equals("1S")))
        {
            decideConditionsMet1788++;
            ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Stdd_Sub_Lob);                                                                       //Natural: ASSIGN DTL-FROM-SUB-LOB ( #DX ) := #STDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_To_Tiaa.getBoolean()))                                                                                                                          //Natural: IF #TO-TIAA
        {
            if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #FR-TIAA
            {
                ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx));              //Natural: ASSIGN DTL-TO-TIAA-CONTRACT ( #DX ) := DTL-FROM-TIAA-CONTRACT ( #DX )
                ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx));              //Natural: ASSIGN DTL-TO-CREF-CONTRACT ( #DX ) := DTL-FROM-CREF-CONTRACT ( #DX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr());                            //Natural: ASSIGN DTL-TO-TIAA-CONTRACT ( #DX ) := #IATA422-TO-PPCN-NBR
                ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx).setValue(pnd_To_Contract_Pair);                                                           //Natural: ASSIGN DTL-TO-CREF-CONTRACT ( #DX ) := #TO-CONTRACT-PAIR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  051309
            if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #FR-TIAA
            {
                ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).setValue(pnd_To_Contract_Pair);                                                           //Natural: ASSIGN DTL-TO-TIAA-CONTRACT ( #DX ) := #TO-CONTRACT-PAIR
                ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx).setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr());                            //Natural: ASSIGN DTL-TO-CREF-CONTRACT ( #DX ) := #IATA422-TO-PPCN-NBR
                //*  051309 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx));              //Natural: ASSIGN DTL-TO-TIAA-CONTRACT ( #DX ) := DTL-FROM-TIAA-CONTRACT ( #DX )
                ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx));              //Natural: ASSIGN DTL-TO-CREF-CONTRACT ( #DX ) := DTL-FROM-CREF-CONTRACT ( #DX )
                //*  051309 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_To_Tckr.getValue(pnd_Dx).setValue(pnd_To_Ticker);                                                                                                             //Natural: ASSIGN #TO-TCKR ( #DX ) := #TO-TICKER
        pnd_To_Acct.getValue(pnd_Dx).setValue(pnd_Fund_Cde);                                                                                                              //Natural: ASSIGN #TO-ACCT ( #DX ) := #FUND-CDE
        if (condition(pnd_To_Ticker.notEquals("TIAA#")))                                                                                                                  //Natural: IF #TO-TICKER NE 'TIAA#'
        {
            pnd_Ws_Ticker.setValue(pnd_To_Ticker);                                                                                                                        //Natural: ASSIGN #WS-TICKER := #TO-TICKER
                                                                                                                                                                          //Natural: PERFORM GET-INVSTMNT-ID
            sub_Get_Invstmnt_Id();
            if (condition(Global.isEscape())) {return;}
            //*  REA2014
            if (condition(pnd_Ws_Ticker.notEquals(" ")))                                                                                                                  //Natural: IF #WS-TICKER NE ' '
            {
                pnd_To_Ticker.setValue(pnd_Ws_Ticker);                                                                                                                    //Natural: ASSIGN #TO-TICKER := #WS-TICKER
                //*  REA2014
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Dx).setValue(pnd_To_Ticker);                                                                     //Natural: ASSIGN DTL-TO-FUND-TICKER-SYMBOL ( #DX ) := #TO-TICKER
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        if (condition(ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).equals(" ")))                                                                       //Natural: IF DTL-TO-TIAA-CONTRACT ( #DX ) = ' '
        {
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr());                                                    //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #IATA422-TO-PPCN-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx));                                        //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := DTL-TO-TIAA-CONTRACT ( #DX )
        }                                                                                                                                                                 //Natural: END-IF
        //*  HAPPENS FOR INTRA CONTRACT
        if (condition(pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().equals(" ")))                                                                                           //Natural: IF NECA4000.PRD-KEY4-ACCT-NBR = ' '
        {
            ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Dx));                                      //Natural: ASSIGN DTL-TO-LOB ( #DX ) := DTL-FROM-LOB ( #DX )
            ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx));                              //Natural: ASSIGN DTL-TO-SUB-LOB ( #DX ) := DTL-FROM-SUB-LOB ( #DX )
            ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx));                  //Natural: ASSIGN DTL-TO-TIAA-CONTRACT ( #DX ) := DTL-FROM-TIAA-CONTRACT ( #DX )
            ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx).setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx));                  //Natural: ASSIGN DTL-TO-CREF-CONTRACT ( #DX ) := DTL-FROM-CREF-CONTRACT ( #DX )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  051309 START
            if (condition(pnd_To_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #TO-TIAA
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx).notEquals(" ")))                                                            //Natural: IF DTL-TO-CREF-CONTRACT ( #DX ) NE ' '
                {
                    pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx));                                //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := DTL-TO-CREF-CONTRACT ( #DX )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx));                              //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := DTL-FROM-CREF-CONTRACT ( #DX )
                }                                                                                                                                                         //Natural: END-IF
                //*  051309 END
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-EXT-MODULE
            sub_Call_Ext_Module();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                         //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
            {
                ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                    //Natural: ASSIGN DTL-TO-LOB ( #DX ) := PRD-PRODUCT-CDE ( 1 )
                ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                            //Natural: ASSIGN DTL-TO-SUB-LOB ( #DX ) := PRD-SUB-PRODUCT-CDE ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("I1");                                                                                         //Natural: ASSIGN #IATA422-RETURN-CD := 'I1'
                getReports().write(0, Global.getPROGRAM());                                                                                                               //Natural: WRITE *PROGRAM
                if (Global.isEscape()) return;
                getReports().write(0, "ERROR FROM EXTERNALIZATION FOR",pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr());                                                      //Natural: WRITE 'ERROR FROM EXTERNALIZATION FOR' NECA4000.PRD-KEY4-ACCT-NBR
                if (Global.isEscape()) return;
                getReports().write(0, "RC=",pdaNeca4000.getNeca4000_Return_Cde(),pdaNeca4000.getNeca4000_Return_Msg());                                                   //Natural: WRITE 'RC=' NECA4000.RETURN-CDE NECA4000.RETURN-MSG
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1864 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #IATA422-LDGR-TO-FUND-CDE ( #X ) = '1G'
        if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(pnd_X).equals("1G")))
        {
            decideConditionsMet1864++;
            ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Grdd_Sub_Lob);                                                                         //Natural: ASSIGN DTL-TO-SUB-LOB ( #DX ) := #GRDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN #IATA422-LDGR-TO-FUND-CDE ( #X ) = '1S'
        else if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(pnd_X).equals("1S")))
        {
            decideConditionsMet1864++;
            ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Stdd_Sub_Lob);                                                                         //Natural: ASSIGN DTL-TO-SUB-LOB ( #DX ) := #STDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ACCORDING TO ROXANE, THIS INDICATOR MUST BE SET TO Y FOR GRD TO STD
        if (condition(pnd_Grd_To_Std.getBoolean()))                                                                                                                       //Natural: IF #GRD-TO-STD
        {
            ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Dx).setValue("Y");                                                                              //Natural: ASSIGN DTL-UNLEDGER-INDICATOR ( #DX ) := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Dx).setValue("N");                                                                              //Natural: ASSIGN DTL-UNLEDGER-INDICATOR ( #DX ) := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Dtl_Reversal_Code().getValue(pnd_Dx).setValue("N");                                                                                       //Natural: ASSIGN DTL-REVERSAL-CODE ( #DX ) := 'N'
        ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Dx).setValue(pnd_Settl_Ind);                                                                            //Natural: ASSIGN DTL-SETTLEMENT-IND ( #DX ) := #SETTL-IND
        if (condition(pnd_Switch.getBoolean()))                                                                                                                           //Natural: IF #SWITCH
        {
            ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx).setValue("S");                                                                                 //Natural: ASSIGN DTL-SETTLEMENT-IND2 ( #DX ) := 'S'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx).setValue(" ");                                                                                 //Natural: ASSIGN DTL-SETTLEMENT-IND2 ( #DX ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Deposit_Acctg.getBoolean() && ! (pnd_Switch.getBoolean())))                                                                                     //Natural: IF #DEPOSIT-ACCTG AND NOT #SWITCH
        {
            ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx).setValue("D");                                                                                 //Natural: ASSIGN DTL-SETTLEMENT-IND2 ( #DX ) := 'D'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dx.nadd(1);                                                                                                                                                   //Natural: ADD 1 TO #DX
        if (condition(pnd_Dx.greater(50)))                                                                                                                                //Natural: IF #DX > 50
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-HDR-RCRD-FIELDS
            sub_Write_Hdr_Rcrd_Fields();
            if (condition(Global.isEscape())) {return;}
            ldaIatlcalm.getLama1001_Group_Pda().reset();                                                                                                                  //Natural: RESET LAMA1001.GROUP-PDA #TO-TCKR ( * )
            pnd_To_Tckr.getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Hdr_Rcrd_Fields() throws Exception                                                                                                             //Natural: WRITE-HDR-RCRD-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
                                                                                                                                                                          //Natural: PERFORM WRITE-HDR-DATA
        sub_Write_Hdr_Data();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-FIELDS
            sub_Write_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *CALLNAT 'LAMN1400' LAMA1001          /* CALM2015 START
        //* *IF LAMA1001.RETURN-CDE NE ' '
        //* *  MOVE 'E6' TO #STTS-CDE
        //* *  PERFORM SET-STTS-CDS
        //* *ELSE
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORTS
        sub_Write_Reports();
        if (condition(Global.isEscape())) {return;}
        //* *END-IF                               /* CALM2015 END
    }
    private void sub_Write_Hdr_Data() throws Exception                                                                                                                    //Natural: WRITE-HDR-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Dx.greater(50)))                                                                                                                                //Natural: IF #DX > 50
        {
            pnd_Dx.setValue(1);                                                                                                                                           //Natural: ASSIGN #DX := 1
            ldaIatlcalm.getLama1001_More_Data().setValue("Y");                                                                                                            //Natural: ASSIGN MORE-DATA := 'Y'
            ldaIatlcalm.getLama1001_Detail_Rec_Cnt().setValue(50);                                                                                                        //Natural: ASSIGN DETAIL-REC-CNT := 50
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatlcalm.getLama1001_More_Data().setValue("N");                                                                                                            //Natural: ASSIGN MORE-DATA := 'N'
            ldaIatlcalm.getLama1001_Detail_Rec_Cnt().compute(new ComputeParameters(false, ldaIatlcalm.getLama1001_Detail_Rec_Cnt()), pnd_Dx.subtract(1));                 //Natural: ASSIGN DETAIL-REC-CNT := #DX - 1
            ldaIatlcalm.getLama1001_Hdr_Total_Transaction_Amt().setValue(pnd_Tot_Trans_Amt);                                                                              //Natural: ASSIGN HDR-TOTAL-TRANSACTION-AMT := #TOT-TRANS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Hdr_Origin_System().setValue("PST");                                                                                                      //Natural: ASSIGN HDR-ORIGIN-SYSTEM := 'PST'
        ldaIatlcalm.getLama1001_Hdr_Record_Type().setValue("H ");                                                                                                         //Natural: ASSIGN HDR-RECORD-TYPE := 'H '
        ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp().setValue(Global.getDATN());                                                                                     //Natural: ASSIGN HDR-CREATION-DATE-STAMP := *DATN
        ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp().setValue(Global.getTIMN());                                                                                     //Natural: ASSIGN HDR-CREATION-TIME-STAMP := *TIMN
        //* * THE #IATA422-LDGR-CYCLE-DTE IS TODAY'S DATE
        //*  051909
        pnd_Dte_Alpha.setValueEdited(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte(),new ReportEditMask("YYYYMMDD"));                                              //Natural: MOVE EDITED #IATA422-LDGR-CYCLE-DTE ( EM = YYYYMMDD ) TO #DTE-ALPHA
        ldaIatlcalm.getLama1001_Hdr_Journal_Date().setValue(pnd_Dte_Alpha_Pnd_Dte);                                                                                       //Natural: ASSIGN HDR-JOURNAL-DATE := #DTE
        //*  060809
        pnd_Dte_Alpha.setValueEdited(pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED #IATA422-LDGR-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DTE-ALPHA
        ldaIatlcalm.getLama1001_Hdr_Participation_Date().setValue(pnd_Dte_Alpha_Pnd_Dte);                                                                                 //Natural: ASSIGN HDR-PARTICIPATION-DATE := #DTE
        ldaIatlcalm.getLama1001_Hdr_Pin_Number().setValue(pnd_Unique_Id);                                                                                                 //Natural: ASSIGN HDR-PIN-NUMBER := #UNIQUE-ID
        ldaIatlcalm.getLama1001_Hdr_Record_Flow_Status().setValue("O");                                                                                                   //Natural: ASSIGN HDR-RECORD-FLOW-STATUS := 'O'
        ldaIatlcalm.getLama1001_Hdr_State_Code().setValue(pnd_Res_Cde);                                                                                                   //Natural: ASSIGN HDR-STATE-CODE := #RES-CDE
    }
    private void sub_Set_Stts_Cds() throws Exception                                                                                                                      //Natural: SET-STTS-CDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue(pnd_Stts_Cde);                                                                                         //Natural: MOVE #STTS-CDE TO #IATA422-RETURN-CD
        getReports().write(0, Global.getPROGRAM());                                                                                                                       //Natural: WRITE *PROGRAM
        if (Global.isEscape()) return;
        getReports().write(0, "Bad return from LAMN1400");                                                                                                                //Natural: WRITE 'Bad return from LAMN1400'
        if (Global.isEscape()) return;
        getReports().write(0, "RC=",ldaIatlcalm.getLama1001_Return_Cde(),ldaIatlcalm.getLama1001_Return_Msg());                                                           //Natural: WRITE 'RC=' LAMA1001.RETURN-CDE LAMA1001.RETURN-MSG
        if (Global.isEscape()) return;
    }
    private void sub_Write_Fields() throws Exception                                                                                                                      //Natural: WRITE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "HEADER RECORD FIELDS");                                                                                                                    //Natural: WRITE 'HEADER RECORD FIELDS'
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaIatlcalm.getLama1001_More_Data(),NEWLINE,"=",ldaIatlcalm.getLama1001_Detail_Rec_Cnt(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Total_Transaction_Amt(), //Natural: WRITE '=' MORE-DATA / '=' DETAIL-REC-CNT / '=' HDR-TOTAL-TRANSACTION-AMT / '=' HDR-ORIGIN-SYSTEM / '=' HDR-RECORD-TYPE / '=' HDR-CREATION-DATE-STAMP / '=' HDR-CREATION-TIME-STAMP / '=' HDR-JOURNAL-DATE / '=' HDR-PARTICIPATION-DATE / '=' HDR-PIN-NUMBER / '=' HDR-STATE-CODE / '=' HDR-RECORD-FLOW-STATUS
            NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Origin_System(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Record_Type(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp(),
            NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Journal_Date(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Participation_Date(),
            NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Pin_Number(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_State_Code(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Record_Flow_Status());
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"DETAIL RECORD FIELDS");                                                                                                            //Natural: WRITE / 'DETAIL RECORD FIELDS'
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #Y 1 50
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(50)); pnd_Y.nadd(1))
        {
            if (condition(ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Y).equals(" ")))                                                                    //Natural: IF DTL-TRANSACTION-TYPE ( #Y ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Y), //Natural: WRITE '=' DTL-TRANSACTION-AMT ( #Y ) / '=' DTL-TRANSACTION-TYPE ( #Y ) / '=' DTL-TYPE-RECORD ( #Y ) / '=' DTL-OCCURRENCE-NBR ( #Y ) / '=' DTL-FROM-TIAA-CONTRACT ( #Y ) / '=' DTL-FROM-CREF-CONTRACT ( #Y ) / '=' DTL-FROM-FUND-TICKER-SYMBOL ( #Y ) / '=' DTL-FROM-LOB ( #Y ) / '=' DTL-FROM-SUB-LOB ( #Y ) / '=' DTL-TO-TIAA-CONTRACT ( #Y ) / '=' DTL-TO-CREF-CONTRACT ( #Y ) / '=' DTL-TO-FUND-TICKER-SYMBOL ( #Y ) / '=' DTL-TO-LOB ( #Y ) / '=' DTL-TO-SUB-LOB ( #Y ) / '=' DTL-UNLEDGER-INDICATOR ( #Y ) / '=' DTL-REVERSAL-CODE ( #Y ) / '=' DTL-SETTLEMENT-IND ( #Y ) / '=' DTL-SETTLEMENT-IND2 ( #Y )
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Type_Record().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Occurrence_Nbr().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Fund_Ticker_Symbol().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Y),NEWLINE,
                "=",ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Reversal_Code().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Y));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Format_Omni_Record() throws Exception                                                                                                                //Natural: FORMAT-OMNI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //* *#PIN := #UNIQUE-ID
        //* *#FROM-CNTRCT := #IATA422-FROM-PPCN-NBR
        //* *#FROM-PAYEE := #IATA422-FROM-PAYEE-CDE
        //* *#TO-CNTRCT := #IATA422-TO-PPCN-NBR
        //* *#TO-PAYEE := #IATA422-TO-PYE-CDE
        //* *#ACTVTY-AS-OF-DTE := HDR-JOURNAL-DATE
        //* *#ACTVTY-AS-OF-DTE := HDR-PARTICIPATION-DATE
        //* *IF #FROM-LOAD
        //*   #ACTION := 'S'  /* SEND SELL TRANSACTION
        //*   IF #IATA422-LDGR-ACCT-CD = 'A'
        //*     #ACTVTY-TYP := 'PS'
        //*   ELSE
        //*     #ACTVTY-TYP := 'MT'
        //*   END-IF
        //*   #RQSTD-CASH := 0 + #IATA422-LDGR-AMT (*)
        //*   ADD 1 TO #OMNI-CNT
        //*   #OMNI-HOLD(#OMNI-CNT) := #OMNI-REC
        //*   IF #IATA422-SWITCH-IND NE ' '  /* SWITCH
        //*     #ACTION := 'B' /* SEND BUY TRANSACTION
        //*     IF #IATA422-LDGR-ACCT-CD = 'A'
        //*       #ACTVTY-TYP := 'MT' /* SWITCH FROM ANNUAL TO MONTHLY
        //*     ELSE
        //*       #ACTVTY-TYP := 'PS' /* SWITCH FROM MONTHLY TO ANNUAL
        //*     END-IF
        //*     ADD 1 TO #OMNI-CNT
        //*     #OMNI-HOLD(#OMNI-CNT) := #OMNI-REC
        //*   END-IF
        //* *ELSE             /* SEND BUY TRANSACTION FOR TRANSFER TO TIAA ACCESS
        //*   #ACTION := 'B'
        //*   IF #IATA422-LDGR-TO-UNIT-TYP (#X) = 'A'
        //*     #ACTVTY-TYP := 'PS'
        //*   ELSE
        //*     #ACTVTY-TYP := 'MT'
        //*   END-IF
        //*   #RQSTD-CASH := #IATA422-LDGR-AMT (#X)
        //*   ADD 1 TO #OMNI-CNT
        //*   #OMNI-HOLD(#OMNI-CNT) := #OMNI-REC
        //* *END-IF
    }
    private void sub_Write_Omni_Record() throws Exception                                                                                                                 //Natural: WRITE-OMNI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //* *RESET NAZ-TABLE-DDM
        //* *NAZ-TBL-RCRD-UPDT-DTE := *DATX
        //* *NAZ-TBL-RCRD-TYP-IND := 'C'
        //* *NAZ-TBL-RCRD-LVL1-ID := 'NAZ070'
        //* *NAZ-TBL-RCRD-LVL2-ID := 'IAX'
        //* *NAZ-TBL-RCRD-LVL3-ID := 'OMNIXFR'
        //* *NAZ-TBL-RCRD-DSCRPTN-TXT := 'IA TRANSFER TO OMNI TRADE'
        //* *NAZ-TBL-SECNDRY-DSCRPTN-TXT(*) := #OMNI-HOLD(*)
        //* *STORE NAZ-TABLE-DDM
    }
    private void sub_Write_Reports() throws Exception                                                                                                                     //Natural: WRITE-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Report_Detail.reset();                                                                                                                                        //Natural: RESET #REPORT-DETAIL #S-REPORT-DETAIL
        pnd_S_Report_Detail.reset();
        pnd_W_Date.setValue(ldaIatlcalm.getLama1001_Hdr_Journal_Date());                                                                                                  //Natural: ASSIGN #W-DATE := HDR-JOURNAL-DATE
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date_Pnd_W_Date_A);                                                                                  //Natural: MOVE EDITED #W-DATE-A TO #DATD ( EM = YYYYMMDD )
        if (condition(pnd_Switch.getBoolean()))                                                                                                                           //Natural: IF #SWITCH
        {
            pnd_S_Hdr2_Pnd_Shdr_Jdate.setValueEdited(pnd_Datd,new ReportEditMask("MM/DD/YYYY"));                                                                          //Natural: MOVE EDITED #DATD ( EM = MM/DD/YYYY ) TO #SHDR-JDATE
            if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #FR-TIAA
            {
                pnd_S_Report_Detail_Pnd_Contract.setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(1));                                                  //Natural: ASSIGN #CONTRACT := DTL-FROM-TIAA-CONTRACT ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_S_Report_Detail_Pnd_Contract.setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(1));                                                  //Natural: ASSIGN #CONTRACT := DTL-FROM-CREF-CONTRACT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #Y 1 50
            for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(50)); pnd_Y.nadd(1))
            {
                if (condition(ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Y).equals(" ")))                                                                //Natural: IF DTL-TRANSACTION-TYPE ( #Y ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_S_Report_Detail_Pnd_Tkr.setValue(pnd_From_Tckr);                                                                                                      //Natural: ASSIGN #TKR := #FROM-TCKR
                pnd_S_Report_Detail_Pnd_Ctls_Ticker.setValue(ldaIatlcalm.getLama1001_Dtl_From_Fund_Ticker_Symbol().getValue(pnd_Y));                                      //Natural: ASSIGN #CTLS-TICKER := DTL-FROM-FUND-TICKER-SYMBOL ( #Y )
                pnd_S_Report_Detail_Pnd_Switch_Amt.setValue(ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Y));                                               //Natural: ASSIGN #SWITCH-AMT := DTL-TRANSACTION-AMT ( #Y )
                //*  051309
                pls_Tot_Switch_Amt.nadd(pnd_S_Report_Detail_Pnd_Switch_Amt);                                                                                              //Natural: ADD #SWITCH-AMT TO +TOT-SWITCH-AMT
                //*  ANNUAL TO MONTHLY SWITCH
                if (condition(pnd_Settl_Ind.equals("A")))                                                                                                                 //Natural: IF #SETTL-IND = 'A'
                {
                    pnd_S_Report_Detail_Pnd_Fr_Reval.setValue("Annual");                                                                                                  //Natural: ASSIGN #FR-REVAL := 'Annual'
                    pnd_S_Report_Detail_Pnd_To_Reval.setValue("Monthly");                                                                                                 //Natural: ASSIGN #TO-REVAL := 'Monthly'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_S_Report_Detail_Pnd_Fr_Reval.setValue("Monthly");                                                                                                 //Natural: ASSIGN #FR-REVAL := 'Monthly'
                    pnd_S_Report_Detail_Pnd_To_Reval.setValue("Annual");                                                                                                  //Natural: ASSIGN #TO-REVAL := 'Annual'
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(5, ReportOption.NOTITLE,pnd_S_Report_Detail_Pnd_Contract,pnd_S_Report_Detail_Pnd_F1,pnd_S_Report_Detail_Pnd_Tkr,pnd_S_Report_Detail_Pnd_F2, //Natural: WRITE ( 5 ) #S-REPORT-DETAIL
                    pnd_S_Report_Detail_Pnd_Ctls_Ticker,pnd_S_Report_Detail_Pnd_F3,pnd_S_Report_Detail_Pnd_Fr_Reval,pnd_S_Report_Detail_Pnd_F4,pnd_S_Report_Detail_Pnd_To_Reval,
                    pnd_S_Report_Detail_Pnd_F5,pnd_S_Report_Detail_Pnd_Switch_Amt);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_T_Hdr2_Pnd_Thdr_Jdate.setValueEdited(pnd_Datd,new ReportEditMask("MM/DD/YYYY"));                                                                          //Natural: MOVE EDITED #DATD ( EM = MM/DD/YYYY ) TO #THDR-JDATE
            if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #FR-TIAA
            {
                pnd_Report_Detail_Pnd_From_Contract.setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(1));                                               //Natural: ASSIGN #FROM-CONTRACT := DTL-FROM-TIAA-CONTRACT ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Report_Detail_Pnd_From_Contract.setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(1));                                               //Natural: ASSIGN #FROM-CONTRACT := DTL-FROM-CREF-CONTRACT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            FOR04:                                                                                                                                                        //Natural: FOR #Y 1 50
            for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(50)); pnd_Y.nadd(1))
            {
                if (condition(ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Y).equals(" ")))                                                                //Natural: IF DTL-TRANSACTION-TYPE ( #Y ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_To_Acct.getValue(pnd_Y).equals(pnd_Tiaa_Fnd_Cdes.getValue("*"))))                                                                       //Natural: IF #TO-ACCT ( #Y ) = #TIAA-FND-CDES ( * )
                {
                    pnd_Report_Detail_Pnd_To_Contract.setValue(ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Y));                                           //Natural: ASSIGN #TO-CONTRACT := DTL-TO-TIAA-CONTRACT ( #Y )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Report_Detail_Pnd_To_Contract.setValue(ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Y));                                           //Natural: ASSIGN #TO-CONTRACT := DTL-TO-CREF-CONTRACT ( #Y )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Report_Detail_Pnd_Fr_Tkr.setValue(pnd_From_Tckr);                                                                                                     //Natural: ASSIGN #FR-TKR := #FROM-TCKR
                pnd_Report_Detail_Pnd_Ctls_Fr_Ticker.setValue(ldaIatlcalm.getLama1001_Dtl_From_Fund_Ticker_Symbol().getValue(pnd_Y));                                     //Natural: ASSIGN #CTLS-FR-TICKER := DTL-FROM-FUND-TICKER-SYMBOL ( #Y )
                pnd_Report_Detail_Pnd_To_Tkr.setValue(pnd_To_Tckr.getValue(pnd_Y));                                                                                       //Natural: ASSIGN #TO-TKR := #TO-TCKR ( #Y )
                pnd_Report_Detail_Pnd_Ctls_To_Ticker.setValue(ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Y));                                       //Natural: ASSIGN #CTLS-TO-TICKER := DTL-TO-FUND-TICKER-SYMBOL ( #Y )
                pnd_Report_Detail_Pnd_Trans_Amt.setValue(ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Y));                                                  //Natural: ASSIGN #TRANS-AMT := DTL-TRANSACTION-AMT ( #Y )
                //*  051309
                pls_Tot_Trans_Amt.nadd(pnd_Report_Detail_Pnd_Trans_Amt);                                                                                                  //Natural: ADD #TRANS-AMT TO +TOT-TRANS-AMT
                if (condition(ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Y).equals("N")))                                                              //Natural: IF DTL-UNLEDGER-INDICATOR ( #Y ) = 'N'
                {
                    pnd_Report_Detail_Pnd_Ldgr_Ind.setValue("Y");                                                                                                         //Natural: ASSIGN #LDGR-IND := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Report_Detail_Pnd_Ldgr_Ind.setValue("N");                                                                                                         //Natural: ASSIGN #LDGR-IND := 'N'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Report_Detail_Pnd_Ind1.setValue(ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Y));                                                        //Natural: ASSIGN #IND1 := DTL-SETTLEMENT-IND ( #Y )
                pnd_Report_Detail_Pnd_Ind2.setValue(ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Y));                                                       //Natural: ASSIGN #IND2 := DTL-SETTLEMENT-IND2 ( #Y )
                getReports().write(4, ReportOption.NOTITLE,pnd_Report_Detail_Pnd_From_Contract,pnd_Report_Detail_Pnd_F1,pnd_Report_Detail_Pnd_Fr_Tkr,pnd_Report_Detail_Pnd_F2, //Natural: WRITE ( 4 ) #REPORT-DETAIL
                    pnd_Report_Detail_Pnd_Ctls_Fr_Ticker,pnd_Report_Detail_Pnd_F3,pnd_Report_Detail_Pnd_To_Contract,pnd_Report_Detail_Pnd_F4,pnd_Report_Detail_Pnd_To_Tkr,
                    pnd_Report_Detail_Pnd_F5,pnd_Report_Detail_Pnd_Ctls_To_Ticker,pnd_Report_Detail_Pnd_F6,pnd_Report_Detail_Pnd_Trans_Amt,pnd_Report_Detail_Pnd_F7,
                    pnd_Report_Detail_Pnd_Ldgr_Ind,pnd_Report_Detail_Pnd_F8,pnd_Report_Detail_Pnd_Ind1,pnd_Report_Detail_Pnd_F9,pnd_Report_Detail_Pnd_Ind2);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_T_Hdr1_Pnd_Tprog.setValue(Global.getPROGRAM());                                                                                                   //Natural: ASSIGN #TPROG := *PROGRAM
                    pnd_T_Hdr2_Pnd_Thdr_Page.nadd(1);                                                                                                                     //Natural: ADD 1 TO #THDR-PAGE
                    getReports().write(4, ReportOption.NOTITLE,pnd_T_Hdr1_Pnd_Tprog,pnd_T_Hdr1_Pnd_Tfill1,pnd_T_Hdr1_Pnd_Thdr1,pnd_T_Hdr1_Pnd_Tfill2,pnd_T_Hdr1_Pnd_Thdr2, //Natural: WRITE ( 4 ) NOTITLE #T-HDR1
                        pnd_T_Hdr1_Pnd_Thdr_Dte);
                    getReports().write(4, ReportOption.NOTITLE,pnd_T_Hdr2_Pnd_Tfill,pnd_T_Hdr2_Pnd_Thdr,pnd_T_Hdr2_Pnd_Thdr_Jdate,pnd_T_Hdr2_Pnd_Tfill2,                  //Natural: WRITE ( 4 ) #T-HDR2
                        pnd_T_Hdr2_Pnd_Tpage_Lit,pnd_T_Hdr2_Pnd_Thdr_Page);
                    getReports().write(4, ReportOption.NOTITLE,pnd_Hdr3_Pnd_Hdr3_Fld1,pnd_Hdr3_Pnd_Hdr3_Fld2,pnd_Hdr3_Pnd_Hdr3_Fld3,pnd_Hdr3_Pnd_Hdr3_Fld4,               //Natural: WRITE ( 4 ) #HDR3
                        pnd_Hdr3_Pnd_Hdr3_Fld5,pnd_Hdr3_Pnd_Hdr3_Fld6,pnd_Hdr3_Pnd_Hdr3_Fld7,pnd_Hdr3_Pnd_Hdr3_Fld8,pnd_Hdr3_Pnd_Hdr3_Fld9,pnd_Hdr3_Pnd_Hdr3_Fld10);
                    getReports().write(4, ReportOption.NOTITLE,pnd_Dashes_Pnd_Dsh_Fld1,pnd_Dashes_Pnd_Dsh_Fld2,pnd_Dashes_Pnd_Dsh_Fld3,pnd_Dashes_Pnd_Dsh_Fld4,           //Natural: WRITE ( 4 ) #DASHES
                        pnd_Dashes_Pnd_Dsh_Fld5,pnd_Dashes_Pnd_Dsh_Fld6,pnd_Dashes_Pnd_Dsh_Fld7,pnd_Dashes_Pnd_Dsh_Fld8,pnd_Dashes_Pnd_Dsh_Fld9,pnd_Dashes_Pnd_Dsh_Fld10);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_S_Hdr1_Pnd_Sprog.setValue(Global.getPROGRAM());                                                                                                   //Natural: ASSIGN #SPROG := *PROGRAM
                    pnd_S_Hdr2_Pnd_Shdr_Page.nadd(1);                                                                                                                     //Natural: ADD 1 TO #SHDR-PAGE
                    getReports().write(5, ReportOption.NOTITLE,pnd_S_Hdr1_Pnd_Sprog,pnd_S_Hdr1_Pnd_Sfill1,pnd_S_Hdr1_Pnd_Shdr1,pnd_S_Hdr1_Pnd_Sfill2,pnd_S_Hdr1_Pnd_Shdr2, //Natural: WRITE ( 5 ) NOTITLE #S-HDR1
                        pnd_S_Hdr1_Pnd_Shdr_Dte);
                    getReports().write(5, ReportOption.NOTITLE,pnd_S_Hdr2_Pnd_Sfill,pnd_S_Hdr2_Pnd_Shdr,pnd_S_Hdr2_Pnd_Shdr_Jdate,pnd_S_Hdr2_Pnd_Sfill2,                  //Natural: WRITE ( 5 ) #S-HDR2
                        pnd_S_Hdr2_Pnd_Spage_Lit,pnd_S_Hdr2_Pnd_Shdr_Page);
                    getReports().write(5, ReportOption.NOTITLE,pnd_Shdr3_Pnd_Shdr3_Fld1,pnd_Shdr3_Pnd_Shdr3_Fld2,pnd_Shdr3_Pnd_Shdr3_Fld3,pnd_Shdr3_Pnd_Shdr3_Fld4,       //Natural: WRITE ( 5 ) #SHDR3
                        pnd_Shdr3_Pnd_Shdr3_Fld5);
                    getReports().write(5, ReportOption.NOTITLE,pnd_S_Dashes);                                                                                             //Natural: WRITE ( 5 ) #S-DASHES
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(4, "LS=133 PS=60");
        Global.format(5, "LS=133 PS=60");
    }
}
