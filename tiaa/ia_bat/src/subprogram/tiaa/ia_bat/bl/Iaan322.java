/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:35:03 AM
**        * FROM NATURAL SUBPROGRAM : Iaan322
************************************************************
**        * FILE NAME            : Iaan322.java
**        * CLASS NAME           : Iaan322
**        * INSTANCE NAME        : Iaan322
************************************************************
************************************************************************
* PROGRAM    : IAAN322
* SYSTEM     : IASTPA
* TITLE      : CALL POST TO GENERATE MANDATORY STATE LETTERS
* GENERATION : MARCH 02, 1996
* FUNCTION   : CALL POST TO GENERATE
*            | MANDATORY STATE LETTERS (MST)
*            |
* 09-26-2005 | ALTHEA YOUNG  - ADDED TEST SWITCH LOGIC TO POPULATE
*            |                 VARIABLES
*            |                 PSTA9611.PRNTR-ID-CDE          := 'TC89'
*            |                 PSTA9611.PCKGE-IMMDTE-PRNT-IND := 'Y'
*            |                 WHICH WILL ALLOW LETTERS TO PRINT
*            |                 IMMEDIATELY AT A LOCAL PRINTER.
*            |                 #TEST MUST BE 'FALSE' IN PRODUCTION!!!
*            |
* 08-10-2005 | JOHN OSTEEN   - RESTOWED TO PICK UP IAAA322 CHANGES
*            |
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan322 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaIaaa322 pdaIaaa322;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private LdaFcpl9700 ldaFcpl9700;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Test;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        ldaFcpl9700 = new LdaFcpl9700();
        registerRecord(ldaFcpl9700);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaPsta9610 = new PdaPsta9610(parameters);
        pdaPsta9612 = new PdaPsta9612(parameters);
        pdaIaaa322 = new PdaIaaa322(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Test = localVariables.newFieldInRecord("pnd_Test", "#TEST", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl9700.initializeValues();

        localVariables.reset();
        pnd_Test.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan322() throws Exception
    {
        super("Iaan322");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  #TEST MUST BE 'FALSE' IN PRODUCTION!  COMMENT OUT THE FOLLOWING LINE!
        //*  #TEST := TRUE                                            /* 09-26-2005
        ldaFcpl9700.getPstl9700_Data_Psta3005_Tax().reset();                                                                                                              //Natural: RESET PSTL9700-DATA.PSTA3005-TAX PSTL9700-DATA.PSTA3005-SET-TAX PSTL9700-DATA.PSTA3006-TAX
        ldaFcpl9700.getPstl9700_Data_Psta3005_Set_Tax().reset();
        ldaFcpl9700.getPstl9700_Data_Psta3006_Tax().reset();
        ldaFcpl9700.getPstl9700_Data().setValuesByName(pdaIaaa322.getIaaa322_Data());                                                                                     //Natural: MOVE BY NAME IAAA322-DATA TO PSTL9700-DATA
        //*  09-26-2005
        if (condition(pnd_Test.getBoolean()))                                                                                                                             //Natural: IF #TEST
        {
            pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue("TC89");                                                                                                      //Natural: ASSIGN PSTA9612.PRNTR-ID-CDE := 'TC89'
            pdaPsta9612.getPsta9612_Pckge_Immdte_Prnt_Ind().setValue("Y");                                                                                                //Natural: ASSIGN PSTA9612.PCKGE-IMMDTE-PRNT-IND := 'Y'
            //*  09-26-2005
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        //*  CALL POST OPEN
        //* **
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //* **
        //*  CHECK FOR ERRORS IN POST OPEN.
        //* **
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        //*  CALL POST WRITE
        //* **
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL9700-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaFcpl9700.getSort_Key(), ldaFcpl9700.getPstl9700_Data());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        //*  CALL POST CLOSE TO FINALISE THE REQUEST
        //* **
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR
    }
    private void sub_Error() throws Exception                                                                                                                             //Natural: ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  FORMAT ##MSG
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
    }

    //
}
