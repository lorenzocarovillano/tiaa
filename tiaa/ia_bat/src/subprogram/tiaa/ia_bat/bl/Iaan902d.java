/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:04 AM
**        * FROM NATURAL SUBPROGRAM : Iaan902d
************************************************************
**        * FILE NAME            : Iaan902d.java
**        * CLASS NAME           : Iaan902d
**        * INSTANCE NAME        : Iaan902d
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN902D CREATES COMBINE CHECK TRANS CODING SHEETS*
*      DATE     -  8/94                                              *
* 04/2017 OS RE-STOWED FOR IAAL902D PIN EXPANSION.                   *
**********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan902d extends BLNatBase
{
    // Data Areas
    private LdaIaal902d ldaIaal902d;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Passed_Data;
    private DbsField pnd_Passed_Data_Pnd_Last_Batch_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Records_Bypassed_Ctr;
    private DbsField pnd_Passed_Data_Pnd_Records_Processed_Ctr;
    private DbsField pnd_Reason;
    private DbsField pnd_Verifier;
    private DbsField pnd_Save_Cmbne_Cde;
    private DbsField pnd_Save_Invrse_Dte;
    private DbsField pnd_Save_Xref;
    private DbsField pnd_Intent_Cde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal902d = new LdaIaal902d();
        registerRecord(ldaIaal902d);
        registerRecord(ldaIaal902d.getVw_iaa_Cntrct());
        registerRecord(ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal902d.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal902d.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();

        pnd_Passed_Data = parameters.newGroupInRecord("pnd_Passed_Data", "#PASSED-DATA");
        pnd_Passed_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Passed_Data_Pnd_Last_Batch_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Passed_Data_Pnd_Records_Bypassed_Ctr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Records_Bypassed_Ctr", "#RECORDS-BYPASSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Passed_Data_Pnd_Records_Processed_Ctr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Records_Processed_Ctr", "#RECORDS-PROCESSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Reason = localVariables.newFieldInRecord("pnd_Reason", "#REASON", FieldType.STRING, 2);
        pnd_Verifier = localVariables.newFieldInRecord("pnd_Verifier", "#VERIFIER", FieldType.STRING, 8);
        pnd_Save_Cmbne_Cde = localVariables.newFieldInRecord("pnd_Save_Cmbne_Cde", "#SAVE-CMBNE-CDE", FieldType.STRING, 12);
        pnd_Save_Invrse_Dte = localVariables.newFieldInRecord("pnd_Save_Invrse_Dte", "#SAVE-INVRSE-DTE", FieldType.NUMERIC, 12);
        pnd_Save_Xref = localVariables.newFieldInRecord("pnd_Save_Xref", "#SAVE-XREF", FieldType.STRING, 9);
        pnd_Intent_Cde = localVariables.newFieldInRecord("pnd_Intent_Cde", "#INTENT-CDE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal902d.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Iaan902d() throws Exception
    {
        super("Iaan902d");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        READWORK01:                                                                                                                                                       //Natural: READ WORK 2 WS-TRANS-304-RCRD
        while (condition(getWorkFiles().read(2, ldaIaal902d.getWs_Trans_304_Rcrd())))
        {
            getSort().writeSortInData(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Invrse_Trans_Dte(),                    //Natural: END-ALL
                ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte(), 
                ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Area(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Dte(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Lst_Trans_Dte(), 
                ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Sub_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde(), 
                ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Id());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Invrse_Trans_Dte(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(),  //Natural: SORT THEM BY WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE WS-TRANS-304-RCRD.#INVRSE-TRANS-DTE WS-TRANS-304-RCRD.#TRANS-PPCN-NBR WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE USING WS-TRANS-304-RCRD.#CNTRL-FRST-TRANS-DTE WS-TRANS-304-RCRD.#TRANS-USER-AREA WS-TRANS-304-RCRD.#TRANS-DTE WS-TRANS-304-RCRD.#LST-TRANS-DTE WS-TRANS-304-RCRD.#TRANS-SUB-CDE WS-TRANS-304-RCRD.#TRANS-CDE WS-TRANS-304-RCRD.#TRANS-ACTVTY-CDE WS-TRANS-304-RCRD.#TRANS-CHECK-DTE WS-TRANS-304-RCRD.#TRANS-USER-ID
            ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());
        SORT01:
        while (condition(getSort().readSortOutData(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Invrse_Trans_Dte(), 
            ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte(), 
            ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Area(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Dte(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Lst_Trans_Dte(), 
            ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Sub_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cde(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde(), 
            ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte(), ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Id())))
        {
            if (condition(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde().equals(pnd_Save_Cmbne_Cde)))                                                             //Natural: IF WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE = #SAVE-CMBNE-CDE
            {
                if (condition(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Invrse_Trans_Dte().equals(pnd_Save_Invrse_Dte)))                                                       //Natural: IF WS-TRANS-304-RCRD.#INVRSE-TRANS-DTE = #SAVE-INVRSE-DTE
                {
                    if (condition(pnd_Intent_Cde.equals(" ")))                                                                                                            //Natural: IF #INTENT-CDE = ' '
                    {
                        pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                                 //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-RCRD
                        sub_Save_Rcrd();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM FIND-CPR
                        sub_Find_Cpr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Intent_Cde.equals("3") && ldaIaal902d.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde().equals(" ")))                               //Natural: IF #INTENT-CDE = '3' AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CMBNE-CDE = ' '
                        {
                            pnd_Passed_Data_Pnd_Records_Processed_Ctr.nadd(1);                                                                                            //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-RCRD
                            sub_Save_Rcrd();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Intent_Cde.equals("1") && ldaIaal902d.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde()))) //Natural: IF #INTENT-CDE = '1' AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CMBNE-CDE = WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE
                            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-RCRD-NBR
                                sub_Determine_Rcrd_Nbr();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Passed_Data_Pnd_Records_Processed_Ctr.nadd(1);                                                                                        //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-RCRD
                                sub_Save_Rcrd();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                         //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Intent_Cde.equals("1") && ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(1)))                                        //Natural: IF #INTENT-CDE = '1' AND #CMBNE-CTR EQ 1
                {
                    pnd_Intent_Cde.reset();                                                                                                                               //Natural: RESET #INTENT-CDE #SAVE-CMBNE-CDE #SAVE-INVRSE-DTE
                    pnd_Save_Cmbne_Cde.reset();
                    pnd_Save_Invrse_Dte.reset();
                    ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().setValue(0);                                                                               //Natural: ASSIGN #CMBNE-CTR := 0
                    pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
                    pnd_Passed_Data_Pnd_Records_Processed_Ctr.nsubtract(1);                                                                                               //Natural: SUBTRACT 1 FROM #RECORDS-PROCESSED-CTR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Intent_Cde.notEquals(" ") && ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().notEquals(4)))                                  //Natural: IF #INTENT-CDE NE ' ' AND #CMBNE-CTR NE 4
                {
                    getWorkFiles().write(1, false, ldaIaal902d.getPnd_Comb_Chk_Trans());                                                                                  //Natural: WRITE WORK FILE 1 #COMB-CHK-TRANS
                    pnd_Intent_Cde.reset();                                                                                                                               //Natural: RESET #INTENT-CDE #SAVE-CMBNE-CDE #SAVE-INVRSE-DTE
                    pnd_Save_Cmbne_Cde.reset();
                    pnd_Save_Invrse_Dte.reset();
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().setValue(0);                                                                                   //Natural: ASSIGN #CMBNE-CTR := 0
                if (condition(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr().notEquals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn()) &&                    //Natural: IF WS-TRANS-304-RCRD.#TRANS-PPCN-NBR NE WS-TRANS-304-RCRD.#TRANS-CC-PPCN AND WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE NE WS-TRANS-304-RCRD.#TRANS-CC-PAYEE
                    ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().notEquals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee())))
                {
                    pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FIND-CPR
                sub_Find_Cpr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaIaal902d.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde().equals(" ")))                                                                     //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CMBNE-CDE = ' '
                {
                    pnd_Intent_Cde.setValue("3");                                                                                                                         //Natural: ASSIGN #INTENT-CDE := '3'
                    ldaIaal902d.getPnd_Comb_Chk_Trans().reset();                                                                                                          //Natural: RESET #COMB-CHK-TRANS
                                                                                                                                                                          //Natural: PERFORM DETERMINE-RCRD-NBR
                    sub_Determine_Rcrd_Nbr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Intent_Code().setValue("3");                                                                                    //Natural: ASSIGN #COMB-CHK-TRANS.#INTENT-CODE := '3'
                    pnd_Passed_Data_Pnd_Records_Processed_Ctr.nadd(1);                                                                                                    //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-RCRD
                    sub_Save_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal902d.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde())))                  //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CMBNE-CDE = WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE
                {
                    pnd_Intent_Cde.setValue("1");                                                                                                                         //Natural: ASSIGN #INTENT-CDE := '1'
                    ldaIaal902d.getPnd_Comb_Chk_Trans().reset();                                                                                                          //Natural: RESET #COMB-CHK-TRANS
                                                                                                                                                                          //Natural: PERFORM DETERMINE-RCRD-NBR
                    sub_Determine_Rcrd_Nbr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Intent_Code().setValue("1");                                                                                    //Natural: ASSIGN #COMB-CHK-TRANS.#INTENT-CODE := '1'
                    pnd_Passed_Data_Pnd_Records_Processed_Ctr.nadd(1);                                                                                                    //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-RCRD
                    sub_Save_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-RCRD
                    sub_Save_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //* *IF #INTENT-CDE NE ' '
        //*  WRITE 'AFTER END-SORT WRITING WORK 1 #COMB-CHK-TRANS'
        //*  WRITE WORK 1 #COMB-CHK-TRANS
        //* *END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-RCRD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-CPR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR
        //*          IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
        //*          IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        //*          IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
        //*          IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
        //*          IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        //*          IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-RCRD-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-HEADER
    }
    private void sub_Save_Rcrd() throws Exception                                                                                                                         //Natural: SAVE-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Cmbne_Cde.setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde());                                                                              //Natural: ASSIGN #SAVE-CMBNE-CDE := WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE
        pnd_Save_Invrse_Dte.setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Invrse_Trans_Dte());                                                                            //Natural: ASSIGN #SAVE-INVRSE-DTE := WS-TRANS-304-RCRD.#INVRSE-TRANS-DTE
    }
    private void sub_Find_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: FIND-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902d.getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                          //Natural: ASSIGN #IAA-CNTRCT-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902d.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(), WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal902d.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal902d.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902d.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA CONTRACT RECORD FOR : ",ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                             //Natural: WRITE 'NO IAA CONTRACT RECORD FOR : ' #TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal902d.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                  //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Find_Cpr() throws Exception                                                                                                                          //Natural: FIND-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                             //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := WS-TRANS-304-RCRD.#TRANS-PPCN-NBR
        ldaIaal902d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());                           //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE
        ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902d.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
        {
            ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO CPR RECORD FOR :",ldaIaal902d.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                                                 //Natural: WRITE 'NO CPR RECORD FOR :' #IAA-CNTRCT-PRTCPNT-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cross_Reference_Nbr() throws Exception                                                                                                           //Natural: GET-CROSS-REFERENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet386 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().equals(1))))
        {
            decideConditionsMet386++;
            if (condition(ldaIaal902d.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902d.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ02",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902d.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ02:
                while (condition(ldaIaal902d.getVw_iaa_Cntrct_Trans().readNextRow("READ02")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr())))                           //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                    pnd_Save_Xref.setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                                                                 //Natural: ASSIGN #SAVE-XREF := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde());                               //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Mm());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dd());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Yy());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-YY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                               //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                    pnd_Save_Xref.setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                                                       //Natural: ASSIGN #SAVE-XREF := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                     //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-YY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().equals(2))))
        {
            decideConditionsMet386++;
            if (condition(ldaIaal902d.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902d.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ03",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902d.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ03:
                while (condition(ldaIaal902d.getVw_iaa_Cntrct_Trans().readNextRow("READ03")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr())))                           //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(pnd_Save_Xref);                                                                        //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := #SAVE-XREF
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde());                               //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Mm());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dd());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Yy());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(pnd_Save_Xref);                                                                        //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := #SAVE-XREF
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                     //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal902d.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ04",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902d.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ04:
                while (condition(ldaIaal902d.getVw_iaa_Cntrct_Trans().readNextRow("READ04")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr())))                           //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(pnd_Save_Xref);                                                                        //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := #SAVE-XREF
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde());                                //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Mm());                              //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dd());                              //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Yy());                              //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(pnd_Save_Xref);                                                                        //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := #SAVE-XREF
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                      //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902d.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ05",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902d.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ05:
                while (condition(ldaIaal902d.getVw_iaa_Cntrct_Trans().readNextRow("READ05")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr())))                           //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde());                               //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Mm());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dd());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Yy());                             //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(pnd_Save_Xref);                                                                        //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := #SAVE-XREF
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                     //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal902d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902d.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte());                                 //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902d.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ06",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902d.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ06:
                while (condition(ldaIaal902d.getVw_iaa_Cntrct_Trans().readNextRow("READ06")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr())))                           //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(pnd_Save_Xref);                                                                        //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := #SAVE-XREF
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde());                                //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Mm());                              //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dd());                              //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Yy());                              //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                      //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-MM
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DD
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-YY
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().greaterOrEqual(3) && ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().lessOrEqual(99)))))
        {
            decideConditionsMet386++;
            ldaIaal902d.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                             //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGE-ID := '1'
            ldaIaal902d.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                        //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal902d.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());                                 //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal902d.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte());                                        //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
            ldaIaal902d.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
            (
            "READ07",
            new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902d.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
            new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
            1
            );
            READ07:
            while (condition(ldaIaal902d.getVw_iaa_Cpr_Trans().readNextRow("READ07")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(ldaIaal902d.getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr()) && ldaIaal902d.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().equals(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde()))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR = #TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = #TRANS-PAYEE-CDE
            {
                ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Ssn().setValue(ldaIaal902d.getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr());                                                  //Natural: ASSIGN #COMB-CHK-TRANS.#SSN := IAA-CPR-TRANS.PRTCPNT-TAX-ID-NBR
                ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902d.getIaa_Cpr_Trans_Bnfcry_Xref_Ind());                                           //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := IAA-CPR-TRANS.BNFCRY-XREF-IND
                ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_A().setValue(" ");                                                                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX-A := ' '
                ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_A().setValue(" ");                                                                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                     //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());                   //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
                (
                "FIND03",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902d.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
                1
                );
                FIND03:
                while (condition(ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND03", true)))
                {
                    ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal902d.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Ssn().setValue(ldaIaal902d.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#SSN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902d.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                             //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_A().setValue(" ");                                                                                     //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX-A := ' '
                    ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_A().setValue(" ");                                                                                     //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-A := ' '
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Rcrd_Nbr() throws Exception                                                                                                                //Natural: DETERMINE-RCRD-NBR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().nadd(1);                                                                                               //Natural: ADD 1 TO #CMBNE-CTR
        short decideConditionsMet535 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #CMBNE-CTR;//Natural: VALUE 1
        if (condition((ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(1))))
        {
            decideConditionsMet535++;
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Batch_Nbr().compute(new ComputeParameters(false, ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Batch_Nbr()),                    //Natural: COMPUTE #COMB-CHK-TRANS.#BATCH-NBR = #LAST-BATCH-NBR + 1
                pnd_Passed_Data_Pnd_Last_Batch_Nbr.add(1));
            pnd_Passed_Data_Pnd_Last_Batch_Nbr.setValue(ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Batch_Nbr());                                                               //Natural: ASSIGN #LAST-BATCH-NBR := #COMB-CHK-TRANS.#BATCH-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                           //Natural: ASSIGN #CNTRCT-NBR := #TRANS-PPCN-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status().setValueEdited(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #RCRD-STATUS
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(2))))
        {
            decideConditionsMet535++;
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #2ND-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr().setValueEdited(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #2ND-STA-NBR
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(3))))
        {
            decideConditionsMet535++;
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #3RD-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr().setValueEdited(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #3RD-STA-NBR
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(4))))
        {
            decideConditionsMet535++;
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #4TH-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr().setValueEdited(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #4TH-STA-NBR
            getWorkFiles().write(1, false, ldaIaal902d.getPnd_Comb_Chk_Trans());                                                                                          //Natural: WRITE WORK FILE 1 #COMB-CHK-TRANS
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(5))))
        {
            decideConditionsMet535++;
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Batch_Nbr().compute(new ComputeParameters(false, ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Batch_Nbr()),                    //Natural: COMPUTE #COMB-CHK-TRANS.#BATCH-NBR = #LAST-BATCH-NBR + 1
                pnd_Passed_Data_Pnd_Last_Batch_Nbr.add(1));
            pnd_Passed_Data_Pnd_Last_Batch_Nbr.setValue(ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Batch_Nbr());                                                               //Natural: ASSIGN #LAST-BATCH-NBR := #COMB-CHK-TRANS.#BATCH-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(2);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 2
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn());                                            //Natural: ASSIGN #CNTRCT-NBR := #TRANS-CC-PPCN
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status().setValueEdited(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #RCRD-STATUS
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #5TH-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr().setValueEdited(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #5TH-STA-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct().setValue(" ");                                                                                        //Natural: ASSIGN #3RD-COMB-CNTRCT := ' '
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr().setValue(" ");                                                                                            //Natural: ASSIGN #3RD-STA-NBR := ' '
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct().setValue(" ");                                                                                        //Natural: ASSIGN #4TH-COMB-CNTRCT := ' '
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr().setValue(" ");                                                                                            //Natural: ASSIGN #4TH-STA-NBR := ' '
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((ldaIaal902d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(6))))
        {
            decideConditionsMet535++;
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(2);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 2
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #6TH-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr().setValueEdited(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #6TH-STA-NBR
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct().setValue(" ");                                                                                        //Natural: ASSIGN #4TH-COMB-CNTRCT := ' '
            ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr().setValue(" ");                                                                                            //Natural: ASSIGN #4TH-STA-NBR := ' '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Assign_Header() throws Exception                                                                                                                     //Natural: ASSIGN-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Mm());                                         //Natural: ASSIGN #CHECK-DTE-MM := #TRANS-CHECK-DTE-MM
        ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Dd());                                         //Natural: ASSIGN #CHECK-DTE-DD := #TRANS-CHECK-DTE-DD
        ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Yy());                                         //Natural: ASSIGN #CHECK-DTE-YY := #TRANS-CHECK-DTE-YY
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT
        sub_Find_Iaa_Cntrct();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR
        sub_Get_Cross_Reference_Nbr();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Trans_Nbr().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_Cde());                                                     //Natural: ASSIGN #COMB-CHK-TRANS.#TRANS-NBR := #TRANS-CDE
        ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_Currency().setValue(ldaIaal902d.getIaa_Cntrct_Cntrct_Crrncy_Cde());                                                         //Natural: ASSIGN #COMB-CHK-TRANS.#CURRENCY := CNTRCT-CRRNCY-CDE
        ldaIaal902d.getPnd_Comb_Chk_Trans_Pnd_User_Area().setValue(ldaIaal902d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Area());                                               //Natural: ASSIGN #USER-AREA := #TRANS-USER-AREA
    }

    //
}
