/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:34:12 AM
**        * FROM NATURAL SUBPROGRAM : Iaan051c
************************************************************
**        * FILE NAME            : Iaan051c.java
**        * CLASS NAME           : Iaan051c
**        * INSTANCE NAME        : Iaan051c
************************************************************
************************************************************************
* PROGRAM  : IAAN051C
* FUNCTION : THIS IS A BRIDGE MODULE THAT CALLS THE NEW STANDARD
*            ROUTINE (IAAN4000) THAT READS THE EXTERNALIZATION FILE.
*            THIS WILL TRANSLATE THE FUND CODE FROM TWO-BYTE ALPHA TO
*            1,2,3,4,6,10,25, OR 35 BYTE(S) DESCRIPTION.  IT WILL ALSO
*            RETURN THE COMPANY WHETHER TIAA, CREF OR T/L. THE LENGTH OF
*            THE DESCRIPTION MUST BE PASSED OR IT WILL DEFAULT TO 3.
*
* 11/05/2013 J TINIO TIAA ACCESS CHANGE - CALL NECN4000 FOR CREF/REA
*                    REDESIGN PROJECT.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan051c extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Fund_2;
    private DbsField pnd_Parm_Desc;
    private DbsField pnd_Cmpny_Desc;
    private DbsField pnd_Parm_Len;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Len;
    private DbsField pnd_Fnd_Cde;
    private DbsField pnd_Fnd_Alpha;
    private DbsField pnd_Cmpy_Cde;
    private DbsField pls_Tckr_Symbl;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;
    private DbsField pls_Cmpny_Cde;
    private DbsField pls_Cmpny_Desc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Fund_2 = parameters.newFieldInRecord("pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Parm_Fund_2.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Desc = parameters.newFieldInRecord("pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);
        pnd_Parm_Desc.setParameterOption(ParameterOption.ByReference);
        pnd_Cmpny_Desc = parameters.newFieldInRecord("pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Cmpny_Desc.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Len = parameters.newFieldInRecord("pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Parm_Len.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.NUMERIC, 2);
        pnd_Fnd_Cde = localVariables.newFieldInRecord("pnd_Fnd_Cde", "#FND-CDE", FieldType.NUMERIC, 2);
        pnd_Fnd_Alpha = localVariables.newFieldInRecord("pnd_Fnd_Alpha", "#FND-ALPHA", FieldType.STRING, 2);
        pnd_Cmpy_Cde = localVariables.newFieldArrayInRecord("pnd_Cmpy_Cde", "#CMPY-CDE", FieldType.STRING, 3, new DbsArrayController(1, 2));
        pls_Tckr_Symbl = WsIndependent.getInstance().newFieldArrayInRecord("pls_Tckr_Symbl", "+TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pls_Cmpny_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Cmpny_Cde", "+CMPNY-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Cmpny_Desc = WsIndependent.getInstance().newFieldArrayInRecord("pls_Cmpny_Desc", "+CMPNY-DESC", FieldType.STRING, 4, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Cmpy_Cde.getValue(1).setInitialValue("001");
        pnd_Cmpy_Cde.getValue(2).setInitialValue("002");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan051c() throws Exception
    {
        super("Iaan051c");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Parm_Desc.reset();                                                                                                                                            //Natural: RESET #PARM-DESC #CMPNY-DESC
        pnd_Cmpny_Desc.reset();
        if (condition(pnd_Parm_Fund_2.equals(" ")))                                                                                                                       //Natural: IF #PARM-FUND-2 = ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.is(pnd_Parm_Fund_2.getText(),"N2")))                                                                                                        //Natural: IF #PARM-FUND-2 IS ( N2 )
        {
            pnd_Fnd_Cde.compute(new ComputeParameters(false, pnd_Fnd_Cde), pnd_Parm_Fund_2.val());                                                                        //Natural: ASSIGN #FND-CDE := VAL ( #PARM-FUND-2 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Len.setValue(pnd_Parm_Len);                                                                                                                                   //Natural: ASSIGN #LEN := #PARM-LEN
        if (condition(pnd_Parm_Fund_2.equals("1G")))                                                                                                                      //Natural: IF #PARM-FUND-2 = '1G'
        {
            short decideConditionsMet1039 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #PARM-LEN;//Natural: VALUE 1
            if (condition((pnd_Parm_Len.equals(1))))
            {
                decideConditionsMet1039++;
                pnd_Parm_Desc.setValue("G");                                                                                                                              //Natural: ASSIGN #PARM-DESC := 'G'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Parm_Len.equals(2))))
            {
                decideConditionsMet1039++;
                pnd_Parm_Desc.setValue("GD");                                                                                                                             //Natural: ASSIGN #PARM-DESC := 'GD'
            }                                                                                                                                                             //Natural: VALUE 3,4,6,0
            else if (condition((pnd_Parm_Len.equals(3) || pnd_Parm_Len.equals(4) || pnd_Parm_Len.equals(6) || pnd_Parm_Len.equals(0))))
            {
                decideConditionsMet1039++;
                pnd_Parm_Desc.setValue("GRD");                                                                                                                            //Natural: ASSIGN #PARM-DESC := 'GRD'
            }                                                                                                                                                             //Natural: VALUE 10,5
            else if (condition((pnd_Parm_Len.equals(10) || pnd_Parm_Len.equals(5))))
            {
                decideConditionsMet1039++;
                pnd_Parm_Desc.setValue("GRADED");                                                                                                                         //Natural: ASSIGN #PARM-DESC := 'GRADED'
            }                                                                                                                                                             //Natural: VALUE 25
            else if (condition((pnd_Parm_Len.equals(25))))
            {
                decideConditionsMet1039++;
                pnd_Parm_Desc.setValue("TIAA GRADED");                                                                                                                    //Natural: ASSIGN #PARM-DESC := 'TIAA GRADED'
            }                                                                                                                                                             //Natural: VALUE 30,35
            else if (condition((pnd_Parm_Len.equals(30) || pnd_Parm_Len.equals(35))))
            {
                decideConditionsMet1039++;
                pnd_Parm_Desc.setValue("TIAA GRADED ACCOUNT");                                                                                                            //Natural: ASSIGN #PARM-DESC := 'TIAA GRADED ACCOUNT'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Cmpny_Desc.setValue("TIAA");                                                                                                                              //Natural: ASSIGN #CMPNY-DESC := 'TIAA'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Fund_2.equals("1S")))                                                                                                                      //Natural: IF #PARM-FUND-2 = '1S'
        {
            short decideConditionsMet1063 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #PARM-LEN;//Natural: VALUE 1
            if (condition((pnd_Parm_Len.equals(1))))
            {
                decideConditionsMet1063++;
                pnd_Parm_Desc.setValue("T");                                                                                                                              //Natural: ASSIGN #PARM-DESC := 'T'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Parm_Len.equals(2))))
            {
                decideConditionsMet1063++;
                pnd_Parm_Desc.setValue("TI");                                                                                                                             //Natural: ASSIGN #PARM-DESC := 'TI'
            }                                                                                                                                                             //Natural: VALUE 3,4,6,0
            else if (condition((pnd_Parm_Len.equals(3) || pnd_Parm_Len.equals(4) || pnd_Parm_Len.equals(6) || pnd_Parm_Len.equals(0))))
            {
                decideConditionsMet1063++;
                pnd_Parm_Desc.setValue("STD");                                                                                                                            //Natural: ASSIGN #PARM-DESC := 'STD'
            }                                                                                                                                                             //Natural: VALUE 10,5
            else if (condition((pnd_Parm_Len.equals(10) || pnd_Parm_Len.equals(5))))
            {
                decideConditionsMet1063++;
                pnd_Parm_Desc.setValue("STANDARD");                                                                                                                       //Natural: ASSIGN #PARM-DESC := 'STANDARD'
            }                                                                                                                                                             //Natural: VALUE 25
            else if (condition((pnd_Parm_Len.equals(25))))
            {
                decideConditionsMet1063++;
                pnd_Parm_Desc.setValue("TIAA STANDARD");                                                                                                                  //Natural: ASSIGN #PARM-DESC := 'TIAA STANDARD'
            }                                                                                                                                                             //Natural: VALUE 30,35
            else if (condition((pnd_Parm_Len.equals(30) || pnd_Parm_Len.equals(35))))
            {
                decideConditionsMet1063++;
                pnd_Parm_Desc.setValue("TIAA STANDARD ACCOUNT");                                                                                                          //Natural: ASSIGN #PARM-DESC := 'TIAA STANDARD ACCOUNT'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Cmpny_Desc.setValue("TIAA");                                                                                                                              //Natural: ASSIGN #CMPNY-DESC := 'TIAA'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Fund_2.equals("1 ")))                                                                                                                      //Natural: IF #PARM-FUND-2 = '1 '
        {
            short decideConditionsMet1087 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #PARM-LEN;//Natural: VALUE 1
            if (condition((pnd_Parm_Len.equals(1))))
            {
                decideConditionsMet1087++;
                pnd_Parm_Desc.setValue("G");                                                                                                                              //Natural: ASSIGN #PARM-DESC := 'G'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Parm_Len.equals(2))))
            {
                decideConditionsMet1087++;
                pnd_Parm_Desc.setValue("GR");                                                                                                                             //Natural: ASSIGN #PARM-DESC := 'GR'
            }                                                                                                                                                             //Natural: VALUE 3,4,6,0
            else if (condition((pnd_Parm_Len.equals(3) || pnd_Parm_Len.equals(4) || pnd_Parm_Len.equals(6) || pnd_Parm_Len.equals(0))))
            {
                decideConditionsMet1087++;
                pnd_Parm_Desc.setValue("GRP");                                                                                                                            //Natural: ASSIGN #PARM-DESC := 'GRP'
            }                                                                                                                                                             //Natural: VALUE 10,5
            else if (condition((pnd_Parm_Len.equals(10) || pnd_Parm_Len.equals(5))))
            {
                decideConditionsMet1087++;
                pnd_Parm_Desc.setValue("GROUP");                                                                                                                          //Natural: ASSIGN #PARM-DESC := 'GROUP'
            }                                                                                                                                                             //Natural: VALUE 25
            else if (condition((pnd_Parm_Len.equals(25))))
            {
                decideConditionsMet1087++;
                pnd_Parm_Desc.setValue("TIAA GROUP");                                                                                                                     //Natural: ASSIGN #PARM-DESC := 'TIAA GROUP'
            }                                                                                                                                                             //Natural: VALUE 35
            else if (condition((pnd_Parm_Len.equals(35))))
            {
                decideConditionsMet1087++;
                pnd_Parm_Desc.setValue("TIAA GROUP ACCOUNT");                                                                                                             //Natural: ASSIGN #PARM-DESC := 'TIAA GROUP ACCOUNT'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Cmpny_Desc.setValue("TIAA");                                                                                                                              //Natural: ASSIGN #CMPNY-DESC := 'TIAA'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pls_Tckr_Symbl.getValue("*").notEquals(" ")))                                                                                                       //Natural: IF +TCKR-SYMBL ( * ) NE ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM POPULATE-INDEPENDENTS
            sub_Populate_Independents();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-NAME-DESC
        sub_Get_Name_Desc();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-DESC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-INDEPENDENTS
    }
    private void sub_Get_Name_Desc() throws Exception                                                                                                                     //Natural: GET-NAME-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1134 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #LEN;//Natural: VALUE 0,3
        if (condition((pnd_Len.equals(0) || pnd_Len.equals(3))))
        {
            decideConditionsMet1134++;
            pnd_Len.setValue(3);                                                                                                                                          //Natural: ASSIGN #LEN := 3
            if (condition(pnd_Fnd_Cde.greater(40)))                                                                                                                       //Natural: IF #FND-CDE GT 40
            {
                pnd_Len.setValue(20);                                                                                                                                     //Natural: ASSIGN #LEN := 20
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 5,6
        else if (condition((pnd_Len.equals(5) || pnd_Len.equals(6))))
        {
            decideConditionsMet1134++;
            pnd_Len.setValue(5);                                                                                                                                          //Natural: ASSIGN #LEN := 5
            if (condition(pnd_Fnd_Cde.greater(40)))                                                                                                                       //Natural: IF #FND-CDE GT 40
            {
                pnd_Len.setValue(21);                                                                                                                                     //Natural: ASSIGN #LEN := 21
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 10
        else if (condition((pnd_Len.equals(10))))
        {
            decideConditionsMet1134++;
            pnd_Len.setValue(19);                                                                                                                                         //Natural: ASSIGN #LEN := 19
            if (condition(pnd_Fnd_Cde.greater(40)))                                                                                                                       //Natural: IF #FND-CDE GT 40
            {
                pnd_Len.setValue(22);                                                                                                                                     //Natural: ASSIGN #LEN := 22
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 25
        else if (condition((pnd_Len.equals(25))))
        {
            decideConditionsMet1134++;
            pnd_Len.setValue(17);                                                                                                                                         //Natural: ASSIGN #LEN := 17
        }                                                                                                                                                                 //Natural: VALUE 30,35
        else if (condition((pnd_Len.equals(30) || pnd_Len.equals(35))))
        {
            decideConditionsMet1134++;
            pnd_Len.setValue(18);                                                                                                                                         //Natural: ASSIGN #LEN := 18
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pls_Fund_Num_Cde.getValue(pnd_I).equals(pnd_Fnd_Cde)))                                                                                          //Natural: IF +FUND-NUM-CDE ( #I ) = #FND-CDE
            {
                pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValueEdited(pnd_Len,new ReportEditMask("99"));                                                     //Natural: MOVE EDITED #LEN ( EM = 99 ) TO NECA4000.FDS-KEY-FUND-NAME-REF-CDE
                pdaNeca4000.getNeca4000_Fds_Key_Ticker_Symbol().setValue(pls_Tckr_Symbl.getValue(pnd_I));                                                                 //Natural: ASSIGN NECA4000.FDS-KEY-TICKER-SYMBOL := +TCKR-SYMBL ( #I )
                pnd_Cmpny_Desc.setValue(pls_Cmpny_Desc.getValue(pnd_I));                                                                                                  //Natural: ASSIGN #CMPNY-DESC := +CMPNY-DESC ( #I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaNeca4000.getNeca4000_Function_Cde().setValue("FDS");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'FDS'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00") || pdaNeca4000.getNeca4000_Return_Cde().equals(" ")))                                             //Natural: IF NECA4000.RETURN-CDE = '00' OR = ' '
        {
            pnd_Parm_Desc.setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1));                                                                                   //Natural: ASSIGN #PARM-DESC := NECA4000.FDS-FUND-NME ( 1 )
            DbsUtil.examine(new ExamineSource(pnd_Parm_Desc), new ExamineSearch("@"), new ExamineReplace(" "));                                                           //Natural: EXAMINE #PARM-DESC FOR '@' REPLACE ' '
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Populate_Independents() throws Exception                                                                                                             //Natural: POPULATE-INDEPENDENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J #K
        pnd_K.reset();
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            if (condition(pnd_J.greater(2)))                                                                                                                              //Natural: IF #J GT 2
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  BY COMPANY FUND
            //*  1 = TIAA, 2 = PA
            pdaNeca4000.getNeca4000().reset();                                                                                                                            //Natural: RESET NECA4000
            pdaNeca4000.getNeca4000_Function_Cde().setValue("CFN");                                                                                                       //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CFN'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                 //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
            pdaNeca4000.getNeca4000_Cfn_Key_Company_Cde().setValue(pnd_Cmpy_Cde.getValue(pnd_J));                                                                         //Natural: ASSIGN NECA4000.CFN-KEY-COMPANY-CDE := #CMPY-CDE ( #J )
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00") || pdaNeca4000.getNeca4000_Return_Cde().equals("  ")))                                        //Natural: IF NECA4000.RETURN-CDE = '00' OR = '  '
            {
                FOR02:                                                                                                                                                    //Natural: FOR #I 1 TO OUTPUT-CNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaNeca4000.getNeca4000_Output_Cnt())); pnd_I.nadd(1))
                {
                    if (condition(pdaNeca4000.getNeca4000_Cfn_Ticker_Symbol().getValue(pnd_I).equals(" ")))                                                               //Natural: IF CFN-TICKER-SYMBOL ( #I ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Fnd_Alpha.setValue(pdaNeca4000.getNeca4000_Cfn_Fnd_Ia_Fund_Cde().getValue(pnd_I), MoveOption.RightJustified);                                     //Natural: MOVE RIGHT CFN-FND-IA-FUND-CDE ( #I ) TO #FND-ALPHA
                    if (condition(DbsUtil.is(pnd_Fnd_Alpha.getText(),"N2")))                                                                                              //Natural: IF #FND-ALPHA IS ( N2 )
                    {
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                        pls_Tckr_Symbl.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Cfn_Ticker_Symbol().getValue(pnd_I));                                             //Natural: ASSIGN +TCKR-SYMBL ( #K ) := CFN-TICKER-SYMBOL ( #I )
                        pls_Fund_Num_Cde.getValue(pnd_K).compute(new ComputeParameters(false, pls_Fund_Num_Cde.getValue(pnd_K)), pdaNeca4000.getNeca4000_Cfn_Fnd_Ia_Fund_Cde().getValue(pnd_I).val()); //Natural: ASSIGN +FUND-NUM-CDE ( #K ) := VAL ( CFN-FND-IA-FUND-CDE ( #I ) )
                        pls_Fund_Alpha_Cde.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Cfn_Fnd_Alpha_Fund_Cde().getValue(pnd_I));                                    //Natural: ASSIGN +FUND-ALPHA-CDE ( #K ) := CFN-FND-ALPHA-FUND-CDE ( #I )
                        pls_Cmpny_Cde.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Cfn_Fnd_Ivc_Company_Cde().getValue(pnd_I));                                        //Natural: ASSIGN +CMPNY-CDE ( #K ) := CFN-FND-IVC-COMPANY-CDE ( #I )
                        short decideConditionsMet1210 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE CFN-FND-IVC-COMPANY-CDE ( #I );//Natural: VALUE 1
                        if (condition((pdaNeca4000.getNeca4000_Cfn_Fnd_Ivc_Company_Cde().getValue(pnd_I).equals(1))))
                        {
                            decideConditionsMet1210++;
                            pls_Cmpny_Desc.getValue(pnd_K).setValue("TIAA");                                                                                              //Natural: ASSIGN +CMPNY-DESC ( #K ) := 'TIAA'
                        }                                                                                                                                                 //Natural: VALUE 2
                        else if (condition((pdaNeca4000.getNeca4000_Cfn_Fnd_Ivc_Company_Cde().getValue(pnd_I).equals(2))))
                        {
                            decideConditionsMet1210++;
                            pls_Cmpny_Desc.getValue(pnd_K).setValue("CREF");                                                                                              //Natural: ASSIGN +CMPNY-DESC ( #K ) := 'CREF'
                        }                                                                                                                                                 //Natural: NONE VALUE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                        if (condition(pls_Fund_Num_Cde.getValue(pnd_K).greater(40)))                                                                                      //Natural: IF +FUND-NUM-CDE ( #K ) GT 40
                        {
                            pls_Cmpny_Desc.getValue(pnd_K).setValue("T/L");                                                                                               //Natural: ASSIGN +CMPNY-DESC ( #K ) := 'T/L'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //
}
