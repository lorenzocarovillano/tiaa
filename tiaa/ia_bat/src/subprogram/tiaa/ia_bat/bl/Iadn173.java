/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:42:13 AM
**        * FROM NATURAL SUBPROGRAM : Iadn173
************************************************************
**        * FILE NAME            : Iadn173.java
**        * CLASS NAME           : Iadn173
**        * INSTANCE NAME        : Iadn173
************************************************************
************************************************************************
* PROGRAM:  IADN173
* DATE   :  04/11/2006
* FUNCTION: THIS WILL EXTRACT THE BEFORE IMAGE PAYMENT AMOUNT FOR TPA
*           AND IPRO CONTRACTS THAT HAVE PARTIAL OR FULL DRAWDOWN FOR
*           THE CHECK CYCLE.
*
* HISTORY  : 4/20/06 - JUN TINIO
*
* 3/12     : RATE BASES EXPANSION - SCAN ON 3/12
************************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadn173 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Check_Dte;
    private DbsField pnd_Per_Pmt_Amt;
    private DbsField pnd_Final_Pmt_Amt;

    private DataAccessProgramView vw_fund_Trans;
    private DbsField fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Trans_Tiaa_Cmpny_Fund_Cde;
    private DbsField fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField fund_Trans_Trans_Check_Dte;
    private DbsField fund_Trans_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField fund_Trans_Tiaa_Rate_Cde;
    private DbsField fund_Trans_Tiaa_Rate_Dte;
    private DbsField fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Tiaa_Fund_Bfre_Key;

    private DbsGroup pnd_Tiaa_Fund_Bfre_Key__R_Field_1;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Trans_Check_Dte;

    private DbsGroup pnd_Trans_Check_Dte__R_Field_2;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Dd;

    private DbsGroup pnd_Trans_Check_Dte__R_Field_3;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Yyyy;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Mm;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 9);
        pnd_Cntrct_Ppcn_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Dte = parameters.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 6);
        pnd_Check_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Per_Pmt_Amt = parameters.newFieldInRecord("pnd_Per_Pmt_Amt", "#PER-PMT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Per_Pmt_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Final_Pmt_Amt = parameters.newFieldInRecord("pnd_Final_Pmt_Amt", "#FINAL-PMT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Final_Pmt_Amt.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_fund_Trans = new DataAccessProgramView(new NameInfo("vw_fund_Trans", "FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        fund_Trans_Tiaa_Tot_Per_Amt = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        fund_Trans_Tiaa_Tot_Div_Amt = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        fund_Trans_Tiaa_Old_Per_Amt = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        fund_Trans_Tiaa_Old_Div_Amt = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        fund_Trans_Trans_Check_Dte = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Count_Casttiaa_Rate_Data_Grp", "C*TIAA-RATE-DATA-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");

        fund_Trans_Tiaa_Rate_Data_Grp = vw_fund_Trans.getRecord().newGroupArrayInGroup("fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", new DbsArrayController(1, 
            250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Cde = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", FieldType.STRING, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Dte = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("FUND_TRANS_TIAA_RATE_DTE", "TIAA-RATE-DTE", FieldType.DATE, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Per_Pay_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Per_Div_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Final_Pay_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Final_Div_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Rate_Final_Div_Amt", "TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        registerRecord(vw_fund_Trans);

        pnd_Tiaa_Fund_Bfre_Key = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Bfre_Key", "#TIAA-FUND-BFRE-KEY", FieldType.STRING, 22);

        pnd_Tiaa_Fund_Bfre_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tiaa_Fund_Bfre_Key__R_Field_1", "REDEFINE", pnd_Tiaa_Fund_Bfre_Key);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Tiaa_Fund_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr = pnd_Tiaa_Fund_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr", 
            "#TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde = pnd_Tiaa_Fund_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde", 
            "#TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Trans_Check_Dte = localVariables.newFieldInRecord("pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Trans_Check_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Trans_Check_Dte__R_Field_2", "REDEFINE", pnd_Trans_Check_Dte);
        pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm = pnd_Trans_Check_Dte__R_Field_2.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm", "#TRANS-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Trans_Check_Dte_Pnd_Trans_Dd = pnd_Trans_Check_Dte__R_Field_2.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Dd", "#TRANS-DD", FieldType.STRING, 
            2);

        pnd_Trans_Check_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Trans_Check_Dte__R_Field_3", "REDEFINE", pnd_Trans_Check_Dte);
        pnd_Trans_Check_Dte_Pnd_Trans_Yyyy = pnd_Trans_Check_Dte__R_Field_3.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Yyyy", "#TRANS-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Trans_Check_Dte_Pnd_Trans_Mm = pnd_Trans_Check_Dte__R_Field_3.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Mm", "#TRANS-MM", FieldType.NUMERIC, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fund_Trans.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Tiaa_Fund_Bfre_Key.setInitialValue("1");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iadn173() throws Exception
    {
        super("Iadn173");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Per_Pmt_Amt.reset();                                                                                                                                          //Natural: RESET #PER-PMT-AMT #FINAL-PMT-AMT
        pnd_Final_Pmt_Amt.reset();
        pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm.setValue(pnd_Check_Dte);                                                                                                     //Natural: ASSIGN #TRANS-YYYYMM := #CHECK-DTE
        pnd_Trans_Check_Dte_Pnd_Trans_Dd.setValue("01");                                                                                                                  //Natural: ASSIGN #TRANS-DD := '01'
        pnd_Trans_Check_Dte_Pnd_Trans_Mm.nsubtract(1);                                                                                                                    //Natural: SUBTRACT 1 FROM #TRANS-MM
        if (condition(pnd_Trans_Check_Dte_Pnd_Trans_Mm.lessOrEqual(getZero())))                                                                                           //Natural: IF #TRANS-MM LE 0
        {
            pnd_Trans_Check_Dte_Pnd_Trans_Mm.setValue(12);                                                                                                                //Natural: ASSIGN #TRANS-MM := 12
            pnd_Trans_Check_Dte_Pnd_Trans_Yyyy.nsubtract(1);                                                                                                              //Natural: SUBTRACT 1 FROM #TRANS-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr.setValue(pnd_Cntrct_Ppcn_Nbr);                                                                                    //Natural: ASSIGN #TIAA-CNTRCT-PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(1);                                                                                                     //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := 01
        vw_fund_Trans.startDatabaseRead                                                                                                                                   //Natural: READ FUND-TRANS BY TIAA-FUND-BFRE-KEY STARTING FROM #TIAA-FUND-BFRE-KEY
        (
        "READ01",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY", ">=", pnd_Tiaa_Fund_Bfre_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_fund_Trans.readNextRow("READ01")))
        {
            if (condition(fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr) || fund_Trans_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde))) //Natural: IF TIAA-CNTRCT-PPCN-NBR NE #TIAA-CNTRCT-PPCN-NBR OR TIAA-CNTRCT-PAYEE-CDE NE #TIAA-CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(fund_Trans_Trans_Check_Dte.equals(pnd_Trans_Check_Dte) && fund_Trans_Tiaa_Cmpny_Fund_Cde.equals("T1S"))))                                     //Natural: ACCEPT IF TRANS-CHECK-DTE = #TRANS-CHECK-DTE AND TIAA-CMPNY-FUND-CDE = 'T1S'
            {
                continue;
            }
            //*  IPRO CONTRACT
            if (condition(DbsUtil.maskMatches(pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr,"'IP'")))                                                                   //Natural: IF #TIAA-CNTRCT-PPCN-NBR = MASK ( 'IP' )
            {
                pnd_Final_Pmt_Amt.compute(new ComputeParameters(false, pnd_Final_Pmt_Amt), DbsField.add(getZero(),fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*")));     //Natural: COMPUTE #FINAL-PMT-AMT = 0 + TIAA-RATE-FINAL-PAY-AMT ( * )
                pnd_Final_Pmt_Amt.nadd(fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                                                 //Natural: ADD TIAA-RATE-FINAL-DIV-AMT ( * ) TO #FINAL-PMT-AMT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Per_Pmt_Amt.compute(new ComputeParameters(false, pnd_Per_Pmt_Amt), fund_Trans_Tiaa_Tot_Per_Amt.add(fund_Trans_Tiaa_Tot_Div_Amt));                         //Natural: ASSIGN #PER-PMT-AMT := TIAA-TOT-PER-AMT + TIAA-TOT-DIV-AMT
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
