/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:42:32 AM
**        * FROM NATURAL SUBPROGRAM : Iatn045
************************************************************
**        * FILE NAME            : Iatn045.java
**        * CLASS NAME           : Iatn045
**        * INSTANCE NAME        : Iatn045
************************************************************
************************************************************************
* SUBPROGRAM IATN045
* SYSTEM   : IA TRANSFER
* TITLE    : CONFIRMATION AUDIT TRAIL EFM INTERFACE
* DATE     : AUG.  1999
* FUNCTION : CREATE ONLINE CONFIRMATION AUDIT TRAIL
*          |
*          |  CREATE EFM TEXT
*          |  CALL EFM
*          |  LOOP TILL FINISHED
*          | RETURN TO CALLER
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
*
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
*
*
*
*
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn045 extends BLNatBase
{
    // Data Areas
    private PdaIaapda_M pdaIaapda_M;
    private PdaNstpda_E pdaNstpda_E;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField ia_Unique_Id;
    private DbsField xfr_Mit_Log_Dte_Tme;
    private DbsField rqst_Unit_Cde;
    private DbsField rqst_Entry_Dte;
    private DbsField rqst_Entry_User_Id;
    private DbsField rqst_Rcvd_Dte;
    private DbsField rqst_Rcvd_User_Id;
    private DbsField xfr_Work_Prcss_Id;
    private DbsField iatn402_Msg;

    private DbsGroup cwfpda_P;
    private DbsField cwfpda_P_Pnd_Pnd_User;
    private DbsField cwfpda_P_Pnd_Pnd_Help_Nme;
    private DbsField cwfpda_P_Pnd_Pnd_Map_Nme;
    private DbsField cwfpda_P_Pnd_Pnd_Upd_User_Ind;
    private DbsField pnd_Call_Efm;
    private DbsField pnd_Date_A8;
    private DbsField pnd_Date_Reformat1;
    private DbsField pnd_Date_Reformat2;
    private DbsField pnd_Error_Msg;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        pdaNstpda_E = new PdaNstpda_E(localVariables);
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);

        // parameters
        parameters = new DbsRecord();
        ia_Unique_Id = parameters.newFieldInRecord("ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12);
        ia_Unique_Id.setParameterOption(ParameterOption.ByReference);
        xfr_Mit_Log_Dte_Tme = parameters.newFieldInRecord("xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        xfr_Mit_Log_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        rqst_Unit_Cde = parameters.newFieldInRecord("rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8);
        rqst_Unit_Cde.setParameterOption(ParameterOption.ByReference);
        rqst_Entry_Dte = parameters.newFieldInRecord("rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE);
        rqst_Entry_Dte.setParameterOption(ParameterOption.ByReference);
        rqst_Entry_User_Id = parameters.newFieldInRecord("rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 8);
        rqst_Entry_User_Id.setParameterOption(ParameterOption.ByReference);
        rqst_Rcvd_Dte = parameters.newFieldInRecord("rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE);
        rqst_Rcvd_Dte.setParameterOption(ParameterOption.ByReference);
        rqst_Rcvd_User_Id = parameters.newFieldInRecord("rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", FieldType.STRING, 8);
        rqst_Rcvd_User_Id.setParameterOption(ParameterOption.ByReference);
        xfr_Work_Prcss_Id = parameters.newFieldInRecord("xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 6);
        xfr_Work_Prcss_Id.setParameterOption(ParameterOption.ByReference);
        iatn402_Msg = parameters.newFieldInRecord("iatn402_Msg", "IATN402-MSG", FieldType.STRING, 79);
        iatn402_Msg.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        cwfpda_P = localVariables.newGroupInRecord("cwfpda_P", "CWFPDA-P");
        cwfpda_P_Pnd_Pnd_User = cwfpda_P.newFieldInGroup("cwfpda_P_Pnd_Pnd_User", "##USER", FieldType.STRING, 8);
        cwfpda_P_Pnd_Pnd_Help_Nme = cwfpda_P.newFieldInGroup("cwfpda_P_Pnd_Pnd_Help_Nme", "##HELP-NME", FieldType.STRING, 8);
        cwfpda_P_Pnd_Pnd_Map_Nme = cwfpda_P.newFieldInGroup("cwfpda_P_Pnd_Pnd_Map_Nme", "##MAP-NME", FieldType.STRING, 8);
        cwfpda_P_Pnd_Pnd_Upd_User_Ind = cwfpda_P.newFieldInGroup("cwfpda_P_Pnd_Pnd_Upd_User_Ind", "##UPD-USER-IND", FieldType.STRING, 1);
        pnd_Call_Efm = localVariables.newFieldInRecord("pnd_Call_Efm", "#CALL-EFM", FieldType.BOOLEAN, 1);
        pnd_Date_A8 = localVariables.newFieldInRecord("pnd_Date_A8", "#DATE-A8", FieldType.STRING, 8);
        pnd_Date_Reformat1 = localVariables.newFieldInRecord("pnd_Date_Reformat1", "#DATE-REFORMAT1", FieldType.STRING, 10);
        pnd_Date_Reformat2 = localVariables.newFieldInRecord("pnd_Date_Reformat2", "#DATE-REFORMAT2", FieldType.STRING, 10);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 80);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Call_Efm.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn045() throws Exception
    {
        super("Iatn045");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120 EFSA902R CDAOBJ.OUTPUTS MSG-INFO-SUB CWFPDA-P
        pdaEfsa902r.getEfsa902r().reset();
        pdaCdaobj.getCdaobj_Outputs().reset();
        pdaIaapda_M.getMsg_Info_Sub().reset();
        cwfpda_P.reset();
        pdaNstpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue("IATN045");                                                                                         //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = 'IATN045'
        //* ***********************************************************************
        //*  >>>          START OF MAIN PROCESSING
        //*  >>> FIRST PAGE - MOVE THE IAIQ PARMS TO THE EFM CALL PARMS
        //* ***********************************************************************
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(ia_Unique_Id);                                                                                                         //Natural: ASSIGN EFSA9120.PIN-NBR := IA-UNIQUE-ID
        //*  TAITG OR TAITY
        //*  ADD NEW FOLDER DOCUMENT WITH NO MIT UPDATE
        pdaNstpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: MOVE *PROGRAM TO ERROR-INFO-SUB.##LAST-PROGRAM
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(xfr_Work_Prcss_Id);                                                                                           //Natural: ASSIGN EFSA9120.WPID ( 1 ) := XFR-WORK-PRCSS-ID
        pdaEfsa9120.getEfsa9120_Action().setValue("AD");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AD'
        pnd_Date_A8.setValueEdited(rqst_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                                                         //Natural: MOVE EDITED RQST-RCVD-DTE ( EM = YYYYMMDD ) TO #DATE-A8
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValueEdited(new ReportEditMask("99999999"),pnd_Date_A8);                                                   //Natural: MOVE EDITED #DATE-A8 TO EFSA9120.TIAA-RCVD-DTE ( 1 ) ( EM = 99999999 )
        pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue(0);                                                                                                    //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) := 0
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(rqst_Unit_Cde);                                                                                           //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := RQST-UNIT-CDE
        pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1).setValue(xfr_Mit_Log_Dte_Tme);                                                                             //Natural: ASSIGN EFSA9120.RQST-LOG-DTE-TME ( 1 ) := XFR-MIT-LOG-DTE-TME
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Gen_Fldr_Ind().getValue(1).reset();                                                                                                       //Natural: RESET EFSA9120.GEN-FLDR-IND ( 1 )
        pdaEfsa9120.getEfsa9120_Work_Rqst_Prty_Cde().getValue(1).setValue("0");                                                                                           //Natural: MOVE '0' TO EFSA9120.WORK-RQST-PRTY-CDE ( 1 ) EFSA9120.DOC-SECURITY-CDE
        pdaEfsa9120.getEfsa9120_Doc_Security_Cde().setValue("0");
        //*  UNIT-NME
        pdaEfsa9120.getEfsa9120_Mj_Pull_Ind().getValue(1).setValue("N");                                                                                                  //Natural: MOVE 'N' TO EFSA9120.MJ-PULL-IND ( 1 ) EFSA9120.CHECK-IND ( 1 ) EFSA9120.DOC-DIRECTION
        pdaEfsa9120.getEfsa9120_Check_Ind().getValue(1).setValue("N");
        pdaEfsa9120.getEfsa9120_Doc_Direction().setValue("N");
        //*  EFSA9120.RQST-ENTRY-OP-CDE := RQST-RCVD-USER-ID
        //*  CHANGED ABOVE TO FOLLOWING   10/99
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(rqst_Entry_User_Id);                                                                                         //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := RQST-ENTRY-USER-ID
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("M");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'M'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(rqst_Unit_Cde);                                                                                           //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := RQST-UNIT-CDE
        //*  INTERNAL PROCESSING
        pdaEfsa9120.getEfsa9120_Doc_Category().setValue("I");                                                                                                             //Natural: MOVE 'I' TO EFSA9120.DOC-CATEGORY
        pdaEfsa9120.getEfsa9120_Doc_Class().setValue("AUD");                                                                                                              //Natural: MOVE 'AUD' TO EFSA9120.DOC-CLASS
        pdaEfsa9120.getEfsa9120_Doc_Specific().setValue("   ");                                                                                                           //Natural: MOVE '   ' TO EFSA9120.DOC-SPECIFIC
        pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                           //Natural: MOVE 'T' TO EFSA9120.DOC-FORMAT-CDE
        pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue("P");                                                                                                        //Natural: MOVE 'P' TO EFSA9120.DOC-RETENTION-CDE
        pdaEfsa9120.getEfsa9120_Object_Nbr().setValue(11);                                                                                                                //Natural: MOVE 11 TO EFSA9120.OBJECT-NBR
        pdaEfsa9120.getEfsa9120_Mj_Pull_Ind().getValue(1).setValue("N");                                                                                                  //Natural: MOVE 'N' TO EFSA9120.MJ-PULL-IND ( 1 )
        pdaEfsa9120.getEfsa9120_System().setValue("IAIQ");                                                                                                                //Natural: MOVE 'IAIQ' TO EFSA9120.SYSTEM
        pdaEfsa9120.getEfsa9120_Contact_Date_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                               //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO EFSA9120.CONTACT-DATE-TIME
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue("*").reset();                                                                                                         //Natural: RESET EFSA9120.DOC-TEXT ( * )
        //*  ===================================
        //*  >>> START OF FORMAT OUTPUT SCREEN
        //*  ===================================
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1).setValue("                   CONFIRMATION AUDIT TRAIL");                                                           //Natural: ASSIGN EFSA9120.DOC-TEXT ( 1 ) := '                   CONFIRMATION AUDIT TRAIL'
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(3).setValue("   IMMEDIATE ANNUITY ADMINISTRATIVE TRANSFER CONFIRMATION AUDIT TRAIL");                                 //Natural: ASSIGN EFSA9120.DOC-TEXT ( 3 ) := '   IMMEDIATE ANNUITY ADMINISTRATIVE TRANSFER CONFIRMATION AUDIT TRAIL'
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(5).setValue("   Electronic image of the Confirmation Letter");                                                        //Natural: ASSIGN EFSA9120.DOC-TEXT ( 5 ) := '   Electronic image of the Confirmation Letter'
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(7).setValue("   _______________________________________________________________");                                    //Natural: ASSIGN EFSA9120.DOC-TEXT ( 7 ) := '   _______________________________________________________________'
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(8).setValue("   DATA ENTRY INFORMATION");                                                                             //Natural: ASSIGN EFSA9120.DOC-TEXT ( 8 ) := '   DATA ENTRY INFORMATION'
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(9).setValue("   _______________________________________________________________");                                    //Natural: ASSIGN EFSA9120.DOC-TEXT ( 9 ) := '   _______________________________________________________________'
        pnd_Date_Reformat1.setValueEdited(rqst_Entry_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                               //Natural: MOVE EDITED RQST-ENTRY-DTE ( EM = MM/DD/YYYY ) TO #DATE-REFORMAT1
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(12).setValue(DbsUtil.compress("   REQUEST FIRST ENTERED ON.........: ", pnd_Date_Reformat1));                         //Natural: COMPRESS '   REQUEST FIRST ENTERED ON.........: ' #DATE-REFORMAT1 INTO EFSA9120.DOC-TEXT ( 12 )
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(13).setValue(DbsUtil.compress("   REQUEST FIRST ENTERED BY.........: ", rqst_Entry_User_Id));                         //Natural: COMPRESS '   REQUEST FIRST ENTERED BY.........: ' RQST-ENTRY-USER-ID INTO EFSA9120.DOC-TEXT ( 13 )
        pnd_Date_Reformat1.setValueEdited(rqst_Rcvd_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                                //Natural: MOVE EDITED RQST-RCVD-DTE ( EM = MM/DD/YYYY ) TO #DATE-REFORMAT1
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(14).setValue(DbsUtil.compress("   REQUEST LAST UPDATED ON..........: ", pnd_Date_Reformat1));                         //Natural: COMPRESS '   REQUEST LAST UPDATED ON..........: ' #DATE-REFORMAT1 INTO EFSA9120.DOC-TEXT ( 14 )
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(15).setValue(DbsUtil.compress("   REQUEST UPDATED BY...............: ", rqst_Rcvd_User_Id));                          //Natural: COMPRESS '   REQUEST UPDATED BY...............: ' RQST-RCVD-USER-ID INTO EFSA9120.DOC-TEXT ( 15 )
        //*  TAITG OR TAITY
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(18).setValue(DbsUtil.compress("INBOUND WPID: ", xfr_Work_Prcss_Id));                                                  //Natural: COMPRESS 'INBOUND WPID: ' XFR-WORK-PRCSS-ID INTO EFSA9120.DOC-TEXT ( 18 )
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(20).setValue("                     ********** End of Data **********");                                               //Natural: ASSIGN EFSA9120.DOC-TEXT ( 20 ) := '                     ********** End of Data **********'
                                                                                                                                                                          //Natural: PERFORM EFM-CALL
        sub_Efm_Call();
        if (condition(Global.isEscape())) {return;}
        //* **************************************************
        //*  CALL ELECTRONIC FOLDER MODULE
        //* **************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EFM-CALL
    }
    private void sub_Efm_Call() throws Exception                                                                                                                          //Natural: EFM-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* **
        pdaCdaobj.getCdaobj_Outputs().reset();                                                                                                                            //Natural: RESET CDAOBJ.OUTPUTS MSG-INFO-SUB CWFPDA-P
        pdaIaapda_M.getMsg_Info_Sub().reset();
        cwfpda_P.reset();
        pdaNstpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue("EFSN9120");                                                                                        //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = 'EFSN9120'
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB CWFPDA-P
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaIaapda_M.getMsg_Info_Sub(), cwfpda_P);
        if (condition(Global.isEscape())) return;
        if (condition(pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero()) || pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))               //Natural: IF MSG-INFO-SUB.##MSG-NR GT 0 OR MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            getReports().write(0, "*******************************************************");                                                                             //Natural: WRITE '*******************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "Following messages produced by Progam-->",Global.getPROGRAM());                                                                        //Natural: WRITE 'Following messages produced by Progam-->' *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "Error Returned From Called EFM Module EFSN9120 Processing continued");                                                                 //Natural: WRITE 'Error Returned From Called EFM Module EFSN9120 Processing continued'
            if (Global.isEscape()) return;
            getReports().write(0, "** Error Msg Number Returned From Called PROGRAMS-->",pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr());                                   //Natural: WRITE '** Error Msg Number Returned From Called PROGRAMS-->' MSG-INFO-SUB.##MSG-NR
            if (Global.isEscape()) return;
            getReports().write(0, "** Error Message    Returned From Called PROGRAMS-->",pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                      //Natural: WRITE '** Error Message    Returned From Called PROGRAMS-->' MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            iatn402_Msg.setValue(pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                              //Natural: ASSIGN IATN402-MSG := MSG-INFO-SUB.##MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue("*").reset();                                                                                                         //Natural: RESET EFSA9120.DOC-TEXT ( * )
    }

    //
}
