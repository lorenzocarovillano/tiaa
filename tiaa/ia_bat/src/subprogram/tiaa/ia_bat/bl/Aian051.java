/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:57:45 PM
**        * FROM NATURAL SUBPROGRAM : Aian051
************************************************************
**        * FILE NAME            : Aian051.java
**        * CLASS NAME           : Aian051
**        * INSTANCE NAME        : Aian051
************************************************************
***** AIAN051 - VERSION 2.0 - 9/17/03   CLONE OF AIAN022
*****************************************************************
* MODIFIED  BY           DESCRIPTION OF CHANGES
*
* 08/18/09 SMOLYAN CHANGED CALCULATION OF DEFERRAL PERIOD FOR SPIA
* CONTRACTS - REMOVED "substract 1 day" STATEMENT.
*
* 02/09/09  SMOLYAN  ADDED CALLS TO AIAN101 TO CONVERT CALL TYPES
* INTO IA RATE MANAGER FUND CODE
*
* 03/19/2008 VE  EXPANDED RATE ARRAY FROM 80 TO 99
*
* 10/04/01  JS   ADDED DA-RATE OF 88 AND IA-RATE OF 27
*                   IN CHOOSE-MORTALITY-TABLE ROUTINE
*
* 09/17/03  JS   GRADED CHANGES:
*                   IF THE #AIAN027-GUARA-INTEREST-RATE FOR
*                   NAZ-ACT-TIAA-GRADED-RATE-CDE(X) IS LESS THAN 4%,
*                   ADD NAZ-ACT-TIAA-GRADED-RATE-AMT(X) TO
*                   NAZ-ACT-TIAA-STD-RATE-AMT(X) AND REMOVE FROM
*                   NAZ-ACT-TIAA-GRADED-RATE-AMT(X).
*
* 09/18/03  JS   MORTALITY-TABLE CHANGES:
*                   IN CHOOSE-MORTALITY-TABLE, CODE WAS REPLACED
*                   WITH A CALL TO AIAN090.
*
* 12/01/03  JS   RATE EXPANSION:
*                   ALL OCCURRENCES (1:60) CHANGED TO (1:80).
*                   NAZZ510 CHANGED BACK TO NAZL510.
*                   REPLACED '1 AIAN027-LINKAGE' WITH
*                     LOCAL USING AIAA027R.
*                   CHANGED RB REFERENCES IN TPA-KEY TO ALPHA.
*                     LINKAGE TO ALPHA.
*                   CHANGED VIEW FOR TPA RATE FILE
*                   REMOVED RB REDEFINITIONS TO NUMERIC
*                   CHANGED CALLNAT FOR AIAN027 TO AIAN027R
*                   CHANGED LOGIC IN CHOOSE-MORTALITY-TABLE ROUTINE
*
* 11/21/05  DY   RE-STOW TO IMPLEMENT - NEW NAZL510 LINKAGE
*
* 09/16/2009 V. ENG
* - ADDED ACCESS CODE TO STATE-OF-FLORIDA-COMPLIANCE-CHECK ROUTINE
* - RESET MORTALITY ASSUMPTIONS AFTER EXECUTING ROUTINE
*   STATE-OF-FLORIDA-COMPLIANCE-CHECK
*
* 04/08/2010 H. LONDON
* - ADDED CODE TO HANDLE SVSAA - CALL IAAN0019 TO GET RATE MGR INFO
*                              - CALL AIAN031 TO GET FUND CODE
*                              - CALL AIAN102  TO GET IA RATE MANAGER
*                              -   PRODUCT CODE
*                              - NEW RATE MGR STRUCTURE
*
* 01/25/2011 V. ENG
* - CORRECTED THE ANNUITY FACTOR FOR SPIA ORIGIN SETTLEMENT.
*   PREVIOUSLY, IT WAS ADDING A 1 TO THE FACTOR IF THE ISSUE DAY IS
*   GREATER THAN 1.
* 06/13/2016 D. YHUN | NON-PENSIONS WILL BE USING IRS TABLES TO
*                      GET LIFE EXPECTANCY VIA AIAN033; REF CODE
*                      CHANGE AS LE01
* 06-23-16   DY  CORRECT 1ST PYMT DATE FOR SPIA WHEN PREMIUM IS REC'd
*                  ON & PRIOR TO THE 20TH OF EACH MTH; ISSUE DATE IS
*                  THE DAY AFTER PREMIUM REC'd date; ref chg DY1
* 12-08-16   DY  CORRECT FINAL PREMIUM SETTLEMENT & INTEREST RATE; MUST
*                  USE FINAL PREMIUM DATE INSTEAD OF ISSUE DATE.
*                  REF CHG DY2
******************************************************************
* THE 'S' CALL TYPE IS FOR SPIA AND 'M' IS FOR OTHER NON-PENSION
* SETTLEMENTS.
* ****************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aian051 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAiaa510 pdaAiaa510;
    private PdaAial0210 pdaAial0210;
    private LdaAial770r ldaAial770r;
    private LdaAial770d ldaAial770d;
    private PdaAiaa074 pdaAiaa074;
    private PdaAiaa027r pdaAiaa027r;
    private PdaIaaa0019 pdaIaaa0019;
    private PdaAial102 pdaAial102;
    private PdaAiaa026 pdaAiaa026;
    private PdaAiaa028 pdaAiaa028;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Aian021_Linkage_Array;
    private DbsField pnd_Ann_Factor;
    private DbsField pnd_Ws_Tpa_Ind;
    private DbsField pnd_Number_Of_Rate_Basis;
    private DbsField pnd_Additional_Irr_Payment;
    private DbsField pnd_Irr_Payment_Sum;
    private DbsField pnd_Ia_Fund_Code;
    private DbsField pnd_Mortality_Rate_Code;
    private DbsField pnd_Rate_File_Date;

    private DbsGroup pnd_Rate_File_Date__R_Field_1;
    private DbsField pnd_Rate_File_Date_Pnd_Rate_File_Date_Without_Day;
    private DbsField pnd_Rate_File_Date_Pnd_Rate_File_Day;
    private DbsField pnd_Certain_Period;
    private DbsField pnd_Final_Certain_Period;
    private DbsField pnd_Ws_Guar_Period;
    private DbsField pnd_Ws_Guar_Period_Mth;
    private DbsField pnd_Aian021_Linkage_X;

    private DbsGroup pnd_Aian021_Linkage_X__R_Field_2;
    private DbsField pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Input;
    private DbsField pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Output;
    private DbsField pnd_New_Ann_Factor;
    private DbsField pnd_Hold_Annuity_Factor;
    private DbsField pnd_Found_Linkage;
    private DbsField pnd_Link_Array_Index;
    private DbsField pnd_Next_Ia_Payment_Date;

    private DbsGroup pnd_Next_Ia_Payment_Date__R_Field_3;
    private DbsField pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Year;
    private DbsField pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Month;
    private DbsField pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Day;

    private DbsGroup pnd_Next_Ia_Payment_Date__R_Field_4;
    private DbsField pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Date_A;
    private DbsField pnd_Last_Payment_Date;

    private DbsGroup pnd_Last_Payment_Date__R_Field_5;
    private DbsField pnd_Last_Payment_Date_Pnd_Last_Payment_Year;
    private DbsField pnd_Last_Payment_Date_Pnd_Last_Payment_Month;
    private DbsField pnd_Last_Payment_Date_Pnd_Last_Payment_Day;

    private DbsGroup pnd_Last_Payment_Date__R_Field_6;
    private DbsField pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A;
    private DbsField pnd_Ipro_Final_Date;

    private DbsGroup pnd_Ipro_Final_Date__R_Field_7;
    private DbsField pnd_Ipro_Final_Date_Pnd_Ipro_Final_Year;
    private DbsField pnd_Ipro_Final_Date_Pnd_Ipro_Final_Month;
    private DbsField pnd_Ipro_Final_Date_Pnd_Ipro_Final_Day;

    private DbsGroup pnd_Ipro_Final_Date__R_Field_8;
    private DbsField pnd_Ipro_Final_Date_Pnd_Ipro_Final_Date_A;
    private DbsField pnd_Final_Payment_Date;

    private DbsGroup pnd_Final_Payment_Date__R_Field_9;
    private DbsField pnd_Final_Payment_Date_Pnd_Final_Payment_Year;
    private DbsField pnd_Final_Payment_Date_Pnd_Final_Payment_Month;
    private DbsField pnd_Final_Payment_Date_Pnd_Final_Payment_Day;

    private DbsGroup pnd_Final_Payment_Date__R_Field_10;
    private DbsField pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A;
    private DbsField pnd_Integer_Periods;
    private DbsField pnd_Last_Payment_Period;
    private DbsField pnd_J;
    private DbsField pnd_I;
    private DbsField pnd_V;
    private DbsField pnd_V_Exponent;
    private DbsField pnd_Full_Years;
    private DbsField pnd_Months_Between_Payments;
    private DbsField pnd_Additional_Months;
    private DbsField pnd_Remaining_Payments_In_Year;
    private DbsField pnd_Num_Of_Payments;
    private DbsField pnd_Install_Ref_Cert_Per;
    private DbsField pnd_Annualized_Annuity_Factor;
    private DbsField pnd_Accumulation_Left;
    private DbsField pnd_Inst_Ref_Ratio;
    private DbsField pnd_Irr_Payment;
    private DbsField pnd_Number_Of_Months;
    private DbsField pnd_Number_Of_Years;
    private DbsField pnd_Num_Of_Payments_In_Guar_Per;
    private DbsField pnd_Type_Of_Call;
    private DbsField pnd_Tiaa_Total;
    private DbsField pnd_Tiaa_Guaranteed;
    private DbsField pnd_Tiaa_Dividend;
    private DbsField pnd_Tiaa_Guar_Periods;
    private DbsField pnd_Tiaa_Rate_Amt;
    private DbsField pnd_Tiaa_Rate_Cde;
    private DbsField pnd_Tiaa_Ia_Rate_Cde;
    private DbsField pnd_Ia_Rate_Cde;
    private DbsField pnd_Cref_Rate_Amt;
    private DbsField pnd_Cref_Rate_Cde;
    private DbsField pnd_Cref_Estimated_Amount;
    private DbsField pnd_Cref_Payout_Units;
    private DbsField pnd_Cref_Payout_Amount;
    private DbsField pnd_Cref_Rate;
    private DbsField pnd_Modal_Adjustment;
    private DbsField pnd_Modal_Numerator;
    private DbsField pnd_Modal_Denominator;

    private DbsGroup pnd_Sum_Amounts;
    private DbsField pnd_Sum_Amounts_Pnd_Tiaa_Sum_Rate_Amt;
    private DbsField pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guaranteed;
    private DbsField pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guar_Periods;
    private DbsField pnd_1st_Ann_Sex_A;

    private DbsGroup pnd_1st_Ann_Sex_A__R_Field_11;
    private DbsField pnd_1st_Ann_Sex_A_Pnd_1st_Ann_Sex_N;
    private DbsField pnd_2nd_Ann_Sex_A;

    private DbsGroup pnd_2nd_Ann_Sex_A__R_Field_12;
    private DbsField pnd_2nd_Ann_Sex_A_Pnd_2nd_Ann_Sex_N;
    private DbsField pnd_Mode;

    private DbsGroup pnd_Mode__R_Field_13;
    private DbsField pnd_Mode_Pnd_Mode_1;
    private DbsField pnd_Mode_Pnd_Mode_2;
    private DbsField pnd_Check_Date_A;

    private DbsGroup pnd_Check_Date_A__R_Field_14;
    private DbsField pnd_Check_Date_A_Pnd_Check_Date_N;

    private DbsGroup pnd_Check_Date_A__R_Field_15;
    private DbsField pnd_Check_Date_A_Pnd_Check_Year_N;
    private DbsField pnd_Check_Date_A_Pnd_Check_Month_N;
    private DbsField pnd_Check_Date_A_Pnd_Check_Day_N;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_16;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N8;

    private DbsGroup pnd_Issue_Date_A__R_Field_17;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Day;

    private DbsGroup pnd_Issue_Date_A__R_Field_18;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Year_N;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Month_N;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Day_N;
    private DbsField pnd_Spia_Start_Date_A;

    private DbsGroup pnd_Spia_Start_Date_A__R_Field_19;
    private DbsField pnd_Spia_Start_Date_A_Pnd_Spia_Start_Date_N8;

    private DbsGroup pnd_Spia_Start_Date_A__R_Field_20;
    private DbsField pnd_Spia_Start_Date_A_Pnd_Spia_Start_Date_N;
    private DbsField pnd_Spia_Start_Date_A_Pnd_Spia_Start_Day;

    private DbsGroup pnd_Spia_Start_Date_A__R_Field_21;
    private DbsField pnd_Spia_Start_Date_A_Pnd_Spia_Start_Year_N;
    private DbsField pnd_Spia_Start_Date_A_Pnd_Spia_Start_Month_N;
    private DbsField pnd_Spia_Start_Date_A_Pnd_Spia_Start_Day_N;
    private DbsField pnd_Eff_Date_A;

    private DbsGroup pnd_Eff_Date_A__R_Field_22;
    private DbsField pnd_Eff_Date_A_Pnd_Eff_Date_N8;

    private DbsGroup pnd_Eff_Date_A__R_Field_23;
    private DbsField pnd_Eff_Date_A_Pnd_Eff_Date_N;
    private DbsField pnd_Eff_Date_A_Eff_Date_Day;

    private DbsGroup pnd_Eff_Date_A__R_Field_24;
    private DbsField pnd_Eff_Date_A_Pnd_Eff_Year_N;
    private DbsField pnd_Eff_Date_A_Pnd_Eff_Month_N;
    private DbsField pnd_Eff_Date_A_Pnd_Eff_Day_N;
    private DbsField pnd_1st_Ann_Birth_Date_A;

    private DbsGroup pnd_1st_Ann_Birth_Date_A__R_Field_25;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N;

    private DbsGroup pnd_1st_Ann_Birth_Date_A__R_Field_26;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year_N;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month_N;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day_N;
    private DbsField pnd_2nd_Ann_Birth_Date_A;

    private DbsGroup pnd_2nd_Ann_Birth_Date_A__R_Field_27;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N;

    private DbsGroup pnd_2nd_Ann_Birth_Date_A__R_Field_28;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year_N;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month_N;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day_N;
    private DbsField pnd_Ws_Date;

    private DbsGroup pnd_Ws_Date__R_Field_29;
    private DbsField pnd_Ws_Date_Pnd_Ws_Date_A;

    private DbsGroup pnd_Ws_Date__R_Field_30;
    private DbsField pnd_Ws_Date_Pnd_Ws_Year;
    private DbsField pnd_Ws_Date_Pnd_Ws_Month;
    private DbsField pnd_Ws_Date_Pnd_Ws_Day;
    private DbsField pnd_Pmt_Switch;
    private DbsField pnd_Pmt_Month_1;
    private DbsField pnd_Pmt_Month_2;
    private DbsField pnd_Pmt_Month_3;
    private DbsField pnd_Pmt_Month_4;
    private DbsField pnd_Tpa_Rate_Key;

    private DbsGroup pnd_Tpa_Rate_Key__R_Field_31;
    private DbsField pnd_Tpa_Rate_Key_Pnd_Tpa_Key_Date;
    private DbsField pnd_Tpa_Rate_Key_Pnd_Tpa_Rate_Basis;
    private DbsField pnd_Cutoff_Date;

    private DbsGroup pnd_Cutoff_Date__R_Field_32;
    private DbsField pnd_Cutoff_Date_Pnd_Cutoff_Month;
    private DbsField pnd_Cutoff_Date_Pnd_Cutoff_Day;
    private DbsField pnd_Ws_Ipro_Rate;
    private DbsField pnd_Mthly_Int_Rate;
    private DbsField pnd_Ws_Pa_Expected_Life;
    private DbsField pnd_Pa_Type_Of_Call;

    private DbsGroup pnd_Aian030_Linkage;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_D;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_End_Date_D;
    private DbsField pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days;

    private DbsGroup pnd_Aian031_Linkage;
    private DbsField pnd_Aian031_Linkage_Pnd_Aian031_In_Rate_Basis;
    private DbsField pnd_Aian031_Linkage_Pnd_Aian031_Out_Rate_Code;
    private DbsField pnd_Aian031_Linkage_Pnd_Aian031_Return_Code;
    private DbsField pnd_Ws_Asd_Date_A;

    private DbsGroup pnd_Ws_Asd_Date_A__R_Field_33;
    private DbsField pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date;
    private DbsField pnd_Ws_Effective_Date_A;

    private DbsGroup pnd_Ws_Effective_Date_A__R_Field_34;
    private DbsField pnd_Ws_Effective_Date_A_Pnd_Ws_Effective_Date;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Ws_Da_Rate_A;
    private DbsField pnd_Ws_Move_Grd_To_Std;

    private DbsGroup pnd_Ws_Graded_Info;
    private DbsField pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Code;
    private DbsField pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Amt;

    private DbsGroup pnd_Aian090_Linkage;
    private DbsField pnd_Aian090_Linkage_Pnd_Aian090_Call_Type;
    private DbsField pnd_Aian090_Linkage_Pnd_Aian090_In_Rate;
    private DbsField pnd_Aian090_Linkage_Pnd_Aian090_Eff_Date;
    private DbsField pnd_Aian090_Linkage_Pnd_Aian090_Out_Rate;
    private DbsField pnd_Hold_Mortality_Table;
    private DbsField pnd_Hold_Interest_Rate;
    private DbsField pnd_Hold_Def_Interest_Rate;
    private DbsField pnd_Hold_Setback;
    private DbsField pnd_Hold_Date1;
    private DbsField pnd_Hold_Date2;
    private DbsField pnd_Hold_Calc;

    private DbsGroup pnd_P_And_I_Calc_Fields;
    private DbsField pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Guar_Rate;
    private DbsField pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum;
    private DbsField pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Guar_Payment;
    private DbsField pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Total_Rate;
    private DbsField pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Tot_Payment;
    private DbsField pnd_Ws_Aian021_Type;

    private DbsGroup collapse_Array;
    private DbsField collapse_Array_Pos_I;
    private DbsField collapse_Array_Grd_Pos;
    private DbsField collapse_Array_Grd_Cnt;
    private DbsField collapse_Array_Collapse_Grd_Ia_Rb;
    private DbsField collapse_Array_Collapse_Grd_Gic;
    private DbsField collapse_Array_Collapse_Grd_Total_Acc;
    private DbsField collapse_Array_Collapse_Grd_Eff_Rate_Intc;

    private DbsGroup collapse_Array_Collapse_Grd_Array;
    private DbsField collapse_Array_Collapse_Grd_I;
    private DbsField collapse_Array_Collapse_Grd_Rb;
    private DbsField collapse_Array_Collapse_Grd_Gic_Code;
    private DbsField collapse_Array_Collapse_Grd_Acc;
    private DbsField collapse_Array_Collapse_Grd_Eff_Rate_Int;
    private DbsField collapse_Array_Std_Pos;
    private DbsField collapse_Array_Std_Cnt;
    private DbsField collapse_Array_Collapse_Std_Ia_Rb;
    private DbsField collapse_Array_Collapse_Std_Gic;
    private DbsField collapse_Array_Collapse_Std_Total_Acc;
    private DbsField collapse_Array_Collapse_Std_Eff_Rate_Intc;

    private DbsGroup collapse_Array_Collapse_Std_Array;
    private DbsField collapse_Array_Collapse_Std_I;
    private DbsField collapse_Array_Collapse_Std_Rb;
    private DbsField collapse_Array_Collapse_Std_Gic_Code;
    private DbsField collapse_Array_Collapse_Std_Acc;
    private DbsField collapse_Array_Collapse_Std_Eff_Rate_Int;
    private DbsField pnd_Hold_Rate_Code;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAial0210 = new PdaAial0210(localVariables);
        ldaAial770r = new LdaAial770r();
        registerRecord(ldaAial770r);
        registerRecord(ldaAial770r.getVw_act_Tpa_Rate_File_Rate_View());
        ldaAial770d = new LdaAial770d();
        registerRecord(ldaAial770d);
        registerRecord(ldaAial770d.getVw_act_Tpa_Rate_File_Date_View());
        pdaAiaa074 = new PdaAiaa074(localVariables);
        pdaAiaa027r = new PdaAiaa027r(localVariables);
        pdaIaaa0019 = new PdaIaaa0019(localVariables);
        pdaAial102 = new PdaAial102(localVariables);
        pdaAiaa026 = new PdaAiaa026(localVariables);
        pdaAiaa028 = new PdaAiaa028(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAiaa510 = new PdaAiaa510(parameters);
        pnd_Aian021_Linkage_Array = parameters.newFieldArrayInRecord("pnd_Aian021_Linkage_Array", "#AIAN021-LINKAGE-ARRAY", FieldType.STRING, 69, new 
            DbsArrayController(1, 250));
        pnd_Aian021_Linkage_Array.setParameterOption(ParameterOption.ByReference);
        pnd_Ann_Factor = parameters.newFieldArrayInRecord("pnd_Ann_Factor", "#ANN-FACTOR", FieldType.NUMERIC, 8, 5, new DbsArrayController(1, 250));
        pnd_Ann_Factor.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Tpa_Ind = parameters.newFieldInRecord("pnd_Ws_Tpa_Ind", "#WS-TPA-IND", FieldType.STRING, 1);
        pnd_Ws_Tpa_Ind.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Number_Of_Rate_Basis = localVariables.newFieldInRecord("pnd_Number_Of_Rate_Basis", "#NUMBER-OF-RATE-BASIS", FieldType.NUMERIC, 3);
        pnd_Additional_Irr_Payment = localVariables.newFieldInRecord("pnd_Additional_Irr_Payment", "#ADDITIONAL-IRR-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Irr_Payment_Sum = localVariables.newFieldInRecord("pnd_Irr_Payment_Sum", "#IRR-PAYMENT-SUM", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ia_Fund_Code = localVariables.newFieldInRecord("pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        pnd_Mortality_Rate_Code = localVariables.newFieldInRecord("pnd_Mortality_Rate_Code", "#MORTALITY-RATE-CODE", FieldType.NUMERIC, 2);
        pnd_Rate_File_Date = localVariables.newFieldInRecord("pnd_Rate_File_Date", "#RATE-FILE-DATE", FieldType.NUMERIC, 8);

        pnd_Rate_File_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Rate_File_Date__R_Field_1", "REDEFINE", pnd_Rate_File_Date);
        pnd_Rate_File_Date_Pnd_Rate_File_Date_Without_Day = pnd_Rate_File_Date__R_Field_1.newFieldInGroup("pnd_Rate_File_Date_Pnd_Rate_File_Date_Without_Day", 
            "#RATE-FILE-DATE-WITHOUT-DAY", FieldType.NUMERIC, 6);
        pnd_Rate_File_Date_Pnd_Rate_File_Day = pnd_Rate_File_Date__R_Field_1.newFieldInGroup("pnd_Rate_File_Date_Pnd_Rate_File_Day", "#RATE-FILE-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Certain_Period = localVariables.newFieldInRecord("pnd_Certain_Period", "#CERTAIN-PERIOD", FieldType.NUMERIC, 7, 5);
        pnd_Final_Certain_Period = localVariables.newFieldInRecord("pnd_Final_Certain_Period", "#FINAL-CERTAIN-PERIOD", FieldType.NUMERIC, 7, 5);
        pnd_Ws_Guar_Period = localVariables.newFieldInRecord("pnd_Ws_Guar_Period", "#WS-GUAR-PERIOD", FieldType.NUMERIC, 2);
        pnd_Ws_Guar_Period_Mth = localVariables.newFieldInRecord("pnd_Ws_Guar_Period_Mth", "#WS-GUAR-PERIOD-MTH", FieldType.NUMERIC, 2);
        pnd_Aian021_Linkage_X = localVariables.newFieldInRecord("pnd_Aian021_Linkage_X", "#AIAN021-LINKAGE-X", FieldType.STRING, 110);

        pnd_Aian021_Linkage_X__R_Field_2 = localVariables.newGroupInRecord("pnd_Aian021_Linkage_X__R_Field_2", "REDEFINE", pnd_Aian021_Linkage_X);
        pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Input = pnd_Aian021_Linkage_X__R_Field_2.newFieldInGroup("pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Input", 
            "#AIAN021-LINKAGE-INPUT", FieldType.STRING, 69);
        pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Output = pnd_Aian021_Linkage_X__R_Field_2.newFieldInGroup("pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Output", 
            "#AIAN021-LINKAGE-OUTPUT", FieldType.STRING, 41);
        pnd_New_Ann_Factor = localVariables.newFieldInRecord("pnd_New_Ann_Factor", "#NEW-ANN-FACTOR", FieldType.NUMERIC, 8, 5);
        pnd_Hold_Annuity_Factor = localVariables.newFieldInRecord("pnd_Hold_Annuity_Factor", "#HOLD-ANNUITY-FACTOR", FieldType.NUMERIC, 8, 5);
        pnd_Found_Linkage = localVariables.newFieldInRecord("pnd_Found_Linkage", "#FOUND-LINKAGE", FieldType.STRING, 1);
        pnd_Link_Array_Index = localVariables.newFieldInRecord("pnd_Link_Array_Index", "#LINK-ARRAY-INDEX", FieldType.NUMERIC, 3);
        pnd_Next_Ia_Payment_Date = localVariables.newFieldInRecord("pnd_Next_Ia_Payment_Date", "#NEXT-IA-PAYMENT-DATE", FieldType.NUMERIC, 8);

        pnd_Next_Ia_Payment_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Next_Ia_Payment_Date__R_Field_3", "REDEFINE", pnd_Next_Ia_Payment_Date);
        pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Year = pnd_Next_Ia_Payment_Date__R_Field_3.newFieldInGroup("pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Year", 
            "#NEXT-IA-PAYMENT-YEAR", FieldType.NUMERIC, 4);
        pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Month = pnd_Next_Ia_Payment_Date__R_Field_3.newFieldInGroup("pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Month", 
            "#NEXT-IA-PAYMENT-MONTH", FieldType.NUMERIC, 2);
        pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Day = pnd_Next_Ia_Payment_Date__R_Field_3.newFieldInGroup("pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Day", 
            "#NEXT-IA-PAYMENT-DAY", FieldType.NUMERIC, 2);

        pnd_Next_Ia_Payment_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Next_Ia_Payment_Date__R_Field_4", "REDEFINE", pnd_Next_Ia_Payment_Date);
        pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Date_A = pnd_Next_Ia_Payment_Date__R_Field_4.newFieldInGroup("pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Date_A", 
            "#NEXT-IA-PAYMENT-DATE-A", FieldType.STRING, 8);
        pnd_Last_Payment_Date = localVariables.newFieldInRecord("pnd_Last_Payment_Date", "#LAST-PAYMENT-DATE", FieldType.NUMERIC, 8);

        pnd_Last_Payment_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Last_Payment_Date__R_Field_5", "REDEFINE", pnd_Last_Payment_Date);
        pnd_Last_Payment_Date_Pnd_Last_Payment_Year = pnd_Last_Payment_Date__R_Field_5.newFieldInGroup("pnd_Last_Payment_Date_Pnd_Last_Payment_Year", 
            "#LAST-PAYMENT-YEAR", FieldType.NUMERIC, 4);
        pnd_Last_Payment_Date_Pnd_Last_Payment_Month = pnd_Last_Payment_Date__R_Field_5.newFieldInGroup("pnd_Last_Payment_Date_Pnd_Last_Payment_Month", 
            "#LAST-PAYMENT-MONTH", FieldType.NUMERIC, 2);
        pnd_Last_Payment_Date_Pnd_Last_Payment_Day = pnd_Last_Payment_Date__R_Field_5.newFieldInGroup("pnd_Last_Payment_Date_Pnd_Last_Payment_Day", "#LAST-PAYMENT-DAY", 
            FieldType.NUMERIC, 2);

        pnd_Last_Payment_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Last_Payment_Date__R_Field_6", "REDEFINE", pnd_Last_Payment_Date);
        pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A = pnd_Last_Payment_Date__R_Field_6.newFieldInGroup("pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A", 
            "#LAST-PAYMENT-DATE-A", FieldType.STRING, 8);
        pnd_Ipro_Final_Date = localVariables.newFieldInRecord("pnd_Ipro_Final_Date", "#IPRO-FINAL-DATE", FieldType.NUMERIC, 8);

        pnd_Ipro_Final_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Ipro_Final_Date__R_Field_7", "REDEFINE", pnd_Ipro_Final_Date);
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Year = pnd_Ipro_Final_Date__R_Field_7.newFieldInGroup("pnd_Ipro_Final_Date_Pnd_Ipro_Final_Year", "#IPRO-FINAL-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Month = pnd_Ipro_Final_Date__R_Field_7.newFieldInGroup("pnd_Ipro_Final_Date_Pnd_Ipro_Final_Month", "#IPRO-FINAL-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Day = pnd_Ipro_Final_Date__R_Field_7.newFieldInGroup("pnd_Ipro_Final_Date_Pnd_Ipro_Final_Day", "#IPRO-FINAL-DAY", 
            FieldType.NUMERIC, 2);

        pnd_Ipro_Final_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Ipro_Final_Date__R_Field_8", "REDEFINE", pnd_Ipro_Final_Date);
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Date_A = pnd_Ipro_Final_Date__R_Field_8.newFieldInGroup("pnd_Ipro_Final_Date_Pnd_Ipro_Final_Date_A", "#IPRO-FINAL-DATE-A", 
            FieldType.STRING, 8);
        pnd_Final_Payment_Date = localVariables.newFieldInRecord("pnd_Final_Payment_Date", "#FINAL-PAYMENT-DATE", FieldType.NUMERIC, 8);

        pnd_Final_Payment_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Final_Payment_Date__R_Field_9", "REDEFINE", pnd_Final_Payment_Date);
        pnd_Final_Payment_Date_Pnd_Final_Payment_Year = pnd_Final_Payment_Date__R_Field_9.newFieldInGroup("pnd_Final_Payment_Date_Pnd_Final_Payment_Year", 
            "#FINAL-PAYMENT-YEAR", FieldType.NUMERIC, 4);
        pnd_Final_Payment_Date_Pnd_Final_Payment_Month = pnd_Final_Payment_Date__R_Field_9.newFieldInGroup("pnd_Final_Payment_Date_Pnd_Final_Payment_Month", 
            "#FINAL-PAYMENT-MONTH", FieldType.NUMERIC, 2);
        pnd_Final_Payment_Date_Pnd_Final_Payment_Day = pnd_Final_Payment_Date__R_Field_9.newFieldInGroup("pnd_Final_Payment_Date_Pnd_Final_Payment_Day", 
            "#FINAL-PAYMENT-DAY", FieldType.NUMERIC, 2);

        pnd_Final_Payment_Date__R_Field_10 = localVariables.newGroupInRecord("pnd_Final_Payment_Date__R_Field_10", "REDEFINE", pnd_Final_Payment_Date);
        pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A = pnd_Final_Payment_Date__R_Field_10.newFieldInGroup("pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A", 
            "#FINAL-PAYMENT-DATE-A", FieldType.STRING, 8);
        pnd_Integer_Periods = localVariables.newFieldInRecord("pnd_Integer_Periods", "#INTEGER-PERIODS", FieldType.INTEGER, 4);
        pnd_Last_Payment_Period = localVariables.newFieldInRecord("pnd_Last_Payment_Period", "#LAST-PAYMENT-PERIOD", FieldType.INTEGER, 4);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_V = localVariables.newFieldInRecord("pnd_V", "#V", FieldType.FLOAT, 8);
        pnd_V_Exponent = localVariables.newFieldInRecord("pnd_V_Exponent", "#V-EXPONENT", FieldType.FLOAT, 8);
        pnd_Full_Years = localVariables.newFieldInRecord("pnd_Full_Years", "#FULL-YEARS", FieldType.NUMERIC, 3);
        pnd_Months_Between_Payments = localVariables.newFieldInRecord("pnd_Months_Between_Payments", "#MONTHS-BETWEEN-PAYMENTS", FieldType.NUMERIC, 2);
        pnd_Additional_Months = localVariables.newFieldInRecord("pnd_Additional_Months", "#ADDITIONAL-MONTHS", FieldType.NUMERIC, 2);
        pnd_Remaining_Payments_In_Year = localVariables.newFieldInRecord("pnd_Remaining_Payments_In_Year", "#REMAINING-PAYMENTS-IN-YEAR", FieldType.NUMERIC, 
            3);
        pnd_Num_Of_Payments = localVariables.newFieldInRecord("pnd_Num_Of_Payments", "#NUM-OF-PAYMENTS", FieldType.NUMERIC, 2);
        pnd_Install_Ref_Cert_Per = localVariables.newFieldInRecord("pnd_Install_Ref_Cert_Per", "#INSTALL-REF-CERT-PER", FieldType.NUMERIC, 8, 5);
        pnd_Annualized_Annuity_Factor = localVariables.newFieldInRecord("pnd_Annualized_Annuity_Factor", "#ANNUALIZED-ANNUITY-FACTOR", FieldType.NUMERIC, 
            7, 5);
        pnd_Accumulation_Left = localVariables.newFieldInRecord("pnd_Accumulation_Left", "#ACCUMULATION-LEFT", FieldType.NUMERIC, 11, 2);
        pnd_Inst_Ref_Ratio = localVariables.newFieldInRecord("pnd_Inst_Ref_Ratio", "#INST-REF-RATIO", FieldType.FLOAT, 8);
        pnd_Irr_Payment = localVariables.newFieldInRecord("pnd_Irr_Payment", "#IRR-PAYMENT", FieldType.NUMERIC, 9, 2);
        pnd_Number_Of_Months = localVariables.newFieldInRecord("pnd_Number_Of_Months", "#NUMBER-OF-MONTHS", FieldType.NUMERIC, 6);
        pnd_Number_Of_Years = localVariables.newFieldInRecord("pnd_Number_Of_Years", "#NUMBER-OF-YEARS", FieldType.NUMERIC, 6);
        pnd_Num_Of_Payments_In_Guar_Per = localVariables.newFieldInRecord("pnd_Num_Of_Payments_In_Guar_Per", "#NUM-OF-PAYMENTS-IN-GUAR-PER", FieldType.NUMERIC, 
            6);
        pnd_Type_Of_Call = localVariables.newFieldInRecord("pnd_Type_Of_Call", "#TYPE-OF-CALL", FieldType.STRING, 2);
        pnd_Tiaa_Total = localVariables.newFieldInRecord("pnd_Tiaa_Total", "#TIAA-TOTAL", FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Guaranteed = localVariables.newFieldInRecord("pnd_Tiaa_Guaranteed", "#TIAA-GUARANTEED", FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Dividend = localVariables.newFieldInRecord("pnd_Tiaa_Dividend", "#TIAA-DIVIDEND", FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Guar_Periods = localVariables.newFieldInRecord("pnd_Tiaa_Guar_Periods", "#TIAA-GUAR-PERIODS", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Rate_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Rate_Amt", "#TIAA-RATE-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Rate_Cde = localVariables.newFieldInRecord("pnd_Tiaa_Rate_Cde", "#TIAA-RATE-CDE", FieldType.STRING, 2);
        pnd_Tiaa_Ia_Rate_Cde = localVariables.newFieldInRecord("pnd_Tiaa_Ia_Rate_Cde", "#TIAA-IA-RATE-CDE", FieldType.STRING, 2);
        pnd_Ia_Rate_Cde = localVariables.newFieldInRecord("pnd_Ia_Rate_Cde", "#IA-RATE-CDE", FieldType.STRING, 2);
        pnd_Cref_Rate_Amt = localVariables.newFieldInRecord("pnd_Cref_Rate_Amt", "#CREF-RATE-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Cref_Rate_Cde = localVariables.newFieldInRecord("pnd_Cref_Rate_Cde", "#CREF-RATE-CDE", FieldType.STRING, 2);
        pnd_Cref_Estimated_Amount = localVariables.newFieldInRecord("pnd_Cref_Estimated_Amount", "#CREF-ESTIMATED-AMOUNT", FieldType.NUMERIC, 9, 2);
        pnd_Cref_Payout_Units = localVariables.newFieldInRecord("pnd_Cref_Payout_Units", "#CREF-PAYOUT-UNITS", FieldType.NUMERIC, 10, 3);
        pnd_Cref_Payout_Amount = localVariables.newFieldInRecord("pnd_Cref_Payout_Amount", "#CREF-PAYOUT-AMOUNT", FieldType.NUMERIC, 9, 2);
        pnd_Cref_Rate = localVariables.newFieldInRecord("pnd_Cref_Rate", "#CREF-RATE", FieldType.STRING, 2);
        pnd_Modal_Adjustment = localVariables.newFieldInRecord("pnd_Modal_Adjustment", "#MODAL-ADJUSTMENT", FieldType.FLOAT, 8);
        pnd_Modal_Numerator = localVariables.newFieldInRecord("pnd_Modal_Numerator", "#MODAL-NUMERATOR", FieldType.FLOAT, 8);
        pnd_Modal_Denominator = localVariables.newFieldInRecord("pnd_Modal_Denominator", "#MODAL-DENOMINATOR", FieldType.FLOAT, 8);

        pnd_Sum_Amounts = localVariables.newGroupInRecord("pnd_Sum_Amounts", "#SUM-AMOUNTS");
        pnd_Sum_Amounts_Pnd_Tiaa_Sum_Rate_Amt = pnd_Sum_Amounts.newFieldInGroup("pnd_Sum_Amounts_Pnd_Tiaa_Sum_Rate_Amt", "#TIAA-SUM-RATE-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guaranteed = pnd_Sum_Amounts.newFieldInGroup("pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guaranteed", "#TIAA-SUM-GUARANTEED", FieldType.NUMERIC, 
            11, 2);
        pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guar_Periods = pnd_Sum_Amounts.newFieldInGroup("pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guar_Periods", "#TIAA-SUM-GUAR-PERIODS", 
            FieldType.NUMERIC, 13, 2);
        pnd_1st_Ann_Sex_A = localVariables.newFieldInRecord("pnd_1st_Ann_Sex_A", "#1ST-ANN-SEX-A", FieldType.STRING, 1);

        pnd_1st_Ann_Sex_A__R_Field_11 = localVariables.newGroupInRecord("pnd_1st_Ann_Sex_A__R_Field_11", "REDEFINE", pnd_1st_Ann_Sex_A);
        pnd_1st_Ann_Sex_A_Pnd_1st_Ann_Sex_N = pnd_1st_Ann_Sex_A__R_Field_11.newFieldInGroup("pnd_1st_Ann_Sex_A_Pnd_1st_Ann_Sex_N", "#1ST-ANN-SEX-N", FieldType.NUMERIC, 
            1);
        pnd_2nd_Ann_Sex_A = localVariables.newFieldInRecord("pnd_2nd_Ann_Sex_A", "#2ND-ANN-SEX-A", FieldType.STRING, 1);

        pnd_2nd_Ann_Sex_A__R_Field_12 = localVariables.newGroupInRecord("pnd_2nd_Ann_Sex_A__R_Field_12", "REDEFINE", pnd_2nd_Ann_Sex_A);
        pnd_2nd_Ann_Sex_A_Pnd_2nd_Ann_Sex_N = pnd_2nd_Ann_Sex_A__R_Field_12.newFieldInGroup("pnd_2nd_Ann_Sex_A_Pnd_2nd_Ann_Sex_N", "#2ND-ANN-SEX-N", FieldType.NUMERIC, 
            1);
        pnd_Mode = localVariables.newFieldInRecord("pnd_Mode", "#MODE", FieldType.NUMERIC, 3);

        pnd_Mode__R_Field_13 = localVariables.newGroupInRecord("pnd_Mode__R_Field_13", "REDEFINE", pnd_Mode);
        pnd_Mode_Pnd_Mode_1 = pnd_Mode__R_Field_13.newFieldInGroup("pnd_Mode_Pnd_Mode_1", "#MODE-1", FieldType.NUMERIC, 1);
        pnd_Mode_Pnd_Mode_2 = pnd_Mode__R_Field_13.newFieldInGroup("pnd_Mode_Pnd_Mode_2", "#MODE-2", FieldType.NUMERIC, 2);
        pnd_Check_Date_A = localVariables.newFieldInRecord("pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 8);

        pnd_Check_Date_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Check_Date_A__R_Field_14", "REDEFINE", pnd_Check_Date_A);
        pnd_Check_Date_A_Pnd_Check_Date_N = pnd_Check_Date_A__R_Field_14.newFieldInGroup("pnd_Check_Date_A_Pnd_Check_Date_N", "#CHECK-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Check_Date_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Check_Date_A__R_Field_15", "REDEFINE", pnd_Check_Date_A);
        pnd_Check_Date_A_Pnd_Check_Year_N = pnd_Check_Date_A__R_Field_15.newFieldInGroup("pnd_Check_Date_A_Pnd_Check_Year_N", "#CHECK-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Check_Date_A_Pnd_Check_Month_N = pnd_Check_Date_A__R_Field_15.newFieldInGroup("pnd_Check_Date_A_Pnd_Check_Month_N", "#CHECK-MONTH-N", FieldType.NUMERIC, 
            2);
        pnd_Check_Date_A_Pnd_Check_Day_N = pnd_Check_Date_A__R_Field_15.newFieldInGroup("pnd_Check_Date_A_Pnd_Check_Day_N", "#CHECK-DAY-N", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Issue_Date_A__R_Field_16 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_16", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N8 = pnd_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N8", "#ISSUE-DATE-N8", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A__R_Field_17 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_17", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N = pnd_Issue_Date_A__R_Field_17.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            6);
        pnd_Issue_Date_A_Pnd_Issue_Date_Day = pnd_Issue_Date_A__R_Field_17.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Day", "#ISSUE-DATE-DAY", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_18 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_18", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Year_N = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Year_N", "#ISSUE-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Issue_Date_A_Pnd_Issue_Month_N = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Month_N", "#ISSUE-MONTH-N", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_A_Pnd_Issue_Day_N = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Day_N", "#ISSUE-DAY-N", FieldType.NUMERIC, 
            2);
        pnd_Spia_Start_Date_A = localVariables.newFieldInRecord("pnd_Spia_Start_Date_A", "#SPIA-START-DATE-A", FieldType.STRING, 8);

        pnd_Spia_Start_Date_A__R_Field_19 = localVariables.newGroupInRecord("pnd_Spia_Start_Date_A__R_Field_19", "REDEFINE", pnd_Spia_Start_Date_A);
        pnd_Spia_Start_Date_A_Pnd_Spia_Start_Date_N8 = pnd_Spia_Start_Date_A__R_Field_19.newFieldInGroup("pnd_Spia_Start_Date_A_Pnd_Spia_Start_Date_N8", 
            "#SPIA-START-DATE-N8", FieldType.NUMERIC, 8);

        pnd_Spia_Start_Date_A__R_Field_20 = localVariables.newGroupInRecord("pnd_Spia_Start_Date_A__R_Field_20", "REDEFINE", pnd_Spia_Start_Date_A);
        pnd_Spia_Start_Date_A_Pnd_Spia_Start_Date_N = pnd_Spia_Start_Date_A__R_Field_20.newFieldInGroup("pnd_Spia_Start_Date_A_Pnd_Spia_Start_Date_N", 
            "#SPIA-START-DATE-N", FieldType.NUMERIC, 6);
        pnd_Spia_Start_Date_A_Pnd_Spia_Start_Day = pnd_Spia_Start_Date_A__R_Field_20.newFieldInGroup("pnd_Spia_Start_Date_A_Pnd_Spia_Start_Day", "#SPIA-START-DAY", 
            FieldType.NUMERIC, 2);

        pnd_Spia_Start_Date_A__R_Field_21 = localVariables.newGroupInRecord("pnd_Spia_Start_Date_A__R_Field_21", "REDEFINE", pnd_Spia_Start_Date_A);
        pnd_Spia_Start_Date_A_Pnd_Spia_Start_Year_N = pnd_Spia_Start_Date_A__R_Field_21.newFieldInGroup("pnd_Spia_Start_Date_A_Pnd_Spia_Start_Year_N", 
            "#SPIA-START-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Spia_Start_Date_A_Pnd_Spia_Start_Month_N = pnd_Spia_Start_Date_A__R_Field_21.newFieldInGroup("pnd_Spia_Start_Date_A_Pnd_Spia_Start_Month_N", 
            "#SPIA-START-MONTH-N", FieldType.NUMERIC, 2);
        pnd_Spia_Start_Date_A_Pnd_Spia_Start_Day_N = pnd_Spia_Start_Date_A__R_Field_21.newFieldInGroup("pnd_Spia_Start_Date_A_Pnd_Spia_Start_Day_N", "#SPIA-START-DAY-N", 
            FieldType.NUMERIC, 2);
        pnd_Eff_Date_A = localVariables.newFieldInRecord("pnd_Eff_Date_A", "#EFF-DATE-A", FieldType.STRING, 8);

        pnd_Eff_Date_A__R_Field_22 = localVariables.newGroupInRecord("pnd_Eff_Date_A__R_Field_22", "REDEFINE", pnd_Eff_Date_A);
        pnd_Eff_Date_A_Pnd_Eff_Date_N8 = pnd_Eff_Date_A__R_Field_22.newFieldInGroup("pnd_Eff_Date_A_Pnd_Eff_Date_N8", "#EFF-DATE-N8", FieldType.NUMERIC, 
            8);

        pnd_Eff_Date_A__R_Field_23 = localVariables.newGroupInRecord("pnd_Eff_Date_A__R_Field_23", "REDEFINE", pnd_Eff_Date_A);
        pnd_Eff_Date_A_Pnd_Eff_Date_N = pnd_Eff_Date_A__R_Field_23.newFieldInGroup("pnd_Eff_Date_A_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 
            6);
        pnd_Eff_Date_A_Eff_Date_Day = pnd_Eff_Date_A__R_Field_23.newFieldInGroup("pnd_Eff_Date_A_Eff_Date_Day", "EFF-DATE-DAY", FieldType.NUMERIC, 2);

        pnd_Eff_Date_A__R_Field_24 = localVariables.newGroupInRecord("pnd_Eff_Date_A__R_Field_24", "REDEFINE", pnd_Eff_Date_A);
        pnd_Eff_Date_A_Pnd_Eff_Year_N = pnd_Eff_Date_A__R_Field_24.newFieldInGroup("pnd_Eff_Date_A_Pnd_Eff_Year_N", "#EFF-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Eff_Date_A_Pnd_Eff_Month_N = pnd_Eff_Date_A__R_Field_24.newFieldInGroup("pnd_Eff_Date_A_Pnd_Eff_Month_N", "#EFF-MONTH-N", FieldType.NUMERIC, 
            2);
        pnd_Eff_Date_A_Pnd_Eff_Day_N = pnd_Eff_Date_A__R_Field_24.newFieldInGroup("pnd_Eff_Date_A_Pnd_Eff_Day_N", "#EFF-DAY-N", FieldType.NUMERIC, 2);
        pnd_1st_Ann_Birth_Date_A = localVariables.newFieldInRecord("pnd_1st_Ann_Birth_Date_A", "#1ST-ANN-BIRTH-DATE-A", FieldType.STRING, 8);

        pnd_1st_Ann_Birth_Date_A__R_Field_25 = localVariables.newGroupInRecord("pnd_1st_Ann_Birth_Date_A__R_Field_25", "REDEFINE", pnd_1st_Ann_Birth_Date_A);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N = pnd_1st_Ann_Birth_Date_A__R_Field_25.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N", 
            "#1ST-ANN-BIRTH-DATE-N", FieldType.NUMERIC, 8);

        pnd_1st_Ann_Birth_Date_A__R_Field_26 = localVariables.newGroupInRecord("pnd_1st_Ann_Birth_Date_A__R_Field_26", "REDEFINE", pnd_1st_Ann_Birth_Date_A);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year_N = pnd_1st_Ann_Birth_Date_A__R_Field_26.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year_N", 
            "#1ST-ANN-BIRTH-YEAR-N", FieldType.NUMERIC, 4);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month_N = pnd_1st_Ann_Birth_Date_A__R_Field_26.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month_N", 
            "#1ST-ANN-BIRTH-MONTH-N", FieldType.NUMERIC, 2);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day_N = pnd_1st_Ann_Birth_Date_A__R_Field_26.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day_N", 
            "#1ST-ANN-BIRTH-DAY-N", FieldType.NUMERIC, 2);
        pnd_2nd_Ann_Birth_Date_A = localVariables.newFieldInRecord("pnd_2nd_Ann_Birth_Date_A", "#2ND-ANN-BIRTH-DATE-A", FieldType.STRING, 8);

        pnd_2nd_Ann_Birth_Date_A__R_Field_27 = localVariables.newGroupInRecord("pnd_2nd_Ann_Birth_Date_A__R_Field_27", "REDEFINE", pnd_2nd_Ann_Birth_Date_A);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N = pnd_2nd_Ann_Birth_Date_A__R_Field_27.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N", 
            "#2ND-ANN-BIRTH-DATE-N", FieldType.NUMERIC, 8);

        pnd_2nd_Ann_Birth_Date_A__R_Field_28 = localVariables.newGroupInRecord("pnd_2nd_Ann_Birth_Date_A__R_Field_28", "REDEFINE", pnd_2nd_Ann_Birth_Date_A);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year_N = pnd_2nd_Ann_Birth_Date_A__R_Field_28.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year_N", 
            "#2ND-ANN-BIRTH-YEAR-N", FieldType.NUMERIC, 4);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month_N = pnd_2nd_Ann_Birth_Date_A__R_Field_28.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month_N", 
            "#2ND-ANN-BIRTH-MONTH-N", FieldType.NUMERIC, 2);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day_N = pnd_2nd_Ann_Birth_Date_A__R_Field_28.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day_N", 
            "#2ND-ANN-BIRTH-DAY-N", FieldType.NUMERIC, 2);
        pnd_Ws_Date = localVariables.newFieldInRecord("pnd_Ws_Date", "#WS-DATE", FieldType.NUMERIC, 8);

        pnd_Ws_Date__R_Field_29 = localVariables.newGroupInRecord("pnd_Ws_Date__R_Field_29", "REDEFINE", pnd_Ws_Date);
        pnd_Ws_Date_Pnd_Ws_Date_A = pnd_Ws_Date__R_Field_29.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Date_A", "#WS-DATE-A", FieldType.STRING, 8);

        pnd_Ws_Date__R_Field_30 = localVariables.newGroupInRecord("pnd_Ws_Date__R_Field_30", "REDEFINE", pnd_Ws_Date);
        pnd_Ws_Date_Pnd_Ws_Year = pnd_Ws_Date__R_Field_30.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Year", "#WS-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Date_Pnd_Ws_Month = pnd_Ws_Date__R_Field_30.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Month", "#WS-MONTH", FieldType.NUMERIC, 2);
        pnd_Ws_Date_Pnd_Ws_Day = pnd_Ws_Date__R_Field_30.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Day", "#WS-DAY", FieldType.NUMERIC, 2);
        pnd_Pmt_Switch = localVariables.newFieldInRecord("pnd_Pmt_Switch", "#PMT-SWITCH", FieldType.NUMERIC, 1);
        pnd_Pmt_Month_1 = localVariables.newFieldInRecord("pnd_Pmt_Month_1", "#PMT-MONTH-1", FieldType.NUMERIC, 2);
        pnd_Pmt_Month_2 = localVariables.newFieldInRecord("pnd_Pmt_Month_2", "#PMT-MONTH-2", FieldType.NUMERIC, 2);
        pnd_Pmt_Month_3 = localVariables.newFieldInRecord("pnd_Pmt_Month_3", "#PMT-MONTH-3", FieldType.NUMERIC, 2);
        pnd_Pmt_Month_4 = localVariables.newFieldInRecord("pnd_Pmt_Month_4", "#PMT-MONTH-4", FieldType.NUMERIC, 2);
        pnd_Tpa_Rate_Key = localVariables.newFieldInRecord("pnd_Tpa_Rate_Key", "#TPA-RATE-KEY", FieldType.STRING, 8);

        pnd_Tpa_Rate_Key__R_Field_31 = localVariables.newGroupInRecord("pnd_Tpa_Rate_Key__R_Field_31", "REDEFINE", pnd_Tpa_Rate_Key);
        pnd_Tpa_Rate_Key_Pnd_Tpa_Key_Date = pnd_Tpa_Rate_Key__R_Field_31.newFieldInGroup("pnd_Tpa_Rate_Key_Pnd_Tpa_Key_Date", "#TPA-KEY-DATE", FieldType.NUMERIC, 
            6);
        pnd_Tpa_Rate_Key_Pnd_Tpa_Rate_Basis = pnd_Tpa_Rate_Key__R_Field_31.newFieldInGroup("pnd_Tpa_Rate_Key_Pnd_Tpa_Rate_Basis", "#TPA-RATE-BASIS", FieldType.STRING, 
            2);
        pnd_Cutoff_Date = localVariables.newFieldInRecord("pnd_Cutoff_Date", "#CUTOFF-DATE", FieldType.NUMERIC, 4);

        pnd_Cutoff_Date__R_Field_32 = localVariables.newGroupInRecord("pnd_Cutoff_Date__R_Field_32", "REDEFINE", pnd_Cutoff_Date);
        pnd_Cutoff_Date_Pnd_Cutoff_Month = pnd_Cutoff_Date__R_Field_32.newFieldInGroup("pnd_Cutoff_Date_Pnd_Cutoff_Month", "#CUTOFF-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Cutoff_Date_Pnd_Cutoff_Day = pnd_Cutoff_Date__R_Field_32.newFieldInGroup("pnd_Cutoff_Date_Pnd_Cutoff_Day", "#CUTOFF-DAY", FieldType.NUMERIC, 
            2);
        pnd_Ws_Ipro_Rate = localVariables.newFieldInRecord("pnd_Ws_Ipro_Rate", "#WS-IPRO-RATE", FieldType.NUMERIC, 7, 6);
        pnd_Mthly_Int_Rate = localVariables.newFieldInRecord("pnd_Mthly_Int_Rate", "#MTHLY-INT-RATE", FieldType.NUMERIC, 7, 6);
        pnd_Ws_Pa_Expected_Life = localVariables.newFieldInRecord("pnd_Ws_Pa_Expected_Life", "#WS-PA-EXPECTED-LIFE", FieldType.NUMERIC, 3);
        pnd_Pa_Type_Of_Call = localVariables.newFieldInRecord("pnd_Pa_Type_Of_Call", "#PA-TYPE-OF-CALL", FieldType.STRING, 1);

        pnd_Aian030_Linkage = localVariables.newGroupInRecord("pnd_Aian030_Linkage", "#AIAN030-LINKAGE");
        pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N", "#AIAN030-BEGIN-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N", "#AIAN030-END-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_D = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_D", "#AIAN030-BEGIN-DATE-D", 
            FieldType.DATE);
        pnd_Aian030_Linkage_Pnd_Aian030_End_Date_D = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_End_Date_D", "#AIAN030-END-DATE-D", 
            FieldType.DATE);
        pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days = pnd_Aian030_Linkage.newFieldInGroup("pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days", 
            "#AIAN030-DAYS-BETWEEN-TWO-DAYS", FieldType.NUMERIC, 8);

        pnd_Aian031_Linkage = localVariables.newGroupInRecord("pnd_Aian031_Linkage", "#AIAN031-LINKAGE");
        pnd_Aian031_Linkage_Pnd_Aian031_In_Rate_Basis = pnd_Aian031_Linkage.newFieldInGroup("pnd_Aian031_Linkage_Pnd_Aian031_In_Rate_Basis", "#AIAN031-IN-RATE-BASIS", 
            FieldType.STRING, 2);
        pnd_Aian031_Linkage_Pnd_Aian031_Out_Rate_Code = pnd_Aian031_Linkage.newFieldInGroup("pnd_Aian031_Linkage_Pnd_Aian031_Out_Rate_Code", "#AIAN031-OUT-RATE-CODE", 
            FieldType.STRING, 1);
        pnd_Aian031_Linkage_Pnd_Aian031_Return_Code = pnd_Aian031_Linkage.newFieldInGroup("pnd_Aian031_Linkage_Pnd_Aian031_Return_Code", "#AIAN031-RETURN-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Asd_Date_A = localVariables.newFieldInRecord("pnd_Ws_Asd_Date_A", "#WS-ASD-DATE-A", FieldType.STRING, 8);

        pnd_Ws_Asd_Date_A__R_Field_33 = localVariables.newGroupInRecord("pnd_Ws_Asd_Date_A__R_Field_33", "REDEFINE", pnd_Ws_Asd_Date_A);
        pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date = pnd_Ws_Asd_Date_A__R_Field_33.newFieldInGroup("pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date", "#WS-ASD-DATE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Effective_Date_A = localVariables.newFieldInRecord("pnd_Ws_Effective_Date_A", "#WS-EFFECTIVE-DATE-A", FieldType.STRING, 8);

        pnd_Ws_Effective_Date_A__R_Field_34 = localVariables.newGroupInRecord("pnd_Ws_Effective_Date_A__R_Field_34", "REDEFINE", pnd_Ws_Effective_Date_A);
        pnd_Ws_Effective_Date_A_Pnd_Ws_Effective_Date = pnd_Ws_Effective_Date_A__R_Field_34.newFieldInGroup("pnd_Ws_Effective_Date_A_Pnd_Ws_Effective_Date", 
            "#WS-EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 3);
        pnd_Ws_Da_Rate_A = localVariables.newFieldInRecord("pnd_Ws_Da_Rate_A", "#WS-DA-RATE-A", FieldType.STRING, 2);
        pnd_Ws_Move_Grd_To_Std = localVariables.newFieldInRecord("pnd_Ws_Move_Grd_To_Std", "#WS-MOVE-GRD-TO-STD", FieldType.STRING, 1);

        pnd_Ws_Graded_Info = localVariables.newGroupArrayInRecord("pnd_Ws_Graded_Info", "#WS-GRADED-INFO", new DbsArrayController(1, 250));
        pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Code = pnd_Ws_Graded_Info.newFieldInGroup("pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Code", "#WS-GRADED-RATE-CODE", 
            FieldType.STRING, 2);
        pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Amt = pnd_Ws_Graded_Info.newFieldInGroup("pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Amt", "#WS-GRADED-RATE-AMT", 
            FieldType.NUMERIC, 9, 2);

        pnd_Aian090_Linkage = localVariables.newGroupInRecord("pnd_Aian090_Linkage", "#AIAN090-LINKAGE");
        pnd_Aian090_Linkage_Pnd_Aian090_Call_Type = pnd_Aian090_Linkage.newFieldInGroup("pnd_Aian090_Linkage_Pnd_Aian090_Call_Type", "#AIAN090-CALL-TYPE", 
            FieldType.STRING, 1);
        pnd_Aian090_Linkage_Pnd_Aian090_In_Rate = pnd_Aian090_Linkage.newFieldInGroup("pnd_Aian090_Linkage_Pnd_Aian090_In_Rate", "#AIAN090-IN-RATE", FieldType.STRING, 
            2);
        pnd_Aian090_Linkage_Pnd_Aian090_Eff_Date = pnd_Aian090_Linkage.newFieldInGroup("pnd_Aian090_Linkage_Pnd_Aian090_Eff_Date", "#AIAN090-EFF-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Aian090_Linkage_Pnd_Aian090_Out_Rate = pnd_Aian090_Linkage.newFieldInGroup("pnd_Aian090_Linkage_Pnd_Aian090_Out_Rate", "#AIAN090-OUT-RATE", 
            FieldType.STRING, 2);
        pnd_Hold_Mortality_Table = localVariables.newFieldInRecord("pnd_Hold_Mortality_Table", "#HOLD-MORTALITY-TABLE", FieldType.NUMERIC, 4);
        pnd_Hold_Interest_Rate = localVariables.newFieldInRecord("pnd_Hold_Interest_Rate", "#HOLD-INTEREST-RATE", FieldType.NUMERIC, 4, 4);
        pnd_Hold_Def_Interest_Rate = localVariables.newFieldInRecord("pnd_Hold_Def_Interest_Rate", "#HOLD-DEF-INTEREST-RATE", FieldType.NUMERIC, 4, 4);
        pnd_Hold_Setback = localVariables.newFieldInRecord("pnd_Hold_Setback", "#HOLD-SETBACK", FieldType.NUMERIC, 7, 5);
        pnd_Hold_Date1 = localVariables.newFieldInRecord("pnd_Hold_Date1", "#HOLD-DATE1", FieldType.NUMERIC, 8);
        pnd_Hold_Date2 = localVariables.newFieldInRecord("pnd_Hold_Date2", "#HOLD-DATE2", FieldType.NUMERIC, 8);
        pnd_Hold_Calc = localVariables.newFieldInRecord("pnd_Hold_Calc", "#HOLD-CALC", FieldType.NUMERIC, 8, 5);

        pnd_P_And_I_Calc_Fields = localVariables.newGroupInRecord("pnd_P_And_I_Calc_Fields", "#P-AND-I-CALC-FIELDS");
        pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Guar_Rate = pnd_P_And_I_Calc_Fields.newFieldInGroup("pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Guar_Rate", 
            "#WS-PERIODIC-GUAR-RATE", FieldType.FLOAT, 8);
        pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum = pnd_P_And_I_Calc_Fields.newFieldInGroup("pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum", "#WS-UPDATED-ACCUM", 
            FieldType.NUMERIC, 9, 2);
        pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Guar_Payment = pnd_P_And_I_Calc_Fields.newFieldInGroup("pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Guar_Payment", "#WS-PI-GUAR-PAYMENT", 
            FieldType.NUMERIC, 9, 2);
        pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Total_Rate = pnd_P_And_I_Calc_Fields.newFieldInGroup("pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Total_Rate", 
            "#WS-PERIODIC-TOTAL-RATE", FieldType.FLOAT, 8);
        pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Tot_Payment = pnd_P_And_I_Calc_Fields.newFieldInGroup("pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Tot_Payment", "#WS-PI-TOT-PAYMENT", 
            FieldType.NUMERIC, 9, 2);
        pnd_Ws_Aian021_Type = localVariables.newFieldInRecord("pnd_Ws_Aian021_Type", "#WS-AIAN021-TYPE", FieldType.STRING, 2);

        collapse_Array = localVariables.newGroupInRecord("collapse_Array", "COLLAPSE-ARRAY");
        collapse_Array_Pos_I = collapse_Array.newFieldInGroup("collapse_Array_Pos_I", "POS-I", FieldType.NUMERIC, 3);
        collapse_Array_Grd_Pos = collapse_Array.newFieldInGroup("collapse_Array_Grd_Pos", "GRD-POS", FieldType.NUMERIC, 3);
        collapse_Array_Grd_Cnt = collapse_Array.newFieldInGroup("collapse_Array_Grd_Cnt", "GRD-CNT", FieldType.NUMERIC, 3);
        collapse_Array_Collapse_Grd_Ia_Rb = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Ia_Rb", "COLLAPSE-GRD-IA-RB", FieldType.STRING, 
            2);
        collapse_Array_Collapse_Grd_Gic = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Gic", "COLLAPSE-GRD-GIC", FieldType.NUMERIC, 11);
        collapse_Array_Collapse_Grd_Total_Acc = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Total_Acc", "COLLAPSE-GRD-TOTAL-ACC", FieldType.NUMERIC, 
            9, 2);
        collapse_Array_Collapse_Grd_Eff_Rate_Intc = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Eff_Rate_Intc", "COLLAPSE-GRD-EFF-RATE-INTC", 
            FieldType.NUMERIC, 5, 3);

        collapse_Array_Collapse_Grd_Array = collapse_Array.newGroupArrayInGroup("collapse_Array_Collapse_Grd_Array", "COLLAPSE-GRD-ARRAY", new DbsArrayController(1, 
            250));
        collapse_Array_Collapse_Grd_I = collapse_Array_Collapse_Grd_Array.newFieldInGroup("collapse_Array_Collapse_Grd_I", "COLLAPSE-GRD-I", FieldType.NUMERIC, 
            3);
        collapse_Array_Collapse_Grd_Rb = collapse_Array_Collapse_Grd_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Rb", "COLLAPSE-GRD-RB", FieldType.STRING, 
            2);
        collapse_Array_Collapse_Grd_Gic_Code = collapse_Array_Collapse_Grd_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Gic_Code", "COLLAPSE-GRD-GIC-CODE", 
            FieldType.NUMERIC, 11);
        collapse_Array_Collapse_Grd_Acc = collapse_Array_Collapse_Grd_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Acc", "COLLAPSE-GRD-ACC", FieldType.NUMERIC, 
            9, 2);
        collapse_Array_Collapse_Grd_Eff_Rate_Int = collapse_Array_Collapse_Grd_Array.newFieldInGroup("collapse_Array_Collapse_Grd_Eff_Rate_Int", "COLLAPSE-GRD-EFF-RATE-INT", 
            FieldType.NUMERIC, 5, 3);
        collapse_Array_Std_Pos = collapse_Array.newFieldInGroup("collapse_Array_Std_Pos", "STD-POS", FieldType.NUMERIC, 3);
        collapse_Array_Std_Cnt = collapse_Array.newFieldInGroup("collapse_Array_Std_Cnt", "STD-CNT", FieldType.NUMERIC, 3);
        collapse_Array_Collapse_Std_Ia_Rb = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Std_Ia_Rb", "COLLAPSE-STD-IA-RB", FieldType.STRING, 
            2);
        collapse_Array_Collapse_Std_Gic = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Std_Gic", "COLLAPSE-STD-GIC", FieldType.NUMERIC, 11);
        collapse_Array_Collapse_Std_Total_Acc = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Std_Total_Acc", "COLLAPSE-STD-TOTAL-ACC", FieldType.NUMERIC, 
            9, 2);
        collapse_Array_Collapse_Std_Eff_Rate_Intc = collapse_Array.newFieldInGroup("collapse_Array_Collapse_Std_Eff_Rate_Intc", "COLLAPSE-STD-EFF-RATE-INTC", 
            FieldType.NUMERIC, 5, 3);

        collapse_Array_Collapse_Std_Array = collapse_Array.newGroupArrayInGroup("collapse_Array_Collapse_Std_Array", "COLLAPSE-STD-ARRAY", new DbsArrayController(1, 
            250));
        collapse_Array_Collapse_Std_I = collapse_Array_Collapse_Std_Array.newFieldInGroup("collapse_Array_Collapse_Std_I", "COLLAPSE-STD-I", FieldType.NUMERIC, 
            3);
        collapse_Array_Collapse_Std_Rb = collapse_Array_Collapse_Std_Array.newFieldInGroup("collapse_Array_Collapse_Std_Rb", "COLLAPSE-STD-RB", FieldType.STRING, 
            2);
        collapse_Array_Collapse_Std_Gic_Code = collapse_Array_Collapse_Std_Array.newFieldInGroup("collapse_Array_Collapse_Std_Gic_Code", "COLLAPSE-STD-GIC-CODE", 
            FieldType.NUMERIC, 11);
        collapse_Array_Collapse_Std_Acc = collapse_Array_Collapse_Std_Array.newFieldInGroup("collapse_Array_Collapse_Std_Acc", "COLLAPSE-STD-ACC", FieldType.NUMERIC, 
            9, 2);
        collapse_Array_Collapse_Std_Eff_Rate_Int = collapse_Array_Collapse_Std_Array.newFieldInGroup("collapse_Array_Collapse_Std_Eff_Rate_Int", "COLLAPSE-STD-EFF-RATE-INT", 
            FieldType.NUMERIC, 5, 3);
        pnd_Hold_Rate_Code = localVariables.newFieldInRecord("pnd_Hold_Rate_Code", "#HOLD-RATE-CODE", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAial770r.initializeValues();
        ldaAial770d.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Aian051() throws Exception
    {
        super("Aian051");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* **********************************************************************
        //* **                    MAIN BODY OF PROGRAM
        //* ***********************************************************************
        //*  FORMAT (1) LS=250 PS=250                                                                                                                                     //Natural: FORMAT LS = 132
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Calc_Return_Area().reset();                                                                                         //Natural: RESET NAZ-ACT-CALC-RETURN-AREA
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Calced_Grd_Info().getValue("*").reset();                                                                       //Natural: RESET NAZ-ACT-TIAA-CALCED-GRD-INFO ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Calced_Std_Info().getValue("*").reset();                                                                       //Natural: RESET NAZ-ACT-TIAA-CALCED-STD-INFO ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Calced_Ann_Info().getValue("*").reset();                                                                       //Natural: RESET NAZ-ACT-CREF-CALCED-ANN-INFO ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Calced_Mth_Info().getValue("*").reset();                                                                       //Natural: RESET NAZ-ACT-CREF-CALCED-MTH-INFO ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Settle_Basis().getValue("*").reset();                                                                          //Natural: RESET NAZ-ACT-CREF-SETTLE-BASIS ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Settlement_Basis_Array().getValue("*").reset();                                                                         //Natural: RESET NAZ-SETTLEMENT-BASIS-ARRAY ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Deferral_Period().reset();                                                                                              //Natural: RESET NAZ-DEFERRAL-PERIOD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Certain_Period().reset();                                                                                               //Natural: RESET NAZ-CERTAIN-PERIOD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Cref().getValue("*").reset();                                                                                      //Natural: RESET NAZ-AGE1-CREF ( * ) NAZ-AGE1-GUAR ( * ) NAZ-AGE1-TOTAL ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Guar().getValue("*").reset();
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Total().getValue("*").reset();
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Guar_Grd().getValue("*").reset();                                                                                  //Natural: RESET NAZ-AGE1-GUAR-GRD ( * ) NAZ-AGE1-TOTAL-GRD ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Total_Grd().getValue("*").reset();
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Cref().getValue("*").reset();                                                                                      //Natural: RESET NAZ-AGE2-CREF ( * ) NAZ-AGE2-GUAR ( * ) NAZ-AGE2-TOTAL ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Guar().getValue("*").reset();
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Total().getValue("*").reset();
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Guar_Grd().getValue("*").reset();                                                                                  //Natural: RESET NAZ-AGE2-GUAR-GRD ( * ) NAZ-AGE2-TOTAL-GRD ( * )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Total_Grd().getValue("*").reset();
        pdaAial0210.getPnd_Aian021_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN021-LINKAGE
        collapse_Array.reset();                                                                                                                                           //Natural: RESET COLLAPSE-ARRAY
        pnd_Aian021_Linkage_Array.getValue("*").reset();                                                                                                                  //Natural: RESET #AIAN021-LINKAGE-ARRAY ( * ) #ANN-FACTOR ( * )
        pnd_Ann_Factor.getValue("*").reset();
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue("AIAN051 000");                                                                              //Natural: MOVE 'AIAN051 000' TO NAZ-ACT-RETURN-CODE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(0);                                                                                //Natural: MOVE 0 TO #AIAN021-IN-DEF-INTEREST-RATE
        //*   RITE '********* AIAN051 ********'
                                                                                                                                                                          //Natural: PERFORM CONVERT-DATE-TO-NUMERIC
        sub_Convert_Date_To_Numeric();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-IAAN0019
        sub_Call_Iaan0019();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NOT = 0
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(25) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(27))) //Natural: IF NAZ-ACT-OPTION-CDE = 25 OR = 27
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(25) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(27)))    //Natural: IF NAZ-ACT-OPTION-CDE = 25 OR = 27
        {
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN074-IPRO-CALCULATION
            sub_Call_Aian074_Ipro_Calculation();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* * SMOLYAN 2/9/09
        //*   RITE '*AIAN022 SMOLYAN 02/04/09'
        //*  RESET #AIAN101-LINKAGE
        //*  MOVE 'NAZL510' TO #IN-FUNCTION-NAME
        //*  MOVE NAZ-ACT-TYPE-OF-CALL TO #IN-CALL-TYPE
        //*  CALLNAT 'AIAN101' #AIAN101-LINKAGE
        //*  IF #OUT-AIAN101-RETURN-CODE >01
        //*   MOVE #OUT-AIAN101-RETURN-CODE TO NAZ-ACT-RETURN-CODE
        //*   ESCAPE ROUTINE
        //*  END-IF
        //* * END-SMOLYAN
        //*   09/17/03  JS
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'S'  OR  NAZ-ACT-TYPE-OF-CALL = 'M' /* SS
        //* SPIA
        //* TIAA AND TCLIFE NON-PENSION
        //* TIAA AND TCLIFE NON-PENSION
        if (condition((pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)) || (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(2)  //Natural: IF ( #IAAN0019-CNTRCT-ORGN-CDE = 40 ) OR ( #IAAN0019-ORIGIN-FAMILY = 2 OR #IAAN0019-ORIGIN-FAMILY = 7 )
            || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(7))))
        {
            pnd_Pa_Type_Of_Call.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call());                                                                   //Natural: MOVE NAZ-ACT-TYPE-OF-CALL TO #PA-TYPE-OF-CALL
            //*  IF  NAZ-ACT-TYPE-OF-CALL = 'M'
            //* TIAA AND TCLIFE NON-PENSION
            //* TIAA AND TCLIFE NON-PENSION
            if (condition((pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(2) || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(7)))) //Natural: IF ( #IAAN0019-ORIGIN-FAMILY = 2 OR #IAAN0019-ORIGIN-FAMILY = 7 )
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("1");                                                                               //Natural: MOVE '1' TO NAZ-ACT-TYPE-OF-CALL
            }                                                                                                                                                             //Natural: END-IF
            //*  END FIX SS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2")))) //Natural: IF ( NAZ-ACT-TYPE-OF-CALL = '1' OR NAZ-ACT-TYPE-OF-CALL = '2' )
        {
            //*  IF (NAZ-ACT-TYPE-OF-CALL = 'I'  OR  NAZ-ACT-TYPE-OF-CALL = 'F')
            pnd_Ws_Move_Grd_To_Std.setValue("N");                                                                                                                         //Natural: MOVE 'N' TO #WS-MOVE-GRD-TO-STD
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN027-FOR-GRADED-TEST
            sub_Call_Aian027_For_Graded_Test();
            if (condition(Global.isEscape())) {return;}
            //*   RITE 'AIAN027R RETURN CODE = '  NAZ-ACT-RETURN-CODE
            if (condition(pnd_Ws_Move_Grd_To_Std.equals("Y")))                                                                                                            //Natural: IF #WS-MOVE-GRD-TO-STD = 'Y'
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("Y");                                                                              //Natural: MOVE 'Y' TO NAZ-ACT-TIAA-STD-CALL
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ANY-GRADED-LEFT
                sub_Check_For_Any_Graded_Left();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   END FIX
                                                                                                                                                                          //Natural: PERFORM INTITALIZE-ROUTINE
        sub_Intitalize_Routine();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN028-TO-CONVERT-OPTION
        sub_Call_Aian028_To_Convert_Option();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Tpa_Ind.equals("Y")))                                                                                                                        //Natural: IF #WS-TPA-IND = 'Y'
        {
            pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Gsra_Ira_Tpa_Mdo_Ind().setValue("Y");                                                                           //Natural: MOVE 'Y' TO #AIAN028-GSRA-IRA-TPA-MDO-IND
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONVERT-MODE
        sub_Convert_Mode();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                                 //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM CONVERT-DATE-TO-NUMERIC
            sub_Convert_Date_To_Numeric();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-DATES
            sub_Check_Dates();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30))  //Natural: IF ( NAZ-ACT-OPTION-CDE = 28 OR = 30 ) AND NAZ-ACT-TYPE-OF-CALL = '1' AND NAZ-ACT-GUAR-PERIOD = 0
            && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")) && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().equals(getZero()))))
        {
            //*  IF (NAZ-ACT-OPTION-CDE = 28 OR = 30) AND NAZ-ACT-TYPE-OF-CALL = 'I'
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(10);                                                                                     //Natural: MOVE 10 TO NAZ-ACT-GUAR-PERIOD
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF  NAZ-ACT-TYPE-OF-CALL = 'S'
        if (condition(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))                                                                        //Natural: IF #IAAN0019-CNTRCT-ORGN-CDE = 40
        {
                                                                                                                                                                          //Natural: PERFORM GET-CUTOFF-DATE
            sub_Get_Cutoff_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().greater(getZero()) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth().greater(getZero()))  //Natural: IF ( NAZ-ACT-GUAR-PERIOD > 0 OR NAZ-ACT-GUAR-PERIOD-MTH > 0 ) OR ( NAZ-ACT-TYPE-OF-CALL = '2' AND NAZ-ACT-OPTION-CDE = 2 )
            || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(2))))
        {
            //*    OR (NAZ-ACT-TYPE-OF-CALL = 'F' AND NAZ-ACT-OPTION-CDE = 2)
            //* *  PERFORM CONVERT-MODE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-LAST-PAYMENT-DATE
            sub_Calculate_Last_Payment_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *ITE 'NAZ-ACT-TYPE-OF-CALL-AIAN022W' NAZ-ACT-TYPE-OF-CALL
        //*  IF  (NAZ-ACT-TYPE-OF-CALL = 'S' AND #ISSUE-DATE-DAY > 01)
        //*   PERFORM GET-CUTOFF-DATE
        //*  END-IF
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR #IAAN0019-CNTRCT-ORGN-CDE = 40
            && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
            //*     (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
            //*     OR NAZ-ACT-TYPE-OF-CALL = 'S'
                                                                                                                                                                          //Natural: PERFORM FILL-AIAN030-LINKAGE
            sub_Fill_Aian030_Linkage();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN030
            sub_Call_Aian030();
            if (condition(Global.isEscape())) {return;}
            //* *IF NAZ-ACT-STD-FNL-PP-PAY-DTE > 0
            //* *   RITE 'NAZ-ACT-FNL-PAY-DATE = ' NAZ-ACT-FNL-PAY-DATE
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date().greater(getZero()) || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().greater(getZero())  //Natural: IF NAZ-ACT-FNL-PAY-DATE > 0 OR ( NAZ-ACT-GUAR-PERIOD > 0 OR NAZ-ACT-GUAR-PERIOD-MTH > 0 )
                || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth().greater(getZero()))))
            {
                //* *     PERFORM CONVERT-MODE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-GUARANTEED-PERIOD
                sub_Calculate_Guaranteed_Period();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   RITE 'CALL' NAZ-ACT-TIAA-STD-CALL
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().equals("Y") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero()))) //Natural: IF NAZ-ACT-TIAA-GRADED-CALL = 'Y' AND NAZ-ACT-RETURN-CODE-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM TIAA-GRADED-ROUTINE
            sub_Tiaa_Graded_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().equals("Y") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero()))) //Natural: IF NAZ-ACT-TIAA-STD-CALL = 'Y' AND NAZ-ACT-RETURN-CODE-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM TIAA-STANDARD-ROUTINE
            sub_Tiaa_Standard_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************************************************
        //* * THE FOLLOWING CODE ADDED TO RESOLVE THE GIC ISSUE FOR COLLAPSE RB.
        //* * CODE ADDED 6/22/2012  -  SLR  (BASED ON CODE FROM VE)
        //* * FIRST WE DO STANDARD
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 250
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
        {
            pnd_Hold_Rate_Code.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_I));                                             //Natural: MOVE NAZ-ACT-TIAA-O-STD-RATE-CDE ( #I ) TO #HOLD-RATE-CODE
            if (condition(pnd_Hold_Rate_Code.equals(" ")))                                                                                                                //Natural: IF #HOLD-RATE-CODE = ' '
            {
                //*  ESCAPES #I LOOP , WE ARE FINISHED STD
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  CLOSES IF FROM (1084)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Hold_Rate_Code.notEquals(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I))))                               //Natural: IF #HOLD-RATE-CODE NE NAZ-ACT-TIAA-STD-RATE-CDE ( #I )
            {
                FOR02:                                                                                                                                                    //Natural: FOR #J = 1 TO 250
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(250)); pnd_J.nadd(1))
                {
                    if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_J).equals(" ")))                                         //Natural: IF NAZ-ACT-TIAA-STD-RATE-CDE ( #J ) EQ ' '
                    {
                        //*  ACTUALLY, THIS WOULD BE AN ERROR
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  CLOSES IF FROM (1096)
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Hold_Rate_Code.equals(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_J))))                          //Natural: IF #HOLD-RATE-CODE = NAZ-ACT-TIAA-STD-RATE-CDE ( #J )
                    {
                        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_J)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-GIC-CODE ( #J ) TO NAZ-ACT-TIAA-O-STD-GIC-CODE ( #I )
                        //*  FOUND A MATCH, ESCAPE #J LOOP
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  CLOSES IF FROM (1102)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CLOSES FOR FROM (1094)
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CLOSES IF FROM (1092)
            }                                                                                                                                                             //Natural: END-IF
            //*  CLOSES FOR FROM (1080)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * NOW WE DO GRADED
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 250
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
        {
            pnd_Hold_Rate_Code.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_I));                                             //Natural: MOVE NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #I ) TO #HOLD-RATE-CODE
            if (condition(pnd_Hold_Rate_Code.equals(" ")))                                                                                                                //Natural: IF #HOLD-RATE-CODE = ' '
            {
                //*  ESCAPES #I LOOP , WE ARE FINISHED GRD
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  CLOSES IF FROM (1120)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Hold_Rate_Code.notEquals(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_I))))                            //Natural: IF #HOLD-RATE-CODE NE NAZ-ACT-TIAA-GRADED-RATE-CDE ( #I )
            {
                FOR04:                                                                                                                                                    //Natural: FOR #J = 1 TO 250
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(250)); pnd_J.nadd(1))
                {
                    if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_J).equals(" ")))                                      //Natural: IF NAZ-ACT-TIAA-GRADED-RATE-CDE ( #J ) EQ ' '
                    {
                        //*  NO MATCH, USE ORIGINAL GIC CODE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  CLOSES IF FROM (1134)
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Hold_Rate_Code.equals(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_J))))                       //Natural: IF #HOLD-RATE-CODE = NAZ-ACT-TIAA-GRADED-RATE-CDE ( #J )
                    {
                        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(pnd_J)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-GIC-CODE ( #J ) TO NAZ-ACT-TIAA-O-GRD-GIC-CODE ( #I )
                        //*  FOUND A MATCH, ESCAPE #J LOOP
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  CLOSES IF FROM (1140)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CLOSES FOR FROM (1132)
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CLOSES IF FROM (1124)
            }                                                                                                                                                             //Natural: END-IF
            //*  CLOSES FOR FROM (1120)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***** END OF COLLAPSE RB CODE  *****************************
        //* ************************************************************
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().equals("Y") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero()))) //Natural: IF NAZ-ACT-CREF-ANNUAL-CALL = 'Y' AND NAZ-ACT-RETURN-CODE-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM CREF-ANNUAL-ROUTINE
            sub_Cref_Annual_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().equals("Y") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero()))) //Natural: IF NAZ-ACT-CREF-MTHLY-CALL = 'Y' AND NAZ-ACT-RETURN-CODE-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM CREF-MONTHLY-ROUTINE
            sub_Cref_Monthly_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #PA-TYPE-OF-CALL = 'S' OR #PA-TYPE-OF-CALL = 'M'  /* SS
        //*  LE01 FIX BEGINS >>
        pnd_Ws_Asd_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED NAZ-ACT-ASD-DATE ( EM = YYYYMMDD ) TO #WS-ASD-DATE-A
        if (condition(pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date.less(20161001)))                                                                                                  //Natural: IF #WS-ASD-DATE < 20161001
        {
            //*  SPIA
            //*  TIAA AND TC LIFE NONPENSN
            if (condition((pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)) || (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(2)  //Natural: IF ( #IAAN0019-CNTRCT-ORGN-CDE = 40 ) OR ( #IAAN0019-ORIGIN-FAMILY = 2 OR #IAAN0019-ORIGIN-FAMILY = 7 )
                || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(7))))
            {
                //*  SS
                                                                                                                                                                          //Natural: PERFORM CALCULATE-LIFE-EXPECTANCY-FOR-PA
                sub_Calculate_Life_Expectancy_For_Pa();
                if (condition(Global.isEscape())) {return;}
                //*  SS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  LE01 FIX ENDS   <<
        //*   09/17/03  JS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN027-FOR-GRADED-TEST
        //* * END OF CODE ADDED 7/21/2011
        //*   12/01/03  JS
        //*     FOR #X = 1 TO 60
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-OF-GRD-TO-STAN-FOR-GRD-ONLY-OPTION
        //* **********************************************************************
        //* *  FOR NAZ-ACT-TIAA-STD-CALL = ' ' (A 100% GRADED CALL)
        //*   12/01/03  JS
        //*  FOR #Y = 1 TO 60
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-ANY-GRADED-LEFT
        //* **********************************************************************
        //* *  CHECK TO SEE IF ANY RB'S HAVE REMAINED GRADED
        //*   12/01/03
        //*  FOR #Y = 1 TO 60
        //* *FOR #Y = 1 TO 99
        //*   END FIX
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INTITALIZE-ROUTINE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-STANDARD-ROUTINE
        //*   RITE (1) '=' STD-POS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-GRADED-ROUTINE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-ANNUAL-ROUTINE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-MONTHLY-ROUTINE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-CREF-RB-TO-LETTER-CODE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-AIAN026-LINKAGE
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
        //*     (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
        //*    OR  NAZ-ACT-TYPE-OF-CALL = 'S'
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN026
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-AIAN021-LINKAGE
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
        //*     (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
        //*    OR  (NAZ-ACT-TYPE-OF-CALL = 'S' AND #ISSUE-DATE-DAY > 01)
        //*  IF (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
        //*    OR  NAZ-ACT-TYPE-OF-CALL = 'S'
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN021
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN028-TO-CONVERT-OPTION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CUTOFF-DATE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-AIAN030-LINKAGE
        //*  IF (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
        //*    OR (NAZ-ACT-TYPE-OF-CALL = 'S' AND #ISSUE-DATE-DAY > 01)
        //*    OR NAZ-ACT-TYPE-OF-CALL = 'S'
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-NEXT-PMT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN030
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-PAYMENT-BY-OPTION-CODE
        //* *WHEN NAZ-ACT-OPTION-CDE = 2
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-SIMPLE-OPTION-ROUTINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-INSTALLMENT-OPTION-ROUTINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-PAYMENT-CALL
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
        //*      (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)  OR
        //*    NAZ-ACT-TYPE-OF-CALL = 'S'
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GUARANTEED-PAYMENT-CALL
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
        //*    NAZ-ACT-TYPE-OF-CALL = 'S' OR
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-OF-TOTAL-PAYMENT
        //* *  (#AIAN021-OUT-ANNUITY-FACTOR + (#MONTHS-BETWEEN-PAYMENTS / 12))
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-OF-GURARANTEED-PAYMENT
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01
        //* *  (#AIAN021-OUT-ANNUITY-FACTOR + (#MONTHS-BETWEEN-PAYMENTS / 12))
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-OF-DIVIDEND-PAYMENT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUM-GUAR-PAYMENT-AND-ACCUM-TOTAL
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IS-OPTION-AN-INSTALLMENT-REFUND
        //*   12/01/03  JS
        //*    FOR #I = 1 TO 60
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-PAYMENT-BY-OPTION-CODE
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
        //*    (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
        //*    OR  NAZ-ACT-TYPE-OF-CALL = 'S'
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-OF-CREF-ESTIMATED-AMT
        //* *  (#AIAN021-OUT-ANNUITY-FACTOR + (#MONTHS-BETWEEN-PAYMENTS / 12))
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-OF-CREF-PAYOUT-UNITS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-OF-CREF-PAYOUT-AMOUNT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-MODAL-ADJUSTMENT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-DATE-TO-NUMERIC
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
        //*     (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
        //*    OR NAZ-ACT-TYPE-OF-CALL = 'S'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-DATES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA770-FILE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHOOSE-MORTALITY-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-MODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-LAST-PAYMENT-DATE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-GUARANTEED-PERIOD
        //* *************************************** #NEXT-IA-PAYMENT-MONTH +
        //* **************************************** #NEXT-IA-PAYMENT-YEAR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-VARIABLES-TO-STD-OUTPUT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-VARIABLES-TO-GRD-OUTPUT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-VARIABLES-TO-ANNUAL-OUTPUT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-VARIABLES-TO-MONTHLY-OUTPUT
        //* ***********************************************************************
        //*   DEFINE SUBROUTINE DISPLAY-TIAA-STANDARD-ROUTINE-OUTPUT
        //* ***********************************************************************
        //*   RITE 'STANDARD-ROUTINE'
        //*   RITE  'TIAA-RATE-BASIS-CODE ' NAZ-ACT-TIAA-O-STD-RATE-CDE (#I)
        //*   RITE  'TIAA-STD-GUAR-AMT    ' NAZ-ACT-TIAA-O-STD-GUAR-AMT (#I)
        //*   RITE  'TIAA-STD-DIV-AMT     ' NAZ-ACT-TIAA-O-STD-DIVD-AMT (#I)
        //*   RITE  'TIAA-STD-FINAL-GUAR  ' NAZ-ACT-TIAA-STD-FNL-GUAR-AMT (#I)
        //*   RITE  'TIAA-STD-FINAL-DIV   ' NAZ-ACT-TIAA-STD-FNL-DIVD-AMT (#I)
        //*   RITE  'TIAA-INT-RATE        ' NAZ-ACT-TIAA-STD-EFF-RATE-INT (#I)
        //*   RITE  'RETURN-CODE          ' NAZ-ACT-RETURN-CODE
        //*   RITE '************************************************************'
        //*  END-SUBROUTINE
        //* **********************************************************************
        //*  DEFINE SUBROUTINE DISPLAY-TIAA-GRADED-ROUTINE-OUTPUT
        //* ***********************************************************************
        //*   RITE 'GRADED-ROUTINE'
        //*   RITE  'TIAA-RATE-BASIS-CODE ' NAZ-ACT-TIAA-O-GRD-RATE-CDE (#I)
        //*   RITE  'TIAA-GRD-GUAR-AMT    ' NAZ-ACT-TIAA-O-GRD-GUAR-AMT (#I)
        //*   RITE  'TIAA-GRD-DIV-AMT     ' NAZ-ACT-TIAA-O-GRD-DIVD-AMT (#I)
        //*   RITE  'TIAA-INT-RATE        ' NAZ-ACT-TIAA-GRD-EFF-RATE-INT (#I)
        //*   RITE  'RETURN-CODE          ' NAZ-ACT-RETURN-CODE
        //*   RITE '************************************************************'
        //*  END-SUBROUTINE
        //* **********************************************************************
        //*  DEFINE SUBROUTINE DISPLAY-CREF-ANNUAL-ROUTINE-OUTPUT
        //* **********************************************************************
        //*   RITE 'ANNUAL ' #AIAN026-LINKAGE
        //*   RITE 'CREF-RATE-BASIS-CODE ' NAZ-ACT-CREF-O-ANN-RATE-CDE (#I)
        //*   RITE 'ANNUITY UNIT VALUE  =  ' #AIAN026-INTERIM-ANNUITY-UNIT-VALUE
        //*   RITE 'ESTIMATED AMOUNT    =  ' NAZ-ACT-CREF-ANNUAL-RATE-AMT (#I)
        //*   ' / ' #AIAN021-OUT-ANNUITY-FACTOR
        //*    '= ' #CREF-ESTIMATED-AMOUNT
        //*   RITE 'PAYOUT UNITS        =  ' #CREF-ESTIMATED-AMOUNT
        //*    ' / ' NAZ-ACT-CREF-O-ANN-UNIT-VALUE (#I)
        //*    ' = ' NAZ-ACT-CREF-O-ANN-PYMNT-UNITS (#I) 'UNITS'
        //*   RITE 'PAYOUT AMOUNT       =  ' NAZ-ACT-CREF-O-ANN-PYMNT-UNITS (#I)
        //*    ' * ' NAZ-ACT-CREF-O-ANN-UNIT-VALUE (#I)
        //*    ' = ' NAZ-ACT-CREF-O-ANN-RATE-AMT (#I)
        //*   RITE  'RETURN-CODE          ' NAZ-ACT-RETURN-CODE
        //* *  RITE '************************************************************'
        //*  END-SUBROUTINE
        //* **********************************************************************
        //*  DEFINE SUBROUTINE DISPLAY-CREF-MONTHLY-ROUTINE-OUTPUT
        //* **********************************************************************
        //*   RITE 'MONTHLY ' #AIAN026-LINKAGE
        //*   RITE 'CREF-RATE-BASIS-CODE ' NAZ-ACT-CREF-O-MTH-RATE-CDE (#I)
        //*   RITE 'ANNUITY UNIT VALUE  =  ' #AIAN026-INTERIM-ANNUITY-UNIT-VALUE
        //*   RITE 'ESTIMATED AMOUNT    =  ' NAZ-ACT-CREF-MTHLY-RATE-AMT (#I)
        //*    ' / ' #AIAN021-OUT-ANNUITY-FACTOR
        //*    '= ' #CREF-ESTIMATED-AMOUNT
        //*   RITE 'PAYOUT UNITS        =  ' #CREF-ESTIMATED-AMOUNT
        //*    ' / ' NAZ-ACT-CREF-O-MTH-UNIT-VALUE (#I)
        //*    ' = ' NAZ-ACT-CREF-O-MTH-PYMNT-UNITS (#I) 'UNITS'
        //*   RITE 'PAYOUT AMOUNT       =  ' NAZ-ACT-CREF-O-MTH-PYMNT-UNITS (#I)
        //*    ' * ' NAZ-ACT-CREF-O-MTH-UNIT-VALUE (#I)
        //*    ' = ' NAZ-ACT-CREF-O-MTH-RATE-AMT (#I)
        //*   RITE  'RETURN-CODE          ' NAZ-ACT-RETURN-CODE
        //*   RITE '************************************************************'
        //*  END-SUBROUTINE
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN074-IPRO-CALCULATION
        //*   12/01/03  JS
        //*  FOR #I = 1 TO 60
        //* *FOR #I = 1 TO 99
        //*   12/01/03
        //*  FOR #I = 1 TO 60
        //* *FOR #I = 1 TO 99
        //* ********************************************************************
        //* *NEW ROUTINE FOR PA - REPLACES LOGIC IN AIAN050 SS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-LIFE-EXPECTANCY-FOR-PA
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-OF-FLORIDA-COMPLIANCE-CHECK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IAAN0019
        //* ************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IS-OPTION-P-AND-I
        //* *  FOR #I = 1 TO 99
        //* ************************************************
        //* * THIS IS A TABLE OF THE CALLNATS WITHIN AIAN051
        //* * ADDITIONS OR DELETIONS OF CALLNATS MUST BE
        //* * DONE MANUALLY. HOWEVER, SO LONG AS THE LINE NUMBERS
        //* * ARE IN PARENTHESES, NATURAL WILL UPDATE THE
        //* * LINE NUMBERS AS CODE IS ADDED AND DELETED
        //* *
        //* * AIAN031   (1918)
        //* * AIAN102   (1938)
        //* * AIAN026   (2108)  (2468)
        //* * AIAN021   (2246)  (5304)  (5376)
        //* * AIAN028   (2426)
        //* * AIAN030   (2730)
        //* * AIAN090   (4210)
        //* * AIAN074   (5100)
        //* * IAAN0019  (5458)
        //* * AIAN027R  (1286)  (1300)  (4120)  (4148)  (4174)
        //* *
    }
    private void sub_Call_Aian027_For_Graded_Test() throws Exception                                                                                                      //Natural: CALL-AIAN027-FOR-GRADED-TEST
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pdaAiaa027r.getPnd_Aian027_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN027-LINKAGE
        //*  MOVE  #AIAN102-IN-FUND-CODE  TO  #AIAN027-FUND-INDICATOR
        //*  MOVE  'T'                 TO  #AIAN027-FUND-INDICATOR
        if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.equals(1)) || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2")  //Natural: IF ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY = 01 ) OR ( NAZ-ACT-TYPE-OF-CALL = '2' AND #EFF-DATE-N > 200312 )
            && pnd_Eff_Date_A_Pnd_Eff_Date_N.greater(200312))))
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'I'
            pnd_Ws_Effective_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED NAZ-ACT-ASD-DATE ( EM = YYYYMMDD ) TO #WS-EFFECTIVE-DATE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Effective_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED NAZ-ACT-EFF-DATE ( EM = YYYYMMDD ) TO #WS-EFFECTIVE-DATE-A
            //*   RITE '#WS-EFFECTIVE-DATE-A = ' #WS-EFFECTIVE-DATE-A
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Date().setValue(pnd_Ws_Effective_Date_A_Pnd_Ws_Effective_Date);                                          //Natural: MOVE #WS-EFFECTIVE-DATE TO #AIAN027-EFFECTIVE-DATE
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Day().setValue(0);                                                                                       //Natural: MOVE 00 TO #AIAN027-EFFECTIVE-DAY
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Type_Of_Call().setValue("TG");                                                                                     //Natural: MOVE 'TG' TO #AIAN027-TYPE-OF-CALL
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Option_Code().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde());                                //Natural: MOVE NAZ-ACT-OPTION-CDE TO #AIAN027-OPTION-CODE
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Fund_Indicator().setValue(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Fixed_Fund_Code());                        //Natural: MOVE #IAAN0019-FIXED-FUND-CODE TO #AIAN027-FUND-INDICATOR
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Origin_Product().setValue(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Fixed_Product());                   //Natural: MOVE #IAAN0019-ORIGIN-FIXED-PRODUCT TO #AIAN027-ORIGIN-PRODUCT
        //*   12/01/03  JS
        //*  FOR #X = 1 TO 60
        //* * FOLLOWING CODE ADDED 7/21/2011  SLR (AS PER VE & DY)
        //*  NOT SPIA
        //*  NON-PENSION
        if (condition((pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().notEquals(40) && (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(2)  //Natural: IF ( #IAAN0019-CNTRCT-ORGN-CDE NE 40 ) AND ( #IAAN0019-ORIGIN-FAMILY = 2 OR = 7 )
            || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(7)))))
        {
            pnd_Ws_Effective_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED NAZ-ACT-ASD-DATE ( EM = YYYYMMDD ) TO #WS-EFFECTIVE-DATE-A
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Date().setValue(pnd_Ws_Effective_Date_A_Pnd_Ws_Effective_Date);                                      //Natural: MOVE #WS-EFFECTIVE-DATE TO #AIAN027-EFFECTIVE-DATE
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().nsubtract(1);                                                                                //Natural: SUBTRACT 1 FROM #AIAN027-EFFECTIVE-MONTH
            if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().equals(getZero())))                                                            //Natural: IF #AIAN027-EFFECTIVE-MONTH = 0
            {
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().setValue(12);                                                                            //Natural: MOVE 12 TO #AIAN027-EFFECTIVE-MONTH
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Year().nsubtract(1);                                                                             //Natural: SUBTRACT 1 FROM #AIAN027-EFFECTIVE-YEAR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #X = 1 TO 250
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(250)); pnd_X.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_X).greater(getZero())))                                       //Natural: IF NAZ-ACT-TIAA-GRADED-RATE-AMT ( #X ) > 0
            {
                pnd_Ws_Da_Rate_A.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_X));                                          //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-CDE ( #X ) TO #WS-DA-RATE-A
                //*        MOVE #WS-DA-RATE-N  TO  #AIAN027-DA-RATE
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate().setValue(pnd_Ws_Da_Rate_A);                                                                      //Natural: MOVE #WS-DA-RATE-A TO #AIAN027-DA-RATE
                //*        CALLNAT 'AIAN027' #AIAN027-LINKAGE
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") && pnd_Eff_Date_A_Pnd_Eff_Date_N.greater(200312)))                 //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' AND #EFF-DATE-N > 200312
                {
                    pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate().setValue("00");                                                                              //Natural: MOVE '00' TO #AIAN027-DA-RATE
                    DbsUtil.callnat(Aian027r.class , getCurrentProcessState(), pdaAiaa027r.getPnd_Aian027_Linkage());                                                     //Natural: CALLNAT 'AIAN027R' #AIAN027-LINKAGE
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code_Nbr().notEquals(getZero())))                                                 //Natural: IF #AIAN027-RETURN-CODE-NBR NOT = 0
                    {
                        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code());               //Natural: MOVE #AIAN027-RETURN-CODE TO NAZ-ACT-RETURN-CODE
                        Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                          //Natural: ESCAPE MODULE
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate());                          //Natural: MOVE #AIAN027-IA-RATE TO #AIAN027-DA-RATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Aian027r.class , getCurrentProcessState(), pdaAiaa027r.getPnd_Aian027_Linkage());                                                         //Natural: CALLNAT 'AIAN027R' #AIAN027-LINKAGE
                if (condition(Global.isEscape())) return;
                //*   END FIX
                if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code_Nbr().notEquals(getZero())))                                                     //Natural: IF #AIAN027-RETURN-CODE-NBR NOT = 0
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code());                   //Natural: MOVE #AIAN027-RETURN-CODE TO NAZ-ACT-RETURN-CODE
                    Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                              //Natural: ESCAPE MODULE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate().less(new DbsDecimal(".04000"))))                                       //Natural: IF #AIAN027-TOTAL-INTEREST-RATE < ( .04000 )
                {
                    if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().equals("Y")))                                                             //Natural: IF NAZ-ACT-TIAA-STD-CALL = 'Y'
                    {
                        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_X).nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_X)); //Natural: ADD NAZ-ACT-TIAA-GRADED-RATE-AMT ( #X ) TO NAZ-ACT-TIAA-STD-RATE-AMT ( #X )
                        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_X).setValue(0);                                                 //Natural: MOVE 0 TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( #X )
                        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_X).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_X)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-CDE ( #X ) TO NAZ-ACT-TIAA-STD-RATE-CDE ( #X )
                        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_X).setValue("  ");                                              //Natural: MOVE '  ' TO NAZ-ACT-TIAA-GRADED-RATE-CDE ( #X )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM MOVE-OF-GRD-TO-STAN-FOR-GRD-ONLY-OPTION
                        sub_Move_Of_Grd_To_Stan_For_Grd_Only_Option();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Move_Grd_To_Std.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #WS-MOVE-GRD-TO-STD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  IF NAZ-ACT-TIAA-STD-CALL  =  ' '
        if (condition(pnd_Ws_Move_Grd_To_Std.equals("Y")))                                                                                                                //Natural: IF #WS-MOVE-GRD-TO-STD = 'Y'
        {
            pnd_Y.setValue(1);                                                                                                                                            //Natural: MOVE 1 TO #Y
            FOR06:                                                                                                                                                        //Natural: FOR #X = 1 TO 250
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(250)); pnd_X.nadd(1))
            {
                //*   END FIX
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_X).greater(getZero())))                                   //Natural: IF NAZ-ACT-TIAA-GRADED-RATE-AMT ( #X ) > 0
                {
                    pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Code.getValue(pnd_Y).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_X)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-CDE ( #X ) TO #WS-GRADED-RATE-CODE ( #Y )
                    pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Amt.getValue(pnd_Y).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_X)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-AMT ( #X ) TO #WS-GRADED-RATE-AMT ( #Y )
                    pnd_Y.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Y
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue("*").setValue(pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Code.getValue("*"));        //Natural: MOVE #WS-GRADED-RATE-CODE ( * ) TO NAZ-ACT-TIAA-GRADED-RATE-CDE ( * )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue("*").setValue(pnd_Ws_Graded_Info_Pnd_Ws_Graded_Rate_Amt.getValue("*"));         //Natural: MOVE #WS-GRADED-RATE-AMT ( * ) TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( * )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Of_Grd_To_Stan_For_Grd_Only_Option() throws Exception                                                                                           //Natural: MOVE-OF-GRD-TO-STAN-FOR-GRD-ONLY-OPTION
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #Y = 1 TO 250
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(250)); pnd_Y.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_Y).equals(getZero())))                                           //Natural: IF NAZ-ACT-TIAA-STD-RATE-AMT ( #Y ) = 0
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_Y).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_X)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-AMT ( #X ) TO NAZ-ACT-TIAA-STD-RATE-AMT ( #Y )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_X).setValue(0);                                                         //Natural: MOVE 0 TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( #X )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_Y).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_X)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-CDE ( #X ) TO NAZ-ACT-TIAA-STD-RATE-CDE ( #Y )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_X).setValue("  ");                                                      //Natural: MOVE '  ' TO NAZ-ACT-TIAA-GRADED-RATE-CDE ( #X )
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  CHANGED 7/25/2012  SLR
    private void sub_Check_For_Any_Graded_Left() throws Exception                                                                                                         //Natural: CHECK-FOR-ANY-GRADED-LEFT
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #Y = 1 TO 250
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(250)); pnd_Y.nadd(1))
        {
            //*   END FIX
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_Y).greater(getZero())))                                       //Natural: IF NAZ-ACT-TIAA-GRADED-RATE-AMT ( #Y ) > 0
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().setValue(" ");                                                                                   //Natural: MOVE ' ' TO NAZ-ACT-TIAA-GRADED-CALL
    }
    private void sub_Intitalize_Routine() throws Exception                                                                                                                //Natural: INTITALIZE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Sum_Amounts.reset();                                                                                                                                          //Natural: RESET #SUM-AMOUNTS
        pnd_Number_Of_Rate_Basis.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #NUMBER-OF-RATE-BASIS
        pnd_Irr_Payment_Sum.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #IRR-PAYMENT-SUM
    }
    private void sub_Tiaa_Standard_Routine() throws Exception                                                                                                             //Natural: TIAA-STANDARD-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Type_Of_Call.setValue("TS");                                                                                                                                  //Natural: MOVE 'TS' TO #TYPE-OF-CALL
                                                                                                                                                                          //Natural: PERFORM INTITALIZE-ROUTINE
        sub_Intitalize_Routine();
        if (condition(Global.isEscape())) {return;}
        //*   12/01/03
        //*  FOR #I = 1 TO 60
        collapse_Array_Std_Pos.setValue(0);                                                                                                                               //Natural: MOVE 0 TO STD-POS
        FOR09:                                                                                                                                                            //Natural: FOR #I = 1 TO 250
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
        {
            if (condition(collapse_Array_Std_Pos.equals(getZero())))                                                                                                      //Natural: IF STD-POS = 0
            {
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I).notEquals("00") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I).notEquals("  ")  //Natural: IF NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) NOT = '00' AND NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) NOT = '  ' AND NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) > 0
                    && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I).greater(getZero())))
                {
                    collapse_Array_Std_Pos.setValue(pnd_I);                                                                                                               //Natural: MOVE #I TO STD-POS
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #I = 250 TO 1 STEP -1
        for (pnd_I.setValue(250); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
        {
            if (condition(pnd_I.equals(collapse_Array_Std_Pos)))                                                                                                          //Natural: IF #I = STD-POS
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I).nadd(collapse_Array_Collapse_Std_Total_Acc);                            //Natural: ADD COLLAPSE-STD-TOTAL-ACC TO NAZ-ACT-TIAA-STD-RATE-AMT ( #I )
                //*        RITE (1) '=' COLLAPSE-STD-TOTAL-ACC
                //*        '=' NAZ-ACT-TIAA-STD-RATE-AMT (#I)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I).notEquals("00") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I).notEquals("  ")  //Natural: IF NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) NOT = '00' AND NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) NOT = '  ' AND NAZ-ACT-RETURN-CODE-NBR = 0
                && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))
            {
                pnd_Tiaa_Rate_Cde.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I));                                            //Natural: MOVE NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) TO #TIAA-RATE-CDE
                pnd_Tiaa_Rate_Amt.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I));                                            //Natural: MOVE NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) TO #TIAA-RATE-AMT NAZ-ACT-TIAA-STD-RATE-AMT ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I));
                //* ***
                //* ***   MODAL ADJUSTMENT IS ALWAYS 1.000000 FOR STANDARD CASE
                //* ***
                if (condition(pnd_Tiaa_Rate_Amt.less(new DbsDecimal("-1.00"))))                                                                                           //Natural: IF #TIAA-RATE-AMT < -1.00
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                 //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(208);                                                                        //Natural: MOVE 208 TO NAZ-ACT-RETURN-CODE-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                    if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30))) //Natural: IF NAZ-ACT-OPTION-CDE = 28 OR = 30
                    {
                                                                                                                                                                          //Natural: PERFORM READ-IAA770-FILE
                        sub_Read_Iaa770_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHOOSE-MORTALITY-TABLE
                    sub_Choose_Mortality_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-MODAL-ADJUSTMENT
                    sub_Calculate_Modal_Adjustment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-PAYMENT-BY-OPTION-CODE
                    sub_Calculate_Tiaa_Payment_By_Option_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM MOVE-VARIABLES-TO-STD-OUTPUT
                    sub_Move_Variables_To_Std_Output();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Number_Of_Rate_Basis.nadd(1);                                                                                                                         //Natural: ADD 1 TO #NUMBER-OF-RATE-BASIS
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I.equals(collapse_Array_Std_Pos)))                                                                                                          //Natural: IF #I = STD-POS
            {
                collapse_Array_Collapse_Std_Ia_Rb.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_I));                          //Natural: MOVE NAZ-ACT-TIAA-O-STD-RATE-CDE ( #I ) TO COLLAPSE-STD-IA-RB
                collapse_Array_Collapse_Std_Gic.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_I));                            //Natural: MOVE NAZ-ACT-TIAA-O-STD-GIC-CODE ( #I ) TO COLLAPSE-STD-GIC
                collapse_Array_Collapse_Std_Eff_Rate_Intc.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I));                //Natural: MOVE NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #I ) TO COLLAPSE-STD-EFF-RATE-INTC
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I).nsubtract(collapse_Array_Collapse_Std_Total_Acc);                       //Natural: SUBTRACT COLLAPSE-STD-TOTAL-ACC FROM NAZ-ACT-TIAA-STD-RATE-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR11:                                                                                                                                                            //Natural: FOR #I = 1 STD-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(collapse_Array_Std_Cnt)); pnd_I.nadd(1))
        {
            collapse_Array_Pos_I.setValue(collapse_Array_Collapse_Std_I.getValue(pnd_I));                                                                                 //Natural: MOVE COLLAPSE-STD-I ( #I ) TO POS-I
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Std_Ia_Rb);               //Natural: MOVE COLLAPSE-STD-IA-RB TO NAZ-ACT-TIAA-O-STD-RATE-CDE ( POS-I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Std_Gic);                 //Natural: MOVE COLLAPSE-STD-GIC TO NAZ-ACT-TIAA-O-STD-GIC-CODE ( POS-I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Std_Eff_Rate_Intc);     //Natural: MOVE COLLAPSE-STD-EFF-RATE-INTC TO NAZ-ACT-TIAA-STD-EFF-RATE-INT ( POS-I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Std_Acc.getValue(pnd_I));   //Natural: MOVE COLLAPSE-STD-ACC ( #I ) TO NAZ-ACT-TIAA-STD-RATE-AMT ( POS-I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                                 //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM IS-OPTION-AN-INSTALLMENT-REFUND
            sub_Is_Option_An_Installment_Refund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                                 //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM IS-OPTION-P-AND-I
            sub_Is_Option_P_And_I();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *R #I = 1 TO 60
        //* *IF NAZ-ACT-TIAA-STD-RATE-CDE (#I) NOT = '00' AND
        //* *    NAZ-ACT-TIAA-STD-RATE-CDE (#I) NOT = '  '
        //* *       PERFORM DISPLAY-TIAA-STANDARD-ROUTINE-OUTPUT
        //* *END-IF
        //* *D-FOR
    }
    private void sub_Tiaa_Graded_Routine() throws Exception                                                                                                               //Natural: TIAA-GRADED-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Type_Of_Call.setValue("TG");                                                                                                                                  //Natural: MOVE 'TG' TO #TYPE-OF-CALL
                                                                                                                                                                          //Natural: PERFORM INTITALIZE-ROUTINE
        sub_Intitalize_Routine();
        if (condition(Global.isEscape())) {return;}
        //*   12/01/03
        //*  FOR #I = 1 TO 60
        collapse_Array_Grd_Pos.setValue(0);                                                                                                                               //Natural: MOVE 0 TO GRD-POS
        FOR12:                                                                                                                                                            //Natural: FOR #I = 1 250
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
        {
            if (condition(collapse_Array_Grd_Pos.equals(getZero())))                                                                                                      //Natural: IF GRD-POS = 0
            {
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_I).notEquals("00") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_I).notEquals("  ")  //Natural: IF NAZ-ACT-TIAA-GRADED-RATE-CDE ( #I ) NOT = '00' AND NAZ-ACT-TIAA-GRADED-RATE-CDE ( #I ) NOT = '  ' AND NAZ-ACT-TIAA-GRADED-RATE-AMT ( #I ) > 0
                    && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_I).greater(getZero())))
                {
                    collapse_Array_Grd_Pos.setValue(pnd_I);                                                                                                               //Natural: MOVE #I TO GRD-POS
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR13:                                                                                                                                                            //Natural: FOR #I = 250 TO 1 STEP -1
        for (pnd_I.setValue(250); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
        {
            if (condition(pnd_I.equals(collapse_Array_Grd_Pos)))                                                                                                          //Natural: IF #I = GRD-POS
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_I).nadd(collapse_Array_Collapse_Grd_Total_Acc);                         //Natural: ADD COLLAPSE-GRD-TOTAL-ACC TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_I).notEquals("00") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_I).notEquals("  ")  //Natural: IF NAZ-ACT-TIAA-GRADED-RATE-CDE ( #I ) NOT = '00' AND NAZ-ACT-TIAA-GRADED-RATE-CDE ( #I ) NOT = '  ' AND NAZ-ACT-RETURN-CODE-NBR = 0
                && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))
            {
                pnd_Tiaa_Rate_Cde.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_I));                                         //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-CDE ( #I ) TO #TIAA-RATE-CDE
                pnd_Tiaa_Rate_Amt.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_I));                                         //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-AMT ( #I ) TO #TIAA-RATE-AMT
                if (condition(pnd_Tiaa_Rate_Amt.less(new DbsDecimal("-1.00"))))                                                                                           //Natural: IF #TIAA-RATE-AMT < -1.00
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                 //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(209);                                                                        //Natural: MOVE 209 TO NAZ-ACT-RETURN-CODE-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                    if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30))) //Natural: IF NAZ-ACT-OPTION-CDE = 28 OR = 30
                    {
                                                                                                                                                                          //Natural: PERFORM READ-IAA770-FILE
                        sub_Read_Iaa770_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHOOSE-MORTALITY-TABLE
                    sub_Choose_Mortality_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-MODAL-ADJUSTMENT
                    sub_Calculate_Modal_Adjustment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-PAYMENT-BY-OPTION-CODE
                    sub_Calculate_Tiaa_Payment_By_Option_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM MOVE-VARIABLES-TO-GRD-OUTPUT
                    sub_Move_Variables_To_Grd_Output();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //* *        PERFORM DISPLAY-TIAA-GRADED-ROUTINE-OUTPUT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I.equals(collapse_Array_Grd_Pos)))                                                                                                          //Natural: IF #I = GRD-POS
            {
                collapse_Array_Collapse_Grd_Ia_Rb.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_I));                          //Natural: MOVE NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #I ) TO COLLAPSE-GRD-IA-RB
                collapse_Array_Collapse_Grd_Gic.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(pnd_I));                            //Natural: MOVE NAZ-ACT-TIAA-O-GRD-GIC-CODE ( #I ) TO COLLAPSE-GRD-GIC
                collapse_Array_Collapse_Grd_Eff_Rate_Intc.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_I));                //Natural: MOVE NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( #I ) TO COLLAPSE-GRD-EFF-RATE-INTC
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_I).nsubtract(collapse_Array_Collapse_Grd_Total_Acc);                    //Natural: SUBTRACT COLLAPSE-GRD-TOTAL-ACC FROM NAZ-ACT-TIAA-GRADED-RATE-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR14:                                                                                                                                                            //Natural: FOR #I = 1 GRD-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(collapse_Array_Grd_Cnt)); pnd_I.nadd(1))
        {
            collapse_Array_Pos_I.setValue(collapse_Array_Collapse_Grd_I.getValue(pnd_I));                                                                                 //Natural: MOVE COLLAPSE-GRD-I ( #I ) TO POS-I
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Grd_Ia_Rb);               //Natural: MOVE COLLAPSE-GRD-IA-RB TO NAZ-ACT-TIAA-O-GRD-RATE-CDE ( POS-I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Grd_Gic);                 //Natural: MOVE COLLAPSE-GRD-GIC TO NAZ-ACT-TIAA-O-GRD-GIC-CODE ( POS-I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Grd_Eff_Rate_Intc);     //Natural: MOVE COLLAPSE-GRD-EFF-RATE-INTC TO NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( POS-I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(collapse_Array_Pos_I).setValue(collapse_Array_Collapse_Grd_Acc.getValue(pnd_I)); //Natural: MOVE COLLAPSE-GRD-ACC ( #I ) TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( POS-I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Cref_Annual_Routine() throws Exception                                                                                                               //Natural: CREF-ANNUAL-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Type_Of_Call.setValue("CA");                                                                                                                                  //Natural: MOVE 'CA' TO #TYPE-OF-CALL
                                                                                                                                                                          //Natural: PERFORM INTITALIZE-ROUTINE
        sub_Intitalize_Routine();
        if (condition(Global.isEscape())) {return;}
        FOR15:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(pnd_I).notEquals("00") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(pnd_I).notEquals("  ")  //Natural: IF NAZ-ACT-CREF-ANNUAL-RATE-CDE ( #I ) NOT = '00' AND NAZ-ACT-CREF-ANNUAL-RATE-CDE ( #I ) NOT = '  ' AND NAZ-ACT-RETURN-CODE-NBR = 0
                && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))
            {
                //* *         MOVE NAZ-ACT-CREF-ANNUAL-RATE-CDE (#I) TO #CREF-RATE
                pnd_Cref_Rate_Cde.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(pnd_I));                                         //Natural: MOVE NAZ-ACT-CREF-ANNUAL-RATE-CDE ( #I ) TO #CREF-RATE-CDE
                pnd_Cref_Rate_Amt.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Amt().getValue(pnd_I));                                         //Natural: MOVE NAZ-ACT-CREF-ANNUAL-RATE-AMT ( #I ) TO #CREF-RATE-AMT
                if (condition(pnd_Cref_Rate_Amt.less(new DbsDecimal("0.00"))))                                                                                            //Natural: IF #CREF-RATE-AMT < 0.00
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                 //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(210);                                                                        //Natural: MOVE 210 TO NAZ-ACT-RETURN-CODE-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CONVERT-CREF-RB-TO-LETTER-CODE
                    sub_Convert_Cref_Rb_To_Letter_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CHOOSE-MORTALITY-TABLE
                    sub_Choose_Mortality_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-PAYMENT-BY-OPTION-CODE
                    sub_Calculate_Cref_Payment_By_Option_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM MOVE-VARIABLES-TO-ANNUAL-OUTPUT
                    sub_Move_Variables_To_Annual_Output();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //* *         PERFORM DISPLAY-CREF-ANNUAL-ROUTINE-OUTPUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Cref_Monthly_Routine() throws Exception                                                                                                              //Natural: CREF-MONTHLY-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Type_Of_Call.setValue("CM");                                                                                                                                  //Natural: MOVE 'CM' TO #TYPE-OF-CALL
                                                                                                                                                                          //Natural: PERFORM INTITALIZE-ROUTINE
        sub_Intitalize_Routine();
        if (condition(Global.isEscape())) {return;}
        FOR16:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(pnd_I).notEquals("00") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(pnd_I).notEquals("  "))) //Natural: IF NAZ-ACT-CREF-MTHLY-RATE-CDE ( #I ) NOT = '00' AND NAZ-ACT-CREF-MTHLY-RATE-CDE ( #I ) NOT = '  '
            {
                pnd_Cref_Rate_Cde.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(pnd_I));                                          //Natural: MOVE NAZ-ACT-CREF-MTHLY-RATE-CDE ( #I ) TO #CREF-RATE-CDE
                pnd_Cref_Rate_Amt.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Amt().getValue(pnd_I));                                          //Natural: MOVE NAZ-ACT-CREF-MTHLY-RATE-AMT ( #I ) TO #CREF-RATE-AMT
                if (condition(pnd_Cref_Rate_Amt.less(new DbsDecimal("0.00"))))                                                                                            //Natural: IF #CREF-RATE-AMT < 0.00
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                 //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(211);                                                                        //Natural: MOVE 211 TO NAZ-ACT-RETURN-CODE-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CONVERT-CREF-RB-TO-LETTER-CODE
                    sub_Convert_Cref_Rb_To_Letter_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CHOOSE-MORTALITY-TABLE
                    sub_Choose_Mortality_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-PAYMENT-BY-OPTION-CODE
                    sub_Calculate_Cref_Payment_By_Option_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF NAZ-ACT-RETURN-CODE-NBR = 0
                {
                                                                                                                                                                          //Natural: PERFORM MOVE-VARIABLES-TO-MONTHLY-OUTPUT
                    sub_Move_Variables_To_Monthly_Output();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //* *         PERFORM DISPLAY-CREF-MONTHLY-ROUTINE-OUTPUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Convert_Cref_Rb_To_Letter_Code() throws Exception                                                                                                    //Natural: CONVERT-CREF-RB-TO-LETTER-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Aian031_Linkage.reset();                                                                                                                                      //Natural: RESET #AIAN031-LINKAGE
        pnd_Aian031_Linkage_Pnd_Aian031_In_Rate_Basis.setValue(pnd_Cref_Rate_Cde);                                                                                        //Natural: MOVE #CREF-RATE-CDE TO #AIAN031-IN-RATE-BASIS
        //* *ITE 'AIAN031 LINKAGE-BEFORE ' #AIAN031-LINKAGE
        DbsUtil.callnat(Aian031.class , getCurrentProcessState(), pnd_Aian031_Linkage);                                                                                   //Natural: CALLNAT 'AIAN031' #AIAN031-LINKAGE
        if (condition(Global.isEscape())) return;
        //* *ITE 'AIAN031 LINKAGE-AFTER ' #AIAN031-LINKAGE
        if (condition(pnd_Aian031_Linkage_Pnd_Aian031_Return_Code.equals(getZero())))                                                                                     //Natural: IF #AIAN031-RETURN-CODE = 0
        {
            pnd_Ia_Fund_Code.setValue(pnd_Aian031_Linkage_Pnd_Aian031_Out_Rate_Code);                                                                                     //Natural: MOVE #AIAN031-OUT-RATE-CODE TO #IA-FUND-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN031 ");                                                                         //Natural: MOVE 'AIAN031 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(pnd_Aian031_Linkage_Pnd_Aian031_Return_Code);                                        //Natural: MOVE #AIAN031-RETURN-CODE TO NAZ-ACT-RETURN-CODE-NBR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial102.getPnd_Aian102_Linkage_Pnd_Aian102_In_Fund_Code().setValue(pnd_Ia_Fund_Code);                                                                          //Natural: MOVE #IA-FUND-CODE TO #AIAN102-IN-FUND-CODE
        DbsUtil.callnat(Aian102.class , getCurrentProcessState(), pdaAial102.getPnd_Aian102_Linkage());                                                                   //Natural: CALLNAT 'AIAN102' #AIAN102-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAial102.getPnd_Aian102_Linkage_Pnd_Aian102_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN102-RETURN-CODE-NBR NOT EQUAL TO 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAial102.getPnd_Aian102_Linkage_Pnd_Aian102_Return_Code());                            //Natural: MOVE #AIAN102-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
        //* *CIDE FOR FIRST CONDITION
        //* *WHEN #CREF-RATE-CDE = '38'
        //* *  MOVE 'C' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '36'
        //* *  MOVE 'C' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '20'
        //* *  MOVE 'M' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '30'
        //* *  MOVE 'M' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '19'
        //* *  MOVE 'B' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '29'
        //* *  MOVE 'B' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '18'
        //* *  MOVE 'S' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '28'
        //* *  MOVE 'S' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '17'
        //* *  MOVE 'W' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '16'
        //* *  MOVE 'L' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '15'
        //* *  MOVE 'E' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '14'
        //* *  MOVE 'R' TO #IA-FUND-CODE
        //* *WHEN #CREF-RATE-CDE = '13'
        //* *  MOVE 'F' TO #IA-FUND-CODE
        //* *WHEN NONE
        //* *  MOVE 201 TO NAZ-ACT-RETURN-CODE
        //* *   RITE 'INVALID CREF RATE ' #I #CREF-RATE-CDE
        //* *  ESCAPE ROUTINE
        //* *D-DECIDE
    }
    private void sub_Fill_Aian026_Linkage() throws Exception                                                                                                              //Natural: FILL-AIAN026-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pdaAiaa026.getPnd_Aian026_Linkage().reset();                                                                                                                      //Natural: RESET #AIAN026-LINKAGE
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR ( #IAAN0019-CNTRCT-ORGN-CDE = 40 )
            && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) || (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40))))
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N8);                                                   //Natural: MOVE #EFF-DATE-N8 TO #AIAN026-IAUV-REQUEST-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N8);                                               //Natural: MOVE #ISSUE-DATE-N8 TO #AIAN026-IAUV-REQUEST-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(pnd_Ia_Fund_Code);                                                                          //Natural: MOVE #IA-FUND-CODE TO #AIAN026-IA-FUND-CODE
        //*  IF NAZ-ACT-TYPE-OF-CALL ='2'
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'I'
        //*  IF #ISSUE-DATE-DAY > 01
        //*    MOVE 'F' TO #AIAN026-CALL-TYPE
        //*  ELSE
        //*    MOVE 'S' TO #AIAN026-CALL-TYPE
        //*  END-IF
        //*  ELSE
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'S'
        //*    MOVE 'F' TO #AIAN026-CALL-TYPE
        //*  ELSE
        //*    MOVE NAZ-ACT-TYPE-OF-CALL TO #AIAN026-CALL-TYPE
        //*  END-IF
        //*  END-IF
        //*   RITE '#AIAN026-CALL-TYPE' #AIAN026-CALL-TYPE
        if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) ||                     //Natural: IF ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR NAZ-ACT-TYPE-OF-CALL = '2' OR #IAAN0019-CNTRCT-ORGN-CDE = 40 THEN
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("F");                                                                                      //Natural: MOVE 'F' TO #AIAN026-CALL-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("S");                                                                                      //Natural: MOVE 'S' TO #AIAN026-CALL-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Type_Of_Call.equals("CA")))                                                                                                                     //Natural: IF #TYPE-OF-CALL = 'CA'
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("A");                                                                             //Natural: MOVE 'A' TO #AIAN026-REVALUATION-METHOD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Type_Of_Call.equals("CM")))                                                                                                                     //Natural: IF #TYPE-OF-CALL = 'CM'
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("M");                                                                             //Natural: MOVE 'M' TO #AIAN026-REVALUATION-METHOD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Aian026() throws Exception                                                                                                                      //Natural: CALL-AIAN026
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* *ITE 'BEFORE AIAN026' #AIAN026-LINKAGE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        //* *ITE 'AFTER AIAN026' #AIAN026-LINKAGE
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                            //Natural: MOVE #AIAN026-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
        //* *ITE 'AFTER AIAN026' #AIAN026-LINKAGE
    }
    private void sub_Fill_Aian021_Linkage() throws Exception                                                                                                              //Natural: FILL-AIAN021-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pdaAial0210.getPnd_Aian021_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN021-LINKAGE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(0);                                                                                //Natural: MOVE 0 TO #AIAN021-IN-DEF-INTEREST-RATE
        //* *****
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR ( #IAAN0019-CNTRCT-ORGN-CDE = 40 AND #ISSUE-DATE-DAY > 01 )
            && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) || (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40) && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1))))
        {
            //*   OR  NAZ-ACT-TYPE-OF-CALL = 'S'
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Issue_Date().setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N8);                                                      //Natural: MOVE #EFF-DATE-N8 TO #AIAN021-IN-ISSUE-DATE
            if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().equals("IR")))                                                                       //Natural: IF #AIAN028-OUT-OPTION = 'IR'
            {
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Issue_Date().setValue(1);                                                                               //Natural: MOVE 01 TO #AIAN021-IN-ISSUE-DATE
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Issue_Year().setValue(pnd_Ws_Date_Pnd_Ws_Year);                                                         //Natural: MOVE #WS-YEAR TO #AIAN021-IN-ISSUE-YEAR
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Issue_Month().setValue(pnd_Ws_Date_Pnd_Ws_Month);                                                       //Natural: MOVE #WS-MONTH TO #AIAN021-IN-ISSUE-MONTH
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Issue_Date().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N8);                                                  //Natural: MOVE #ISSUE-DATE-N8 TO #AIAN021-IN-ISSUE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Mode().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode());                            //Natural: MOVE NAZ-ACT-PYMNT-MODE TO #AIAN021-IN-PAYMENT-MODE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option());                          //Natural: MOVE #AIAN028-OUT-OPTION TO #AIAN021-IN-PAYMENT-OPTION
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Pct_To_Second_Ann().setValue(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Pct_To_2nd_Ann());               //Natural: MOVE #AIAN028-OUT-PCT-TO-2ND-ANN TO #AIAN021-IN-PCT-TO-SECOND-ANN
        pnd_1st_Ann_Sex_A.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex());                                                                              //Natural: MOVE NAZ-ACT-ANN-SEX TO #1ST-ANN-SEX-A
        pnd_2nd_Ann_Sex_A.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex());                                                                          //Natural: MOVE NAZ-ACT-2ND-ANN-SEX TO #2ND-ANN-SEX-A
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Sex().setValue(pnd_1st_Ann_Sex_A_Pnd_1st_Ann_Sex_N);                                                    //Natural: MOVE #1ST-ANN-SEX-N TO #AIAN021-IN-1ST-ANN-SEX
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_2nd_Ann_Sex().setValue(pnd_2nd_Ann_Sex_A_Pnd_2nd_Ann_Sex_N);                                                    //Natural: MOVE #2ND-ANN-SEX-N TO #AIAN021-IN-2ND-ANN-SEX
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_1st_Ann_Birth_Date().setValue(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N);                               //Natural: MOVE #1ST-ANN-BIRTH-DATE-N TO #AIAN021-IN-1ST-ANN-BIRTH-DATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_2nd_Ann_Birth_Date().setValue(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N);                               //Natural: MOVE #2ND-ANN-BIRTH-DATE-N TO #AIAN021-IN-2ND-ANN-BIRTH-DATE
        //* *COMPUTE ROUNDED #CERTAIN-PERIOD = NAZ-ACT-GUAR-PERIOD +
        //* *             (NAZ-ACT-GUAR-PERIOD-MTH / 12)
        pnd_Certain_Period.compute(new ComputeParameters(true, pnd_Certain_Period), pnd_Ws_Guar_Period.add((pnd_Ws_Guar_Period_Mth.divide(12))));                         //Natural: COMPUTE ROUNDED #CERTAIN-PERIOD = #WS-GUAR-PERIOD + ( #WS-GUAR-PERIOD-MTH / 12 )
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period().setValue(pnd_Certain_Period);                                                                  //Natural: MOVE #CERTAIN-PERIOD TO #AIAN021-IN-CERTAIN-PERIOD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Certain_Period().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period());                          //Natural: MOVE #AIAN021-IN-CERTAIN-PERIOD TO NAZ-CERTAIN-PERIOD
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2")))                                                                          //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2'
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'F'
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().compute(new ComputeParameters(true, pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period()),  //Natural: COMPUTE ROUNDED #AIAN021-IN-DEFERRED-PERIOD = #AIAN030-DAYS-BETWEEN-TWO-DAYS - 1
                pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days.subtract(1));
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period().setValue(pnd_Final_Certain_Period);                                                        //Natural: MOVE #FINAL-CERTAIN-PERIOD TO #AIAN021-IN-CERTAIN-PERIOD
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Deferral_Period().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period());                    //Natural: MOVE #AIAN021-IN-DEFERRED-PERIOD TO NAZ-DEFERRAL-PERIOD
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Certain_Period().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period());                      //Natural: MOVE #AIAN021-IN-CERTAIN-PERIOD TO NAZ-CERTAIN-PERIOD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) ||                    //Natural: IF ( ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR #IAAN0019-CNTRCT-ORGN-CDE = 40 ) AND #AIAN028-OUT-OPTION NOT = 'IR'
            pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)) && pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().notEquals("IR")))
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().setValue(pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days);                          //Natural: MOVE #AIAN030-DAYS-BETWEEN-TWO-DAYS TO #AIAN021-IN-DEFERRED-PERIOD
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period().setValue(pnd_Final_Certain_Period);                                                        //Natural: MOVE #FINAL-CERTAIN-PERIOD TO #AIAN021-IN-CERTAIN-PERIOD
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Deferral_Period().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period());                    //Natural: MOVE #AIAN021-IN-DEFERRED-PERIOD TO NAZ-DEFERRAL-PERIOD
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Certain_Period().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period());                      //Natural: MOVE #AIAN021-IN-CERTAIN-PERIOD TO NAZ-CERTAIN-PERIOD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Aian021() throws Exception                                                                                                                      //Natural: CALL-AIAN021
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* * RITE '=' *PROGRAM '=' *LINE '=' #AIAN021-IN-MORTALITY-TABLE
        DbsUtil.callnat(Aian021.class , getCurrentProcessState(), pdaAial0210.getPnd_Aian021_Linkage());                                                                  //Natural: CALLNAT 'AIAN021' #AIAN021-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code_Nbr().notEquals(getZero())))                                                         //Natural: IF #AIAN021-OUT-RETURN-CODE-NBR NOT = 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code());                       //Natural: MOVE #AIAN021-OUT-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30))   //Natural: IF ( NAZ-ACT-OPTION-CDE = 28 OR = 30 ) AND #ISSUE-DATE-N < 200111
            && pnd_Issue_Date_A_Pnd_Issue_Date_N.less(200111))))
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().compute(new ComputeParameters(false, pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()),  //Natural: COMPUTE #AIAN021-OUT-ANNUITY-FACTOR = #AIAN021-OUT-ANNUITY-FACTOR / ( 1 + #AIAN021-IN-INTEREST-RATE )
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().divide((DbsField.add(1,pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate()))));
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Found_Linkage.setValue("N");                                                                                                                                  //Natural: MOVE 'N' TO #FOUND-LINKAGE
        pnd_Aian021_Linkage_X.setValue(pdaAial0210.getPnd_Aian021_Linkage());                                                                                             //Natural: MOVE #AIAN021-LINKAGE TO #AIAN021-LINKAGE-X
        FOR17:                                                                                                                                                            //Natural: FOR #LINK-ARRAY-INDEX = 1 TO 250
        for (pnd_Link_Array_Index.setValue(1); condition(pnd_Link_Array_Index.lessOrEqual(250)); pnd_Link_Array_Index.nadd(1))
        {
            if (condition(pnd_Aian021_Linkage_Array.getValue(pnd_Link_Array_Index).equals(" ")))                                                                          //Natural: IF #AIAN021-LINKAGE-ARRAY ( #LINK-ARRAY-INDEX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Aian021_Linkage_Array.getValue(pnd_Link_Array_Index).equals(pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Input)))                          //Natural: IF #AIAN021-LINKAGE-ARRAY ( #LINK-ARRAY-INDEX ) = #AIAN021-LINKAGE-INPUT
                {
                    pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().setValue(pnd_Ann_Factor.getValue(pnd_Link_Array_Index));                          //Natural: MOVE #ANN-FACTOR ( #LINK-ARRAY-INDEX ) TO #AIAN021-OUT-ANNUITY-FACTOR
                    pnd_Found_Linkage.setValue("Y");                                                                                                                      //Natural: MOVE 'Y' TO #FOUND-LINKAGE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Found_Linkage.equals("N")))                                                                                                                     //Natural: IF #FOUND-LINKAGE = 'N'
        {
            pnd_Aian021_Linkage_Array.getValue(pnd_Link_Array_Index).setValue(pnd_Aian021_Linkage_X_Pnd_Aian021_Linkage_Input);                                           //Natural: MOVE #AIAN021-LINKAGE-INPUT TO #AIAN021-LINKAGE-ARRAY ( #LINK-ARRAY-INDEX )
            pnd_Ann_Factor.getValue(pnd_Link_Array_Index).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());                                  //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #ANN-FACTOR ( #LINK-ARRAY-INDEX )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Aian021_Type.equals("TT")))                                                                                                                  //Natural: IF #WS-AIAN021-TYPE = 'TT'
        {
            if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                                 //Natural: IF #TYPE-OF-CALL = 'TG'
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Mort_Table_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table()); //Natural: MOVE #AIAN021-IN-MORTALITY-TABLE TO NAZ-TOTAL-MORT-TABLE-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Set_Back_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback());     //Natural: MOVE #AIAN021-IN-SETBACK TO NAZ-TOTAL-SET-BACK-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Interest_Rate_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate()); //Natural: MOVE #AIAN021-IN-INTEREST-RATE TO NAZ-TOTAL-INTEREST-RATE-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Total_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age1());               //Natural: MOVE #AIAN021-AGE1 TO NAZ-AGE1-TOTAL-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Total_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age2());               //Natural: MOVE #AIAN021-AGE2 TO NAZ-AGE2-TOTAL-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-TOTAL-ANN-FACTOR-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Aian021_Linkage_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage());                     //Natural: MOVE #AIAN021-LINKAGE TO NAZ-TOTAL-AIAN021-LINKAGE-GRD ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Mort_Table().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table()); //Natural: MOVE #AIAN021-IN-MORTALITY-TABLE TO NAZ-TOTAL-MORT-TABLE ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Set_Back().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback());         //Natural: MOVE #AIAN021-IN-SETBACK TO NAZ-TOTAL-SET-BACK ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Interest_Rate().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate()); //Natural: MOVE #AIAN021-IN-INTEREST-RATE TO NAZ-TOTAL-INTEREST-RATE ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Total().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age1());                   //Natural: MOVE #AIAN021-AGE1 TO NAZ-AGE1-TOTAL ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Total().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age2());                   //Natural: MOVE #AIAN021-AGE2 TO NAZ-AGE2-TOTAL ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-TOTAL-ANN-FACTOR ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Aian021_Linkage_Array().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage());                   //Natural: MOVE #AIAN021-LINKAGE TO NAZ-TOTAL-AIAN021-LINKAGE-ARRAY ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Aian021_Type.equals("TG")))                                                                                                                  //Natural: IF #WS-AIAN021-TYPE = 'TG'
        {
            if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                                 //Natural: IF #TYPE-OF-CALL = 'TG'
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Mort_Table_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table()); //Natural: MOVE #AIAN021-IN-MORTALITY-TABLE TO NAZ-GUAR-MORT-TABLE-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Set_Back_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback());      //Natural: MOVE #AIAN021-IN-SETBACK TO NAZ-GUAR-SET-BACK-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Interest_Rate_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate()); //Natural: MOVE #AIAN021-IN-INTEREST-RATE TO NAZ-GUAR-INTEREST-RATE-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Guar_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age1());                //Natural: MOVE #AIAN021-AGE1 TO NAZ-AGE1-GUAR-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Guar_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age2());                //Natural: MOVE #AIAN021-AGE2 TO NAZ-AGE2-GUAR-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-GUAR-ANN-FACTOR-GRD ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Aian021_Linkage_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage());                      //Natural: MOVE #AIAN021-LINKAGE TO NAZ-GUAR-AIAN021-LINKAGE-GRD ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Mort_Table().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table()); //Natural: MOVE #AIAN021-IN-MORTALITY-TABLE TO NAZ-GUAR-MORT-TABLE ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Set_Back().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback());          //Natural: MOVE #AIAN021-IN-SETBACK TO NAZ-GUAR-SET-BACK ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Interest_Rate().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate()); //Natural: MOVE #AIAN021-IN-INTEREST-RATE TO NAZ-GUAR-INTEREST-RATE ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Guar().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age1());                    //Natural: MOVE #AIAN021-AGE1 TO NAZ-AGE1-GUAR ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Guar().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age2());                    //Natural: MOVE #AIAN021-AGE2 TO NAZ-AGE2-GUAR ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-GUAR-ANN-FACTOR ( #I )
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Aian021_Linkage_Array().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage());                    //Natural: MOVE #AIAN021-LINKAGE TO NAZ-GUAR-AIAN021-LINKAGE-ARRAY ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Aian021_Type.equals("CT")))                                                                                                                  //Natural: IF #WS-AIAN021-TYPE = 'CT'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Mortality().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table());     //Natural: MOVE #AIAN021-IN-MORTALITY-TABLE TO NAZ-CREF-MORTALITY ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Setback().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback());               //Natural: MOVE #AIAN021-IN-SETBACK TO NAZ-CREF-SETBACK ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Int_Rate().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate());        //Natural: MOVE #AIAN021-IN-INTEREST-RATE TO NAZ-CREF-INT-RATE ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Cref().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age1());                        //Natural: MOVE #AIAN021-AGE1 TO NAZ-AGE1-CREF ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Cref().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Age2());                        //Natural: MOVE #AIAN021-AGE2 TO NAZ-AGE2-CREF ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());    //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-CREF-ANN-FACTOR ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Aian021_Linkage_Array().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage());                        //Natural: MOVE #AIAN021-LINKAGE TO NAZ-CREF-AIAN021-LINKAGE-ARRAY ( #I )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Aian028_To_Convert_Option() throws Exception                                                                                                    //Natural: CALL-AIAN028-TO-CONVERT-OPTION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* * RITE 'BEFORE LINKAGE AIAN028' #AIAN028-LINKAGE
        pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_In_Option().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde());                                   //Natural: MOVE NAZ-ACT-OPTION-CDE TO #AIAN028-IN-OPTION
        //*   RITE 'NAZ-OPTION ' NAZ-ACT-OPTION-CDE
        pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Type_Of_Call().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call());                              //Natural: MOVE NAZ-ACT-TYPE-OF-CALL TO #AIAN028-TYPE-OF-CALL
        //*  IF NAZ-ACT-TYPE-OF-CALL ='1' MOVE 'I' TO #AIAN028-TYPE-OF-CALL
        //*  END-IF
        pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Tiaa_Cntrct_Nmbr().getValue("*").setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Cntrct_Nmbr().getValue("*")); //Natural: MOVE NAZ-ACT-TIAA-CNTRCT-NMBR ( * ) TO #AIAN028-TIAA-CNTRCT-NMBR ( * )
        pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Cref_Cert_Nmbr().getValue("*").setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Cert_Nmbr().getValue("*")); //Natural: MOVE NAZ-ACT-CREF-CERT-NMBR ( * ) TO #AIAN028-CREF-CERT-NMBR ( * )
        DbsUtil.callnat(Aian028.class , getCurrentProcessState(), pdaAiaa028.getPnd_Aian028_Linkage());                                                                   //Natural: CALLNAT 'AIAN028' #AIAN028-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Return_Code_Nbr().notEquals(getZero())))                                                          //Natural: IF #AIAN028-OUT-RETURN-CODE-NBR NOT = 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Return_Code());                        //Natural: MOVE #AIAN028-OUT-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            //* *      RITE 'INVALID OPTION CODE ' NAZ-ACT-OPTION-CDE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *   RITE 'GUAR-YEARS ' #AIAN028-OUT-GUAR-YEARS
        //* *   RITE 'GUAR-MONTHS ' #AIAN028-OUT-GUAR-MONTHS
        if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Guar_Years().greater(getZero()) || pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Guar_Months().greater(getZero()))) //Natural: IF #AIAN028-OUT-GUAR-YEARS > 0 OR #AIAN028-OUT-GUAR-MONTHS > 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Guar_Years());                         //Natural: MOVE #AIAN028-OUT-GUAR-YEARS TO NAZ-ACT-GUAR-PERIOD
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth().setValue(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Guar_Months());                    //Natural: MOVE #AIAN028-OUT-GUAR-MONTHS TO NAZ-ACT-GUAR-PERIOD-MTH
        }                                                                                                                                                                 //Natural: END-IF
        //* * RITE 'AFTER LINKAGE AIAN028' #AIAN028-LINKAGE
    }
    private void sub_Get_Cutoff_Date() throws Exception                                                                                                                   //Natural: GET-CUTOFF-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("C");                                                                                          //Natural: MOVE 'C' TO #AIAN026-CALL-TYPE
        //*   RITE 'GET CUTOFF '
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N8);                                                   //Natural: MOVE #ISSUE-DATE-N8 TO #AIAN026-IAUV-REQUEST-DATE
        //*   RITE 'DATE' #AIAN026-IAUV-REQUEST-DATE
        //* *CALLNAT 'AIAN0264' #AIAN026-LINKAGE --- AIAN0264 NOW OBSOLETE
        //*  0264 FUNCTIONALITY NOW IN 026
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAN026-RETURN-CODE-NBR NOT = 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code());                            //Natural: MOVE #AIAN026-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cutoff_Date_Pnd_Cutoff_Day.setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Day_Returned());                                                       //Natural: MOVE #AIAN026-IAUV-DAY-RETURNED TO #CUTOFF-DAY
        //*   RITE 'CUT DATE ' #AIAN026-IAUV-DAY-RETURNED         #CUTOFF-DAY
    }
    private void sub_Fill_Aian030_Linkage() throws Exception                                                                                                              //Natural: FILL-AIAN030-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'S'
        if (condition(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))                                                                        //Natural: IF #IAAN0019-CNTRCT-ORGN-CDE = 40
        {
            //* *MOVE #ISSUE-DATE-N8 TO #AIAN030-BEGIN-DATE-N
            pnd_Ws_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N8);                                                                                                     //Natural: MOVE #ISSUE-DATE-N8 TO #WS-DATE
            pnd_Ws_Date_Pnd_Ws_Day.setValue(1);                                                                                                                           //Natural: MOVE 01 TO #WS-DAY
            //*   RITE 'ISSUE' #ISSUE-DATE-N  #WS-DATE
            //*   RITE 'CUT' #CUTOFF-DAY
            //*   RITE 'MODE' #MODE-1
            short decideConditionsMet1788 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MODE-1 = 1
            if (condition(pnd_Mode_Pnd_Mode_1.equals(1)))
            {
                decideConditionsMet1788++;
                pnd_Ws_Date_Pnd_Ws_Month.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-MONTH
                //*      IF #ISSUE-DATE-DAY > #CUTOFF-DAY               /* DY1
                //*  DY1
                if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(21)))                                                                                           //Natural: IF #ISSUE-DATE-DAY > 21
                {
                    pnd_Ws_Date_Pnd_Ws_Month.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-MONTH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #MODE-1 = 6
            else if (condition(pnd_Mode_Pnd_Mode_1.equals(6)))
            {
                decideConditionsMet1788++;
                pnd_Ws_Date_Pnd_Ws_Month.nadd(3);                                                                                                                         //Natural: ADD 3 TO #WS-MONTH
            }                                                                                                                                                             //Natural: WHEN #MODE-1 = 7
            else if (condition(pnd_Mode_Pnd_Mode_1.equals(7)))
            {
                decideConditionsMet1788++;
                pnd_Ws_Date_Pnd_Ws_Month.nadd(6);                                                                                                                         //Natural: ADD 6 TO #WS-MONTH
            }                                                                                                                                                             //Natural: WHEN #MODE-1 = 8
            else if (condition(pnd_Mode_Pnd_Mode_1.equals(8)))
            {
                decideConditionsMet1788++;
                pnd_Ws_Date_Pnd_Ws_Month.nadd(12);                                                                                                                        //Natural: ADD 12 TO #WS-MONTH
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  IF #ISSUE-DATE-DAY > #CUTOFF-DAY
            //*    ADD 2 TO #WS-MONTH ELSE
            //*    ADD 1 TO #WS-MONTH
            //*  END-IF
            if (condition(pnd_Ws_Date_Pnd_Ws_Month.greater(12)))                                                                                                          //Natural: IF #WS-MONTH > 12
            {
                pnd_Ws_Date_Pnd_Ws_Month.nsubtract(12);                                                                                                                   //Natural: SUBTRACT 12 FROM #WS-MONTH
                pnd_Ws_Date_Pnd_Ws_Year.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WS-YEAR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N.setValue(pnd_Ws_Date);                                                                                             //Natural: MOVE #WS-DATE TO #AIAN030-END-DATE-N
            //*   RITE 'WS' #WS-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2")))                                                                      //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2'
            {
                //*  ELSE IF NAZ-ACT-TYPE-OF-CALL = 'F'
                pnd_Ws_Date.setValue(pnd_Next_Ia_Payment_Date);                                                                                                           //Natural: MOVE #NEXT-IA-PAYMENT-DATE TO #WS-DATE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-NEXT-PMT
                sub_Determine_Next_Pmt();
                if (condition(Global.isEscape())) {return;}
                pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N.setValue(pnd_Ws_Date);                                                                                         //Natural: MOVE #WS-DATE TO #AIAN030-END-DATE-N
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1))))              //Natural: IF ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 )
                {
                    //*    IF (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
                    pnd_Ws_Date_Pnd_Ws_Year.setValue(pnd_Issue_Date_A_Pnd_Issue_Year_N);                                                                                  //Natural: MOVE #ISSUE-YEAR-N TO #WS-YEAR
                    pnd_Ws_Date_Pnd_Ws_Month.setValue(pnd_Issue_Date_A_Pnd_Issue_Month_N);                                                                                //Natural: MOVE #ISSUE-MONTH-N TO #WS-MONTH
                    pnd_Ws_Date_Pnd_Ws_Day.setValue(1);                                                                                                                   //Natural: MOVE 01 TO #WS-DAY
                    pnd_Ws_Date_Pnd_Ws_Month.nadd(pnd_Months_Between_Payments);                                                                                           //Natural: ADD #MONTHS-BETWEEN-PAYMENTS TO #WS-MONTH
                    if (condition(pnd_Ws_Date_Pnd_Ws_Month.greater(12)))                                                                                                  //Natural: IF #WS-MONTH > 12
                    {
                        pnd_Ws_Date_Pnd_Ws_Month.nsubtract(12);                                                                                                           //Natural: SUBTRACT 12 FROM #WS-MONTH
                        pnd_Ws_Date_Pnd_Ws_Year.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WS-YEAR
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N.setValue(pnd_Ws_Date);                                                                                     //Natural: MOVE #WS-DATE TO #AIAN030-END-DATE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Aian030_Linkage_Pnd_Aian030_End_Date_N.setValue(pnd_Next_Ia_Payment_Date);                                                                        //Natural: MOVE #NEXT-IA-PAYMENT-DATE TO #AIAN030-END-DATE-N
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) ||                     //Natural: IF ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR #IAAN0019-CNTRCT-ORGN-CDE = 40
            pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))
        {
            pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N8);                                                                    //Natural: MOVE #ISSUE-DATE-N8 TO #AIAN030-BEGIN-DATE-N
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Aian030_Linkage_Pnd_Aian030_Begin_Date_N.setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N8);                                                                        //Natural: MOVE #EFF-DATE-N8 TO #AIAN030-BEGIN-DATE-N
        }                                                                                                                                                                 //Natural: END-IF
        //*   RITE ' NEXTDATE'  #AIAN030-END-DATE-N
        //*   RITE 'END  ' #AIAN030-END-DATE-N
        //*   RITE 'NAZ ' NAZ-ACT-TYPE-OF-CALL
        //*   RITE 'BEGIN ' #AIAN030-BEGIN-DATE-N
    }
    private void sub_Determine_Next_Pmt() throws Exception                                                                                                                //Natural: DETERMINE-NEXT-PMT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*   RITE 'NEXT-DAY' #WS-DATE
        pnd_Pmt_Month_1.setValue(pnd_Issue_Date_A_Pnd_Issue_Month_N);                                                                                                     //Natural: MOVE #ISSUE-MONTH-N TO #PMT-MONTH-1
        pnd_Pmt_Month_2.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #PMT-MONTH-2
        pnd_Pmt_Month_3.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #PMT-MONTH-3
        pnd_Pmt_Month_4.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #PMT-MONTH-4
        short decideConditionsMet1855 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MODE-1 = 6
        if (condition(pnd_Mode_Pnd_Mode_1.equals(6)))
        {
            decideConditionsMet1855++;
            pnd_Pmt_Month_2.compute(new ComputeParameters(false, pnd_Pmt_Month_2), pnd_Pmt_Month_1.add(3));                                                               //Natural: COMPUTE #PMT-MONTH-2 = #PMT-MONTH-1 + 3
            pnd_Pmt_Month_3.compute(new ComputeParameters(false, pnd_Pmt_Month_3), pnd_Pmt_Month_1.add(6));                                                               //Natural: COMPUTE #PMT-MONTH-3 = #PMT-MONTH-1 + 6
            pnd_Pmt_Month_4.compute(new ComputeParameters(false, pnd_Pmt_Month_4), pnd_Pmt_Month_1.add(9));                                                               //Natural: COMPUTE #PMT-MONTH-4 = #PMT-MONTH-1 + 9
        }                                                                                                                                                                 //Natural: WHEN #MODE-1 = 7
        else if (condition(pnd_Mode_Pnd_Mode_1.equals(7)))
        {
            decideConditionsMet1855++;
            pnd_Pmt_Month_2.compute(new ComputeParameters(false, pnd_Pmt_Month_2), pnd_Pmt_Month_1.add(6));                                                               //Natural: COMPUTE #PMT-MONTH-2 = #PMT-MONTH-1 + 6
        }                                                                                                                                                                 //Natural: WHEN #MODE-1 = 8
        else if (condition(pnd_Mode_Pnd_Mode_1.equals(8)))
        {
            decideConditionsMet1855++;
            pnd_Pmt_Month_2.compute(new ComputeParameters(false, pnd_Pmt_Month_2), pnd_Pmt_Month_1.add(12));                                                              //Natural: COMPUTE #PMT-MONTH-2 = #PMT-MONTH-1 + 12
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Pmt_Month_2.greater(12)))                                                                                                                       //Natural: IF #PMT-MONTH-2 > 12
        {
            pnd_Pmt_Month_2.nsubtract(12);                                                                                                                                //Natural: SUBTRACT 12 FROM #PMT-MONTH-2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pmt_Month_3.greater(12)))                                                                                                                       //Natural: IF #PMT-MONTH-3 > 12
        {
            pnd_Pmt_Month_3.nsubtract(12);                                                                                                                                //Natural: SUBTRACT 12 FROM #PMT-MONTH-3
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pmt_Month_4.greater(12)))                                                                                                                       //Natural: IF #PMT-MONTH-4 > 12
        {
            pnd_Pmt_Month_4.nsubtract(12);                                                                                                                                //Natural: SUBTRACT 12 FROM #PMT-MONTH-4
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pmt_Switch.setValue(0);                                                                                                                                       //Natural: MOVE 0 TO #PMT-SWITCH
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Pmt_Switch.equals(1))) {break;}                                                                                                             //Natural: UNTIL #PMT-SWITCH = 1
            if (condition(pnd_Mode_Pnd_Mode_1.equals(1) || (pnd_Ws_Date_Pnd_Ws_Month.equals(pnd_Pmt_Month_1) || pnd_Ws_Date_Pnd_Ws_Month.equals(pnd_Pmt_Month_2)          //Natural: IF #MODE-1 = 1 OR ( #WS-MONTH = #PMT-MONTH-1 OR #WS-MONTH = #PMT-MONTH-2 OR #WS-MONTH = #PMT-MONTH-3 OR #WS-MONTH = #PMT-MONTH-4 )
                || pnd_Ws_Date_Pnd_Ws_Month.equals(pnd_Pmt_Month_3) || pnd_Ws_Date_Pnd_Ws_Month.equals(pnd_Pmt_Month_4))))
            {
                pnd_Pmt_Switch.setValue(1);                                                                                                                               //Natural: MOVE 1 TO #PMT-SWITCH
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Date_Pnd_Ws_Month.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-MONTH
                if (condition(pnd_Ws_Date_Pnd_Ws_Month.equals(13)))                                                                                                       //Natural: IF #WS-MONTH = 13
                {
                    pnd_Ws_Date_Pnd_Ws_Month.setValue(1);                                                                                                                 //Natural: MOVE 1 TO #WS-MONTH
                    pnd_Ws_Date_Pnd_Ws_Year.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WS-YEAR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*   RITE 'NEXT-DA2' #WS-DATE
    }
    private void sub_Call_Aian030() throws Exception                                                                                                                      //Natural: CALL-AIAN030
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*   RITE 'BEFORE AIAN030 LINKAGE ' #AIAN030-LINKAGE
        DbsUtil.callnat(Aian030.class , getCurrentProcessState(), pnd_Aian030_Linkage);                                                                                   //Natural: CALLNAT 'AIAN030' #AIAN030-LINKAGE
        if (condition(Global.isEscape())) return;
        //*  SMOLYAN 08/19/2009 - COMMENTED OUT 3 LINES BELOW
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'S'
        //*    SUBTRACT 1 FROM #AIAN030-DAYS-BETWEEN-TWO-DAYS
        //*  END-IF
        //*   RITE 'AFTER AIAN030 LINKAGE ' #AIAN030-LINKAGE
    }
    private void sub_Calculate_Tiaa_Payment_By_Option_Code() throws Exception                                                                                             //Natural: CALCULATE-TIAA-PAYMENT-BY-OPTION-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        short decideConditionsMet1903 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #AIAN028-OUT-OPTION NOT = 'IR'
        if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().notEquals("IR")))
        {
            decideConditionsMet1903++;
            //* *WHEN NAZ-ACT-OPTION-CDE NOT = 2
                                                                                                                                                                          //Natural: PERFORM TIAA-SIMPLE-OPTION-ROUTINE
            sub_Tiaa_Simple_Option_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #AIAN028-OUT-OPTION = 'IR'
        else if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().equals("IR")))
        {
            decideConditionsMet1903++;
                                                                                                                                                                          //Natural: PERFORM TIAA-INSTALLMENT-OPTION-ROUTINE
            sub_Tiaa_Installment_Option_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Tiaa_Simple_Option_Routine() throws Exception                                                                                                        //Natural: TIAA-SIMPLE-OPTION-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM TOTAL-PAYMENT-CALL
        sub_Total_Payment_Call();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NOT = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GUARANTEED-PAYMENT-CALL
        sub_Guaranteed_Payment_Call();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-OF-DIVIDEND-PAYMENT
        sub_Calc_Of_Dividend_Payment();
        if (condition(Global.isEscape())) {return;}
        //* *  IF NAZ-ACT-GUAR-PERIOD > 0 OR NAZ-ACT-GUAR-PERIOD-MTH > 0
        //* *        PERFORM CALCULATE-LAST-PAYMENT-DATE
        //* *  END-IF
    }
    private void sub_Tiaa_Installment_Option_Routine() throws Exception                                                                                                   //Natural: TIAA-INSTALLMENT-OPTION-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
                                                                                                                                                                          //Natural: PERFORM CONVERT-MODE
        sub_Convert_Mode();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GUARANTEED-PAYMENT-CALL
        sub_Guaranteed_Payment_Call();
        if (condition(Global.isEscape())) {return;}
        //* *   PERFORM SUM-GUAR-PAYMENT-AND-ACCUM-TOTAL
        //* ***   ALMOST THE SAME AS A TOTAL-PAYMENT-CALL, BUT CALL WITH AN
        //* ***   SLA (OPTION SL AND A CERTAIN PERIOD EQUAL TO THE ANNUITY FACTOR
        //* ***   OF THE GUARANTEED CALL
        //* ***
        //* ***  TWO VARIABLES OBTAINED FROM CHOOSE-MORTALITY-FUNTION
        //* ***
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table());             //Natural: MOVE #AIAN027-TOTAL-MORTALITY-TABLE TO #AIAN021-IN-MORTALITY-TABLE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate());                 //Natural: MOVE #AIAN027-TOTAL-INTEREST-RATE TO #AIAN021-IN-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Setback());                             //Natural: MOVE #AIAN027-TOTAL-SETBACK TO #AIAN021-IN-SETBACK
        //* ***
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("SL");                                                                                //Natural: MOVE 'SL' TO #AIAN021-IN-PAYMENT-OPTION
        //* * RITE 'ANN-FACT '  #AIAN021-OUT-ANNUITY-FACTOR #NUM-OF-PAYMENTS
        pnd_Integer_Periods.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());                                                                //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #INTEGER-PERIODS
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)))                        //Natural: IF NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01
            pnd_Annualized_Annuity_Factor.compute(new ComputeParameters(true, pnd_Annualized_Annuity_Factor), (pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().subtract(1)).divide(pnd_Num_Of_Payments)); //Natural: COMPUTE ROUNDED #ANNUALIZED-ANNUITY-FACTOR = ( #AIAN021-OUT-ANNUITY-FACTOR - 1 ) / #NUM-OF-PAYMENTS
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Issue_Date().setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N8);                                                      //Natural: MOVE #EFF-DATE-N8 TO #AIAN021-IN-ISSUE-DATE
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().setValue(pnd_Aian030_Linkage_Pnd_Aian030_Days_Between_Two_Days);                          //Natural: MOVE #AIAN030-DAYS-BETWEEN-TWO-DAYS TO #AIAN021-IN-DEFERRED-PERIOD
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate());         //Natural: MOVE #AIAN027-TOTAL-INTEREST-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Deferral_Period().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period());                    //Natural: MOVE #AIAN021-IN-DEFERRED-PERIOD TO NAZ-DEFERRAL-PERIOD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Annualized_Annuity_Factor.compute(new ComputeParameters(true, pnd_Annualized_Annuity_Factor), pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().divide(pnd_Num_Of_Payments)); //Natural: COMPUTE ROUNDED #ANNUALIZED-ANNUITY-FACTOR = #AIAN021-OUT-ANNUITY-FACTOR / #NUM-OF-PAYMENTS
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period().setValue(pnd_Annualized_Annuity_Factor);                                                       //Natural: MOVE #ANNUALIZED-ANNUITY-FACTOR TO #AIAN021-IN-CERTAIN-PERIOD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Certain_Period().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period());                          //Natural: MOVE #AIAN021-IN-CERTAIN-PERIOD TO NAZ-CERTAIN-PERIOD
        //* * RITE 'INT-PER ' #ANNUALIZED-ANNUITY-FACTOR #INTEGER-PERIODS
        //* *                 NAZ-ACT-TIAA-STD-RATE-CDE (#I)
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().reset();                                                                                      //Natural: RESET #AIAN021-OUT-ANNUITY-FACTOR
        pnd_Ws_Aian021_Type.setValue("TT");                                                                                                                               //Natural: MOVE 'TT' TO #WS-AIAN021-TYPE
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN021
        sub_Call_Aian021();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NOT = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALC-OF-TOTAL-PAYMENT
        sub_Calc_Of_Total_Payment();
        if (condition(Global.isEscape())) {return;}
        //* ***
                                                                                                                                                                          //Natural: PERFORM CALC-OF-DIVIDEND-PAYMENT
        sub_Calc_Of_Dividend_Payment();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUM-GUAR-PAYMENT-AND-ACCUM-TOTAL
        sub_Sum_Guar_Payment_And_Accum_Total();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Total_Payment_Call() throws Exception                                                                                                                //Natural: TOTAL-PAYMENT-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*   RITE 'TOTAL-PAYMENT-CALL'
                                                                                                                                                                          //Natural: PERFORM FILL-AIAN021-LINKAGE
        sub_Fill_Aian021_Linkage();
        if (condition(Global.isEscape())) {return;}
        //* ***  TWO VARIABLES OBTAINED FROM CHOOSE-MORTALITY-FUNTION
        //* ***
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30)))    //Natural: IF NAZ-ACT-OPTION-CDE = 28 OR = 30
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(ldaAial770r.getAct_Tpa_Rate_File_Rate_View_Div_Rate());                            //Natural: MOVE DIV-RATE TO #AIAN021-IN-INTEREST-RATE
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(ldaAial770r.getAct_Tpa_Rate_File_Rate_View_Div_Rate());                        //Natural: MOVE DIV-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Mortality_Table());         //Natural: MOVE #AIAN027-TOTAL-MORTALITY-TABLE TO #AIAN021-IN-MORTALITY-TABLE
            //* *****************************************************************
            //*  THE FOLLOWING THREE LINES SHOULD BE UNCOMMENTED IF AND WHEN IT IS
            //*  DECIDED THAT THE PA PRODUCT SHOULD USE A DIFFERENT TABLE FOR SLA
            //* *****************************************************************
            //*  IF #AIAN028-OUT-OPTION = 'SL' AND NAZ-ACT-TYPE-OF-CALL = 'S'
            //*  MOVE  0364 TO #AIAN021-IN-MORTALITY-TABLE
            //*  END-IF
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate());             //Natural: MOVE #AIAN027-TOTAL-INTEREST-RATE TO #AIAN021-IN-INTEREST-RATE
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Setback());                         //Natural: MOVE #AIAN027-TOTAL-SETBACK TO #AIAN021-IN-SETBACK
            //*    RITE 'NAZ-ACT-TYPE-OF-CALL = ' NAZ-ACT-TYPE-OF-CALL
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR #IAAN0019-CNTRCT-ORGN-CDE = 40
                && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))
            {
                //*    IF #TYPE-OF-CALL = 'TG'
                //*      MOVE #AIAN027-MODAL-INTEREST-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
                //*    ELSE
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate());     //Natural: MOVE #AIAN027-TOTAL-INTEREST-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
                //*    END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Aian021_Type.setValue("TT");                                                                                                                               //Natural: MOVE 'TT' TO #WS-AIAN021-TYPE
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN021
        sub_Call_Aian021();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NOT = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALC-OF-TOTAL-PAYMENT
        sub_Calc_Of_Total_Payment();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Guaranteed_Payment_Call() throws Exception                                                                                                           //Natural: GUARANTEED-PAYMENT-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM FILL-AIAN021-LINKAGE
        sub_Fill_Aian021_Linkage();
        if (condition(Global.isEscape())) {return;}
        //* ***  TWO VARIABLES OBTAINED FROM CHOOSE-MORTALITY-FUNTION
        //* ***
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30)))    //Natural: IF NAZ-ACT-OPTION-CDE = 28 OR = 30
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(ldaAial770r.getAct_Tpa_Rate_File_Rate_View_Guar_Rate());                           //Natural: MOVE GUAR-RATE TO #AIAN021-IN-INTEREST-RATE
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(ldaAial770r.getAct_Tpa_Rate_File_Rate_View_Guar_Rate());                       //Natural: MOVE GUAR-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Mortality_Table());         //Natural: MOVE #AIAN027-GUARA-MORTALITY-TABLE TO #AIAN021-IN-MORTALITY-TABLE
            //* *****************************************************************
            //*  THE FOLLOWING THREE LINES SHOULD BE UNCOMMENTED IF AND WHEN IT IS
            //*  DECIDED THAT THE PA PRODUCT SHOULD USE A DIFFERENT TABLE FOR SLA
            //* *****************************************************************
            //*  IF #AIAN028-OUT-OPTION = 'SL' AND NAZ-ACT-TYPE-OF-CALL = 'S'
            //*  MOVE  0364 TO #AIAN021-IN-MORTALITY-TABLE
            //*  END-IF
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate());             //Natural: MOVE #AIAN027-GUARA-INTEREST-RATE TO #AIAN021-IN-INTEREST-RATE
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Setback());                         //Natural: MOVE #AIAN027-GUARA-SETBACK TO #AIAN021-IN-SETBACK
            //* ***
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR #IAAN0019-CNTRCT-ORGN-CDE = 40 OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 )
                || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1))))
            {
                //*      (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
                pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate());     //Natural: MOVE #AIAN027-GUARA-INTEREST-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Aian021_Type.setValue("TG");                                                                                                                               //Natural: MOVE 'TG' TO #WS-AIAN021-TYPE
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN021
        sub_Call_Aian021();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NOT = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALC-OF-GURARANTEED-PAYMENT
        sub_Calc_Of_Guraranteed_Payment();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Calc_Of_Total_Payment() throws Exception                                                                                                             //Natural: CALC-OF-TOTAL-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*   RITE 'CALC-OF-TOTAL-PAYMENT'
        //* *COMPUTE ROUNDED #TIAA-TOTAL = #TIAA-RATE-AMT * #MODAL-ADJUSTMENT /
        //* *                               #AIAN021-OUT-ANNUITY-FACTOR
        //*   RITE '%%' NAZ-ACT-TYPE-OF-CALL '%%' #ISSUE-DATE-DAY
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1) && pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().notEquals(40))) //Natural: IF NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 AND #IAAN0019-CNTRCT-ORGN-CDE NOT = 40
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01
            pnd_New_Ann_Factor.compute(new ComputeParameters(true, pnd_New_Ann_Factor), (pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().add(1)));    //Natural: COMPUTE ROUNDED #NEW-ANN-FACTOR = ( #AIAN021-OUT-ANNUITY-FACTOR + 1 )
            pnd_Tiaa_Total.compute(new ComputeParameters(true, pnd_Tiaa_Total), pnd_Tiaa_Rate_Amt.divide(pnd_New_Ann_Factor));                                            //Natural: COMPUTE ROUNDED #TIAA-TOTAL = #TIAA-RATE-AMT / #NEW-ANN-FACTOR
            if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                                 //Natural: IF #TYPE-OF-CALL = 'TG'
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor_Grd().getValue(pnd_I).setValue(pnd_New_Ann_Factor);                                            //Natural: MOVE #NEW-ANN-FACTOR TO NAZ-TOTAL-ANN-FACTOR-GRD ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor().getValue(pnd_I).setValue(pnd_New_Ann_Factor);                                                //Natural: MOVE #NEW-ANN-FACTOR TO NAZ-TOTAL-ANN-FACTOR ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(22) && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())  //Natural: IF NAZ-ACT-OPTION-CDE = 22 AND NAZ-ACT-RETURN-CODE-NBR = 0 AND #AIAN021-OUT-ANNUITY-FACTOR = 0
                && pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().equals(getZero())))
            {
                pnd_Tiaa_Total.compute(new ComputeParameters(true, pnd_Tiaa_Total), pnd_Tiaa_Rate_Amt);                                                                   //Natural: COMPUTE ROUNDED #TIAA-TOTAL = #TIAA-RATE-AMT
                if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                             //Natural: IF #TYPE-OF-CALL = 'TG'
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-TOTAL-ANN-FACTOR-GRD ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-TOTAL-ANN-FACTOR ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Total.compute(new ComputeParameters(true, pnd_Tiaa_Total), pnd_Tiaa_Rate_Amt.divide(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor())); //Natural: COMPUTE ROUNDED #TIAA-TOTAL = #TIAA-RATE-AMT / #AIAN021-OUT-ANNUITY-FACTOR
                if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                             //Natural: IF #TYPE-OF-CALL = 'TG'
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-TOTAL-ANN-FACTOR-GRD ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-TOTAL-ANN-FACTOR ( #I )
                }                                                                                                                                                         //Natural: END-IF
                //*   RITE (1) '=' #TIAA-RATE-AMT '=' #AIAN021-OUT-ANNUITY-FACTOR ' '
                //*            #TYPE-OF-CALL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Total.compute(new ComputeParameters(true, pnd_Tiaa_Total), pnd_Tiaa_Total.multiply(pnd_Modal_Adjustment));                                               //Natural: COMPUTE ROUNDED #TIAA-TOTAL = #TIAA-TOTAL * #MODAL-ADJUSTMENT
    }
    private void sub_Calc_Of_Guraranteed_Payment() throws Exception                                                                                                       //Natural: CALC-OF-GURARANTEED-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1) && pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().notEquals(40)  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 AND #IAAN0019-CNTRCT-ORGN-CDE NOT = 40 AND #AIAN028-OUT-OPTION NOT = 'IR'
            && pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().notEquals("IR")))
        {
            pnd_New_Ann_Factor.compute(new ComputeParameters(true, pnd_New_Ann_Factor), (pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().add(1)));    //Natural: COMPUTE ROUNDED #NEW-ANN-FACTOR = ( #AIAN021-OUT-ANNUITY-FACTOR + 1 )
            pnd_Tiaa_Guaranteed.compute(new ComputeParameters(true, pnd_Tiaa_Guaranteed), pnd_Tiaa_Rate_Amt.divide(pnd_New_Ann_Factor));                                  //Natural: COMPUTE ROUNDED #TIAA-GUARANTEED = #TIAA-RATE-AMT / #NEW-ANN-FACTOR
            if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                                 //Natural: IF #TYPE-OF-CALL = 'TG'
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor_Grd().getValue(pnd_I).setValue(pnd_New_Ann_Factor);                                             //Natural: MOVE #NEW-ANN-FACTOR TO NAZ-GUAR-ANN-FACTOR-GRD ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor().getValue(pnd_I).setValue(pnd_New_Ann_Factor);                                                 //Natural: MOVE #NEW-ANN-FACTOR TO NAZ-GUAR-ANN-FACTOR ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(22) && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())  //Natural: IF NAZ-ACT-OPTION-CDE = 22 AND NAZ-ACT-RETURN-CODE-NBR = 0 AND #AIAN021-OUT-ANNUITY-FACTOR = 0
                && pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().equals(getZero())))
            {
                pnd_Tiaa_Guaranteed.compute(new ComputeParameters(true, pnd_Tiaa_Guaranteed), pnd_Tiaa_Rate_Amt);                                                         //Natural: COMPUTE ROUNDED #TIAA-GUARANTEED = #TIAA-RATE-AMT
                if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                             //Natural: IF #TYPE-OF-CALL = 'TG'
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-GUAR-ANN-FACTOR-GRD ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-GUAR-ANN-FACTOR ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Guaranteed.compute(new ComputeParameters(true, pnd_Tiaa_Guaranteed), pnd_Tiaa_Rate_Amt.divide(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor())); //Natural: COMPUTE ROUNDED #TIAA-GUARANTEED = #TIAA-RATE-AMT / #AIAN021-OUT-ANNUITY-FACTOR
                if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                             //Natural: IF #TYPE-OF-CALL = 'TG'
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor_Grd().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-GUAR-ANN-FACTOR-GRD ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-GUAR-ANN-FACTOR ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *ITE 'TIAA-GUAR ' #TIAA-GUARANTEED #AIAN021-OUT-ANNUITY-FACTOR
        //* *                 #NEW-ANN-FACTOR
    }
    private void sub_Calc_Of_Dividend_Payment() throws Exception                                                                                                          //Natural: CALC-OF-DIVIDEND-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Tiaa_Dividend.compute(new ComputeParameters(true, pnd_Tiaa_Dividend), pnd_Tiaa_Total.subtract(pnd_Tiaa_Guaranteed));                                          //Natural: COMPUTE ROUNDED #TIAA-DIVIDEND = #TIAA-TOTAL - #TIAA-GUARANTEED
        if (condition(pnd_Tiaa_Total.greater(new DbsDecimal("0.00")) && (pnd_Tiaa_Guaranteed.less(new DbsDecimal("0.00")) || pnd_Tiaa_Guaranteed.equals(new               //Natural: IF #TIAA-TOTAL > 0.00 AND ( #TIAA-GUARANTEED < 0.00 OR #TIAA-GUARANTEED = 0.00 )
            DbsDecimal("0.00")))))
        {
            pnd_Tiaa_Guaranteed.setValue(pnd_Tiaa_Dividend);                                                                                                              //Natural: MOVE #TIAA-DIVIDEND TO #TIAA-GUARANTEED #TIAA-TOTAL
            pnd_Tiaa_Total.setValue(pnd_Tiaa_Dividend);
            pnd_Tiaa_Dividend.setValue(0);                                                                                                                                //Natural: MOVE 0.00 TO #TIAA-DIVIDEND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa_Total.less(pnd_Tiaa_Guaranteed)))                                                                                                          //Natural: IF #TIAA-TOTAL < #TIAA-GUARANTEED
        {
            pnd_Tiaa_Total.setValue(pnd_Tiaa_Guaranteed);                                                                                                                 //Natural: MOVE #TIAA-GUARANTEED TO #TIAA-TOTAL
            pnd_Tiaa_Dividend.setValue(0);                                                                                                                                //Natural: MOVE 0.00 TO #TIAA-DIVIDEND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa_Total.less(new DbsDecimal("0.00")) || pnd_Tiaa_Total.equals(new DbsDecimal("0.00"))))                                                      //Natural: IF #TIAA-TOTAL < 0.00 OR #TIAA-TOTAL = 0.00
        {
            if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") && pnd_Tiaa_Rate_Amt.less(new DbsDecimal(".06")))                     //Natural: IF ( NAZ-ACT-TYPE-OF-CALL = '2' AND #TIAA-RATE-AMT < .06 ) OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #TIAA-RATE-AMT < .01 )
                || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Tiaa_Rate_Amt.less(new DbsDecimal(".01")))))
            {
                //*  IF (NAZ-ACT-TYPE-OF-CALL = 'F' AND #TIAA-RATE-AMT < .06) OR
                //*      (NAZ-ACT-TYPE-OF-CALL = 'I' AND #TIAA-RATE-AMT < .01)
                pnd_Tiaa_Guaranteed.setValue(0);                                                                                                                          //Natural: MOVE 0.00 TO #TIAA-GUARANTEED
                pnd_Tiaa_Dividend.setValue(0);                                                                                                                            //Natural: MOVE 0.00 TO #TIAA-DIVIDEND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    MOVE 0.01 TO #TIAA-GUARANTEED
                pnd_Tiaa_Guaranteed.setValue(0);                                                                                                                          //Natural: MOVE 0.00 TO #TIAA-GUARANTEED
                pnd_Tiaa_Dividend.setValue(0);                                                                                                                            //Natural: MOVE 0.00 TO #TIAA-DIVIDEND
                pnd_Tiaa_Total.setValue(0);                                                                                                                               //Natural: MOVE 0.00 TO #TIAA-TOTAL
                if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                             //Natural: IF #TYPE-OF-CALL = 'TG'
                {
                    collapse_Array_Grd_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO GRD-CNT
                    collapse_Array_Collapse_Grd_I.getValue(collapse_Array_Grd_Cnt).setValue(pnd_I);                                                                       //Natural: MOVE #I TO COLLAPSE-GRD-I ( GRD-CNT )
                    collapse_Array_Collapse_Grd_Rb.getValue(collapse_Array_Grd_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-CDE ( #I ) TO COLLAPSE-GRD-RB ( GRD-CNT )
                    collapse_Array_Collapse_Grd_Gic_Code.getValue(collapse_Array_Grd_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Gic_Code().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-GIC-CODE ( #I ) TO COLLAPSE-GRD-GIC-CODE ( GRD-CNT )
                    collapse_Array_Collapse_Grd_Acc.getValue(collapse_Array_Grd_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-RATE-AMT ( #I ) TO COLLAPSE-GRD-ACC ( GRD-CNT )
                    collapse_Array_Collapse_Grd_Eff_Rate_Int.getValue(collapse_Array_Grd_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( #I ) TO COLLAPSE-GRD-EFF-RATE-INT ( GRD-CNT )
                    collapse_Array_Collapse_Grd_Total_Acc.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_I));                     //Natural: ADD NAZ-ACT-TIAA-GRADED-RATE-AMT ( #I ) TO COLLAPSE-GRD-TOTAL-ACC
                    //*       MOVE '  ' TO NAZ-ACT-TIAA-GRADED-RATE-CDE (#I) #TIAA-RATE-CDE
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_I).setValue(0);                                                     //Natural: MOVE 0 TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( #I ) #TIAA-RATE-AMT
                    pnd_Tiaa_Rate_Amt.setValue(0);
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Calced_Grd_Info().getValue(pnd_I).reset();                                                         //Natural: RESET NAZ-ACT-TIAA-CALCED-GRD-INFO ( #I )
                    //*       MOVE '  ' TO NAZ-ACT-TIAA-O-GRD-RATE-CDE (#I)
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue(pnd_I).setValue(0);                                                      //Natural: MOVE 0 TO NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( #I )
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_I).setValue(0);                                                      //Natural: MOVE 0 TO NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #I )
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_I).setValue(0);                                                    //Natural: MOVE 0 TO NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    collapse_Array_Std_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO STD-CNT
                    collapse_Array_Collapse_Std_I.getValue(collapse_Array_Std_Cnt).setValue(pnd_I);                                                                       //Natural: MOVE #I TO COLLAPSE-STD-I ( STD-CNT )
                    collapse_Array_Collapse_Std_Rb.getValue(collapse_Array_Std_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) TO COLLAPSE-STD-RB ( STD-CNT )
                    collapse_Array_Collapse_Std_Gic_Code.getValue(collapse_Array_Std_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Gic_Code().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-GIC-CODE ( #I ) TO COLLAPSE-STD-GIC-CODE ( STD-CNT )
                    collapse_Array_Collapse_Std_Acc.getValue(collapse_Array_Std_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) TO COLLAPSE-STD-ACC ( STD-CNT )
                    collapse_Array_Collapse_Std_Eff_Rate_Int.getValue(collapse_Array_Std_Cnt).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #I ) TO COLLAPSE-STD-EFF-RATE-INT ( STD-CNT )
                    collapse_Array_Collapse_Std_Total_Acc.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I));                        //Natural: ADD NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) TO COLLAPSE-STD-TOTAL-ACC
                    //*       MOVE '  ' TO NAZ-ACT-TIAA-STD-RATE-CDE (#I) #TIAA-RATE-CDE
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I).setValue(0);                                                        //Natural: MOVE 0 TO NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) #TIAA-RATE-AMT
                    pnd_Tiaa_Rate_Amt.setValue(0);
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Calced_Std_Info().getValue(pnd_I).reset();                                                         //Natural: RESET NAZ-ACT-TIAA-CALCED-STD-INFO ( #I )
                    //*       MOVE '  ' TO NAZ-ACT-TIAA-O-STD-RATE-CDE (#I)
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_I).setValue(0);                                                      //Natural: MOVE 0 TO NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #I )
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_I).setValue(0);                                                      //Natural: MOVE 0 TO NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #I )
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I).setValue(0);                                                    //Natural: MOVE 0 TO NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #I )
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Guar_Amt().getValue(pnd_I).setValue(0);                                                    //Natural: MOVE 0 TO NAZ-ACT-TIAA-STD-FNL-GUAR-AMT ( #I )
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Divd_Amt().getValue(pnd_I).setValue(0);                                                    //Natural: MOVE 0 TO NAZ-ACT-TIAA-STD-FNL-DIVD-AMT ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa_Total.greater(new DbsDecimal("0.00")) && (pnd_Tiaa_Guaranteed.less(new DbsDecimal("0.00")) || pnd_Tiaa_Guaranteed.equals(new               //Natural: IF #TIAA-TOTAL > 0.00 AND ( #TIAA-GUARANTEED < 0.00 OR #TIAA-GUARANTEED = 0.00 )
            DbsDecimal("0.00")))))
        {
            pnd_Tiaa_Guaranteed.setValue(pnd_Tiaa_Dividend);                                                                                                              //Natural: MOVE #TIAA-DIVIDEND TO #TIAA-GUARANTEED
            pnd_Tiaa_Dividend.setValue(0);                                                                                                                                //Natural: MOVE 0.00 TO #TIAA-DIVIDEND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa_Total.less(pnd_Tiaa_Guaranteed)))                                                                                                          //Natural: IF #TIAA-TOTAL < #TIAA-GUARANTEED
        {
            pnd_Tiaa_Total.setValue(pnd_Tiaa_Guaranteed);                                                                                                                 //Natural: MOVE #TIAA-GUARANTEED TO #TIAA-TOTAL
            pnd_Tiaa_Dividend.setValue(0);                                                                                                                                //Natural: MOVE 0.00 TO #TIAA-DIVIDEND
        }                                                                                                                                                                 //Natural: END-IF
        //* * #TIAA-GUARANTEED NOT > .01
        //* *IF (NAZ-ACT-TYPE-OF-CALL = 'F' AND #TIAA-RATE-AMT < .06) OR
        //* *   (NAZ-ACT-TYPE-OF-CALL = 'I' AND #TIAA-RATE-AMT < .01)
        //* *  MOVE 0.00 TO #TIAA-GUARANTEED
        //* *ELSE
        //* *  MOVE 0.01 TO #TIAA-GUARANTEED
        //* *END-IF
        //* *D-IF
        //* * #TIAA-DIVIDEND NOT > .01
        //* *MOVE 0.00 TO #TIAA-DIVIDEND
        //* *D-IF
        //* *ITE 'TIAA-DIVIDEND ' #TIAA-DIVIDEND
        //* *     'TIAA-TOTAL    ' #TIAA-TOTAL
        //* *     'TIAA-GUARANTEED ' #TIAA-GUARANTEED
    }
    private void sub_Sum_Guar_Payment_And_Accum_Total() throws Exception                                                                                                  //Natural: SUM-GUAR-PAYMENT-AND-ACCUM-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guaranteed.nadd(pnd_Tiaa_Guaranteed);                                                                                                //Natural: ADD #TIAA-GUARANTEED TO #TIAA-SUM-GUARANTEED
        pnd_Sum_Amounts_Pnd_Tiaa_Sum_Rate_Amt.nadd(pnd_Tiaa_Rate_Amt);                                                                                                    //Natural: ADD #TIAA-RATE-AMT TO #TIAA-SUM-RATE-AMT
        pnd_Tiaa_Guar_Periods.compute(new ComputeParameters(false, pnd_Tiaa_Guar_Periods), pnd_Tiaa_Guaranteed.multiply(pnd_Integer_Periods));                            //Natural: COMPUTE #TIAA-GUAR-PERIODS = #TIAA-GUARANTEED * #INTEGER-PERIODS
        pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guar_Periods.nadd(pnd_Tiaa_Guar_Periods);                                                                                            //Natural: ADD #TIAA-GUAR-PERIODS TO #TIAA-SUM-GUAR-PERIODS
        //* * RITE 'GUAR-PER ' #TIAA-GUAR-PERIODS #TIAA-SUM-GUAR-PERIODS
        //* * RITE 'GUAR-AMT ' #TIAA-GUARANTEED   #TIAA-SUM-GUARANTEED
    }
    private void sub_Is_Option_An_Installment_Refund() throws Exception                                                                                                   //Natural: IS-OPTION-AN-INSTALLMENT-REFUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* * IF NAZ-ACT-OPTION-CDE = 02
        if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().equals("IR")))                                                                           //Natural: IF #AIAN028-OUT-OPTION = 'IR'
        {
            //* *COMPUTE  #LAST-PAYMENT-PERIOD = #TIAA-SUM-GUAR-PERIODS /
            pnd_Last_Payment_Period.compute(new ComputeParameters(false, pnd_Last_Payment_Period), pnd_Sum_Amounts_Pnd_Tiaa_Sum_Rate_Amt.divide(pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guaranteed)); //Natural: COMPUTE #LAST-PAYMENT-PERIOD = #TIAA-SUM-RATE-AMT / #TIAA-SUM-GUARANTEED
                                                                                                                                                                          //Natural: PERFORM CALCULATE-LAST-PAYMENT-DATE
            sub_Calculate_Last_Payment_Date();
            if (condition(Global.isEscape())) {return;}
            FOR18:                                                                                                                                                        //Natural: FOR #I = 1 TO 250
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
            {
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I).notEquals("00") && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I).notEquals("  "))) //Natural: IF NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) NOT = '00' AND NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) NOT = '  '
                {
                    pnd_Accumulation_Left.compute(new ComputeParameters(false, pnd_Accumulation_Left), pnd_Sum_Amounts_Pnd_Tiaa_Sum_Rate_Amt.subtract((pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guaranteed.multiply(pnd_Last_Payment_Period)))); //Natural: COMPUTE #ACCUMULATION-LEFT = #TIAA-SUM-RATE-AMT - ( #TIAA-SUM-GUARANTEED * #LAST-PAYMENT-PERIOD )
                    pnd_Inst_Ref_Ratio.compute(new ComputeParameters(true, pnd_Inst_Ref_Ratio), pnd_Accumulation_Left.divide(pnd_Sum_Amounts_Pnd_Tiaa_Sum_Guaranteed));   //Natural: COMPUTE ROUNDED #INST-REF-RATIO = #ACCUMULATION-LEFT / #TIAA-SUM-GUARANTEED
                    pnd_Irr_Payment.compute(new ComputeParameters(true, pnd_Irr_Payment), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_I).multiply(pnd_Inst_Ref_Ratio)); //Natural: COMPUTE ROUNDED #IRR-PAYMENT = NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #I ) * #INST-REF-RATIO
                    pnd_Irr_Payment_Sum.nadd(pnd_Irr_Payment);                                                                                                            //Natural: ADD #IRR-PAYMENT TO #IRR-PAYMENT-SUM
                    if (condition(pnd_I.equals(pnd_Number_Of_Rate_Basis)))                                                                                                //Natural: IF #I = #NUMBER-OF-RATE-BASIS
                    {
                        pnd_Additional_Irr_Payment.compute(new ComputeParameters(false, pnd_Additional_Irr_Payment), pnd_Accumulation_Left.subtract(pnd_Irr_Payment_Sum)); //Natural: COMPUTE #ADDITIONAL-IRR-PAYMENT = #ACCUMULATION-LEFT - #IRR-PAYMENT-SUM
                        pnd_Irr_Payment.nadd(pnd_Additional_Irr_Payment);                                                                                                 //Natural: ADD #ADDITIONAL-IRR-PAYMENT TO #IRR-PAYMENT
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Guar_Amt().getValue(pnd_I).setValue(pnd_Irr_Payment);                                      //Natural: MOVE #IRR-PAYMENT TO NAZ-ACT-TIAA-STD-FNL-GUAR-AMT ( #I )
                    //* *   RITE 'ANNUITY-FACTOR     ' #AIAN021-OUT-ANNUITY-FACTOR
                    //* *   RITE 'INTEGER-PER        ' #LAST-PAYMENT-PERIOD
                    //* *   RITE 'ACCUMMULATION LEFT ' #ACCUMULATION-LEFT
                    //* *   RITE 'GUARANTEED SUM     ' #TIAA-SUM-GUARANTEED
                    //* *   RITE 'TOTAL-ACCUMULATION ' #TIAA-SUM-RATE-AMT
                    //* *   RITE 'RATIO              ' #INST-REF-RATIO
                    //* *     RITE 'GUARANTEED PAYMENT ' NAZ-ACT-TIAA-O-STD-GUAR-AMT (#I)
                    //* *     RITE 'FINAL PAYMENT      ' #IRR-PAYMENT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Cref_Payment_By_Option_Code() throws Exception                                                                                             //Natural: CALCULATE-CREF-PAYMENT-BY-OPTION-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
                                                                                                                                                                          //Natural: PERFORM FILL-AIAN026-LINKAGE
        sub_Fill_Aian026_Linkage();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN026
        sub_Call_Aian026();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FILL-AIAN021-LINKAGE
        sub_Fill_Aian021_Linkage();
        if (condition(Global.isEscape())) {return;}
        //* ***  TWO VARIABLES OBTAINED FROM CHOOSE-MORTALITY-FUNTION
        //* ***
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Cref_Mortality_Table());              //Natural: MOVE #AIAN027-CREF-MORTALITY-TABLE TO #AIAN021-IN-MORTALITY-TABLE
        //* *****************************************************************
        //*  THE FOLLOWING THREE LINES SHOULD BE UNCOMMENTED IF AND WHEN IT IS
        //*  DECIDED THAT THE PA PRODUCT SHOULD USE A DIFFERENT TABLE FOR SLA
        //* *****************************************************************
        //*  IF #AIAN028-OUT-OPTION = 'SL' AND NAZ-ACT-TYPE-OF-CALL = 'S'
        //*   MOVE  0364 TO #AIAN021-IN-MORTALITY-TABLE
        //*  END-IF
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Cref_Interest_Rate());                  //Natural: MOVE #AIAN027-CREF-INTEREST-RATE TO #AIAN021-IN-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Cref_Setback());                              //Natural: MOVE #AIAN027-CREF-SETBACK TO #AIAN021-IN-SETBACK
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR #IAAN0019-CNTRCT-ORGN-CDE = 40
            && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Cref_Interest_Rate());          //Natural: MOVE #AIAN027-CREF-INTEREST-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
        }                                                                                                                                                                 //Natural: END-IF
        //* * RITE 'AIAN022W INTEREST-RATE ' #AIAN021-IN-INTEREST-RATE
        //* ***
        pnd_Ws_Aian021_Type.setValue("CT");                                                                                                                               //Natural: MOVE 'CT' TO #WS-AIAN021-TYPE
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN021
        sub_Call_Aian021();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NOT = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALC-OF-CREF-ESTIMATED-AMT
        sub_Calc_Of_Cref_Estimated_Amt();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-OF-CREF-PAYOUT-UNITS
        sub_Calc_Of_Cref_Payout_Units();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-OF-CREF-PAYOUT-AMOUNT
        sub_Calc_Of_Cref_Payout_Amount();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue().equals("11") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue().equals("FL"))) //Natural: IF NAZ-ACT-STATE-AT-ISSUE = '11' OR = 'FL'
        {
            if (condition(pnd_Ia_Fund_Code.equals("R") || pnd_Ia_Fund_Code.equals("F") || pnd_Ia_Fund_Code.equals("U") || pnd_Ia_Fund_Code.equals("V")                    //Natural: IF #IA-FUND-CODE = 'R' OR = 'F' OR = 'U' OR = 'V' OR = 'N' OR = 'O' OR = 'P' OR = 'Q' OR = 'J' OR = 'K' OR = 'X' OR = 'D'
                || pnd_Ia_Fund_Code.equals("N") || pnd_Ia_Fund_Code.equals("O") || pnd_Ia_Fund_Code.equals("P") || pnd_Ia_Fund_Code.equals("Q") || pnd_Ia_Fund_Code.equals("J") 
                || pnd_Ia_Fund_Code.equals("K") || pnd_Ia_Fund_Code.equals("X") || pnd_Ia_Fund_Code.equals("D")))
            {
                                                                                                                                                                          //Natural: PERFORM STATE-OF-FLORIDA-COMPLIANCE-CHECK
                sub_State_Of_Florida_Compliance_Check();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF NAZ-ACT-GUAR-PERIOD > 0 OR NAZ-ACT-GUAR-PERIOD-MTH > 0
        //* *       PERFORM CALCULATE-LAST-PAYMENT-DATE
        //* *END-IF
    }
    private void sub_Calc_Of_Cref_Estimated_Amt() throws Exception                                                                                                        //Natural: CALC-OF-CREF-ESTIMATED-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*   RITE 'CREF ANNUITY-FACTOR ' #AIAN021-OUT-ANNUITY-FACTOR
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1) && pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().notEquals(40))) //Natural: IF NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 AND #IAAN0019-CNTRCT-ORGN-CDE NOT = 40
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01
            pnd_New_Ann_Factor.compute(new ComputeParameters(true, pnd_New_Ann_Factor), (pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().add(1)));    //Natural: COMPUTE ROUNDED #NEW-ANN-FACTOR = ( #AIAN021-OUT-ANNUITY-FACTOR + 1 )
            pnd_Cref_Estimated_Amount.compute(new ComputeParameters(true, pnd_Cref_Estimated_Amount), pnd_Cref_Rate_Amt.divide(pnd_New_Ann_Factor));                      //Natural: COMPUTE ROUNDED #CREF-ESTIMATED-AMOUNT = #CREF-RATE-AMT / #NEW-ANN-FACTOR
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Ann_Factor().getValue(pnd_I).setValue(pnd_New_Ann_Factor);                                                     //Natural: MOVE #NEW-ANN-FACTOR TO NAZ-CREF-ANN-FACTOR ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(22) && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(getZero())  //Natural: IF NAZ-ACT-OPTION-CDE = 22 AND NAZ-ACT-RETURN-CODE-NBR = 0 AND #AIAN021-OUT-ANNUITY-FACTOR = 0
                && pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().equals(getZero())))
            {
                pnd_Cref_Estimated_Amount.compute(new ComputeParameters(true, pnd_Cref_Estimated_Amount), pnd_Cref_Rate_Amt);                                             //Natural: COMPUTE ROUNDED #CREF-ESTIMATED-AMOUNT = #CREF-RATE-AMT
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-CREF-ANN-FACTOR ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Estimated_Amount.compute(new ComputeParameters(true, pnd_Cref_Estimated_Amount), pnd_Cref_Rate_Amt.divide(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor())); //Natural: COMPUTE ROUNDED #CREF-ESTIMATED-AMOUNT = #CREF-RATE-AMT / #AIAN021-OUT-ANNUITY-FACTOR
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Ann_Factor().getValue(pnd_I).setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor()); //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO NAZ-CREF-ANN-FACTOR ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Of_Cref_Payout_Units() throws Exception                                                                                                         //Natural: CALC-OF-CREF-PAYOUT-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va().notEquals(getZero())))                                                      //Natural: IF #AIAN026-INTERIM-ANNUITY-UNIT-VA NOT = 0
        {
            pnd_Cref_Payout_Units.compute(new ComputeParameters(true, pnd_Cref_Payout_Units), pnd_Cref_Estimated_Amount.divide(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va())); //Natural: COMPUTE ROUNDED #CREF-PAYOUT-UNITS = #CREF-ESTIMATED-AMOUNT / #AIAN026-INTERIM-ANNUITY-UNIT-VA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN026 ");                                                                         //Natural: MOVE 'AIAN026 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(901);                                                                                //Natural: MOVE 901 TO NAZ-ACT-RETURN-CODE-NBR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/10/03  JS  FIX FOR CREF PAYOUTS UNITS < .001
        if (condition(pnd_Cref_Payout_Units.less(new DbsDecimal(".001"))))                                                                                                //Natural: IF #CREF-PAYOUT-UNITS < .001
        {
            pnd_Cref_Payout_Units.setValue(0);                                                                                                                            //Natural: MOVE .000 TO #CREF-PAYOUT-UNITS
            //*    IF #CREF-RATE-AMT < 5.01
            //*      MOVE 207 TO NAZ-ACT-RETURN-CODE
            //*      ESCAPE ROUTINE
            //*    ELSE
            //*      MOVE .001 TO #CREF-PAYOUT-UNITS
            //*    END-IF
            //*  END FIX
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Of_Cref_Payout_Amount() throws Exception                                                                                                        //Natural: CALC-OF-CREF-PAYOUT-AMOUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* *ITE 'ANNUITY-UNIT-V ' #AIAN026-INTERIM-ANNUITY-UNIT-VALUE
        pnd_Cref_Payout_Amount.compute(new ComputeParameters(true, pnd_Cref_Payout_Amount), pnd_Cref_Payout_Units.multiply(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va())); //Natural: COMPUTE ROUNDED #CREF-PAYOUT-AMOUNT = #CREF-PAYOUT-UNITS * #AIAN026-INTERIM-ANNUITY-UNIT-VA
    }
    private void sub_Calculate_Modal_Adjustment() throws Exception                                                                                                        //Natural: CALCULATE-MODAL-ADJUSTMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Type_Of_Call.equals("TG")))                                                                                                                     //Natural: IF #TYPE-OF-CALL = 'TG'
        {
                                                                                                                                                                          //Natural: PERFORM CONVERT-MODE
            sub_Convert_Mode();
            if (condition(Global.isEscape())) {return;}
            pnd_V_Exponent.compute(new ComputeParameters(false, pnd_V_Exponent), (new DbsDecimal("1.0").divide(pnd_Num_Of_Payments)));                                    //Natural: COMPUTE #V-EXPONENT = ( 1.0 / #NUM-OF-PAYMENTS )
            pnd_V.compute(new ComputeParameters(false, pnd_V), (DbsField.divide(1,NMath.pow ((DbsField.add(1,pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate()))  //Natural: COMPUTE #V = 1 / ( 1 + #AIAN027-TOTAL-INTEREST-RATE ) ** #V-EXPONENT
                ,pnd_V_Exponent))));
            if (condition(pnd_V.equals(1)))                                                                                                                               //Natural: IF #V = 1
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                     //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(215);                                                                            //Natural: MOVE 215 TO NAZ-ACT-RETURN-CODE-NBR
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Modal_Numerator.compute(new ComputeParameters(false, pnd_Modal_Numerator), ((DbsField.subtract(1,NMath.pow (pnd_V ,pnd_Num_Of_Payments))).divide((DbsField.subtract(1, //Natural: COMPUTE #MODAL-NUMERATOR = ( 1 - ( #V ** #NUM-OF-PAYMENTS ) ) / ( 1 - #V )
                pnd_V)))));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Modal_Numerator().getValue(pnd_I).setValue(pnd_Modal_Numerator);                                                    //Natural: MOVE #MODAL-NUMERATOR TO NAZ-MODAL-NUMERATOR ( #I )
            pnd_V.compute(new ComputeParameters(false, pnd_V), (DbsField.divide(1,NMath.pow ((DbsField.add(1,pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Modal_Interest_Rate()))  //Natural: COMPUTE #V = 1 / ( 1 + #AIAN027-MODAL-INTEREST-RATE ) ** #V-EXPONENT
                ,pnd_V_Exponent))));
            if (condition(pnd_V.equals(1)))                                                                                                                               //Natural: IF #V = 1
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                     //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(215);                                                                            //Natural: MOVE 215 TO NAZ-ACT-RETURN-CODE-NBR
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Modal_Denominator.compute(new ComputeParameters(false, pnd_Modal_Denominator), ((DbsField.subtract(1,NMath.pow (pnd_V ,pnd_Num_Of_Payments))).divide((DbsField.subtract(1, //Natural: COMPUTE #MODAL-DENOMINATOR = ( 1 - ( #V ** #NUM-OF-PAYMENTS ) ) / ( 1 - #V )
                pnd_V)))));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Modal_Denominator().getValue(pnd_I).setValue(pnd_Modal_Denominator);                                                //Natural: MOVE #MODAL-DENOMINATOR TO NAZ-MODAL-DENOMINATOR ( #I )
            if (condition(pnd_Modal_Denominator.equals(getZero())))                                                                                                       //Natural: IF #MODAL-DENOMINATOR = 0
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                     //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(215);                                                                            //Natural: MOVE 215 TO NAZ-ACT-RETURN-CODE-NBR
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Modal_Adjustment.compute(new ComputeParameters(true, pnd_Modal_Adjustment), pnd_Modal_Numerator.divide(pnd_Modal_Denominator));                           //Natural: COMPUTE ROUNDED #MODAL-ADJUSTMENT = #MODAL-NUMERATOR / #MODAL-DENOMINATOR
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Modal_Adjustment().getValue(pnd_I).setValue(pnd_Modal_Adjustment);                                                  //Natural: MOVE #MODAL-ADJUSTMENT TO NAZ-MODAL-ADJUSTMENT ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Modal_Adjustment.setValue(1);                                                                                                                             //Natural: MOVE 1.0000000 TO #MODAL-ADJUSTMENT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Convert_Date_To_Numeric() throws Exception                                                                                                           //Natural: CONVERT-DATE-TO-NUMERIC
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Check_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Curr_Bsnss_Date(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED NAZ-ACT-CURR-BSNSS-DATE ( EM = YYYYMMDD ) TO #CHECK-DATE-A
        pnd_Issue_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(),new ReportEditMask("YYYYMMDD"));                                         //Natural: MOVE EDITED NAZ-ACT-ASD-DATE ( EM = YYYYMMDD ) TO #ISSUE-DATE-A
        //*  MOVE EDITED NAZ-ACT-ASD-DATE (EM=YYYYMMDD) TO #SPIA-START-DATE-A
        pnd_Eff_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date(),new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED NAZ-ACT-EFF-DATE ( EM = YYYYMMDD ) TO #EFF-DATE-A
        pnd_1st_Ann_Birth_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth(),new ReportEditMask("YYYYMMDD"));                           //Natural: MOVE EDITED NAZ-ACT-ANN-DT-OF-BRTH ( EM = YYYYMMDD ) TO #1ST-ANN-BIRTH-DATE-A
        pnd_2nd_Ann_Birth_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth(),new ReportEditMask("YYYYMMDD"));                       //Natural: MOVE EDITED NAZ-ACT-2ND-ANN-DT-OF-BRTH ( EM = YYYYMMDD ) TO #2ND-ANN-BIRTH-DATE-A
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) OR #IAAN0019-CNTRCT-ORGN-CDE = 40
            && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)) || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))
        {
            pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date(),new ReportEditMask("YYYYMMDD"));    //Natural: MOVE EDITED NAZ-ACT-FNL-PAY-DATE ( EM = YYYYMMDD ) TO #LAST-PAYMENT-DATE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pp_Pay_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED NAZ-ACT-STD-FNL-PP-PAY-DTE ( EM = YYYYMMDD ) TO #LAST-PAYMENT-DATE-A
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pay_Dte(),new ReportEditMask("YYYYMMDD"));   //Natural: MOVE EDITED NAZ-ACT-STD-FNL-PAY-DTE ( EM = YYYYMMDD ) TO #FINAL-PAYMENT-DATE-A
        pnd_Next_Ia_Payment_Date_Pnd_Next_Ia_Payment_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ia_Next_Pymt_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED NAZ-ACT-IA-NEXT-PYMT-DTE ( EM = YYYYMMDD ) TO #NEXT-IA-PAYMENT-DATE-A
    }
    private void sub_Check_Dates() throws Exception                                                                                                                       //Natural: CHECK-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Check_Date_A_Pnd_Check_Month_N.less(1) || pnd_Check_Date_A_Pnd_Check_Month_N.greater(12)))                                                      //Natural: IF #CHECK-MONTH-N < 1 OR #CHECK-MONTH-N > 12
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                         //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(202);                                                                                //Natural: MOVE 202 TO NAZ-ACT-RETURN-CODE-NBR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Issue_Date_A_Pnd_Issue_Month_N.less(1) || pnd_Issue_Date_A_Pnd_Issue_Month_N.greater(12)))                                                      //Natural: IF #ISSUE-MONTH-N < 1 OR #ISSUE-MONTH-N > 12
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                         //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(203);                                                                                //Natural: MOVE 203 TO NAZ-ACT-RETURN-CODE-NBR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month_N.less(1) || pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month_N.greater(12)))                      //Natural: IF #1ST-ANN-BIRTH-MONTH-N < 1 OR #1ST-ANN-BIRTH-MONTH-N > 12
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                         //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(204);                                                                                //Natural: MOVE 204 TO NAZ-ACT-RETURN-CODE-NBR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N.greater(getZero())))                                                                              //Natural: IF #2ND-ANN-BIRTH-DATE-N > 0
        {
            if (condition((pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month_N.less(1) || pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month_N.greater(12))))                //Natural: IF ( #2ND-ANN-BIRTH-MONTH-N < 1 OR #2ND-ANN-BIRTH-MONTH-N > 12 )
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                     //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(205);                                                                            //Natural: MOVE 205 TO NAZ-ACT-RETURN-CODE-NBR
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Iaa770_File() throws Exception                                                                                                                  //Natural: READ-IAA770-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*     RITE 'READ-IAA770-FILE'
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().less(10)))                                                                              //Natural: IF NAZ-ACT-GUAR-PERIOD < 10
        {
            //*   12/01/03  JS
            //*    FIND ACT-TPA-RATE-FILE-DATE-CTL-REC-V
            //*        WITH TPA-RATE-KEY = 0
            ldaAial770d.getVw_act_Tpa_Rate_File_Date_View().startDatabaseFind                                                                                             //Natural: FIND ACT-TPA-RATE-FILE-DATE-VIEW WITH TPA-RATE-KEY = '00000000'
            (
            "FIND01",
            new Wc[] { new Wc("TPA_RATE_KEY", "=", "00000000", WcType.WITH) }
            );
            FIND01:
            while (condition(ldaAial770d.getVw_act_Tpa_Rate_File_Date_View().readNextRow("FIND01", true)))
            {
                ldaAial770d.getVw_act_Tpa_Rate_File_Date_View().setIfNotFoundControlFlag(false);
                //*   END FIX
                if (condition(ldaAial770d.getVw_act_Tpa_Rate_File_Date_View().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                {
                    //*       RITE 'NO RECORD FOUND FOR ' #TPA-RATE-KEY
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                 //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(1);                                                                          //Natural: MOVE 001 TO NAZ-ACT-RETURN-CODE-NBR
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_N.greater(ldaAial770d.getAct_Tpa_Rate_File_Date_View_Last_Tpa_Date())))                                     //Natural: IF #ISSUE-DATE-N > LAST-TPA-DATE
                {
                    pnd_Tpa_Rate_Key_Pnd_Tpa_Key_Date.setValue(ldaAial770d.getAct_Tpa_Rate_File_Date_View_Last_Tpa_Date());                                               //Natural: MOVE LAST-TPA-DATE TO #TPA-KEY-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tpa_Rate_Key_Pnd_Tpa_Key_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                        //Natural: MOVE #ISSUE-DATE-N TO #TPA-KEY-DATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tpa_Rate_Key_Pnd_Tpa_Key_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                                //Natural: MOVE #ISSUE-DATE-N TO #TPA-KEY-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*   12/01/03
        //*  MOVE #TIAA-RATE-CDE-N TO #TPA-RATE-BASIS
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2")))                                                                          //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2'
        {
            pnd_Tpa_Rate_Key_Pnd_Tpa_Key_Date.setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N);                                                                                    //Natural: MOVE #EFF-DATE-N TO #TPA-KEY-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tpa_Rate_Key_Pnd_Tpa_Rate_Basis.setValue(pnd_Tiaa_Rate_Cde);                                                                                                  //Natural: MOVE #TIAA-RATE-CDE TO #TPA-RATE-BASIS
        //*   END FIX
        ldaAial770r.getVw_act_Tpa_Rate_File_Rate_View().startDatabaseFind                                                                                                 //Natural: FIND ACT-TPA-RATE-FILE-RATE-VIEW WITH TPA-RATE-KEY = #TPA-RATE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("TPA_RATE_KEY", "=", pnd_Tpa_Rate_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(ldaAial770r.getVw_act_Tpa_Rate_File_Rate_View().readNextRow("FIND02", true)))
        {
            ldaAial770r.getVw_act_Tpa_Rate_File_Rate_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial770r.getVw_act_Tpa_Rate_File_Rate_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
            {
                //*     RITE 'NO RECORD FOUND FOR ' #TPA-RATE-KEY
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                     //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(1);                                                                              //Natural: MOVE 001 TO NAZ-ACT-RETURN-CODE-NBR
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Choose_Mortality_Table() throws Exception                                                                                                            //Natural: CHOOSE-MORTALITY-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* * WRITE '*** CHOOSE-MORTALITY-TABLE **'
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().less(10)       //Natural: IF NAZ-ACT-OPTION-CDE = 28 AND NAZ-ACT-GUAR-PERIOD < 10 AND NAZ-ACT-TYPE-OF-CALL = '1'
            && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")))
        {
            //*    AND NAZ-ACT-TYPE-OF-CALL = 'I'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa027r.getPnd_Aian027_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN027-LINKAGE
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Gsra_Ira_Tpa_Mdo_Ind().setValue(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Gsra_Ira_Tpa_Mdo_Ind());             //Natural: MOVE #AIAN028-GSRA-IRA-TPA-MDO-IND TO #AIAN027-GSRA-IRA-TPA-MDO-IND
        //*   12/01/03
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' OR
        //*    (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 )
            && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1))))
        {
            //*  IF (NAZ-ACT-TYPE-OF-CALL = 'I' AND #ISSUE-DATE-DAY > 01)
            //*   END FIX
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effect_Date_Without_Day().setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N);                                             //Natural: MOVE #EFF-DATE-N TO #AIAN027-EFFECT-DATE-WITHOUT-DAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effect_Date_Without_Day().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                         //Natural: MOVE #ISSUE-DATE-N TO #AIAN027-EFFECT-DATE-WITHOUT-DAY
        }                                                                                                                                                                 //Natural: END-IF
        //*   12/01/03
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'I' AND #TIAA-RATE-CDE-N = 32
        //*  IF NAZ-ACT-TYPE-OF-CALL = '1'  AND #TIAA-RATE-CDE = '32'
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") && pnd_Issue_Date_A_Pnd_Issue_Date_Day.equals(1) && (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(2)  //Natural: IF NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY = 01 AND ( #IAAN0019-ORIGIN-FAMILY = 2 OR #IAAN0019-ORIGIN-FAMILY = 7 ) AND NAZ-ACT-TIAA-STD-CALL = 'Y'
            || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(7)) && pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().equals("Y")))
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'I' AND #TIAA-RATE-CDE = '32'
            //*   END FIX
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().nsubtract(1);                                                                                //Natural: SUBTRACT 1 FROM #AIAN027-EFFECTIVE-MONTH
            if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().equals(getZero())))                                                            //Natural: IF #AIAN027-EFFECTIVE-MONTH = 0
            {
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().setValue(12);                                                                            //Natural: MOVE 12 TO #AIAN027-EFFECTIVE-MONTH
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Year().nsubtract(1);                                                                             //Natural: SUBTRACT 1 FROM #AIAN027-EFFECTIVE-YEAR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   RITE 'TYPE CALL ' NAZ-ACT-TYPE-OF-CALL #TIAA-RATE-CDE
        //*  #AIAN027-EFFECTIVE-MONTH
        //* * START OF CODE ADDED 7/21/2011  SLR (AS PER VE & DY)
        //*  NOT A SPIA
        //*  NON-PENSION
        if (condition((pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().notEquals(40) && (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(2)  //Natural: IF ( #IAAN0019-CNTRCT-ORGN-CDE NE 40 ) AND ( #IAAN0019-ORIGIN-FAMILY = 2 OR = 7 )
            || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(7)))))
        {
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effect_Date_Without_Day().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                         //Natural: MOVE #ISSUE-DATE-N TO #AIAN027-EFFECT-DATE-WITHOUT-DAY
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().nsubtract(1);                                                                                //Natural: SUBTRACT 1 FROM #AIAN027-EFFECTIVE-MONTH
            if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().equals(getZero())))                                                            //Natural: IF #AIAN027-EFFECTIVE-MONTH = 0
            {
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Month().setValue(12);                                                                            //Natural: MOVE 12 TO #AIAN027-EFFECTIVE-MONTH
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Year().nsubtract(1);                                                                             //Natural: SUBTRACT 1 FROM #AIAN027-EFFECTIVE-YEAR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * END OF CODE ADDED 7/21/2011
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effective_Day().setValue(0);                                                                                       //Natural: MOVE 00 TO #AIAN027-EFFECTIVE-DAY
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Type_Of_Call().setValue(pnd_Type_Of_Call);                                                                         //Natural: MOVE #TYPE-OF-CALL TO #AIAN027-TYPE-OF-CALL
        pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Option_Code().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde());                                //Natural: MOVE NAZ-ACT-OPTION-CDE TO #AIAN027-OPTION-CODE
        if (condition(pnd_Type_Of_Call.equals("TS") || pnd_Type_Of_Call.equals("TG")))                                                                                    //Natural: IF #TYPE-OF-CALL = 'TS' OR #TYPE-OF-CALL = 'TG'
        {
            //*  12/01/03  JS
            //*    MOVE  #TIAA-RATE-CDE-N  TO       #AIAN027-DA-RATE
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate().setValue(pnd_Tiaa_Rate_Cde);                                                                         //Natural: MOVE #TIAA-RATE-CDE TO #AIAN027-DA-RATE
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Fund_Indicator().setValue(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Fixed_Fund_Code());                    //Natural: MOVE #IAAN0019-FIXED-FUND-CODE TO #AIAN027-FUND-INDICATOR
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Origin_Product().setValue(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Fixed_Product());               //Natural: MOVE #IAAN0019-ORIGIN-FIXED-PRODUCT TO #AIAN027-ORIGIN-PRODUCT
            //*  MOVE  'T'             TO  #AIAN027-FUND-INDICATOR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate().setValue(99);                                                                                        //Natural: MOVE 99 TO #AIAN027-DA-RATE
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Fund_Indicator().setValue(pdaAial102.getPnd_Aian102_Linkage_Pnd_Aian102_In_Fund_Code());                       //Natural: MOVE #AIAN102-IN-FUND-CODE TO #AIAN027-FUND-INDICATOR
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Origin_Product().setValue(pdaAial102.getPnd_Aian102_Linkage_Pnd_Aian102_Out_Var_Product_Code());               //Natural: MOVE #AIAN102-OUT-VAR-PRODUCT-CODE TO #AIAN027-ORIGIN-PRODUCT
        }                                                                                                                                                                 //Natural: END-IF
        //*   12/01/03  JS
        //*  CALLNAT 'AIAN027R' #AIAN027-LINKAGE
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(21) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28)      //Natural: IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28 OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25 OR NAZ-ACT-OPTION-CDE = 27
            || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(25) 
            || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(27)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-FUND-INDICATOR = 'T'
            if (condition((((pnd_Type_Of_Call.equals("TS") || pnd_Type_Of_Call.equals("TG")) && (pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(2)  //Natural: IF ( #TYPE-OF-CALL = 'TS' OR = 'TG' ) AND ( #IAAN0019-ORIGIN-FAMILY = 2 OR = 7 ) AND #AIAN027-EFFECT-DATE-WITHOUT-DAY > 200312
                || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(7))) && pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effect_Date_Without_Day().greater(200312))))
            {
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate().setValue("00");                                                                                  //Natural: MOVE '00' TO #AIAN027-DA-RATE
                DbsUtil.callnat(Aian027r.class , getCurrentProcessState(), pdaAiaa027r.getPnd_Aian027_Linkage());                                                         //Natural: CALLNAT 'AIAN027R' #AIAN027-LINKAGE
                if (condition(Global.isEscape())) return;
                //*  IF ADDED 6/15/2012  SLR
                if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code_Nbr().notEquals(getZero())))                                                     //Natural: IF #AIAN027-RETURN-CODE-NBR NOT = 0
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code());                   //Natural: MOVE #AIAN027-RETURN-CODE TO NAZ-ACT-RETURN-CODE
                    Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                              //Natural: ESCAPE MODULE
                }                                                                                                                                                         //Natural: END-IF
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate());                              //Natural: MOVE #AIAN027-IA-RATE TO #AIAN027-DA-RATE
                //*  DY2 FIX BEGINS >>
                //*  ELSE
                //*     IF NAZ-ACT-TYPE-OF-CALL='2' AND (#TYPE-OF-CALL ='TS' OR
                //*         #TYPE-OF-CALL ='TG' )
                //*         AND #EFF-DATE-N > 200312
                //*       MOVE #ISSUE-DATE-N TO #AIAN027-EFFECT-DATE-WITHOUT-DAY
                //*       MOVE 00            TO #AIAN027-EFFECTIVE-DAY
                //*       MOVE '00'  TO  #AIAN027-DA-RATE
                //*       CALLNAT 'AIAN027R' #AIAN027-LINKAGE
                //*       IF #AIAN027-RETURN-CODE-NBR  NOT =  0
                //*         MOVE #AIAN027-RETURN-CODE      TO  NAZ-ACT-RETURN-CODE
                //*         ESCAPE MODULE
                //*       END-IF
                //*       MOVE #AIAN027-IA-RATE  TO  #AIAN027-DA-RATE
                //*     END-IF
                //*  DY2 FIX ENDS   <<
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(4) || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Family().equals(11))) //Natural: IF #IAAN0019-ORIGIN-FAMILY = 4 OR = 11
        {
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Gsra_Ira_Tpa_Mdo_Ind().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #AIAN027-GSRA-IRA-TPA-MDO-IND
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Aian027r.class , getCurrentProcessState(), pdaAiaa027r.getPnd_Aian027_Linkage());                                                                 //Natural: CALLNAT 'AIAN027R' #AIAN027-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code_Nbr().notEquals(getZero())))                                                             //Natural: IF #AIAN027-RETURN-CODE-NBR NOT = 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code());                           //Natural: MOVE #AIAN027-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-FUND-INDICATOR = 'T'
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") && (pnd_Type_Of_Call.equals("TS") || pnd_Type_Of_Call.equals("TG"))        //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2' AND ( #TYPE-OF-CALL = 'TS' OR #TYPE-OF-CALL = 'TG' ) AND #EFF-DATE-N < 200401
            && pnd_Eff_Date_A_Pnd_Eff_Date_N.less(200401)))
        {
            pnd_Aian090_Linkage_Pnd_Aian090_Call_Type.setValue("F");                                                                                                      //Natural: MOVE 'F' TO #AIAN090-CALL-TYPE
            pnd_Aian090_Linkage_Pnd_Aian090_In_Rate.setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Da_Rate());                                                   //Natural: MOVE #AIAN027-DA-RATE TO #AIAN090-IN-RATE
            pnd_Aian090_Linkage_Pnd_Aian090_Eff_Date.setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Effect_Date_Without_Day());                                  //Natural: MOVE #AIAN027-EFFECT-DATE-WITHOUT-DAY TO #AIAN090-EFF-DATE
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(21) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28)  //Natural: IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28 OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25 OR NAZ-ACT-OPTION-CDE = 27
                || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(25) 
                || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(27)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Aian090_Linkage_Pnd_Aian090_Out_Rate.notEquals("  ")))                                                                                  //Natural: IF #AIAN090-OUT-RATE <> '  '
                {
                    DbsUtil.callnat(Aian090.class , getCurrentProcessState(), pnd_Aian090_Linkage);                                                                       //Natural: CALLNAT 'AIAN090' #AIAN090-LINKAGE
                    if (condition(Global.isEscape())) return;
                    pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate().setValue(pnd_Aian090_Linkage_Pnd_Aian090_Out_Rate);                                          //Natural: MOVE #AIAN090-OUT-RATE TO #AIAN027-IA-RATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code_Nbr().notEquals(getZero())))                                                             //Natural: IF #AIAN027-RETURN-CODE-NBR NOT = 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Return_Code());                           //Natural: MOVE #AIAN027-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*   END FIX
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 83
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200002
        //*      AND (NAZ-ACT-OPTION-CDE �= 21 OR NAZ-ACT-OPTION-CDE �= 28
        //*      OR NAZ-ACT-OPTION-CDE �= 30)
        //*    MOVE 25 TO #AIAN027-IA-RATE
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 83
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200006
        //*    MOVE 26 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 83 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 88
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200109
        //*    MOVE 27 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 88 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 91
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200202
        //*    MOVE 28 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 91 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 93
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200206
        //*    MOVE 30 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 93 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 95
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200209
        //*    MOVE 31 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 95 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 97
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200212
        //*    MOVE 34 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 97 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 99
        //*      AND #AIAN027-FUND-INDICATOR = 'T'
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200302
        //*    MOVE 35 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 99 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  IF NAZ-ACT-TYPE-OF-CALL = 'F' AND #AIAN027-DA-RATE = 99
        //*      AND #AIAN027-FUND-INDICATOR = 'T'
        //*      AND #AIAN027-EFFECTIVE-DATE-WITHOUT-DAY > 200306
        //*    MOVE 38 TO #AIAN027-IA-RATE
        //*    IF NAZ-ACT-OPTION-CDE = 21 OR NAZ-ACT-OPTION-CDE = 28
        //*        OR NAZ-ACT-OPTION-CDE = 30 OR NAZ-ACT-OPTION-CDE = 25
        //*        OR NAZ-ACT-OPTION-CDE = 27
        //*      MOVE 99 TO #AIAN027-IA-RATE
        //*    END-IF
        //*  END-IF
        //*  END FIX
        //*  IF #AIAN027-RETURN-CODE NOT = 0
        //*    MOVE #AIAN027-RETURN-CODE TO NAZ-ACT-RETURN-CODE
        //*    ESCAPE ROUTINE
        //*  END-IF
    }
    private void sub_Convert_Mode() throws Exception                                                                                                                      //Natural: CONVERT-MODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Mode.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode());                                                                                    //Natural: MOVE NAZ-ACT-PYMNT-MODE TO #MODE
        short decideConditionsMet2631 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MODE-1 = 1
        if (condition(pnd_Mode_Pnd_Mode_1.equals(1)))
        {
            decideConditionsMet2631++;
            pnd_Num_Of_Payments.setValue(12);                                                                                                                             //Natural: MOVE 12 TO #NUM-OF-PAYMENTS
            pnd_Months_Between_Payments.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #MONTHS-BETWEEN-PAYMENTS
        }                                                                                                                                                                 //Natural: WHEN #MODE-1 = 6
        else if (condition(pnd_Mode_Pnd_Mode_1.equals(6)))
        {
            decideConditionsMet2631++;
            pnd_Num_Of_Payments.setValue(4);                                                                                                                              //Natural: MOVE 4 TO #NUM-OF-PAYMENTS
            pnd_Months_Between_Payments.setValue(3);                                                                                                                      //Natural: MOVE 3 TO #MONTHS-BETWEEN-PAYMENTS
        }                                                                                                                                                                 //Natural: WHEN #MODE-1 = 7
        else if (condition(pnd_Mode_Pnd_Mode_1.equals(7)))
        {
            decideConditionsMet2631++;
            pnd_Num_Of_Payments.setValue(2);                                                                                                                              //Natural: MOVE 2 TO #NUM-OF-PAYMENTS
            pnd_Months_Between_Payments.setValue(6);                                                                                                                      //Natural: MOVE 6 TO #MONTHS-BETWEEN-PAYMENTS
        }                                                                                                                                                                 //Natural: WHEN #MODE-1 = 8
        else if (condition(pnd_Mode_Pnd_Mode_1.equals(8)))
        {
            decideConditionsMet2631++;
            pnd_Num_Of_Payments.setValue(1);                                                                                                                              //Natural: MOVE 1 TO #NUM-OF-PAYMENTS
            pnd_Months_Between_Payments.setValue(12);                                                                                                                     //Natural: MOVE 12 TO #MONTHS-BETWEEN-PAYMENTS
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                         //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(206);                                                                                //Natural: MOVE 206 TO NAZ-ACT-RETURN-CODE-NBR
            //* *      RITE 'INVALID MODE CODE ' #MODE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Calculate_Last_Payment_Date() throws Exception                                                                                                       //Natural: CALCULATE-LAST-PAYMENT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  IF NAZ-ACT-TYPE-OF-CALL ='1' OR NAZ-ACT-TYPE-OF-CALL = 'S'
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1") || pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40))) //Natural: IF NAZ-ACT-TYPE-OF-CALL = '1' OR #IAAN0019-CNTRCT-ORGN-CDE = 40
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'I'  OR NAZ-ACT-TYPE-OF-CALL = 'S'
            pnd_Ws_Guar_Period.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period());                                                                     //Natural: MOVE NAZ-ACT-GUAR-PERIOD TO #WS-GUAR-PERIOD
            pnd_Ws_Guar_Period_Mth.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth());                                                             //Natural: MOVE NAZ-ACT-GUAR-PERIOD-MTH TO #WS-GUAR-PERIOD-MTH
            //*   RITE 'GUAR' #WS-GUAR-PERIOD
            if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)))                                                                                                //Natural: IF #ISSUE-DATE-DAY > 01
            {
                //*     RITE 'ISSUE-N ' #ISSUE-MONTH-N #ISSUE-YEAR-N
                pnd_Last_Payment_Date_Pnd_Last_Payment_Month.setValue(pnd_Issue_Date_A_Pnd_Issue_Month_N);                                                                //Natural: MOVE #ISSUE-MONTH-N TO #LAST-PAYMENT-MONTH
                pnd_Last_Payment_Date_Pnd_Last_Payment_Year.setValue(pnd_Issue_Date_A_Pnd_Issue_Year_N);                                                                  //Natural: MOVE #ISSUE-YEAR-N TO #LAST-PAYMENT-YEAR
                pnd_Last_Payment_Date_Pnd_Last_Payment_Day.setValue(1);                                                                                                   //Natural: MOVE 01 TO #LAST-PAYMENT-DAY
                //* *  MOVE #NEXT-IA-PAYMENT-DATE TO #LAST-PAYMENT-DATE
                //* *  SUBTRACT #MONTHS-BETWEEN-PAYMENTS FROM #WS-GUAR-PERIOD-MTH
                //* *  IF #WS-GUAR-PERIOD-MTH < 0
                //* *    ADD 12 TO #WS-GUAR-PERIOD-MTH
                //* *    SUBTRACT 1 FROM #WS-GUAR-PERIOD
                //* *  END-IF
                //* *  RITE #WS-GUAR-PERIOD #WS-GUAR-PERIOD-MTH
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Last_Payment_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N8);                                                                                       //Natural: MOVE #ISSUE-DATE-N8 TO #LAST-PAYMENT-DATE
            }                                                                                                                                                             //Natural: END-IF
            //*   RITE 'LAST-PAY ' #LAST-PAYMENT-DATE
            //*  IF NAZ-ACT-TYPE-OF-CALL   = '1'
            if (condition(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().equals(40)))                                                                    //Natural: IF #IAAN0019-CNTRCT-ORGN-CDE = 40
            {
                //*     ADD 1 TO #SPIA-START-MONTH-N
                //*     MOVE 1 TO #SPIA-START-DAY-N
                short decideConditionsMet2679 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MODE-1 = 1
                if (condition(pnd_Mode_Pnd_Mode_1.equals(1)))
                {
                    decideConditionsMet2679++;
                    pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nadd(1);                                                                                                 //Natural: ADD 1 TO #LAST-PAYMENT-MONTH
                    //*         IF #ISSUE-DATE-DAY > #CUTOFF-DAY              /* DY1
                    //*  DY1
                    if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(21)))                                                                                       //Natural: IF #ISSUE-DATE-DAY > 21
                    {
                        pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nadd(1);                                                                                             //Natural: ADD 1 TO #LAST-PAYMENT-MONTH
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #MODE-1 = 6
                else if (condition(pnd_Mode_Pnd_Mode_1.equals(6)))
                {
                    decideConditionsMet2679++;
                    pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nadd(3);                                                                                                 //Natural: ADD 3 TO #LAST-PAYMENT-MONTH
                }                                                                                                                                                         //Natural: WHEN #MODE-1 = 7
                else if (condition(pnd_Mode_Pnd_Mode_1.equals(7)))
                {
                    decideConditionsMet2679++;
                    pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nadd(6);                                                                                                 //Natural: ADD 6 TO #LAST-PAYMENT-MONTH
                }                                                                                                                                                         //Natural: WHEN #MODE-1 = 8
                else if (condition(pnd_Mode_Pnd_Mode_1.equals(8)))
                {
                    decideConditionsMet2679++;
                    pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nadd(12);                                                                                                //Natural: ADD 12 TO #LAST-PAYMENT-MONTH
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*    ADD 1 TO #LAST-PAYMENT-MONTH
                if (condition(pnd_Last_Payment_Date_Pnd_Last_Payment_Month.greater(12)))                                                                                  //Natural: IF #LAST-PAYMENT-MONTH > 12
                {
                    pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nsubtract(12);                                                                                           //Natural: SUBTRACT 12 FROM #LAST-PAYMENT-MONTH
                    pnd_Last_Payment_Date_Pnd_Last_Payment_Year.nadd(1);                                                                                                  //Natural: ADD 1 TO #LAST-PAYMENT-YEAR
                    //*       MOVE 01 TO #SPIA-START-MONTH-N
                    //*       ADD 1 TO #SPIA-START-YEAR-N
                }                                                                                                                                                         //Natural: END-IF
                //*     RITE 'CUTCOMPARE'  #ISSUE-DATE-DAY  #CUTOFF-DAY
                //*     RITE #LAST-PAYMENT-MONTH
                //*    IF #ISSUE-DATE-DAY > #CUTOFF-DAY
                //*      ADD 1 TO #LAST-PAYMENT-MONTH
                //*       ADD 1 TO #SPIA-START-MONTH-N
                //*       RITE 'AFTER' #LAST-PAYMENT-MONTH
                //*      IF #LAST-PAYMENT-MONTH > 12
                //*        MOVE 01 TO #LAST-PAYMENT-MONTH
                //*        ADD 1 TO #LAST-PAYMENT-YEAR
                //*         MOVE 01 TO #SPIA-START-MONTH-N
                //*        ADD 1 TO #SPIA-START-YEAR-N
                //*      END-IF
                //*  END-IF
                //*     MOVE EDITED #SPIA-START-DATE-A TO NAZ-ACT-ASD-DATE (EM=YYYYMMDD)
            }                                                                                                                                                             //Natural: END-IF
            //*   RITE 'LAST-PAYMENT-DATE ' #LAST-PAYMENT-DATE #INTEGER-PERIODS
            //*                            #LAST-PAYMENT-YEAR #LAST-PAYMENT-MONTH
            //* * IF NAZ-ACT-OPTION-CDE NOT = 02
            if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().notEquals("IR")))                                                                    //Natural: IF #AIAN028-OUT-OPTION NOT = 'IR'
            {
                                                                                                                                                                          //Natural: PERFORM CONVERT-MODE
                sub_Convert_Mode();
                if (condition(Global.isEscape())) {return;}
                pnd_Last_Payment_Period.compute(new ComputeParameters(false, pnd_Last_Payment_Period), pnd_Ws_Guar_Period.multiply(pnd_Num_Of_Payments).add((pnd_Ws_Guar_Period_Mth.divide(pnd_Months_Between_Payments)))); //Natural: COMPUTE #LAST-PAYMENT-PERIOD = #WS-GUAR-PERIOD * #NUM-OF-PAYMENTS + ( #WS-GUAR-PERIOD-MTH / #MONTHS-BETWEEN-PAYMENTS )
                //* *                     - 1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Remaining_Payments_In_Year.compute(new ComputeParameters(false, pnd_Remaining_Payments_In_Year), pnd_Last_Payment_Period.mod(pnd_Num_Of_Payments));       //Natural: DIVIDE #NUM-OF-PAYMENTS INTO #LAST-PAYMENT-PERIOD GIVING #FULL-YEARS REMAINDER #REMAINING-PAYMENTS-IN-YEAR
            pnd_Full_Years.compute(new ComputeParameters(false, pnd_Full_Years), pnd_Last_Payment_Period.divide(pnd_Num_Of_Payments));
            pnd_Additional_Months.compute(new ComputeParameters(false, pnd_Additional_Months), pnd_Remaining_Payments_In_Year.multiply(pnd_Months_Between_Payments));     //Natural: COMPUTE #ADDITIONAL-MONTHS = #REMAINING-PAYMENTS-IN-YEAR * #MONTHS-BETWEEN-PAYMENTS
            //*   RITE 'ADDITIONAL-MONTHS ' #ADDITIONAL-MONTHS #NUM-OF-PAYMENTS
            //*                             #FULL-YEARS #MONTHS-BETWEEN-PAYMENTS
            pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nadd(pnd_Additional_Months);                                                                                     //Natural: ADD #ADDITIONAL-MONTHS TO #LAST-PAYMENT-MONTH
            pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nsubtract(pnd_Months_Between_Payments);                                                                          //Natural: SUBTRACT #MONTHS-BETWEEN-PAYMENTS FROM #LAST-PAYMENT-MONTH
            if (condition(pnd_Last_Payment_Date_Pnd_Last_Payment_Month.less(1)))                                                                                          //Natural: IF #LAST-PAYMENT-MONTH < 1
            {
                pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nadd(12);                                                                                                    //Natural: ADD 12 TO #LAST-PAYMENT-MONTH
                pnd_Last_Payment_Date_Pnd_Last_Payment_Year.nsubtract(1);                                                                                                 //Natural: SUBTRACT 1 FROM #LAST-PAYMENT-YEAR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Last_Payment_Date_Pnd_Last_Payment_Month.greater(12)))                                                                                      //Natural: IF #LAST-PAYMENT-MONTH > 12
            {
                pnd_Last_Payment_Date_Pnd_Last_Payment_Month.nsubtract(12);                                                                                               //Natural: SUBTRACT 12 FROM #LAST-PAYMENT-MONTH
                pnd_Last_Payment_Date_Pnd_Last_Payment_Year.nadd(1);                                                                                                      //Natural: ADD 1 TO #LAST-PAYMENT-YEAR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Payment_Date_Pnd_Last_Payment_Year.nadd(pnd_Full_Years);                                                                                             //Natural: ADD #FULL-YEARS TO #LAST-PAYMENT-YEAR
            //*   RITE 'LAST-PAYMENT-DATE ' #LAST-PAYMENT-DATE #LAST-PAYMENT-PERIOD
            //*                            #LAST-PAYMENT-YEAR #LAST-PAYMENT-MONTH
            //* *IF NAZ-ACT-OPTION-CDE = 02
            if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().equals("IR")))                                                                       //Natural: IF #AIAN028-OUT-OPTION = 'IR'
            {
                pnd_Final_Payment_Date.setValue(pnd_Last_Payment_Date);                                                                                                   //Natural: MOVE #LAST-PAYMENT-DATE TO #FINAL-PAYMENT-DATE
                pnd_Final_Payment_Date_Pnd_Final_Payment_Month.nadd(pnd_Months_Between_Payments);                                                                         //Natural: ADD #MONTHS-BETWEEN-PAYMENTS TO #FINAL-PAYMENT-MONTH
                if (condition(pnd_Final_Payment_Date_Pnd_Final_Payment_Month.greater(12)))                                                                                //Natural: IF #FINAL-PAYMENT-MONTH > 12
                {
                    pnd_Final_Payment_Date_Pnd_Final_Payment_Month.nsubtract(12);                                                                                         //Natural: SUBTRACT 12 FROM #FINAL-PAYMENT-MONTH
                    pnd_Final_Payment_Date_Pnd_Final_Payment_Year.nadd(1);                                                                                                //Natural: ADD 1 TO #FINAL-PAYMENT-YEAR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A.notEquals("        ")))                                                                           //Natural: IF #LAST-PAYMENT-DATE-A NOT = '        '
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pp_Pay_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A); //Natural: MOVE EDITED #LAST-PAYMENT-DATE-A TO NAZ-ACT-STD-FNL-PP-PAY-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A.notEquals("        ")))                                                                         //Natural: IF #FINAL-PAYMENT-DATE-A NOT = '        '
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pay_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A); //Natural: MOVE EDITED #FINAL-PAYMENT-DATE-A TO NAZ-ACT-STD-FNL-PAY-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            //*   RITE 'FINAL-PAYMENT-DATE ' #FINAL-PAYMENT-DATE
            //*   RITE 'LAST-PAYMENT-DATE ' #LAST-PAYMENT-DATE #LAST-PAYMENT-PERIOD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2")))                                                                          //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2'
        {
            //*  IF NAZ-ACT-TYPE-OF-CALL = 'F'
            if (condition(pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A.notEquals("        ")))                                                                           //Natural: IF #LAST-PAYMENT-DATE-A NOT = '        '
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pp_Pay_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A); //Natural: MOVE EDITED #LAST-PAYMENT-DATE-A TO NAZ-ACT-STD-FNL-PP-PAY-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A.notEquals("        ")))                                                                         //Natural: IF #FINAL-PAYMENT-DATE-A NOT = '        '
            {
                //*     RITE 'MOVING FINAL DATE'
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pay_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Final_Payment_Date_Pnd_Final_Payment_Date_A); //Natural: MOVE EDITED #FINAL-PAYMENT-DATE-A TO NAZ-ACT-STD-FNL-PAY-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   RITE 'FINAL-PAYMENT-DATE ' #FINAL-PAYMENT-DATE
        //*   RITE 'LAST-PAYMENT-DATE ' #LAST-PAYMENT-DATE #LAST-PAYMENT-PERIOD
        if (condition(pnd_Last_Payment_Date_Pnd_Last_Payment_Date_A.equals("        ")))                                                                                  //Natural: IF #LAST-PAYMENT-DATE-A = '        '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Last_Payment_Date.less(pnd_Issue_Date_A_Pnd_Issue_Date_N8)))                                                                                    //Natural: IF #LAST-PAYMENT-DATE < #ISSUE-DATE-N8
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                         //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(216);                                                                                //Natural: MOVE 216 TO NAZ-ACT-RETURN-CODE-NBR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Guaranteed_Period() throws Exception                                                                                                       //Natural: CALCULATE-GUARANTEED-PERIOD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *  RITE 'CALCULATE-GUARANTEED-PERIOD'
        //* *  RITE 'MONTHS  ' #NUMBER-OF-MONTHS
        //* *  RITE 'LAST-PAYMENT ' #LAST-PAYMENT-MONTH
        //* *  RITE 'NEXT-IA-PAY  ' #NEXT-IA-PAYMENT-MONTH
        //* *  RITE 'LAST-PAYMENT ' #LAST-PAYMENT-YEAR
        //* *  RITE 'NEXT-IA-PAY  ' #NEXT-IA-PAYMENT-YEAR
        pnd_Number_Of_Months.compute(new ComputeParameters(false, pnd_Number_Of_Months), pnd_Last_Payment_Date_Pnd_Last_Payment_Month.subtract(pnd_Ws_Date_Pnd_Ws_Month).add(pnd_Months_Between_Payments)); //Natural: COMPUTE #NUMBER-OF-MONTHS = #LAST-PAYMENT-MONTH - #WS-MONTH + #MONTHS-BETWEEN-PAYMENTS
        pnd_Number_Of_Years.compute(new ComputeParameters(false, pnd_Number_Of_Years), pnd_Last_Payment_Date_Pnd_Last_Payment_Year.subtract(pnd_Ws_Date_Pnd_Ws_Year));    //Natural: COMPUTE #NUMBER-OF-YEARS = #LAST-PAYMENT-YEAR - #WS-YEAR
        if (condition(pnd_Number_Of_Months.less(getZero())))                                                                                                              //Natural: IF #NUMBER-OF-MONTHS < 0
        {
            pnd_Number_Of_Months.nadd(12);                                                                                                                                //Natural: ADD 12 TO #NUMBER-OF-MONTHS
            pnd_Number_Of_Years.nsubtract(1);                                                                                                                             //Natural: SUBTRACT 1 FROM #NUMBER-OF-YEARS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Final_Certain_Period.compute(new ComputeParameters(true, pnd_Final_Certain_Period), pnd_Number_Of_Years.add((pnd_Number_Of_Months.divide(12))));              //Natural: COMPUTE ROUNDED #FINAL-CERTAIN-PERIOD = #NUMBER-OF-YEARS + ( #NUMBER-OF-MONTHS / 12 )
        //* *COMPUTE #NUM-OF-PAYMENTS-IN-GUAR-PER = (#NUMBER-OF-YEARS *
        //* *                                         #NUM-OF-PAYMENTS ) +
        //* *                                        (#NUMBER-OF-MONTHS /
        //* *                                         #MONTHS-BETWEEN-PAYMENTS)
        //* *ITE 'NUMBER-OF-PAYMENTS ' #NUM-OF-PAYMENTS-IN-GUAR-PER
        //* *  RITE 'FINAL-CERTAIN-PERIOD  ' #FINAL-CERTAIN-PERIOD
    }
    private void sub_Move_Variables_To_Std_Output() throws Exception                                                                                                      //Natural: MOVE-VARIABLES-TO-STD-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *OVE NAZ-ACT-TIAA-STD-RATE-CDE (#I) TO NAZ-ACT-TIAA-O-STD-RATE-CDE (#I)
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_I).setValue(pnd_Tiaa_Guaranteed);                                                //Natural: MOVE #TIAA-GUARANTEED TO NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_I).setValue(pnd_Tiaa_Dividend);                                                  //Natural: MOVE #TIAA-DIVIDEND TO NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #I )
        //*  IF #TIAA-GUARANTEED = 0 AND #TIAA-DIVIDEND = 0
        //*     MOVE '  '             TO NAZ-ACT-TIAA-O-STD-RATE-CDE (#I)
        //*  ELSE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_I).setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate());           //Natural: MOVE #AIAN027-IA-RATE TO NAZ-ACT-TIAA-O-STD-RATE-CDE ( #I )
        //*  END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Gic_Code().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-GIC-CODE ( #I ) TO NAZ-ACT-TIAA-O-STD-GIC-CODE ( #I )
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(28) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(30)))    //Natural: IF NAZ-ACT-OPTION-CDE = 28 OR = 30
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I)),  //Natural: MULTIPLY GUAR-RATE BY 100.00 GIVING NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #I )
                ldaAial770r.getAct_Tpa_Rate_File_Rate_View_Guar_Rate().multiply(new DbsDecimal("100.00")));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I)),  //Natural: MULTIPLY #AIAN027-GUARA-INTEREST-RATE BY 100.00 GIVING NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #I )
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate().multiply(new DbsDecimal("100.00")));
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Variables_To_Grd_Output() throws Exception                                                                                                      //Natural: MOVE-VARIABLES-TO-GRD-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* *OVE NAZ-ACT-TIAA-GRADED-RATE-CDE (#I) TO
        //* *                                      NAZ-ACT-TIAA-O-GRD-RATE-CDE (#I)
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue(pnd_I).setValue(pnd_Tiaa_Guaranteed);                                                //Natural: MOVE #TIAA-GUARANTEED TO NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_I).setValue(pnd_Tiaa_Dividend);                                                  //Natural: MOVE #TIAA-DIVIDEND TO NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #I )
        //*  IF #TIAA-GUARANTEED = 0 AND #TIAA-DIVIDEND = 0
        //*     MOVE '  '             TO NAZ-ACT-TIAA-O-GRD-RATE-CDE (#I)
        //*  ELSE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_I).setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate());           //Natural: MOVE #AIAN027-IA-RATE TO NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #I )
        //*  END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Gic_Code().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-GRADED-GIC-CODE ( #I ) TO NAZ-ACT-TIAA-O-GRD-GIC-CODE ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_I)),  //Natural: MULTIPLY #AIAN027-GUARA-INTEREST-RATE BY 100.00 GIVING NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( #I )
            pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate().multiply(new DbsDecimal("100.00")));
    }
    private void sub_Move_Variables_To_Annual_Output() throws Exception                                                                                                   //Natural: MOVE-VARIABLES-TO-ANNUAL-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* *MOVE NAZ-ACT-CREF-ANNUAL-RATE-CDE (#I) TO
        //* *                                    NAZ-ACT-CREF-O-ANN-RATE-CDE (#I)
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Cde().getValue(pnd_I).setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate());           //Natural: MOVE #AIAN027-IA-RATE TO NAZ-ACT-CREF-O-ANN-RATE-CDE ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Pymnt_Units().getValue(pnd_I).setValue(pnd_Cref_Payout_Units);                                           //Natural: MOVE #CREF-PAYOUT-UNITS TO NAZ-ACT-CREF-O-ANN-PYMNT-UNITS ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Unit_Value().getValue(pnd_I).setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va()); //Natural: MOVE #AIAN026-INTERIM-ANNUITY-UNIT-VA TO NAZ-ACT-CREF-O-ANN-UNIT-VALUE ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Amt().getValue(pnd_I).setValue(pnd_Cref_Payout_Amount);                                             //Natural: MOVE #CREF-PAYOUT-AMOUNT TO NAZ-ACT-CREF-O-ANN-RATE-AMT ( #I )
    }
    private void sub_Move_Variables_To_Monthly_Output() throws Exception                                                                                                  //Natural: MOVE-VARIABLES-TO-MONTHLY-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* *  MOVE NAZ-ACT-CREF-MTHLY-RATE-CDE (#I) TO
        //* *                                    NAZ-ACT-CREF-O-MTH-RATE-CDE (#I)
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Cde().getValue(pnd_I).setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate());           //Natural: MOVE #AIAN027-IA-RATE TO NAZ-ACT-CREF-O-MTH-RATE-CDE ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Pymnt_Units().getValue(pnd_I).setValue(pnd_Cref_Payout_Units);                                           //Natural: MOVE #CREF-PAYOUT-UNITS TO NAZ-ACT-CREF-O-MTH-PYMNT-UNITS ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Unit_Value().getValue(pnd_I).setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va()); //Natural: MOVE #AIAN026-INTERIM-ANNUITY-UNIT-VA TO NAZ-ACT-CREF-O-MTH-UNIT-VALUE ( #I )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Amt().getValue(pnd_I).setValue(pnd_Cref_Payout_Amount);                                             //Natural: MOVE #CREF-PAYOUT-AMOUNT TO NAZ-ACT-CREF-O-MTH-RATE-AMT ( #I )
    }
    private void sub_Call_Aian074_Ipro_Calculation() throws Exception                                                                                                     //Natural: CALL-AIAN074-IPRO-CALCULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Rate_Basis().getValue("*").reset();                                                                                   //Natural: RESET #L-IPRO-RATE-BASIS ( * )
        pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Return_Code().reset();                                                                                                //Natural: RESET #L-IPRO-RETURN-CODE
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().equals(25)))                                                                             //Natural: IF NAZ-ACT-OPTION-CDE = 25
        {
            pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Product_Code().setValue("D");                                                                                     //Natural: MOVE 'D' TO #L-IPRO-PRODUCT-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Product_Code().setValue("G");                                                                                     //Natural: MOVE 'G' TO #L-IPRO-PRODUCT-CODE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONVERT-DATE-TO-NUMERIC
        sub_Convert_Date_To_Numeric();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-DATES
        sub_Check_Dates();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Date_Pnd_Ws_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(),new ReportEditMask("YYYYMMDD"));                                //Natural: MOVE EDITED NAZ-ACT-ASD-DATE ( EM = YYYYMMDD ) TO #WS-DATE-A
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2")))                                                                          //Natural: IF NAZ-ACT-TYPE-OF-CALL = '2'
        {
            pnd_Ws_Date.setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N8);                                                                                                         //Natural: MOVE #EFF-DATE-N8 TO #WS-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHANGED 7/25/2012  SLR
        pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Eff_Date().setValue(pnd_Ws_Date);                                                                                     //Natural: MOVE #WS-DATE TO #L-IPRO-EFF-DATE
        FOR19:                                                                                                                                                            //Natural: FOR #I = 1 TO 250
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
        {
            //*   END FIX
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I).equals("  ")))                                                //Natural: IF NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) = '  '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Rate_Basis().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-RATE-CDE ( #I ) TO #L-IPRO-RATE-BASIS ( #I )
            pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Rate_Accum().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) TO #L-IPRO-RATE-ACCUM ( #I ) NAZ-ACT-TIAA-STD-FNL-GUAR-AMT ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Guar_Amt().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MOVE 'Y' TO #L-IPRO-DISPLAY-INDICATOR
        DbsUtil.callnat(Aian074.class , getCurrentProcessState(), pdaAiaa074.getPnd_L_Ipro_Linkage());                                                                    //Natural: CALLNAT 'AIAN074' #L-IPRO-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Return_Code().notEquals(getZero())))                                                                    //Natural: IF #L-IPRO-RETURN-CODE NOT = 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Return_Program_Code());                      //Natural: MOVE #L-IPRO-RETURN-PROGRAM-CODE TO NAZ-ACT-RETURN-CODE
            //*   RITE 'AIAN074 RETURN CODE = ' #L-IPRO-RETURN-CODE
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Date_A.setValue(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N);                                                            //Natural: MOVE #1ST-ANN-BIRTH-DATE-N TO #IPRO-FINAL-DATE-A
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Year.nadd(70);                                                                                                                 //Natural: ADD 70 TO #IPRO-FINAL-YEAR
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Month.nadd(6);                                                                                                                 //Natural: ADD 6 TO #IPRO-FINAL-MONTH
        if (condition(pnd_Ipro_Final_Date_Pnd_Ipro_Final_Month.greater(12)))                                                                                              //Natural: IF #IPRO-FINAL-MONTH > 12
        {
            pnd_Ipro_Final_Date_Pnd_Ipro_Final_Month.nsubtract(12);                                                                                                       //Natural: SUBTRACT 12 FROM #IPRO-FINAL-MONTH
            pnd_Ipro_Final_Date_Pnd_Ipro_Final_Year.nadd(1);                                                                                                              //Natural: ADD 1 TO #IPRO-FINAL-YEAR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Year.nadd(1);                                                                                                                  //Natural: ADD 1 TO #IPRO-FINAL-YEAR
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Month.setValue(4);                                                                                                             //Natural: MOVE 04 TO #IPRO-FINAL-MONTH
        pnd_Ipro_Final_Date_Pnd_Ipro_Final_Day.setValue(1);                                                                                                               //Natural: MOVE 01 TO #IPRO-FINAL-DAY
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pp_Pay_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ipro_Final_Date_Pnd_Ipro_Final_Date_A);      //Natural: MOVE EDITED #IPRO-FINAL-DATE-A TO NAZ-ACT-STD-FNL-PP-PAY-DTE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pay_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ipro_Final_Date_Pnd_Ipro_Final_Date_A);         //Natural: MOVE EDITED #IPRO-FINAL-DATE-A TO NAZ-ACT-STD-FNL-PAY-DTE ( EM = YYYYMMDD )
        //*  CHANGED 7/25/2012  SLR
        pnd_Type_Of_Call.setValue("TS");                                                                                                                                  //Natural: MOVE 'TS' TO #TYPE-OF-CALL
        FOR20:                                                                                                                                                            //Natural: FOR #I = 1 TO 250
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
        {
            //*   END FIX
            if (condition(pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Rate_Basis().getValue(pnd_I).equals("  ")))                                                         //Natural: IF #L-IPRO-RATE-BASIS ( #I ) = '  '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Mthly_Int_Rate.compute(new ComputeParameters(false, pnd_Mthly_Int_Rate), NMath.pow (pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Tot_Rate().getValue(pnd_I)  //Natural: COMPUTE #MTHLY-INT-RATE = #L-IPRO-TOT-RATE ( #I ) ** ( 1 / 12E0 )
                ,(DbsField.divide(1,12))));
            pnd_Ann_Factor.getValue(pnd_I).compute(new ComputeParameters(true, pnd_Ann_Factor.getValue(pnd_I)), pnd_Mthly_Int_Rate.subtract(1));                          //Natural: COMPUTE ROUNDED #ANN-FACTOR ( #I ) = #MTHLY-INT-RATE - 1
            //*   RITE '#M-INT-RATE=' #MTHLY-INT-RATE 'I-TRATE=' #L-IPRO-TOT-RATE (#I)
            //*   RITE '#ANN-FACTOR(' #I ')= ' #ANN-FACTOR (#I)
            //*   RITE '#I=' #I '#L-IPRO-RATE-BASIS = '   #L-IPRO-RATE-BASIS (#I)
            //*    '#I=' #I '#L-IPRO-GUAR-PAYMENT = ' #L-IPRO-GUAR-PAYMENT (#I)
            //*    '#L-IPRO-TOT-PAYMENT  = ' #L-IPRO-TOT-PAYMENT (#I)
            //*    '#L-IPRO-GUAR-RATE    = ' #L-IPRO-GUAR-RATE (#I)
            //*    '#L-IPRO-TOT-RATE     = ' #L-IPRO-TOT-RATE (#I)
            pnd_Tiaa_Rate_Cde.setValue(pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Rate_Basis().getValue(pnd_I));                                                         //Natural: MOVE #L-IPRO-RATE-BASIS ( #I ) TO #TIAA-RATE-CDE
            //*  PERFORM CHOOSE-MORTALITY-TABLE
            //*  MOVE #L-IPRO-RATE-BASIS (#I) TO NAZ-ACT-TIAA-O-STD-RATE-CDE (#I)
            //*  MOVE #AIAN027-IA-RATE TO NAZ-ACT-TIAA-O-STD-RATE-CDE (#I)
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_I).setValue(pnd_Tiaa_Rate_Cde);                                              //Natural: MOVE #TIAA-RATE-CDE TO NAZ-ACT-TIAA-O-STD-RATE-CDE ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Gic_Code().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-GIC-CODE ( #I ) TO NAZ-ACT-TIAA-O-STD-GIC-CODE ( #I )
            pnd_Ws_Ipro_Rate.compute(new ComputeParameters(false, pnd_Ws_Ipro_Rate), pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Guar_Rate().getValue(pnd_I).subtract(1)); //Natural: COMPUTE #WS-IPRO-RATE = #L-IPRO-GUAR-RATE ( #I ) - 1
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I)),  //Natural: MULTIPLY #WS-IPRO-RATE BY 100.00 GIVING NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #I )
                pnd_Ws_Ipro_Rate.multiply(new DbsDecimal("100.00")));
            //*   RITE 'EFF' NAZ-ACT-TIAA-STD-EFF-RATE-INT (#I)
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_I).setValue(pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Guar_Payment().getValue(pnd_I)); //Natural: MOVE #L-IPRO-GUAR-PAYMENT ( #I ) TO NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_I)),  //Natural: COMPUTE NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #I ) = #L-IPRO-TOT-PAYMENT ( #I ) - #L-IPRO-GUAR-PAYMENT ( #I )
                pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Tot_Payment().getValue(pnd_I).subtract(pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Guar_Payment().getValue(pnd_I)));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Interest_Rate().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Interest_Rate().getValue(pnd_I)),  //Natural: COMPUTE NAZ-GUAR-INTEREST-RATE ( #I ) = #L-IPRO-GUAR-RATE ( #I ) - 1
                pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Guar_Rate().getValue(pnd_I).subtract(1));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor().getValue(pnd_I).compute(new ComputeParameters(true, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED NAZ-GUAR-ANN-FACTOR ( #I ) = #L-IPRO-GUAR-RATE ( #I ) ** ( 1 / 12E0 ) - 1
                (NMath.pow (pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Guar_Rate().getValue(pnd_I) ,(DbsField.divide(1,12))).subtract(1)));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Interest_Rate().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Interest_Rate().getValue(pnd_I)),  //Natural: COMPUTE NAZ-TOTAL-INTEREST-RATE ( #I ) = #L-IPRO-TOT-RATE ( #I ) - 1
                pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Tot_Rate().getValue(pnd_I).subtract(1));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor().getValue(pnd_I).compute(new ComputeParameters(true, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED NAZ-TOTAL-ANN-FACTOR ( #I ) = #L-IPRO-TOT-RATE ( #I ) ** ( 1 / 12E0 ) - 1
                (NMath.pow (pdaAiaa074.getPnd_L_Ipro_Linkage_Pnd_L_Ipro_Tot_Rate().getValue(pnd_I) ,(DbsField.divide(1,12))).subtract(1)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Life_Expectancy_For_Pa() throws Exception                                                                                                  //Natural: CALCULATE-LIFE-EXPECTANCY-FOR-PA
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Mode_1().setValue(8);                                                                                   //Natural: MOVE 8 TO #AIAN021-IN-PAYMENT-MODE-1
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Mode_2().setValue(pnd_Issue_Date_A_Pnd_Issue_Month_N);                                                  //Natural: MOVE #ISSUE-MONTH-N TO #AIAN021-IN-PAYMENT-MODE-2
        //* *MOVE 1   TO #AIAN021-IN-1ST-ANN-SEX
        //* *MOVE #IN-1ST-ANN-BIRTH-DATE-N TO #AIAN021-IN-1ST-ANN-BIRTH-DATE
        //*  IF #IAAN0019-CNTRCT-ORGN-CDE = 40 AND #ISSUE-DATE-DAY = 01
        //*    MOVE 0 TO #AIAN021-IN-DEFERRED-PERIOD
        //* *  MOVE 0 TO NAZ-DEFERRAL-PERIOD
        //*  END-IF
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Deferred_Period().setValue(0);                                                                                  //Natural: MOVE 0 TO #AIAN021-IN-DEFERRED-PERIOD
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("SL");                                                                                //Natural: MOVE 'SL' TO #AIAN021-IN-PAYMENT-OPTION
        if (condition(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N.greater(getZero())))                                                                              //Natural: IF #2ND-ANN-BIRTH-DATE-N > 0
        {
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_2nd_Ann_Birth_Date().setValue(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N);                           //Natural: MOVE #2ND-ANN-BIRTH-DATE-N TO #AIAN021-IN-2ND-ANN-BIRTH-DATE
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().setValue("LS");                                                                            //Natural: MOVE 'LS' TO #AIAN021-IN-PAYMENT-OPTION
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Pct_To_Second_Ann().setValue(100);                                                                          //Natural: MOVE 100 TO #AIAN021-IN-PCT-TO-SECOND-ANN
            pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_2nd_Ann_Sex().setValue(2);                                                                                  //Natural: MOVE 2 TO #AIAN021-IN-2ND-ANN-SEX
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE #IN-ISSUE-YEAR-N  TO #AIAN027-EFFECTIVE-YEAR
        //* *MOVE #IN-ISSUE-MONTH-N TO #AIAN027-EFFECTIVE-MONTH
        //* *MOVE 0 TO #AIAN027-EFFECTIVE-DAY
        //* *MOVE 'T' TO #AIAN027-FUND-INDICATOR
        //*   11/01/03  JS
        //*  MOVE 32 TO #AIAN027-DA-RATE
        //* *MOVE '32' TO #AIAN027-DA-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(0);                                                                                    //Natural: MOVE 0 TO #AIAN021-IN-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(0);                                                                                //Natural: MOVE 0 TO #AIAN021-IN-DEF-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Certain_Period().setValue(0);                                                                                   //Natural: MOVE 0 TO #AIAN021-IN-CERTAIN-PERIOD
        //*  MOVE 0 TO NAZ-CERTAIN-PERIOD
        //* *IAN021-IN-DEFERRED-PERIOD
        //* * RITE '=' *PROGRAM '=' *LINE '=' #AIAN021-IN-MORTALITY-TABLE
        DbsUtil.callnat(Aian021.class , getCurrentProcessState(), pdaAial0210.getPnd_Aian021_Linkage());                                                                  //Natural: CALLNAT 'AIAN021' #AIAN021-LINKAGE
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pa_Expected_Life.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());                                                            //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #WS-PA-EXPECTED-LIFE
        getReports().write(0, "N051* PA-LE>>","=",pnd_Ws_Pa_Expected_Life,"=",pnd_Certain_Period);                                                                        //Natural: WRITE 'N051* PA-LE>>' '=' #WS-PA-EXPECTED-LIFE '=' #CERTAIN-PERIOD
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pa_Expected_Life.less(pnd_Certain_Period)))                                                                                                  //Natural: IF #WS-PA-EXPECTED-LIFE < #CERTAIN-PERIOD
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                         //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(301);                                                                                //Natural: MOVE 301 TO NAZ-ACT-RETURN-CODE-NBR
            //*   RITE 'CAN NOT TAKE GUARANTEE'
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF ADDED 6/15/2012  SLR
        if (condition(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code_Nbr().notEquals(getZero())))                                                         //Natural: IF #AIAN021-OUT-RETURN-CODE-NBR NOT = 0
        {
            //*       RITE 'ERROR IN AIAN021 ' #AIAN021-LINKAGE
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code());                       //Natural: MOVE #AIAN021-OUT-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_State_Of_Florida_Compliance_Check() throws Exception                                                                                                 //Natural: STATE-OF-FLORIDA-COMPLIANCE-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  THIS SECTION ADDED 08/2007 BY ANDY SMITH
        //*  CHECKS THE ANNUITY FACTOR USED FOR REAL ESTATE AND TC LIFE VARIABLE
        //*  AGAINST THE ANNUITY FACTOR FROM THE '37 SA(0,0) TABLE @ 3.50% FOR
        //*  COMPLIANCE WITH FLORIDA STATUTE 69O-162.008
        //*  NOT APPLICABLE FOR ANNUITY CERTAIN ISSUES
        //* **********************************************************************
        pnd_Hold_Annuity_Factor.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor());                                                            //Natural: MOVE #AIAN021-OUT-ANNUITY-FACTOR TO #HOLD-ANNUITY-FACTOR
        pnd_Hold_Mortality_Table.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table());                                                           //Natural: MOVE #AIAN021-IN-MORTALITY-TABLE TO #HOLD-MORTALITY-TABLE
        pnd_Hold_Interest_Rate.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate());                                                               //Natural: MOVE #AIAN021-IN-INTEREST-RATE TO #HOLD-INTEREST-RATE
        pnd_Hold_Def_Interest_Rate.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate());                                                       //Natural: MOVE #AIAN021-IN-DEF-INTEREST-RATE TO #HOLD-DEF-INTEREST-RATE
        pnd_Hold_Setback.setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback());                                                                           //Natural: MOVE #AIAN021-IN-SETBACK TO #HOLD-SETBACK
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(63);                                                                                 //Natural: MOVE 0063 TO #AIAN021-IN-MORTALITY-TABLE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(0);                                                                                    //Natural: MOVE .0350 TO #AIAN021-IN-INTEREST-RATE #AIAN021-IN-DEF-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(0);
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(0);                                                                                          //Natural: MOVE 0 TO #AIAN021-IN-SETBACK
        //* * RITE '=' *PROGRAM '=' *LINE '=' #AIAN021-IN-MORTALITY-TABLE
        DbsUtil.callnat(Aian021.class , getCurrentProcessState(), pdaAial0210.getPnd_Aian021_Linkage());                                                                  //Natural: CALLNAT 'AIAN021' #AIAN021-LINKAGE
        if (condition(Global.isEscape())) return;
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Mortality_Table().setValue(pnd_Hold_Mortality_Table);                                                           //Natural: MOVE #HOLD-MORTALITY-TABLE TO #AIAN021-IN-MORTALITY-TABLE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Interest_Rate().setValue(pnd_Hold_Interest_Rate);                                                               //Natural: MOVE #HOLD-INTEREST-RATE TO #AIAN021-IN-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Def_Interest_Rate().setValue(pnd_Hold_Def_Interest_Rate);                                                       //Natural: MOVE #HOLD-DEF-INTEREST-RATE TO #AIAN021-IN-DEF-INTEREST-RATE
        pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Setback().setValue(pnd_Hold_Setback);                                                                           //Natural: MOVE #HOLD-SETBACK TO #AIAN021-IN-SETBACK
        if (condition(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Annuity_Factor().greater(pnd_Hold_Annuity_Factor) && pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_In_Payment_Option().notEquals("AC"))) //Natural: IF #AIAN021-OUT-ANNUITY-FACTOR > #HOLD-ANNUITY-FACTOR AND #AIAN021-IN-PAYMENT-OPTION NOT = 'AC'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue("AIAN051 ");                                                                         //Natural: MOVE 'AIAN051 ' TO NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(212);                                                                                //Natural: MOVE 212 TO NAZ-ACT-RETURN-CODE-NBR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF ADDED 6/15/2012  SLR
        if (condition(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code_Nbr().notEquals(getZero())))                                                         //Natural: IF #AIAN021-OUT-RETURN-CODE-NBR NOT = 0
        {
            //*       RITE 'ERROR IN AIAN021 ' #AIAN021-LINKAGE
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaAial0210.getPnd_Aian021_Linkage_Pnd_Aian021_Out_Return_Code());                       //Natural: MOVE #AIAN021-OUT-RETURN-CODE TO NAZ-ACT-RETURN-CODE
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Iaan0019() throws Exception                                                                                                                     //Natural: CALL-IAAN0019
    {
        if (BLNatReinput.isReinput()) return;

        //*  MOVE NAZ-ACT-ANN-DT-OF-BRTH              TO #HOLD-DATE1
        //*  MOVE NAZ-ACT-2ND-ANN-DT-OF-BRTH          TO #HOLD-DATE2
        pnd_Hold_Date1.setValue(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N);                                                                                       //Natural: MOVE #1ST-ANN-BIRTH-DATE-N TO #HOLD-DATE1
        pnd_Hold_Date2.setValue(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N);                                                                                       //Natural: MOVE #2ND-ANN-BIRTH-DATE-N TO #HOLD-DATE2
        pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Orgn_Cde().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde());                             //Natural: MOVE NAZ-ACT-ORIGIN-CDE TO #IAAN0019-CNTRCT-ORGN-CDE
        pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Cntrct_Ppcn_Nbr().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Ia_Cntrct_Nmbr());                    //Natural: MOVE NAZ-ACT-TIAA-IA-CNTRCT-NMBR TO #IAAN0019-CNTRCT-PPCN-NBR
        //*  MOVE #EFF-DATE-N             TO #IAAN0019-EFFECTIVE-DATE
        if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("2") || (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().equals("1")  //Natural: IF ( NAZ-ACT-TYPE-OF-CALL = '2' OR ( NAZ-ACT-TYPE-OF-CALL = '1' AND #ISSUE-DATE-DAY > 01 ) ) THEN
            && pnd_Issue_Date_A_Pnd_Issue_Date_Day.greater(1)))))
        {
            pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Effective_Date().setValue(pnd_Eff_Date_A_Pnd_Eff_Date_N8);                                                      //Natural: MOVE #EFF-DATE-N8 TO #IAAN0019-EFFECTIVE-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Effective_Date().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N8);                                                  //Natural: MOVE #ISSUE-DATE-N8 TO #IAAN0019-EFFECTIVE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Iaiq_Option().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde());                                 //Natural: MOVE NAZ-ACT-OPTION-CDE TO #IAAN0019-IAIQ-OPTION
        if (condition(pnd_Hold_Date1.greater(pnd_Hold_Date2)))                                                                                                            //Natural: IF #HOLD-DATE1 GT #HOLD-DATE2
        {
            pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Date_Birth().setValue(pnd_Hold_Date1);                                                                          //Natural: MOVE #HOLD-DATE1 TO #IAAN0019-DATE-BIRTH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Date_Birth().setValue(pnd_Hold_Date2);                                                                          //Natural: MOVE #HOLD-DATE2 TO #IAAN0019-DATE-BIRTH
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hold_Calc.compute(new ComputeParameters(false, pnd_Hold_Calc), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth().divide(12));                   //Natural: COMPUTE #HOLD-CALC = NAZ-ACT-GUAR-PERIOD-MTH / 12
        pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Final_Prd_Payt().compute(new ComputeParameters(false, pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Final_Prd_Payt()),  //Natural: COMPUTE #IAAN0019-FINAL-PRD-PAYT = NAZ-ACT-GUAR-PERIOD + #HOLD-CALC
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().add(pnd_Hold_Calc));
        DbsUtil.callnat(Iaan0019.class , getCurrentProcessState(), pdaIaaa0019.getPnd_Iaan0019_Parm());                                                                   //Natural: CALLNAT 'IAAN0019' USING #IAAN0019-PARM
        if (condition(Global.isEscape())) return;
        //*  IF RE-WRITTEN 6/15/2012  SLR
        if (condition(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Rtrn_Cde().notEquals(getZero())))                                                                     //Natural: IF #IAAN0019-RTRN-CDE NE 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code().setValue(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Rtrn_Cde_Pgm());                           //Natural: MOVE #IAAN0019-RTRN-CDE-PGM TO NAZ-ACT-RETURN-CODE
            //*  FOR DEBUGGING   5/5/2012 SLR
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIaaa0019.getPnd_Iaan0019_Parm_Pnd_Iaan0019_Origin_Fixed_Product().equals(" ")))                                                                  //Natural: IF #IAAN0019-ORIGIN-FIXED-PRODUCT = ' '
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().setValue(19);                                                                                 //Natural: MOVE 019 TO NAZ-ACT-RETURN-CODE-NBR
            Global.setEscapeCode(EscapeType.Module); Global.setEscape(true); return;                                                                                      //Natural: ESCAPE MODULE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #IAAN0019-RTRN-CDE NOT EQUAL 0
        //* *IF #IAAN0019-RTRN-CDE NOT EQUAL 0 OR
        //* *   #IAAN0019-ORIGIN-FIXED-PRODUCT = ' '
        //* *    MOVE 'IAAN0019' TO NAZ-ACT-RETURN-CODE-PGM
        //* *MOVE 999 TO NAZ-ACT-RETURN-CODE-NBR
        //* *ESCAPE ROUTINE
        //* *D-IF
    }
    private void sub_Is_Option_P_And_I() throws Exception                                                                                                                 //Natural: IS-OPTION-P-AND-I
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  IF NAZ-ACT-OPTION-CDE NOT EQUAL 21 ESCAPE ROUTINE END-IF
        //*  CHANGED 7/25/2012   SLR
        if (condition(pdaAiaa028.getPnd_Aian028_Linkage_Pnd_Aian028_Out_Option().notEquals("PI")))                                                                        //Natural: IF #AIAN028-OUT-OPTION NOT EQUAL TO 'PI'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR21:                                                                                                                                                            //Natural: FOR #I = 1 TO 250
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
        {
            //*  COMPUTE #WS-PERIODIC-GUAR-RATE = #AIAN027-GUARA-INTEREST-RATE ** (1 /
            //*               #NUM-OF-PAYMENTS )
            pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Guar_Rate.compute(new ComputeParameters(false, pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Guar_Rate),                    //Natural: COMPUTE #WS-PERIODIC-GUAR-RATE = ( 1 + #AIAN027-GUARA-INTEREST-RATE ) ** ( 1 / #NUM-OF-PAYMENTS )
                NMath.pow ((DbsField.add(1,pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate())) ,(DbsField.divide(1,pnd_Num_Of_Payments))));
            pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum.compute(new ComputeParameters(true, pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I).multiply(pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Guar_Rate)); //Natural: COMPUTE ROUNDED #WS-UPDATED-ACCUM = NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) * #WS-PERIODIC-GUAR-RATE
            pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Guar_Payment.compute(new ComputeParameters(false, pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Guar_Payment), pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum.subtract(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I))); //Natural: COMPUTE #WS-PI-GUAR-PAYMENT = #WS-UPDATED-ACCUM - NAZ-ACT-TIAA-STD-RATE-AMT ( #I )
            //*  COMPUTE #WS-PERIODIC-TOTAL-RATE = #AIAN027-TOTAL-INTEREST-RATE ** (1 /
            //*        #NUM-OF-PAYMENTS )
            pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Total_Rate.compute(new ComputeParameters(false, pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Total_Rate),                  //Natural: COMPUTE #WS-PERIODIC-TOTAL-RATE = ( 1 + #AIAN027-TOTAL-INTEREST-RATE ) ** ( 1 / #NUM-OF-PAYMENTS )
                NMath.pow ((DbsField.add(1,pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Total_Interest_Rate())) ,(DbsField.divide(1,pnd_Num_Of_Payments))));
            pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum.compute(new ComputeParameters(true, pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I).multiply(pnd_P_And_I_Calc_Fields_Pnd_Ws_Periodic_Total_Rate)); //Natural: COMPUTE ROUNDED #WS-UPDATED-ACCUM = NAZ-ACT-TIAA-STD-RATE-AMT ( #I ) * #WS-PERIODIC-TOTAL-RATE
            pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Tot_Payment.compute(new ComputeParameters(false, pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Tot_Payment), pnd_P_And_I_Calc_Fields_Pnd_Ws_Updated_Accum.subtract(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_I))); //Natural: COMPUTE #WS-PI-TOT-PAYMENT = #WS-UPDATED-ACCUM - NAZ-ACT-TIAA-STD-RATE-AMT ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_I).setValue(pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Ia_Rate());       //Natural: MOVE #AIAN027-IA-RATE TO NAZ-ACT-TIAA-O-STD-RATE-CDE ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Gic_Code().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-TIAA-STD-GIC-CODE ( #I ) TO NAZ-ACT-TIAA-O-STD-GIC-CODE ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_I)),  //Natural: MULTIPLY #AIAN027-GUARA-INTEREST-RATE BY 100.00 GIVING NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #I )
                pdaAiaa027r.getPnd_Aian027_Linkage_Pnd_Aian027_Guara_Interest_Rate().multiply(new DbsDecimal("100.00")));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_I).setValue(pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Guar_Payment);                 //Natural: MOVE #WS-PI-GUAR-PAYMENT TO NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #I )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_I)),  //Natural: COMPUTE NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #I ) = #WS-PI-TOT-PAYMENT - #WS-PI-GUAR-PAYMENT
                pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Tot_Payment.subtract(pnd_P_And_I_Calc_Fields_Pnd_Ws_Pi_Guar_Payment));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
}
