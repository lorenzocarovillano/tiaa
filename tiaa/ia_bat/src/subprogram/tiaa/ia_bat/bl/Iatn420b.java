/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:18 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420b
************************************************************
**        * FILE NAME            : Iatn420b.java
**        * CLASS NAME           : Iatn420b
**        * INSTANCE NAME        : Iatn420b
************************************************************
************************************************************************
*  PROGRAM: IATN420B
*   AUTHOR: ARI GROSSMAN
*     DATE: FEB 11, 1996
*  PURPOSE: STORE CREF/TIAA HISTORICAL RATE INFORMATION
* HISTORY : 03/07/97  ADDED MULTI FUND FUNCTIONALITY AND
*                     CONTRACT BEFORE IMAGES
*           01/02/98  TRANSFER PROCESSING - 1998 (FORMERLY IATN170B)
*           01/08/09  O SOTTO  TIAA ACCESS CHANGES. SC 010809.
*           04/12/12  J TINIO  RATE BASE EXPANSION. SC 041212.
*           04/2017   O SOTTO  RE-STOWED FOR IAAL999 PIN EXPANSION.
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420b extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;
    private LdaIaal163b ldaIaal163b;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Iaa_From_Cntrct;
    private DbsField pnd_Iaa_From_Pyee;

    private DbsGroup pnd_Iaa_From_Pyee__R_Field_1;
    private DbsField pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N;
    private DbsField pnd_Iaa_New_Issue;
    private DbsField pnd_From_Fund;
    private DbsField pnd_Iaa_To_Cntrct;

    private DbsGroup pnd_Iaa_To_Cntrct__R_Field_2;
    private DbsField pnd_Iaa_To_Cntrct_Pnd_Iaa_To_Cntrct_1;
    private DbsField pnd_Iaa_To_Pyee;

    private DbsGroup pnd_Iaa_To_Pyee__R_Field_3;
    private DbsField pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Last_Check_Date;

    private DbsGroup pnd_Last_Check_Date__R_Field_4;
    private DbsField pnd_Last_Check_Date_Pnd_Last_Check_Date_N;
    private DbsField pnd_Time;
    private DbsField pnd_Return_Code;
    private DbsField pnd_On_File_Already;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_6;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_O_Cntrct_Fund_Key;

    private DbsGroup pnd_O_Cntrct_Fund_Key__R_Field_7;
    private DbsField pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Ppcn_Nbr;
    private DbsField pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Payee;
    private DbsField pnd_O_Cntrct_Fund_Key_Pnd_O_Invrse_Lst_Pd_Dte;
    private DbsField pnd_O_Cntrct_Fund_Key_Pnd_O_Fund_Code;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_Pd_Dte;
    private DbsField pnd_File_Mode;
    private DbsField pnd_W_Cnt_Mode;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_To_Cntrct_Fund;
    private DbsField pnd_I;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_W_Check_Date;

    private DbsGroup pnd_W_Check_Date__R_Field_8;
    private DbsField pnd_W_Check_Date_Pnd_Check_Date_A;

    private DbsGroup pnd_W_Check_Date__R_Field_9;
    private DbsField pnd_W_Check_Date_Pnd_Check_Date_A_Cc;
    private DbsField pnd_W_Check_Date_Pnd_Check_Date_A_Yy;
    private DbsField pnd_W_Check_Date_Pnd_Check_Date_A_Mm;
    private DbsField pnd_W_Check_Date_Pnd_Check_Date_A_Dd;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());
        ldaIaal163b = new LdaIaal163b();
        registerRecord(ldaIaal163b);
        registerRecord(ldaIaal163b.getVw_old_Cref_Rates_S());
        registerRecord(ldaIaal163b.getVw_old_Cref_Rates_R());
        registerRecord(ldaIaal163b.getVw_old_Tiaa_Rates());

        // parameters
        parameters = new DbsRecord();
        pnd_Iaa_From_Cntrct = parameters.newFieldInRecord("pnd_Iaa_From_Cntrct", "#IAA-FROM-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_From_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_From_Pyee = parameters.newFieldInRecord("pnd_Iaa_From_Pyee", "#IAA-FROM-PYEE", FieldType.STRING, 2);
        pnd_Iaa_From_Pyee.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_From_Pyee__R_Field_1 = parameters.newGroupInRecord("pnd_Iaa_From_Pyee__R_Field_1", "REDEFINE", pnd_Iaa_From_Pyee);
        pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N = pnd_Iaa_From_Pyee__R_Field_1.newFieldInGroup("pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N", "#IAA-FROM-PYEE-N", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_New_Issue = parameters.newFieldInRecord("pnd_Iaa_New_Issue", "#IAA-NEW-ISSUE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue.setParameterOption(ParameterOption.ByReference);
        pnd_From_Fund = parameters.newFieldInRecord("pnd_From_Fund", "#FROM-FUND", FieldType.STRING, 1);
        pnd_From_Fund.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_To_Cntrct = parameters.newFieldInRecord("pnd_Iaa_To_Cntrct", "#IAA-TO-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_To_Cntrct.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_To_Cntrct__R_Field_2 = parameters.newGroupInRecord("pnd_Iaa_To_Cntrct__R_Field_2", "REDEFINE", pnd_Iaa_To_Cntrct);
        pnd_Iaa_To_Cntrct_Pnd_Iaa_To_Cntrct_1 = pnd_Iaa_To_Cntrct__R_Field_2.newFieldInGroup("pnd_Iaa_To_Cntrct_Pnd_Iaa_To_Cntrct_1", "#IAA-TO-CNTRCT-1", 
            FieldType.STRING, 1);
        pnd_Iaa_To_Pyee = parameters.newFieldInRecord("pnd_Iaa_To_Pyee", "#IAA-TO-PYEE", FieldType.STRING, 2);
        pnd_Iaa_To_Pyee.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_To_Pyee__R_Field_3 = parameters.newGroupInRecord("pnd_Iaa_To_Pyee__R_Field_3", "REDEFINE", pnd_Iaa_To_Pyee);
        pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N = pnd_Iaa_To_Pyee__R_Field_3.newFieldInGroup("pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N", "#IAA-TO-PYEE-N", FieldType.NUMERIC, 
            2);
        pnd_Check_Date = parameters.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Check_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Last_Check_Date = parameters.newFieldInRecord("pnd_Last_Check_Date", "#LAST-CHECK-DATE", FieldType.STRING, 8);
        pnd_Last_Check_Date.setParameterOption(ParameterOption.ByReference);

        pnd_Last_Check_Date__R_Field_4 = parameters.newGroupInRecord("pnd_Last_Check_Date__R_Field_4", "REDEFINE", pnd_Last_Check_Date);
        pnd_Last_Check_Date_Pnd_Last_Check_Date_N = pnd_Last_Check_Date__R_Field_4.newFieldInGroup("pnd_Last_Check_Date_Pnd_Last_Check_Date_N", "#LAST-CHECK-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Time = parameters.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Time.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_On_File_Already = localVariables.newFieldInRecord("pnd_On_File_Already", "#ON-FILE-ALREADY", FieldType.STRING, 1);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_6", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_O_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_O_Cntrct_Fund_Key", "#O-CNTRCT-FUND-KEY", FieldType.STRING, 23);

        pnd_O_Cntrct_Fund_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_O_Cntrct_Fund_Key__R_Field_7", "REDEFINE", pnd_O_Cntrct_Fund_Key);
        pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Ppcn_Nbr = pnd_O_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Ppcn_Nbr", 
            "#O-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Payee = pnd_O_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Payee", "#O-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_O_Cntrct_Fund_Key_Pnd_O_Invrse_Lst_Pd_Dte = pnd_O_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_O_Cntrct_Fund_Key_Pnd_O_Invrse_Lst_Pd_Dte", 
            "#O-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_O_Cntrct_Fund_Key_Pnd_O_Fund_Code = pnd_O_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_O_Cntrct_Fund_Key_Pnd_O_Fund_Code", "#O-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_Pd_Dte = localVariables.newFieldInRecord("pnd_Pd_Dte", "#PD-DTE", FieldType.DATE);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_W_Cnt_Mode = localVariables.newFieldInRecord("pnd_W_Cnt_Mode", "#W-CNT-MODE", FieldType.NUMERIC, 3);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_To_Cntrct_Fund = localVariables.newFieldInRecord("pnd_To_Cntrct_Fund", "#TO-CNTRCT-FUND", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.STRING, 1);
        pnd_W_Check_Date = localVariables.newFieldInRecord("pnd_W_Check_Date", "#W-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_W_Check_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_W_Check_Date__R_Field_8", "REDEFINE", pnd_W_Check_Date);
        pnd_W_Check_Date_Pnd_Check_Date_A = pnd_W_Check_Date__R_Field_8.newFieldInGroup("pnd_W_Check_Date_Pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 
            8);

        pnd_W_Check_Date__R_Field_9 = pnd_W_Check_Date__R_Field_8.newGroupInGroup("pnd_W_Check_Date__R_Field_9", "REDEFINE", pnd_W_Check_Date_Pnd_Check_Date_A);
        pnd_W_Check_Date_Pnd_Check_Date_A_Cc = pnd_W_Check_Date__R_Field_9.newFieldInGroup("pnd_W_Check_Date_Pnd_Check_Date_A_Cc", "#CHECK-DATE-A-CC", 
            FieldType.STRING, 2);
        pnd_W_Check_Date_Pnd_Check_Date_A_Yy = pnd_W_Check_Date__R_Field_9.newFieldInGroup("pnd_W_Check_Date_Pnd_Check_Date_A_Yy", "#CHECK-DATE-A-YY", 
            FieldType.STRING, 2);
        pnd_W_Check_Date_Pnd_Check_Date_A_Mm = pnd_W_Check_Date__R_Field_9.newFieldInGroup("pnd_W_Check_Date_Pnd_Check_Date_A_Mm", "#CHECK-DATE-A-MM", 
            FieldType.STRING, 2);
        pnd_W_Check_Date_Pnd_Check_Date_A_Dd = pnd_W_Check_Date__R_Field_9.newFieldInGroup("pnd_W_Check_Date_Pnd_Check_Date_A_Dd", "#CHECK-DATE-A-DD", 
            FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal999.initializeValues();
        ldaIaal163b.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420b() throws Exception
    {
        super("Iatn420b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420B", onError);
        //* *********
        //*  WRITE 'IATN420B'
        pnd_W_Check_Date.setValue(pnd_Last_Check_Date_Pnd_Last_Check_Date_N);                                                                                             //Natural: MOVE #LAST-CHECK-DATE-N TO #W-CHECK-DATE
        //*  MOVE '04' TO #CHECK-DATE-A-MM
        //*  MOVE '01' TO #CHECK-DATE-A-DD
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ON ERROR;//Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                            //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        //*  NEW VIEW PREFIX FROM IAAL999                            041212 START
        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPX",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPX:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CPX")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                pnd_W_Cnt_Mode.setValue(ldaIaal999.getCpr_Cntrct_Mode_Ind());                                                                                             //Natural: ASSIGN #W-CNT-MODE = CPR.CNTRCT-MODE-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                           //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE = ' '
                                                                                                                                                                          //Natural: PERFORM #GET-INVERSE-DATE
        sub_Pnd_Get_Inverse_Date();
        if (condition(Global.isEscape())) {return;}
        pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                        //Natural: ASSIGN #O-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                         //Natural: ASSIGN #O-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_O_Cntrct_Fund_Key_Pnd_O_Fund_Code.setValue(" ");                                                                                                              //Natural: ASSIGN #O-FUND-CODE = ' '
        //*  010809
        if (condition(pnd_From_Fund.equals("T") || pnd_From_Fund.equals("G") || pnd_From_Fund.equals("R") || pnd_From_Fund.equals("D")))                                  //Natural: IF #FROM-FUND = 'T' OR = 'G' OR = 'R' OR = 'D'
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-ON-FILE
            sub_Pnd_Check_If_On_File();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_On_File_Already.equals("Y")))                                                                                                               //Natural: IF #ON-FILE-ALREADY = 'Y'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Return_Code.setValue("M5");                                                                                                                           //Natural: ASSIGN #RETURN-CODE = 'M5'
                                                                                                                                                                          //Natural: PERFORM #STORE-TIAA-FUND
                sub_Pnd_Store_Tiaa_Fund();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-ON-FILE
            sub_Pnd_Check_If_On_File();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_On_File_Already.equals("Y")))                                                                                                               //Natural: IF #ON-FILE-ALREADY = 'Y'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Return_Code.setValue("M6");                                                                                                                           //Natural: ASSIGN #RETURN-CODE = 'M6'
                                                                                                                                                                          //Natural: PERFORM #STORE-CREF-FUND
                sub_Pnd_Store_Cref_Fund();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Iaa_New_Issue.equals(" ") || pnd_Iaa_New_Issue.equals("E")))                                                                                    //Natural: IF #IAA-NEW-ISSUE = ' ' OR = 'E'
        {
            if (condition(pnd_Iaa_To_Cntrct.notEquals(" ")))                                                                                                              //Natural: IF #IAA-TO-CNTRCT NE ' '
            {
                //*    PERFORM #GET-INVERSE-DATE
                pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                  //Natural: ASSIGN #O-CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
                pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N);                                                                     //Natural: ASSIGN #O-CNTRCT-PAYEE = #IAA-TO-PYEE-N
                pnd_O_Cntrct_Fund_Key_Pnd_O_Fund_Code.setValue(" ");                                                                                                      //Natural: ASSIGN #O-FUND-CODE = ' '
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-ON-FILE
                sub_Pnd_Check_If_On_File();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_On_File_Already.equals("Y")))                                                                                                           //Natural: IF #ON-FILE-ALREADY = 'Y'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N);                                                                   //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-TO-PYEE-N
                    pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                    //Natural: ASSIGN #W-FUND-CODE = ' '
                                                                                                                                                                          //Natural: PERFORM #CHECK-COMPANY-CODE
                    sub_Pnd_Check_Company_Code();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pnd_Cref_Cnt.equals(" ")))                                                                                                              //Natural: IF #CREF-CNT = ' '
                    {
                        pnd_Return_Code.setValue("M5");                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'M5'
                                                                                                                                                                          //Natural: PERFORM #STORE-TIAA-FUND
                        sub_Pnd_Store_Tiaa_Fund();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Return_Code.setValue("M6");                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'M6'
                                                                                                                                                                          //Natural: PERFORM #STORE-CREF-FUND
                        sub_Pnd_Store_Cref_Fund();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                        //Natural: IF #RETURN-CODE NE ' '
                    {
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-COMPANY-CODE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-TIAA-FUND
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-CREF-FUND
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-RE-REC
        //* **********************************************************************
        //*  ASSIGN #W-FUND-CODE       = 'U09'
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-IF-ON-FILE
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-INVERSE-DATE
        //* **********************************************************************
    }
    private void sub_Pnd_Check_Company_Code() throws Exception                                                                                                            //Natural: #CHECK-COMPANY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Cnt.reset();                                                                                                                                             //Natural: RESET #CREF-CNT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                             //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N);                                                                                //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-TO-PYEE-N
        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPZ",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPZ:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CPZ")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                if (condition(ldaIaal999.getCpr_Cntrct_Company_Cd().getValue(1).equals(" ")))                                                                             //Natural: IF CPR.CNTRCT-COMPANY-CD ( 1 ) = ' '
                {
                    pnd_Cref_Cnt.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #CREF-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Tiaa_Fund() throws Exception                                                                                                               //Natural: #STORE-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1:
        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1")))
        {
            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*  010809
                if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U09") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W09")  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'U09' OR = 'W09' OR = 'U11' OR = 'W11'
                    || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U11") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W11")))
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE TO #W-FUND-CODE
                                                                                                                                                                          //Natural: PERFORM #STORE-RE-REC
                    sub_Pnd_Store_Re_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal163b.getVw_old_Tiaa_Rates().reset();                                                                                                           //Natural: RESET OLD-TIAA-RATES
                    ldaIaal163b.getVw_old_Tiaa_Rates().setValuesByName(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO OLD-TIAA-RATES
                    //*           MOVE EDITED '19960401' TO
                    ldaIaal163b.getOld_Tiaa_Rates_Fund_Lst_Pd_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Check_Date_Pnd_Check_Date_A);                     //Natural: MOVE EDITED #CHECK-DATE-A TO OLD-TIAA-RATES.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                    //*           ASSIGN  #DATE-TIME-P = OLD-TIAA-RATES.FUND-LST-PD-DTE
                    ldaIaal163b.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().compute(new ComputeParameters(false, ldaIaal163b.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte()),  //Natural: COMPUTE OLD-TIAA-RATES.FUND-INVRSE-LST-PD-DTE = 100000000 - #W-CHECK-DATE
                        DbsField.subtract(100000000,pnd_W_Check_Date));
                    ldaIaal163b.getOld_Tiaa_Rates_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN OLD-TIAA-RATES.TRANS-DTE = #TIME
                    ldaIaal163b.getOld_Tiaa_Rates_Trans_User_Area().setValue(" ");                                                                                        //Natural: ASSIGN OLD-TIAA-RATES.TRANS-USER-AREA = ' '
                    ldaIaal163b.getOld_Tiaa_Rates_Trans_User_Id().setValue(" ");                                                                                          //Natural: ASSIGN OLD-TIAA-RATES.TRANS-USER-ID = ' '
                    ldaIaal163b.getOld_Tiaa_Rates_Trans_Verify_Id().setValue(" ");                                                                                        //Natural: ASSIGN OLD-TIAA-RATES.TRANS-VERIFY-ID = ' '
                    ldaIaal163b.getOld_Tiaa_Rates_Trans_Verify_Dte().setValue(0);                                                                                         //Natural: ASSIGN OLD-TIAA-RATES.TRANS-VERIFY-DTE = 0
                    ldaIaal163b.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr());                                    //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-PPCN-NBR = IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR
                    ldaIaal163b.getOld_Tiaa_Rates_Cntrct_Payee_Cde().setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde());                                  //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE
                    ldaIaal163b.getOld_Tiaa_Rates_Cntrct_Mode_Ind().setValue(pnd_W_Cnt_Mode);                                                                             //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-MODE-IND = #W-CNT-MODE
                    ldaIaal163b.getOld_Tiaa_Rates_Cmpny_Fund_Cde().setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                      //Natural: ASSIGN OLD-TIAA-RATES.CMPNY-FUND-CDE = IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE
                    ldaIaal163b.getOld_Tiaa_Rates_Rcrd_Srce().setValue("TR");                                                                                             //Natural: ASSIGN OLD-TIAA-RATES.RCRD-SRCE = 'TR'
                    ldaIaal163b.getOld_Tiaa_Rates_Rcrd_Status().setValue("A");                                                                                            //Natural: ASSIGN OLD-TIAA-RATES.RCRD-STATUS = 'A'
                    ldaIaal163b.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt().setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                     //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-TOT-PER-AMT = IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                    ldaIaal163b.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt().setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                     //Natural: ASSIGN OLD-TIAA-RATES.CNTRCT-TOT-DIV-AMT = IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                    ldaIaal163b.getOld_Tiaa_Rates_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN OLD-TIAA-RATES.LST-TRANS-DTE = #TIME
                    //*        WRITE 'OLD TIAA RATES '
                    //*        WRITE OLD-CREF-RATES-S
                    ldaIaal163b.getVw_old_Tiaa_Rates().insertDBRow();                                                                                                     //Natural: STORE OLD-TIAA-RATES
                    pnd_Return_Code.reset();                                                                                                                              //Natural: RESET #RETURN-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Store_Cref_Fund() throws Exception                                                                                                               //Natural: #STORE-CREF-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R2",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R2:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R2")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                ldaIaal163b.getVw_old_Cref_Rates_S().reset();                                                                                                             //Natural: RESET OLD-CREF-RATES-S
                ldaIaal163b.getVw_old_Cref_Rates_S().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                              //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO OLD-CREF-RATES-S
                //*         MOVE EDITED '19960401' TO
                ldaIaal163b.getOld_Cref_Rates_S_Fund_Lst_Pd_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Check_Date_Pnd_Check_Date_A);                       //Natural: MOVE EDITED #CHECK-DATE-A TO OLD-CREF-RATES-S.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                //*         ASSIGN  #DATE-TIME-P = OLD-CREF-RATES-S.FUND-LST-PD-DTE
                ldaIaal163b.getOld_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte().compute(new ComputeParameters(false, ldaIaal163b.getOld_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte()),  //Natural: COMPUTE OLD-CREF-RATES-S.FUND-INVRSE-LST-PD-DTE = 100000000 - #W-CHECK-DATE
                    DbsField.subtract(100000000,pnd_W_Check_Date));
                ldaIaal163b.getOld_Cref_Rates_S_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-DTE = #TIME
                ldaIaal163b.getOld_Cref_Rates_S_Trans_User_Area().setValue(" ");                                                                                          //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-USER-AREA = ' '
                ldaIaal163b.getOld_Cref_Rates_S_Trans_User_Id().setValue(" ");                                                                                            //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-USER-ID = ' '
                ldaIaal163b.getOld_Cref_Rates_S_Trans_Verify_Id().setValue(" ");                                                                                          //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-VERIFY-ID = ' '
                ldaIaal163b.getOld_Cref_Rates_S_Trans_Verify_Dte().setValue(0);                                                                                           //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-VERIFY-DTE = 0
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Ppcn_Nbr().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr());                                      //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-PPCN-NBR = IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Payee_Cde().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde());                                    //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-PAYEE-CDE = IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Mode_Ind().setValue(pnd_W_Cnt_Mode);                                                                               //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-MODE-IND = #W-CNT-MODE
                ldaIaal163b.getOld_Cref_Rates_S_Cmpny_Fund_Cde().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde());                                        //Natural: ASSIGN OLD-CREF-RATES-S.CMPNY-FUND-CDE = IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Unit_Val().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val());                                             //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-UNIT-VAL = IAA-CREF-FUND-RCRD.CREF-UNIT-VAL
                ldaIaal163b.getOld_Cref_Rates_S_Rcrd_Srce().setValue("TR");                                                                                               //Natural: ASSIGN OLD-CREF-RATES-S.RCRD-SRCE = 'TR'
                ldaIaal163b.getOld_Cref_Rates_S_Rcrd_Status().setValue("A");                                                                                              //Natural: ASSIGN OLD-CREF-RATES-S.RCRD-STATUS = 'A'
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Tot_Per_Amt().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt());                                       //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-TOT-PER-AMT = IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Tot_Units().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1));                               //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-TOT-UNITS = IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Cref_Rate_Cde().getValue(1).setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(1));                       //Natural: ASSIGN OLD-CREF-RATES-S.CREF-RATE-CDE ( 1 ) = IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Cref_Rate_Dte().getValue(1).setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Dte().getValue(1));                       //Natural: ASSIGN OLD-CREF-RATES-S.CREF-RATE-DTE ( 1 ) = IAA-CREF-FUND-RCRD.CREF-RATE-DTE ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Cref_Per_Pay_Amt().getValue(1).setValue(0);                                                                               //Natural: ASSIGN OLD-CREF-RATES-S.CREF-PER-PAY-AMT ( 1 ) = 0
                ldaIaal163b.getOld_Cref_Rates_S_Cref_No_Units().getValue(1).setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1));                      //Natural: ASSIGN OLD-CREF-RATES-S.CREF-NO-UNITS ( 1 ) = IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Lst_Trans_Dte().setValue(pnd_Time);                                                                                       //Natural: ASSIGN OLD-CREF-RATES-S.LST-TRANS-DTE = #TIME
                ldaIaal163b.getVw_old_Cref_Rates_S().insertDBRow();                                                                                                       //Natural: STORE OLD-CREF-RATES-S
                //*        WRITE 'OLD CREF RATES'
                //*        WRITE OLD-CREF-RATES-S
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Store_Re_Rec() throws Exception                                                                                                                  //Natural: #STORE-RE-REC
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "RW",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        RW:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("RW")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal163b.getVw_old_Cref_Rates_S().reset();                                                                                                             //Natural: RESET OLD-CREF-RATES-S
                ldaIaal163b.getVw_old_Cref_Rates_S().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                              //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO OLD-CREF-RATES-S
                //*         MOVE EDITED '19960401' TO
                ldaIaal163b.getOld_Cref_Rates_S_Fund_Lst_Pd_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Check_Date_Pnd_Check_Date_A);                       //Natural: MOVE EDITED #CHECK-DATE-A TO OLD-CREF-RATES-S.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                //*         ASSIGN  #DATE-TIME-P = OLD-CREF-RATES-S.FUND-LST-PD-DTE
                ldaIaal163b.getOld_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte().compute(new ComputeParameters(false, ldaIaal163b.getOld_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte()),  //Natural: COMPUTE OLD-CREF-RATES-S.FUND-INVRSE-LST-PD-DTE = 100000000 - #W-CHECK-DATE
                    DbsField.subtract(100000000,pnd_W_Check_Date));
                ldaIaal163b.getOld_Cref_Rates_S_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-DTE = #TIME
                ldaIaal163b.getOld_Cref_Rates_S_Trans_User_Area().setValue(" ");                                                                                          //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-USER-AREA = ' '
                ldaIaal163b.getOld_Cref_Rates_S_Trans_User_Id().setValue(" ");                                                                                            //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-USER-ID = ' '
                ldaIaal163b.getOld_Cref_Rates_S_Trans_Verify_Id().setValue(" ");                                                                                          //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-VERIFY-ID = ' '
                ldaIaal163b.getOld_Cref_Rates_S_Trans_Verify_Dte().setValue(0);                                                                                           //Natural: ASSIGN OLD-CREF-RATES-S.TRANS-VERIFY-DTE = 0
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Ppcn_Nbr().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr());                                      //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-PPCN-NBR = IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Payee_Cde().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde());                                    //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-PAYEE-CDE = IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Mode_Ind().setValue(pnd_W_Cnt_Mode);                                                                               //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-MODE-IND = #W-CNT-MODE
                ldaIaal163b.getOld_Cref_Rates_S_Cmpny_Fund_Cde().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde());                                        //Natural: ASSIGN OLD-CREF-RATES-S.CMPNY-FUND-CDE = IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Unit_Val().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val());                                             //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-UNIT-VAL = IAA-CREF-FUND-RCRD.CREF-UNIT-VAL
                ldaIaal163b.getOld_Cref_Rates_S_Rcrd_Srce().setValue("TR");                                                                                               //Natural: ASSIGN OLD-CREF-RATES-S.RCRD-SRCE = 'TR'
                ldaIaal163b.getOld_Cref_Rates_S_Rcrd_Status().setValue("A");                                                                                              //Natural: ASSIGN OLD-CREF-RATES-S.RCRD-STATUS = 'A'
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Tot_Per_Amt().setValue(0);                                                                                         //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-TOT-PER-AMT = 0
                ldaIaal163b.getOld_Cref_Rates_S_Cntrct_Tot_Units().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1));                               //Natural: ASSIGN OLD-CREF-RATES-S.CNTRCT-TOT-UNITS = IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Cref_Rate_Cde().getValue(1).setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(1));                       //Natural: ASSIGN OLD-CREF-RATES-S.CREF-RATE-CDE ( 1 ) = IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Cref_Rate_Dte().getValue(1).setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Dte().getValue(1));                       //Natural: ASSIGN OLD-CREF-RATES-S.CREF-RATE-DTE ( 1 ) = IAA-CREF-FUND-RCRD.CREF-RATE-DTE ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Cref_Per_Pay_Amt().getValue(1).setValue(0);                                                                               //Natural: ASSIGN OLD-CREF-RATES-S.CREF-PER-PAY-AMT ( 1 ) = 0
                ldaIaal163b.getOld_Cref_Rates_S_Cref_No_Units().getValue(1).setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1));                      //Natural: ASSIGN OLD-CREF-RATES-S.CREF-NO-UNITS ( 1 ) = IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 )
                ldaIaal163b.getOld_Cref_Rates_S_Lst_Trans_Dte().setValue(pnd_Time);                                                                                       //Natural: ASSIGN OLD-CREF-RATES-S.LST-TRANS-DTE = #TIME
                ldaIaal163b.getVw_old_Cref_Rates_S().insertDBRow();                                                                                                       //Natural: STORE OLD-CREF-RATES-S
                //*        WRITE 'OLD RATES REAL ESTATE'
                //*        WRITE OLD-CREF-RATES-S
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RW;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RW. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  NEW VIEW PREFIX FROM IAAL999                            041212 END
    }
    private void sub_Pnd_Check_If_On_File() throws Exception                                                                                                              //Natural: #CHECK-IF-ON-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File_Already.reset();                                                                                                                                      //Natural: RESET #ON-FILE-ALREADY
        ldaIaal163b.getVw_old_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) OLD-TIAA-RATES BY CNTRCT-PY-DTE-KEY STARTING FROM #O-CNTRCT-FUND-KEY
        (
        "R3",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_O_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") },
        1
        );
        R3:
        while (condition(ldaIaal163b.getVw_old_Tiaa_Rates().readNextRow("R3")))
        {
            if (condition(ldaIaal163b.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().equals(pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Ppcn_Nbr) && ldaIaal163b.getOld_Tiaa_Rates_Cntrct_Payee_Cde().equals(pnd_O_Cntrct_Fund_Key_Pnd_O_Cntrct_Payee)  //Natural: IF OLD-TIAA-RATES.CNTRCT-PPCN-NBR = #O-CNTRCT-PPCN-NBR AND OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = #O-CNTRCT-PAYEE AND OLD-TIAA-RATES.FUND-INVRSE-LST-PD-DTE = #O-INVRSE-LST-PD-DTE
                && ldaIaal163b.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().equals(pnd_O_Cntrct_Fund_Key_Pnd_O_Invrse_Lst_Pd_Dte)))
            {
                pnd_On_File_Already.setValue("Y");                                                                                                                        //Natural: MOVE 'Y' TO #ON-FILE-ALREADY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Get_Inverse_Date() throws Exception                                                                                                              //Natural: #GET-INVERSE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*    MOVE EDITED #CHECK-DATE-A TO
        //*     MOVE EDITED '19960401' TO
        //*              #PD-DTE(EM=YYYYMMDD)
        //*     ASSIGN  #DATE-TIME-P = #PD-DTE
        pnd_O_Cntrct_Fund_Key_Pnd_O_Invrse_Lst_Pd_Dte.compute(new ComputeParameters(false, pnd_O_Cntrct_Fund_Key_Pnd_O_Invrse_Lst_Pd_Dte), DbsField.subtract(100000000,   //Natural: COMPUTE #O-INVRSE-LST-PD-DTE = 100000000 - #W-CHECK-DATE
            pnd_W_Check_Date));
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
