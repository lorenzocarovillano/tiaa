/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:55:54 PM
**        * FROM NATURAL SUBPROGRAM : Adsn9920
************************************************************
**        * FILE NAME            : Adsn9920.java
**        * CLASS NAME           : Adsn9920
**        * INSTANCE NAME        : Adsn9920
************************************************************
************************************************************************
* PROGRAM: ADSN9920
* GET TOTAL PAYMENTS FROM IA.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn9920 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ia_Ppcn;
    private DbsField pnd_Ia_Pay;
    private DbsField pnd_Tot_Per_Amt;
    private DbsField pnd_Stts;
    private DbsField pnd_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Cntrct_Mode;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Pend_Cde;
    private DbsField cpr_Cntrct_Trmnte_Rsn;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Mode_Ind;

    private DataAccessProgramView vw_fund;
    private DbsField fund_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup fund__R_Field_1;
    private DbsField fund_Cmpny;
    private DbsField fund_Fund_Cde;
    private DbsField fund_Tiaa_Per_Ivc_Amt;
    private DbsField fund_Tiaa_Rtb_Amt;
    private DbsField fund_Tiaa_Tot_Per_Amt;
    private DbsField fund_Tiaa_Tot_Div_Amt;
    private DbsField fund_Tiaa_Old_Per_Amt;
    private DbsField fund_Tiaa_Old_Div_Amt;

    private DbsGroup fund_Tiaa_Rate_Data_Grp;
    private DbsField fund_Tiaa_Units_Cnt;

    private DbsGroup aian026;

    private DbsGroup aian026_Pnd_Input;
    private DbsField aian026_Pnd_Call_Type;
    private DbsField aian026_Pnd_Ia_Fund_Code;
    private DbsField aian026_Pnd_Revaluation_Method;
    private DbsField aian026_Pnd_Uv_Req_Dte;

    private DbsGroup aian026__R_Field_2;
    private DbsField aian026_Pnd_Uv_Req_Dte_A;

    private DbsGroup aian026__R_Field_3;
    private DbsField aian026_Pnd_Req_Yyyy;
    private DbsField aian026_Pnd_Req_Mm;
    private DbsField aian026_Pnd_Req_Dd;
    private DbsField aian026_Pnd_Prtcptn_Dte;

    private DbsGroup aian026__R_Field_4;
    private DbsField aian026_Pnd_Prtcptn_Dte_A;
    private DbsField aian026_Pnd_Prtcptn_Dd;

    private DbsGroup aian026_Pnd_Output;
    private DbsField aian026_Pnd_Rtrn_Cd_Pgm;

    private DbsGroup aian026__R_Field_5;
    private DbsField aian026_Pnd_Rtrn_Pgm;
    private DbsField aian026_Pnd_Rtrn_Cd;
    private DbsField aian026_Pnd_Iuv;
    private DbsField aian026_Pnd_Iuv_Dt;

    private DbsGroup aian026__R_Field_6;
    private DbsField aian026_Pnd_Iuv_Dt_A;
    private DbsField aian026_Pnd_Aian026_Days_In_Request_Month;
    private DbsField aian026_Pnd_Aian026_Days_In_Particip_Month;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_7;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_8;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cpr_Key;
    private DbsField pnd_I;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Count;
    private DbsField pnd_Active;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Ia_Ppcn = parameters.newFieldInRecord("pnd_Ia_Ppcn", "#IA-PPCN", FieldType.STRING, 10);
        pnd_Ia_Ppcn.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Pay = parameters.newFieldInRecord("pnd_Ia_Pay", "#IA-PAY", FieldType.NUMERIC, 2);
        pnd_Ia_Pay.setParameterOption(ParameterOption.ByReference);
        pnd_Tot_Per_Amt = parameters.newFieldInRecord("pnd_Tot_Per_Amt", "#TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Per_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Stts = parameters.newFieldInRecord("pnd_Stts", "#STTS", FieldType.STRING, 1);
        pnd_Stts.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Final_Per_Pay_Dte = parameters.newFieldInRecord("pnd_Cntrct_Final_Per_Pay_Dte", "#CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6);
        pnd_Cntrct_Final_Per_Pay_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Mode = parameters.newFieldInRecord("pnd_Cntrct_Mode", "#CNTRCT-MODE", FieldType.NUMERIC, 3);
        pnd_Cntrct_Mode.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Cntrct_Trmnte_Rsn = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TRMNTE_RSN");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        registerRecord(vw_cpr);

        vw_fund = new DataAccessProgramView(new NameInfo("vw_fund", "FUND"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        fund_Tiaa_Cntrct_Ppcn_Nbr = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CREF_CNTRCT_PPCN_NBR");
        fund_Tiaa_Cntrct_Payee_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        fund_Tiaa_Cmpny_Fund_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIAA_CMPNY_FUND_CDE");

        fund__R_Field_1 = vw_fund.getRecord().newGroupInGroup("fund__R_Field_1", "REDEFINE", fund_Tiaa_Cmpny_Fund_Cde);
        fund_Cmpny = fund__R_Field_1.newFieldInGroup("fund_Cmpny", "CMPNY", FieldType.STRING, 1);
        fund_Fund_Cde = fund__R_Field_1.newFieldInGroup("fund_Fund_Cde", "FUND-CDE", FieldType.NUMERIC, 2);
        fund_Tiaa_Per_Ivc_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_PER_IVC_AMT");
        fund_Tiaa_Rtb_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_RTB_AMT");
        fund_Tiaa_Tot_Per_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_TOT_PER_AMT");
        fund_Tiaa_Tot_Div_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_TOT_DIV_AMT");
        fund_Tiaa_Old_Per_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "AJ");
        fund_Tiaa_Old_Div_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_OLD_UNIT_VAL");

        fund_Tiaa_Rate_Data_Grp = vw_fund.getRecord().newGroupInGroup("fund_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Units_Cnt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new 
            DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_fund);

        aian026 = localVariables.newGroupInRecord("aian026", "AIAN026");

        aian026_Pnd_Input = aian026.newGroupInGroup("aian026_Pnd_Input", "#INPUT");
        aian026_Pnd_Call_Type = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        aian026_Pnd_Ia_Fund_Code = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        aian026_Pnd_Revaluation_Method = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Revaluation_Method", "#REVALUATION-METHOD", FieldType.STRING, 
            1);
        aian026_Pnd_Uv_Req_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_2 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_2", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Uv_Req_Dte_A = aian026__R_Field_2.newFieldInGroup("aian026_Pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);

        aian026__R_Field_3 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_3", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Req_Yyyy = aian026__R_Field_3.newFieldInGroup("aian026_Pnd_Req_Yyyy", "#REQ-YYYY", FieldType.NUMERIC, 4);
        aian026_Pnd_Req_Mm = aian026__R_Field_3.newFieldInGroup("aian026_Pnd_Req_Mm", "#REQ-MM", FieldType.NUMERIC, 2);
        aian026_Pnd_Req_Dd = aian026__R_Field_3.newFieldInGroup("aian026_Pnd_Req_Dd", "#REQ-DD", FieldType.NUMERIC, 2);
        aian026_Pnd_Prtcptn_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Prtcptn_Dte", "#PRTCPTN-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_4 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_4", "REDEFINE", aian026_Pnd_Prtcptn_Dte);
        aian026_Pnd_Prtcptn_Dte_A = aian026__R_Field_4.newFieldInGroup("aian026_Pnd_Prtcptn_Dte_A", "#PRTCPTN-DTE-A", FieldType.STRING, 6);
        aian026_Pnd_Prtcptn_Dd = aian026__R_Field_4.newFieldInGroup("aian026_Pnd_Prtcptn_Dd", "#PRTCPTN-DD", FieldType.STRING, 2);

        aian026_Pnd_Output = aian026.newGroupInGroup("aian026_Pnd_Output", "#OUTPUT");
        aian026_Pnd_Rtrn_Cd_Pgm = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Rtrn_Cd_Pgm", "#RTRN-CD-PGM", FieldType.STRING, 11);

        aian026__R_Field_5 = aian026_Pnd_Output.newGroupInGroup("aian026__R_Field_5", "REDEFINE", aian026_Pnd_Rtrn_Cd_Pgm);
        aian026_Pnd_Rtrn_Pgm = aian026__R_Field_5.newFieldInGroup("aian026_Pnd_Rtrn_Pgm", "#RTRN-PGM", FieldType.STRING, 8);
        aian026_Pnd_Rtrn_Cd = aian026__R_Field_5.newFieldInGroup("aian026_Pnd_Rtrn_Cd", "#RTRN-CD", FieldType.NUMERIC, 3);
        aian026_Pnd_Iuv = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv", "#IUV", FieldType.NUMERIC, 8, 4);
        aian026_Pnd_Iuv_Dt = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv_Dt", "#IUV-DT", FieldType.NUMERIC, 8);

        aian026__R_Field_6 = aian026_Pnd_Output.newGroupInGroup("aian026__R_Field_6", "REDEFINE", aian026_Pnd_Iuv_Dt);
        aian026_Pnd_Iuv_Dt_A = aian026__R_Field_6.newFieldInGroup("aian026_Pnd_Iuv_Dt_A", "#IUV-DT-A", FieldType.STRING, 8);
        aian026_Pnd_Aian026_Days_In_Request_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Aian026_Days_In_Request_Month", "#AIAN026-DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        aian026_Pnd_Aian026_Days_In_Particip_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Aian026_Days_In_Particip_Month", "#AIAN026-DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_7", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr = pnd_Tiaa_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr", 
            "#TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde", 
            "#TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde", 
            "#TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_8", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cpr_Key = pnd_Tiaa_Cntrct_Fund_Key__R_Field_8.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cpr_Key", "#CPR-KEY", 
            FieldType.STRING, 12);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Active = localVariables.newFieldInRecord("pnd_Active", "#ACTIVE", FieldType.BOOLEAN, 1);
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cpr.reset();
        vw_fund.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Fund_2.setInitialValue("09");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn9920() throws Exception
    {
        super("Adsn9920");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtrn_Cde);                                                                 //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTRN-CDE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Rtrn_Cde.notEquals(getZero())))                                                                                                                 //Natural: IF #RTRN-CDE NE 0
        {
            getReports().write(0, "Error in Externalization");                                                                                                            //Natural: WRITE 'Error in Externalization'
            if (Global.isEscape()) return;
            DbsUtil.terminate(66);  if (true) return;                                                                                                                     //Natural: TERMINATE 66
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr.setValue(pnd_Ia_Ppcn);                                                                                          //Natural: ASSIGN #TIAA-CNTRCT-PPCN-NBR := #IA-PPCN
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(pnd_Ia_Pay);                                                                                          //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := #IA-PAY
        pnd_Tot_Per_Amt.reset();                                                                                                                                          //Natural: RESET #TOT-PER-AMT #TIAA-CMPNY-FUND-CDE #STTS
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde.reset();
        pnd_Stts.reset();
        vw_cpr.startDatabaseFind                                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cpr_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cpr.readNextRow("FIND01")))
        {
            vw_cpr.setIfNotFoundControlFlag(false);
            //*  TERMINATED
            short decideConditionsMet104 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-ACTVTY-CDE = 9
            if (condition(cpr_Cntrct_Actvty_Cde.equals(9)))
            {
                decideConditionsMet104++;
                pnd_Stts.setValue("T");                                                                                                                                   //Natural: ASSIGN #STTS := 'T'
                //*  NACT
                if (condition(cpr_Cntrct_Trmnte_Rsn.equals("NI") || cpr_Cntrct_Trmnte_Rsn.equals("TR")))                                                                  //Natural: IF CNTRCT-TRMNTE-RSN = 'NI' OR = 'TR'
                {
                    pnd_Stts.setValue("N");                                                                                                                               //Natural: ASSIGN #STTS := 'N'
                    //*  EXPIRED
                    //*  ACTIVE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PEND-CDE = 'R'
            else if (condition(cpr_Cntrct_Pend_Cde.equals("R")))
            {
                decideConditionsMet104++;
                pnd_Stts.setValue("E");                                                                                                                                   //Natural: ASSIGN #STTS := 'E'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Stts.setValue("A");                                                                                                                                   //Natural: ASSIGN #STTS := 'A'
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Cntrct_Final_Per_Pay_Dte.setValue(cpr_Cntrct_Final_Per_Pay_Dte);                                                                                          //Natural: ASSIGN #CNTRCT-FINAL-PER-PAY-DTE := CNTRCT-FINAL-PER-PAY-DTE
            pnd_Cntrct_Mode.setValue(cpr_Cntrct_Mode_Ind);                                                                                                                //Natural: ASSIGN #CNTRCT-MODE := CNTRCT-MODE-IND
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_fund.startDatabaseRead                                                                                                                                         //Natural: READ FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #IA-PPCN
        (
        "READ01",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Ia_Ppcn, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_fund.readNextRow("READ01")))
        {
            if (condition(fund_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Ia_Ppcn)))                                                                                              //Natural: IF FUND.TIAA-CNTRCT-PPCN-NBR NE #IA-PPCN
            {
                //*      OR FUND.TIAA-CNTRCT-PAYEE-CDE NE #IA-PAY
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(fund_Tiaa_Cntrct_Payee_Cde.equals(pnd_Ia_Pay))))                                                                                              //Natural: ACCEPT IF FUND.TIAA-CNTRCT-PAYEE-CDE EQ #IA-PAY
            {
                continue;
            }
            //*  MONTHLY REVALUED
            if (condition(fund_Cmpny.equals("4") || fund_Cmpny.equals("W")))                                                                                              //Natural: IF CMPNY = '4' OR = 'W'
            {
                                                                                                                                                                          //Natural: PERFORM GET-UNIT-VALUE
                sub_Get_Unit_Value();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(fund_Cmpny.notEquals("T")))                                                                                                                     //Natural: IF CMPNY NE 'T'
            {
                fund_Tiaa_Tot_Div_Amt.reset();                                                                                                                            //Natural: RESET TIAA-TOT-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tot_Per_Amt.compute(new ComputeParameters(false, pnd_Tot_Per_Amt), pnd_Tot_Per_Amt.add(fund_Tiaa_Tot_Per_Amt).add(fund_Tiaa_Tot_Div_Amt));                //Natural: ASSIGN #TOT-PER-AMT := #TOT-PER-AMT + TIAA-TOT-PER-AMT + TIAA-TOT-DIV-AMT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-UNIT-VALUE
        //* ***********************************************************************
    }
    private void sub_Get_Unit_Value() throws Exception                                                                                                                    //Natural: GET-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pls_Fund_Num_Cde.getValue(pnd_I).equals(fund_Fund_Cde)))                                                                                        //Natural: IF +FUND-NUM-CDE ( #I ) = FUND-CDE
            {
                aian026_Pnd_Ia_Fund_Code.setValue(pls_Fund_Alpha_Cde.getValue(pnd_I));                                                                                    //Natural: ASSIGN #IA-FUND-CODE := +FUND-ALPHA-CDE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(aian026_Pnd_Ia_Fund_Code.equals(" ")))                                                                                                              //Natural: IF #IA-FUND-CODE = ' '
        {
            getReports().write(0, "Error: Invalid fund code for:",fund_Tiaa_Cntrct_Ppcn_Nbr,fund_Tiaa_Cntrct_Payee_Cde,fund_Tiaa_Cmpny_Fund_Cde);                         //Natural: WRITE 'Error: Invalid fund code for:' FUND.TIAA-CNTRCT-PPCN-NBR FUND.TIAA-CNTRCT-PAYEE-CDE FUND.TIAA-CMPNY-FUND-CDE
            if (Global.isEscape()) return;
            DbsUtil.terminate(77);  if (true) return;                                                                                                                     //Natural: TERMINATE 77
        }                                                                                                                                                                 //Natural: END-IF
        aian026_Pnd_Call_Type.setValue("L");                                                                                                                              //Natural: ASSIGN #CALL-TYPE := 'L'
        aian026_Pnd_Revaluation_Method.setValue("M");                                                                                                                     //Natural: ASSIGN #REVALUATION-METHOD := 'M'
        aian026_Pnd_Prtcptn_Dte.setValue(0);                                                                                                                              //Natural: ASSIGN #PRTCPTN-DTE := 0
        aian026_Pnd_Uv_Req_Dte.setValue(99999999);                                                                                                                        //Natural: ASSIGN #UV-REQ-DTE := 99999999
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), aian026);                                                                                               //Natural: CALLNAT 'AIAN026' AIAN026
        if (condition(Global.isEscape())) return;
        if (condition(aian026_Pnd_Rtrn_Cd.equals(getZero())))                                                                                                             //Natural: IF #RTRN-CD = 0
        {
            fund_Tiaa_Tot_Per_Amt.compute(new ComputeParameters(false, fund_Tiaa_Tot_Per_Amt), aian026_Pnd_Iuv.multiply(fund_Tiaa_Units_Cnt.getValue(1)));                //Natural: COMPUTE FUND.TIAA-TOT-PER-AMT = #IUV * FUND.TIAA-UNITS-CNT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "Error in cntrct:",fund_Tiaa_Cntrct_Ppcn_Nbr,"Payee",fund_Tiaa_Cntrct_Payee_Cde,NEWLINE,"No unit value for fund:",aian026_Pnd_Ia_Fund_Code); //Natural: WRITE 'Error in cntrct:' FUND.TIAA-CNTRCT-PPCN-NBR 'Payee' FUND.TIAA-CNTRCT-PAYEE-CDE / 'No unit value for fund:' #IA-FUND-CODE
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
