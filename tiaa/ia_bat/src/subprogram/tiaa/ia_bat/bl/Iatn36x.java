/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:44:21 AM
**        * FROM NATURAL SUBPROGRAM : Iatn36x
************************************************************
**        * FILE NAME            : Iatn36x.java
**        * CLASS NAME           : Iatn36x
**        * INSTANCE NAME        : Iatn36x
************************************************************
************************************************************************
* PROGRAM  : IATN36X
* SYSTEM   : IAD
* TITLE    : CALL MODULE TO PASS BACK WORK UNIT IDS
* CREATED  : MAY 14, 1999
* FUNCTION : PASS BACK LIST OF UNIT-IDS FOR XFR REPORTING PROGRAMS
* TITLE    : TO REPORT TRANSFER/SWITCHES BY WORK-UNIT-ID
*
*
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn36x extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIata36x pdaIata36x;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_Tab_Wrk_Unit_Cde_Ids;

    private DbsGroup pnd_Tab_Wrk_Unit_Cde_Ids__R_Field_1;

    private DbsGroup pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Unit_Cde_Ids;
    private DbsField pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id;
    private DbsField pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id1;
    private DbsField pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id2;
    private DbsField pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id3;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaIata36x = new PdaIata36x(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_Tab_Wrk_Unit_Cde_Ids = localVariables.newFieldArrayInRecord("pnd_Tab_Wrk_Unit_Cde_Ids", "#TAB-WRK-UNIT-CDE-IDS", FieldType.STRING, 14, new 
            DbsArrayController(1, 10));

        pnd_Tab_Wrk_Unit_Cde_Ids__R_Field_1 = localVariables.newGroupInRecord("pnd_Tab_Wrk_Unit_Cde_Ids__R_Field_1", "REDEFINE", pnd_Tab_Wrk_Unit_Cde_Ids);

        pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Unit_Cde_Ids = pnd_Tab_Wrk_Unit_Cde_Ids__R_Field_1.newGroupArrayInGroup("pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Unit_Cde_Ids", 
            "#TAB-UNIT-CDE-IDS", new DbsArrayController(1, 10));
        pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id = pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Unit_Cde_Ids.newFieldInGroup("pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id", 
            "#TAB-WRK-UNIT-ID", FieldType.STRING, 8);
        pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id1 = pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Unit_Cde_Ids.newFieldInGroup("pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id1", 
            "#TAB-PRNTR-ID1", FieldType.NUMERIC, 2);
        pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id2 = pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Unit_Cde_Ids.newFieldInGroup("pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id2", 
            "#TAB-PRNTR-ID2", FieldType.NUMERIC, 2);
        pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id3 = pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Unit_Cde_Ids.newFieldInGroup("pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id3", 
            "#TAB-PRNTR-ID3", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(1).setInitialValue("BPSRB1  010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(2).setInitialValue("BPSRB2  010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(3).setInitialValue("BPSRBW  010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(4).setInitialValue("BPSRBW1 010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(5).setInitialValue("BPSRBW2 010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(6).setInitialValue("ALL     010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(7).setInitialValue("        010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(8).setInitialValue("        010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(9).setInitialValue("        010203");
        pnd_Tab_Wrk_Unit_Cde_Ids.getValue(10).setInitialValue("        010203");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn36x() throws Exception
    {
        super("Iatn36x");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *
        //*  LENGTH OF UNIT-CDE TO SELECT FOR REPORTING
        getReports().write(0, " LIST OF REPORTING UNIT-IDS PASSED BACK BY ",Global.getPROGRAM());                                                                         //Natural: WRITE ' LIST OF REPORTING UNIT-IDS PASSED BACK BY ' *PROGRAM
        if (Global.isEscape()) return;
        pdaIata36x.getIata36x_Iata36x_Lngth().setValue(6);                                                                                                                //Natural: ASSIGN IATA36X-LNGTH := 6
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.greater(1)))                                                                                                                              //Natural: IF #I GT 1
            {
                if (condition(DbsUtil.maskMatches(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id.getValue(pnd_I),"A")))                                                     //Natural: IF #TAB-WRK-UNIT-ID ( #I ) = MASK ( A )
                {
                    pdaIata36x.getIata36x_Iata36x_All_Valid_Unit_Cdes().getValue(pnd_I).setValue(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id.getValue(pnd_I));           //Natural: ASSIGN IATA36X-ALL-VALID-UNIT-CDES ( #I ) := #TAB-WRK-UNIT-ID ( #I )
                    if (condition(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id.getValue(pnd_I).getSubstring(1,pdaIata36x.getIata36x_Iata36x_Lngth().getInt()).equals(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id.getValue(pnd_I2).getSubstring(1, //Natural: IF SUBSTR ( #TAB-WRK-UNIT-ID ( #I ) ,1,IATA36X-LNGTH ) = SUBSTR ( #TAB-WRK-UNIT-ID ( #I2 ) ,1,IATA36X-LNGTH )
                        pdaIata36x.getIata36x_Iata36x_Lngth().getInt()))))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id.getValue(pnd_I),"A")))                                                         //Natural: IF #TAB-WRK-UNIT-ID ( #I ) = MASK ( A )
            {
                pdaIata36x.getIata36x_Iata36x_Nbr_Of_Unit_Ids().nadd(1);                                                                                                  //Natural: ADD 1 TO IATA36X-NBR-OF-UNIT-IDS
                pnd_I2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I2
                pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I2).setValue(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Wrk_Unit_Id.getValue(pnd_I));                         //Natural: ASSIGN IATA36X-UNIT-CDE ( #I2 ) := #TAB-WRK-UNIT-ID ( #I )
                pdaIata36x.getIata36x_Iata36x_Prntr_Id1().getValue(pnd_I2).setValue(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id1.getValue(pnd_I));                          //Natural: ASSIGN IATA36X-PRNTR-ID1 ( #I2 ) := #TAB-PRNTR-ID1 ( #I )
                pdaIata36x.getIata36x_Iata36x_Prntr_Id2().getValue(pnd_I2).setValue(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id2.getValue(pnd_I));                          //Natural: ASSIGN IATA36X-PRNTR-ID2 ( #I2 ) := #TAB-PRNTR-ID2 ( #I )
                pdaIata36x.getIata36x_Iata36x_Prntr_Id3().getValue(pnd_I2).setValue(pnd_Tab_Wrk_Unit_Cde_Ids_Pnd_Tab_Prntr_Id3.getValue(pnd_I));                          //Natural: ASSIGN IATA36X-PRNTR-ID3 ( #I2 ) := #TAB-PRNTR-ID3 ( #I )
                getReports().write(0, "=",pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I2));                                                                     //Natural: WRITE '=' IATA36X-UNIT-CDE ( #I2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
