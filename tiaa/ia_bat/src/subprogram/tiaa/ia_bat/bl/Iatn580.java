/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:46:47 AM
**        * FROM NATURAL SUBPROGRAM : Iatn580
************************************************************
**        * FILE NAME            : Iatn580.java
**        * CLASS NAME           : Iatn580
**        * INSTANCE NAME        : Iatn580
************************************************************
*********************************************************************
*             ***  TRANSFER REMINDER LETTER  ***
*
*  PROG NAME :- IATN580
*  FUNCTION  :- CREATES TRANSFER REMINDER LETTER FOR REPETITIVE RQSTS.
*  AUTHOR    :- JUN F. TINIO
*  DATE      :- FEB 24, 2000
*  CHANGES   :-
*
*  05/01/02 TD ADDED #ACCT-TCKR IN IATN216 PARM
*  02/19/09 OS TIAA ACCESS CHANGES. SC 021909.
*  04/22/09    PUT A DASH (-) BETEWEEN CONTRACT AND 'REA' AS
*              REQUESTED BY LIZETTE.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO  ADDITIONAL PIN EXPANSION MARKED 082017.
*********************************************************************
* NOTE:- THIS SUBPROG PERFORMS AN ET OR BT.
*
*
* THIS MODULE INTERFACES WITH POST SYSTEM TO PRODUCE REMINDER LETTER
* FOR SUBSEQUENT REPETITIVE REQUESTS.
*
* THIS PROGRAM CALLS FOLLOWING MODULES;
*
*  1. IATN216  - PRODUCT INFORMATION
*  2. NAZN553  - CONVERSION OF NAME TO LOWER CASE
*  3. PSTN9610 - POST OPEN
*  4. PSTN9502 - POST WRITE
*  5. PSTN9680 - POST CLOSE
*
***********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn580 extends BLNatBase
{
    // Data Areas
    private LdaPstl6365 ldaPstl6365;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Rqst_Type;
    private DbsField pnd_Request_Id;
    private DbsField pnd_Printer_Id;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Msg;
    private DbsField pnd_Mit_Status;
    private DbsField pnd_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst_View;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Cntct_Mde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Srce;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rep_Nme;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Sttmnt_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Opn_Clsd_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcprcl_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Ssn;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Mit_Log_Dte_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Rjctn_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_In_Progress_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Retry_Cnt;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Hold_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_To_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_To_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Appl_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_New_Issue;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Rsn_For_Ovrrde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Ovrrde_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Gnrl_Pend_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Xfr_Type;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Unit_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_New_Iss_Prt_Pull;

    private DbsGroup pnd_Iatn0201_Out;
    private DbsField pnd_Iatn0201_Out_Pnd_Post_Fund_N2;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Tiaa_Std;
    private DbsField pnd_Const_Pnd_Tiaa_Grad;
    private DbsField pnd_Const_Pnd_Tiaa_Re;
    private DbsField pnd_Const_Pnd_Tiaa_Acc;
    private DbsField pnd_Const_Pnd_Annual;
    private DbsField pnd_Const_Pnd_Yearly;
    private DbsField pnd_Const_Pnd_Percent;
    private DbsField pnd_Const_Pnd_Dollars;
    private DbsField pnd_Const_Pnd_Units;
    private DbsField pnd_Trans_Type;

    private DbsGroup pnd_Trans_Type__R_Field_1;
    private DbsField pnd_Trans_Type_Pnd_From_Grad;
    private DbsField pnd_Trans_Type_Pnd_From_Std;
    private DbsField pnd_Trans_Type_Pnd_From_Re;
    private DbsField pnd_Trans_Type_Pnd_From_Acc;
    private DbsField pnd_Trans_Type_Pnd_From_Cref;
    private DbsField pnd_Trans_Type_Pnd_To_Grad;
    private DbsField pnd_Trans_Type_Pnd_To_Std;
    private DbsField pnd_Trans_Type_Pnd_To_Re;
    private DbsField pnd_Trans_Type_Pnd_To_Acc;
    private DbsField pnd_Trans_Type_Pnd_To_Cref;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Date_Yyyymmdd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_3;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyy;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Dd;
    private DbsField pnd_Mod_Ts;

    private DbsGroup pnd_Mod_Ts__R_Field_4;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Mm;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Dd;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Yyyy;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Time;

    private DbsGroup pnd_Mod_Ts__R_Field_5;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Hrs;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Mins;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Secs;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Am;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField temp_Ia_Frm_Payee_A;

    private DbsGroup temp_Ia_Frm_Payee_A__R_Field_6;
    private DbsField temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N;
    private DbsField pnd_Appl_Key_Counter;
    private DbsField pnd_Prod_Code_N;
    private DbsField pnd_Name;
    private DbsField pnd_Post_Error;

    private DbsGroup iatn216_Data_Area;
    private DbsField iatn216_Data_Area_Pnd_Prod_Ct;
    private DbsField iatn216_Data_Area_Pnd_Prod_Cd;
    private DbsField iatn216_Data_Area_Pnd_Acct_Nme_5;
    private DbsField iatn216_Data_Area_Pnd_Acct_Tckr;
    private DbsField iatn216_Data_Area_Pnd_Acct_Effctve_Dte;
    private DbsField iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind;
    private DbsField iatn216_Data_Area_Pnd_Prod_Cd_N;
    private DbsField iatn216_Data_Area_Pnd_Acct_Rpt_Seq;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl;
    private DbsField cwf_Org_Empl_Tbl_Empl_Unit_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Nme;

    private DbsGroup cwf_Org_Empl_Tbl__R_Field_7;
    private DbsField cwf_Org_Empl_Tbl_Empl_First_Nme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Last_Nme;
    private DbsField cwf_Org_Empl_Tbl_Actve_Ind;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Signatory;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_8;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPstl6365 = new LdaPstl6365();
        registerRecord(ldaPstl6365);
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Rqst_Type = parameters.newFieldInRecord("pnd_Rqst_Type", "#RQST-TYPE", FieldType.STRING, 1);
        pnd_Rqst_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Request_Id = parameters.newFieldInRecord("pnd_Request_Id", "#REQUEST-ID", FieldType.STRING, 34);
        pnd_Request_Id.setParameterOption(ParameterOption.ByReference);
        pnd_Printer_Id = parameters.newFieldInRecord("pnd_Printer_Id", "#PRINTER-ID", FieldType.STRING, 4);
        pnd_Printer_Id.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Msg = parameters.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 79);
        pnd_Msg.setParameterOption(ParameterOption.ByReference);
        pnd_Mit_Status = parameters.newFieldInRecord("pnd_Mit_Status", "#MIT-STATUS", FieldType.STRING, 4);
        pnd_Mit_Status.setParameterOption(ParameterOption.ByReference);
        pnd_Rqst_Log_Dte_Tme = parameters.newFieldInRecord("pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Rqst_Log_Dte_Tme.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_iaa_Trnsfr_Sw_Rqst_View = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst_View", "IAA-TRNSFR-SW-RQST-VIEW"), "IAA_TRNSFR_SW_RQST", 
            "IA_TRANSFER_KDO", DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde", "RCRD-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            34, RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Dte", "RQST-ENTRY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Tme = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Tme", "RQST-ENTRY-TME", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RQST_ENTRY_TME");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte", 
            "RQST-LST-ACTVTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Tme = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Tme", "RQST-RCVD-TME", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RQST_RCVD_TME");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_User_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_User_Id", 
            "RQST-RCVD-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id", 
            "RQST-ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Cntct_Mde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_CNTCT_MDE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Srce = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_SRCE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rep_Nme = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rep_Nme", "RQST-REP-NME", 
            FieldType.STRING, 40, RepeatingFieldStrategy.None, "RQST_REP_NME");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Sttmnt_Ind = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_STTMNT_IND");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Opn_Clsd_Ind = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Opn_Clsd_Ind", 
            "RQST-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcprcl_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RQST_RCPRCL_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Ssn = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RQST_SSN");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id", 
            "XFR-WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Mit_Log_Dte_Tme = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Mit_Log_Dte_Tme", 
            "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Stts_Cde", "XFR-STTS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Rjctn_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "XFR_RJCTN_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_In_Progress_Ind = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_In_Progress_Ind", 
            "XFR-IN-PROGRESS-IND", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Retry_Cnt = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Retry_Cnt", "XFR-RETRY-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "XFR_RETRY_CNT");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee", "IA-FRM-PAYEE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id", "IA-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Hold_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Hold_Cde", "IA-HOLD-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "IA_HOLD_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_Frm_Acct_Dta", 
            "C*XFR-FRM-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta", 
            "XFR-FRM-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde", 
            "XFR-FRM-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ", 
            "XFR-FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ", "XFR-FRM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty", "XFR-FRM-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Est_Amt = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Est_Amt", 
            "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Asset_Amt = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Asset_Amt", 
            "XFR-FRM-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_To_Acct_Dta", 
            "C*XFR-TO-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde", 
            "XFR-TO-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ", 
            "XFR-TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ", "XFR-TO-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty", "XFR-TO-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Est_Amt = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Est_Amt", 
            "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Asset_Amt = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Asset_Amt", 
            "XFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct", "IA-TO-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_View_Ia_To_Payee = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_To_Payee", "IA-TO-PAYEE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Appl_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Appl_Rcvd_Dte", 
            "IA-APPL-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "IA_APPL_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_New_Issue = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_New_Issue", "IA-NEW-ISSUE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_NEW_ISSUE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Rsn_For_Ovrrde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Rsn_For_Ovrrde", 
            "IA-RSN-FOR-OVRRDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_RSN_FOR_OVRRDE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Ovrrde_User_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Ovrrde_User_Id", 
            "IA-OVRRDE-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "IA_OVRRDE_USER_ID");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Gnrl_Pend_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Gnrl_Pend_Cde", 
            "IA-GNRL-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_GNRL_PEND_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Xfr_Type = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Xfr_Type", "RQST-XFR-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Unit_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Unit_Cde", "RQST-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_New_Iss_Prt_Pull = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_New_Iss_Prt_Pull", 
            "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst_View);

        pnd_Iatn0201_Out = localVariables.newGroupInRecord("pnd_Iatn0201_Out", "#IATN0201-OUT");
        pnd_Iatn0201_Out_Pnd_Post_Fund_N2 = pnd_Iatn0201_Out.newFieldInGroup("pnd_Iatn0201_Out_Pnd_Post_Fund_N2", "#POST-FUND-N2", FieldType.NUMERIC, 
            2);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Tiaa_Std = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Std", "#TIAA-STD", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Grad = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Grad", "#TIAA-GRAD", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Re = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Re", "#TIAA-RE", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Acc = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Acc", "#TIAA-ACC", FieldType.STRING, 1);
        pnd_Const_Pnd_Annual = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Annual", "#ANNUAL", FieldType.STRING, 1);
        pnd_Const_Pnd_Yearly = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Yearly", "#YEARLY", FieldType.STRING, 1);
        pnd_Const_Pnd_Percent = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Percent", "#PERCENT", FieldType.STRING, 1);
        pnd_Const_Pnd_Dollars = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Dollars", "#DOLLARS", FieldType.STRING, 1);
        pnd_Const_Pnd_Units = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Units", "#UNITS", FieldType.STRING, 1);
        pnd_Trans_Type = localVariables.newFieldInRecord("pnd_Trans_Type", "#TRANS-TYPE", FieldType.STRING, 10);

        pnd_Trans_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Trans_Type__R_Field_1", "REDEFINE", pnd_Trans_Type);
        pnd_Trans_Type_Pnd_From_Grad = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_From_Grad", "#FROM-GRAD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Std = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_From_Std", "#FROM-STD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Re = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_From_Re", "#FROM-RE", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Acc = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_From_Acc", "#FROM-ACC", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_From_Cref = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_From_Cref", "#FROM-CREF", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Grad = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_To_Grad", "#TO-GRAD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Std = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_To_Std", "#TO-STD", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Re = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_To_Re", "#TO-RE", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Acc = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_To_Acc", "#TO-ACC", FieldType.STRING, 1);
        pnd_Trans_Type_Pnd_To_Cref = pnd_Trans_Type__R_Field_1.newFieldInGroup("pnd_Trans_Type_Pnd_To_Cref", "#TO-CREF", FieldType.STRING, 1);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Date_Yyyymmdd__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_3", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyy = pnd_Date_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Yyyymmdd_Pnd_Date_Mm = pnd_Date_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Yyyymmdd_Pnd_Date_Dd = pnd_Date_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Mod_Ts = localVariables.newFieldInRecord("pnd_Mod_Ts", "#MOD-TS", FieldType.NUMERIC, 15);

        pnd_Mod_Ts__R_Field_4 = localVariables.newGroupInRecord("pnd_Mod_Ts__R_Field_4", "REDEFINE", pnd_Mod_Ts);
        pnd_Mod_Ts_Pnd_Mod_Date_Mm = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Mm", "#MOD-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Date_Dd = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Dd", "#MOD-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Date_Yyyy = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Yyyy", "#MOD-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Mod_Ts_Pnd_Mod_Time = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Time", "#MOD-TIME", FieldType.NUMERIC, 7);

        pnd_Mod_Ts__R_Field_5 = pnd_Mod_Ts__R_Field_4.newGroupInGroup("pnd_Mod_Ts__R_Field_5", "REDEFINE", pnd_Mod_Ts_Pnd_Mod_Time);
        pnd_Mod_Ts_Pnd_Mod_Hrs = pnd_Mod_Ts__R_Field_5.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Hrs", "#MOD-HRS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Mins = pnd_Mod_Ts__R_Field_5.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Mins", "#MOD-MINS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Secs = pnd_Mod_Ts__R_Field_5.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Secs", "#MOD-SECS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Am = pnd_Mod_Ts__R_Field_5.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Am", "#MOD-AM", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        temp_Ia_Frm_Payee_A = localVariables.newFieldInRecord("temp_Ia_Frm_Payee_A", "TEMP-IA-FRM-PAYEE-A", FieldType.STRING, 2);

        temp_Ia_Frm_Payee_A__R_Field_6 = localVariables.newGroupInRecord("temp_Ia_Frm_Payee_A__R_Field_6", "REDEFINE", temp_Ia_Frm_Payee_A);
        temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N = temp_Ia_Frm_Payee_A__R_Field_6.newFieldInGroup("temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N", "TEMP-IA-FRM-PAYEE-N", 
            FieldType.NUMERIC, 2);
        pnd_Appl_Key_Counter = localVariables.newFieldInRecord("pnd_Appl_Key_Counter", "#APPL-KEY-COUNTER", FieldType.NUMERIC, 5);
        pnd_Prod_Code_N = localVariables.newFieldArrayInRecord("pnd_Prod_Code_N", "#PROD-CODE-N", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 50);
        pnd_Post_Error = localVariables.newFieldInRecord("pnd_Post_Error", "#POST-ERROR", FieldType.STRING, 1);

        iatn216_Data_Area = localVariables.newGroupInRecord("iatn216_Data_Area", "IATN216-DATA-AREA");
        iatn216_Data_Area_Pnd_Prod_Ct = iatn216_Data_Area.newFieldInGroup("iatn216_Data_Area_Pnd_Prod_Ct", "#PROD-CT", FieldType.PACKED_DECIMAL, 3);
        iatn216_Data_Area_Pnd_Prod_Cd = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Prod_Cd", "#PROD-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        iatn216_Data_Area_Pnd_Acct_Nme_5 = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 
            6, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Tckr = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Tckr", "#ACCT-TCKR", FieldType.STRING, 10, 
            new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Effctve_Dte = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", 
            FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Prod_Cd_N = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Prod_Cd_N", "#PROD-CD-N", FieldType.NUMERIC, 2, 
            new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Rpt_Seq = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 20));

        vw_cwf_Org_Empl_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl", "CWF-ORG-EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Empl_Tbl_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Empl_Nme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        cwf_Org_Empl_Tbl_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        cwf_Org_Empl_Tbl__R_Field_7 = vw_cwf_Org_Empl_Tbl.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl__R_Field_7", "REDEFINE", cwf_Org_Empl_Tbl_Empl_Nme);
        cwf_Org_Empl_Tbl_Empl_First_Nme = cwf_Org_Empl_Tbl__R_Field_7.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_First_Nme", "EMPL-FIRST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Empl_Last_Nme = cwf_Org_Empl_Tbl__R_Field_7.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Last_Nme", "EMPL-LAST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Actve_Ind = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Empl_Tbl_Empl_Signatory = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Signatory", "EMPL-SIGNATORY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "EMPL_SIGNATORY");
        cwf_Org_Empl_Tbl_Empl_Signatory.setDdmHeader("SIGNATORY");
        registerRecord(vw_cwf_Org_Empl_Tbl);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 29);

        pnd_Naz_Table_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_8", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_8.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_8.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_8.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst_View.reset();
        vw_cwf_Org_Empl_Tbl.reset();
        vw_naz_Table_Ddm.reset();

        ldaPstl6365.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Const_Pnd_Tiaa_Std.setInitialValue("T");
        pnd_Const_Pnd_Tiaa_Grad.setInitialValue("G");
        pnd_Const_Pnd_Tiaa_Re.setInitialValue("R");
        pnd_Const_Pnd_Tiaa_Acc.setInitialValue("D");
        pnd_Const_Pnd_Annual.setInitialValue("A");
        pnd_Const_Pnd_Yearly.setInitialValue("Y");
        pnd_Const_Pnd_Percent.setInitialValue("P");
        pnd_Const_Pnd_Dollars.setInitialValue("D");
        pnd_Const_Pnd_Units.setInitialValue("U");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn580() throws Exception
    {
        super("Iatn580");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ======================================================================
        //*                          START OF SUB-PROGRAM
        //* ============================================ =========================
        pnd_Return_Code.reset();                                                                                                                                          //Natural: RESET #RETURN-CODE #MSG
        pnd_Msg.reset();
        if (condition(pnd_Request_Id.equals(" ")))                                                                                                                        //Natural: IF #REQUEST-ID = ' '
        {
            pnd_Return_Code.setValue("E");                                                                                                                                //Natural: MOVE 'E' TO #RETURN-CODE
            pnd_Msg.setValue("Request id is blank");                                                                                                                      //Natural: MOVE 'Request id is blank' TO #MSG
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
            //*  FOR POST & EFM CONSTRUCT
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Iatn216.class , getCurrentProcessState(), iatn216_Data_Area);                                                                                     //Natural: CALLNAT 'IATN216' IATN216-DATA-AREA
        if (condition(Global.isEscape())) return;
        vw_iaa_Trnsfr_Sw_Rqst_View.startDatabaseFind                                                                                                                      //Natural: FIND ( 1 ) IAA-TRNSFR-SW-RQST-VIEW WITH IAA-TRNSFR-SW-RQST-VIEW.RQST-ID = #REQUEST-ID
        (
        "FND1",
        new Wc[] { new Wc("RQST_ID", "=", pnd_Request_Id, WcType.WITH) },
        1
        );
        FND1:
        while (condition(vw_iaa_Trnsfr_Sw_Rqst_View.readNextRow("FND1", true)))
        {
            vw_iaa_Trnsfr_Sw_Rqst_View.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Trnsfr_Sw_Rqst_View.getAstCOUNTER().equals(0)))                                                                                          //Natural: IF NO RECORD FOUND
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RQST Id '", pnd_Request_Id, "' not on file."));       //Natural: COMPRESS 'RQST Id "' #REQUEST-ID '" not on file.' INTO ##MSG LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            //*   '1'=TRANSFER  '2'=SWITCH
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde.notEquals("1")))                                                                                          //Natural: IF RCRD-TYPE-CDE NE '1'
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Invalid Record Type. RCRD-TYPE-CDE must be 1."));                                    //Natural: COMPRESS 'Invalid Record Type. RCRD-TYPE-CDE must be 1.' INTO ##MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  021909
                                                                                                                                                                          //Natural: PERFORM GET-EMPLOYEE-SIGNATURE
            sub_Get_Employee_Signature();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-OPEN
            sub_Call_Post_For_Open();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-AC-CONTRACT-REC
            sub_Write_Ac_Contract_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-AC-FROM-REC
            sub_Write_Ac_From_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-AC-TO-REC
            sub_Write_Ac_To_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PERFORM WRITE-AC-FYI-REC
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
            sub_Call_Post_For_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ET
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //* ********************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
            //*  FND1
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* ======================================================================
        //*                        START OF SUBROUTINES
        //* ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-OPEN
        //* *PSTA9611.PIN-NBR         := IA-UNIQUE-ID
        //*  PSTA9611
        //* ************************************************* 021909 **************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-EMPLOYEE-SIGNATURE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AC-CONTRACT-REC
        //*  REQUEST MODE:- 'C'=PHONE 'M'=MAIL 'V'=PERSONAL VISIT  'A'=ATS REQUEST
        //* *MOVE RQST-CNTCT-MDE  TO AC-REQ-SOURCE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AC-FROM-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AC-TO-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AC-FYI-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-CLOSE
        //*  PSTA9611
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-CONTRACT-NUMBERS
        //* ***********************************************************************
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Return_Code.setValue("E");                                                                                                                                    //Natural: MOVE 'E' TO #RETURN-CODE
        pnd_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), " (", Global.getPROGRAM(), ")"));                     //Natural: COMPRESS ##MSG ' (' *PROGRAM ')' INTO #MSG LEAVING NO
        //*  BT
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( FND1. )
        Global.setEscapeCode(EscapeType.Bottom, "FND1");
        if (true) return;
    }
    private void sub_Call_Post_For_Open() throws Exception                                                                                                                //Natural: CALL-POST-FOR-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  082017 REPLACED PSTA9611 WITH PSTA9612
        //* *RESET PSTA9611
        //*  082017 NEW FIELD
        //*  082017 NEW FIELD
        pdaPsta9612.getPsta9612().reset();                                                                                                                                //Natural: RESET PSTA9612
        pdaPsta9612.getPsta9612_Univ_Id().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id);                                                                                 //Natural: ASSIGN PSTA9612.UNIV-ID := IA-UNIQUE-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("IAIQ");                                                                                                          //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'IAIQ'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFIAREM");                                                                                                          //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFIAREM'
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                        //Natural: ASSIGN PSTA9612.PCKGE-DLVRY-TYP := '   '
        pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue(pnd_Printer_Id);                                                                                                  //Natural: ASSIGN PSTA9612.PRNTR-ID-CDE := #PRINTER-ID
        pdaPsta9612.getPsta9612_Tiaa_Cntrct_Nbr().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                                        //Natural: ASSIGN PSTA9612.TIAA-CNTRCT-NBR := IA-FRM-CNTRCT
        temp_Ia_Frm_Payee_A.setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee);                                                                                               //Natural: ASSIGN TEMP-IA-FRM-PAYEE-A := IA-FRM-PAYEE
        pdaPsta9612.getPsta9612_Payee_Cde().setValue(temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N);                                                                            //Natural: ASSIGN PSTA9612.PAYEE-CDE := TEMP-IA-FRM-PAYEE-N
        pdaPsta9612.getPsta9612_Dont_Use_Finalist().setValue(false);                                                                                                      //Natural: ASSIGN DONT-USE-FINALIST := FALSE
        pdaPsta9612.getPsta9612_Addrss_Lines_Rule().setValue(false);                                                                                                      //Natural: ASSIGN ADDRSS-LINES-RULE := FALSE
        pdaPsta9612.getPsta9612_Last_Nme().setValue(" ");                                                                                                                 //Natural: ASSIGN LAST-NME := ' '
        pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(" ");                                                                                                           //Natural: ASSIGN ADDRSS-TYP-CDE := ' '
        DbsUtil.callnat(Pstn9525.class , getCurrentProcessState(), pdaPsta9612.getPsta9612_Letter_Dte(), pdaCwfpda_M.getMsg_Info_Sub());                                  //Natural: CALLNAT 'PSTN9525' LETTER-DTE MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaPsta9612.getPsta9612_Next_Bsnss_Day_Ind().setValue(true);                                                                                                      //Natural: ASSIGN NEXT-BSNSS-DAY-IND := TRUE
        pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(" ");                                                                                                           //Natural: ASSIGN LTTR-SLTTN-TXT := ' '
        pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue(" ");                                                                                                          //Natural: ASSIGN EMPL-SGNTRY-NME := ' '
        pdaPsta9612.getPsta9612_Empl_Unit_Work_Nme().setValue(" ");                                                                                                       //Natural: ASSIGN EMPL-UNIT-WORK-NME := ' '
        pdaPsta9612.getPsta9612_Print_Fclty_Cde().setValue("S");                                                                                                          //Natural: ASSIGN PRINT-FCLTY-CDE := 'S'
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(false);                                                                                                    //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := FALSE
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(false);                                                                                                    //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := FALSE
        pdaPsta9612.getPsta9612_Form_Lbrry_Id().setValue(" ");                                                                                                            //Natural: ASSIGN FORM-LBRRY-ID := ' '
        pdaPsta9612.getPsta9612_Fnshng_Cde().setValue(" ");                                                                                                               //Natural: ASSIGN FNSHNG-CDE := ' '
        pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Rqst_Log_Dte_Tme);                                                                            //Natural: ASSIGN PSTA9612.RQST-LOG-DTE-TME ( 1 ) := #RQST-LOG-DTE-TME
        pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(1).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id);                                                          //Natural: ASSIGN PSTA9612.WORK-PRCSS-ID ( 1 ) := XFR-WORK-PRCSS-ID
        pdaPsta9612.getPsta9612_Mit_Contract_Nbr().getValue(1,1).setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                         //Natural: ASSIGN PSTA9612.MIT-CONTRACT-NBR ( 1,1 ) := IA-FRM-CNTRCT
        pdaPsta9612.getPsta9612_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                               //Natural: ASSIGN PSTA9612.WPID-VLDTE-IND ( 1 ) := 'Y'
        pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue(pnd_Mit_Status);                                                                                        //Natural: ASSIGN PSTA9612.STATUS-CDE ( 1 ) := #MIT-STATUS
        //*  082017 END
        //*  CALL POST OPEN
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *CALLNAT 'PSTN9610'                             /* 082017 START
        //*  082017 END
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST-FOR-OPEN
    }
    private void sub_Get_Employee_Signature() throws Exception                                                                                                            //Natural: GET-EMPLOYEE-SIGNATURE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Name.reset();                                                                                                                                                 //Natural: RESET #NAME
        vw_cwf_Org_Empl_Tbl.startDatabaseRead                                                                                                                             //Natural: READ CWF-ORG-EMPL-TBL BY EMPL-RACF-ID-KEY STARTING FROM RQST-ENTRY-USER-ID
        (
        "READ01",
        new Wc[] { new Wc("EMPL_RACF_ID_KEY", ">=", iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id, WcType.BY) },
        new Oc[] { new Oc("EMPL_RACF_ID_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Org_Empl_Tbl_Empl_Racf_Id.notEquals(iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id)))                                                           //Natural: IF CWF-ORG-EMPL-TBL.EMPL-RACF-ID NE RQST-ENTRY-USER-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Org_Empl_Tbl_Actve_Ind.equals("A ") && cwf_Org_Empl_Tbl_Empl_Signatory.notEquals(" "))))                                                  //Natural: ACCEPT IF ACTVE-IND = 'A ' AND EMPL-SIGNATORY NE ' '
            {
                continue;
            }
            pnd_Name.setValue(cwf_Org_Empl_Tbl_Empl_Signatory);                                                                                                           //Natural: ASSIGN #NAME := EMPL-SIGNATORY
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET DEFAULT SIGNATURE FROM ADAT
        if (condition(pnd_Name.equals(" ")))                                                                                                                              //Natural: IF #NAME EQ ' '
        {
            pnd_Naz_Table_Key.setValue("NAZ021UTLUTL9");                                                                                                                  //Natural: ASSIGN #NAZ-TABLE-KEY := 'NAZ021UTLUTL9'
            vw_naz_Table_Ddm.startDatabaseFind                                                                                                                            //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = #NAZ-TABLE-KEY
            (
            "FIND01",
            new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Naz_Table_Key, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_naz_Table_Ddm.readNextRow("FIND01")))
            {
                vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
                pnd_Name.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                                //Natural: ASSIGN #NAME := NAZ-TBL-RCRD-DSCRPTN-TXT
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Ac_Contract_Rec() throws Exception                                                                                                             //Natural: WRITE-AC-CONTRACT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO AC-CONTRACT-STRUCT
        //*  CREF, TIAA AND TIAA REA
                                                                                                                                                                          //Natural: PERFORM FILL-CONTRACT-NUMBERS
        sub_Fill_Contract_Numbers();
        if (condition(Global.isEscape())) {return;}
        ldaPstl6365.getAc_Contract_Struct_Ac_Req_Source().setValue("V");                                                                                                  //Natural: ASSIGN AC-REQ-SOURCE := 'V'
        //*  CONVERT THE SERVICE REPS NAME TO LOWER CASE
        //* *#NAME := 'Ellen Maxwell'                       /* 021909
        //* *MOVE RQST-REP-NME TO #NAME
        //* *CALLNAT 'NAZN553' #NAME
        ldaPstl6365.getAc_Contract_Struct_Ac_Service_Rep().setValue(pnd_Name);                                                                                            //Natural: MOVE #NAME TO AC-SERVICE-REP
        //*  CHECK THE ACKNOWLEDGEMENT TYPE
        short decideConditionsMet1025 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #RQST-TYPE;//Natural: VALUE ' '
        if (condition((pnd_Rqst_Type.equals(" "))))
        {
            decideConditionsMet1025++;
            //*  FIRST TRANSFER
            ldaPstl6365.getAc_Contract_Struct_Ac_Ack_Type().setValue(1);                                                                                                  //Natural: MOVE 1 TO AC-ACK-TYPE
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((pnd_Rqst_Type.equals("D"))))
        {
            decideConditionsMet1025++;
            //*  CANCELLATION
            ldaPstl6365.getAc_Contract_Struct_Ac_Ack_Type().setValue(2);                                                                                                  //Natural: MOVE 2 TO AC-ACK-TYPE
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pnd_Rqst_Type.equals("C"))))
        {
            decideConditionsMet1025++;
            //*  CHANGE
            ldaPstl6365.getAc_Contract_Struct_Ac_Ack_Type().setValue(3);                                                                                                  //Natural: MOVE 3 TO AC-ACK-TYPE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Date_A.setValueEdited(iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte,new ReportEditMask("MMDDYYYY"));                                                                //Natural: MOVE EDITED RQST-EFFCTV-DTE ( EM = MMDDYYYY ) TO #DATE-A
        ldaPstl6365.getAc_Contract_Struct_Ac_Effective_Date().setValue(pnd_Date_A_Pnd_Date_N);                                                                            //Natural: MOVE #DATE-N TO AC-EFFECTIVE-DATE
        //*  GET VALUES FOR AC-MOD-DATE-TIME
        pnd_Date_A.setValueEdited(iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED RQST-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Date_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                                //Natural: MOVE #DATE-N TO #DATE-YYYYMMDD
        pnd_Mod_Ts_Pnd_Mod_Date_Mm.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Mm);                                                                                               //Natural: MOVE #DATE-MM TO #MOD-DATE-MM
        pnd_Mod_Ts_Pnd_Mod_Date_Dd.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Dd);                                                                                               //Natural: MOVE #DATE-DD TO #MOD-DATE-DD
        pnd_Mod_Ts_Pnd_Mod_Date_Yyyy.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Yyyy);                                                                                           //Natural: MOVE #DATE-YYYY TO #MOD-DATE-YYYY
        pnd_Mod_Ts_Pnd_Mod_Time.setValue(Global.getTIMN());                                                                                                               //Natural: MOVE *TIMN TO #MOD-TIME
        if (condition(pnd_Mod_Ts_Pnd_Mod_Hrs.greater(12)))                                                                                                                //Natural: IF #MOD-HRS GT 12
        {
            pnd_Mod_Ts_Pnd_Mod_Hrs.nsubtract(12);                                                                                                                         //Natural: SUBTRACT 12 FROM #MOD-HRS
            //*  PM
            pnd_Mod_Ts_Pnd_Mod_Am.setValue(1);                                                                                                                            //Natural: MOVE 1 TO #MOD-AM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  AM
            pnd_Mod_Ts_Pnd_Mod_Am.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #MOD-AM
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6365.getAc_Contract_Struct_Ac_Mod_Date_Time().setValue(pnd_Mod_Ts);                                                                                        //Natural: MOVE #MOD-TS TO AC-MOD-DATE-TIME
        ldaPstl6365.getAc_Contract_Struct_Ac_From_Tran_Type().setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(1));                                                  //Natural: MOVE XFR-FRM-TYP ( 1 ) TO AC-FROM-TRAN-TYPE
        ldaPstl6365.getAc_Contract_Struct_Ac_To_Tran_Type().setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ.getValue(1));                                                     //Natural: MOVE XFR-TO-TYP ( 1 ) TO AC-TO-TRAN-TYPE
        //*  ??
        ldaPstl6365.getAc_Contract_Struct_Ac_Mach_Func_1().setValue(2);                                                                                                   //Natural: MOVE 2 TO AC-MACH-FUNC-1
        //*  ??
        ldaPstl6365.getAc_Contract_Struct_Ac_Image_Ind().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO AC-IMAGE-IND
        //*   SET UP SORT KEY
        ldaPstl6365.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6365.getSort_Key_Struct_Key_L3_Req_Date_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                     //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO SORT-KEY-STRUCT.KEY-L3-REQ-DATE-TIME
        ldaPstl6365.getSort_Key_Struct_Key_L3_From_Cont_Number().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                         //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L3-FROM-CONT-NUMBER := IA-FRM-CNTRCT
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-CONTRACT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6365.getSort_Key_Struct(), ldaPstl6365.getAc_Contract_Struct());
        if (condition(Global.isEscape())) return;
        //*   PSTL6145-DATA
        //*  OPEN  MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-AC-CONTRACT-REC
    }
    private void sub_Write_Ac_From_Rec() throws Exception                                                                                                                 //Natural: WRITE-AC-FROM-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Rec().getValue("*").reset();                                                                                              //Natural: RESET AC-FRM-REC-STRUCT.AC-FRM-REC ( * ) AC-FRM-PERCENT ( * ) AC-FRM-DOLLARS ( * ) AC-FRM-UNITS ( * ) AC-TO-REC-STRUCT.AC-TO-REC ( * ) AC-TO-PERCENT ( * ) AC-TO-DOLLARS ( * ) AC-TO-UNITS ( * )
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Percent().getValue("*").reset();
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Dollars().getValue("*").reset();
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Units().getValue("*").reset();
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Rec().getValue("*").reset();
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Percent().getValue("*").reset();
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Dollars().getValue("*").reset();
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Units().getValue("*").reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  SET AC-DATA-OCCURS
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(" ") && iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(" ")))       //Natural: IF XFR-FRM-ACCT-CDE ( #I ) = ' ' AND XFR-FRM-TYP ( #I ) = ' '
            {
                ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Data_Occurs().compute(new ComputeParameters(false, ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Data_Occurs()),                //Natural: ASSIGN AC-FRM-REC-STRUCT.AC-DATA-OCCURS := #I - 1
                    pnd_I.subtract(1));
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Data_Occurs().setValue(pnd_I);                                                                                        //Natural: ASSIGN AC-FRM-REC-STRUCT.AC-DATA-OCCURS := #I
            }                                                                                                                                                             //Natural: END-IF
            //*  CONVERT ONE BYTE FUND CODE TO THE (N2) FUND CODE USED BY POST.
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF XFR-FRM-ACCT-CDE ( #I ) NE ' '
            {
                DbsUtil.callnat(Iatn0201.class , getCurrentProcessState(), iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I), pnd_Iatn0201_Out_Pnd_Post_Fund_N2);  //Natural: CALLNAT 'IATN0201' XFR-FRM-ACCT-CDE ( #I ) #IATN0201-OUT.#POST-FUND-N2
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Account_Type().getValue(pnd_I).setValue(pnd_Iatn0201_Out_Pnd_Post_Fund_N2);                                       //Natural: ASSIGN AC-FRM-ACCOUNT-TYPE ( #I ) := #IATN0201-OUT.#POST-FUND-N2
                if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                     //Natural: IF XFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Myi().getValue(pnd_I).setValue(pnd_Const_Pnd_Yearly);                                                         //Natural: ASSIGN AC-FRM-MYI ( #I ) := #YEARLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Myi().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ.getValue(pnd_I));                     //Natural: ASSIGN AC-FRM-MYI ( #I ) := XFR-FRM-UNIT-TYP ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK IF THE TRANSFER WAS FROM PERCENT, DOLLARS OR UNITS
            short decideConditionsMet1117 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF XFR-FRM-TYP ( #I );//Natural: VALUE #PERCENT
            if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Percent))))
            {
                decideConditionsMet1117++;
                ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Percent().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty.getValue(pnd_I));                          //Natural: ASSIGN AC-FRM-PERCENT ( #I ) := XFR-FRM-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #DOLLARS
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Dollars))))
            {
                decideConditionsMet1117++;
                ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Dollars().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty.getValue(pnd_I));                          //Natural: ASSIGN AC-FRM-DOLLARS ( #I ) := XFR-FRM-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #UNITS
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Units))))
            {
                decideConditionsMet1117++;
                ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Units().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty.getValue(pnd_I));                            //Natural: ASSIGN AC-FRM-UNITS ( #I ) := XFR-FRM-QTY ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Trn_Typ().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I));                              //Natural: ASSIGN AC-FRM-TRN-TYP ( #I ) := XFR-FRM-TYP ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6365.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6365.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Account_Type().getValue(1));                                //Natural: MOVE AC-FRM-ACCOUNT-TYPE ( 1 ) TO SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE
        ldaPstl6365.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM
        ldaPstl6365.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-FRM-REC-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6365.getSort_Key_Struct(), ldaPstl6365.getAc_Frm_Rec_Struct());
        if (condition(Global.isEscape())) return;
        //*  OPEN  MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-AC-FROM-REC
    }
    private void sub_Write_Ac_To_Rec() throws Exception                                                                                                                   //Natural: WRITE-AC-TO-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Rec().getValue("*").reset();                                                                                                //Natural: RESET AC-TO-REC-STRUCT.AC-TO-REC ( * ) AC-TO-PERCENT ( * ) AC-TO-DOLLARS ( * ) AC-TO-UNITS ( * ) AC-FRM-REC-STRUCT.AC-FRM-REC ( * ) AC-FRM-PERCENT ( * ) AC-FRM-DOLLARS ( * ) AC-FRM-UNITS ( * )
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Percent().getValue("*").reset();
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Dollars().getValue("*").reset();
        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Units().getValue("*").reset();
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Rec().getValue("*").reset();
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Percent().getValue("*").reset();
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Dollars().getValue("*").reset();
        ldaPstl6365.getAc_Frm_Rec_Struct_Ac_Frm_Units().getValue("*").reset();
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 40
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            //*  SET AC-DATA-OCCURS
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).equals(" ") && iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ.getValue(pnd_I).equals(" ")))         //Natural: IF XFR-TO-ACCT-CDE ( #I ) = ' ' AND XFR-TO-TYP ( #I ) = ' '
            {
                ldaPstl6365.getAc_To_Rec_Struct_Ac_Data_Occurs().compute(new ComputeParameters(false, ldaPstl6365.getAc_To_Rec_Struct_Ac_Data_Occurs()),                  //Natural: ASSIGN AC-TO-REC-STRUCT.AC-DATA-OCCURS := #I - 1
                    pnd_I.subtract(1));
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6365.getAc_To_Rec_Struct_Ac_Data_Occurs().setValue(pnd_I);                                                                                         //Natural: ASSIGN AC-TO-REC-STRUCT.AC-DATA-OCCURS := #I
            }                                                                                                                                                             //Natural: END-IF
            //*  CONVERT ONE BYTE FUND CODE TO THE (N2) FUND CODE USED BY POST.
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).notEquals(" ")))                                                                        //Natural: IF XFR-TO-ACCT-CDE ( #I ) NE ' '
            {
                DbsUtil.callnat(Iatn0201.class , getCurrentProcessState(), iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I), pnd_Iatn0201_Out_Pnd_Post_Fund_N2);   //Natural: CALLNAT 'IATN0201' XFR-TO-ACCT-CDE ( #I ) #IATN0201-OUT.#POST-FUND-N2
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Account_Type().getValue(pnd_I).setValue(pnd_Iatn0201_Out_Pnd_Post_Fund_N2);                                         //Natural: ASSIGN AC-TO-ACCOUNT-TYPE ( #I ) := #IATN0201-OUT.#POST-FUND-N2
                if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                      //Natural: IF XFR-TO-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Myi().getValue(pnd_I).setValue(pnd_Const_Pnd_Yearly);                                                           //Natural: ASSIGN AC-TO-MYI ( #I ) := #YEARLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ.getValue(pnd_I).notEquals(" ")))                                                                //Natural: IF XFR-TO-UNIT-TYP ( #I ) NE ' '
                    {
                        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Myi().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ.getValue(pnd_I));                    //Natural: ASSIGN AC-TO-MYI ( #I ) := XFR-TO-UNIT-TYP ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(iaa_Trnsfr_Sw_Rqst_View_Rqst_Xfr_Type.equals(" ")))                                                                                 //Natural: IF RQST-XFR-TYPE = ' '
                        {
                            ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Myi().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ.getValue(1));                   //Natural: ASSIGN AC-TO-MYI ( #I ) := XFR-FRM-UNIT-TYP ( 1 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Myi().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ.getValue(pnd_I));                //Natural: ASSIGN AC-TO-MYI ( #I ) := XFR-TO-UNIT-TYP ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Myi().getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                              //Natural: IF AC-TO-MYI ( #I ) = #ANNUAL
                    {
                        ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Myi().getValue(pnd_I).setValue(pnd_Const_Pnd_Yearly);                                                       //Natural: ASSIGN AC-TO-MYI ( #I ) := #YEARLY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK IF THE TRANSFER IS TO PERCENT, DOLLARS OR UNITS
            short decideConditionsMet1191 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF XFR-TO-TYP ( #I );//Natural: VALUE #PERCENT
            if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Percent))))
            {
                decideConditionsMet1191++;
                ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Percent().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty.getValue(pnd_I));                             //Natural: ASSIGN AC-TO-PERCENT ( #I ) := XFR-TO-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #DOLLARS
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Dollars))))
            {
                decideConditionsMet1191++;
                ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Dollars().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty.getValue(pnd_I));                             //Natural: ASSIGN AC-TO-DOLLARS ( #I ) := XFR-TO-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #UNITS
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Units))))
            {
                decideConditionsMet1191++;
                ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Units().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty.getValue(pnd_I));                               //Natural: ASSIGN AC-TO-UNITS ( #I ) := XFR-TO-QTY ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Trn_Typ().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ.getValue(pnd_I));                                 //Natural: ASSIGN AC-TO-TRN-TYP ( #I ) := XFR-TO-TYP ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6365.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6365.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6365.getAc_To_Rec_Struct_Ac_To_Account_Type().getValue(1));                                  //Natural: MOVE AC-TO-ACCOUNT-TYPE ( 1 ) TO SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE
        ldaPstl6365.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM
        ldaPstl6365.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-TO-REC-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6365.getSort_Key_Struct(), ldaPstl6365.getAc_To_Rec_Struct());
        if (condition(Global.isEscape())) return;
        //*  OPEN  MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-AC-TO-REC
    }
    private void sub_Write_Ac_Fyi_Rec() throws Exception                                                                                                                  //Natural: WRITE-AC-FYI-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        //*  NOT A CANCELLATION
        if (condition(iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Dte.equals(getZero()) && pnd_Rqst_Type.notEquals("D")))                                                           //Natural: IF RQST-RCVD-DTE EQ 0 AND #RQST-TYPE NE 'D'
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ASSIGN #J := #J +1
            //*  "We have received your TIAA..."
            ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Msg_Number().getValue(pnd_J).setValue(10);                                                                                //Natural: MOVE 10 TO AC-FYI-MSG-NUMBER ( #J )
            ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(pnd_J).setValue(10);                                                                                   //Natural: MOVE 010 TO AC-FYI-SRT-NUM ( #J )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.nadd(1);                                                                                                                                                    //Natural: ASSIGN #J := #J + 1
        //*  "If you have any Questions...."
        ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Msg_Number().getValue(pnd_J).setValue(20);                                                                                    //Natural: MOVE 20 TO AC-FYI-MSG-NUMBER ( #J )
        ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(pnd_J).setValue(20);                                                                                       //Natural: MOVE 020 TO AC-FYI-SRT-NUM ( #J )
        //*  NOT A CANCELLATION
        if (condition(pnd_Rqst_Type.notEquals("D")))                                                                                                                      //Natural: IF #RQST-TYPE NE 'D'
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ASSIGN #J := #J + 1
            //*  "You can change or cancel....."
            ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Msg_Number().getValue(pnd_J).setValue(30);                                                                                //Natural: MOVE 30 TO AC-FYI-MSG-NUMBER ( #J )
            ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(pnd_J).setValue(30);                                                                                   //Natural: MOVE 030 TO AC-FYI-SRT-NUM ( #J )
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 40
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
            {
                //*  TIAA STD
                if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).equals("T")))                                                                       //Natural: IF XFR-TO-ACCT-CDE ( #I ) = 'T'
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ASSIGN #J := #J + 1
                    //*  "Transfers to TIAA Tradit.."
                    ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Msg_Number().getValue(pnd_J).setValue(40);                                                                        //Natural: MOVE 40 TO AC-FYI-MSG-NUMBER ( #J )
                    ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(pnd_J).setValue(40);                                                                           //Natural: MOVE 040 TO AC-FYI-SRT-NUM ( #J )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_J.nadd(1);                                                                                                                                                //Natural: ASSIGN #J := #J + 1
            //*  "You will recieve a written..."
            ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Msg_Number().getValue(pnd_J).setValue(50);                                                                                //Natural: MOVE 50 TO AC-FYI-MSG-NUMBER ( #J )
            ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(pnd_J).setValue(50);                                                                                   //Natural: MOVE 050 TO AC-FYI-SRT-NUM ( #J )
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6365.getAc_Fyi_Struct_Ac_Data_Occurs().setValue(pnd_J);                                                                                                    //Natural: ASSIGN AC-FYI-STRUCT.AC-DATA-OCCURS := #J
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6365.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6365.getSort_Key_Struct_Key_L4_Account_Type().setValue(99);                                                                                                //Natural: MOVE 99 TO SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE
        ldaPstl6365.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(ldaPstl6365.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(1));                                          //Natural: MOVE AC-FYI-SRT-NUM ( 1 ) TO SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM
        ldaPstl6365.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM
        pdaPsta9500.getPsta9500_Application_Key().reset();                                                                                                                //Natural: RESET PSTA9500.APPLICATION-KEY
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-FYI-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6365.getSort_Key_Struct(), ldaPstl6365.getAc_Fyi_Struct());
        if (condition(Global.isEscape())) return;
        //*  OPEN  MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-AC-FYI-REC
    }
    private void sub_Call_Post_For_Close() throws Exception                                                                                                               //Natural: CALL-POST-FOR-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CALL POST CLOSE TO FINALISE THE REQUEST
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *CALLNAT 'PSTN9680'                             /* 082017 START
        //*  082017 END
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //*  OPEN  MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST-FOR-CLOSE
    }
    private void sub_Fill_Contract_Numbers() throws Exception                                                                                                             //Natural: FILL-CONTRACT-NUMBERS
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  CHECK THE FROM FUNDS
            //*  021909
            //*  021909
            //*  THIS IS A CREF FUND
            short decideConditionsMet1312 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF XFR-FRM-ACCT-CDE ( #I );//Natural: VALUE ' '
            if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet1312++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-STD
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Std))))
            {
                decideConditionsMet1312++;
                pnd_Trans_Type_Pnd_From_Std.setValue("Y");                                                                                                                //Natural: ASSIGN #FROM-STD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-GRAD
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Grad))))
            {
                decideConditionsMet1312++;
                pnd_Trans_Type_Pnd_From_Grad.setValue("Y");                                                                                                               //Natural: ASSIGN #FROM-GRAD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-RE
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Re))))
            {
                decideConditionsMet1312++;
                pnd_Trans_Type_Pnd_From_Re.setValue("Y");                                                                                                                 //Natural: ASSIGN #FROM-RE := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-ACC
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Acc))))
            {
                decideConditionsMet1312++;
                pnd_Trans_Type_Pnd_From_Acc.setValue("Y");                                                                                                                //Natural: ASSIGN #FROM-ACC := 'Y'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Trans_Type_Pnd_From_Cref.setValue("Y");                                                                                                               //Natural: ASSIGN #FROM-CREF := 'Y'
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CHECK THE TO FUNDS
            //*  021909
            //*  021909
            //*  THIS IS A CREF FUND
            short decideConditionsMet1332 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF XFR-TO-ACCT-CDE ( #I );//Natural: VALUE ' '
            if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet1332++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-STD
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Std))))
            {
                decideConditionsMet1332++;
                pnd_Trans_Type_Pnd_To_Std.setValue("Y");                                                                                                                  //Natural: ASSIGN #TO-STD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-GRAD
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Grad))))
            {
                decideConditionsMet1332++;
                pnd_Trans_Type_Pnd_To_Grad.setValue("Y");                                                                                                                 //Natural: ASSIGN #TO-GRAD := 'Y'
            }                                                                                                                                                             //Natural: VALUE #TIAA-RE
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Re))))
            {
                decideConditionsMet1332++;
                pnd_Trans_Type_Pnd_To_Re.setValue("Y");                                                                                                                   //Natural: ASSIGN #TO-RE := 'Y'
            }                                                                                                                                                             //Natural: VALUE #TIAA-ACC
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Acc))))
            {
                decideConditionsMet1332++;
                pnd_Trans_Type_Pnd_To_Acc.setValue("Y");                                                                                                                  //Natural: ASSIGN #TO-ACC := 'Y'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Trans_Type_Pnd_To_Cref.setValue("Y");                                                                                                                 //Natural: ASSIGN #TO-CREF := 'Y'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Trans_Type_Pnd_From_Grad.equals("Y") || pnd_Trans_Type_Pnd_From_Std.equals("Y")))                                                               //Natural: IF #FROM-GRAD = 'Y' OR #FROM-STD = 'Y'
        {
            ldaPstl6365.getAc_Contract_Struct_Ac_Tiaa_Contract_Num().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                     //Natural: ASSIGN AC-TIAA-CONTRACT-NUM := IA-FRM-CNTRCT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  FROM REAL ESTATE
            //*  021909
            if (condition(pnd_Trans_Type_Pnd_From_Re.equals("Y") || pnd_Trans_Type_Pnd_From_Acc.equals("Y")))                                                             //Natural: IF #FROM-RE = 'Y' OR #FROM-ACC = 'Y'
            {
                ldaPstl6365.getAc_Contract_Struct_Ac_Real_Estate_Contract().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                              //Natural: ASSIGN AC-REAL-ESTATE-CONTRACT := IA-FRM-CNTRCT
                //*  042209
                ldaPstl6365.getAc_Contract_Struct_Ac_Real_Estate_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaPstl6365.getAc_Contract_Struct_Ac_Real_Estate_Contract(),  //Natural: COMPRESS AC-REAL-ESTATE-CONTRACT '-REA' INTO AC-REAL-ESTATE-CONTRACT LEAVING NO
                    "-REA"));
                if (condition(pnd_Trans_Type_Pnd_To_Cref.equals("Y")))                                                                                                    //Natural: IF #TO-CREF = 'Y'
                {
                    ldaPstl6365.getAc_Contract_Struct_Ac_Cref_Contract_Num().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct);                                              //Natural: ASSIGN AC-CREF-CONTRACT-NUM := IA-TO-CNTRCT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Trans_Type_Pnd_To_Std.equals("Y") || pnd_Trans_Type_Pnd_To_Grad.equals("Y")))                                                           //Natural: IF #TO-STD = 'Y' OR #TO-GRAD = 'Y'
                {
                    ldaPstl6365.getAc_Contract_Struct_Ac_Tiaa_Contract_Num().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                             //Natural: ASSIGN AC-TIAA-CONTRACT-NUM := IA-FRM-CNTRCT
                }                                                                                                                                                         //Natural: END-IF
                //*  FROM CREF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6365.getAc_Contract_Struct_Ac_Cref_Contract_Num().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                 //Natural: ASSIGN AC-CREF-CONTRACT-NUM := IA-FRM-CNTRCT
                //*  021909
                if (condition(pnd_Trans_Type_Pnd_To_Re.equals("Y") || pnd_Trans_Type_Pnd_To_Acc.equals("Y")))                                                             //Natural: IF #TO-RE = 'Y' OR #TO-ACC = 'Y'
                {
                    ldaPstl6365.getAc_Contract_Struct_Ac_Real_Estate_Contract().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct);                                           //Natural: ASSIGN AC-REAL-ESTATE-CONTRACT := IA-TO-CNTRCT
                    //*  042209
                    ldaPstl6365.getAc_Contract_Struct_Ac_Real_Estate_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaPstl6365.getAc_Contract_Struct_Ac_Real_Estate_Contract(),  //Natural: COMPRESS AC-REAL-ESTATE-CONTRACT '-REA' INTO AC-REAL-ESTATE-CONTRACT LEAVING NO
                        "-REA"));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Trans_Type_Pnd_To_Std.equals("Y") || pnd_Trans_Type_Pnd_To_Grad.equals("Y")))                                                           //Natural: IF #TO-STD = 'Y' OR #TO-GRAD = 'Y'
                {
                    ldaPstl6365.getAc_Contract_Struct_Ac_Tiaa_Contract_Num().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct);                                              //Natural: ASSIGN AC-TIAA-CONTRACT-NUM := IA-TO-CNTRCT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FILL-CONTRACT-NUMBERS
    }

    //
}
