/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:58 AM
**        * FROM NATURAL SUBPROGRAM : Iaan902b
************************************************************
**        * FILE NAME            : Iaan902b.java
**        * CLASS NAME           : Iaan902b
**        * INSTANCE NAME        : Iaan902b
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN902B CREATES MANUAL NEW ISSUE CODING SHEETS   *
*      DATE     -  8/94                                              *
*                                                                    *
**********************************************************************
* CHANGE HISTORY
* CHANGED ON APRIL 10, 1995 BY D ROBINSON
* >   REMOVED 42 TRANSACTION PROCESS CODING
* CHANGED ON MAY 10, 1995 BY D ROBINSON
* >   CHANGED TO BYPASS CONTRACTUAL CORRECTIONS WHEN NEW ISSUE
* >   EXIST FOR DEATH CLAIMS
* CHANGED ON JUNE 1, 1995 BY D ROBINSON
* >   REMOVED BEFORE IMAGE READ OF CNTRCT TRANS WHEN ASSIGNING
* >   SECOND ANNUITANT CROSS REFERENCE
* CHANGED ON JUNE 1, 1995 BY D ROBINSON
* >   SET CORRECT CONTRACT OPTION CODE INTERROGATION
* CHANGED ON AUGUST 1, 1995 BY D ROBINSON
* >   CHANGED TO CORRESPOND TO NEW CREF FUND VIEW
* CHANGED ON SEPT 1, 1995 BY D ROBINSON
* >   CHANGED TO PROCESS CONTRACTUAL CORRECTIONS AND SUSPEND PAYMENT
* >   TRANSACTIONS DIFFERENTLY IF CONTRACT HAS REVERSAL
* CHANGED ON DEC 6, 1995 BY D ROBINSON
* >   CHANGED TO FORCE OUT CONTRACT OPTION CODE WHEN RATE CHANGE.
* > 04/2017 OS RE-STOWED FOR IAAL902B PIN EXPANSION.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan902b extends BLNatBase
{
    // Data Areas
    private LdaIaal902b ldaIaal902b;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Passed_Data;
    private DbsField pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_1;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte_P;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Passed_Data__R_Field_2;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8;
    private DbsField pnd_Passed_Data_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_3;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_4;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Area;
    private DbsField pnd_Passed_Data_Pnd_Last_Batch_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Trans_Sw;
    private DbsField pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Save_37_Payee_Cde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal902b = new LdaIaal902b();
        registerRecord(ldaIaal902b);
        registerRecord(ldaIaal902b.getVw_iaa_Cntrct());
        registerRecord(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal902b.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal902b.getVw_iaa_Cpr_Trans());
        registerRecord(ldaIaal902b.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal902b.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal902b.getVw_iaa_Cref_Fund_Rcrd_1());
        registerRecord(ldaIaal902b.getVw_iaa_Cref_Fund_Trans_1());

        // parameters
        parameters = new DbsRecord();

        pnd_Passed_Data = parameters.newGroupInRecord("pnd_Passed_Data", "#PASSED-DATA");
        pnd_Passed_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte", "#CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Passed_Data__R_Field_1 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_1", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Dte);
        pnd_Passed_Data_Pnd_Trans_Dte_P = pnd_Passed_Data__R_Field_1.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte_P", "#TRANS-DTE-P", FieldType.PACKED_DECIMAL, 
            12);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Passed_Data__R_Field_2 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_2", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8 = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8", "#TRANS-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Passed_Data_Pnd_Trans_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Passed_Data_Pnd_Trans_Check_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_3 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_3", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Check_Dte);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc", "#TRANS-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy", "#TRANS-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm", "#TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd", "#TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_4 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_4", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Effctve_Dte);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc", "#TRANS-EFFCTVE-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy", "#TRANS-EFFCTVE-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm", "#TRANS-EFFCTVE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd", "#TRANS-EFFCTVE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_User_Area = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Area", "#TRANS-USER-AREA", FieldType.STRING, 
            6);
        pnd_Passed_Data_Pnd_Last_Batch_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Passed_Data_Pnd_Trans_Sw = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Sw", "#TRANS-SW", FieldType.STRING, 1);
        pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr", "#SAVE-37-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Passed_Data_Pnd_Save_37_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_37_Payee_Cde", "#SAVE-37-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal902b.initializeValues();

        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan902b() throws Exception
    {
        super("Iaan902b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: DECIDE ON FIRST VALUE OF #TRANS-CDE
        short decideConditionsMet809 = 0;                                                                                                                                 //Natural: VALUE 033
        if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(33))))
        {
            decideConditionsMet809++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-33
            sub_Process_33();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 035
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(35))))
        {
            decideConditionsMet809++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-35
            sub_Process_35();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 050
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(50))))
        {
            decideConditionsMet809++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-50
            sub_Process_50();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-33
        //* *********************************************************************
        //*  #ST/CNTRY-RES := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-35
        //* *********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-50
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-HEADER
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-TIAA-FUND-KEY
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-TIAA-FUND
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-CREF-FUND
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CNTRCT-BFRE-KEY
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CPR-BFRE-KEY
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CREF-FUND-KEY
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIAA-33
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CREF-33
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIAA-50
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CREF-50
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-TIAA-RATE-CHANGES
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CREF-RATE-CHANGES
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-FINAL-PYMT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PROD-CODE
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal902b.getPnd_Coding_Sheets().reset();                                                                                                                       //Natural: RESET #CODING-SHEETS #NO-CNTRCT-REC #ISSUE-101-WRITTEN
        ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().reset();
        ldaIaal902b.getPnd_Logical_Variables_Pnd_Issue_101_Written().reset();
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT
        sub_Find_Iaa_Cntrct();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR
        sub_Get_Cross_Reference_Nbr();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_User_Area1().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                                  //Natural: ASSIGN #USER-AREA1 := #TRANS-USER-AREA
    }
    private void sub_Find_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: FIND-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().reset();                                                                                                 //Natural: RESET #NO-CNTRCT-REC
        ldaIaal902b.getPnd_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                 //Natural: ASSIGN #CNTRCT-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal902b.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal902b.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902b.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA CONTRACT RECORD FOR : ",pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                //Natural: WRITE 'NO IAA CONTRACT RECORD FOR : ' #TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                  //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cross_Reference_Nbr() throws Exception                                                                                                           //Natural: GET-CROSS-REFERENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet937 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(1))))
        {
            decideConditionsMet937++;
            if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ01",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ01:
                while (condition(ldaIaal902b.getVw_iaa_Cntrct_Trans().readNextRow("READ01")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                              //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                               //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(2))))
        {
            decideConditionsMet937++;
            if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ02",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ02:
                while (condition(ldaIaal902b.getVw_iaa_Cntrct_Trans().readNextRow("READ02")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                              //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                               //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ03",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ03:
                while (condition(ldaIaal902b.getVw_iaa_Cntrct_Trans().readNextRow("READ03")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                              //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                          //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ04",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ04:
                while (condition(ldaIaal902b.getVw_iaa_Cntrct_Trans().readNextRow("READ04")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                    //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                               //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                      //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                    //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
                ldaIaal902b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                    //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ05",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ05:
                while (condition(ldaIaal902b.getVw_iaa_Cntrct_Trans().readNextRow("READ05")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                    //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                          //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((pnd_Passed_Data_Pnd_Trans_Payee_Cde.greaterOrEqual(3) && pnd_Passed_Data_Pnd_Trans_Payee_Cde.lessOrEqual(99)))))
        {
            decideConditionsMet937++;
            ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                             //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGE-ID := '1'
            ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                      //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                           //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
            ldaIaal902b.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
            (
            "READ06",
            new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
            new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
            1
            );
            READ06:
            while (condition(ldaIaal902b.getVw_iaa_Cpr_Trans().readNextRow("READ06")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR = #TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = #TRANS-PAYEE-CDE
            {
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cpr_Trans_Bnfcry_Xref_Ind());                                           //Natural: ASSIGN #CROSS-REF-NBR := IAA-CPR-TRANS.BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                   //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                            //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
                (
                "FIND02",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902b.getPnd_Cntrct_Payee_Key(), WcType.WITH) },
                1
                );
                FIND02:
                while (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
                {
                    ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                             //Natural: ASSIGN #CROSS-REF-NBR := BNFCRY-XREF-IND
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_33() throws Exception                                                                                                                        //Natural: PROCESS-33
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                           //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902b.getPnd_Cntrct_Payee_Key(), WcType.WITH) },
        1
        );
        FIND03:
        while (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND03", true)))
        {
            ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT PARTICIPANT RECORD FOR: ",ldaIaal902b.getPnd_Cntrct_Payee_Key());                                                      //Natural: WRITE 'NO CONTRACT PARTICIPANT RECORD FOR: ' #CNTRCT-PAYEE-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE1
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PROD-CODE
        sub_Determine_Prod_Code();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Currency().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Crrncy_Cde());                                                          //Natural: ASSIGN #CURRENCY := IAA-CNTRCT.CNTRCT-CRRNCY-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mode().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                                   //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Pend_Code().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde());                                              //Natural: ASSIGN #PEND-CODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Hold_Check_Cde().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde());                                         //Natural: ASSIGN #HOLD-CHECK-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte());                                            //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Pend_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                               //Natural: ASSIGN #PEND-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Pend_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                               //Natural: ASSIGN #PEND-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Option_N().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde());                                                            //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Origin().setValueEdited(ldaIaal902b.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                               //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORIGIN
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Issue_Dte());                                                        //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                                //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                                //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                              //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                        //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                        //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                               //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                        //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                        //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                                   //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                        //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                        //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                                    //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                                 //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                                 //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                                 //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                                   //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                                           //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                                           //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue1().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE1 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("10");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("1");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
            ldaIaal902b.getPnd_Logical_Variables_Pnd_Issue_101_Written().setValue(true);                                                                                  //Natural: ASSIGN #ISSUE-101-WRITTEN := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue2().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE2
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Citizen().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde());                                            //Natural: ASSIGN #CITIZEN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Stfslash_Cntry_Iss().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde());                                       //Natural: ASSIGN #ST/CNTRY-ISS := IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Stfslash_Cntry_Res().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde());                                       //Natural: ASSIGN #ST/CNTRY-RES := IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Coll_Iss().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Inst_Iss_Cde());                                                        //Natural: ASSIGN #COLL-ISS := IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        if (condition(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).notEquals(" ")))                                                             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) NE ' '
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(1));                       //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(2));                       //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Ssn().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr());                                                 //Natural: ASSIGN #SSN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Joint_Cnvrt().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind());                                             //Natural: ASSIGN #JOINT-CNVRT := IAA-CNTRCT.CNTRCT-JOINT-CNVRT-RCRD-IND
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Spirt().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce());                                                //Natural: ASSIGN #SPIRT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-SPIRT-SRCE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Pen_Pln_Cde().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde());                                                     //Natural: ASSIGN #PEN-PLN-CDE := IAA-CNTRCT.CNTRCT-PNSN-PLN-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cntrct_Type().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Type_Cde());                                                         //Natural: ASSIGN #CNTRCT-TYPE := IAA-CNTRCT.CNTRCT-TYPE-CDE
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue2().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE2 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("10");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("2");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue2());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE2
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue3().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE3
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Annt_X_Ref().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                           //Natural: ASSIGN #1ST-ANNT-X-REF := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Sex().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                                   //Natural: ASSIGN #1ST-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dob_Mm().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                                 //Natural: ASSIGN #1ST-DOB-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dob_Dd().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                                 //Natural: ASSIGN #1ST-DOB-DD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DD
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dob_Yy().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy());                                                 //Natural: ASSIGN #1ST-DOB-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-YY
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dod_Mm().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Mm());                                                 //Natural: ASSIGN #1ST-DOD-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dod_Yy().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Yy());                                                 //Natural: ASSIGN #1ST-DOD-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-YY
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte());                                         //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mort_Yob3().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                                       //Natural: ASSIGN #MORT-YOB3 := #DTE4-YY
        if (condition(pnd_Passed_Data_Pnd_Trans_Payee_Cde.greater(2)))                                                                                                    //Natural: IF #TRANS-PAYEE-CDE > 02
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Life_Cnt3().setValue(0);                                                                                                 //Natural: ASSIGN #LIFE-CNT3 := 0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Life_Cnt3().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt());                                             //Natural: ASSIGN #LIFE-CNT3 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Div_Payee().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Div_Payee_Cde());                                                      //Natural: ASSIGN #DIV-PAYEE := IAA-CNTRCT.CNTRCT-DIV-PAYEE-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Coll_Cde().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Div_Coll_Cde());                                                        //Natural: ASSIGN #COLL-CDE := IAA-CNTRCT.CNTRCT-DIV-COLL-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Orig_Cntrct().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr());                                               //Natural: ASSIGN #ORIG-CNTRCT := IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cash_Cde().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde());                                               //Natural: ASSIGN #CASH-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CASH-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Emp_Term().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde());                                     //Natural: ASSIGN #EMP-TERM := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue3().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE3 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("20");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("1");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue3());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE3
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue4().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE4
        if (condition(((((ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 10 THRU 18 )
            || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(10) && ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(18)))))
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                        //Natural: ASSIGN #2ND-ANNT-X-REF := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Sex().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                                //Natural: ASSIGN #2ND-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dob_Mm().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm());                                              //Natural: ASSIGN #2ND-DOB-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dob_Dd().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                                              //Natural: ASSIGN #2ND-DOB-DD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DD
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dob_Yy().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy());                                              //Natural: ASSIGN #2ND-DOB-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-YY
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dod_Mm().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm());                                              //Natural: ASSIGN #2ND-DOD-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dod_Yy().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy());                                              //Natural: ASSIGN #2ND-DOD-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-YY
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte());                                      //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mort_Yob4().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                                   //Natural: ASSIGN #MORT-YOB4 := #DTE4-YY
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Life_Cnt4().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt());                                             //Natural: ASSIGN #LIFE-CNT4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((((ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(2) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5)) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 2 OR = 5 OR = 6 OR = 21 OR = 24 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 8 THRU 17 )
            || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(24)) || (ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(8) 
            && ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(17)))))
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Ben_Xref().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                                           //Natural: ASSIGN #BEN-XREF := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
            if (condition(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte().equals(getZero())))                                                                     //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-DOD-DTE = 0
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Ben_Dod_A().setValue(" ");                                                                                           //Natural: ASSIGN #BEN-DOD-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte());                                     //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-DOD-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Ben_Dod_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                        //Natural: ASSIGN #BEN-DOD-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Ben_Dod_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                        //Natural: ASSIGN #BEN-DOD-YY := #CCYYMM-DTE-YY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))                                                                                            //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Dest_Prev().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde());                                     //Natural: ASSIGN #DEST-PREV := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PREV-DIST-CDE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Dest_Curr().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde());                                     //Natural: ASSIGN #DEST-CURR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue4().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE4 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("20");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("2");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue4());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE4
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE5
        if (condition(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T")))                                                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T'
        {
            if (condition(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6L7") || pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6M7")        //Natural: IF SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6L7' OR SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6M7' OR SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6N7'
                || pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6N7")))
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-CREF-33
                sub_Process_Cref_33();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIAA-33
                sub_Process_Tiaa_33();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-CREF-33
            sub_Process_Cref_33();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_35() throws Exception                                                                                                                        //Natural: PROCESS-35
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                           //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND04",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902b.getPnd_Cntrct_Payee_Key(), WcType.WITH) },
        1
        );
        FIND04:
        while (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND04", true)))
        {
            ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT PARTICIPANT RECORD FOR: ",ldaIaal902b.getPnd_Cntrct_Payee_Key());                                                      //Natural: WRITE 'NO CONTRACT PARTICIPANT RECORD FOR: ' #CNTRCT-PAYEE-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BUILD-CPR-BFRE-KEY
        sub_Build_Cpr_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
        (
        "READ07",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ07:
        while (condition(ldaIaal902b.getVw_iaa_Cpr_Trans().readNextRow("READ07")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BUILD-CNTRCT-BFRE-KEY
        sub_Build_Cntrct_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
        (
        "READ08",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        READ08:
        while (condition(ldaIaal902b.getVw_iaa_Cntrct_Trans().readNextRow("READ08")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE1
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Issue_Dte());                                                        //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                                //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                                //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                                   //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                        //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                        //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                                    //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                                 //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                                 //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                                 //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
        sub_Assign_Header();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("10");                                                                                               //Natural: ASSIGN #RECORD-TYPE1 := '10'
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("1");                                                                                                 //Natural: ASSIGN #RECORD-NBR1 := '1'
        getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1());                                                                         //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
        pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                       //Natural: ASSIGN #TRANS-SW := 'Y'
    }
    private void sub_Process_50() throws Exception                                                                                                                        //Natural: PROCESS-50
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        if (condition(pnd_Passed_Data_Pnd_Save_37_Ppcn_Nbr.notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                                //Natural: IF #SAVE-37-PPCN-NBR NE #TRANS-PPCN-NBR
        {
            pnd_Passed_Data_Pnd_Save_37_Payee_Cde.reset();                                                                                                                //Natural: RESET #SAVE-37-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                           //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CNTRCT-PAYEE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND05",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902b.getPnd_Cntrct_Payee_Key(), WcType.WITH) },
        1
        );
        FIND05:
        while (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND05", true)))
        {
            ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT PARTICIPANT RECORD FOR: ",ldaIaal902b.getPnd_Cntrct_Payee_Key());                                                      //Natural: WRITE 'NO CONTRACT PARTICIPANT RECORD FOR: ' #CNTRCT-PAYEE-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BUILD-CPR-BFRE-KEY
        sub_Build_Cpr_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
        (
        "READ09",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ09:
        while (condition(ldaIaal902b.getVw_iaa_Cpr_Trans().readNextRow("READ09")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr().notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) || ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().notEquals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR NE #TRANS-PPCN-NBR OR IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE NE #TRANS-PAYEE-CDE
        {
            ldaIaal902b.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal902b.getVw_iaa_Cntrct_Prtcpnt_Role());                                                               //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BUILD-CNTRCT-BFRE-KEY
        sub_Build_Cntrct_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
        (
        "READ10",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        READ10:
        while (condition(ldaIaal902b.getVw_iaa_Cntrct_Trans().readNextRow("READ10")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                   //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR NE #TRANS-PPCN-NBR
        {
            ldaIaal902b.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal902b.getVw_iaa_Cntrct());                                                                         //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().equals(2) && pnd_Passed_Data_Pnd_Save_37_Payee_Cde.equals(1) && ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = 02 AND #SAVE-37-PAYEE-CDE = 1 AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE NE 0
        {
            ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind().setValue(" ");                                                                                    //Natural: ASSIGN IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND := ' '
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(" ");                                                                                          //Natural: ASSIGN #CROSS-REF-NBR := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde().greater(2) && (pnd_Passed_Data_Pnd_Save_37_Payee_Cde.equals(1) || pnd_Passed_Data_Pnd_Save_37_Payee_Cde.equals(2))))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE > 02 AND ( #SAVE-37-PAYEE-CDE = 1 OR = 2 )
        {
            ldaIaal902b.getIaa_Cpr_Trans_Bnfcry_Xref_Ind().setValue(" ");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.BNFCRY-XREF-IND := ' '
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(" ");                                                                                          //Natural: ASSIGN #CROSS-REF-NBR := ' '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE1
        if (condition(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T")))                                                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T'
        {
            if (condition(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6L7") || pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6M7")        //Natural: IF SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6L7' OR SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6M7' OR SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6N7'
                || pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6N7")))
            {
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-FUND-KEY
                sub_Build_Cref_Fund_Key();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FIND-CREF-FUND
                sub_Find_Cref_Fund();
                if (condition(Global.isEscape())) {return;}
                if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                   //Natural: IF #NO-RECORD-FOUND
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CREF-RATE-CHANGES
                sub_Determine_Cref_Rate_Changes();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BUILD-TIAA-FUND-KEY
                sub_Build_Tiaa_Fund_Key();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FIND-TIAA-FUND
                sub_Find_Tiaa_Fund();
                if (condition(Global.isEscape())) {return;}
                if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                   //Natural: IF #NO-RECORD-FOUND
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-TIAA-RATE-CHANGES
                sub_Determine_Tiaa_Rate_Changes();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-FUND-KEY
            sub_Build_Cref_Fund_Key();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FIND-CREF-FUND
            sub_Find_Cref_Fund();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                       //Natural: IF #NO-RECORD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CREF-RATE-CHANGES
            sub_Determine_Cref_Rate_Changes();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Mode_Ind().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind()) || ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().getBoolean())) //Natural: IF IAA-CPR-TRANS.CNTRCT-MODE-IND NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND OR #RATE-CHANGED
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mode().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                               //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Optn_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde())))                                          //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-OPTN-CDE NE IAA-CNTRCT.CNTRCT-OPTN-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Option_N().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde());                                                        //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Orgn_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Orgn_Cde())))                                          //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-ORGN-CDE NE IAA-CNTRCT.CNTRCT-ORGN-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Origin().setValueEdited(ldaIaal902b.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                           //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORIGIN
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-PYMNT-DUE-DTE NE IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                          //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                    //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                    //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-PYMNT-PD-DTE NE IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                           //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                    //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                    //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Issue_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Issue_Dte()) || ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().getBoolean())) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-ISSUE-DTE NE IAA-CNTRCT.CNTRCT-ISSUE-DTE OR #RATE-CHANGED
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Issue_Dte());                                                    //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                            //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                            //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Option_N().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde());                                                        //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte())))              //Natural: IF IAA-CPR-TRANS.CNTRCT-FINAL-PER-PAY-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                               //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                    //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                    //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Final_Pay_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte())))                      //Natural: IF IAA-CPR-TRANS.CNTRCT-FINAL-PAY-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                                //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                             //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                             //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                             //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Wthdrwl_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte())))                          //Natural: IF IAA-CPR-TRANS.CNTRCT-WTHDRWL-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                               //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                                       //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                                       //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue1().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE1 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("10");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("1");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
            ldaIaal902b.getPnd_Logical_Variables_Pnd_Issue_101_Written().setValue(true);                                                                                  //Natural: ASSIGN #ISSUE-101-WRITTEN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal902b.getPnd_Logical_Variables_Pnd_Issue_101_Written().setValue(false);                                                                                 //Natural: ASSIGN #ISSUE-101-WRITTEN := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue2().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE2
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde())))                        //Natural: IF IAA-CPR-TRANS.PRTCPNT-CTZNSHP-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Citizen().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde());                                        //Natural: ASSIGN #CITIZEN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-RSDNCY-AT-ISSUE-CDE NE IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Stfslash_Cntry_Iss().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde());                                   //Natural: ASSIGN #ST/CNTRY-ISS := IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Inst_Iss_Cde())))                                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-INST-ISS-CDE NE IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Coll_Iss().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Inst_Iss_Cde());                                                    //Natural: ASSIGN #COLL-ISS := IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T")))                                                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T'
        {
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Rtb_Amt().getValue(1).notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(1))))      //Natural: IF IAA-CPR-TRANS.CNTRCT-RTB-AMT ( 1 ) NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 1 )
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(1));                   //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Rtb_Amt().getValue(2).notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(2))))      //Natural: IF IAA-CPR-TRANS.CNTRCT-RTB-AMT ( 2 ) NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 2 )
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(2));                   //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr())))                          //Natural: IF IAA-CPR-TRANS.PRTCPNT-TAX-ID-NBR NE IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Ssn().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr());                                             //Natural: ASSIGN #SSN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind())))                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-JOINT-CNVRT-RCRD-IND NE IAA-CNTRCT.CNTRCT-JOINT-CNVRT-RCRD-IND
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Joint_Cnvrt().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind());                                         //Natural: ASSIGN #JOINT-CNVRT := IAA-CNTRCT.CNTRCT-JOINT-CNVRT-RCRD-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue2().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE2 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("10");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("2");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue2());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE2
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue3().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE3
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Annt_X_Ref().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                       //Natural: ASSIGN #1ST-ANNT-X-REF := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Sex().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                               //Natural: ASSIGN #1ST-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                                        //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dob_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                                  //Natural: ASSIGN #1ST-DOB-MM := #DTE8-MM
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dob_Dd().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                                  //Natural: ASSIGN #1ST-DOB-DD := #DTE8-DD
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Dob_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                                  //Natural: ASSIGN #1ST-DOB-YY := #DTE8-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte())))        //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE
        {
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte());                                     //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mort_Yob3().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                                   //Natural: ASSIGN #MORT-YOB3 := #DTE4-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-LFE-CNT NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Life_Cnt3().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt());                                             //Natural: ASSIGN #LIFE-CNT3 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Div_Payee_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Div_Payee_Cde())))                                //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-DIV-PAYEE-CDE NE IAA-CNTRCT.CNTRCT-DIV-PAYEE-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Div_Payee().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Div_Payee_Cde());                                                  //Natural: ASSIGN #DIV-PAYEE := IAA-CNTRCT.CNTRCT-DIV-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Div_Coll_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Div_Coll_Cde())))                                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-DIV-COLL-CDE NE IAA-CNTRCT.CNTRCT-DIV-COLL-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Coll_Cde().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Div_Coll_Cde());                                                    //Natural: ASSIGN #COLL-CDE := IAA-CNTRCT.CNTRCT-DIV-COLL-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Cash_Cde().notEquals(" ") && ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde().equals(" ")))             //Natural: IF IAA-CPR-TRANS.CNTRCT-CASH-CDE NE ' ' AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CASH-CDE = ' '
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cash_Cde().setValue("R");                                                                                                //Natural: ASSIGN #CASH-CDE := 'R'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Cash_Cde().notEquals(" ") && ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde().equals(" ")))         //Natural: IF IAA-CPR-TRANS.CNTRCT-CASH-CDE NE ' ' AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CASH-CDE = ' '
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cash_Cde().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde());                                       //Natural: ASSIGN #CASH-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CASH-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde())))            //Natural: IF IAA-CPR-TRANS.CNTRCT-EMPLYMNT-TRMNT-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
        {
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Emp_Term().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde());                                 //Natural: ASSIGN #EMP-TERM := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde())))        //Natural: IF IAA-CPR-TRANS.CNTRCT-EMPLYMNT-TRMNT-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Emp_Term().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde());                             //Natural: ASSIGN #EMP-TERM := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue3().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE3 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("20");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("1");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue3());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE3
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue4().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE4
        if (condition(((((ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 10 THRU 18 )
            || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(10) && ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(18)))))
        {
            if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind())))                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND NE IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                    //Natural: ASSIGN #2ND-ANNT-X-REF := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE NE IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Sex().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                            //Natural: ASSIGN #2ND-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE NE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
            {
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte());                                     //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dob_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                              //Natural: ASSIGN #2ND-DOB-MM := #DTE8-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dob_Dd().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                              //Natural: ASSIGN #2ND-DOB-DD := #DTE8-DD
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_2nd_Dob_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                              //Natural: ASSIGN #2ND-DOB-YY := #DTE8-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte())))      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE NE IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE
            {
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte());                                  //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mort_Yob4().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                               //Natural: ASSIGN #MORT-YOB4 := #DTE4-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt().notEquals(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt())))                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-LIFE-CNT NE IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Life_Cnt4().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt());                                         //Natural: ASSIGN #LIFE-CNT4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))                                                                                            //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Curr_Dist_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde())))                  //Natural: IF IAA-CPR-TRANS.CNTRCT-CURR-DIST-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Dest_Curr().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde());                                 //Natural: ASSIGN #DEST-CURR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Cntrct_Prev_Dist_Cde().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde())))                  //Natural: IF IAA-CPR-TRANS.CNTRCT-PREV-DIST-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PREV-DIST-CDE
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Dest_Prev().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde());                                 //Natural: ASSIGN #DEST-PREV := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PREV-DIST-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((((ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(2) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5)) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 2 OR = 5 OR = 6 OR = 21 OR = 24 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 8 THRU 17 )
            || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)) || ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(24)) || (ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(8) 
            && ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(17)))))
        {
            if (condition(ldaIaal902b.getIaa_Cpr_Trans_Bnfcry_Xref_Ind().notEquals(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind())))                            //Natural: IF IAA-CPR-TRANS.BNFCRY-XREF-IND NE IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Ben_Xref().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                                       //Natural: ASSIGN #BEN-XREF := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rest_Of_Issue4().notEquals(" ")))                                                                              //Natural: IF #REST-OF-ISSUE4 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("20");                                                                                           //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("2");                                                                                             //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue4());                                                                     //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE4
            pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #TRANS-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5().reset();                                                                                                 //Natural: RESET #MANUAL-NEW-ISSUE5
        if (condition(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T")))                                                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T'
        {
            if (condition(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6L7") || pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6M7")        //Natural: IF SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6L7' OR SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6M7' OR SUBSTR ( #TRANS-PPCN-NBR,1,3 ) = '6N7'
                || pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,3).equals("6N7")))
            {
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-FUND-KEY
                sub_Build_Cref_Fund_Key();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FIND-CREF-FUND
                sub_Find_Cref_Fund();
                if (condition(Global.isEscape())) {return;}
                if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                   //Natural: IF #NO-RECORD-FOUND
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CREF-RATE-CHANGES
                sub_Determine_Cref_Rate_Changes();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-CREF-50
                sub_Process_Cref_50();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BUILD-TIAA-FUND-KEY
                sub_Build_Tiaa_Fund_Key();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FIND-TIAA-FUND
                sub_Find_Tiaa_Fund();
                if (condition(Global.isEscape())) {return;}
                if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                   //Natural: IF #NO-RECORD-FOUND
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-TIAA-RATE-CHANGES
                sub_Determine_Tiaa_Rate_Changes();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIAA-50
                sub_Process_Tiaa_50();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-FUND-KEY
            sub_Build_Cref_Fund_Key();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FIND-CREF-FUND
            sub_Find_Cref_Fund();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                       //Natural: IF #NO-RECORD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CREF-RATE-CHANGES
            sub_Determine_Cref_Rate_Changes();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-CREF-50
            sub_Process_Cref_50();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Assign_Header() throws Exception                                                                                                                     //Natural: ASSIGN-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Batch_Nbr1().compute(new ComputeParameters(false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Batch_Nbr1()), pnd_Passed_Data_Pnd_Last_Batch_Nbr.add(1)); //Natural: COMPUTE #BATCH-NBR1 = #LAST-BATCH-NBR + 1
        pnd_Passed_Data_Pnd_Last_Batch_Nbr.setValue(ldaIaal902b.getPnd_Coding_Sheets_Pnd_Batch_Nbr1());                                                                   //Natural: ASSIGN #LAST-BATCH-NBR := #BATCH-NBR1
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Check_Dte_Mm1().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm);                                                            //Natural: ASSIGN #CHECK-DTE-MM1 := #TRANS-CHECK-DTE-MM
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Check_Dte_Dd1().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd);                                                            //Natural: ASSIGN #CHECK-DTE-DD1 := #TRANS-CHECK-DTE-DD
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Check_Dte_Yy1().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy);                                                            //Natural: ASSIGN #CHECK-DTE-YY1 := #TRANS-CHECK-DTE-YY
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cntrct_Nbr1().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8);                                                                //Natural: ASSIGN #CNTRCT-NBR1 := #TRANS-PPCN-NBR-8
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Status1().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                              //Natural: ASSIGN #RECORD-STATUS1 := #TRANS-PAYEE-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cross_Ref_Nbr1().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr());                                            //Natural: ASSIGN #CROSS-REF-NBR1 := #CROSS-REF-NBR
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_Trans_Nbr1().setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                        //Natural: ASSIGN #TRANS-NBR1 := #TRANS-CDE
        ldaIaal902b.getPnd_Coding_Sheets_Pnd_User_Area1().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                                  //Natural: ASSIGN #USER-AREA1 := #TRANS-USER-AREA
    }
    private void sub_Build_Tiaa_Fund_Key() throws Exception                                                                                                               //Natural: BUILD-TIAA-FUND-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  IF (IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD(1) = 'T')   AND
        if (condition((pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W024999") > 0 && pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W090000")  //Natural: IF ( SUBSTR ( #TRANS-PPCN-NBR,1,7 ) GT 'W024999' AND SUBSTR ( #TRANS-PPCN-NBR,1,7 ) LT 'W090000' )
            < 0)))
        {
            ldaIaal902b.getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde().setValue("T1 ");                                                                                   //Natural: ASSIGN #TIAA-FUND-KEY.#TIAA-CMPNY-FUND-CDE := 'T1 '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,2).equals("GA") || pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,2).equals("GW")))        //Natural: IF SUBSTR ( #TRANS-PPCN-NBR,1,2 ) = 'GA' OR SUBSTR ( #TRANS-PPCN-NBR,1,2 ) = 'GW'
            {
                ldaIaal902b.getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde().setValue("T1G");                                                                               //Natural: ASSIGN #TIAA-FUND-KEY.#TIAA-CMPNY-FUND-CDE := 'T1G'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902b.getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde().setValue("T1S");                                                                               //Natural: ASSIGN #TIAA-FUND-KEY.#TIAA-CMPNY-FUND-CDE := 'T1S'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Find_Tiaa_Fund() throws Exception                                                                                                                    //Natural: FIND-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().setValue(false);                                                                                       //Natural: ASSIGN #NO-RECORD-FOUND := FALSE
        ldaIaal902b.getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                         //Natural: ASSIGN #TIAA-FUND-KEY.#TIAA-CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                       //Natural: ASSIGN #TIAA-FUND-KEY.#TIAA-CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902b.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseFind                                                                                                          //Natural: FIND ( 1 ) IAA-TIAA-FUND-RCRD WITH TIAA-CNTRCT-FUND-KEY = #TIAA-FUND-KEY
        (
        "FIND06",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", "=", ldaIaal902b.getPnd_Tiaa_Fund_Key(), WcType.WITH) },
        1
        );
        FIND06:
        while (condition(ldaIaal902b.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("FIND06", true)))
        {
            ldaIaal902b.getVw_iaa_Tiaa_Fund_Rcrd().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902b.getVw_iaa_Tiaa_Fund_Rcrd().getAstCOUNTER().equals(0)))                                                                              //Natural: IF NO RECORD FOUND
            {
                ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().setValue(true);                                                                                //Natural: ASSIGN #NO-RECORD-FOUND := TRUE
                getReports().write(0, "NO TIAA FUND RECORD FOR: ",ldaIaal902b.getPnd_Tiaa_Fund_Key());                                                                    //Natural: WRITE 'NO TIAA FUND RECORD FOR: ' #TIAA-FUND-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Find_Cref_Fund() throws Exception                                                                                                                    //Natural: FIND-CREF-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().setValue(false);                                                                                       //Natural: ASSIGN #NO-RECORD-FOUND := FALSE
        ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                         //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                       //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902b.getVw_iaa_Cref_Fund_Rcrd_1().startDatabaseFind                                                                                                        //Natural: FIND ( 1 ) IAA-CREF-FUND-RCRD-1 WITH CREF-CNTRCT-FUND-KEY = #CREF-FUND-KEY
        (
        "FIND07",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", "=", ldaIaal902b.getPnd_Cref_Fund_Key(), WcType.WITH) },
        1
        );
        FIND07:
        while (condition(ldaIaal902b.getVw_iaa_Cref_Fund_Rcrd_1().readNextRow("FIND07", true)))
        {
            ldaIaal902b.getVw_iaa_Cref_Fund_Rcrd_1().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902b.getVw_iaa_Cref_Fund_Rcrd_1().getAstCOUNTER().equals(0)))                                                                            //Natural: IF NO RECORD FOUND
            {
                ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().setValue(true);                                                                                //Natural: ASSIGN #NO-RECORD-FOUND := TRUE
                getReports().write(0, "NO CREF FUND RECORD FOR: ",ldaIaal902b.getPnd_Cref_Fund_Key());                                                                    //Natural: WRITE 'NO CREF FUND RECORD FOR: ' #CREF-FUND-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Build_Cntrct_Bfre_Key() throws Exception                                                                                                             //Natural: BUILD-CNTRCT-BFRE-KEY
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                              //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                            //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
    }
    private void sub_Build_Cpr_Bfre_Key() throws Exception                                                                                                                //Natural: BUILD-CPR-BFRE-KEY
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                                 //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                          //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde());                             //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE
        ldaIaal902b.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                               //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
    }
    private void sub_Build_Cref_Fund_Key() throws Exception                                                                                                               //Natural: BUILD-CREF-FUND-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet1614 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TRANS-PPCN-NBR = MASK ( '0L' ) OR = MASK ( '0M' ) OR = MASK ( '0N' ) OR = MASK ( '0T' ) OR = MASK ( '0U' )
        if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0L'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0M'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0N'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0T'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,
            "'0U'")))
        {
            decideConditionsMet1614++;
            ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde().setValue("202");                                                                                   //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CMPNY-FUND-CDE := '202'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L3' ) OR = MASK ( '6M3' ) OR = MASK ( '6N3' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L3'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M3'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N3'")))
        {
            decideConditionsMet1614++;
            ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde().setValue("204");                                                                                   //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CMPNY-FUND-CDE := '204'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L4' ) OR = MASK ( '6M4' ) OR = MASK ( '6N4' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L4'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M4'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N4'")))
        {
            decideConditionsMet1614++;
            ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde().setValue("206");                                                                                   //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CMPNY-FUND-CDE := '206'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L5' ) OR = MASK ( '6M5' ) OR = MASK ( '6N5' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L5'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M5'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N5'")))
        {
            decideConditionsMet1614++;
            ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde().setValue("208");                                                                                   //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CMPNY-FUND-CDE := '208'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L6' ) OR = MASK ( '6M6' ) OR = MASK ( '6N6' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L6'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M6'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N6'")))
        {
            decideConditionsMet1614++;
            ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde().setValue("207");                                                                                   //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CMPNY-FUND-CDE := '207'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L7'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M7'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N7'")))
        {
            decideConditionsMet1614++;
            ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde().setValue("U09");                                                                                   //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CMPNY-FUND-CDE := 'U09'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L' ) OR = MASK ( '6M' ) OR = MASK ( '6N' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N'")))
        {
            decideConditionsMet1614++;
            ldaIaal902b.getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde().setValue("203");                                                                                   //Natural: ASSIGN #CREF-FUND-KEY.#CREF-CMPNY-FUND-CDE := '203'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Tiaa_33() throws Exception                                                                                                                   //Natural: PROCESS-TIAA-33
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
                                                                                                                                                                          //Natural: PERFORM BUILD-TIAA-FUND-KEY
        sub_Build_Tiaa_Fund_Key();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FIND-TIAA-FUND
        sub_Find_Tiaa_Fund();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                           //Natural: IF #NO-RECORD-FOUND
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-FINAL-PYMT
        sub_Determine_Final_Pymt();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                                    //Natural: ASSIGN #REC-TYPE := 50
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                            //Natural: RESET #INDX1
        FOR01:                                                                                                                                                            //Natural: FOR #INDX = 1 TO 30
        for (ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(30)); ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
        {
            if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                       //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE ' '
            {
                if (condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                                   //Natural: IF #INDX1 = 3
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                             //Natural: ADD 1 TO #REC-TYPE
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                            //Natural: ASSIGN #INDX1 := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                                //Natural: ADD 1 TO #INDX1
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rate().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX )
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Per_Pay().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-PAY := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #INDX )
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Per_Div().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-DIV := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #INDX )
                if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_Fin_Pay_Gt_0().getBoolean()))                                                                      //Natural: IF #FIN-PAY-GT-0
                {
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pay().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #FIN-PAY := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pay_A().setValue(" ");                                                                                       //Natural: ASSIGN #FIN-PAY-A := ' '
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().greater(getZero())))                                                                   //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT > 0
                {
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Div().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt());                                //Natural: ASSIGN #TOT-OLD-PER-DIV := IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Div_A().setValue(" ");                                                                               //Natural: ASSIGN #TOT-OLD-PER-DIV-A := ' '
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().greater(getZero())))                                                                   //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT > 0
                {
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt());                                //Natural: ASSIGN #TOT-OLD-PER-PAY := IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay_A().setValue(" ");                                                                               //Natural: ASSIGN #TOT-OLD-PER-PAY-A := ' '
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                sub_Assign_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1());                                               //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type());                                           //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                //*  ********************************************************************
                //*  IF #CNTRCT-NBR5 = 'IP031090' OR = 'IP031116' OR = 'IP031082'
                //*   WRITE 'IN IAAN902B LINE 4700'
                //*   WRITE '=' #CNTRCT-NBR5 '=' #BATCH-NBR5
                //*   WRITE '=' #TRANS-PPCN-NBR '=' #TRANS-CDE '=' #REC-TYPE
                //*   WRITE '=' #PER-PAY '=' #PER-DIV '=' #FIN-PAY
                //*  END-IF
                //*  ********************************************************************
                getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5());                                                                 //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                               //Natural: ASSIGN #TRANS-SW := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Cref_33() throws Exception                                                                                                                   //Natural: PROCESS-CREF-33
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                                    //Natural: ASSIGN #REC-TYPE := 50
        ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                            //Natural: RESET #INDX1
        FOR02:                                                                                                                                                            //Natural: FOR #INDX = 1 TO 5
        for (ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(5)); ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-FUND-KEY
            sub_Build_Cref_Fund_Key();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM FIND-CREF-FUND
            sub_Find_Cref_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_No_Record_Found().getBoolean()))                                                                       //Natural: IF #NO-RECORD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                     //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( #INDX ) NE ' '
            {
                if (condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                                   //Natural: IF #INDX1 = 3
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                             //Natural: ADD 1 TO #REC-TYPE
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                            //Natural: ASSIGN #INDX1 := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                                //Natural: ADD 1 TO #INDX1
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rate().setValue(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( #INDX )
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cref_Units().setValue(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #CREF-UNITS := IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( #INDX )
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                sub_Assign_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type());                                           //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1());                                               //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5());                                                                 //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                               //Natural: ASSIGN #TRANS-SW := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Tiaa_50() throws Exception                                                                                                                   //Natural: PROCESS-TIAA-50
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().getBoolean()))                                                                              //Natural: IF #RATE-CHANGED
        {
            if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_Issue_101_Written().getBoolean()))                                                                     //Natural: IF #ISSUE-101-WRITTEN
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mode().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                           //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Option_N().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde());                                                    //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Origin().setValueEdited(ldaIaal902b.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                       //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORIGIN
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Issue_Dte());                                                //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                        //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                        //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                      //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                       //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                           //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                            //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                         //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                         //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                         //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                           //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                                   //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                                   //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                sub_Assign_Header();
                if (condition(Global.isEscape())) {return;}
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("10");                                                                                       //Natural: ASSIGN #RECORD-TYPE1 := '10'
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("1");                                                                                         //Natural: ASSIGN #RECORD-NBR1 := '1'
                getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1());                                                                 //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
                pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                               //Natural: ASSIGN #TRANS-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                                //Natural: ASSIGN #REC-TYPE := 50
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                        //Natural: RESET #INDX1
            FOR03:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 30
            for (ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(30)); ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
            {
                if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                   //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE ' '
                {
                    if (condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                               //Natural: IF #INDX1 = 3
                    {
                        ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                         //Natural: ADD 1 TO #REC-TYPE
                        ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                        //Natural: ASSIGN #INDX1 := 1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                            //Natural: ADD 1 TO #INDX1
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5().reset();                                                                                     //Natural: RESET #MANUAL-NEW-ISSUE5
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rate().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX )
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Per_Pay().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-PAY := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #INDX )
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Per_Div().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-DIV := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #INDX )
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pay().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #FIN-PAY := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX )
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                    sub_Assign_Header();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type());                                       //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1());                                           //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                    getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5());                                                             //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                    pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                           //Natural: ASSIGN #TRANS-SW := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Cref_50() throws Exception                                                                                                                   //Natural: PROCESS-CREF-50
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().getBoolean()))                                                                              //Natural: IF #RATE-CHANGED
        {
            if (condition(ldaIaal902b.getPnd_Logical_Variables_Pnd_Issue_101_Written().getBoolean()))                                                                     //Natural: IF #ISSUE-101-WRITTEN
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Mode().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                           //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Option_N().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Optn_Cde());                                                    //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Origin().setValueEdited(ldaIaal902b.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                       //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORIGIN
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_Issue_Dte());                                                //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                        //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Iss_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                        //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                      //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                       //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                           //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                            //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                         //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                         //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                         //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
                ldaIaal902b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal902b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                           //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                                   //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                                   //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                sub_Assign_Header();
                if (condition(Global.isEscape())) {return;}
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type1().setValue("10");                                                                                       //Natural: ASSIGN #RECORD-TYPE1 := '10'
                ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr1().setValue("1");                                                                                         //Natural: ASSIGN #RECORD-NBR1 := '1'
                getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue1());                                                                 //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
                pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                               //Natural: ASSIGN #TRANS-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                                //Natural: ASSIGN #REC-TYPE := 50
            ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                        //Natural: RESET #INDX1
            FOR04:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 5
            for (ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(5)); ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
            {
                if (condition(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                 //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( #INDX ) NE ' '
                {
                    if (condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                               //Natural: IF #INDX1 = 3
                    {
                        ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                         //Natural: ADD 1 TO #REC-TYPE
                        ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                        //Natural: ASSIGN #INDX1 := 1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                            //Natural: ADD 1 TO #INDX1
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5().reset();                                                                                     //Natural: RESET #MANUAL-NEW-ISSUE5
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Rate().setValue(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( #INDX )
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Cref_Units().setValue(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #CREF-UNITS := IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( #INDX )
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                    sub_Assign_Header();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Type5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Rec_Type());                                       //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                    ldaIaal902b.getPnd_Coding_Sheets_Pnd_Record_Nbr5().setValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx1());                                           //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                    getWorkFiles().write(1, false, ldaIaal902b.getPnd_Coding_Sheets_Pnd_Manual_New_Issue5());                                                             //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                    pnd_Passed_Data_Pnd_Trans_Sw.setValue("Y");                                                                                                           //Natural: ASSIGN #TRANS-SW := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Tiaa_Rate_Changes() throws Exception                                                                                                       //Natural: DETERMINE-TIAA-RATE-CHANGES
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                           //Natural: ASSIGN #TIAA-FUND-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902b.getPnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #TIAA-FUND-BFRE-KEY.#TIAA-CNTRCT-PPCN-NBR := #PASSED-DATA.#TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                  //Natural: ASSIGN #TIAA-FUND-BFRE-KEY.#TIAA-CNTRCT-PAYEE-CDE := #PASSED-DATA.#TRANS-PAYEE-CDE
        ldaIaal902b.getPnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Fund_Cde().setValue(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().getSubstring(1,2));                    //Natural: ASSIGN #TIAA-FUND-BFRE-KEY.#TIAA-FUND-CDE := SUBSTR ( IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE,1,2 )
        ldaIaal902b.getPnd_Tiaa_Fund_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                         //Natural: ASSIGN #TIAA-FUND-BFRE-KEY.#TRANS-DTE := #PASSED-DATA.#CNTRL-FRST-TRANS-DTE
        ldaIaal902b.getVw_iaa_Tiaa_Fund_Trans().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY STARTING FROM #TIAA-FUND-BFRE-KEY
        (
        "READ11",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY", ">=", ldaIaal902b.getPnd_Tiaa_Fund_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY", "ASC") },
        1
        );
        READ11:
        while (condition(ldaIaal902b.getVw_iaa_Tiaa_Fund_Trans().readNextRow("READ11")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().reset();                                                                                                  //Natural: RESET #RATE-CHANGED
        if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && ldaIaal902b.getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde().equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = #PASSED-DATA.#TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = #PASSED-DATA.#TRANS-PAYEE-CDE
        {
            FOR05:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 30
            for (ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(30)); ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
            {
                if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                   //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE ' '
                {
                    short decideConditionsMet1856 = 0;                                                                                                                    //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-RATE-CDE ( #INDX )
                    if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal902b.getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()))))
                    {
                        decideConditionsMet1856++;
                        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                           //Natural: ASSIGN #RATE-CHANGED := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-PER-PAY-AMT ( #INDX )
                    if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal902b.getIaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()))))
                    {
                        decideConditionsMet1856++;
                        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                           //Natural: ASSIGN #RATE-CHANGED := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-PER-DIV-AMT ( #INDX )
                    if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal902b.getIaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()))))
                    {
                        decideConditionsMet1856++;
                        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                           //Natural: ASSIGN #RATE-CHANGED := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( #INDX )
                    if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal902b.getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()))))
                    {
                        decideConditionsMet1856++;
                        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                           //Natural: ASSIGN #RATE-CHANGED := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet1856 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Cref_Rate_Changes() throws Exception                                                                                                       //Natural: DETERMINE-CREF-RATE-CHANGES
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902b.getPnd_Cref_Fund_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                           //Natural: ASSIGN #CREF-FUND-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902b.getPnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CREF-FUND-BFRE-KEY.#CREF-CNTRCT-PPCN-NBR := #PASSED-DATA.#TRANS-PPCN-NBR
        ldaIaal902b.getPnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                  //Natural: ASSIGN #CREF-FUND-BFRE-KEY.#CREF-CNTRCT-PAYEE-CDE := #PASSED-DATA.#TRANS-PAYEE-CDE
        ldaIaal902b.getPnd_Cref_Fund_Bfre_Key_Pnd_Cref_Fund_Cde().setValue(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde().getSubstring(1,2));                  //Natural: ASSIGN #CREF-FUND-BFRE-KEY.#CREF-FUND-CDE := SUBSTR ( IAA-CREF-FUND-RCRD-1.CREF-CMPNY-FUND-CDE,1,2 )
        ldaIaal902b.getPnd_Cref_Fund_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                         //Natural: ASSIGN #CREF-FUND-BFRE-KEY.#TRANS-DTE := #PASSED-DATA.#CNTRL-FRST-TRANS-DTE
        ldaIaal902b.getVw_iaa_Cref_Fund_Trans_1().startDatabaseRead                                                                                                       //Natural: READ ( 1 ) IAA-CREF-FUND-TRANS-1 BY CREF-FUND-BFRE-KEY STARTING FROM #CREF-FUND-BFRE-KEY
        (
        "READ12",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY", ">=", ldaIaal902b.getPnd_Cref_Fund_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY", "ASC") },
        1
        );
        READ12:
        while (condition(ldaIaal902b.getVw_iaa_Cref_Fund_Trans_1().readNextRow("READ12")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().reset();                                                                                                  //Natural: RESET #RATE-CHANGED
        if (condition(ldaIaal902b.getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && ldaIaal902b.getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde().equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-CREF-FUND-TRANS-1.CREF-CNTRCT-PPCN-NBR = #PASSED-DATA.#TRANS-PPCN-NBR AND IAA-CREF-FUND-TRANS-1.CREF-CNTRCT-PAYEE-CDE = #PASSED-DATA.#TRANS-PAYEE-CDE
        {
            FOR06:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 5
            for (ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(5)); ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
            {
                if (condition(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                 //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( #INDX ) NE ' '
                {
                    short decideConditionsMet1893 = 0;                                                                                                                    //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( #INDX ) NE IAA-CREF-FUND-TRANS-1.CREF-RATE-CDE ( #INDX )
                    if (condition(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal902b.getIaa_Cref_Fund_Trans_1_Cref_Rate_Cde().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()))))
                    {
                        decideConditionsMet1893++;
                        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                           //Natural: ASSIGN #RATE-CHANGED := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: WHEN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( #INDX ) NE IAA-CREF-FUND-TRANS-1.CREF-UNITS-CNT ( #INDX )
                    if (condition(ldaIaal902b.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal902b.getIaa_Cref_Fund_Trans_1_Cref_Units_Cnt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()))))
                    {
                        decideConditionsMet1893++;
                        ldaIaal902b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                           //Natural: ASSIGN #RATE-CHANGED := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet1893 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Final_Pymt() throws Exception                                                                                                              //Natural: DETERMINE-FINAL-PYMT
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #INDX = 1 TO 30
        for (ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(30)); ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
        {
            if (condition(ldaIaal902b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal902b.getPnd_Misc_Variables_Pnd_Indx()).greater(getZero())))         //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX ) > 0
            {
                ldaIaal902b.getPnd_Logical_Variables_Pnd_Fin_Pay_Gt_0().setValue(true);                                                                                   //Natural: ASSIGN #FIN-PAY-GT-0 := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902b.getPnd_Logical_Variables_Pnd_Fin_Pay_Gt_0().setValue(false);                                                                                  //Natural: ASSIGN #FIN-PAY-GT-0 := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Prod_Code() throws Exception                                                                                                               //Natural: DETERMINE-PROD-CODE
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1923 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #TRANS-PPCN-NBR,1,7 ) GT 'W024999' AND SUBSTR ( #TRANS-PPCN-NBR,1,7 ) LT 'W090000'
        if (condition(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W024999") > 0 && pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W090000") 
            < 0))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("G");                                                                                                 //Natural: ASSIGN #PRODUCT := 'G'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '0L' ) OR = MASK ( '0M' ) OR = MASK ( '0N' ) OR = MASK ( '0T' ) OR = MASK ( '0U' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0L'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0M'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0N'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0T'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,
            "'0U'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("C");                                                                                                 //Natural: ASSIGN #PRODUCT := 'C'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L3' ) OR = MASK ( '6M3' ) OR = MASK ( '6N3' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L3'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M3'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N3'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("S");                                                                                                 //Natural: ASSIGN #PRODUCT := 'S'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L4' ) OR = MASK ( '6M4' ) OR = MASK ( '6N4' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L4'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M4'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N4'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("W");                                                                                                 //Natural: ASSIGN #PRODUCT := 'W'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L5' ) OR = MASK ( '6M5' ) OR = MASK ( '6N5' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L5'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M5'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N5'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("L");                                                                                                 //Natural: ASSIGN #PRODUCT := 'L'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L6' ) OR = MASK ( '6M6' ) OR = MASK ( '6N6' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L6'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M6'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N6'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("E");                                                                                                 //Natural: ASSIGN #PRODUCT := 'E'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L7'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M7'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N7'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("R");                                                                                                 //Natural: ASSIGN #PRODUCT := 'R'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L' ) OR = MASK ( '6M' ) OR = MASK ( '6N' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("M");                                                                                                 //Natural: ASSIGN #PRODUCT := 'M'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( 'GA' ) OR = MASK ( 'GW' ) OR = MASK ( 'IA' ) OR = MASK ( 'IB' ) OR = MASK ( 'IC' ) OR = MASK ( 'ID' ) OR = MASK ( 'IE' ) OR = MASK ( 'IF' ) OR = MASK ( 'S0' ) OR = MASK ( 'W0' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'GA'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'GW'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IA'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IB'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IC'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'ID'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IE'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IF'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'S0'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'W0'")))
        {
            decideConditionsMet1923++;
            ldaIaal902b.getPnd_Coding_Sheets_Pnd_Product().setValue("T");                                                                                                 //Natural: ASSIGN #PRODUCT := 'T'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
