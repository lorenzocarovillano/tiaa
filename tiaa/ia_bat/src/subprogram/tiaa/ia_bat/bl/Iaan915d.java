/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:22 AM
**        * FROM NATURAL SUBPROGRAM : Iaan915d
************************************************************
**        * FILE NAME            : Iaan915d.java
**        * CLASS NAME           : Iaan915d
**        * INSTANCE NAME        : Iaan915d
************************************************************
***********************************************************************
*                                                                     *
*   PROGRAM     -  IAAN912D CREATES COMBINE CHECK TRANS SELECTION RCRD*
*      DATE     -  8/94                                               *
*  04/2017 OS RE-STOWED FOR IAAL915D PIN EXPANSION.                   *
***********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan915d extends BLNatBase
{
    // Data Areas
    private LdaIaal915d ldaIaal915d;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Trans_Seq_Nbr;
    private DbsField pnd_Ctr;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal915d = new LdaIaal915d();
        registerRecord(ldaIaal915d);
        registerRecord(ldaIaal915d.getVw_iaa_Cntrct());
        registerRecord(ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal915d.getVw_iaa_Cntrct_Trans());

        // parameters
        parameters = new DbsRecord();
        pnd_Trans_Seq_Nbr = parameters.newFieldInRecord("pnd_Trans_Seq_Nbr", "#TRANS-SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Trans_Seq_Nbr.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal915d.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    public Iaan915d() throws Exception
    {
        super("Iaan915d");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        READWORK01:                                                                                                                                                       //Natural: READ WORK 2 WS-TRANS-304-RCRD
        while (condition(getWorkFiles().read(2, ldaIaal915d.getWs_Trans_304_Rcrd())))
        {
            getSort().writeSortInData(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Area(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde(),                     //Natural: END-ALL
                ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte(), 
                ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Dte(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Lst_Trans_Dte(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Sub_Cde(), 
                ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cde(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte(), 
                ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Id());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Area(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(),  //Natural: SORT THEM BY WS-TRANS-304-RCRD.#TRANS-USER-AREA WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE WS-TRANS-304-RCRD.#TRANS-PPCN-NBR WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE USING WS-TRANS-304-RCRD.#CNTRL-FRST-TRANS-DTE WS-TRANS-304-RCRD.#TRANS-DTE WS-TRANS-304-RCRD.#LST-TRANS-DTE WS-TRANS-304-RCRD.#TRANS-SUB-CDE WS-TRANS-304-RCRD.#TRANS-CDE WS-TRANS-304-RCRD.#TRANS-ACTVTY-CDE WS-TRANS-304-RCRD.#TRANS-CHECK-DTE WS-TRANS-304-RCRD.#TRANS-EFFCTVE-DTE WS-TRANS-304-RCRD.#TRANS-USER-ID
            ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());
        SORT01:
        while (condition(getSort().readSortOutData(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Area(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde(), 
            ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte(), 
            ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Dte(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Lst_Trans_Dte(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Sub_Cde(), 
            ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cde(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte(), 
            ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte(), ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Id())))
        {
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            //*  WRITE '=' #CTR
            if (condition((ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr().equals(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn())) && (ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().equals(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee())))) //Natural: IF ( WS-TRANS-304-RCRD.#TRANS-PPCN-NBR = WS-TRANS-304-RCRD.#TRANS-CC-PPCN ) AND ( WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE = WS-TRANS-304-RCRD.#TRANS-CC-PAYEE )
            {
                if (condition(ldaIaal915d.getPnd_Logical_Variables_Pnd_Trans_Sw().getBoolean()))                                                                          //Natural: IF #TRANS-SW
                {
                    ldaIaal915d.getPnd_Logical_Variables_Pnd_Rcrd_In_Progress().setValue(true);                                                                           //Natural: ASSIGN #RCRD-IN-PROGRESS := TRUE
                    ldaIaal915d.getPnd_Logical_Variables_Pnd_Trans_Sw().reset();                                                                                          //Natural: RESET #TRANS-SW
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                     //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := WS-TRANS-304-RCRD.#TRANS-PPCN-NBR
                ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());                   //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE
                ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
                (
                "FIND01",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
                1
                );
                FIND01:
                while (condition(ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND01", true)))
                {
                    ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        getReports().write(0, "NO CPR RECORD FOR :",ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                                         //Natural: WRITE 'NO CPR RECORD FOR :' #IAA-CNTRCT-PRTCPNT-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-NOREC
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaIaal915d.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde().equals(" ")))                                                                     //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CMBNE-CDE = ' '
                {
                    ldaIaal915d.getPnd_Comb_Chk_Trans().reset();                                                                                                          //Natural: RESET #COMB-CHK-TRANS
                    ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#CNTRCT-NBR := #TRANS-PPCN-NBR
                    ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde(),new ReportEditMask("99"));  //Natural: MOVE EDITED #TRANS-PAYEE-CDE ( EM = 99 ) TO #COMB-CHK-TRANS.#RCRD-STATUS
                    ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Intent_Code().setValue("3");                                                                                    //Natural: ASSIGN #COMB-CHK-TRANS.#INTENT-CODE := '3'
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                    sub_Assign_Header();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                         //Natural: ASSIGN #RCRD-NBR := 1
                    getWorkFiles().write(1, false, ldaIaal915d.getPnd_Comb_Chk_Trans());                                                                                  //Natural: WRITE WORK FILE 1 #COMB-CHK-TRANS
                    ldaIaal915d.getPnd_Comb_Chk_Trans().reset();                                                                                                          //Natural: RESET #COMB-CHK-TRANS #TRANS-SW
                    ldaIaal915d.getPnd_Logical_Variables_Pnd_Trans_Sw().reset();
                    ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().setValue(1);                                                                               //Natural: ASSIGN #CMBNE-CTR := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal915d.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde().notEquals(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde())))           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CMBNE-CDE NE WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE
                    {
                        if (condition(ldaIaal915d.getPnd_Logical_Variables_Pnd_Rcrd_In_Progress().getBoolean()))                                                          //Natural: IF #RCRD-IN-PROGRESS
                        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                            sub_Assign_Header();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getWorkFiles().write(1, false, ldaIaal915d.getPnd_Comb_Chk_Trans());                                                                          //Natural: WRITE WORK FILE 1 #COMB-CHK-TRANS
                            ldaIaal915d.getPnd_Logical_Variables_Pnd_Trans_Sw().reset();                                                                                  //Natural: RESET #TRANS-SW #COMB-CHK-TRANS #RCRD-IN-PROGRESS
                            ldaIaal915d.getPnd_Comb_Chk_Trans().reset();
                            ldaIaal915d.getPnd_Logical_Variables_Pnd_Rcrd_In_Progress().reset();
                            ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().setValue(1);                                                                       //Natural: ASSIGN #CMBNE-CTR := 1
                            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
                        }                                                                                                                                                 //Natural: END-IF
                        ldaIaal915d.getPnd_Logical_Variables_Pnd_Trans_Sw().reset();                                                                                      //Natural: RESET #TRANS-SW
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal915d.getPnd_Comb_Chk_Trans().reset();                                                                                                      //Natural: RESET #COMB-CHK-TRANS
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                        sub_Assign_Header();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaal915d.getPnd_Logical_Variables_Pnd_Trans_Sw().setValue(true);                                                                               //Natural: ASSIGN #TRANS-SW := TRUE
                        ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().setValue(1);                                                                           //Natural: ASSIGN #CMBNE-CTR := 1
                                                                                                                                                                          //Natural: PERFORM DETERMINE-RCRD-NBR
                        sub_Determine_Rcrd_Nbr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (ldaIaal915d.getPnd_Logical_Variables_Pnd_Trans_Sw().getBoolean())))                                                                      //Natural: IF NOT #TRANS-SW
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                 //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := WS-TRANS-304-RCRD.#TRANS-PPCN-NBR
                    ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());               //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE
                    ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                         //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
                    (
                    "FIND02",
                    new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
                    1
                    );
                    FIND02:
                    while (condition(ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
                    {
                        ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                        if (condition(ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                             //Natural: IF NO RECORDS FOUND
                        {
                            getReports().write(0, "NO CPR RECORD FOR :",ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                                     //Natural: WRITE 'NO CPR RECORD FOR :' #IAA-CNTRCT-PRTCPNT-KEY
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-NOREC
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(ldaIaal915d.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde().notEquals(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde())))           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CMBNE-CDE NE WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-RCRD-NBR
                        sub_Determine_Rcrd_Nbr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaal915d.getPnd_Logical_Variables_Pnd_Rcrd_In_Progress().setValue(true);                                                                       //Natural: ASSIGN #RCRD-IN-PROGRESS := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-RCRD-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-HEADER
    }
    private void sub_Find_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: FIND-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915d.getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                          //Natural: ASSIGN #IAA-CNTRCT-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915d.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr(), WcType.WITH) },
        1
        );
        FIND03:
        while (condition(ldaIaal915d.getVw_iaa_Cntrct().readNextRow("FIND03", true)))
        {
            ldaIaal915d.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal915d.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA CONTRACT RECORD FOR : ",ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                             //Natural: WRITE 'NO IAA CONTRACT RECORD FOR : ' #TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal915d.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                  //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cross_Reference_Nbr() throws Exception                                                                                                           //Natural: GET-CROSS-REFERENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet306 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().equals(1))))
        {
            decideConditionsMet306++;
            if (condition(ldaIaal915d.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := CNTRCT-FIRST-ANNT-DOB-MM
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := CNTRCT-FIRST-ANNT-DOB-DD
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := CNTRCT-FIRST-ANNT-DOB-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().equals(2))))
        {
            decideConditionsMet306++;
            if (condition(ldaIaal915d.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := CNTRCT-FIRST-ANNT-DOB-MM
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := CNTRCT-FIRST-ANNT-DOB-DD
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := CNTRCT-FIRST-ANNT-DOB-DD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := CNTRCT-FIRST-ANNT-DOB-MM
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := CNTRCT-FIRST-ANNT-DOB-DD
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := CNTRCT-FIRST-ANNT-DOB-DD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := CNTRCT-FIRST-ANNT-DOB-MM
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := CNTRCT-FIRST-ANNT-DOB-DD
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                       //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := CNTRCT-FIRST-ANNT-DOB-DD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal915d.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                    //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := CNTRCT-SCND-ANNT-XREF-IND
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                          //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm());                                        //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-MM := CNTRCT-SCND-ANNT-DOB-MM
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                                        //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-DD := CNTRCT-SCND-ANNT-DOB-DD
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                                        //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-YY := CNTRCT-SCND-ANNT-DOB-DD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().greaterOrEqual(3) && ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().lessOrEqual(99)))))
        {
            decideConditionsMet306++;
            ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                         //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde());                       //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
            (
            "FIND04",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
            1
            );
            FIND04:
            while (condition(ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND04", true)))
            {
                ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal915d.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO CPR TRANS RECORD FOR ",ldaIaal915d.getPnd_Iaa_Cntrct_Prtcpnt_Key());                                                        //Natural: WRITE 'NO CPR TRANS RECORD FOR ' #IAA-CNTRCT-PRTCPNT-KEY
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Ssn().setValue(ldaIaal915d.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr());                                        //Natural: ASSIGN #COMB-CHK-TRANS.#SSN := PRTCPNT-TAX-ID-NBR
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915d.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                                 //Natural: ASSIGN #COMB-CHK-TRANS.#CROSS-REF-NBR := BNFCRY-XREF-IND
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_A().setValue(" ");                                                                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-SEX-A := ' '
                ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_A().setValue(" ");                                                                                         //Natural: ASSIGN #COMB-CHK-TRANS.#ANNT-DOB-A := ' '
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Rcrd_Nbr() throws Exception                                                                                                                //Natural: DETERMINE-RCRD-NBR
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet373 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #CMBNE-CTR;//Natural: VALUE 1
        if (condition((ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(1))))
        {
            decideConditionsMet373++;
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn());                                            //Natural: ASSIGN #CNTRCT-NBR := #TRANS-CC-PPCN
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #RCRD-STATUS
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(2))))
        {
            decideConditionsMet373++;
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #2ND-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #2ND-STA-NBR
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(3))))
        {
            decideConditionsMet373++;
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #3RD-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #3RD-STA-NBR
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(4))))
        {
            decideConditionsMet373++;
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(1);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 1
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #4TH-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #4TH-STA-NBR
            getWorkFiles().write(1, false, ldaIaal915d.getPnd_Comb_Chk_Trans());                                                                                          //Natural: WRITE WORK FILE 1 #COMB-CHK-TRANS
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(5))))
        {
            decideConditionsMet373++;
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(2);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 2
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn());                                            //Natural: ASSIGN #CNTRCT-NBR := #TRANS-CC-PPCN
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #RCRD-STATUS
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #5TH-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #5TH-STA-NBR
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().equals(6))))
        {
            decideConditionsMet373++;
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr().setValue(2);                                                                                                 //Natural: ASSIGN #RCRD-NBR := 2
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #6TH-COMB-CNTRCT := #TRANS-PPCN-NBR
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee(),new ReportEditMask("99"));           //Natural: MOVE EDITED #TRANS-CC-PAYEE ( EM = 99 ) TO #6TH-STA-NBR
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct().setValue(" ");                                                                                        //Natural: ASSIGN #4TH-COMB-CNTRCT := ' '
            ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr().setValue(" ");                                                                                            //Natural: ASSIGN #4TH-STA-NBR := ' '
            getWorkFiles().write(1, false, ldaIaal915d.getPnd_Comb_Chk_Trans());                                                                                          //Natural: WRITE WORK FILE 1 #COMB-CHK-TRANS
            ldaIaal915d.getPnd_Comb_Chk_Trans().reset();                                                                                                                  //Natural: RESET #COMB-CHK-TRANS
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Intent_Code().setValue("1");                                                                                                //Natural: ASSIGN #COMB-CHK-TRANS.#INTENT-CODE := '1'
        ldaIaal915d.getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr().nadd(1);                                                                                               //Natural: ADD 1 TO #CMBNE-CTR
    }
    private void sub_Assign_Header() throws Exception                                                                                                                     //Natural: ASSIGN-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Mm());                                         //Natural: ASSIGN #CHECK-DTE-MM := #TRANS-CHECK-DTE-MM
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Dd());                                         //Natural: ASSIGN #CHECK-DTE-DD := #TRANS-CHECK-DTE-DD
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Yy());                                         //Natural: ASSIGN #CHECK-DTE-YY := #TRANS-CHECK-DTE-YY
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT
        sub_Find_Iaa_Cntrct();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR
        sub_Get_Cross_Reference_Nbr();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Trans_Nbr().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Cde());                                                     //Natural: ASSIGN #COMB-CHK-TRANS.#TRANS-NBR := #TRANS-CDE
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Currency().setValue(ldaIaal915d.getIaa_Cntrct_Cntrct_Crrncy_Cde());                                                         //Natural: ASSIGN #COMB-CHK-TRANS.#CURRENCY := CNTRCT-CRRNCY-CDE
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_User_Area().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Area());                                               //Natural: ASSIGN #USER-AREA := #TRANS-USER-AREA
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_User_Id().setValue(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_User_Id());                                                   //Natural: ASSIGN #USER-ID := #TRANS-USER-ID
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Seq_Nbr().setValue(pnd_Trans_Seq_Nbr);                                                                                      //Natural: ASSIGN #SEQ-NBR := #TRANS-SEQ-NBR
        ldaIaal915d.getPnd_Comb_Chk_Trans_Pnd_Trans_Dte().setValueEdited(ldaIaal915d.getWs_Trans_304_Rcrd_Pnd_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED WS-TRANS-304-RCRD.#TRANS-DTE ( EM = YYYYMMDD ) TO #COMB-CHK-TRANS.#TRANS-DTE
    }

    //
}
