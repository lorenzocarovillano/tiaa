/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:33 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420g
************************************************************
**        * FILE NAME            : Iatn420g.java
**        * CLASS NAME           : Iatn420g
**        * INSTANCE NAME        : Iatn420g
************************************************************
************************************************************************
*  PROGRAM: IATN420G
*   AUTHOR: ARI GROSSMAN
*     DATE: FEB 08, 1996
*  PURPOSE: UPDATE AND ADD FUND RECS FOR CREF TO CREF TRANSFER
*         : UPDATE CPR AND STORE AFTER IMAGES FOR EVERYTHING
*  HISTORY: 01/16/97 : ADDED MULTI FUND FUNCTIONALITY/ CONTRACT TRANS
*           01/12/98 : TRANSFER PROCESSING - 1998 (FORMERLY IATN170G)
*                      MANY TO MANY PROCESSING/ MONTHLY AND ANNUAL FUND
*                      PROCESSING
*           01/16/09 OS TIAA ACCESS CHANGES. SC 011609.
*           04/12/12 JT RATE BASE EXPANSION. SC 041212.
*           05/11/12 OS ADDED CHANGES TO STORE CREF-CMPNY-FUND-CDE
*                       IN CREF-OLD-CMPNY-FUND FOR SWITCHES. SC 051112.
*           04/2017  OS RE-STOWED FRO IAAL999 PIN EXPANSION.
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420g extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Iaa_From_Cntrct;
    private DbsField pnd_Iaa_From_Pyee_N;
    private DbsField pnd_From_Fund;
    private DbsField pnd_From_Acct_Code;
    private DbsField pnd_Frm_Unit_Typ;
    private DbsField pnd_From_Aftr_Xfr_Units;
    private DbsField pnd_From_Aftr_Xfr_Guar;
    private DbsField pnd_From_Reval_Unit_Val;
    private DbsField pnd_Rate_Code_Table;
    private DbsField pnd_To_Fund;
    private DbsField pnd_To_Acct_Code;
    private DbsField pnd_To_Unit_Typ;
    private DbsField pnd_To_Aftr_Xfr_Units;
    private DbsField pnd_To_Aftr_Xfr_Guar;
    private DbsField pnd_To_Reval_Unit_Val;
    private DbsField pnd_From_Fund_Only;
    private DbsField pnd_Rcrd_Type_Cde;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Time;
    private DbsField pnd_Eff_Dte_03_31;
    private DbsField pnd_Return_Code;
    private DbsField pnd_On_File_Already;
    private DbsField pnd_Rate_Code_Breakdown;

    private DbsGroup pnd_Rate_Code_Breakdown__R_Field_1;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2;
    private DbsField pnd_From_Fund_H;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_2;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_3;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_File_Mode;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_To_Cntrct_Fund;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Z;
    private DbsField pnd_Max_Prods;
    private DbsField pnd_One_Byte_Fund;
    private DbsField pnd_Two_Byte_Fund;
    private DbsField pnd_Fund_Cd_1;
    private DbsField pnd_Partial_Transfer;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();
        pnd_Iaa_From_Cntrct = parameters.newFieldInRecord("pnd_Iaa_From_Cntrct", "#IAA-FROM-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_From_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_From_Pyee_N = parameters.newFieldInRecord("pnd_Iaa_From_Pyee_N", "#IAA-FROM-PYEE-N", FieldType.NUMERIC, 2);
        pnd_Iaa_From_Pyee_N.setParameterOption(ParameterOption.ByReference);
        pnd_From_Fund = parameters.newFieldArrayInRecord("pnd_From_Fund", "#FROM-FUND", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_From_Fund.setParameterOption(ParameterOption.ByReference);
        pnd_From_Acct_Code = parameters.newFieldArrayInRecord("pnd_From_Acct_Code", "#FROM-ACCT-CODE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_From_Acct_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Frm_Unit_Typ = parameters.newFieldArrayInRecord("pnd_Frm_Unit_Typ", "#FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_Frm_Unit_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_From_Aftr_Xfr_Units = parameters.newFieldArrayInRecord("pnd_From_Aftr_Xfr_Units", "#FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new 
            DbsArrayController(1, 20));
        pnd_From_Aftr_Xfr_Units.setParameterOption(ParameterOption.ByReference);
        pnd_From_Aftr_Xfr_Guar = parameters.newFieldArrayInRecord("pnd_From_Aftr_Xfr_Guar", "#FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 20));
        pnd_From_Aftr_Xfr_Guar.setParameterOption(ParameterOption.ByReference);
        pnd_From_Reval_Unit_Val = parameters.newFieldArrayInRecord("pnd_From_Reval_Unit_Val", "#FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new 
            DbsArrayController(1, 20));
        pnd_From_Reval_Unit_Val.setParameterOption(ParameterOption.ByReference);
        pnd_Rate_Code_Table = parameters.newFieldArrayInRecord("pnd_Rate_Code_Table", "#RATE-CODE-TABLE", FieldType.STRING, 3, new DbsArrayController(1, 
            80));
        pnd_Rate_Code_Table.setParameterOption(ParameterOption.ByReference);
        pnd_To_Fund = parameters.newFieldArrayInRecord("pnd_To_Fund", "#TO-FUND", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_To_Fund.setParameterOption(ParameterOption.ByReference);
        pnd_To_Acct_Code = parameters.newFieldArrayInRecord("pnd_To_Acct_Code", "#TO-ACCT-CODE", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_To_Acct_Code.setParameterOption(ParameterOption.ByReference);
        pnd_To_Unit_Typ = parameters.newFieldArrayInRecord("pnd_To_Unit_Typ", "#TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_To_Unit_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_To_Aftr_Xfr_Units = parameters.newFieldArrayInRecord("pnd_To_Aftr_Xfr_Units", "#TO-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 
            20));
        pnd_To_Aftr_Xfr_Units.setParameterOption(ParameterOption.ByReference);
        pnd_To_Aftr_Xfr_Guar = parameters.newFieldArrayInRecord("pnd_To_Aftr_Xfr_Guar", "#TO-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_To_Aftr_Xfr_Guar.setParameterOption(ParameterOption.ByReference);
        pnd_To_Reval_Unit_Val = parameters.newFieldArrayInRecord("pnd_To_Reval_Unit_Val", "#TO-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 
            20));
        pnd_To_Reval_Unit_Val.setParameterOption(ParameterOption.ByReference);
        pnd_From_Fund_Only = parameters.newFieldArrayInRecord("pnd_From_Fund_Only", "#FROM-FUND-ONLY", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_From_Fund_Only.setParameterOption(ParameterOption.ByReference);
        pnd_Rcrd_Type_Cde = parameters.newFieldInRecord("pnd_Rcrd_Type_Cde", "#RCRD-TYPE-CDE", FieldType.STRING, 1);
        pnd_Rcrd_Type_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Date = parameters.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Check_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Todays_Dte = parameters.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.DATE);
        pnd_Todays_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Time = parameters.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Time.setParameterOption(ParameterOption.ByReference);
        pnd_Eff_Dte_03_31 = parameters.newFieldInRecord("pnd_Eff_Dte_03_31", "#EFF-DTE-03-31", FieldType.STRING, 1);
        pnd_Eff_Dte_03_31.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_On_File_Already = localVariables.newFieldInRecord("pnd_On_File_Already", "#ON-FILE-ALREADY", FieldType.STRING, 1);
        pnd_Rate_Code_Breakdown = localVariables.newFieldInRecord("pnd_Rate_Code_Breakdown", "#RATE-CODE-BREAKDOWN", FieldType.STRING, 3);

        pnd_Rate_Code_Breakdown__R_Field_1 = localVariables.newGroupInRecord("pnd_Rate_Code_Breakdown__R_Field_1", "REDEFINE", pnd_Rate_Code_Breakdown);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1 = pnd_Rate_Code_Breakdown__R_Field_1.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1", "#RATE-CODE-1", 
            FieldType.STRING, 1);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2 = pnd_Rate_Code_Breakdown__R_Field_1.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2", "#RATE-CODE-2", 
            FieldType.STRING, 2);
        pnd_From_Fund_H = localVariables.newFieldInRecord("pnd_From_Fund_H", "#FROM-FUND-H", FieldType.STRING, 1);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_To_Cntrct_Fund = localVariables.newFieldInRecord("pnd_To_Cntrct_Fund", "#TO-CNTRCT-FUND", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Prods = localVariables.newFieldInRecord("pnd_Max_Prods", "#MAX-PRODS", FieldType.PACKED_DECIMAL, 3);
        pnd_One_Byte_Fund = localVariables.newFieldInRecord("pnd_One_Byte_Fund", "#ONE-BYTE-FUND", FieldType.STRING, 1);
        pnd_Two_Byte_Fund = localVariables.newFieldInRecord("pnd_Two_Byte_Fund", "#TWO-BYTE-FUND", FieldType.STRING, 2);
        pnd_Fund_Cd_1 = localVariables.newFieldInRecord("pnd_Fund_Cd_1", "#FUND-CD-1", FieldType.STRING, 1);
        pnd_Partial_Transfer = localVariables.newFieldInRecord("pnd_Partial_Transfer", "#PARTIAL-TRANSFER", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal999.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Prods.setInitialValue(80);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420g() throws Exception
    {
        super("Iatn420g");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420G", onError);
        //* *********
        getReports().write(0, "IATN420G");                                                                                                                                //Natural: WRITE 'IATN420G'
        if (Global.isEscape()) return;
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        pnd_Return_Code.setValue("T1");                                                                                                                                   //Natural: ON ERROR;//Natural: ASSIGN #RETURN-CODE = 'T1'
                                                                                                                                                                          //Natural: PERFORM #AI-CONTRACTS
        sub_Pnd_Ai_Contracts();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                         //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("M2");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'M2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                              //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CPR
        sub_Pnd_Update_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                         //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("T2");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                              //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #AI-CPR
        sub_Pnd_Ai_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                         //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE #FROM-FUND TO #FROM-FUND-H
        //*  PERFORM #CONVERT-FROM-FUND
        FF:                                                                                                                                                               //Natural: FOR #J = 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            if (condition(pnd_From_Fund.getValue(pnd_J).equals(" ")))                                                                                                     //Natural: IF #FROM-FUND ( #J ) = ' '
            {
                //*       ESCAPE BOTTOM(FF.)
                //*  THERE CAN BE BLANK FUNDS COMING IN SO CONT. TO NEXT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  FUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_From_Fund_Only.getValue(pnd_J).equals("Y")))                                                                                                //Natural: IF #FROM-FUND-ONLY ( #J ) = 'Y'
            {
                pnd_Two_Byte_Fund.setValue(pnd_From_Fund.getValue(pnd_J));                                                                                                //Natural: ASSIGN #TWO-BYTE-FUND := #FROM-FUND ( #J )
                pnd_One_Byte_Fund.setValue(pnd_From_Acct_Code.getValue(pnd_J));                                                                                           //Natural: ASSIGN #ONE-BYTE-FUND := #FROM-ACCT-CODE ( #J )
                if (condition(pnd_Frm_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                              //Natural: IF #FRM-UNIT-TYP ( #J ) = 'A'
                {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-CNV-FUND
                    sub_Pnd_Annual_Cnv_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FF"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_From_Aftr_Xfr_Units.getValue(pnd_J).greater(getZero()) && pnd_From_Aftr_Xfr_Guar.getValue(pnd_J).greater(getZero())))               //Natural: IF #FROM-AFTR-XFR-UNITS ( #J ) > 0 AND #FROM-AFTR-XFR-GUAR ( #J ) > 0
                    {
                        pnd_Partial_Transfer.setValue("Y");                                                                                                               //Natural: MOVE 'Y' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Partial_Transfer.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                    sub_Pnd_Monthly_Cnv_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FF"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_From_Aftr_Xfr_Units.getValue(pnd_J).greater(getZero())))                                                                            //Natural: IF #FROM-AFTR-XFR-UNITS ( #J ) > 0
                    {
                        pnd_Partial_Transfer.setValue("Y");                                                                                                               //Natural: MOVE 'Y' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Partial_Transfer.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FROM-FUNDS
                sub_Pnd_Process_From_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F3:                                                                                                                                                               //Natural: FOR #J = 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            if (condition(pnd_To_Fund.getValue(pnd_J).equals(" ")))                                                                                                       //Natural: IF #TO-FUND ( #J ) = ' '
            {
                //*        ESCAPE BOTTOM
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Two_Byte_Fund.setValue(pnd_To_Fund.getValue(pnd_J));                                                                                                      //Natural: ASSIGN #TWO-BYTE-FUND := #TO-FUND ( #J )
            pnd_One_Byte_Fund.setValue(pnd_To_Acct_Code.getValue(pnd_J));                                                                                                 //Natural: ASSIGN #ONE-BYTE-FUND := #TO-ACCT-CODE ( #J )
            if (condition(pnd_To_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                                   //Natural: IF #TO-UNIT-TYP ( #J ) = 'A'
            {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-CNV-FUND
                sub_Pnd_Annual_Cnv_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                sub_Pnd_Monthly_Cnv_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-ON-FILE
            sub_Pnd_Check_If_On_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F3"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_On_File_Already.equals("Y")))                                                                                                               //Natural: IF #ON-FILE-ALREADY = 'Y'
            {
                pnd_Return_Code.setValue("M4");                                                                                                                           //Natural: ASSIGN #RETURN-CODE = 'M4'
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CREF-FUND-TO
                sub_Pnd_Update_Cref_Fund_To();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Return_Code.setValue("M4");                                                                                                                           //Natural: ASSIGN #RETURN-CODE = 'M4'
                                                                                                                                                                          //Natural: PERFORM #STORE-CREF-FUND-TO
                sub_Pnd_Store_Cref_Fund_To();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                     //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Return_Code.setValue("T4");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T4'
                                                                                                                                                                          //Natural: PERFORM #STORE-CREF-FUND-TO-AI
        sub_Pnd_Store_Cref_Fund_To_Ai();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                         //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FROM-FUNDS
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CREF-FUND-FROM
        //* **********************************************************************
        //*  NEW VIEW PREFIX FROM IAAL999                     041212 START
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DELETE-CREF-FUND-FROM
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-CREF-FUND-FROM-AI
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-CREF-FUND-TO-AI
        //* **********************************************************************
        //*       #W-FUND-CODE        := #FROM-FUND-TOT
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-CREF-FUND-TO
        //* *. FOR #I = 1 TO 20
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CONTRACTS
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CPR
        //* **********************************************************************
        //*  WRITE 'AFTER IMAGE CPR'
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CPR
        //* **********************************************************************
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-IF-ON-FILE
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CREF-FUND-TO
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-CNV-FUND
        //* ******************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-CNV-FUND
        //* ******************************************************************
    }
    private void sub_Pnd_Process_From_Funds() throws Exception                                                                                                            //Natural: #PROCESS-FROM-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                             //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        if (condition(pnd_Partial_Transfer.equals("Y")))                                                                                                                  //Natural: IF #PARTIAL-TRANSFER = 'Y'
        {
            pnd_Return_Code.setValue("M4");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M4'
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CREF-FUND-FROM
            sub_Pnd_Update_Cref_Fund_From();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*    ASSIGN #RETURN-CODE  = 'T4'
            //*    PERFORM #STORE-CREF-FUND-FROM-AI
            //*    IF #RETURN-CODE NE ' '
            //*      ESCAPE ROUTINE
            //*    END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Return_Code.setValue("M4");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M4'
                                                                                                                                                                          //Natural: PERFORM #DELETE-CREF-FUND-FROM
            sub_Pnd_Delete_Cref_Fund_From();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Update_Cref_Fund_From() throws Exception                                                                                                         //Natural: #UPDATE-CREF-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1B",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1B:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1B")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                if (condition(pnd_Frm_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                              //Natural: IF #FRM-UNIT-TYP ( #J ) = 'A'
                {
                    ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt().setValue(pnd_From_Aftr_Xfr_Guar.getValue(pnd_J));                                                 //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT := #FROM-AFTR-XFR-GUAR ( #J )
                    if (condition(pnd_Eff_Dte_03_31.equals("Y")))                                                                                                         //Natural: IF #EFF-DTE-03-31 = 'Y'
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(pnd_From_Reval_Unit_Val.getValue(pnd_J));                                               //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := #FROM-REVAL-UNIT-VAL ( #J )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(0);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                      //Natural: ASSIGN IAA-CREF-FUND-RCRD.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1).setValue(pnd_From_Aftr_Xfr_Units.getValue(pnd_J));                                          //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := #FROM-AFTR-XFR-UNITS ( #J )
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte().setValue(pnd_Todays_Dte);                                                                         //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-LST-XFR-OUT-DTE := #TODAYS-DTE
                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().updateDBRow("R1B");                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1B;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1B. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Delete_Cref_Fund_From() throws Exception                                                                                                         //Natural: #DELETE-CREF-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1D",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1D:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1D")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().deleteDBRow("R1D");                                                                                                 //Natural: DELETE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1D;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1D. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Cref_Fund_From_Ai() throws Exception                                                                                                       //Natural: #STORE-CREF-FUND-FROM-AI
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1C",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1C:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1C")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1C;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1C. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Store_Cref_Fund_To_Ai() throws Exception                                                                                                         //Natural: #STORE-CREF-FUND-TO-AI
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                             //Natural: ASSIGN #W-CNTRCT-PAYEE := #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1T",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1T:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1T")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*          IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE   =  #W-FUND-CODE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1T;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1T. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Store_Cref_Fund_To() throws Exception                                                                                                            //Natural: #STORE-CREF-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().reset();                                                                                                                    //Natural: RESET IAA-CREF-FUND-RCRD
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().setValue(pnd_Iaa_From_Cntrct);                                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR := #IAA-FROM-CNTRCT
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().setValue(pnd_Iaa_From_Pyee_N);                                                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE := #IAA-FROM-PYEE-N
        //*  SWITCH               /* 051112 START
        if (condition(pnd_Rcrd_Type_Cde.equals("2")))                                                                                                                     //Natural: IF #RCRD-TYPE-CDE = '2'
        {
            ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund().setValue(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde());                                      //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-OLD-CMPNY-FUND := IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE
            //*  051112 END
            //*  011609
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().setValue(pnd_From_Fund_Tot);                                                                               //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE := #FROM-FUND-TOT
        F5:                                                                                                                                                               //Natural: FOR #I = 1 TO #MAX-PRODS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Prods)); pnd_I.nadd(1))
        {
            pnd_Rate_Code_Breakdown.setValue(pnd_Rate_Code_Table.getValue(pnd_I));                                                                                        //Natural: MOVE #RATE-CODE-TABLE ( #I ) TO #RATE-CODE-BREAKDOWN
            if (condition(pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1.equals(pnd_To_Acct_Code.getValue(pnd_J))))                                                              //Natural: IF #RATE-CODE-1 = #TO-ACCT-CODE ( #J )
            {
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(1).setValue(pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2);                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( 1 ) := #RATE-CODE-2
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Dte().getValue(1).reset();                                                                                     //Natural: RESET IAA-CREF-FUND-RCRD.CREF-RATE-DTE ( 1 )
                if (true) break F5;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F5. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_To_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                                       //Natural: IF #TO-UNIT-TYP ( #J ) = 'A'
        {
            ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt().setValue(pnd_To_Aftr_Xfr_Guar.getValue(pnd_J));                                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT := #TO-AFTR-XFR-GUAR ( #J )
            if (condition(pnd_Eff_Dte_03_31.equals("Y")))                                                                                                                 //Natural: IF #EFF-DTE-03-31 = 'Y'
            {
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(pnd_To_Reval_Unit_Val.getValue(pnd_J));                                                         //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := #TO-REVAL-UNIT-VAL ( #J )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(0);                                                                                             //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1).setValue(pnd_To_Aftr_Xfr_Units.getValue(pnd_J));                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := #TO-AFTR-XFR-UNITS ( #J )
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                              //Natural: ASSIGN IAA-CREF-FUND-RCRD.LST-TRANS-DTE := #TIME
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte().setValue(pnd_Todays_Dte);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-XFR-ISS-DTE := #TODAYS-DTE
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                                  //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-LST-XFR-IN-DTE := #TODAYS-DTE
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-CREF-FUND-RCRD
        pnd_Return_Code.reset();                                                                                                                                          //Natural: RESET #RETURN-CODE
    }
    private void sub_Pnd_Ai_Contracts() throws Exception                                                                                                                  //Natural: #AI-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        (
        "FNR",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_From_Cntrct, WcType.WITH) },
        1
        );
        FNR:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FNR")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal999.getVw_iaa_Cntrct_Trans().reset();                                                                                                                  //Natural: RESET IAA-CNTRCT-TRANS
            ldaIaal999.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cntrct());                                                                           //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
            ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Time);                                                                                                //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE = #TIME
            pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte());                                                                                         //Natural: ASSIGN #DATE-TIME-P = IAA-CNTRCT-TRANS.TRANS-DTE
            ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte()),                    //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
            ldaIaal999.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                            //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE = #TIME
            ldaIaal999.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID = '2'
            ldaIaal999.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                    //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
            ldaIaal999.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                            //Natural: STORE IAA-CNTRCT-TRANS
            pnd_Return_Code.reset();                                                                                                                                      //Natural: RESET #RETURN-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Ai_Cpr() throws Exception                                                                                                                        //Natural: #AI-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPX",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPX:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CPX")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal999.getVw_iaa_Cpr_Trans().reset();                                                                                                                 //Natural: RESET IAA-CPR-TRANS
                ldaIaal999.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal999.getVw_cpr());                                                                                 //Natural: MOVE BY NAME CPR TO IAA-CPR-TRANS
                ldaIaal999.getIaa_Cpr_Trans_Trans_Dte().setValue(pnd_Time);                                                                                               //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cpr_Trans_Trans_Dte());                                                                                        //Natural: ASSIGN #DATE-TIME-P = IAA-CPR-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte()), new                  //Natural: COMPUTE IAA-CPR-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cpr_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                           //Natural: ASSIGN IAA-CPR-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal999.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                   //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CPR-TRANS
                //*          WRITE IAA-CPR-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Update_Cpr() throws Exception                                                                                                                    //Natural: #UPDATE-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "CP",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CP:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CP")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal999.getCpr_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                     //Natural: ASSIGN CPR.LST-TRANS-DTE = #TIME
                //*           WRITE 'UPDATE CPR'
                ldaIaal999.getVw_cpr().updateDBRow("CP");                                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_If_On_File() throws Exception                                                                                                              //Natural: #CHECK-IF-ON-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File_Already.reset();                                                                                                                                      //Natural: RESET #ON-FILE-ALREADY
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                             //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Z",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1Z:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1Z")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_On_File_Already.setValue("Y");                                                                                                                        //Natural: MOVE 'Y' TO #ON-FILE-ALREADY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Update_Cref_Fund_To() throws Exception                                                                                                           //Natural: #UPDATE-CREF-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                             //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1R",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1R:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1R")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                if (condition(pnd_To_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                               //Natural: IF #TO-UNIT-TYP ( #J ) = 'A'
                {
                    ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt().setValue(pnd_To_Aftr_Xfr_Guar.getValue(pnd_J));                                                   //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT := #TO-AFTR-XFR-GUAR ( #J )
                    if (condition(pnd_Eff_Dte_03_31.equals("Y")))                                                                                                         //Natural: IF #EFF-DTE-03-31 = 'Y'
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(pnd_To_Reval_Unit_Val.getValue(pnd_J));                                                 //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := #TO-REVAL-UNIT-VAL ( #J )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(0);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                      //Natural: ASSIGN IAA-CREF-FUND-RCRD.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1).setValue(pnd_To_Aftr_Xfr_Units.getValue(pnd_J));                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := #TO-AFTR-XFR-UNITS ( #J )
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                          //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-LST-XFR-IN-DTE := #TODAYS-DTE
                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().updateDBRow("R1R");                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1R;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1R. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  NEW VIEW PREFIX FROM IAAL999                     041212 END
    }
    private void sub_Pnd_Annual_Cnv_Fund() throws Exception                                                                                                               //Natural: #ANNUAL-CNV-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet975 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet975++;
            //*  011609
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet975++;
            pnd_Fund_Cd_1.setValue("U");                                                                                                                                  //Natural: MOVE 'U' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("2");                                                                                                                                  //Natural: MOVE '2' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_From_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                    //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FROM-FUND-TOT LEAVING NO
    }
    private void sub_Pnd_Monthly_Cnv_Fund() throws Exception                                                                                                              //Natural: #MONTHLY-CNV-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet989 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet989++;
            //*  011609
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet989++;
            pnd_Fund_Cd_1.setValue("W");                                                                                                                                  //Natural: MOVE 'W' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("4");                                                                                                                                  //Natural: MOVE '4' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_From_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                    //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FROM-FUND-TOT LEAVING NO
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
