/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:22 AM
**        * FROM NATURAL SUBPROGRAM : Iaan586a
************************************************************
**        * FILE NAME            : Iaan586a.java
**        * CLASS NAME           : Iaan586a
**        * INSTANCE NAME        : Iaan586a
************************************************************
************************************************************************
* PROGRAM: IAAN586A
* DATE   : 02/28/99
* AUTHOR : ARI G
* DESC   : THIS SUBPROGRAM WILL BREAK UP FUND INFO GOING BACK A SPECIFIC
*           AMOUNT OF TIME. USED FOR OVERPAYMENT REPORTING SYSTEM.
* HISTORY:
*
* 04/15/14  O SOTTO RECEIVE AND PASS OVRPYMNT-ID-NBR TO IAAN586B.
*                   CHANGES MARKED 041514.
* 04/2017   O SOTTO RE-STOWED FOR IAAL200B PIN EXPANSION.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan586a extends BLNatBase
{
    // Data Areas
    private PdaIaaa299a pdaIaaa299a;
    private LdaIaal202h ldaIaal202h;
    private PdaIaapda_M pdaIaapda_M;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal200a ldaIaal200a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_W_Cntrct;

    private DbsGroup pnd_W_Cntrct__R_Field_1;
    private DbsField pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3;

    private DbsGroup pnd_W_Cntrct__R_Field_2;
    private DbsField pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1;
    private DbsField pnd_W_Payee_Cde_1;
    private DbsField pnd_W_Payee_Cde_2;
    private DbsField pnd_Tab_Install_Date;
    private DbsField pnd_Tab_Per_Amt;
    private DbsField pnd_Tab_Div_Amt;
    private DbsField pnd_Tab_File;
    private DbsField pnd_Tab_Inverse_Date;
    private DbsField pnd_Num_Of_Instlmnts;
    private DbsField pnd_Lst_Pd_Dte;
    private DbsField pnd_Year;
    private DbsField pnd_Numb;
    private DbsField pnd_Overpay_Years;
    private DbsField pnd_Overpay_Id_Nbr;
    private DbsField pnd_Table_Years_Year;
    private DbsField pnd_Table_Years_Instlmnts;
    private DbsField pnd_Table_Years_Guar;
    private DbsField pnd_Table_Years_Divd;
    private DbsField pnd_Table_Years_Unit;
    private DbsField pnd_Table_Years_Totals;
    private DbsField pnd_G_Instlmnts;
    private DbsField pnd_G_Guar;
    private DbsField pnd_G_Divd;
    private DbsField pnd_G_Unit;
    private DbsField pnd_G_Totals;
    private DbsField pnd_Num_Field;
    private DbsField pnd_W_Month_Field;
    private DbsField pnd_W_Delete_Field;
    private DbsField pnd_Month_Field;
    private DbsField pnd_Delete_Field;
    private DbsField pnd_Msg_1;
    private DbsField pnd_Ov_Tot_Amt;
    private DbsField pnd_Ov_Per_Amt;
    private DbsField pnd_Ov_Div_Amt;
    private DbsField pnd_Ov_Unt_Amt;
    private DbsField pnd_Ov_Instl_Date;
    private DbsField pnd_Ov_Fund;
    private DbsField pnd_J;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_3;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_4;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_5;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_6;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Fund_Code_1_3;

    private DbsGroup pnd_Fund_Code_1_3__R_Field_7;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_1;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3;

    private DbsGroup pnd_Count;
    private DbsField pnd_Count_Pnd_Err_Cnt;
    private DbsField pnd_Count_Pnd_Rec_Cnt;
    private DbsField pnd_Count_Pnd_Install_Cnt;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_8;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_9;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_10;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_11;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_12;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_13;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_14;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_15;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N;

    private DbsGroup pnd_Issue_Date_A__R_Field_16;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Dd;

    private DbsGroup pnd_Issue_Date_A__R_Field_17;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Mm;

    private DbsGroup pnd_Issue_Date_A__R_Field_18;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd;
    private DbsField pnd_Xfr_Issue_Date_A;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_19;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_20;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_21;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_22;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd;
    private DbsField pnd_W_Issue_Date;
    private DbsField pnd_W_Option;
    private DbsField pnd_I;

    private DbsGroup pnd_A26_Fields;
    private DbsField pnd_A26_Fields_Pnd_A26_Call_Type;
    private DbsField pnd_A26_Fields_Pnd_A26_Fnd_Cde;
    private DbsField pnd_A26_Fields_Pnd_A26_Reval_Meth;
    private DbsField pnd_A26_Fields_Pnd_A26_Req_Dte;
    private DbsField pnd_A26_Fields_Pnd_A26_Prtc_Dte;
    private DbsField pnd_Auv_Ret_Dte;
    private DbsField pnd_Rc;
    private DbsField pnd_Auv;
    private DbsField pnd_W_Auv;
    private DbsField pnd_Change_In_Record;
    private DbsField pnd_Finish_Processing;
    private DbsField pnd_Valid_Date;
    private DbsField pnd_Val_Meth;
    private DbsField pnd_Error;
    private DbsField pnd_W_Fund_Dte;
    private DbsField pnd_Tiaa_No_Units;
    private DbsField pnd_W_Bottom_Year;

    private DbsGroup pnd_W_Bottom_Year__R_Field_23;
    private DbsField pnd_W_Bottom_Year_Pnd_W_Date_Yyyy;
    private DbsField pnd_W_Bottom_Year_Pnd_W_Date_Mm;
    private DbsField pnd_Dod_Date;

    private DbsGroup pnd_Dod_Date__R_Field_24;
    private DbsField pnd_Dod_Date_Pnd_Dod_Date_Yyyy;
    private DbsField pnd_Dod_Date_Pnd_Dod_Date_Mm;
    private DbsField pnd_Year_Hold;
    private DbsField pnd_Table_Year;
    private DbsField pnd_Table_File;
    private DbsField pnd_Table_Inv_Date;
    private DbsField pnd_First_Time_Thru;
    private DbsField pnd_Yearly_Instlmnts;
    private DbsField pnd_Tot_Guar_Ov;
    private DbsField pnd_Tot_Divd_Ov;
    private DbsField pnd_Tot_Unit_Ov;
    private DbsField pnd_Tot_Ov;
    private DbsField pnd_Years;
    private DbsField pnd_Install_Date_A;

    private DbsGroup pnd_Install_Date_A__R_Field_25;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_N;

    private DbsGroup pnd_Install_Date_A__R_Field_26;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Yyyymm;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Dd;

    private DbsGroup pnd_Install_Date_A__R_Field_27;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Yyyy;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_Mm;

    private DbsGroup pnd_Install_Date_A__R_Field_28;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Mm;
    private DbsField pnd_Install_Date_A_Pnd_Install_Date_A8_Dd;
    private DbsField pnd_W_Lst_Pd_Dte;

    private DbsGroup pnd_W_Lst_Pd_Dte__R_Field_29;
    private DbsField pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymmdd;

    private DbsGroup pnd_W_Lst_Pd_Dte__R_Field_30;
    private DbsField pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymm;
    private DbsField pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Dd;
    private DbsField pnd_End_Program;
    private DbsField pnd_Option_Code;
    private DbsField pnd_W_Mode;
    private DbsField pnd_Year_Num;
    private DbsField pnd_X_Num;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa299a = new PdaIaaa299a(localVariables);
        ldaIaal202h = new LdaIaal202h();
        registerRecord(ldaIaal202h);
        registerRecord(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1());
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());

        // parameters
        parameters = new DbsRecord();
        pnd_W_Cntrct = parameters.newFieldInRecord("pnd_W_Cntrct", "#W-CNTRCT", FieldType.STRING, 10);
        pnd_W_Cntrct.setParameterOption(ParameterOption.ByReference);

        pnd_W_Cntrct__R_Field_1 = parameters.newGroupInRecord("pnd_W_Cntrct__R_Field_1", "REDEFINE", pnd_W_Cntrct);
        pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3 = pnd_W_Cntrct__R_Field_1.newFieldInGroup("pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3", "#PPCN-NBR-A3", FieldType.STRING, 3);

        pnd_W_Cntrct__R_Field_2 = pnd_W_Cntrct__R_Field_1.newGroupInGroup("pnd_W_Cntrct__R_Field_2", "REDEFINE", pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3);
        pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1 = pnd_W_Cntrct__R_Field_2.newFieldInGroup("pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1", "#PPCN-NBR-A1", FieldType.STRING, 1);
        pnd_W_Payee_Cde_1 = parameters.newFieldInRecord("pnd_W_Payee_Cde_1", "#W-PAYEE-CDE-1", FieldType.NUMERIC, 2);
        pnd_W_Payee_Cde_1.setParameterOption(ParameterOption.ByReference);
        pnd_W_Payee_Cde_2 = parameters.newFieldInRecord("pnd_W_Payee_Cde_2", "#W-PAYEE-CDE-2", FieldType.NUMERIC, 2);
        pnd_W_Payee_Cde_2.setParameterOption(ParameterOption.ByReference);
        pnd_Tab_Install_Date = parameters.newFieldArrayInRecord("pnd_Tab_Install_Date", "#TAB-INSTALL-DATE", FieldType.STRING, 8, new DbsArrayController(1, 
            120));
        pnd_Tab_Install_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Tab_Per_Amt = parameters.newFieldArrayInRecord("pnd_Tab_Per_Amt", "#TAB-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_Per_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Tab_Div_Amt = parameters.newFieldArrayInRecord("pnd_Tab_Div_Amt", "#TAB-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_Div_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Tab_File = parameters.newFieldArrayInRecord("pnd_Tab_File", "#TAB-FILE", FieldType.STRING, 1, new DbsArrayController(1, 120));
        pnd_Tab_File.setParameterOption(ParameterOption.ByReference);
        pnd_Tab_Inverse_Date = parameters.newFieldArrayInRecord("pnd_Tab_Inverse_Date", "#TAB-INVERSE-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            120));
        pnd_Tab_Inverse_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Num_Of_Instlmnts = parameters.newFieldInRecord("pnd_Num_Of_Instlmnts", "#NUM-OF-INSTLMNTS", FieldType.NUMERIC, 3);
        pnd_Num_Of_Instlmnts.setParameterOption(ParameterOption.ByReference);
        pnd_Lst_Pd_Dte = parameters.newFieldInRecord("pnd_Lst_Pd_Dte", "#LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Lst_Pd_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Year = parameters.newFieldInRecord("pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        pnd_Year.setParameterOption(ParameterOption.ByReference);
        pnd_Numb = parameters.newFieldArrayInRecord("pnd_Numb", "#NUMB", FieldType.NUMERIC, 4, new DbsArrayController(1, 7));
        pnd_Numb.setParameterOption(ParameterOption.ByReference);
        pnd_Overpay_Years = parameters.newFieldInRecord("pnd_Overpay_Years", "#OVERPAY-YEARS", FieldType.NUMERIC, 1);
        pnd_Overpay_Years.setParameterOption(ParameterOption.ByReference);
        pnd_Overpay_Id_Nbr = parameters.newFieldInRecord("pnd_Overpay_Id_Nbr", "#OVERPAY-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Overpay_Id_Nbr.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Table_Years_Year = localVariables.newFieldArrayInRecord("pnd_Table_Years_Year", "#TABLE-YEARS-YEAR", FieldType.NUMERIC, 4, new DbsArrayController(1, 
            7));
        pnd_Table_Years_Instlmnts = localVariables.newFieldArrayInRecord("pnd_Table_Years_Instlmnts", "#TABLE-YEARS-INSTLMNTS", FieldType.NUMERIC, 2, 
            new DbsArrayController(1, 7));
        pnd_Table_Years_Guar = localVariables.newFieldArrayInRecord("pnd_Table_Years_Guar", "#TABLE-YEARS-GUAR", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            7));
        pnd_Table_Years_Divd = localVariables.newFieldArrayInRecord("pnd_Table_Years_Divd", "#TABLE-YEARS-DIVD", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            7));
        pnd_Table_Years_Unit = localVariables.newFieldArrayInRecord("pnd_Table_Years_Unit", "#TABLE-YEARS-UNIT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            7));
        pnd_Table_Years_Totals = localVariables.newFieldArrayInRecord("pnd_Table_Years_Totals", "#TABLE-YEARS-TOTALS", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 7));
        pnd_G_Instlmnts = localVariables.newFieldInRecord("pnd_G_Instlmnts", "#G-INSTLMNTS", FieldType.NUMERIC, 2);
        pnd_G_Guar = localVariables.newFieldInRecord("pnd_G_Guar", "#G-GUAR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_G_Divd = localVariables.newFieldInRecord("pnd_G_Divd", "#G-DIVD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_G_Unit = localVariables.newFieldInRecord("pnd_G_Unit", "#G-UNIT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_G_Totals = localVariables.newFieldInRecord("pnd_G_Totals", "#G-TOTALS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Num_Field = localVariables.newFieldArrayInRecord("pnd_Num_Field", "#NUM-FIELD", FieldType.NUMERIC, 4, new DbsArrayController(1, 12));
        pnd_W_Month_Field = localVariables.newFieldArrayInRecord("pnd_W_Month_Field", "#W-MONTH-FIELD", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            12));
        pnd_W_Delete_Field = localVariables.newFieldArrayInRecord("pnd_W_Delete_Field", "#W-DELETE-FIELD", FieldType.STRING, 1, new DbsArrayController(1, 
            12));
        pnd_Month_Field = localVariables.newFieldArrayInRecord("pnd_Month_Field", "#MONTH-FIELD", FieldType.NUMERIC, 2, new DbsArrayController(1, 7, 1, 
            12));
        pnd_Delete_Field = localVariables.newFieldArrayInRecord("pnd_Delete_Field", "#DELETE-FIELD", FieldType.STRING, 1, new DbsArrayController(1, 7, 
            1, 12));
        pnd_Msg_1 = localVariables.newFieldArrayInRecord("pnd_Msg_1", "#MSG-1", FieldType.STRING, 3, new DbsArrayController(1, 208));
        pnd_Ov_Tot_Amt = localVariables.newFieldArrayInRecord("pnd_Ov_Tot_Amt", "#OV-TOT-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            208));
        pnd_Ov_Per_Amt = localVariables.newFieldArrayInRecord("pnd_Ov_Per_Amt", "#OV-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            208));
        pnd_Ov_Div_Amt = localVariables.newFieldArrayInRecord("pnd_Ov_Div_Amt", "#OV-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            208));
        pnd_Ov_Unt_Amt = localVariables.newFieldArrayInRecord("pnd_Ov_Unt_Amt", "#OV-UNT-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            208));
        pnd_Ov_Instl_Date = localVariables.newFieldArrayInRecord("pnd_Ov_Instl_Date", "#OV-INSTL-DATE", FieldType.STRING, 8, new DbsArrayController(1, 
            208));
        pnd_Ov_Fund = localVariables.newFieldArrayInRecord("pnd_Ov_Fund", "#OV-FUND", FieldType.STRING, 5, new DbsArrayController(1, 208));
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 1);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_4", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_4.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_4.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr", 
            "#OLD-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee", "#OLD-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte", 
            "#OLD-INVERSE-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code = pnd_Cntrct_Py_Dte_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code", "#OLD-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_6", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_Fund_Code_1_3", "#FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_Fund_Code_1_3__R_Field_7 = localVariables.newGroupInRecord("pnd_Fund_Code_1_3__R_Field_7", "REDEFINE", pnd_Fund_Code_1_3);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_1 = pnd_Fund_Code_1_3__R_Field_7.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_1", "#FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3 = pnd_Fund_Code_1_3__R_Field_7.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3", "#FUND-CODE-2-3", FieldType.STRING, 
            2);

        pnd_Count = localVariables.newGroupInRecord("pnd_Count", "#COUNT");
        pnd_Count_Pnd_Err_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Err_Cnt", "#ERR-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Rec_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Install_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Install_Cnt", "#INSTALL-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_8", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_9 = pnd_Save_Check_Dte_A__R_Field_8.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_9", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);

        pnd_Save_Check_Dte_A__R_Field_10 = pnd_Save_Check_Dte_A__R_Field_9.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_10", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_10.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_10.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_11 = pnd_Save_Check_Dte_A__R_Field_10.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_11", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yy);
        pnd_Save_Check_Dte_A_Pnd_Yy_A = pnd_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy_A", "#YY-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_12 = pnd_Save_Check_Dte_A__R_Field_9.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_12", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_13 = pnd_Save_Check_Dte_A__R_Field_9.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_13", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Dd);
        pnd_Save_Check_Dte_A_Pnd_Dd_A = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd_A", "#DD-A", FieldType.STRING, 2);

        pnd_Save_Check_Dte_A__R_Field_14 = pnd_Save_Check_Dte_A__R_Field_8.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_14", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Issue_Date_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_15", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N = pnd_Issue_Date_A__R_Field_15.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A__R_Field_16 = pnd_Issue_Date_A__R_Field_15.newGroupInGroup("pnd_Issue_Date_A__R_Field_16", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm = pnd_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm", "#ISSUE-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Issue_Date_A_Pnd_Issue_Date_Dd = pnd_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_17 = pnd_Issue_Date_A__R_Field_15.newGroupInGroup("pnd_Issue_Date_A__R_Field_17", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy = pnd_Issue_Date_A__R_Field_17.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_Mm = pnd_Issue_Date_A__R_Field_17.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_18 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_18", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy", "#ISSUE-DATE-A8-CCYY", 
            FieldType.STRING, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm", "#ISSUE-DATE-A8-MM", 
            FieldType.STRING, 2);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd", "#ISSUE-DATE-A8-DD", 
            FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A = localVariables.newFieldInRecord("pnd_Xfr_Issue_Date_A", "#XFR-ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Xfr_Issue_Date_A__R_Field_19 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_19", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N = pnd_Xfr_Issue_Date_A__R_Field_19.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N", "#XFR-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Xfr_Issue_Date_A__R_Field_20 = pnd_Xfr_Issue_Date_A__R_Field_19.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_20", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm = pnd_Xfr_Issue_Date_A__R_Field_20.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm", 
            "#XFR-ISSUE-DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd = pnd_Xfr_Issue_Date_A__R_Field_20.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd", "#XFR-ISSUE-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_21 = pnd_Xfr_Issue_Date_A__R_Field_19.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_21", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy = pnd_Xfr_Issue_Date_A__R_Field_21.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy", 
            "#XFR-ISSUE-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm = pnd_Xfr_Issue_Date_A__R_Field_21.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm", "#XFR-ISSUE-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_22 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_22", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy = pnd_Xfr_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy", 
            "#XFR-ISSUE-DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm = pnd_Xfr_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm", 
            "#XFR-ISSUE-DATE-A8-MM", FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd = pnd_Xfr_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd", 
            "#XFR-ISSUE-DATE-A8-DD", FieldType.STRING, 2);
        pnd_W_Issue_Date = localVariables.newFieldInRecord("pnd_W_Issue_Date", "#W-ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_W_Option = localVariables.newFieldInRecord("pnd_W_Option", "#W-OPTION", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 6);

        pnd_A26_Fields = localVariables.newGroupInRecord("pnd_A26_Fields", "#A26-FIELDS");
        pnd_A26_Fields_Pnd_A26_Call_Type = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Call_Type", "#A26-CALL-TYPE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Fnd_Cde = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Fnd_Cde", "#A26-FND-CDE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Reval_Meth = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Reval_Meth", "#A26-REVAL-METH", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Req_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Req_Dte", "#A26-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_A26_Prtc_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Prtc_Dte", "#A26-PRTC-DTE", FieldType.NUMERIC, 8);
        pnd_Auv_Ret_Dte = localVariables.newFieldInRecord("pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_Auv = localVariables.newFieldInRecord("pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_W_Auv = localVariables.newFieldInRecord("pnd_W_Auv", "#W-AUV", FieldType.NUMERIC, 6, 2);
        pnd_Change_In_Record = localVariables.newFieldInRecord("pnd_Change_In_Record", "#CHANGE-IN-RECORD", FieldType.STRING, 1);
        pnd_Finish_Processing = localVariables.newFieldInRecord("pnd_Finish_Processing", "#FINISH-PROCESSING", FieldType.STRING, 1);
        pnd_Valid_Date = localVariables.newFieldInRecord("pnd_Valid_Date", "#VALID-DATE", FieldType.STRING, 1);
        pnd_Val_Meth = localVariables.newFieldInRecord("pnd_Val_Meth", "#VAL-METH", FieldType.STRING, 1);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 1);
        pnd_W_Fund_Dte = localVariables.newFieldInRecord("pnd_W_Fund_Dte", "#W-FUND-DTE", FieldType.NUMERIC, 8);
        pnd_Tiaa_No_Units = localVariables.newFieldInRecord("pnd_Tiaa_No_Units", "#TIAA-NO-UNITS", FieldType.PACKED_DECIMAL, 7, 3);
        pnd_W_Bottom_Year = localVariables.newFieldInRecord("pnd_W_Bottom_Year", "#W-BOTTOM-YEAR", FieldType.NUMERIC, 8);

        pnd_W_Bottom_Year__R_Field_23 = localVariables.newGroupInRecord("pnd_W_Bottom_Year__R_Field_23", "REDEFINE", pnd_W_Bottom_Year);
        pnd_W_Bottom_Year_Pnd_W_Date_Yyyy = pnd_W_Bottom_Year__R_Field_23.newFieldInGroup("pnd_W_Bottom_Year_Pnd_W_Date_Yyyy", "#W-DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_W_Bottom_Year_Pnd_W_Date_Mm = pnd_W_Bottom_Year__R_Field_23.newFieldInGroup("pnd_W_Bottom_Year_Pnd_W_Date_Mm", "#W-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Dod_Date = localVariables.newFieldInRecord("pnd_Dod_Date", "#DOD-DATE", FieldType.NUMERIC, 6);

        pnd_Dod_Date__R_Field_24 = localVariables.newGroupInRecord("pnd_Dod_Date__R_Field_24", "REDEFINE", pnd_Dod_Date);
        pnd_Dod_Date_Pnd_Dod_Date_Yyyy = pnd_Dod_Date__R_Field_24.newFieldInGroup("pnd_Dod_Date_Pnd_Dod_Date_Yyyy", "#DOD-DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Dod_Date_Pnd_Dod_Date_Mm = pnd_Dod_Date__R_Field_24.newFieldInGroup("pnd_Dod_Date_Pnd_Dod_Date_Mm", "#DOD-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Year_Hold = localVariables.newFieldInRecord("pnd_Year_Hold", "#YEAR-HOLD", FieldType.NUMERIC, 4);
        pnd_Table_Year = localVariables.newFieldArrayInRecord("pnd_Table_Year", "#TABLE-YEAR", FieldType.NUMERIC, 8, new DbsArrayController(1, 12));
        pnd_Table_File = localVariables.newFieldArrayInRecord("pnd_Table_File", "#TABLE-FILE", FieldType.STRING, 1, new DbsArrayController(1, 12));
        pnd_Table_Inv_Date = localVariables.newFieldArrayInRecord("pnd_Table_Inv_Date", "#TABLE-INV-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            12));
        pnd_First_Time_Thru = localVariables.newFieldInRecord("pnd_First_Time_Thru", "#FIRST-TIME-THRU", FieldType.STRING, 1);
        pnd_Yearly_Instlmnts = localVariables.newFieldInRecord("pnd_Yearly_Instlmnts", "#YEARLY-INSTLMNTS", FieldType.NUMERIC, 2);
        pnd_Tot_Guar_Ov = localVariables.newFieldInRecord("pnd_Tot_Guar_Ov", "#TOT-GUAR-OV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Divd_Ov = localVariables.newFieldInRecord("pnd_Tot_Divd_Ov", "#TOT-DIVD-OV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Unit_Ov = localVariables.newFieldInRecord("pnd_Tot_Unit_Ov", "#TOT-UNIT-OV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Ov = localVariables.newFieldInRecord("pnd_Tot_Ov", "#TOT-OV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Years = localVariables.newFieldInRecord("pnd_Years", "#YEARS", FieldType.NUMERIC, 2);
        pnd_Install_Date_A = localVariables.newFieldInRecord("pnd_Install_Date_A", "#INSTALL-DATE-A", FieldType.STRING, 8);

        pnd_Install_Date_A__R_Field_25 = localVariables.newGroupInRecord("pnd_Install_Date_A__R_Field_25", "REDEFINE", pnd_Install_Date_A);
        pnd_Install_Date_A_Pnd_Install_Date_N = pnd_Install_Date_A__R_Field_25.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_N", "#INSTALL-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Install_Date_A__R_Field_26 = pnd_Install_Date_A__R_Field_25.newGroupInGroup("pnd_Install_Date_A__R_Field_26", "REDEFINE", pnd_Install_Date_A_Pnd_Install_Date_N);
        pnd_Install_Date_A_Pnd_Install_Date_Yyyymm = pnd_Install_Date_A__R_Field_26.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Yyyymm", "#INSTALL-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Install_Date_A_Pnd_Install_Date_Dd = pnd_Install_Date_A__R_Field_26.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Dd", "#INSTALL-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Install_Date_A__R_Field_27 = pnd_Install_Date_A__R_Field_25.newGroupInGroup("pnd_Install_Date_A__R_Field_27", "REDEFINE", pnd_Install_Date_A_Pnd_Install_Date_N);
        pnd_Install_Date_A_Pnd_Install_Date_Yyyy = pnd_Install_Date_A__R_Field_27.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Yyyy", "#INSTALL-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Install_Date_A_Pnd_Install_Date_Mm = pnd_Install_Date_A__R_Field_27.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_Mm", "#INSTALL-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Install_Date_A__R_Field_28 = localVariables.newGroupInRecord("pnd_Install_Date_A__R_Field_28", "REDEFINE", pnd_Install_Date_A);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy = pnd_Install_Date_A__R_Field_28.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Ccyy", "#INSTALL-DATE-A8-CCYY", 
            FieldType.STRING, 4);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Mm = pnd_Install_Date_A__R_Field_28.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Mm", "#INSTALL-DATE-A8-MM", 
            FieldType.STRING, 2);
        pnd_Install_Date_A_Pnd_Install_Date_A8_Dd = pnd_Install_Date_A__R_Field_28.newFieldInGroup("pnd_Install_Date_A_Pnd_Install_Date_A8_Dd", "#INSTALL-DATE-A8-DD", 
            FieldType.STRING, 2);
        pnd_W_Lst_Pd_Dte = localVariables.newFieldInRecord("pnd_W_Lst_Pd_Dte", "#W-LST-PD-DTE", FieldType.NUMERIC, 8);

        pnd_W_Lst_Pd_Dte__R_Field_29 = localVariables.newGroupInRecord("pnd_W_Lst_Pd_Dte__R_Field_29", "REDEFINE", pnd_W_Lst_Pd_Dte);
        pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymmdd = pnd_W_Lst_Pd_Dte__R_Field_29.newFieldInGroup("pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymmdd", "#W-LST-PD-DTE-YYYYMMDD", 
            FieldType.NUMERIC, 8);

        pnd_W_Lst_Pd_Dte__R_Field_30 = pnd_W_Lst_Pd_Dte__R_Field_29.newGroupInGroup("pnd_W_Lst_Pd_Dte__R_Field_30", "REDEFINE", pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymmdd);
        pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymm = pnd_W_Lst_Pd_Dte__R_Field_30.newFieldInGroup("pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymm", "#W-LST-PD-DTE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Dd = pnd_W_Lst_Pd_Dte__R_Field_30.newFieldInGroup("pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Dd", "#W-LST-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_End_Program = localVariables.newFieldInRecord("pnd_End_Program", "#END-PROGRAM", FieldType.STRING, 1);
        pnd_Option_Code = localVariables.newFieldInRecord("pnd_Option_Code", "#OPTION-CODE", FieldType.NUMERIC, 2);
        pnd_W_Mode = localVariables.newFieldInRecord("pnd_W_Mode", "#W-MODE", FieldType.NUMERIC, 3);
        pnd_Year_Num = localVariables.newFieldInRecord("pnd_Year_Num", "#YEAR-NUM", FieldType.NUMERIC, 1);
        pnd_X_Num = localVariables.newFieldInRecord("pnd_X_Num", "#X-NUM", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal202h.initializeValues();
        ldaIaal200b.initializeValues();
        ldaIaal200a.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan586a() throws Exception
    {
        super("Iaan586a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_W_Lst_Pd_Dte.setValue(pnd_Lst_Pd_Dte);                                                                                                                        //Natural: MOVE #LST-PD-DTE TO #W-LST-PD-DTE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CNTRCT
        sub_Pnd_Process_Cntrct();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CPR
        sub_Pnd_Process_Cpr();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #FILL-TABLES
        sub_Pnd_Fill_Tables();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PROCESS-TABLES
        sub_Pnd_Process_Tables();
        if (condition(Global.isEscape())) {return;}
        //* *. FOR #I = #NUM-OF-INSTLMNTS TO 1 STEP -1
        F1:                                                                                                                                                               //Natural: FOR #I = 1 TO #NUM-OF-INSTLMNTS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Num_Of_Instlmnts)); pnd_I.nadd(1))
        {
            pnd_Install_Date_A.setValue(pnd_Tab_Install_Date.getValue(pnd_I));                                                                                            //Natural: ASSIGN #INSTALL-DATE-A := #TAB-INSTALL-DATE ( #I )
            short decideConditionsMet480 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DOD-DATE > #INSTALL-DATE-YYYYMM
            if (condition(pnd_Dod_Date.greater(pnd_Install_Date_A_Pnd_Install_Date_Yyyymm)))
            {
                decideConditionsMet480++;
                //*         IGNORE
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
            }                                                                                                                                                             //Natural: WHEN #W-LST-PD-DTE-YYYYMM < #INSTALL-DATE-YYYYMM
            else if (condition(pnd_W_Lst_Pd_Dte_Pnd_W_Lst_Pd_Dte_Yyyymm.less(pnd_Install_Date_A_Pnd_Install_Date_Yyyymm)))
            {
                decideConditionsMet480++;
                //*         ESCAPE BOTTOM(F1.)
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-OV-INSTALLMENT
                sub_Pnd_Process_Ov_Installment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_End_Program.equals("Y")))                                                                                                                   //Natural: IF #END-PROGRAM = 'Y'
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Year_Num.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #YEAR-NUM
        if (condition(pnd_Year.equals(9999)))                                                                                                                             //Natural: IF #YEAR = 9999
        {
                                                                                                                                                                          //Natural: PERFORM #REPORT-BREAKDOWN
            sub_Pnd_Report_Breakdown();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-OV-INSTALLMENT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CNTRCT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #REPORT-BREAKDOWN
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #OVERPAYMENT-SUMMARY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CPR
        //* ***********************************************************************
        //* ***********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-TABLES
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-TABLES
        //* ********************************************************************
        //* *F9A. FOR #J = 1 TO #OVERPAY-YEARS
        //* ********************************************************************
    }
    private void sub_Pnd_Process_Ov_Installment() throws Exception                                                                                                        //Natural: #PROCESS-OV-INSTALLMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_First_Time_Thru.equals(" ")))                                                                                                                   //Natural: IF #FIRST-TIME-THRU = ' '
        {
            pnd_Year_Hold.setValue(pnd_Install_Date_A_Pnd_Install_Date_Yyyy);                                                                                             //Natural: MOVE #INSTALL-DATE-YYYY TO #YEAR-HOLD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Install_Date_A_Pnd_Install_Date_Yyyy.notEquals(pnd_Year_Hold)))                                                                                 //Natural: IF #INSTALL-DATE-YYYY NE #YEAR-HOLD
        {
            pnd_Year_Num.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #YEAR-NUM
            if (condition(pnd_Year.equals(9999)))                                                                                                                         //Natural: IF #YEAR = 9999
            {
                                                                                                                                                                          //Natural: PERFORM #REPORT-BREAKDOWN
                sub_Pnd_Report_Breakdown();
                if (condition(Global.isEscape())) {return;}
                pnd_Year_Hold.setValue(pnd_Install_Date_A_Pnd_Install_Date_Yyyy);                                                                                         //Natural: MOVE #INSTALL-DATE-YYYY TO #YEAR-HOLD
                pnd_Yearly_Instlmnts.reset();                                                                                                                             //Natural: RESET #YEARLY-INSTLMNTS #TABLE-YEAR ( 1:12 ) #TABLE-FILE ( 1:12 ) #TABLE-INV-DATE ( 1:12 )
                pnd_Table_Year.getValue(1,":",12).reset();
                pnd_Table_File.getValue(1,":",12).reset();
                pnd_Table_Inv_Date.getValue(1,":",12).reset();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Year.equals(getZero())))                                                                                                                //Natural: IF #YEAR = 0
                {
                                                                                                                                                                          //Natural: PERFORM #OVERPAYMENT-SUMMARY
                    sub_Pnd_Overpayment_Summary();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Year_Hold.setValue(pnd_Install_Date_A_Pnd_Install_Date_Yyyy);                                                                                     //Natural: MOVE #INSTALL-DATE-YYYY TO #YEAR-HOLD
                    pnd_Yearly_Instlmnts.reset();                                                                                                                         //Natural: RESET #YEARLY-INSTLMNTS #TABLE-YEAR ( 1:12 ) #TABLE-FILE ( 1:12 ) #TABLE-INV-DATE ( 1:12 )
                    pnd_Table_Year.getValue(1,":",12).reset();
                    pnd_Table_File.getValue(1,":",12).reset();
                    pnd_Table_Inv_Date.getValue(1,":",12).reset();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Year_Hold.setValue(pnd_Install_Date_A_Pnd_Install_Date_Yyyy);                                                                                     //Natural: MOVE #INSTALL-DATE-YYYY TO #YEAR-HOLD
                    pnd_Yearly_Instlmnts.reset();                                                                                                                         //Natural: RESET #YEARLY-INSTLMNTS #TABLE-YEAR ( 1:12 ) #TABLE-FILE ( 1:12 ) #TABLE-INV-DATE ( 1:12 )
                    pnd_Table_Year.getValue(1,":",12).reset();
                    pnd_Table_File.getValue(1,":",12).reset();
                    pnd_Table_Inv_Date.getValue(1,":",12).reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_First_Time_Thru.setValue("N");                                                                                                                                //Natural: ASSIGN #FIRST-TIME-THRU := 'N'
        pnd_Yearly_Instlmnts.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #YEARLY-INSTLMNTS
        pnd_Table_Year.getValue(pnd_Yearly_Instlmnts).setValue(pnd_Install_Date_A_Pnd_Install_Date_N);                                                                    //Natural: MOVE #INSTALL-DATE-N TO #TABLE-YEAR ( #YEARLY-INSTLMNTS )
        pnd_Table_File.getValue(pnd_Yearly_Instlmnts).setValue(pnd_Tab_File.getValue(pnd_I));                                                                             //Natural: MOVE #TAB-FILE ( #I ) TO #TABLE-FILE ( #YEARLY-INSTLMNTS )
        pnd_Table_Inv_Date.getValue(pnd_Yearly_Instlmnts).setValue(pnd_Tab_Inverse_Date.getValue(pnd_I));                                                                 //Natural: MOVE #TAB-INVERSE-DATE ( #I ) TO #TABLE-INV-DATE ( #YEARLY-INSTLMNTS )
    }
    private void sub_Pnd_Process_Cntrct() throws Exception                                                                                                                //Natural: #PROCESS-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W-CNTRCT
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_W_Cntrct, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT FOUND FOR ",pnd_W_Cntrct);                                                                                             //Natural: WRITE 'NO CONTRACT FOUND FOR ' #W-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  IF #W-CNTRCT = 'IC220978' OR
            //*     #W-CNTRCT = '0T183894'
            //*    WRITE '=' #W-CNTRCT
            //*    ESCAPE ROUTINE
            //*  END-IF
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero())))                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0
            {
                pnd_Dod_Date.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte());                                                                             //Natural: ASSIGN #DOD-DATE := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dod_Date.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte());                                                                              //Natural: ASSIGN #DOD-DATE := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Dod_Date_Pnd_Dod_Date_Mm.nadd(1);                                                                                                                             //Natural: ADD 1 TO #DOD-DATE-MM
        if (condition(pnd_Dod_Date_Pnd_Dod_Date_Mm.greater(12)))                                                                                                          //Natural: IF #DOD-DATE-MM > 12
        {
            pnd_Dod_Date_Pnd_Dod_Date_Yyyy.nadd(1);                                                                                                                       //Natural: ADD 1 TO #DOD-DATE-YYYY
            pnd_Dod_Date_Pnd_Dod_Date_Mm.setValue(1);                                                                                                                     //Natural: MOVE 01 TO #DOD-DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Option_Code.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde());                                                                                            //Natural: ASSIGN #OPTION-CODE := IAA-CNTRCT.CNTRCT-OPTN-CDE
    }
    private void sub_Pnd_Report_Breakdown() throws Exception                                                                                                              //Natural: #REPORT-BREAKDOWN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  IF #W-CNTRCT = 'IC220978' OR
        //*     #W-CNTRCT = '0T183894'
        //*    WRITE '=' #W-CNTRCT
        //*    ESCAPE ROUTINE
        //*  END-IF
        //*  041514
        DbsUtil.callnat(Iaan586b.class , getCurrentProcessState(), pnd_W_Cntrct, pnd_W_Payee_Cde_1, pnd_W_Payee_Cde_2, pnd_Table_Year.getValue(1,":",12),                 //Natural: CALLNAT 'IAAN586B' #W-CNTRCT #W-PAYEE-CDE-1 #W-PAYEE-CDE-2 #TABLE-YEAR ( 1:12 ) #TABLE-FILE ( 1:12 ) #TABLE-INV-DATE ( 1:12 ) #MONTH-FIELD ( 1:7,1:12 ) #DELETE-FIELD ( 1:7,1:12 ) #YEAR-NUM #YEARLY-INSTLMNTS #YEAR #OPTION-CODE #W-MODE #MSG-1 ( 1:208 ) #OV-TOT-AMT ( 1:208 ) #OV-PER-AMT ( 1:208 ) #OV-DIV-AMT ( 1:208 ) #OV-UNT-AMT ( 1:208 ) #OV-INSTL-DATE ( 1:208 ) #OV-FUND ( 1:208 ) #OVERPAY-ID-NBR
            pnd_Table_File.getValue(1,":",12), pnd_Table_Inv_Date.getValue(1,":",12), pnd_Month_Field.getValue(1,":",7,1,":",12), pnd_Delete_Field.getValue(1,":",7,1,":",12), 
            pnd_Year_Num, pnd_Yearly_Instlmnts, pnd_Year, pnd_Option_Code, pnd_W_Mode, pnd_Msg_1.getValue(1,":",208), pnd_Ov_Tot_Amt.getValue(1,":",208), 
            pnd_Ov_Per_Amt.getValue(1,":",208), pnd_Ov_Div_Amt.getValue(1,":",208), pnd_Ov_Unt_Amt.getValue(1,":",208), pnd_Ov_Instl_Date.getValue(1,":",208), 
            pnd_Ov_Fund.getValue(1,":",208), pnd_Overpay_Id_Nbr);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Overpayment_Summary() throws Exception                                                                                                           //Natural: #OVERPAYMENT-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Tot_Guar_Ov.reset();                                                                                                                                          //Natural: RESET #TOT-GUAR-OV #TOT-DIVD-OV #TOT-UNIT-OV
        pnd_Tot_Divd_Ov.reset();
        pnd_Tot_Unit_Ov.reset();
        DbsUtil.callnat(Iaan460c.class , getCurrentProcessState(), pnd_W_Cntrct, pnd_W_Payee_Cde_1, pnd_W_Payee_Cde_2, pnd_Table_Year.getValue(1,":",12),                 //Natural: CALLNAT 'IAAN460C' #W-CNTRCT #W-PAYEE-CDE-1 #W-PAYEE-CDE-2 #TABLE-YEAR ( 1:12 ) #TABLE-FILE ( 1:12 ) #TABLE-INV-DATE ( 1:12 ) #YEARLY-INSTLMNTS #TOT-GUAR-OV #TOT-DIVD-OV #TOT-UNIT-OV
            pnd_Table_File.getValue(1,":",12), pnd_Table_Inv_Date.getValue(1,":",12), pnd_Yearly_Instlmnts, pnd_Tot_Guar_Ov, pnd_Tot_Divd_Ov, pnd_Tot_Unit_Ov);
        if (condition(Global.isEscape())) return;
        pnd_Years.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #YEARS
        pnd_Table_Years_Year.getValue(pnd_Years).setValue(pnd_Year_Hold);                                                                                                 //Natural: MOVE #YEAR-HOLD TO #TABLE-YEARS-YEAR ( #YEARS )
        pnd_Table_Years_Instlmnts.getValue(pnd_Years).setValue(pnd_Yearly_Instlmnts);                                                                                     //Natural: MOVE #YEARLY-INSTLMNTS TO #TABLE-YEARS-INSTLMNTS ( #YEARS )
        pnd_Table_Years_Guar.getValue(pnd_Years).setValue(pnd_Tot_Guar_Ov);                                                                                               //Natural: MOVE #TOT-GUAR-OV TO #TABLE-YEARS-GUAR ( #YEARS )
        pnd_Table_Years_Divd.getValue(pnd_Years).setValue(pnd_Tot_Divd_Ov);                                                                                               //Natural: MOVE #TOT-DIVD-OV TO #TABLE-YEARS-DIVD ( #YEARS )
        pnd_Table_Years_Unit.getValue(pnd_Years).setValue(pnd_Tot_Unit_Ov);                                                                                               //Natural: MOVE #TOT-UNIT-OV TO #TABLE-YEARS-UNIT ( #YEARS )
        pnd_Table_Years_Totals.getValue(pnd_Years).compute(new ComputeParameters(false, pnd_Table_Years_Totals.getValue(pnd_Years)), pnd_Table_Years_Guar.getValue(pnd_Years).add(pnd_Table_Years_Divd.getValue(pnd_Years)).add(pnd_Table_Years_Unit.getValue(pnd_Years))); //Natural: COMPUTE #TABLE-YEARS-TOTALS ( #YEARS ) = #TABLE-YEARS-GUAR ( #YEARS ) + #TABLE-YEARS-DIVD ( #YEARS ) + #TABLE-YEARS-UNIT ( #YEARS )
    }
    private void sub_Pnd_Process_Cpr() throws Exception                                                                                                                   //Natural: #PROCESS-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_W_Cntrct);                                                                                                  //Natural: ASSIGN #CNTRCT-PPCN-NBR := #W-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_W_Payee_Cde_1);                                                                                            //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #W-PAYEE-CDE-1
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
        {
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CPR RECORD FOUND FOR ",pnd_W_Cntrct,pnd_W_Payee_Cde_1);                                                                         //Natural: WRITE 'NO CPR RECORD FOUND FOR ' #W-CNTRCT #W-PAYEE-CDE-1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_W_Mode.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                                                                //Natural: ASSIGN #W-MODE := CNTRCT-MODE-IND
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Tables() throws Exception                                                                                                                   //Natural: #FILL-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pnd_Num_Field.getValue(1).setValue(1);                                                                                                                            //Natural: MOVE 1 TO #NUM-FIELD ( 1 )
        pnd_Num_Field.getValue(2).setValue(2);                                                                                                                            //Natural: MOVE 2 TO #NUM-FIELD ( 2 )
        pnd_Num_Field.getValue(3).setValue(4);                                                                                                                            //Natural: MOVE 4 TO #NUM-FIELD ( 3 )
        pnd_Num_Field.getValue(4).setValue(8);                                                                                                                            //Natural: MOVE 8 TO #NUM-FIELD ( 4 )
        pnd_Num_Field.getValue(5).setValue(16);                                                                                                                           //Natural: MOVE 16 TO #NUM-FIELD ( 5 )
        pnd_Num_Field.getValue(6).setValue(32);                                                                                                                           //Natural: MOVE 32 TO #NUM-FIELD ( 6 )
        pnd_Num_Field.getValue(7).setValue(64);                                                                                                                           //Natural: MOVE 64 TO #NUM-FIELD ( 7 )
        pnd_Num_Field.getValue(8).setValue(128);                                                                                                                          //Natural: MOVE 128 TO #NUM-FIELD ( 8 )
        pnd_Num_Field.getValue(9).setValue(256);                                                                                                                          //Natural: MOVE 256 TO #NUM-FIELD ( 9 )
        pnd_Num_Field.getValue(10).setValue(512);                                                                                                                         //Natural: MOVE 512 TO #NUM-FIELD ( 10 )
        pnd_Num_Field.getValue(11).setValue(1024);                                                                                                                        //Natural: MOVE 1024 TO #NUM-FIELD ( 11 )
        pnd_Num_Field.getValue(12).setValue(2048);                                                                                                                        //Natural: MOVE 2048 TO #NUM-FIELD ( 12 )
    }
    private void sub_Pnd_Process_Tables() throws Exception                                                                                                                //Natural: #PROCESS-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #J = 1 TO 7
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(7)); pnd_J.nadd(1))
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 12
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
            {
                pnd_Month_Field.getValue(pnd_J,pnd_I).setValue(pnd_I);                                                                                                    //Natural: MOVE #I TO #MONTH-FIELD ( #J,#I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F9A:                                                                                                                                                              //Natural: FOR #J = #OVERPAY-YEARS TO 1 STEP -1
        for (pnd_J.setValue(pnd_Overpay_Years); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
        {
            F9B:                                                                                                                                                          //Natural: FOR #I = 12 TO 1 STEP -1
            for (pnd_I.setValue(12); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
            {
                //*    WRITE '=' #I '=' #NUM-FIELD(#I) '=' #NUMB(#J)
                if (condition(pnd_Num_Field.getValue(pnd_I).lessOrEqual(pnd_Numb.getValue(pnd_J))))                                                                       //Natural: IF #NUM-FIELD ( #I ) <= #NUMB ( #J )
                {
                    pnd_Delete_Field.getValue(pnd_J,pnd_I).setValue("D");                                                                                                 //Natural: MOVE 'D' TO #DELETE-FIELD ( #J,#I )
                    pnd_Numb.getValue(pnd_J).nsubtract(pnd_Num_Field.getValue(pnd_I));                                                                                    //Natural: COMPUTE #NUMB ( #J ) = #NUMB ( #J ) - #NUM-FIELD ( #I )
                    if (condition(pnd_Numb.getValue(pnd_J).equals(getZero())))                                                                                            //Natural: IF #NUMB ( #J ) = 0
                    {
                        if (true) break F9B;                                                                                                                              //Natural: ESCAPE BOTTOM ( F9B. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F9A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F9A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *FOR #J = 1 TO 7
        //*   FOR #I = 1 TO 12
        //*  WRITE '=' #MONTH-FIELD(#J,#I) '=' #DELETE-FIELD(#J,#I)
        //*   END-FOR
        //* *END-FOR
    }

    //
}
