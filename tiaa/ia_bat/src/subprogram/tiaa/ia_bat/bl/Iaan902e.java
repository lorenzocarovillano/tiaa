/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:06 AM
**        * FROM NATURAL SUBPROGRAM : Iaan902e
************************************************************
**        * FILE NAME            : Iaan902e.java
**        * CLASS NAME           : Iaan902e
**        * INSTANCE NAME        : Iaan902e
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN902E CREATES MULTIPLE DED TRANS CODING SHEETS *
*      DATE     -  8/94                                              *
*                                                                    *
**********************************************************************
* CHANGE HISTORY
* CHANGED ON APRIL 10, 1995 BY D ROBINSON
* >   REMOVED COMMENTED CODE
* >
* >
* > 04/05/2017 - RCC RESTOWED FOR PIN EXPANSION
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan902e extends BLNatBase
{
    // Data Areas
    private LdaIaal902e ldaIaal902e;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Passed_Data;
    private DbsField pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Passed_Data__R_Field_1;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8;
    private DbsField pnd_Passed_Data_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_2;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_3;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Area;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Id;
    private DbsField pnd_Passed_Data_Pnd_Trans_Verify_Id;
    private DbsField pnd_Passed_Data_Pnd_Trans_Cmbne_Cde;

    private DbsGroup pnd_Passed_Data__R_Field_4;
    private DbsField pnd_Passed_Data_Pnd_Trans_Seq_Ddctn_Cde;
    private DbsField pnd_Passed_Data_Pnd_Filler;
    private DbsField pnd_Passed_Data_Pnd_Last_Batch_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Reason;
    private DbsField pnd_Passed_Data_Pnd_Records_Bypassed_Ctr;
    private DbsField pnd_Passed_Data_Pnd_Records_Processed_Ctr;
    private DbsField pnd_Passed_Data_Pnd_Save_Trans_Ppcn_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Save_Trans_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Save_Trans_Cde;
    private DbsField pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl;

    private DataAccessProgramView vw_iaa_Ded;
    private DbsField iaa_Ded_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ded_Ddctn_Payee_Cde;
    private DbsField iaa_Ded_Ddctn_Cde;
    private DbsField iaa_Ded_Ddctn_Seq_Nbr;
    private DbsField iaa_Ded_Ddctn_Stp_Dte;
    private DbsField iaa_Ded_Ddctn_Final_Dte;
    private DbsField pnd_Iaa_Ded_Key;

    private DbsGroup pnd_Iaa_Ded_Key__R_Field_5;
    private DbsField pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Payee_Cde;
    private DbsField pnd_Iaa_Ded_Seq;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal902e = new LdaIaal902e();
        registerRecord(ldaIaal902e);
        registerRecord(ldaIaal902e.getVw_iaa_Cntrct());
        registerRecord(ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal902e.getVw_iaa_Deduction());
        registerRecord(ldaIaal902e.getVw_iaa_Ddctn_Trans());

        // parameters
        parameters = new DbsRecord();

        pnd_Passed_Data = parameters.newGroupInRecord("pnd_Passed_Data", "#PASSED-DATA");
        pnd_Passed_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte", "#CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Passed_Data__R_Field_1 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_1", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8 = pnd_Passed_Data__R_Field_1.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8", "#TRANS-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Passed_Data_Pnd_Trans_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Passed_Data_Pnd_Trans_Check_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_2 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_2", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Check_Dte);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc", "#TRANS-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy", "#TRANS-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm", "#TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd", "#TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_3 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_3", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Effctve_Dte);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc", "#TRANS-EFFCTVE-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy", "#TRANS-EFFCTVE-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm", "#TRANS-EFFCTVE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd", "#TRANS-EFFCTVE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_User_Area = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Area", "#TRANS-USER-AREA", FieldType.STRING, 
            6);
        pnd_Passed_Data_Pnd_Trans_User_Id = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Id", "#TRANS-USER-ID", FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Verify_Id = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Verify_Id", "#TRANS-VERIFY-ID", FieldType.STRING, 
            8);
        pnd_Passed_Data_Pnd_Trans_Cmbne_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Cmbne_Cde", "#TRANS-CMBNE-CDE", FieldType.STRING, 
            12);

        pnd_Passed_Data__R_Field_4 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_4", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Cmbne_Cde);
        pnd_Passed_Data_Pnd_Trans_Seq_Ddctn_Cde = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Seq_Ddctn_Cde", "#TRANS-SEQ-DDCTN-CDE", 
            FieldType.STRING, 6);
        pnd_Passed_Data_Pnd_Filler = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Filler", "#FILLER", FieldType.STRING, 6);
        pnd_Passed_Data_Pnd_Last_Batch_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Passed_Data_Pnd_Reason = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Reason", "#REASON", FieldType.STRING, 2);
        pnd_Passed_Data_Pnd_Records_Bypassed_Ctr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Records_Bypassed_Ctr", "#RECORDS-BYPASSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Passed_Data_Pnd_Records_Processed_Ctr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Records_Processed_Ctr", "#RECORDS-PROCESSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Passed_Data_Pnd_Save_Trans_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_Trans_Ppcn_Nbr", "#SAVE-TRANS-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Passed_Data_Pnd_Save_Trans_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_Trans_Payee_Cde", "#SAVE-TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Save_Trans_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Save_Trans_Cde", "#SAVE-TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl = pnd_Passed_Data.newFieldArrayInGroup("pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl", "#SEQ-DDCTN-TBL", FieldType.STRING, 
            6, new DbsArrayController(1, 5));
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Ded = new DataAccessProgramView(new NameInfo("vw_iaa_Ded", "IAA-DED"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Ded_Ddctn_Ppcn_Nbr = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "DDCTN_PPCN_NBR");
        iaa_Ded_Ddctn_Payee_Cde = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE_CDE");
        iaa_Ded_Ddctn_Cde = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Ded_Ddctn_Seq_Nbr = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "DDCTN_SEQ_NBR");
        iaa_Ded_Ddctn_Stp_Dte = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_STP_DTE");
        iaa_Ded_Ddctn_Final_Dte = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Ded);

        pnd_Iaa_Ded_Key = localVariables.newFieldInRecord("pnd_Iaa_Ded_Key", "#IAA-DED-KEY", FieldType.STRING, 12);

        pnd_Iaa_Ded_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Iaa_Ded_Key__R_Field_5", "REDEFINE", pnd_Iaa_Ded_Key);
        pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Ppcn_Nbr = pnd_Iaa_Ded_Key__R_Field_5.newFieldInGroup("pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Ppcn_Nbr", "#IAA-DDCTN-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Payee_Cde = pnd_Iaa_Ded_Key__R_Field_5.newFieldInGroup("pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Payee_Cde", "#IAA-DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Ded_Seq = localVariables.newFieldInRecord("pnd_Iaa_Ded_Seq", "#IAA-DED-SEQ", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Ded.reset();

        ldaIaal902e.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan902e() throws Exception
    {
        super("Iaan902e");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: DECIDE ON FIRST VALUE OF #TRANS-CDE
        short decideConditionsMet250 = 0;                                                                                                                                 //Natural: VALUE 902
        if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(902))))
        {
            decideConditionsMet250++;
            FOR01:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 5
            for (ldaIaal902e.getPnd_Indx().setValue(1); condition(ldaIaal902e.getPnd_Indx().lessOrEqual(5)); ldaIaal902e.getPnd_Indx().nadd(1))
            {
                if (condition(pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).equals(pnd_Passed_Data_Pnd_Trans_Seq_Ddctn_Cde) &&                    //Natural: IF #SEQ-DDCTN-TBL ( #INDX ) = #TRANS-SEQ-DDCTN-CDE AND #SEQ-DDCTN-TBL ( #INDX ) NE ' '
                    pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).notEquals(" ")))
                {
                    pnd_Passed_Data_Pnd_Reason.setValue("D ");                                                                                                            //Natural: ASSIGN #REASON := 'D '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).equals(" ")))                                                     //Natural: IF #SEQ-DDCTN-TBL ( #INDX ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Check_Dte_Mm().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm);                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#CHECK-DTE-MM := #TRANS-CHECK-DTE-MM
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Check_Dte_Dd().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd);                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#CHECK-DTE-DD := #TRANS-CHECK-DTE-DD
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Check_Dte_Yy().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy);                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#CHECK-DTE-YY := #TRANS-CHECK-DTE-YY
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Cntrct_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8);                                                            //Natural: ASSIGN #DDCTN-FROM-NET.#CNTRCT-NBR := #TRANS-PPCN-NBR-8
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Record_Status().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                          //Natural: ASSIGN #DDCTN-FROM-NET.#RECORD-STATUS := #TRANS-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT
            sub_Find_Iaa_Cntrct();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR-902
            sub_Get_Cross_Reference_Nbr_902();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Trans_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                    //Natural: ASSIGN #DDCTN-FROM-NET.#TRANS-NBR := #TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-902
            sub_Determine_Intent_Cde_902();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().equals(" ")))                                                                               //Natural: IF #DDCTN-FROM-NET.#INTENT-CODE = ' '
            {
                pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                                //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Batch_Nbr().compute(new ComputeParameters(false, ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Batch_Nbr()),                    //Natural: COMPUTE #DDCTN-FROM-NET.#BATCH-NBR = #LAST-BATCH-NBR + 1
                pnd_Passed_Data_Pnd_Last_Batch_Nbr.add(1));
            pnd_Passed_Data_Pnd_Last_Batch_Nbr.setValue(ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Batch_Nbr());                                                               //Natural: ASSIGN #LAST-BATCH-NBR := #DDCTN-FROM-NET.#BATCH-NBR
                                                                                                                                                                          //Natural: PERFORM DEDUCTION-SEQUENCE-NBR
            sub_Deduction_Sequence_Nbr();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Sequence_Nbr().setValue(pnd_Iaa_Ded_Seq);                                                                               //Natural: ASSIGN #DDCTN-FROM-NET.#SEQUENCE-NBR := #IAA-DED-SEQ
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Ddctn_Cde().setValue(ldaIaal902e.getIaa_Deduction_Ddctn_Cde());                                                         //Natural: ASSIGN #DDCTN-FROM-NET.#DDCTN-CDE := IAA-DEDUCTION.DDCTN-CDE
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Ddctn_Payee().setValue(ldaIaal902e.getIaa_Deduction_Ddctn_Payee());                                                     //Natural: ASSIGN #DDCTN-FROM-NET.#DDCTN-PAYEE := IAA-DEDUCTION.DDCTN-PAYEE
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N().setValue(ldaIaal902e.getIaa_Deduction_Ddctn_Per_Amt());                                               //Natural: ASSIGN #DDCTN-FROM-NET.#PER-DDCTN-AMT-N := IAA-DEDUCTION.DDCTN-PER-AMT
            if (condition(ldaIaal902e.getIaa_Deduction_Ddctn_Tot_Amt().equals(getZero()) || ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().equals("2")))             //Natural: IF IAA-DEDUCTION.DDCTN-TOT-AMT = 0 OR #DDCTN-FROM-NET.#INTENT-CODE = '2'
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn().setValue(" ");                                                                                          //Natural: ASSIGN #DDCTN-FROM-NET.#TOT-DDCTN := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N().setValue(ldaIaal902e.getIaa_Deduction_Ddctn_Tot_Amt());                                               //Natural: ASSIGN #DDCTN-FROM-NET.#TOT-DDCTN-N := IAA-DEDUCTION.DDCTN-TOT-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().equals("3")))                                                                               //Natural: IF #DDCTN-FROM-NET.#INTENT-CODE = '3'
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Ddctn_Payee().setValue(" ");                                                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#DDCTN-PAYEE := ' '
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt().setValue(" ");                                                                                      //Natural: ASSIGN #DDCTN-FROM-NET.#PER-DDCTN-AMT := ' '
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn().setValue(" ");                                                                                          //Natural: ASSIGN #DDCTN-FROM-NET.#TOT-DDCTN := ' '
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte().setValue(" ");                                                                                    //Natural: ASSIGN #FINAL-DDCTN-DTE := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().equals("2")))                                                                           //Natural: IF #DDCTN-FROM-NET.#INTENT-CODE = '2'
                {
                    ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte().setValue(" ");                                                                                //Natural: ASSIGN #DDCTN-FROM-NET.#FINAL-DDCTN-DTE := ' '
                    ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn().setValue(" ");                                                                                      //Natural: ASSIGN #DDCTN-FROM-NET.#TOT-DDCTN := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal902e.getIaa_Deduction_Ddctn_Final_Dte().equals(getZero())))                                                                      //Natural: IF IAA-DEDUCTION.DDCTN-FINAL-DTE = 0
                    {
                        ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte().setValue(" ");                                                                            //Natural: ASSIGN #FINAL-DDCTN-DTE := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal902e.getPnd_Ddctn_Final_Dte().setValue(ldaIaal902e.getIaa_Deduction_Ddctn_Final_Dte());                                                    //Natural: ASSIGN #DDCTN-FINAL-DTE := IAA-DEDUCTION.DDCTN-FINAL-DTE
                        ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm().setValue(ldaIaal902e.getPnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Mm());                 //Natural: ASSIGN #FINAL-DDCTN-DTE-MM := #DDCTN-FINAL-DTE-MM
                        ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy().setValue(ldaIaal902e.getPnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Yy());                 //Natural: ASSIGN #FINAL-DDCTN-DTE-YY := #DDCTN-FINAL-DTE-YY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_User_Area().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                              //Natural: ASSIGN #DDCTN-FROM-NET.#USER-AREA := #TRANS-USER-AREA
            getWorkFiles().write(1, false, ldaIaal902e.getPnd_Ddctn_From_Net());                                                                                          //Natural: WRITE WORK FILE 1 #DDCTN-FROM-NET
            pnd_Passed_Data_Pnd_Records_Processed_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
            pnd_Passed_Data_Pnd_Save_Trans_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                         //Natural: ASSIGN #SAVE-TRANS-PPCN-NBR := #TRANS-PPCN-NBR
            pnd_Passed_Data_Pnd_Save_Trans_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                       //Natural: ASSIGN #SAVE-TRANS-PAYEE-CDE := #TRANS-PAYEE-CDE
            pnd_Passed_Data_Pnd_Save_Trans_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                                   //Natural: ASSIGN #SAVE-TRANS-CDE := #TRANS-CDE
        }                                                                                                                                                                 //Natural: VALUE 906
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(906))))
        {
            decideConditionsMet250++;
            FOR02:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 5
            for (ldaIaal902e.getPnd_Indx().setValue(1); condition(ldaIaal902e.getPnd_Indx().lessOrEqual(5)); ldaIaal902e.getPnd_Indx().nadd(1))
            {
                if (condition(pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).equals(pnd_Passed_Data_Pnd_Trans_Seq_Ddctn_Cde) &&                    //Natural: IF #SEQ-DDCTN-TBL ( #INDX ) = #TRANS-SEQ-DDCTN-CDE AND #SEQ-DDCTN-TBL ( #INDX ) NE ' '
                    pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).notEquals(" ")))
                {
                    pnd_Passed_Data_Pnd_Reason.setValue("D ");                                                                                                            //Natural: ASSIGN #REASON := 'D '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).equals(" ")))                                                     //Natural: IF #SEQ-DDCTN-TBL ( #INDX ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm);                                                         //Natural: ASSIGN #HIS-YTD-DDCTN.#CHECK-DTE-MM := #TRANS-CHECK-DTE-MM
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd);                                                         //Natural: ASSIGN #HIS-YTD-DDCTN.#CHECK-DTE-DD := #TRANS-CHECK-DTE-DD
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy);                                                         //Natural: ASSIGN #HIS-YTD-DDCTN.#CHECK-DTE-YY := #TRANS-CHECK-DTE-YY
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8);                                                             //Natural: ASSIGN #HIS-YTD-DDCTN.#CNTRCT-NBR := #TRANS-PPCN-NBR-8
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Record_Status().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                           //Natural: ASSIGN #HIS-YTD-DDCTN.#RECORD-STATUS := #TRANS-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT
            sub_Find_Iaa_Cntrct();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR-906
            sub_Get_Cross_Reference_Nbr_906();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Trans_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                     //Natural: ASSIGN #HIS-YTD-DDCTN.#TRANS-NBR := #TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-906
            sub_Determine_Intent_Cde_906();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Intent_Code().equals(" ")))                                                                                //Natural: IF #HIS-YTD-DDCTN.#INTENT-CODE = ' '
            {
                pnd_Passed_Data_Pnd_Reason.setValue("I ");                                                                                                                //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Batch_Nbr().compute(new ComputeParameters(false, ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Batch_Nbr()), pnd_Passed_Data_Pnd_Last_Batch_Nbr.add(1)); //Natural: COMPUTE #HIS-YTD-DDCTN.#BATCH-NBR = #LAST-BATCH-NBR + 1
            pnd_Passed_Data_Pnd_Last_Batch_Nbr.setValue(ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Batch_Nbr());                                                                //Natural: ASSIGN #LAST-BATCH-NBR := #HIS-YTD-DDCTN.#BATCH-NBR
                                                                                                                                                                          //Natural: PERFORM DEDUCTION-SEQUENCE-NBR
            sub_Deduction_Sequence_Nbr();
            if (condition(Global.isEscape())) {return;}
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr().setValue(pnd_Iaa_Ded_Seq);                                                                                //Natural: ASSIGN #HIS-YTD-DDCTN.#SEQUENCE-NBR := #IAA-DED-SEQ
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde().setValue(ldaIaal902e.getIaa_Deduction_Ddctn_Cde());                                                          //Natural: ASSIGN #HIS-YTD-DDCTN.#DDCTN-CDE := IAA-DEDUCTION.DDCTN-CDE
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee().setValue(ldaIaal902e.getIaa_Deduction_Ddctn_Payee());                                                      //Natural: ASSIGN #HIS-YTD-DDCTN.#DDCTN-PAYEE := IAA-DEDUCTION.DDCTN-PAYEE
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_User_Area().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                               //Natural: ASSIGN #HIS-YTD-DDCTN.#USER-AREA := #TRANS-USER-AREA
            ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde_N().nadd(50);                                                                                                  //Natural: COMPUTE #HIS-YTD-DDCTN.#DDCTN-CDE-N = #HIS-YTD-DDCTN.#DDCTN-CDE-N + 50
            getWorkFiles().write(1, false, ldaIaal902e.getPnd_His_Ytd_Ddctn());                                                                                           //Natural: WRITE WORK FILE 1 #HIS-YTD-DDCTN
            pnd_Passed_Data_Pnd_Records_Processed_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
            pnd_Passed_Data_Pnd_Save_Trans_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                         //Natural: ASSIGN #SAVE-TRANS-PPCN-NBR := #TRANS-PPCN-NBR
            pnd_Passed_Data_Pnd_Save_Trans_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                       //Natural: ASSIGN #SAVE-TRANS-PAYEE-CDE := #TRANS-PAYEE-CDE
            pnd_Passed_Data_Pnd_Save_Trans_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                                   //Natural: ASSIGN #SAVE-TRANS-CDE := #TRANS-CDE
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR-902
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR-906
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-902
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-906
        //*    #SEQ-DDCTN-TBL(#INDX) := IAA-DDCTN-TRANS.DDCTN-SEQ-CDE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEDUCTION-SEQUENCE-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASS-TRANSACTION
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902e.getPnd_Ddctn_From_Net().reset();                                                                                                                      //Natural: RESET #DDCTN-FROM-NET #HIS-YTD-DDCTN #NO-CNTRCT-REC
        ldaIaal902e.getPnd_His_Ytd_Ddctn().reset();
        ldaIaal902e.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().reset();
        if (condition(pnd_Passed_Data_Pnd_Save_Trans_Cde.equals(pnd_Passed_Data_Pnd_Trans_Cde)))                                                                          //Natural: IF #SAVE-TRANS-CDE = #TRANS-CDE
        {
            if (condition(pnd_Passed_Data_Pnd_Save_Trans_Ppcn_Nbr.equals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) && pnd_Passed_Data_Pnd_Save_Trans_Payee_Cde.equals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF #SAVE-TRANS-PPCN-NBR = #TRANS-PPCN-NBR AND #SAVE-TRANS-PAYEE-CDE = #TRANS-PAYEE-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue("*").reset();                                                                                                  //Natural: RESET #SEQ-DDCTN-TBL ( * )
                ldaIaal902e.getPnd_Indx().setValue(1);                                                                                                                    //Natural: ASSIGN #INDX := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue("*").reset();                                                                                                      //Natural: RESET #SEQ-DDCTN-TBL ( * )
            ldaIaal902e.getPnd_Indx().setValue(1);                                                                                                                        //Natural: ASSIGN #INDX := 1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Find_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: FIND-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902e.getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                             //Natural: ASSIGN #IAA-CNTRCT-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902e.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal902e.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal902e.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902e.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA CONTRACT RECORD FOR : ",pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                //Natural: WRITE 'NO IAA CONTRACT RECORD FOR : ' #TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal902e.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                  //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cross_Reference_Nbr_902() throws Exception                                                                                                       //Natural: GET-CROSS-REFERENCE-NBR-902
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet411 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(1))))
        {
            decideConditionsMet411++;
            if (condition(ldaIaal902e.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #DDCTN-FROM-NET.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(2))))
        {
            decideConditionsMet411++;
            if (condition(ldaIaal902e.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #DDCTN-FROM-NET.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #DDCTN-FROM-NET.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #DDCTN-FROM-NET.#CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                    //Natural: ASSIGN #DDCTN-FROM-NET.#CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((pnd_Passed_Data_Pnd_Trans_Payee_Cde.greaterOrEqual(3) && pnd_Passed_Data_Pnd_Trans_Payee_Cde.lessOrEqual(99)))))
        {
            decideConditionsMet411++;
            ldaIaal902e.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                            //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal902e.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                          //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
            (
            "FIND02",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902e.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
            1
            );
            FIND02:
            while (condition(ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
            {
                ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                                 //Natural: ASSIGN #DDCTN-FROM-NET.#CROSS-REF-NBR := BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Cross_Reference_Nbr_906() throws Exception                                                                                                       //Natural: GET-CROSS-REFERENCE-NBR-906
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet454 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(1))))
        {
            decideConditionsMet454++;
            if (condition(ldaIaal902e.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                    //Natural: ASSIGN #HIS-YTD-DDCTN.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(2))))
        {
            decideConditionsMet454++;
            if (condition(ldaIaal902e.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                    //Natural: ASSIGN #HIS-YTD-DDCTN.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                    //Natural: ASSIGN #HIS-YTD-DDCTN.#CROSS-REF-NBR := CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                    //Natural: ASSIGN #HIS-YTD-DDCTN.#CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal902e.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                     //Natural: ASSIGN #HIS-YTD-DDCTN.#CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((pnd_Passed_Data_Pnd_Trans_Payee_Cde.greaterOrEqual(3) && pnd_Passed_Data_Pnd_Trans_Payee_Cde.lessOrEqual(99)))))
        {
            decideConditionsMet454++;
            ldaIaal902e.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                            //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal902e.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                          //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
            (
            "FIND03",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902e.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
            1
            );
            FIND03:
            while (condition(ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND03", true)))
            {
                ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal902e.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr().setValue(ldaIaal902e.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                                  //Natural: ASSIGN #HIS-YTD-DDCTN.#CROSS-REF-NBR := BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Intent_Cde_902() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-902
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                               //Natural: ASSIGN #DDCTN-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                              //Natural: ASSIGN #DDCTN-BFRE-KEY.#DDCTN-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                            //Natural: ASSIGN #DDCTN-BFRE-KEY.#DDCTN-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Seq_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Seq_Ddctn_Cde);                                                          //Natural: ASSIGN #DDCTN-BFRE-KEY.#DDCTN-SEQ-CDE := #TRANS-SEQ-DDCTN-CDE
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                             //Natural: ASSIGN #DDCTN-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
        ldaIaal902e.getVw_iaa_Ddctn_Trans().startDatabaseRead                                                                                                             //Natural: READ ( 1 ) IAA-DDCTN-TRANS BY DDCTN-BFRE-KEY STARTING FROM #DDCTN-BFRE-KEY
        (
        "READ01",
        new Wc[] { new Wc("DDCTN_BFRE_KEY", ">=", ldaIaal902e.getPnd_Ddctn_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("DDCTN_BFRE_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIaal902e.getVw_iaa_Ddctn_Trans().readNextRow("READ01")))
        {
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Ppcn_Nbr());                                     //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-PPCN-NBR := IAA-DDCTN-TRANS.DDCTN-PPCN-NBR
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Payee_Cde());                                   //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-PAYEE-CDE := IAA-DDCTN-TRANS.DDCTN-PAYEE-CDE
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Seq_Nbr());                                       //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-SEQ-NBR := IAA-DDCTN-TRANS.DDCTN-SEQ-NBR
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Cde().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Cde());                                               //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-CDE := IAA-DDCTN-TRANS.DDCTN-CDE
            FOR03:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 5
            for (ldaIaal902e.getPnd_Indx().setValue(1); condition(ldaIaal902e.getPnd_Indx().lessOrEqual(5)); ldaIaal902e.getPnd_Indx().nadd(1))
            {
                if (condition(pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).equals(" ")))                                                         //Natural: IF #SEQ-DDCTN-TBL ( #INDX ) = ' '
                {
                    pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Seq_Cde());                       //Natural: ASSIGN #SEQ-DDCTN-TBL ( #INDX ) := IAA-DDCTN-TRANS.DDCTN-SEQ-CDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal902e.getVw_iaa_Deduction().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) IAA-DEDUCTION WITH CNTRCT-PAYEE-DDCTN-KEY = #CNTRCT-PAYEE-DDCTN-KEY
        (
        "FIND04",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", "=", ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key(), WcType.WITH) },
        1
        );
        FIND04:
        while (condition(ldaIaal902e.getVw_iaa_Deduction().readNextRow("FIND04", true)))
        {
            ldaIaal902e.getVw_iaa_Deduction().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902e.getVw_iaa_Deduction().getAstCOUNTER().equals(0)))                                                                                   //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO DDCTN RECORD FOR : ",ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key());                                                              //Natural: WRITE 'NO DDCTN RECORD FOR : ' #CNTRCT-PAYEE-DDCTN-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition((ldaIaal902e.getIaa_Deduction_Ddctn_Per_Amt().notEquals(getZero()) || ldaIaal902e.getIaa_Deduction_Ddctn_Tot_Amt().notEquals(getZero())         //Natural: IF ( IAA-DEDUCTION.DDCTN-PER-AMT NE 0 OR IAA-DEDUCTION.DDCTN-TOT-AMT NE 0 OR IAA-DEDUCTION.DDCTN-FINAL-DTE NE 0 ) AND IAA-DDCTN-TRANS.DDCTN-PAYEE = 'START'
                || ldaIaal902e.getIaa_Deduction_Ddctn_Final_Dte().notEquals(getZero())) && ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Payee().equals("START")))
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#INTENT-CODE := '1'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaIaal902e.getIaa_Deduction_Ddctn_Per_Amt().notEquals(getZero()) || ldaIaal902e.getIaa_Deduction_Ddctn_Tot_Amt().notEquals(getZero())         //Natural: IF ( IAA-DEDUCTION.DDCTN-PER-AMT NE 0 OR IAA-DEDUCTION.DDCTN-TOT-AMT NE 0 OR IAA-DEDUCTION.DDCTN-FINAL-DTE NE 0 ) AND ( IAA-DDCTN-TRANS.DDCTN-PER-AMT = 0 AND IAA-DDCTN-TRANS.DDCTN-TOT-AMT = 0 AND IAA-DDCTN-TRANS.DDCTN-FINAL-DTE = 0 )
                || ldaIaal902e.getIaa_Deduction_Ddctn_Final_Dte().notEquals(getZero())) && (ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Per_Amt().equals(getZero()) 
                && ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Tot_Amt().equals(getZero()) && ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Final_Dte().equals(getZero()))))
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#INTENT-CODE := '1'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Deduction_Ddctn_Stp_Dte().equals(pnd_Passed_Data_Pnd_Trans_Check_Dte)))                                                      //Natural: IF IAA-DEDUCTION.DDCTN-STP-DTE = #PASSED-DATA.#TRANS-CHECK-DTE
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().setValue("3");                                                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#INTENT-CODE := '3'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Deduction_Ddctn_Per_Amt().notEquals(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Per_Amt()) || ldaIaal902e.getIaa_Deduction_Ddctn_Tot_Amt().notEquals(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Tot_Amt())  //Natural: IF IAA-DEDUCTION.DDCTN-PER-AMT NE IAA-DDCTN-TRANS.DDCTN-PER-AMT OR IAA-DEDUCTION.DDCTN-TOT-AMT NE IAA-DDCTN-TRANS.DDCTN-TOT-AMT OR IAA-DEDUCTION.DDCTN-FINAL-DTE NE IAA-DDCTN-TRANS.DDCTN-FINAL-DTE
                || ldaIaal902e.getIaa_Deduction_Ddctn_Final_Dte().notEquals(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Final_Dte())))
            {
                ldaIaal902e.getPnd_Ddctn_From_Net_Pnd_Intent_Code().setValue("2");                                                                                        //Natural: ASSIGN #DDCTN-FROM-NET.#INTENT-CODE := '2'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Intent_Cde_906() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-906
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                               //Natural: ASSIGN #DDCTN-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                              //Natural: ASSIGN #DDCTN-BFRE-KEY.#DDCTN-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                            //Natural: ASSIGN #DDCTN-BFRE-KEY.#DDCTN-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Seq_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Seq_Ddctn_Cde);                                                          //Natural: ASSIGN #DDCTN-BFRE-KEY.#DDCTN-SEQ-CDE := #TRANS-SEQ-DDCTN-CDE
        ldaIaal902e.getPnd_Ddctn_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                             //Natural: ASSIGN #DDCTN-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
        ldaIaal902e.getVw_iaa_Ddctn_Trans().startDatabaseRead                                                                                                             //Natural: READ ( 1 ) IAA-DDCTN-TRANS BY DDCTN-BFRE-KEY STARTING FROM #DDCTN-BFRE-KEY
        (
        "READ02",
        new Wc[] { new Wc("DDCTN_BFRE_KEY", ">=", ldaIaal902e.getPnd_Ddctn_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("DDCTN_BFRE_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(ldaIaal902e.getVw_iaa_Ddctn_Trans().readNextRow("READ02")))
        {
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Ppcn_Nbr());                                     //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-PPCN-NBR := IAA-DDCTN-TRANS.DDCTN-PPCN-NBR
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Payee_Cde());                                   //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-PAYEE-CDE := IAA-DDCTN-TRANS.DDCTN-PAYEE-CDE
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Seq_Nbr());                                       //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-SEQ-NBR := IAA-DDCTN-TRANS.DDCTN-SEQ-NBR
            ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Cde().setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Cde());                                               //Natural: ASSIGN #CNTRCT-PAYEE-DDCTN-KEY.#DDCTN-CDE := IAA-DDCTN-TRANS.DDCTN-CDE
            FOR04:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 5
            for (ldaIaal902e.getPnd_Indx().setValue(1); condition(ldaIaal902e.getPnd_Indx().lessOrEqual(5)); ldaIaal902e.getPnd_Indx().nadd(1))
            {
                if (condition(pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).equals(" ")))                                                         //Natural: IF #SEQ-DDCTN-TBL ( #INDX ) = ' '
                {
                    pnd_Passed_Data_Pnd_Seq_Ddctn_Tbl.getValue(ldaIaal902e.getPnd_Indx()).setValue(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Seq_Cde());                       //Natural: ASSIGN #SEQ-DDCTN-TBL ( #INDX ) := IAA-DDCTN-TRANS.DDCTN-SEQ-CDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal902e.getVw_iaa_Deduction().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) IAA-DEDUCTION WITH CNTRCT-PAYEE-DDCTN-KEY = #CNTRCT-PAYEE-DDCTN-KEY
        (
        "FIND05",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", "=", ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key(), WcType.WITH) },
        1
        );
        FIND05:
        while (condition(ldaIaal902e.getVw_iaa_Deduction().readNextRow("FIND05", true)))
        {
            ldaIaal902e.getVw_iaa_Deduction().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902e.getVw_iaa_Deduction().getAstCOUNTER().equals(0)))                                                                                   //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO DDCTN RECORD FOR : ",ldaIaal902e.getPnd_Cntrct_Payee_Ddctn_Key());                                                              //Natural: WRITE 'NO DDCTN RECORD FOR : ' #CNTRCT-PAYEE-DDCTN-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaIaal902e.getIaa_Deduction_Ddctn_Ytd_Amt().greater(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Ytd_Amt())))                                          //Natural: IF IAA-DEDUCTION.DDCTN-YTD-AMT > IAA-DDCTN-TRANS.DDCTN-YTD-AMT
            {
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N().compute(new ComputeParameters(false, ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N()),      //Natural: COMPUTE #HIS-DDCTN-AMT-N = IAA-DEDUCTION.DDCTN-YTD-AMT - IAA-DDCTN-TRANS.DDCTN-YTD-AMT
                    ldaIaal902e.getIaa_Deduction_Ddctn_Ytd_Amt().subtract(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Ytd_Amt()));
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Intent_Code().setValue("1");                                                                                         //Natural: ASSIGN #HIS-YTD-DDCTN.#INTENT-CODE := '1'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902e.getIaa_Deduction_Ddctn_Ytd_Amt().less(ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Ytd_Amt())))                                             //Natural: IF IAA-DEDUCTION.DDCTN-YTD-AMT < IAA-DDCTN-TRANS.DDCTN-YTD-AMT
            {
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N().compute(new ComputeParameters(false, ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N()),      //Natural: COMPUTE #HIS-DDCTN-AMT-N = IAA-DDCTN-TRANS.DDCTN-YTD-AMT - IAA-DEDUCTION.DDCTN-YTD-AMT
                    ldaIaal902e.getIaa_Ddctn_Trans_Ddctn_Ytd_Amt().subtract(ldaIaal902e.getIaa_Deduction_Ddctn_Ytd_Amt()));
                ldaIaal902e.getPnd_His_Ytd_Ddctn_Pnd_Intent_Code().setValue("2");                                                                                         //Natural: ASSIGN #HIS-YTD-DDCTN.#INTENT-CODE := '2'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Deduction_Sequence_Nbr() throws Exception                                                                                                            //Natural: DEDUCTION-SEQUENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                              //Natural: ASSIGN #IAA-DDCTN-PPCN-NBR := #TRANS-PPCN-NBR
        pnd_Iaa_Ded_Key_Pnd_Iaa_Ddctn_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                            //Natural: ASSIGN #IAA-DDCTN-PAYEE-CDE := #TRANS-PAYEE-CDE
        vw_iaa_Ded.startDatabaseRead                                                                                                                                      //Natural: READ IAA-DED BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #IAA-DED-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Iaa_Ded_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Ded.readNextRow("READ03")))
        {
            if (condition(iaa_Ded_Ddctn_Ppcn_Nbr.notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) || iaa_Ded_Ddctn_Payee_Cde.notEquals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF IAA-DED.DDCTN-PPCN-NBR NE #TRANS-PPCN-NBR OR IAA-DED.DDCTN-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                pnd_Iaa_Ded_Seq.setValue(0);                                                                                                                              //Natural: ASSIGN #IAA-DED-SEQ := 0
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Ded_Ddctn_Stp_Dte.greater(getZero()) && iaa_Ded_Ddctn_Stp_Dte.less(199501)))                                                                //Natural: IF IAA-DED.DDCTN-STP-DTE GT 0 AND IAA-DED.DDCTN-STP-DTE LT 199501
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Iaa_Ded_Seq.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #IAA-DED-SEQ
            if (condition(iaa_Ded_Ddctn_Seq_Nbr.equals(ldaIaal902e.getIaa_Deduction_Ddctn_Seq_Nbr()) && iaa_Ded_Ddctn_Cde.equals(ldaIaal902e.getIaa_Deduction_Ddctn_Cde()))) //Natural: IF IAA-DED.DDCTN-SEQ-NBR EQ IAA-DEDUCTION.DDCTN-SEQ-NBR AND IAA-DED.DDCTN-CDE EQ IAA-DEDUCTION.DDCTN-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Bypass_Transaction() throws Exception                                                                                                                //Natural: BYPASS-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Passed_Data_Pnd_Records_Bypassed_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
        getWorkFiles().write(9, false, pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr, pnd_Passed_Data_Pnd_Trans_Payee_Cde, pnd_Passed_Data_Pnd_Trans_Cde, pnd_Passed_Data_Pnd_Trans_User_Area,  //Natural: WRITE WORK FILE 9 #PASSED-DATA.#TRANS-PPCN-NBR #PASSED-DATA.#TRANS-PAYEE-CDE #PASSED-DATA.#TRANS-CDE #PASSED-DATA.#TRANS-USER-AREA #PASSED-DATA.#TRANS-USER-ID #PASSED-DATA.#TRANS-VERIFY-ID #PASSED-DATA.#TRANS-DTE #PASSED-DATA.#REASON
            pnd_Passed_Data_Pnd_Trans_User_Id, pnd_Passed_Data_Pnd_Trans_Verify_Id, pnd_Passed_Data_Pnd_Trans_Dte, pnd_Passed_Data_Pnd_Reason);
    }

    //
}
