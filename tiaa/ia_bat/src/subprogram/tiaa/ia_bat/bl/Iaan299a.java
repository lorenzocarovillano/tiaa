/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:35:01 AM
**        * FROM NATURAL SUBPROGRAM : Iaan299a
************************************************************
**        * FILE NAME            : Iaan299a.java
**        * CLASS NAME           : Iaan299a
**        * INSTANCE NAME        : Iaan299a
************************************************************
***********************************************************************
*   SUBPROGRAM :- IAAN299A
*   SYSTEM     :- IA
*   BY         :- LEN B
*
*   DESCRIPTION
*   -----------
*   SUBPROGRAM USED TO RETRIEVE LATEST CONTROL RECORD.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan299a extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIaaa299a pdaIaaa299a;
    private PdaIaaa202h pdaIaaa202h;
    private LdaIaal202h ldaIaal202h;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Error;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal202h = new LdaIaal202h();
        registerRecord(ldaIaal202h);
        registerRecord(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1());

        // parameters
        parameters = new DbsRecord();
        pdaIaaa299a = new PdaIaaa299a(parameters);
        pdaIaaa202h = new PdaIaaa202h(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Error = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Error", "#ERROR", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal202h.initializeValues();

        localVariables.reset();
        pnd_Const_Pnd_Error.setInitialValue("E");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan299a() throws Exception
    {
        super("Iaan299a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  =========================== START OF PROGRAM =========================
        pdaIaaa299a.getPnd_Iaan299a_Out().reset();                                                                                                                        //Natural: RESET #IAAN299A-OUT #IAA-CNTRL-RCRD-1
        pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1().reset();
        //*  FETCH THE LATEST CONTROL RECORD
        ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM #IAAN299A-IN.CNTRL-CDE
        (
        "RD1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pdaIaaa299a.getPnd_Iaan299a_In_Cntrl_Cde(), WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        RD1:
        while (condition(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().readNextRow("RD1")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal202h.getIaa_Cntrl_Rcrd_1_Cntrl_Cde().equals(pdaIaaa299a.getPnd_Iaan299a_In_Cntrl_Cde())))                                                    //Natural: IF IAA-CNTRL-RCRD-1.CNTRL-CDE EQ #IAAN299A-IN.CNTRL-CDE
        {
            pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1().setValuesByName(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1());                                                                  //Natural: MOVE BY NAME IAA-CNTRL-RCRD-1 TO #IAA-CNTRL-RCRD-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIaaa299a.getPnd_Iaan299a_Out_Pnd_Return_Code().setValue(pnd_Const_Pnd_Error);                                                                              //Natural: ASSIGN #IAAN299A-OUT.#RETURN-CODE := #ERROR
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
