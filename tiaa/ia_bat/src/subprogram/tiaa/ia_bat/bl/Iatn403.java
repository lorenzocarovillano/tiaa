/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:44:36 AM
**        * FROM NATURAL SUBPROGRAM : Iatn403
************************************************************
**        * FILE NAME            : Iatn403.java
**        * CLASS NAME           : Iatn403
**        * INSTANCE NAME        : Iatn403
************************************************************
************************************************************************
* PROGRAM  : IATN403
* SYSTEM   : IA
* GENERATED: 12/10/1997
* AUTHOR   : LEN BERNSTEIN
* FUNCTION : THIS PROGRAM CHECKS IF THE CONTRACT HAS BEEN APPROVED BY
*          : THE STATE OR NOT.
*          :
* HISTORY
* 01/15/2009 O. SOTTO  TIAA ACCESS CHANGES. SC 011509.
* 03/12/2010 O. SOTTO  FIXED AN ERROR. SC 031210.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn403 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl403p pdaIatl403p;
    private PdaIaapda_M pdaIaapda_M;
    private PdaIata211 pdaIata211;
    private PdaIata201 pdaIata201;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Iatn211_In;
    private DbsField pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_1;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Num;
    private DbsField pnd_I;

    private DbsGroup pnd_Issue_Pnd_Rsdnce;
    private DbsField pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste;
    private DbsField pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste;

    private DbsGroup pnd_Iatn204_Pda;
    private DbsField pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct;
    private DbsField pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N;

    private DbsGroup pnd_Iatn204_Pda__R_Field_2;
    private DbsField pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A;
    private DbsField pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste;
    private DbsField pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste;
    private DbsField pnd_Iatn204_Pda_Pnd_Pend_Cde;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Real_Estate;
    private DbsField pnd_Const_Pnd_Tiaa_Access;
    private DbsField pnd_Const_Pnd_Cref;
    private DbsField pnd_Const_Pnd_Tiaa;
    private DbsField pnd_Const_Pnd_Graded;
    private DbsField pnd_Const_Pnd_Transfer_T;
    private DbsField pnd_Const_Pnd_Switch_S;
    private DbsField pnd_Const_Pnd_Yes;
    private DbsField pnd_Const_Pnd_No;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_To_Tiaa;
    private DbsField pnd_L_Pnd_To_Real_Estate;
    private DbsField pnd_L_Pnd_To_Tiaa_Access;
    private DbsField pnd_L_Pnd_To_Cref;
    private DbsField pnd_M367i_Cntct_Mthd;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        pdaIata211 = new PdaIata211(localVariables);
        pdaIata201 = new PdaIata201(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl403p = new PdaIatl403p(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Iatn211_In = localVariables.newGroupInRecord("pnd_Iatn211_In", "#IATN211-IN");
        pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte = pnd_Iatn211_In.newFieldInGroup("pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte", "#RQST-EFFCTV-DTE", FieldType.NUMERIC, 
            8);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_1", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_1.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Num = localVariables.newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        pnd_Issue_Pnd_Rsdnce = localVariables.newGroupInRecord("pnd_Issue_Pnd_Rsdnce", "#ISSUE-#RSDNCE");
        pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste = pnd_Issue_Pnd_Rsdnce.newFieldInGroup("pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste", "#NUMERIC-STE", FieldType.STRING, 
            2);
        pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste = pnd_Issue_Pnd_Rsdnce.newFieldInGroup("pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste", "#ALPHA-STE", FieldType.STRING, 
            2);

        pnd_Iatn204_Pda = localVariables.newGroupInRecord("pnd_Iatn204_Pda", "#IATN204-PDA");
        pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct", "#IA-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N", "#IA-FRM-PAYEE-N", FieldType.NUMERIC, 
            2);

        pnd_Iatn204_Pda__R_Field_2 = pnd_Iatn204_Pda.newGroupInGroup("pnd_Iatn204_Pda__R_Field_2", "REDEFINE", pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N);
        pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A = pnd_Iatn204_Pda__R_Field_2.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A", "#IA-FRM-PAYEE-A", FieldType.STRING, 
            2);
        pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste", "#CNTRCT-ISSUE-STE", FieldType.STRING, 
            3);
        pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste", "#CNTRCT-RSDNCE-STE", FieldType.STRING, 
            3);
        pnd_Iatn204_Pda_Pnd_Pend_Cde = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Real_Estate = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Real_Estate", "#REAL-ESTATE", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Access = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Access", "#TIAA-ACCESS", FieldType.STRING, 1);
        pnd_Const_Pnd_Cref = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Cref", "#CREF", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa", "#TIAA", FieldType.STRING, 1);
        pnd_Const_Pnd_Graded = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Graded", "#GRADED", FieldType.STRING, 1);
        pnd_Const_Pnd_Transfer_T = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Transfer_T", "#TRANSFER-T", FieldType.STRING, 1);
        pnd_Const_Pnd_Switch_S = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Switch_S", "#SWITCH-S", FieldType.STRING, 1);
        pnd_Const_Pnd_Yes = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Yes", "#YES", FieldType.STRING, 1);
        pnd_Const_Pnd_No = pnd_Const.newFieldInGroup("pnd_Const_Pnd_No", "#NO", FieldType.STRING, 1);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_To_Tiaa = pnd_L.newFieldInGroup("pnd_L_Pnd_To_Tiaa", "#TO-TIAA", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_To_Real_Estate = pnd_L.newFieldInGroup("pnd_L_Pnd_To_Real_Estate", "#TO-REAL-ESTATE", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_To_Tiaa_Access = pnd_L.newFieldInGroup("pnd_L_Pnd_To_Tiaa_Access", "#TO-TIAA-ACCESS", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_To_Cref = pnd_L.newFieldInGroup("pnd_L_Pnd_To_Cref", "#TO-CREF", FieldType.BOOLEAN, 1);
        pnd_M367i_Cntct_Mthd = localVariables.newFieldInRecord("pnd_M367i_Cntct_Mthd", "#M367I-CNTCT-MTHD", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Const_Pnd_Real_Estate.setInitialValue("R");
        pnd_Const_Pnd_Tiaa_Access.setInitialValue("D");
        pnd_Const_Pnd_Cref.setInitialValue("C");
        pnd_Const_Pnd_Tiaa.setInitialValue("T");
        pnd_Const_Pnd_Graded.setInitialValue("G");
        pnd_Const_Pnd_Transfer_T.setInitialValue("T");
        pnd_Const_Pnd_Switch_S.setInitialValue("S");
        pnd_Const_Pnd_Yes.setInitialValue("Y");
        pnd_Const_Pnd_No.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn403() throws Exception
    {
        super("Iatn403");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN403", onError);
        //* ======================================================================
        //*                     START OF MAIN PROGRAM LOGIC
        //* ======================================================================
        pdaIatl403p.getPnd_Iatn403_Out().reset();                                                                                                                         //Natural: RESET #IATN403-OUT
        RPT1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM VALIDATE-INPUT
            sub_Validate_Input();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Date_A.setValueEdited(pdaIatl403p.getPnd_Iatn403_In_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DATE-A
            pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte.setValue(pnd_Date_A_Pnd_Date_N);                                                                                           //Natural: ASSIGN #RQST-EFFCTV-DTE := #DATE-N
            pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct.setValue(pdaIatl403p.getPnd_Iatn403_In_Ia_Frm_Cntrct());                                                                    //Natural: ASSIGN #IATN204-PDA.#IA-FRM-CNTRCT := #IATN403-IN.IA-FRM-CNTRCT
            pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A.setValue(pdaIatl403p.getPnd_Iatn403_In_Ia_Frm_Payee());                                                                    //Natural: ASSIGN #IATN204-PDA.#IA-FRM-PAYEE-A := #IATN403-IN.IA-FRM-PAYEE
                                                                                                                                                                          //Natural: PERFORM GET-STATE
            sub_Get_State();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue(pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                             //Natural: MOVE MSG-INFO-SUB.##MSG TO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONVERT-STATE-FROM-N-TO-A
            sub_Convert_State_From_N_To_A();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FETCH TO CONTRACT ISSUE STATE
            if (condition(pdaIatl403p.getPnd_Iatn403_In_Ia_To_Cntrct().notEquals(" ") && pdaIatl403p.getPnd_Iatn403_In_Ia_To_Payee().notEquals(" ")))                     //Natural: IF #IATN403-IN.IA-TO-CNTRCT NE ' ' AND #IATN403-IN.IA-TO-PAYEE NE ' '
            {
                pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct.setValue(pdaIatl403p.getPnd_Iatn403_In_Ia_To_Cntrct());                                                                 //Natural: ASSIGN #IATN204-PDA.#IA-FRM-CNTRCT := #IATN403-IN.IA-TO-CNTRCT
                pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A.setValue(pdaIatl403p.getPnd_Iatn403_In_Ia_To_Payee());                                                                 //Natural: ASSIGN #IATN204-PDA.#IA-FRM-PAYEE-A := #IATN403-IN.IA-TO-PAYEE
                                                                                                                                                                          //Natural: PERFORM GET-STATE
                sub_Get_State();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THE TO CONTRACT WILL NOT EXIST FOR NEW ISSUES
                if (condition(pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                             //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CONVERT-TO-STATE-FROM-N-TO-A
                    sub_Convert_To_State_From_N_To_A();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RPT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-STATE-APPROVED
            sub_Check_If_State_Approved();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break RPT1;                                                                                                                                         //Natural: ESCAPE BOTTOM ( RPT1. )
            //* *******************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
            //*  RPT1.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ======================================================================
        //*                    START OF SUBROUTINES
        //* ======================================================================
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-INPUT
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-STATE-FROM-N-TO-A
        //* ***********************************************************************
        //*  CONVERT ISSUE STATE FROM A NUMERIC VALUE TO AN ALPH VALUE
        //*  CONVERT RSDNCE STATE FROM A NUMERIC VALUE TO AN ALPH VALUE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-TO-STATE-FROM-N-TO-A
        //* ***********************************************************************
        //*  CONVERT TO CONTRACT STATE FROM A NUMERIC VALUE TO AN ALPH VALUE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CALL-NAZN031
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-STATE-APPROVED
        //* ***********************************************************************
        //* *IF XFR-FRM-ACCT-CDE(1:20) EQ #REAL-ESTATE
        //* *ELSE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CALL-IATN211
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pdaIatl403p.getPnd_Iatn403_Out_Pnd_Return_Code().setValue("E");                                                                                                   //Natural: MOVE 'E' TO #IATN403-OUT.#RETURN-CODE
        pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue(DbsUtil.compress(pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg(), Global.getPROGRAM()));                               //Natural: COMPRESS #IATN403-OUT.#MSG *PROGRAM INTO #IATN403-OUT.#MSG
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( RPT1. )
        Global.setEscapeCode(EscapeType.Bottom, "RPT1");
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Validate_Input() throws Exception                                                                                                                    //Natural: VALIDATE-INPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pdaIatl403p.getPnd_Iatn403_In_Pnd_Transfer_Switch().notEquals(pnd_Const_Pnd_Transfer_T) && pdaIatl403p.getPnd_Iatn403_In_Pnd_Transfer_Switch().notEquals(pnd_Const_Pnd_Switch_S))) //Natural: IF #TRANSFER-SWITCH NE #TRANSFER-T AND #TRANSFER-SWITCH NE #SWITCH-S
        {
            pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue("Transfer Switch ind must be 'T' or 'S'");                                                                  //Natural: MOVE 'Transfer Switch ind must be "T" or "S"' TO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl403p.getPnd_Iatn403_In_Ia_Frm_Cntrct().equals(" ")))                                                                                         //Natural: IF IA-FRM-CNTRCT = ' '
        {
            pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue("IA From contract is required.");                                                                           //Natural: MOVE 'IA From contract is required.' TO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl403p.getPnd_Iatn403_In_Ia_Frm_Payee().equals(" ")))                                                                                          //Natural: IF IA-FRM-PAYEE = ' '
        {
            pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue("IA From payee is required.");                                                                              //Natural: MOVE 'IA From payee is required.' TO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl403p.getPnd_Iatn403_In_Rqst_Effctv_Dte().equals(getZero())))                                                                                 //Natural: IF RQST-EFFCTV-DTE = 0
        {
            pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue("EFFECTIVE DATE  is required.");                                                                            //Natural: MOVE 'EFFECTIVE DATE  is required.' TO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-INPUT
    }
    private void sub_Get_State() throws Exception                                                                                                                         //Natural: GET-STATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        DbsUtil.callnat(Iatn204.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Iatn204_Pda);                                                        //Natural: CALLNAT 'IATN204' MSG-INFO-SUB #IATN204-PDA
        if (condition(Global.isEscape())) return;
        //*  GET-STATE
    }
    private void sub_Convert_State_From_N_To_A() throws Exception                                                                                                         //Natural: CONVERT-STATE-FROM-N-TO-A
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste.setValue(pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste);                                                                              //Natural: ASSIGN #NUMERIC-STE := #CNTRCT-ISSUE-STE
                                                                                                                                                                          //Natural: PERFORM #CALL-NAZN031
        sub_Pnd_Call_Nazn031();
        if (condition(Global.isEscape())) {return;}
        pdaIata211.getIata211_Pnd_I_From_Issue_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                      //Natural: ASSIGN IATA211.#I-FROM-ISSUE-STATE := #ALPHA-STE
        pdaIatl403p.getPnd_Iatn403_Out_Pnd_Issue_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                    //Natural: ASSIGN #IATN403-OUT.#ISSUE-STATE := #ALPHA-STE
        pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste.setValue(pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste);                                                                             //Natural: ASSIGN #NUMERIC-STE := #CNTRCT-RSDNCE-STE
                                                                                                                                                                          //Natural: PERFORM #CALL-NAZN031
        sub_Pnd_Call_Nazn031();
        if (condition(Global.isEscape())) {return;}
        pdaIata211.getIata211_Pnd_I_From_Residence_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                  //Natural: ASSIGN IATA211.#I-FROM-RESIDENCE-STATE := #ALPHA-STE
        pdaIatl403p.getPnd_Iatn403_Out_Pnd_Residence_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                //Natural: ASSIGN #IATN403-OUT.#RESIDENCE-STATE := #ALPHA-STE
        //*  CONVERT-STATE-FROM-N-TO-A
    }
    private void sub_Convert_To_State_From_N_To_A() throws Exception                                                                                                      //Natural: CONVERT-TO-STATE-FROM-N-TO-A
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste.setValue(pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste);                                                                              //Natural: ASSIGN #NUMERIC-STE := #CNTRCT-ISSUE-STE
                                                                                                                                                                          //Natural: PERFORM #CALL-NAZN031
        sub_Pnd_Call_Nazn031();
        if (condition(Global.isEscape())) {return;}
        pdaIata211.getIata211_Pnd_I_To_Issue_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                        //Natural: ASSIGN IATA211.#I-TO-ISSUE-STATE := #ALPHA-STE
        pdaIatl403p.getPnd_Iatn403_Out_Pnd_To_Issue_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                 //Natural: ASSIGN #IATN403-OUT.#TO-ISSUE-STATE := #ALPHA-STE
        //*  CONVERT-TO-STATE-FROM-N-TO-A
    }
    private void sub_Pnd_Call_Nazn031() throws Exception                                                                                                                  //Natural: #CALL-NAZN031
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CONVERT STATE FROM NUMERIC TO ALPHA
        pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste.reset();                                                                                                                       //Natural: RESET #ALPHA-STE
        DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste, pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste); //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #ALPHA-STE #NUMERIC-STE
        if (condition(Global.isEscape())) return;
        //*  CONVERT-STATE-FROM-N-TO-A #CALL-NAZN031
    }
    //*  T=TRANSFER S=SWITCH
    private void sub_Check_If_State_Approved() throws Exception                                                                                                           //Natural: CHECK-IF-STATE-APPROVED
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata211.getIata211_Pnd_I_Transfer_Switch().setValue(pdaIatl403p.getPnd_Iatn403_In_Pnd_Transfer_Switch());                                                      //Natural: ASSIGN IATA211.#I-TRANSFER-SWITCH := #TRANSFER-SWITCH
        if (condition(pdaIatl403p.getPnd_Iatn403_In_Pnd_Transfer_Switch().equals(pnd_Const_Pnd_Switch_S)))                                                                //Natural: IF #TRANSFER-SWITCH = #SWITCH-S
        {
            //*  VALIDATE TO AND FROM FIELDS
            DbsUtil.examine(new ExamineSource(pdaIatl403p.getPnd_Iatn403_In_Xfr_To_Acct_Cde().getValue("*"),true), new ExamineSearch("H'20'", true), new                  //Natural: EXAMINE FULL XFR-TO-ACCT-CDE ( * ) FOR FULL H'20' GIVING NUMBER #NUM
                ExamineGivingNumber(pnd_Num));
            if (condition(pnd_Num.equals(20)))                                                                                                                            //Natural: IF #NUM = 20
            {
                pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue(DbsUtil.compress("At least one to account code is required."));                                         //Natural: COMPRESS 'At least one to account code is required.' INTO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue("*"),true), new ExamineSearch("H'20'", true),                     //Natural: EXAMINE FULL XFR-FRM-ACCT-CDE ( * ) FOR FULL H'20' GIVING NUMBER #NUM
                new ExamineGivingNumber(pnd_Num));
            if (condition(pnd_Num.equals(20)))                                                                                                                            //Natural: IF #NUM = 20
            {
                pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue(DbsUtil.compress("At least one from account code is required."));                                       //Natural: COMPRESS 'At least one from account code is required.' INTO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  A SWITCH REQUEST CAN ONLY BE FOR REAL ESTATE OR CREF
            //*  AND ALSO TIAA ACCESS
            //*  011509
            //*  011509
            //*  011509
            //*  011509
            //*  011509
            //*  011509
            short decideConditionsMet376 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN XFR-FRM-ACCT-CDE ( 1:20 ) EQ #REAL-ESTATE
            if (condition(pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue(1,":",20).equals(pnd_Const_Pnd_Real_Estate)))
            {
                decideConditionsMet376++;
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(1).setValue(pnd_Const_Pnd_Real_Estate);                                                           //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( 1 ) := #REAL-ESTATE
                pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue(pnd_Const_Pnd_Real_Estate);                                                                     //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := #REAL-ESTATE
            }                                                                                                                                                             //Natural: WHEN XFR-FRM-ACCT-CDE ( 1:20 ) EQ #TIAA-ACCESS
            else if (condition(pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue(1,":",20).equals(pnd_Const_Pnd_Tiaa_Access)))
            {
                decideConditionsMet376++;
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(1).setValue(pnd_Const_Pnd_Tiaa_Access);                                                           //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( 1 ) := #TIAA-ACCESS
                pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue(pnd_Const_Pnd_Tiaa_Access);                                                                     //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := #TIAA-ACCESS
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(1).setValue(pnd_Const_Pnd_Cref);                                                                  //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( 1 ) := #CREF
                pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue(pnd_Const_Pnd_Cref);                                                                            //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := #CREF
                //* *END-IF                                              /* 011509
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CHECKS IF THE STATE WAS APPROVED
                                                                                                                                                                          //Natural: PERFORM #CALL-IATN211
            sub_Pnd_Call_Iatn211();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaIata211.getIata211_Pnd_O_Accept_Request().getValue(1).equals(pnd_Const_Pnd_Yes)))                                                            //Natural: IF IATA211.#O-ACCEPT-REQUEST ( 1 ) EQ #YES
            {
                pdaIatl403p.getPnd_Iatn403_Out_Pnd_State_Approved().setValue(true);                                                                                       //Natural: MOVE TRUE TO #IATN403-OUT.#STATE-APPROVED
            }                                                                                                                                                             //Natural: END-IF
            //*  TRANSFER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  FILL THE TO TRANSFER TYPES
            DbsUtil.examine(new ExamineSource(pdaIatl403p.getPnd_Iatn403_In_Xfr_To_Acct_Cde().getValue("*"),true), new ExamineSearch("H'20'", true), new                  //Natural: EXAMINE FULL XFR-TO-ACCT-CDE ( * ) FOR FULL H'20' GIVING NUMBER #NUM
                ExamineGivingNumber(pnd_Num));
            if (condition(pnd_Num.equals(20)))                                                                                                                            //Natural: IF #NUM = 20
            {
                pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue(DbsUtil.compress("At least one to account code is required."));                                         //Natural: COMPRESS 'At least one to account code is required.' INTO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Num.compute(new ComputeParameters(false, pnd_Num), DbsField.subtract(20,pnd_Num));                                                                        //Natural: ASSIGN #NUM := 20 - #NUM
            FR1:                                                                                                                                                          //Natural: FOR #I = 1 TO #NUM
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Num)); pnd_I.nadd(1))
            {
                //*    DECIDE ON FIRST VALUE OF  XFR-FRM-ACCT-CDE(#I)    /* 031210
                //*  031210
                short decideConditionsMet412 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF XFR-TO-ACCT-CDE ( #I );//Natural: VALUE #TIAA, #GRADED
                if (condition((pdaIatl403p.getPnd_Iatn403_In_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa) || pdaIatl403p.getPnd_Iatn403_In_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Graded))))
                {
                    decideConditionsMet412++;
                    pnd_L_Pnd_To_Tiaa.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #TO-TIAA
                }                                                                                                                                                         //Natural: VALUE #REAL-ESTATE
                else if (condition((pdaIatl403p.getPnd_Iatn403_In_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Real_Estate))))
                {
                    decideConditionsMet412++;
                    //*  011509
                    pnd_L_Pnd_To_Real_Estate.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #TO-REAL-ESTATE
                }                                                                                                                                                         //Natural: VALUE #TIAA-ACCESS
                else if (condition((pdaIatl403p.getPnd_Iatn403_In_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Access))))
                {
                    decideConditionsMet412++;
                    //*  011509
                    pnd_L_Pnd_To_Tiaa_Access.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #TO-TIAA-ACCESS
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_L_Pnd_To_Cref.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #TO-CREF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_I.setValue(1);                                                                                                                                            //Natural: ASSIGN #I := 1
            //*  011509 START
            //*  011509 END
            short decideConditionsMet428 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #TO-TIAA
            if (condition(pnd_L_Pnd_To_Tiaa.getBoolean()))
            {
                decideConditionsMet428++;
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(pnd_I).setValue(pnd_Const_Pnd_Tiaa);                                                              //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( #I ) := #TIAA
                pnd_I.nadd(1);                                                                                                                                            //Natural: ASSIGN #I := #I + 1
            }                                                                                                                                                             //Natural: WHEN #TO-REAL-ESTATE
            if (condition(pnd_L_Pnd_To_Real_Estate.getBoolean()))
            {
                decideConditionsMet428++;
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(pnd_I).setValue(pnd_Const_Pnd_Real_Estate);                                                       //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( #I ) := #REAL-ESTATE
                pnd_I.nadd(1);                                                                                                                                            //Natural: ASSIGN #I := #I + 1
            }                                                                                                                                                             //Natural: WHEN #TO-TIAA-ACCESS
            if (condition(pnd_L_Pnd_To_Tiaa_Access.getBoolean()))
            {
                decideConditionsMet428++;
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(pnd_I).setValue(pnd_Const_Pnd_Tiaa_Access);                                                       //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( #I ) := #TIAA-ACCESS
                pnd_I.nadd(1);                                                                                                                                            //Natural: ASSIGN #I := #I + 1
            }                                                                                                                                                             //Natural: WHEN #TO-CREF
            if (condition(pnd_L_Pnd_To_Cref.getBoolean()))
            {
                decideConditionsMet428++;
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(pnd_I).setValue(pnd_Const_Pnd_Cref);                                                              //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( #I ) := #CREF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet428 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  PROCES EACH FROM CODE ONCE
            DbsUtil.examine(new ExamineSource(pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue("*"),true), new ExamineSearch("H'20'", true),                     //Natural: EXAMINE FULL XFR-FRM-ACCT-CDE ( * ) FOR FULL H'20' GIVING NUMBER #NUM
                new ExamineGivingNumber(pnd_Num));
            if (condition(pnd_Num.equals(20)))                                                                                                                            //Natural: IF #NUM = 20
            {
                pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg().setValue(DbsUtil.compress("At least one from account code is required."));                                       //Natural: COMPRESS 'At least one from account code is required.' INTO #IATN403-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Num.compute(new ComputeParameters(false, pnd_Num), DbsField.subtract(20,pnd_Num));                                                                        //Natural: ASSIGN #NUM := 20 - #NUM
            //*  PROCESS FOR MANY TO MANY TRANSFERS
            FR2:                                                                                                                                                          //Natural: FOR #I = 1 TO #NUM
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Num)); pnd_I.nadd(1))
            {
                //*  011509
                //*  011509
                short decideConditionsMet460 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF XFR-FRM-ACCT-CDE ( #I );//Natural: VALUE #TIAA, #GRADED
                if (condition((pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa) || pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Graded))))
                {
                    decideConditionsMet460++;
                    pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue(pnd_Const_Pnd_Tiaa);                                                                        //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := #TIAA
                }                                                                                                                                                         //Natural: VALUE #REAL-ESTATE
                else if (condition((pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Real_Estate))))
                {
                    decideConditionsMet460++;
                    pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue(pnd_Const_Pnd_Real_Estate);                                                                 //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := #REAL-ESTATE
                }                                                                                                                                                         //Natural: VALUE #TIAA-ACCESS
                else if (condition((pdaIatl403p.getPnd_Iatn403_In_Xfr_Frm_Acct_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Access))))
                {
                    decideConditionsMet460++;
                    pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue(pnd_Const_Pnd_Tiaa_Access);                                                                 //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := #TIAA-ACCESS
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue(pnd_Const_Pnd_Cref);                                                                        //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := #CREF
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  CHECKS IF THE STATE WAS APPROVED
                                                                                                                                                                          //Natural: PERFORM #CALL-IATN211
                sub_Pnd_Call_Iatn211();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FR2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FR2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    IF IATA211.#O-ACCEPT-REQUEST(1:3) EQ #NO          /* 011509
                //*  011509
                if (condition(pdaIata211.getIata211_Pnd_O_Accept_Request().getValue(1,":",4).equals(pnd_Const_Pnd_No)))                                                   //Natural: IF IATA211.#O-ACCEPT-REQUEST ( 1:4 ) EQ #NO
                {
                    pdaIatl403p.getPnd_Iatn403_Out_Pnd_State_Approved().reset();                                                                                          //Natural: RESET #IATN403-OUT.#STATE-APPROVED
                    if (true) break FR2;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FR2. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      IF IATA211.#O-ACCEPT-REQUEST(1:3) EQ #YES       /* 011509
                    //*  011509
                    if (condition(pdaIata211.getIata211_Pnd_O_Accept_Request().getValue(1,":",4).equals(pnd_Const_Pnd_Yes)))                                              //Natural: IF IATA211.#O-ACCEPT-REQUEST ( 1:4 ) EQ #YES
                    {
                        pdaIatl403p.getPnd_Iatn403_Out_Pnd_State_Approved().setValue(true);                                                                               //Natural: MOVE TRUE TO #IATN403-OUT.#STATE-APPROVED
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-STATE-APPROVED
    }
    private void sub_Pnd_Call_Iatn211() throws Exception                                                                                                                  //Natural: #CALL-IATN211
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iatn211.class , getCurrentProcessState(), pdaIata211.getIata211(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Iatn211_In);                                //Natural: CALLNAT 'IATN211' IATA211 MSG-INFO-SUB #IATN211-IN
        if (condition(Global.isEscape())) return;
        //*  #CALL-IATN211
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",pdaIatl403p.getPnd_Iatn403_In_Ia_Frm_Cntrct(),pdaIatl403p.getPnd_Iatn403_In_Ia_Frm_Payee()); //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' IA-FRM-CNTRCT IA-FRM-PAYEE
    };                                                                                                                                                                    //Natural: END-ERROR
}
