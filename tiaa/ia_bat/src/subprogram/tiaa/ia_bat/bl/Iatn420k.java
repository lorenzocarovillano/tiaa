/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:48 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420k
************************************************************
**        * FILE NAME            : Iatn420k.java
**        * CLASS NAME           : Iatn420k
**        * INSTANCE NAME        : Iatn420k
************************************************************
************************************************************************
*  PROGRAM: IATN420K
*   AUTHOR: ARI GROSSMAN
*     DATE: APR 27, 1998
*  PURPOSE: FILL NEW ISSUE FIELD ON AUDIT FILE
*  HISTORY:
*  01/08/09  O SOTTO  TIAA ACCESS CHANGES. SC 010809.
*  04/12/12  J TINIO  RATE BASE EXPANSION. SC 041212.
*  04/2017   O SOTTO  RE-STOWED FOR IAAL999 PIN EXPANSION.
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420k extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ia_Frm_Cntrct;
    private DbsField pnd_Ia_Frm_Payee;

    private DbsGroup pnd_Ia_Frm_Payee__R_Field_1;
    private DbsField pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N;
    private DbsField pnd_Ia_To_Cntrct;

    private DbsGroup pnd_Ia_To_Cntrct__R_Field_2;
    private DbsField pnd_Ia_To_Cntrct_Pnd_Ia_To_Cntrct_1;
    private DbsField pnd_Ia_To_Payee;

    private DbsGroup pnd_Ia_To_Payee__R_Field_3;
    private DbsField pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N;
    private DbsField pnd_Ia_New_Issue;
    private DbsField pnd_Iaa_New_Issue;
    private DbsField pnd_Iaa_New_Issue_Phys;
    private DbsField pnd_Iaa_New_Issue_Re;
    private DbsField pnd_Xfr_To_Acct_Cde;
    private DbsField pnd_Return_Code;
    private DbsField pnd_W_Cntr;

    private DbsGroup pnd_W_Cntr__R_Field_4;
    private DbsField pnd_W_Cntr_Pnd_W_Cntr_1;
    private DbsField pnd_From_Teachers;
    private DbsField pnd_Found_Teacher_Fund;
    private DbsField pnd_Create_Teacher_Cnt;
    private DbsField pnd_Create_Cref_Cnt;
    private DbsField pnd_New_Issue_Cnt;
    private DbsField pnd_Found_Cnt;
    private DbsField pnd_Cntrct_Nbr;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_6;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_File_Mode;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_To_Cntrct_Fund;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_Q;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();
        pnd_Ia_Frm_Cntrct = parameters.newFieldInRecord("pnd_Ia_Frm_Cntrct", "#IA-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_Ia_Frm_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Frm_Payee = parameters.newFieldInRecord("pnd_Ia_Frm_Payee", "#IA-FRM-PAYEE", FieldType.STRING, 2);
        pnd_Ia_Frm_Payee.setParameterOption(ParameterOption.ByReference);

        pnd_Ia_Frm_Payee__R_Field_1 = parameters.newGroupInRecord("pnd_Ia_Frm_Payee__R_Field_1", "REDEFINE", pnd_Ia_Frm_Payee);
        pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N = pnd_Ia_Frm_Payee__R_Field_1.newFieldInGroup("pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N", "#IA-FRM-PYEE-N", FieldType.NUMERIC, 
            2);
        pnd_Ia_To_Cntrct = parameters.newFieldInRecord("pnd_Ia_To_Cntrct", "#IA-TO-CNTRCT", FieldType.STRING, 10);
        pnd_Ia_To_Cntrct.setParameterOption(ParameterOption.ByReference);

        pnd_Ia_To_Cntrct__R_Field_2 = parameters.newGroupInRecord("pnd_Ia_To_Cntrct__R_Field_2", "REDEFINE", pnd_Ia_To_Cntrct);
        pnd_Ia_To_Cntrct_Pnd_Ia_To_Cntrct_1 = pnd_Ia_To_Cntrct__R_Field_2.newFieldInGroup("pnd_Ia_To_Cntrct_Pnd_Ia_To_Cntrct_1", "#IA-TO-CNTRCT-1", FieldType.STRING, 
            1);
        pnd_Ia_To_Payee = parameters.newFieldInRecord("pnd_Ia_To_Payee", "#IA-TO-PAYEE", FieldType.STRING, 2);
        pnd_Ia_To_Payee.setParameterOption(ParameterOption.ByReference);

        pnd_Ia_To_Payee__R_Field_3 = parameters.newGroupInRecord("pnd_Ia_To_Payee__R_Field_3", "REDEFINE", pnd_Ia_To_Payee);
        pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N = pnd_Ia_To_Payee__R_Field_3.newFieldInGroup("pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N", "#IA-TO-PAYEE-N", FieldType.NUMERIC, 
            2);
        pnd_Ia_New_Issue = parameters.newFieldInRecord("pnd_Ia_New_Issue", "#IA-NEW-ISSUE", FieldType.STRING, 1);
        pnd_Ia_New_Issue.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_New_Issue = parameters.newFieldInRecord("pnd_Iaa_New_Issue", "#IAA-NEW-ISSUE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_New_Issue_Phys = parameters.newFieldInRecord("pnd_Iaa_New_Issue_Phys", "#IAA-NEW-ISSUE-PHYS", FieldType.STRING, 1);
        pnd_Iaa_New_Issue_Phys.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_New_Issue_Re = parameters.newFieldInRecord("pnd_Iaa_New_Issue_Re", "#IAA-NEW-ISSUE-RE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue_Re.setParameterOption(ParameterOption.ByReference);
        pnd_Xfr_To_Acct_Cde = parameters.newFieldArrayInRecord("pnd_Xfr_To_Acct_Cde", "#XFR-TO-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Xfr_To_Acct_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_W_Cntr = localVariables.newFieldInRecord("pnd_W_Cntr", "#W-CNTR", FieldType.STRING, 10);

        pnd_W_Cntr__R_Field_4 = localVariables.newGroupInRecord("pnd_W_Cntr__R_Field_4", "REDEFINE", pnd_W_Cntr);
        pnd_W_Cntr_Pnd_W_Cntr_1 = pnd_W_Cntr__R_Field_4.newFieldInGroup("pnd_W_Cntr_Pnd_W_Cntr_1", "#W-CNTR-1", FieldType.STRING, 1);
        pnd_From_Teachers = localVariables.newFieldInRecord("pnd_From_Teachers", "#FROM-TEACHERS", FieldType.STRING, 1);
        pnd_Found_Teacher_Fund = localVariables.newFieldInRecord("pnd_Found_Teacher_Fund", "#FOUND-TEACHER-FUND", FieldType.STRING, 1);
        pnd_Create_Teacher_Cnt = localVariables.newFieldInRecord("pnd_Create_Teacher_Cnt", "#CREATE-TEACHER-CNT", FieldType.STRING, 1);
        pnd_Create_Cref_Cnt = localVariables.newFieldInRecord("pnd_Create_Cref_Cnt", "#CREATE-CREF-CNT", FieldType.STRING, 1);
        pnd_New_Issue_Cnt = localVariables.newFieldInRecord("pnd_New_Issue_Cnt", "#NEW-ISSUE-CNT", FieldType.NUMERIC, 1);
        pnd_Found_Cnt = localVariables.newFieldInRecord("pnd_Found_Cnt", "#FOUND-CNT", FieldType.STRING, 1);
        pnd_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_6", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_To_Cntrct_Fund = localVariables.newFieldInRecord("pnd_To_Cntrct_Fund", "#TO-CNTRCT-FUND", FieldType.STRING, 1);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_Q = localVariables.newFieldInRecord("pnd_Q", "#Q", FieldType.PACKED_DECIMAL, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal999.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420k() throws Exception
    {
        super("Iatn420k");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420K", onError);
        //* *********
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        pnd_W_Cntr.setValue(pnd_Ia_Frm_Cntrct);                                                                                                                           //Natural: ON ERROR;//Natural: ASSIGN #W-CNTR := #IA-FRM-CNTRCT
        if (condition(pnd_W_Cntr_Pnd_W_Cntr_1.less("Z")))                                                                                                                 //Natural: IF #W-CNTR-1 < 'Z'
        {
            pnd_From_Teachers.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #FROM-TEACHERS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ia_New_Issue.equals("Y")))                                                                                                                      //Natural: IF #IA-NEW-ISSUE = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-IF-NEW-ISSUE
            sub_Pnd_Determine_If_New_Issue();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Iaa_New_Issue.equals("Y")))                                                                                                                 //Natural: IF #IAA-NEW-ISSUE = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM #CHECK-NEW-ISSUE-PHYS
                sub_Pnd_Check_New_Issue_Phys();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Iaa_New_Issue.setValue(pnd_Ia_New_Issue);                                                                                                                 //Natural: ASSIGN #IAA-NEW-ISSUE := #IA-NEW-ISSUE
            pnd_Iaa_New_Issue_Phys.setValue(pnd_Ia_New_Issue);                                                                                                            //Natural: ASSIGN #IAA-NEW-ISSUE-PHYS := #IA-NEW-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Ia_New_Issue.equals("Y") && pnd_Iaa_New_Issue.equals(" ")) || (pnd_Ia_New_Issue.equals("E"))))                                                 //Natural: IF ( #IA-NEW-ISSUE = 'Y' AND #IAA-NEW-ISSUE = ' ' ) OR ( #IA-NEW-ISSUE = 'E' )
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-R-G-S
            sub_Pnd_Check_R_G_S();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-IF-NEW-ISSUE
        //*  NEW VIEW PREFIX FROM IAAL999                     041212 START
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-R-G-S
        //* ******************************************************************
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-NEW-ISSUE-PHYS
        //* ******************************************************************
    }
    private void sub_Pnd_Determine_If_New_Issue() throws Exception                                                                                                        //Natural: #DETERMINE-IF-NEW-ISSUE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Found_Cnt.reset();                                                                                                                                            //Natural: RESET #FOUND-CNT
        pnd_Cntrct_Nbr.setValue(pnd_Ia_To_Cntrct);                                                                                                                        //Natural: ASSIGN #CNTRCT-NBR := #IA-TO-CNTRCT
        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-NBR
        (
        "FN",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Nbr, WcType.WITH) },
        1
        );
        FN:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FN")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            pnd_Found_Cnt.setValue("Y");                                                                                                                                  //Natural: MOVE 'Y' TO #FOUND-CNT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Found_Cnt.equals("Y")))                                                                                                                         //Natural: IF #FOUND-CNT = 'Y'
        {
            pnd_Iaa_New_Issue.setValue(" ");                                                                                                                              //Natural: MOVE ' ' TO #IAA-NEW-ISSUE
            pnd_Iaa_New_Issue_Phys.setValue(" ");                                                                                                                         //Natural: MOVE ' ' TO #IAA-NEW-ISSUE-PHYS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Iaa_New_Issue.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #IAA-NEW-ISSUE
            pnd_Iaa_New_Issue_Phys.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #IAA-NEW-ISSUE-PHYS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_R_G_S() throws Exception                                                                                                                   //Natural: #CHECK-R-G-S
    {
        if (BLNatReinput.isReinput()) return;

        FXZ:                                                                                                                                                              //Natural: FOR #Q = 1 TO 20
        for (pnd_Q.setValue(1); condition(pnd_Q.lessOrEqual(20)); pnd_Q.nadd(1))
        {
            if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals(" ")))                                                                                               //Natural: IF #XFR-TO-ACCT-CDE ( #Q ) = ' '
            {
                if (true) break FXZ;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FXZ. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("R")))                                                                                               //Natural: IF #XFR-TO-ACCT-CDE ( #Q ) = 'R'
            {
                if (condition(DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6L7'") || DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6M7'") || DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6N7'")  //Natural: IF #IA-FRM-CNTRCT = MASK ( '6L7' ) OR #IA-FRM-CNTRCT = MASK ( '6M7' ) OR #IA-FRM-CNTRCT = MASK ( '6N7' ) OR #FROM-TEACHERS = 'Y'
                    || pnd_From_Teachers.equals("Y")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntrct_Nbr.setValue(pnd_Ia_To_Cntrct);                                                                                                            //Natural: ASSIGN #CNTRCT-NBR := #IA-TO-CNTRCT
                    ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                       //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-NBR
                    (
                    "NF",
                    new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Nbr, WcType.WITH) },
                    1
                    );
                    NF:
                    while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("NF")))
                    {
                        ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
                        if (condition(ldaIaal999.getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re().equals(" ")))                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE = ' '
                        {
                            pnd_Iaa_New_Issue_Re.setValue("R");                                                                                                           //Natural: MOVE 'R' TO #IAA-NEW-ISSUE-RE
                            pnd_Iaa_New_Issue_Phys.setValue("R");                                                                                                         //Natural: MOVE 'R' TO #IAA-NEW-ISSUE-PHYS
                            if (true) break FXZ;                                                                                                                          //Natural: ESCAPE BOTTOM ( FXZ. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FXZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FXZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  010809 START
            if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("D")))                                                                                               //Natural: IF #XFR-TO-ACCT-CDE ( #Q ) = 'D'
            {
                pnd_Cntrct_Nbr.setValue(pnd_Ia_To_Cntrct);                                                                                                                //Natural: ASSIGN #CNTRCT-NBR := #IA-TO-CNTRCT
                ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                           //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-NBR
                (
                "FIND01",
                new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Nbr, WcType.WITH) },
                1
                );
                FIND01:
                while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
                {
                    ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
                    if (condition(ldaIaal999.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                               //Natural: IF NO RECORDS FOUND
                    {
                        pnd_Iaa_New_Issue_Phys.setValue("D");                                                                                                             //Natural: MOVE 'D' TO #IAA-NEW-ISSUE-PHYS
                        if (true) break FXZ;                                                                                                                              //Natural: ESCAPE BOTTOM ( FXZ. )
                    }                                                                                                                                                     //Natural: END-NOREC
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FXZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FXZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  010809 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("T") || pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("G")))                                            //Natural: IF #XFR-TO-ACCT-CDE ( #Q ) = 'T' OR = 'G'
            {
                pnd_Found_Teacher_Fund.reset();                                                                                                                           //Natural: RESET #FOUND-TEACHER-FUND
                if (condition(pnd_From_Teachers.equals("Y")))                                                                                                             //Natural: IF #FROM-TEACHERS = 'Y'
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Ia_Frm_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #IA-FRM-CNTRCT
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N);                                                                  //Natural: ASSIGN #W-CNTRCT-PAYEE := #IA-FRM-PYEE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Ia_To_Cntrct);                                                                                 //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #IA-TO-CNTRCT
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Ia_To_Payee_Pnd_Ia_To_Payee_N);                                                                   //Natural: ASSIGN #W-CNTRCT-PAYEE := #IA-TO-PAYEE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                        //Natural: ASSIGN #W-FUND-CODE := ' '
                ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                   //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
                (
                "RA1",
                new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
                new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
                );
                RA1:
                while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("RA1")))
                {
                    if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
                    {
                        if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1G") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1S"))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'T1G' OR = 'T1S'
                        {
                            pnd_Found_Teacher_Fund.setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO #FOUND-TEACHER-FUND
                            if (true) break RA1;                                                                                                                          //Natural: ESCAPE BOTTOM ( RA1. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (true) break RA1;                                                                                                                              //Natural: ESCAPE BOTTOM ( RA1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FXZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FXZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Found_Teacher_Fund.equals(" ")))                                                                                                        //Natural: IF #FOUND-TEACHER-FUND = ' '
                {
                    pnd_Iaa_New_Issue_Phys.setValue("T");                                                                                                                 //Natural: MOVE 'T' TO #IAA-NEW-ISSUE-PHYS
                    if (true) break FXZ;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FXZ. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_New_Issue_Phys() throws Exception                                                                                                          //Natural: #CHECK-NEW-ISSUE-PHYS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_New_Issue_Cnt.reset();                                                                                                                                        //Natural: RESET #NEW-ISSUE-CNT #CREATE-TEACHER-CNT #CREATE-CREF-CNT
        pnd_Create_Teacher_Cnt.reset();
        pnd_Create_Cref_Cnt.reset();
        FPZ:                                                                                                                                                              //Natural: FOR #Q = 1 TO 20
        for (pnd_Q.setValue(1); condition(pnd_Q.lessOrEqual(20)); pnd_Q.nadd(1))
        {
            if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals(" ")))                                                                                               //Natural: IF #XFR-TO-ACCT-CDE ( #Q ) = ' '
            {
                if (true) break FPZ;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FPZ. )
            }                                                                                                                                                             //Natural: END-IF
            //*  010809
            if (condition(pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("R") || pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("D")))                                            //Natural: IF #XFR-TO-ACCT-CDE ( #Q ) = 'R' OR = 'D'
            {
                if (condition(DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6L7'") || DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6M7'") || DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6N7'")  //Natural: IF #IA-FRM-CNTRCT = MASK ( '6L7' ) OR #IA-FRM-CNTRCT = MASK ( '6M7' ) OR #IA-FRM-CNTRCT = MASK ( '6N7' ) OR #FROM-TEACHERS = 'Y'
                    || pnd_From_Teachers.equals("Y")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Iaa_New_Issue_Phys.setValue("R");                                                                                                                 //Natural: MOVE 'R' TO #IAA-NEW-ISSUE-PHYS
                    pnd_New_Issue_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #NEW-ISSUE-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("T") || pnd_Xfr_To_Acct_Cde.getValue(pnd_Q).equals("G")) && pnd_Create_Teacher_Cnt.equals(" ")))) //Natural: IF ( #XFR-TO-ACCT-CDE ( #Q ) = 'T' OR = 'G' ) AND #CREATE-TEACHER-CNT = ' '
                {
                    pnd_Found_Teacher_Fund.reset();                                                                                                                       //Natural: RESET #FOUND-TEACHER-FUND
                    if (condition(pnd_From_Teachers.equals("Y")))                                                                                                         //Natural: IF #FROM-TEACHERS = 'Y'
                    {
                        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Ia_Frm_Cntrct);                                                                            //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #IA-FRM-CNTRCT
                        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Ia_Frm_Payee_Pnd_Ia_Frm_Pyee_N);                                                              //Natural: ASSIGN #W-CNTRCT-PAYEE := #IA-FRM-PYEE-N
                        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
                        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
                        (
                        "RQ1",
                        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
                        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
                        );
                        RQ1:
                        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("RQ1")))
                        {
                            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) &&                    //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
                                ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)))
                            {
                                if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1G") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("T1S"))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'T1G' OR = 'T1S'
                                {
                                    pnd_Found_Teacher_Fund.setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO #FOUND-TEACHER-FUND
                                    if (true) break RQ1;                                                                                                                  //Natural: ESCAPE BOTTOM ( RQ1. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (true) break RQ1;                                                                                                                      //Natural: ESCAPE BOTTOM ( RQ1. )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FPZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FPZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Found_Teacher_Fund.equals(" ")))                                                                                                //Natural: IF #FOUND-TEACHER-FUND = ' '
                        {
                            pnd_Create_Teacher_Cnt.setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO #CREATE-TEACHER-CNT
                            pnd_Iaa_New_Issue_Phys.setValue("T");                                                                                                         //Natural: MOVE 'T' TO #IAA-NEW-ISSUE-PHYS
                            pnd_New_Issue_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #NEW-ISSUE-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Create_Teacher_Cnt.setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO #CREATE-TEACHER-CNT
                        pnd_Iaa_New_Issue_Phys.setValue("T");                                                                                                             //Natural: MOVE 'T' TO #IAA-NEW-ISSUE-PHYS
                        pnd_New_Issue_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #NEW-ISSUE-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((((DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6L7'") || DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6M7'")) || DbsUtil.maskMatches(pnd_Ia_Frm_Cntrct,"'6N7'"))  //Natural: IF ( #IA-FRM-CNTRCT = MASK ( '6L7' ) OR #IA-FRM-CNTRCT = MASK ( '6M7' ) OR #IA-FRM-CNTRCT = MASK ( '6N7' ) OR #FROM-TEACHERS = 'Y' ) AND #CREATE-CREF-CNT = ' '
                        || pnd_From_Teachers.equals("Y")) && pnd_Create_Cref_Cnt.equals(" "))))
                    {
                        pnd_Create_Cref_Cnt.setValue("Y");                                                                                                                //Natural: MOVE 'Y' TO #CREATE-CREF-CNT
                        pnd_Iaa_New_Issue_Phys.setValue("C");                                                                                                             //Natural: MOVE 'C' TO #IAA-NEW-ISSUE-PHYS
                        pnd_New_Issue_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #NEW-ISSUE-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_New_Issue_Cnt.greater(1)))                                                                                                                      //Natural: IF #NEW-ISSUE-CNT > 1
        {
            pnd_Iaa_New_Issue_Phys.setValue("B");                                                                                                                         //Natural: MOVE 'B' TO #IAA-NEW-ISSUE-PHYS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NEW VIEW PREFIX FROM IAAL999                     041212 END
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
