/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:42:04 AM
**        * FROM NATURAL SUBPROGRAM : Iadn161
************************************************************
**        * FILE NAME            : Iadn161.java
**        * CLASS NAME           : Iadn161
**        * INSTANCE NAME        : Iadn161
************************************************************
************************************************************************
* PROGRAM: IADN16
* DATE   : 08/28/99
* AUTHOR : ILYA KIZHNERMAN
* MODE   : BATCH
* DESC   : THIS SUB-PROGRAM IS CALLED FROM IADP160.FOR A SPECIFIC DA
*        : NUMBER (#I-DA-CONTRCT) CHECK THE NON-PREMIUM FILE FOR TPA
*        : TRANSACTION
*
*
* HISTORY:
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadn161 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Ia_Np_Call_Parms;
    private DbsField pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Issue_Dte_Np;
    private DbsField pnd_Ia_Np_Call_Parms_Pnd_I_Da_Contract_Np;
    private DbsField pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Contract_Np;
    private DbsField pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Amount_Np;
    private DbsField pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np;
    private DbsField pnd_Ia_Np_Call_Parms_Pnd_O_Found;
    private DbsField pnd_Ia_Np_Call_Parms_Pnd_O_25_Over_Sw;

    private DataAccessProgramView vw_non_Premiums_View;
    private DbsField non_Premiums_View_Count_Castnp_Non_Premium_Transaction;
    private DbsField non_Premiums_View_Aa_Physical_Record_Cd;
    private DbsField non_Premiums_View_Aa_Tiaa_Contract_No;

    private DbsGroup non_Premiums_View_Np_Non_Premium_Transaction;
    private DbsField non_Premiums_View_Np_Transaction_Type_Cd;
    private DbsField non_Premiums_View_Np_Transaction_Effective_Dt;
    private DbsField non_Premiums_View_Np_Contract_Rate_Accum_Adj_Amt;
    private DbsField pnd_Issue_Dt;
    private DbsField pnd_Issue_Date;

    private DbsGroup pnd_Issue_Date__R_Field_1;
    private DbsField pnd_Issue_Date_Pnd_Issue_Dte;
    private DbsField pnd_Count_Pe;
    private DbsField pnd_I;
    private DbsField pnd_Eff_Date;

    private DbsGroup np_Super;
    private DbsField np_Super_Pnd_Record_Cd;
    private DbsField np_Super_Pnd_Contract_No;

    private DbsGroup np_Super__R_Field_2;
    private DbsField np_Super_Pnd_Np_Super;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Ia_Np_Call_Parms = parameters.newGroupInRecord("pnd_Ia_Np_Call_Parms", "#IA-NP-CALL-PARMS");
        pnd_Ia_Np_Call_Parms.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Issue_Dte_Np = pnd_Ia_Np_Call_Parms.newFieldInGroup("pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Issue_Dte_Np", "#I-IA-ISSUE-DTE-NP", 
            FieldType.STRING, 8);
        pnd_Ia_Np_Call_Parms_Pnd_I_Da_Contract_Np = pnd_Ia_Np_Call_Parms.newFieldInGroup("pnd_Ia_Np_Call_Parms_Pnd_I_Da_Contract_Np", "#I-DA-CONTRACT-NP", 
            FieldType.STRING, 8);
        pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Contract_Np = pnd_Ia_Np_Call_Parms.newFieldInGroup("pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Contract_Np", "#I-IA-CONTRACT-NP", 
            FieldType.STRING, 8);
        pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Amount_Np = pnd_Ia_Np_Call_Parms.newFieldInGroup("pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Amount_Np", "#I-IA-AMOUNT-NP", FieldType.NUMERIC, 
            11, 2);
        pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np = pnd_Ia_Np_Call_Parms.newFieldInGroup("pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np", "#O-SETTLE-AMT-NP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ia_Np_Call_Parms_Pnd_O_Found = pnd_Ia_Np_Call_Parms.newFieldInGroup("pnd_Ia_Np_Call_Parms_Pnd_O_Found", "#O-FOUND", FieldType.STRING, 1);
        pnd_Ia_Np_Call_Parms_Pnd_O_25_Over_Sw = pnd_Ia_Np_Call_Parms.newFieldInGroup("pnd_Ia_Np_Call_Parms_Pnd_O_25_Over_Sw", "#O-25-OVER-SW", FieldType.STRING, 
            1);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_non_Premiums_View = new DataAccessProgramView(new NameInfo("vw_non_Premiums_View", "NON-PREMIUMS-VIEW"), "ANNTY_ACTVTY_NON_PREMIUMS", "ANNUITY_ACTIVITY", 
            DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_NON_PREMIUMS"));
        non_Premiums_View_Count_Castnp_Non_Premium_Transaction = vw_non_Premiums_View.getRecord().newFieldInGroup("non_Premiums_View_Count_Castnp_Non_Premium_Transaction", 
            "C*NP-NON-PREMIUM-TRANSACTION", RepeatingFieldStrategy.CAsteriskVariable, "ANNUITY_ACTIVITY_NP_NON_PREMIUM_TRANSACTION");
        non_Premiums_View_Aa_Physical_Record_Cd = vw_non_Premiums_View.getRecord().newFieldInGroup("non_Premiums_View_Aa_Physical_Record_Cd", "AA-PHYSICAL-RECORD-CD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AA_PHYSICAL_RECORD_CD");
        non_Premiums_View_Aa_Tiaa_Contract_No = vw_non_Premiums_View.getRecord().newFieldInGroup("non_Premiums_View_Aa_Tiaa_Contract_No", "AA-TIAA-CONTRACT-NO", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AA_TIAA_CONTRACT_NO");

        non_Premiums_View_Np_Non_Premium_Transaction = vw_non_Premiums_View.getRecord().newGroupInGroup("non_Premiums_View_Np_Non_Premium_Transaction", 
            "NP-NON-PREMIUM-TRANSACTION", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ANNUITY_ACTIVITY_NP_NON_PREMIUM_TRANSACTION");
        non_Premiums_View_Np_Transaction_Type_Cd = non_Premiums_View_Np_Non_Premium_Transaction.newFieldArrayInGroup("non_Premiums_View_Np_Transaction_Type_Cd", 
            "NP-TRANSACTION-TYPE-CD", FieldType.STRING, 2, new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NP_TRANSACTION_TYPE_CD", 
            "ANNUITY_ACTIVITY_NP_NON_PREMIUM_TRANSACTION");
        non_Premiums_View_Np_Transaction_Effective_Dt = non_Premiums_View_Np_Non_Premium_Transaction.newFieldArrayInGroup("non_Premiums_View_Np_Transaction_Effective_Dt", 
            "NP-TRANSACTION-EFFECTIVE-DT", FieldType.NUMERIC, 8, new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NP_TRANSACTION_EFFECTIVE_DT", 
            "ANNUITY_ACTIVITY_NP_NON_PREMIUM_TRANSACTION");
        non_Premiums_View_Np_Contract_Rate_Accum_Adj_Amt = non_Premiums_View_Np_Non_Premium_Transaction.newFieldArrayInGroup("non_Premiums_View_Np_Contract_Rate_Accum_Adj_Amt", 
            "NP-CONTRACT-RATE-ACCUM-ADJ-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NP_CONTRACT_RATE_ACCUM_ADJ_AMT", 
            "ANNUITY_ACTIVITY_NP_NON_PREMIUM_TRANSACTION");
        registerRecord(vw_non_Premiums_View);

        pnd_Issue_Dt = localVariables.newFieldInRecord("pnd_Issue_Dt", "#ISSUE-DT", FieldType.DATE);
        pnd_Issue_Date = localVariables.newFieldInRecord("pnd_Issue_Date", "#ISSUE-DATE", FieldType.STRING, 8);

        pnd_Issue_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Issue_Date__R_Field_1", "REDEFINE", pnd_Issue_Date);
        pnd_Issue_Date_Pnd_Issue_Dte = pnd_Issue_Date__R_Field_1.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 8);
        pnd_Count_Pe = localVariables.newFieldInRecord("pnd_Count_Pe", "#COUNT-PE", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        np_Super = localVariables.newGroupInRecord("np_Super", "NP-SUPER");
        np_Super_Pnd_Record_Cd = np_Super.newFieldInGroup("np_Super_Pnd_Record_Cd", "#RECORD-CD", FieldType.STRING, 1);
        np_Super_Pnd_Contract_No = np_Super.newFieldInGroup("np_Super_Pnd_Contract_No", "#CONTRACT-NO", FieldType.STRING, 8);

        np_Super__R_Field_2 = localVariables.newGroupInRecord("np_Super__R_Field_2", "REDEFINE", np_Super);
        np_Super_Pnd_Np_Super = np_Super__R_Field_2.newFieldInGroup("np_Super_Pnd_Np_Super", "#NP-SUPER", FieldType.STRING, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_non_Premiums_View.reset();

        parameters.reset();
        localVariables.reset();
        np_Super_Pnd_Record_Cd.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iadn161() throws Exception
    {
        super("Iadn161");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60
        //*                          START OF PROGRAM
        //* *======================================================================
        pnd_Ia_Np_Call_Parms_Pnd_O_Found.setValue("N");                                                                                                                   //Natural: ASSIGN #O-FOUND := 'N'
        pnd_Ia_Np_Call_Parms_Pnd_O_25_Over_Sw.setValue("N");                                                                                                              //Natural: ASSIGN #O-25-OVER-SW := 'N'
        np_Super_Pnd_Record_Cd.setValue("N");                                                                                                                             //Natural: ASSIGN #RECORD-CD := 'N'
        np_Super_Pnd_Contract_No.setValue(pnd_Ia_Np_Call_Parms_Pnd_I_Da_Contract_Np);                                                                                     //Natural: ASSIGN #CONTRACT-NO := #I-DA-CONTRACT-NP
        pnd_Issue_Dt.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Issue_Dte_Np);                                                           //Natural: MOVE EDITED #I-IA-ISSUE-DTE-NP TO #ISSUE-DT ( EM = YYYYMMDD )
        pnd_Issue_Dt.nsubtract(1);                                                                                                                                        //Natural: COMPUTE #ISSUE-DT = #ISSUE-DT - 1
        pnd_Issue_Date.setValueEdited(pnd_Issue_Dt,new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED #ISSUE-DT ( EM = YYYYMMDD ) TO #ISSUE-DATE
        vw_non_Premiums_View.startDatabaseRead                                                                                                                            //Natural: READ NON-PREMIUMS-VIEW BY NP-SUPER-DE-1 STARTING FROM #NP-SUPER
        (
        "RD1",
        new Wc[] { new Wc("NP_SUPER_DE_1", ">=", np_Super_Pnd_Np_Super, WcType.BY) },
        new Oc[] { new Oc("NP_SUPER_DE_1", "ASC") }
        );
        RD1:
        while (condition(vw_non_Premiums_View.readNextRow("RD1")))
        {
            if (condition(non_Premiums_View_Aa_Tiaa_Contract_No.equals(np_Super_Pnd_Contract_No)))                                                                        //Natural: IF AA-TIAA-CONTRACT-NO = #CONTRACT-NO
            {
                pnd_Ia_Np_Call_Parms_Pnd_O_Found.setValue("Y");                                                                                                           //Natural: ASSIGN #O-FOUND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(non_Premiums_View_Aa_Tiaa_Contract_No.greater(np_Super_Pnd_Contract_No)))                                                                       //Natural: IF AA-TIAA-CONTRACT-NO GT #CONTRACT-NO
            {
                pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np.compute(new ComputeParameters(false, pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np), pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np.multiply(-1)); //Natural: COMPUTE #O-SETTLE-AMT-NP = #O-SETTLE-AMT-NP * -1
                if (condition(pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np.greater(pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Amount_Np.add(5)) || pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np.less(pnd_Ia_Np_Call_Parms_Pnd_I_Ia_Amount_Np.subtract(5)))) //Natural: IF #O-SETTLE-AMT-NP GT #I-IA-AMOUNT-NP + 5 OR #O-SETTLE-AMT-NP LT #I-IA-AMOUNT-NP - 5
                {
                    pnd_Ia_Np_Call_Parms_Pnd_O_25_Over_Sw.setValue("Y");                                                                                                  //Natural: ASSIGN #O-25-OVER-SW := 'Y'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Pe.setValue(non_Premiums_View_Count_Castnp_Non_Premium_Transaction);                                                                                //Natural: ASSIGN #COUNT-PE := C*NP-NON-PREMIUM-TRANSACTION
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #COUNT-PE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
            {
                if (condition(non_Premiums_View_Np_Transaction_Type_Cd.getValue(pnd_I).equals("55") && non_Premiums_View_Np_Transaction_Effective_Dt.getValue(pnd_I).equals(pnd_Issue_Date_Pnd_Issue_Dte))) //Natural: IF NP-TRANSACTION-TYPE-CD ( #I ) = '55' AND NP-TRANSACTION-EFFECTIVE-DT ( #I ) = #ISSUE-DTE
                {
                    pnd_Ia_Np_Call_Parms_Pnd_O_Settle_Amt_Np.nadd(non_Premiums_View_Np_Contract_Rate_Accum_Adj_Amt.getValue(pnd_I));                                      //Natural: ADD NP-CONTRACT-RATE-ACCUM-ADJ-AMT ( #I ) TO #O-SETTLE-AMT-NP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
    }
}
