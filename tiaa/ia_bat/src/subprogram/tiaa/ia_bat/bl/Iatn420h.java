/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:37 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420h
************************************************************
**        * FILE NAME            : Iatn420h.java
**        * CLASS NAME           : Iatn420h
**        * INSTANCE NAME        : Iatn420h
************************************************************
************************************************************************
*  PROGRAM: IATN420H
*   AUTHOR: ARI GROSSMAN
*     DATE: JAN 28, 1998
*  PURPOSE: TRANSFER PROCESSING FROM CREF TO CREF OR TEACHERS CONTRACT.
*
*  HISTORY: 04/98 LB: FIXED GRADED RATE DATE.
*           10/02 TD: ADDED ROUTINE TO DELETE CREF RECORDS THAT ARE
*                     FULLY TRANSFERRED TO TIAA
*           10/11 TD: ADDED ESCAPE BOTTOM LINE 5820
*
*           05/03 JT: FIXED THE TOTAL GUAR AND DIV AMOUNTS.
*
*           01/09 OS  TIAA ACCESS CHANGES. SC 010809.
*           04/12 OS  RATE BASE EXPANSION. SC 041212.
*        04/07/17 RC  RESTOWED FOR PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420h extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Iaa_From_Cntrct;
    private DbsField pnd_Iaa_From_Pyee_N;
    private DbsField pnd_Iaa_To_Cntrct;
    private DbsField pnd_Iaa_To_Pyee_N;

    private DbsGroup pnd_Iaa_To_Pyee_N__R_Field_1;
    private DbsField pnd_Iaa_To_Pyee_N_Pnd_Iaa_To_Pyee;
    private DbsField pnd_From_Fund_Only;
    private DbsField pnd_From_Fund;
    private DbsField pnd_From_Acct_Code;
    private DbsField pnd_Frm_Unit_Typ;
    private DbsField pnd_From_Aftr_Xfr_Units;
    private DbsField pnd_From_Aftr_Xfr_Guar;
    private DbsField pnd_From_Reval_Unit_Val;
    private DbsField pnd_Rate_Code_Table;
    private DbsField pnd_To_Fund;
    private DbsField pnd_To_Acct_Code;
    private DbsField pnd_To_Unit_Typ;
    private DbsField pnd_To_Rate_Cde;
    private DbsField pnd_To_Xfr_Units;
    private DbsField pnd_To_Xfr_Guar;
    private DbsField pnd_To_Xfr_Divid;
    private DbsField pnd_To_Aftr_Xfr_Units;
    private DbsField pnd_To_Aftr_Xfr_Guar;
    private DbsField pnd_To_Aftr_Xfr_Divid;
    private DbsField pnd_To_Reval_Unit_Val;
    private DbsField pnd_Ivc_Pro_Adj;
    private DbsField pnd_Per_Ivc_Pro_Adj;
    private DbsField pnd_Ivc_Ind;
    private DbsField pnd_Iaa_New_Issue;
    private DbsField pnd_Iaa_New_Issue_Re;
    private DbsField pnd_Full_Contract_Out;
    private DbsField pnd_Check_Date;

    private DbsGroup pnd_Check_Date__R_Field_2;
    private DbsField pnd_Check_Date_Pnd_Check_Date_A;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Effective_Date;
    private DbsField pnd_Next_Bus_Dte;
    private DbsField pnd_Next_Pay_Dte;
    private DbsField pnd_Time;
    private DbsField pnd_Eff_Dte_03_31;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Res;
    private DbsField pnd_Num;
    private DbsField pnd_Found_Rate;
    private DbsField pnd_On_File_Already;
    private DbsField pnd_Frst_Pymnt_Curr_Dte;
    private DbsField pnd_Tiaa_Rate_Code;
    private DbsField pnd_Datd;
    private DbsField pnd_Res_Code;
    private DbsField pnd_Rate_Code_Breakdown;

    private DbsGroup pnd_Rate_Code_Breakdown__R_Field_3;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2;
    private DbsField pnd_From_Fund_H;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_4;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_5;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_File_Mode;
    private DbsField pnd_Fund_Tot;
    private DbsField pnd_To_Cntrct_Fund;
    private DbsField pnd_Mode;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_E;
    private DbsField pnd_C;
    private DbsField pnd_Max_Prods;
    private DbsField pnd_Wk_Pmt;
    private DbsField pnd_Wk_Div;
    private DbsField pnd_From_Contract_Type;
    private DbsField pnd_One_Byte_Fund;
    private DbsField pnd_Two_Byte_Fund;
    private DbsField pnd_Fund_Cd_1;
    private DbsField pnd_Partial_Transfer;
    private DbsField pnd_Contract;
    private DbsField pnd_From_Funds_Count;
    private DbsField pnd_X;
    private DbsField no_Of_Cref_Funds;
    private DbsField first_Pass;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();
        pnd_Iaa_From_Cntrct = parameters.newFieldInRecord("pnd_Iaa_From_Cntrct", "#IAA-FROM-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_From_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_From_Pyee_N = parameters.newFieldInRecord("pnd_Iaa_From_Pyee_N", "#IAA-FROM-PYEE-N", FieldType.NUMERIC, 2);
        pnd_Iaa_From_Pyee_N.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_To_Cntrct = parameters.newFieldInRecord("pnd_Iaa_To_Cntrct", "#IAA-TO-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_To_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_To_Pyee_N = parameters.newFieldInRecord("pnd_Iaa_To_Pyee_N", "#IAA-TO-PYEE-N", FieldType.NUMERIC, 2);
        pnd_Iaa_To_Pyee_N.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_To_Pyee_N__R_Field_1 = parameters.newGroupInRecord("pnd_Iaa_To_Pyee_N__R_Field_1", "REDEFINE", pnd_Iaa_To_Pyee_N);
        pnd_Iaa_To_Pyee_N_Pnd_Iaa_To_Pyee = pnd_Iaa_To_Pyee_N__R_Field_1.newFieldInGroup("pnd_Iaa_To_Pyee_N_Pnd_Iaa_To_Pyee", "#IAA-TO-PYEE", FieldType.STRING, 
            2);
        pnd_From_Fund_Only = parameters.newFieldArrayInRecord("pnd_From_Fund_Only", "#FROM-FUND-ONLY", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_From_Fund_Only.setParameterOption(ParameterOption.ByReference);
        pnd_From_Fund = parameters.newFieldArrayInRecord("pnd_From_Fund", "#FROM-FUND", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_From_Fund.setParameterOption(ParameterOption.ByReference);
        pnd_From_Acct_Code = parameters.newFieldArrayInRecord("pnd_From_Acct_Code", "#FROM-ACCT-CODE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_From_Acct_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Frm_Unit_Typ = parameters.newFieldArrayInRecord("pnd_Frm_Unit_Typ", "#FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_Frm_Unit_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_From_Aftr_Xfr_Units = parameters.newFieldArrayInRecord("pnd_From_Aftr_Xfr_Units", "#FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new 
            DbsArrayController(1, 20));
        pnd_From_Aftr_Xfr_Units.setParameterOption(ParameterOption.ByReference);
        pnd_From_Aftr_Xfr_Guar = parameters.newFieldArrayInRecord("pnd_From_Aftr_Xfr_Guar", "#FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 20));
        pnd_From_Aftr_Xfr_Guar.setParameterOption(ParameterOption.ByReference);
        pnd_From_Reval_Unit_Val = parameters.newFieldArrayInRecord("pnd_From_Reval_Unit_Val", "#FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new 
            DbsArrayController(1, 20));
        pnd_From_Reval_Unit_Val.setParameterOption(ParameterOption.ByReference);
        pnd_Rate_Code_Table = parameters.newFieldArrayInRecord("pnd_Rate_Code_Table", "#RATE-CODE-TABLE", FieldType.STRING, 3, new DbsArrayController(1, 
            80));
        pnd_Rate_Code_Table.setParameterOption(ParameterOption.ByReference);
        pnd_To_Fund = parameters.newFieldArrayInRecord("pnd_To_Fund", "#TO-FUND", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_To_Fund.setParameterOption(ParameterOption.ByReference);
        pnd_To_Acct_Code = parameters.newFieldArrayInRecord("pnd_To_Acct_Code", "#TO-ACCT-CODE", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_To_Acct_Code.setParameterOption(ParameterOption.ByReference);
        pnd_To_Unit_Typ = parameters.newFieldArrayInRecord("pnd_To_Unit_Typ", "#TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_To_Unit_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_To_Rate_Cde = parameters.newFieldArrayInRecord("pnd_To_Rate_Cde", "#TO-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_To_Rate_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_To_Xfr_Units = parameters.newFieldArrayInRecord("pnd_To_Xfr_Units", "#TO-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 
            20));
        pnd_To_Xfr_Units.setParameterOption(ParameterOption.ByReference);
        pnd_To_Xfr_Guar = parameters.newFieldArrayInRecord("pnd_To_Xfr_Guar", "#TO-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_To_Xfr_Guar.setParameterOption(ParameterOption.ByReference);
        pnd_To_Xfr_Divid = parameters.newFieldArrayInRecord("pnd_To_Xfr_Divid", "#TO-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_To_Xfr_Divid.setParameterOption(ParameterOption.ByReference);
        pnd_To_Aftr_Xfr_Units = parameters.newFieldArrayInRecord("pnd_To_Aftr_Xfr_Units", "#TO-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 
            20));
        pnd_To_Aftr_Xfr_Units.setParameterOption(ParameterOption.ByReference);
        pnd_To_Aftr_Xfr_Guar = parameters.newFieldArrayInRecord("pnd_To_Aftr_Xfr_Guar", "#TO-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_To_Aftr_Xfr_Guar.setParameterOption(ParameterOption.ByReference);
        pnd_To_Aftr_Xfr_Divid = parameters.newFieldArrayInRecord("pnd_To_Aftr_Xfr_Divid", "#TO-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_To_Aftr_Xfr_Divid.setParameterOption(ParameterOption.ByReference);
        pnd_To_Reval_Unit_Val = parameters.newFieldArrayInRecord("pnd_To_Reval_Unit_Val", "#TO-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 
            20));
        pnd_To_Reval_Unit_Val.setParameterOption(ParameterOption.ByReference);
        pnd_Ivc_Pro_Adj = parameters.newFieldInRecord("pnd_Ivc_Pro_Adj", "#IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Pro_Adj.setParameterOption(ParameterOption.ByReference);
        pnd_Per_Ivc_Pro_Adj = parameters.newFieldInRecord("pnd_Per_Ivc_Pro_Adj", "#PER-IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Ivc_Pro_Adj.setParameterOption(ParameterOption.ByReference);
        pnd_Ivc_Ind = parameters.newFieldInRecord("pnd_Ivc_Ind", "#IVC-IND", FieldType.STRING, 1);
        pnd_Ivc_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_New_Issue = parameters.newFieldInRecord("pnd_Iaa_New_Issue", "#IAA-NEW-ISSUE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_New_Issue_Re = parameters.newFieldInRecord("pnd_Iaa_New_Issue_Re", "#IAA-NEW-ISSUE-RE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue_Re.setParameterOption(ParameterOption.ByReference);
        pnd_Full_Contract_Out = parameters.newFieldInRecord("pnd_Full_Contract_Out", "#FULL-CONTRACT-OUT", FieldType.STRING, 1);
        pnd_Full_Contract_Out.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Date = parameters.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Check_Date.setParameterOption(ParameterOption.ByReference);

        pnd_Check_Date__R_Field_2 = parameters.newGroupInRecord("pnd_Check_Date__R_Field_2", "REDEFINE", pnd_Check_Date);
        pnd_Check_Date_Pnd_Check_Date_A = pnd_Check_Date__R_Field_2.newFieldInGroup("pnd_Check_Date_Pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 
            8);
        pnd_Todays_Dte = parameters.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.DATE);
        pnd_Todays_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Effective_Date = parameters.newFieldInRecord("pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.DATE);
        pnd_Effective_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Next_Bus_Dte = parameters.newFieldInRecord("pnd_Next_Bus_Dte", "#NEXT-BUS-DTE", FieldType.DATE);
        pnd_Next_Bus_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Next_Pay_Dte = parameters.newFieldInRecord("pnd_Next_Pay_Dte", "#NEXT-PAY-DTE", FieldType.DATE);
        pnd_Next_Pay_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Time = parameters.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Time.setParameterOption(ParameterOption.ByReference);
        pnd_Eff_Dte_03_31 = parameters.newFieldInRecord("pnd_Eff_Dte_03_31", "#EFF-DTE-03-31", FieldType.STRING, 1);
        pnd_Eff_Dte_03_31.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Res = localVariables.newFieldInRecord("pnd_Res", "#RES", FieldType.STRING, 3);
        pnd_Num = localVariables.newFieldInRecord("pnd_Num", "#NUM", FieldType.PACKED_DECIMAL, 3);
        pnd_Found_Rate = localVariables.newFieldInRecord("pnd_Found_Rate", "#FOUND-RATE", FieldType.STRING, 1);
        pnd_On_File_Already = localVariables.newFieldInRecord("pnd_On_File_Already", "#ON-FILE-ALREADY", FieldType.STRING, 1);
        pnd_Frst_Pymnt_Curr_Dte = localVariables.newFieldInRecord("pnd_Frst_Pymnt_Curr_Dte", "#FRST-PYMNT-CURR-DTE", FieldType.DATE);
        pnd_Tiaa_Rate_Code = localVariables.newFieldInRecord("pnd_Tiaa_Rate_Code", "#TIAA-RATE-CODE", FieldType.STRING, 2);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Res_Code = localVariables.newFieldInRecord("pnd_Res_Code", "#RES-CODE", FieldType.STRING, 3);
        pnd_Rate_Code_Breakdown = localVariables.newFieldInRecord("pnd_Rate_Code_Breakdown", "#RATE-CODE-BREAKDOWN", FieldType.STRING, 3);

        pnd_Rate_Code_Breakdown__R_Field_3 = localVariables.newGroupInRecord("pnd_Rate_Code_Breakdown__R_Field_3", "REDEFINE", pnd_Rate_Code_Breakdown);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1 = pnd_Rate_Code_Breakdown__R_Field_3.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1", "#RATE-CODE-1", 
            FieldType.STRING, 1);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2 = pnd_Rate_Code_Breakdown__R_Field_3.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2", "#RATE-CODE-2", 
            FieldType.STRING, 2);
        pnd_From_Fund_H = localVariables.newFieldInRecord("pnd_From_Fund_H", "#FROM-FUND-H", FieldType.STRING, 1);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_Fund_Tot = localVariables.newFieldInRecord("pnd_Fund_Tot", "#FUND-TOT", FieldType.STRING, 3);
        pnd_To_Cntrct_Fund = localVariables.newFieldInRecord("pnd_To_Cntrct_Fund", "#TO-CNTRCT-FUND", FieldType.STRING, 1);
        pnd_Mode = localVariables.newFieldInRecord("pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_E = localVariables.newFieldInRecord("pnd_E", "#E", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Prods = localVariables.newFieldInRecord("pnd_Max_Prods", "#MAX-PRODS", FieldType.PACKED_DECIMAL, 3);
        pnd_Wk_Pmt = localVariables.newFieldInRecord("pnd_Wk_Pmt", "#WK-PMT", FieldType.NUMERIC, 9, 2);
        pnd_Wk_Div = localVariables.newFieldInRecord("pnd_Wk_Div", "#WK-DIV", FieldType.NUMERIC, 9, 2);
        pnd_From_Contract_Type = localVariables.newFieldInRecord("pnd_From_Contract_Type", "#FROM-CONTRACT-TYPE", FieldType.STRING, 1);
        pnd_One_Byte_Fund = localVariables.newFieldInRecord("pnd_One_Byte_Fund", "#ONE-BYTE-FUND", FieldType.STRING, 1);
        pnd_Two_Byte_Fund = localVariables.newFieldInRecord("pnd_Two_Byte_Fund", "#TWO-BYTE-FUND", FieldType.STRING, 2);
        pnd_Fund_Cd_1 = localVariables.newFieldInRecord("pnd_Fund_Cd_1", "#FUND-CD-1", FieldType.STRING, 1);
        pnd_Partial_Transfer = localVariables.newFieldInRecord("pnd_Partial_Transfer", "#PARTIAL-TRANSFER", FieldType.STRING, 1);
        pnd_Contract = localVariables.newFieldInRecord("pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_From_Funds_Count = localVariables.newFieldInRecord("pnd_From_Funds_Count", "#FROM-FUNDS-COUNT", FieldType.NUMERIC, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        no_Of_Cref_Funds = localVariables.newFieldInRecord("no_Of_Cref_Funds", "NO-OF-CREF-FUNDS", FieldType.NUMERIC, 3);
        first_Pass = localVariables.newFieldInRecord("first_Pass", "FIRST-PASS", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal999.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Prods.setInitialValue(80);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420h() throws Exception
    {
        super("Iatn420h");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420H", onError);
        //* *********
        //*   WRITE 'IATN420H'
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        pnd_Contract.setValue(pnd_Iaa_From_Cntrct);                                                                                                                       //Natural: ON ERROR;//Natural: MOVE #IAA-FROM-CNTRCT TO #CONTRACT
        pnd_Return_Code.setValue("T1");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T1'
                                                                                                                                                                          //Natural: PERFORM #AI-CONTRACTS
        sub_Pnd_Ai_Contracts();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                         //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #UPDATE-AND-STORE-CPR-RECS
        sub_Pnd_Update_And_Store_Cpr_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                             //Natural: ASSIGN #CNTRCT-PPCN-NBR := #IAA-TO-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                                //Natural: ASSIGN #CNTRCT-PAYEE := #IAA-TO-PYEE-N
        //*  NEW VIEW PREFIX FROM IAAL999                        041212 START
        ldaIaal999.getVw_cpr().startDatabaseFind                                                                                                                          //Natural: FIND ( 1 ) CPR WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "R1R",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        R1R:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("R1R")))
        {
            ldaIaal999.getVw_cpr().setIfNotFoundControlFlag(false);
            //*  IF    IAA-CPR-1.CNTRCT-PART-PPCN-NBR NE #CNTRCT-PPCN-NBR
            //*      OR IAA-CPR-1.CNTRCT-PART-PAYEE-CDE NE #CNTRCT-PAYEE
            //*    ESCAPE BOTTOM(R1R.)
            //*  END-IF
            pnd_Mode.setValue(ldaIaal999.getCpr_Cntrct_Mode_Ind());                                                                                                       //Natural: ASSIGN #MODE := CPR.CNTRCT-MODE-IND
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_From_Contract_Type.setValue("C");                                                                                                                             //Natural: MOVE 'C' TO #FROM-CONTRACT-TYPE
        DbsUtil.callnat(Nazn6031.class , getCurrentProcessState(), pnd_Datd, ldaIaal999.getIaa_Cntrct_Cntrct_Issue_Dte(), pnd_Check_Date_Pnd_Check_Date_A,                //Natural: CALLNAT 'NAZN6031' #DATD IAA-CNTRCT.CNTRCT-ISSUE-DTE #CHECK-DATE-A IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE IAA-CNTRCT.CNTRCT-FP-DUE-DTE-DD #MODE IAA-CNTRCT.CNTRCT-OPTN-CDE
            ldaIaal999.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte(), ldaIaal999.getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd(), pnd_Mode, ldaIaal999.getIaa_Cntrct_Cntrct_Optn_Cde());
        if (condition(Global.isEscape())) return;
        //* **
        //* **  GET NO OF FROM-FUNDS
        FX:                                                                                                                                                               //Natural: FOR #X 1 TO 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            if (condition(pnd_From_Fund.getValue(pnd_X).equals(" ")))                                                                                                     //Natural: IF #FROM-FUND ( #X ) = ' '
            {
                if (true) break FX;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FX. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_From_Funds_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #FROM-FUNDS-COUNT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* **
        //* **                                                      /* TD 10/2002
        pnd_Return_Code.setValue("T4");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T4'
        FF:                                                                                                                                                               //Natural: FOR #J = 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            if (condition(pnd_From_Fund.getValue(pnd_J).equals(" ")))                                                                                                     //Natural: IF #FROM-FUND ( #J ) = ' '
            {
                if (true) break FF;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FF. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_From_Fund_Only.getValue(pnd_J).equals("Y")))                                                                                                //Natural: IF #FROM-FUND-ONLY ( #J ) = 'Y'
            {
                pnd_Two_Byte_Fund.setValue(pnd_From_Fund.getValue(pnd_J));                                                                                                //Natural: ASSIGN #TWO-BYTE-FUND := #FROM-FUND ( #J )
                pnd_One_Byte_Fund.setValue(pnd_From_Acct_Code.getValue(pnd_J));                                                                                           //Natural: ASSIGN #ONE-BYTE-FUND := #FROM-ACCT-CODE ( #J )
                if (condition(pnd_Frm_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                              //Natural: IF #FRM-UNIT-TYP ( #J ) = 'A'
                {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
                    sub_Pnd_Annual_Fund_Cnv();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FF"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_From_Aftr_Xfr_Units.getValue(pnd_J).greater(getZero()) && pnd_From_Aftr_Xfr_Guar.getValue(pnd_J).greater(getZero())))               //Natural: IF #FROM-AFTR-XFR-UNITS ( #J ) > 0 AND #FROM-AFTR-XFR-GUAR ( #J ) > 0
                    {
                        pnd_Partial_Transfer.setValue("Y");                                                                                                               //Natural: MOVE 'Y' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Partial_Transfer.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                    sub_Pnd_Monthly_Cnv_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FF"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_From_Aftr_Xfr_Units.getValue(pnd_J).greater(getZero())))                                                                            //Natural: IF #FROM-AFTR-XFR-UNITS ( #J ) > 0
                    {
                        pnd_Partial_Transfer.setValue("Y");                                                                                                               //Natural: MOVE 'Y' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Partial_Transfer.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO #PARTIAL-TRANSFER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                  //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_Fund_Tot);                                                                                               //Natural: ASSIGN #W-FUND-CODE = #FUND-TOT
                //* *
                if (condition(pnd_Partial_Transfer.equals("Y") || (pnd_Partial_Transfer.equals(" ") && pnd_Full_Contract_Out.equals("Y"))))                               //Natural: IF #PARTIAL-TRANSFER = 'Y' OR ( #PARTIAL-TRANSFER = ' ' AND #FULL-CONTRACT-OUT = 'Y' )
                {
                    //* *                                                         /* TD 10/02
                    if (condition(pnd_Partial_Transfer.equals(" ")))                                                                                                      //Natural: IF #PARTIAL-TRANSFER = ' '
                    {
                        if (condition(first_Pass.equals(" ")))                                                                                                            //Natural: IF FIRST-PASS = ' '
                        {
                                                                                                                                                                          //Natural: PERFORM GET-NO-OF-EXISTING-FROM-FUNDS
                            sub_Get_No_Of_Existing_From_Funds();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FF"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        first_Pass.setValue("Y");                                                                                                                         //Natural: ASSIGN FIRST-PASS := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    //* *                                                         /**
                    pnd_Return_Code.setValue("M4");                                                                                                                       //Natural: ASSIGN #RETURN-CODE = 'M4'
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CREF-FUND-FROM
                    sub_Pnd_Update_Cref_Fund_From();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FF"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                        //Natural: IF #RETURN-CODE NE ' '
                    {
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Partial_Transfer.equals(" ") && pnd_Full_Contract_Out.equals(" ")))                                                                 //Natural: IF #PARTIAL-TRANSFER = ' ' AND #FULL-CONTRACT-OUT = ' '
                    {
                                                                                                                                                                          //Natural: PERFORM #DELETE-CREF-FUND-FROM
                        sub_Pnd_Delete_Cref_Fund_From();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FF"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* *
                if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                            //Natural: IF #RETURN-CODE NE ' '
                {
                    getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                 //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FF"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FF"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F3:                                                                                                                                                               //Natural: FOR #J = 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            if (condition(pnd_To_Fund.getValue(pnd_J).equals(" ")))                                                                                                       //Natural: IF #TO-FUND ( #J ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Two_Byte_Fund.setValue(pnd_To_Fund.getValue(pnd_J));                                                                                                      //Natural: ASSIGN #TWO-BYTE-FUND := #TO-FUND ( #J )
            pnd_One_Byte_Fund.setValue(pnd_To_Acct_Code.getValue(pnd_J));                                                                                                 //Natural: ASSIGN #ONE-BYTE-FUND := #TO-ACCT-CODE ( #J )
            if (condition(pnd_To_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                                   //Natural: IF #TO-UNIT-TYP ( #J ) = 'A'
            {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
                sub_Pnd_Annual_Fund_Cnv();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                sub_Pnd_Monthly_Cnv_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_To_Acct_Code.getValue(pnd_J).equals("T") || pnd_To_Acct_Code.getValue(pnd_J).equals("G")))                                                  //Natural: IF #TO-ACCT-CODE ( #J ) = 'T' OR = 'G'
            {
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                    //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                       //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-TO-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-ON-FILE
                sub_Pnd_Check_If_On_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_On_File_Already.equals("Y")))                                                                                                           //Natural: IF #ON-FILE-ALREADY = 'Y'
                {
                    pnd_Return_Code.setValue("M3");                                                                                                                       //Natural: ASSIGN #RETURN-CODE = 'M3'
                                                                                                                                                                          //Natural: PERFORM #UPDATE-TIAA-FUND-TO
                    sub_Pnd_Update_Tiaa_Fund_To();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Return_Code.setValue("M3");                                                                                                                       //Natural: ASSIGN #RETURN-CODE = 'M3'
                                                                                                                                                                          //Natural: PERFORM #STORE-TIAA-FUND-TO
                    sub_Pnd_Store_Tiaa_Fund_To();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  010809
                if (condition(pnd_To_Acct_Code.getValue(pnd_J).equals("R") || pnd_To_Acct_Code.getValue(pnd_J).equals("D")))                                              //Natural: IF #TO-ACCT-CODE ( #J ) = 'R' OR = 'D'
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                   //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-TO-PYEE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                              //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                 //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-ON-FILE
                sub_Pnd_Check_If_On_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_On_File_Already.equals("Y")))                                                                                                           //Natural: IF #ON-FILE-ALREADY = 'Y'
                {
                    pnd_Return_Code.setValue("M4");                                                                                                                       //Natural: ASSIGN #RETURN-CODE = 'M4'
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CREF-FUND-TO
                    sub_Pnd_Update_Cref_Fund_To();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Return_Code.setValue("M4");                                                                                                                       //Natural: ASSIGN #RETURN-CODE = 'M4'
                                                                                                                                                                          //Natural: PERFORM #STORE-CREF-FUND-TO
                    sub_Pnd_Store_Cref_Fund_To();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                     //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Return_Code.setValue("T4");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T4'
                                                                                                                                                                          //Natural: PERFORM #STORE-CREF-FUND-FROM-AI
        sub_Pnd_Store_Cref_Fund_From_Ai();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                         //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("T3");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T3'
                                                                                                                                                                          //Natural: PERFORM #STORE-TIAA-FUND-TO-AI
        sub_Pnd_Store_Tiaa_Fund_To_Ai();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            getReports().write(0, "BAD RETURN CODE",pnd_Return_Code," FROM",Global.getPROGRAM());                                                                         //Natural: WRITE 'BAD RETURN CODE' #RETURN-CODE ' FROM' *PROGRAM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CREF-FUND-FROM
        //* **********************************************************************
        //* *
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DELETE-CREF-FUND-FROM
        //* **********************************************************************
        //* *
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-CREF-FUND-FROM-AI
        //* **********************************************************************
        //*       #W-FUND-CODE        := #FUND-TOT
        //* *
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-CREF-FUND-TO
        //* *. FOR #I = 1 TO 20
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-TIAA-FUND-TO-AI
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CONTRACTS
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #AI-CPR
        //* **********************************************************************
        //*  WRITE 'AFTER IMAGE CPR'
        //* **********************************************************************
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-IF-ON-FILE
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CREF-FUND-TO
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-FUND-CNV
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-CNV-FUND
        //* ******************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-AND-STORE-CPR-RECS
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CLONE-NEW-CONTRACT
        //* **********************************************************************
        //*  COMMENT OUT THE REFERENCES TO IAA-CNTRCT-2 OR       041212 START
        //*  REPLACE WITH IAA-CNTRCT
        //*  RESET IAA-CNTRCT-2
        //*  MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-2
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CONTRACT-TO
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CLONE-NEW-CPR
        //* **********************************************************************
        //*  COMMENT OUT THE REFERENCES TO CPR OR       041212 START
        //*  REPLACE WITH CPR
        //*    RESET CPR
        //*    MOVE BY NAME CPR TO CPR
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CPR-FROM-FULL-OUT
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CPR-FROM
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-CPR-TO
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DELETE-ZERO-UNIT-FUNDS
        //* **********************************************************************
        //* **********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-TIAA-FUND-TO
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WEIGHTED-AVERAGE-98
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-TIAA-FUND-TO
        //*          PERFORM #WEIGHTED-AVERAGE-98
        //*  05/03 MAKE SURE THAT THE NEW TOTAL REFLECTS CORRECTLY THE ARRAY TOTAL
        //* **********************************************************************
        //* * ?? THIS ROUTINE IS NOT PERFORMED ANYWHERE IN THIS PROGRAM - OS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-CREF-FUND-AI
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-REAL-AI
        //* **********************************************************************
        //* **
        //* *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NO-OF-EXISTING-FROM-FUNDS
        //* ************************************
        //* *
        //* *
        //* **
        //* **********************************************************************
    }
    private void sub_Pnd_Update_Cref_Fund_From() throws Exception                                                                                                         //Natural: #UPDATE-CREF-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1B",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1B:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1B")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                if (condition(pnd_Frm_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                              //Natural: IF #FRM-UNIT-TYP ( #J ) = 'A'
                {
                    ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt().setValue(pnd_From_Aftr_Xfr_Guar.getValue(pnd_J));                                                 //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT := #FROM-AFTR-XFR-GUAR ( #J )
                    if (condition(pnd_Eff_Dte_03_31.equals("Y")))                                                                                                         //Natural: IF #EFF-DTE-03-31 = 'Y'
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(pnd_From_Reval_Unit_Val.getValue(pnd_J));                                               //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := #FROM-REVAL-UNIT-VAL ( #J )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(0);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                      //Natural: ASSIGN IAA-CREF-FUND-RCRD.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1).setValue(pnd_From_Aftr_Xfr_Units.getValue(pnd_J));                                          //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := #FROM-AFTR-XFR-UNITS ( #J )
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte().setValue(pnd_Todays_Dte);                                                                         //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-LST-XFR-OUT-DTE := #TODAYS-DTE
                //* ***
                //*  DELETE FUNDS THAT ARE FULLY TRANSFERRED              /* TD 10/2002
                //* *
                if (condition(pnd_From_Aftr_Xfr_Units.getValue(pnd_J).equals(getZero())))                                                                                 //Natural: IF #FROM-AFTR-XFR-UNITS ( #J ) = 0
                {
                    if (condition(no_Of_Cref_Funds.greater(pnd_From_Funds_Count)))                                                                                        //Natural: IF NO-OF-CREF-FUNDS GT #FROM-FUNDS-COUNT
                    {
                        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().deleteDBRow("R1B");                                                                                         //Natural: DELETE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(no_Of_Cref_Funds.equals(pnd_From_Funds_Count)))                                                                                     //Natural: IF NO-OF-CREF-FUNDS = #FROM-FUNDS-COUNT
                        {
                            if (condition(no_Of_Cref_Funds.greater(pnd_J)))                                                                                               //Natural: IF NO-OF-CREF-FUNDS GT #J
                            {
                                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().deleteDBRow("R1B");                                                                                 //Natural: DELETE
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(no_Of_Cref_Funds.equals(pnd_J)))                                                                                                //Natural: IF NO-OF-CREF-FUNDS = #J
                            {
                                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().updateDBRow("R1B");                                                                                 //Natural: UPDATE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* ***                                                      /* TD 10/2002
                    ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().updateDBRow("R1B");                                                                                             //Natural: UPDATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1B;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1B. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Delete_Cref_Fund_From() throws Exception                                                                                                         //Natural: #DELETE-CREF-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1D",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1D:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1D")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().deleteDBRow("R1D");                                                                                                 //Natural: DELETE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1D;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1D. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Cref_Fund_From_Ai() throws Exception                                                                                                       //Natural: #STORE-CREF-FUND-FROM-AI
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                             //Natural: ASSIGN #W-CNTRCT-PAYEE := #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1T",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1T:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1T")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*          IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE   =  #W-FUND-CODE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1T;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1T. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Store_Cref_Fund_To() throws Exception                                                                                                            //Natural: #STORE-CREF-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  010809
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().reset();                                                                                                                    //Natural: RESET IAA-CREF-FUND-RCRD
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().setValue(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr);                                                      //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR := #W-CNTRCT-PPCN-NBR
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().setValue(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee);                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE := #W-CNTRCT-PAYEE
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().setValue(pnd_Fund_Tot);                                                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE := #FUND-TOT
        F5:                                                                                                                                                               //Natural: FOR #I = 1 TO #MAX-PRODS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Prods)); pnd_I.nadd(1))
        {
            pnd_Rate_Code_Breakdown.setValue(pnd_Rate_Code_Table.getValue(pnd_I));                                                                                        //Natural: MOVE #RATE-CODE-TABLE ( #I ) TO #RATE-CODE-BREAKDOWN
            if (condition(pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1.equals(pnd_To_Acct_Code.getValue(pnd_J))))                                                              //Natural: IF #RATE-CODE-1 = #TO-ACCT-CODE ( #J )
            {
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(1).setValue(pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2);                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( 1 ) := #RATE-CODE-2
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Dte().getValue(1).reset();                                                                                     //Natural: RESET IAA-CREF-FUND-RCRD.CREF-RATE-DTE ( 1 )
                if (true) break F5;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F5. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_To_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                                       //Natural: IF #TO-UNIT-TYP ( #J ) = 'A'
        {
            ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt().setValue(pnd_To_Aftr_Xfr_Guar.getValue(pnd_J));                                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT := #TO-AFTR-XFR-GUAR ( #J )
            if (condition(pnd_Eff_Dte_03_31.equals("Y")))                                                                                                                 //Natural: IF #EFF-DTE-03-31 = 'Y'
            {
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(pnd_To_Reval_Unit_Val.getValue(pnd_J));                                                         //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := #TO-REVAL-UNIT-VAL ( #J )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(0);                                                                                             //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1).setValue(pnd_To_Aftr_Xfr_Units.getValue(pnd_J));                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := #TO-AFTR-XFR-UNITS ( #J )
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                              //Natural: ASSIGN IAA-CREF-FUND-RCRD.LST-TRANS-DTE := #TIME
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte().setValue(pnd_Todays_Dte);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-XFR-ISS-DTE := #TODAYS-DTE
        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                                  //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-LST-XFR-IN-DTE := #TODAYS-DTE
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-CREF-FUND-RCRD
        pnd_Return_Code.reset();                                                                                                                                          //Natural: RESET #RETURN-CODE
    }
    private void sub_Pnd_Store_Tiaa_Fund_To_Ai() throws Exception                                                                                                         //Natural: #STORE-TIAA-FUND-TO-AI
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                            //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                               //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-TO-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE = ' '
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1G",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1G:
        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1G")))
        {
            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*  010809
                if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U09") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W09")  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'U09' OR = 'W09' OR = 'U11' OR = 'W11'
                    || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U11") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W11")))
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE TO #W-FUND-CODE
                                                                                                                                                                          //Natural: PERFORM #STORE-REAL-AI
                    sub_Pnd_Store_Real_Ai();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1G"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1G"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().reset();                                                                                                       //Natural: RESET IAA-TIAA-FUND-TRANS
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());                                                        //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE = #TIME
                    pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Dte());                                                                              //Natural: ASSIGN #DATE-TIME-P = IAA-TIAA-FUND-TRANS.TRANS-DTE
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte()),      //Natural: COMPUTE IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                        new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID = '2'
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                 //Natural: ASSIGN IAA-TIAA-FUND-TRANS.LST-TRANS-DTE = #TIME
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                         //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                 //Natural: STORE IAA-TIAA-FUND-TRANS
                    pnd_Return_Code.reset();                                                                                                                              //Natural: RESET #RETURN-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1G;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1G. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Ai_Contracts() throws Exception                                                                                                                  //Natural: #AI-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CONTRACT
        (
        "FNR",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Contract, WcType.WITH) },
        1
        );
        FNR:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FNR")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal999.getVw_iaa_Cntrct_Trans().reset();                                                                                                                  //Natural: RESET IAA-CNTRCT-TRANS
            ldaIaal999.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cntrct());                                                                           //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
            ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Time);                                                                                                //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE = #TIME
            pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte());                                                                                         //Natural: ASSIGN #DATE-TIME-P = IAA-CNTRCT-TRANS.TRANS-DTE
            ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte()),                    //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
            ldaIaal999.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                            //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE = #TIME
            ldaIaal999.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID = '2'
            ldaIaal999.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                    //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
            ldaIaal999.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                            //Natural: STORE IAA-CNTRCT-TRANS
            pnd_Return_Code.reset();                                                                                                                                      //Natural: RESET #RETURN-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Ai_Cpr() throws Exception                                                                                                                        //Natural: #AI-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseFind                                                                                                                          //Natural: FIND ( 1 ) CPR WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPX",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        CPX:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CPX")))
        {
            ldaIaal999.getVw_cpr().setIfNotFoundControlFlag(false);
            //*  IF CPR.CNTRCT-PART-PPCN-NBR =  #CNTRCT-PPCN-NBR AND
            //*    CPR.CNTRCT-PART-PAYEE-CDE =  #CNTRCT-PAYEE
            ldaIaal999.getVw_iaa_Cpr_Trans().reset();                                                                                                                     //Natural: RESET IAA-CPR-TRANS
            ldaIaal999.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal999.getVw_cpr());                                                                                     //Natural: MOVE BY NAME CPR TO IAA-CPR-TRANS
            ldaIaal999.getIaa_Cpr_Trans_Trans_Dte().setValue(pnd_Time);                                                                                                   //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE = #TIME
            pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cpr_Trans_Trans_Dte());                                                                                            //Natural: ASSIGN #DATE-TIME-P = IAA-CPR-TRANS.TRANS-DTE
            ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte()), new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P)); //Natural: COMPUTE IAA-CPR-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
            ldaIaal999.getIaa_Cpr_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                               //Natural: ASSIGN IAA-CPR-TRANS.LST-TRANS-DTE = #TIME
            ldaIaal999.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                     //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID = '2'
            ldaIaal999.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                       //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
            ldaIaal999.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                               //Natural: STORE IAA-CPR-TRANS
            //*          WRITE IAA-CPR-TRANS
            pnd_Return_Code.reset();                                                                                                                                      //Natural: RESET #RETURN-CODE
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_If_On_File() throws Exception                                                                                                              //Natural: #CHECK-IF-ON-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File_Already.reset();                                                                                                                                      //Natural: RESET #ON-FILE-ALREADY
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_Fund_Tot);                                                                                                       //Natural: ASSIGN #W-FUND-CODE = #FUND-TOT
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Z",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1Z:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1Z")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_On_File_Already.setValue("Y");                                                                                                                        //Natural: MOVE 'Y' TO #ON-FILE-ALREADY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Update_Cref_Fund_To() throws Exception                                                                                                           //Natural: #UPDATE-CREF-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_Fund_Tot);                                                                                                       //Natural: ASSIGN #W-FUND-CODE = #FUND-TOT
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R12",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R12:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R12")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                if (condition(pnd_To_Unit_Typ.getValue(pnd_J).equals("A")))                                                                                               //Natural: IF #TO-UNIT-TYP ( #J ) = 'A'
                {
                    ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt().setValue(pnd_To_Aftr_Xfr_Guar.getValue(pnd_J));                                                   //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT := #TO-AFTR-XFR-GUAR ( #J )
                    if (condition(pnd_Eff_Dte_03_31.equals("Y")))                                                                                                         //Natural: IF #EFF-DTE-03-31 = 'Y'
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(pnd_To_Reval_Unit_Val.getValue(pnd_J));                                                 //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := #TO-REVAL-UNIT-VAL ( #J )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Unit_Val().setValue(0);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                      //Natural: ASSIGN IAA-CREF-FUND-RCRD.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1).setValue(pnd_To_Aftr_Xfr_Units.getValue(pnd_J));                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := #TO-AFTR-XFR-UNITS ( #J )
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                          //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-LST-XFR-IN-DTE := #TODAYS-DTE
                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().updateDBRow("R12");                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R12;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R12. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Annual_Fund_Cnv() throws Exception                                                                                                               //Natural: #ANNUAL-FUND-CNV
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet1141 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet1141++;
            //*  010809
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet1141++;
            pnd_Fund_Cd_1.setValue("U");                                                                                                                                  //Natural: MOVE 'U' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("2");                                                                                                                                  //Natural: MOVE '2' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                         //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FUND-TOT LEAVING NO
    }
    private void sub_Pnd_Monthly_Cnv_Fund() throws Exception                                                                                                              //Natural: #MONTHLY-CNV-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet1155 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet1155++;
            //*  010809
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet1155++;
            pnd_Fund_Cd_1.setValue("W");                                                                                                                                  //Natural: MOVE 'W' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("4");                                                                                                                                  //Natural: MOVE '4' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                         //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FUND-TOT LEAVING NO
    }
    private void sub_Pnd_Update_And_Store_Cpr_Recs() throws Exception                                                                                                     //Natural: #UPDATE-AND-STORE-CPR-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Iaa_New_Issue.equals("Y")))                                                                                                                     //Natural: IF #IAA-NEW-ISSUE = 'Y'
        {
            pnd_Return_Code.setValue("M1");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M1'
                                                                                                                                                                          //Natural: PERFORM #CLONE-NEW-CONTRACT
            sub_Pnd_Clone_New_Contract();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract.setValue(pnd_Iaa_To_Cntrct);                                                                                                                     //Natural: MOVE #IAA-TO-CNTRCT TO #CONTRACT
            pnd_Return_Code.setValue("T1");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'T1'
                                                                                                                                                                          //Natural: PERFORM #AI-CONTRACTS
            sub_Pnd_Ai_Contracts();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Return_Code.setValue("M2");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M2'
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                       //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                          //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #CLONE-NEW-CPR
            sub_Pnd_Clone_New_Cpr();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("M2");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'M2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                              //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        //* *
        if (condition(pnd_Full_Contract_Out.equals("Y")))                                                                                                                 //Natural: IF #FULL-CONTRACT-OUT = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CPR-FROM-FULL-OUT
            sub_Pnd_Update_Cpr_From_Full_Out();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CPR-FROM
            sub_Pnd_Update_Cpr_From();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        pnd_Return_Code.setValue("T2");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                              //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #AI-CPR
        sub_Pnd_Ai_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Iaa_New_Issue.equals("Y")))                                                                                                                     //Natural: IF #IAA-NEW-ISSUE = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Iaa_New_Issue_Re.equals("R")))                                                                                                              //Natural: IF #IAA-NEW-ISSUE-RE = 'R'
            {
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CONTRACT-TO
                sub_Pnd_Update_Contract_To();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract.setValue(pnd_Iaa_To_Cntrct);                                                                                                                     //Natural: MOVE #IAA-TO-CNTRCT TO #CONTRACT
            pnd_Return_Code.setValue("T1");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'T1'
                                                                                                                                                                          //Natural: PERFORM #AI-CONTRACTS
            sub_Pnd_Ai_Contracts();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Return_Code.setValue("M2");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'M2'
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                         //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                            //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-TO-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #UPDATE-CPR-TO
            sub_Pnd_Update_Cpr_To();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                //Natural: IF #RETURN-CODE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Code.setValue("T2");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                             //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                                //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-TO-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #AI-CPR
        sub_Pnd_Ai_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Clone_New_Contract() throws Exception                                                                                                            //Natural: #CLONE-NEW-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        (
        "FND",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_From_Cntrct, WcType.WITH) },
        1
        );
        FND:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FND")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal999.getIaa_Cntrct_Cntrct_Ppcn_Nbr().setValue(pnd_Iaa_To_Cntrct);                                                                                       //Natural: ASSIGN IAA-CNTRCT.CNTRCT-PPCN-NBR := #IAA-TO-CNTRCT
            ldaIaal999.getIaa_Cntrct_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                  //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE := #TIME
            E8:                                                                                                                                                           //Natural: FOR #E = 1 TO 20
            for (pnd_E.setValue(1); condition(pnd_E.lessOrEqual(20)); pnd_E.nadd(1))
            {
                //*  010809
                if (condition(pnd_To_Acct_Code.getValue(pnd_E).equals("R") || pnd_To_Acct_Code.getValue(pnd_E).equals("D")))                                              //Natural: IF #TO-ACCT-CODE ( #E ) = 'R' OR = 'D'
                {
                    pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                               //Natural: ASSIGN #CNTRCT-PPCN-NBR := #IAA-FROM-CNTRCT
                    pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_N);                                                                                  //Natural: ASSIGN #CNTRCT-PAYEE := #IAA-FROM-PYEE-N
                    ldaIaal999.getVw_cpr().startDatabaseFind                                                                                                              //Natural: FIND ( 1 ) CPR WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
                    (
                    "R5R",
                    new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
                    1
                    );
                    R5R:
                    while (condition(ldaIaal999.getVw_cpr().readNextRow("R5R")))
                    {
                        ldaIaal999.getVw_cpr().setIfNotFoundControlFlag(false);
                        //*        IF IAA-CPR-1.CNTRCT-PART-PPCN-NBR  NE #CNTRCT-PPCN-NBR
                        //*            OR IAA-CPR-1.CNTRCT-PART-PAYEE-CDE NE #CNTRCT-PAYEE
                        //*          ESCAPE BOTTOM(E8.)
                        //*        END-IF
                        if (condition(! (DbsUtil.maskMatches(ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde(),"NNN")) || ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde().equals("098")       //Natural: IF CPR.PRTCPNT-RSDNCY-CDE NOT = MASK ( NNN ) OR CPR.PRTCPNT-RSDNCY-CDE = '098' OR = '099'
                            || ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde().equals("099")))
                        {
                            ldaIaal999.getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re().setValue("035");                                                                           //Natural: ASSIGN IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE := '035'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaIaal999.getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re().setValue(ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde());                                          //Natural: ASSIGN IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE := CPR.PRTCPNT-RSDNCY-CDE
                        }                                                                                                                                                 //Natural: END-IF
                        if (true) break E8;                                                                                                                               //Natural: ESCAPE BOTTOM ( E8. )
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("E8"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("E8"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal999.getVw_iaa_Cntrct().insertDBRow();                                                                                                                  //Natural: STORE IAA-CNTRCT
            //*  COMMENT OUT THE REFERENCES TO IAA-CNTRCT-2 OR       041212 END
            //*  REPLACE WITH IAA-CNTRCT
            pnd_Return_Code.reset();                                                                                                                                      //Natural: RESET #RETURN-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Update_Contract_To() throws Exception                                                                                                            //Natural: #UPDATE-CONTRACT-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Res.reset();                                                                                                                                                  //Natural: RESET #RES
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                             //Natural: ASSIGN #CNTRCT-PPCN-NBR := #IAA-TO-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                                //Natural: ASSIGN #CNTRCT-PAYEE := #IAA-TO-PYEE-N
        ldaIaal999.getVw_cpr().startDatabaseFind                                                                                                                          //Natural: FIND ( 1 ) CPR WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "RR",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        RR:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("RR")))
        {
            ldaIaal999.getVw_cpr().setIfNotFoundControlFlag(false);
            //*  IF IAA-CPR-1.CNTRCT-PART-PPCN-NBR  NE #CNTRCT-PPCN-NBR
            //*      OR IAA-CPR-1.CNTRCT-PART-PAYEE-CDE NE #CNTRCT-PAYEE
            //*    ESCAPE BOTTOM(RR.)
            //*  END-IF
            if (condition(! (DbsUtil.maskMatches(ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde(),"NNN")) || ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde().equals("098")                   //Natural: IF CPR.PRTCPNT-RSDNCY-CDE NOT = MASK ( NNN ) OR CPR.PRTCPNT-RSDNCY-CDE = '098' OR = '099'
                || ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde().equals("099")))
            {
                pnd_Res.setValue("035");                                                                                                                                  //Natural: ASSIGN #RES := '035'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Res.setValue(ldaIaal999.getCpr_Prtcpnt_Rsdncy_Cde());                                                                                                 //Natural: ASSIGN #RES := CPR.PRTCPNT-RSDNCY-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
        (
        "FJD",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_To_Cntrct, WcType.WITH) },
        1
        );
        FJD:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FJD")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal999.getIaa_Cntrct_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                  //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE := #TIME
            ldaIaal999.getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re().setValue(pnd_Res);                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE := #RES
            ldaIaal999.getVw_iaa_Cntrct().updateDBRow("FJD");                                                                                                             //Natural: UPDATE
            pnd_Return_Code.reset();                                                                                                                                      //Natural: RESET #RETURN-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Clone_New_Cpr() throws Exception                                                                                                                 //Natural: #CLONE-NEW-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPQ",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPQ:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CPQ")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Iaa_To_Cntrct);                                                                                     //Natural: ASSIGN CPR.CNTRCT-PART-PPCN-NBR := #IAA-TO-CNTRCT
                ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().setValue(pnd_Iaa_To_Pyee_N);                                                                                    //Natural: ASSIGN CPR.CNTRCT-PART-PAYEE-CDE := #IAA-TO-PYEE-N
                ldaIaal999.getCpr_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                     //Natural: ASSIGN CPR.LST-TRANS-DTE := #TIME
                ldaIaal999.getCpr_Cntrct_Actvty_Cde().setValue(1);                                                                                                        //Natural: ASSIGN CPR.CNTRCT-ACTVTY-CDE := 1
                //*  INSERTED BY JUN TO MAKE SURE THAT CREF & TIAA DATA ARE IN THE
                //*  PROPER OCCURRENCE
                ldaIaal999.getCpr_Cntrct_Resdl_Ivc_Amt().getValue("*").reset();                                                                                           //Natural: RESET CPR.CNTRCT-RESDL-IVC-AMT ( * ) CPR.CNTRCT-RCVRY-TYPE-IND ( * ) CPR.CNTRCT-PER-IVC-AMT ( * ) CPR.CNTRCT-IVC-AMT ( * ) CPR.CNTRCT-IVC-USED-AMT ( * ) CPR.CNTRCT-RTB-AMT ( * ) CPR.CNTRCT-RTB-PERCENT ( * ) CPR.CNTRCT-COMPANY-CD ( * )
                ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue("*").reset();
                ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue("*").reset();
                ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue("*").reset();
                ldaIaal999.getCpr_Cntrct_Ivc_Used_Amt().getValue("*").reset();
                ldaIaal999.getCpr_Cntrct_Rtb_Amt().getValue("*").reset();
                ldaIaal999.getCpr_Cntrct_Rtb_Percent().getValue("*").reset();
                ldaIaal999.getCpr_Cntrct_Company_Cd().getValue("*").reset();
                if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().less("Z") || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),"'6L7'")                 //Natural: IF CPR.CNTRCT-PART-PPCN-NBR < 'Z' OR CPR.CNTRCT-PART-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
                    || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),"'6M7'") || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),
                    "'6N7'")))
                {
                    ldaIaal999.getCpr_Cntrct_Company_Cd().getValue(1).setValue("T");                                                                                      //Natural: ASSIGN CPR.CNTRCT-COMPANY-CD ( 1 ) := 'T'
                    if (condition(pnd_Ivc_Ind.equals("Y")))                                                                                                               //Natural: IF #IVC-IND = 'Y'
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(1).setValue(pnd_Per_Ivc_Pro_Adj);                                                                 //Natural: ASSIGN CPR.CNTRCT-PER-IVC-AMT ( 1 ) := #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(1).setValue(pnd_Ivc_Pro_Adj);                                                                         //Natural: ASSIGN CPR.CNTRCT-IVC-AMT ( 1 ) := #IVC-PRO-ADJ
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FROM TIAA
                    if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().less("Z") || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),"'6L7'")             //Natural: IF CPR.CNTRCT-PART-PPCN-NBR < 'Z' OR CPR.CNTRCT-PART-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
                        || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),"'6M7'") || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),
                        "'6N7'")))
                    {
                        ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(1).setValue(ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(1));                            //Natural: ASSIGN CPR.CNTRCT-RCVRY-TYPE-IND ( 1 ) := CPR.CNTRCT-RCVRY-TYPE-IND ( 1 )
                        //*  FROM CREF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(1).setValue(ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(2));                            //Natural: ASSIGN CPR.CNTRCT-RCVRY-TYPE-IND ( 1 ) := CPR.CNTRCT-RCVRY-TYPE-IND ( 2 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal999.getCpr_Cntrct_Company_Cd().getValue(2).setValue("C");                                                                                      //Natural: ASSIGN CPR.CNTRCT-COMPANY-CD ( 2 ) := 'C'
                    if (condition(pnd_Ivc_Ind.equals("Y")))                                                                                                               //Natural: IF #IVC-IND = 'Y'
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(2).setValue(pnd_Per_Ivc_Pro_Adj);                                                                 //Natural: ASSIGN CPR.CNTRCT-PER-IVC-AMT ( 2 ) := #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(2).setValue(pnd_Ivc_Pro_Adj);                                                                         //Natural: ASSIGN CPR.CNTRCT-IVC-AMT ( 2 ) := #IVC-PRO-ADJ
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FROM TIAA
                    if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().less("Z") || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),"'6L7'")             //Natural: IF CPR.CNTRCT-PART-PPCN-NBR < 'Z' OR CPR.CNTRCT-PART-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
                        || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),"'6M7'") || DbsUtil.maskMatches(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),
                        "'6N7'")))
                    {
                        ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(2).setValue(ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(1));                            //Natural: ASSIGN CPR.CNTRCT-RCVRY-TYPE-IND ( 2 ) := CPR.CNTRCT-RCVRY-TYPE-IND ( 1 )
                        //*  FROM CREF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(2).setValue(ldaIaal999.getCpr_Cntrct_Rcvry_Type_Ind().getValue(2));                            //Natural: ASSIGN CPR.CNTRCT-RCVRY-TYPE-IND ( 2 ) := CPR.CNTRCT-RCVRY-TYPE-IND ( 2 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getCpr_Cntrct_Cmbne_Cde().setValue(" ");                                                                                                       //Natural: ASSIGN CPR.CNTRCT-CMBNE-CDE := ' '
                ldaIaal999.getCpr_Cntrct_Spirt_Cde().setValue(" ");                                                                                                       //Natural: ASSIGN CPR.CNTRCT-SPIRT-CDE := ' '
                ldaIaal999.getCpr_Cntrct_Spirt_Amt().setValue(0);                                                                                                         //Natural: ASSIGN CPR.CNTRCT-SPIRT-AMT := 0
                ldaIaal999.getCpr_Cntrct_Spirt_Srce().setValue(" ");                                                                                                      //Natural: ASSIGN CPR.CNTRCT-SPIRT-SRCE := ' '
                ldaIaal999.getCpr_Cntrct_Spirt_Arr_Dte().setValue(0);                                                                                                     //Natural: ASSIGN CPR.CNTRCT-SPIRT-ARR-DTE := 0
                ldaIaal999.getCpr_Cntrct_Spirt_Prcss_Dte().setValue(0);                                                                                                   //Natural: ASSIGN CPR.CNTRCT-SPIRT-PRCSS-DTE := 0
                ldaIaal999.getCpr_Cntrct_Fed_Tax_Amt().setValue(0);                                                                                                       //Natural: ASSIGN CPR.CNTRCT-FED-TAX-AMT := 0
                ldaIaal999.getCpr_Cntrct_State_Cde().setValue(" ");                                                                                                       //Natural: ASSIGN CPR.CNTRCT-STATE-CDE := ' '
                ldaIaal999.getCpr_Cntrct_State_Tax_Amt().setValue(0);                                                                                                     //Natural: ASSIGN CPR.CNTRCT-STATE-TAX-AMT := 0
                ldaIaal999.getCpr_Cntrct_Local_Cde().setValue(" ");                                                                                                       //Natural: ASSIGN CPR.CNTRCT-LOCAL-CDE := ' '
                ldaIaal999.getCpr_Cntrct_Local_Tax_Amt().setValue(0);                                                                                                     //Natural: ASSIGN CPR.CNTRCT-LOCAL-TAX-AMT := 0
                ldaIaal999.getCpr_Cntrct_Lst_Chnge_Dte().setValue(0);                                                                                                     //Natural: ASSIGN CPR.CNTRCT-LST-CHNGE-DTE := 0
                ldaIaal999.getCpr_Cpr_Xfr_Term_Cde().setValue(" ");                                                                                                       //Natural: ASSIGN CPR.CPR-XFR-TERM-CDE := ' '
                ldaIaal999.getCpr_Cpr_Lgl_Res_Cde().setValue(" ");                                                                                                        //Natural: ASSIGN CPR.CPR-LGL-RES-CDE := ' '
                ldaIaal999.getCpr_Cpr_Xfr_Iss_Dte().setValue(pnd_Effective_Date);                                                                                         //Natural: ASSIGN CPR.CPR-XFR-ISS-DTE := #EFFECTIVE-DATE
                ldaIaal999.getVw_cpr().insertDBRow();                                                                                                                     //Natural: STORE CPR
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  COMMENT OUT THE REFERENCES TO CPR OR       041212 END
        //*  REPLACE WITH CPR
    }
    private void sub_Pnd_Update_Cpr_From_Full_Out() throws Exception                                                                                                      //Natural: #UPDATE-CPR-FROM-FULL-OUT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "CC",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CC:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CC")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                if (condition(pnd_Ivc_Ind.equals("Y")))                                                                                                                   //Natural: IF #IVC-IND = 'Y'
                {
                    //*  INSERTED BY JUN TO MAKE SURE THAT CREF IVC GOES TO CREF AND
                    //*  TIAA IVC GOES TO TIAA
                    //*  TIAA
                    if (condition(ldaIaal999.getCpr_Cntrct_Company_Cd().getValue(1).notEquals(" ")))                                                                      //Natural: IF CPR.CNTRCT-COMPANY-CD ( 1 ) NE ' '
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(1).nsubtract(pnd_Per_Ivc_Pro_Adj);                                                                //Natural: COMPUTE CPR.CNTRCT-PER-IVC-AMT ( 1 ) = CPR.CNTRCT-PER-IVC-AMT ( 1 ) - #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(1).nsubtract(pnd_Ivc_Pro_Adj);                                                                        //Natural: COMPUTE CPR.CNTRCT-IVC-AMT ( 1 ) = CPR.CNTRCT-IVC-AMT ( 1 ) - #IVC-PRO-ADJ
                        //*  CREF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(2).nsubtract(pnd_Per_Ivc_Pro_Adj);                                                                //Natural: COMPUTE CPR.CNTRCT-PER-IVC-AMT ( 2 ) = CPR.CNTRCT-PER-IVC-AMT ( 2 ) - #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(2).nsubtract(pnd_Ivc_Pro_Adj);                                                                        //Natural: COMPUTE CPR.CNTRCT-IVC-AMT ( 2 ) = CPR.CNTRCT-IVC-AMT ( 2 ) - #IVC-PRO-ADJ
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getCpr_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                     //Natural: ASSIGN CPR.LST-TRANS-DTE = #TIME
                ldaIaal999.getCpr_Cntrct_Actvty_Cde().setValue(9);                                                                                                        //Natural: ASSIGN CPR.CNTRCT-ACTVTY-CDE = 9
                ldaIaal999.getCpr_Cntrct_Trmnte_Rsn().setValue("TR");                                                                                                     //Natural: ASSIGN CPR.CNTRCT-TRMNTE-RSN = 'TR'
                ldaIaal999.getVw_cpr().updateDBRow("CC");                                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Update_Cpr_From() throws Exception                                                                                                               //Natural: #UPDATE-CPR-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "CJ",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CJ:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CJ")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                if (condition(pnd_Ivc_Ind.equals("Y")))                                                                                                                   //Natural: IF #IVC-IND = 'Y'
                {
                    //*  INSERTED BY JUN TO MAKE SURE THAT CREF IVC GOES TO CREF AND
                    //*  TIAA IVC GOES TO TIAA
                    //*  TIAA
                    if (condition(ldaIaal999.getCpr_Cntrct_Company_Cd().getValue(1).notEquals(" ")))                                                                      //Natural: IF CPR.CNTRCT-COMPANY-CD ( 1 ) NE ' '
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(1).nsubtract(pnd_Per_Ivc_Pro_Adj);                                                                //Natural: COMPUTE CPR.CNTRCT-PER-IVC-AMT ( 1 ) = CPR.CNTRCT-PER-IVC-AMT ( 1 ) - #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(1).nsubtract(pnd_Ivc_Pro_Adj);                                                                        //Natural: COMPUTE CPR.CNTRCT-IVC-AMT ( 1 ) = CPR.CNTRCT-IVC-AMT ( 1 ) - #IVC-PRO-ADJ
                        //*  CREF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(2).nsubtract(pnd_Per_Ivc_Pro_Adj);                                                                //Natural: COMPUTE CPR.CNTRCT-PER-IVC-AMT ( 2 ) = CPR.CNTRCT-PER-IVC-AMT ( 2 ) - #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(2).nsubtract(pnd_Ivc_Pro_Adj);                                                                        //Natural: COMPUTE CPR.CNTRCT-IVC-AMT ( 2 ) = CPR.CNTRCT-IVC-AMT ( 2 ) - #IVC-PRO-ADJ
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getCpr_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                     //Natural: ASSIGN CPR.LST-TRANS-DTE = #TIME
                ldaIaal999.getVw_cpr().updateDBRow("CJ");                                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Update_Cpr_To() throws Exception                                                                                                                 //Natural: #UPDATE-CPR-TO
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "CS",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CS:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CS")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                if (condition(pnd_Ivc_Ind.equals("Y")))                                                                                                                   //Natural: IF #IVC-IND = 'Y'
                {
                    //*  INSERTED BY JUN TO MAKE SURE THAT CREF IVC GOES TO CREF AND
                    //*  TIAA IVC GOES TO TIAA
                    //*  TIAA
                    if (condition(ldaIaal999.getCpr_Cntrct_Company_Cd().getValue(1).notEquals(" ")))                                                                      //Natural: IF CPR.CNTRCT-COMPANY-CD ( 1 ) NE ' '
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(1).nadd(pnd_Per_Ivc_Pro_Adj);                                                                     //Natural: COMPUTE CPR.CNTRCT-PER-IVC-AMT ( 1 ) = CPR.CNTRCT-PER-IVC-AMT ( 1 ) + #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(1).nadd(pnd_Ivc_Pro_Adj);                                                                             //Natural: COMPUTE CPR.CNTRCT-IVC-AMT ( 1 ) = CPR.CNTRCT-IVC-AMT ( 1 ) + #IVC-PRO-ADJ
                        //*  CREF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getCpr_Cntrct_Per_Ivc_Amt().getValue(2).nadd(pnd_Per_Ivc_Pro_Adj);                                                                     //Natural: COMPUTE CPR.CNTRCT-PER-IVC-AMT ( 2 ) = CPR.CNTRCT-PER-IVC-AMT ( 2 ) + #PER-IVC-PRO-ADJ
                        ldaIaal999.getCpr_Cntrct_Ivc_Amt().getValue(2).nadd(pnd_Ivc_Pro_Adj);                                                                             //Natural: COMPUTE CPR.CNTRCT-IVC-AMT ( 2 ) = CPR.CNTRCT-IVC-AMT ( 2 ) + #IVC-PRO-ADJ
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal999.getCpr_Cntrct_Actvty_Cde().equals(9)))                                                                                           //Natural: IF CPR.CNTRCT-ACTVTY-CDE = 9
                {
                                                                                                                                                                          //Natural: PERFORM #DELETE-ZERO-UNIT-FUNDS
                    sub_Pnd_Delete_Zero_Unit_Funds();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getCpr_Cntrct_Actvty_Cde().setValue(1);                                                                                                        //Natural: ASSIGN CPR.CNTRCT-ACTVTY-CDE = 1
                ldaIaal999.getCpr_Cntrct_Trmnte_Rsn().setValue("  ");                                                                                                     //Natural: ASSIGN CPR.CNTRCT-TRMNTE-RSN = '  '
                ldaIaal999.getCpr_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                     //Natural: ASSIGN CPR.LST-TRANS-DTE = #TIME
                ldaIaal999.getVw_cpr().updateDBRow("CS");                                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Delete_Zero_Unit_Funds() throws Exception                                                                                                        //Natural: #DELETE-ZERO-UNIT-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                            //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #IAA-TO-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_N);                                                                                               //Natural: ASSIGN #W-CNTRCT-PAYEE := #IAA-TO-PYEE-N
        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Q",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1Q:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1Q")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(1).equals(getZero()) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt().equals(getZero()))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) = 0 AND IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT = 0
                {
                    ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().deleteDBRow("R1Q");                                                                                             //Natural: DELETE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1Q;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1Q. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Tiaa_Fund_To() throws Exception                                                                                                            //Natural: #STORE-TIAA-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  WRITE 'STORE TIAA'
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().reset();                                                                                                                    //Natural: RESET IAA-TIAA-FUND-RCRD
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().setValue(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr);                                                      //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR := #W-CNTRCT-PPCN-NBR
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().setValue(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee);                                                        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := #W-CNTRCT-PAYEE
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().setValue(pnd_Fund_Tot);                                                                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := #FUND-TOT
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().setValue(pnd_To_Aftr_Xfr_Guar.getValue(pnd_J));                                                               //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := #TO-AFTR-XFR-GUAR ( #J )
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().setValue(pnd_To_Aftr_Xfr_Divid.getValue(pnd_J));                                                              //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := #TO-AFTR-XFR-DIVID ( #J )
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(1).setValue(pnd_To_Rate_Cde.getValue(pnd_J));                                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( 1 ) := #TO-RATE-CDE ( #J )
        if (condition(pnd_To_Acct_Code.getValue(pnd_J).equals("T")))                                                                                                      //Natural: IF #TO-ACCT-CODE ( #J ) = 'T'
        {
            ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(1).setValue(pnd_Effective_Date);                                                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( 1 ) := #EFFECTIVE-DATE
            //*  LB 04/98
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(1).compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(1)),  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( 1 ) := #NEXT-PAY-DTE - 1
                pnd_Next_Pay_Dte.subtract(1));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1).setValue(pnd_To_Aftr_Xfr_Guar.getValue(pnd_J));                                                   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1 ) := #TO-AFTR-XFR-GUAR ( #J )
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1).setValue(pnd_To_Aftr_Xfr_Divid.getValue(pnd_J));                                                  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( 1 ) := #TO-AFTR-XFR-DIVID ( #J )
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                              //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TIME
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                                  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-LST-XFR-IN-DTE := #TODAYS-DTE
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte().setValue(pnd_Todays_Dte);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-XFR-ISS-DTE := #TODAYS-DTE
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-TIAA-FUND-RCRD
        pnd_Return_Code.reset();                                                                                                                                          //Natural: RESET #RETURN-CODE
    }
    private void sub_Pnd_Weighted_Average_98() throws Exception                                                                                                           //Natural: #WEIGHTED-AVERAGE-98
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_C).greater(getZero())))                                                               //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #C ) GT 0
        {
            pnd_Frst_Pymnt_Curr_Dte.setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_C));                                                           //Natural: ASSIGN #FRST-PYMNT-CURR-DTE := IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #C )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Frst_Pymnt_Curr_Dte.setValue(pnd_Datd);                                                                                                                   //Natural: ASSIGN #FRST-PYMNT-CURR-DTE := #DATD
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
        (
        "FDR",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_To_Cntrct, WcType.WITH) },
        1
        );
        FDR:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FDR")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Wk_Pmt.setValue(pnd_To_Xfr_Guar.getValue(pnd_J));                                                                                                             //Natural: ASSIGN #WK-PMT := #TO-XFR-GUAR ( #J )
        pnd_Wk_Div.setValue(pnd_To_Xfr_Divid.getValue(pnd_J));                                                                                                            //Natural: ASSIGN #WK-DIV := #TO-XFR-DIVID ( #J )
        if (condition(pnd_To_Acct_Code.getValue(pnd_J).equals("G")))                                                                                                      //Natural: IF #TO-ACCT-CODE ( #J ) = 'G'
        {
            DbsUtil.callnat(Nazn6033.class , getCurrentProcessState(), ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_C), pnd_Frst_Pymnt_Curr_Dte,         //Natural: CALLNAT 'NAZN6033' IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #C ) #FRST-PYMNT-CURR-DTE #NEXT-PAY-DTE IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #C ) IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #C ) #WK-PMT #WK-DIV IAA-CNTRCT.CNTRCT-OPTN-CDE IAA-CNTRCT.CNTRCT-ISSUE-DTE
                pnd_Next_Pay_Dte, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_C), ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_C), 
                pnd_Wk_Pmt, pnd_Wk_Div, ldaIaal999.getIaa_Cntrct_Cntrct_Optn_Cde(), ldaIaal999.getIaa_Cntrct_Cntrct_Issue_Dte());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Nazn6033.class , getCurrentProcessState(), ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_C), pnd_Frst_Pymnt_Curr_Dte,         //Natural: CALLNAT 'NAZN6033' IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #C ) #FRST-PYMNT-CURR-DTE #EFFECTIVE-DATE IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #C ) IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #C ) #WK-PMT #WK-DIV IAA-CNTRCT.CNTRCT-OPTN-CDE IAA-CNTRCT.CNTRCT-ISSUE-DTE
                pnd_Effective_Date, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_C), ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_C), 
                pnd_Wk_Pmt, pnd_Wk_Div, ldaIaal999.getIaa_Cntrct_Cntrct_Optn_Cde(), ldaIaal999.getIaa_Cntrct_Cntrct_Issue_Dte());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Update_Tiaa_Fund_To() throws Exception                                                                                                           //Natural: #UPDATE-TIAA-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Found_Rate.reset();                                                                                                                                           //Natural: RESET #FOUND-RATE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_Fund_Tot);                                                                                                       //Natural: ASSIGN #W-FUND-CODE = #FUND-TOT
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1W",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1W:
        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1W")))
        {
            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte().equals(pnd_Todays_Dte) || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte().equals(pnd_Todays_Dte))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-LST-XFR-IN-DTE = #TODAYS-DTE OR IAA-TIAA-FUND-RCRD.TIAA-LST-XFR-OUT-DTE = #TODAYS-DTE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                }                                                                                                                                                         //Natural: END-IF
                //*  05/03 COMMENTED OUT AND REPLACED WITH LINES 5440 - 5455
                //*    IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := #TO-AFTR-XFR-GUAR(#J)
                //*    IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := #TO-AFTR-XFR-DIVID(#J)
                pnd_Num.setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp());                                                                        //Natural: MOVE C*TIAA-RATE-DATA-GRP TO #NUM
                FM:                                                                                                                                                       //Natural: FOR #C = 1 TO #NUM
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_Num)); pnd_C.nadd(1))
                {
                    pnd_Tiaa_Rate_Code.setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_C));                                                        //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #C ) TO #TIAA-RATE-CODE
                    if (condition(pnd_Tiaa_Rate_Code.equals(pnd_To_Rate_Cde.getValue(pnd_J))))                                                                            //Natural: IF #TIAA-RATE-CODE = #TO-RATE-CDE ( #J )
                    {
                                                                                                                                                                          //Natural: PERFORM #WEIGHTED-AVERAGE-98
                        sub_Pnd_Weighted_Average_98();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_C).nadd(pnd_To_Xfr_Guar.getValue(pnd_J));                                        //Natural: ADD #TO-XFR-GUAR ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #C )
                        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_C).nadd(pnd_To_Xfr_Divid.getValue(pnd_J));                                       //Natural: ADD #TO-XFR-DIVID ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #C )
                        pnd_Found_Rate.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #FOUND-RATE
                        if (true) break FM;                                                                                                                               //Natural: ESCAPE BOTTOM ( FM. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1W"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1W"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Found_Rate.equals(" ")))                                                                                                                //Natural: IF #FOUND-RATE = ' '
                {
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_C).setValue(pnd_To_Rate_Cde.getValue(pnd_J));                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #C ) := #TO-RATE-CDE ( #J )
                    if (condition(pnd_To_Acct_Code.getValue(pnd_J).equals("T")))                                                                                          //Natural: IF #TO-ACCT-CODE ( #J ) = 'T'
                    {
                        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_C).setValue(pnd_Effective_Date);                                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #C ) := #EFFECTIVE-DATE
                        //*  LB
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_C).compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_C)),  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #C ) := #NEXT-PAY-DTE - 1
                            pnd_Next_Pay_Dte.subtract(1));
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_C).setValue(pnd_To_Xfr_Guar.getValue(pnd_J));                                        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #C ) := #TO-XFR-GUAR ( #J )
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_C).setValue(pnd_To_Xfr_Divid.getValue(pnd_J));                                       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #C ) := #TO-XFR-DIVID ( #J )
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()),            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( * )
                    DbsField.add(getZero(),ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*")));
                ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()),            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( * )
                    DbsField.add(getZero(),ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*")));
                ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                      //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TIME
                ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte().setValue(pnd_Todays_Dte);                                                                          //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-LST-XFR-IN-DTE := #TODAYS-DTE
                //* *
                ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().updateDBRow("R1W");                                                                                                 //Natural: UPDATE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Store_Cref_Fund_Ai() throws Exception                                                                                                            //Natural: #STORE-CREF-FUND-AI
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R19",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R19:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R19")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*    AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE   =  #W-FUND-CODE
                if (condition(!(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals("U09") || ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals("W09")))) //Natural: ACCEPT IF IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = 'U09' OR = 'W09'
                {
                    continue;
                }
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R19;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R19. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Store_Real_Ai() throws Exception                                                                                                                 //Natural: #STORE-REAL-AI
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R20",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R20:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R20")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-CREF-FUND-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID = '2'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R20;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R20. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_No_Of_Existing_From_Funds() throws Exception                                                                                                     //Natural: GET-NO-OF-EXISTING-FROM-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "RX",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        RX:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("RX")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(" ")))                                                                        //Natural: IF IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = ' '
                {
                    if (true) break RX;                                                                                                                                   //Natural: ESCAPE BOTTOM ( RX. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    no_Of_Cref_Funds.nadd(1);                                                                                                                             //Natural: ADD 1 TO NO-OF-CREF-FUNDS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RX;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RX. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
