/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:34:13 AM
**        * FROM NATURAL SUBPROGRAM : Iaan051e
************************************************************
**        * FILE NAME            : Iaan051e.java
**        * CLASS NAME           : Iaan051e
**        * INSTANCE NAME        : Iaan051e
************************************************************
************************************************************************
* PROGRAM  : IAAN051E
* FUNCTION : THIS IS A BRIDGE MODULE THAT CALLS THE NEW STANDARD
*            ROUTINE (IAAN0510) THAT READS THE EXTERNALIZATION FILE.
*            CALLED BY IAAP110 BECAUSE OF SPACE PROBLEMS IN IAAP110.
* AUTHOR   :
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan051e extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cm_Std_Srt_Seq_Parm;
    private DbsField pnd_Ia_Std_Nm_Cd_Parm;
    private DbsField pnd_Q;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Cm_Std_Srt_Seq_Parm = parameters.newFieldArrayInRecord("pnd_Cm_Std_Srt_Seq_Parm", "#CM-STD-SRT-SEQ-PARM", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            80));
        pnd_Cm_Std_Srt_Seq_Parm.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Std_Nm_Cd_Parm = parameters.newFieldArrayInRecord("pnd_Ia_Std_Nm_Cd_Parm", "#IA-STD-NM-CD-PARM", FieldType.STRING, 2, new DbsArrayController(1, 
            80));
        pnd_Ia_Std_Nm_Cd_Parm.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Q = localVariables.newFieldInRecord("pnd_Q", "#Q", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan051e() throws Exception
    {
        super("Iaan051e");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Cm_Std_Srt_Seq_Parm.getValue(1,":",80).reset();                                                                                                               //Natural: RESET #CM-STD-SRT-SEQ-PARM ( 1:80 ) #IA-STD-NM-CD-PARM ( 1:80 )
        pnd_Ia_Std_Nm_Cd_Parm.getValue(1,":",80).reset();
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Cm_Std_Srt_Seq_Parm.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Cm_Std_Srt_Seq().getValue("*"));                                                       //Natural: ASSIGN #CM-STD-SRT-SEQ-PARM ( * ) := IAAA051Z.#CM-STD-SRT-SEQ ( * )
        pnd_Ia_Std_Nm_Cd_Parm.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"));                                                           //Natural: ASSIGN #IA-STD-NM-CD-PARM ( * ) := IAAA051Z.#IA-STD-NM-CD ( * )
    }

    //
}
