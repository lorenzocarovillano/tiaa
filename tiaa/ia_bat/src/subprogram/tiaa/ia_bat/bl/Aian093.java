/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:58:22 PM
**        * FROM NATURAL SUBPROGRAM : Aian093
************************************************************
**        * FILE NAME            : Aian093.java
**        * CLASS NAME           : Aian093
**        * INSTANCE NAME        : Aian093
************************************************************
**AIAN093 - WRITES PAYMENTS AND UNITS DUE TO AUV COLLECTION FILE

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aian093 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAiaa093 pdaAiaa093;
    private LdaAial0260 ldaAial0260;
    private LdaAial0261 ldaAial0261;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Iauv_Key;

    private DbsGroup pnd_Iauv_Key__R_Field_1;
    private DbsField pnd_Iauv_Key_Pnd_Iauv_Fund_Code;
    private DbsField pnd_Iauv_Key_Filler1;
    private DbsField pnd_Iauv_Key_Pnd_Iauv_Date;

    private DbsGroup pnd_Iauv_Key__R_Field_2;
    private DbsField pnd_Iauv_Key_Pnd_Iauv_Year;
    private DbsField pnd_Iauv_Key_Pnd_Iauv_Month;
    private DbsField pnd_Iauv_Key_Pnd_Iauv_Cutoff_Day;

    private DataAccessProgramView vw_auvcol_View;
    private DbsField auvcol_View_Key_Area;

    private DbsGroup auvcol_View__R_Field_3;
    private DbsField auvcol_View_Pnd_Auv_Fund_Code;
    private DbsField auvcol_View_Pnd_Auv_Date;

    private DbsGroup auvcol_View__R_Field_4;
    private DbsField auvcol_View_Pnd_Auv_Year;
    private DbsField auvcol_View_Pnd_Auv_Month;
    private DbsField auvcol_View_Pnd_Auv_Day;
    private DbsField auvcol_View_Pnd_Auv_Code;
    private DbsField auvcol_View_Filler_1;
    private DbsField auvcol_View_Annuity_Fund;
    private DbsField auvcol_View_Filler_2;
    private DbsField auvcol_View_Future_Payments;
    private DbsField auvcol_View_Act_Est_Indicator;
    private DbsField auvcol_View_Future_Units;
    private DbsField auvcol_View_Filler_3;
    private DbsField auvcol_View_Withdrawals;
    private DbsField auvcol_View_Filler_4;
    private DbsField auvcol_View_New_Considerations;
    private DbsField auvcol_View_Filler_5;
    private DbsField auvcol_View_Ia_Pmts;
    private DbsField auvcol_View_Filler_6;
    private DbsField auvcol_View_Ia_Units;
    private DbsField auvcol_View_Filler_7;
    private DbsField auvcol_View_Post_Cutoff_New_Considerations;
    private DbsField pnd_I;
    private DbsField pnd_Verification_Date;

    private DbsGroup pnd_Verification_Date__R_Field_5;
    private DbsField pnd_Verification_Date_Pnd_Verification_Century;
    private DbsField pnd_Verification_Date_Pnd_Verification_Year;
    private DbsField pnd_Verification_Date_Pnd_Verification_Month;
    private DbsField pnd_Verification_Date_Pnd_Verification_Day;
    private DbsField pnd_Last_Day_Of_Month;
    private DbsField pnd_Auvcol_Key;

    private DbsGroup pnd_Auvcol_Key__R_Field_6;
    private DbsField pnd_Auvcol_Key_Pnd_Auvcol_Fund_Code;
    private DbsField pnd_Auvcol_Key_Pnd_Auvcol_Date;

    private DbsGroup pnd_Auvcol_Key__R_Field_7;
    private DbsField pnd_Auvcol_Key_Pnd_Auvcol_Year;
    private DbsField pnd_Auvcol_Key_Pnd_Auvcol_Month;
    private DbsField pnd_Auvcol_Key_Pnd_Auvcol_Day;
    private DbsField pnd_Auvcol_Key_Pnd_Auvcol_Reval;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0260 = new LdaAial0260();
        registerRecord(ldaAial0260);
        ldaAial0261 = new LdaAial0261();
        registerRecord(ldaAial0261);
        registerRecord(ldaAial0261.getVw_diauvs_View());

        // parameters
        parameters = new DbsRecord();
        pdaAiaa093 = new PdaAiaa093(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Iauv_Key = localVariables.newFieldInRecord("pnd_Iauv_Key", "#IAUV-KEY", FieldType.STRING, 10);

        pnd_Iauv_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Iauv_Key__R_Field_1", "REDEFINE", pnd_Iauv_Key);
        pnd_Iauv_Key_Pnd_Iauv_Fund_Code = pnd_Iauv_Key__R_Field_1.newFieldInGroup("pnd_Iauv_Key_Pnd_Iauv_Fund_Code", "#IAUV-FUND-CODE", FieldType.STRING, 
            1);
        pnd_Iauv_Key_Filler1 = pnd_Iauv_Key__R_Field_1.newFieldInGroup("pnd_Iauv_Key_Filler1", "FILLER1", FieldType.STRING, 1);
        pnd_Iauv_Key_Pnd_Iauv_Date = pnd_Iauv_Key__R_Field_1.newFieldInGroup("pnd_Iauv_Key_Pnd_Iauv_Date", "#IAUV-DATE", FieldType.STRING, 8);

        pnd_Iauv_Key__R_Field_2 = pnd_Iauv_Key__R_Field_1.newGroupInGroup("pnd_Iauv_Key__R_Field_2", "REDEFINE", pnd_Iauv_Key_Pnd_Iauv_Date);
        pnd_Iauv_Key_Pnd_Iauv_Year = pnd_Iauv_Key__R_Field_2.newFieldInGroup("pnd_Iauv_Key_Pnd_Iauv_Year", "#IAUV-YEAR", FieldType.NUMERIC, 4);
        pnd_Iauv_Key_Pnd_Iauv_Month = pnd_Iauv_Key__R_Field_2.newFieldInGroup("pnd_Iauv_Key_Pnd_Iauv_Month", "#IAUV-MONTH", FieldType.NUMERIC, 2);
        pnd_Iauv_Key_Pnd_Iauv_Cutoff_Day = pnd_Iauv_Key__R_Field_2.newFieldInGroup("pnd_Iauv_Key_Pnd_Iauv_Cutoff_Day", "#IAUV-CUTOFF-DAY", FieldType.NUMERIC, 
            2);

        vw_auvcol_View = new DataAccessProgramView(new NameInfo("vw_auvcol_View", "AUVCOL-VIEW"), "AUVCOL1", "AUVCOL1");
        auvcol_View_Key_Area = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Key_Area", "KEY-AREA", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "KEY_AREA");

        auvcol_View__R_Field_3 = vw_auvcol_View.getRecord().newGroupInGroup("auvcol_View__R_Field_3", "REDEFINE", auvcol_View_Key_Area);
        auvcol_View_Pnd_Auv_Fund_Code = auvcol_View__R_Field_3.newFieldInGroup("auvcol_View_Pnd_Auv_Fund_Code", "#AUV-FUND-CODE", FieldType.STRING, 1);
        auvcol_View_Pnd_Auv_Date = auvcol_View__R_Field_3.newFieldInGroup("auvcol_View_Pnd_Auv_Date", "#AUV-DATE", FieldType.STRING, 9);

        auvcol_View__R_Field_4 = auvcol_View__R_Field_3.newGroupInGroup("auvcol_View__R_Field_4", "REDEFINE", auvcol_View_Pnd_Auv_Date);
        auvcol_View_Pnd_Auv_Year = auvcol_View__R_Field_4.newFieldInGroup("auvcol_View_Pnd_Auv_Year", "#AUV-YEAR", FieldType.NUMERIC, 4);
        auvcol_View_Pnd_Auv_Month = auvcol_View__R_Field_4.newFieldInGroup("auvcol_View_Pnd_Auv_Month", "#AUV-MONTH", FieldType.NUMERIC, 2);
        auvcol_View_Pnd_Auv_Day = auvcol_View__R_Field_4.newFieldInGroup("auvcol_View_Pnd_Auv_Day", "#AUV-DAY", FieldType.NUMERIC, 2);
        auvcol_View_Pnd_Auv_Code = auvcol_View__R_Field_4.newFieldInGroup("auvcol_View_Pnd_Auv_Code", "#AUV-CODE", FieldType.STRING, 1);
        auvcol_View_Filler_1 = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Filler_1", "FILLER-1", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FILLER_1");
        auvcol_View_Annuity_Fund = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Annuity_Fund", "ANNUITY-FUND", FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, 
            "ANNUITY_FUND");
        auvcol_View_Filler_2 = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Filler_2", "FILLER-2", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FILLER_2");
        auvcol_View_Future_Payments = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Future_Payments", "FUTURE-PAYMENTS", FieldType.NUMERIC, 
            13, 2, RepeatingFieldStrategy.None, "FUTURE_PAYMENTS");
        auvcol_View_Act_Est_Indicator = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Act_Est_Indicator", "ACT-EST-INDICATOR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ACT_EST_INDICATOR");
        auvcol_View_Future_Units = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Future_Units", "FUTURE-UNITS", FieldType.NUMERIC, 13, 3, RepeatingFieldStrategy.None, 
            "FUTURE_UNITS");
        auvcol_View_Filler_3 = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Filler_3", "FILLER-3", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FILLER_3");
        auvcol_View_Withdrawals = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Withdrawals", "WITHDRAWALS", FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, 
            "WITHDRAWALS");
        auvcol_View_Filler_4 = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Filler_4", "FILLER-4", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FILLER_4");
        auvcol_View_New_Considerations = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_New_Considerations", "NEW-CONSIDERATIONS", FieldType.NUMERIC, 
            13, 2, RepeatingFieldStrategy.None, "NEW_CONSIDERATIONS");
        auvcol_View_Filler_5 = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Filler_5", "FILLER-5", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FILLER_5");
        auvcol_View_Ia_Pmts = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Ia_Pmts", "IA-PMTS", FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, 
            "IA_PMTS");
        auvcol_View_Filler_6 = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Filler_6", "FILLER-6", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FILLER_6");
        auvcol_View_Ia_Units = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Ia_Units", "IA-UNITS", FieldType.NUMERIC, 13, 3, RepeatingFieldStrategy.None, 
            "IA_UNITS");
        auvcol_View_Filler_7 = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Filler_7", "FILLER-7", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FILLER_7");
        auvcol_View_Post_Cutoff_New_Considerations = vw_auvcol_View.getRecord().newFieldInGroup("auvcol_View_Post_Cutoff_New_Considerations", "POST-CUTOFF-NEW-CONSIDERATIONS", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "POST_CUTOFF_NEW_CONSIDERATIONS");
        registerRecord(vw_auvcol_View);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Verification_Date = localVariables.newFieldInRecord("pnd_Verification_Date", "#VERIFICATION-DATE", FieldType.NUMERIC, 8);

        pnd_Verification_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Verification_Date__R_Field_5", "REDEFINE", pnd_Verification_Date);
        pnd_Verification_Date_Pnd_Verification_Century = pnd_Verification_Date__R_Field_5.newFieldInGroup("pnd_Verification_Date_Pnd_Verification_Century", 
            "#VERIFICATION-CENTURY", FieldType.NUMERIC, 2);
        pnd_Verification_Date_Pnd_Verification_Year = pnd_Verification_Date__R_Field_5.newFieldInGroup("pnd_Verification_Date_Pnd_Verification_Year", 
            "#VERIFICATION-YEAR", FieldType.NUMERIC, 2);
        pnd_Verification_Date_Pnd_Verification_Month = pnd_Verification_Date__R_Field_5.newFieldInGroup("pnd_Verification_Date_Pnd_Verification_Month", 
            "#VERIFICATION-MONTH", FieldType.NUMERIC, 2);
        pnd_Verification_Date_Pnd_Verification_Day = pnd_Verification_Date__R_Field_5.newFieldInGroup("pnd_Verification_Date_Pnd_Verification_Day", "#VERIFICATION-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Last_Day_Of_Month = localVariables.newFieldInRecord("pnd_Last_Day_Of_Month", "#LAST-DAY-OF-MONTH", FieldType.NUMERIC, 2);
        pnd_Auvcol_Key = localVariables.newFieldInRecord("pnd_Auvcol_Key", "#AUVCOL-KEY", FieldType.STRING, 10);

        pnd_Auvcol_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Auvcol_Key__R_Field_6", "REDEFINE", pnd_Auvcol_Key);
        pnd_Auvcol_Key_Pnd_Auvcol_Fund_Code = pnd_Auvcol_Key__R_Field_6.newFieldInGroup("pnd_Auvcol_Key_Pnd_Auvcol_Fund_Code", "#AUVCOL-FUND-CODE", FieldType.STRING, 
            1);
        pnd_Auvcol_Key_Pnd_Auvcol_Date = pnd_Auvcol_Key__R_Field_6.newFieldInGroup("pnd_Auvcol_Key_Pnd_Auvcol_Date", "#AUVCOL-DATE", FieldType.STRING, 
            8);

        pnd_Auvcol_Key__R_Field_7 = pnd_Auvcol_Key__R_Field_6.newGroupInGroup("pnd_Auvcol_Key__R_Field_7", "REDEFINE", pnd_Auvcol_Key_Pnd_Auvcol_Date);
        pnd_Auvcol_Key_Pnd_Auvcol_Year = pnd_Auvcol_Key__R_Field_7.newFieldInGroup("pnd_Auvcol_Key_Pnd_Auvcol_Year", "#AUVCOL-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Auvcol_Key_Pnd_Auvcol_Month = pnd_Auvcol_Key__R_Field_7.newFieldInGroup("pnd_Auvcol_Key_Pnd_Auvcol_Month", "#AUVCOL-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Auvcol_Key_Pnd_Auvcol_Day = pnd_Auvcol_Key__R_Field_7.newFieldInGroup("pnd_Auvcol_Key_Pnd_Auvcol_Day", "#AUVCOL-DAY", FieldType.NUMERIC, 2);
        pnd_Auvcol_Key_Pnd_Auvcol_Reval = pnd_Auvcol_Key__R_Field_6.newFieldInGroup("pnd_Auvcol_Key_Pnd_Auvcol_Reval", "#AUVCOL-REVAL", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_auvcol_View.reset();

        ldaAial0260.initializeValues();
        ldaAial0261.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Aian093() throws Exception
    {
        super("Aian093");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM GET-CUTOFF-AND-LAST-DAY
        sub_Get_Cutoff_And_Last_Day();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-COLLECTION-FILE
        sub_Update_Collection_File();
        if (condition(Global.isEscape())) {return;}
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CUTOFF-AND-LAST-DAY
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-COLLECTION-FILE
        //* ****************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FUTURE-PAYMENTS
    }
    //* * THERE ARE NO CALLNAT'S WITHIN THIS SUBPROGRAM
    private void sub_Get_Cutoff_And_Last_Day() throws Exception                                                                                                           //Natural: GET-CUTOFF-AND-LAST-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_Iauv_Key_Pnd_Iauv_Fund_Code.setValue("C");                                                                                                                    //Natural: MOVE 'C' TO #IAUV-FUND-CODE
        pnd_Iauv_Key_Pnd_Iauv_Date.setValue(99999999);                                                                                                                    //Natural: MOVE 99999999 TO #IAUV-DATE
        //*  WRITE '          '
        //*  WRITE ' JS5   AIAN093'
        //*  WRITE '#KEY ' #IAUV-KEY
        ldaAial0261.getVw_diauvs_View().startDatabaseFind                                                                                                                 //Natural: FIND DIAUVS-VIEW WITH AUV-RECORD-KEY = #IAUV-KEY
        (
        "FIND01",
        new Wc[] { new Wc("AUV_RECORD_KEY", "=", pnd_Iauv_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAial0261.getVw_diauvs_View().readNextRow("FIND01", true)))
        {
            ldaAial0261.getVw_diauvs_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0261.getVw_diauvs_View().getAstCOUNTER().equals(0)))                                                                                     //Natural: IF NO RECORDS FOUND
            {
                //*    WRITE 'JS5  NO RECS FOUND '
                pdaAiaa093.getAian093_Linkage_Aian093_Return_Code_Nbr().setValue(614);                                                                                    //Natural: MOVE 614 TO AIAN093-RETURN-CODE-NBR
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAial0260.getDiauvs_Data_Record_Iauv_Month_Data().setValue(ldaAial0261.getDiauvs_View_Auv_Month_Data());                                                    //Natural: MOVE AUV-MONTH-DATA TO IAUV-MONTH-DATA
            //*  WRITE 'AUV-RECORD-KEY = '  AUV-RECORD-KEY
            //*  WRITE 'JS IAUV-PSD-ANL-RVL-VERIF-DATE = ' IAUV-PSD-ANL-RVL-VERIF-DATE
            pnd_Verification_Date_Pnd_Verification_Century.setValue(ldaAial0260.getDiauvs_Data_Record_Iauv_Psd_Anl_Rvl_Verif_Century());                                  //Natural: MOVE IAUV-PSD-ANL-RVL-VERIF-CENTURY TO #VERIFICATION-CENTURY
            pnd_Verification_Date_Pnd_Verification_Year.setValue(ldaAial0260.getDiauvs_Data_Record_Iauv_Psd_Anl_Rvl_Verif_Year());                                        //Natural: MOVE IAUV-PSD-ANL-RVL-VERIF-YEAR TO #VERIFICATION-YEAR
            pnd_Verification_Date_Pnd_Verification_Month.setValue(ldaAial0260.getDiauvs_Data_Record_Iauv_Psd_Anl_Rvl_Verif_Month());                                      //Natural: MOVE IAUV-PSD-ANL-RVL-VERIF-MONTH TO #VERIFICATION-MONTH
            //*  MOVE IAUV-PSD-ANL-RVL-VERIF-DAY TO #VERIFICATION-DAY
            pnd_Verification_Date_Pnd_Verification_Day.setValue(0);                                                                                                       //Natural: MOVE 0 TO #VERIFICATION-DAY
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE '#AIAN093-RETURN-CODE = ' AIAN093-RETURN-CODE-NBR
        pnd_Iauv_Key_Pnd_Iauv_Date.reset();                                                                                                                               //Natural: RESET #IAUV-DATE
        pnd_Iauv_Key_Pnd_Iauv_Date.setValue(pnd_Verification_Date);                                                                                                       //Natural: MOVE #VERIFICATION-DATE TO #IAUV-DATE
        pnd_Iauv_Key_Pnd_Iauv_Fund_Code.setValue("C");                                                                                                                    //Natural: MOVE 'C' TO #IAUV-FUND-CODE
        //*  WRITE ' #IAUV-FUND-CODE ' #IAUV-FUND-CODE
        //*  WRITE ' #IAUV-DATE(B) ' #IAUV-DATE
        //* *MOVE 0 TO #IAUV-CUTOFF-DAY
        //*  WRITE ' #IAUV-DATE(1) ' #IAUV-DATE
        //* *WRITE ' #PROD-CDE-3 ' #PROD-CDE
        //*  WRITE ' #IAUV-DATE-KEY ' #IAUV-KEY
        ldaAial0261.getVw_diauvs_View().startDatabaseFind                                                                                                                 //Natural: FIND ( 1 ) DIAUVS-VIEW WITH AUV-RECORD-KEY = #IAUV-KEY
        (
        "FIND02",
        new Wc[] { new Wc("AUV_RECORD_KEY", "=", pnd_Iauv_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaAial0261.getVw_diauvs_View().readNextRow("FIND02", true)))
        {
            ldaAial0261.getVw_diauvs_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAial0261.getVw_diauvs_View().getAstCOUNTER().equals(0)))                                                                                     //Natural: IF NO RECORDS FOUND
            {
                DbsUtil.terminateApplication("External Subroutine ERROR_MSG_1 is missing from the collection!");                                                          //Natural: PERFORM ERROR-MSG-1
                //*  WRITE 'ERROR1'
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAial0260.getDiauvs_Data_Record_Iauv_Month_Data().setValue(ldaAial0261.getDiauvs_View_Auv_Month_Data());                                                    //Natural: MOVE AUV-MONTH-DATA TO IAUV-MONTH-DATA
            pnd_Iauv_Key_Pnd_Iauv_Cutoff_Day.setValue(ldaAial0260.getDiauvs_Data_Record_Iauv_Md_Cutoff_Day());                                                            //Natural: MOVE IAUV-MD-CUTOFF-DAY TO #IAUV-CUTOFF-DAY
            //*   WRITE ' #IAUV-CUTOFF-DAY ' #IAUV-CUTOFF-DAY
            pnd_Last_Day_Of_Month.setValue(ldaAial0260.getDiauvs_Data_Record_Iauv_Md_Days_In_Month());                                                                    //Natural: MOVE IAUV-MD-DAYS-IN-MONTH TO #LAST-DAY-OF-MONTH
            //*   WRITE '#LAST-DAY-OF-MONTH ' #LAST-DAY-OF-MONTH
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Update_Collection_File() throws Exception                                                                                                            //Natural: UPDATE-COLLECTION-FILE
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I).equals(" ")))                                                                         //Natural: IF FUND-CODE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I).equals("I")))                                                                         //Natural: IF FUND-CODE ( #I ) = 'I'
            {
                pnd_Auvcol_Key_Pnd_Auvcol_Fund_Code.setValue("F");                                                                                                        //Natural: MOVE 'F' TO #AUVCOL-FUND-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Auvcol_Key_Pnd_Auvcol_Fund_Code.setValue(pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I));                                                  //Natural: MOVE FUND-CODE ( #I ) TO #AUVCOL-FUND-CODE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Auvcol_Key_Pnd_Auvcol_Day.setValue(pnd_Last_Day_Of_Month);                                                                                                //Natural: MOVE #LAST-DAY-OF-MONTH TO #AUVCOL-DAY
            pnd_Auvcol_Key_Pnd_Auvcol_Reval.setValue("A");                                                                                                                //Natural: MOVE 'A' TO #AUVCOL-REVAL
            pnd_Auvcol_Key_Pnd_Auvcol_Year.setValue(pnd_Iauv_Key_Pnd_Iauv_Year);                                                                                          //Natural: MOVE #IAUV-YEAR TO #AUVCOL-YEAR
            pnd_Auvcol_Key_Pnd_Auvcol_Month.setValue(pnd_Iauv_Key_Pnd_Iauv_Month);                                                                                        //Natural: MOVE #IAUV-MONTH TO #AUVCOL-MONTH
            //*  WRITE '#AUVCOL-DATE-KEY = ' #AUVCOL-KEY
            vw_auvcol_View.startDatabaseFind                                                                                                                              //Natural: FIND AUVCOL-VIEW WITH KEY-AREA = #AUVCOL-KEY
            (
            "FIND03",
            new Wc[] { new Wc("KEY_AREA", "=", pnd_Auvcol_Key, WcType.WITH) }
            );
            FIND03:
            while (condition(vw_auvcol_View.readNextRow("FIND03", true)))
            {
                vw_auvcol_View.setIfNotFoundControlFlag(false);
                if (condition(vw_auvcol_View.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "ERROR2");                                                                                                                      //Natural: WRITE 'ERROR2'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  WRITE 'JS5  NO RECS FOUND '
                    pdaAiaa093.getAian093_Linkage_Aian093_Return_Code_Nbr().setValue(615);                                                                                //Natural: MOVE 615 TO AIAN093-RETURN-CODE-NBR
                    //*    PERFORM ERROR-MSG-2
                }                                                                                                                                                         //Natural: END-NOREC
                auvcol_View_Ia_Units.setValue(pdaAiaa093.getAian093_Linkage_Fund_Annual_Units().getValue(pnd_I));                                                         //Natural: MOVE FUND-ANNUAL-UNITS ( #I ) TO IA-UNITS
                //*   WRITE ' #ANNUITY-UNITS ' IA-UNITS
                auvcol_View_Ia_Pmts.setValue(pdaAiaa093.getAian093_Linkage_Fund_Annual_Payments().getValue(pnd_I));                                                       //Natural: MOVE FUND-ANNUAL-PAYMENTS ( #I ) TO IA-PMTS
                //*   WRITE ' #ANNUITY-VALUE ' IA-PMTS
                //*   WRITE '#AUVCOL-DATE-KEY = ' #AUVCOL-KEY
                vw_auvcol_View.updateDBRow("FIND03");                                                                                                                     //Natural: UPDATE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Auvcol_Key_Pnd_Auvcol_Reval.setValue("M");                                                                                                                //Natural: MOVE 'M' TO #AUVCOL-REVAL
            vw_auvcol_View.startDatabaseFind                                                                                                                              //Natural: FIND AUVCOL-VIEW WITH KEY-AREA = #AUVCOL-KEY
            (
            "FIND04",
            new Wc[] { new Wc("KEY_AREA", "=", pnd_Auvcol_Key, WcType.WITH) }
            );
            FIND04:
            while (condition(vw_auvcol_View.readNextRow("FIND04", true)))
            {
                vw_auvcol_View.setIfNotFoundControlFlag(false);
                if (condition(vw_auvcol_View.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "ERROR1");                                                                                                                      //Natural: WRITE 'ERROR1'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  WRITE 'JS5  NO RECS FOUND '
                    pdaAiaa093.getAian093_Linkage_Aian093_Return_Code_Nbr().setValue(616);                                                                                //Natural: MOVE 616 TO AIAN093-RETURN-CODE-NBR
                    //*    PERFORM ERROR-MSG-2
                }                                                                                                                                                         //Natural: END-NOREC
                auvcol_View_Ia_Units.setValue(pdaAiaa093.getAian093_Linkage_Fund_Monthly_Units().getValue(pnd_I));                                                        //Natural: MOVE FUND-MONTHLY-UNITS ( #I ) TO IA-UNITS
                //*   WRITE ' #ANNUITY-UNITS ' IA-UNITS
                auvcol_View_Ia_Pmts.setValue(pdaAiaa093.getAian093_Linkage_Fund_Monthly_Payments().getValue(pnd_I));                                                      //Natural: MOVE FUND-MONTHLY-PAYMENTS ( #I ) TO IA-PMTS
                //*   WRITE ' #ANNUITY-VALUE ' IA-PMTS
                //*   WRITE '#AUVCOL-DATE-KEY = ' #AUVCOL-KEY
                vw_auvcol_View.updateDBRow("FIND04");                                                                                                                     //Natural: UPDATE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-FUTURE-PAYMENTS
            sub_Update_Future_Payments();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Update_Future_Payments() throws Exception                                                                                                            //Natural: UPDATE-FUTURE-PAYMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  WRITE 'SUBROUTINE UPDATE-FUTURE-PAYMENTS'
        pnd_Auvcol_Key_Pnd_Auvcol_Year.setValue(pnd_Iauv_Key_Pnd_Iauv_Year);                                                                                              //Natural: MOVE #IAUV-YEAR TO #AUVCOL-YEAR
        pnd_Auvcol_Key_Pnd_Auvcol_Month.setValue(pnd_Iauv_Key_Pnd_Iauv_Month);                                                                                            //Natural: MOVE #IAUV-MONTH TO #AUVCOL-MONTH
        pnd_Auvcol_Key_Pnd_Auvcol_Day.setValue(pnd_Iauv_Key_Pnd_Iauv_Cutoff_Day);                                                                                         //Natural: MOVE #IAUV-CUTOFF-DAY TO #AUVCOL-DAY
        pnd_Auvcol_Key_Pnd_Auvcol_Reval.setValue("M");                                                                                                                    //Natural: MOVE 'M' TO #AUVCOL-REVAL
        //*  WRITE ' #AUVCOL-DATE-KEY ' #AUVCOL-KEY
        //*  WRITE ' #AUVCOL-CODE ' #AUVCOL-FUND-CODE
        vw_auvcol_View.startDatabaseRead                                                                                                                                  //Natural: READ AUVCOL-VIEW BY KEY-AREA STARTING FROM #AUVCOL-KEY
        (
        "READ01",
        new Wc[] { new Wc("KEY_AREA", ">=", pnd_Auvcol_Key, WcType.BY) },
        new Oc[] { new Oc("KEY_AREA", "ASC") }
        );
        READ01:
        while (condition(vw_auvcol_View.readNextRow("READ01")))
        {
            pnd_Auvcol_Key_Pnd_Auvcol_Fund_Code.setValue(auvcol_View_Pnd_Auv_Fund_Code);                                                                                  //Natural: MOVE #AUV-FUND-CODE TO #AUVCOL-FUND-CODE
            pnd_Auvcol_Key_Pnd_Auvcol_Year.setValue(auvcol_View_Pnd_Auv_Year);                                                                                            //Natural: MOVE #AUV-YEAR TO #AUVCOL-YEAR
            pnd_Auvcol_Key_Pnd_Auvcol_Month.setValue(auvcol_View_Pnd_Auv_Month);                                                                                          //Natural: MOVE #AUV-MONTH TO #AUVCOL-MONTH
            pnd_Auvcol_Key_Pnd_Auvcol_Day.setValue(auvcol_View_Pnd_Auv_Day);                                                                                              //Natural: MOVE #AUV-DAY TO #AUVCOL-DAY
            pnd_Auvcol_Key_Pnd_Auvcol_Reval.setValue(auvcol_View_Pnd_Auv_Code);                                                                                           //Natural: MOVE #AUV-CODE TO #AUVCOL-REVAL
            //*   WRITE 'COUNTER 2 ' *COUNTER
            if (condition(vw_auvcol_View.getAstCOUNTER().equals(2)))                                                                                                      //Natural: IF *COUNTER = 2
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*     WRITE 'COUNTER 1 ' *COUNTER
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  WRITE '#AUVCOL-DATE-KEY = ' #AUVCOL-KEY
        //*  WRITE '#AUVCOL-CODE     = ' #AUVCOL-FUND-CODE
        vw_auvcol_View.startDatabaseFind                                                                                                                                  //Natural: FIND AUVCOL-VIEW WITH KEY-AREA = #AUVCOL-KEY
        (
        "FIND05",
        new Wc[] { new Wc("KEY_AREA", "=", pnd_Auvcol_Key, WcType.WITH) }
        );
        FIND05:
        while (condition(vw_auvcol_View.readNextRow("FIND05", true)))
        {
            vw_auvcol_View.setIfNotFoundControlFlag(false);
            if (condition(vw_auvcol_View.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "ERROR3");                                                                                                                          //Natural: WRITE 'ERROR3'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PERFORM ERROR-MSG-3
            }                                                                                                                                                             //Natural: END-NOREC
            //*   WRITE 'FP (1). KEY ' #AUVCOL-KEY
            //*   MOVE FUND-ANNUAL-UNITS (#I) TO FUTURE-UNITS
            auvcol_View_Future_Units.compute(new ComputeParameters(true, auvcol_View_Future_Units), pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Units().getValue(pnd_I).divide(12)); //Natural: COMPUTE ROUNDED FUTURE-UNITS = FUND-ANNUAL-ANNLZD-UNITS ( #I ) / 12
            //*   MOVE FUND-ANNUAL-PAYMENTS (#I) TO FUTURE-PAYMENTS
            auvcol_View_Future_Payments.compute(new ComputeParameters(true, auvcol_View_Future_Payments), pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Payts().getValue(pnd_I).divide(12)); //Natural: COMPUTE ROUNDED FUTURE-PAYMENTS = FUND-ANNUAL-ANNLZD-PAYTS ( #I ) / 12
            //*  WRITE 'FUND-ANNUAL-ANNLZD-UNITS(#I) ' FUND-ANNUAL-ANNLZD-UNITS(#I)
            //*  WRITE 'FUTURE-UNITS                 ' FUTURE-UNITS
            //*  WRITE 'FUND-ANNUAL-ANNLZD-PAYTS(#I) ' FUND-ANNUAL-ANNLZD-PAYTS(#I)
            //*  WRITE 'FUTURE-PAYMENTS              ' FUTURE-PAYMENTS
            vw_auvcol_View.updateDBRow("FIND05");                                                                                                                         //Natural: UPDATE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
