/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:44:33 AM
**        * FROM NATURAL SUBPROGRAM : Iatn402
************************************************************
**        * FILE NAME            : Iatn402.java
**        * CLASS NAME           : Iatn402
**        * INSTANCE NAME        : Iatn402
************************************************************
***********************************************************************
*
*   PROGRAM    :- IATN402
*   SYSTEM     :- IA
*   AUTHOR     :- LEN BERNSTEIN
*   DATE       :- 11/26/1997
*
*   DESCRIPTION
*   -----------
*   THIS SUBPROGRAM UPDATES MIT AND POST.
*   THE FOLLOWING STATUS MAY BE CHANGED:-
*   M0 TO M2, P0 TO P2, M2 TO M1
*
*
*   ERROR NBRS:-
*   -----------
*   '201' RETURNED FROM IATN420 WITH AN ERROR AND RETRY < NUM-RETRY
*   '202' VALIDATES THE STATUS MUS BE M0 OR P0 OR M1
*   WARNINGS:-
*   ----------
*   '250' RETURNED FROM POST WITH AN ERROR AND RETRY = NUM RETRY
*         SO THE RQST WAS CLOSED AND THE RQST STATUS WAS CHANGED.
*
*
*   HISTORY
*   -------
*   10/26/1999  ARI G.
*      ADDED M1 STATUS CODE FOR  T TO C  NEW ISSUES.
*   01/30/2009  ADDED WPID TAITW TO THE #M1-CHECK ROUTINE SINCE
*               WE ARE NOW ISSUING CONTRACTS THRU CIS. M1 STATUS
*               IS CHECKED IN IATP407 AND IATP408. SC 013009.
*   08/29/2010  PASSING #IATN402-IN.#TODAYS-DTE TO IATN410,11,13.
*               GGG-08/10
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO        RE-STOWED ONLY FOR PIN EXPANSION.
* 07/2017  O SOTTO        ADDITIONAL CHNAGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn402 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl402p pdaIatl402p;
    private PdaIatl400p pdaIatl400p;
    private PdaIatl410p pdaIatl410p;
    private PdaIatl412p pdaIatl412p;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Srce;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Ssn;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_New_Issue;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Stat_Complete_M0;
    private DbsField pnd_Const_Pnd_Stat_Complete_M1;
    private DbsField pnd_Const_Pnd_Stat_Complete_M2;
    private DbsField pnd_Const_Pnd_Stat_Rjctd_P0;
    private DbsField pnd_Const_Pnd_Stat_Rjctd_P2;
    private DbsField pnd_Const_Pnd_Active;
    private DbsField pnd_Const_Pnd_Closed;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Trnsfr;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Switch;
    private DbsField pnd_Const_Pnd_Num_Retry;
    private DbsField pnd_M1_Wpid;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIatl400p = new PdaIatl400p(localVariables);
        pdaIatl410p = new PdaIatl410p(localVariables);
        pdaIatl412p = new PdaIatl412p(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl402p = new PdaIatl402p(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_iaa_Trnsfr_Sw_Rqst = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst", "IAA-TRNSFR-SW-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO");
        iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_ENTRY_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_RCVD_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_CNTCT_MDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Srce = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_SRCE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "RQST_REP_NME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_STTMNT_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RQST_RCPRCL_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Ssn = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RQST_SSN");
        iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_Ia_New_Issue = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISSUE");
        iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_RJCTN_CDE");
        iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "XFR_RETRY_CNT");
        iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Stat_Complete_M0 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Complete_M0", "#STAT-COMPLETE-M0", FieldType.STRING, 2);
        pnd_Const_Pnd_Stat_Complete_M1 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Complete_M1", "#STAT-COMPLETE-M1", FieldType.STRING, 2);
        pnd_Const_Pnd_Stat_Complete_M2 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Complete_M2", "#STAT-COMPLETE-M2", FieldType.STRING, 2);
        pnd_Const_Pnd_Stat_Rjctd_P0 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Rjctd_P0", "#STAT-RJCTD-P0", FieldType.STRING, 2);
        pnd_Const_Pnd_Stat_Rjctd_P2 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Rjctd_P2", "#STAT-RJCTD-P2", FieldType.STRING, 2);
        pnd_Const_Pnd_Active = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Active", "#ACTIVE", FieldType.STRING, 1);
        pnd_Const_Pnd_Closed = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Closed", "#CLOSED", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Trnsfr = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Trnsfr", "#RCRD-TYPE-TRNSFR", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Switch = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Switch", "#RCRD-TYPE-SWITCH", FieldType.STRING, 1);
        pnd_Const_Pnd_Num_Retry = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Num_Retry", "#NUM-RETRY", FieldType.NUMERIC, 1);
        pnd_M1_Wpid = localVariables.newFieldArrayInRecord("pnd_M1_Wpid", "#M1-WPID", FieldType.STRING, 6, new DbsArrayController(1, 3));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst.reset();

        localVariables.reset();
        pnd_Const_Pnd_Stat_Complete_M0.setInitialValue("M0");
        pnd_Const_Pnd_Stat_Complete_M1.setInitialValue("M1");
        pnd_Const_Pnd_Stat_Complete_M2.setInitialValue("M2");
        pnd_Const_Pnd_Stat_Rjctd_P0.setInitialValue("P0");
        pnd_Const_Pnd_Stat_Rjctd_P2.setInitialValue("P2");
        pnd_Const_Pnd_Active.setInitialValue("A");
        pnd_Const_Pnd_Closed.setInitialValue("C");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr.setInitialValue("1");
        pnd_Const_Pnd_Rcrd_Type_Switch.setInitialValue("2");
        pnd_Const_Pnd_Num_Retry.setInitialValue(2);
        pnd_M1_Wpid.getValue(1).setInitialValue("TAITY ");
        pnd_M1_Wpid.getValue(2).setInitialValue("TAITG ");
        pnd_M1_Wpid.getValue(3).setInitialValue("TAITW ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn402() throws Exception
    {
        super("Iatn402");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *======================================================================
        //*                       START OF PROGRAM
        //* *======================================================================
        pdaIatl402p.getPnd_Iatn402_Out().reset();                                                                                                                         //Natural: RESET #IATN402-OUT
        RPT1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM VALIDATE-INPUT
            sub_Validate_Input();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CALLING PROG CONTROLS IF WE
            if (condition(pdaIatl402p.getPnd_Iatn402_In_Pnd_Check_Control_Rec().getBoolean()))                                                                            //Natural: IF #CHECK-CONTROL-REC
            {
                //*  CHECK STATUS OF CONTROL RECORD
                                                                                                                                                                          //Natural: PERFORM CHECK-CONTROL-RECORD
                sub_Check_Control_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  VALIDATE REC AND ADD 1 TO CNT
                                                                                                                                                                          //Natural: PERFORM UPDATE-RQST-RETRY-CNT
            sub_Update_Rqst_Retry_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHECK  RETRY COUNT
            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.greater(pnd_Const_Pnd_Num_Retry)))                                                                             //Natural: IF IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT GT #NUM-RETRY
            {
                pdaIatl402p.getPnd_Iatn402_Out_Pnd_Return_Code().setValue("E");                                                                                           //Natural: MOVE 'E' TO #IATN402-OUT.#RETURN-CODE
                pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(DbsUtil.compress(" Retry = Retry Count. RQST Closed."));                                                //Natural: COMPRESS ' Retry = Retry Count. RQST Closed.' INTO #IATN402-OUT.#MSG
                pdaIatl402p.getPnd_Iatn402_Out_Pnd_Error_Nbr().setValue("250");                                                                                           //Natural: MOVE '250' TO #IATN402-OUT.#ERROR-NBR
                //*  INCLUDES ET
                                                                                                                                                                          //Natural: PERFORM CLOSE-RQST
                sub_Close_Rqst();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  INCLUDES BT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
                sub_Pnd_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*   CREATE THE TRANSFER OR SWITCH ACKNOWLEDGMENT LETTERS
                                                                                                                                                                          //Natural: PERFORM UPDATE-POST-MIT
            sub_Update_Post_Mit();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  AN ERROR OCCURED
            if (condition(pdaIatl410p.getPnd_Iatn410_Out_Pnd_Return_Code().equals(" ")))                                                                                  //Natural: IF #IATN410-OUT.#RETURN-CODE = ' '
            {
                if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals(pnd_Const_Pnd_Stat_Complete_M0)))                                                                    //Natural: IF IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = #STAT-COMPLETE-M0
                {
                    DbsUtil.callnat(Iatn045.class , getCurrentProcessState(), iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id, iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme,                    //Natural: CALLNAT 'IATN045' USING IAA-TRNSFR-SW-RQST.IA-UNIQUE-ID IAA-TRNSFR-SW-RQST.XFR-MIT-LOG-DTE-TME IAA-TRNSFR-SW-RQST.RQST-UNIT-CDE IAA-TRNSFR-SW-RQST.RQST-ENTRY-DTE IAA-TRNSFR-SW-RQST.RQST-ENTRY-USER-ID IAA-TRNSFR-SW-RQST.RQST-RCVD-DTE IAA-TRNSFR-SW-RQST.RQST-RCVD-USER-ID IAA-TRNSFR-SW-RQST.XFR-WORK-PRCSS-ID #IATN402-OUT.#MSG-2
                        iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde, iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte, iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id, iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte, 
                        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id, iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id, pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg_2());
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  INCLUDES ET
                                                                                                                                                                          //Natural: PERFORM CLOSE-RQST
                sub_Close_Rqst();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.less(pnd_Const_Pnd_Num_Retry)))                                                                            //Natural: IF IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT LT #NUM-RETRY
                {
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(pdaIatl410p.getPnd_Iatn410_Out_Pnd_Msg());                                                          //Natural: ASSIGN #IATN402-OUT.#MSG := #IATN410-OUT.#MSG
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg_2().setValue(DbsUtil.compress("Post had an error. Retry < Retry Count. Nothing", "updated."));                 //Natural: COMPRESS 'Post had an error. Retry < Retry Count. Nothing' 'updated.' INTO #IATN402-OUT.#MSG-2
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Error_Nbr().setValue("201");                                                                                       //Natural: MOVE '201' TO #IATN402-OUT.#ERROR-NBR
                    //*  INCLUDES BT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
                    sub_Pnd_Process_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RPT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  BT
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Return_Code().setValue("E");                                                                                       //Natural: MOVE 'E' TO #IATN402-OUT.#RETURN-CODE
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(pdaIatl410p.getPnd_Iatn410_Out_Pnd_Msg());                                                          //Natural: ASSIGN #IATN402-OUT.#MSG := #IATN410-OUT.#MSG
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg_2().setValue(DbsUtil.compress("Post had an error. Retry = Retry Count. RQST Closed.", Global.getPROGRAM()));   //Natural: COMPRESS 'Post had an error. Retry = Retry Count. RQST Closed.' *PROGRAM INTO #IATN402-OUT.#MSG-2
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Error_Nbr().setValue("250");                                                                                       //Natural: MOVE '250' TO #IATN402-OUT.#ERROR-NBR
                    //*  INCLUDES ET
                                                                                                                                                                          //Natural: PERFORM CLOSE-RQST
                    sub_Close_Rqst();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RPT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #ESCAPE-SUBPROG
            sub_Pnd_Escape_Subprog();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *******************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ESCAPE-SUBPROG
            //* ******************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-ERROR
            //*  RPT1.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* *======================================================================
        //*                       START OF SUBROUTINES
        //* *======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-INPUT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CONTROL-RECORD
        //* ***********************************************************************
        //*  CHECK THAT THE FILE IS ACTIVE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RQST-RETRY-CNT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CLOSE-RQST
        //*  SET RETURN STATUS CODE TO BE THE NEW STATUS CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #M1-CHECK
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-POST-MIT
    }
    private void sub_Pnd_Escape_Subprog() throws Exception                                                                                                                //Natural: #ESCAPE-SUBPROG
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( RPT1. )
        Global.setEscapeCode(EscapeType.Bottom, "RPT1");
        if (true) return;
    }
    private void sub_Pnd_Process_Error() throws Exception                                                                                                                 //Natural: #PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pdaIatl402p.getPnd_Iatn402_Out_Pnd_Return_Code().setValue("E");                                                                                                   //Natural: MOVE 'E' TO #IATN402-OUT.#RETURN-CODE
        pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(DbsUtil.compress(pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg(), Global.getPROGRAM()));                               //Natural: COMPRESS #IATN402-OUT.#MSG *PROGRAM INTO #IATN402-OUT.#MSG
        //*  BT
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( RPT1. )
        Global.setEscapeCode(EscapeType.Bottom, "RPT1");
        if (true) return;
        //*  #PROCESS-ERROR
    }
    private void sub_Validate_Input() throws Exception                                                                                                                    //Natural: VALIDATE-INPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  VALIDATE RQST ID AND ISN
        if (condition(pdaIatl402p.getPnd_Iatn402_In_Pnd_Rqst_Id().equals(" ") && pdaIatl402p.getPnd_Iatn402_In_Pnd_Isn().equals(getZero())))                              //Natural: IF #IATN402-IN.#RQST-ID = ' ' AND #IATN402-IN.#ISN = 0
        {
            pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue("Either RQST id or the ISN number is required.");                                                           //Natural: MOVE 'Either RQST id or the ISN number is required.' TO #IATN402-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  FETCH THE ISN NUMBER
        if (condition(pdaIatl402p.getPnd_Iatn402_In_Pnd_Rqst_Id().notEquals(" ") && pdaIatl402p.getPnd_Iatn402_In_Pnd_Isn().equals(getZero())))                           //Natural: IF #IATN402-IN.#RQST-ID NE ' ' AND #IATN402-IN.#ISN = 0
        {
            vw_iaa_Trnsfr_Sw_Rqst.startDatabaseFind                                                                                                                       //Natural: FIND ( 1 ) IAA-TRNSFR-SW-RQST WITH RQST-ID = #IATN402-IN.#RQST-ID
            (
            "FND1",
            new Wc[] { new Wc("RQST_ID", "=", pdaIatl402p.getPnd_Iatn402_In_Pnd_Rqst_Id(), WcType.WITH) },
            1
            );
            FND1:
            while (condition(vw_iaa_Trnsfr_Sw_Rqst.readNextRow("FND1", true)))
            {
                vw_iaa_Trnsfr_Sw_Rqst.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Trnsfr_Sw_Rqst.getAstCOUNTER().equals(0)))                                                                                           //Natural: IF NO RECORDS FOUND
                {
                    pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Request id '", pdaIatl402p.getPnd_Iatn402_In_Pnd_Rqst_Id(),  //Natural: COMPRESS 'Request id "' #IATN402-IN.#RQST-ID '" is not on file.' INTO #IATN402-OUT.#MSG LEAVING NO SPACE
                        "' is not on file."));
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
                    sub_Pnd_Process_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FND1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-NOREC
                pdaIatl402p.getPnd_Iatn402_In_Pnd_Isn().setValue(vw_iaa_Trnsfr_Sw_Rqst.getAstISN("FND1"));                                                                //Natural: MOVE *ISN TO #IATN402-IN.#ISN
                //*  FND1.
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-INPUT
    }
    private void sub_Check_Control_Record() throws Exception                                                                                                              //Natural: CHECK-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("AA");                                                                                                         //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'AA'
        //*  FETCH THE LATEST CONTROL RECORD
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Actvty_Cde().equals(pnd_Const_Pnd_Active) || pdaIatl400p.getPnd_Iatn400_Out_Pnd_Return_Code().equals(" ")))) //Natural: IF NOT ( #IATN400-OUT.CNTRL-ACTVTY-CDE EQ #CONST.#ACTIVE OR #IATN400-OUT.#RETURN-CODE EQ ' ' )
        {
            pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(pdaIatl400p.getPnd_Iatn400_Out_Pnd_Msg());                                                                  //Natural: MOVE #IATN400-OUT.#MSG TO #IATN402-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   CHECK-CONTROL-RECORD
    }
    private void sub_Update_Rqst_Retry_Cnt() throws Exception                                                                                                             //Natural: UPDATE-RQST-RETRY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        GET_REC1:                                                                                                                                                         //Natural: GET IAA-TRNSFR-SW-RQST #ISN
        vw_iaa_Trnsfr_Sw_Rqst.readByID(pdaIatl402p.getPnd_Iatn402_In_Pnd_Isn().getLong(), "GET_REC1");
        //*  VALIDATE THE STATUS OF THE REQUEST
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.notEquals(pnd_Const_Pnd_Stat_Complete_M0) && iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.notEquals(pnd_Const_Pnd_Stat_Rjctd_P0))) //Natural: IF IAA-TRNSFR-SW-RQST.XFR-STTS-CDE NE #STAT-COMPLETE-M0 AND IAA-TRNSFR-SW-RQST.XFR-STTS-CDE NE #STAT-RJCTD-P0
        {
            pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RQST has a status", "H'00'", "of '", iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde,  //Natural: COMPRESS 'RQST has a status' H'00' 'of "'IAA-TRNSFR-SW-RQST.XFR-STTS-CDE '" Valid values are M0,P0' INTO #IATN402-OUT.#MSG LEAVING NO SPACE
                "' Valid values are M0,P0"));
            pdaIatl402p.getPnd_Iatn402_Out_Pnd_Error_Nbr().setValue("202");                                                                                               //Natural: MOVE '202' TO #IATN402-OUT.#ERROR-NBR
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK THAT THIS TRANSFER REQUEST IS OPEN
        if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind.equals(pnd_Const_Pnd_Closed)))                                                                                 //Natural: IF IAA-TRNSFR-SW-RQST.RQST-OPN-CLSD-IND EQ #CLOSED
        {
            pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "No action is permitted on a closed request"));             //Natural: COMPRESS 'No action is permitted on a closed request' INTO #IATN402-OUT.#MSG LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
            //*  UPDATE
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.nadd(1);                                                                                                                         //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT := IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT + 1
        vw_iaa_Trnsfr_Sw_Rqst.updateDBRow("GET_REC1");                                                                                                                    //Natural: UPDATE ( GET-REC1. )
        //*  ET
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  UPDATE-RQST-RETRY-CNT
    }
    private void sub_Close_Rqst() throws Exception                                                                                                                        //Natural: CLOSE-RQST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        GET_REC2:                                                                                                                                                         //Natural: GET IAA-TRNSFR-SW-RQST #ISN
        vw_iaa_Trnsfr_Sw_Rqst.readByID(pdaIatl402p.getPnd_Iatn402_In_Pnd_Isn().getLong(), "GET_REC2");
        short decideConditionsMet425 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = #STAT-COMPLETE-M0
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals(pnd_Const_Pnd_Stat_Complete_M0)))
        {
            decideConditionsMet425++;
            iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.setValue(pnd_Const_Pnd_Stat_Complete_M2);                                                                                     //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE := #STAT-COMPLETE-M2
        }                                                                                                                                                                 //Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = #STAT-RJCTD-P0
        else if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals(pnd_Const_Pnd_Stat_Rjctd_P0)))
        {
            decideConditionsMet425++;
            iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.setValue(pnd_Const_Pnd_Stat_Rjctd_P2);                                                                                        //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE := #STAT-RJCTD-P2
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals("M2")))                                                                                                      //Natural: IF IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = 'M2'
        {
                                                                                                                                                                          //Natural: PERFORM #M1-CHECK
            sub_Pnd_M1_Check();
            if (condition(Global.isEscape())) {return;}
            //* UPDATE
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind.setValue(pnd_Const_Pnd_Closed);                                                                                              //Natural: ASSIGN IAA-TRNSFR-SW-RQST.RQST-OPN-CLSD-IND := #CLOSED
        iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind.setValue(false);                                                                                                           //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-IN-PROGRESS-IND := FALSE
        vw_iaa_Trnsfr_Sw_Rqst.updateDBRow("GET_REC2");                                                                                                                    //Natural: UPDATE ( GET-REC2. )
        //*  ET
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pdaIatl402p.getPnd_Iatn402_Out_Pnd_Xfr_Stts_Cde().setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde);                                                                      //Natural: ASSIGN #IATN402-OUT.#XFR-STTS-CDE := IAA-TRNSFR-SW-RQST.XFR-STTS-CDE
        //*  CLOSE-RQST
    }
    private void sub_Pnd_M1_Check() throws Exception                                                                                                                      //Natural: #M1-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* * IF IAA-TRNSFR-SW-RQST.XFR-WORK-PRCSS-ID = 'TAITY ' OR = 'TAITG ' AND
        //*  013009
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id.equals(pnd_M1_Wpid.getValue("*")) && iaa_Trnsfr_Sw_Rqst_Ia_New_Issue.equals("Y")))                             //Natural: IF IAA-TRNSFR-SW-RQST.XFR-WORK-PRCSS-ID = #M1-WPID ( * ) AND IAA-TRNSFR-SW-RQST.IA-NEW-ISSUE = 'Y'
        {
            iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.setValue(pnd_Const_Pnd_Stat_Complete_M1);                                                                                     //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE := #STAT-COMPLETE-M1
        }                                                                                                                                                                 //Natural: END-IF
        //*  #M1-CHECK
    }
    private void sub_Update_Post_Mit() throws Exception                                                                                                                   //Natural: UPDATE-POST-MIT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE A TRANSFER CONFIRMATION LETTER
        //*  GGG-08/10
        pdaIatl410p.getPnd_Iatn410_In_Pnd_Todays_Dte().setValue(pdaIatl402p.getPnd_Iatn402_In_Pnd_Todays_Dte());                                                          //Natural: MOVE #IATN402-IN.#TODAYS-DTE TO #IATN410-IN.#TODAYS-DTE
        short decideConditionsMet464 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = #STAT-COMPLETE-M0
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals(pnd_Const_Pnd_Stat_Complete_M0)))
        {
            decideConditionsMet464++;
            pdaIatl410p.getPnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO #IAA-TRNSFR-SW-RQST
            short decideConditionsMet469 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE;//Natural: VALUE #RCRD-TYPE-TRNSFR
            if (condition((iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Trnsfr))))
            {
                decideConditionsMet469++;
                if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals(" ")))                                                                                              //Natural: IF IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = ' '
                {
                    //*  TRANSFER CONFIRMATION
                    DbsUtil.callnat(Iatn410.class , getCurrentProcessState(), pdaIatl410p.getPnd_Iatn410_In(), pdaIatl410p.getPnd_Iatn410_Out());                         //Natural: CALLNAT 'IATN410' #IATN410-IN #IATN410-OUT
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  T TO C CONFIRMATION
                    DbsUtil.callnat(Iatn413.class , getCurrentProcessState(), pdaIatl410p.getPnd_Iatn410_In(), pdaIatl410p.getPnd_Iatn410_Out());                         //Natural: CALLNAT 'IATN413' #IATN410-IN #IATN410-OUT
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-SWITCH
            else if (condition((iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch))))
            {
                decideConditionsMet469++;
                //*  SWITCH CONFIRMATION
                DbsUtil.callnat(Iatn411.class , getCurrentProcessState(), pdaIatl410p.getPnd_Iatn410_In(), pdaIatl410p.getPnd_Iatn410_Out());                             //Natural: CALLNAT 'IATN411' #IATN410-IN #IATN410-OUT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = #STAT-RJCTD-P0
        else if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals(pnd_Const_Pnd_Stat_Rjctd_P0)))
        {
            decideConditionsMet464++;
            short decideConditionsMet488 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE;//Natural: VALUE #RCRD-TYPE-TRNSFR
            if (condition((iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Trnsfr))))
            {
                decideConditionsMet488++;
                pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg_2().setValue(DbsUtil.compress("Transfer RQST is in Status P0. There is no Reject ", "letter for Transfers."));     //Natural: COMPRESS 'Transfer RQST is in Status P0. There is no Reject ' 'letter for Transfers.' INTO #IATN402-OUT.#MSG-2
                //*  INCLUDES BT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
                sub_Pnd_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-SWITCH
            else if (condition((iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch))))
            {
                decideConditionsMet488++;
                pdaIatl412p.getPnd_Iatn412_In().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                   //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO #IATN412-IN
                //*  SWITCH CONFIRMATION
                DbsUtil.callnat(Iatn412.class , getCurrentProcessState(), pdaIatl412p.getPnd_Iatn412_In(), pdaIatl412p.getPnd_Iatn412_Out());                             //Natural: CALLNAT 'IATN412' #IATN412-IN #IATN412-OUT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg_2().setValue(DbsUtil.compress("RQST is not in a valid status "));                                                      //Natural: COMPRESS 'RQST is not in a valid status ' INTO #IATN402-OUT.#MSG-2
            //*  INCLUDES BT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
