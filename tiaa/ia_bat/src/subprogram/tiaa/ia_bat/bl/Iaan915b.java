/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:16 AM
**        * FROM NATURAL SUBPROGRAM : Iaan915b
************************************************************
**        * FILE NAME            : Iaan915b.java
**        * CLASS NAME           : Iaan915b
**        * INSTANCE NAME        : Iaan915b
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN915B CREATES MANUAL NEW ISSUE SELECTION RCRDS *
*      DATE     -  10/94                                             *
*                                                                    *
* HISTORY                                                            *
*
* 3/12  JFT : RATE BASE EXPANSION - SCAN ON 3/12 FOR CHANGES.
* 04/2017 OS RE-STOWED FOR IAAL915B PIN EXPANSION.
**********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan915b extends BLNatBase
{
    // Data Areas
    private LdaIaal915b ldaIaal915b;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Passed_Data;
    private DbsField pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_1;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte_P;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Passed_Data__R_Field_2;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8;
    private DbsField pnd_Passed_Data_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_3;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_4;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Area;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Id;
    private DbsField pnd_Passed_Data_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Seq_Nbr;
    private DbsField pnd_Passed_Data_Pnd_Trans_Mode;
    private DbsField pnd_Fund_Aftr_Key;

    private DbsGroup pnd_Fund_Aftr_Key__R_Field_5;
    private DbsField pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde;
    private DbsField pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Fund_Bfre_Key_2;

    private DbsGroup pnd_Fund_Bfre_Key_2__R_Field_6;
    private DbsField pnd_Fund_Bfre_Key_2_Pnd_B_Imge;
    private DbsField pnd_Fund_Bfre_Key_2_Pnd_B_Cntrct;
    private DbsField pnd_Fund_Bfre_Key_2_Pnd_B_Payee;
    private DbsField pnd_Fund_Bfre_Key_2_Pnd_B_Trans_Dte;
    private DbsField pnd_Fund_Bfre_Key_2_Pnd_B_Cmpny;
    private DbsField pnd_Fund_Aftr_Key_2;

    private DbsGroup pnd_Fund_Aftr_Key_2__R_Field_7;
    private DbsField pnd_Fund_Aftr_Key_2_Pnd_A_Imge;
    private DbsField pnd_Fund_Aftr_Key_2_Pnd_A_Cntrct;
    private DbsField pnd_Fund_Aftr_Key_2_Pnd_A_Payee;
    private DbsField pnd_Fund_Aftr_Key_2_Pnd_A_Invrse;
    private DbsField pnd_Fund_Aftr_Key_2_Pnd_A_Cmpny;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal915b = new LdaIaal915b();
        registerRecord(ldaIaal915b);
        registerRecord(ldaIaal915b.getVw_iaa_Cntrct());
        registerRecord(ldaIaal915b.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal915b.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal915b.getVw_iaa_Cpr_Trans());
        registerRecord(ldaIaal915b.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal915b.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal915b.getVw_iaa_Cref_Fund_Trans());

        // parameters
        parameters = new DbsRecord();

        pnd_Passed_Data = parameters.newGroupInRecord("pnd_Passed_Data", "#PASSED-DATA");
        pnd_Passed_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte", "#CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Passed_Data__R_Field_1 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_1", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Dte);
        pnd_Passed_Data_Pnd_Trans_Dte_P = pnd_Passed_Data__R_Field_1.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte_P", "#TRANS-DTE-P", FieldType.PACKED_DECIMAL, 
            12);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Passed_Data__R_Field_2 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_2", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8 = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8", "#TRANS-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Passed_Data_Pnd_Trans_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Passed_Data_Pnd_Trans_Check_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_3 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_3", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Check_Dte);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc", "#TRANS-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy", "#TRANS-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm", "#TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd", "#TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_4 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_4", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Effctve_Dte);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc", "#TRANS-EFFCTVE-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy", "#TRANS-EFFCTVE-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm", "#TRANS-EFFCTVE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd = pnd_Passed_Data__R_Field_4.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd", "#TRANS-EFFCTVE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_User_Area = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Area", "#TRANS-USER-AREA", FieldType.STRING, 
            6);
        pnd_Passed_Data_Pnd_Trans_User_Id = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Id", "#TRANS-USER-ID", FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Invrse_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12);
        pnd_Passed_Data_Pnd_Trans_Seq_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Seq_Nbr", "#TRANS-SEQ-NBR", FieldType.NUMERIC, 
            4);
        pnd_Passed_Data_Pnd_Trans_Mode = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Mode", "#TRANS-MODE", FieldType.NUMERIC, 3);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Fund_Aftr_Key = localVariables.newFieldInRecord("pnd_Fund_Aftr_Key", "#FUND-AFTR-KEY", FieldType.STRING, 27);

        pnd_Fund_Aftr_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Fund_Aftr_Key__R_Field_5", "REDEFINE", pnd_Fund_Aftr_Key);
        pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Fund_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Fund_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde = pnd_Fund_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde = pnd_Fund_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde", "#CMPNY-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Fund_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Fund_Bfre_Key_2 = localVariables.newFieldInRecord("pnd_Fund_Bfre_Key_2", "#FUND-BFRE-KEY-2", FieldType.STRING, 23);

        pnd_Fund_Bfre_Key_2__R_Field_6 = localVariables.newGroupInRecord("pnd_Fund_Bfre_Key_2__R_Field_6", "REDEFINE", pnd_Fund_Bfre_Key_2);
        pnd_Fund_Bfre_Key_2_Pnd_B_Imge = pnd_Fund_Bfre_Key_2__R_Field_6.newFieldInGroup("pnd_Fund_Bfre_Key_2_Pnd_B_Imge", "#B-IMGE", FieldType.STRING, 
            1);
        pnd_Fund_Bfre_Key_2_Pnd_B_Cntrct = pnd_Fund_Bfre_Key_2__R_Field_6.newFieldInGroup("pnd_Fund_Bfre_Key_2_Pnd_B_Cntrct", "#B-CNTRCT", FieldType.STRING, 
            10);
        pnd_Fund_Bfre_Key_2_Pnd_B_Payee = pnd_Fund_Bfre_Key_2__R_Field_6.newFieldInGroup("pnd_Fund_Bfre_Key_2_Pnd_B_Payee", "#B-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Fund_Bfre_Key_2_Pnd_B_Trans_Dte = pnd_Fund_Bfre_Key_2__R_Field_6.newFieldInGroup("pnd_Fund_Bfre_Key_2_Pnd_B_Trans_Dte", "#B-TRANS-DTE", FieldType.TIME);
        pnd_Fund_Bfre_Key_2_Pnd_B_Cmpny = pnd_Fund_Bfre_Key_2__R_Field_6.newFieldInGroup("pnd_Fund_Bfre_Key_2_Pnd_B_Cmpny", "#B-CMPNY", FieldType.STRING, 
            3);
        pnd_Fund_Aftr_Key_2 = localVariables.newFieldInRecord("pnd_Fund_Aftr_Key_2", "#FUND-AFTR-KEY-2", FieldType.STRING, 28);

        pnd_Fund_Aftr_Key_2__R_Field_7 = localVariables.newGroupInRecord("pnd_Fund_Aftr_Key_2__R_Field_7", "REDEFINE", pnd_Fund_Aftr_Key_2);
        pnd_Fund_Aftr_Key_2_Pnd_A_Imge = pnd_Fund_Aftr_Key_2__R_Field_7.newFieldInGroup("pnd_Fund_Aftr_Key_2_Pnd_A_Imge", "#A-IMGE", FieldType.STRING, 
            1);
        pnd_Fund_Aftr_Key_2_Pnd_A_Cntrct = pnd_Fund_Aftr_Key_2__R_Field_7.newFieldInGroup("pnd_Fund_Aftr_Key_2_Pnd_A_Cntrct", "#A-CNTRCT", FieldType.STRING, 
            10);
        pnd_Fund_Aftr_Key_2_Pnd_A_Payee = pnd_Fund_Aftr_Key_2__R_Field_7.newFieldInGroup("pnd_Fund_Aftr_Key_2_Pnd_A_Payee", "#A-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Fund_Aftr_Key_2_Pnd_A_Invrse = pnd_Fund_Aftr_Key_2__R_Field_7.newFieldInGroup("pnd_Fund_Aftr_Key_2_Pnd_A_Invrse", "#A-INVRSE", FieldType.NUMERIC, 
            12);
        pnd_Fund_Aftr_Key_2_Pnd_A_Cmpny = pnd_Fund_Aftr_Key_2__R_Field_7.newFieldInGroup("pnd_Fund_Aftr_Key_2_Pnd_A_Cmpny", "#A-CMPNY", FieldType.STRING, 
            3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal915b.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Fund_Bfre_Key_2.setInitialValue("1");
        pnd_Fund_Aftr_Key_2.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan915b() throws Exception
    {
        super("Iaan915b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: DECIDE ON FIRST VALUE OF #TRANS-CDE
        short decideConditionsMet823 = 0;                                                                                                                                 //Natural: VALUE 033
        if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(33))))
        {
            decideConditionsMet823++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-33
            sub_Process_33();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 035
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(35))))
        {
            decideConditionsMet823++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-35
            sub_Process_35();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 050
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(50))))
        {
            decideConditionsMet823++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-50
            sub_Process_50();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT-AFTER
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-33
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-35
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-50
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-HEADER
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CNTRCT-BFRE-KEY
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR-BFRE-KEY
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR-AFTR-KEY
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-TIAA-FUND-KEY
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-TIAA-RATE-CHANGES
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CREF-RATE-CHANGES
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-FINAL-PYMT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PROD-CODE
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Selection_Rcrd().reset();                                                                                                                      //Natural: RESET #SELECTION-RCRD #NO-CNTRCT-REC #ISSUE-101-WRITTEN
        ldaIaal915b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().reset();
        ldaIaal915b.getPnd_Logical_Variables_Pnd_Issue_101_Written().reset();
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT-AFTER
        sub_Find_Iaa_Cntrct_After();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR
        sub_Get_Cross_Reference_Nbr();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Find_Iaa_Cntrct_After() throws Exception                                                                                                             //Natural: FIND-IAA-CNTRCT-AFTER
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                              //Natural: ASSIGN #CNTRCT-AFTR-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal915b.getPnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915b.getPnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                         //Natural: ASSIGN #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
        ldaIaal915b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #CNTRCT-AFTR-KEY
        (
        "READ01",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", ldaIaal915b.getPnd_Cntrct_Aftr_Key(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIaal915b.getVw_iaa_Cntrct_Trans().readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915b.getPnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr().notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                            //Natural: IF #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR NE #TRANS-PPCN-NBR
        {
            getReports().write(0, "NO IAA CONTRACT TRANS RECORD FOR : ",ldaIaal915b.getPnd_Cntrct_Aftr_Key());                                                            //Natural: WRITE 'NO IAA CONTRACT TRANS RECORD FOR : ' #CNTRCT-AFTR-KEY
            if (Global.isEscape()) return;
            ldaIaal915b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                      //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getVw_iaa_Cntrct().setValuesByName(ldaIaal915b.getVw_iaa_Cntrct_Trans());                                                                             //Natural: MOVE BY NAME IAA-CNTRCT-TRANS TO IAA-CNTRCT
    }
    private void sub_Get_Cross_Reference_Nbr() throws Exception                                                                                                           //Natural: GET-CROSS-REFERENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet904 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(1))))
        {
            decideConditionsMet904++;
            if (condition(ldaIaal915b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(2))))
        {
            decideConditionsMet904++;
            if (condition(ldaIaal915b.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                       //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                    //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                   //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                    //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((pnd_Passed_Data_Pnd_Trans_Payee_Cde.greaterOrEqual(3) && pnd_Passed_Data_Pnd_Trans_Payee_Cde.lessOrEqual(99)))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM GET-CPR-AFTR-KEY
            sub_Get_Cpr_Aftr_Key();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915b.getIaa_Cpr_Trans_Bnfcry_Xref_Ind());                                               //Natural: ASSIGN #CROSS-REF-NBR := IAA-CPR-TRANS.BNFCRY-XREF-IND
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_33() throws Exception                                                                                                                        //Natural: PROCESS-33
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM GET-CPR-AFTR-KEY
        sub_Get_Cpr_Aftr_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915b.getPnd_Cntrct_Payee_Key().setValue(ldaIaal915b.getPnd_Cpr_Aftr_Key().getSubstring(2,12));                                                             //Natural: MOVE SUBSTR ( #CPR-AFTR-KEY,2,12 ) TO #CNTRCT-PAYEE-KEY
        ldaIaal915b.getVw_iaa_Cntrct_Prtcpnt_Role().setValuesByName(ldaIaal915b.getVw_iaa_Cpr_Trans());                                                                   //Natural: MOVE BY NAME IAA-CPR-TRANS TO IAA-CNTRCT-PRTCPNT-ROLE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE1
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PROD-CODE
        sub_Determine_Prod_Code();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Currency().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Crrncy_Cde());                                                         //Natural: ASSIGN #CURRENCY := IAA-CNTRCT.CNTRCT-CRRNCY-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mode().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                                  //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Pend_Code().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde());                                             //Natural: ASSIGN #PEND-CODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Hold_Check_Cde().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde());                                        //Natural: ASSIGN #HOLD-CHECK-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte());                                            //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Pend_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                              //Natural: ASSIGN #PEND-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Pend_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                              //Natural: ASSIGN #PEND-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Option_N().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde());                                                           //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Origin().setValueEdited(ldaIaal915b.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                              //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORIGIN
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Issue_Dte());                                                        //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                               //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                               //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                              //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                       //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                       //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                               //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                       //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                       //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                                   //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                       //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                       //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                                    //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                                //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                                //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                                //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                                   //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                                          //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                                          //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue1().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE1 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("10");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("1");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
            ldaIaal915b.getPnd_Logical_Variables_Pnd_Issue_101_Written().setValue(true);                                                                                  //Natural: ASSIGN #ISSUE-101-WRITTEN := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue2().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE2
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Citizen().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde());                                           //Natural: ASSIGN #CITIZEN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde());                                      //Natural: ASSIGN #ST/CNTRY-ISS := IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Res().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde());                                 //Natural: ASSIGN #ST/CNTRY-RES := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Coll_Iss().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Inst_Iss_Cde());                                                       //Natural: ASSIGN #COLL-ISS := IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        if (condition(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).notEquals(" ")))                                                             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) NE ' '
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(1));                      //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(2));                      //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Ssn().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr());                                                //Natural: ASSIGN #SSN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Joint_Cnvrt().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind());                                            //Natural: ASSIGN #JOINT-CNVRT := IAA-CNTRCT.CNTRCT-JOINT-CNVRT-RCRD-IND
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Spirt().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce());                                               //Natural: ASSIGN #SPIRT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-SPIRT-SRCE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Pen_Pln_Cde().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde());                                                    //Natural: ASSIGN #PEN-PLN-CDE := IAA-CNTRCT.CNTRCT-PNSN-PLN-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Cntrct_Type().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Type_Cde());                                                        //Natural: ASSIGN #CNTRCT-TYPE := IAA-CNTRCT.CNTRCT-TYPE-CDE
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue2().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE2 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("10");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("2");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue2());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE2
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue3().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE3
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                          //Natural: ASSIGN #1ST-ANNT-X-REF := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Sex().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                                  //Natural: ASSIGN #1ST-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dob_Mm().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm());                                                //Natural: ASSIGN #1ST-DOB-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dob_Dd().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd());                                                //Natural: ASSIGN #1ST-DOB-DD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DD
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dob_Yy().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy());                                                //Natural: ASSIGN #1ST-DOB-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-YY
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dod_Mm().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Mm());                                                //Natural: ASSIGN #1ST-DOD-MM := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dod_Yy().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dod_Yy());                                                //Natural: ASSIGN #1ST-DOD-YY := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-YY
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte());                                         //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mort_Yob3().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                                      //Natural: ASSIGN #MORT-YOB3 := #DTE4-YY
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Life_Cnt3().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt());                                                //Natural: ASSIGN #LIFE-CNT3 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Div_Payee().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Div_Payee_Cde());                                                     //Natural: ASSIGN #DIV-PAYEE := IAA-CNTRCT.CNTRCT-DIV-PAYEE-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Coll_Cde().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Div_Coll_Cde());                                                       //Natural: ASSIGN #COLL-CDE := IAA-CNTRCT.CNTRCT-DIV-COLL-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Orig_Cntrct().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr());                                              //Natural: ASSIGN #ORIG-CNTRCT := IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Cash_Cde().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde());                                              //Natural: ASSIGN #CASH-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CASH-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Emp_Term().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde());                                    //Natural: ASSIGN #EMP-TERM := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue3().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE3 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("20");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("1");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue3());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE3
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue4().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE4
        if (condition(((((ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 10 THRU 18 )
            || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(10) && ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(18)))))
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                       //Natural: ASSIGN #2ND-ANNT-X-REF := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Sex().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                               //Natural: ASSIGN #2ND-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dob_Mm().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm());                                             //Natural: ASSIGN #2ND-DOB-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dob_Dd().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd());                                             //Natural: ASSIGN #2ND-DOB-DD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DD
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dob_Yy().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy());                                             //Natural: ASSIGN #2ND-DOB-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-YY
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dod_Mm().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm());                                             //Natural: ASSIGN #2ND-DOD-MM := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dod_Yy().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy());                                             //Natural: ASSIGN #2ND-DOD-YY := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-YY
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte());                                      //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mort_Yob4().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                                  //Natural: ASSIGN #MORT-YOB4 := #DTE4-YY
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Life_Cnt4().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt());                                            //Natural: ASSIGN #LIFE-CNT4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6)) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 5 OR = 6 OR = 21 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 8 THRU 17 )
            || (ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(8) && ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(17)))))
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Ben_Xref().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                                          //Natural: ASSIGN #BEN-XREF := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
            if (condition(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte().equals(getZero())))                                                                     //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-DOD-DTE = 0
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Ben_Dod_A().setValue(" ");                                                                                          //Natural: ASSIGN #BEN-DOD-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte());                                     //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-DOD-DTE
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Ben_Dod_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                       //Natural: ASSIGN #BEN-DOD-MM := #CCYYMM-DTE-MM
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Ben_Dod_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                       //Natural: ASSIGN #BEN-DOD-YY := #CCYYMM-DTE-YY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))                                                                                            //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Dest_Prev().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde());                                    //Natural: ASSIGN #DEST-PREV := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PREV-DIST-CDE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Dest_Curr().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde());                                    //Natural: ASSIGN #DEST-CURR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue4().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE4 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("20");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("2");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue4());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE4
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE5
        if (condition(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T") || ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("G"))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T' OR = 'G'
        {
            pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                             //Natural: ASSIGN #FUND-AFTR-KEY.#AFTR-IMGE-ID := '2'
            pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                           //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
            pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                         //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
            pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                                        //Natural: ASSIGN #FUND-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
                                                                                                                                                                          //Natural: PERFORM BUILD-TIAA-FUND-KEY
            sub_Build_Tiaa_Fund_Key();
            if (condition(Global.isEscape())) {return;}
            setValueToSubstring(pnd_Fund_Aftr_Key.getSubstring(2,14),ldaIaal915b.getPnd_Tiaa_Fund_Key(),1,14);                                                            //Natural: MOVE SUBSTR ( #FUND-AFTR-KEY,2,14 ) TO SUBSTR ( #TIAA-FUND-KEY,1,14 )
            ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().startDatabaseRead                                                                                                     //Natural: READ ( 1 ) IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY STARTING FROM #FUND-AFTR-KEY
            (
            "READ02",
            new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", ">=", pnd_Fund_Aftr_Key, WcType.BY) },
            new Oc[] { new Oc("TIAA_FUND_AFTR_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().readNextRow("READ02")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) || pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde.notEquals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF #FUND-AFTR-KEY.#CNTRCT-PPCN-NBR NE #TRANS-PPCN-NBR OR #FUND-AFTR-KEY.#CNTRCT-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                getReports().write(0, "NO AFTER TIAA FUND RECORD FOR: ",pnd_Fund_Aftr_Key);                                                                               //Natural: WRITE 'NO AFTER TIAA FUND RECORD FOR: ' #FUND-AFTR-KEY
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal915b.getVw_iaa_Tiaa_Fund_Rcrd().setValuesByName(ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans());                                                              //Natural: MOVE BY NAME IAA-TIAA-FUND-TRANS TO IAA-TIAA-FUND-RCRD
                                                                                                                                                                          //Natural: PERFORM DETERMINE-FINAL-PYMT
            sub_Determine_Final_Pymt();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                                //Natural: ASSIGN #REC-TYPE := 50
            //*  3/12 FROM 30
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                        //Natural: RESET #INDX1
            FOR01:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 250
            for (ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(250)); ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
            {
                if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                   //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE ' '
                {
                    if (condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                               //Natural: IF #INDX1 = 3
                    {
                        ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                         //Natural: ADD 1 TO #REC-TYPE
                        ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                        //Natural: ASSIGN #INDX1 := 1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                            //Natural: ADD 1 TO #INDX1
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rate().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX )
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Per_Pay().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-PAY := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #INDX )
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Per_Div().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-DIV := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #INDX )
                    if (condition(ldaIaal915b.getPnd_Logical_Variables_Pnd_Fin_Pay_Gt_0().getBoolean()))                                                                  //Natural: IF #FIN-PAY-GT-0
                    {
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pay().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #FIN-PAY := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pay_A().setValue(" ");                                                                                  //Natural: ASSIGN #FIN-PAY-A := ' '
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().greater(getZero())))                                                               //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT > 0
                    {
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt());                           //Natural: ASSIGN #TOT-OLD-PER-DIV := IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div_A().setValue(" ");                                                                          //Natural: ASSIGN #TOT-OLD-PER-DIV-A := ' '
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().greater(getZero())))                                                               //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT > 0
                    {
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt());                           //Natural: ASSIGN #TOT-OLD-PER-PAY := IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay_A().setValue(" ");                                                                          //Natural: ASSIGN #TOT-OLD-PER-PAY-A := ' '
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                    sub_Assign_Header();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1());                                          //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type());                                      //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                    getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5());                                                            //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                                //Natural: ASSIGN #REC-TYPE := 50
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                        //Natural: RESET #INDX1
            FOR02:                                                                                                                                                        //Natural: FOR #INDX = 1 TO 5
            for (ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(5)); ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
            {
                pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                         //Natural: ASSIGN #FUND-AFTR-KEY.#AFTR-IMGE-ID := '2'
                pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                       //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                     //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
                pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde.setValue(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product());                                                           //Natural: ASSIGN #FUND-AFTR-KEY.#CMPNY-FUND-CDE := #PRODUCT
                pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                                    //Natural: ASSIGN #FUND-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
                ldaIaal915b.getVw_iaa_Cref_Fund_Trans().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) IAA-CREF-FUND-TRANS WITH CREF-FUND-AFTR-KEY = #FUND-AFTR-KEY
                (
                "FIND01",
                new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", "=", pnd_Fund_Aftr_Key, WcType.WITH) },
                1
                );
                FIND01:
                while (condition(ldaIaal915b.getVw_iaa_Cref_Fund_Trans().readNextRow("FIND01")))
                {
                    ldaIaal915b.getVw_iaa_Cref_Fund_Trans().setIfNotFoundControlFlag(false);
                    ldaIaal915b.getVw_iaa_Cref_Fund_Rcrd().setValuesByName(ldaIaal915b.getVw_iaa_Cref_Fund_Trans());                                                      //Natural: MOVE BY NAME IAA-CREF-FUND-TRANS TO IAA-CREF-FUND-RCRD
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                   //Natural: IF IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( #INDX ) NE ' '
                {
                    if (condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                               //Natural: IF #INDX1 = 3
                    {
                        ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                         //Natural: ADD 1 TO #REC-TYPE
                        ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                        //Natural: ASSIGN #INDX1 := 1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                            //Natural: ADD 1 TO #INDX1
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rate().setValue(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( #INDX )
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Cref_Units().setValue(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #CREF-UNITS := IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( #INDX )
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                    sub_Assign_Header();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type());                                      //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1());                                          //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                    getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5());                                                            //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_35() throws Exception                                                                                                                        //Natural: PROCESS-35
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM GET-CPR-AFTR-KEY
        sub_Get_Cpr_Aftr_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915b.getPnd_Cntrct_Payee_Key().setValue(ldaIaal915b.getPnd_Cpr_Aftr_Key().getSubstring(2,12));                                                             //Natural: MOVE SUBSTR ( #CPR-AFTR-KEY,2,12 ) TO #CNTRCT-PAYEE-KEY
        ldaIaal915b.getVw_iaa_Cntrct_Prtcpnt_Role().setValuesByName(ldaIaal915b.getVw_iaa_Cpr_Trans());                                                                   //Natural: MOVE BY NAME IAA-CPR-TRANS TO IAA-CNTRCT-PRTCPNT-ROLE
                                                                                                                                                                          //Natural: PERFORM GET-CPR-BFRE-KEY
        sub_Get_Cpr_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-BFRE-KEY
        sub_Get_Cntrct_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE1
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte())))              //Natural: IF IAA-CPR-TRANS.CNTRCT-FINAL-PER-PAY-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                               //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                   //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                   //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Final_Pay_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte())))                      //Natural: IF IAA-CPR-TRANS.CNTRCT-FINAL-PAY-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                                //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                            //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                            //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                            //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
        sub_Assign_Header();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("10");                                                                                              //Natural: ASSIGN #RECORD-TYPE1 := '10'
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("1");                                                                                                //Natural: ASSIGN #RECORD-NBR1 := '1'
        getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1());                                                                        //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
    }
    private void sub_Process_50() throws Exception                                                                                                                        //Natural: PROCESS-50
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM GET-CPR-AFTR-KEY
        sub_Get_Cpr_Aftr_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915b.getPnd_Cntrct_Payee_Key().setValue(ldaIaal915b.getPnd_Cpr_Aftr_Key().getSubstring(2,12));                                                             //Natural: MOVE SUBSTR ( #CPR-AFTR-KEY,2,12 ) TO #CNTRCT-PAYEE-KEY
        ldaIaal915b.getVw_iaa_Cntrct_Prtcpnt_Role().setValuesByName(ldaIaal915b.getVw_iaa_Cpr_Trans());                                                                   //Natural: MOVE BY NAME IAA-CPR-TRANS TO IAA-CNTRCT-PRTCPNT-ROLE
                                                                                                                                                                          //Natural: PERFORM GET-CPR-BFRE-KEY
        sub_Get_Cpr_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-BFRE-KEY
        sub_Get_Cntrct_Bfre_Key();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE1
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Mode_Ind().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind())))                                //Natural: IF IAA-CPR-TRANS.CNTRCT-MODE-IND NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mode().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                              //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Optn_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde())))                                          //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-OPTN-CDE NE IAA-CNTRCT.CNTRCT-OPTN-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Option_N().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde());                                                       //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Orgn_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Orgn_Cde())))                                          //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-ORGN-CDE NE IAA-CNTRCT.CNTRCT-ORGN-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Origin().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                         //Natural: ASSIGN #ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Issue_Dte());                                                        //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                               //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                               //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-PYMNT-DUE-DTE NE IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                          //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                   //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                   //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-PYMNT-PD-DTE NE IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                           //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                   //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                   //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte())))              //Natural: IF IAA-CPR-TRANS.CNTRCT-FINAL-PER-PAY-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                               //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                   //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                   //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Final_Pay_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte())))                      //Natural: IF IAA-CPR-TRANS.CNTRCT-FINAL-PAY-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                                //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Cc());                                            //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-CC
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                            //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Wthdrwl_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte())))                          //Natural: IF IAA-CPR-TRANS.CNTRCT-WTHDRWL-DTE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                               //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                                      //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                                      //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue1().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE1 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("10");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("1");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
            ldaIaal915b.getPnd_Logical_Variables_Pnd_Issue_101_Written().setValue(true);                                                                                  //Natural: ASSIGN #ISSUE-101-WRITTEN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal915b.getPnd_Logical_Variables_Pnd_Issue_101_Written().setValue(false);                                                                                 //Natural: ASSIGN #ISSUE-101-WRITTEN := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue2().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE2
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde())))                        //Natural: IF IAA-CPR-TRANS.PRTCPNT-CTZNSHP-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Citizen().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde());                                       //Natural: ASSIGN #CITIZEN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-RSDNCY-AT-ISSUE-CDE NE IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde());                                  //Natural: ASSIGN #ST/CNTRY-ISS := IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Inst_Iss_Cde())))                                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-INST-ISS-CDE NE IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Coll_Iss().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Inst_Iss_Cde());                                                   //Natural: ASSIGN #COLL-ISS := IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T")))                                                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T'
        {
            if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Rtb_Amt().getValue(1).notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(1))))      //Natural: IF IAA-CPR-TRANS.CNTRCT-RTB-AMT ( 1 ) NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 1 )
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(1));                  //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Rtb_Amt().getValue(2).notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(2))))      //Natural: IF IAA-CPR-TRANS.CNTRCT-RTB-AMT ( 2 ) NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 2 )
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue(2));                  //Natural: ASSIGN #RTB/TTB-AMT := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr())))                          //Natural: IF IAA-CPR-TRANS.PRTCPNT-TAX-ID-NBR NE IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Ssn().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr());                                            //Natural: ASSIGN #SSN := IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind())))                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-JOINT-CNVRT-RCRD-IND NE IAA-CNTRCT.CNTRCT-JOINT-CNVRT-RCRD-IND
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Joint_Cnvrt().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind());                                        //Natural: ASSIGN #JOINT-CNVRT := IAA-CNTRCT.CNTRCT-JOINT-CNVRT-RCRD-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue2().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE2 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("10");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '10'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("2");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue2());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE2
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue3().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE3
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind())))                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind());                                      //Natural: ASSIGN #1ST-ANNT-X-REF := IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Sex().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                              //Natural: ASSIGN #1ST-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                                        //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dob_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                                 //Natural: ASSIGN #1ST-DOB-MM := #DTE8-MM
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dob_Dd().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                                 //Natural: ASSIGN #1ST-DOB-DD := #DTE8-DD
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Dob_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                                 //Natural: ASSIGN #1ST-DOB-YY := #DTE8-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte())))        //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE
        {
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte());                                     //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mort_Yob3().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                                  //Natural: ASSIGN #MORT-YOB3 := #DTE4-YY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt())))                      //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-LFE-CNT NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Life_Cnt3().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt());                                            //Natural: ASSIGN #LIFE-CNT3 := IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Div_Payee_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Div_Payee_Cde())))                                //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-DIV-PAYEE-CDE NE IAA-CNTRCT.CNTRCT-DIV-PAYEE-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Div_Payee().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Div_Payee_Cde());                                                 //Natural: ASSIGN #DIV-PAYEE := IAA-CNTRCT.CNTRCT-DIV-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Div_Coll_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Div_Coll_Cde())))                                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-DIV-COLL-CDE NE IAA-CNTRCT.CNTRCT-DIV-COLL-CDE
        {
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Coll_Cde().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Div_Coll_Cde());                                                   //Natural: ASSIGN #COLL-CDE := IAA-CNTRCT.CNTRCT-DIV-COLL-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))                                                                                            //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Cash_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde())))                            //Natural: IF IAA-CPR-TRANS.CNTRCT-CASH-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CASH-CDE
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Cash_Cde().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde());                                      //Natural: ASSIGN #CASH-CDE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CASH-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde())))        //Natural: IF IAA-CPR-TRANS.CNTRCT-EMPLYMNT-TRMNT-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Emp_Term().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde());                            //Natural: ASSIGN #EMP-TERM := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue3().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE3 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("20");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("1");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '1'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue3());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE3
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue4().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE4
        if (condition(((((ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 10 THRU 18 )
            || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(10) && ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(18)))))
        {
            if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind())))                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND NE IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                   //Natural: ASSIGN #2ND-ANNT-X-REF := IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde())))                   //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Sex().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                           //Natural: ASSIGN #2ND-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte())))                   //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte());                                     //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dob_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                             //Natural: ASSIGN #2ND-DOB-MM := #DTE8-MM
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dob_Dd().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                             //Natural: ASSIGN #2ND-DOB-DD := #DTE8-DD
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_2nd_Dob_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                             //Natural: ASSIGN #2ND-DOB-YY := #DTE8-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte())))     //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE NE IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE
            {
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyy_Dte4().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte());                                  //Natural: ASSIGN #CCYY-DTE4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mort_Yob4().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte4_Yy());                                              //Natural: ASSIGN #MORT-YOB4 := #DTE4-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt().notEquals(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt())))                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-LIFE-CNT NE IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Life_Cnt4().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt());                                        //Natural: ASSIGN #LIFE-CNT4 := IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))                                                                                            //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Curr_Dist_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde())))                  //Natural: IF IAA-CPR-TRANS.CNTRCT-CURR-DIST-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Dest_Curr().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde());                                //Natural: ASSIGN #DEST-CURR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Cpr_Trans_Cntrct_Prev_Dist_Cde().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde())))                  //Natural: IF IAA-CPR-TRANS.CNTRCT-PREV-DIST-CDE NE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PREV-DIST-CDE
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Dest_Prev().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde());                                //Natural: ASSIGN #DEST-PREV := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PREV-DIST-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6)) || ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 5 OR = 6 OR = 21 OR ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 8 THRU 17 )
            || (ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(8) && ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(17)))))
        {
            if (condition(ldaIaal915b.getIaa_Cpr_Trans_Bnfcry_Xref_Ind().notEquals(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind())))                            //Natural: IF IAA-CPR-TRANS.BNFCRY-XREF-IND NE IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
            {
                ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Ben_Xref().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind());                                      //Natural: ASSIGN #BEN-XREF := IAA-CNTRCT-PRTCPNT-ROLE.BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue4().notEquals(" ")))                                                                             //Natural: IF #REST-OF-ISSUE4 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
            sub_Assign_Header();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("20");                                                                                          //Natural: ASSIGN #RECORD-TYPE1 := '20'
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("2");                                                                                            //Natural: ASSIGN #RECORD-NBR1 := '2'
            getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue4());                                                                    //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE4
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5().reset();                                                                                                //Natural: RESET #MANUAL-NEW-ISSUE5
        if (condition(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T") || ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("G"))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T' OR = 'G'
        {
            pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                             //Natural: ASSIGN #FUND-AFTR-KEY.#AFTR-IMGE-ID := '2'
            pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                           //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
            pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                         //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
            pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                                        //Natural: ASSIGN #FUND-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
                                                                                                                                                                          //Natural: PERFORM BUILD-TIAA-FUND-KEY
            sub_Build_Tiaa_Fund_Key();
            if (condition(Global.isEscape())) {return;}
            setValueToSubstring(pnd_Fund_Aftr_Key.getSubstring(2,14),ldaIaal915b.getPnd_Tiaa_Fund_Key(),1,14);                                                            //Natural: MOVE SUBSTR ( #FUND-AFTR-KEY,2,14 ) TO SUBSTR ( #TIAA-FUND-KEY,1,14 )
            ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-TIAA-FUND-TRANS WITH TIAA-FUND-AFTR-KEY-2 = #FUND-AFTR-KEY-2
            (
            "FIND02",
            new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", "=", pnd_Fund_Aftr_Key_2, WcType.WITH) },
            1
            );
            FIND02:
            while (condition(ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().readNextRow("FIND02", true)))
            {
                ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
                {
                    getReports().write(0, "NO TIAA FUND RECORD FOR: ",pnd_Fund_Aftr_Key);                                                                                 //Natural: WRITE 'NO TIAA FUND RECORD FOR: ' #FUND-AFTR-KEY
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                ldaIaal915b.getVw_iaa_Tiaa_Fund_Rcrd().setValuesByName(ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans());                                                          //Natural: MOVE BY NAME IAA-TIAA-FUND-TRANS TO IAA-TIAA-FUND-RCRD
                pnd_Fund_Bfre_Key_2_Pnd_B_Cntrct.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                            //Natural: ASSIGN #B-CNTRCT := #TRANS-PPCN-NBR
                pnd_Fund_Bfre_Key_2_Pnd_B_Payee.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                            //Natural: ASSIGN #B-PAYEE := #TRANS-PAYEE-CDE
                pnd_Fund_Bfre_Key_2_Pnd_B_Trans_Dte.setValue(ldaIaal915b.getIaa_Tiaa_Fund_Trans_Trans_Dte());                                                             //Natural: ASSIGN #B-TRANS-DTE := IAA-TIAA-FUND-TRANS.TRANS-DTE
                pnd_Fund_Bfre_Key_2_Pnd_B_Cmpny.setValue(ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde());                                                       //Natural: ASSIGN #B-CMPNY := IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-TIAA-RATE-CHANGES
            sub_Determine_Tiaa_Rate_Changes();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().getBoolean()))                                                                          //Natural: IF #RATE-CHANGED
            {
                if (condition(ldaIaal915b.getPnd_Logical_Variables_Pnd_Issue_101_Written().getBoolean()))                                                                 //Natural: IF #ISSUE-101-WRITTEN
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mode().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                      //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Option_N().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde());                                               //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Origin().setValueEdited(ldaIaal915b.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                  //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORIGIN
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Issue_Dte());                                            //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                   //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                   //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                  //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                           //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                           //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                   //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                           //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                           //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                       //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                           //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                           //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                        //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                    //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                    //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                    //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                       //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                              //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                              //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                    sub_Assign_Header();
                    if (condition(Global.isEscape())) {return;}
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("10");                                                                                  //Natural: ASSIGN #RECORD-TYPE1 := '10'
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("1");                                                                                    //Natural: ASSIGN #RECORD-NBR1 := '1'
                    getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1());                                                            //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                            //Natural: ASSIGN #REC-TYPE := 50
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                    //Natural: RESET #INDX1
                FOR03:                                                                                                                                                    //Natural: FOR #INDX = 1 TO 30
                for (ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(30)); 
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
                {
                    if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))               //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE ' '
                    {
                        if (condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                           //Natural: IF #INDX1 = 3
                        {
                            ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                     //Natural: ADD 1 TO #REC-TYPE
                            ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                    //Natural: ASSIGN #INDX1 := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                        //Natural: ADD 1 TO #INDX1
                        }                                                                                                                                                 //Natural: END-IF
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5().reset();                                                                                //Natural: RESET #MANUAL-NEW-ISSUE5
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rate().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX )
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Per_Pay().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-PAY := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #INDX )
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Per_Div().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #PER-DIV := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #INDX )
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pay().setValue(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #FIN-PAY := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX )
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                        sub_Assign_Header();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type());                                  //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1());                                      //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                        getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5());                                                        //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                             //Natural: ASSIGN #FUND-AFTR-KEY.#AFTR-IMGE-ID := '2'
            pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                           //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
            pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                         //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
            pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde.setValue(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product());                                                               //Natural: ASSIGN #FUND-AFTR-KEY.#CMPNY-FUND-CDE := #PRODUCT
            pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                                        //Natural: ASSIGN #FUND-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
            ldaIaal915b.getVw_iaa_Cref_Fund_Trans().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CREF-FUND-TRANS WITH CREF-FUND-AFTR-KEY = #FUND-AFTR-KEY
            (
            "FIND03",
            new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", "=", pnd_Fund_Aftr_Key, WcType.WITH) },
            1
            );
            FIND03:
            while (condition(ldaIaal915b.getVw_iaa_Cref_Fund_Trans().readNextRow("FIND03")))
            {
                ldaIaal915b.getVw_iaa_Cref_Fund_Trans().setIfNotFoundControlFlag(false);
                ldaIaal915b.getVw_iaa_Cref_Fund_Rcrd().setValuesByName(ldaIaal915b.getVw_iaa_Cref_Fund_Trans());                                                          //Natural: MOVE BY NAME IAA-CREF-FUND-TRANS TO IAA-CREF-FUND-RCRD
                pnd_Fund_Bfre_Key_2_Pnd_B_Cntrct.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                            //Natural: ASSIGN #B-CNTRCT := #TRANS-PPCN-NBR
                pnd_Fund_Bfre_Key_2_Pnd_B_Payee.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                            //Natural: ASSIGN #B-PAYEE := #TRANS-PAYEE-CDE
                pnd_Fund_Bfre_Key_2_Pnd_B_Trans_Dte.setValue(ldaIaal915b.getIaa_Cref_Fund_Trans_Trans_Dte());                                                             //Natural: ASSIGN #B-TRANS-DTE := IAA-CREF-FUND-TRANS.TRANS-DTE
                pnd_Fund_Bfre_Key_2_Pnd_B_Cmpny.setValue(ldaIaal915b.getIaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde());                                                       //Natural: ASSIGN #B-CMPNY := IAA-CREF-FUND-TRANS.CREF-CMPNY-FUND-CDE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CREF-RATE-CHANGES
            sub_Determine_Cref_Rate_Changes();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().getBoolean()))                                                                          //Natural: IF #RATE-CHANGED
            {
                if (condition(ldaIaal915b.getPnd_Logical_Variables_Pnd_Issue_101_Written().getBoolean()))                                                                 //Natural: IF #ISSUE-101-WRITTEN
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mode().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                      //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Option_N().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Optn_Cde());                                               //Natural: ASSIGN #OPTION-N := IAA-CNTRCT.CNTRCT-OPTN-CDE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Origin().setValueEdited(ldaIaal915b.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                  //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORIGIN
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_Issue_Dte());                                            //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                                   //Natural: ASSIGN #ISS-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                                   //Natural: ASSIGN #ISS-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte());                                  //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                           //Natural: ASSIGN #1ST-PAY-DUE-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                           //Natural: ASSIGN #1ST-PAY-DUE-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte());                                   //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                           //Natural: ASSIGN #LST-MAN-CHK-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                           //Natural: ASSIGN #LST-MAN-CHK-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                       //Natural: ASSIGN #CCYYMM-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm());                           //Natural: ASSIGN #FIN-PER-PAY-DTE-MM := #CCYYMM-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy());                           //Natural: ASSIGN #FIN-PER-PAY-DTE-YY := #CCYYMM-DTE-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte());                        //Natural: ASSIGN #CCYYMMDD-DTE8 := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Mm());                                    //Natural: ASSIGN #FIN-PYMT-DTE-MM := #DTE8-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Dd());                                    //Natural: ASSIGN #FIN-PYMT-DTE-DD := #DTE8-DD
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Dte8_Yy());                                    //Natural: ASSIGN #FIN-PYMT-DTE-YY := #DTE8-YY
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte().setValue(ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte());                       //Natural: ASSIGN #HOLD-WTHDRWL-DTE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-WTHDRWL-DTE
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm());                              //Natural: ASSIGN #WDRAWAL-DTE-MM := #WTHDRWL-DTE-MM
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy());                              //Natural: ASSIGN #WDRAWAL-DTE-YY := #WTHDRWL-DTE-YY
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                    sub_Assign_Header();
                    if (condition(Global.isEscape())) {return;}
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type1().setValue("10");                                                                                  //Natural: ASSIGN #RECORD-TYPE1 := '10'
                    ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr1().setValue("1");                                                                                    //Natural: ASSIGN #RECORD-NBR1 := '1'
                    getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1());                                                            //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE1
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().setValue(50);                                                                                            //Natural: ASSIGN #REC-TYPE := 50
                ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().reset();                                                                                                    //Natural: RESET #INDX1
                FOR04:                                                                                                                                                    //Natural: FOR #INDX = 1 TO 5
                for (ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(5)); 
                    ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
                {
                    pnd_Fund_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                     //Natural: ASSIGN #FUND-AFTR-KEY.#AFTR-IMGE-ID := '2'
                    pnd_Fund_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                                   //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
                    pnd_Fund_Aftr_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                                 //Natural: ASSIGN #FUND-AFTR-KEY.#CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
                    pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde.setValue(ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product());                                                       //Natural: ASSIGN #FUND-AFTR-KEY.#CMPNY-FUND-CDE := #PRODUCT
                    pnd_Fund_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                                //Natural: ASSIGN #FUND-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
                    ldaIaal915b.getVw_iaa_Cref_Fund_Trans().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) IAA-CREF-FUND-TRANS WITH CREF-FUND-AFTR-KEY = #FUND-AFTR-KEY
                    (
                    "FIND04",
                    new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", "=", pnd_Fund_Aftr_Key, WcType.WITH) },
                    1
                    );
                    FIND04:
                    while (condition(ldaIaal915b.getVw_iaa_Cref_Fund_Trans().readNextRow("FIND04")))
                    {
                        ldaIaal915b.getVw_iaa_Cref_Fund_Trans().setIfNotFoundControlFlag(false);
                        ldaIaal915b.getVw_iaa_Cref_Fund_Rcrd().setValuesByName(ldaIaal915b.getVw_iaa_Cref_Fund_Trans());                                                  //Natural: MOVE BY NAME IAA-CREF-FUND-TRANS TO IAA-CREF-FUND-RCRD
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))               //Natural: IF IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( #INDX ) NE ' '
                    {
                        if (condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().equals(3)))                                                                           //Natural: IF #INDX1 = 3
                        {
                            ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type().nadd(1);                                                                                     //Natural: ADD 1 TO #REC-TYPE
                            ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().setValue(1);                                                                                    //Natural: ASSIGN #INDX1 := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1().nadd(1);                                                                                        //Natural: ADD 1 TO #INDX1
                        }                                                                                                                                                 //Natural: END-IF
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5().reset();                                                                                //Natural: RESET #MANUAL-NEW-ISSUE5
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Rate().setValue(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #RATE := IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( #INDX )
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Cref_Units().setValue(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx())); //Natural: ASSIGN #CREF-UNITS := IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( #INDX )
                                                                                                                                                                          //Natural: PERFORM ASSIGN-HEADER
                        sub_Assign_Header();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Type5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Rec_Type());                                  //Natural: ASSIGN #RECORD-TYPE5 := #REC-TYPE
                        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Nbr5().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx1());                                      //Natural: ASSIGN #RECORD-NBR5 := #INDX1
                        getWorkFiles().write(1, false, ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5());                                                        //Natural: WRITE WORK FILE 1 #MANUAL-NEW-ISSUE5
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Assign_Header() throws Exception                                                                                                                     //Natural: ASSIGN-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Check_Dte_Mm1().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm);                                                           //Natural: ASSIGN #CHECK-DTE-MM1 := #TRANS-CHECK-DTE-MM
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Check_Dte_Dd1().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd);                                                           //Natural: ASSIGN #CHECK-DTE-DD1 := #TRANS-CHECK-DTE-DD
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Check_Dte_Yy1().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy);                                                           //Natural: ASSIGN #CHECK-DTE-YY1 := #TRANS-CHECK-DTE-YY
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8);                                                               //Natural: ASSIGN #CNTRCT-NBR1 := #TRANS-PPCN-NBR-8
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Record_Status1().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                             //Natural: ASSIGN #RECORD-STATUS1 := #TRANS-PAYEE-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1().setValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr());                                           //Natural: ASSIGN #CROSS-REF-NBR1 := #CROSS-REF-NBR
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1().setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                       //Natural: ASSIGN #TRANS-NBR1 := #TRANS-CDE
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Trans_Dte1().setValueEdited(pnd_Passed_Data_Pnd_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED #PASSED-DATA.#TRANS-DTE ( EM = YYYYMMDD ) TO #TRANS-DTE1
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_User_Area1().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                                 //Natural: ASSIGN #USER-AREA1 := #TRANS-USER-AREA
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_User_Id1().setValue(pnd_Passed_Data_Pnd_Trans_User_Id);                                                                     //Natural: ASSIGN #USER-ID1 := #TRANS-USER-ID
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Seq_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Seq_Nbr);                                                                      //Natural: ASSIGN #SEQ-NBR := #TRANS-SEQ-NBR
        ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Mode1().setValue(pnd_Passed_Data_Pnd_Trans_Mode);                                                                           //Natural: ASSIGN #MODE1 := #TRANS-MODE
    }
    private void sub_Get_Cntrct_Bfre_Key() throws Exception                                                                                                               //Natural: GET-CNTRCT-BFRE-KEY
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                              //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal915b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915b.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Trans_Dte);                                                                       //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #PASSED-DATA.#TRANS-DTE
        ldaIaal915b.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal915b.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(ldaIaal915b.getVw_iaa_Cntrct_Trans().readNextRow("READ03")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915b.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                            //Natural: IF #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR NE #TRANS-PPCN-NBR
        {
            getReports().write(0, "NO CNTRCT BEFORE RECORD FOR: ",ldaIaal915b.getPnd_Cntrct_Bfre_Key());                                                                  //Natural: WRITE 'NO CNTRCT BEFORE RECORD FOR: ' #CNTRCT-BFRE-KEY
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Cpr_Bfre_Key() throws Exception                                                                                                                  //Natural: GET-CPR-BFRE-KEY
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGE-ID := '2'
        ldaIaal915b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                               //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                        //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal915b.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Trans_Dte);                                                                          //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #PASSED-DATA.#TRANS-DTE
        ldaIaal915b.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
        (
        "READ04",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal915b.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(ldaIaal915b.getVw_iaa_Cpr_Trans().readNextRow("READ04")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) || ldaIaal915b.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().notEquals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF #CPR-BFRE-KEY.#CNTRCT-PPCN-NBR NE #TRANS-PPCN-NBR OR #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE NE #TRANS-PAYEE-CDE
        {
            getReports().write(0, "NO CPR BEFORE RECORD FOR: ",ldaIaal915b.getPnd_Cpr_Bfre_Key());                                                                        //Natural: WRITE 'NO CPR BEFORE RECORD FOR: ' #CPR-BFRE-KEY
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Cpr_Aftr_Key() throws Exception                                                                                                                  //Natural: GET-CPR-AFTR-KEY
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN #CPR-AFTR-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal915b.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                               //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915b.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                        //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal915b.getPnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                            //Natural: ASSIGN #CPR-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
        ldaIaal915b.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
        (
        "READ05",
        new Wc[] { new Wc("CPR_AFTR_KEY", ">=", ldaIaal915b.getPnd_Cpr_Aftr_Key(), WcType.BY) },
        new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
        1
        );
        READ05:
        while (condition(ldaIaal915b.getVw_iaa_Cpr_Trans().readNextRow("READ05")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915b.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr().notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr) || ldaIaal915b.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde().notEquals(pnd_Passed_Data_Pnd_Trans_Payee_Cde))) //Natural: IF #CPR-AFTR-KEY.#CNTRCT-PPCN-NBR NE #TRANS-PPCN-NBR OR #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE NE #TRANS-PAYEE-CDE
        {
            getReports().write(0, "NO CPR AFTER RECORD FOR: ",ldaIaal915b.getPnd_Cpr_Aftr_Key());                                                                         //Natural: WRITE 'NO CPR AFTER RECORD FOR: ' #CPR-AFTR-KEY
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Build_Tiaa_Fund_Key() throws Exception                                                                                                               //Natural: BUILD-TIAA-FUND-KEY
    {
        if (BLNatReinput.isReinput()) return;

        if (condition((ldaIaal915b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals("T")) && (pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W024999")  //Natural: IF ( IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T' ) AND ( SUBSTR ( #TRANS-PPCN-NBR,1,7 ) GT 'W024999' AND SUBSTR ( #TRANS-PPCN-NBR,1,7 ) LT 'W090000' )
            > 0 && pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W090000") < 0)))
        {
            pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde.setValue("TG ");                                                                                                         //Natural: ASSIGN #FUND-AFTR-KEY.#CMPNY-FUND-CDE := 'TG '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fund_Aftr_Key_Pnd_Cmpny_Fund_Cde.setValue("TT ");                                                                                                         //Natural: ASSIGN #FUND-AFTR-KEY.#CMPNY-FUND-CDE := 'TT '
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Tiaa_Rate_Changes() throws Exception                                                                                                       //Natural: DETERMINE-TIAA-RATE-CHANGES
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().reset();                                                                                                  //Natural: RESET #RATE-CHANGED
        //*  WHERE
        ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #FUND-BFRE-KEY-2
        (
        "READ06",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Fund_Bfre_Key_2.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") },
        1
        );
        READ06:
        while (condition(ldaIaal915b.getVw_iaa_Tiaa_Fund_Trans().readNextRow("READ06")))
        {
            if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().notEquals("1") || ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Cntrct)  //Natural: IF IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID NE '1' OR IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR NE #B-CNTRCT OR IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE NE #B-PAYEE OR IAA-TIAA-FUND-TRANS.TRANS-DTE NE #B-TRANS-DTE OR IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE NE #B-CMPNY
                || ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Payee) || ldaIaal915b.getIaa_Tiaa_Fund_Trans_Trans_Dte().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Trans_Dte) 
                || ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Cmpny)))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #INDX = 1 TO IAA-TIAA-FUND-RCRD.C*TIAA-RATE-DATA-GRP
        for (ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp())); 
            ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
        {
            if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                       //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE ' '
            {
                short decideConditionsMet1627 = 0;                                                                                                                        //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-RATE-CDE ( #INDX )
                if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()))))
                {
                    decideConditionsMet1627++;
                    ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                               //Natural: ASSIGN #RATE-CHANGED := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-PER-PAY-AMT ( #INDX )
                if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()))))
                {
                    decideConditionsMet1627++;
                    ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                               //Natural: ASSIGN #RATE-CHANGED := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-PER-DIV-AMT ( #INDX )
                if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()))))
                {
                    decideConditionsMet1627++;
                    ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                               //Natural: ASSIGN #RATE-CHANGED := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX ) NE IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( #INDX )
                if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal915b.getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()))))
                {
                    decideConditionsMet1627++;
                    ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                               //Natural: ASSIGN #RATE-CHANGED := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet1627 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Cref_Rate_Changes() throws Exception                                                                                                       //Natural: DETERMINE-CREF-RATE-CHANGES
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().reset();                                                                                                  //Natural: RESET #RATE-CHANGED
        ldaIaal915b.getVw_iaa_Cref_Fund_Trans().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) IAA-CREF-FUND-TRANS BY CREF-FUND-BFRE-KEY-2 STARTING FROM #FUND-BFRE-KEY-2
        (
        "READ07",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Fund_Bfre_Key_2.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") },
        1
        );
        READ07:
        while (condition(ldaIaal915b.getVw_iaa_Cref_Fund_Trans().readNextRow("READ07")))
        {
            if (condition(ldaIaal915b.getIaa_Cref_Fund_Trans_Bfre_Imge_Id().notEquals("1") || ldaIaal915b.getIaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Cntrct)  //Natural: IF IAA-CREF-FUND-TRANS.BFRE-IMGE-ID NE '1' OR IAA-CREF-FUND-TRANS.CREF-CNTRCT-PPCN-NBR NE #B-CNTRCT OR IAA-CREF-FUND-TRANS.CREF-CNTRCT-PAYEE-CDE NE #B-PAYEE OR IAA-CREF-FUND-TRANS.TRANS-DTE NE #B-TRANS-DTE OR IAA-CREF-FUND-TRANS.CREF-CMPNY-FUND-CDE NE #B-CMPNY
                || ldaIaal915b.getIaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Payee) || ldaIaal915b.getIaa_Cref_Fund_Trans_Trans_Dte().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Trans_Dte) 
                || ldaIaal915b.getIaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde().notEquals(pnd_Fund_Bfre_Key_2_Pnd_B_Cmpny)))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #INDX = 1 TO 5
        for (ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(5)); ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
        {
            if (condition(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(" ")))                       //Natural: IF IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( #INDX ) NE ' '
            {
                short decideConditionsMet1660 = 0;                                                                                                                        //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( #INDX ) NE IAA-CREF-FUND-TRANS.CREF-RATE-CDE ( #INDX )
                if (condition(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal915b.getIaa_Cref_Fund_Trans_Cref_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()))))
                {
                    decideConditionsMet1660++;
                    ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                               //Natural: ASSIGN #RATE-CHANGED := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( #INDX ) NE IAA-CREF-FUND-TRANS.CREF-UNITS-CNT ( #INDX )
                if (condition(ldaIaal915b.getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).notEquals(ldaIaal915b.getIaa_Cref_Fund_Trans_Cref_Units_Cnt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()))))
                {
                    decideConditionsMet1660++;
                    ldaIaal915b.getPnd_Logical_Variables_Pnd_Rate_Changed().setValue(true);                                                                               //Natural: ASSIGN #RATE-CHANGED := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet1660 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  3/12 FROM 30
    private void sub_Determine_Final_Pymt() throws Exception                                                                                                              //Natural: DETERMINE-FINAL-PYMT
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #INDX = 1 TO 250
        for (ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().setValue(1); condition(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().lessOrEqual(250)); ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx().nadd(1))
        {
            if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).equals(" ")))                          //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915b.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal915b.getPnd_Misc_Variables_Pnd_Indx()).greater(getZero())))         //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #INDX ) > 0
            {
                ldaIaal915b.getPnd_Logical_Variables_Pnd_Fin_Pay_Gt_0().setValue(true);                                                                                   //Natural: ASSIGN #FIN-PAY-GT-0 := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915b.getPnd_Logical_Variables_Pnd_Fin_Pay_Gt_0().setValue(false);                                                                                  //Natural: ASSIGN #FIN-PAY-GT-0 := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Prod_Code() throws Exception                                                                                                               //Natural: DETERMINE-PROD-CODE
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1693 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #TRANS-PPCN-NBR,1,7 ) GT 'W024999' AND SUBSTR ( #TRANS-PPCN-NBR,1,7 ) LT 'W090000'
        if (condition(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W024999") > 0 && pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr.getSubstring(1,7).compareTo("W090000") 
            < 0))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("G");                                                                                                //Natural: ASSIGN #PRODUCT := 'G'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '0L' ) OR = MASK ( '0M' ) OR = MASK ( '0N' ) OR = MASK ( '0T' ) OR = MASK ( '0U' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0L'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0M'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0N'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'0T'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,
            "'0U'")))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("C");                                                                                                //Natural: ASSIGN #PRODUCT := 'C'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L3' ) OR = MASK ( '6M3' ) OR = MASK ( '6N3' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L3'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M3'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N3'")))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("S");                                                                                                //Natural: ASSIGN #PRODUCT := 'S'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L4' ) OR = MASK ( '6M4' ) OR = MASK ( '6N4' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L4'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M4'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N4'")))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("W");                                                                                                //Natural: ASSIGN #PRODUCT := 'W'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L5' ) OR = MASK ( '6M5' ) OR = MASK ( '6N5' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L5'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M5'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N5'")))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("L");                                                                                                //Natural: ASSIGN #PRODUCT := 'L'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L6' ) OR = MASK ( '6M6' ) OR = MASK ( '6N6' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L6'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M6'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N6'")))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("E");                                                                                                //Natural: ASSIGN #PRODUCT := 'E'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( '6L' ) OR = MASK ( '6M' ) OR = MASK ( '6N' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6L'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6M'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'6N'")))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("M");                                                                                                //Natural: ASSIGN #PRODUCT := 'M'
        }                                                                                                                                                                 //Natural: WHEN #TRANS-PPCN-NBR = MASK ( 'GA' ) OR = MASK ( 'GW' ) OR = MASK ( 'IA' ) OR = MASK ( 'IB' ) OR = MASK ( 'IC' ) OR = MASK ( 'ID' ) OR = MASK ( 'IE' ) OR = MASK ( 'IF' ) OR = MASK ( 'S0' ) OR = MASK ( 'W0' )
        else if (condition(DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'GA'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'GW'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IA'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IB'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IC'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'ID'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IE'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'IF'") 
            || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'S0'") || DbsUtil.maskMatches(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr,"'W0'")))
        {
            decideConditionsMet1693++;
            ldaIaal915b.getPnd_Selection_Rcrd_Pnd_Product().setValue("T");                                                                                                //Natural: ASSIGN #PRODUCT := 'T'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
