/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:42:08 AM
**        * FROM NATURAL SUBPROGRAM : Iadn171
************************************************************
**        * FILE NAME            : Iadn171.java
**        * CLASS NAME           : Iadn171
**        * INSTANCE NAME        : Iadn171
************************************************************
************************************************************************
* PROGRAM: IADN171
* DATE   : 11/16/01
* AUTHOR : RAY MORAN
* MODE   : BATCH
* DESC   : THIS SUB-PROGRAM IS CALLED FROM IADP170 FOR A SPECIFIC IA
*        : CONTRACT NUMBER GET OPENNING VALUE & 1ST PAYMENT FOR ADAM
*        : NEW ISSUE TPA CONTRACTS
*
*
* HISTORY:
*  1/02 REMOVED CALCULATION TO SUBTRACT PMT-AMT FROM TOTAL ACCUM-AMT
*       DO SCAN ON 1/02 FOR CHANGES
*
*  4/02 ADDED READ-NAZ-FOR-SWEEP FOR IA TRANSACTION CODE 60
*       DO SCAN ON 4/02 FOR CHANGES
*
* 10/03 INCREASE RATE INFO FROM 1:60 TO 1:80
*       DO SCAN ON 10/03 FOR CHANGES
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadn171 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Ia_Naz_Call_Parms;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_I_Unique_Id_Nbr;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_I_Da_Contract_Naz;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Payee_Cde;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Amount_Naz;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_I_Read_Type;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_O_Found;
    private DbsField pnd_Ia_Naz_Call_Parms_Pnd_O_25_Over_Sw;

    private DataAccessProgramView vw_naz_Ia_Rslt_Ddm;
    private DbsField naz_Ia_Rslt_Ddm_Rqst_Id;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Ia_Rcrd_Cde;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Sqnce_Nbr;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Stts_Cde;
    private DbsGroup naz_Ia_Rslt_Ddm_Nai_Da_Tiaa_NbrsMuGroup;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Da_Tiaa_Nbrs;
    private DbsGroup naz_Ia_Rslt_Ddm_Nai_Da_Cref_NbrsMuGroup;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Da_Cref_Nbrs;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Nbr;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Payee_Cde;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Ia_Cref_Nbr;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Ia_Cref_Payee_Cde;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Curncy_Cde;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Pymnt_Cde;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Pymnt_Mode;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Lst_Actvty_Dte;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Annty_Strt_Dte;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Instllmnt_Dte;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Frst_Pymnt_Due_Dte;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Frst_Ck_Pd_Dte;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Finl_Periodic_Py_Dte;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Finl_Prtl_Py_Dte;
    private DbsField naz_Ia_Rslt_Ddm_Count_Castnai_Dtl_Tiaa_Data;

    private DbsGroup naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Std_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Acct_Cd;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Std_Pay_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Optn_Cde;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Tiaa_Sttlmnt;
    private DbsField naz_Ia_Rslt_Ddm_Nai_Tiaa_Re_Sttlmnt;
    private DbsField pnd_Count_Pe;
    private DbsField pnd_I;
    private DbsField pnd_Eff_Date;

    private DbsGroup naz_Key;
    private DbsField naz_Key_Naz_Ia_Tiaa_Nbr;
    private DbsField naz_Key_Naz_Ia_Tiaa_Payee_Cde;
    private DbsField naz_Key_Naz_Ia_Rcrd_Cde;
    private DbsField naz_Key_Naz_Sqnce_Nbr;

    private DbsGroup naz_Key__R_Field_1;
    private DbsField naz_Key_Pnd_Naz_Super;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Ia_Naz_Call_Parms = parameters.newGroupInRecord("pnd_Ia_Naz_Call_Parms", "#IA-NAZ-CALL-PARMS");
        pnd_Ia_Naz_Call_Parms.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Naz_Call_Parms_Pnd_I_Unique_Id_Nbr = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_I_Unique_Id_Nbr", "#I-UNIQUE-ID-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ia_Naz_Call_Parms_Pnd_I_Da_Contract_Naz = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_I_Da_Contract_Naz", "#I-DA-CONTRACT-NAZ", 
            FieldType.STRING, 8);
        pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz", "#I-IA-CONTRACT-NAZ", 
            FieldType.STRING, 8);
        pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Payee_Cde = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Payee_Cde", "#I-IA-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Amount_Naz = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Amount_Naz", "#I-IA-AMOUNT-NAZ", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ia_Naz_Call_Parms_Pnd_I_Read_Type = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_I_Read_Type", "#I-READ-TYPE", FieldType.STRING, 
            1);
        pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz", "#O-SETTLE-AMT-NAZ", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt", "#O-TOTAL-ACCUM-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt", "#O-PMT-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Ia_Naz_Call_Parms_Pnd_O_Found = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_O_Found", "#O-FOUND", FieldType.STRING, 1);
        pnd_Ia_Naz_Call_Parms_Pnd_O_25_Over_Sw = pnd_Ia_Naz_Call_Parms.newFieldInGroup("pnd_Ia_Naz_Call_Parms_Pnd_O_25_Over_Sw", "#O-25-OVER-SW", FieldType.STRING, 
            1);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_naz_Ia_Rslt_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Ia_Rslt_Ddm", "NAZ-IA-RSLT-DDM"), "NAZ_IA_RSLT_DDM", "NAZ_IA_RSLT_RCRD", DdmPeriodicGroups.getInstance().getGroups("NAZ_IA_RSLT_DDM"));
        naz_Ia_Rslt_Ddm_Rqst_Id = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Rqst_Id", "RQST-ID", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "RQST_ID");
        naz_Ia_Rslt_Ddm_Nai_Ia_Rcrd_Cde = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Ia_Rcrd_Cde", "NAI-IA-RCRD-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "NAI_IA_RCRD_CDE");
        naz_Ia_Rslt_Ddm_Nai_Sqnce_Nbr = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Sqnce_Nbr", "NAI-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NAI_SQNCE_NBR");
        naz_Ia_Rslt_Ddm_Nai_Stts_Cde = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Stts_Cde", "NAI-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "NAI_STTS_CDE");
        naz_Ia_Rslt_Ddm_Nai_Da_Tiaa_NbrsMuGroup = vw_naz_Ia_Rslt_Ddm.getRecord().newGroupInGroup("NAZ_IA_RSLT_DDM_NAI_DA_TIAA_NBRSMuGroup", "NAI_DA_TIAA_NBRSMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_IA_RSLT_RCRD_NAI_DA_TIAA_NBRS");
        naz_Ia_Rslt_Ddm_Nai_Da_Tiaa_Nbrs = naz_Ia_Rslt_Ddm_Nai_Da_Tiaa_NbrsMuGroup.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Da_Tiaa_Nbrs", "NAI-DA-TIAA-NBRS", 
            FieldType.STRING, 10, new DbsArrayController(1, 6), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAI_DA_TIAA_NBRS");
        naz_Ia_Rslt_Ddm_Nai_Da_Cref_NbrsMuGroup = vw_naz_Ia_Rslt_Ddm.getRecord().newGroupInGroup("NAZ_IA_RSLT_DDM_NAI_DA_CREF_NBRSMuGroup", "NAI_DA_CREF_NBRSMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_IA_RSLT_RCRD_NAI_DA_CREF_NBRS");
        naz_Ia_Rslt_Ddm_Nai_Da_Cref_Nbrs = naz_Ia_Rslt_Ddm_Nai_Da_Cref_NbrsMuGroup.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Da_Cref_Nbrs", "NAI-DA-CREF-NBRS", 
            FieldType.STRING, 10, new DbsArrayController(1, 6), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAI_DA_CREF_NBRS");
        naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Nbr = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Nbr", "NAI-IA-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NAI_IA_TIAA_NBR");
        naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Payee_Cde = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Payee_Cde", "NAI-IA-TIAA-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NAI_IA_TIAA_PAYEE_CDE");
        naz_Ia_Rslt_Ddm_Nai_Ia_Cref_Nbr = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Ia_Cref_Nbr", "NAI-IA-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NAI_IA_CREF_NBR");
        naz_Ia_Rslt_Ddm_Nai_Ia_Cref_Payee_Cde = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Ia_Cref_Payee_Cde", "NAI-IA-CREF-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NAI_IA_CREF_PAYEE_CDE");
        naz_Ia_Rslt_Ddm_Nai_Curncy_Cde = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Curncy_Cde", "NAI-CURNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NAI_CURNCY_CDE");
        naz_Ia_Rslt_Ddm_Nai_Pymnt_Cde = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Pymnt_Cde", "NAI-PYMNT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NAI_PYMNT_CDE");
        naz_Ia_Rslt_Ddm_Nai_Pymnt_Mode = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Pymnt_Mode", "NAI-PYMNT-MODE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NAI_PYMNT_MODE");
        naz_Ia_Rslt_Ddm_Nai_Lst_Actvty_Dte = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Lst_Actvty_Dte", "NAI-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAI_LST_ACTVTY_DTE");
        naz_Ia_Rslt_Ddm_Nai_Annty_Strt_Dte = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Annty_Strt_Dte", "NAI-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAI_ANNTY_STRT_DTE");
        naz_Ia_Rslt_Ddm_Nai_Instllmnt_Dte = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Instllmnt_Dte", "NAI-INSTLLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "NAI_INSTLLMNT_DTE");
        naz_Ia_Rslt_Ddm_Nai_Frst_Pymnt_Due_Dte = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Frst_Pymnt_Due_Dte", "NAI-FRST-PYMNT-DUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAI_FRST_PYMNT_DUE_DTE");
        naz_Ia_Rslt_Ddm_Nai_Frst_Ck_Pd_Dte = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Frst_Ck_Pd_Dte", "NAI-FRST-CK-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAI_FRST_CK_PD_DTE");
        naz_Ia_Rslt_Ddm_Nai_Finl_Periodic_Py_Dte = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Finl_Periodic_Py_Dte", "NAI-FINL-PERIODIC-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAI_FINL_PERIODIC_PY_DTE");
        naz_Ia_Rslt_Ddm_Nai_Finl_Prtl_Py_Dte = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Finl_Prtl_Py_Dte", "NAI-FINL-PRTL-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAI_FINL_PRTL_PY_DTE");
        naz_Ia_Rslt_Ddm_Count_Castnai_Dtl_Tiaa_Data = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Count_Castnai_Dtl_Tiaa_Data", "C*NAI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");

        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data = vw_naz_Ia_Rslt_Ddm.getRecord().newGroupInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data", "NAI-DTL-TIAA-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Rate_Cd = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Rate_Cd", "NAI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_DA_RATE_CD", "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Std_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Std_Amt", "NAI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_DA_STD_AMT", "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Grd_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Grd_Amt", "NAI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_DA_GRD_AMT", "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Acct_Cd = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Acct_Cd", "NAI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_ACCT_CD", "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Ia_Rate_Cd = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Ia_Rate_Cd", "NAI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_IA_RATE_CD", "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Eff_Rte_Intrst = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Eff_Rte_Intrst", 
            "NAI-DTL-TIAA-EFF-RTE-INTRST", FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_EFF_RTE_INTRST", 
            "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Grd_Grntd_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Grd_Grntd_Amt", 
            "NAI-DTL-TIAA-GRD-GRNTD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_GRD_GRNTD_AMT", 
            "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt", 
            "NAI-DTL-TIAA-GRD-DVDND-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_GRD_DVDND_AMT", 
            "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Grd_Pay_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Grd_Pay_Amt", "NAI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_FNL_GRD_PAY_AMT", "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Grd_Dvdnd_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Grd_Dvdnd_Amt", 
            "NAI-DTL-FNL-GRD-DVDND-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_FNL_GRD_DVDND_AMT", 
            "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Grntd_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Grntd_Amt", 
            "NAI-DTL-TIAA-STD-GRNTD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_STD_GRNTD_AMT", 
            "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Dvdnd_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Dvdnd_Amt", 
            "NAI-DTL-TIAA-STD-DVDND-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_TIAA_STD_DVDND_AMT", 
            "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Std_Pay_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Std_Pay_Amt", "NAI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_FNL_STD_PAY_AMT", "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Std_Dvdnd_Amt = naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Std_Dvdnd_Amt", 
            "NAI-DTL-FNL-STD-DVDND-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAI_DTL_FNL_STD_DVDND_AMT", 
            "NAZ_IA_RSLT_RCRD_NAI_DTL_TIAA_DATA");
        naz_Ia_Rslt_Ddm_Nai_Optn_Cde = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Optn_Cde", "NAI-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "NAI_OPTN_CDE");
        naz_Ia_Rslt_Ddm_Nai_Tiaa_Sttlmnt = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Tiaa_Sttlmnt", "NAI-TIAA-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NAI_TIAA_STTLMNT");
        naz_Ia_Rslt_Ddm_Nai_Tiaa_Re_Sttlmnt = vw_naz_Ia_Rslt_Ddm.getRecord().newFieldInGroup("naz_Ia_Rslt_Ddm_Nai_Tiaa_Re_Sttlmnt", "NAI-TIAA-RE-STTLMNT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAI_TIAA_RE_STTLMNT");
        registerRecord(vw_naz_Ia_Rslt_Ddm);

        pnd_Count_Pe = localVariables.newFieldInRecord("pnd_Count_Pe", "#COUNT-PE", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        naz_Key = localVariables.newGroupInRecord("naz_Key", "NAZ-KEY");
        naz_Key_Naz_Ia_Tiaa_Nbr = naz_Key.newFieldInGroup("naz_Key_Naz_Ia_Tiaa_Nbr", "NAZ-IA-TIAA-NBR", FieldType.STRING, 10);
        naz_Key_Naz_Ia_Tiaa_Payee_Cde = naz_Key.newFieldInGroup("naz_Key_Naz_Ia_Tiaa_Payee_Cde", "NAZ-IA-TIAA-PAYEE-CDE", FieldType.NUMERIC, 2);
        naz_Key_Naz_Ia_Rcrd_Cde = naz_Key.newFieldInGroup("naz_Key_Naz_Ia_Rcrd_Cde", "NAZ-IA-RCRD-CDE", FieldType.STRING, 3);
        naz_Key_Naz_Sqnce_Nbr = naz_Key.newFieldInGroup("naz_Key_Naz_Sqnce_Nbr", "NAZ-SQNCE-NBR", FieldType.NUMERIC, 3);

        naz_Key__R_Field_1 = localVariables.newGroupInRecord("naz_Key__R_Field_1", "REDEFINE", naz_Key);
        naz_Key_Pnd_Naz_Super = naz_Key__R_Field_1.newFieldInGroup("naz_Key_Pnd_Naz_Super", "#NAZ-SUPER", FieldType.STRING, 18);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Ia_Rslt_Ddm.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iadn171() throws Exception
    {
        super("Iadn171");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IADN171", onError);
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  ADDED FOLLOWING 4/02
        if (condition(pnd_Ia_Naz_Call_Parms_Pnd_I_Read_Type.equals("N")))                                                                                                 //Natural: IF #I-READ-TYPE = 'N'
        {
                                                                                                                                                                          //Natural: PERFORM READ-NAZ-FOR-NEW-ISSUE
            sub_Read_Naz_For_New_Issue();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM READ-NAZ-FOR-SWEEP
            sub_Read_Naz_For_Sweep();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  END  OF ADD 4/02
        //* *======================================================================
        //* ***********************************************************************
        //*  READ NAZ FOR NEW ISSUE GET TOTAL DA AMT INTO CONTRACT
        //*            REFLECTS OPENING BALANCE FOR QTRLY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAZ-FOR-NEW-ISSUE
        //*  ADDED FOLLOWING  4/02
        //* ***********************************************************************
        //*  READ NAZ FOR RESETTLEMENT TRANS SWEEPS TO GET DA PREMIUM INTO CONTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAZ-FOR-SWEEP
    }
    private void sub_Read_Naz_For_New_Issue() throws Exception                                                                                                            //Natural: READ-NAZ-FOR-NEW-ISSUE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt.reset();                                                                                                                      //Natural: RESET #O-PMT-AMT #O-SETTLE-AMT-NAZ #O-TOTAL-ACCUM-AMT
        pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz.reset();
        pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt.reset();
        pnd_Ia_Naz_Call_Parms_Pnd_O_Found.setValue("N");                                                                                                                  //Natural: ASSIGN #O-FOUND := 'N'
        pnd_Ia_Naz_Call_Parms_Pnd_O_25_Over_Sw.setValue("N");                                                                                                             //Natural: ASSIGN #O-25-OVER-SW := 'N'
        naz_Key_Naz_Ia_Tiaa_Nbr.setValue(pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz);                                                                                    //Natural: ASSIGN NAZ-IA-TIAA-NBR := #I-IA-CONTRACT-NAZ
        naz_Key_Naz_Ia_Tiaa_Payee_Cde.setValue(1);                                                                                                                        //Natural: ASSIGN NAZ-IA-TIAA-PAYEE-CDE := 01
        naz_Key_Naz_Ia_Rcrd_Cde.setValue("PP");                                                                                                                           //Natural: ASSIGN NAZ-IA-RCRD-CDE := 'PP'
        vw_naz_Ia_Rslt_Ddm.startDatabaseRead                                                                                                                              //Natural: READ NAZ-IA-RSLT-DDM BY NAI-SUPER2 STARTING FROM #NAZ-SUPER
        (
        "RD1",
        new Wc[] { new Wc("NAI_SUPER2", ">=", naz_Key_Pnd_Naz_Super, WcType.BY) },
        new Oc[] { new Oc("NAI_SUPER2", "ASC") }
        );
        RD1:
        while (condition(vw_naz_Ia_Rslt_Ddm.readNextRow("RD1")))
        {
            if (condition(naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Nbr.equals(pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz)))                                                           //Natural: IF NAI-IA-TIAA-NBR = #I-IA-CONTRACT-NAZ
            {
                pnd_Ia_Naz_Call_Parms_Pnd_O_Found.setValue("Y");                                                                                                          //Natural: ASSIGN #O-FOUND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Nbr.greater(pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz)))                                                          //Natural: IF NAI-IA-TIAA-NBR GT #I-IA-CONTRACT-NAZ
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Pe.setValue(naz_Ia_Rslt_Ddm_Count_Castnai_Dtl_Tiaa_Data);                                                                                           //Natural: ASSIGN #COUNT-PE := C*NAI-DTL-TIAA-DATA
            if (condition(naz_Ia_Rslt_Ddm_Nai_Pymnt_Cde.equals("F") || naz_Ia_Rslt_Ddm_Nai_Pymnt_Cde.equals(" ")))                                                        //Natural: IF NAI-PYMNT-CDE = 'F' OR = ' '
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt.compute(new ComputeParameters(false, pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt), pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt.add(naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_I)).add(naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_I))); //Natural: COMPUTE #O-PMT-AMT = #O-PMT-AMT + NAI-DTL-TIAA-STD-GRNTD-AMT ( #I ) + NAI-DTL-TIAA-STD-DVDND-AMT ( #I )
                    pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt.nadd(naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_I));                                            //Natural: ADD NAI-DTL-TIAA-DA-STD-AMT ( #I ) TO #O-TOTAL-ACCUM-AMT
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz.setValue(pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt);                                                         //Natural: ASSIGN #O-SETTLE-AMT-NAZ := #O-TOTAL-ACCUM-AMT
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Pe.setValue(naz_Ia_Rslt_Ddm_Count_Castnai_Dtl_Tiaa_Data);                                                                                           //Natural: ASSIGN #COUNT-PE := C*NAI-DTL-TIAA-DATA
            if (condition(naz_Ia_Rslt_Ddm_Nai_Ia_Rcrd_Cde.equals("PP ") && naz_Ia_Rslt_Ddm_Nai_Sqnce_Nbr.equals(999)))                                                    //Natural: IF NAI-IA-RCRD-CDE = 'PP ' AND NAI-SQNCE-NBR = 999
            {
                FOR02:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt.nadd(naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_I));                                            //Natural: ADD NAI-DTL-TIAA-DA-STD-AMT ( #I ) TO #O-TOTAL-ACCUM-AMT
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz.setValue(pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt);                                                         //Natural: ASSIGN #O-SETTLE-AMT-NAZ := #O-TOTAL-ACCUM-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Naz_For_Sweep() throws Exception                                                                                                                //Natural: READ-NAZ-FOR-SWEEP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Naz_Call_Parms_Pnd_O_Pmt_Amt.reset();                                                                                                                      //Natural: RESET #O-PMT-AMT #O-SETTLE-AMT-NAZ #O-TOTAL-ACCUM-AMT
        pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz.reset();
        pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt.reset();
        pnd_Ia_Naz_Call_Parms_Pnd_O_Found.setValue("N");                                                                                                                  //Natural: ASSIGN #O-FOUND := 'N'
        pnd_Ia_Naz_Call_Parms_Pnd_O_25_Over_Sw.setValue("N");                                                                                                             //Natural: ASSIGN #O-25-OVER-SW := 'N'
        naz_Key_Naz_Ia_Tiaa_Nbr.setValue(pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz);                                                                                    //Natural: ASSIGN NAZ-IA-TIAA-NBR := #I-IA-CONTRACT-NAZ
        naz_Key_Naz_Ia_Tiaa_Payee_Cde.setValue(1);                                                                                                                        //Natural: ASSIGN NAZ-IA-TIAA-PAYEE-CDE := 01
        naz_Key_Naz_Ia_Rcrd_Cde.setValue("RS");                                                                                                                           //Natural: ASSIGN NAZ-IA-RCRD-CDE := 'RS'
        vw_naz_Ia_Rslt_Ddm.startDatabaseRead                                                                                                                              //Natural: READ NAZ-IA-RSLT-DDM BY NAI-SUPER2 STARTING FROM #NAZ-SUPER
        (
        "READ01",
        new Wc[] { new Wc("NAI_SUPER2", ">=", naz_Key_Pnd_Naz_Super, WcType.BY) },
        new Oc[] { new Oc("NAI_SUPER2", "ASC") }
        );
        READ01:
        while (condition(vw_naz_Ia_Rslt_Ddm.readNextRow("READ01")))
        {
            //*  OR= 11 OR = 12)
            if (condition(!(((naz_Ia_Rslt_Ddm_Nai_Sqnce_Nbr.equals(12) || naz_Ia_Rslt_Ddm_Nai_Sqnce_Nbr.equals(22)) && naz_Ia_Rslt_Ddm_Nai_Ia_Rcrd_Cde.equals("RS")))))   //Natural: ACCEPT IF ( NAI-SQNCE-NBR = 12 OR = 22 ) AND NAI-IA-RCRD-CDE = 'RS'
            {
                continue;
            }
            if (condition(naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Nbr.equals(pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz)))                                                           //Natural: IF NAI-IA-TIAA-NBR = #I-IA-CONTRACT-NAZ
            {
                pnd_Ia_Naz_Call_Parms_Pnd_O_Found.setValue("Y");                                                                                                          //Natural: ASSIGN #O-FOUND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(naz_Ia_Rslt_Ddm_Nai_Ia_Tiaa_Nbr.greater(pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz)))                                                          //Natural: IF NAI-IA-TIAA-NBR GT #I-IA-CONTRACT-NAZ
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ia_Naz_Call_Parms_Pnd_O_Total_Accum_Amt.nadd(naz_Ia_Rslt_Ddm_Nai_Dtl_Fnl_Std_Pay_Amt.getValue("*"));                                                      //Natural: ADD NAI-DTL-FNL-STD-PAY-AMT ( * ) TO #O-TOTAL-ACCUM-AMT
            pnd_Ia_Naz_Call_Parms_Pnd_O_Settle_Amt_Naz.nadd(naz_Ia_Rslt_Ddm_Nai_Dtl_Tiaa_Da_Std_Amt.getValue("*"));                                                       //Natural: ADD NAI-DTL-TIAA-DA-STD-AMT ( * ) TO #O-SETTLE-AMT-NAZ
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "************************************",NEWLINE,"ERROR PROCESSING - QUARTERLY 1160IAM",NEWLINE,"************************************",       //Natural: WRITE '************************************' / 'ERROR PROCESSING - QUARTERLY 1160IAM' / '************************************' / 'PROGRAM' *PROGRAM / 'CNTRCT' #I-IA-CONTRACT-NAZ / 'PAYEE' #I-IA-PAYEE-CDE / 'PE CNT' #COUNT-PE / '************************************'
            NEWLINE,"PROGRAM",Global.getPROGRAM(),NEWLINE,"CNTRCT",pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Contract_Naz,NEWLINE,"PAYEE",pnd_Ia_Naz_Call_Parms_Pnd_I_Ia_Payee_Cde,
            NEWLINE,"PE CNT",pnd_Count_Pe,NEWLINE,"************************************");
    };                                                                                                                                                                    //Natural: END-ERROR
}
