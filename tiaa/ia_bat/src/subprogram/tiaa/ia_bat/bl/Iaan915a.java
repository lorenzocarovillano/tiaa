/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:13 AM
**        * FROM NATURAL SUBPROGRAM : Iaan915a
************************************************************
**        * FILE NAME            : Iaan915a.java
**        * CLASS NAME           : Iaan915a
**        * INSTANCE NAME        : Iaan915a
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN915A CREATES MISC NON TAX TRANS SELECTION RCRD*
*      DATE     -  10/94                                             *
*  04/2017 OS PIN EXPANSION - SC 082017 FOR CHANGES.                 *
**********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan915a extends BLNatBase
{
    // Data Areas
    private LdaIaal915a ldaIaal915a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup pnd_Passed_Data;
    private DbsField pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Passed_Data__R_Field_1;
    private DbsField pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8;
    private DbsField pnd_Passed_Data_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Sub_Cde;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_2;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte;

    private DbsGroup pnd_Passed_Data__R_Field_3;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm;
    private DbsField pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Area;
    private DbsField pnd_Passed_Data_Pnd_Trans_User_Id;
    private DbsField pnd_Passed_Data_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Passed_Data_Pnd_Trans_Seq_Nbr;

    private DbsGroup iaa_Cntrct_Bfore;
    private DbsField iaa_Cntrct_Bfore_Trans_Dte;
    private DbsField iaa_Cntrct_Bfore_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Bfore_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Bfore_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Bfore_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Bfore_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Bfore_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Bfore_Aftr_Imge_Id;

    private DbsGroup iaa_Cpr_Bfore;
    private DbsField iaa_Cpr_Bfore_Trans_Dte;
    private DbsField iaa_Cpr_Bfore_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Bfore_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Bfore_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Bfore_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Bfore_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Bfore_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Bfore_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Bfore_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Bfore_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Bfore_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Bfore_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Bfore_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Bfore_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Bfore_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Bfore_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Bfore_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Bfore_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Bfore_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Bfore_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Bfore_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Bfore_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Bfore_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Bfore_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Bfore_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Bfore_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Bfore_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Bfore_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Bfore_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Bfore_Trans_Check_Dte;
    private DbsField iaa_Cpr_Bfore_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Bfore_Aftr_Imge_Id;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal915a = new LdaIaal915a();
        registerRecord(ldaIaal915a);
        registerRecord(ldaIaal915a.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal915a.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal915a.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();

        pnd_Passed_Data = parameters.newGroupInRecord("pnd_Passed_Data", "#PASSED-DATA");
        pnd_Passed_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte", "#CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Passed_Data__R_Field_1 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_1", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);
        pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8 = pnd_Passed_Data__R_Field_1.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8", "#TRANS-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Trans_Payee_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Passed_Data_Pnd_Trans_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Passed_Data_Pnd_Trans_Sub_Cde = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Sub_Cde", "#TRANS-SUB-CDE", FieldType.STRING, 3);
        pnd_Passed_Data_Pnd_Trans_Check_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_2 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_2", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Check_Dte);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Cc", "#TRANS-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy", "#TRANS-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm", "#TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd = pnd_Passed_Data__R_Field_2.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd", "#TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Passed_Data__R_Field_3 = pnd_Passed_Data.newGroupInGroup("pnd_Passed_Data__R_Field_3", "REDEFINE", pnd_Passed_Data_Pnd_Trans_Effctve_Dte);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Cc", "#TRANS-EFFCTVE-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy", "#TRANS-EFFCTVE-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm", "#TRANS-EFFCTVE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd = pnd_Passed_Data__R_Field_3.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Dd", "#TRANS-EFFCTVE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Passed_Data_Pnd_Trans_User_Area = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Area", "#TRANS-USER-AREA", FieldType.STRING, 
            6);
        pnd_Passed_Data_Pnd_Trans_User_Id = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_User_Id", "#TRANS-USER-ID", FieldType.STRING, 8);
        pnd_Passed_Data_Pnd_Invrse_Trans_Dte = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12);
        pnd_Passed_Data_Pnd_Trans_Seq_Nbr = pnd_Passed_Data.newFieldInGroup("pnd_Passed_Data_Pnd_Trans_Seq_Nbr", "#TRANS-SEQ-NBR", FieldType.NUMERIC, 
            4);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        iaa_Cntrct_Bfore = localVariables.newGroupInRecord("iaa_Cntrct_Bfore", "IAA-CNTRCT-BFORE");
        iaa_Cntrct_Bfore_Trans_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Trans_Dte", "TRANS-DTE", FieldType.TIME);
        iaa_Cntrct_Bfore_Invrse_Trans_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12);
        iaa_Cntrct_Bfore_Lst_Trans_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME);
        iaa_Cntrct_Bfore_Cntrct_Ppcn_Nbr = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        iaa_Cntrct_Bfore_Cntrct_Optn_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2);
        iaa_Cntrct_Bfore_Cntrct_Orgn_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2);
        iaa_Cntrct_Bfore_Cntrct_Acctng_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2);
        iaa_Cntrct_Bfore_Cntrct_Issue_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6);
        iaa_Cntrct_Bfore_Cntrct_First_Pymnt_Due_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        iaa_Cntrct_Bfore_Cntrct_First_Pymnt_Pd_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6);
        iaa_Cntrct_Bfore_Cntrct_Crrncy_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_Bfore_Cntrct_Type_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1);
        iaa_Cntrct_Bfore_Cntrct_Pymnt_Mthd = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1);
        iaa_Cntrct_Bfore_Cntrct_Pnsn_Pln_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1);
        iaa_Cntrct_Bfore_Cntrct_Joint_Cnvrt_Rcrd_Ind = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1);
        iaa_Cntrct_Bfore_Cntrct_Orig_Da_Cntrct_Nbr = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        iaa_Cntrct_Bfore_Cntrct_Rsdncy_At_Issue_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3);
        iaa_Cntrct_Bfore_Cntrct_First_Annt_Xref_Ind = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        iaa_Cntrct_Bfore_Cntrct_First_Annt_Dob_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        iaa_Cntrct_Bfore_Cntrct_First_Annt_Mrtlty_Yob_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        iaa_Cntrct_Bfore_Cntrct_First_Annt_Sex_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        iaa_Cntrct_Bfore_Cntrct_First_Annt_Lfe_Cnt = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1);
        iaa_Cntrct_Bfore_Cntrct_First_Annt_Dod_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6);
        iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Xref_Ind = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dob_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Sex_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dod_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6);
        iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Life_Cnt = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Ssn = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        iaa_Cntrct_Bfore_Cntrct_Div_Payee_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1);
        iaa_Cntrct_Bfore_Cntrct_Div_Coll_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5);
        iaa_Cntrct_Bfore_Cntrct_Inst_Iss_Cde = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5);
        iaa_Cntrct_Bfore_Trans_Check_Dte = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);
        iaa_Cntrct_Bfore_Bfre_Imge_Id = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1);
        iaa_Cntrct_Bfore_Aftr_Imge_Id = iaa_Cntrct_Bfore.newFieldInGroup("iaa_Cntrct_Bfore_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1);

        iaa_Cpr_Bfore = localVariables.newGroupInRecord("iaa_Cpr_Bfore", "IAA-CPR-BFORE");
        iaa_Cpr_Bfore_Trans_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Trans_Dte", "TRANS-DTE", FieldType.TIME);
        iaa_Cpr_Bfore_Invrse_Trans_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        iaa_Cpr_Bfore_Lst_Trans_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME);
        iaa_Cpr_Bfore_Cntrct_Part_Ppcn_Nbr = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10);
        iaa_Cpr_Bfore_Cntrct_Part_Payee_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        iaa_Cpr_Bfore_Cpr_Id_Nbr = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12);
        iaa_Cpr_Bfore_Prtcpnt_Ctznshp_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3);
        iaa_Cpr_Bfore_Prtcpnt_Rsdncy_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3);
        iaa_Cpr_Bfore_Prtcpnt_Rsdncy_Sw = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Prtcpnt_Tax_Id_Nbr = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9);
        iaa_Cpr_Bfore_Prtcpnt_Tax_Id_Typ = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Cntrct_Actvty_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1);
        iaa_Cpr_Bfore_Cntrct_Trmnte_Rsn = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2);
        iaa_Cpr_Bfore_Cntrct_Rwrttn_Ind = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1);
        iaa_Cpr_Bfore_Cntrct_Cash_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Cntrct_Emplymnt_Trmnt_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1);
        iaa_Cpr_Bfore_Cntrct_Company_Cd = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", FieldType.STRING, 
            1, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Per_Ivc_Amt = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Ivc_Amt = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Ivc_Used_Amt = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Rtb_Amt = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Rtb_Percent = iaa_Cpr_Bfore.newFieldArrayInGroup("iaa_Cpr_Bfore_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 
            7, 4, new DbsArrayController(1, 5));
        iaa_Cpr_Bfore_Cntrct_Mode_Ind = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3);
        iaa_Cpr_Bfore_Cntrct_Wthdrwl_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6);
        iaa_Cpr_Bfore_Cntrct_Final_Per_Pay_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6);
        iaa_Cpr_Bfore_Cntrct_Final_Pay_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 
            8);
        iaa_Cpr_Bfore_Bnfcry_Xref_Ind = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9);
        iaa_Cpr_Bfore_Bnfcry_Dod_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6);
        iaa_Cpr_Bfore_Cntrct_Pend_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Cntrct_Hold_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Cntrct_Pend_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6);
        iaa_Cpr_Bfore_Cntrct_Prev_Dist_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 
            4);
        iaa_Cpr_Bfore_Cntrct_Curr_Dist_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 
            4);
        iaa_Cpr_Bfore_Cntrct_Cmbne_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 12);
        iaa_Cpr_Bfore_Cntrct_Spirt_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Cntrct_Spirt_Amt = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        iaa_Cpr_Bfore_Cntrct_Spirt_Srce = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Cntrct_Spirt_Arr_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 
            4);
        iaa_Cpr_Bfore_Cntrct_Spirt_Prcss_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6);
        iaa_Cpr_Bfore_Cntrct_Fed_Tax_Amt = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        iaa_Cpr_Bfore_Cntrct_State_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 3);
        iaa_Cpr_Bfore_Cntrct_State_Tax_Amt = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        iaa_Cpr_Bfore_Cntrct_Local_Cde = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 3);
        iaa_Cpr_Bfore_Cntrct_Local_Tax_Amt = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        iaa_Cpr_Bfore_Cntrct_Lst_Chnge_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 
            6);
        iaa_Cpr_Bfore_Trans_Check_Dte = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8);
        iaa_Cpr_Bfore_Bfre_Imge_Id = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1);
        iaa_Cpr_Bfore_Aftr_Imge_Id = iaa_Cpr_Bfore.newFieldInGroup("iaa_Cpr_Bfore_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal915a.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan915a() throws Exception
    {
        super("Iaan915a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *ITE '=' *PROGRAM
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: DECIDE ON FIRST VALUE OF #TRANS-CDE
        short decideConditionsMet359 = 0;                                                                                                                                 //Natural: VALUE 037
        if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(37))))
        {
            decideConditionsMet359++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-037
            sub_Determine_Intent_Cde_037();
            if (condition(Global.isEscape())) {return;}
            //*    IF #INTENT-CODE = ' '
            //*      ESCAPE ROUTINE
            //*    END-IF
                                                                                                                                                                          //Natural: PERFORM GET-ANNT-INFO
            sub_Get_Annt_Info();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 066
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(66))))
        {
            decideConditionsMet359++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-064-066-070
            sub_Determine_Intent_Cde_064_066_070();
            if (condition(Global.isEscape())) {return;}
            //*    IF #INTENT-CODE = ' '
            //*      ESCAPE ROUTINE
            //*    END-IF
                                                                                                                                                                          //Natural: PERFORM GET-ANNT-INFO
            sub_Get_Annt_Info();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(102))))
        {
            decideConditionsMet359++;
            if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.equals("066")))                                                                                               //Natural: IF #TRANS-SUB-CDE = '066'
            {
                ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                         //Natural: ASSIGN #CPR-AFTR-KEY.#AFTR-IMGE-ID := '2'
                ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                  //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
                ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
                ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                    //Natural: ASSIGN #CPR-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
                ldaIaal915a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                       //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
                (
                "READ01",
                new Wc[] { new Wc("CPR_AFTR_KEY", ">=", ldaIaal915a.getPnd_Cpr_Aftr_Key(), WcType.BY) },
                new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
                1
                );
                READ01:
                while (condition(ldaIaal915a.getVw_iaa_Cpr_Trans().readNextRow("READ01")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Pend_Cde().equals("0")))                                                                                //Natural: IF IAA-CPR-TRANS.CNTRCT-PEND-CDE = '0'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                //Natural: ASSIGN #INTENT-CODE := '1'
                    ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde().setValue(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Pend_Cde());                               //Natural: ASSIGN #SUS-PYMNT-RSN-CDE := IAA-CPR-TRANS.CNTRCT-PEND-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-102
                sub_Determine_Intent_Cde_102();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Passed_Data_Pnd_Trans_Sub_Cde.notEquals("066")))                                                                                        //Natural: IF #TRANS-SUB-CDE NE '066'
                {
                    if (condition(ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().equals("3")))                                                                   //Natural: IF #INTENT-CODE = '3'
                    {
                        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde().setValue(" ");                                                                      //Natural: ASSIGN #SUS-PYMNT-RSN-CDE := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde().setValue(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Pend_Cde());                           //Natural: ASSIGN #SUS-PYMNT-RSN-CDE := IAA-CPR-TRANS.CNTRCT-PEND-CDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 104
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(104))))
        {
            decideConditionsMet359++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTENT-CDE-104
            sub_Determine_Intent_Cde_104();
            if (condition(Global.isEscape())) {return;}
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde().setValue(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Hold_Cde());                                      //Natural: ASSIGN #HOLD-CHECK-RSN-CDE := IAA-CPR-TRANS.CNTRCT-HOLD-CDE
            //*    IF #INTENT-CODE = ' '
            //*      ESCAPE ROUTINE
            //*    END-IF
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((pnd_Passed_Data_Pnd_Trans_Cde.equals(106))))
        {
            decideConditionsMet359++;
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                             //Natural: ASSIGN #CPR-AFTR-KEY.#AFTR-IMGE-ID := '2'
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                      //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                        //Natural: ASSIGN #CPR-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
            ldaIaal915a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
            (
            "READ02",
            new Wc[] { new Wc("CPR_AFTR_KEY", ">=", ldaIaal915a.getPnd_Cpr_Aftr_Key(), WcType.BY) },
            new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(ldaIaal915a.getVw_iaa_Cpr_Trans().readNextRow("READ02")))
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res().setValue(ldaIaal915a.getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde());                                  //Natural: ASSIGN #STATE-CNTRY-RES := IAA-CPR-TRANS.PRTCPNT-RSDNCY-CDE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm().equals(getZero())))                                                             //Natural: IF #EFF-DTE-CHNG-RES-MM = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A().setValue(" ");                                                                             //Natural: ASSIGN #EFF-DTE-CHNG-RES-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm().setValue(pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Mm);                                       //Natural: ASSIGN #EFF-DTE-CHNG-RES-MM := #TRANS-EFFCTVE-DTE-MM
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy().setValue(pnd_Passed_Data_Pnd_Trans_Effctve_Dte_Yy);                                       //Natural: ASSIGN #EFF-DTE-CHNG-RES-YY := #TRANS-EFFCTVE-DTE-YY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getWorkFiles().write(1, false, ldaIaal915a.getPnd_Misc_Non_Tax_Trans());                                                                                          //Natural: WRITE WORK FILE 1 #MISC-NON-TAX-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CROSS-REFERENCE-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-037
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-064-066-070
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-102
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTENT-CDE-104
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ANNT-INFO
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915a.getPnd_Misc_Non_Tax_Trans().reset();                                                                                                                  //Natural: RESET #MISC-NON-TAX-TRANS #NO-CNTRCT-REC
        ldaIaal915a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().reset();
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Mm);                                                        //Natural: ASSIGN #CHECK-DTE-MM := #TRANS-CHECK-DTE-MM
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Dd);                                                        //Natural: ASSIGN #CHECK-DTE-DD := #TRANS-CHECK-DTE-DD
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy().setValue(pnd_Passed_Data_Pnd_Trans_Check_Dte_Yy);                                                        //Natural: ASSIGN #CHECK-DTE-YY := #TRANS-CHECK-DTE-YY
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr_8);                                                            //Natural: ASSIGN #CNTRCT-NBR := #TRANS-PPCN-NBR-8
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Record_Status().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                          //Natural: ASSIGN #RECORD-STATUS := #TRANS-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT
        sub_Find_Iaa_Cntrct();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CROSS-REFERENCE-NBR
        sub_Get_Cross_Reference_Nbr();
        if (condition(Global.isEscape())) {return;}
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Cde);                                                                    //Natural: ASSIGN #TRANS-NBR := #TRANS-CDE
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte().setValueEdited(pnd_Passed_Data_Pnd_Trans_Dte,new ReportEditMask("YYYYMMDD"));                               //Natural: MOVE EDITED #PASSED-DATA.#TRANS-DTE ( EM = YYYYMMDD ) TO #MISC-NON-TAX-TRANS.#TRANS-DTE
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_User_Area().setValue(pnd_Passed_Data_Pnd_Trans_User_Area);                                                              //Natural: ASSIGN #USER-AREA := #TRANS-USER-AREA
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_User_Id().setValue(pnd_Passed_Data_Pnd_Trans_User_Id);                                                                  //Natural: ASSIGN #USER-ID := #TRANS-USER-ID
        ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Seq_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Seq_Nbr);                                                                  //Natural: ASSIGN #SEQ-NBR := #TRANS-SEQ-NBR
    }
    private void sub_Find_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: FIND-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915a.getPnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                              //Natural: ASSIGN #CNTRCT-AFTR-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal915a.getPnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915a.getPnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                         //Natural: ASSIGN #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
        ldaIaal915a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #CNTRCT-AFTR-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", ldaIaal915a.getPnd_Cntrct_Aftr_Key(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(ldaIaal915a.getVw_iaa_Cntrct_Trans().readNextRow("READ03")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr().notEquals(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr)))                                                   //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR NE #TRANS-PPCN-NBR
        {
            ldaIaal915a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(true);                                                                                      //Natural: ASSIGN #NO-CNTRCT-REC := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal915a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().setValue(false);                                                                                     //Natural: ASSIGN #NO-CNTRCT-REC := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Cross_Reference_Nbr() throws Exception                                                                                                           //Natural: GET-CROSS-REFERENCE-NBR
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet481 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TRANS-PAYEE-CDE;//Natural: VALUE 01
        if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(1))))
        {
            decideConditionsMet481++;
            if (condition(ldaIaal915a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Passed_Data_Pnd_Trans_Payee_Cde.equals(2))))
        {
            decideConditionsMet481++;
            if (condition(ldaIaal915a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                         //Natural: IF #NO-CNTRCT-REC
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                 //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().equals(getZero())))                                                                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                          //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE GE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE
                && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().greaterOrEqual(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte())))
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind());                         //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().notEquals(getZero()) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().notEquals(getZero())  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE NE 0 AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE GE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE
                && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().greaterOrEqual(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte())))
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind());                          //Natural: ASSIGN #CROSS-REF-NBR := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-XREF-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03 : 99
        else if (condition(((pnd_Passed_Data_Pnd_Trans_Payee_Cde.greaterOrEqual(3) && pnd_Passed_Data_Pnd_Trans_Payee_Cde.lessOrEqual(99)))))
        {
            decideConditionsMet481++;
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                             //Natural: ASSIGN #CPR-AFTR-KEY.#AFTR-IMGE-ID := '2'
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                      //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                    //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
            ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                        //Natural: ASSIGN #CPR-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
            ldaIaal915a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
            (
            "READ04",
            new Wc[] { new Wc("CPR_AFTR_KEY", ">=", ldaIaal915a.getPnd_Cpr_Aftr_Key(), WcType.BY) },
            new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
            1
            );
            READ04:
            while (condition(ldaIaal915a.getVw_iaa_Cpr_Trans().readNextRow("READ04")))
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr().setValue(ldaIaal915a.getIaa_Cpr_Trans_Bnfcry_Xref_Ind());                                       //Natural: ASSIGN #CROSS-REF-NBR := IAA-CPR-TRANS.BNFCRY-XREF-IND
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Intent_Cde_037() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-037
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915a.getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imgr_Id().setValue("2");                                                                                              //Natural: ASSIGN #CNTRCT-BFRE-KEY.#BFRE-IMGR-ID := '2'
        ldaIaal915a.getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915a.getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Trans_Dte);                                                                       //Natural: ASSIGN #CNTRCT-BFRE-KEY.#TRANS-DTE := #PASSED-DATA.#TRANS-DTE
        ldaIaal915a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
        (
        "READ05",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", ldaIaal915a.getPnd_Cntrct_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        READ05:
        while (condition(ldaIaal915a.getVw_iaa_Cntrct_Trans().readNextRow("READ05")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        iaa_Cntrct_Bfore.setValuesByName(ldaIaal915a.getVw_iaa_Cntrct_Trans());                                                                                           //Natural: MOVE BY NAME IAA-CNTRCT-TRANS TO IAA-CNTRCT-BFORE
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte().equals(iaa_Cntrct_Bfore_Cntrct_First_Annt_Dob_Dte) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde().equals(iaa_Cntrct_Bfore_Cntrct_First_Annt_Sex_Cde)  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE = IAA-CNTRCT-BFORE.CNTRCT-FIRST-ANNT-DOB-DTE AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE = IAA-CNTRCT-BFORE.CNTRCT-FIRST-ANNT-SEX-CDE AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = IAA-CNTRCT-BFORE.CNTRCT-FIRST-ANNT-DOD-DTE AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE = IAA-CNTRCT-BFORE.CNTRCT-SCND-ANNT-DOB-DTE AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE = IAA-CNTRCT-BFORE.CNTRCT-SCND-ANNT-SEX-CDE AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE = IAA-CNTRCT-BFORE.CNTRCT-SCND-ANNT-DOD-DTE
            && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().equals(iaa_Cntrct_Bfore_Cntrct_First_Annt_Dod_Dte) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte().equals(iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dob_Dte) 
            && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde().equals(iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Sex_Cde) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().equals(iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dod_Dte)))
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                        //Natural: ASSIGN #INTENT-CODE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte().equals(iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dob_Dte) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde().equals(iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Sex_Cde)  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE = IAA-CNTRCT-BFORE.CNTRCT-SCND-ANNT-DOB-DTE AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE = IAA-CNTRCT-BFORE.CNTRCT-SCND-ANNT-SEX-CDE AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE = IAA-CNTRCT-BFORE.CNTRCT-SCND-ANNT-DOD-DTE
            && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().equals(iaa_Cntrct_Bfore_Cntrct_Scnd_Annt_Dod_Dte)))
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                        //Natural: ASSIGN #INTENT-CODE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte().equals(iaa_Cntrct_Bfore_Cntrct_First_Annt_Dob_Dte) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde().equals(iaa_Cntrct_Bfore_Cntrct_First_Annt_Sex_Cde)  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE = IAA-CNTRCT-BFORE.CNTRCT-FIRST-ANNT-DOB-DTE AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE = IAA-CNTRCT-BFORE.CNTRCT-FIRST-ANNT-SEX-CDE AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = IAA-CNTRCT-BFORE.CNTRCT-FIRST-ANNT-DOD-DTE
            && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().equals(iaa_Cntrct_Bfore_Cntrct_First_Annt_Dod_Dte)))
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().equals(iaa_Cntrct_Bfore_Cntrct_First_Annt_Dod_Dte)))                                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = IAA-CNTRCT-BFORE.CNTRCT-FIRST-ANNT-DOD-DTE
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Intent_Cde_064_066_070() throws Exception                                                                                                  //Natural: DETERMINE-INTENT-CDE-064-066-070
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915a.getPnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                              //Natural: ASSIGN #CNTRCT-AFTR-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal915a.getPnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915a.getPnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                         //Natural: ASSIGN #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
        ldaIaal915a.getVw_iaa_Cntrct_Trans().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #CNTRCT-AFTR-KEY
        (
        "READ06",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", ldaIaal915a.getPnd_Cntrct_Aftr_Key(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
        1
        );
        READ06:
        while (condition(ldaIaal915a.getVw_iaa_Cntrct_Trans().readNextRow("READ06")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().greater(getZero()) && ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().greater(getZero()))) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE GT 0 AND IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE GT 0
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                        //Natural: ASSIGN #INTENT-CODE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().greater(getZero())))                                                                    //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE GT 0
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte().greater(getZero())))                                                                     //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE GT 0
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                        //Natural: ASSIGN #INTENT-CODE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Intent_Cde_102() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-102
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN #CPR-AFTR-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                          //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                        //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                            //Natural: ASSIGN #CPR-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
        ldaIaal915a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
        (
        "READ07",
        new Wc[] { new Wc("CPR_AFTR_KEY", ">=", ldaIaal915a.getPnd_Cpr_Aftr_Key(), WcType.BY) },
        new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
        1
        );
        READ07:
        while (condition(ldaIaal915a.getVw_iaa_Cpr_Trans().readNextRow("READ07")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal915a.getPnd_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                    //Natural: ASSIGN #CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915a.getPnd_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                  //Natural: ASSIGN #CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal915a.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PRTCPNT-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal915a.getPnd_Cntrct_Prtcpnt_Key(), WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal915a.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND01", true)))
        {
            ldaIaal915a.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal915a.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO PRTCPNT ROLE FOUND FOR: ",ldaIaal915a.getPnd_Cntrct_Prtcpnt_Key());                                                             //Natural: WRITE 'NO PRTCPNT ROLE FOUND FOR: ' #CNTRCT-PRTCPNT-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaIaal915a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().notEquals("0") && ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Pend_Cde().equals("0")))             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE NE '0' AND IAA-CPR-TRANS.CNTRCT-PEND-CDE = '0'
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().equals("0") && ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Pend_Cde().notEquals("0")))             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE = '0' AND IAA-CPR-TRANS.CNTRCT-PEND-CDE NE '0'
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                        //Natural: ASSIGN #INTENT-CODE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().notEquals(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Pend_Cde())))                                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE NE IAA-CPR-TRANS.CNTRCT-PEND-CDE
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                        //Natural: ASSIGN #INTENT-CODE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Intent_Cde_104() throws Exception                                                                                                          //Natural: DETERMINE-INTENT-CDE-104
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN #CPR-AFTR-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                          //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                        //Natural: ASSIGN #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal915a.getPnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Invrse_Trans_Dte);                                                            //Natural: ASSIGN #CPR-AFTR-KEY.#INVRSE-TRANS-DTE := #PASSED-DATA.#INVRSE-TRANS-DTE
        ldaIaal915a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
        (
        "READ08",
        new Wc[] { new Wc("CPR_AFTR_KEY", ">=", ldaIaal915a.getPnd_Cpr_Aftr_Key(), WcType.BY) },
        new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
        1
        );
        READ08:
        while (condition(ldaIaal915a.getVw_iaa_Cpr_Trans().readNextRow("READ08")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal915a.getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imgr_Id().setValue("1");                                                                                                 //Natural: ASSIGN #CPR-BFRE-KEY.#BFRE-IMGR-ID := '1'
        ldaIaal915a.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(pnd_Passed_Data_Pnd_Trans_Ppcn_Nbr);                                                          //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR := #TRANS-PPCN-NBR
        ldaIaal915a.getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(pnd_Passed_Data_Pnd_Trans_Payee_Cde);                                                        //Natural: ASSIGN #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE := #TRANS-PAYEE-CDE
        ldaIaal915a.getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte().setValue(pnd_Passed_Data_Pnd_Cntrl_Frst_Trans_Dte);                                                               //Natural: ASSIGN #CPR-BFRE-KEY.#TRANS-DTE := #CNTRL-FRST-TRANS-DTE
        ldaIaal915a.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
        (
        "READ09",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal915a.getPnd_Cpr_Bfre_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ09:
        while (condition(ldaIaal915a.getVw_iaa_Cpr_Trans().readNextRow("READ09")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Hold_Cde().notEquals("0") && ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Hold_Cde().equals("0")))                       //Natural: IF IAA-CPR-TRANS.CNTRCT-HOLD-CDE NE '0' AND IAA-CPR-TRANS.CNTRCT-HOLD-CDE = '0'
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("1");                                                                                        //Natural: ASSIGN #INTENT-CODE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Hold_Cde().equals("0") && ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Hold_Cde().notEquals("0")))                       //Natural: IF IAA-CPR-TRANS.CNTRCT-HOLD-CDE = '0' AND IAA-CPR-TRANS.CNTRCT-HOLD-CDE NE '0'
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("3");                                                                                        //Natural: ASSIGN #INTENT-CODE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Hold_Cde().notEquals(ldaIaal915a.getIaa_Cpr_Trans_Cntrct_Hold_Cde())))                                          //Natural: IF IAA-CPR-TRANS.CNTRCT-HOLD-CDE NE IAA-CPR-TRANS.CNTRCT-HOLD-CDE
        {
            ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code().setValue("2");                                                                                        //Natural: ASSIGN #INTENT-CODE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Annt_Info() throws Exception                                                                                                                     //Natural: GET-ANNT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal915a.getPnd_Logical_Variables_Pnd_No_Cntrct_Rec().getBoolean()))                                                                             //Natural: IF #NO-CNTRCT-REC
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde().equals(getZero())))                                                                 //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-SEX-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde());                           //Natural: ASSIGN #1ST-ANNT-SEX := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte().equals(getZero())))                                                                 //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-DOB-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Mm());                         //Natural: ASSIGN #1ST-ANNT-DOB-MM := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-MM
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dd());                         //Natural: ASSIGN #1ST-ANNT-DOB-DD := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DD
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Yy());                         //Natural: ASSIGN #1ST-ANNT-DOB-YY := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                 //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A().setValue(" ");                                                                                 //Natural: ASSIGN #1ST-ANNT-DOD-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Mm());                         //Natural: ASSIGN #1ST-ANNT-DOD-MM := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-MM
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Yy());                         //Natural: ASSIGN #1ST-ANNT-DOD-YY := IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde().equals(getZero())))                                                                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-SEX-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde());                            //Natural: ASSIGN #2ND-ANNT-SEX := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte().equals(getZero())))                                                                  //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-DOB-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Mm());                          //Natural: ASSIGN #2ND-ANNT-DOB-MM := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-MM
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dd());                          //Natural: ASSIGN #2ND-ANNT-DOB-DD := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DD
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Yy());                          //Natural: ASSIGN #2ND-ANNT-DOB-YY := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-YY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte().equals(getZero())))                                                                 //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A().setValue(" ");                                                                                 //Natural: ASSIGN #2ND-ANNT-DOD-A := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Mm());                          //Natural: ASSIGN #2ND-ANNT-DOD-MM := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-MM
                ldaIaal915a.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy().setValue(ldaIaal915a.getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Yy());                          //Natural: ASSIGN #2ND-ANNT-DOD-YY := IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-YY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
