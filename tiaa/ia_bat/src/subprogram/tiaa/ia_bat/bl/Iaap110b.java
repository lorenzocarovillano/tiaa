/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:44 AM
**        * FROM NATURAL SUBPROGRAM : Iaap110b
************************************************************
**        * FILE NAME            : Iaap110b.java
**        * CLASS NAME           : Iaap110b
**        * INSTANCE NAME        : Iaap110b
************************************************************
***********************************************************************
*                                                                     *
*   PROGRAM     -  IAAP110B    PRINT ROLLOVER MODE REPORTS CALLED BY  *
*      DATE     -  09/01       IAAP110A                               *
*    AUTHOR     -  R.M                                                *
*                                                                     *
*                                                                     *
*   HISTORY     -                                                     *
*                                                                     *
***********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap110b extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Fmt3_Tiaa_Table_Tot;
    private DbsField pnd_Fmt3_Tiaa_Table;
    private DbsField pnd_Fmt5_Tiaa_Table_Tot;
    private DbsField pnd_Fmt5_Tiaa_Table;
    private DbsField pnd_Fmt6_Tiaa_Table_Tot;
    private DbsField pnd_Fmt6_Tiaa_Table;
    private DbsField pnd_Fmt7_Tiaa_Table_Tot;
    private DbsField pnd_Fmt7_Tiaa_Table;
    private DbsField pnd_Fmt8_Tiaa_Table_Tot;
    private DbsField pnd_Fmt8_Tiaa_Table;
    private DbsField pnd_Fmt9_Tiaa_Table_Tot;
    private DbsField pnd_Fmt9_Tiaa_Table;
    private DbsField pnd_Fmt10_Tiaa_Table_Tot;
    private DbsField pnd_Fmt10_Tiaa_Table;
    private DbsField pnd_Fmt11_Tiaa_Table_Tot;
    private DbsField pnd_Fmt11_Tiaa_Table;
    private DbsField pnd_Fmt7_Units_Tot;
    private DbsField pnd_Fmt7_Units;
    private DbsField pnd_Fmt7_Dollars;
    private DbsField pnd_Fmt7_Dollars_Tot;
    private DbsField pnd_Pay_Mode_Table;
    private DbsField pnd_Unit_Text_Table;
    private DbsField pnd_Dollars_Text_Table;
    private DbsField pnd_Tpa_Rinv_Cntrcts;
    private DbsField pnd_Tpa_Roll_Cntrcts;
    private DbsField pnd_Ipr_Roll_Cntrcts;
    private DbsField pnd_P_I_Roll_Cntrcts;
    private DbsField pnd_Ac_Roll_Cntrcts;
    private DbsField pnd_U;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy;
    private DbsField pnd_Z;
    private DbsField pnd_Y;
    private DbsField pnd_Tpa_Rinv_Cntrcts_Tot;
    private DbsField pnd_Tpa_Roll_Cntrcts_Tot;
    private DbsField pnd_Ipr_Roll_Cntrcts_Tot;
    private DbsField pnd_P_I_Roll_Cntrcts_Tot;
    private DbsField pnd_Ac_Roll_Cntrcts_Tot;
    private DbsField pnd_Heading1;
    private DbsField pnd_Debug;
    private DbsField pnd_Rollover_Title;
    private DbsField pnd_Print_Tpa_Ipro_Pi;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Fmt3_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt3_Tiaa_Table_Tot", "#FMT3-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt3_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt3_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt3_Tiaa_Table", "#FMT3-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt3_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt5_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt5_Tiaa_Table_Tot", "#FMT5-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt5_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt5_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt5_Tiaa_Table", "#FMT5-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt5_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt6_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt6_Tiaa_Table_Tot", "#FMT6-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt6_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt6_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt6_Tiaa_Table", "#FMT6-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt6_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt7_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt7_Tiaa_Table_Tot", "#FMT7-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt7_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt7_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt7_Tiaa_Table", "#FMT7-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt7_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt8_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt8_Tiaa_Table_Tot", "#FMT8-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt8_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt8_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt8_Tiaa_Table", "#FMT8-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt8_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt9_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt9_Tiaa_Table_Tot", "#FMT9-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt9_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt9_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt9_Tiaa_Table", "#FMT9-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt9_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt10_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt10_Tiaa_Table_Tot", "#FMT10-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt10_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt10_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt10_Tiaa_Table", "#FMT10-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt10_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt11_Tiaa_Table_Tot = parameters.newFieldInRecord("pnd_Fmt11_Tiaa_Table_Tot", "#FMT11-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt11_Tiaa_Table_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt11_Tiaa_Table = parameters.newFieldArrayInRecord("pnd_Fmt11_Tiaa_Table", "#FMT11-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt11_Tiaa_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt7_Units_Tot = parameters.newFieldInRecord("pnd_Fmt7_Units_Tot", "#FMT7-UNITS-TOT", FieldType.NUMERIC, 11, 3);
        pnd_Fmt7_Units_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt7_Units = parameters.newFieldArrayInRecord("pnd_Fmt7_Units", "#FMT7-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20, 1, 
            4));
        pnd_Fmt7_Units.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt7_Dollars = parameters.newFieldArrayInRecord("pnd_Fmt7_Dollars", "#FMT7-DOLLARS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20, 
            1, 4));
        pnd_Fmt7_Dollars.setParameterOption(ParameterOption.ByReference);
        pnd_Fmt7_Dollars_Tot = parameters.newFieldInRecord("pnd_Fmt7_Dollars_Tot", "#FMT7-DOLLARS-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt7_Dollars_Tot.setParameterOption(ParameterOption.ByReference);
        pnd_Pay_Mode_Table = parameters.newFieldArrayInRecord("pnd_Pay_Mode_Table", "#PAY-MODE-TABLE", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            4));
        pnd_Pay_Mode_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Unit_Text_Table = parameters.newFieldArrayInRecord("pnd_Unit_Text_Table", "#UNIT-TEXT-TABLE", FieldType.STRING, 24, new DbsArrayController(1, 
            20));
        pnd_Unit_Text_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Dollars_Text_Table = parameters.newFieldArrayInRecord("pnd_Dollars_Text_Table", "#DOLLARS-TEXT-TABLE", FieldType.STRING, 24, new DbsArrayController(1, 
            20));
        pnd_Dollars_Text_Table.setParameterOption(ParameterOption.ByReference);
        pnd_Tpa_Rinv_Cntrcts = parameters.newFieldArrayInRecord("pnd_Tpa_Rinv_Cntrcts", "#TPA-RINV-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Tpa_Rinv_Cntrcts.setParameterOption(ParameterOption.ByReference);
        pnd_Tpa_Roll_Cntrcts = parameters.newFieldArrayInRecord("pnd_Tpa_Roll_Cntrcts", "#TPA-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Tpa_Roll_Cntrcts.setParameterOption(ParameterOption.ByReference);
        pnd_Ipr_Roll_Cntrcts = parameters.newFieldArrayInRecord("pnd_Ipr_Roll_Cntrcts", "#IPR-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Ipr_Roll_Cntrcts.setParameterOption(ParameterOption.ByReference);
        pnd_P_I_Roll_Cntrcts = parameters.newFieldArrayInRecord("pnd_P_I_Roll_Cntrcts", "#P-I-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_P_I_Roll_Cntrcts.setParameterOption(ParameterOption.ByReference);
        pnd_Ac_Roll_Cntrcts = parameters.newFieldArrayInRecord("pnd_Ac_Roll_Cntrcts", "#AC-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Ac_Roll_Cntrcts.setParameterOption(ParameterOption.ByReference);
        pnd_U = parameters.newFieldInRecord("pnd_U", "#U", FieldType.NUMERIC, 2);
        pnd_U.setParameterOption(ParameterOption.ByReference);
        pnd_Payment_Due_Dte = parameters.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 10);
        pnd_Payment_Due_Dte.setParameterOption(ParameterOption.ByReference);

        pnd_Payment_Due_Dte__R_Field_1 = parameters.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_1", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy", "#PAYMENT-DUE-YYYY", 
            FieldType.NUMERIC, 4);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 2);
        pnd_Tpa_Rinv_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_Tpa_Rinv_Cntrcts_Tot", "#TPA-RINV-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_Tpa_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_Tpa_Roll_Cntrcts_Tot", "#TPA-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_Ipr_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_Ipr_Roll_Cntrcts_Tot", "#IPR-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_P_I_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_P_I_Roll_Cntrcts_Tot", "#P-I-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_Ac_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_Ac_Roll_Cntrcts_Tot", "#AC-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_Heading1 = localVariables.newFieldInRecord("pnd_Heading1", "#HEADING1", FieldType.STRING, 69);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Rollover_Title = localVariables.newFieldInRecord("pnd_Rollover_Title", "#ROLLOVER-TITLE", FieldType.STRING, 20);
        pnd_Print_Tpa_Ipro_Pi = localVariables.newFieldInRecord("pnd_Print_Tpa_Ipro_Pi", "#PRINT-TPA-IPRO-PI", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Heading1.setInitialValue("IA ADMINISTRATION - ANNUAL REVALUATION MODE CONTROL FOR PAYMENTS DUE ");
        pnd_Debug.setInitialValue(false);
        pnd_Print_Tpa_Ipro_Pi.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaap110b() throws Exception
    {
        super("Iaap110b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP110B", onError);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        setupReports();
        //* *=====================================================================
        //*                     SET AT TOP OF PAGE
        //* *=====================================================================
        //*  PAGE 7
        //*                                                                                                                                                               //Natural: FORMAT ( 7 ) LS = 133 PS = 57
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 7 )
        //* *======================================================================
        //*                    START OF MAIN PROGRAM LOGIC
        //* *======================================================================
        getReports().write(0, "*****************************");                                                                                                           //Natural: WRITE '*****************************'
        if (Global.isEscape()) return;
        getReports().write(0, "  START OF IAAP110B PROGRAM");                                                                                                             //Natural: WRITE '  START OF IAAP110B PROGRAM'
        if (Global.isEscape()) return;
        getReports().write(0, "*****************************");                                                                                                           //Natural: WRITE '*****************************'
        if (Global.isEscape()) return;
        //*   WRITE ROLL & REINV REPORTS
        //* *
        pnd_Rollover_Title.setValue("(AC ROLLOVERS)");                                                                                                                    //Natural: MOVE '(AC ROLLOVERS)' TO #ROLLOVER-TITLE
        getReports().write(7, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 7 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(7, 2);                                                                                                                                          //Natural: SKIP ( 7 ) 2
        pnd_Fmt7_Tiaa_Table_Tot.nadd(pnd_Fmt7_Tiaa_Table.getValue(1,"*"));                                                                                                //Natural: ADD #FMT7-TIAA-TABLE ( 1,* ) TO #FMT7-TIAA-TABLE-TOT
        //*  ADDED FOLLOWING 9/01
        //*  ADD #AC-ROLL-CNTRCTS  (*) TO #AC-ROLL-CNTRCTS-TOT
        //*  WRITE (7) 'AC TIAA ROLL CONTRACTS  '
        //*   7X #AC-ROLL-CNTRCTS(1) (EM=ZZ,ZZZ,ZZZ,ZZ9)
        //*   7X #AC-ROLL-CNTRCTS(2) (EM=ZZ,ZZZ,ZZZ,ZZ9)
        //*   7X #AC-ROLL-CNTRCTS(3) (EM=ZZ,ZZZ,ZZZ,ZZ9)
        //*   7X #AC-ROLL-CNTRCTS(4) (EM=ZZ,ZZZ,ZZZ,ZZ9)
        //*   7X #AC-ROLL-CNTRCTS-TOT  (EM=ZZ,ZZZ,ZZZ,ZZ9)
        //*  END OF ADD  9/01
        getReports().write(7, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 7 ) 'TIAA CONTRACTUAL AMOUNT ' 7X #FMT7-TIAA-TABLE ( 1,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE ( 1,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE ( 1,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE ( 1,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt7_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT7-TIAA-TABLE-TOT
        pnd_Fmt7_Tiaa_Table_Tot.nadd(pnd_Fmt7_Tiaa_Table.getValue(2,"*"));                                                                                                //Natural: ADD #FMT7-TIAA-TABLE ( 2,* ) TO #FMT7-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(13),pnd_Fmt7_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 7 ) 'PERIODIC DIVIDEND ' 13X #FMT7-TIAA-TABLE ( 2,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE ( 2,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE ( 2,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE ( 2,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(7),pnd_Fmt7_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt7_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT7-TIAA-TABLE-TOT
        FY7:                                                                                                                                                              //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(7, 1);                                                                                                                                      //Natural: SKIP ( 7 ) 1
            pnd_Fmt7_Units_Tot.nadd(pnd_Fmt7_Units.getValue(pnd_Y,"*"));                                                                                                  //Natural: ADD #FMT7-UNITS ( #Y,* ) TO #FMT7-UNITS-TOT
            getReports().write(7, ReportOption.NOTITLE,pnd_Unit_Text_Table.getValue(pnd_Y),new ColumnSpacing(7),pnd_Fmt7_Units.getValue(pnd_Y,1), new                     //Natural: WRITE ( 7 ) #UNIT-TEXT-TABLE ( #Y ) 7X #FMT7-UNITS ( #Y,1 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS ( #Y,2 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS ( #Y,3 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS ( #Y,4 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS-TOT ( EM = ZZ,ZZZ,ZZ9.999 )
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt7_Units.getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt7_Units.getValue(pnd_Y,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt7_Units.getValue(pnd_Y,4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt7_Units_Tot, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY7"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY7"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt7_Units_Tot.reset();                                                                                                                                   //Natural: RESET #FMT7-UNITS-TOT
            pnd_Fmt7_Dollars_Tot.nadd(pnd_Fmt7_Dollars.getValue(pnd_Y,"*"));                                                                                              //Natural: ADD #FMT7-DOLLARS ( #Y,* ) TO #FMT7-DOLLARS-TOT
            getReports().write(7, ReportOption.NOTITLE,pnd_Dollars_Text_Table.getValue(pnd_Y),new ColumnSpacing(7),pnd_Fmt7_Dollars.getValue(pnd_Y,1),                    //Natural: WRITE ( 7 ) #DOLLARS-TEXT-TABLE ( #Y ) 7X #FMT7-DOLLARS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Dollars.getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(7),pnd_Fmt7_Dollars.getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Dollars.getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt7_Dollars_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY7"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY7"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt7_Dollars_Tot.reset();                                                                                                                                 //Natural: RESET #FMT7-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   TPA ROLLOVERS
        getReports().newPage(new ReportSpecification(7));                                                                                                                 //Natural: NEWPAGE ( 7 )
        if (condition(Global.isEscape())){return;}
        pnd_Rollover_Title.setValue("(TPA ROLLOVERS)");                                                                                                                   //Natural: MOVE '(TPA ROLLOVERS)' TO #ROLLOVER-TITLE
        getReports().write(7, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 7 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        pnd_Fmt8_Tiaa_Table_Tot.nadd(pnd_Fmt8_Tiaa_Table.getValue(1,"*"));                                                                                                //Natural: ADD #FMT8-TIAA-TABLE ( 1,* ) TO #FMT8-TIAA-TABLE-TOT
        getReports().skip(7, 2);                                                                                                                                          //Natural: SKIP ( 7 ) 2
        getReports().write(7, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 7 ) 'TIAA CONTRACTUAL AMOUNT ' 7X #FMT8-TIAA-TABLE ( 1,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE ( 1,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE ( 1,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE ( 1,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt8_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT8-TIAA-TABLE-TOT
        pnd_Fmt8_Tiaa_Table_Tot.nadd(pnd_Fmt8_Tiaa_Table.getValue(2,"*"));                                                                                                //Natural: ADD #FMT8-TIAA-TABLE ( 2,* ) TO #FMT8-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(13),pnd_Fmt8_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 7 ) 'PERIODIC DIVIDEND ' 13X #FMT8-TIAA-TABLE ( 2,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE ( 2,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE ( 2,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE ( 2,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT8-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(7),pnd_Fmt8_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt8_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tpa_Roll_Cntrcts_Tot.nadd(pnd_Tpa_Roll_Cntrcts.getValue("*"));                                                                                                //Natural: ADD #TPA-ROLL-CNTRCTS ( * ) TO #TPA-ROLL-CNTRCTS-TOT
        getReports().write(7, ReportOption.NOTITLE,"TPA ROLL CONTRACTS      ",new ColumnSpacing(7),pnd_Tpa_Roll_Cntrcts.getValue(1), new ReportEditMask                   //Natural: WRITE ( 7 ) 'TPA ROLL CONTRACTS      ' 7X #TPA-ROLL-CNTRCTS ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-ROLL-CNTRCTS ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-ROLL-CNTRCTS ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-ROLL-CNTRCTS ( 4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-ROLL-CNTRCTS-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
            ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Roll_Cntrcts.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Roll_Cntrcts.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Roll_Cntrcts.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Roll_Cntrcts_Tot, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Fmt8_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT8-TIAA-TABLE-TOT
        getReports().newPage(new ReportSpecification(7));                                                                                                                 //Natural: NEWPAGE ( 7 )
        if (condition(Global.isEscape())){return;}
        //*  IPRO ROLLOVERS
        pnd_Rollover_Title.setValue("(IPRO ROLLOVERS)");                                                                                                                  //Natural: MOVE '(IPRO ROLLOVERS)' TO #ROLLOVER-TITLE
        getReports().write(7, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 7 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(7, 2);                                                                                                                                          //Natural: SKIP ( 7 ) 2
        pnd_Fmt9_Tiaa_Table_Tot.nadd(pnd_Fmt9_Tiaa_Table.getValue(1,"*"));                                                                                                //Natural: ADD #FMT9-TIAA-TABLE ( 1,* ) TO #FMT9-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 7 ) 'TIAA CONTRACTUAL AMOUNT ' 7X #FMT9-TIAA-TABLE ( 1,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE ( 1,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE ( 1,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE ( 1,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt9_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT9-TIAA-TABLE-TOT
        pnd_Fmt9_Tiaa_Table_Tot.nadd(pnd_Fmt9_Tiaa_Table.getValue(2,"*"));                                                                                                //Natural: ADD #FMT9-TIAA-TABLE ( 2,* ) TO #FMT9-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(13),pnd_Fmt9_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 7 ) 'PERIODIC DIVIDEND ' 13X #FMT9-TIAA-TABLE ( 2,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE ( 2,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE ( 2,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE ( 2,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT9-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(7),pnd_Fmt9_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt9_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Ipr_Roll_Cntrcts_Tot.nadd(pnd_Ipr_Roll_Cntrcts.getValue("*"));                                                                                                //Natural: ADD #IPR-ROLL-CNTRCTS ( * ) TO #IPR-ROLL-CNTRCTS-TOT
        getReports().write(7, ReportOption.NOTITLE,"IPRO ROLL CONTRACTS     ",new ColumnSpacing(7),pnd_Ipr_Roll_Cntrcts.getValue(1), new ReportEditMask                   //Natural: WRITE ( 7 ) 'IPRO ROLL CONTRACTS     ' 7X #IPR-ROLL-CNTRCTS ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #IPR-ROLL-CNTRCTS ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #IPR-ROLL-CNTRCTS ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #IPR-ROLL-CNTRCTS ( 4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #IPR-ROLL-CNTRCTS-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
            ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Ipr_Roll_Cntrcts.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Ipr_Roll_Cntrcts.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Ipr_Roll_Cntrcts.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Ipr_Roll_Cntrcts_Tot, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Fmt9_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT9-TIAA-TABLE-TOT
        //*  P/I ROLLOVERS
        getReports().newPage(new ReportSpecification(7));                                                                                                                 //Natural: NEWPAGE ( 7 )
        if (condition(Global.isEscape())){return;}
        pnd_Rollover_Title.setValue("(P/I ROLLOVERS)");                                                                                                                   //Natural: MOVE '(P/I ROLLOVERS)' TO #ROLLOVER-TITLE
        getReports().write(7, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 7 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(7, 2);                                                                                                                                          //Natural: SKIP ( 7 ) 2
        pnd_Fmt10_Tiaa_Table_Tot.nadd(pnd_Fmt10_Tiaa_Table.getValue(1,"*"));                                                                                              //Natural: ADD #FMT10-TIAA-TABLE ( 1,* ) TO #FMT10-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table.getValue(1,1), new ReportEditMask                 //Natural: WRITE ( 7 ) 'TIAA CONTRACTUAL AMOUNT ' 7X #FMT10-TIAA-TABLE ( 1,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE ( 1,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE ( 1,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE ( 1,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt10_Tiaa_Table_Tot.reset();                                                                                                                                 //Natural: RESET #FMT10-TIAA-TABLE-TOT
        pnd_Fmt10_Tiaa_Table_Tot.nadd(pnd_Fmt10_Tiaa_Table.getValue(2,"*"));                                                                                              //Natural: ADD #FMT10-TIAA-TABLE ( 2,* ) TO #FMT10-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(13),pnd_Fmt10_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 7 ) 'PERIODIC DIVIDEND ' 13X #FMT10-TIAA-TABLE ( 2,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE ( 2,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE ( 2,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE ( 2,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT10-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(7),pnd_Fmt10_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt10_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_P_I_Roll_Cntrcts_Tot.nadd(pnd_P_I_Roll_Cntrcts.getValue("*"));                                                                                                //Natural: ADD #P-I-ROLL-CNTRCTS ( * ) TO #P-I-ROLL-CNTRCTS-TOT
        getReports().write(7, ReportOption.NOTITLE,"P/I  ROLL CONTRACTS     ",new ColumnSpacing(7),pnd_P_I_Roll_Cntrcts.getValue(1), new ReportEditMask                   //Natural: WRITE ( 7 ) 'P/I  ROLL CONTRACTS     ' 7X #P-I-ROLL-CNTRCTS ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #P-I-ROLL-CNTRCTS ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #P-I-ROLL-CNTRCTS ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #P-I-ROLL-CNTRCTS ( 4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #P-I-ROLL-CNTRCTS-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
            ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_P_I_Roll_Cntrcts.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_P_I_Roll_Cntrcts.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_P_I_Roll_Cntrcts.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_P_I_Roll_Cntrcts_Tot, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Fmt10_Tiaa_Table_Tot.reset();                                                                                                                                 //Natural: RESET #FMT10-TIAA-TABLE-TOT
        getReports().newPage(new ReportSpecification(7));                                                                                                                 //Natural: NEWPAGE ( 7 )
        if (condition(Global.isEscape())){return;}
        pnd_Rollover_Title.setValue("(TPA REINVESTMENT)");                                                                                                                //Natural: MOVE '(TPA REINVESTMENT)' TO #ROLLOVER-TITLE
        getReports().write(7, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 7 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(7, 2);                                                                                                                                          //Natural: SKIP ( 7 ) 2
        pnd_Fmt11_Tiaa_Table_Tot.nadd(pnd_Fmt11_Tiaa_Table.getValue(1,"*"));                                                                                              //Natural: ADD #FMT11-TIAA-TABLE ( 1,* ) TO #FMT11-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table.getValue(1,1), new ReportEditMask                 //Natural: WRITE ( 7 ) 'TIAA CONTRACTUAL AMOUNT ' 7X #FMT11-TIAA-TABLE ( 1,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE ( 1,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE ( 1,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE ( 1,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt11_Tiaa_Table_Tot.reset();                                                                                                                                 //Natural: RESET #FMT11-TIAA-TABLE-TOT
        pnd_Fmt11_Tiaa_Table_Tot.nadd(pnd_Fmt11_Tiaa_Table.getValue(2,"*"));                                                                                              //Natural: ADD #FMT11-TIAA-TABLE ( 2,* ) TO #FMT11-TIAA-TABLE-TOT
        getReports().write(7, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(13),pnd_Fmt11_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 7 ) 'PERIODIC DIVIDEND ' 13X #FMT11-TIAA-TABLE ( 2,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE ( 2,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE ( 2,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE ( 2,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT11-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(7),pnd_Fmt11_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt11_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt11_Tiaa_Table_Tot.reset();                                                                                                                                 //Natural: RESET #FMT11-TIAA-TABLE-TOT
        pnd_Tpa_Rinv_Cntrcts_Tot.nadd(pnd_Tpa_Rinv_Cntrcts.getValue("*"));                                                                                                //Natural: ADD #TPA-RINV-CNTRCTS ( * ) TO #TPA-RINV-CNTRCTS-TOT
        getReports().write(7, ReportOption.NOTITLE,"TPA RINV CONTRACTS      ",new ColumnSpacing(7),pnd_Tpa_Rinv_Cntrcts.getValue(1), new ReportEditMask                   //Natural: WRITE ( 7 ) 'TPA RINV CONTRACTS      ' 7X #TPA-RINV-CNTRCTS ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-RINV-CNTRCTS ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-RINV-CNTRCTS ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-RINV-CNTRCTS ( 4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 7X #TPA-RINV-CNTRCTS-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9 )
            ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Rinv_Cntrcts.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Rinv_Cntrcts.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Rinv_Cntrcts.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Tpa_Rinv_Cntrcts_Tot, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  **********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(7, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 7 ) NOTITLE ' '
                    getReports().write(7, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 7 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 7 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(7));
                    getReports().write(7, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 7 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 7 ) 51T '                CURRENT MODE                 '
                    //*  WRITE (7) 51T '               (AC ROLLOVERS)                '
                    //*  TAM 02/2001
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(66),pnd_Rollover_Title);                                                                    //Natural: WRITE ( 7 ) 66T #ROLLOVER-TITLE
                    getReports().skip(7, 2);                                                                                                                              //Natural: SKIP ( 7 ) 2
                    //*  PAGE 7
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(7, "LS=133 PS=57");
    }
}
