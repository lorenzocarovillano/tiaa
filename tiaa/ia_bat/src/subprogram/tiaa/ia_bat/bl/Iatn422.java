/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:46:03 AM
**        * FROM NATURAL SUBPROGRAM : Iatn422
************************************************************
**        * FILE NAME            : Iatn422.java
**        * CLASS NAME           : Iatn422
**        * INSTANCE NAME        : Iatn422
************************************************************
************************************************************************
* PROGRAM:  IATN422
* FUNCTION: SUBPROGRAM FOR TRANSFER PROCESSING OF TEACHERS TO CREF
* CREATED:  07/01/99 : BY ARI GROSSMAN
*
* 05/10/02  TD  CHANGE IAAN050F TO IAAN051Z
* 01/21/09  OS  TIAA ACCESS CHANGES. SC 012109.
* 02/10/10  JT  AIAN019 LINKAGE CHANGE.
* 04/06/12  OS  RATE BASE EXPANSION CHANGES. SC 040612.
* 09/08/14  OS  PROD FIX - RATE BASE ISSUE.  CHANGES MARKED 090814.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017   OS  RE-STOWED FOR PDA's and LDA'S PIN EXPANSION.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn422 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl420z pdaIatl420z;
    private PdaIata422 pdaIata422;
    private LdaIatl422 ldaIatl422;
    private LdaIaal422 ldaIaal422;
    private LdaIatl171 ldaIatl171;
    private PdaIatl420x pdaIatl420x;
    private PdaAial0190 pdaAial0190;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsField pnd_Wrk_Dte;

    private DbsGroup pnd_Wrk_Dte__R_Field_1;
    private DbsField pnd_Wrk_Dte_Pnd_Wrk_Yyyymm;
    private DbsField pnd_Wrk_Dte_Pnd_Wrk_Dd;
    private DbsField pnd_Lst_Updte_Dte;
    private DbsField pnd_Nxt_Updte_Dte;

    private DbsGroup pnd_Nxt_Updte_Dte__R_Field_2;
    private DbsField pnd_Nxt_Updte_Dte_Pnd_N_Mm;
    private DbsField pnd_Nxt_Updte_Dte__Filler1;
    private DbsField pnd_Nxt_Updte_Dte_Pnd_N_Dd;
    private DbsField pnd_Nxt_Updte_Dte__Filler2;
    private DbsField pnd_Nxt_Updte_Dte_Pnd_N_Yyyy;
    private DbsField pnd_W_Pct;
    private DbsField pnd_W_Pct_To;
    private DbsField pnd_Rate_Code_Breakdown;

    private DbsGroup pnd_Rate_Code_Breakdown__R_Field_3;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2;

    private DbsGroup pnd_Rate_Code_Breakdown__R_Field_4;
    private DbsField pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2_N;
    private DbsField pnd_Total_Issue_Date;

    private DbsGroup pnd_Total_Issue_Date__R_Field_5;
    private DbsField pnd_Total_Issue_Date_Pnd_Issue_Date_6;
    private DbsField pnd_Total_Issue_Date_Pnd_Issue_Date_2;

    private DbsGroup pnd_Total_Issue_Date__R_Field_6;
    private DbsField pnd_Total_Issue_Date_Pnd_Issue_Date_2_A;
    private DbsField pnd_Per;
    private DbsField pnd_W_Per;

    private DbsGroup pnd_Tiaa_Rates;
    private DbsField pnd_Tiaa_Rates_Pnd_Rate_Code;

    private DbsGroup pnd_Tiaa_Rates__R_Field_7;
    private DbsField pnd_Tiaa_Rates_Pnd_Rate_Code_A;
    private DbsField pnd_Tiaa_Rates_Pnd_Gtd_Pmt;
    private DbsField pnd_Tiaa_Rates_Pnd_Dvd_Pmt;
    private DbsField pnd_Fr_Gtd_Pmt;
    private DbsField pnd_Fr_Dvd_Pmt;
    private DbsField pnd_Dte_Time_Hold_A;

    private DbsGroup pnd_Dte_Time_Hold_A__R_Field_8;
    private DbsField pnd_Dte_Time_Hold_A_Pnd_Dte_Time_Hold;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_9;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;
    private DbsField pnd_Cntrct_Nbr;
    private DbsField pnd_Q;
    private DbsField pnd_R;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_D;
    private DbsField pnd_Fund_Break;

    private DbsGroup pnd_Fund_Break__R_Field_10;
    private DbsField pnd_Fund_Break_Pnd_Funds_1;
    private DbsField pnd_Fund_Break_Pnd_Funds_2;
    private DbsField pnd_From_Funds;
    private DbsField pnd_Tiaa_Units_Cnt;
    private DbsField pnd_To_Qty_Total;
    private DbsField pnd_Acc;
    private DbsField pnd_W_Diff;
    private DbsField pnd_W_Diff2;
    private DbsField pnd_Ivc_Pro_Adj;
    private DbsField pnd_Per_Ivc_Pro_Adj;
    private DbsField pnd_Ivc_Ind;
    private DbsField pnd_Contract_Type;
    private DbsField pnd_Intra_Fund;
    private DbsField pnd_Full_Cref_Contract_Out;
    private DbsField pnd_Full_Tiaa_Contract_Out;
    private DbsField pnd_Create_Teacher_Cnt;
    private DbsField pnd_Create_Cref_Cnt;
    private DbsField pnd_Full_Contract_Out;
    private DbsField pnd_Found_Fund;
    private DbsField pnd_From_Cref;
    private DbsField pnd_From_Tiaa;
    private DbsField pnd_From_Real;
    private DbsField pnd_From_Real_6;
    private DbsField pnd_On_File;
    private DbsField pnd_One_Byte_Fund;
    private DbsField pnd_Two_Byte_Fund;
    private DbsField pnd_T_Table;
    private DbsField pnd_C_Table;
    private DbsField pnd_Check_Teachers;
    private DbsField pnd_Same_From_Fund;
    private DbsField pnd_Sub_Fr;
    private DbsField pnd_Sub_To;
    private DbsField pnd_I;
    private DbsField pnd_Ii;
    private DbsField pnd_J;
    private DbsField pnd_L;
    private DbsField pnd_Var1;
    private DbsField pnd_Var2;
    private DbsField pnd_Var3;
    private DbsField pnd_Var4;
    private DbsField pnd_T;
    private DbsField pnd_O;
    private DbsField pnd_To_Units;
    private DbsField pnd_W_Units;
    private DbsField pnd_D_Units;
    private DbsField pnd_E_Units;
    private DbsField pnd_To_Pymts;
    private DbsField pnd_W_Pymts;
    private DbsField pnd_D_Pymts;
    private DbsField pnd_E_Pymts;
    private DbsField pnd_New_Val_Dollar;
    private DbsField pnd_To_Div;
    private DbsField pnd_Curr_Frm_Amt;
    private DbsField pnd_Xfr_Frm_Per_Amt;
    private DbsField pnd_Xfr_Frm_Div_Amt;
    private DbsField pnd_To_Gtd;
    private DbsField pnd_Original_Trn_Amt;
    private DbsField pnd_Date8;
    private DbsField pnd_To_Asset_Amt;
    private DbsField pnd_Tot_Guar_Divid;
    private DbsField pnd_Time;
    private DbsField pnd_Iaa_New_Issue;
    private DbsField pnd_Iaa_New_Issue_Phys;
    private DbsField pnd_Iaa_New_Issue_Re;
    private DbsField pnd_From_Contract_Number;
    private DbsField pnd_From_Contract_Payee;
    private DbsField pnd_To_Contract_Number;
    private DbsField pnd_To_Contract_Payee;
    private DbsField pnd_From_On_To_Side;
    private DbsField pnd_Found_Cnt;
    private DbsField pnd_Not_Found_Yet;
    private DbsField pnd_From_Fund;
    private DbsField pnd_From_Fund_H;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_Ctl_To_Fund_Hold;
    private DbsField pnd_Other_Funds_Found;
    private DbsField pnd_Found_Teacher_Fund;
    private DbsField pnd_Auv_Dte;
    private DbsField pnd_Auv;
    private DbsField pnd_W_Auv;
    private DbsField pnd_Auv_Ret_Dte;
    private DbsField pnd_Rate_Dt;
    private DbsField pnd_Rc;
    private DbsField pnd_W_Rc;
    private DbsField pnd_Nn;
    private DbsField pnd_Qq;
    private DbsField pnd_Zz;
    private DbsField pnd_Max_Prods;
    private DbsField pnd_Prod_Cnt;
    private DbsField pnd_W_Ia_Std_Alpha_Cd;
    private DbsField pnd_W_Ia_Rpt_Nm_Cd;
    private DbsField pnd_W_Ia_Rpt_Cmpny_Cd_A;
    private DbsField pnd_W3_Reject_Cde;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_11;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_12;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_13;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_14;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_15;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_16;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_17;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_18;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Cmpny_Fund_3;
    private DbsField pnd_Invrse_Recvd_Time;

    private DbsGroup pnd_Invrse_Recvd_Time__R_Field_19;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Fund;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Dte;
    private DbsField pnd_Savetime;
    private DbsField pnd_Todays_Bus_Date_A;

    private DbsGroup pnd_Todays_Bus_Date_A__R_Field_20;
    private DbsField pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date;
    private DbsField pnd_W_Alpha_Cde;
    private DbsField pnd_W_Nm_Cd;
    private DbsField pnd_W_Rate_Cde;

    private DbsGroup pnd_W_Rate_Cde__R_Field_21;
    private DbsField pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A;
    private DbsField pnd_Z;
    private DbsField pnd_Rem;
    private DbsField pnd_H;
    private DbsField pnd_S;
    private DbsField pnd_F;
    private DbsField pnd_G;
    private DbsField pnd_Gg;
    private DbsField pnd_Aa;
    private DbsField pnd_Ggg;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_Not_Full_Out;
    private DbsField pnd_Not_All_From_Transfer_Out;
    private DbsField pnd_Eff_Dte_03_31;
    private DbsField pnd_At_Least_One_Switch;
    private DbsField pnd_Switch_Fund_Found;
    private DbsField pnd_W_Other_Funds;
    private DbsField pnd_Fund_Cd_1;
    private DbsField pnd_Ctl_Fund_Cde;
    private DbsField pnd_Alpha_Cd_Table;
    private DbsField pnd_Rate_Code_Table;
    private DbsField pnd_Next_Pay_Dte;
    private DbsField pnd_Next_Chk_Dt;

    private DbsGroup pnd_Next_Chk_Dt__R_Field_22;
    private DbsField pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N;
    private DbsField pnd_Last_Chk_Dt;

    private DbsGroup pnd_Last_Chk_Dt__R_Field_23;
    private DbsField pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N;
    private DbsField pnd_Last_Dt;

    private DbsGroup pnd_Last_Dt__R_Field_24;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Mm;
    private DbsField pnd_Last_Dt_Pnd_Filler_1;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Dd;
    private DbsField pnd_Last_Dt_Pnd_Filler_2;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Yyyy;
    private DbsField pnd_Next_Dt;

    private DbsGroup pnd_Next_Dt__R_Field_25;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Mm;
    private DbsField pnd_Next_Dt_Pnd_Filler_1;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Dd;
    private DbsField pnd_Next_Dt_Pnd_Filler_2;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Yyyy;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_26;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Cc;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Yy;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Dd;

    private DbsGroup pnd_Eff_Date__R_Field_27;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_N;
    private DbsField pnd_Reval_Dte;

    private DbsGroup pnd_Reval_Dte__R_Field_28;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Yyyy;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Mm;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Dd;
    private DbsField pnd_Reval_Dte_D;
    private DbsField pnd_Tiaa_Fund_Pymt;
    private DbsField pnd_Transfer_Units;
    private DbsField pnd_Transfer_Dollars;
    private DbsField pnd_To_Acct_Cd;
    private DbsField pls_Adat_Accessed;
    private DbsField pls_Debug;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIata422 = new PdaIata422(localVariables);
        ldaIatl422 = new LdaIatl422();
        registerRecord(ldaIatl422);
        registerRecord(ldaIatl422.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIatl422.getVw_iaa_Cntrct());
        registerRecord(ldaIatl422.getVw_iaa_Tiaa_Fund_Rcrd_View());
        ldaIaal422 = new LdaIaal422();
        registerRecord(ldaIaal422);
        registerRecord(ldaIaal422.getVw_iaa_Cref_Fund());
        registerRecord(ldaIaal422.getVw_iaa_Tiaa_Fund());
        registerRecord(ldaIaal422.getVw_iaa_Cpr());
        ldaIatl171 = new LdaIatl171();
        registerRecord(ldaIatl171);
        registerRecord(ldaIatl171.getVw_iatl171());
        pdaIatl420x = new PdaIatl420x(localVariables);
        pdaAial0190 = new PdaAial0190(localVariables);
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl420z = new PdaIatl420z(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Wrk_Dte = localVariables.newFieldInRecord("pnd_Wrk_Dte", "#WRK-DTE", FieldType.STRING, 8);

        pnd_Wrk_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Wrk_Dte__R_Field_1", "REDEFINE", pnd_Wrk_Dte);
        pnd_Wrk_Dte_Pnd_Wrk_Yyyymm = pnd_Wrk_Dte__R_Field_1.newFieldInGroup("pnd_Wrk_Dte_Pnd_Wrk_Yyyymm", "#WRK-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Wrk_Dte_Pnd_Wrk_Dd = pnd_Wrk_Dte__R_Field_1.newFieldInGroup("pnd_Wrk_Dte_Pnd_Wrk_Dd", "#WRK-DD", FieldType.NUMERIC, 2);
        pnd_Lst_Updte_Dte = localVariables.newFieldInRecord("pnd_Lst_Updte_Dte", "#LST-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Nxt_Updte_Dte = localVariables.newFieldInRecord("pnd_Nxt_Updte_Dte", "#NXT-UPDTE-DTE", FieldType.STRING, 10);

        pnd_Nxt_Updte_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Nxt_Updte_Dte__R_Field_2", "REDEFINE", pnd_Nxt_Updte_Dte);
        pnd_Nxt_Updte_Dte_Pnd_N_Mm = pnd_Nxt_Updte_Dte__R_Field_2.newFieldInGroup("pnd_Nxt_Updte_Dte_Pnd_N_Mm", "#N-MM", FieldType.STRING, 2);
        pnd_Nxt_Updte_Dte__Filler1 = pnd_Nxt_Updte_Dte__R_Field_2.newFieldInGroup("pnd_Nxt_Updte_Dte__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Nxt_Updte_Dte_Pnd_N_Dd = pnd_Nxt_Updte_Dte__R_Field_2.newFieldInGroup("pnd_Nxt_Updte_Dte_Pnd_N_Dd", "#N-DD", FieldType.STRING, 2);
        pnd_Nxt_Updte_Dte__Filler2 = pnd_Nxt_Updte_Dte__R_Field_2.newFieldInGroup("pnd_Nxt_Updte_Dte__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Nxt_Updte_Dte_Pnd_N_Yyyy = pnd_Nxt_Updte_Dte__R_Field_2.newFieldInGroup("pnd_Nxt_Updte_Dte_Pnd_N_Yyyy", "#N-YYYY", FieldType.STRING, 4);
        pnd_W_Pct = localVariables.newFieldInRecord("pnd_W_Pct", "#W-PCT", FieldType.PACKED_DECIMAL, 6, 3);
        pnd_W_Pct_To = localVariables.newFieldInRecord("pnd_W_Pct_To", "#W-PCT-TO", FieldType.PACKED_DECIMAL, 6, 3);
        pnd_Rate_Code_Breakdown = localVariables.newFieldInRecord("pnd_Rate_Code_Breakdown", "#RATE-CODE-BREAKDOWN", FieldType.STRING, 3);

        pnd_Rate_Code_Breakdown__R_Field_3 = localVariables.newGroupInRecord("pnd_Rate_Code_Breakdown__R_Field_3", "REDEFINE", pnd_Rate_Code_Breakdown);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1 = pnd_Rate_Code_Breakdown__R_Field_3.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1", "#RATE-CODE-1", 
            FieldType.STRING, 1);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2 = pnd_Rate_Code_Breakdown__R_Field_3.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2", "#RATE-CODE-2", 
            FieldType.STRING, 2);

        pnd_Rate_Code_Breakdown__R_Field_4 = pnd_Rate_Code_Breakdown__R_Field_3.newGroupInGroup("pnd_Rate_Code_Breakdown__R_Field_4", "REDEFINE", pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2);
        pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2_N = pnd_Rate_Code_Breakdown__R_Field_4.newFieldInGroup("pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2_N", "#RATE-CODE-2-N", 
            FieldType.NUMERIC, 2);
        pnd_Total_Issue_Date = localVariables.newFieldInRecord("pnd_Total_Issue_Date", "#TOTAL-ISSUE-DATE", FieldType.NUMERIC, 8);

        pnd_Total_Issue_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Total_Issue_Date__R_Field_5", "REDEFINE", pnd_Total_Issue_Date);
        pnd_Total_Issue_Date_Pnd_Issue_Date_6 = pnd_Total_Issue_Date__R_Field_5.newFieldInGroup("pnd_Total_Issue_Date_Pnd_Issue_Date_6", "#ISSUE-DATE-6", 
            FieldType.NUMERIC, 6);
        pnd_Total_Issue_Date_Pnd_Issue_Date_2 = pnd_Total_Issue_Date__R_Field_5.newFieldInGroup("pnd_Total_Issue_Date_Pnd_Issue_Date_2", "#ISSUE-DATE-2", 
            FieldType.NUMERIC, 2);

        pnd_Total_Issue_Date__R_Field_6 = pnd_Total_Issue_Date__R_Field_5.newGroupInGroup("pnd_Total_Issue_Date__R_Field_6", "REDEFINE", pnd_Total_Issue_Date_Pnd_Issue_Date_2);
        pnd_Total_Issue_Date_Pnd_Issue_Date_2_A = pnd_Total_Issue_Date__R_Field_6.newFieldInGroup("pnd_Total_Issue_Date_Pnd_Issue_Date_2_A", "#ISSUE-DATE-2-A", 
            FieldType.STRING, 2);
        pnd_Per = localVariables.newFieldInRecord("pnd_Per", "#PER", FieldType.NUMERIC, 7, 4);
        pnd_W_Per = localVariables.newFieldInRecord("pnd_W_Per", "#W-PER", FieldType.NUMERIC, 7, 4);

        pnd_Tiaa_Rates = localVariables.newGroupArrayInRecord("pnd_Tiaa_Rates", "#TIAA-RATES", new DbsArrayController(1, 250));
        pnd_Tiaa_Rates_Pnd_Rate_Code = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Rate_Code", "#RATE-CODE", FieldType.NUMERIC, 2);

        pnd_Tiaa_Rates__R_Field_7 = pnd_Tiaa_Rates.newGroupInGroup("pnd_Tiaa_Rates__R_Field_7", "REDEFINE", pnd_Tiaa_Rates_Pnd_Rate_Code);
        pnd_Tiaa_Rates_Pnd_Rate_Code_A = pnd_Tiaa_Rates__R_Field_7.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Rate_Code_A", "#RATE-CODE-A", FieldType.STRING, 
            2);
        pnd_Tiaa_Rates_Pnd_Gtd_Pmt = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Gtd_Pmt", "#GTD-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tiaa_Rates_Pnd_Dvd_Pmt = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Dvd_Pmt", "#DVD-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Fr_Gtd_Pmt = localVariables.newFieldArrayInRecord("pnd_Fr_Gtd_Pmt", "#FR-GTD-PMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            2, 1, 250));
        pnd_Fr_Dvd_Pmt = localVariables.newFieldArrayInRecord("pnd_Fr_Dvd_Pmt", "#FR-DVD-PMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            2, 1, 250));
        pnd_Dte_Time_Hold_A = localVariables.newFieldInRecord("pnd_Dte_Time_Hold_A", "#DTE-TIME-HOLD-A", FieldType.STRING, 14);

        pnd_Dte_Time_Hold_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Dte_Time_Hold_A__R_Field_8", "REDEFINE", pnd_Dte_Time_Hold_A);
        pnd_Dte_Time_Hold_A_Pnd_Dte_Time_Hold = pnd_Dte_Time_Hold_A__R_Field_8.newFieldInGroup("pnd_Dte_Time_Hold_A_Pnd_Dte_Time_Hold", "#DTE-TIME-HOLD", 
            FieldType.NUMERIC, 14);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_9", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_9.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_9.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Q = localVariables.newFieldInRecord("pnd_Q", "#Q", FieldType.NUMERIC, 3);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 3);
        pnd_Fund_Break = localVariables.newFieldInRecord("pnd_Fund_Break", "#FUND-BREAK", FieldType.STRING, 3);

        pnd_Fund_Break__R_Field_10 = localVariables.newGroupInRecord("pnd_Fund_Break__R_Field_10", "REDEFINE", pnd_Fund_Break);
        pnd_Fund_Break_Pnd_Funds_1 = pnd_Fund_Break__R_Field_10.newFieldInGroup("pnd_Fund_Break_Pnd_Funds_1", "#FUNDS-1", FieldType.STRING, 1);
        pnd_Fund_Break_Pnd_Funds_2 = pnd_Fund_Break__R_Field_10.newFieldInGroup("pnd_Fund_Break_Pnd_Funds_2", "#FUNDS-2", FieldType.STRING, 2);
        pnd_From_Funds = localVariables.newFieldArrayInRecord("pnd_From_Funds", "#FROM-FUNDS", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Tiaa_Units_Cnt = localVariables.newFieldInRecord("pnd_Tiaa_Units_Cnt", "#TIAA-UNITS-CNT", FieldType.NUMERIC, 8, 3);
        pnd_To_Qty_Total = localVariables.newFieldInRecord("pnd_To_Qty_Total", "#TO-QTY-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Acc = localVariables.newFieldInRecord("pnd_Acc", "#ACC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_W_Diff = localVariables.newFieldInRecord("pnd_W_Diff", "#W-DIFF", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_W_Diff2 = localVariables.newFieldInRecord("pnd_W_Diff2", "#W-DIFF2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ivc_Pro_Adj = localVariables.newFieldInRecord("pnd_Ivc_Pro_Adj", "#IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Ivc_Pro_Adj = localVariables.newFieldInRecord("pnd_Per_Ivc_Pro_Adj", "#PER-IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Ind = localVariables.newFieldInRecord("pnd_Ivc_Ind", "#IVC-IND", FieldType.STRING, 1);
        pnd_Contract_Type = localVariables.newFieldInRecord("pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 1);
        pnd_Intra_Fund = localVariables.newFieldInRecord("pnd_Intra_Fund", "#INTRA-FUND", FieldType.STRING, 1);
        pnd_Full_Cref_Contract_Out = localVariables.newFieldInRecord("pnd_Full_Cref_Contract_Out", "#FULL-CREF-CONTRACT-OUT", FieldType.STRING, 1);
        pnd_Full_Tiaa_Contract_Out = localVariables.newFieldInRecord("pnd_Full_Tiaa_Contract_Out", "#FULL-TIAA-CONTRACT-OUT", FieldType.STRING, 1);
        pnd_Create_Teacher_Cnt = localVariables.newFieldInRecord("pnd_Create_Teacher_Cnt", "#CREATE-TEACHER-CNT", FieldType.STRING, 1);
        pnd_Create_Cref_Cnt = localVariables.newFieldInRecord("pnd_Create_Cref_Cnt", "#CREATE-CREF-CNT", FieldType.STRING, 1);
        pnd_Full_Contract_Out = localVariables.newFieldInRecord("pnd_Full_Contract_Out", "#FULL-CONTRACT-OUT", FieldType.STRING, 1);
        pnd_Found_Fund = localVariables.newFieldInRecord("pnd_Found_Fund", "#FOUND-FUND", FieldType.STRING, 1);
        pnd_From_Cref = localVariables.newFieldInRecord("pnd_From_Cref", "#FROM-CREF", FieldType.STRING, 1);
        pnd_From_Tiaa = localVariables.newFieldInRecord("pnd_From_Tiaa", "#FROM-TIAA", FieldType.STRING, 1);
        pnd_From_Real = localVariables.newFieldInRecord("pnd_From_Real", "#FROM-REAL", FieldType.STRING, 1);
        pnd_From_Real_6 = localVariables.newFieldInRecord("pnd_From_Real_6", "#FROM-REAL-6", FieldType.STRING, 1);
        pnd_On_File = localVariables.newFieldInRecord("pnd_On_File", "#ON-FILE", FieldType.STRING, 1);
        pnd_One_Byte_Fund = localVariables.newFieldInRecord("pnd_One_Byte_Fund", "#ONE-BYTE-FUND", FieldType.STRING, 1);
        pnd_Two_Byte_Fund = localVariables.newFieldInRecord("pnd_Two_Byte_Fund", "#TWO-BYTE-FUND", FieldType.STRING, 2);
        pnd_T_Table = localVariables.newFieldArrayInRecord("pnd_T_Table", "#T-TABLE", FieldType.STRING, 1, new DbsArrayController(1, 3));
        pnd_C_Table = localVariables.newFieldArrayInRecord("pnd_C_Table", "#C-TABLE", FieldType.STRING, 1, new DbsArrayController(1, 18));
        pnd_Check_Teachers = localVariables.newFieldInRecord("pnd_Check_Teachers", "#CHECK-TEACHERS", FieldType.STRING, 1);
        pnd_Same_From_Fund = localVariables.newFieldInRecord("pnd_Same_From_Fund", "#SAME-FROM-FUND", FieldType.STRING, 1);
        pnd_Sub_Fr = localVariables.newFieldInRecord("pnd_Sub_Fr", "#SUB-FR", FieldType.INTEGER, 2);
        pnd_Sub_To = localVariables.newFieldInRecord("pnd_Sub_To", "#SUB-TO", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Ii = localVariables.newFieldInRecord("pnd_Ii", "#II", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_Var1 = localVariables.newFieldInRecord("pnd_Var1", "#VAR1", FieldType.INTEGER, 2);
        pnd_Var2 = localVariables.newFieldInRecord("pnd_Var2", "#VAR2", FieldType.INTEGER, 2);
        pnd_Var3 = localVariables.newFieldInRecord("pnd_Var3", "#VAR3", FieldType.INTEGER, 2);
        pnd_Var4 = localVariables.newFieldInRecord("pnd_Var4", "#VAR4", FieldType.INTEGER, 2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.INTEGER, 2);
        pnd_O = localVariables.newFieldInRecord("pnd_O", "#O", FieldType.INTEGER, 2);
        pnd_To_Units = localVariables.newFieldInRecord("pnd_To_Units", "#TO-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_W_Units = localVariables.newFieldInRecord("pnd_W_Units", "#W-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_D_Units = localVariables.newFieldInRecord("pnd_D_Units", "#D-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_E_Units = localVariables.newFieldInRecord("pnd_E_Units", "#E-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_To_Pymts = localVariables.newFieldInRecord("pnd_To_Pymts", "#TO-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Pymts = localVariables.newFieldInRecord("pnd_W_Pymts", "#W-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_D_Pymts = localVariables.newFieldInRecord("pnd_D_Pymts", "#D-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_E_Pymts = localVariables.newFieldInRecord("pnd_E_Pymts", "#E-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_New_Val_Dollar = localVariables.newFieldInRecord("pnd_New_Val_Dollar", "#NEW-VAL-DOLLAR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_To_Div = localVariables.newFieldInRecord("pnd_To_Div", "#TO-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Curr_Frm_Amt = localVariables.newFieldInRecord("pnd_Curr_Frm_Amt", "#CURR-FRM-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Frm_Per_Amt = localVariables.newFieldInRecord("pnd_Xfr_Frm_Per_Amt", "#XFR-FRM-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Frm_Div_Amt = localVariables.newFieldInRecord("pnd_Xfr_Frm_Div_Amt", "#XFR-FRM-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_To_Gtd = localVariables.newFieldInRecord("pnd_To_Gtd", "#TO-GTD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Original_Trn_Amt = localVariables.newFieldInRecord("pnd_Original_Trn_Amt", "#ORIGINAL-TRN-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);
        pnd_To_Asset_Amt = localVariables.newFieldArrayInRecord("pnd_To_Asset_Amt", "#TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_Tot_Guar_Divid = localVariables.newFieldInRecord("pnd_Tot_Guar_Divid", "#TOT-GUAR-DIVID", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Iaa_New_Issue = localVariables.newFieldInRecord("pnd_Iaa_New_Issue", "#IAA-NEW-ISSUE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue_Phys = localVariables.newFieldInRecord("pnd_Iaa_New_Issue_Phys", "#IAA-NEW-ISSUE-PHYS", FieldType.STRING, 1);
        pnd_Iaa_New_Issue_Re = localVariables.newFieldInRecord("pnd_Iaa_New_Issue_Re", "#IAA-NEW-ISSUE-RE", FieldType.STRING, 1);
        pnd_From_Contract_Number = localVariables.newFieldInRecord("pnd_From_Contract_Number", "#FROM-CONTRACT-NUMBER", FieldType.STRING, 10);
        pnd_From_Contract_Payee = localVariables.newFieldInRecord("pnd_From_Contract_Payee", "#FROM-CONTRACT-PAYEE", FieldType.STRING, 2);
        pnd_To_Contract_Number = localVariables.newFieldInRecord("pnd_To_Contract_Number", "#TO-CONTRACT-NUMBER", FieldType.STRING, 10);
        pnd_To_Contract_Payee = localVariables.newFieldInRecord("pnd_To_Contract_Payee", "#TO-CONTRACT-PAYEE", FieldType.STRING, 2);
        pnd_From_On_To_Side = localVariables.newFieldInRecord("pnd_From_On_To_Side", "#FROM-ON-TO-SIDE", FieldType.STRING, 1);
        pnd_Found_Cnt = localVariables.newFieldInRecord("pnd_Found_Cnt", "#FOUND-CNT", FieldType.STRING, 1);
        pnd_Not_Found_Yet = localVariables.newFieldInRecord("pnd_Not_Found_Yet", "#NOT-FOUND-YET", FieldType.STRING, 1);
        pnd_From_Fund = localVariables.newFieldInRecord("pnd_From_Fund", "#FROM-FUND", FieldType.STRING, 1);
        pnd_From_Fund_H = localVariables.newFieldInRecord("pnd_From_Fund_H", "#FROM-FUND-H", FieldType.STRING, 1);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_Ctl_To_Fund_Hold = localVariables.newFieldInRecord("pnd_Ctl_To_Fund_Hold", "#CTL-TO-FUND-HOLD", FieldType.STRING, 1);
        pnd_Other_Funds_Found = localVariables.newFieldInRecord("pnd_Other_Funds_Found", "#OTHER-FUNDS-FOUND", FieldType.STRING, 1);
        pnd_Found_Teacher_Fund = localVariables.newFieldInRecord("pnd_Found_Teacher_Fund", "#FOUND-TEACHER-FUND", FieldType.STRING, 1);
        pnd_Auv_Dte = localVariables.newFieldInRecord("pnd_Auv_Dte", "#AUV-DTE", FieldType.NUMERIC, 8);
        pnd_Auv = localVariables.newFieldInRecord("pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_W_Auv = localVariables.newFieldInRecord("pnd_W_Auv", "#W-AUV", FieldType.NUMERIC, 5, 2);
        pnd_Auv_Ret_Dte = localVariables.newFieldInRecord("pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_Rate_Dt = localVariables.newFieldInRecord("pnd_Rate_Dt", "#RATE-DT", FieldType.NUMERIC, 6);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_W_Rc = localVariables.newFieldInRecord("pnd_W_Rc", "#W-RC", FieldType.NUMERIC, 2);
        pnd_Nn = localVariables.newFieldInRecord("pnd_Nn", "#NN", FieldType.NUMERIC, 2);
        pnd_Qq = localVariables.newFieldInRecord("pnd_Qq", "#QQ", FieldType.NUMERIC, 2);
        pnd_Zz = localVariables.newFieldInRecord("pnd_Zz", "#ZZ", FieldType.NUMERIC, 2);
        pnd_Max_Prods = localVariables.newFieldInRecord("pnd_Max_Prods", "#MAX-PRODS", FieldType.PACKED_DECIMAL, 3);
        pnd_Prod_Cnt = localVariables.newFieldInRecord("pnd_Prod_Cnt", "#PROD-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Ia_Std_Alpha_Cd = localVariables.newFieldArrayInRecord("pnd_W_Ia_Std_Alpha_Cd", "#W-IA-STD-ALPHA-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_W_Ia_Rpt_Nm_Cd = localVariables.newFieldArrayInRecord("pnd_W_Ia_Rpt_Nm_Cd", "#W-IA-RPT-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            80));
        pnd_W_Ia_Rpt_Cmpny_Cd_A = localVariables.newFieldArrayInRecord("pnd_W_Ia_Rpt_Cmpny_Cd_A", "#W-IA-RPT-CMPNY-CD-A", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_W3_Reject_Cde = localVariables.newFieldInRecord("pnd_W3_Reject_Cde", "#W3-REJECT-CDE", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_11", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_12 = pnd_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_12", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 
            4);

        pnd_Save_Check_Dte_A__R_Field_13 = pnd_Save_Check_Dte_A__R_Field_12.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_13", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_14 = pnd_Save_Check_Dte_A__R_Field_13.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_14", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yy);
        pnd_Save_Check_Dte_A_Pnd_Yy_A = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy_A", "#YY-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_15 = pnd_Save_Check_Dte_A__R_Field_12.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_15", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_15.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_16 = pnd_Save_Check_Dte_A__R_Field_12.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_16", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Dd);
        pnd_Save_Check_Dte_A_Pnd_Dd_A = pnd_Save_Check_Dte_A__R_Field_16.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd_A", "#DD-A", FieldType.STRING, 2);

        pnd_Save_Check_Dte_A__R_Field_17 = pnd_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_17", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_18 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_18", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_18.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_18.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_18.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Cmpny_Fund_3 = localVariables.newFieldInRecord("pnd_Cmpny_Fund_3", "#CMPNY-FUND-3", FieldType.STRING, 3);
        pnd_Invrse_Recvd_Time = localVariables.newFieldInRecord("pnd_Invrse_Recvd_Time", "#INVRSE-RECVD-TIME", FieldType.NUMERIC, 14);

        pnd_Invrse_Recvd_Time__R_Field_19 = localVariables.newGroupInRecord("pnd_Invrse_Recvd_Time__R_Field_19", "REDEFINE", pnd_Invrse_Recvd_Time);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Fund = pnd_Invrse_Recvd_Time__R_Field_19.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Fund", "#IRT-FUND", FieldType.NUMERIC, 
            2);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Dte = pnd_Invrse_Recvd_Time__R_Field_19.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Dte", "#IRT-DTE", FieldType.NUMERIC, 
            12);
        pnd_Savetime = localVariables.newFieldInRecord("pnd_Savetime", "#SAVETIME", FieldType.TIME);
        pnd_Todays_Bus_Date_A = localVariables.newFieldInRecord("pnd_Todays_Bus_Date_A", "#TODAYS-BUS-DATE-A", FieldType.STRING, 8);

        pnd_Todays_Bus_Date_A__R_Field_20 = localVariables.newGroupInRecord("pnd_Todays_Bus_Date_A__R_Field_20", "REDEFINE", pnd_Todays_Bus_Date_A);
        pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date = pnd_Todays_Bus_Date_A__R_Field_20.newFieldInGroup("pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date", "#TODAYS-BUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_W_Alpha_Cde = localVariables.newFieldArrayInRecord("pnd_W_Alpha_Cde", "#W-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 80));
        pnd_W_Nm_Cd = localVariables.newFieldArrayInRecord("pnd_W_Nm_Cd", "#W-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_W_Rate_Cde = localVariables.newFieldArrayInRecord("pnd_W_Rate_Cde", "#W-RATE-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 80));

        pnd_W_Rate_Cde__R_Field_21 = localVariables.newGroupInRecord("pnd_W_Rate_Cde__R_Field_21", "REDEFINE", pnd_W_Rate_Cde);
        pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A = pnd_W_Rate_Cde__R_Field_21.newFieldArrayInGroup("pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A", "#W-RATE-CDE-A", FieldType.STRING, 
            2, new DbsArrayController(1, 80));
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 3);
        pnd_Rem = localVariables.newFieldInRecord("pnd_Rem", "#REM", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_H = localVariables.newFieldInRecord("pnd_H", "#H", FieldType.NUMERIC, 3);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.NUMERIC, 3);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.NUMERIC, 3);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 3);
        pnd_Gg = localVariables.newFieldInRecord("pnd_Gg", "#GG", FieldType.NUMERIC, 3);
        pnd_Aa = localVariables.newFieldInRecord("pnd_Aa", "#AA", FieldType.NUMERIC, 3);
        pnd_Ggg = localVariables.newFieldInRecord("pnd_Ggg", "#GGG", FieldType.NUMERIC, 3);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_Not_Full_Out = localVariables.newFieldInRecord("pnd_Not_Full_Out", "#NOT-FULL-OUT", FieldType.STRING, 1);
        pnd_Not_All_From_Transfer_Out = localVariables.newFieldInRecord("pnd_Not_All_From_Transfer_Out", "#NOT-ALL-FROM-TRANSFER-OUT", FieldType.STRING, 
            1);
        pnd_Eff_Dte_03_31 = localVariables.newFieldArrayInRecord("pnd_Eff_Dte_03_31", "#EFF-DTE-03-31", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_At_Least_One_Switch = localVariables.newFieldInRecord("pnd_At_Least_One_Switch", "#AT-LEAST-ONE-SWITCH", FieldType.STRING, 1);
        pnd_Switch_Fund_Found = localVariables.newFieldInRecord("pnd_Switch_Fund_Found", "#SWITCH-FUND-FOUND", FieldType.STRING, 1);
        pnd_W_Other_Funds = localVariables.newFieldInRecord("pnd_W_Other_Funds", "#W-OTHER-FUNDS", FieldType.STRING, 1);
        pnd_Fund_Cd_1 = localVariables.newFieldInRecord("pnd_Fund_Cd_1", "#FUND-CD-1", FieldType.STRING, 1);
        pnd_Ctl_Fund_Cde = localVariables.newFieldArrayInRecord("pnd_Ctl_Fund_Cde", "#CTL-FUND-CDE", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_Alpha_Cd_Table = localVariables.newFieldArrayInRecord("pnd_Alpha_Cd_Table", "#ALPHA-CD-TABLE", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_Rate_Code_Table = localVariables.newFieldArrayInRecord("pnd_Rate_Code_Table", "#RATE-CODE-TABLE", FieldType.STRING, 3, new DbsArrayController(1, 
            80));
        pnd_Next_Pay_Dte = localVariables.newFieldInRecord("pnd_Next_Pay_Dte", "#NEXT-PAY-DTE", FieldType.DATE);
        pnd_Next_Chk_Dt = localVariables.newFieldInRecord("pnd_Next_Chk_Dt", "#NEXT-CHK-DT", FieldType.STRING, 8);

        pnd_Next_Chk_Dt__R_Field_22 = localVariables.newGroupInRecord("pnd_Next_Chk_Dt__R_Field_22", "REDEFINE", pnd_Next_Chk_Dt);
        pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N = pnd_Next_Chk_Dt__R_Field_22.newFieldInGroup("pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N", "#NEXT-CHK-DT-N", FieldType.NUMERIC, 
            8);
        pnd_Last_Chk_Dt = localVariables.newFieldInRecord("pnd_Last_Chk_Dt", "#LAST-CHK-DT", FieldType.STRING, 8);

        pnd_Last_Chk_Dt__R_Field_23 = localVariables.newGroupInRecord("pnd_Last_Chk_Dt__R_Field_23", "REDEFINE", pnd_Last_Chk_Dt);
        pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N = pnd_Last_Chk_Dt__R_Field_23.newFieldInGroup("pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N", "#LAST-CHK-DT-N", FieldType.NUMERIC, 
            8);
        pnd_Last_Dt = localVariables.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 10);

        pnd_Last_Dt__R_Field_24 = localVariables.newGroupInRecord("pnd_Last_Dt__R_Field_24", "REDEFINE", pnd_Last_Dt);
        pnd_Last_Dt_Pnd_Last_Dt_Mm = pnd_Last_Dt__R_Field_24.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Mm", "#LAST-DT-MM", FieldType.STRING, 2);
        pnd_Last_Dt_Pnd_Filler_1 = pnd_Last_Dt__R_Field_24.newFieldInGroup("pnd_Last_Dt_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Last_Dt_Pnd_Last_Dt_Dd = pnd_Last_Dt__R_Field_24.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Dd", "#LAST-DT-DD", FieldType.STRING, 2);
        pnd_Last_Dt_Pnd_Filler_2 = pnd_Last_Dt__R_Field_24.newFieldInGroup("pnd_Last_Dt_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Last_Dt_Pnd_Last_Dt_Yyyy = pnd_Last_Dt__R_Field_24.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Yyyy", "#LAST-DT-YYYY", FieldType.STRING, 4);
        pnd_Next_Dt = localVariables.newFieldInRecord("pnd_Next_Dt", "#NEXT-DT", FieldType.STRING, 10);

        pnd_Next_Dt__R_Field_25 = localVariables.newGroupInRecord("pnd_Next_Dt__R_Field_25", "REDEFINE", pnd_Next_Dt);
        pnd_Next_Dt_Pnd_Next_Dt_Mm = pnd_Next_Dt__R_Field_25.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Mm", "#NEXT-DT-MM", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_1 = pnd_Next_Dt__R_Field_25.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Dd = pnd_Next_Dt__R_Field_25.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Dd", "#NEXT-DT-DD", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_2 = pnd_Next_Dt__R_Field_25.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Yyyy = pnd_Next_Dt__R_Field_25.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Yyyy", "#NEXT-DT-YYYY", FieldType.STRING, 4);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_26 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_26", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Cc = pnd_Eff_Date__R_Field_26.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Cc", "#EFF-DATE-CC", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Yy = pnd_Eff_Date__R_Field_26.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Yy", "#EFF-DATE-YY", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Mm = pnd_Eff_Date__R_Field_26.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Mm", "#EFF-DATE-MM", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Dd = pnd_Eff_Date__R_Field_26.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Dd", "#EFF-DATE-DD", FieldType.STRING, 2);

        pnd_Eff_Date__R_Field_27 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_27", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_N = pnd_Eff_Date__R_Field_27.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 8);
        pnd_Reval_Dte = localVariables.newFieldInRecord("pnd_Reval_Dte", "#REVAL-DTE", FieldType.STRING, 8);

        pnd_Reval_Dte__R_Field_28 = localVariables.newGroupInRecord("pnd_Reval_Dte__R_Field_28", "REDEFINE", pnd_Reval_Dte);
        pnd_Reval_Dte_Pnd_Reval_Yyyy = pnd_Reval_Dte__R_Field_28.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Yyyy", "#REVAL-YYYY", FieldType.STRING, 4);
        pnd_Reval_Dte_Pnd_Reval_Mm = pnd_Reval_Dte__R_Field_28.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Mm", "#REVAL-MM", FieldType.STRING, 2);
        pnd_Reval_Dte_Pnd_Reval_Dd = pnd_Reval_Dte__R_Field_28.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Dd", "#REVAL-DD", FieldType.STRING, 2);
        pnd_Reval_Dte_D = localVariables.newFieldInRecord("pnd_Reval_Dte_D", "#REVAL-DTE-D", FieldType.DATE);
        pnd_Tiaa_Fund_Pymt = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Pymt", "#TIAA-FUND-PYMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Transfer_Units = localVariables.newFieldInRecord("pnd_Transfer_Units", "#TRANSFER-UNITS", FieldType.PACKED_DECIMAL, 8, 3);
        pnd_Transfer_Dollars = localVariables.newFieldInRecord("pnd_Transfer_Dollars", "#TRANSFER-DOLLARS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_To_Acct_Cd = localVariables.newFieldArrayInRecord("pnd_To_Acct_Cd", "#TO-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pls_Adat_Accessed = WsIndependent.getInstance().newFieldInRecord("pls_Adat_Accessed", "+ADAT-ACCESSED", FieldType.BOOLEAN, 1);
        pls_Debug = WsIndependent.getInstance().newFieldInRecord("pls_Debug", "+DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        ldaIatl422.initializeValues();
        ldaIaal422.initializeValues();
        ldaIatl171.initializeValues();

        localVariables.reset();
        pnd_Max_Prods.setInitialValue(80);
        pnd_Max_Rate.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn422() throws Exception
    {
        super("Iatn422");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN422", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56;//Natural: FORMAT LS = 133 PS = 56
        //*  WRITE '***************************' /
        //*        '  START OF IATN422 PROGRAM'/
        //*        '***************************'
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        if (condition(! (pls_Adat_Accessed.getBoolean())))                                                                                                                //Natural: ON ERROR;//Natural: IF NOT +ADAT-ACCESSED
        {
            pls_Adat_Accessed.setValue(true);                                                                                                                             //Natural: ASSIGN +ADAT-ACCESSED := TRUE
            vw_naz_Table_Ddm.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = 'NAZ021UTLUTL3'
            (
            "FIND01",
            new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", "NAZ021UTLUTL3", WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_naz_Table_Ddm.readNextRow("FIND01")))
            {
                vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
                if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                        //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y'
                {
                    continue;
                }
                if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.equals("TEST")))                                                                                     //Natural: IF NAZ-TBL-RCRD-DSCRPTN-TXT = 'TEST'
                {
                    pls_Debug.setValue(true);                                                                                                                             //Natural: ASSIGN +DEBUG := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pls_Debug.setValue(false);                                                                                                                            //Natural: ASSIGN +DEBUG := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #LOAD-EXTERNALIZE-TABLE-CODES
        sub_Pnd_Load_Externalize_Table_Codes();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #LOAD-TABLES
        sub_Pnd_Load_Tables();
        if (condition(Global.isEscape())) {return;}
        //* *WRITE '='  IATL010.IA-FRM-CNTRCT
        //*       '='  IATL010.IA-FRM-PAYEE
        pnd_Time.setValue(pdaIatl420z.getIatl010_Pnd_Sys_Time());                                                                                                         //Natural: ASSIGN #TIME := IATL010.#SYS-TIME
        //*   WRITE /// 'CONTRACT NUMBER !!!' '=' IATL010.IA-FRM-CNTRCT
        pnd_Save_Check_Dte_A.setValueEdited(pdaIatl420z.getIatl010_Pnd_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED IATL010.#CHECK-DTE ( EM = YYYYMMDD ) TO #SAVE-CHECK-DTE-A
        pnd_Eff_Date.setValueEdited(pdaIatl420z.getIatl010_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED IATL010.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
        //*  GET LAST BUSINESS DATE OF MARCH FOR REVALUATION DATE
        pnd_Reval_Dte.setValue(pnd_Eff_Date);                                                                                                                             //Natural: ASSIGN #REVAL-DTE := #EFF-DATE
        pnd_Reval_Dte_Pnd_Reval_Dd.setValue("31");                                                                                                                        //Natural: ASSIGN #REVAL-DD := '31'
        if (condition(pnd_Reval_Dte_Pnd_Reval_Mm.equals("03")))                                                                                                           //Natural: IF #REVAL-MM = '03'
        {
            pnd_Reval_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Reval_Dte);                                                                                 //Natural: MOVE EDITED #REVAL-DTE TO #REVAL-DTE-D ( EM = YYYYMMDD )
            DbsUtil.callnat(Adsn607.class , getCurrentProcessState(), pnd_Reval_Dte_D, pnd_W_Rc);                                                                         //Natural: CALLNAT 'ADSN607' #REVAL-DTE-D #W-RC
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W_Rc.equals(getZero())))                                                                                                                    //Natural: IF #W-RC = 0
            {
                pnd_Reval_Dte.setValueEdited(pnd_Reval_Dte_D,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #REVAL-DTE-D ( EM = YYYYMMDD ) TO #REVAL-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-IF-NEW-ISSUE
        sub_Pnd_Determine_If_New_Issue();
        if (condition(Global.isEscape())) {return;}
        pnd_Last_Dt.reset();                                                                                                                                              //Natural: RESET #LAST-DT #NEXT-DT #NEXT-PAY-DTE #NEXT-CHK-DT
        pnd_Next_Dt.reset();
        pnd_Next_Pay_Dte.reset();
        pnd_Next_Chk_Dt.reset();
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Dt, pnd_Next_Dt);                                                                             //Natural: CALLNAT 'IAAN0020' #LAST-DT #NEXT-DT
        if (condition(Global.isEscape())) return;
        pnd_Next_Chk_Dt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Next_Dt_Pnd_Next_Dt_Yyyy, pnd_Next_Dt_Pnd_Next_Dt_Mm, pnd_Next_Dt_Pnd_Next_Dt_Dd));  //Natural: COMPRESS #NEXT-DT-YYYY #NEXT-DT-MM #NEXT-DT-DD INTO #NEXT-CHK-DT LEAVING NO
        pnd_Last_Chk_Dt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Last_Dt_Pnd_Last_Dt_Yyyy, pnd_Last_Dt_Pnd_Last_Dt_Mm, pnd_Last_Dt_Pnd_Last_Dt_Dd));  //Natural: COMPRESS #LAST-DT-YYYY #LAST-DT-MM #LAST-DT-DD INTO #LAST-CHK-DT LEAVING NO
        //*  CALLNAT 'NAZN6032' #NEXT-PAY-DTE #NEXT-CHK-DT IATA001.#MODE
        //*  LOAD AUDIT FILE
                                                                                                                                                                          //Natural: PERFORM #FILL-AUDIT-WITH-REQUEST-INFO
        sub_Pnd_Fill_Audit_With_Request_Info();
        if (condition(Global.isEscape())) {return;}
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  STORE BEFORE IMAGES
        DbsUtil.callnat(Iatn420a.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pnd_Iaa_New_Issue,      //Natural: CALLNAT 'IATN420A' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE #IAA-NEW-ISSUE IATL010.XFR-FRM-ACCT-CDE ( 1 ) IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE #SAVE-CHECK-DTE #TIME #W3-REJECT-CDE
            pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1), pdaIatl420z.getIatl010_Ia_To_Cntrct(), pdaIatl420z.getIatl010_Ia_To_Payee(), pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, 
            pnd_Time, pnd_W3_Reject_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  STORE HISTORICAL RATE INFO FOR
        //*  TIAA AND CREF
        DbsUtil.callnat(Iatn420b.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pnd_Iaa_New_Issue,      //Natural: CALLNAT 'IATN420B' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE #IAA-NEW-ISSUE IATL010.XFR-FRM-ACCT-CDE ( 1 ) IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE #SAVE-CHECK-DTE #LAST-CHK-DT #TIME #W3-REJECT-CDE
            pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1), pdaIatl420z.getIatl010_Ia_To_Cntrct(), pdaIatl420z.getIatl010_Ia_To_Payee(), pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, 
            pnd_Last_Chk_Dt, pnd_Time, pnd_W3_Reject_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FILL AIAN019 LINKAGE
                                                                                                                                                                          //Natural: PERFORM #FILL-AIAN019-LINKAGE
        sub_Pnd_Fill_Aian019_Linkage();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FZ:                                                                                                                                                               //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals(" ")))                                                                         //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) EQ ' '
            {
                if (true) break FZ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FZ. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                                        //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-FRM-ACCT-CDE ( #T )
            //*  FLD NOT FILLED
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
            sub_Pnd_Convert_One_Byte_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T).setValue(pnd_Two_Byte_Fund);                                                                      //Natural: ASSIGN IATL171.IAXFR-FROM-FUND-CDE ( #T ) := #TWO-BYTE-FUND
            ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                //Natural: ASSIGN IATL171.IAXFR-FRM-ACCT-CD ( #T ) := IATL010.XFR-FRM-ACCT-CDE ( #T )
            ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T));                               //Natural: ASSIGN IATL171.IAXFR-FRM-UNIT-TYP ( #T ) := IATL010.XFR-FRM-UNIT-TYP ( #T )
            ldaIatl171.getIatl171_Iaxfr_From_Typ().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T));                                        //Natural: ASSIGN IATL171.IAXFR-FROM-TYP ( #T ) := IATL010.XFR-FRM-TYP ( #T )
            ldaIatl171.getIatl171_Iaxfr_From_Qty().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                        //Natural: ASSIGN IATL171.IAXFR-FROM-QTY ( #T ) := IATL010.XFR-FRM-QTY ( #T )
            ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Unit_Val().getValue(pnd_T).setValue(0);                                                                          //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-UNIT-VAL ( #T ) := 0
            ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(pnd_T).setValue(0);                                                                                //Natural: ASSIGN IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( #T ) := 0
            if (condition(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T).equals("A")))                                                                        //Natural: IF IATL171.IAXFR-FRM-UNIT-TYP ( #T ) = 'A'
            {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
                sub_Pnd_Annual_Fund_Cnv();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                sub_Pnd_Monthly_Cnv_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
            //*  LOAD AUDIT FILE
                                                                                                                                                                          //Natural: PERFORM #FILL-AUDIT-WITH-FUND-INFO
            sub_Pnd_Fill_Audit_With_Fund_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                             //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payment_Method().setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                          //Natural: ASSIGN #AIAN019-PAYMENT-METHOD := IATL010.XFR-FRM-ACCT-CDE ( #T )
            pnd_W_Pct.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                                                                     //Natural: ASSIGN #W-PCT := IATL010.XFR-FRM-QTY ( #T )
            ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Units().getValue(pnd_T).setValue(0);                                                                               //Natural: ASSIGN IATL171.IAXFR-FROM-RQSTD-XFR-UNITS ( #T ) := 0
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number());                                         //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #AIAN019-CONTRACT-NUMBER
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code());                                                 //Natural: ASSIGN #W-CNTRCT-PAYEE = #AIAN019-PAYEE-CODE
                                                                                                                                                                          //Natural: PERFORM #READ-FROM-FUND-RECORD
            sub_Pnd_Read_From_Fund_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FX:                                                                                                                                                           //Natural: FOR #S = 1 TO 20
            for (pnd_S.setValue(1); condition(pnd_S.lessOrEqual(20)); pnd_S.nadd(1))
            {
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_S).equals(" ")))                                                                      //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #S ) EQ ' '
                {
                    if (true) break FX;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FX. )
                }                                                                                                                                                         //Natural: END-IF
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_In().getValue(pnd_S).setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_S));         //Natural: MOVE IATL010.XFR-TO-ACCT-CDE ( #S ) TO #AIAN019-CREF-FUND-IN ( #S )
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Reval_Method().getValue(pnd_S).setValue(pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_S));    //Natural: MOVE IATL010.XFR-TO-UNIT-TYP ( #S ) TO #AIAN019-CREF-REVAL-METHOD ( #S )
                pnd_W_Pct_To.setValue(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_S));                                                                               //Natural: ASSIGN #W-PCT-TO := IATL010.XFR-TO-QTY ( #S )
                                                                                                                                                                          //Natural: PERFORM #READ-FUND-RECORD
                sub_Pnd_Read_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FX"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FX"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  #SIS LAST BUCKET, WHERE EXCESS IS
            //*  040612
            pnd_S.nsubtract(1);                                                                                                                                           //Natural: COMPUTE #S = #S - 1
            //*  FA. FOR #Z = 1 TO 99
            FA:                                                                                                                                                           //Natural: FOR #Z = 1 TO #MAX-RATE
            for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(pnd_Max_Rate)); pnd_Z.nadd(1))
            {
                pnd_Rem.compute(new ComputeParameters(false, pnd_Rem), pnd_Fr_Gtd_Pmt.getValue(pnd_T,pnd_Z).subtract(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gtd_Pmt().getValue("*", //Natural: COMPUTE #REM = #FR-GTD-PMT ( #T,#Z ) - #AIAN019-GTD-PMT ( *,#Z )
                    pnd_Z)));
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gtd_Pmt().getValue(pnd_S,pnd_Z).nadd(pnd_Rem);                                                             //Natural: ADD #REM TO #AIAN019-GTD-PMT ( #S,#Z )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Type_Of_Run().setValue("A");                                                                                   //Natural: ASSIGN #AIAN019-TYPE-OF-RUN := 'A'
            DbsUtil.callnat(Aian019.class , getCurrentProcessState(), pdaAial0190.getPnd_Aian019_Linkage());                                                              //Natural: CALLNAT 'AIAN019' #AIAN019-LINKAGE
            if (condition(Global.isEscape())) return;
            if (condition(pls_Debug.getBoolean()))                                                                                                                        //Natural: IF +DEBUG
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-AIAN019-LINKAGE
                sub_Display_Aian019_Linkage();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  040612
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Return_Code_Nbr().greater(getZero())))                                                           //Natural: IF #AIAN019-RETURN-CODE-NBR GT 0
            {
                //*  040612
                getReports().write(0, NEWLINE,"ERROR IN AIAN019 - ERROR CD =>",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Return_Code_Nbr());                         //Natural: WRITE / 'ERROR IN AIAN019 - ERROR CD =>' #AIAN019-RETURN-CODE-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "CONTRACT =>",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number(),pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code()); //Natural: WRITE 'CONTRACT =>' #AIAN019-CONTRACT-NUMBER #AIAN019-PAYEE-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue("C5");                                                                                          //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := 'C5'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*   WRITE 'CALLING AIAN019' CALLNAT 'ARI015' #AIAN019-LINKAGE
            ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_T).setValue(0);                                                                                //Natural: ASSIGN IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( #T ) := 0
            ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( #T ) = IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) - IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T )
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).subtract(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T)));
            ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Divid().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Divid().getValue(pnd_T)),  //Natural: COMPUTE IATL171.IAXFR-FROM-AFTR-XFR-DIVID ( #T ) = IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T ) - IATL171.IAXFR-FROM-RQSTD-XFR-DIVID ( #T )
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T).subtract(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Divid().getValue(pnd_T)));
            //*  FILL AUDIT FILE
                                                                                                                                                                          //Natural: PERFORM #FILLING-TO-SIDE
            sub_Pnd_Filling_To_Side();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIatl171.getIatl171_Iaxfr_Calc_Unit_Val_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Effctv_Dte());                                                           //Natural: ASSIGN IATL171.IAXFR-CALC-UNIT-VAL-DTE := IATL010.RQST-EFFCTV-DTE
            ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Amt().getValue(pnd_T).setValue(0);                                                                                 //Natural: ASSIGN IATL171.IAXFR-FROM-RQSTD-XFR-AMT ( #T ) := 0
            ldaIatl171.getIatl171_Iaxfr_From_Asset_Xfr_Amt().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Asset_Xfr_Amt().getValue(pnd_T)),  //Natural: ASSIGN IATL171.IAXFR-FROM-ASSET-XFR-AMT ( #T ) := 0 + #AIAN019-LINKAGE.#AIAN019-TOTAL-TRANSFER-AMT-OUT ( * )
                DbsField.add(getZero(),pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue("*")));
            ldaIatl171.getIatl171_Iaxfr_From_Pmt_Aftr_Xfr().getValue(pnd_T).setValue(0);                                                                                  //Natural: ASSIGN IATL171.IAXFR-FROM-PMT-AFTR-XFR ( #T ) := 0
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
                                                                                                                                                                          //Natural: PERFORM #FILL-LEDGER-PDA
            sub_Pnd_Fill_Ledger_Pda();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().notEquals(" ")))                                                                              //Natural: IF #IATA422-RETURN-CD NE ' '
            {
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd());                                             //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #IATA422-RETURN-CD
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #RESET-AIAN019-LINKAGE
            sub_Pnd_Reset_Aian019_Linkage();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FK:                                                                                                                                                               //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(" ")))                                                                          //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = ' '
            {
                if (true) break FK;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FK. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)),  //Natural: COMPUTE IATL171.IAXFR-TO-AFTR-XFR-UNITS ( #I ) = IATL171.IAXFR-TO-BFR-XFR-UNITS ( #I ) + IATL171.IAXFR-TO-XFR-UNITS ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).add(ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(pnd_I)));
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_I).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_I)),  //Natural: COMPUTE IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #I ) = IATL171.IAXFR-TO-BFR-XFR-GUAR ( #I ) + IATL171.IAXFR-TO-XFR-GUAR ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_I).add(ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_I)));
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(pnd_I).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(pnd_I)),  //Natural: COMPUTE IATL171.IAXFR-TO-AFTR-XFR-DIVID ( #I ) = IATL171.IAXFR-TO-BFR-XFR-DIVID ( #I ) + IATL171.IAXFR-TO-XFR-DIVID ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Divid().getValue(pnd_I).add(ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(pnd_I)));
                //* *  IF (#EFF-DATE-MM = '03' AND #EFF-DATE-DD = '31') OR
                if (condition((pnd_Eff_Date_Pnd_Eff_Date_Mm.equals("03") && pnd_Eff_Date_Pnd_Eff_Date_Dd.equals(pnd_Reval_Dte_Pnd_Reval_Dd)) || (ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).equals(getZero())))) //Natural: IF ( #EFF-DATE-MM = '03' AND #EFF-DATE-DD = #REVAL-DD ) OR ( IATL171.IAXFR-TO-BFR-XFR-UNITS ( #I ) = 0 )
                {
                                                                                                                                                                          //Natural: PERFORM #REVALUE-03-31
                    sub_Pnd_Revalue_03_31();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FK"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FK"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Eff_Dte_03_31.getValue(pnd_I).setValue("Y");                                                                                                      //Natural: MOVE 'Y' TO #EFF-DTE-03-31 ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaIatl420x.getPnd_Iaa_Xfr_Audit().setValuesByName(ldaIatl171.getVw_iatl171());                                                                                   //Natural: MOVE BY NAME IATL171 TO #IAA-XFR-AUDIT
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        if (condition(pdaIatl420z.getIatl010_Ia_To_Cntrct().notEquals(" ")))                                                                                              //Natural: IF IATL010.IA-TO-CNTRCT NOT = ' '
        {
            DbsUtil.callnat(Iatn420d.class , getCurrentProcessState(), pdaIatl420x.getPnd_Iatn420x_In(), pdaIatl420x.getPnd_Iaa_Xfr_Audit(), pnd_Ivc_Pro_Adj,             //Natural: CALLNAT 'IATN420D' #IATN420X-IN #IAA-XFR-AUDIT #IVC-PRO-ADJ #PER-IVC-PRO-ADJ #IVC-IND #W3-REJECT-CDE
                pnd_Per_Ivc_Pro_Adj, pnd_Ivc_Ind, pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Iatn420c.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pdaIatl420z.getIatl010_Ia_To_Cntrct(),  //Natural: CALLNAT 'IATN420C' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE #TIME #IVC-IND #IAA-NEW-ISSUE IATL010.#CHECK-DTE IATL010.#TODAYS-DTE IATL010.RQST-EFFCTV-DTE IATL010.RCRD-TYPE-CDE IATL010.RQST-XFR-TYPE
            pdaIatl420z.getIatl010_Ia_To_Payee(), pnd_Time, pnd_Ivc_Ind, pnd_Iaa_New_Issue, pdaIatl420z.getIatl010_Pnd_Check_Dte(), pdaIatl420z.getIatl010_Pnd_Todays_Dte(), 
            pdaIatl420z.getIatl010_Rqst_Effctv_Dte(), pdaIatl420z.getIatl010_Rcrd_Type_Cde(), pdaIatl420z.getIatl010_Rqst_Xfr_Type());
        if (condition(Global.isEscape())) return;
        //*         IATL010.#NEXT-BUS-DTE
        if (condition(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue("*").equals("M") || ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue("*").equals("M")))       //Natural: IF IATL171.IAXFR-FRM-UNIT-TYP ( * ) = 'M' OR IATL171.IAXFR-TO-UNIT-TYP ( * ) = 'M'
        {
            //*   AND IATL171.RCRD-TYPE-CDE = '1'
                                                                                                                                                                          //Natural: PERFORM #FILL-MONTHLY-DOLLARS
            sub_Pnd_Fill_Monthly_Dollars();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #UPDATE-MASTER
        sub_Pnd_Update_Master();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            getReports().write(0, " UPDATE MASTER ERROR ","=",pnd_W3_Reject_Cde);                                                                                         //Natural: WRITE ' UPDATE MASTER ERROR ' '=' #W3-REJECT-CDE
            if (Global.isEscape()) return;
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* * PERFORM #WRITE-AUDIT
        ldaIatl171.getVw_iatl171().insertDBRow();                                                                                                                         //Natural: STORE IATL171
        pdaIatl420x.getPnd_Iaa_Xfr_Audit().setValuesByName(ldaIatl171.getVw_iatl171());                                                                                   //Natural: MOVE BY NAME IATL171 TO #IAA-XFR-AUDIT
        DbsUtil.callnat(Iatn420x.class , getCurrentProcessState(), pdaIatl420x.getPnd_Iatn420x_In(), pdaIatl420x.getPnd_Iaa_Xfr_Audit());                                 //Natural: CALLNAT 'IATN420X' #IATN420X-IN #IAA-XFR-AUDIT
        if (condition(Global.isEscape())) return;
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-AUDIT-WITH-REQUEST-INFO
        //* *******************************************************************
        //*     IATL171.IAXFR-CALC-SSN            := IATL010.RQST-SSN
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONVERT-ONE-BYTE-FUND
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-AUDIT-WITH-FUND-INFO
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TIAA-FUND-FROM
        //* **********************************************************************
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-LEDGER-PDA
        //* *******************************************************************
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #LOAD-TABLES
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-MASTER
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILLING-TO-SIDE
        //* *********************************************************************
        //* *PERFORM #FROM-FUND-TABLE
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-UNITS-LINKAGE
        //* *********************************************************************
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #LOAD-EXTERNALIZE-TABLE-CODES
        //* *
        //* *F. FOR #F = 1 TO 20
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-AIAN019-LINKAGE
        //* *********************************************************************
        //*      #AIAN019-PAYMENT-METHOD := IAA-CNTRCT.CNTRCT-PYMNT-MTHD
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-NEW-FUND
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-CREF-NEW-FUND
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-TIAA-NEW-FUND
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-FUND-CNV
        //* ******************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-CNV-FUND
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-FULL-TIAA-REAL-OUT
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-IF-FULL-OUT
        //* *********************************************************************
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-AIAN019-LINKAGE
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #REVALUE-03-31
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-MONTHLY-DOLLARS
        //* ******************************************************************
        //* ******************************************************************
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-FUND-RECORD
        //* ******************************************************************
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-FROM-FUND-RECORD
        //* ***   FILLING AUDIT-FIELDS
        //* ******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-IF-NEW-ISSUE
        //* ******************************************************************
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-AUDIT
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-AIAN019-LINKAGE
    }
    private void sub_Pnd_Fill_Audit_With_Request_Info() throws Exception                                                                                                  //Natural: #FILL-AUDIT-WITH-REQUEST-INFO
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl171.getIatl171_Rqst_Id().setValue(pdaIatl420z.getIatl010_Rqst_Id());                                                                                       //Natural: ASSIGN IATL171.RQST-ID := IATL010.RQST-ID
        ldaIatl171.getIatl171_Rcrd_Type_Cde().setValue(pdaIatl420z.getIatl010_Rcrd_Type_Cde());                                                                           //Natural: ASSIGN IATL171.RCRD-TYPE-CDE := IATL010.RCRD-TYPE-CDE
        ldaIatl171.getIatl171_Iaxfr_Entry_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Entry_Dte());                                                                        //Natural: ASSIGN IATL171.IAXFR-ENTRY-DTE := IATL010.RQST-ENTRY-DTE
        ldaIatl171.getIatl171_Iaxfr_Entry_Tme().setValue(pdaIatl420z.getIatl010_Rqst_Entry_Tme());                                                                        //Natural: ASSIGN IATL171.IAXFR-ENTRY-TME := IATL010.RQST-ENTRY-TME
        ldaIatl171.getIatl171_Iaxfr_Opn_Clsd_Ind().setValue(pdaIatl420z.getIatl010_Rqst_Opn_Clsd_Ind());                                                                  //Natural: ASSIGN IATL171.IAXFR-OPN-CLSD-IND := IATL010.RQST-OPN-CLSD-IND
        ldaIatl171.getIatl171_Iaxfr_In_Progress_Ind().setValue(pdaIatl420z.getIatl010_Xfr_In_Progress_Ind().getBoolean());                                                //Natural: ASSIGN IATL171.IAXFR-IN-PROGRESS-IND := IATL010.XFR-IN-PROGRESS-IND
        ldaIatl171.getIatl171_Iaxfr_Retry_Cnt().setValue(pdaIatl420z.getIatl010_Xfr_Retry_Cnt());                                                                         //Natural: ASSIGN IATL171.IAXFR-RETRY-CNT := IATL010.XFR-RETRY-CNT
        ldaIatl171.getIatl171_Iaxfr_Calc_Unique_Id().setValue(pdaIatl420z.getIatl010_Ia_Unique_Id());                                                                     //Natural: ASSIGN IATL171.IAXFR-CALC-UNIQUE-ID := IATL010.IA-UNIQUE-ID
        ldaIatl171.getIatl171_Iaxfr_Effctve_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Effctv_Dte());                                                                     //Natural: ASSIGN IATL171.IAXFR-EFFCTVE-DTE := IATL010.RQST-EFFCTV-DTE
        ldaIatl171.getIatl171_Iaxfr_Rqst_Rcvd_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Rcvd_Dte());                                                                     //Natural: ASSIGN IATL171.IAXFR-RQST-RCVD-DTE := IATL010.RQST-RCVD-DTE
        ldaIatl171.getIatl171_Iaxfr_Rqst_Rcvd_Tme().setValue(pdaIatl420z.getIatl010_Rqst_Rcvd_Tme());                                                                     //Natural: ASSIGN IATL171.IAXFR-RQST-RCVD-TME := IATL010.RQST-RCVD-TME
        ldaIatl171.getIatl171_Iaxfr_Invrse_Rcvd_Tme().setValue(0);                                                                                                        //Natural: ASSIGN IATL171.IAXFR-INVRSE-RCVD-TME := 0
        ldaIatl171.getIatl171_Iaxfr_Invrse_Effctve_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Rcprcl_Dte());                                                              //Natural: ASSIGN IATL171.IAXFR-INVRSE-EFFCTVE-DTE := IATL010.RQST-RCPRCL-DTE
        ldaIatl171.getIatl171_Iaxfr_Calc_Sttmnt_Indctr().setValue(pdaIatl420z.getIatl010_Rqst_Sttmnt_Ind());                                                              //Natural: ASSIGN IATL171.IAXFR-CALC-STTMNT-INDCTR := IATL010.RQST-STTMNT-IND
        ldaIatl171.getIatl171_Iaxfr_Calc_Status_Cde().setValue("M0");                                                                                                     //Natural: ASSIGN IATL171.IAXFR-CALC-STATUS-CDE := 'M0'
        ldaIatl171.getIatl171_Iaxfr_Cwf_Wpid().setValue(pdaIatl420z.getIatl010_Xfr_Work_Prcss_Id());                                                                      //Natural: ASSIGN IATL171.IAXFR-CWF-WPID := IATL010.XFR-WORK-PRCSS-ID
        ldaIatl171.getIatl171_Iaxfr_Cwf_Log_Dte_Time().setValue(pdaIatl420z.getIatl010_Xfr_Mit_Log_Dte_Tme());                                                            //Natural: ASSIGN IATL171.IAXFR-CWF-LOG-DTE-TIME := IATL010.XFR-MIT-LOG-DTE-TME
        ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                     //Natural: ASSIGN IATL171.IAXFR-FROM-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        if (condition(pdaIatl420z.getIatl010_Ia_Frm_Payee().equals(" ")))                                                                                                 //Natural: IF IATL010.IA-FRM-PAYEE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde().compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde()), pdaIatl420z.getIatl010_Ia_Frm_Payee().val()); //Natural: ASSIGN IATL171.IAXFR-FROM-PAYEE-CDE := VAL ( IATL010.IA-FRM-PAYEE )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl171.getIatl171_Iaxfr_Calc_Reject_Cde().setValue(pdaIatl420z.getIatl010_Xfr_Rjctn_Cde());                                                                   //Natural: ASSIGN IATL171.IAXFR-CALC-REJECT-CDE := IATL010.XFR-RJCTN-CDE
        ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                                   //Natural: ASSIGN IATL171.IAXFR-CALC-TO-PPCN-NBR := IATL010.IA-TO-CNTRCT
        if (condition(pdaIatl420z.getIatl010_Ia_To_Payee().equals(" ")))                                                                                                  //Natural: IF IATL010.IA-TO-PAYEE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde().compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde()), pdaIatl420z.getIatl010_Ia_To_Payee().val()); //Natural: ASSIGN IATL171.IAXFR-CALC-TO-PAYEE-CDE := VAL ( IATL010.IA-TO-PAYEE )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_New_Issue_Ind().setValue(pnd_Iaa_New_Issue_Phys);                                                                        //Natural: ASSIGN IATL171.IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND := #IAA-NEW-ISSUE-PHYS
        ldaIatl171.getIatl171_Iaxfr_Cycle_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Todays_Dte());                                                                        //Natural: ASSIGN IATL171.IAXFR-CYCLE-DTE := IATL010.#TODAYS-DTE
        ldaIatl171.getIatl171_Iaxfr_Acctng_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte());                                                                     //Natural: ASSIGN IATL171.IAXFR-ACCTNG-DTE := IATL010.#NEXT-BUS-DTE
        ldaIatl171.getIatl171_Lst_Chnge_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Sys_Time());                                                                            //Natural: ASSIGN IATL171.LST-CHNGE-DTE := IATL010.#SYS-TIME
        ldaIatl171.getIatl171_Rqst_Xfr_Type().setValue(pdaIatl420z.getIatl010_Rqst_Xfr_Type());                                                                           //Natural: ASSIGN IATL171.RQST-XFR-TYPE := IATL010.RQST-XFR-TYPE
        ldaIatl171.getIatl171_Rqst_Unit_Cde().setValue(pdaIatl420z.getIatl010_Rqst_Unit_Cde());                                                                           //Natural: ASSIGN IATL171.RQST-UNIT-CDE := IATL010.RQST-UNIT-CDE
        ldaIatl171.getIatl171_Ia_New_Iss_Prt_Pull().setValue(pdaIatl420z.getIatl010_Ia_New_Iss_Prt_Pull());                                                               //Natural: ASSIGN IATL171.IA-NEW-ISS-PRT-PULL := IATL010.IA-NEW-ISS-PRT-PULL
    }
    private void sub_Pnd_Convert_One_Byte_Fund() throws Exception                                                                                                         //Natural: #CONVERT-ONE-BYTE-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_Two_Byte_Fund.reset();                                                                                                                                        //Natural: RESET #TWO-BYTE-FUND
        F30:                                                                                                                                                              //Natural: FOR #II = 1 TO 40
        for (pnd_Ii.setValue(1); condition(pnd_Ii.lessOrEqual(40)); pnd_Ii.nadd(1))
        {
            if (condition(pnd_Alpha_Cd_Table.getValue(pnd_Ii).equals(" ")))                                                                                               //Natural: IF #ALPHA-CD-TABLE ( #II ) EQ ' '
            {
                if (true) break F30;                                                                                                                                      //Natural: ESCAPE BOTTOM ( F30. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Alpha_Cd_Table.getValue(pnd_Ii).equals(pnd_One_Byte_Fund)))                                                                             //Natural: IF #ALPHA-CD-TABLE ( #II ) = #ONE-BYTE-FUND
                {
                    pnd_Two_Byte_Fund.setValue(pnd_Ctl_Fund_Cde.getValue(pnd_Ii));                                                                                        //Natural: MOVE #CTL-FUND-CDE ( #II ) TO #TWO-BYTE-FUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Audit_With_Fund_Info() throws Exception                                                                                                     //Natural: #FILL-AUDIT-WITH-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_W3_Reject_Cde.setValue("T3");                                                                                                                                 //Natural: ASSIGN #W3-REJECT-CDE = 'T3'
                                                                                                                                                                          //Natural: PERFORM #TIAA-FUND-FROM
        sub_Pnd_Tiaa_Fund_From();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Pnd_Tiaa_Fund_From() throws Exception                                                                                                                //Natural: #TIAA-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := IATL010.#IA-FRM-PAYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE := #FROM-FUND-TOT
        ldaIaal422.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1A",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1A:
        while (condition(ldaIaal422.getVw_iaa_Tiaa_Fund().readNextRow("R1A")))
        {
            if (condition(ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).setValue(ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt());                             //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) := IAA-TIAA-FUND.TIAA-TOT-PER-AMT
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T).setValue(ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt());                            //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T ) := IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
                //*    #RATE-CODE-A(*) := IAA-TIAA-FUND.TIAA-RATE-CDE(*)
                //*    #RATE-DATE(*)   := IAA-TIAA-FUND.TIAA-RATE-DTE(*)
                //*    #GTD-PMT(*)     := IAA-TIAA-FUND.TIAA-PER-PAY-AMT(*)
                //*    #DVD-PMT(*)     := IAA-TIAA-FUND.TIAA-PER-DIV-AMT(*)
                pnd_W3_Reject_Cde.reset();                                                                                                                                //Natural: RESET #W3-REJECT-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1A;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1A. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Ledger_Pda() throws Exception                                                                                                               //Natural: #FILL-LEDGER-PDA
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata422.getPnd_Iata422_Pnd_Iata422_Rqst_Id().setValue(pdaIatl420z.getIatl010_Rqst_Id());                                                                       //Natural: ASSIGN #IATA422-RQST-ID := IATL010.RQST-ID
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Todays_Dte());                                                         //Natural: ASSIGN #IATA422-LDGR-CYCLE-DTE := IATL010.#TODAYS-DTE
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Effctv_Dte());                                                       //Natural: ASSIGN #IATA422-LDGR-EFFCTV-DTE := IATL010.RQST-EFFCTV-DTE
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Accntng_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte());                                                     //Natural: ASSIGN #IATA422-LDGR-ACCNTNG-DTE := IATL010.#NEXT-BUS-DTE
        pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                           //Natural: ASSIGN #IATA422-FROM-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Payee_Cde().compute(new ComputeParameters(false, pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Payee_Cde()),              //Natural: ASSIGN #IATA422-FROM-PAYEE-CDE := VAL ( IATL010.IA-FRM-PAYEE )
            pdaIatl420z.getIatl010_Ia_Frm_Payee().val());
        pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                              //Natural: ASSIGN #IATA422-TO-PPCN-NBR := IATL010.IA-TO-CNTRCT
        if (condition(pdaIatl420z.getIatl010_Ia_To_Payee().equals(" ")))                                                                                                  //Natural: IF IATL010.IA-TO-PAYEE = ' '
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Pye_Cde().setValue(0);                                                                                               //Natural: ASSIGN #IATA422-TO-PYE-CDE := 0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Pye_Cde().compute(new ComputeParameters(false, pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Pye_Cde()),                  //Natural: ASSIGN #IATA422-TO-PYE-CDE := VAL ( IATL010.IA-TO-PAYEE )
                pdaIatl420z.getIatl010_Ia_To_Payee().val());
        }                                                                                                                                                                 //Natural: END-IF
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().setValue(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T));                                 //Natural: ASSIGN #IATA422-LDGR-FROM-FUND-CDE := IATL171.IAXFR-FROM-FUND-CDE ( #T )
        //*  012109
        pnd_To_Acct_Cd.getValue("*").reset();                                                                                                                             //Natural: RESET #TO-ACCT-CD ( * )
        //*  012109
        if (condition(pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("2")))                                                                                                //Natural: IF IATL010.RCRD-TYPE-CDE = '2'
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Switch_Ind().setValue("S");                                                                                             //Natural: ASSIGN #IATA422-SWITCH-IND := 'S'
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(1).setValue(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T));                     //Natural: ASSIGN #IATA422-LDGR-TO-FUND-CDE ( 1 ) := IATL171.IAXFR-TO-FUND-CDE ( #T )
            pnd_To_Acct_Cd.getValue(1).setValue(ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(pnd_T));                                                                //Natural: ASSIGN #TO-ACCT-CD ( 1 ) := IATL171.IAXFR-TO-ACCT-CD ( #T )
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Amt().getValue(1).setValue(pnd_To_Asset_Amt.getValue(pnd_T));                                                      //Natural: ASSIGN #IATA422-LDGR-AMT ( 1 ) := #TO-ASSET-AMT ( #T )
            //*  012109
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Switch_Ind().setValue(" ");                                                                                             //Natural: ASSIGN #IATA422-SWITCH-IND := ' '
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(1,":",20).setValue(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,                    //Natural: ASSIGN #IATA422-LDGR-TO-FUND-CDE ( 1:20 ) := IATL171.IAXFR-TO-FUND-CDE ( 1:20 )
                ":",20));
            pnd_To_Acct_Cd.getValue("*").setValue(ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue("*"));                                                                //Natural: ASSIGN #TO-ACCT-CD ( * ) := IATL171.IAXFR-TO-ACCT-CD ( * )
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ().getValue(1,":",20).setValue(ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,                    //Natural: ASSIGN #IATA422-LDGR-TO-UNIT-TYP ( 1:20 ) := IATL171.IAXFR-TO-UNIT-TYP ( 1:20 )
                ":",20));
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Amt().getValue(1,":",20).setValue(pnd_To_Asset_Amt.getValue(1,":",20));                                            //Natural: ASSIGN #IATA422-LDGR-AMT ( 1:20 ) := #TO-ASSET-AMT ( 1:20 )
        }                                                                                                                                                                 //Natural: END-IF
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd().setValue(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T));                                        //Natural: ASSIGN #IATA422-LDGR-ACCT-CD := IATL171.IAXFR-FRM-UNIT-TYP ( #T )
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Lst_Chnge_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Sys_Time());                                                            //Natural: ASSIGN #IATA422-LST-CHNGE-DTE := IATL010.#SYS-TIME
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("C4");                                                                                                 //Natural: ASSIGN #IATA422-RETURN-CD := 'C4'
        //* *CALLNAT 'IATN420V' #IATA422                         /* 012109 START
        //*  012109 END
        DbsUtil.callnat(Iatn421v.class , getCurrentProcessState(), pdaIata422.getPnd_Iata422(), pdaIatl420z.getIatl010_Ia_Unique_Id(), ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(pnd_T),  //Natural: CALLNAT 'IATN421V' #IATA422 IATL010.IA-UNIQUE-ID IATL171.IAXFR-FRM-ACCT-CD ( #T ) IATL171.IAXFR-FRM-UNIT-TYP ( #T ) #TO-ACCT-CD ( * )
            ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T), pnd_To_Acct_Cd.getValue("*"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Load_Tables() throws Exception                                                                                                                   //Natural: #LOAD-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_T_Table.getValue(1).setValue("T");                                                                                                                            //Natural: MOVE 'T' TO #T-TABLE ( 1 )
        pnd_T_Table.getValue(2).setValue("G");                                                                                                                            //Natural: MOVE 'G' TO #T-TABLE ( 2 )
        pnd_T_Table.getValue(3).setValue("R");                                                                                                                            //Natural: MOVE 'R' TO #T-TABLE ( 3 )
        pnd_C_Table.getValue(1).setValue("C");                                                                                                                            //Natural: MOVE 'C' TO #C-TABLE ( 1 )
        pnd_C_Table.getValue(2).setValue("M");                                                                                                                            //Natural: MOVE 'M' TO #C-TABLE ( 2 )
        pnd_C_Table.getValue(3).setValue("S");                                                                                                                            //Natural: MOVE 'S' TO #C-TABLE ( 3 )
        pnd_C_Table.getValue(4).setValue("B");                                                                                                                            //Natural: MOVE 'B' TO #C-TABLE ( 4 )
        pnd_C_Table.getValue(5).setValue("W");                                                                                                                            //Natural: MOVE 'W' TO #C-TABLE ( 5 )
        pnd_C_Table.getValue(6).setValue("L");                                                                                                                            //Natural: MOVE 'L' TO #C-TABLE ( 6 )
        pnd_C_Table.getValue(7).setValue("E");                                                                                                                            //Natural: MOVE 'E' TO #C-TABLE ( 7 )
        pnd_C_Table.getValue(8).setValue("I");                                                                                                                            //Natural: MOVE 'I' TO #C-TABLE ( 8 )
    }
    private void sub_Pnd_Update_Master() throws Exception                                                                                                                 //Natural: #UPDATE-MASTER
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-FULL-TIAA-REAL-OUT
        sub_Pnd_Determine_Full_Tiaa_Real_Out();
        if (condition(Global.isEscape())) {return;}
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  ADD & UPDATE CREF FUND RECORDS
        //*  UPDATE CONTRACT & CPR RECORD
        //*  CREATE AFTER IMAGES FOR ALL
        //*  040612 CHANGED FROM :99
        //*  040612
        DbsUtil.callnat(Iatn422a.class , getCurrentProcessState(), ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde(),             //Natural: CALLNAT 'IATN422A' IATL171.IAXFR-FROM-PPCN-NBR IATL171.IAXFR-FROM-PAYEE-CDE IATL171.IAXFR-CALC-TO-PPCN-NBR IATL171.IAXFR-CALC-TO-PAYEE-CDE IATL171.IAXFR-FROM-FUND-CDE ( 1:20 ) IATL171.IAXFR-FRM-ACCT-CD ( 1:20 ) IATL171.IAXFR-FRM-UNIT-TYP ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1:20 ) #FR-GTD-PMT ( 1:2,1:250 ) #FR-DVD-PMT ( 1:2,1:250 ) #RATE-CODE-TABLE ( 1:20 ) IATL171.IAXFR-TO-FUND-CDE ( 1:20 ) IATL171.IAXFR-TO-ACCT-CD ( 1:20 ) IATL171.IAXFR-TO-UNIT-TYP ( 1:20 ) IATL171.IAXFR-TO-RATE-CDE ( 1:20 ) IATL171.IAXFR-TO-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-REVAL-UNIT-VAL ( 1:20 ) #IVC-PRO-ADJ #PER-IVC-PRO-ADJ #IVC-IND #IAA-NEW-ISSUE #FULL-CONTRACT-OUT #SAVE-CHECK-DTE IATL010.#TODAYS-DTE IATL171.IAXFR-EFFCTVE-DTE IATL010.#NEXT-BUS-DTE #NEXT-PAY-DTE #TIME #EFF-DTE-03-31 ( 1:20 ) #W3-REJECT-CDE
            ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde(), ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(1,":",20), 
            ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1,":",20), 
            pnd_Fr_Gtd_Pmt.getValue(1,":",2,1,":",250), pnd_Fr_Dvd_Pmt.getValue(1,":",2,1,":",250), pnd_Rate_Code_Table.getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,":",20), 
            ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(1,":",20), 
            ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(1,":",20), 
            ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(1,":",20), 
            ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(1,":",20), pnd_Ivc_Pro_Adj, pnd_Per_Ivc_Pro_Adj, pnd_Ivc_Ind, pnd_Iaa_New_Issue, pnd_Full_Contract_Out, 
            pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, pdaIatl420z.getIatl010_Pnd_Todays_Dte(), ldaIatl171.getIatl171_Iaxfr_Effctve_Dte(), pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte(), 
            pnd_Next_Pay_Dte, pnd_Time, pnd_Eff_Dte_03_31.getValue(1,":",20), pnd_W3_Reject_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Filling_To_Side() throws Exception                                                                                                               //Natural: #FILLING-TO-SIDE
    {
        if (BLNatReinput.isReinput()) return;

        FS:                                                                                                                                                               //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(" ")))                                                                          //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = ' '
            {
                if (true) break FS;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FS. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                                    //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = IATL010.IA-TO-CNTRCT
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_To_Payee_N());                                                                  //Natural: ASSIGN #W-CNTRCT-PAYEE = IATL010.#IA-TO-PAYEE-N
            pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                                                                         //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-TO-ACCT-CDE ( #I )
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
            sub_Pnd_Convert_One_Byte_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                                          //Natural: IF IATL010.XFR-TO-UNIT-TYP ( #I ) = 'A'
            {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
                sub_Pnd_Annual_Fund_Cnv();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                sub_Pnd_Monthly_Cnv_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Divid().getValue(pnd_I).setValue(0);                                                                                   //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-DIVID ( #I ) := 0
            ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(pnd_I).setValue(0);                                                                                       //Natural: ASSIGN IATL171.IAXFR-TO-XFR-DIVID ( #I ) := 0
                                                                                                                                                                          //Natural: PERFORM #CHECK-NEW-FUND
            sub_Pnd_Check_New_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_On_File.equals("Y")))                                                                                                                       //Natural: IF #ON-FILE = 'Y'
            {
                ldaIatl171.getIatl171_Iaxfr_To_New_Fund_Rec().getValue(pnd_I).setValue(" ");                                                                              //Natural: ASSIGN IATL171.IAXFR-TO-NEW-FUND-REC ( #I ) := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl171.getIatl171_Iaxfr_To_New_Fund_Rec().getValue(pnd_I).setValue("Y");                                                                              //Natural: ASSIGN IATL171.IAXFR-TO-NEW-FUND-REC ( #I ) := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                                                                         //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-TO-ACCT-CDE ( #I )
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
            sub_Pnd_Convert_One_Byte_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_I).setValue(pnd_Two_Byte_Fund);                                                                        //Natural: ASSIGN IATL171.IAXFR-TO-FUND-CDE ( #I ) := #TWO-BYTE-FUND
            ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                                  //Natural: ASSIGN IATL171.IAXFR-TO-ACCT-CD ( #I ) := IATL010.XFR-TO-ACCT-CDE ( #I )
            ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_I));                                 //Natural: ASSIGN IATL171.IAXFR-TO-UNIT-TYP ( #I ) := IATL010.XFR-TO-UNIT-TYP ( #I )
            ldaIatl171.getIatl171_Iaxfr_To_Typ().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Typ().getValue(pnd_I));                                           //Natural: ASSIGN IATL171.IAXFR-TO-TYP ( #I ) := IATL010.XFR-TO-TYP ( #I )
            ldaIatl171.getIatl171_Iaxfr_To_Qty().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I));                                           //Natural: ASSIGN IATL171.IAXFR-TO-QTY ( #I ) := IATL010.XFR-TO-QTY ( #I )
            FH:                                                                                                                                                           //Natural: FOR #H = 1 TO 20
            for (pnd_H.setValue(1); condition(pnd_H.lessOrEqual(20)); pnd_H.nadd(1))
            {
                pnd_Rate_Code_Breakdown.setValue(pnd_Rate_Code_Table.getValue(pnd_H));                                                                                    //Natural: MOVE #RATE-CODE-TABLE ( #H ) TO #RATE-CODE-BREAKDOWN
                if (condition(pnd_Rate_Code_Breakdown_Pnd_Rate_Code_1.equals(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I))))                                  //Natural: IF #RATE-CODE-1 = IATL010.XFR-TO-ACCT-CDE ( #I )
                {
                    ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(pnd_I).setValue(pnd_Rate_Code_Breakdown_Pnd_Rate_Code_2_N);                                        //Natural: ASSIGN IATL171.IAXFR-TO-RATE-CDE ( #I ) := #RATE-CODE-2-N
                    if (true) break FH;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FH. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #CHECK-UNITS-LINKAGE
            sub_Pnd_Check_Units_Linkage();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Units_Linkage() throws Exception                                                                                                           //Natural: #CHECK-UNITS-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        FQ:                                                                                                                                                               //Natural: FOR #J = 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_Out().getValue(pnd_J)) //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = #AIAN019-CREF-FUND-OUT ( #J ) AND IATL010.XFR-TO-UNIT-TYP ( #I ) = #AIAN019-CREF-REVAL-METHOD ( #J )
                && pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_I).equals(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Reval_Method().getValue(pnd_J))))
            {
                ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_I).setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Auv().getValue(pnd_J));      //Natural: ASSIGN IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #I ) := #AIAN019-CREF-AUV ( #J )
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_I).nadd(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Pmt().getValue(pnd_J));                //Natural: ADD #AIAN019-CREF-PMT ( #J ) TO IATL171.IAXFR-TO-XFR-GUAR ( #I )
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(pnd_I).nadd(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Units().getValue(pnd_J));             //Natural: ADD #AIAN019-CREF-UNITS ( #J ) TO IATL171.IAXFR-TO-XFR-UNITS ( #I )
                ldaIatl171.getIatl171_Iaxfr_To_Asset_Amt().getValue(pnd_I).nadd(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue(pnd_J)); //Natural: ADD #AIAN019-TOTAL-TRANSFER-AMT-OUT ( #J ) TO IATL171.IAXFR-TO-ASSET-AMT ( #I )
                pnd_To_Asset_Amt.getValue(pnd_I).setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue(pnd_J));                       //Natural: ASSIGN #TO-ASSET-AMT ( #I ) := #AIAN019-TOTAL-TRANSFER-AMT-OUT ( #J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Load_Externalize_Table_Codes() throws Exception                                                                                                  //Natural: #LOAD-EXTERNALIZE-TABLE-CODES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_G.reset();                                                                                                                                                    //Natural: RESET #G #GG #GGG
        pnd_Gg.reset();
        pnd_Ggg.reset();
        //*  CALLNAT 'IAAN050F' #W-ALPHA-CDE(1:40) #W-RATE-CDE(1:40) #W-NM-CD(1:40)
        //*   #W-IA-STD-ALPHA-CD(1:40)
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_W_Alpha_Cde.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue("*"));                                                              //Natural: ASSIGN #W-ALPHA-CDE ( * ) := IAAA051Z.#IA-STD-ALPHA-CD ( * )
        pnd_W_Ia_Std_Alpha_Cd.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue("*"));                                                        //Natural: ASSIGN #W-IA-STD-ALPHA-CD ( * ) := IAAA051Z.#IA-STD-ALPHA-CD ( * )
        pnd_W_Nm_Cd.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"));                                                                     //Natural: ASSIGN #W-NM-CD ( * ) := IAAA051Z.#IA-STD-NM-CD ( * )
        pnd_W_Ia_Rpt_Cmpny_Cd_A.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Cmpny_Cd_A().getValue("*"));                                                    //Natural: ASSIGN #W-IA-RPT-CMPNY-CD-A ( * ) := IAAA051Z.#IA-RPT-CMPNY-CD-A ( * )
        //* *#W-RATE-CDE (*)           := IAAA051Z.#V2-RT-1(*)   /* 090814 START
        pnd_Prod_Cnt.reset();                                                                                                                                             //Natural: RESET #PROD-CNT
        FOR01:                                                                                                                                                            //Natural: FOR #F 1 #MAX-PRODS
        for (pnd_F.setValue(1); condition(pnd_F.lessOrEqual(pnd_Max_Prods)); pnd_F.nadd(1))
        {
            if (condition(pnd_W_Alpha_Cde.getValue(pnd_F).equals(" ")))                                                                                                   //Natural: IF #W-ALPHA-CDE ( #F ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prod_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #PROD-CNT
            pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A.getValue(pnd_F).setValueEdited(pdaIaaa051z.getIaaa051z_Pnd_V2_Rt_1().getValue(pnd_F),new ReportEditMask("99"));               //Natural: MOVE EDITED IAAA051Z.#V2-RT-1 ( #F ) ( EM = 99 ) TO #W-RATE-CDE-A ( #F )
            if (condition((pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A.getValue(pnd_F).equals("00") && ! ((pnd_W_Alpha_Cde.getValue(pnd_F).equals("T") || pnd_W_Alpha_Cde.getValue(pnd_F).equals("G")))))) //Natural: IF #W-RATE-CDE-A ( #F ) = '00' AND NOT #W-ALPHA-CDE ( #F ) = 'T' OR = 'G'
            {
                DbsUtil.callnat(Iatn421.class , getCurrentProcessState(), pnd_W_Nm_Cd.getValue(pnd_F), pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A.getValue(pnd_F));                  //Natural: CALLNAT 'IATN421' #W-NM-CD ( #F ) #W-RATE-CDE-A ( #F )
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  090814 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FFF:                                                                                                                                                              //Natural: FOR #F = 1 TO #PROD-CNT
        for (pnd_F.setValue(1); condition(pnd_F.lessOrEqual(pnd_Prod_Cnt)); pnd_F.nadd(1))
        {
            //*  IF #W-RATE-CDE(#F) NE 0
            //*  090814 END
            if (condition(pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A.getValue(pnd_F).notEquals("00") && pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A.getValue(pnd_F).notEquals(" ")))             //Natural: IF #W-RATE-CDE-A ( #F ) NE '00' AND #W-RATE-CDE-A ( #F ) NE ' '
            {
                pnd_G.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #G
                pnd_Rate_Code_Table.getValue(pnd_G).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Alpha_Cde.getValue(pnd_F), pnd_W_Rate_Cde_Pnd_W_Rate_Cde_A.getValue(pnd_F))); //Natural: COMPRESS #W-ALPHA-CDE ( #F ) #W-RATE-CDE-A ( #F ) INTO #RATE-CODE-TABLE ( #G ) LEAVING NO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W_Nm_Cd.getValue(pnd_F).equals("07")))                                                                                                      //Natural: IF #W-NM-CD ( #F ) = '07'
            {
                pnd_W_Nm_Cd.getValue(pnd_F).setValue("08");                                                                                                               //Natural: MOVE '08' TO #W-NM-CD ( #F )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_W_Nm_Cd.getValue(pnd_F).equals("08")))                                                                                                  //Natural: IF #W-NM-CD ( #F ) = '08'
                {
                    pnd_W_Nm_Cd.getValue(pnd_F).setValue("07");                                                                                                           //Natural: MOVE '07' TO #W-NM-CD ( #F )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W_Nm_Cd.getValue(pnd_F).equals("1 ")))                                                                                                      //Natural: IF #W-NM-CD ( #F ) = '1 '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Gg.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #GG
                pnd_Ctl_Fund_Cde.getValue(pnd_Gg).setValue(pnd_W_Nm_Cd.getValue(pnd_F));                                                                                  //Natural: MOVE #W-NM-CD ( #F ) TO #CTL-FUND-CDE ( #GG )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_F.greater(1)))                                                                                                                              //Natural: IF #F > 1
            {
                pnd_Ggg.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #GGG
                if (condition(pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).equals("L")))                                                                                         //Natural: IF #W-IA-STD-ALPHA-CD ( #F ) = 'L'
                {
                    pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).setValue("E");                                                                                                  //Natural: MOVE 'E' TO #W-IA-STD-ALPHA-CD ( #F )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).equals("E")))                                                                                     //Natural: IF #W-IA-STD-ALPHA-CD ( #F ) = 'E'
                    {
                        pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).setValue("L");                                                                                              //Natural: MOVE 'L' TO #W-IA-STD-ALPHA-CD ( #F )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Alpha_Cd_Table.getValue(pnd_Ggg).setValue(pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F));                                                                     //Natural: MOVE #W-IA-STD-ALPHA-CD ( #F ) TO #ALPHA-CD-TABLE ( #GGG )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Aian019_Linkage() throws Exception                                                                                                          //Natural: #FILL-AIAN019-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number().setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                //Natural: ASSIGN #AIAN019-CONTRACT-NUMBER := IATL010.IA-FRM-CNTRCT
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code().compute(new ComputeParameters(false, pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code()),    //Natural: ASSIGN #AIAN019-PAYEE-CODE := VAL ( IATL010.IA-FRM-PAYEE )
            pdaIatl420z.getIatl010_Ia_Frm_Payee().val());
        ldaIatl422.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #AIAN019-CONTRACT-NUMBER
        (
        "Z1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number(), WcType.WITH) },
        1
        );
        Z1:
        while (condition(ldaIatl422.getVw_iaa_Cntrct().readNextRow("Z1", true)))
        {
            ldaIatl422.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIatl422.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                       //Natural: IF NO RECORDS FOUND
            {
                pnd_W3_Reject_Cde.setValue("M1");                                                                                                                         //Natural: MOVE 'M1' TO #W3-REJECT-CDE
                if (true) break Z1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( Z1. )
            }                                                                                                                                                             //Natural: END-NOREC
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Option().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Optn_Cde());                                                 //Natural: ASSIGN #AIAN019-OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                 //Natural: ASSIGN #AIAN019-ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Yyyy_Mm().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Issue_Dte());                                         //Natural: ASSIGN #AIAN019-ISSUE-YYYY-MM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Day().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                          //Natural: ASSIGN #AIAN019-ISSUE-DAY := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            //*  OS 061510 START
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Day().equals(getZero())))                                                                  //Natural: IF #AIAN019-ISSUE-DAY = 0
            {
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Day().setValue(1);                                                                                   //Natural: ASSIGN #AIAN019-ISSUE-DAY := 1
                //*  OS 061510 END
            }                                                                                                                                                             //Natural: END-IF
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dob().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                                //Natural: ASSIGN #AIAN019-FIRST-ANN-DOB := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Sex().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                //Natural: ASSIGN #AIAN019-FIRST-ANN-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dod().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte());                                //Natural: ASSIGN #AIAN019-FIRST-ANN-DOD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dob().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte());                                //Natural: ASSIGN #AIAN019-SECOND-ANN-DOB := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Sex().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                //Natural: ASSIGN #AIAN019-SECOND-ANN-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dod().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte());                                //Natural: ASSIGN #AIAN019-SECOND-ANN-DOD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
            //*  TPA FROM IPRO
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(54) || pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(62)        //Natural: IF #AIAN019-ORIGIN = 54 OR = 62 OR = 74
                || pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(74)))
            {
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Tpa_Ipro_Issue_Date().setValue(ldaIatl422.getIaa_Cntrct_Cntrct_Ssnng_Dte());                               //Natural: ASSIGN #AIAN019-TPA-IPRO-ISSUE-DATE := IAA-CNTRCT.CNTRCT-SSNNG-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number());                                                     //Natural: ASSIGN #PPCN-NBR := #AIAN019-CONTRACT-NUMBER
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code());                                                         //Natural: ASSIGN #PAYEE-CDE := #AIAN019-PAYEE-CODE
        ldaIatl422.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseRead                                                                                                      //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "Y1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        Y1:
        while (condition(ldaIatl422.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("Y1")))
        {
            if (condition(ldaIatl422.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().notEquals(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number())        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE #AIAN019-CONTRACT-NUMBER OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE #AIAN019-PAYEE-CODE
                || ldaIatl422.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().notEquals(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code())))
            {
                pnd_W3_Reject_Cde.setValue("M2");                                                                                                                         //Natural: MOVE 'M2' TO #W3-REJECT-CDE
                if (true) break Y1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( Y1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIatl422.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9)))                                                                           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
            {
                pnd_W3_Reject_Cde.setValue("D1");                                                                                                                         //Natural: MOVE 'D1' TO #W3-REJECT-CDE
                if (true) break Y1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( Y1. )
            }                                                                                                                                                             //Natural: END-IF
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Mode().setValue(ldaIatl422.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                      //Natural: ASSIGN #AIAN019-MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Final_Per_Pay_Date().setValue(ldaIatl422.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());               //Natural: ASSIGN #AIAN019-FINAL-PER-PAY-DATE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Eff_Date.setValueEdited(pdaIatl420z.getIatl010_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED IATL010.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Effective_Date().setValue(pnd_Eff_Date_Pnd_Eff_Date_N);                                                   //Natural: ASSIGN #AIAN019-TRANSFER-EFFECTIVE-DATE := #EFF-DATE-N
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Type_Of_Run().setValue(" ");                                                                                       //Natural: ASSIGN #AIAN019-TYPE-OF-RUN := ' '
        pnd_Todays_Bus_Date_A.setValueEdited(pdaIatl420z.getIatl010_Pnd_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED IATL010.#TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-BUS-DATE-A
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Processing_Date().setValue(pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date);                                             //Natural: ASSIGN #AIAN019-PROCESSING-DATE := #TODAYS-BUS-DATE
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Pct().setValue(0);                                                                                            //Natural: ASSIGN #AIAN019-GBPM-PCT := 0
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Rate().setValue(0);                                                                                           //Natural: ASSIGN #AIAN019-GBPM-RATE := 0
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Prorate_Ind().setValue(0);                                                                                    //Natural: ASSIGN #AIAN019-GBPM-PRORATE-IND := 0
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Lst_Updte_Dte, pnd_Nxt_Updte_Dte);                                                                 //Natural: CALLNAT 'IAAN0020' #LST-UPDTE-DTE #NXT-UPDTE-DTE
        if (condition(Global.isEscape())) return;
        pnd_Wrk_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Nxt_Updte_Dte_Pnd_N_Yyyy, pnd_Nxt_Updte_Dte_Pnd_N_Mm, pnd_Nxt_Updte_Dte_Pnd_N_Dd));      //Natural: COMPRESS #N-YYYY #N-MM #N-DD INTO #WRK-DTE LEAVING NO SPACE
        DbsUtil.callnat(Nazn6032.class , getCurrentProcessState(), pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Next_Pymnt_Dte(), pnd_Wrk_Dte, pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Mode()); //Natural: CALLNAT 'NAZN6032' #AIAN019-NEXT-PYMNT-DTE #WRK-DTE #AIAN019-MODE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Check_New_Fund() throws Exception                                                                                                                //Natural: #CHECK-NEW-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("T") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))      //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'T' OR = 'G'
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-TIAA-NEW-FUND
            sub_Pnd_Check_Tiaa_New_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-CREF-NEW-FUND
            sub_Pnd_Check_Cref_New_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_Cref_New_Fund() throws Exception                                                                                                           //Natural: #CHECK-CREF-NEW-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File.reset();                                                                                                                                              //Natural: RESET #ON-FILE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIaal422.getVw_iaa_Cref_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-CREF-FUND BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Y",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1Y:
        while (condition(ldaIaal422.getVw_iaa_Cref_Fund().readNextRow("R1Y")))
        {
            //*       WRITE '=' CREF-CNTRCT-PPCN-NBR '=' CREF-CMPNY-FUND-CDE
            if (condition(ldaIaal422.getIaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal422.getIaa_Cref_Fund_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal422.getIaa_Cref_Fund_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_On_File.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #ON-FILE
                ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).setValue(ldaIaal422.getIaa_Cref_Fund_Cref_Units_Cnt().getValue(1));                        //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-UNITS ( #I ) := IAA-CREF-FUND.CREF-UNITS-CNT ( 1 )
                ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_I).setValue(ldaIaal422.getIaa_Cref_Fund_Cref_Tot_Per_Amt());                                   //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-GUAR ( #I ) := IAA-CREF-FUND.CREF-TOT-PER-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Tiaa_New_Fund() throws Exception                                                                                                           //Natural: #CHECK-TIAA-NEW-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File.reset();                                                                                                                                              //Natural: RESET #ON-FILE
        //*  ASSIGN #W-CNTRCT-PPCN-NBR = IATA010.IAA-TO-CNTRCT
        //*  ASSIGN #W-CNTRCT-PAYEE = IATA010.#IAA-TO-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIaal422.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Q",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1Q:
        while (condition(ldaIaal422.getVw_iaa_Tiaa_Fund().readNextRow("R1Q")))
        {
            if (condition(ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal422.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_On_File.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #ON-FILE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Annual_Fund_Cnv() throws Exception                                                                                                               //Natural: #ANNUAL-FUND-CNV
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet1841 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet1841++;
            //*  012109
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet1841++;
            pnd_Fund_Cd_1.setValue("U");                                                                                                                                  //Natural: MOVE 'U' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("2");                                                                                                                                  //Natural: MOVE '2' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_From_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                    //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FROM-FUND-TOT LEAVING NO
    }
    private void sub_Pnd_Monthly_Cnv_Fund() throws Exception                                                                                                              //Natural: #MONTHLY-CNV-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet1856 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet1856++;
            //*  012109
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet1856++;
            pnd_Fund_Cd_1.setValue("W");                                                                                                                                  //Natural: MOVE 'W' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("4");                                                                                                                                  //Natural: MOVE '4' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_From_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                    //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FROM-FUND-TOT LEAVING NO
    }
    private void sub_Pnd_Determine_Full_Tiaa_Real_Out() throws Exception                                                                                                  //Natural: #DETERMINE-FULL-TIAA-REAL-OUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Not_Full_Out.reset();                                                                                                                                         //Natural: RESET #NOT-FULL-OUT
        if (condition(ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1).equals(getZero()) && ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(2).equals(getZero()))) //Natural: IF IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1 ) = 0 AND IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 2 ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-FULL-OUT
            sub_Pnd_Check_If_Full_Out();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Not_Full_Out.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #NOT-FULL-OUT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Not_Full_Out.equals(" ")))                                                                                                                      //Natural: IF #NOT-FULL-OUT = ' '
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue("Y");                                                                                          //Natural: MOVE 'Y' TO #IATN420X-IN.#NEW-INACTIVE-IND
            pnd_Full_Contract_Out.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #FULL-CONTRACT-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #IATN420X-IN.#NEW-INACTIVE-IND
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  # OF FUNDS IN FROM CONTRACT
    private void sub_Pnd_Check_If_Full_Out() throws Exception                                                                                                             //Natural: #CHECK-IF-FULL-OUT
    {
        if (BLNatReinput.isReinput()) return;

        F1:                                                                                                                                                               //Natural: FOR #Q = 1 TO #A
        for (pnd_Q.setValue(1); condition(pnd_Q.lessOrEqual(pnd_A)); pnd_Q.nadd(1))
        {
            pnd_Found_Fund.reset();                                                                                                                                       //Natural: RESET #FOUND-FUND
            F2:                                                                                                                                                           //Natural: FOR #R = 1 TO 20
            for (pnd_R.setValue(1); condition(pnd_R.lessOrEqual(20)); pnd_R.nadd(1))
            {
                if (condition(pnd_From_Funds.getValue(pnd_Q).equals(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_R))))                                        //Natural: IF #FROM-FUNDS ( #Q ) = IATL171.IAXFR-FROM-FUND-CDE ( #R )
                {
                    pnd_Found_Fund.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #FOUND-FUND
                    if (true) break F2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F2. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Found_Fund.equals(" ")))                                                                                                                    //Natural: IF #FOUND-FUND = ' '
            {
                pnd_Not_Full_Out.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #NOT-FULL-OUT
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Aian019_Linkage() throws Exception                                                                                                         //Natural: #RESET-AIAN019-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_To_Cref_Arrays().getValue("*").reset();                                                                            //Natural: RESET #AIAN019-TO-CREF-ARRAYS ( * ) #AIAN019-TIAA-ARRAYS ( *,* ) #AIAN019-CREF-ARRAYS ( * ) #AIAN019-PAYMENT-METHOD
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Tiaa_Arrays().getValue("*","*").reset();
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Arrays().getValue("*").reset();
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payment_Method().reset();
    }
    private void sub_Pnd_Revalue_03_31() throws Exception                                                                                                                 //Natural: #REVALUE-03-31
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //* * WRITE 'REVALUE PARA' '='  IATL171.IAXFR-TO-BFR-XFR-UNITS(#I)
        if (condition(((pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("T"))     //Natural: IF ( IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G' OR = 'T' ) OR ( IATL010.XFR-TO-UNIT-TYP ( #I ) = 'M' AND IATL010.RCRD-TYPE-CDE = '1' )
            || (pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_I).equals("M") && pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("1")))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Diff.compute(new ComputeParameters(true, pnd_W_Diff), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).multiply(ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_I))); //Natural: COMPUTE ROUNDED #W-DIFF = IATL171.IAXFR-TO-AFTR-XFR-UNITS ( #I ) * IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #I )
            pnd_W_Diff2.compute(new ComputeParameters(false, pnd_W_Diff2), pnd_W_Diff.subtract(ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_I)));          //Natural: COMPUTE #W-DIFF2 = #W-DIFF - IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #I )
            //*    WRITE '=' #W-DIFF2 '=' IATL171.IAXFR-TO-AFTR-XFR-GUAR(#I)
            ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_I).nadd(pnd_W_Diff2);                                                                             //Natural: ADD #W-DIFF2 TO IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #I )
            ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_I).nadd(pnd_W_Diff2);                                                                                  //Natural: ADD #W-DIFF2 TO IATL171.IAXFR-TO-XFR-GUAR ( #I )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Fill_Monthly_Dollars() throws Exception                                                                                                          //Natural: #FILL-MONTHLY-DOLLARS
    {
        if (BLNatReinput.isReinput()) return;

        ZL:                                                                                                                                                               //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T).equals(" ")))                                                                         //Natural: IF IATL171.IAXFR-TO-FUND-CDE ( #T ) = ' '
            {
                if (true) break ZL;                                                                                                                                       //Natural: ESCAPE BOTTOM ( ZL. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T).equals("1G") || ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T).equals("1S"))) //Natural: IF IATL171.IAXFR-TO-FUND-CDE ( #T ) = '1G' OR = '1S'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(pnd_T).equals("M")))                                                                     //Natural: IF IATL171.IAXFR-TO-UNIT-TYP ( #T ) = 'M'
                {
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(true, ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE ROUNDED IATL171.IAXFR-TO-BFR-XFR-GUAR ( #T ) = IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #T ) * IATL171.IAXFR-TO-BFR-XFR-UNITS ( #T )
                        ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_T)));
                    ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(true, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE ROUNDED IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #T ) = IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #T ) * IATL171.IAXFR-TO-AFTR-XFR-UNITS ( #T )
                        ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_T)));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_Fund_Record() throws Exception                                                                                                              //Natural: #READ-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        pnd_A.reset();                                                                                                                                                    //Natural: RESET #A
        ldaIatl422.getVw_iaa_Tiaa_Fund_Rcrd_View().startDatabaseRead                                                                                                      //Natural: READ IAA-TIAA-FUND-RCRD-VIEW BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1X",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1X:
        while (condition(ldaIatl422.getVw_iaa_Tiaa_Fund_Rcrd_View().readNextRow("R1X")))
        {
            if (condition(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                pnd_A.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #A
                pnd_Fund_Break.setValue(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde());                                                                     //Natural: MOVE IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE TO #FUND-BREAK
                pnd_From_Funds.getValue(pnd_A).setValue(pnd_Fund_Break_Pnd_Funds_2);                                                                                      //Natural: MOVE #FUNDS-2 TO #FROM-FUNDS ( #A )
                //*    ACCEPT IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE = MASK('T')
                if (condition(!(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code))))                                //Natural: ACCEPT IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                {
                    continue;
                }
                DbsUtil.callnat(Iatn270a.class , getCurrentProcessState(), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue("*"),  //Natural: CALLNAT 'IATN270A' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( * ) #W-PCT
                    pnd_W_Pct);
                if (condition(Global.isEscape())) return;
                //*  02/10
                //*  02/10
                //*  02/10
                DbsUtil.callnat(Iatn270a.class , getCurrentProcessState(), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue("*"),  //Natural: CALLNAT 'IATN270A' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( * ) #W-PCT
                    pnd_W_Pct);
                if (condition(Global.isEscape())) return;
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Rate_Code().getValue(pnd_S,"*").setValue(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*")); //Natural: ASSIGN #AIAN019-RATE-CODE ( #S,* ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * )
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Date().getValue(pnd_S,"*").setValue(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte().getValue("*")); //Natural: ASSIGN #AIAN019-TRANSFER-DATE ( #S,* ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-DTE ( * )
                pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue("*").setValue(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue("*"));                                //Natural: ASSIGN #GTD-PMT ( * ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( * )
                pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue("*").setValue(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue("*"));                                //Natural: ASSIGN #DVD-PMT ( * ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( * )
                //*         #AIAN019-GTD-PMT(#S,*) :=
                //*             IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT(*)
                DbsUtil.callnat(Iatn270a.class , getCurrentProcessState(), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue("*"),  //Natural: CALLNAT 'IATN270A' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) #GTD-PMT ( * ) #W-PCT-TO
                    pnd_W_Pct_To);
                if (condition(Global.isEscape())) return;
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gtd_Pmt().getValue(pnd_S,"*").setValue(pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue("*"));                          //Natural: ASSIGN #AIAN019-GTD-PMT ( #S,* ) := #GTD-PMT ( * )
                //*  02/10
                //*  02/10
                //*  02/10
                DbsUtil.callnat(Iatn270a.class , getCurrentProcessState(), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue("*"),  //Natural: CALLNAT 'IATN270A' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) #DVD-PMT ( * ) #W-PCT-TO
                    pnd_W_Pct_To);
                if (condition(Global.isEscape())) return;
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Div_Pmt().getValue(pnd_S,"*").setValue(pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue("*"));                          //Natural: ASSIGN #AIAN019-DIV-PMT ( #S,* ) := #DVD-PMT ( * )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1X;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1X. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_From_Fund_Record() throws Exception                                                                                                         //Natural: #READ-FROM-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIatl422.getVw_iaa_Tiaa_Fund_Rcrd_View().startDatabaseRead                                                                                                      //Natural: READ IAA-TIAA-FUND-RCRD-VIEW BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1R",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1R:
        while (condition(ldaIatl422.getVw_iaa_Tiaa_Fund_Rcrd_View().readNextRow("R1R")))
        {
            if (condition(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*     ACCEPT IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE = MASK('T')
                if (condition(!(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code))))                                //Natural: ACCEPT IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                {
                    continue;
                }
                //*  040612 CHANGED FROM :99
                DbsUtil.callnat(Iatn270a.class , getCurrentProcessState(), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue("*"),  //Natural: CALLNAT 'IATN270A' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( * ) #W-PCT
                    pnd_W_Pct);
                if (condition(Global.isEscape())) return;
                ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T)),  //Natural: ASSIGN IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T ) := 0 + IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( * )
                    DbsField.add(getZero(),ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue("*")));
                pnd_Fr_Gtd_Pmt.getValue(pnd_T,1,":",250).setValue(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue(1,":",250));                          //Natural: ASSIGN #FR-GTD-PMT ( #T,1:250 ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( 1:250 )
                //*  040612 CHANGED FROM :99
                DbsUtil.callnat(Iatn270a.class , getCurrentProcessState(), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue("*"),  //Natural: CALLNAT 'IATN270A' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( * ) #W-PCT
                    pnd_W_Pct);
                if (condition(Global.isEscape())) return;
                ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Divid().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Divid().getValue(pnd_T)),  //Natural: ASSIGN IATL171.IAXFR-FROM-RQSTD-XFR-DIVID ( #T ) := 0 + IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( * )
                    DbsField.add(getZero(),ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue("*")));
                pnd_Fr_Dvd_Pmt.getValue(pnd_T,1,":",250).setValue(ldaIatl422.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue(1,":",250));                          //Natural: ASSIGN #FR-DVD-PMT ( #T,1:250 ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( 1:250 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1R;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1R. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Determine_If_New_Issue() throws Exception                                                                                                        //Natural: #DETERMINE-IF-NEW-ISSUE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Found_Cnt.reset();                                                                                                                                            //Natural: RESET #FOUND-CNT
        pnd_Cntrct_Nbr.setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                                                                   //Natural: ASSIGN #CNTRCT-NBR := IATL010.IA-TO-CNTRCT
        ldaIatl422.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-NBR
        (
        "FN",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Nbr, WcType.WITH) },
        1
        );
        FN:
        while (condition(ldaIatl422.getVw_iaa_Cntrct().readNextRow("FN")))
        {
            ldaIatl422.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            pnd_Found_Cnt.setValue("Y");                                                                                                                                  //Natural: MOVE 'Y' TO #FOUND-CNT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Found_Cnt.equals("Y")))                                                                                                                         //Natural: IF #FOUND-CNT = 'Y'
        {
            pnd_Iaa_New_Issue.setValue(" ");                                                                                                                              //Natural: MOVE ' ' TO #IAA-NEW-ISSUE
            pnd_Iaa_New_Issue_Phys.setValue(" ");                                                                                                                         //Natural: MOVE ' ' TO #IAA-NEW-ISSUE-PHYS
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Cref_Payee().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #IATN420X-IN.#NEW-CREF-PAYEE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Iaa_New_Issue.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #IAA-NEW-ISSUE
            pnd_Iaa_New_Issue_Phys.setValue("C");                                                                                                                         //Natural: MOVE 'C' TO #IAA-NEW-ISSUE-PHYS
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Cref_Payee().setValue("Y");                                                                                            //Natural: MOVE 'Y' TO #IATN420X-IN.#NEW-CREF-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Write_Audit() throws Exception                                                                                                                   //Natural: #WRITE-AUDIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(0, "=",ldaIatl171.getIatl171_Rcrd_Type_Cde(),NEWLINE,"=",ldaIatl171.getIatl171_Rqst_Id(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Calc_Unique_Id(), //Natural: WRITE '=' IATL171.RCRD-TYPE-CDE / '=' IATL171.RQST-ID / '=' IATL171.IAXFR-CALC-UNIQUE-ID / '=' IATL171.IAXFR-EFFCTVE-DTE / '=' IATL171.IAXFR-RQST-RCVD-DTE / '=' IATL171.IAXFR-RQST-RCVD-TME / '=' IATL171.IAXFR-INVRSE-EFFCTVE-DTE / '=' IATL171.IAXFR-INVRSE-RCVD-TME / '=' IATL171.IAXFR-ENTRY-DTE / '=' IATL171.IAXFR-ENTRY-TME / '=' IATL171.IAXFR-CALC-STTMNT-INDCTR / '=' IATL171.IAXFR-CALC-STATUS-CDE / '=' IATL171.IAXFR-CWF-WPID / '=' IATL171.IAXFR-CWF-LOG-DTE-TIME / '=' IATL171.IAXFR-FROM-PPCN-NBR / '=' IATL171.IAXFR-FROM-PAYEE-CDE / '=' IATL171.IAXFR-CALC-REJECT-CDE / '=' IATL171.IAXFR-OPN-CLSD-IND / '=' IATL171.IAXFR-IN-PROGRESS-IND / '=' IATL171.IAXFR-RETRY-CNT / '=' IATL171.C*IAXFR-CALC-FROM-ACCT-DATA / '=' IATL171.IAXFR-FROM-FUND-CDE ( 1:5 ) / '=' IATL171.IAXFR-FRM-ACCT-CD ( 1:5 ) / '=' IATL171.IAXFR-FRM-UNIT-TYP ( 1:5 ) / '=' IATL171.IAXFR-FROM-TYP ( 1:5 ) / '=' IATL171.IAXFR-FROM-QTY ( 1:5 ) / '=' IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( 1:5 ) / '=' IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( 1:5 ) / '=' IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( 1:5 ) / '=' IATL171.IAXFR-FROM-CURRENT-PMT-UNIT-VAL ( 1:5 ) / '=' IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( 1:5 ) / '=' IATL171.IAXFR-FROM-RQSTD-XFR-AMT ( 1:5 ) / '=' IATL171.IAXFR-FROM-RQSTD-XFR-UNITS ( 1:5 ) / '=' IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( 1:5 ) / '=' IATL171.IAXFR-FROM-RQSTD-XFR-DIVID ( 1:5 ) / '=' IATL171.IAXFR-FROM-ASSET-XFR-AMT ( 1:5 ) / '=' IATL171.IAXFR-FROM-PMT-AFTR-XFR ( 1:5 ) / '=' IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1:5 ) / '=' IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1:5 ) / '=' IATL171.IAXFR-FROM-AFTR-XFR-DIVID ( 1:5 ) / '=' IATL171.C*IAXFR-CALC-TO-ACCT-DATA / '=' IATL171.IAXFR-TO-FUND-CDE ( 1:5 ) / '=' IATL171.IAXFR-TO-ACCT-CD ( 1:5 ) / '=' IATL171.IAXFR-TO-UNIT-TYP ( 1:5 ) / '=' IATL171.IAXFR-TO-NEW-FUND-REC ( 1:5 ) / '=' IATL171.IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE ( 1:5 ) / '=' IATL171.IAXFR-TO-TYP ( 1:5 ) / '=' IATL171.IAXFR-TO-QTY ( 1:5 ) / '=' IATL171.IAXFR-TO-BFR-XFR-GUAR ( 1:5 ) / '=' IATL171.IAXFR-TO-BFR-XFR-DIVID ( 1:5 ) / '=' IATL171.IAXFR-TO-BFR-XFR-UNITS ( 1:5 ) / '=' IATL171.IAXFR-TO-REVAL-UNIT-VAL ( 1:5 ) / '=' IATL171.IAXFR-TO-XFR-UNITS ( 1:5 ) / '=' IATL171.IAXFR-TO-RATE-CDE ( 1:5 ) / '=' IATL171.IAXFR-TO-XFR-GUAR ( 1:5 ) /
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Effctve_Dte(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Rqst_Rcvd_Dte(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Rqst_Rcvd_Tme(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Invrse_Effctve_Dte(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Invrse_Rcvd_Tme(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Entry_Dte(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Entry_Tme(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Calc_Sttmnt_Indctr(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Calc_Status_Cde(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Cwf_Wpid(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Cwf_Log_Dte_Time(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Calc_Reject_Cde(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Opn_Clsd_Ind(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_In_Progress_Ind(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Retry_Cnt(),NEWLINE,"=",ldaIatl171.getIatl171_Count_Castiaxfr_Calc_From_Acct_Data(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(1,":",
            5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Typ().getValue(1,":",
            5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Qty().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Unit_Val().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Amt().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Units().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Divid().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Asset_Xfr_Amt().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Pmt_Aftr_Xfr().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Divid().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Count_Castiaxfr_Calc_To_Acct_Data(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(1,":",
            5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_New_Fund_Rec().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_New_Phys_Cntrct_Issue().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Typ().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Qty().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Divid().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(1,
            ":",5),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Asset_Amt().getValue(1,         //Natural: WRITE '=' IATL171.IAXFR-TO-XFR-DIVID ( 1:5 ) / '=' IATL171.IAXFR-TO-ASSET-AMT ( 1:5 ) / '=' IATL171.IAXFR-TO-AFTR-XFR-UNITS ( 1:5 ) / '=' IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:5 ) / '=' IATL171.IAXFR-TO-AFTR-XFR-DIVID ( 1:5 ) / '=' IATL171.IAXFR-CALC-TO-PPCN-NBR / '=' IATL171.IAXFR-CALC-TO-PAYEE-CDE / '=' IATL171.IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND / '=' IATL171.IAXFR-CALC-UNIT-VAL-DTE / '=' IATL171.IAXFR-CYCLE-DTE / '=' IATL171.IAXFR-ACCTNG-DTE / '=' IATL171.LST-CHNGE-DTE / '=' IATL171.RQST-XFR-TYPE / '=' IATL171.RQST-UNIT-CDE / '=' IATL171.IA-NEW-ISS-PRT-PULL /
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,
            ":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(1,":",5),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr(),
            NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_New_Issue_Ind(),NEWLINE,"=",
            ldaIatl171.getIatl171_Iaxfr_Calc_Unit_Val_Dte(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Cycle_Dte(),NEWLINE,"=",ldaIatl171.getIatl171_Iaxfr_Acctng_Dte(),
            NEWLINE,"=",ldaIatl171.getIatl171_Lst_Chnge_Dte(),NEWLINE,"=",ldaIatl171.getIatl171_Rqst_Xfr_Type(),NEWLINE,"=",ldaIatl171.getIatl171_Rqst_Unit_Cde(),
            NEWLINE,"=",ldaIatl171.getIatl171_Ia_New_Iss_Prt_Pull(),NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Display_Aian019_Linkage() throws Exception                                                                                                           //Natural: DISPLAY-AIAN019-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        getReports().write(0, "*** #AIAN019-LINKAGE ***");                                                                                                                //Natural: WRITE '*** #AIAN019-LINKAGE ***'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Payee(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Mode(),      //Natural: WRITE '=' #AIAN019-CONTRACT-PAYEE / '=' #AIAN019-MODE ( EM = 999 ) / '=' #AIAN019-PAYMENT-METHOD / '=' #AIAN019-ORIGIN ( EM = 99 ) / '=' #AIAN019-OPTION ( EM = 99 ) / '=' #AIAN019-ISSUE-DATE / '=' #AIAN019-TPA-IPRO-ISSUE-DATE / '=' #AIAN019-FINAL-PER-PAY-DATE / '=' #AIAN019-FIRST-ANN-DOB / '=' #AIAN019-FIRST-ANN-SEX / '=' #AIAN019-FIRST-ANN-DOD / '=' #AIAN019-SECOND-ANN-DOB / '=' #AIAN019-SECOND-ANN-SEX / '=' #AIAN019-SECOND-ANN-DOD / '=' #AIAN019-TRANSFER-EFFECTIVE-DATE / '=' #AIAN019-PROCESSING-DATE / '=' #AIAN019-TYPE-OF-RUN /
            new ReportEditMask ("999"),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payment_Method(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin(), 
            new ReportEditMask ("99"),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Option(), new ReportEditMask ("99"),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Date(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Tpa_Ipro_Issue_Date(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Final_Per_Pay_Date(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dob(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Sex(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dod(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dob(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Sex(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dod(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Effective_Date(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Processing_Date(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Type_Of_Run(),NEWLINE);
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #B 1 20
        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(20)); pnd_B.nadd(1))
        {
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_In().getValue(pnd_B).equals(" ")))                                                     //Natural: IF #AIAN019-CREF-FUND-IN ( #B ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_In().getValue(pnd_B),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Reval_Method().getValue(pnd_B)); //Natural: WRITE '=' #AIAN019-CREF-FUND-IN ( #B ) / '=' #AIAN019-CREF-REVAL-METHOD ( #B )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR03:                                                                                                                                                        //Natural: FOR #D 1 #MAX-RATE
            for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(pnd_Max_Rate)); pnd_D.nadd(1))
            {
                if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Rate_Code().getValue(pnd_B,pnd_D).equals(" ")))                                              //Natural: IF #AIAN019-RATE-CODE ( #B,#D ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Rate_Code().getValue(pnd_B,pnd_D),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gtd_Pmt().getValue(pnd_B, //Natural: WRITE '=' #AIAN019-RATE-CODE ( #B,#D ) / '=' #AIAN019-GTD-PMT ( #B,#D ) / '=' #AIAN019-DIV-PMT ( #B,#D ) / '=' #AIAN019-TRANSFER-DATE ( #B,#D ) /
                    pnd_D),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Div_Pmt().getValue(pnd_B,pnd_D),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Date().getValue(pnd_B,
                    pnd_D),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Next_Pymnt_Dte(),NEWLINE);                                                               //Natural: WRITE '=' #AIAN019-NEXT-PYMNT-DTE /
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #B 1 20
        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(20)); pnd_B.nadd(1))
        {
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_Out().getValue(pnd_B).equals(" ")))                                                    //Natural: IF #AIAN019-CREF-FUND-OUT ( #B ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_Out().getValue(pnd_B),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Units().getValue(pnd_B), //Natural: WRITE '=' #AIAN019-CREF-FUND-OUT ( #B ) / '=' #AIAN019-CREF-UNITS ( #B ) / '=' #AIAN019-CREF-AUV ( #B ) / '=' #AIAN019-CREF-PMT ( #B ) / '=' #AIAN019-TOTAL-TRANSFER-AMT-OUT ( #B )
                NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Auv().getValue(pnd_B),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Pmt().getValue(pnd_B),
                NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue(pnd_B));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Pct(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Rate(),       //Natural: WRITE '=' #AIAN019-GBPM-PCT / '=' #AIAN019-GBPM-RATE / '=' #AIAN019-GBPM-PRORATE-IND / '=' #AIAN019-RETURN-CODE
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Prorate_Ind(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Return_Code());
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
        Global.format(0, "LS=133 PS=56");
    }
}
