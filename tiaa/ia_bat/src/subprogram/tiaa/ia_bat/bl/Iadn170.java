/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:42:05 AM
**        * FROM NATURAL SUBPROGRAM : Iadn170
************************************************************
**        * FILE NAME            : Iadn170.java
**        * CLASS NAME           : Iadn170
**        * INSTANCE NAME        : Iadn170
************************************************************
***********************************************************************
* PROGRAM  : IADN170
* SYSTEM   : IAD
* TITLE    : PRODUCE TPA/IPRO & P/I FILE FOR QTRLY REPORTING
* CREATED  : JUNE 6, 2001
* FUNCTION : PRODUCE TPA/IPRO & P/I FILE FROM IA MASTER FOR QCOT QTRLY
* TITLE    : REPORTING
*
*
*
* CHANGED HISTORY
*  4/02         LOGIC TO CALL REPORT ADAM SWEEP TRANSACTION AMT & CNT
*               PREMIUM SWEEP (RESETTLE) IAIQ TRANS 60 TO PASS QTRLY
*               T1,T2  OR I1, I2 TRANS TO REFLECT NEW MONEY INTO THE
*               CONTRACT
*               DO SCAN ON  4/02
*
*
***********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadn170 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_W_Date;

    private DbsGroup pnd_W_Date__R_Field_1;
    private DbsField pnd_W_Date_Pnd_W_Wrk_Mm;
    private DbsField pnd_W_Date_Pnd_W_Wrk_Dd;
    private DbsField pnd_W_Date_Pnd_W_Wrk_Ccyy;
    private DbsField pnd_W_Check_Date;

    private DbsGroup pnd_W_Check_Date__R_Field_2;
    private DbsField pnd_W_Check_Date_Pnd_W_Check_Ccyy;
    private DbsField pnd_W_Check_Date_Pnd_W_Check_Mm;

    private DbsGroup pnd_W_Check_Date__R_Field_3;
    private DbsField pnd_W_Check_Date_Pnd_W_Check_Ccyymm;
    private DbsField ia_Mast_Rec;

    private DbsGroup ia_Mast_Rec__R_Field_4;
    private DbsField ia_Mast_Rec_High_Value;
    private DbsField ia_Mast_Rec_Tiaa_Active_Payee_Mitr;
    private DbsField ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr;
    private DbsField ia_Mast_Rec_Tiaa_Periodic_Div_Mitr;
    private DbsField ia_Mast_Rec_Tiaa_Final_Pmt_Mitr;
    private DbsField ia_Mast_Rec_Tpa_Active_Payees_Mitr;
    private DbsField ia_Mast_Rec_Tpa_Per_Amt;
    private DbsField ia_Mast_Rec_Tpa_Div_Amt;
    private DbsField ia_Mast_Rec_Ipro_Active_Payees_Mitr;
    private DbsField ia_Mast_Rec_Ipro_Per_Amt;
    private DbsField ia_Mast_Rec_Ipro_Per_Div;
    private DbsField ia_Mast_Rec_Ipro_Fin_Pmt;
    private DbsField ia_Mast_Rec_P_I_Active_Payees_Mitr;
    private DbsField ia_Mast_Rec_P_I_Per_Amt;
    private DbsField ia_Mast_Rec_P_I_Per_Div;
    private DbsField ia_Mast_Rec_P_I_Fin_Pmt;
    private DbsField ia_Mast_Rec_Inactive_Payees_Mitr;
    private DbsField pnd_W_Total_Record;

    private DbsGroup pnd_W_Total_Record__R_Field_5;
    private DbsField pnd_W_Total_Record_W_Ia_Record_Type_Cd_X;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Written_B;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Written_N;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Written_B;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Written_N;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Written_B;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Written_N;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Open_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Close_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Open_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Close_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Pullouts;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Pullouts;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts;

    private DbsGroup pnd_Summary_Report_Counters;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Issue;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Family_Income_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly;
    private DbsField pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_D_Flag_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt;
    private DbsField pnd_Gtot_Issues;
    private DbsField pnd_Tot_Pmt_Amt;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_W_Date = parameters.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.NUMERIC, 8);
        pnd_W_Date.setParameterOption(ParameterOption.ByReference);

        pnd_W_Date__R_Field_1 = parameters.newGroupInRecord("pnd_W_Date__R_Field_1", "REDEFINE", pnd_W_Date);
        pnd_W_Date_Pnd_W_Wrk_Mm = pnd_W_Date__R_Field_1.newFieldInGroup("pnd_W_Date_Pnd_W_Wrk_Mm", "#W-WRK-MM", FieldType.NUMERIC, 2);
        pnd_W_Date_Pnd_W_Wrk_Dd = pnd_W_Date__R_Field_1.newFieldInGroup("pnd_W_Date_Pnd_W_Wrk_Dd", "#W-WRK-DD", FieldType.NUMERIC, 2);
        pnd_W_Date_Pnd_W_Wrk_Ccyy = pnd_W_Date__R_Field_1.newFieldInGroup("pnd_W_Date_Pnd_W_Wrk_Ccyy", "#W-WRK-CCYY", FieldType.NUMERIC, 4);
        pnd_W_Check_Date = parameters.newFieldInRecord("pnd_W_Check_Date", "#W-CHECK-DATE", FieldType.NUMERIC, 6);
        pnd_W_Check_Date.setParameterOption(ParameterOption.ByReference);

        pnd_W_Check_Date__R_Field_2 = parameters.newGroupInRecord("pnd_W_Check_Date__R_Field_2", "REDEFINE", pnd_W_Check_Date);
        pnd_W_Check_Date_Pnd_W_Check_Ccyy = pnd_W_Check_Date__R_Field_2.newFieldInGroup("pnd_W_Check_Date_Pnd_W_Check_Ccyy", "#W-CHECK-CCYY", FieldType.NUMERIC, 
            4);
        pnd_W_Check_Date_Pnd_W_Check_Mm = pnd_W_Check_Date__R_Field_2.newFieldInGroup("pnd_W_Check_Date_Pnd_W_Check_Mm", "#W-CHECK-MM", FieldType.NUMERIC, 
            2);

        pnd_W_Check_Date__R_Field_3 = parameters.newGroupInRecord("pnd_W_Check_Date__R_Field_3", "REDEFINE", pnd_W_Check_Date);
        pnd_W_Check_Date_Pnd_W_Check_Ccyymm = pnd_W_Check_Date__R_Field_3.newFieldInGroup("pnd_W_Check_Date_Pnd_W_Check_Ccyymm", "#W-CHECK-CCYYMM", FieldType.NUMERIC, 
            6);
        ia_Mast_Rec = parameters.newFieldInRecord("ia_Mast_Rec", "IA-MAST-REC", FieldType.STRING, 100);
        ia_Mast_Rec.setParameterOption(ParameterOption.ByReference);

        ia_Mast_Rec__R_Field_4 = parameters.newGroupInRecord("ia_Mast_Rec__R_Field_4", "REDEFINE", ia_Mast_Rec);
        ia_Mast_Rec_High_Value = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_High_Value", "HIGH-VALUE", FieldType.STRING, 2);
        ia_Mast_Rec_Tiaa_Active_Payee_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Tiaa_Active_Payee_Mitr", "TIAA-ACTIVE-PAYEE-MITR", FieldType.PACKED_DECIMAL, 
            7);
        ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr", "TIAA-PERIODIC-CONT-MITR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Tiaa_Periodic_Div_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Tiaa_Periodic_Div_Mitr", "TIAA-PERIODIC-DIV-MITR", FieldType.PACKED_DECIMAL, 
            13, 2);
        ia_Mast_Rec_Tiaa_Final_Pmt_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Tiaa_Final_Pmt_Mitr", "TIAA-FINAL-PMT-MITR", FieldType.PACKED_DECIMAL, 
            13, 2);
        ia_Mast_Rec_Tpa_Active_Payees_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Tpa_Active_Payees_Mitr", "TPA-ACTIVE-PAYEES-MITR", FieldType.PACKED_DECIMAL, 
            7);
        ia_Mast_Rec_Tpa_Per_Amt = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Tpa_Per_Amt", "TPA-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Tpa_Div_Amt = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Tpa_Div_Amt", "TPA-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Ipro_Active_Payees_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Ipro_Active_Payees_Mitr", "IPRO-ACTIVE-PAYEES-MITR", 
            FieldType.PACKED_DECIMAL, 7);
        ia_Mast_Rec_Ipro_Per_Amt = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Ipro_Per_Amt", "IPRO-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Ipro_Per_Div = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Ipro_Per_Div", "IPRO-PER-DIV", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Ipro_Fin_Pmt = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Ipro_Fin_Pmt", "IPRO-FIN-PMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_P_I_Active_Payees_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_P_I_Active_Payees_Mitr", "P-I-ACTIVE-PAYEES-MITR", FieldType.PACKED_DECIMAL, 
            7);
        ia_Mast_Rec_P_I_Per_Amt = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_P_I_Per_Amt", "P-I-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_P_I_Per_Div = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_P_I_Per_Div", "P-I-PER-DIV", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_P_I_Fin_Pmt = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_P_I_Fin_Pmt", "P-I-FIN-PMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Inactive_Payees_Mitr = ia_Mast_Rec__R_Field_4.newFieldInGroup("ia_Mast_Rec_Inactive_Payees_Mitr", "INACTIVE-PAYEES-MITR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_W_Total_Record = parameters.newFieldInRecord("pnd_W_Total_Record", "#W-TOTAL-RECORD", FieldType.STRING, 118);
        pnd_W_Total_Record.setParameterOption(ParameterOption.ByReference);

        pnd_W_Total_Record__R_Field_5 = parameters.newGroupInRecord("pnd_W_Total_Record__R_Field_5", "REDEFINE", pnd_W_Total_Record);
        pnd_W_Total_Record_W_Ia_Record_Type_Cd_X = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Record_Type_Cd_X", "W-IA-RECORD-TYPE-CD-X", 
            FieldType.STRING, 1);
        pnd_W_Total_Record_W_Ia_Tpa_Written_B = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Written_B", "W-IA-TPA-WRITTEN-B", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Tpa_Written_N = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Written_N", "W-IA-TPA-WRITTEN-N", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Ipro_Written_B = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Written_B", "W-IA-IPRO-WRITTEN-B", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Ipro_Written_N = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Written_N", "W-IA-IPRO-WRITTEN-N", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_B = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Written_B", "W-IA-P&I-WRITTEN-B", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_N = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Written_N", "W-IA-P&I-WRITTEN-N", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Tpa_Open_Value = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Open_Value", "W-IA-TPA-OPEN-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Tpa_Close_Value = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Close_Value", "W-IA-TPA-CLOSE-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount", 
            "W-IA-TPA-NON-PREMIUM-AMOUNT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Ipro_Open_Value = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Open_Value", "W-IA-IPRO-OPEN-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Ipro_Close_Value = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Close_Value", "W-IA-IPRO-CLOSE-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount", 
            "W-IA-IPRO-NON-PREMIUM-AMOUNT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value", "W-IA-P&I-OPEN-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value", "W-IA-P&I-CLOSE-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount", 
            "W-IA-P&I-NON-PREMIUM-AMOUNT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Tpa_Pullouts = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Pullouts", "W-IA-TPA-PULLOUTS", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Ipro_Pullouts = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Pullouts", "W-IA-IPRO-PULLOUTS", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts = pnd_W_Total_Record__R_Field_5.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts", "W-IA-P&I-PULLOUTS", 
            FieldType.PACKED_DECIMAL, 11);

        pnd_Summary_Report_Counters = parameters.newGroupInRecord("pnd_Summary_Report_Counters", "#SUMMARY-REPORT-COUNTERS");
        pnd_Summary_Report_Counters.setParameterOption(ParameterOption.ByReference);
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue", "#TOT-OLD-ISSUE", 
            FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt", 
            "#TOT-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div", 
            "#TOT-OLD-ISSU-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issue = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Issue", "#TOT-NEW-ISSUE", 
            FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt", 
            "#TOT-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div", 
            "#TOT-NEW-ISSU-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees", 
            "#TPA-BEN-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt", 
            "#TOT-BEN-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div", "#TOT-BEN-DIV", 
            FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees", 
            "#P&I-ORGN3-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt", 
            "#TOT-ORGN3-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div", "#TOT-ORGN3-DIV", 
            FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected", 
            "#IA-NEW-ISSU-NOT-SELECTED", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt", 
            "#TOT-NEW-NOT-SEL-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div", 
            "#TOT-NEW-NOT-SEL-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Family_Income_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Family_Income_Payees", 
            "#FAMILY-INCOME-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt", 
            "#TOT-FMLY-INC-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div", 
            "#TOT-FMLY-INC-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly", 
            "#INACTIVE-PAYEE-SENT-TO-QTRLY", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other", 
            "#INACTIVE-PAYEE-OTHER", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees", 
            "#TOT-INACTIVE-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt", 
            "#TPA-NEW-ISSU-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt", 
            "#TPA-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt", 
            "#TPA-NEW-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt", 
            "#TPA-NEW-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value", 
            "#TPA-NEW-ISSU-COMM-VALUE", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2", 
            "#TPA-NON-PREMIUM-AMT-T1-T2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt", 
            "#TPA-TO-CASH-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash", "#TPA-TO-CASH", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt", 
            "#TPA-TO-ALT-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier", 
            "#TPA-TO-ALT-CARRIER", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt", 
            "#TPA-TO-TIAA-CREF-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt", 
            "#TPA-TO-TIAA-CREF-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt", 
            "#TPA-WITHDRAWAL-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8", 
            "#TPA-WITHDRAWAL-T8", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt", 
            "#TPA-REWRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt", 
            "#TPA-REWRITE-80-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt", 
            "#TPA-FLAG-N-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt", 
            "#TPA-OLD-CNTRCT-CNT", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt", 
            "#TPA-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt", 
            "#TPA-OLD-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt", 
            "#TPA-OLD-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value", 
            "#TPA-OLD-ISSU-COMM-VALUE", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value", 
            "#TPA-OLD-ISSU-CLS-VALUE", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt", 
            "#IPRO-NEW-ISSU-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt", 
            "#IPRO-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt", 
            "#IPRO-NEW-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt", 
            "#IPRO-NEW-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt", 
            "#IPRO-NEW-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt", 
            "#IPRO-OLD-ISSU-CNTRCT-CNT", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt", 
            "#IPRO-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt", 
            "#IPRO-OLD-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt", 
            "#IPRO-OLD-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt", 
            "#IPRO-OLD-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2", 
            "#IPRO-NON-PREMIUM-AMT-I1-I2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt", 
            "#IPRO-INTEREST-CRED-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred", 
            "#IPRO-INTEREST-CRED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt", "#IPRO-PMT-CNT", 
            FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt", "#IPRO-PMT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt", 
            "#IPRO-WITHDRAWAL-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8", 
            "#IPRO-WITHDRAWAL-I8", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt", 
            "#IPRO-REWRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_D_Flag_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_D_Flag_Amt", 
            "#IPRO-REWRITE-D-FLAG-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt", 
            "#P&I-NEW-ISSU-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt", 
            "#P&I-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt", 
            "#P&I-NEW-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt", 
            "#P&I-NEW-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt", 
            "#P&I-NEW-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt", 
            "#P&I-OLD-ISSU-CNTRCT-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt", 
            "#P&I-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt", 
            "#P&I-OLD-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt", 
            "#P&I-OLD-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt", 
            "#P&I-OLD-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1", 
            "#P&I-NON-PREMIUM-AMT-P1", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt", 
            "#P&I-INTEREST-CRED-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred", 
            "#P&I-INTEREST-CRED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt", 
            "#P&I-PMT-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt", 
            "#P&I-PMT-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt", 
            "#P&I-WITHDRAWAL-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8", 
            "#P&I-WITHDRAWAL-P8", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt", 
            "#P&I-REWRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt", 
            "#P&I-REWRITE-D-FLAG-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt", "#TPA-SWEEP-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt", "#TPA-SWEEP-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt", 
            "#IPRO-SWEEP-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt", 
            "#IPRO-SWEEP-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Gtot_Issues = localVariables.newFieldInRecord("pnd_Gtot_Issues", "#GTOT-ISSUES", FieldType.NUMERIC, 7);
        pnd_Tot_Pmt_Amt = localVariables.newFieldInRecord("pnd_Tot_Pmt_Amt", "#TOT-PMT-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Gtot_Issues.setInitialValue(0);
        pnd_Tot_Pmt_Amt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iadn170() throws Exception
    {
        super("Iadn170");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 56
        //* *******************************************                                                                                                                   //Natural: AT TOP OF PAGE ( 1 )
        //*  >>>> WRITE SUMMARY REPORTS
        //* *******************************************
        getReports().write(1, "                    QTRLY TRANSACTION TOTALS                   ",new TabSetting(75),"INPUT IA MASTER TRAILER TOTALS FOR IA CHECK DATE:",pnd_W_Check_Date,  //Natural: WRITE ( 1 ) '                    QTRLY TRANSACTION TOTALS                   ' 75T 'INPUT IA MASTER TRAILER TOTALS FOR IA CHECK DATE:' #W-CHECK-DATE ( EM = 9999/99 )
            new ReportEditMask ("9999/99"));
        if (Global.isEscape()) return;
        getReports().write(1, "---------------------------------------------------------------",new TabSetting(75),"---------------------------------------------------------"); //Natural: WRITE ( 1 ) '---------------------------------------------------------------' 75T '---------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(1, " TOTAL TPA CONTRACTUAL (B) RECORDS.......: ",new TabSetting(47),pnd_W_Total_Record_W_Ia_Tpa_Written_B, new ReportEditMask                  //Natural: WRITE ( 1 ) ' TOTAL TPA CONTRACTUAL (B) RECORDS.......: ' 47T W-IA-TPA-WRITTEN-B ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'TIAA ACTIVE-PAYEES........:' 111T TIAA-ACTIVE-PAYEE-MITR ( EM = Z,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(75),"TIAA ACTIVE-PAYEES........:",new TabSetting(111),ia_Mast_Rec_Tiaa_Active_Payee_Mitr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, " TOTAL TPA TRANSACTION (N) RECORDS.......: ",new TabSetting(47),pnd_W_Total_Record_W_Ia_Tpa_Written_N, new ReportEditMask                  //Natural: WRITE ( 1 ) ' TOTAL TPA TRANSACTION (N) RECORDS.......: ' 47T W-IA-TPA-WRITTEN-N ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'TIAA PERIODIC PAYMENT.....:' 103T TIAA-PERIODIC-CONT-MITR ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(75),"TIAA PERIODIC PAYMENT.....:",new TabSetting(103),ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, " TOTAL IPRO CONTRACTUAL (B) RECORDS......: ",new TabSetting(47),pnd_W_Total_Record_W_Ia_Ipro_Written_B, new ReportEditMask                 //Natural: WRITE ( 1 ) ' TOTAL IPRO CONTRACTUAL (B) RECORDS......: ' 47T W-IA-IPRO-WRITTEN-B ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'TIAA PERIODIC DIVIIDEND...:' 103T TIAA-PERIODIC-DIV-MITR ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(75),"TIAA PERIODIC DIVIIDEND...:",new TabSetting(103),ia_Mast_Rec_Tiaa_Periodic_Div_Mitr, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, " TOTAL IPRO TRANSACTION (N) RECORDS......: ",new TabSetting(47),pnd_W_Total_Record_W_Ia_Ipro_Written_N, new ReportEditMask                 //Natural: WRITE ( 1 ) ' TOTAL IPRO TRANSACTION (N) RECORDS......: ' 47T W-IA-IPRO-WRITTEN-N ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'TIAA FINAL PAYMENT........:' 103T TIAA-FINAL-PMT-MITR ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(75),"TIAA FINAL PAYMENT........:",new TabSetting(103),ia_Mast_Rec_Tiaa_Final_Pmt_Mitr, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, " TOTAL P&I CONTRACTUAL (B) RECORDS.......: ",new TabSetting(47),pnd_W_Total_Record_W_Ia_Pamp_I_Written_B, new ReportEditMask               //Natural: WRITE ( 1 ) ' TOTAL P&I CONTRACTUAL (B) RECORDS.......: ' 47T W-IA-P&I-WRITTEN-B ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'TPA ACTIVE PAYEES.........:' 111T TPA-ACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(75),"TPA ACTIVE PAYEES.........:",new TabSetting(111),ia_Mast_Rec_Tpa_Active_Payees_Mitr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  75T 'TIAA FINAL DIV............:'
        //*  108T  TIAA-FINAL-DIV-MITR     (EM=Z,ZZZ,ZZZ.99)
        getReports().write(1, " TOTAL P&I TRANSACTION (N) RECORDS.......: ",new TabSetting(47),pnd_W_Total_Record_W_Ia_Pamp_I_Written_N, new ReportEditMask               //Natural: WRITE ( 1 ) ' TOTAL P&I TRANSACTION (N) RECORDS.......: ' 47T W-IA-P&I-WRITTEN-N ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'TPA PER PAYMENT...........:' 103T TPA-PER-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(75),"TPA PER PAYMENT...........:",new TabSetting(103),ia_Mast_Rec_Tpa_Per_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  >>>>> PULLOUT COUNT
        getReports().write(1, " TOTAL TPA PULLOUTS......................: ",pnd_W_Total_Record_W_Ia_Tpa_Pullouts, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new              //Natural: WRITE ( 1 ) ' TOTAL TPA PULLOUTS......................: ' W-IA-TPA-PULLOUTS ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'TPA PER DIVIDEND..........:' 103T TPA-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            TabSetting(75),"TPA PER DIVIDEND..........:",new TabSetting(103),ia_Mast_Rec_Tpa_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, " TOTAL IPRO PULLOUTS.....................: ",pnd_W_Total_Record_W_Ia_Ipro_Pullouts, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new             //Natural: WRITE ( 1 ) ' TOTAL IPRO PULLOUTS.....................: ' W-IA-IPRO-PULLOUTS ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T 'IPRO ACTIVE PAYEES........:' 111T IPRO-ACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZ9 )
            TabSetting(75),"IPRO ACTIVE PAYEES........:",new TabSetting(111),ia_Mast_Rec_Ipro_Active_Payees_Mitr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, " TOTAL P&I PULLOUTS......................: ",pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,new   //Natural: WRITE ( 1 ) ' TOTAL P&I PULLOUTS......................: ' W-IA-P&I-PULLOUTS ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) / 75T 'IPRO PER PAYMENT..........:' 103T IPRO-PER-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            TabSetting(75),"IPRO PER PAYMENT..........:",new TabSetting(103),ia_Mast_Rec_Ipro_Per_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(75),"IPRO PER DIVIDEND.........:",new TabSetting(103),ia_Mast_Rec_Ipro_Per_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));   //Natural: WRITE ( 1 ) 75T 'IPRO PER DIVIDEND.........:' 103T IPRO-PER-DIV ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(75),"IPRO FINAL PAYMENT........:",new TabSetting(103),ia_Mast_Rec_Ipro_Fin_Pmt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));   //Natural: WRITE ( 1 ) 75T 'IPRO FINAL PAYMENT........:' 103T IPRO-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(75),"P&I ACTIVE PAYEES.........:",new TabSetting(111),ia_Mast_Rec_P_I_Active_Payees_Mitr, new ReportEditMask                 //Natural: WRITE ( 1 ) 75T 'P&I ACTIVE PAYEES.........:' 111T P-I-ACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZ9 )
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(75),"P&I PER PAYMENT...........:",new TabSetting(103),ia_Mast_Rec_P_I_Per_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));    //Natural: WRITE ( 1 ) 75T 'P&I PER PAYMENT...........:' 103T P-I-PER-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(75),"P&I PER DIVIDEND..........:",new TabSetting(103),ia_Mast_Rec_P_I_Per_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));    //Natural: WRITE ( 1 ) 75T 'P&I PER DIVIDEND..........:' 103T P-I-PER-DIV ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(75),"P&I FINAL PAYMENT.........:",new TabSetting(103),ia_Mast_Rec_P_I_Fin_Pmt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));    //Natural: WRITE ( 1 ) 75T 'P&I FINAL PAYMENT.........:' 103T P-I-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr.add(ia_Mast_Rec_Tiaa_Periodic_Div_Mitr));              //Natural: COMPUTE #TOT-PMT-AMT = TIAA-PERIODIC-CONT-MITR + TIAA-PERIODIC-DIV-MITR
        getReports().write(1, new TabSetting(75),"TIAA GUAR + DIV...........:",new TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));            //Natural: WRITE ( 1 ) 75T 'TIAA GUAR + DIV...........:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(75),"INACTIVE PAYEES...........:",new TabSetting(111),ia_Mast_Rec_Inactive_Payees_Mitr, new ReportEditMask                   //Natural: WRITE ( 1 ) 75T 'INACTIVE PAYEES...........:' 111T INACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZ9 ) /
            ("Z,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  >>>>>>>>>> TPA INFO <<<<<<<<<<<
        getReports().write(1, "          QTRLY TRANSACTION TOTALS             DOLLARS      COUNTS",new TabSetting(75),"INPUT IA MASTER TRAILER TOTALS FOR IA CHECK DATE:",pnd_W_Check_Date,  //Natural: WRITE ( 1 ) '          QTRLY TRANSACTION TOTALS             DOLLARS      COUNTS' 75T 'INPUT IA MASTER TRAILER TOTALS FOR IA CHECK DATE:' #W-CHECK-DATE ( EM = 9999/99 )
            new ReportEditMask ("9999/99"));
        if (Global.isEscape()) return;
        getReports().write(1, "------------------------------------   ----------------- -----------",new TabSetting(75),"---------------------------------------------------------"); //Natural: WRITE ( 1 ) '------------------------------------   ----------------- -----------' 75T '---------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL TPA OPENING VALUE............: ",pnd_W_Total_Record_W_Ia_Tpa_Open_Value, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new              //Natural: WRITE ( 1 ) 'TOTAL TPA OPENING VALUE............: ' W-IA-TPA-OPEN-VALUE ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 75T 'TIAA ACTIVE-PAYEES........:' 111T TIAA-ACTIVE-PAYEE-MITR ( EM = Z,ZZZ,ZZ9 )
            TabSetting(75),"TIAA ACTIVE-PAYEES........:",new TabSetting(111),ia_Mast_Rec_Tiaa_Active_Payee_Mitr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL TPA CLOSING VALUE............: ",pnd_W_Total_Record_W_Ia_Tpa_Close_Value, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new             //Natural: WRITE ( 1 ) 'TOTAL TPA CLOSING VALUE............: ' W-IA-TPA-CLOSE-VALUE ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 75T 'TIAA PERIODIC PAYMENT.....:' 103T TIAA-PERIODIC-CONT-MITR ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            TabSetting(75),"TIAA PERIODIC PAYMENT.....:",new TabSetting(103),ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  WRITE (1)
        //*   ' TOTAL TPA NON PREMIUM AMOUNT............: '
        //*   W-IA-TPA-NON-PREMIUM-AMOUNT (EM=ZZZ,ZZZ,ZZZ,ZZZ.99-)
        getReports().write(1, "TPA TRANSACTION RECORDS (N)               ",new TabSetting(75),"TIAA PERIODIC DIVIIDEND...:",new TabSetting(103),ia_Mast_Rec_Tiaa_Periodic_Div_Mitr,  //Natural: WRITE ( 1 ) 'TPA TRANSACTION RECORDS (N)               ' 75T 'TIAA PERIODIC DIVIIDEND...:' 103T TIAA-PERIODIC-DIV-MITR ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "   TPA NON PREMIUM AMOUNT (+)                 ",new TabSetting(75),"TIAA FINAL PAYMENT........:",new TabSetting(103),ia_Mast_Rec_Tiaa_Final_Pmt_Mitr,  //Natural: WRITE ( 1 ) '   TPA NON PREMIUM AMOUNT (+)                 ' 75T 'TIAA FINAL PAYMENT........:' 103T TIAA-FINAL-PMT-MITR ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  T1 & T2
        getReports().write(1, "           New Issues..............: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2, new                   //Natural: WRITE ( 1 ) '           New Issues..............: ' 39T #TPA-NON-PREMIUM-AMT-T1-T2 ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #TPA-NEW-ISSU-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T 'INACTIVE PAYEES...........:' 111T INACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZ9 )
            ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
            TabSetting(75),"INACTIVE PAYEES...........:",new TabSetting(111),ia_Mast_Rec_Inactive_Payees_Mitr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*   ADDED FOLLOWING 4/02
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr.add(ia_Mast_Rec_Tiaa_Periodic_Div_Mitr));              //Natural: COMPUTE #TOT-PMT-AMT = TIAA-PERIODIC-CONT-MITR + TIAA-PERIODIC-DIV-MITR
        //*  T1 & T2
        getReports().write(1, "           Adam Tpa Resettlements..: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt, new ReportEditMask                //Natural: WRITE ( 1 ) '           Adam Tpa Resettlements..: ' 39T #TPA-SWEEP-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #TPA-SWEEP-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T 'TIAA GUAR + DIV...........:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"TIAA GUAR + DIV...........:",new 
            TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*   END OF ADD 4/02
        getReports().write(1, "   TPA NON PREMIUM AMOUNT (-)                 ");                                                                                          //Natural: WRITE ( 1 ) '   TPA NON PREMIUM AMOUNT (-)                 '
        if (Global.isEscape()) return;
        //*  T5
        getReports().write(1, "           Transfer to Cash........: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash, new ReportEditMask                  //Natural: WRITE ( 1 ) '           Transfer to Cash........: ' 39T #TPA-TO-CASH ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #TPA-TO-CASH-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  T3
        getReports().write(1, "           Transfer to Alt Carrier.: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier, new ReportEditMask           //Natural: WRITE ( 1 ) '           Transfer to Alt Carrier.: ' 39T #TPA-TO-ALT-CARRIER ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #TPA-TO-ALT-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  T4, T6, T7
        getReports().write(1, "           Reinvestment Transfer...: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt, new ReportEditMask         //Natural: WRITE ( 1 ) '           Reinvestment Transfer...: ' 39T #TPA-TO-TIAA-CREF-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #TPA-TO-TIAA-CREF-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  T8
        getReports().write(1, "           Full Conversion.........: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8, new ReportEditMask            //Natural: WRITE ( 1 ) '           Full Conversion.........: ' 39T #TPA-WITHDRAWAL-T8 ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #TPA-WITHDRAWAL-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T 'Qtrly Active Payess------------------------------'
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),
            "Qtrly Active Payess------------------------------");
        if (Global.isEscape()) return;
        getReports().write(1, "NEW ISSUES - N FLAG................: ",new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt, new ReportEditMask               //Natural: WRITE ( 1 ) 'NEW ISSUES - N FLAG................: ' 58T #TPA-FLAG-N-CNT ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 75T '  Old Issues..............:' 111T #TOT-OLD-ISSUE ( EM = Z,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"  Old Issues..............:",new TabSetting(111),pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue, new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "REWRITE 80.........................: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt, new ReportEditMask           //Natural: WRITE ( 1 ) 'REWRITE 80.........................: ' 39T #TPA-REWRITE-80-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #TPA-REWRITE-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  New Issues..............:' 111T #TOT-NEW-ISSUE ( EM = Z,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"  New Issues..............:",new 
            TabSetting(111),pnd_Summary_Report_Counters_Pnd_Tot_New_Issue, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Gtot_Issues.compute(new ComputeParameters(false, pnd_Gtot_Issues), pnd_Summary_Report_Counters_Pnd_Tot_New_Issue.add(pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue)); //Natural: ASSIGN #GTOT-ISSUES := #TOT-NEW-ISSUE + #TOT-OLD-ISSUE
        //*  TPA
        getReports().write(1, "                         SUB TOTAL.: ",new TabSetting(39),pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount, new ReportEditMask               //Natural: WRITE ( 1 ) '                         SUB TOTAL.: ' 39T W-IA-TPA-NON-PREMIUM-AMOUNT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T W-IA-TPA-WRITTEN-N ( EM = ZZZ,ZZZ,ZZ9 ) 75T '             SUB TOTAL....:' 111T #GTOT-ISSUES ( EM = Z,ZZZ,ZZ9 ) /
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_W_Total_Record_W_Ia_Tpa_Written_N, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"             SUB TOTAL....:",new 
            TabSetting(111),pnd_Gtot_Issues, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        //*  >>>>> IPRO INFO <<<<<<<
        //*  FINAL-PMT-AMT
        getReports().write(1, "TOTAL IPRO OPENING VALUE...........: ",pnd_W_Total_Record_W_Ia_Ipro_Open_Value, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new             //Natural: WRITE ( 1 ) 'TOTAL IPRO OPENING VALUE...........: ' W-IA-IPRO-OPEN-VALUE ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 75T '  Family Icome Option.....:' 111T #FAMILY-INCOME-PAYEES ( EM = Z,ZZZ,ZZ9 )
            TabSetting(75),"  Family Icome Option.....:",new TabSetting(111),pnd_Summary_Report_Counters_Pnd_Family_Income_Payees, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  FINAL-PMT-AMT
        getReports().write(1, "TOTAL IPRO CLOSING VALUE...........: ",pnd_W_Total_Record_W_Ia_Ipro_Close_Value, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new            //Natural: WRITE ( 1 ) 'TOTAL IPRO CLOSING VALUE...........: ' W-IA-IPRO-CLOSE-VALUE ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 75T '  Beneficiary recs (RS>01)-'
            TabSetting(75),"  Beneficiary recs (RS>01)-");
        if (Global.isEscape()) return;
        getReports().write(1, "IPRO TRANSACTION RECORDS (N)              ",new TabSetting(75),"  not sent to qrtly.......:",new TabSetting(111),pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees,  //Natural: WRITE ( 1 ) 'IPRO TRANSACTION RECORDS (N)              ' 75T '  not sent to qrtly.......:' 111T #TPA-BEN-PAYEES ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "   IPRO NON PREMIUM AMOUNT (+)                ",new TabSetting(75),"  New issue NOT in Quarter:",new TabSetting(111),pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected,  //Natural: WRITE ( 1 ) '   IPRO NON PREMIUM AMOUNT (+)                ' 75T '  New issue NOT in Quarter:' 111T #IA-NEW-ISSU-NOT-SELECTED ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  I1 & I2
        getReports().write(1, "           New Issues..............: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2, new                  //Natural: WRITE ( 1 ) '           New Issues..............: ' 39T #IPRO-NON-PREMIUM-AMT-I1-I2 ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #IPRO-NEW-ISSU-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  P&I origin 03 not sent..:' 111T #P&I-ORGN3-PAYEES ( EM = Z,ZZZ,ZZ9 )
            ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
            TabSetting(75),"  P&I origin 03 not sent..:",new TabSetting(111),pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Gtot_Issues.reset();                                                                                                                                          //Natural: RESET #GTOT-ISSUES
        pnd_Gtot_Issues.compute(new ComputeParameters(false, pnd_Gtot_Issues), pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees.add(pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected).add(pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees).add(pnd_Summary_Report_Counters_Pnd_Family_Income_Payees)); //Natural: ASSIGN #GTOT-ISSUES := #TPA-BEN-PAYEES + #IA-NEW-ISSU-NOT-SELECTED + #P&I-ORGN3-PAYEES + #FAMILY-INCOME-PAYEES
        //*  I3
        //*  TOTAL CNTRCTS
        //*  NOT SELECTED
        getReports().write(1, "           Interest Credited.......: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred, new ReportEditMask           //Natural: WRITE ( 1 ) '           Interest Credited.......: ' 39T #IPRO-INTEREST-CRED ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #IPRO-INTEREST-CRED-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '             SUB TOTAL....:' 111T #GTOT-ISSUES ( EM = Z,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"             SUB TOTAL....:",new 
            TabSetting(111),pnd_Gtot_Issues, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*   ADDED FOLLOWING 4/02 PRINT SWEEP TRANSACTION TOTALS
        //*  I1 & I2
        getReports().write(1, "          Adam Ipro Resettlements..: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt, new ReportEditMask               //Natural: WRITE ( 1 ) '          Adam Ipro Resettlements..: ' 39T #IPRO-SWEEP-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #IPRO-SWEEP-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*   END OF ADD 4/02
        getReports().write(1, "   IPRO NON PREMIUM AMOUNT (-)                ",new TabSetting(75),"Qtrly Inactive Payess----------------------------");                   //Natural: WRITE ( 1 ) '   IPRO NON PREMIUM AMOUNT (-)                ' 75T 'Qtrly Inactive Payess----------------------------'
        if (Global.isEscape()) return;
        //*  I4
        getReports().write(1, "           Payment.................: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt, new ReportEditMask                 //Natural: WRITE ( 1 ) '           Payment.................: ' 39T #IPRO-PMT-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #IPRO-PMT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  Inactive Payees sent to  '
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),
            "  Inactive Payees sent to  ");
        if (Global.isEscape()) return;
        //*  I8
        getReports().write(1, "           Full Conversion.........: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8, new ReportEditMask           //Natural: WRITE ( 1 ) '           Full Conversion.........: ' 39T #IPRO-WITHDRAWAL-I8 ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #IPRO-WITHDRAWAL-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  Quarterly...............:' 111T #INACTIVE-PAYEE-SENT-TO-QTRLY ( EM = Z,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"  Quarterly...............:",new 
            TabSetting(111),pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other.compute(new ComputeParameters(false, pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other),                  //Natural: ASSIGN #INACTIVE-PAYEE-OTHER := #TOT-INACTIVE-PAYEES - #INACTIVE-PAYEE-SENT-TO-QTRLY
            pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees.subtract(pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly));
        getReports().write(1, "REWRITE - D Flag...................: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_D_Flag_Amt, new                     //Natural: WRITE ( 1 ) 'REWRITE - D Flag...................: ' 39T #IPRO-REWRITE-D-FLAG-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #IPRO-REWRITE-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  Other Inactive not sent.:' 111T #INACTIVE-PAYEE-OTHER ( EM = ZZZ,ZZZ,ZZ9 )
            ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
            TabSetting(75),"  Other Inactive not sent.:",new TabSetting(111),pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "                         SUB TOTAL.: ",new TabSetting(39),pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount, new ReportEditMask              //Natural: WRITE ( 1 ) '                         SUB TOTAL.: ' 39T W-IA-IPRO-NON-PREMIUM-AMOUNT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T W-IA-IPRO-WRITTEN-N ( EM = ZZZ,ZZZ,ZZ9 ) 75T '             SUB TOTAL....:' 111T #TOT-INACTIVE-PAYEES ( EM = Z,ZZZ,ZZ9 ) /
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_W_Total_Record_W_Ia_Ipro_Written_N, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"             SUB TOTAL....:",new 
            TabSetting(111),pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        //* ***
        //*  >>>>>> P&I INFO <<<<<<<
        //* ***
        getReports().write(1, "TOTAL P&I OPENING VALUE............: ",pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new           //Natural: WRITE ( 1 ) 'TOTAL P&I OPENING VALUE............: ' W-IA-P&I-OPEN-VALUE ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 75T 'Qtrly Active Payments Guar+Div-------------------'
            TabSetting(75),"Qtrly Active Payments Guar+Div-------------------");
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-OLD-ISSU-GUAR-PMT + #TOT-OLD-ISSU-DIV
        getReports().write(1, "TOTAL P&I CLOSING VALUE............: ",pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new          //Natural: WRITE ( 1 ) 'TOTAL P&I CLOSING VALUE............: ' W-IA-P&I-CLOSE-VALUE ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 75T '  Old Tot Payment.........:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            TabSetting(75),"  Old Tot Payment.........:",new TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-NEW-ISSU-GUAR-PMT + #TOT-NEW-ISSU-DIV
        getReports().write(1, "P&I TRANSACTION RECORDS (N)               ",new TabSetting(75),"  New Tot Payment.........:",new TabSetting(103),pnd_Tot_Pmt_Amt,          //Natural: WRITE ( 1 ) 'P&I TRANSACTION RECORDS (N)               ' 75T '  New Tot Payment.........:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-OLD-ISSU-GUAR-PMT + #TOT-OLD-ISSU-DIV + #TOT-NEW-ISSU-GUAR-PMT + #TOT-NEW-ISSU-DIV
        getReports().write(1, "   P&I NON PREMIUM AMOUNT (+)                 ",new TabSetting(75),"             SUB TOTAL....:",new TabSetting(103),pnd_Tot_Pmt_Amt,      //Natural: WRITE ( 1 ) '   P&I NON PREMIUM AMOUNT (+)                 ' 75T '             SUB TOTAL....:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  P1
        getReports().write(1, "           New Issue...............: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1, new                   //Natural: WRITE ( 1 ) '           New Issue...............: ' 39T #P&I-NON-PREMIUM-AMT-P1 ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #P&I-NEW-ISSU-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-FMLY-INC-GUAR-PMT + #TOT-FMLY-INC-DIV
        //*  P3
        getReports().write(1, "           Interest Credited.......: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred, new ReportEditMask         //Natural: WRITE ( 1 ) '           Interest Credited.......: ' 39T #P&I-INTEREST-CRED ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #P&I-INTEREST-CRED-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  Family Icome Option.....:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
            TabSetting(75),"  Family Icome Option.....:",new TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "   P&I NON PREMIUM AMOUNT (-)                 ",new TabSetting(75),"  Beneficiary recs (RS>01)-");                                         //Natural: WRITE ( 1 ) '   P&I NON PREMIUM AMOUNT (-)                 ' 75T '  Beneficiary recs (RS>01)-'
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-BEN-GUAR-PMT + #TOT-BEN-DIV
        //*  P4
        getReports().write(1, "           Payment.................: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt, new ReportEditMask               //Natural: WRITE ( 1 ) '           Payment.................: ' 39T #P&I-PMT-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #P&I-PMT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  not sent to qrtly.......:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"  not sent to qrtly.......:",new 
            TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-NEW-NOT-SEL-PMT + #TOT-NEW-NOT-SEL-DIV
        //*  P8
        getReports().write(1, "           Full Conversion.........: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8, new ReportEditMask         //Natural: WRITE ( 1 ) '           Full Conversion.........: ' 39T #P&I-WITHDRAWAL-P8 ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #P&I-WITHDRAWAL-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  New issue NOT in Quarter:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"  New issue NOT in Quarter:",new 
            TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-ORGN3-GUAR-PMT + #TOT-ORGN3-DIV
        getReports().write(1, "REWRITE - D Flag...................: ",new TabSetting(39),pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt, new                   //Natural: WRITE ( 1 ) 'REWRITE - D Flag...................: ' 39T #P&I-REWRITE-D-FLAG-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T #P&I-REWRITE-CNT ( EM = ZZZ,ZZZ,ZZ9 ) 75T '  P&I origin 03 not sent..:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
            TabSetting(75),"  P&I origin 03 not sent..:",new TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-BEN-GUAR-PMT + #TOT-BEN-DIV + #TOT-ORGN3-GUAR-PMT + #TOT-ORGN3-DIV + #TOT-NEW-NOT-SEL-PMT + #TOT-NEW-NOT-SEL-DIV + #TOT-FMLY-INC-GUAR-PMT + #TOT-FMLY-INC-DIV
        getReports().write(1, "                         SUB TOTAL.: ",new TabSetting(39),pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount, new ReportEditMask            //Natural: WRITE ( 1 ) '                         SUB TOTAL.: ' 39T W-IA-P&I-NON-PREMIUM-AMOUNT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99- ) 58T W-IA-P&I-WRITTEN-N ( EM = ZZZ,ZZZ,ZZ9 ) 75T '             SUB TOTAL....:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(58),pnd_W_Total_Record_W_Ia_Pamp_I_Written_N, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(75),"             SUB TOTAL....:",new 
            TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Tot_Pmt_Amt.compute(new ComputeParameters(false, pnd_Tot_Pmt_Amt), pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div).add(pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div)); //Natural: COMPUTE #TOT-PMT-AMT = #TOT-BEN-GUAR-PMT + #TOT-ORGN3-GUAR-PMT + #TOT-NEW-NOT-SEL-PMT + #TOT-FMLY-INC-GUAR-PMT + #TOT-NEW-ISSU-GUAR-PMT + #TOT-OLD-ISSU-GUAR-PMT + #TOT-NEW-ISSU-DIV + #TOT-OLD-ISSU-DIV + #TOT-NEW-NOT-SEL-DIV + #TOT-BEN-DIV + #TOT-ORGN3-DIV + #TOT-FMLY-INC-DIV
        getReports().write(1, NEWLINE,new TabSetting(75),"        TOTAL GUAR+DIV....:",new TabSetting(103),pnd_Tot_Pmt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));    //Natural: WRITE ( 1 ) / 75T '        TOTAL GUAR+DIV....:' 103T #TOT-PMT-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, "1) -------- TOTALS FOR TPA NEW ISSUES-------- ");                                                                                          //Natural: WRITE ( 1 ) '1) -------- TOTALS FOR TPA NEW ISSUES-------- '
        if (Global.isEscape()) return;
        getReports().write(1, "               TOTAL NEW ISSUE TPA CONTRACTS:  ",new TabSetting(57),pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt, new                  //Natural: WRITE ( 1 ) '               TOTAL NEW ISSUE TPA CONTRACTS:  ' 57T #TPA-NEW-ISSU-CNT ( EM = Z,ZZZ,ZZ9 )
            ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      TPA PER PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '                      TPA PER PAYMENT AMOUNT:  ' #TPA-NEW-ISSU-GUAR-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      TPA DIV PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt, new ReportEditMask                  //Natural: WRITE ( 1 ) '                      TPA DIV PAYMENT AMOUNT:  ' #TPA-NEW-ISSU-DIV-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                          TPA PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt, new ReportEditMask                  //Natural: WRITE ( 1 ) '                          TPA PAYMENT AMOUNT:  ' #TPA-NEW-ISSU-TOT-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                   TPA COMMUTED VALUE AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value, new ReportEditMask               //Natural: WRITE ( 1 ) '                   TPA COMMUTED VALUE AMOUNT:  ' #TPA-NEW-ISSU-COMM-VALUE ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) //
            ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "2) -------- TOTALS FOR TPA OLD ISSUES-------- ");                                                                                          //Natural: WRITE ( 1 ) '2) -------- TOTALS FOR TPA OLD ISSUES-------- '
        if (Global.isEscape()) return;
        getReports().write(1, "               TOTAL OLD ISSUE TPA CONTRACTS:  ",new TabSetting(57),pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt,                    //Natural: WRITE ( 1 ) '               TOTAL OLD ISSUE TPA CONTRACTS:  ' 57T #TPA-OLD-CNTRCT-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      TPA PER PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '                      TPA PER PAYMENT AMOUNT:  ' #TPA-OLD-ISSU-GUAR-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      TPA DIV PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt, new ReportEditMask                  //Natural: WRITE ( 1 ) '                      TPA DIV PAYMENT AMOUNT:  ' #TPA-OLD-ISSU-DIV-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                PER + DIV TPA PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt, new ReportEditMask                  //Natural: WRITE ( 1 ) '                PER + DIV TPA PAYMENT AMOUNT:  ' #TPA-OLD-ISSU-TOT-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "           TPA OPENING COMMUTED VALUE AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value, new ReportEditMask               //Natural: WRITE ( 1 ) '           TPA OPENING COMMUTED VALUE AMOUNT:  ' #TPA-OLD-ISSU-COMM-VALUE ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "           TPA CLOSING COMMUTED VALUE AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value, new ReportEditMask                //Natural: WRITE ( 1 ) '           TPA CLOSING COMMUTED VALUE AMOUNT:  ' #TPA-OLD-ISSU-CLS-VALUE ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) //
            ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "3) -------- TOTALS FOR IPRO NEW ISSUES-------- ");                                                                                         //Natural: WRITE ( 1 ) '3) -------- TOTALS FOR IPRO NEW ISSUES-------- '
        if (Global.isEscape()) return;
        getReports().write(1, "              TOTAL NEW ISSUE IPRO CONTRACTS:  ",new TabSetting(57),pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt,                     //Natural: WRITE ( 1 ) '              TOTAL NEW ISSUE IPRO CONTRACTS:  ' 57T #IPRO-NEW-ISSU-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "                     IPRO PER PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt, new ReportEditMask                //Natural: WRITE ( 1 ) '                     IPRO PER PAYMENT AMOUNT:  ' #IPRO-NEW-ISSU-GUAR-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                     IPRO DIV PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '                     IPRO DIV PAYMENT AMOUNT:  ' #IPRO-NEW-ISSU-DIV-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "               PER + DIV IPRO PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '               PER + DIV IPRO PAYMENT AMOUNT:  ' #IPRO-NEW-ISSU-TOT-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                   IPRO FINAL PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '                   IPRO FINAL PAYMENT AMOUNT:  ' #IPRO-NEW-ISSU-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) //
            ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "4) -------- TOTALS FOR IPRO OLD ISSUES-------- ");                                                                                         //Natural: WRITE ( 1 ) '4) -------- TOTALS FOR IPRO OLD ISSUES-------- '
        if (Global.isEscape()) return;
        getReports().write(1, "              TOTAL OLD ISSUE IPRO CONTRACTS:  ",new TabSetting(57),pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt,              //Natural: WRITE ( 1 ) '              TOTAL OLD ISSUE IPRO CONTRACTS:  ' 57T #IPRO-OLD-ISSU-CNTRCT-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "                     IPRO PER PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt, new ReportEditMask                //Natural: WRITE ( 1 ) '                     IPRO PER PAYMENT AMOUNT:  ' #IPRO-OLD-ISSU-GUAR-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                     IPRO DIV PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '                     IPRO DIV PAYMENT AMOUNT:  ' #IPRO-OLD-ISSU-DIV-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "               PER + DIV IPRO PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '               PER + DIV IPRO PAYMENT AMOUNT:  ' #IPRO-OLD-ISSU-TOT-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                   IPRO FINAL PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt, new ReportEditMask                 //Natural: WRITE ( 1 ) '                   IPRO FINAL PAYMENT AMOUNT:  ' #IPRO-OLD-ISSU-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) /
            ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "5) -------- TOTALS FOR P&I  NEW ISSUES-------- ");                                                                                         //Natural: WRITE ( 1 ) '5) -------- TOTALS FOR P&I  NEW ISSUES-------- '
        if (Global.isEscape()) return;
        getReports().write(1, "              TOTAL NEW ISSUE P&I  CONTRACTS:  ",new TabSetting(57),pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt,                   //Natural: WRITE ( 1 ) '              TOTAL NEW ISSUE P&I  CONTRACTS:  ' 57T #P&I-NEW-ISSU-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      P&I PER PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt, new ReportEditMask              //Natural: WRITE ( 1 ) '                      P&I PER PAYMENT AMOUNT:  ' #P&I-NEW-ISSU-GUAR-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      P&I DIV PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt, new ReportEditMask               //Natural: WRITE ( 1 ) '                      P&I DIV PAYMENT AMOUNT:  ' #P&I-NEW-ISSU-DIV-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "               PER + DIV IPRO PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt, new ReportEditMask               //Natural: WRITE ( 1 ) '               PER + DIV IPRO PAYMENT AMOUNT:  ' #P&I-NEW-ISSU-TOT-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                    P&I FINAL PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt, new ReportEditMask               //Natural: WRITE ( 1 ) '                    P&I FINAL PAYMENT AMOUNT:  ' #P&I-NEW-ISSU-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) //
            ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "6) -------- TOTALS FOR P&I  OLD ISSUES-------- ");                                                                                         //Natural: WRITE ( 1 ) '6) -------- TOTALS FOR P&I  OLD ISSUES-------- '
        if (Global.isEscape()) return;
        getReports().write(1, "              TOTAL OLD ISSUE P&I  CONTRACTS:  ",new TabSetting(57),pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt,            //Natural: WRITE ( 1 ) '              TOTAL OLD ISSUE P&I  CONTRACTS:  ' 57T#P&I-OLD-ISSU-CNTRCT-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      P&I PER PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt, new ReportEditMask              //Natural: WRITE ( 1 ) '                      P&I PER PAYMENT AMOUNT:  ' #P&I-OLD-ISSU-GUAR-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                      P&I DIV PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt, new ReportEditMask               //Natural: WRITE ( 1 ) '                      P&I DIV PAYMENT AMOUNT:  ' #P&I-OLD-ISSU-DIV-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "               PER + DIV P&I  PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt, new ReportEditMask               //Natural: WRITE ( 1 ) '               PER + DIV P&I  PAYMENT AMOUNT:  ' #P&I-OLD-ISSU-TOT-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "                    P&I FINAL PAYMENT AMOUNT:  ",pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt, new ReportEditMask               //Natural: WRITE ( 1 ) '                    P&I FINAL PAYMENT AMOUNT:  ' #P&I-OLD-ISSU-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new TabSetting(1),"Run Date: ",Global.getDATU(),new TabSetting(40),"SUMMARY OF SELECTED TPA, IPRO & P&I CONTRACTS FOR THE MONTH ENDING : ",pnd_W_Date,  //Natural: WRITE ( 1 ) 1T'Run Date: ' *DATU 40T'SUMMARY OF SELECTED TPA, IPRO & P&I CONTRACTS FOR THE MONTH ENDING : ' #W-DATE ( EM = 99-99-9999 ) / 'Program: ' *PROGRAM //
                        new ReportEditMask ("99-99-9999"),NEWLINE,"Program: ",Global.getPROGRAM(),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
