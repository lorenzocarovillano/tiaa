/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:46 AM
**        * FROM NATURAL SUBPROGRAM : Iaan702r
************************************************************
**        * FILE NAME            : Iaan702r.java
**        * CLASS NAME           : Iaan702r
**        * INSTANCE NAME        : Iaan702r
************************************************************
**********************************************************************
* PROGRAM - IAAN702R                                                 *
* PURPOSE - DETERMINE PEND DATE - USE OVERRIDE OR CALCULATED DATE    *
*         - APPLIES ONLY FOR PEND CODE A WHERE THE PAYEE IS NOT 02   *
*                                                                    *
* HISTORY - 08/2010 ORIGINAL CODE - JUN F. TINIO                     *
* 05/2014 - JUN TINIO - CREF/REA PROJECT. SEE 05/2014 FOR CHANGES    *
*         - ESCAPE ROUTINE IF THERE IS NO DATE OF DEATH              *
*         - CALCULATE PEND DATE FOR ALL PAYEES, NOT JUST BENES       *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan702r extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cntrct_Nbr;
    private DbsField pnd_Cntrct_Payee;
    private DbsField pnd_Dod_1;
    private DbsField pnd_Dod_2;
    private DbsField pnd_Mode;

    private DbsGroup pnd_Mode__R_Field_1;
    private DbsField pnd_Mode__Filler1;
    private DbsField pnd_Mode_Pnd_Mode_Mm;
    private DbsField pnd_Pend_Cde;
    private DbsField pnd_Pend_Dte;

    private DataAccessProgramView vw_cpr_Trans;
    private DbsField cpr_Trans_Bfre_Imge_Id;
    private DbsField cpr_Trans_Aftr_Imge_Id;
    private DbsField cpr_Trans_Trans_Dte;
    private DbsField cpr_Trans_Invrse_Trans_Dte;
    private DbsField cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Trans_Cntrct_Pend_Cde;
    private DbsField cpr_Trans_Cntrct_Pend_Dte;
    private DbsField cpr_Trans_Cntrct_Actvty_Cde;

    private DataAccessProgramView vw_trans;
    private DbsField trans_Trans_Ppcn_Nbr;
    private DbsField trans_Trans_Payee_Cde;
    private DbsField trans_Trans_Dte;
    private DbsField trans_Invrse_Trans_Dte;
    private DbsField trans_Trans_Cde;
    private DbsField trans_Trans_Sub_Cde;
    private DbsField trans_Trans_Verify_Cde;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_2;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Cde;
    private DbsField pnd_Cpr_Bfre_Key;

    private DbsGroup pnd_Cpr_Bfre_Key__R_Field_3;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_B_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_B_Part_Payee_Cde;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Cpr_Aftr_Key;

    private DbsGroup pnd_Cpr_Aftr_Key__R_Field_4;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_A_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_A_Part_Payee_Cde;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_A_Invrse_Dte;
    private DbsField pnd_A_Pend_Dte;
    private DbsField pnd_B_Pend_Dte;
    private DbsField pnd_T_Dte;

    private DbsGroup pnd_T_Dte__R_Field_5;
    private DbsField pnd_T_Dte_Pnd_T_Yyyy;
    private DbsField pnd_T_Dte_Pnd_T_Mm;
    private DbsField pnd_Interval;
    private DbsField pnd_Dod;

    private DbsGroup pnd_Dod__R_Field_6;
    private DbsField pnd_Dod_Pnd_Dod_Yyyy;
    private DbsField pnd_Dod_Pnd_Dod_Mm;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Cntrct_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Payee = parameters.newFieldInRecord("pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee.setParameterOption(ParameterOption.ByReference);
        pnd_Dod_1 = parameters.newFieldInRecord("pnd_Dod_1", "#DOD-1", FieldType.NUMERIC, 6);
        pnd_Dod_1.setParameterOption(ParameterOption.ByReference);
        pnd_Dod_2 = parameters.newFieldInRecord("pnd_Dod_2", "#DOD-2", FieldType.NUMERIC, 6);
        pnd_Dod_2.setParameterOption(ParameterOption.ByReference);
        pnd_Mode = parameters.newFieldInRecord("pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        pnd_Mode.setParameterOption(ParameterOption.ByReference);

        pnd_Mode__R_Field_1 = parameters.newGroupInRecord("pnd_Mode__R_Field_1", "REDEFINE", pnd_Mode);
        pnd_Mode__Filler1 = pnd_Mode__R_Field_1.newFieldInGroup("pnd_Mode__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Mode_Pnd_Mode_Mm = pnd_Mode__R_Field_1.newFieldInGroup("pnd_Mode_Pnd_Mode_Mm", "#MODE-MM", FieldType.NUMERIC, 2);
        pnd_Pend_Cde = parameters.newFieldInRecord("pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Pend_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Pend_Dte = parameters.newFieldInRecord("pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);
        pnd_Pend_Dte.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cpr_Trans = new DataAccessProgramView(new NameInfo("vw_cpr_Trans", "CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        cpr_Trans_Bfre_Imge_Id = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        cpr_Trans_Aftr_Imge_Id = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        cpr_Trans_Trans_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cpr_Trans_Invrse_Trans_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        cpr_Trans_Cntrct_Part_Payee_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr_Trans_Cntrct_Pend_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Trans_Cntrct_Pend_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        cpr_Trans_Cntrct_Actvty_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        registerRecord(vw_cpr_Trans);

        vw_trans = new DataAccessProgramView(new NameInfo("vw_trans", "TRANS"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        trans_Trans_Ppcn_Nbr = vw_trans.getRecord().newFieldInGroup("trans_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TRANS_PPCN_NBR");
        trans_Trans_Payee_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TRANS_PAYEE_CDE");
        trans_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        trans_Invrse_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "INVRSE_TRANS_DTE");
        trans_Trans_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TRANS_CDE");
        trans_Trans_Sub_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TRANS_SUB_CDE");
        trans_Trans_Verify_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_VERIFY_CDE");
        registerRecord(vw_trans);

        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Trans_Cntrct_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_2", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_2.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_2.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Cntrct_Key__R_Field_2.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde = pnd_Trans_Cntrct_Key__R_Field_2.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Cpr_Bfre_Key = localVariables.newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);

        pnd_Cpr_Bfre_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cpr_Bfre_Key__R_Field_3", "REDEFINE", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Bfre_Key_Pnd_B_Part_Ppcn_Nbr = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_B_Part_Ppcn_Nbr", "#B-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Bfre_Key_Pnd_B_Part_Payee_Cde = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_B_Part_Payee_Cde", "#B-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_Trans_Dte = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Cpr_Aftr_Key = localVariables.newFieldInRecord("pnd_Cpr_Aftr_Key", "#CPR-AFTR-KEY", FieldType.STRING, 25);

        pnd_Cpr_Aftr_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cpr_Aftr_Key__R_Field_4", "REDEFINE", pnd_Cpr_Aftr_Key);
        pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Aftr_Key_Pnd_A_Part_Ppcn_Nbr = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_A_Part_Ppcn_Nbr", "#A-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Aftr_Key_Pnd_A_Part_Payee_Cde = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_A_Part_Payee_Cde", "#A-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Aftr_Key_Pnd_A_Invrse_Dte = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_A_Invrse_Dte", "#A-INVRSE-DTE", FieldType.NUMERIC, 
            12);
        pnd_A_Pend_Dte = localVariables.newFieldInRecord("pnd_A_Pend_Dte", "#A-PEND-DTE", FieldType.NUMERIC, 6);
        pnd_B_Pend_Dte = localVariables.newFieldInRecord("pnd_B_Pend_Dte", "#B-PEND-DTE", FieldType.NUMERIC, 6);
        pnd_T_Dte = localVariables.newFieldInRecord("pnd_T_Dte", "#T-DTE", FieldType.NUMERIC, 6);

        pnd_T_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_T_Dte__R_Field_5", "REDEFINE", pnd_T_Dte);
        pnd_T_Dte_Pnd_T_Yyyy = pnd_T_Dte__R_Field_5.newFieldInGroup("pnd_T_Dte_Pnd_T_Yyyy", "#T-YYYY", FieldType.NUMERIC, 4);
        pnd_T_Dte_Pnd_T_Mm = pnd_T_Dte__R_Field_5.newFieldInGroup("pnd_T_Dte_Pnd_T_Mm", "#T-MM", FieldType.NUMERIC, 2);
        pnd_Interval = localVariables.newFieldInRecord("pnd_Interval", "#INTERVAL", FieldType.PACKED_DECIMAL, 3);
        pnd_Dod = localVariables.newFieldInRecord("pnd_Dod", "#DOD", FieldType.NUMERIC, 6);

        pnd_Dod__R_Field_6 = localVariables.newGroupInRecord("pnd_Dod__R_Field_6", "REDEFINE", pnd_Dod);
        pnd_Dod_Pnd_Dod_Yyyy = pnd_Dod__R_Field_6.newFieldInGroup("pnd_Dod_Pnd_Dod_Yyyy", "#DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_Dod_Pnd_Dod_Mm = pnd_Dod__R_Field_6.newFieldInGroup("pnd_Dod_Pnd_Dod_Mm", "#DOD-MM", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cpr_Trans.reset();
        vw_trans.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Cpr_Bfre_Key.setInitialValue("1");
        pnd_Cpr_Aftr_Key.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan702r() throws Exception
    {
        super("Iaan702r");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pnd_Pend_Cde.notEquals("A") || (pnd_Dod_1.equals(getZero()) && pnd_Dod_2.equals(getZero()))))                                                       //Natural: IF #PEND-CDE NE 'A' OR ( #DOD-1 = 0 AND #DOD-2 = 0 )
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE #DOD
        if (condition(pnd_Dod_1.greater(pnd_Dod_2)))                                                                                                                      //Natural: IF #DOD-1 GT #DOD-2
        {
            pnd_Dod.setValue(pnd_Dod_1);                                                                                                                                  //Natural: ASSIGN #DOD := #DOD-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Dod.setValue(pnd_Dod_2);                                                                                                                                  //Natural: ASSIGN #DOD := #DOD-2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(pnd_Cntrct_Nbr);                                                                                                 //Natural: ASSIGN #TRANS-PPCN-NBR := #CNTRCT-NBR
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde.setValue(pnd_Cntrct_Payee);                                                                                              //Natural: ASSIGN #TRANS-PAYEE-CDE := #CNTRCT-PAYEE
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte.reset();                                                                                                                //Natural: RESET #INVRSE-TRANS-DTE #TRANS-CDE #B-PEND-DTE
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde.reset();
        pnd_B_Pend_Dte.reset();
        vw_trans.startDatabaseRead                                                                                                                                        //Natural: READ TRANS BY TRANS-CNTRCT-KEY STARTING FROM #TRANS-CNTRCT-KEY
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trans_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_trans.readNextRow("READ01")))
        {
            if (condition(trans_Trans_Ppcn_Nbr.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr) || trans_Trans_Payee_Cde.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde))) //Natural: IF TRANS-PPCN-NBR NE #TRANS-PPCN-NBR OR TRANS-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(trans_Trans_Cde.equals(102) && trans_Trans_Verify_Cde.notEquals("V"))))                                                                       //Natural: ACCEPT IF TRANS-CDE = 102 AND TRANS-VERIFY-CDE NE 'V'
            {
                continue;
            }
            pnd_Cpr_Bfre_Key_Pnd_B_Part_Ppcn_Nbr.setValue(trans_Trans_Ppcn_Nbr);                                                                                          //Natural: ASSIGN #B-PART-PPCN-NBR := TRANS-PPCN-NBR
            pnd_Cpr_Bfre_Key_Pnd_B_Part_Payee_Cde.setValue(trans_Trans_Payee_Cde);                                                                                        //Natural: ASSIGN #B-PART-PAYEE-CDE := TRANS-PAYEE-CDE
            pnd_Cpr_Aftr_Key_Pnd_A_Part_Ppcn_Nbr.setValue(trans_Trans_Ppcn_Nbr);                                                                                          //Natural: ASSIGN #A-PART-PPCN-NBR := TRANS-PPCN-NBR
            pnd_Cpr_Aftr_Key_Pnd_A_Part_Payee_Cde.setValue(trans_Trans_Payee_Cde);                                                                                        //Natural: ASSIGN #A-PART-PAYEE-CDE := TRANS-PAYEE-CDE
            vw_cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
            (
            "READ02",
            new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Cpr_Aftr_Key, WcType.BY) },
            new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") }
            );
            READ02:
            while (condition(vw_cpr_Trans.readNextRow("READ02")))
            {
                if (condition(cpr_Trans_Aftr_Imge_Id.notEquals("2") || cpr_Trans_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Cpr_Aftr_Key_Pnd_A_Part_Ppcn_Nbr)                     //Natural: IF CPR-TRANS.AFTR-IMGE-ID NE '2' OR CPR-TRANS.CNTRCT-PART-PPCN-NBR NE #A-PART-PPCN-NBR OR CPR-TRANS.CNTRCT-PART-PAYEE-CDE NE #A-PART-PAYEE-CDE
                    || cpr_Trans_Cntrct_Part_Payee_Cde.notEquals(pnd_Cpr_Aftr_Key_Pnd_A_Part_Payee_Cde)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cpr_Trans_Cntrct_Pend_Cde.equals("0")))                                                                                                     //Natural: IF CNTRCT-PEND-CDE = '0'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cpr_Bfre_Key_Pnd_Trans_Dte.setValue(cpr_Trans_Trans_Dte);                                                                                             //Natural: ASSIGN #TRANS-DTE := CPR-TRANS.TRANS-DTE
                pnd_A_Pend_Dte.setValue(cpr_Trans_Cntrct_Pend_Dte);                                                                                                       //Natural: ASSIGN #A-PEND-DTE := CPR-TRANS.CNTRCT-PEND-DTE
                if (condition(trans_Trans_Sub_Cde.equals("066")))                                                                                                         //Natural: IF TRANS-SUB-CDE = '066'
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PEND-DTE
                    sub_Calculate_Pend_Dte();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-BEFORE-IMAGE
                sub_Get_Before_Image();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_B_Pend_Dte.equals(pnd_A_Pend_Dte) || pnd_B_Pend_Dte.greater(getZero())))                                                                //Natural: IF #B-PEND-DTE = #A-PEND-DTE OR #B-PEND-DTE GT 0
                {
                    pnd_Pend_Dte.setValue(pnd_A_Pend_Dte);                                                                                                                //Natural: ASSIGN #PEND-DTE := #A-PEND-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PEND-DTE
                    sub_Calculate_Pend_Dte();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      DISPLAY(1)
                    //*        #CNTRCT-NBR
                    //*        #CNTRCT-PAYEE
                    //*        #DOD
                    //*        #MODE
                    //*        #PEND-CDE
                    //*        #PEND-DTE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  NO SP TRANSACTION FOUND
        if (condition(pnd_A_Pend_Dte.equals(getZero())))                                                                                                                  //Natural: IF #A-PEND-DTE = 0
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PEND-DTE
            sub_Calculate_Pend_Dte();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BEFORE-IMAGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-PEND-DTE
        //* ***********************************************************************
    }
    private void sub_Get_Before_Image() throws Exception                                                                                                                  //Natural: GET-BEFORE-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_cpr_Trans.startDatabaseRead                                                                                                                                    //Natural: READ CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
        (
        "READ03",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Cpr_Bfre_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_cpr_Trans.readNextRow("READ03")))
        {
            if (condition(cpr_Trans_Bfre_Imge_Id.notEquals("1") || cpr_Trans_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Cpr_Bfre_Key_Pnd_B_Part_Ppcn_Nbr) || cpr_Trans_Cntrct_Part_Payee_Cde.notEquals(pnd_Cpr_Bfre_Key_Pnd_B_Part_Payee_Cde)  //Natural: IF CPR-TRANS.BFRE-IMGE-ID NE '1' OR CPR-TRANS.CNTRCT-PART-PPCN-NBR NE #B-PART-PPCN-NBR OR CPR-TRANS.CNTRCT-PART-PAYEE-CDE NE #B-PART-PAYEE-CDE OR CPR-TRANS.TRANS-DTE NE #TRANS-DTE
                || cpr_Trans_Trans_Dte.notEquals(pnd_Cpr_Bfre_Key_Pnd_Trans_Dte)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_B_Pend_Dte.setValue(cpr_Trans_Cntrct_Pend_Dte);                                                                                                           //Natural: ASSIGN #B-PEND-DTE := CPR-TRANS.CNTRCT-PEND-DTE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Pend_Dte() throws Exception                                                                                                                //Natural: CALCULATE-PEND-DTE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_T_Dte.setValue(pnd_Dod);                                                                                                                                      //Natural: ASSIGN #T-DTE := #DOD
        short decideConditionsMet178 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #MODE;//Natural: VALUE 100
        if (condition((pnd_Mode.equals(100))))
        {
            decideConditionsMet178++;
            pnd_Interval.setValue(1);                                                                                                                                     //Natural: ASSIGN #INTERVAL := 01
        }                                                                                                                                                                 //Natural: VALUE 601 : 603
        else if (condition(((pnd_Mode.greaterOrEqual(601) && pnd_Mode.lessOrEqual(603)))))
        {
            decideConditionsMet178++;
            pnd_Interval.setValue(3);                                                                                                                                     //Natural: ASSIGN #INTERVAL := 03
        }                                                                                                                                                                 //Natural: VALUE 701 : 712
        else if (condition(((pnd_Mode.greaterOrEqual(701) && pnd_Mode.lessOrEqual(712)))))
        {
            decideConditionsMet178++;
            pnd_Interval.setValue(6);                                                                                                                                     //Natural: ASSIGN #INTERVAL := 06
        }                                                                                                                                                                 //Natural: VALUE 801 : 812
        else if (condition(((pnd_Mode.greaterOrEqual(801) && pnd_Mode.lessOrEqual(812)))))
        {
            decideConditionsMet178++;
            pnd_Interval.setValue(12);                                                                                                                                    //Natural: ASSIGN #INTERVAL := 12
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  IF NOT MONTHLY MODE
        //*  START MONTH BASED ON THE MODE MONTH
        if (condition(pnd_Mode_Pnd_Mode_Mm.greater(getZero())))                                                                                                           //Natural: IF #MODE-MM GT 0
        {
            pnd_T_Dte_Pnd_T_Mm.setValue(pnd_Mode_Pnd_Mode_Mm);                                                                                                            //Natural: ASSIGN #T-MM := #MODE-MM
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  FIRST DATE GREATER THAN DOD
            if (condition(pnd_T_Dte.greater(pnd_Dod)))                                                                                                                    //Natural: IF #T-DTE GT #DOD
            {
                pnd_Pend_Dte.setValue(pnd_T_Dte);                                                                                                                         //Natural: ASSIGN #PEND-DTE := #T-DTE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_T_Dte_Pnd_T_Mm.nadd(pnd_Interval);                                                                                                                        //Natural: ADD #INTERVAL TO #T-MM
            if (condition(pnd_T_Dte_Pnd_T_Mm.greater(12)))                                                                                                                //Natural: IF #T-MM GT 12
            {
                pnd_T_Dte_Pnd_T_Yyyy.nadd(1);                                                                                                                             //Natural: ADD 1 TO #T-YYYY
                pnd_T_Dte_Pnd_T_Mm.nsubtract(12);                                                                                                                         //Natural: SUBTRACT 12 FROM #T-MM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //
}
