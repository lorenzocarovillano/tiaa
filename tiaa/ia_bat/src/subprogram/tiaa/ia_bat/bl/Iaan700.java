/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:43 AM
**        * FROM NATURAL SUBPROGRAM : Iaan700
************************************************************
**        * FILE NAME            : Iaan700.java
**        * CLASS NAME           : Iaan700
**        * INSTANCE NAME        : Iaan700
************************************************************
************************************************************************
*
* PROGRAM:    IAAN700
* FUNCTION:   THIS ROUTINE READS THE IA LEGAL OVERRIDE AND DETERMINES
*             THE CURRENCY BEING REQUESTED FOR GLOBAL EFT PAY.
* DATE:       12/19/2001
* HISTORY:    12/19/2001 - JUN F. TINIO - ORIGINAL CODE.
*
*
************************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan700 extends BLNatBase
{
    // Data Areas
    private LdaIagl100 ldaIagl100;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Low_Record_Key;

    private DbsGroup pnd_Low_Record_Key__R_Field_1;
    private DbsField pnd_Low_Record_Key_Pnd_Low_Contract;
    private DbsField pnd_Low_Record_Key_Pnd_Low_Payee;
    private DbsField pnd_Low_Record_Key_Pnd_Low_Seq_Num;
    private DbsField pnd_Low_Common_Data;

    private DbsGroup pnd_Low_Common_Data__R_Field_2;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Status;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Entry_Date;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Entry_Oper;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Effect_Date;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Pay_Date;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Pay_Status;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Geo_Code;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Ia_Name;
    private DbsField pnd_Low_Common_Data_Pnd_Low_Slack_4;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIagl100 = new LdaIagl100();
        registerRecord(ldaIagl100);
        registerRecord(ldaIagl100.getVw_legal_Override_Rec_View());

        // parameters
        parameters = new DbsRecord();
        pnd_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Ppcn_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Payee_Cde = parameters.newFieldInRecord("pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Pymnt_Pay_Type_Req_Ind = parameters.newFieldInRecord("pnd_Pymnt_Pay_Type_Req_Ind", "#PYMNT-PAY-TYPE-REQ-IND", FieldType.STRING, 1);
        pnd_Pymnt_Pay_Type_Req_Ind.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Low_Record_Key = localVariables.newFieldInRecord("pnd_Low_Record_Key", "#LOW-RECORD-KEY", FieldType.STRING, 13);

        pnd_Low_Record_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Low_Record_Key__R_Field_1", "REDEFINE", pnd_Low_Record_Key);
        pnd_Low_Record_Key_Pnd_Low_Contract = pnd_Low_Record_Key__R_Field_1.newFieldInGroup("pnd_Low_Record_Key_Pnd_Low_Contract", "#LOW-CONTRACT", FieldType.STRING, 
            8);
        pnd_Low_Record_Key_Pnd_Low_Payee = pnd_Low_Record_Key__R_Field_1.newFieldInGroup("pnd_Low_Record_Key_Pnd_Low_Payee", "#LOW-PAYEE", FieldType.STRING, 
            2);
        pnd_Low_Record_Key_Pnd_Low_Seq_Num = pnd_Low_Record_Key__R_Field_1.newFieldInGroup("pnd_Low_Record_Key_Pnd_Low_Seq_Num", "#LOW-SEQ-NUM", FieldType.STRING, 
            3);
        pnd_Low_Common_Data = localVariables.newFieldInRecord("pnd_Low_Common_Data", "#LOW-COMMON-DATA", FieldType.STRING, 67);

        pnd_Low_Common_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Low_Common_Data__R_Field_2", "REDEFINE", pnd_Low_Common_Data);
        pnd_Low_Common_Data_Pnd_Low_Status = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Status", "#LOW-STATUS", FieldType.STRING, 
            1);
        pnd_Low_Common_Data_Pnd_Low_Entry_Date = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Entry_Date", "#LOW-ENTRY-DATE", 
            FieldType.STRING, 8);
        pnd_Low_Common_Data_Pnd_Low_Entry_Oper = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Entry_Oper", "#LOW-ENTRY-OPER", 
            FieldType.STRING, 4);
        pnd_Low_Common_Data_Pnd_Low_Effect_Date = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Effect_Date", "#LOW-EFFECT-DATE", 
            FieldType.STRING, 8);
        pnd_Low_Common_Data_Pnd_Low_Pay_Date = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Pay_Date", "#LOW-PAY-DATE", 
            FieldType.STRING, 8);
        pnd_Low_Common_Data_Pnd_Low_Pay_Status = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Pay_Status", "#LOW-PAY-STATUS", 
            FieldType.STRING, 1);
        pnd_Low_Common_Data_Pnd_Low_Geo_Code = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Geo_Code", "#LOW-GEO-CODE", 
            FieldType.STRING, 3);
        pnd_Low_Common_Data_Pnd_Low_Ia_Name = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Ia_Name", "#LOW-IA-NAME", FieldType.STRING, 
            30);
        pnd_Low_Common_Data_Pnd_Low_Slack_4 = pnd_Low_Common_Data__R_Field_2.newFieldInGroup("pnd_Low_Common_Data_Pnd_Low_Slack_4", "#LOW-SLACK-4", FieldType.STRING, 
            4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIagl100.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan700() throws Exception
    {
        super("Iaan700");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Low_Record_Key_Pnd_Low_Contract.setValue(pnd_Cntrct_Ppcn_Nbr);                                                                                                //Natural: ASSIGN #LOW-CONTRACT := #CNTRCT-PPCN-NBR
        pnd_Low_Record_Key_Pnd_Low_Payee.setValueEdited(pnd_Cntrct_Payee_Cde,new ReportEditMask("99"));                                                                   //Natural: MOVE EDITED #CNTRCT-PAYEE-CDE ( EM = 99 ) TO #LOW-PAYEE
        pnd_Low_Record_Key_Pnd_Low_Seq_Num.setValue("19I");                                                                                                               //Natural: ASSIGN #LOW-SEQ-NUM := '19I'
        pnd_Pymnt_Pay_Type_Req_Ind.setValue(" ");                                                                                                                         //Natural: ASSIGN #PYMNT-PAY-TYPE-REQ-IND := ' '
        ldaIagl100.getVw_legal_Override_Rec_View().startDatabaseFind                                                                                                      //Natural: FIND LEGAL-OVERRIDE-REC-VIEW WITH LO-RECORD-KEY = #LOW-RECORD-KEY
        (
        "FIND01",
        new Wc[] { new Wc("LO_RECORD_KEY", "=", pnd_Low_Record_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaIagl100.getVw_legal_Override_Rec_View().readNextRow("FIND01")))
        {
            ldaIagl100.getVw_legal_Override_Rec_View().setIfNotFoundControlFlag(false);
            pnd_Low_Common_Data.setValue(ldaIagl100.getLegal_Override_Rec_View_Lo_Common_Data_X());                                                                       //Natural: MOVE LO-COMMON-DATA-X TO #LOW-COMMON-DATA
            if (condition(pnd_Low_Common_Data_Pnd_Low_Status.equals("A")))                                                                                                //Natural: IF #LOW-STATUS = 'A'
            {
                if (condition(pnd_Low_Common_Data_Pnd_Low_Slack_4.equals("USD")))                                                                                         //Natural: IF #LOW-SLACK-4 = 'USD'
                {
                    pnd_Pymnt_Pay_Type_Req_Ind.setValue("4");                                                                                                             //Natural: ASSIGN #PYMNT-PAY-TYPE-REQ-IND := '4'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Pymnt_Pay_Type_Req_Ind.setValue("9");                                                                                                             //Natural: ASSIGN #PYMNT-PAY-TYPE-REQ-IND := '9'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
