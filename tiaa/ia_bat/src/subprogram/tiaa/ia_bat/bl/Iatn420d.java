/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:22 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420d
************************************************************
**        * FILE NAME            : Iatn420d.java
**        * CLASS NAME           : Iatn420d
**        * INSTANCE NAME        : Iatn420d
************************************************************
************************************************************************
* PROGRAM : IATN170D
* FUNCTION: CALCULATE IVC
* DATE    : FEB 21, 1996
*
* HISTORY : JUN 1999  KN  CHANGED TO HANDLE TIAA TO CREF TRANSFERS
*
*         05/10/2002  TD
*         01/08/09    OS  TIAA ACCESS CHANGES. SC 010809.
*         04/06/12    OS  RESTOWED FOR RATE BASE CHANGES.
*         04/2017     OS  RE-STOWED FOR PIN EXPANSION.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420d extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl420x pdaIatl420x;
    private LdaIaal050 ldaIaal050;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ivc_Pro_Adj;
    private DbsField pnd_Per_Ivc_Pro_Adj;
    private DbsField pnd_Ivc_Ind;
    private DbsField pnd_Ret_Cde;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;

    private DbsGroup cpr_Cntrct_Company_Data;
    private DbsField cpr_Cntrct_Company_Cd;
    private DbsField cpr_Cntrct_Rcvry_Type_Ind;
    private DbsField cpr_Cntrct_Per_Ivc_Amt;
    private DbsField cpr_Cntrct_Ivc_Amt;
    private DbsField cpr_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Cntrct_Fund_Payee_Key;

    private DbsGroup pnd_Cntrct_Fund_Payee_Key__R_Field_2;
    private DbsField pnd_Cntrct_Fund_Payee_Key_Pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Fund_Payee_Key__R_Field_3;
    private DbsField pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Payee_Key_Pnd_Payee_Cde;
    private DbsField pnd_Cntrct_Fund_Payee_Key_Pnd_Fund_Cde;
    private DbsField pnd_Pay_Amt;
    private DbsField pnd_Frm_Amt;
    private DbsField pnd_To_Amt;
    private DbsField pnd_Xfr_Amt;
    private DbsField pnd_Pct;
    private DbsField pnd_Ivc_Diff;
    private DbsField pnd_I;
    private DbsField pnd_Transfer_From;
    private DbsField pnd_Cntrct_Rcvry_Type_Ind;
    private DbsField pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Warn_Ind;
    private DbsField pnd_Reas_Cde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl420x = new PdaIatl420x(parameters);
        pnd_Ivc_Pro_Adj = parameters.newFieldInRecord("pnd_Ivc_Pro_Adj", "#IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Pro_Adj.setParameterOption(ParameterOption.ByReference);
        pnd_Per_Ivc_Pro_Adj = parameters.newFieldInRecord("pnd_Per_Ivc_Pro_Adj", "#PER-IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Ivc_Pro_Adj.setParameterOption(ParameterOption.ByReference);
        pnd_Ivc_Ind = parameters.newFieldInRecord("pnd_Ivc_Ind", "#IVC-IND", FieldType.STRING, 1);
        pnd_Ivc_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Ret_Cde = parameters.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.STRING, 2);
        pnd_Ret_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_1", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");

        cpr_Cntrct_Company_Data = vw_cpr.getRecord().newGroupInGroup("cpr_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Company_Cd = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Rcvry_Type_Ind = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Per_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Ivc_Used_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        registerRecord(vw_cpr);

        pnd_Cntrct_Fund_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Payee_Key", "#CNTRCT-FUND-PAYEE-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Payee_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Payee_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Fund_Payee_Key);
        pnd_Cntrct_Fund_Payee_Key_Pnd_Cntrct_Payee_Key = pnd_Cntrct_Fund_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Payee_Key_Pnd_Cntrct_Payee_Key", 
            "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Fund_Payee_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Payee_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Fund_Payee_Key);
        pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Fund_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Fund_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Payee_Key_Pnd_Fund_Cde = pnd_Cntrct_Fund_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Payee_Key_Pnd_Fund_Cde", "#FUND-CDE", 
            FieldType.STRING, 3);
        pnd_Pay_Amt = localVariables.newFieldInRecord("pnd_Pay_Amt", "#PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Frm_Amt = localVariables.newFieldInRecord("pnd_Frm_Amt", "#FRM-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_To_Amt = localVariables.newFieldInRecord("pnd_To_Amt", "#TO-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Amt = localVariables.newFieldInRecord("pnd_Xfr_Amt", "#XFR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Pct = localVariables.newFieldInRecord("pnd_Pct", "#PCT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Diff = localVariables.newFieldInRecord("pnd_Ivc_Diff", "#IVC-DIFF", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Transfer_From = localVariables.newFieldInRecord("pnd_Transfer_From", "#TRANSFER-FROM", FieldType.BOOLEAN, 1);
        pnd_Cntrct_Rcvry_Type_Ind = localVariables.newFieldInRecord("pnd_Cntrct_Rcvry_Type_Ind", "#CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1);
        pnd_Cntrct_Per_Ivc_Amt = localVariables.newFieldInRecord("pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Warn_Ind = localVariables.newFieldInRecord("pnd_Warn_Ind", "#WARN-IND", FieldType.STRING, 1);
        pnd_Reas_Cde = localVariables.newFieldInRecord("pnd_Reas_Cde", "#REAS-CDE", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cntrct.reset();
        vw_cpr.reset();

        ldaIaal050.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420d() throws Exception
    {
        super("Iatn420d");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 5 ) PS = 66 LS = 133
        pnd_Ivc_Pro_Adj.reset();                                                                                                                                          //Natural: RESET #IVC-PRO-ADJ #PER-IVC-PRO-ADJ #IVC-IND
        pnd_Per_Ivc_Pro_Adj.reset();
        pnd_Ivc_Ind.reset();
        //*  CALLNAT 'IAAN0500' IAAA0500
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        //*  WITHIN  CONTRACT
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr().equals(" ")))                                                                             //Natural: IF #IAA-XFR-AUDIT.IAXFR-CALC-TO-PPCN-NBR = ' '
        {
            pnd_Ivc_Ind.setValue("N");                                                                                                                                    //Natural: ASSIGN #IVC-IND := 'N'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SWITCH
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Rcrd_Type_Cde().equals("2")))                                                                                      //Natural: IF #IAA-XFR-AUDIT.RCRD-TYPE-CDE = '2'
        {
            pnd_Ivc_Ind.setValue("N");                                                                                                                                    //Natural: ASSIGN #IVC-IND := 'N'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr());                                                          //Natural: ASSIGN #PPCN-NBR := #IAA-XFR-AUDIT.IAXFR-FROM-PPCN-NBR
        pnd_Cntrct_Fund_Payee_Key_Pnd_Payee_Cde.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Payee_Cde());                                                        //Natural: ASSIGN #PAYEE-CDE := #IAA-XFR-AUDIT.IAXFR-FROM-PAYEE-CDE
        pnd_Transfer_From.setValue(true);                                                                                                                                 //Natural: ASSIGN #TRANSFER-FROM := TRUE
                                                                                                                                                                          //Natural: PERFORM GET-MASTER-INFO
        sub_Get_Master_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Reas_Cde.equals("99") || pnd_Warn_Ind.equals("Y") || pnd_Reas_Cde.equals("02") || pnd_Ivc_Ind.equals("N")))                                     //Natural: IF #REAS-CDE = '99' OR #WARN-IND = 'Y' OR #REAS-CDE = '02' OR #IVC-IND = 'N'
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Transfer_From.setValue(false);                                                                                                                                //Natural: ASSIGN #TRANSFER-FROM := FALSE
        pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr());                                                       //Natural: ASSIGN #PPCN-NBR := #IAA-XFR-AUDIT.IAXFR-CALC-TO-PPCN-NBR
        pnd_Cntrct_Fund_Payee_Key_Pnd_Payee_Cde.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde());                                                     //Natural: ASSIGN #PAYEE-CDE := #IAA-XFR-AUDIT.IAXFR-CALC-TO-PAYEE-CDE
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind().notEquals("Y")))                                                                //Natural: IF #IAA-XFR-AUDIT.IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND NE 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM GET-MASTER-INFO
            sub_Get_Master_Info();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Warn_Ind.equals("Y") || pnd_Reas_Cde.equals("02") || pnd_Ivc_Ind.equals("N")))                                                              //Natural: IF #WARN-IND = 'Y' OR #REAS-CDE = '02' OR #IVC-IND = 'N'
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE TRANSFER AMOUNT
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals("  ")))                                                           //Natural: IF #IAA-XFR-AUDIT.IAXFR-FROM-FUND-CDE ( #I ) = '  '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*   FROM TIAA
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF #IAA-XFR-AUDIT.IAXFR-FROM-FUND-CDE ( #I )
            short decideConditionsMet310 = 0;                                                                                                                             //Natural: VALUE '1G' : '1S'
            if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals("1G' : '1S"))))
            {
                decideConditionsMet310++;
                pnd_Xfr_Amt.nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_I));                                                           //Natural: COMPUTE ROUNDED #XFR-AMT = #XFR-AMT + #IAA-XFR-AUDIT.IAXFR-FROM-RQSTD-XFR-GUAR ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ().getValue(pnd_I).equals("M")))                                                         //Natural: IF #IAA-XFR-AUDIT.IAXFR-FRM-UNIT-TYP ( #I ) = 'M'
                {
                    pnd_Xfr_Amt.compute(new ComputeParameters(true, pnd_Xfr_Amt), pnd_Xfr_Amt.add((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units().getValue(pnd_I).multiply(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val().getValue(pnd_I))))); //Natural: COMPUTE ROUNDED #XFR-AMT = #XFR-AMT + ( #IAA-XFR-AUDIT.IAXFR-FROM-RQSTD-XFR-UNITS ( #I ) * #IAA-XFR-AUDIT.IAXFR-FROM-REVAL-UNIT-VAL ( #I ) )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Xfr_Amt.nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_I));                                                       //Natural: COMPUTE ROUNDED #XFR-AMT = #XFR-AMT + #IAA-XFR-AUDIT.IAXFR-FROM-RQSTD-XFR-GUAR ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  DETERMINE TO TRANSFER AMOUNT BACK TO THE SAME CONTRACT
        //*  FROM 6L7 6M7 6N7 ALL MUST BE ACROSS CONTRACT TO CREF OR TIAA
        pnd_Pay_Amt.reset();                                                                                                                                              //Natural: RESET #PAY-AMT
        if (condition(DbsUtil.maskMatches(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr(),"'6L7'") || DbsUtil.maskMatches(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr(),"'6M7'")  //Natural: IF #IAA-XFR-AUDIT.IAXFR-FROM-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' ) OR ( #IAA-XFR-AUDIT.IAXFR-FROM-FUND-CDE ( 1 ) = '1G' OR = '1S' )
            || DbsUtil.maskMatches(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr(),"'6N7'") || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(1).equals("1G") 
            || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(1).equals("1S")))
        {
            pnd_To_Amt.setValue(pnd_Xfr_Amt);                                                                                                                             //Natural: ASSIGN #TO-AMT := #XFR-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  DETERMINE TRANSFER AMOUNT TO CREF
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                //*  010809
                if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals("1G") || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals("1S")  //Natural: IF #IAA-XFR-AUDIT.IAXFR-TO-FUND-CDE ( #I ) = '1G' OR = '1S' OR = '09' OR = '11'
                    || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals("09") || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals("11")))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("M")))                                                      //Natural: IF #IAA-XFR-AUDIT.IAXFR-TO-UNIT-TYP ( #I ) = 'M'
                    {
                        pnd_Pay_Amt.compute(new ComputeParameters(true, pnd_Pay_Amt), (pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units().getValue(pnd_I).multiply(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val().getValue(pnd_I)))); //Natural: COMPUTE ROUNDED #PAY-AMT = ( #IAA-XFR-AUDIT.IAXFR-TO-XFR-UNITS ( #I ) * #IAA-XFR-AUDIT.IAXFR-FROM-REVAL-UNIT-VAL ( #I ) )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Pay_Amt.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar().getValue(pnd_I));                                                       //Natural: ASSIGN #PAY-AMT := #IAA-XFR-AUDIT.IAXFR-TO-XFR-GUAR ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_To_Amt.nadd(pnd_Pay_Amt);                                                                                                                             //Natural: ADD #PAY-AMT TO #TO-AMT
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  010809
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(1).equals("09") || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(1).equals("11")  //Natural: IF #IAA-XFR-AUDIT.IAXFR-FROM-FUND-CDE ( 1 ) = '09' OR = '11' OR ( #IAA-XFR-AUDIT.IAXFR-FROM-FUND-CDE ( 1 ) = '1G' OR = '1S' )
            || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(1).equals("1G") || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(1).equals("1S")))
        {
            pnd_Pct.compute(new ComputeParameters(true, pnd_Pct), pnd_To_Amt.divide(pnd_Frm_Amt));                                                                        //Natural: COMPUTE ROUNDED #PCT = #TO-AMT / #FRM-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pct.compute(new ComputeParameters(true, pnd_Pct), (pnd_Xfr_Amt.subtract(pnd_To_Amt)).divide(pnd_Frm_Amt));                                                //Natural: COMPUTE ROUNDED #PCT = ( #XFR-AMT - #TO-AMT ) / #FRM-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pct.greater(1)))                                                                                                                                //Natural: IF #PCT GT 1
        {
            pnd_Pct.setValue(1);                                                                                                                                          //Natural: ASSIGN #PCT := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pct.less(getZero())))                                                                                                                           //Natural: IF #PCT LT 0
        {
            pnd_Pct.setValue(0);                                                                                                                                          //Natural: ASSIGN #PCT := 0
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ivc_Pro_Adj.compute(new ComputeParameters(true, pnd_Ivc_Pro_Adj), (pnd_Ivc_Diff.multiply(pnd_Pct)));                                                          //Natural: COMPUTE ROUNDED #IVC-PRO-ADJ = ( #IVC-DIFF * #PCT )
        pnd_Per_Ivc_Pro_Adj.compute(new ComputeParameters(true, pnd_Per_Ivc_Pro_Adj), (pnd_Cntrct_Per_Ivc_Amt.multiply(pnd_Pct)));                                        //Natural: COMPUTE ROUNDED #PER-IVC-PRO-ADJ = ( #CNTRCT-PER-IVC-AMT * #PCT )
        pnd_Ivc_Ind.setValue("Y");                                                                                                                                        //Natural: ASSIGN #IVC-IND := 'Y'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MASTER-INFO
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CURRENT-PAYMENT
    }
    private void sub_Get_Master_Info() throws Exception                                                                                                                   //Natural: GET-MASTER-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ ( 1 ) IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM #PPCN-NBR
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
        1
        );
        R1:
        while (condition(vw_iaa_Cntrct.readNextRow("R1")))
        {
            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.equals(pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr)))                                                                     //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #PPCN-NBR
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reas_Cde.setValue("99");                                                                                                                              //Natural: ASSIGN #REAS-CDE := '99'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_cpr.startDatabaseRead                                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R2",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Fund_Payee_Key_Pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R2:
        while (condition(vw_cpr.readNextRow("R2")))
        {
            if (condition(cpr_Cntrct_Part_Ppcn_Nbr.equals(pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr) && cpr_Cntrct_Part_Payee_Cde.equals(pnd_Cntrct_Fund_Payee_Key_Pnd_Payee_Cde))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #PAYEE-CDE
            {
                if (condition(cpr_Cntrct_Part_Ppcn_Nbr.less("Z") || DbsUtil.maskMatches(cpr_Cntrct_Part_Ppcn_Nbr,"'6L7'") || DbsUtil.maskMatches(cpr_Cntrct_Part_Ppcn_Nbr,"'6M7'")  //Natural: IF CPR.CNTRCT-PART-PPCN-NBR LT 'Z' OR ( CPR.CNTRCT-PART-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' ) )
                    || DbsUtil.maskMatches(cpr_Cntrct_Part_Ppcn_Nbr,"'6N7'")))
                {
                    pnd_I.setValue(1);                                                                                                                                    //Natural: ASSIGN #I := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_I.setValue(2);                                                                                                                                    //Natural: ASSIGN #I := 2
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Transfer_From.getBoolean()))                                                                                                            //Natural: IF #TRANSFER-FROM
                {
                    pnd_Ivc_Diff.compute(new ComputeParameters(false, pnd_Ivc_Diff), cpr_Cntrct_Ivc_Amt.getValue(pnd_I).subtract(cpr_Cntrct_Ivc_Used_Amt.getValue(pnd_I))); //Natural: ASSIGN #IVC-DIFF := CPR.CNTRCT-IVC-AMT ( #I ) - CPR.CNTRCT-IVC-USED-AMT ( #I )
                    pnd_Cntrct_Rcvry_Type_Ind.setValue(cpr_Cntrct_Rcvry_Type_Ind.getValue(pnd_I));                                                                        //Natural: ASSIGN #CNTRCT-RCVRY-TYPE-IND := CPR.CNTRCT-RCVRY-TYPE-IND ( #I )
                    pnd_Cntrct_Per_Ivc_Amt.setValue(cpr_Cntrct_Per_Ivc_Amt.getValue(pnd_I));                                                                              //Natural: ASSIGN #CNTRCT-PER-IVC-AMT := CPR.CNTRCT-PER-IVC-AMT ( #I )
                    if (condition(cpr_Cntrct_Ivc_Amt.getValue(pnd_I).equals(getZero())))                                                                                  //Natural: IF CPR.CNTRCT-IVC-AMT ( #I ) = 0
                    {
                        pnd_Ivc_Diff.setValue(0);                                                                                                                         //Natural: ASSIGN #IVC-DIFF := 0
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Ivc_Diff.lessOrEqual(getZero())))                                                                                               //Natural: IF #IVC-DIFF LE 0
                        {
                            pnd_Ivc_Ind.setValue("N");                                                                                                                    //Natural: ASSIGN #IVC-IND := 'N'
                            if (condition(pnd_Ivc_Diff.less(getZero())))                                                                                                  //Natural: IF #IVC-DIFF LT 0
                            {
                                pnd_Ret_Cde.setValue("IV");                                                                                                               //Natural: ASSIGN #RET-CDE := 'IV'
                                pnd_Warn_Ind.setValue("Y");                                                                                                               //Natural: ASSIGN #WARN-IND := 'Y'
                                pnd_Reas_Cde.setValue("02");                                                                                                              //Natural: ASSIGN #REAS-CDE := '02'
                            }                                                                                                                                             //Natural: END-IF
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Cntrct_Rcvry_Type_Ind.notEquals(cpr_Cntrct_Rcvry_Type_Ind.getValue(pnd_I))))                                                        //Natural: IF #CNTRCT-RCVRY-TYPE-IND NE CPR.CNTRCT-RCVRY-TYPE-IND ( #I )
                    {
                        pnd_Ret_Cde.setValue("IV");                                                                                                                       //Natural: ASSIGN #RET-CDE := 'IV'
                        pnd_Ivc_Ind.setValue("N");                                                                                                                        //Natural: ASSIGN #IVC-IND := 'N'
                        pnd_Warn_Ind.setValue("Y");                                                                                                                       //Natural: ASSIGN #WARN-IND := 'Y'
                        pnd_Reas_Cde.setValue("01");                                                                                                                      //Natural: ASSIGN #REAS-CDE := '01'
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reas_Cde.setValue("99");                                                                                                                              //Natural: ASSIGN #REAS-CDE := '99'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Transfer_From.getBoolean()))                                                                                                                    //Natural: IF #TRANSFER-FROM
        {
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-PAYMENT
            sub_Get_Current_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Current_Payment() throws Exception                                                                                                               //Natural: GET-CURRENT-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-PAYEE-KEY
        (
        "READ01",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ01")))
        {
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Cntrct_Fund_Payee_Key_Pnd_Ppcn_Nbr) || iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Cntrct_Fund_Payee_Key_Pnd_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR NE #PPCN-NBR OR IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE NE #PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.equals("T") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.equals("U") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.equals("2"))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-CDE = 'T' OR = 'U' OR = '2'
            {
                pnd_Frm_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                                    //Natural: COMPUTE #FRM-AMT = #FRM-AMT + IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.equals("W") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.equals("4")))                                                //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-CDE = 'W' OR = '4'
            {
                ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                          //Natural: ASSIGN IA-REVAL-METHD := 'M'
                ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();                                                                                                  //Natural: RESET AUV-RETURNED RETURN-CODE AUV-DATE-RETURN
                ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();
                ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
                DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"),true), new ExamineSearch(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde,     //Natural: EXAMINE FULL IAAA051Z.#IA-STD-NM-CD ( * ) FOR FULL IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE GIVING INDEX #I
                    true), new ExamineGivingIndex(pnd_I));
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                              //Natural: ASSIGN IA-FUND-CODE := IAAA051Z.#IA-STD-ALPHA-CD ( #I )
                    ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue("F");                                                                                        //Natural: ASSIGN IA-CALL-TYPE := 'F'
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date_A().setValueEdited(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte(),new ReportEditMask("YYYYMMDD"));   //Natural: MOVE EDITED #IAA-XFR-AUDIT.IAXFR-EFFCTVE-DTE ( EM = YYYYMMDD ) TO IA-CHECK-DATE-A
                ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date().setValue(0);                                                                                             //Natural: ASSIGN IA-ISSUE-DATE := 0
                DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                            //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
                if (condition(Global.isEscape())) return;
                pnd_Pay_Amt.compute(new ComputeParameters(true, pnd_Pay_Amt), ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().multiply(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1))); //Natural: COMPUTE ROUNDED #PAY-AMT = AUV-RETURNED * IAA-TIAA-FUND-RCRD.TIAA-UNITS-CNT ( 1 )
                pnd_Frm_Amt.nadd(pnd_Pay_Amt);                                                                                                                            //Natural: COMPUTE #FRM-AMT = #FRM-AMT + #PAY-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(5, "PS=66 LS=133");
    }
}
