/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:15 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420a
************************************************************
**        * FILE NAME            : Iatn420a.java
**        * CLASS NAME           : Iatn420a
**        * INSTANCE NAME        : Iatn420a
************************************************************
************************************************************************
*  PROGRAM: IATN420A
*   AUTHOR: ARI GROSSMAN
*     DATE: FEB 08, 1996
*  PURPOSE: CREATE BEFORE IMAGES OF CPR, CREF FUND, AND TIAA FUND VIEWS
* HISTORY : 03/07/97  ADDED MULTI FUND FUNCTIONALITY AND
*                     CONTRACT BEFORE IMAGES
*           12/30/97  TRANSFER PROCESSING - 1998 (FORMERLY IATN170A)
*           01/08/09  O SOTTO TIAA ACCESS CHANGES. SC 010809.
*           04/12/12  T TINIO RATE BASE EXPANSION. SC 041212.
*           04/06/17  RCC RESTOWED FOR PIN EXPANSION
************************************************************************
*  DEFINE DATA AREAS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420a extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Iaa_From_Cntrct;
    private DbsField pnd_Iaa_From_Pyee;

    private DbsGroup pnd_Iaa_From_Pyee__R_Field_1;
    private DbsField pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N;
    private DbsField pnd_Iaa_New_Issue;
    private DbsField pnd_From_Fund;
    private DbsField pnd_Iaa_To_Cntrct;

    private DbsGroup pnd_Iaa_To_Cntrct__R_Field_2;
    private DbsField pnd_Iaa_To_Cntrct_Pnd_Iaa_To_Cntrct_1;
    private DbsField pnd_Iaa_To_Pyee;

    private DbsGroup pnd_Iaa_To_Pyee__R_Field_3;
    private DbsField pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Time;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_4;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_5;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_File_Mode;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_To_Cntrct_Fund;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());

        // parameters
        parameters = new DbsRecord();
        pnd_Iaa_From_Cntrct = parameters.newFieldInRecord("pnd_Iaa_From_Cntrct", "#IAA-FROM-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_From_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_From_Pyee = parameters.newFieldInRecord("pnd_Iaa_From_Pyee", "#IAA-FROM-PYEE", FieldType.STRING, 2);
        pnd_Iaa_From_Pyee.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_From_Pyee__R_Field_1 = parameters.newGroupInRecord("pnd_Iaa_From_Pyee__R_Field_1", "REDEFINE", pnd_Iaa_From_Pyee);
        pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N = pnd_Iaa_From_Pyee__R_Field_1.newFieldInGroup("pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N", "#IAA-FROM-PYEE-N", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_New_Issue = parameters.newFieldInRecord("pnd_Iaa_New_Issue", "#IAA-NEW-ISSUE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue.setParameterOption(ParameterOption.ByReference);
        pnd_From_Fund = parameters.newFieldInRecord("pnd_From_Fund", "#FROM-FUND", FieldType.STRING, 1);
        pnd_From_Fund.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_To_Cntrct = parameters.newFieldInRecord("pnd_Iaa_To_Cntrct", "#IAA-TO-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_To_Cntrct.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_To_Cntrct__R_Field_2 = parameters.newGroupInRecord("pnd_Iaa_To_Cntrct__R_Field_2", "REDEFINE", pnd_Iaa_To_Cntrct);
        pnd_Iaa_To_Cntrct_Pnd_Iaa_To_Cntrct_1 = pnd_Iaa_To_Cntrct__R_Field_2.newFieldInGroup("pnd_Iaa_To_Cntrct_Pnd_Iaa_To_Cntrct_1", "#IAA-TO-CNTRCT-1", 
            FieldType.STRING, 1);
        pnd_Iaa_To_Pyee = parameters.newFieldInRecord("pnd_Iaa_To_Pyee", "#IAA-TO-PYEE", FieldType.STRING, 2);
        pnd_Iaa_To_Pyee.setParameterOption(ParameterOption.ByReference);

        pnd_Iaa_To_Pyee__R_Field_3 = parameters.newGroupInRecord("pnd_Iaa_To_Pyee__R_Field_3", "REDEFINE", pnd_Iaa_To_Pyee);
        pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N = pnd_Iaa_To_Pyee__R_Field_3.newFieldInGroup("pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N", "#IAA-TO-PYEE-N", FieldType.NUMERIC, 
            2);
        pnd_Check_Date = parameters.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Check_Date.setParameterOption(ParameterOption.ByReference);
        pnd_Time = parameters.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Time.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_File_Mode = localVariables.newFieldInRecord("pnd_File_Mode", "#FILE-MODE", FieldType.NUMERIC, 3);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_To_Cntrct_Fund = localVariables.newFieldInRecord("pnd_To_Cntrct_Fund", "#TO-CNTRCT-FUND", FieldType.STRING, 1);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal999.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420a() throws Exception
    {
        super("Iatn420a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420A", onError);
        //* *********
        //*  WRITE 'IATN170A'
        pnd_Return_Code.setValue("T1");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = 'T1'
                                                                                                                                                                          //Natural: PERFORM #BI-CONTRACTS
        sub_Pnd_Bi_Contracts();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* ***************************
        //*  COPYCODE: IAAC400
        //*  BY KAMIL AYDIN
        //* ***************************
        pnd_Return_Code.setValue("T2");                                                                                                                                   //Natural: ON ERROR;//Natural: ASSIGN #RETURN-CODE = 'T2'
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                            //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-FROM-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #BI-CPR
        sub_Pnd_Bi_Cpr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                          //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_From_Pyee_Pnd_Iaa_From_Pyee_N);                                                                           //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-FROM-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE = ' '
        //*  010809
        if (condition(pnd_From_Fund.equals("T") || pnd_From_Fund.equals("G") || pnd_From_Fund.equals("R") || pnd_From_Fund.equals("D")))                                  //Natural: IF #FROM-FUND = 'T' OR = 'G' OR = 'R' OR = 'D'
        {
            pnd_Return_Code.setValue("T3");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'T3'
                                                                                                                                                                          //Natural: PERFORM #BI-TIAA-FUND-FROM
            sub_Pnd_Bi_Tiaa_Fund_From();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Return_Code.setValue("T4");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = 'T4'
                                                                                                                                                                          //Natural: PERFORM #BI-CREF-FUND-FROM
            sub_Pnd_Bi_Cref_Fund_From();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*   WRITE '=' #IAA-NEW-ISSUE
        if (condition(pnd_Iaa_New_Issue.equals(" ") || pnd_Iaa_New_Issue.equals("E")))                                                                                    //Natural: IF #IAA-NEW-ISSUE = ' ' OR = 'E'
        {
            if (condition(pnd_Iaa_To_Cntrct.notEquals(" ")))                                                                                                              //Natural: IF #IAA-TO-CNTRCT NE ' '
            {
                pnd_Return_Code.setValue("T2");                                                                                                                           //Natural: ASSIGN #RETURN-CODE = 'T2'
                pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                     //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
                pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N);                                                                        //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-TO-PYEE-N
                                                                                                                                                                          //Natural: PERFORM #BI-CPR
                sub_Pnd_Bi_Cpr();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                            //Natural: IF #RETURN-CODE NE ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                    //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N);                                                                       //Natural: ASSIGN #W-CNTRCT-PAYEE = #IAA-TO-PYEE-N
                pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                        //Natural: ASSIGN #W-FUND-CODE = ' '
                                                                                                                                                                          //Natural: PERFORM #CHECK-COMPANY-CODE
                sub_Pnd_Check_Company_Code();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Cref_Cnt.equals(" ")))                                                                                                                  //Natural: IF #CREF-CNT = ' '
                {
                    pnd_Return_Code.setValue("T3");                                                                                                                       //Natural: ASSIGN #RETURN-CODE = 'T3'
                                                                                                                                                                          //Natural: PERFORM #BI-TIAA-FUND-TO
                    sub_Pnd_Bi_Tiaa_Fund_To();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Return_Code.setValue("T4");                                                                                                                       //Natural: ASSIGN #RETURN-CODE = 'T4'
                                                                                                                                                                          //Natural: PERFORM #BI-CREF-FUND-TO
                    sub_Pnd_Bi_Cref_Fund_To();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  WRITE 'OUTSIDE OF IF'
                if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                            //Natural: IF #RETURN-CODE NE ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-COMPANY-CODE
        //*  USE NEW VIEW PREFIX FROM IAAL999          041212 START
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-CONTRACTS
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-CPR
        //* **********************************************************************
        //*   WRITE 'BI-CPR' '=' #CNTRCT-PAYEE-KEY
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-TIAA-FUND-FROM
        //* **********************************************************************
        //*  WRITE 'TIAA FUND FROM'
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-CREF-FUND-FROM
        //* **********************************************************************
        //*  WRITE 'CREF FUND FROM'
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-TIAA-FUND-TO
        //* **********************************************************************
        //*     WRITE 'ENTERING BI-TIAA-FUND-TO'
        //*    WRITE '=' #W-CNTRCT-PPCN-NBR '=' #W-CNTRCT-PAYEE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #BI-CREF-FUND-TO
        //* **********************************************************************
        //*     WRITE 'ENTERING BI-CREF-FUND-TO'
        //*      WRITE '=' #W-CNTRCT-PPCN-NBR '=' #W-CNTRCT-PAYEE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #STORE-RE-FUND
        //* **********************************************************************
        //*    WRITE 'ENTERING STORE RE-FUND'
        //*     WRITE '=' #W-CNTRCT-PPCN-NBR '=' #W-CNTRCT-PAYEE
        //*    #W-FUND-CODE := 'U09'
    }
    private void sub_Pnd_Check_Company_Code() throws Exception                                                                                                            //Natural: #CHECK-COMPANY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Cnt.reset();                                                                                                                                             //Natural: RESET #CREF-CNT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                             //Natural: ASSIGN #CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Iaa_To_Pyee_Pnd_Iaa_To_Pyee_N);                                                                                //Natural: ASSIGN #CNTRCT-PAYEE = #IAA-TO-PYEE-N
        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPZ",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPZ:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CPZ")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                if (condition(ldaIaal999.getCpr_Cntrct_Company_Cd().getValue(1).equals(" ")))                                                                             //Natural: IF CPR.CNTRCT-COMPANY-CD ( 1 ) = ' '
                {
                    pnd_Cref_Cnt.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #CREF-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Bi_Contracts() throws Exception                                                                                                                  //Natural: #BI-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-FROM-CNTRCT
        (
        "FNR",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_From_Cntrct, WcType.WITH) },
        1
        );
        FNR:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FNR")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal999.getVw_iaa_Cntrct_Trans().reset();                                                                                                                  //Natural: RESET IAA-CNTRCT-TRANS
            ldaIaal999.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cntrct());                                                                           //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
            ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Time);                                                                                                //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE = #TIME
            pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte());                                                                                         //Natural: ASSIGN #DATE-TIME-P = IAA-CNTRCT-TRANS.TRANS-DTE
            ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte()),                    //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
            ldaIaal999.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                            //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE = #TIME
            ldaIaal999.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue("1");                                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID = '1'
            ldaIaal999.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                    //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
            ldaIaal999.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                            //Natural: STORE IAA-CNTRCT-TRANS
            pnd_Return_Code.reset();                                                                                                                                      //Natural: RESET #RETURN-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition((pnd_Iaa_To_Cntrct.notEquals(" ") && (pnd_Iaa_New_Issue.equals(" ") || pnd_Iaa_New_Issue.equals("E")))))                                            //Natural: IF #IAA-TO-CNTRCT NE ' ' AND ( #IAA-NEW-ISSUE = ' ' OR = 'E' )
        {
            ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-TO-CNTRCT
            (
            "FNR2",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_To_Cntrct, WcType.WITH) },
            1
            );
            FNR2:
            while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FNR2")))
            {
                ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
                ldaIaal999.getVw_iaa_Cntrct_Trans().reset();                                                                                                              //Natural: RESET IAA-CNTRCT-TRANS
                ldaIaal999.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cntrct());                                                                       //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
                ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Time);                                                                                            //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte());                                                                                     //Natural: ASSIGN #DATE-TIME-P = IAA-CNTRCT-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte()),                //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                        //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE = #TIME
                ldaIaal999.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue("1");                                                                                              //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID = '1'
                ldaIaal999.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                        //Natural: STORE IAA-CNTRCT-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Bi_Cpr() throws Exception                                                                                                                        //Natural: #BI-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "CPX",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        CPX:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("CPX")))
        {
            if (condition(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr) && ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee))) //Natural: IF CPR.CNTRCT-PART-PPCN-NBR = #CNTRCT-PPCN-NBR AND CPR.CNTRCT-PART-PAYEE-CDE = #CNTRCT-PAYEE
            {
                ldaIaal999.getVw_iaa_Cpr_Trans().reset();                                                                                                                 //Natural: RESET IAA-CPR-TRANS
                ldaIaal999.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal999.getVw_cpr());                                                                                 //Natural: MOVE BY NAME CPR TO IAA-CPR-TRANS
                ldaIaal999.getIaa_Cpr_Trans_Trans_Dte().setValue(pnd_Time);                                                                                               //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cpr_Trans_Trans_Dte());                                                                                        //Natural: ASSIGN #DATE-TIME-P = IAA-CPR-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte()), new                  //Natural: COMPUTE IAA-CPR-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cpr_Trans_Bfre_Imge_Id().setValue("1");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID = '1'
                ldaIaal999.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                                   //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CPR-TRANS
                //*         WRITE IAA-CPR-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Bi_Tiaa_Fund_From() throws Exception                                                                                                             //Natural: #BI-TIAA-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1A",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1A:
        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1A")))
        {
            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*           WRITE '=' IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE
                //*  010809
                if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U09") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W09")  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'U09' OR = 'W09' OR = 'U11' OR = 'W11'
                    || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U11") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W11")))
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE TO #W-FUND-CODE
                                                                                                                                                                          //Natural: PERFORM #STORE-RE-FUND
                    sub_Pnd_Store_Re_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1A"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1A"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().reset();                                                                                                       //Natural: RESET IAA-TIAA-FUND-TRANS
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());                                                        //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE = #TIME
                    pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Dte());                                                                              //Natural: ASSIGN #DATE-TIME-P = IAA-TIAA-FUND-TRANS.TRANS-DTE
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte()),      //Natural: COMPUTE IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                        new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID = '1'
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                         //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                 //Natural: STORE IAA-TIAA-FUND-TRANS
                    //*     WRITE '=' IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE
                    //*     WRITE '=' IAA-TIAA-FUND-TRANS.TIAA-PER-PAY-AMT(1)
                    //*     WRITE '=' IAA-TIAA-FUND-TRANS.TIAA-PER-DIV-AMT(1)
                    //*     WRITE '=' IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE
                    pnd_Return_Code.reset();                                                                                                                              //Natural: RESET #RETURN-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1A;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1A. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Bi_Cref_Fund_From() throws Exception                                                                                                             //Natural: #BI-CREF-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1B",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1B:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1B")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                //*        WRITE '=' IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR
                //*        WRITE '=' IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE
                //*        WRITE '=' IAA-CREF-FUND-RCRD.CREF-FUND-CDE
                //*        WRITE '=' IAA-CREF-FUND-RCRD.CREF-UNITS-CNT(1)
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.BFRE-IMGE-ID = '1'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                //*    WRITE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1B;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1B. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Bi_Tiaa_Fund_To() throws Exception                                                                                                               //Natural: #BI-TIAA-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1:
        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1")))
        {
            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*            WRITE '=' IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE
                //*  010809
                if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U09") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W09")  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'U09' OR = 'W09' OR = 'U11' OR = 'W11'
                    || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("U11") || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals("W11")))
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE TO #W-FUND-CODE
                                                                                                                                                                          //Natural: PERFORM #STORE-RE-FUND
                    sub_Pnd_Store_Re_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().reset();                                                                                                       //Natural: RESET IAA-TIAA-FUND-TRANS
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());                                                        //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE = #TIME
                    pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Dte());                                                                              //Natural: ASSIGN #DATE-TIME-P = IAA-TIAA-FUND-TRANS.TRANS-DTE
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte()),      //Natural: COMPUTE IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                        new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID = '1'
                    ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                         //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                    ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                 //Natural: STORE IAA-TIAA-FUND-TRANS
                    //*   WRITE 'TIAA-FUND-TRANS'
                    pnd_Return_Code.reset();                                                                                                                              //Natural: RESET #RETURN-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Bi_Cref_Fund_To() throws Exception                                                                                                               //Natural: #BI-CREF-FUND-TO
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R2",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R2:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R2")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*          WRITE '=' IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.BFRE-IMGE-ID = '1'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                //*     WRITE '=' CREF-CMPNY-FUND-CDE
                //*           '=' CREF-UNITS-CNT(1)
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                //*   WRITE 'CREF-FUND-TRANS'
                //*   WRITE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                                    //Natural: IF #RETURN-CODE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Store_Re_Fund() throws Exception                                                                                                                 //Natural: #STORE-RE-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R2A",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R2A:
        while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R2A")))
        {
            if (condition(ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                //*          WRITE '=' IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().reset();                                                                                                           //Natural: RESET IAA-CREF-FUND-TRANS
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(pnd_Time);                                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE = #TIME
                pnd_Date_Time_P.setValue(ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte());                                                                                  //Natural: ASSIGN #DATE-TIME-P = IAA-CREF-FUND-TRANS.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte()),          //Natural: COMPUTE IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
                    new DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
                ldaIaal999.getIaa_Cref_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.BFRE-IMGE-ID = '1'
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Check_Date);                                                                             //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE = #CHECK-DATE
                //*          WRITE '=' CREF-CMPNY-FUND-CDE
                //*             '=' CREF-UNITS-CNT(1)
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                //*          WRITE 'CREF-FUND-TRANS REAL'
                //*          WRITE IAA-CREF-FUND-TRANS
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R2A;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R2A. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  USE NEW VIEW PREFIX FROM IAAL999          041212 END
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
