/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:52 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420x
************************************************************
**        * FILE NAME            : Iatn420x.java
**        * CLASS NAME           : Iatn420x
**        * INSTANCE NAME        : Iatn420x
************************************************************
************************************************************************
*
* NAME       :- IATN420X
* AUTHOR     :- LEN B
* DATE       :- 01/09/1998
* DESCRIPTION:-
*
* HISTORY    :- 06/99  KN  ADDED TIAA TO CREF TRANSFERS
*               05/02  TD  CHANGE IAAN0500 TO IAAN051Z
*               03/09  OS  TIAA ACCESS CHANGES. SC 030209.
*               05/09  OS  REMOVE UNNECESSARY DISPLAYS.
*               04/2017 OS RE=STOWED FOR PIN EXPANSION.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420x extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl420x pdaIatl420x;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_pnd_Hst_Iaa_Xfr_Cntrl;
    private DbsField pnd_Hst_Iaa_Xfr_Cntrl_Iaxfr_Ctl_Super_De_1;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1;

    private DbsGroup pnd_Iaxfr_Ctl_Super_De_1__R_Field_1;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte;

    private DataAccessProgramView vw_iaa_Xfr_Cntrl;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Accntng_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Invrse_Cycle_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Nbr_Rqst_Recs_Processd;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data;

    private DbsGroup iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Unit_Typ;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data;

    private DbsGroup iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Lst_Chnge_Dte;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Yes;
    private DbsField pnd_Const_Pnd_Annual;
    private DbsField pnd_Const_Pnd_Monthly;
    private DbsField pnd_Const_Pnd_Rcrd_Type_1g;
    private DbsField pnd_Const_Pnd_Rcrd_Type_1s;
    private DbsField pnd_Const_Pnd_Max_Funds;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_New_Control;
    private DbsField pnd_L_Pnd_Errors_Found;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Fund_Code_A;

    private DbsGroup pnd_Fund_Code_A__R_Field_3;
    private DbsField pnd_Fund_Code_A_Pnd_Fund_Code_N;
    private DbsField pnd_I;
    private DbsField pnd_Fund_Indx;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl420x = new PdaIatl420x(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_pnd_Hst_Iaa_Xfr_Cntrl = new DataAccessProgramView(new NameInfo("vw_pnd_Hst_Iaa_Xfr_Cntrl", "#HST-IAA-XFR-CNTRL"), "IAA_XFR_CNTRL", "IA_TRANSFERS");
        pnd_Hst_Iaa_Xfr_Cntrl_Iaxfr_Ctl_Super_De_1 = vw_pnd_Hst_Iaa_Xfr_Cntrl.getRecord().newFieldInGroup("pnd_Hst_Iaa_Xfr_Cntrl_Iaxfr_Ctl_Super_De_1", 
            "IAXFR-CTL-SUPER-DE-1", FieldType.BINARY, 9, RepeatingFieldStrategy.None, "IAXFR_CTL_SUPER_DE_1");
        pnd_Hst_Iaa_Xfr_Cntrl_Iaxfr_Ctl_Super_De_1.setSuperDescriptor(true);
        registerRecord(vw_pnd_Hst_Iaa_Xfr_Cntrl);

        pnd_Iaxfr_Ctl_Super_De_1 = localVariables.newFieldInRecord("pnd_Iaxfr_Ctl_Super_De_1", "#IAXFR-CTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Iaxfr_Ctl_Super_De_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Iaxfr_Ctl_Super_De_1__R_Field_1", "REDEFINE", pnd_Iaxfr_Ctl_Super_De_1);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type", 
            "#IAXFR-CTL-RCRD-TYPE", FieldType.STRING, 1);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte", 
            "#IAXFR-CTL-CYCLE-DTE", FieldType.DATE);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte", 
            "#IAXFR-CTL-EFFCTVE-DTE", FieldType.DATE);

        vw_iaa_Xfr_Cntrl = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Cntrl", "IAA-XFR-CNTRL"), "IAA_XFR_CNTRL", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_CNTRL"));
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type", "IAXFR-CTL-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IAXFR_CTL_RCRD_TYPE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Accntng_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Accntng_Dte", "IAXFR-CTL-ACCNTNG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CTL_ACCNTNG_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte", "IAXFR-CTL-EFFCTVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CTL_EFFCTVE_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte", "IAXFR-CTL-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CTL_CYCLE_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Invrse_Cycle_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Invrse_Cycle_Dte", "IAXFR-CTL-INVRSE-CYCLE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "IAXFR_CTL_INVRSE_CYCLE_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees", "IAXFR-CTL-NEW-TIAA-PAYEES", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_CTL_NEW_TIAA_PAYEES");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees", "IAXFR-CTL-NEW-CREF-PAYEES", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_CTL_NEW_CREF_PAYEES");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees", "IAXFR-CTL-NEW-INACTIVE-PAYEES", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_CTL_NEW_INACTIVE_PAYEES");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Nbr_Rqst_Recs_Processd = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Nbr_Rqst_Recs_Processd", 
            "IAXFR-CTL-NBR-RQST-RECS-PROCESSD", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "IAXFR_CTL_NBR_RQST_RECS_PROCESSD");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt", "IAXFR-CTL-TOTAL-FRM-ASSET-AMT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "IAXFR_CTL_TOTAL_FRM_ASSET_AMT");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt", "IAXFR-CTL-TOTAL-TO-ASSET-AMT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "IAXFR_CTL_TOTAL_TO_ASSET_AMT");
        iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data", "C*IAXFR-CTL-FRM-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");

        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data = vw_iaa_Xfr_Cntrl.getRecord().newGroupInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data", "IAXFR-CTL-FRM-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde", "IAXFR-CTL-FRM-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_FUND_CDE", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Unit_Typ = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Unit_Typ", "IAXFR-CTL-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_UNIT_TYP", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units", "IAXFR-CTL-FRM-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_UNITS", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt", "IAXFR-CTL-FRM-GUAR-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_GUAR_AMT", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt", 
            "IAXFR-CTL-FRM-PER-DIVID-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_PER_DIVID_AMT", 
            "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars", "IAXFR-CTL-FRM-DOLLARS", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_DOLLARS", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds", "IAXFR-CTL-FRM-FUND-RCRDS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_FUND_RCRDS", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds", 
            "IAXFR-CTL-FRM-FULL-OUT-RCRDS", FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_FULL_OUT_RCRDS", 
            "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt", "IAXFR-CTL-FRM-ASSET-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_ASSET_AMT", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data", "C*IAXFR-CTL-TO-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CTL_TO_DATA");

        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data = vw_iaa_Xfr_Cntrl.getRecord().newGroupInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data", "IAXFR-CTL-TO-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde", "IAXFR-CTL-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units", "IAXFR-CTL-TO-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_UNITS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ", "IAXFR-CTL-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_UNIT_TYP", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt", "IAXFR-CTL-TO-GUAR-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_GUAR_AMT", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt", "IAXFR-CTL-TO-PER-DIVID-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_PER_DIVID_AMT", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars", "IAXFR-CTL-TO-DOLLARS", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_DOLLARS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds", "IAXFR-CTL-TO-FUND-RCRDS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_FUND_RCRDS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs", "IAXFR-CTL-NEW-FUND-RECS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_NEW_FUND_RECS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt", "IAXFR-CTL-TO-ASSET-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Lst_Chnge_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_CHNGE_DTE");
        registerRecord(vw_iaa_Xfr_Cntrl);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Yes = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Yes", "#YES", FieldType.STRING, 1);
        pnd_Const_Pnd_Annual = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Annual", "#ANNUAL", FieldType.STRING, 1);
        pnd_Const_Pnd_Monthly = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Monthly", "#MONTHLY", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_1g = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_1g", "#RCRD-TYPE-1G", FieldType.STRING, 2);
        pnd_Const_Pnd_Rcrd_Type_1s = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_1s", "#RCRD-TYPE-1S", FieldType.STRING, 2);
        pnd_Const_Pnd_Max_Funds = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Max_Funds", "#MAX-FUNDS", FieldType.PACKED_DECIMAL, 3);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_New_Control = pnd_L.newFieldInGroup("pnd_L_Pnd_New_Control", "#NEW-CONTROL", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Errors_Found = pnd_L.newFieldInGroup("pnd_L_Pnd_Errors_Found", "#ERRORS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Fund_Code_A = localVariables.newFieldInRecord("pnd_Fund_Code_A", "#FUND-CODE-A", FieldType.STRING, 2);

        pnd_Fund_Code_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Fund_Code_A__R_Field_3", "REDEFINE", pnd_Fund_Code_A);
        pnd_Fund_Code_A_Pnd_Fund_Code_N = pnd_Fund_Code_A__R_Field_3.newFieldInGroup("pnd_Fund_Code_A_Pnd_Fund_Code_N", "#FUND-CODE-N", FieldType.NUMERIC, 
            2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Fund_Indx = localVariables.newFieldInRecord("pnd_Fund_Indx", "#FUND-INDX", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_pnd_Hst_Iaa_Xfr_Cntrl.reset();
        vw_iaa_Xfr_Cntrl.reset();

        localVariables.reset();
        pnd_Const_Pnd_Yes.setInitialValue("Y");
        pnd_Const_Pnd_Annual.setInitialValue("A");
        pnd_Const_Pnd_Monthly.setInitialValue("M");
        pnd_Const_Pnd_Rcrd_Type_1g.setInitialValue("1G");
        pnd_Const_Pnd_Rcrd_Type_1s.setInitialValue("1S");
        pnd_Const_Pnd_Max_Funds.setInitialValue(80);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420x() throws Exception
    {
        super("Iatn420x");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  ======================================================================
        //*                          START OF SUBPROGRAM
        //*  ======================================================================
        pdaIatl420x.getPnd_Iatn420x_In_Pnd_Return_Code().reset();                                                                                                         //Natural: RESET #RETURN-CODE #MSG
        pdaIatl420x.getPnd_Iatn420x_In_Pnd_Msg().reset();
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.getValue("*").reset();                                                                                                           //Natural: RESET IAXFR-CTL-FRM-DATA ( * ) IAXFR-CTL-TO-DATA ( * )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.getValue("*").reset();
        //*  VALIDATE THAT THE KEY FIELDS ARE VALID
        if (condition(! (pdaIatl420x.getPnd_Iaa_Xfr_Audit_Rcrd_Type_Cde().equals("1") || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Rcrd_Type_Cde().equals("2"))))                  //Natural: IF NOT ( RCRD-TYPE-CDE = '1' OR = '2' )
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_Return_Code().setValue("E");                                                                                               //Natural: ASSIGN #RETURN-CODE := 'E'
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Record Type is not valid. Valid values are: 1 or 2.(",     //Natural: COMPRESS 'Record Type is not valid. Valid values are: 1 or 2.(' *PROGRAM ')' INTO #MSG LEAVING NO
                Global.getPROGRAM(), ")"));
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte().equals(getZero())))                                                                              //Natural: IF IAXFR-CYCLE-DTE = 0
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_Return_Code().setValue("E");                                                                                               //Natural: ASSIGN #RETURN-CODE := 'E'
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Cycle Date is required. (", Global.getPROGRAM(),           //Natural: COMPRESS 'Cycle Date is required. (' *PROGRAM ')' INTO #MSG LEAVING NO
                ")"));
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte().equals(getZero())))                                                                            //Natural: IF IAXFR-EFFCTVE-DTE = 0
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_Return_Code().setValue("E");                                                                                               //Natural: ASSIGN #RETURN-CODE := 'E'
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Effective Date is required. (", Global.getPROGRAM(),       //Natural: COMPRESS 'Effective Date is required. (' *PROGRAM ')' INTO #MSG LEAVING NO
                ")"));
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-NEW-CONTROL
        sub_Check_If_New_Control();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_L_Pnd_New_Control.getBoolean()))                                                                                                                //Natural: IF #NEW-CONTROL
        {
                                                                                                                                                                          //Natural: PERFORM ADD-NEW-IAA-XFR-CNTRL
            sub_Add_New_Iaa_Xfr_Cntrl();
            if (condition(Global.isEscape())) {return;}
            vw_iaa_Xfr_Cntrl.insertDBRow();                                                                                                                               //Natural: STORE IAA-XFR-CNTRL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_iaa_Xfr_Cntrl.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) IAA-XFR-CNTRL WITH IAXFR-CTL-SUPER-DE-1 = #IAXFR-CTL-SUPER-DE-1
            (
            "FND1",
            new Wc[] { new Wc("IAXFR_CTL_SUPER_DE_1", "=", pnd_Iaxfr_Ctl_Super_De_1.getBinary(), WcType.WITH) },
            1
            );
            FND1:
            while (condition(vw_iaa_Xfr_Cntrl.readNextRow("FND1")))
            {
                vw_iaa_Xfr_Cntrl.setIfNotFoundControlFlag(false);
                                                                                                                                                                          //Natural: PERFORM UPDATE-IAA-XFR-CNTRL
                sub_Update_Iaa_Xfr_Cntrl();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                vw_iaa_Xfr_Cntrl.updateDBRow("FND1");                                                                                                                     //Natural: UPDATE ( FND1. )
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  ======================================================================
        //*                          START OF SUBROUTINES
        //*  ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-NEW-CONTROL
        //* ***********************************************************************
        //*  CHECK IF THE CONTROL RECORD EXISTS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-NEW-IAA-XFR-CNTRL
        //* ***********************************************************************
        //*  MOVE VALUES FROM THE INPUT IAA-XFR-AUDIT TO THE IAA-XFR-CNTRL
        //*  AND THEN STORE THE RECORD
        //*  ==============================================================
        //*  SET THE ANNUAL AND MONTHLY FROM AND TO FUND CODES & UNIT TYPES
        //*  ==============================================================
        //* *FOR #I=1 TO 20
        //*     OR #IA-STD-NM-CD(#I) > '10'
        //*  ==================================================================
        //*  ADD FROM FUNDS
        //*  POSITIONS OF FUNDS ARE AS FOLLOWS :- 1G = 1, 1S = 21
        //*    - ANNUAL FUNDS ARE IN THE SAME POSITION AS NUMERIC VALUE OF FUND
        //*    - MONTHLY FUNDS ARE +20 POSITIONS FROM THE ANNUAL FUNDS
        //*  ==================================================================
        //*  6/99 KN
        //*  ==================================================================
        //*  ADD TO FUNDS
        //*  POSITIONS OF FUNDS ARE AS FOLLOWS : 1G = 1, 1S = 21
        //*    - ANNUAL FUNDS ARE IN THE SAME POSITION AS NUMERIC VALUE OF FUND
        //*    - MONTHLY FUNDS ARE +20 POSITIONS FROM THE ANNUAL FUNDS
        //*  ==================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-FROM-FUNDS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-TO-FUNDS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-IAA-XFR-CNTRL
        //* ***********************************************************************
        //*  UPDATE FIELDS
        //*  ==================================================================
        //*  CHANGE FROM FUNDS
        //*  POSITIONS OF FUNDS ARE AS FOLLOWS :- 1G = 1, 1S = 21
        //*    - ANNUAL FUNDS ARE IN THE SAME POSITION AS NUMERIC VALUE OF FUND
        //*    - MONTHLY FUNDS ARE +20 POSITIONS FROM THE ANNUAL FUNDS
        //*  ==================================================================
        //*  6/99 KN
        //*  ==================================================================
        //*  CHANGE TO FUNDS
        //*  POSITIONS OF FUNDS ARE AS FOLLOWS : 1G = 1, 1S = 21
        //*    - ANNUAL FUNDS ARE IN THE SAME POSITION AS NUMERIC VALUE OF FUND
        //*    - MONTHLY FUNDS ARE +20 POSITIONS FROM THE ANNUAL FUNDS
        //*  ==================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FROM-FUNDS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-TO-FUNDS
        //* ***********************************************************************
    }
    private void sub_Check_If_New_Control() throws Exception                                                                                                              //Natural: CHECK-IF-NEW-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Rcrd_Type_Cde());                                                      //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE := RCRD-TYPE-CDE
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte());                                                    //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-CYCLE-DTE := IAXFR-CYCLE-DTE
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte());                                                //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-EFFCTVE-DTE := IAXFR-EFFCTVE-DTE
        vw_pnd_Hst_Iaa_Xfr_Cntrl.createHistogram                                                                                                                          //Natural: HISTOGRAM ( 1 ) #HST-IAA-XFR-CNTRL FOR IAXFR-CTL-SUPER-DE-1 WHERE IAXFR-CTL-SUPER-DE-1 = #IAXFR-CTL-SUPER-DE-1
        (
        "HIST",
        "IAXFR_CTL_SUPER_DE_1",
        new Wc[] { new Wc("IAXFR_CTL_SUPER_DE_1", "=", pnd_Iaxfr_Ctl_Super_De_1, WcType.WITH) },
        1
        );
        HIST:
        while (condition(vw_pnd_Hst_Iaa_Xfr_Cntrl.readNextRow("HIST")))
        {
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        if (condition(vw_pnd_Hst_Iaa_Xfr_Cntrl.getAstNUMBER().equals(getZero())))                                                                                         //Natural: IF *NUMBER ( HIST. ) EQ 0
        {
            pnd_L_Pnd_New_Control.setValue(true);                                                                                                                         //Natural: ASSIGN #NEW-CONTROL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-NEW-CONTROL
    }
    private void sub_Add_New_Iaa_Xfr_Cntrl() throws Exception                                                                                                             //Natural: ADD-NEW-IAA-XFR-CNTRL
    {
        if (BLNatReinput.isReinput()) return;

        iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Rcrd_Type_Cde());                                                                     //Natural: ASSIGN IAXFR-CTL-RCRD-TYPE := RCRD-TYPE-CDE
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Accntng_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Acctng_Dte());                                                                //Natural: ASSIGN IAXFR-CTL-ACCNTNG-DTE := IAXFR-ACCTNG-DTE
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte());                                                               //Natural: ASSIGN IAXFR-CTL-EFFCTVE-DTE := IAXFR-EFFCTVE-DTE
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte());                                                                   //Natural: ASSIGN IAXFR-CTL-CYCLE-DTE := IAXFR-CYCLE-DTE
        pnd_Date_A.setValueEdited(iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED IAXFR-CTL-CYCLE-DTE ( EM = YYYYMMDD ) TO #DATE-A
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Invrse_Cycle_Dte.compute(new ComputeParameters(false, iaa_Xfr_Cntrl_Iaxfr_Ctl_Invrse_Cycle_Dte), DbsField.subtract(100000000,             //Natural: COMPUTE IAXFR-CTL-INVRSE-CYCLE-DTE = 100000000 - #DATE-N
            pnd_Date_A_Pnd_Date_N));
        if (condition(pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Tiaa_Payee().equals(pnd_Const_Pnd_Yes)))                                                                     //Natural: IF #NEW-TIAA-PAYEE = #YES
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees.setValue(1);                                                                                                          //Natural: ASSIGN IAXFR-CTL-NEW-TIAA-PAYEES := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Cref_Payee().equals(pnd_Const_Pnd_Yes)))                                                                     //Natural: IF #NEW-CREF-PAYEE = #YES
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees.setValue(1);                                                                                                          //Natural: ASSIGN IAXFR-CTL-NEW-CREF-PAYEES := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().equals(pnd_Const_Pnd_Yes)))                                                                   //Natural: IF #NEW-INACTIVE-IND = #YES
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees.setValue(1);                                                                                                      //Natural: ASSIGN IAXFR-CTL-NEW-INACTIVE-PAYEES := 1
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Nbr_Rqst_Recs_Processd.setValue(1);                                                                                                       //Natural: ASSIGN IAXFR-CTL-NBR-RQST-RECS-PROCESSD := 1
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt.nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt().getValue("*"));                                      //Natural: ADD IAXFR-FROM-ASSET-XFR-AMT ( * ) TO IAXFR-CTL-TOTAL-FRM-ASSET-AMT
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt.nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt().getValue("*"));                                             //Natural: ADD IAXFR-TO-ASSET-AMT ( * ) TO IAXFR-CTL-TOTAL-TO-ASSET-AMT
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(1).setValue(pnd_Const_Pnd_Rcrd_Type_1g);                                                                            //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( 1 ) := #RCRD-TYPE-1G
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(21).setValue(pnd_Const_Pnd_Rcrd_Type_1s);                                                                           //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( 21 ) := #RCRD-TYPE-1S
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde.getValue(1).setValue(pnd_Const_Pnd_Rcrd_Type_1g);                                                                             //Natural: ASSIGN IAXFR-CTL-TO-FUND-CDE ( 1 ) := #RCRD-TYPE-1G
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde.getValue(21).setValue(pnd_Const_Pnd_Rcrd_Type_1s);                                                                            //Natural: ASSIGN IAXFR-CTL-TO-FUND-CDE ( 21 ) := #RCRD-TYPE-1S
        //*  CALLNAT 'IAAN0500'  IAAA0500
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #MAX-FUNDS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Const_Pnd_Max_Funds)); pnd_I.nadd(1))
        {
            if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).equals("  ")))                                                                       //Natural: IF #IA-STD-NM-CD ( #I ) = '  '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  030209 END
            }                                                                                                                                                             //Natural: END-IF
            //*  030209
            if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).equals("1 ") || pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).equals("1G")  //Natural: IF #IA-STD-NM-CD ( #I ) = '1 ' OR = '1G' OR = '1S' OR = ' ' OR #IA-STD-NM-CD ( #I ) > '11'
                || pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).equals("1S") || pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).equals(" ") 
                || pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).greater("11")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Code_A.setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I));                                                                         //Natural: ASSIGN #FUND-CODE-A := #IA-STD-NM-CD ( #I )
            iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_Fund_Code_A_Pnd_Fund_Code_N).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I));          //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( #FUND-CODE-N ) := #IA-STD-NM-CD ( #I )
            iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde.getValue(pnd_Fund_Code_A_Pnd_Fund_Code_N).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I));           //Natural: ASSIGN IAXFR-CTL-TO-FUND-CDE ( #FUND-CODE-N ) := #IA-STD-NM-CD ( #I )
            iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Unit_Typ.getValue(pnd_Fund_Code_A_Pnd_Fund_Code_N).setValue(pnd_Const_Pnd_Annual);                                                //Natural: ASSIGN IAXFR-CTL-FRM-UNIT-TYP ( #FUND-CODE-N ) := #ANNUAL
            iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_Fund_Code_A_Pnd_Fund_Code_N.getDec().add(20)).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I)); //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( #FUND-CODE-N + 20 ) := #IA-STD-NM-CD ( #I )
            iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde.getValue(pnd_Fund_Code_A_Pnd_Fund_Code_N.getDec().add(20)).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I)); //Natural: ASSIGN IAXFR-CTL-TO-FUND-CDE ( #FUND-CODE-N + 20 ) := #IA-STD-NM-CD ( #I )
            iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ.getValue(pnd_Fund_Code_A_Pnd_Fund_Code_N.getDec().add(20)).setValue(pnd_Const_Pnd_Monthly);                               //Natural: ASSIGN IAXFR-CTL-TO-UNIT-TYP ( #FUND-CODE-N + 20 ) := #MONTHLY
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            short decideConditionsMet464 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF IAXFR-FROM-FUND-CDE ( #I );//Natural: VALUE ' '
            if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet464++;
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1G
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g))))
            {
                decideConditionsMet464++;
                pnd_Fund_Indx.setValue(1);                                                                                                                                //Natural: ASSIGN #FUND-INDX := 1
                                                                                                                                                                          //Natural: PERFORM STORE-FROM-FUNDS
                sub_Store_From_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1S
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))))
            {
                decideConditionsMet464++;
                pnd_Fund_Indx.setValue(21);                                                                                                                               //Natural: ASSIGN #FUND-INDX := 21
                                                                                                                                                                          //Natural: PERFORM STORE-FROM-FUNDS
                sub_Store_From_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THIS TRANSFER IS FROM A VARIABLE FUND
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Fund_Code_A.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I));                                                         //Natural: ASSIGN #FUND-CODE-A := IAXFR-FROM-FUND-CDE ( #I )
                pnd_Fund_Indx.setValue(pnd_Fund_Code_A_Pnd_Fund_Code_N);                                                                                                  //Natural: ASSIGN #FUND-INDX := #FUND-CODE-N
                //*  MONTHLY FUNDS IN POS +20
                if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ().getValue(pnd_I).equals(pnd_Const_Pnd_Monthly)))                                       //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #MONTHLY
                {
                    pnd_Fund_Indx.nadd(20);                                                                                                                               //Natural: ASSIGN #FUND-INDX := #FUND-INDX + 20
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM STORE-FROM-FUNDS
                sub_Store_From_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            short decideConditionsMet489 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF IAXFR-TO-FUND-CDE ( #I );//Natural: VALUE ' '
            if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet489++;
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1G
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g))))
            {
                decideConditionsMet489++;
                pnd_Fund_Indx.setValue(1);                                                                                                                                //Natural: ASSIGN #FUND-INDX := 1
                                                                                                                                                                          //Natural: PERFORM STORE-TO-FUNDS
                sub_Store_To_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1S
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))))
            {
                decideConditionsMet489++;
                pnd_Fund_Indx.setValue(21);                                                                                                                               //Natural: ASSIGN #FUND-INDX := 21
                                                                                                                                                                          //Natural: PERFORM STORE-TO-FUNDS
                sub_Store_To_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THIS TRANSFER IS TO A VARIABLE FUND
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Fund_Code_A.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I));                                                           //Natural: ASSIGN #FUND-CODE-A := IAXFR-TO-FUND-CDE ( #I )
                pnd_Fund_Indx.setValue(pnd_Fund_Code_A_Pnd_Fund_Code_N);                                                                                                  //Natural: ASSIGN #FUND-INDX := #FUND-CODE-N
                //*  MONTHLY FUNDS IN POS +20
                if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals(pnd_Const_Pnd_Monthly)))                                        //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #MONTHLY
                {
                    pnd_Fund_Indx.nadd(20);                                                                                                                               //Natural: ASSIGN #FUND-INDX := #FUND-INDX + 20
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM STORE-TO-FUNDS
                sub_Store_To_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        iaa_Xfr_Cntrl_Lst_Chnge_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Lst_Chnge_Dte());                                                                           //Natural: ASSIGN IAA-XFR-CNTRL.LST-CHNGE-DTE := #IAA-XFR-AUDIT.LST-CHNGE-DTE
        //*  ADD-NEW-IAA-XFR-CNTRL
    }
    private void sub_Store_From_Funds() throws Exception                                                                                                                  //Natural: STORE-FROM-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I));                    //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( #FUND-INDX ) := IAXFR-FROM-FUND-CDE ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Unit_Typ.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ().getValue(pnd_I));                     //Natural: ASSIGN IAXFR-CTL-FRM-UNIT-TYP ( #FUND-INDX ) := IAXFR-FRM-UNIT-TYP ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units().getValue(pnd_I));                //Natural: ASSIGN IAXFR-CTL-FRM-UNITS ( #FUND-INDX ) := IAXFR-FROM-RQSTD-XFR-UNITS ( #I )
        //*  6/99 KN
        //*  FOR GRADED FROM FUNDS ONLY
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g) || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))) //Natural: IF IAXFR-FROM-FUND-CDE ( #I ) = #RCRD-TYPE-1G OR = #RCRD-TYPE-1S
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_I));          //Natural: ASSIGN IAXFR-CTL-FRM-GUAR-AMT ( #FUND-INDX ) := IAXFR-FROM-RQSTD-XFR-GUAR ( #I )
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_I).equals(getZero())))                                                 //Natural: IF IAXFR-FROM-AFTR-XFR-GUAR ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_Fund_Indx).setValue(1);                                                                           //Natural: ASSIGN IAXFR-CTL-FRM-FULL-OUT-RCRDS ( #FUND-INDX ) := 1
            }                                                                                                                                                             //Natural: END-IF
            //*  FOR ALL VARIABLE FUNDS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_I));           //Natural: ASSIGN IAXFR-CTL-FRM-DOLLARS ( #FUND-INDX ) := IAXFR-FROM-RQSTD-XFR-GUAR ( #I )
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_I).equals(getZero())))                                                //Natural: IF IAXFR-FROM-AFTR-XFR-UNITS ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_Fund_Indx).setValue(1);                                                                           //Natural: ASSIGN IAXFR-CTL-FRM-FULL-OUT-RCRDS ( #FUND-INDX ) := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid().getValue(pnd_I));        //Natural: ASSIGN IAXFR-CTL-FRM-PER-DIVID-AMT ( #FUND-INDX ) := IAXFR-FROM-RQSTD-XFR-DIVID ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds.getValue(pnd_Fund_Indx).setValue(1);                                                                                       //Natural: ASSIGN IAXFR-CTL-FRM-FUND-RCRDS ( #FUND-INDX ) := 1
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt().getValue(pnd_I));              //Natural: ASSIGN IAXFR-CTL-FRM-ASSET-AMT ( #FUND-INDX ) := IAXFR-FROM-ASSET-XFR-AMT ( #I )
        //*  STORE-FROM-FUNDS
    }
    private void sub_Store_To_Funds() throws Exception                                                                                                                    //Natural: STORE-TO-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I));                       //Natural: ASSIGN IAXFR-CTL-TO-FUND-CDE ( #FUND-INDX ) := IAXFR-TO-FUND-CDE ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ().getValue(pnd_I));                       //Natural: ASSIGN IAXFR-CTL-TO-UNIT-TYP ( #FUND-INDX ) := IAXFR-TO-UNIT-TYP ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units().getValue(pnd_I));                         //Natural: ASSIGN IAXFR-CTL-TO-UNITS ( #FUND-INDX ) := IAXFR-TO-XFR-UNITS ( #I )
        //*  FOR GRADED AND STANDARD FUNDS
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g) || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))) //Natural: IF IAXFR-TO-FUND-CDE ( #I ) = #RCRD-TYPE-1G OR = #RCRD-TYPE-1S
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar().getValue(pnd_I));                   //Natural: ASSIGN IAXFR-CTL-TO-GUAR-AMT ( #FUND-INDX ) := IAXFR-TO-XFR-GUAR ( #I )
            //*  6/99 KN
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_I).equals(getZero())))                                                    //Natural: IF IAXFR-TO-BFR-XFR-GUAR ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs.getValue(pnd_Fund_Indx).setValue(1);                                                                                //Natural: ASSIGN IAXFR-CTL-NEW-FUND-RECS ( #FUND-INDX ) := 1
            }                                                                                                                                                             //Natural: END-IF
            //*  FOR ALL VARIABLE FUNDS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar().getValue(pnd_I));                    //Natural: ASSIGN IAXFR-CTL-TO-DOLLARS ( #FUND-INDX ) := IAXFR-TO-XFR-GUAR ( #I )
            //*  6/99 KN
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).equals(getZero())))                                                   //Natural: IF IAXFR-TO-BFR-XFR-UNITS ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs.getValue(pnd_Fund_Indx).setValue(1);                                                                                //Natural: ASSIGN IAXFR-CTL-NEW-FUND-RECS ( #FUND-INDX ) := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid().getValue(pnd_I));                 //Natural: ASSIGN IAXFR-CTL-TO-PER-DIVID-AMT ( #FUND-INDX ) := IAXFR-TO-XFR-DIVID ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds.getValue(pnd_Fund_Indx).setValue(1);                                                                                        //Natural: ASSIGN IAXFR-CTL-TO-FUND-RCRDS ( #FUND-INDX ) := 1
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt().getValue(pnd_I));                     //Natural: ASSIGN IAXFR-CTL-TO-ASSET-AMT ( #FUND-INDX ) := IAXFR-TO-ASSET-AMT ( #I )
        //*  STORE-TO-FUNDS
    }
    private void sub_Update_Iaa_Xfr_Cntrl() throws Exception                                                                                                              //Natural: UPDATE-IAA-XFR-CNTRL
    {
        if (BLNatReinput.isReinput()) return;

        iaa_Xfr_Cntrl_Iaxfr_Ctl_Accntng_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Acctng_Dte());                                                                //Natural: ASSIGN IAXFR-CTL-ACCNTNG-DTE := IAXFR-ACCTNG-DTE
        if (condition(pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Tiaa_Payee().equals(pnd_Const_Pnd_Yes)))                                                                     //Natural: IF #NEW-TIAA-PAYEE = #YES
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees.nadd(1);                                                                                                              //Natural: COMPUTE IAXFR-CTL-NEW-TIAA-PAYEES = IAXFR-CTL-NEW-TIAA-PAYEES + 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Cref_Payee().equals(pnd_Const_Pnd_Yes)))                                                                     //Natural: IF #NEW-CREF-PAYEE = #YES
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees.nadd(1);                                                                                                              //Natural: COMPUTE IAXFR-CTL-NEW-CREF-PAYEES = IAXFR-CTL-NEW-CREF-PAYEES + 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().equals(pnd_Const_Pnd_Yes)))                                                                   //Natural: IF #NEW-INACTIVE-IND = #YES
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees.nadd(1);                                                                                                          //Natural: COMPUTE IAXFR-CTL-NEW-INACTIVE-PAYEES = IAXFR-CTL-NEW-INACTIVE-PAYEES + 1
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Nbr_Rqst_Recs_Processd.nadd(1);                                                                                                           //Natural: COMPUTE IAXFR-CTL-NBR-RQST-RECS-PROCESSD = IAXFR-CTL-NBR-RQST-RECS-PROCESSD + 1
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt.compute(new ComputeParameters(false, iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt), pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt().getValue("*").add(iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt)); //Natural: ADD IAXFR-FROM-ASSET-XFR-AMT ( * ) IAXFR-CTL-TOTAL-FRM-ASSET-AMT GIVING IAXFR-CTL-TOTAL-FRM-ASSET-AMT
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt.compute(new ComputeParameters(false, iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt), pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt().getValue("*").add(iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt)); //Natural: ADD IAXFR-TO-ASSET-AMT ( * ) IAXFR-CTL-TOTAL-TO-ASSET-AMT GIVING IAXFR-CTL-TOTAL-TO-ASSET-AMT
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            short decideConditionsMet595 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF IAXFR-FROM-FUND-CDE ( #I );//Natural: VALUE ' '
            if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet595++;
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1G
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g))))
            {
                decideConditionsMet595++;
                pnd_Fund_Indx.setValue(1);                                                                                                                                //Natural: ASSIGN #FUND-INDX := 1
                                                                                                                                                                          //Natural: PERFORM UPDATE-FROM-FUNDS
                sub_Update_From_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1S
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))))
            {
                decideConditionsMet595++;
                pnd_Fund_Indx.setValue(21);                                                                                                                               //Natural: ASSIGN #FUND-INDX := 21
                                                                                                                                                                          //Natural: PERFORM UPDATE-FROM-FUNDS
                sub_Update_From_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THIS TRANSFER IS FROM A VARIABLE FUND
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Fund_Code_A.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I));                                                         //Natural: ASSIGN #FUND-CODE-A := IAXFR-FROM-FUND-CDE ( #I )
                pnd_Fund_Indx.setValue(pnd_Fund_Code_A_Pnd_Fund_Code_N);                                                                                                  //Natural: ASSIGN #FUND-INDX := #FUND-CODE-N
                //*  MONTHLY FUNDS IN POS +20
                if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ().getValue(pnd_I).equals(pnd_Const_Pnd_Monthly)))                                       //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = #MONTHLY
                {
                    pnd_Fund_Indx.nadd(20);                                                                                                                               //Natural: ASSIGN #FUND-INDX := #FUND-INDX + 20
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-FROM-FUNDS
                sub_Update_From_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            short decideConditionsMet620 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF IAXFR-TO-FUND-CDE ( #I );//Natural: VALUE ' '
            if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet620++;
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1G
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g))))
            {
                decideConditionsMet620++;
                pnd_Fund_Indx.setValue(1);                                                                                                                                //Natural: ASSIGN #FUND-INDX := 1
                                                                                                                                                                          //Natural: PERFORM UPDATE-TO-FUNDS
                sub_Update_To_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE #RCRD-TYPE-1S
            else if (condition((pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))))
            {
                decideConditionsMet620++;
                pnd_Fund_Indx.setValue(21);                                                                                                                               //Natural: ASSIGN #FUND-INDX := 21
                                                                                                                                                                          //Natural: PERFORM UPDATE-TO-FUNDS
                sub_Update_To_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THIS TRANSFER IS TO A VARIABLE FUND
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Fund_Code_A.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I));                                                           //Natural: ASSIGN #FUND-CODE-A := IAXFR-TO-FUND-CDE ( #I )
                pnd_Fund_Indx.setValue(pnd_Fund_Code_A_Pnd_Fund_Code_N);                                                                                                  //Natural: ASSIGN #FUND-INDX := #FUND-CODE-N
                //*  MONTHLY FUNDS IN POS +20
                if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals(pnd_Const_Pnd_Monthly)))                                        //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = #MONTHLY
                {
                    pnd_Fund_Indx.nadd(20);                                                                                                                               //Natural: ASSIGN #FUND-INDX := #FUND-INDX + 20
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-TO-FUNDS
                sub_Update_To_Funds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        iaa_Xfr_Cntrl_Lst_Chnge_Dte.setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Lst_Chnge_Dte());                                                                           //Natural: ASSIGN IAA-XFR-CNTRL.LST-CHNGE-DTE := #IAA-XFR-AUDIT.LST-CHNGE-DTE
        //*  UPDATE-IAA-XFR-CNTRL
    }
    private void sub_Update_From_Funds() throws Exception                                                                                                                 //Natural: UPDATE-FROM-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I));                    //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( #FUND-INDX ) := IAXFR-FROM-FUND-CDE ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Unit_Typ.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ().getValue(pnd_I));                     //Natural: ASSIGN IAXFR-CTL-FRM-UNIT-TYP ( #FUND-INDX ) := IAXFR-FRM-UNIT-TYP ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units().getValue(pnd_I));                    //Natural: ADD IAXFR-FROM-RQSTD-XFR-UNITS ( #I ) TO IAXFR-CTL-FRM-UNITS ( #FUND-INDX )
        //*  6/99 KN
        //*  FOR GRADED FROM FUNDS
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g) || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))) //Natural: IF IAXFR-FROM-FUND-CDE ( #I ) = #RCRD-TYPE-1G OR = #RCRD-TYPE-1S
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_I));              //Natural: ADD IAXFR-FROM-RQSTD-XFR-GUAR ( #I ) TO IAXFR-CTL-FRM-GUAR-AMT ( #FUND-INDX )
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_I).equals(getZero())))                                                 //Natural: IF IAXFR-FROM-AFTR-XFR-GUAR ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_Fund_Indx).nadd(1);                                                                               //Natural: ADD 1 TO IAXFR-CTL-FRM-FULL-OUT-RCRDS ( #FUND-INDX )
            }                                                                                                                                                             //Natural: END-IF
            //*  FOR ALL VARIABLE FUNDS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_I));               //Natural: ADD IAXFR-FROM-RQSTD-XFR-GUAR ( #I ) TO IAXFR-CTL-FRM-DOLLARS ( #FUND-INDX )
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_I).equals(getZero())))                                                //Natural: IF IAXFR-FROM-AFTR-XFR-UNITS ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_Fund_Indx).nadd(1);                                                                               //Natural: ADD 1 TO IAXFR-CTL-FRM-FULL-OUT-RCRDS ( #FUND-INDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid().getValue(pnd_I));            //Natural: ADD IAXFR-FROM-RQSTD-XFR-DIVID ( #I ) TO IAXFR-CTL-FRM-PER-DIVID-AMT ( #FUND-INDX )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds.getValue(pnd_Fund_Indx).nadd(1);                                                                                           //Natural: ADD 1 TO IAXFR-CTL-FRM-FUND-RCRDS ( #FUND-INDX )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt().getValue(pnd_I));                  //Natural: ADD IAXFR-FROM-ASSET-XFR-AMT ( #I ) TO IAXFR-CTL-FRM-ASSET-AMT ( #FUND-INDX )
        //*  UPDATE-FROM-FUNDS
    }
    private void sub_Update_To_Funds() throws Exception                                                                                                                   //Natural: UPDATE-TO-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I));                       //Natural: ASSIGN IAXFR-CTL-TO-FUND-CDE ( #FUND-INDX ) := IAXFR-TO-FUND-CDE ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ.getValue(pnd_Fund_Indx).setValue(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ().getValue(pnd_I));                       //Natural: ASSIGN IAXFR-CTL-TO-UNIT-TYP ( #FUND-INDX ) := IAXFR-TO-UNIT-TYP ( #I )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units().getValue(pnd_I));                             //Natural: ADD IAXFR-TO-XFR-UNITS ( #I ) TO IAXFR-CTL-TO-UNITS ( #FUND-INDX )
        //*  FOR GRADED AND STANDARD FUNDS
        if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1g) || pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde().getValue(pnd_I).equals(pnd_Const_Pnd_Rcrd_Type_1s))) //Natural: IF IAXFR-TO-FUND-CDE ( #I ) = #RCRD-TYPE-1G OR = #RCRD-TYPE-1S
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar().getValue(pnd_I));                       //Natural: ADD IAXFR-TO-XFR-GUAR ( #I ) TO IAXFR-CTL-TO-GUAR-AMT ( #FUND-INDX )
            //*  6/99 KN
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_I).equals(getZero())))                                                    //Natural: IF IAXFR-TO-BFR-XFR-GUAR ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs.getValue(pnd_Fund_Indx).setValue(1);                                                                                //Natural: ASSIGN IAXFR-CTL-NEW-FUND-RECS ( #FUND-INDX ) := 1
            }                                                                                                                                                             //Natural: END-IF
            //*  FOR ALL VARIABLE FUNDS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar().getValue(pnd_I));                        //Natural: ADD IAXFR-TO-XFR-GUAR ( #I ) TO IAXFR-CTL-TO-DOLLARS ( #FUND-INDX )
            //*  6/99 KN
            if (condition(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).equals(getZero())))                                                   //Natural: IF IAXFR-TO-BFR-XFR-UNITS ( #I ) = 0
            {
                iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs.getValue(pnd_Fund_Indx).setValue(1);                                                                                //Natural: ASSIGN IAXFR-CTL-NEW-FUND-RECS ( #FUND-INDX ) := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid().getValue(pnd_I));                     //Natural: ADD IAXFR-TO-XFR-DIVID ( #I ) TO IAXFR-CTL-TO-PER-DIVID-AMT ( #FUND-INDX )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds.getValue(pnd_Fund_Indx).nadd(1);                                                                                            //Natural: ADD 1 TO IAXFR-CTL-TO-FUND-RCRDS ( #FUND-INDX )
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt.getValue(pnd_Fund_Indx).nadd(pdaIatl420x.getPnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt().getValue(pnd_I));                         //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO IAXFR-CTL-TO-ASSET-AMT ( #FUND-INDX )
        //*  UPDATE-TO-FUNDS
    }

    //
}
