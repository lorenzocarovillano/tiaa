/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:34 AM
**        * FROM NATURAL SUBPROGRAM : Iaangrp
************************************************************
**        * FILE NAME            : Iaangrp.java
**        * CLASS NAME           : Iaangrp
**        * INSTANCE NAME        : Iaangrp
************************************************************
**********************************************************************
* PROGRAM: IAANGRP
* PURPOSE: PERIODIC IVC CALCULATION ROUTINE FOR HARVARD AND CASE WEST-
*          ERN UNIVERSITY CONTRACTS.
* DATE   : 7/2/2004
*
*
* HISTORY:
*
*
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaangrp extends BLNatBase
{
    // Data Areas
    private PdaNaza600i pdaNaza600i;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ivc_Amt;
    private DbsField pnd_Pmt_Mode;
    private DbsField pnd_Optn_Cde;
    private DbsField pnd_Orgn_Cde;
    private DbsField pnd_Issue_Dte;
    private DbsField pnd_1st_Pay_Due_Dte;
    private DbsField pnd_Finl_Per_Pay_Dte;
    private DbsField pnd_Check_Dte_N8;

    private DbsGroup pnd_Check_Dte_N8__R_Field_1;
    private DbsField pnd_Check_Dte_N8_Pnd_Check_Dte_N6;

    private DbsGroup pnd_Check_Dte_N8__R_Field_2;
    private DbsField pnd_Check_Dte_N8_Pnd_Check_Dte_A;
    private DbsField pnd_1st_Dob;
    private DbsField pnd_2nd_Dob;
    private DbsField pnd_Per_Pmt_Amt;
    private DbsField pnd_Final_Pmt_Amt;
    private DbsField pnd_Per_Ivc_Amt;
    private DbsField pnd_Ivc_Used_Amt;
    private DbsField pnd_Sim_Rule;
    private DbsField pnd_Sim_Age;
    private DbsField pnd_No_Pays;
    private DbsField pnd_No_Per_Yr;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNaza600i = new PdaNaza600i(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Ivc_Amt = parameters.newFieldInRecord("pnd_Ivc_Amt", "#IVC-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Ivc_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Pmt_Mode = parameters.newFieldInRecord("pnd_Pmt_Mode", "#PMT-MODE", FieldType.NUMERIC, 3);
        pnd_Pmt_Mode.setParameterOption(ParameterOption.ByReference);
        pnd_Optn_Cde = parameters.newFieldInRecord("pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Optn_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Orgn_Cde = parameters.newFieldInRecord("pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Orgn_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Issue_Dte = parameters.newFieldInRecord("pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Issue_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_1st_Pay_Due_Dte = parameters.newFieldInRecord("pnd_1st_Pay_Due_Dte", "#1ST-PAY-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_1st_Pay_Due_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Finl_Per_Pay_Dte = parameters.newFieldInRecord("pnd_Finl_Per_Pay_Dte", "#FINL-PER-PAY-DTE", FieldType.NUMERIC, 6);
        pnd_Finl_Per_Pay_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Dte_N8 = parameters.newFieldInRecord("pnd_Check_Dte_N8", "#CHECK-DTE-N8", FieldType.NUMERIC, 8);
        pnd_Check_Dte_N8.setParameterOption(ParameterOption.ByReference);

        pnd_Check_Dte_N8__R_Field_1 = parameters.newGroupInRecord("pnd_Check_Dte_N8__R_Field_1", "REDEFINE", pnd_Check_Dte_N8);
        pnd_Check_Dte_N8_Pnd_Check_Dte_N6 = pnd_Check_Dte_N8__R_Field_1.newFieldInGroup("pnd_Check_Dte_N8_Pnd_Check_Dte_N6", "#CHECK-DTE-N6", FieldType.NUMERIC, 
            6);

        pnd_Check_Dte_N8__R_Field_2 = parameters.newGroupInRecord("pnd_Check_Dte_N8__R_Field_2", "REDEFINE", pnd_Check_Dte_N8);
        pnd_Check_Dte_N8_Pnd_Check_Dte_A = pnd_Check_Dte_N8__R_Field_2.newFieldInGroup("pnd_Check_Dte_N8_Pnd_Check_Dte_A", "#CHECK-DTE-A", FieldType.STRING, 
            8);
        pnd_1st_Dob = parameters.newFieldInRecord("pnd_1st_Dob", "#1ST-DOB", FieldType.NUMERIC, 8);
        pnd_1st_Dob.setParameterOption(ParameterOption.ByReference);
        pnd_2nd_Dob = parameters.newFieldInRecord("pnd_2nd_Dob", "#2ND-DOB", FieldType.NUMERIC, 8);
        pnd_2nd_Dob.setParameterOption(ParameterOption.ByReference);
        pnd_Per_Pmt_Amt = parameters.newFieldInRecord("pnd_Per_Pmt_Amt", "#PER-PMT-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Per_Pmt_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Final_Pmt_Amt = parameters.newFieldInRecord("pnd_Final_Pmt_Amt", "#FINAL-PMT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Final_Pmt_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Per_Ivc_Amt = parameters.newFieldInRecord("pnd_Per_Ivc_Amt", "#PER-IVC-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Per_Ivc_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Ivc_Used_Amt = parameters.newFieldInRecord("pnd_Ivc_Used_Amt", "#IVC-USED-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Ivc_Used_Amt.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Sim_Rule = localVariables.newFieldInRecord("pnd_Sim_Rule", "#SIM-RULE", FieldType.STRING, 1);
        pnd_Sim_Age = localVariables.newFieldInRecord("pnd_Sim_Age", "#SIM-AGE", FieldType.NUMERIC, 3);
        pnd_No_Pays = localVariables.newFieldInRecord("pnd_No_Pays", "#NO-PAYS", FieldType.PACKED_DECIMAL, 5);
        pnd_No_Per_Yr = localVariables.newFieldInRecord("pnd_No_Per_Yr", "#NO-PER-YR", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaangrp() throws Exception
    {
        super("Iaangrp");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Per_Ivc_Amt.reset();                                                                                                                                          //Natural: RESET #PER-IVC-AMT #IVC-USED-AMT
        pnd_Ivc_Used_Amt.reset();
        if (condition((((((pnd_Optn_Cde.equals(3) || pnd_Optn_Cde.equals(4)) || pnd_Optn_Cde.equals(7)) || pnd_Optn_Cde.equals(8)) || (pnd_Optn_Cde.greaterOrEqual(10)    //Natural: IF #OPTN-CDE = 03 OR = 04 OR = 07 OR = 08 OR #OPTN-CDE = 10 THRU 18 OR #OPTN-CDE = 54 THRU 57
            && pnd_Optn_Cde.lessOrEqual(18))) || (pnd_Optn_Cde.greaterOrEqual(54) && pnd_Optn_Cde.lessOrEqual(57)))))
        {
            if (condition(pnd_2nd_Dob.equals(getZero())))                                                                                                                 //Natural: IF #2ND-DOB = 0
            {
                pnd_2nd_Dob.setValue(pnd_1st_Dob);                                                                                                                        //Natural: ASSIGN #2ND-DOB := #1ST-DOB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Optn_Cde.equals(21)))                                                                                                                           //Natural: IF #OPTN-CDE = 21
        {
            pnd_Sim_Rule.setValue("N");                                                                                                                                   //Natural: ASSIGN #SIM-RULE := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Iaan037m.class , getCurrentProcessState(), pnd_Sim_Rule, pnd_Issue_Dte, pnd_Optn_Cde, pnd_Orgn_Cde, pnd_1st_Dob, pnd_2nd_Dob,                 //Natural: CALLNAT 'IAAN037M' #SIM-RULE #ISSUE-DTE #OPTN-CDE #ORGN-CDE #1ST-DOB #2ND-DOB #SIM-AGE
                pnd_Sim_Age);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sim_Rule.equals("N")))                                                                                                                          //Natural: IF #SIM-RULE = 'N'
        {
            pdaNaza600i.getNaza600i_Check_D().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Check_Dte_N8_Pnd_Check_Dte_A);                                            //Natural: MOVE EDITED #CHECK-DTE-A TO NAZA600I.CHECK-D ( EM = YYYYMMDD )
            pdaNaza600i.getNaza600i_Cntrct_Optn_Cde().setValue(pnd_Optn_Cde);                                                                                             //Natural: ASSIGN NAZA600I.CNTRCT-OPTN-CDE := #OPTN-CDE
            pdaNaza600i.getNaza600i_Cntrct_Mode_Ind().setValue(pnd_Pmt_Mode);                                                                                             //Natural: ASSIGN NAZA600I.CNTRCT-MODE-IND := #PMT-MODE
            pdaNaza600i.getNaza600i_Cntrct_Issue_Dte().setValue(pnd_Issue_Dte);                                                                                           //Natural: ASSIGN NAZA600I.CNTRCT-ISSUE-DTE := #ISSUE-DTE
            pdaNaza600i.getNaza600i_Cntrct_First_Pymnt_Due_Dte().setValue(pnd_1st_Pay_Due_Dte);                                                                           //Natural: ASSIGN NAZA600I.CNTRCT-FIRST-PYMNT-DUE-DTE := #1ST-PAY-DUE-DTE
            pdaNaza600i.getNaza600i_Cntrct_First_Annt_Dob_Dte().setValue(pnd_1st_Dob);                                                                                    //Natural: ASSIGN NAZA600I.CNTRCT-FIRST-ANNT-DOB-DTE := #1ST-DOB
            pdaNaza600i.getNaza600i_Cntrct_Scnd_Annt_Dob_Dte().setValue(pnd_2nd_Dob);                                                                                     //Natural: ASSIGN NAZA600I.CNTRCT-SCND-ANNT-DOB-DTE := #2ND-DOB
            pdaNaza600i.getNaza600i_Cntrct_Final_Per_Pay_Dte().setValue(pnd_Finl_Per_Pay_Dte);                                                                            //Natural: ASSIGN NAZA600I.CNTRCT-FINAL-PER-PAY-DTE := #FINL-PER-PAY-DTE
            pdaNaza600i.getNaza600i_Cntrct_Ivc_Amt().setValue(pnd_Ivc_Amt);                                                                                               //Natural: ASSIGN NAZA600I.CNTRCT-IVC-AMT := #IVC-AMT
            pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().setValue(pnd_Per_Pmt_Amt);                                                                                          //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := #PER-PMT-AMT
            pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().setValue(pnd_Final_Pmt_Amt);                                                                                      //Natural: ASSIGN NAZA600I.#FINAL-PAY-AMT := #FINAL-PMT-AMT
            pdaNaza600i.getNaza600i_Cntrct_Company_Cd().setValue("1");                                                                                                    //Natural: ASSIGN NAZA600I.CNTRCT-COMPANY-CD := '1'
            DbsUtil.callnat(Nazn637e.class , getCurrentProcessState(), pdaNaza600i.getNaza600i());                                                                        //Natural: CALLNAT 'NAZN637E' NAZA600I
            if (condition(Global.isEscape())) return;
            pnd_Per_Ivc_Amt.setValue(pdaNaza600i.getNaza600i_Cntrct_Per_Ivc_Amt());                                                                                       //Natural: ASSIGN #PER-IVC-AMT := NAZA600I.CNTRCT-PER-IVC-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM NON-SIMPLIFIED-RULE
            sub_Non_Simplified_Rule();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NON-SIMPLIFIED-RULE
    }
    private void sub_Non_Simplified_Rule() throws Exception                                                                                                               //Natural: NON-SIMPLIFIED-RULE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet145 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PMT-MODE;//Natural: VALUE 100
        if (condition((pnd_Pmt_Mode.equals(100))))
        {
            decideConditionsMet145++;
            pnd_No_Per_Yr.setValue(12);                                                                                                                                   //Natural: ASSIGN #NO-PER-YR := 12
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pnd_Pmt_Mode.greaterOrEqual(601) && pnd_Pmt_Mode.lessOrEqual(603)))))
        {
            decideConditionsMet145++;
            pnd_No_Per_Yr.setValue(4);                                                                                                                                    //Natural: ASSIGN #NO-PER-YR := 4
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pnd_Pmt_Mode.greaterOrEqual(701) && pnd_Pmt_Mode.lessOrEqual(706)))))
        {
            decideConditionsMet145++;
            pnd_No_Per_Yr.setValue(2);                                                                                                                                    //Natural: ASSIGN #NO-PER-YR := 2
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pnd_Pmt_Mode.greaterOrEqual(801) && pnd_Pmt_Mode.lessOrEqual(812)))))
        {
            decideConditionsMet145++;
            pnd_No_Per_Yr.setValue(1);                                                                                                                                    //Natural: ASSIGN #NO-PER-YR := 1
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_No_Per_Yr.setValue(12);                                                                                                                                   //Natural: ASSIGN #NO-PER-YR := 12
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition((pnd_Issue_Dte.greater(199712) && (((((pnd_Optn_Cde.equals(3) || pnd_Optn_Cde.equals(4)) || pnd_Optn_Cde.equals(7)) || pnd_Optn_Cde.equals(8))      //Natural: IF #ISSUE-DTE GT 199712 AND ( #OPTN-CDE = 03 OR = 04 OR = 07 OR = 08 OR #OPTN-CDE = 10 THRU 17 OR #OPTN-CDE = 54 THRU 57 )
            || (pnd_Optn_Cde.greaterOrEqual(10) && pnd_Optn_Cde.lessOrEqual(17))) || (pnd_Optn_Cde.greaterOrEqual(54) && pnd_Optn_Cde.lessOrEqual(57))))))
        {
            short decideConditionsMet160 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SIM-AGE GT 140
            if (condition(pnd_Sim_Age.greater(140)))
            {
                decideConditionsMet160++;
                pnd_No_Pays.setValue(210);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 210
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 130
            else if (condition(pnd_Sim_Age.greater(130)))
            {
                decideConditionsMet160++;
                pnd_No_Pays.setValue(260);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 260
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 120
            else if (condition(pnd_Sim_Age.greater(120)))
            {
                decideConditionsMet160++;
                pnd_No_Pays.setValue(310);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 310
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 110
            else if (condition(pnd_Sim_Age.greater(110)))
            {
                decideConditionsMet160++;
                pnd_No_Pays.setValue(360);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 360
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_No_Pays.setValue(410);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 410
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet175 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SIM-AGE GT 70
            if (condition(pnd_Sim_Age.greater(70)))
            {
                decideConditionsMet175++;
                pnd_No_Pays.setValue(160);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 160
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 65
            else if (condition(pnd_Sim_Age.greater(65)))
            {
                decideConditionsMet175++;
                pnd_No_Pays.setValue(210);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 210
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 60
            else if (condition(pnd_Sim_Age.greater(60)))
            {
                decideConditionsMet175++;
                pnd_No_Pays.setValue(260);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 260
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 55
            else if (condition(pnd_Sim_Age.greater(55)))
            {
                decideConditionsMet175++;
                pnd_No_Pays.setValue(310);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 310
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE LE 55
            else if (condition(pnd_Sim_Age.lessOrEqual(55)))
            {
                decideConditionsMet175++;
                pnd_No_Pays.setValue(360);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 360
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_No_Pays.setValue(360);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 360
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Per_Ivc_Amt.compute(new ComputeParameters(true, pnd_Per_Ivc_Amt), (pnd_Ivc_Amt.multiply(12)).divide((pnd_No_Pays.multiply(pnd_No_Per_Yr))));                  //Natural: COMPUTE ROUNDED #PER-IVC-AMT = ( #IVC-AMT * 12 ) / ( #NO-PAYS * #NO-PER-YR )
    }

    //
}
