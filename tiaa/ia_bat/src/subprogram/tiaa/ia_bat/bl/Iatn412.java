/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:44:53 AM
**        * FROM NATURAL SUBPROGRAM : Iatn412
************************************************************
**        * FILE NAME            : Iatn412.java
**        * CLASS NAME           : Iatn412
**        * INSTANCE NAME        : Iatn412
************************************************************
*********************************************************************
*             ***  SWITCH REJECTION LETTER  ***
*
*  PROG NAME :- IATN412 (VERY SIMILAR TO IATN570)
*  AUTHOR    :- LEN B
*  DATE      :- FEB '98
*  CHANGES   :-
*
*  05/01/02 TD ADDED #ACCT-TCKR IN IATN216 PARM
*  01/21/09 OS TIAA ACCESS CHANGES. SC 012109.
*********************************************************************
* NOTE:- THIS SUBPROG DOES NOT PERFORMS AN ET OR A BT.
*
* THIS MODULE INTERFACES WITH POST SYSTEM TO PRODUCE REJECTION LETTER.
* WHICH IS VERY SIMILAR TO THE ACKNOWLEDGEMENT LETTER.
*
* THIS PROGRAM CALLS FOLLOWING MODULES;
*
*  1. IATN216  - PRODUCT INFORMATION
*  2. NAZN553  - CONVERSION OF NAME TO LOWER CASE
*  3. PSTN9610 - POST OPEN
*  4. PSTN9502 - POST WRITE
*  5. PSTN9680 - POST CLOSE
*
* 08/01/99 KN CHANGED MIT  STATUS CODE
*             CHANGE CONTACT METHOD TO ALWAYS BE "V"
* 02/20/09 OS TIAA ACCESS CHANGES. SC 022009.
* 04/22/09    PUT A DASH (-) BETEWEEN CONTRACT AND 'REA' AS
*             REQUESTED BY LIZETTE.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn412 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl412p pdaIatl412p;
    private LdaPstl6055 ldaPstl6055;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst_View;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Cntct_Mde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Srce;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Rep_Nme;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Sttmnt_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Opn_Clsd_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Rqst_Ssn;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Mit_Log_Dte_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_To_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_View_Ia_To_Payee;

    private DbsGroup pnd_Iatn0201_Out;
    private DbsField pnd_Iatn0201_Out_Pnd_Post_Fund_N2;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Tiaa_Std;
    private DbsField pnd_Const_Pnd_Tiaa_Grad;
    private DbsField pnd_Const_Pnd_Tiaa_Re;
    private DbsField pnd_Const_Pnd_Tiaa_Acc;
    private DbsField pnd_Const_Pnd_Annual;
    private DbsField pnd_Const_Pnd_Yearly;
    private DbsField pnd_Const_Pnd_Percent;
    private DbsField pnd_Const_Pnd_Dollars;
    private DbsField pnd_Const_Pnd_Units;
    private DbsField pnd_Const_Pnd_Mit_Status;
    private DbsField pnd_From_Re;
    private DbsField pnd_From_Acc;
    private DbsField pnd_From_Cref;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_1;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Date_Yyyymmdd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_2;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyy;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Dd;
    private DbsField pnd_Mod_Ts;

    private DbsGroup pnd_Mod_Ts__R_Field_3;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Mm;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Dd;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Date_Yyyy;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Time;

    private DbsGroup pnd_Mod_Ts__R_Field_4;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Hrs;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Mins;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Secs;
    private DbsField pnd_Mod_Ts_Pnd_Mod_Am;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField temp_Ia_Frm_Payee_A;

    private DbsGroup temp_Ia_Frm_Payee_A__R_Field_5;
    private DbsField temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N;
    private DbsField pnd_Appl_Key_Counter;
    private DbsField pnd_Prod_Code_N;
    private DbsField pnd_Name;
    private DbsField pnd_Post_Error;

    private DbsGroup iatn216_Data_Area;
    private DbsField iatn216_Data_Area_Pnd_Prod_Ct;
    private DbsField iatn216_Data_Area_Pnd_Prod_Cd;
    private DbsField iatn216_Data_Area_Pnd_Acct_Nme_5;
    private DbsField iatn216_Data_Area_Pnd_Acct_Tckr;
    private DbsField iatn216_Data_Area_Pnd_Acct_Effctve_Dte;
    private DbsField iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind;
    private DbsField iatn216_Data_Area_Pnd_Prod_Cd_N;
    private DbsField iatn216_Data_Area_Pnd_Acct_Rpt_Seq;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPstl6055 = new LdaPstl6055();
        registerRecord(ldaPstl6055);
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl412p = new PdaIatl412p(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_iaa_Trnsfr_Sw_Rqst_View = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst_View", "IAA-TRNSFR-SW-RQST-VIEW"), "IAA_TRNSFR_SW_RQST", 
            "IA_TRANSFER_KDO", DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde", "RCRD-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            34, RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Dte", "RQST-ENTRY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Tme = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_Tme", "RQST-ENTRY-TME", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RQST_ENTRY_TME");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte", 
            "RQST-LST-ACTVTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_User_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rcvd_User_Id", 
            "RQST-RCVD-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Entry_User_Id", 
            "RQST-ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Cntct_Mde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_CNTCT_MDE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Srce = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_SRCE");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Rep_Nme = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Rep_Nme", "RQST-REP-NME", 
            FieldType.STRING, 40, RepeatingFieldStrategy.None, "RQST_REP_NME");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Sttmnt_Ind = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_STTMNT_IND");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Opn_Clsd_Ind = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Opn_Clsd_Ind", 
            "RQST-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        iaa_Trnsfr_Sw_Rqst_View_Rqst_Ssn = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RQST_SSN");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id", 
            "XFR-WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Mit_Log_Dte_Tme = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Mit_Log_Dte_Tme", 
            "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Stts_Cde", "XFR-STTS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee", "IA-FRM-PAYEE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id", "IA-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_Frm_Acct_Dta", 
            "C*XFR-FRM-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta", 
            "XFR-FRM-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde", 
            "XFR-FRM-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ", 
            "XFR-FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ", "XFR-FRM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty", "XFR-FRM-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Est_Amt = iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Est_Amt", 
            "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Count_Castxfr_To_Acct_Dta", 
            "C*XFR-TO-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Cde", 
            "XFR-TO-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Unit_Typ", 
            "XFR-TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ", "XFR-TO-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Qty", "XFR-TO-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Est_Amt = iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Est_Amt", 
            "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_To_Cntrct", "IA-TO-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_View_Ia_To_Payee = vw_iaa_Trnsfr_Sw_Rqst_View.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_View_Ia_To_Payee", "IA-TO-PAYEE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst_View);

        pnd_Iatn0201_Out = localVariables.newGroupInRecord("pnd_Iatn0201_Out", "#IATN0201-OUT");
        pnd_Iatn0201_Out_Pnd_Post_Fund_N2 = pnd_Iatn0201_Out.newFieldInGroup("pnd_Iatn0201_Out_Pnd_Post_Fund_N2", "#POST-FUND-N2", FieldType.NUMERIC, 
            2);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Tiaa_Std = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Std", "#TIAA-STD", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Grad = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Grad", "#TIAA-GRAD", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Re = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Re", "#TIAA-RE", FieldType.STRING, 1);
        pnd_Const_Pnd_Tiaa_Acc = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Tiaa_Acc", "#TIAA-ACC", FieldType.STRING, 1);
        pnd_Const_Pnd_Annual = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Annual", "#ANNUAL", FieldType.STRING, 1);
        pnd_Const_Pnd_Yearly = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Yearly", "#YEARLY", FieldType.STRING, 1);
        pnd_Const_Pnd_Percent = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Percent", "#PERCENT", FieldType.STRING, 1);
        pnd_Const_Pnd_Dollars = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Dollars", "#DOLLARS", FieldType.STRING, 1);
        pnd_Const_Pnd_Units = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Units", "#UNITS", FieldType.STRING, 1);
        pnd_Const_Pnd_Mit_Status = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Mit_Status", "#MIT-STATUS", FieldType.STRING, 4);
        pnd_From_Re = localVariables.newFieldInRecord("pnd_From_Re", "#FROM-RE", FieldType.STRING, 1);
        pnd_From_Acc = localVariables.newFieldInRecord("pnd_From_Acc", "#FROM-ACC", FieldType.STRING, 1);
        pnd_From_Cref = localVariables.newFieldInRecord("pnd_From_Cref", "#FROM-CREF", FieldType.STRING, 1);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_1", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_1.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Date_Yyyymmdd__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_2", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyy = pnd_Date_Yyyymmdd__R_Field_2.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Yyyymmdd_Pnd_Date_Mm = pnd_Date_Yyyymmdd__R_Field_2.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Yyyymmdd_Pnd_Date_Dd = pnd_Date_Yyyymmdd__R_Field_2.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Mod_Ts = localVariables.newFieldInRecord("pnd_Mod_Ts", "#MOD-TS", FieldType.NUMERIC, 15);

        pnd_Mod_Ts__R_Field_3 = localVariables.newGroupInRecord("pnd_Mod_Ts__R_Field_3", "REDEFINE", pnd_Mod_Ts);
        pnd_Mod_Ts_Pnd_Mod_Date_Mm = pnd_Mod_Ts__R_Field_3.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Mm", "#MOD-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Date_Dd = pnd_Mod_Ts__R_Field_3.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Dd", "#MOD-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Date_Yyyy = pnd_Mod_Ts__R_Field_3.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Date_Yyyy", "#MOD-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Mod_Ts_Pnd_Mod_Time = pnd_Mod_Ts__R_Field_3.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Time", "#MOD-TIME", FieldType.NUMERIC, 7);

        pnd_Mod_Ts__R_Field_4 = pnd_Mod_Ts__R_Field_3.newGroupInGroup("pnd_Mod_Ts__R_Field_4", "REDEFINE", pnd_Mod_Ts_Pnd_Mod_Time);
        pnd_Mod_Ts_Pnd_Mod_Hrs = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Hrs", "#MOD-HRS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Mins = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Mins", "#MOD-MINS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Secs = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Secs", "#MOD-SECS", FieldType.NUMERIC, 2);
        pnd_Mod_Ts_Pnd_Mod_Am = pnd_Mod_Ts__R_Field_4.newFieldInGroup("pnd_Mod_Ts_Pnd_Mod_Am", "#MOD-AM", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        temp_Ia_Frm_Payee_A = localVariables.newFieldInRecord("temp_Ia_Frm_Payee_A", "TEMP-IA-FRM-PAYEE-A", FieldType.STRING, 2);

        temp_Ia_Frm_Payee_A__R_Field_5 = localVariables.newGroupInRecord("temp_Ia_Frm_Payee_A__R_Field_5", "REDEFINE", temp_Ia_Frm_Payee_A);
        temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N = temp_Ia_Frm_Payee_A__R_Field_5.newFieldInGroup("temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N", "TEMP-IA-FRM-PAYEE-N", 
            FieldType.NUMERIC, 2);
        pnd_Appl_Key_Counter = localVariables.newFieldInRecord("pnd_Appl_Key_Counter", "#APPL-KEY-COUNTER", FieldType.NUMERIC, 5);
        pnd_Prod_Code_N = localVariables.newFieldArrayInRecord("pnd_Prod_Code_N", "#PROD-CODE-N", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 50);
        pnd_Post_Error = localVariables.newFieldInRecord("pnd_Post_Error", "#POST-ERROR", FieldType.STRING, 1);

        iatn216_Data_Area = localVariables.newGroupInRecord("iatn216_Data_Area", "IATN216-DATA-AREA");
        iatn216_Data_Area_Pnd_Prod_Ct = iatn216_Data_Area.newFieldInGroup("iatn216_Data_Area_Pnd_Prod_Ct", "#PROD-CT", FieldType.PACKED_DECIMAL, 3);
        iatn216_Data_Area_Pnd_Prod_Cd = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Prod_Cd", "#PROD-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        iatn216_Data_Area_Pnd_Acct_Nme_5 = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 
            6, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Tckr = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Tckr", "#ACCT-TCKR", FieldType.STRING, 10, 
            new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Effctve_Dte = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", 
            FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Prod_Cd_N = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Prod_Cd_N", "#PROD-CD-N", FieldType.NUMERIC, 2, 
            new DbsArrayController(1, 20));
        iatn216_Data_Area_Pnd_Acct_Rpt_Seq = iatn216_Data_Area.newFieldArrayInGroup("iatn216_Data_Area_Pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst_View.reset();

        ldaPstl6055.initializeValues();

        localVariables.reset();
        pnd_Const_Pnd_Tiaa_Std.setInitialValue("T");
        pnd_Const_Pnd_Tiaa_Grad.setInitialValue("G");
        pnd_Const_Pnd_Tiaa_Re.setInitialValue("R");
        pnd_Const_Pnd_Tiaa_Acc.setInitialValue("D");
        pnd_Const_Pnd_Annual.setInitialValue("A");
        pnd_Const_Pnd_Yearly.setInitialValue("Y");
        pnd_Const_Pnd_Percent.setInitialValue("P");
        pnd_Const_Pnd_Dollars.setInitialValue("D");
        pnd_Const_Pnd_Units.setInitialValue("U");
        pnd_Const_Pnd_Mit_Status.setInitialValue("8050");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn412() throws Exception
    {
        super("Iatn412");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ======================================================================
        //*                              START OF SUB-PROGRAM
        //* ======================================================================
        pdaIatl412p.getPnd_Iatn412_Out().reset();                                                                                                                         //Natural: RESET #IATN412-OUT
        if (condition(pdaIatl412p.getPnd_Iatn412_In_Rqst_Id().equals(" ")))                                                                                               //Natural: IF #IATN412-IN.RQST-ID = ' '
        {
            pdaIatl412p.getPnd_Iatn412_Out_Pnd_Return_Code().setValue("E");                                                                                               //Natural: MOVE 'E' TO #RETURN-CODE
            pdaIatl412p.getPnd_Iatn412_Out_Pnd_Msg().setValue("Request id is blank");                                                                                     //Natural: MOVE 'Request id is blank' TO #MSG
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
            //*  FOR POST & EFM CONSTRUCT
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Iatn216.class , getCurrentProcessState(), iatn216_Data_Area);                                                                                     //Natural: CALLNAT 'IATN216' IATN216-DATA-AREA
        if (condition(Global.isEscape())) return;
        vw_iaa_Trnsfr_Sw_Rqst_View.startDatabaseFind                                                                                                                      //Natural: FIND ( 1 ) IAA-TRNSFR-SW-RQST-VIEW WITH IAA-TRNSFR-SW-RQST-VIEW.RQST-ID = #IATN412-IN.RQST-ID
        (
        "FND1",
        new Wc[] { new Wc("RQST_ID", "=", pdaIatl412p.getPnd_Iatn412_In_Rqst_Id(), WcType.WITH) },
        1
        );
        FND1:
        while (condition(vw_iaa_Trnsfr_Sw_Rqst_View.readNextRow("FND1", true)))
        {
            vw_iaa_Trnsfr_Sw_Rqst_View.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Trnsfr_Sw_Rqst_View.getAstCOUNTER().equals(0)))                                                                                          //Natural: IF NO RECORD FOUND
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RQST Id '", pdaIatl412p.getPnd_Iatn412_In_Rqst_Id(),  //Natural: COMPRESS 'RQST Id "' #IATN412-IN.RQST-ID '" not on file.' INTO ##MSG LEAVING NO SPACE
                    "' not on file."));
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            //*   '1'=TRANSFER  '2'=SWITCH
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Rcrd_Type_Cde.notEquals("2")))                                                                                          //Natural: IF RCRD-TYPE-CDE NE '2'
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Invalid Record Type. RCRD-TYPE-CDE must be 2."));                                    //Natural: COMPRESS 'Invalid Record Type. RCRD-TYPE-CDE must be 2.' INTO ##MSG
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  OPEN POST
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-OPEN
            sub_Call_Post_For_Open();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-AC-CONTRACT-REC
            sub_Write_Ac_Contract_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-AC-FROM-REC
            sub_Write_Ac_From_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-AC-FYI-REC
            sub_Write_Ac_Fyi_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CLOSE POST
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
            sub_Call_Post_For_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *********************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
            //*  FND1.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* ======================================================================
        //*                              START OF SUBROUTINES
        //* ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-OPEN
        //* *PSTA9611.PIN-NBR         := IA-UNIQUE-ID
        //*  PSTA9611
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AC-CONTRACT-REC
        //*  REQUEST MODE:- 'C'=PHONE 'M'=MAIL 'V'=PERSONAL VISIT  'A'=ATS REQUEST
        //*  MOVE IAA-TRNSFR-SW-RQST-VIEW.RQST-CNTCT-MDE  TO AC-REQ-SOURCE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AC-FROM-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AC-FYI-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-CLOSE
        //*  PSTA9611
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-CONTRACT-NUMBERS
        //* ***********************************************************************
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pdaIatl412p.getPnd_Iatn412_Out_Pnd_Return_Code().setValue("E");                                                                                                   //Natural: MOVE 'E' TO #RETURN-CODE
        pdaIatl412p.getPnd_Iatn412_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), " (",                //Natural: COMPRESS ##MSG ' (' *PROGRAM ')' INTO #MSG LEAVING NO
            Global.getPROGRAM(), ")"));
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( FND1. )
        Global.setEscapeCode(EscapeType.Bottom, "FND1");
        if (true) return;
    }
    private void sub_Call_Post_For_Open() throws Exception                                                                                                                //Natural: CALL-POST-FOR-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  082017 START - REPLACED PSTA9611 WITH PSTA9612
        //*  082017 NEW
        //*  082017 NEW
        pdaPsta9612.getPsta9612().reset();                                                                                                                                //Natural: RESET PSTA9612
        pdaPsta9612.getPsta9612_Univ_Id().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Unique_Id);                                                                                 //Natural: ASSIGN PSTA9612.UNIV-ID := IA-UNIQUE-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("IAIQ");                                                                                                          //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'IAIQ'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFIAIL");                                                                                                           //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFIAIL'
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                        //Natural: ASSIGN PSTA9612.PCKGE-DLVRY-TYP := '   '
        pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue(pdaIatl412p.getPnd_Iatn412_In_Pnd_Printer_Id());                                                                  //Natural: ASSIGN PSTA9612.PRNTR-ID-CDE := #PRINTER-ID
        pdaPsta9612.getPsta9612_Tiaa_Cntrct_Nbr().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                                        //Natural: ASSIGN PSTA9612.TIAA-CNTRCT-NBR := IA-FRM-CNTRCT
        temp_Ia_Frm_Payee_A.setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Payee);                                                                                               //Natural: ASSIGN TEMP-IA-FRM-PAYEE-A := IA-FRM-PAYEE
        pdaPsta9612.getPsta9612_Payee_Cde().setValue(temp_Ia_Frm_Payee_A_Temp_Ia_Frm_Payee_N);                                                                            //Natural: ASSIGN PSTA9612.PAYEE-CDE := TEMP-IA-FRM-PAYEE-N
        pdaPsta9612.getPsta9612_Dont_Use_Finalist().setValue(false);                                                                                                      //Natural: ASSIGN DONT-USE-FINALIST := FALSE
        pdaPsta9612.getPsta9612_Addrss_Lines_Rule().setValue(false);                                                                                                      //Natural: ASSIGN ADDRSS-LINES-RULE := FALSE
        pdaPsta9612.getPsta9612_Last_Nme().setValue(" ");                                                                                                                 //Natural: ASSIGN LAST-NME := ' '
        pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(" ");                                                                                                           //Natural: ASSIGN ADDRSS-TYP-CDE := ' '
        DbsUtil.callnat(Pstn9525.class , getCurrentProcessState(), pdaPsta9612.getPsta9612_Letter_Dte(), pdaCwfpda_M.getMsg_Info_Sub());                                  //Natural: CALLNAT 'PSTN9525' LETTER-DTE MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaPsta9612.getPsta9612_Next_Bsnss_Day_Ind().setValue(true);                                                                                                      //Natural: ASSIGN NEXT-BSNSS-DAY-IND := TRUE
        pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(" ");                                                                                                           //Natural: ASSIGN LTTR-SLTTN-TXT := ' '
        pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue(" ");                                                                                                          //Natural: ASSIGN EMPL-SGNTRY-NME := ' '
        pdaPsta9612.getPsta9612_Empl_Unit_Work_Nme().setValue(" ");                                                                                                       //Natural: ASSIGN EMPL-UNIT-WORK-NME := ' '
        pdaPsta9612.getPsta9612_Print_Fclty_Cde().setValue("S");                                                                                                          //Natural: ASSIGN PRINT-FCLTY-CDE := 'S'
        pdaPsta9612.getPsta9612_Form_Lbrry_Id().setValue(" ");                                                                                                            //Natural: ASSIGN FORM-LBRRY-ID := ' '
        pdaPsta9612.getPsta9612_Fnshng_Cde().setValue(" ");                                                                                                               //Natural: ASSIGN FNSHNG-CDE := ' '
        pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Mit_Log_Dte_Tme);                                                     //Natural: ASSIGN PSTA9612.RQST-LOG-DTE-TME ( 1 ) := XFR-MIT-LOG-DTE-TME
        pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(1).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Work_Prcss_Id);                                                          //Natural: ASSIGN PSTA9612.WORK-PRCSS-ID ( 1 ) := XFR-WORK-PRCSS-ID
        pdaPsta9612.getPsta9612_Mit_Contract_Nbr().getValue(1,1).setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                         //Natural: ASSIGN PSTA9612.MIT-CONTRACT-NBR ( 1,1 ) := IA-FRM-CNTRCT
        pdaPsta9612.getPsta9612_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                               //Natural: ASSIGN PSTA9612.WPID-VLDTE-IND ( 1 ) := 'Y'
        pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue(pnd_Const_Pnd_Mit_Status);                                                                              //Natural: ASSIGN PSTA9612.STATUS-CDE ( 1 ) := #MIT-STATUS
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(false);                                                                                                    //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := FALSE
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(false);                                                                                                    //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := FALSE
        //*  082017 END REPLACED
        //*  CALL POST OPEN
        //* *CALLNAT 'PSTN9610'                              /* 082017 START
        //*  082017 END
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST-FOR-OPEN
    }
    private void sub_Write_Ac_Contract_Rec() throws Exception                                                                                                             //Natural: WRITE-AC-CONTRACT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MOVE DATA INTO AC-CONTRACT-STRUCT
        //*  CREF, TIAA AND TIAA REA
                                                                                                                                                                          //Natural: PERFORM FILL-CONTRACT-NUMBERS
        sub_Fill_Contract_Numbers();
        if (condition(Global.isEscape())) {return;}
        ldaPstl6055.getAc_Contract_Struct_Ac_Req_Source().setValue("V");                                                                                                  //Natural: ASSIGN AC-REQ-SOURCE := 'V'
        //*  CONVERT THE SERVICE REPS NAME TO LOWER CASE
        //*  MOVE IAA-TRNSFR-SW-RQST-VIEW.RQST-REP-NME TO #NAME
        //*  CALLNAT 'NAZN553' #NAME
        //*  MOVE #NAME TO AC-SERVICE-REP
        ldaPstl6055.getAc_Contract_Struct_Ac_Ack_Type().setValue(1);                                                                                                      //Natural: MOVE 1 TO AC-ACK-TYPE
        //*  SWITCH
        ldaPstl6055.getAc_Contract_Struct_Ac_Frm_Switch().setValue("S");                                                                                                  //Natural: MOVE 'S' TO AC-FRM-SWITCH
        pnd_Date_A.setValueEdited(iaa_Trnsfr_Sw_Rqst_View_Rqst_Effctv_Dte,new ReportEditMask("MMDDYYYY"));                                                                //Natural: MOVE EDITED RQST-EFFCTV-DTE ( EM = MMDDYYYY ) TO #DATE-A
        ldaPstl6055.getAc_Contract_Struct_Ac_Effective_Date().setValue(pnd_Date_A_Pnd_Date_N);                                                                            //Natural: MOVE #DATE-N TO AC-EFFECTIVE-DATE
        //*  GET VALUES FOR AC-MOD-DATE-TIME
        pnd_Date_A.setValueEdited(iaa_Trnsfr_Sw_Rqst_View_Rqst_Lst_Actvty_Dte,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED RQST-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Date_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                                //Natural: MOVE #DATE-N TO #DATE-YYYYMMDD
        pnd_Mod_Ts_Pnd_Mod_Date_Mm.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Mm);                                                                                               //Natural: MOVE #DATE-MM TO #MOD-DATE-MM
        pnd_Mod_Ts_Pnd_Mod_Date_Dd.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Dd);                                                                                               //Natural: MOVE #DATE-DD TO #MOD-DATE-DD
        pnd_Mod_Ts_Pnd_Mod_Date_Yyyy.setValue(pnd_Date_Yyyymmdd_Pnd_Date_Yyyy);                                                                                           //Natural: MOVE #DATE-YYYY TO #MOD-DATE-YYYY
        pnd_Mod_Ts_Pnd_Mod_Time.setValue(Global.getTIMN());                                                                                                               //Natural: MOVE *TIMN TO #MOD-TIME
        if (condition(pnd_Mod_Ts_Pnd_Mod_Hrs.greater(12)))                                                                                                                //Natural: IF #MOD-HRS GT 12
        {
            pnd_Mod_Ts_Pnd_Mod_Hrs.nsubtract(12);                                                                                                                         //Natural: SUBTRACT 12 FROM #MOD-HRS
            //*  PM
            pnd_Mod_Ts_Pnd_Mod_Am.setValue(1);                                                                                                                            //Natural: MOVE 1 TO #MOD-AM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  AM
            pnd_Mod_Ts_Pnd_Mod_Am.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #MOD-AM
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6055.getAc_Contract_Struct_Ac_Mod_Date_Time().setValue(pnd_Mod_Ts);                                                                                        //Natural: MOVE #MOD-TS TO AC-MOD-DATE-TIME
        //*  TRANS TYPE P=PERCENT D=DOLLARS U=UNITS
        ldaPstl6055.getAc_Contract_Struct_Ac_From_Tran_Type().setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(1));                                                  //Natural: MOVE XFR-FRM-TYP ( 1 ) TO AC-FROM-TRAN-TYPE
        ldaPstl6055.getAc_Contract_Struct_Ac_To_Tran_Type().setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_To_Typ.getValue(1));                                                     //Natural: MOVE XFR-TO-TYP ( 1 ) TO AC-TO-TRAN-TYPE
        //*  ??
        ldaPstl6055.getAc_Contract_Struct_Ac_Mach_Func_1().setValue(2);                                                                                                   //Natural: MOVE 2 TO AC-MACH-FUNC-1
        //*  ??
        ldaPstl6055.getAc_Contract_Struct_Ac_Image_Ind().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO AC-IMAGE-IND
        //*   SET UP SORT KEY
        ldaPstl6055.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6055.getSort_Key_Struct_Key_L3_Req_Date_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                     //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO SORT-KEY-STRUCT.KEY-L3-REQ-DATE-TIME
        ldaPstl6055.getSort_Key_Struct_Key_L3_From_Cont_Number().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                         //Natural: ASSIGN SORT-KEY-STRUCT.KEY-L3-FROM-CONT-NUMBER := IA-FRM-CNTRCT
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-CONTRACT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6055.getSort_Key_Struct(), ldaPstl6055.getAc_Contract_Struct());
        if (condition(Global.isEscape())) return;
        //*   PSTL6145-DATA
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-AC-CONTRACT-REC
    }
    private void sub_Write_Ac_From_Rec() throws Exception                                                                                                                 //Natural: WRITE-AC-FROM-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Rec().getValue("*").reset();                                                                                              //Natural: RESET AC-FRM-REC-STRUCT.AC-FRM-REC ( * ) AC-FRM-PERCENT ( * ) AC-FRM-DOLLARS ( * ) AC-FRM-UNITS ( * )
        ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Percent().getValue("*").reset();
        ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Dollars().getValue("*").reset();
        ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Units().getValue("*").reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  SET AC-DATA-OCCURS
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(" ") && iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(" ")))       //Natural: IF XFR-FRM-ACCT-CDE ( #I ) = ' ' AND XFR-FRM-TYP ( #I ) = ' '
            {
                ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Data_Occurs().compute(new ComputeParameters(false, ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Data_Occurs()),                //Natural: ASSIGN AC-FRM-REC-STRUCT.AC-DATA-OCCURS := #I - 1
                    pnd_I.subtract(1));
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Data_Occurs().setValue(pnd_I);                                                                                        //Natural: ASSIGN AC-FRM-REC-STRUCT.AC-DATA-OCCURS := #I
            }                                                                                                                                                             //Natural: END-IF
            //*  CONVERT ONE BYTE FUND CODE TO THE (N2) FUND CODE USED BY POST
            if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF XFR-FRM-ACCT-CDE ( #I ) NE ' '
            {
                DbsUtil.callnat(Iatn0201.class , getCurrentProcessState(), iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I), pnd_Iatn0201_Out_Pnd_Post_Fund_N2);  //Natural: CALLNAT 'IATN0201' XFR-FRM-ACCT-CDE ( #I ) #IATN0201-OUT.#POST-FUND-N2
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Account_Type().getValue(pnd_I).setValue(pnd_Iatn0201_Out_Pnd_Post_Fund_N2);                                       //Natural: ASSIGN AC-FRM-ACCOUNT-TYPE ( #I ) := #IATN0201-OUT.#POST-FUND-N2
                if (condition(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Annual)))                                                     //Natural: IF XFR-FRM-UNIT-TYP ( #I ) = #ANNUAL
                {
                    ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Myi().getValue(pnd_I).setValue(pnd_Const_Pnd_Yearly);                                                         //Natural: ASSIGN AC-FRM-MYI ( #I ) := #YEARLY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Myi().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Unit_Typ.getValue(pnd_I));                     //Natural: ASSIGN AC-FRM-MYI ( #I ) := XFR-FRM-UNIT-TYP ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK IF THE SWITCH IS FROM PERCENT, DOLLARS OR UNITS
            short decideConditionsMet1888 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF XFR-FRM-TYP ( #I );//Natural: VALUE #PERCENT
            if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Percent))))
            {
                decideConditionsMet1888++;
                ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Percent().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty.getValue(pnd_I));                          //Natural: ASSIGN AC-FRM-PERCENT ( #I ) := XFR-FRM-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #DOLLARS
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Dollars))))
            {
                decideConditionsMet1888++;
                ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Dollars().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty.getValue(pnd_I));                          //Natural: ASSIGN AC-FRM-DOLLARS ( #I ) := XFR-FRM-QTY ( #I )
            }                                                                                                                                                             //Natural: VALUE #UNITS
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I).equals(pnd_Const_Pnd_Units))))
            {
                decideConditionsMet1888++;
                ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Units().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Qty.getValue(pnd_I));                            //Natural: ASSIGN AC-FRM-UNITS ( #I ) := XFR-FRM-QTY ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Trn_Typ().getValue(pnd_I).setValue(iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Typ.getValue(pnd_I));                              //Natural: ASSIGN AC-FRM-TRN-TYP ( #I ) := XFR-FRM-TYP ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6055.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6055.getSort_Key_Struct_Key_L4_Account_Type().setValue(ldaPstl6055.getAc_Frm_Rec_Struct_Ac_Frm_Account_Type().getValue(1));                                //Natural: MOVE AC-FRM-ACCOUNT-TYPE ( 1 ) TO SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE
        ldaPstl6055.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(0);                                                                                                  //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM
        ldaPstl6055.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-FRM-REC-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6055.getSort_Key_Struct(), ldaPstl6055.getAc_Frm_Rec_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-AC-FROM-REC
    }
    private void sub_Write_Ac_Fyi_Rec() throws Exception                                                                                                                  //Natural: WRITE-AC-FYI-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        pnd_J.nadd(1);                                                                                                                                                    //Natural: ASSIGN #J := #J + 1
        //*  "If you have any Questions..."
        ldaPstl6055.getAc_Fyi_Struct_Ac_Fyi_Msg_Number().getValue(pnd_J).setValue(20);                                                                                    //Natural: MOVE 20 TO AC-FYI-MSG-NUMBER ( #J )
        ldaPstl6055.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(pnd_J).setValue(20);                                                                                       //Natural: MOVE 020 TO AC-FYI-SRT-NUM ( #J )
        pnd_J.nadd(1);                                                                                                                                                    //Natural: ASSIGN #J := #J + 1
        //*  STATE REJECTION COMMENT
        ldaPstl6055.getAc_Fyi_Struct_Ac_Fyi_Msg_Number().getValue(pnd_J).setValue(60);                                                                                    //Natural: MOVE 60 TO AC-FYI-MSG-NUMBER ( #J )
        ldaPstl6055.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(pnd_J).setValue(60);                                                                                       //Natural: MOVE 060 TO AC-FYI-SRT-NUM ( #J )
        ldaPstl6055.getAc_Fyi_Struct_Ac_Data_Occurs().setValue(pnd_J);                                                                                                    //Natural: ASSIGN AC-FYI-STRUCT.AC-DATA-OCCURS := #J
        //*  SET UP SORT KEY LEVEL 4
        ldaPstl6055.getSort_Key_Struct_Level_4_Sort_Flds_Reset().moveAll("H'00'");                                                                                        //Natural: MOVE ALL H'00' TO LEVEL-4-SORT-FLDS-RESET
        ldaPstl6055.getSort_Key_Struct_Key_L4_Account_Type().setValue(99);                                                                                                //Natural: MOVE 99 TO SORT-KEY-STRUCT.KEY-L4-ACCOUNT-TYPE
        ldaPstl6055.getSort_Key_Struct_Key_L4_Fyi_Mes_Num().setValue(ldaPstl6055.getAc_Fyi_Struct_Ac_Fyi_Srt_Num().getValue(1));                                          //Natural: MOVE AC-FYI-SRT-NUM ( 1 ) TO SORT-KEY-STRUCT.KEY-L4-FYI-MES-NUM
        ldaPstl6055.getSort_Key_Struct_Key_L4_Quest_Mes_Num().setValue(0);                                                                                                //Natural: MOVE 0 TO SORT-KEY-STRUCT.KEY-L4-QUEST-MES-NUM
        pdaPsta9500.getPsta9500_Application_Key().reset();                                                                                                                //Natural: RESET PSTA9500.APPLICATION-KEY
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-FYI-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6055.getSort_Key_Struct(), ldaPstl6055.getAc_Fyi_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-AC-FYI-REC
    }
    private void sub_Call_Post_For_Close() throws Exception                                                                                                               //Natural: CALL-POST-FOR-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CALL POST CLOSE TO FINALISE THE REQUEST
        //* *CALLNAT 'PSTN9680'                             /* 082017 START
        //*  082017 END
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST-FOR-CLOSE
    }
    private void sub_Fill_Contract_Numbers() throws Exception                                                                                                             //Natural: FILL-CONTRACT-NUMBERS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  CHECK THE FROM FUNDS
            //*  022009
            //*  022009
            //*  THIS IS A CREF FUND
            short decideConditionsMet1969 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF XFR-FRM-ACCT-CDE ( #I );//Natural: VALUE ' '
            if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet1969++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-RE
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Re))))
            {
                decideConditionsMet1969++;
                pnd_From_Re.setValue("Y");                                                                                                                                //Natural: ASSIGN #FROM-RE := 'Y'
            }                                                                                                                                                             //Natural: VALUE #CONST.#TIAA-ACC
            else if (condition((iaa_Trnsfr_Sw_Rqst_View_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Const_Pnd_Tiaa_Acc))))
            {
                decideConditionsMet1969++;
                pnd_From_Acc.setValue("Y");                                                                                                                               //Natural: ASSIGN #FROM-ACC := 'Y'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_From_Cref.setValue("Y");                                                                                                                              //Natural: ASSIGN #FROM-CREF := 'Y'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  022009
        if (condition(pnd_From_Re.equals("Y") || pnd_From_Acc.equals("Y")))                                                                                               //Natural: IF #FROM-RE = 'Y' OR #FROM-ACC = 'Y'
        {
            ldaPstl6055.getAc_Contract_Struct_Ac_Real_Estate_Contract().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                  //Natural: ASSIGN AC-REAL-ESTATE-CONTRACT := IA-FRM-CNTRCT
            //*  042209
            ldaPstl6055.getAc_Contract_Struct_Ac_Real_Estate_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaPstl6055.getAc_Contract_Struct_Ac_Real_Estate_Contract(),  //Natural: COMPRESS AC-REAL-ESTATE-CONTRACT '-REA' INTO AC-REAL-ESTATE-CONTRACT LEAVING NO
                "-REA"));
            //*  FROM CREF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl6055.getAc_Contract_Struct_Ac_Cref_Contract_Num().setValue(iaa_Trnsfr_Sw_Rqst_View_Ia_Frm_Cntrct);                                                     //Natural: ASSIGN AC-CREF-CONTRACT-NUM := IA-FRM-CNTRCT
        }                                                                                                                                                                 //Natural: END-IF
        //*  FILL-CONTRACT-NUMBERS
    }

    //
}
