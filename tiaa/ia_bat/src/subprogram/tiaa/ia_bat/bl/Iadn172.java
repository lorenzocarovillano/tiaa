/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:42:11 AM
**        * FROM NATURAL SUBPROGRAM : Iadn172
************************************************************
**        * FILE NAME            : Iadn172.java
**        * CLASS NAME           : Iadn172
**        * INSTANCE NAME        : Iadn172
************************************************************
************************************************************************
* PROGRAM:  IADN172
* DATE   :  04/11/2006
* FUNCTION: THIS WILL RECREATE THE 50 REC TYPE FOR IA TO QUARTERLY
*           FEED TO CAPTURE THE PREVIOUS PAYMENT BEFORE THE PARTIAL
*           DRAWDOWN. THE PURPOSE IS TO PASS TO ACTUARIAL THE
*           PAID AMOUNT BEFORE THE PARTIAL DRAWDOWN TO CALCULATE THE
*           CORRECT OPENING BALANCE.
*
* HISTORY  : 4/20/06 - JUN TINIO
*
************************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadn172 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Check_Dte;
    private DbsField pnd_Rec5;

    private DataAccessProgramView vw_fund_Trans;
    private DbsField fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Trans_Tiaa_Cmpny_Fund_Cde;
    private DbsField fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField fund_Trans_Trans_Check_Dte;
    private DbsField fund_Trans_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField fund_Trans_Tiaa_Rate_Cde;
    private DbsField fund_Trans_Tiaa_Rate_Dte;
    private DbsField fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Tiaa_Fund_Bfre_Key;

    private DbsGroup pnd_Tiaa_Fund_Bfre_Key__R_Field_1;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Rec_5;

    private DbsGroup pnd_Rec_5__R_Field_2;
    private DbsField pnd_Rec_5_Pnd_Rec_Cntrct;
    private DbsField pnd_Rec_5_Pnd_Rec_Payee;
    private DbsField pnd_Rec_5_Pnd_Rec_Level_Nbr;

    private DbsGroup pnd_Rec_5_Rec_Type_10;
    private DbsField pnd_Rec_5_Rate_Filler;
    private DbsField pnd_Rec_5_Rate_Tot_Old_Tiaa_Div;

    private DbsGroup pnd_Rec_5_Lvl_5_Grp;
    private DbsField pnd_Rec_5_Rate_Rate;
    private DbsField pnd_Rec_5_Rate_Per_Pay;
    private DbsField pnd_Rec_5_Rate_Per_Div;
    private DbsField pnd_Rec_5_Rate_Final_Pay;
    private DbsField pnd_Rec_5_Rate_Date;
    private DbsField pnd_Rec_5_Rate_Trans_Code;
    private DbsField pnd_Rec_5_Rate_Trans_Date;
    private DbsField pnd_Rec_5_Filler_1a;
    private DbsField pnd_Rec_5_Rate_Tot_Old_Tiaa_Pmt;
    private DbsField pnd_Rate_Date_A8;

    private DbsGroup pnd_Rate_Date_A8__R_Field_3;
    private DbsField pnd_Rate_Date_A8_Pnd_Rate_Date_N6;
    private DbsField pnd_Trans_Check_Dte;

    private DbsGroup pnd_Trans_Check_Dte__R_Field_4;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Dd;

    private DbsGroup pnd_Trans_Check_Dte__R_Field_5;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Yyyy;
    private DbsField pnd_Trans_Check_Dte_Pnd_Trans_Mm;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_J;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 9);
        pnd_Cntrct_Ppcn_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Dte = parameters.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 6);
        pnd_Check_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Rec5 = parameters.newFieldArrayInRecord("pnd_Rec5", "#REC5", FieldType.STRING, 100, new DbsArrayController(1, 30));
        pnd_Rec5.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_fund_Trans = new DataAccessProgramView(new NameInfo("vw_fund_Trans", "FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        fund_Trans_Tiaa_Tot_Per_Amt = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        fund_Trans_Tiaa_Tot_Div_Amt = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        fund_Trans_Tiaa_Old_Per_Amt = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        fund_Trans_Tiaa_Old_Div_Amt = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        fund_Trans_Trans_Check_Dte = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Count_Casttiaa_Rate_Data_Grp", "C*TIAA-RATE-DATA-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");

        fund_Trans_Tiaa_Rate_Data_Grp = vw_fund_Trans.getRecord().newGroupArrayInGroup("fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", new DbsArrayController(1, 
            90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Cde = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", FieldType.STRING, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Dte = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("FUND_TRANS_TIAA_RATE_DTE", "TIAA-RATE-DTE", FieldType.DATE, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Per_Pay_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Per_Div_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Final_Pay_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Final_Div_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldInGroup("fund_Trans_Tiaa_Rate_Final_Div_Amt", "TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        registerRecord(vw_fund_Trans);

        pnd_Tiaa_Fund_Bfre_Key = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Bfre_Key", "#TIAA-FUND-BFRE-KEY", FieldType.STRING, 22);

        pnd_Tiaa_Fund_Bfre_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tiaa_Fund_Bfre_Key__R_Field_1", "REDEFINE", pnd_Tiaa_Fund_Bfre_Key);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Tiaa_Fund_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr = pnd_Tiaa_Fund_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr", 
            "#TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde = pnd_Tiaa_Fund_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde", 
            "#TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Rec_5 = localVariables.newFieldInRecord("pnd_Rec_5", "#REC-5", FieldType.STRING, 100);

        pnd_Rec_5__R_Field_2 = localVariables.newGroupInRecord("pnd_Rec_5__R_Field_2", "REDEFINE", pnd_Rec_5);
        pnd_Rec_5_Pnd_Rec_Cntrct = pnd_Rec_5__R_Field_2.newFieldInGroup("pnd_Rec_5_Pnd_Rec_Cntrct", "#REC-CNTRCT", FieldType.STRING, 8);
        pnd_Rec_5_Pnd_Rec_Payee = pnd_Rec_5__R_Field_2.newFieldInGroup("pnd_Rec_5_Pnd_Rec_Payee", "#REC-PAYEE", FieldType.NUMERIC, 2);
        pnd_Rec_5_Pnd_Rec_Level_Nbr = pnd_Rec_5__R_Field_2.newFieldInGroup("pnd_Rec_5_Pnd_Rec_Level_Nbr", "#REC-LEVEL-NBR", FieldType.NUMERIC, 2);

        pnd_Rec_5_Rec_Type_10 = pnd_Rec_5__R_Field_2.newGroupInGroup("pnd_Rec_5_Rec_Type_10", "REC-TYPE-10");
        pnd_Rec_5_Rate_Filler = pnd_Rec_5_Rec_Type_10.newFieldInGroup("pnd_Rec_5_Rate_Filler", "RATE-FILLER", FieldType.STRING, 3);
        pnd_Rec_5_Rate_Tot_Old_Tiaa_Div = pnd_Rec_5_Rec_Type_10.newFieldInGroup("pnd_Rec_5_Rate_Tot_Old_Tiaa_Div", "RATE-TOT-OLD-TIAA-DIV", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Rec_5_Lvl_5_Grp = pnd_Rec_5_Rec_Type_10.newGroupArrayInGroup("pnd_Rec_5_Lvl_5_Grp", "LVL-5-GRP", new DbsArrayController(1, 3));
        pnd_Rec_5_Rate_Rate = pnd_Rec_5_Lvl_5_Grp.newFieldInGroup("pnd_Rec_5_Rate_Rate", "RATE-RATE", FieldType.STRING, 2);
        pnd_Rec_5_Rate_Per_Pay = pnd_Rec_5_Lvl_5_Grp.newFieldInGroup("pnd_Rec_5_Rate_Per_Pay", "RATE-PER-PAY", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Rec_5_Rate_Per_Div = pnd_Rec_5_Lvl_5_Grp.newFieldInGroup("pnd_Rec_5_Rate_Per_Div", "RATE-PER-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Rec_5_Rate_Final_Pay = pnd_Rec_5_Lvl_5_Grp.newFieldInGroup("pnd_Rec_5_Rate_Final_Pay", "RATE-FINAL-PAY", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Rec_5_Rate_Date = pnd_Rec_5_Lvl_5_Grp.newFieldInGroup("pnd_Rec_5_Rate_Date", "RATE-DATE", FieldType.PACKED_DECIMAL, 6);
        pnd_Rec_5_Rate_Trans_Code = pnd_Rec_5_Rec_Type_10.newFieldInGroup("pnd_Rec_5_Rate_Trans_Code", "RATE-TRANS-CODE", FieldType.NUMERIC, 3);
        pnd_Rec_5_Rate_Trans_Date = pnd_Rec_5_Rec_Type_10.newFieldInGroup("pnd_Rec_5_Rate_Trans_Date", "RATE-TRANS-DATE", FieldType.PACKED_DECIMAL, 6);
        pnd_Rec_5_Filler_1a = pnd_Rec_5_Rec_Type_10.newFieldInGroup("pnd_Rec_5_Filler_1a", "FILLER-1A", FieldType.STRING, 6);
        pnd_Rec_5_Rate_Tot_Old_Tiaa_Pmt = pnd_Rec_5_Rec_Type_10.newFieldInGroup("pnd_Rec_5_Rate_Tot_Old_Tiaa_Pmt", "RATE-TOT-OLD-TIAA-PMT", FieldType.BINARY, 
            4);
        pnd_Rate_Date_A8 = localVariables.newFieldInRecord("pnd_Rate_Date_A8", "#RATE-DATE-A8", FieldType.STRING, 8);

        pnd_Rate_Date_A8__R_Field_3 = localVariables.newGroupInRecord("pnd_Rate_Date_A8__R_Field_3", "REDEFINE", pnd_Rate_Date_A8);
        pnd_Rate_Date_A8_Pnd_Rate_Date_N6 = pnd_Rate_Date_A8__R_Field_3.newFieldInGroup("pnd_Rate_Date_A8_Pnd_Rate_Date_N6", "#RATE-DATE-N6", FieldType.NUMERIC, 
            6);
        pnd_Trans_Check_Dte = localVariables.newFieldInRecord("pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Trans_Check_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Trans_Check_Dte__R_Field_4", "REDEFINE", pnd_Trans_Check_Dte);
        pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm = pnd_Trans_Check_Dte__R_Field_4.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm", "#TRANS-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Trans_Check_Dte_Pnd_Trans_Dd = pnd_Trans_Check_Dte__R_Field_4.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Dd", "#TRANS-DD", FieldType.STRING, 
            2);

        pnd_Trans_Check_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Trans_Check_Dte__R_Field_5", "REDEFINE", pnd_Trans_Check_Dte);
        pnd_Trans_Check_Dte_Pnd_Trans_Yyyy = pnd_Trans_Check_Dte__R_Field_5.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Yyyy", "#TRANS-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Trans_Check_Dte_Pnd_Trans_Mm = pnd_Trans_Check_Dte__R_Field_5.newFieldInGroup("pnd_Trans_Check_Dte_Pnd_Trans_Mm", "#TRANS-MM", FieldType.NUMERIC, 
            2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fund_Trans.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Tiaa_Fund_Bfre_Key.setInitialValue("1");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iadn172() throws Exception
    {
        super("Iadn172");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Trans_Check_Dte_Pnd_Trans_Yyyymm.setValue(pnd_Check_Dte);                                                                                                     //Natural: ASSIGN #TRANS-YYYYMM := #CHECK-DTE
        pnd_Trans_Check_Dte_Pnd_Trans_Dd.setValue("01");                                                                                                                  //Natural: ASSIGN #TRANS-DD := '01'
        pnd_Trans_Check_Dte_Pnd_Trans_Mm.nsubtract(1);                                                                                                                    //Natural: SUBTRACT 1 FROM #TRANS-MM
        if (condition(pnd_Trans_Check_Dte_Pnd_Trans_Mm.lessOrEqual(getZero())))                                                                                           //Natural: IF #TRANS-MM LE 0
        {
            pnd_Trans_Check_Dte_Pnd_Trans_Mm.setValue(12);                                                                                                                //Natural: ASSIGN #TRANS-MM := 12
            pnd_Trans_Check_Dte_Pnd_Trans_Yyyy.nsubtract(1);                                                                                                              //Natural: SUBTRACT 1 FROM #TRANS-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr.setValue(pnd_Cntrct_Ppcn_Nbr);                                                                                    //Natural: ASSIGN #TIAA-CNTRCT-PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(1);                                                                                                     //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := 01
        pnd_Rec_5_Pnd_Rec_Cntrct.setValue(pnd_Cntrct_Ppcn_Nbr);                                                                                                           //Natural: ASSIGN #REC-CNTRCT := #CNTRCT-PPCN-NBR
        pnd_Rec_5_Pnd_Rec_Payee.setValue(1);                                                                                                                              //Natural: ASSIGN #REC-PAYEE := 01
        pnd_Rec_5_Pnd_Rec_Level_Nbr.setValue(50);                                                                                                                         //Natural: ASSIGN #REC-LEVEL-NBR := 50
        pnd_Rec5.getValue("*").reset();                                                                                                                                   //Natural: RESET #REC5 ( * )
        vw_fund_Trans.startDatabaseRead                                                                                                                                   //Natural: READ FUND-TRANS BY TIAA-FUND-BFRE-KEY STARTING FROM #TIAA-FUND-BFRE-KEY
        (
        "READ01",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY", ">=", pnd_Tiaa_Fund_Bfre_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_fund_Trans.readNextRow("READ01")))
        {
            if (condition(fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr) || fund_Trans_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde))) //Natural: IF TIAA-CNTRCT-PPCN-NBR NE #TIAA-CNTRCT-PPCN-NBR OR TIAA-CNTRCT-PAYEE-CDE NE #TIAA-CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(fund_Trans_Trans_Check_Dte.equals(pnd_Trans_Check_Dte) && fund_Trans_Tiaa_Cmpny_Fund_Cde.equals("T1S"))))                                     //Natural: ACCEPT IF TRANS-CHECK-DTE = #TRANS-CHECK-DTE AND TIAA-CMPNY-FUND-CDE = 'T1S'
            {
                continue;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO C*TIAA-RATE-DATA-GRP
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(fund_Trans_Count_Casttiaa_Rate_Data_Grp)); pnd_I.nadd(1))
            {
                if (condition(fund_Trans_Tiaa_Rate_Cde.getValue(pnd_I).equals(" ")))                                                                                      //Natural: IF TIAA-RATE-CDE ( #I ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_I2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I2
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    pnd_Rec_5_Rate_Tot_Old_Tiaa_Div.setValue(fund_Trans_Tiaa_Old_Div_Amt);                                                                                //Natural: ASSIGN RATE-TOT-OLD-TIAA-DIV := TIAA-OLD-DIV-AMT
                    pnd_Rec_5_Rate_Tot_Old_Tiaa_Pmt.setValue(fund_Trans_Tiaa_Old_Per_Amt);                                                                                //Natural: ASSIGN RATE-TOT-OLD-TIAA-PMT := TIAA-OLD-PER-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(fund_Trans_Tiaa_Rate_Cde.getValue(pnd_I).notEquals(" ")))                                                                                   //Natural: IF TIAA-RATE-CDE ( #I ) NE ' '
                {
                    pnd_Rec_5_Rate_Rate.getValue(pnd_I2).setValue(fund_Trans_Tiaa_Rate_Cde.getValue(pnd_I));                                                              //Natural: ASSIGN RATE-RATE ( #I2 ) := TIAA-RATE-CDE ( #I )
                    pnd_Rec_5_Rate_Per_Pay.getValue(pnd_I2).setValue(fund_Trans_Tiaa_Per_Pay_Amt.getValue(pnd_I));                                                        //Natural: ASSIGN RATE-PER-PAY ( #I2 ) := TIAA-PER-PAY-AMT ( #I )
                    pnd_Rec_5_Rate_Per_Div.getValue(pnd_I2).setValue(fund_Trans_Tiaa_Per_Div_Amt.getValue(pnd_I));                                                        //Natural: ASSIGN RATE-PER-DIV ( #I2 ) := TIAA-PER-DIV-AMT ( #I )
                    pnd_Rec_5_Rate_Final_Pay.getValue(pnd_I2).setValue(fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I));                                               //Natural: ASSIGN RATE-FINAL-PAY ( #I2 ) := TIAA-RATE-FINAL-PAY-AMT ( #I )
                    if (condition(fund_Trans_Tiaa_Rate_Dte.getValue(pnd_I).greater(getZero())))                                                                           //Natural: IF TIAA-RATE-DTE ( #I ) GT 0
                    {
                        pnd_Rate_Date_A8.setValueEdited(fund_Trans_Tiaa_Rate_Dte.getValue(pnd_I),new ReportEditMask("YYYYMMDD"));                                         //Natural: MOVE EDITED TIAA-RATE-DTE ( #I ) ( EM = YYYYMMDD ) TO #RATE-DATE-A8
                        pnd_Rec_5_Rate_Date.getValue(pnd_I2).setValue(pnd_Rate_Date_A8_Pnd_Rate_Date_N6);                                                                 //Natural: MOVE #RATE-DATE-N6 TO RATE-DATE ( #I2 )
                        if (condition(pnd_Rate_Date_A8_Pnd_Rate_Date_N6.greaterOrEqual(pnd_Check_Dte)))                                                                   //Natural: IF #RATE-DATE-N6 GE #CHECK-DTE
                        {
                            pnd_Rec_5_Rate_Date.getValue(pnd_I2).reset();                                                                                                 //Natural: RESET RATE-DATE ( #I2 )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I2.equals(3)))                                                                                                                          //Natural: IF #I2 = 3
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    pnd_Rec5.getValue(pnd_J).setValue(pnd_Rec_5);                                                                                                         //Natural: ASSIGN #REC5 ( #J ) := #REC-5
                    pnd_Rec_5_Rec_Type_10.reset();                                                                                                                        //Natural: RESET REC-TYPE-10 #I2 RATE-TOT-OLD-TIAA-DIV RATE-TOT-OLD-TIAA-PMT RATE-RATE ( * ) RATE-PER-PAY ( * ) RATE-PER-DIV ( * ) RATE-FINAL-PAY ( * ) RATE-DATE ( * ) RATE-TRANS-CODE RATE-TRANS-DATE
                    pnd_I2.reset();
                    pnd_Rec_5_Rate_Tot_Old_Tiaa_Div.reset();
                    pnd_Rec_5_Rate_Tot_Old_Tiaa_Pmt.reset();
                    pnd_Rec_5_Rate_Rate.getValue("*").reset();
                    pnd_Rec_5_Rate_Per_Pay.getValue("*").reset();
                    pnd_Rec_5_Rate_Per_Div.getValue("*").reset();
                    pnd_Rec_5_Rate_Final_Pay.getValue("*").reset();
                    pnd_Rec_5_Rate_Date.getValue("*").reset();
                    pnd_Rec_5_Rate_Trans_Code.reset();
                    pnd_Rec_5_Rate_Trans_Date.reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_I2.greater(getZero())))                                                                                                                         //Natural: IF #I2 GT 0
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            pnd_Rec5.getValue(pnd_J).setValue(pnd_Rec_5);                                                                                                                 //Natural: ASSIGN #REC5 ( #J ) := #REC-5
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
