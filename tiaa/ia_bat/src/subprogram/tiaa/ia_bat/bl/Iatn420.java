/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:05 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420
************************************************************
**        * FILE NAME            : Iatn420.java
**        * CLASS NAME           : Iatn420
**        * INSTANCE NAME        : Iatn420
************************************************************
************************************************************************
* PROGRAM:  IATN420
* FUNCTION: MAIN SUBPROGRAM FOR TRANSFER PROCESSING 1998 - TAKING SOME
*           PARTS OF ORIGINAL MAIN PROGRAM (IATP170) FOR ANNUAL TRANSFER
*           PROCESSING.
*           THIS MODULE IS USED FOR PROCESSING ALL TRANSFERS EXCEPT
*           TIAA TO CREF (SINGLE OR REPETITIVE). TIAA TO CREF IS HANDLED
*           BY IATN422.
* CREATED:  01/12/98 : BY ARI GROSSMAN
*
* HISTORY:- 06/99 KN ADDED TIAA TO CREF TRANSFERS
*           05/02 TD CHANGE IAAN050F TO IAAN051Z
* 12/23/08  O SOTTO ISA TO CALM CONVERSION - CALL IATN421V INSTEAD OF
*                   IATN420V.  SC 122308.
* 05/29/09  O SOTTO FIXED PRODUCTION PROBLEM. SC 052909.
* 09/21/09  O SOTTO FIXED PRODUCTION PROBLEM. SC 092109.
* 02/21/10  J TINIO LINKAGE TO AIAL0130
* 06/28/10  O SOTTO REMOVED CONVERSION OF FUND TO RECEIVE. ASSIGN T
*                   FOR STANDARD AND 2 FOR GRADED. SC 062810.
* 06/30/11  J TINIO ADDED NEW FIELDS FOR MEOW
* 04/06/12  O SOTTO RATE BASE EXPANSION CHANGES.  SC 040612.
* 05/11/12  O SOTTO ADDED A PARAMETER TO IATN420G. SC 051112.
* 10/10/12  J TINIO RECATALOGED USING UPDATED VERSION OF IATA002
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO  RE-STOWED FOR CHANGES TO IATL420Z, IATA422, IATL420,
*                   IAAL420, IATL171, IATL420X, AND IAAL162G PIN EXP.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl420z pdaIatl420z;
    private PdaIata422 pdaIata422;
    private PdaAial0130 pdaAial0130;
    private PdaIata002 pdaIata002;
    private LdaIatl420 ldaIatl420;
    private LdaIaal420 ldaIaal420;
    private LdaIatl171 ldaIatl171;
    private PdaIatl420x pdaIatl420x;
    private LdaIaal162g ldaIaal162g;
    private PdaAiaa079 pdaAiaa079;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsField pnd_Total_Issue_Date;

    private DbsGroup pnd_Total_Issue_Date__R_Field_1;
    private DbsField pnd_Total_Issue_Date_Pnd_Issue_Date_6;
    private DbsField pnd_Total_Issue_Date_Pnd_Issue_Date_2;

    private DbsGroup pnd_Total_Issue_Date__R_Field_2;
    private DbsField pnd_Total_Issue_Date_Pnd_Issue_Date_2_A;
    private DbsField pnd_Per;
    private DbsField pnd_W_Per;

    private DbsGroup pnd_A26_Fields;
    private DbsField pnd_A26_Fields_Pnd_A26_Call_Type;
    private DbsField pnd_A26_Fields_Pnd_A26_Fnd_Cde;
    private DbsField pnd_A26_Fields_Pnd_A26_Reval_Meth;
    private DbsField pnd_A26_Fields_Pnd_A26_Req_Dte;
    private DbsField pnd_A26_Fields_Pnd_A26_Prtc_Dte;
    private DbsField pnd_A26_Fields_Pnd_Rc_Pgm;

    private DbsGroup pnd_A26_Fields__R_Field_3;
    private DbsField pnd_A26_Fields_Pnd_Pgm;
    private DbsField pnd_A26_Fields_Pnd_Rc;
    private DbsField pnd_A26_Fields_Pnd_Auv;
    private DbsField pnd_A26_Fields_Pnd_Auv_Ret_Dte;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Request_Month;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Particip_Month;

    private DbsGroup pnd_Tiaa_Rates;
    private DbsField pnd_Tiaa_Rates_Pnd_Rate_Code;
    private DbsField pnd_Tiaa_Rates_Pnd_Rate_Date;
    private DbsField pnd_Tiaa_Rates_Pnd_Gtd_Pmt;
    private DbsField pnd_Tiaa_Rates_Pnd_Dvd_Pmt;
    private DbsField pnd_Dte_Time_Hold_A;

    private DbsGroup pnd_Dte_Time_Hold_A__R_Field_4;
    private DbsField pnd_Dte_Time_Hold_A_Pnd_Dte_Time_Hold;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;
    private DbsField pnd_Cntrct_Nbr;
    private DbsField pnd_Tiaa_Units_Cnt;
    private DbsField pnd_To_Qty_Total;
    private DbsField pnd_Acc;
    private DbsField pnd_W_Diff;
    private DbsField pnd_W_Diff2;
    private DbsField pnd_Ivc_Pro_Adj;
    private DbsField pnd_Per_Ivc_Pro_Adj;
    private DbsField pnd_Ivc_Ind;
    private DbsField pnd_Contract_Type;
    private DbsField pnd_Intra_Fund;
    private DbsField pnd_Full_Cref_Contract_Out;
    private DbsField pnd_Full_Tiaa_Contract_Out;
    private DbsField pnd_Create_Teacher_Cnt;
    private DbsField pnd_Create_Cref_Cnt;
    private DbsField pnd_Full_Contract_Out;
    private DbsField pnd_Found_Fund;
    private DbsField pnd_From_Cref;
    private DbsField pnd_From_Tiaa;
    private DbsField pnd_From_Real;
    private DbsField pnd_From_Acc;
    private DbsField pnd_From_Real_6;
    private DbsField pnd_On_File;
    private DbsField pnd_One_Byte_Fund;
    private DbsField pnd_Two_Byte_Fund;
    private DbsField pnd_New_Issue_Cnt;
    private DbsField pnd_Ctl_Fund_Cde;
    private DbsField pnd_T_Table;
    private DbsField pnd_C_Table;
    private DbsField pnd_Check_Teachers;
    private DbsField pnd_Same_From_Fund;
    private DbsField pnd_Sub_Fr;
    private DbsField pnd_Sub_To;
    private DbsField pnd_A;
    private DbsField pnd_I;
    private DbsField pnd_Ii;
    private DbsField pnd_J;
    private DbsField pnd_L;
    private DbsField pnd_Var1;
    private DbsField pnd_Var2;
    private DbsField pnd_Var3;
    private DbsField pnd_Var4;
    private DbsField pnd_T;
    private DbsField pnd_O;
    private DbsField pnd_To_Units;
    private DbsField pnd_W_Units;
    private DbsField pnd_D_Units;
    private DbsField pnd_E_Units;
    private DbsField pnd_To_Pymts;
    private DbsField pnd_W_Pymts;
    private DbsField pnd_D_Pymts;
    private DbsField pnd_E_Pymts;
    private DbsField pnd_New_Val_Dollar;
    private DbsField pnd_To_Div;
    private DbsField pnd_Curr_Frm_Amt;
    private DbsField pnd_Xfr_Frm_Per_Amt;
    private DbsField pnd_Xfr_Frm_Div_Amt;
    private DbsField pnd_To_Gtd;
    private DbsField pnd_D_Div;
    private DbsField pnd_D_Gtd;
    private DbsField pnd_E_Div;
    private DbsField pnd_E_Gtd;
    private DbsField pnd_W_Div;
    private DbsField pnd_W_Gtd;
    private DbsField pnd_D_Amt;
    private DbsField pnd_W_Amt;
    private DbsField pnd_E_Amt;
    private DbsField pnd_Original_Trn_Amt;
    private DbsField pnd_Date8;
    private DbsField pnd_W_Gtd_Pmt;
    private DbsField pnd_W_Dvd_Pmt;
    private DbsField pnd_To_Asset_Amt;
    private DbsField pnd_Tot_Guar_Divid;
    private DbsField pnd_Time;
    private DbsField pnd_Iaa_New_Issue;
    private DbsField pnd_Iaa_New_Issue_Phys;
    private DbsField pnd_Iaa_New_Issue_Re;
    private DbsField pnd_From_Contract_Number;
    private DbsField pnd_From_Contract_Payee;
    private DbsField pnd_To_Contract_Number;
    private DbsField pnd_To_Contract_Payee;
    private DbsField pnd_From_On_To_Side;
    private DbsField pnd_Found_Cnt;
    private DbsField pnd_Not_Found_Yet;
    private DbsField pnd_From_Fund;
    private DbsField pnd_From_Fund_H;
    private DbsField pnd_From_Fund_Tot;
    private DbsField pnd_Ctl_To_Fund_Hold;
    private DbsField pnd_Other_Funds_Found;
    private DbsField pnd_Found_Teacher_Fund;
    private DbsField pnd_Auv_Dte;
    private DbsField pnd_W_Auv;
    private DbsField pnd_Rate_Dt;
    private DbsField pnd_W_Rc;
    private DbsField pnd_Nn;
    private DbsField pnd_Qq;
    private DbsField pnd_Q;
    private DbsField pnd_Zz;
    private DbsField pnd_From_Fund_Only;
    private DbsField pnd_W_Ia_Std_Alpha_Cd;
    private DbsField pnd_W3_Reject_Cde;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_6;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_7;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_8;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_9;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_10;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_11;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_12;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_13;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_Cmpny_Fund_3;
    private DbsField pnd_Invrse_Recvd_Time;

    private DbsGroup pnd_Invrse_Recvd_Time__R_Field_14;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Fund;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Dte;
    private DbsField pnd_Savetime;
    private DbsField pnd_Todays_Bus_Date_A;

    private DbsGroup pnd_Todays_Bus_Date_A__R_Field_15;
    private DbsField pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date;
    private DbsField pnd_W_Alpha_Cde;
    private DbsField pnd_W_Nm_Cd;
    private DbsField pnd_W_Rate_Cde;
    private DbsField pnd_F;
    private DbsField pnd_G;
    private DbsField pnd_Gg;
    private DbsField pnd_Aa;
    private DbsField pnd_Ggg;
    private DbsField pnd_Not_Full_Out;
    private DbsField pnd_Not_All_From_Transfer_Out;
    private DbsField pnd_Eff_Dte_03_31;
    private DbsField pnd_At_Least_One_Switch;
    private DbsField pnd_Switch_Fund_Found;
    private DbsField pnd_W_Other_Funds;
    private DbsField pnd_Fund_Cd_1;
    private DbsField pnd_Alpha_Cd_Table;
    private DbsField pnd_Rate_Code_Table;
    private DbsField pnd_Next_Pay_Dte;
    private DbsField pnd_Next_Chk_Dt;

    private DbsGroup pnd_Next_Chk_Dt__R_Field_16;
    private DbsField pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N;
    private DbsField pnd_Last_Chk_Dt;

    private DbsGroup pnd_Last_Chk_Dt__R_Field_17;
    private DbsField pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N;
    private DbsField pnd_Last_Dt;

    private DbsGroup pnd_Last_Dt__R_Field_18;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Mm;
    private DbsField pnd_Last_Dt_Pnd_Filler_1;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Dd;
    private DbsField pnd_Last_Dt_Pnd_Filler_2;
    private DbsField pnd_Last_Dt_Pnd_Last_Dt_Yyyy;
    private DbsField pnd_Next_Dt;

    private DbsGroup pnd_Next_Dt__R_Field_19;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Mm;
    private DbsField pnd_Next_Dt_Pnd_Filler_1;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Dd;
    private DbsField pnd_Next_Dt_Pnd_Filler_2;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Yyyy;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_20;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Cc;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Yy;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Dd;

    private DbsGroup pnd_Eff_Date__R_Field_21;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_N;
    private DbsField pnd_Reval_Dte;

    private DbsGroup pnd_Reval_Dte__R_Field_22;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Yyyy;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Mm;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Dd;
    private DbsField pnd_Reval_Dte_D;
    private DbsField pnd_Tiaa_Fund_Pymt;
    private DbsField pnd_Xfr_Units;
    private DbsField pnd_Transfer_Dollars;
    private DbsField pnd_Same_Fund_Units;
    private DbsField pnd_Same_Fund_Dollars;
    private DbsField pnd_W_Ia_Rpt_Nm_Cd;
    private DbsField pnd_W_Ia_Rpt_Cmpny_Cd_A;
    private DbsField pnd_Fp_Yrs;
    private DbsField pnd_Fp_Mos;
    private DbsField pnd_W1_Dte;

    private DbsGroup pnd_W1_Dte__R_Field_23;
    private DbsField pnd_W1_Dte_Pnd_W1_Yyyy;
    private DbsField pnd_W1_Dte_Pnd_W1_Mm;
    private DbsField pnd_W2_Dte;

    private DbsGroup pnd_W2_Dte__R_Field_24;
    private DbsField pnd_W2_Dte_Pnd_W2_Yyyy;
    private DbsField pnd_W2_Dte_Pnd_W2_Mm;
    private DbsField pnd_Xfr_Eff_Dte_Out;

    private DbsGroup pnd_Xfr_Eff_Dte_Out__R_Field_25;
    private DbsField pnd_Xfr_Eff_Dte_Out_Pnd_Trnsfr_Eff_Date_Out_A;
    private DbsField pnd_To_G;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Fund_Rcvd;
    private DbsField pnd_To_Acct_Cd;
    private DbsField pnd_Max_T_Occurs;
    private DbsField pnd_Max_Prods;
    private DbsField pnd_Max_Rate;
    private DbsField pls_Adat_Accessed;
    private DbsField pls_Debug;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIata422 = new PdaIata422(localVariables);
        pdaAial0130 = new PdaAial0130(localVariables);
        pdaIata002 = new PdaIata002(localVariables);
        ldaIatl420 = new LdaIatl420();
        registerRecord(ldaIatl420);
        registerRecord(ldaIatl420.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIatl420.getVw_iaa_Cntrct());
        registerRecord(ldaIatl420.getVw_iaa_Tiaa_Fund_Rcrd_View());
        ldaIaal420 = new LdaIaal420();
        registerRecord(ldaIaal420);
        registerRecord(ldaIaal420.getVw_iaa_Cref_Fund());
        registerRecord(ldaIaal420.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal420.getVw_iaa_Tiaa_Fund());
        registerRecord(ldaIaal420.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal420.getVw_iaa_Cpr());
        registerRecord(ldaIaal420.getVw_iaa_Cpr_Trans());
        ldaIatl171 = new LdaIatl171();
        registerRecord(ldaIatl171);
        registerRecord(ldaIatl171.getVw_iatl171());
        pdaIatl420x = new PdaIatl420x(localVariables);
        ldaIaal162g = new LdaIaal162g();
        registerRecord(ldaIaal162g);
        registerRecord(ldaIaal162g.getVw_iaa_Cntrct_1());
        registerRecord(ldaIaal162g.getVw_iaa_Cntrct_2());
        registerRecord(ldaIaal162g.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal162g.getVw_iaa_Cpr_2());
        pdaAiaa079 = new PdaAiaa079(localVariables);
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl420z = new PdaIatl420z(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Total_Issue_Date = localVariables.newFieldInRecord("pnd_Total_Issue_Date", "#TOTAL-ISSUE-DATE", FieldType.NUMERIC, 8);

        pnd_Total_Issue_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Total_Issue_Date__R_Field_1", "REDEFINE", pnd_Total_Issue_Date);
        pnd_Total_Issue_Date_Pnd_Issue_Date_6 = pnd_Total_Issue_Date__R_Field_1.newFieldInGroup("pnd_Total_Issue_Date_Pnd_Issue_Date_6", "#ISSUE-DATE-6", 
            FieldType.NUMERIC, 6);
        pnd_Total_Issue_Date_Pnd_Issue_Date_2 = pnd_Total_Issue_Date__R_Field_1.newFieldInGroup("pnd_Total_Issue_Date_Pnd_Issue_Date_2", "#ISSUE-DATE-2", 
            FieldType.NUMERIC, 2);

        pnd_Total_Issue_Date__R_Field_2 = pnd_Total_Issue_Date__R_Field_1.newGroupInGroup("pnd_Total_Issue_Date__R_Field_2", "REDEFINE", pnd_Total_Issue_Date_Pnd_Issue_Date_2);
        pnd_Total_Issue_Date_Pnd_Issue_Date_2_A = pnd_Total_Issue_Date__R_Field_2.newFieldInGroup("pnd_Total_Issue_Date_Pnd_Issue_Date_2_A", "#ISSUE-DATE-2-A", 
            FieldType.STRING, 2);
        pnd_Per = localVariables.newFieldInRecord("pnd_Per", "#PER", FieldType.NUMERIC, 7, 4);
        pnd_W_Per = localVariables.newFieldInRecord("pnd_W_Per", "#W-PER", FieldType.NUMERIC, 7, 4);

        pnd_A26_Fields = localVariables.newGroupInRecord("pnd_A26_Fields", "#A26-FIELDS");
        pnd_A26_Fields_Pnd_A26_Call_Type = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Call_Type", "#A26-CALL-TYPE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Fnd_Cde = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Fnd_Cde", "#A26-FND-CDE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Reval_Meth = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Reval_Meth", "#A26-REVAL-METH", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Req_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Req_Dte", "#A26-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_A26_Prtc_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Prtc_Dte", "#A26-PRTC-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Rc_Pgm = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Rc_Pgm", "#RC-PGM", FieldType.STRING, 11);

        pnd_A26_Fields__R_Field_3 = pnd_A26_Fields.newGroupInGroup("pnd_A26_Fields__R_Field_3", "REDEFINE", pnd_A26_Fields_Pnd_Rc_Pgm);
        pnd_A26_Fields_Pnd_Pgm = pnd_A26_Fields__R_Field_3.newFieldInGroup("pnd_A26_Fields_Pnd_Pgm", "#PGM", FieldType.STRING, 8);
        pnd_A26_Fields_Pnd_Rc = pnd_A26_Fields__R_Field_3.newFieldInGroup("pnd_A26_Fields_Pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_A26_Fields_Pnd_Auv = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_A26_Fields_Pnd_Auv_Ret_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Days_In_Request_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_A26_Fields_Pnd_Days_In_Particip_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);

        pnd_Tiaa_Rates = localVariables.newGroupArrayInRecord("pnd_Tiaa_Rates", "#TIAA-RATES", new DbsArrayController(1, 250));
        pnd_Tiaa_Rates_Pnd_Rate_Code = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Rate_Code", "#RATE-CODE", FieldType.STRING, 2);
        pnd_Tiaa_Rates_Pnd_Rate_Date = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Rate_Date", "#RATE-DATE", FieldType.DATE);
        pnd_Tiaa_Rates_Pnd_Gtd_Pmt = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Gtd_Pmt", "#GTD-PMT", FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Rates_Pnd_Dvd_Pmt = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Dvd_Pmt", "#DVD-PMT", FieldType.NUMERIC, 9, 2);
        pnd_Dte_Time_Hold_A = localVariables.newFieldInRecord("pnd_Dte_Time_Hold_A", "#DTE-TIME-HOLD-A", FieldType.STRING, 14);

        pnd_Dte_Time_Hold_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Dte_Time_Hold_A__R_Field_4", "REDEFINE", pnd_Dte_Time_Hold_A);
        pnd_Dte_Time_Hold_A_Pnd_Dte_Time_Hold = pnd_Dte_Time_Hold_A__R_Field_4.newFieldInGroup("pnd_Dte_Time_Hold_A_Pnd_Dte_Time_Hold", "#DTE-TIME-HOLD", 
            FieldType.NUMERIC, 14);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Units_Cnt = localVariables.newFieldInRecord("pnd_Tiaa_Units_Cnt", "#TIAA-UNITS-CNT", FieldType.NUMERIC, 9, 3);
        pnd_To_Qty_Total = localVariables.newFieldInRecord("pnd_To_Qty_Total", "#TO-QTY-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Acc = localVariables.newFieldInRecord("pnd_Acc", "#ACC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_W_Diff = localVariables.newFieldInRecord("pnd_W_Diff", "#W-DIFF", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_W_Diff2 = localVariables.newFieldInRecord("pnd_W_Diff2", "#W-DIFF2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ivc_Pro_Adj = localVariables.newFieldInRecord("pnd_Ivc_Pro_Adj", "#IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Ivc_Pro_Adj = localVariables.newFieldInRecord("pnd_Per_Ivc_Pro_Adj", "#PER-IVC-PRO-ADJ", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Ind = localVariables.newFieldInRecord("pnd_Ivc_Ind", "#IVC-IND", FieldType.STRING, 1);
        pnd_Contract_Type = localVariables.newFieldInRecord("pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 1);
        pnd_Intra_Fund = localVariables.newFieldInRecord("pnd_Intra_Fund", "#INTRA-FUND", FieldType.STRING, 1);
        pnd_Full_Cref_Contract_Out = localVariables.newFieldInRecord("pnd_Full_Cref_Contract_Out", "#FULL-CREF-CONTRACT-OUT", FieldType.STRING, 1);
        pnd_Full_Tiaa_Contract_Out = localVariables.newFieldInRecord("pnd_Full_Tiaa_Contract_Out", "#FULL-TIAA-CONTRACT-OUT", FieldType.STRING, 1);
        pnd_Create_Teacher_Cnt = localVariables.newFieldInRecord("pnd_Create_Teacher_Cnt", "#CREATE-TEACHER-CNT", FieldType.STRING, 1);
        pnd_Create_Cref_Cnt = localVariables.newFieldInRecord("pnd_Create_Cref_Cnt", "#CREATE-CREF-CNT", FieldType.STRING, 1);
        pnd_Full_Contract_Out = localVariables.newFieldInRecord("pnd_Full_Contract_Out", "#FULL-CONTRACT-OUT", FieldType.STRING, 1);
        pnd_Found_Fund = localVariables.newFieldInRecord("pnd_Found_Fund", "#FOUND-FUND", FieldType.STRING, 1);
        pnd_From_Cref = localVariables.newFieldInRecord("pnd_From_Cref", "#FROM-CREF", FieldType.STRING, 1);
        pnd_From_Tiaa = localVariables.newFieldInRecord("pnd_From_Tiaa", "#FROM-TIAA", FieldType.STRING, 1);
        pnd_From_Real = localVariables.newFieldInRecord("pnd_From_Real", "#FROM-REAL", FieldType.STRING, 1);
        pnd_From_Acc = localVariables.newFieldInRecord("pnd_From_Acc", "#FROM-ACC", FieldType.STRING, 1);
        pnd_From_Real_6 = localVariables.newFieldInRecord("pnd_From_Real_6", "#FROM-REAL-6", FieldType.STRING, 1);
        pnd_On_File = localVariables.newFieldInRecord("pnd_On_File", "#ON-FILE", FieldType.STRING, 1);
        pnd_One_Byte_Fund = localVariables.newFieldInRecord("pnd_One_Byte_Fund", "#ONE-BYTE-FUND", FieldType.STRING, 1);
        pnd_Two_Byte_Fund = localVariables.newFieldInRecord("pnd_Two_Byte_Fund", "#TWO-BYTE-FUND", FieldType.STRING, 2);
        pnd_New_Issue_Cnt = localVariables.newFieldInRecord("pnd_New_Issue_Cnt", "#NEW-ISSUE-CNT", FieldType.NUMERIC, 1);
        pnd_Ctl_Fund_Cde = localVariables.newFieldArrayInRecord("pnd_Ctl_Fund_Cde", "#CTL-FUND-CDE", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_T_Table = localVariables.newFieldArrayInRecord("pnd_T_Table", "#T-TABLE", FieldType.STRING, 1, new DbsArrayController(1, 4));
        pnd_C_Table = localVariables.newFieldArrayInRecord("pnd_C_Table", "#C-TABLE", FieldType.STRING, 1, new DbsArrayController(1, 18));
        pnd_Check_Teachers = localVariables.newFieldInRecord("pnd_Check_Teachers", "#CHECK-TEACHERS", FieldType.STRING, 1);
        pnd_Same_From_Fund = localVariables.newFieldInRecord("pnd_Same_From_Fund", "#SAME-FROM-FUND", FieldType.STRING, 1);
        pnd_Sub_Fr = localVariables.newFieldInRecord("pnd_Sub_Fr", "#SUB-FR", FieldType.INTEGER, 2);
        pnd_Sub_To = localVariables.newFieldInRecord("pnd_Sub_To", "#SUB-TO", FieldType.INTEGER, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ii = localVariables.newFieldInRecord("pnd_Ii", "#II", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_Var1 = localVariables.newFieldInRecord("pnd_Var1", "#VAR1", FieldType.PACKED_DECIMAL, 3);
        pnd_Var2 = localVariables.newFieldInRecord("pnd_Var2", "#VAR2", FieldType.PACKED_DECIMAL, 3);
        pnd_Var3 = localVariables.newFieldInRecord("pnd_Var3", "#VAR3", FieldType.PACKED_DECIMAL, 3);
        pnd_Var4 = localVariables.newFieldInRecord("pnd_Var4", "#VAR4", FieldType.PACKED_DECIMAL, 3);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 3);
        pnd_O = localVariables.newFieldInRecord("pnd_O", "#O", FieldType.PACKED_DECIMAL, 3);
        pnd_To_Units = localVariables.newFieldInRecord("pnd_To_Units", "#TO-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_W_Units = localVariables.newFieldInRecord("pnd_W_Units", "#W-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_D_Units = localVariables.newFieldInRecord("pnd_D_Units", "#D-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_E_Units = localVariables.newFieldInRecord("pnd_E_Units", "#E-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_To_Pymts = localVariables.newFieldInRecord("pnd_To_Pymts", "#TO-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Pymts = localVariables.newFieldInRecord("pnd_W_Pymts", "#W-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_D_Pymts = localVariables.newFieldInRecord("pnd_D_Pymts", "#D-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_E_Pymts = localVariables.newFieldInRecord("pnd_E_Pymts", "#E-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_New_Val_Dollar = localVariables.newFieldInRecord("pnd_New_Val_Dollar", "#NEW-VAL-DOLLAR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_To_Div = localVariables.newFieldInRecord("pnd_To_Div", "#TO-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Curr_Frm_Amt = localVariables.newFieldInRecord("pnd_Curr_Frm_Amt", "#CURR-FRM-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Frm_Per_Amt = localVariables.newFieldInRecord("pnd_Xfr_Frm_Per_Amt", "#XFR-FRM-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Frm_Div_Amt = localVariables.newFieldInRecord("pnd_Xfr_Frm_Div_Amt", "#XFR-FRM-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_To_Gtd = localVariables.newFieldInRecord("pnd_To_Gtd", "#TO-GTD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_D_Div = localVariables.newFieldInRecord("pnd_D_Div", "#D-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_D_Gtd = localVariables.newFieldInRecord("pnd_D_Gtd", "#D-GTD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_E_Div = localVariables.newFieldInRecord("pnd_E_Div", "#E-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_E_Gtd = localVariables.newFieldInRecord("pnd_E_Gtd", "#E-GTD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Div = localVariables.newFieldInRecord("pnd_W_Div", "#W-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Gtd = localVariables.newFieldInRecord("pnd_W_Gtd", "#W-GTD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_D_Amt = localVariables.newFieldInRecord("pnd_D_Amt", "#D-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Amt = localVariables.newFieldInRecord("pnd_W_Amt", "#W-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_E_Amt = localVariables.newFieldInRecord("pnd_E_Amt", "#E-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Original_Trn_Amt = localVariables.newFieldInRecord("pnd_Original_Trn_Amt", "#ORIGINAL-TRN-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);
        pnd_W_Gtd_Pmt = localVariables.newFieldArrayInRecord("pnd_W_Gtd_Pmt", "#W-GTD-PMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_W_Dvd_Pmt = localVariables.newFieldArrayInRecord("pnd_W_Dvd_Pmt", "#W-DVD-PMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_To_Asset_Amt = localVariables.newFieldArrayInRecord("pnd_To_Asset_Amt", "#TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_Tot_Guar_Divid = localVariables.newFieldInRecord("pnd_Tot_Guar_Divid", "#TOT-GUAR-DIVID", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Iaa_New_Issue = localVariables.newFieldInRecord("pnd_Iaa_New_Issue", "#IAA-NEW-ISSUE", FieldType.STRING, 1);
        pnd_Iaa_New_Issue_Phys = localVariables.newFieldInRecord("pnd_Iaa_New_Issue_Phys", "#IAA-NEW-ISSUE-PHYS", FieldType.STRING, 1);
        pnd_Iaa_New_Issue_Re = localVariables.newFieldInRecord("pnd_Iaa_New_Issue_Re", "#IAA-NEW-ISSUE-RE", FieldType.STRING, 1);
        pnd_From_Contract_Number = localVariables.newFieldInRecord("pnd_From_Contract_Number", "#FROM-CONTRACT-NUMBER", FieldType.STRING, 10);
        pnd_From_Contract_Payee = localVariables.newFieldInRecord("pnd_From_Contract_Payee", "#FROM-CONTRACT-PAYEE", FieldType.STRING, 2);
        pnd_To_Contract_Number = localVariables.newFieldInRecord("pnd_To_Contract_Number", "#TO-CONTRACT-NUMBER", FieldType.STRING, 10);
        pnd_To_Contract_Payee = localVariables.newFieldInRecord("pnd_To_Contract_Payee", "#TO-CONTRACT-PAYEE", FieldType.STRING, 2);
        pnd_From_On_To_Side = localVariables.newFieldInRecord("pnd_From_On_To_Side", "#FROM-ON-TO-SIDE", FieldType.STRING, 1);
        pnd_Found_Cnt = localVariables.newFieldInRecord("pnd_Found_Cnt", "#FOUND-CNT", FieldType.STRING, 1);
        pnd_Not_Found_Yet = localVariables.newFieldInRecord("pnd_Not_Found_Yet", "#NOT-FOUND-YET", FieldType.STRING, 1);
        pnd_From_Fund = localVariables.newFieldInRecord("pnd_From_Fund", "#FROM-FUND", FieldType.STRING, 1);
        pnd_From_Fund_H = localVariables.newFieldInRecord("pnd_From_Fund_H", "#FROM-FUND-H", FieldType.STRING, 1);
        pnd_From_Fund_Tot = localVariables.newFieldInRecord("pnd_From_Fund_Tot", "#FROM-FUND-TOT", FieldType.STRING, 3);
        pnd_Ctl_To_Fund_Hold = localVariables.newFieldInRecord("pnd_Ctl_To_Fund_Hold", "#CTL-TO-FUND-HOLD", FieldType.STRING, 1);
        pnd_Other_Funds_Found = localVariables.newFieldInRecord("pnd_Other_Funds_Found", "#OTHER-FUNDS-FOUND", FieldType.STRING, 1);
        pnd_Found_Teacher_Fund = localVariables.newFieldInRecord("pnd_Found_Teacher_Fund", "#FOUND-TEACHER-FUND", FieldType.STRING, 1);
        pnd_Auv_Dte = localVariables.newFieldInRecord("pnd_Auv_Dte", "#AUV-DTE", FieldType.NUMERIC, 8);
        pnd_W_Auv = localVariables.newFieldInRecord("pnd_W_Auv", "#W-AUV", FieldType.NUMERIC, 5, 2);
        pnd_Rate_Dt = localVariables.newFieldInRecord("pnd_Rate_Dt", "#RATE-DT", FieldType.NUMERIC, 6);
        pnd_W_Rc = localVariables.newFieldInRecord("pnd_W_Rc", "#W-RC", FieldType.NUMERIC, 2);
        pnd_Nn = localVariables.newFieldInRecord("pnd_Nn", "#NN", FieldType.NUMERIC, 2);
        pnd_Qq = localVariables.newFieldInRecord("pnd_Qq", "#QQ", FieldType.NUMERIC, 2);
        pnd_Q = localVariables.newFieldInRecord("pnd_Q", "#Q", FieldType.NUMERIC, 2);
        pnd_Zz = localVariables.newFieldInRecord("pnd_Zz", "#ZZ", FieldType.NUMERIC, 2);
        pnd_From_Fund_Only = localVariables.newFieldArrayInRecord("pnd_From_Fund_Only", "#FROM-FUND-ONLY", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_W_Ia_Std_Alpha_Cd = localVariables.newFieldArrayInRecord("pnd_W_Ia_Std_Alpha_Cd", "#W-IA-STD-ALPHA-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_W3_Reject_Cde = localVariables.newFieldInRecord("pnd_W3_Reject_Cde", "#W3-REJECT-CDE", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_6", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_6.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_7 = pnd_Save_Check_Dte_A__R_Field_6.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_7", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_7.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);

        pnd_Save_Check_Dte_A__R_Field_8 = pnd_Save_Check_Dte_A__R_Field_7.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_8", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_9 = pnd_Save_Check_Dte_A__R_Field_8.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_9", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yy);
        pnd_Save_Check_Dte_A_Pnd_Yy_A = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy_A", "#YY-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_7.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_10 = pnd_Save_Check_Dte_A__R_Field_7.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_10", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_10.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_7.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_11 = pnd_Save_Check_Dte_A__R_Field_7.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_11", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Dd);
        pnd_Save_Check_Dte_A_Pnd_Dd_A = pnd_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd_A", "#DD-A", FieldType.STRING, 2);

        pnd_Save_Check_Dte_A__R_Field_12 = pnd_Save_Check_Dte_A__R_Field_6.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_12", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_13", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_13.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_13.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_13.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_Cmpny_Fund_3 = localVariables.newFieldInRecord("pnd_Cmpny_Fund_3", "#CMPNY-FUND-3", FieldType.STRING, 3);
        pnd_Invrse_Recvd_Time = localVariables.newFieldInRecord("pnd_Invrse_Recvd_Time", "#INVRSE-RECVD-TIME", FieldType.NUMERIC, 14);

        pnd_Invrse_Recvd_Time__R_Field_14 = localVariables.newGroupInRecord("pnd_Invrse_Recvd_Time__R_Field_14", "REDEFINE", pnd_Invrse_Recvd_Time);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Fund = pnd_Invrse_Recvd_Time__R_Field_14.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Fund", "#IRT-FUND", FieldType.NUMERIC, 
            2);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Dte = pnd_Invrse_Recvd_Time__R_Field_14.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Dte", "#IRT-DTE", FieldType.NUMERIC, 
            12);
        pnd_Savetime = localVariables.newFieldInRecord("pnd_Savetime", "#SAVETIME", FieldType.TIME);
        pnd_Todays_Bus_Date_A = localVariables.newFieldInRecord("pnd_Todays_Bus_Date_A", "#TODAYS-BUS-DATE-A", FieldType.STRING, 8);

        pnd_Todays_Bus_Date_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Todays_Bus_Date_A__R_Field_15", "REDEFINE", pnd_Todays_Bus_Date_A);
        pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date = pnd_Todays_Bus_Date_A__R_Field_15.newFieldInGroup("pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date", "#TODAYS-BUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_W_Alpha_Cde = localVariables.newFieldArrayInRecord("pnd_W_Alpha_Cde", "#W-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 80));
        pnd_W_Nm_Cd = localVariables.newFieldArrayInRecord("pnd_W_Nm_Cd", "#W-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_W_Rate_Cde = localVariables.newFieldArrayInRecord("pnd_W_Rate_Cde", "#W-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.NUMERIC, 2);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 2);
        pnd_Gg = localVariables.newFieldInRecord("pnd_Gg", "#GG", FieldType.NUMERIC, 2);
        pnd_Aa = localVariables.newFieldInRecord("pnd_Aa", "#AA", FieldType.NUMERIC, 2);
        pnd_Ggg = localVariables.newFieldInRecord("pnd_Ggg", "#GGG", FieldType.NUMERIC, 2);
        pnd_Not_Full_Out = localVariables.newFieldInRecord("pnd_Not_Full_Out", "#NOT-FULL-OUT", FieldType.STRING, 1);
        pnd_Not_All_From_Transfer_Out = localVariables.newFieldInRecord("pnd_Not_All_From_Transfer_Out", "#NOT-ALL-FROM-TRANSFER-OUT", FieldType.STRING, 
            1);
        pnd_Eff_Dte_03_31 = localVariables.newFieldInRecord("pnd_Eff_Dte_03_31", "#EFF-DTE-03-31", FieldType.STRING, 1);
        pnd_At_Least_One_Switch = localVariables.newFieldInRecord("pnd_At_Least_One_Switch", "#AT-LEAST-ONE-SWITCH", FieldType.STRING, 1);
        pnd_Switch_Fund_Found = localVariables.newFieldInRecord("pnd_Switch_Fund_Found", "#SWITCH-FUND-FOUND", FieldType.STRING, 1);
        pnd_W_Other_Funds = localVariables.newFieldInRecord("pnd_W_Other_Funds", "#W-OTHER-FUNDS", FieldType.STRING, 1);
        pnd_Fund_Cd_1 = localVariables.newFieldInRecord("pnd_Fund_Cd_1", "#FUND-CD-1", FieldType.STRING, 1);
        pnd_Alpha_Cd_Table = localVariables.newFieldArrayInRecord("pnd_Alpha_Cd_Table", "#ALPHA-CD-TABLE", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_Rate_Code_Table = localVariables.newFieldArrayInRecord("pnd_Rate_Code_Table", "#RATE-CODE-TABLE", FieldType.STRING, 3, new DbsArrayController(1, 
            80));
        pnd_Next_Pay_Dte = localVariables.newFieldInRecord("pnd_Next_Pay_Dte", "#NEXT-PAY-DTE", FieldType.DATE);
        pnd_Next_Chk_Dt = localVariables.newFieldInRecord("pnd_Next_Chk_Dt", "#NEXT-CHK-DT", FieldType.STRING, 8);

        pnd_Next_Chk_Dt__R_Field_16 = localVariables.newGroupInRecord("pnd_Next_Chk_Dt__R_Field_16", "REDEFINE", pnd_Next_Chk_Dt);
        pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N = pnd_Next_Chk_Dt__R_Field_16.newFieldInGroup("pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N", "#NEXT-CHK-DT-N", FieldType.NUMERIC, 
            8);
        pnd_Last_Chk_Dt = localVariables.newFieldInRecord("pnd_Last_Chk_Dt", "#LAST-CHK-DT", FieldType.STRING, 8);

        pnd_Last_Chk_Dt__R_Field_17 = localVariables.newGroupInRecord("pnd_Last_Chk_Dt__R_Field_17", "REDEFINE", pnd_Last_Chk_Dt);
        pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N = pnd_Last_Chk_Dt__R_Field_17.newFieldInGroup("pnd_Last_Chk_Dt_Pnd_Last_Chk_Dt_N", "#LAST-CHK-DT-N", FieldType.NUMERIC, 
            8);
        pnd_Last_Dt = localVariables.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 10);

        pnd_Last_Dt__R_Field_18 = localVariables.newGroupInRecord("pnd_Last_Dt__R_Field_18", "REDEFINE", pnd_Last_Dt);
        pnd_Last_Dt_Pnd_Last_Dt_Mm = pnd_Last_Dt__R_Field_18.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Mm", "#LAST-DT-MM", FieldType.STRING, 2);
        pnd_Last_Dt_Pnd_Filler_1 = pnd_Last_Dt__R_Field_18.newFieldInGroup("pnd_Last_Dt_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Last_Dt_Pnd_Last_Dt_Dd = pnd_Last_Dt__R_Field_18.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Dd", "#LAST-DT-DD", FieldType.STRING, 2);
        pnd_Last_Dt_Pnd_Filler_2 = pnd_Last_Dt__R_Field_18.newFieldInGroup("pnd_Last_Dt_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Last_Dt_Pnd_Last_Dt_Yyyy = pnd_Last_Dt__R_Field_18.newFieldInGroup("pnd_Last_Dt_Pnd_Last_Dt_Yyyy", "#LAST-DT-YYYY", FieldType.STRING, 4);
        pnd_Next_Dt = localVariables.newFieldInRecord("pnd_Next_Dt", "#NEXT-DT", FieldType.STRING, 10);

        pnd_Next_Dt__R_Field_19 = localVariables.newGroupInRecord("pnd_Next_Dt__R_Field_19", "REDEFINE", pnd_Next_Dt);
        pnd_Next_Dt_Pnd_Next_Dt_Mm = pnd_Next_Dt__R_Field_19.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Mm", "#NEXT-DT-MM", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_1 = pnd_Next_Dt__R_Field_19.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Dd = pnd_Next_Dt__R_Field_19.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Dd", "#NEXT-DT-DD", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_2 = pnd_Next_Dt__R_Field_19.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Yyyy = pnd_Next_Dt__R_Field_19.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Yyyy", "#NEXT-DT-YYYY", FieldType.STRING, 4);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_20 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_20", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Cc = pnd_Eff_Date__R_Field_20.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Cc", "#EFF-DATE-CC", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Yy = pnd_Eff_Date__R_Field_20.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Yy", "#EFF-DATE-YY", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Mm = pnd_Eff_Date__R_Field_20.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Mm", "#EFF-DATE-MM", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Dd = pnd_Eff_Date__R_Field_20.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Dd", "#EFF-DATE-DD", FieldType.STRING, 2);

        pnd_Eff_Date__R_Field_21 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_21", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_N = pnd_Eff_Date__R_Field_21.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 8);
        pnd_Reval_Dte = localVariables.newFieldInRecord("pnd_Reval_Dte", "#REVAL-DTE", FieldType.STRING, 8);

        pnd_Reval_Dte__R_Field_22 = localVariables.newGroupInRecord("pnd_Reval_Dte__R_Field_22", "REDEFINE", pnd_Reval_Dte);
        pnd_Reval_Dte_Pnd_Reval_Yyyy = pnd_Reval_Dte__R_Field_22.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Yyyy", "#REVAL-YYYY", FieldType.STRING, 4);
        pnd_Reval_Dte_Pnd_Reval_Mm = pnd_Reval_Dte__R_Field_22.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Mm", "#REVAL-MM", FieldType.STRING, 2);
        pnd_Reval_Dte_Pnd_Reval_Dd = pnd_Reval_Dte__R_Field_22.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Dd", "#REVAL-DD", FieldType.STRING, 2);
        pnd_Reval_Dte_D = localVariables.newFieldInRecord("pnd_Reval_Dte_D", "#REVAL-DTE-D", FieldType.DATE);
        pnd_Tiaa_Fund_Pymt = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Pymt", "#TIAA-FUND-PYMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Units = localVariables.newFieldInRecord("pnd_Xfr_Units", "#XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Transfer_Dollars = localVariables.newFieldInRecord("pnd_Transfer_Dollars", "#TRANSFER-DOLLARS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Same_Fund_Units = localVariables.newFieldArrayInRecord("pnd_Same_Fund_Units", "#SAME-FUND-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 
            20));
        pnd_Same_Fund_Dollars = localVariables.newFieldArrayInRecord("pnd_Same_Fund_Dollars", "#SAME-FUND-DOLLARS", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 20));
        pnd_W_Ia_Rpt_Nm_Cd = localVariables.newFieldArrayInRecord("pnd_W_Ia_Rpt_Nm_Cd", "#W-IA-RPT-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            80));
        pnd_W_Ia_Rpt_Cmpny_Cd_A = localVariables.newFieldArrayInRecord("pnd_W_Ia_Rpt_Cmpny_Cd_A", "#W-IA-RPT-CMPNY-CD-A", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_Fp_Yrs = localVariables.newFieldInRecord("pnd_Fp_Yrs", "#FP-YRS", FieldType.NUMERIC, 3);
        pnd_Fp_Mos = localVariables.newFieldInRecord("pnd_Fp_Mos", "#FP-MOS", FieldType.NUMERIC, 2);
        pnd_W1_Dte = localVariables.newFieldInRecord("pnd_W1_Dte", "#W1-DTE", FieldType.STRING, 6);

        pnd_W1_Dte__R_Field_23 = localVariables.newGroupInRecord("pnd_W1_Dte__R_Field_23", "REDEFINE", pnd_W1_Dte);
        pnd_W1_Dte_Pnd_W1_Yyyy = pnd_W1_Dte__R_Field_23.newFieldInGroup("pnd_W1_Dte_Pnd_W1_Yyyy", "#W1-YYYY", FieldType.NUMERIC, 4);
        pnd_W1_Dte_Pnd_W1_Mm = pnd_W1_Dte__R_Field_23.newFieldInGroup("pnd_W1_Dte_Pnd_W1_Mm", "#W1-MM", FieldType.NUMERIC, 2);
        pnd_W2_Dte = localVariables.newFieldInRecord("pnd_W2_Dte", "#W2-DTE", FieldType.STRING, 6);

        pnd_W2_Dte__R_Field_24 = localVariables.newGroupInRecord("pnd_W2_Dte__R_Field_24", "REDEFINE", pnd_W2_Dte);
        pnd_W2_Dte_Pnd_W2_Yyyy = pnd_W2_Dte__R_Field_24.newFieldInGroup("pnd_W2_Dte_Pnd_W2_Yyyy", "#W2-YYYY", FieldType.NUMERIC, 4);
        pnd_W2_Dte_Pnd_W2_Mm = pnd_W2_Dte__R_Field_24.newFieldInGroup("pnd_W2_Dte_Pnd_W2_Mm", "#W2-MM", FieldType.NUMERIC, 2);
        pnd_Xfr_Eff_Dte_Out = localVariables.newFieldInRecord("pnd_Xfr_Eff_Dte_Out", "#XFR-EFF-DTE-OUT", FieldType.NUMERIC, 8);

        pnd_Xfr_Eff_Dte_Out__R_Field_25 = localVariables.newGroupInRecord("pnd_Xfr_Eff_Dte_Out__R_Field_25", "REDEFINE", pnd_Xfr_Eff_Dte_Out);
        pnd_Xfr_Eff_Dte_Out_Pnd_Trnsfr_Eff_Date_Out_A = pnd_Xfr_Eff_Dte_Out__R_Field_25.newFieldInGroup("pnd_Xfr_Eff_Dte_Out_Pnd_Trnsfr_Eff_Date_Out_A", 
            "#TRNSFR-EFF-DATE-OUT-A", FieldType.STRING, 8);
        pnd_To_G = localVariables.newFieldInRecord("pnd_To_G", "#TO-G", FieldType.STRING, 1);
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 1);
        pnd_Fund_Rcvd = localVariables.newFieldInRecord("pnd_Fund_Rcvd", "#FUND-RCVD", FieldType.STRING, 1);
        pnd_To_Acct_Cd = localVariables.newFieldArrayInRecord("pnd_To_Acct_Cd", "#TO-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_Max_T_Occurs = localVariables.newFieldInRecord("pnd_Max_T_Occurs", "#MAX-T-OCCURS", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Prods = localVariables.newFieldInRecord("pnd_Max_Prods", "#MAX-PRODS", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pls_Adat_Accessed = WsIndependent.getInstance().newFieldInRecord("pls_Adat_Accessed", "+ADAT-ACCESSED", FieldType.BOOLEAN, 1);
        pls_Debug = WsIndependent.getInstance().newFieldInRecord("pls_Debug", "+DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        ldaIatl420.initializeValues();
        ldaIaal420.initializeValues();
        ldaIatl171.initializeValues();
        ldaIaal162g.initializeValues();

        localVariables.reset();
        pnd_To_G.setInitialValue("G");
        pnd_Max_T_Occurs.setInitialValue(4);
        pnd_Max_Prods.setInitialValue(80);
        pnd_Max_Rate.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420() throws Exception
    {
        super("Iatn420");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATN420", onError);
        setupReports();
        getReports().write(0, " START OF IATN420 ");                                                                                                                      //Natural: WRITE ' START OF IATN420 '
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56;//Natural: FORMAT LS = 133 PS = 56
        //*  WRITE '***************************' /
        //*        '  START OF IATN420 PROGRAM'/
        //*        '***************************'
        //* *INCLUDE IAAC400
        if (condition(! (pls_Adat_Accessed.getBoolean())))                                                                                                                //Natural: IF NOT +ADAT-ACCESSED
        {
            pls_Adat_Accessed.setValue(true);                                                                                                                             //Natural: ASSIGN +ADAT-ACCESSED := TRUE
            vw_naz_Table_Ddm.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = 'NAZ021UTLUTL3'
            (
            "FIND01",
            new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", "NAZ021UTLUTL3", WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_naz_Table_Ddm.readNextRow("FIND01")))
            {
                vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
                if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                        //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y'
                {
                    continue;
                }
                if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.equals("TEST")))                                                                                     //Natural: IF NAZ-TBL-RCRD-DSCRPTN-TXT = 'TEST'
                {
                    pls_Debug.setValue(true);                                                                                                                             //Natural: ASSIGN +DEBUG := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pls_Debug.setValue(false);                                                                                                                            //Natural: ASSIGN +DEBUG := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #LOAD-EXTERNALIZE-TABLE-CODES
        sub_Pnd_Load_Externalize_Table_Codes();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #LOAD-TABLES
        sub_Pnd_Load_Tables();
        if (condition(Global.isEscape())) {return;}
        pnd_Time.setValue(pdaIatl420z.getIatl010_Pnd_Sys_Time());                                                                                                         //Natural: ASSIGN #TIME := IATL010.#SYS-TIME
        //*   WRITE /// 'CONTRACT NUMBER !!!' '=' IATL010.IA-FRM-CNTRCT
        pnd_Save_Check_Dte_A.setValueEdited(pdaIatl420z.getIatl010_Pnd_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED IATL010.#CHECK-DTE ( EM = YYYYMMDD ) TO #SAVE-CHECK-DTE-A
        pnd_Eff_Date.setValueEdited(pdaIatl420z.getIatl010_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED IATL010.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
        //*  GET LAST BUSINESS DATE OF MARCH FOR REVALUATION DATE
        pnd_Reval_Dte.setValue(pnd_Eff_Date);                                                                                                                             //Natural: ASSIGN #REVAL-DTE := #EFF-DATE
        pnd_Reval_Dte_Pnd_Reval_Dd.setValue("31");                                                                                                                        //Natural: ASSIGN #REVAL-DD := '31'
        if (condition(pnd_Reval_Dte_Pnd_Reval_Mm.equals("03")))                                                                                                           //Natural: IF #REVAL-MM = '03'
        {
            pnd_Reval_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Reval_Dte);                                                                                 //Natural: MOVE EDITED #REVAL-DTE TO #REVAL-DTE-D ( EM = YYYYMMDD )
            DbsUtil.callnat(Adsn607.class , getCurrentProcessState(), pnd_Reval_Dte_D, pnd_W_Rc);                                                                         //Natural: CALLNAT 'ADSN607' #REVAL-DTE-D #W-RC
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W_Rc.equals(getZero())))                                                                                                                    //Natural: IF #W-RC = 0
            {
                pnd_Reval_Dte.setValueEdited(pnd_Reval_Dte_D,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #REVAL-DTE-D ( EM = YYYYMMDD ) TO #REVAL-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue("*").equals("G")))                                                                                //Natural: IF IATL010.XFR-TO-ACCT-CDE ( * ) = 'G'
        {
                                                                                                                                                                          //Natural: PERFORM #GET-CONTRACT-INFO
            sub_Pnd_Get_Contract_Info();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CPR-INFO
            sub_Pnd_Process_Cpr_Info();
            if (condition(Global.isEscape())) {return;}
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Contract().setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                    //Natural: ASSIGN #AIAA079-CONTRACT := IATL010.IA-FRM-CNTRCT
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Origin().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                  //Natural: ASSIGN #AIAA079-ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Option().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Optn_Cde());                                                  //Natural: ASSIGN #AIAA079-OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
            if (condition(ldaIatl420.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte().greater(getZero())))                                                                        //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE GT 0
            {
                if (condition(ldaIatl420.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte().less(ldaIatl420.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte())))                            //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE LT IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
                {
                    pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Youngest_Dob().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte());                           //Natural: ASSIGN #AIAA079-YOUNGEST-DOB := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Youngest_Dob().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                          //Natural: ASSIGN #AIAA079-YOUNGEST-DOB := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Youngest_Dob().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                              //Natural: ASSIGN #AIAA079-YOUNGEST-DOB := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Call_Type().setValue("3");                                                                                      //Natural: ASSIGN #AIAA079-CALL-TYPE := '3'
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Effective_Date().compute(new ComputeParameters(false, pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Effective_Date()),  //Natural: ASSIGN #AIAA079-EFFECTIVE-DATE := VAL ( #EFF-DATE )
                pnd_Eff_Date.val());
            if (condition(ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().greater(getZero())))                                                           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE GT 0
            {
                pnd_W1_Dte.setValue(pnd_Eff_Date.getSubstring(1,6));                                                                                                      //Natural: ASSIGN #W1-DTE := SUBSTR ( #EFF-DATE,1,6 )
                pnd_W2_Dte.setValueEdited(ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte(),new ReportEditMask("999999"));                                 //Natural: MOVE EDITED IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE ( EM = 999999 ) TO #W2-DTE
                if (condition(pnd_W1_Dte_Pnd_W1_Mm.greater(pnd_W2_Dte_Pnd_W2_Mm)))                                                                                        //Natural: IF #W1-MM GT #W2-MM
                {
                    pnd_W2_Dte_Pnd_W2_Mm.nadd(12);                                                                                                                        //Natural: ADD 12 TO #W2-MM
                    pnd_W2_Dte_Pnd_W2_Yyyy.nsubtract(1);                                                                                                                  //Natural: SUBTRACT 1 FROM #W2-YYYY
                }                                                                                                                                                         //Natural: END-IF
                pnd_Fp_Mos.compute(new ComputeParameters(false, pnd_Fp_Mos), pnd_W2_Dte_Pnd_W2_Mm.subtract(pnd_W1_Dte_Pnd_W1_Mm));                                        //Natural: SUBTRACT #W1-MM FROM #W2-MM GIVING #FP-MOS
                pnd_Fp_Yrs.compute(new ComputeParameters(false, pnd_Fp_Yrs), pnd_W2_Dte_Pnd_W2_Yyyy.subtract(pnd_W1_Dte_Pnd_W1_Yyyy));                                    //Natural: SUBTRACT #W1-YYYY FROM #W2-YYYY GIVING #FP-YRS
                pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Final_Payt_Prd().compute(new ComputeParameters(true, pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Final_Payt_Prd()),  //Natural: COMPUTE ROUNDED #AIAA079-FINAL-PAYT-PRD = ( ( #FP-YRS * 12 ) + ( #FP-MOS ) ) / 12
                    ((pnd_Fp_Yrs.multiply(12)).add((pnd_Fp_Mos))).divide(12));
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Aian079.class , getCurrentProcessState(), pdaAiaa079.getPnd_Aiaa079_Linkage());                                                               //Natural: CALLNAT 'AIAN079' #AIAA079-LINKAGE
            if (condition(Global.isEscape())) return;
            if (condition(pls_Debug.getBoolean()))                                                                                                                        //Natural: IF +DEBUG
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-AIAN079-LINKAGE
                sub_Display_Aian079_Linkage();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_S_G_Code().getValue(1).equals("G")))                                                              //Natural: IF #AIAA079-S-G-CODE ( 1 ) = 'G'
            {
                ignore();
                //*  GRADED VINTAGE LESS THAN 4%
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue("C6");                                                                                          //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := 'C6'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CHECK ON CROSS CONTRACT TRANSFER
        DbsUtil.callnat(Iatn420l.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pdaIatl420z.getIatl010_Ia_To_Cntrct(),  //Natural: CALLNAT 'IATN420L' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE #W3-REJECT-CDE
            pdaIatl420z.getIatl010_Ia_To_Payee(), pnd_W3_Reject_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  FILL AUDIT NEW ISSUE FIELD
        //*  COMING FROM REQUEST FILE
        //*  USED IN UPDATING IA MASTER SUBPROGRAMS
        //*  USED TO DETERMINE IF NEW CONTRACT TO BE ISSUED
        //*  USED IN FILLING/NOT FILLING CNTRCT-RSDNCY-AT-IS
        DbsUtil.callnat(Iatn420k.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pdaIatl420z.getIatl010_Ia_To_Cntrct(),  //Natural: CALLNAT 'IATN420K' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE IATL010.IA-NEW-ISSUE #IAA-NEW-ISSUE #IAA-NEW-ISSUE-PHYS #IAA-NEW-ISSUE-RE IATL010.XFR-TO-ACCT-CDE ( 1:20 ) #W3-REJECT-CDE
            pdaIatl420z.getIatl010_Ia_To_Payee(), pdaIatl420z.getIatl010_Ia_New_Issue(), pnd_Iaa_New_Issue, pnd_Iaa_New_Issue_Phys, pnd_Iaa_New_Issue_Re, 
            pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(1,":",20), pnd_W3_Reject_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #FILL-CONTROL-FILE-FIELDS
        sub_Pnd_Fill_Control_File_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  GET CONTRACT INFO FOR AIAN013 LINK
                                                                                                                                                                          //Natural: PERFORM #GET-CONTRACT-INFO
        sub_Pnd_Get_Contract_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  GET CPR INFO FOR AIAN013 LINK
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CPR-INFO
        sub_Pnd_Process_Cpr_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Last_Dt.reset();                                                                                                                                              //Natural: RESET #LAST-DT #NEXT-DT #NEXT-PAY-DTE #NEXT-CHK-DT
        pnd_Next_Dt.reset();
        pnd_Next_Pay_Dte.reset();
        pnd_Next_Chk_Dt.reset();
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Dt, pnd_Next_Dt);                                                                             //Natural: CALLNAT 'IAAN0020' #LAST-DT #NEXT-DT
        if (condition(Global.isEscape())) return;
        pnd_Next_Chk_Dt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Next_Dt_Pnd_Next_Dt_Yyyy, pnd_Next_Dt_Pnd_Next_Dt_Mm, pnd_Next_Dt_Pnd_Next_Dt_Dd));  //Natural: COMPRESS #NEXT-DT-YYYY #NEXT-DT-MM #NEXT-DT-DD INTO #NEXT-CHK-DT LEAVING NO
        pnd_Last_Chk_Dt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Last_Dt_Pnd_Last_Dt_Yyyy, pnd_Last_Dt_Pnd_Last_Dt_Mm, pnd_Last_Dt_Pnd_Last_Dt_Dd));  //Natural: COMPRESS #LAST-DT-YYYY #LAST-DT-MM #LAST-DT-DD INTO #LAST-CHK-DT LEAVING NO
        //*  !!! FOR TESTING ONLY !!!!
        if (condition(pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N.less(19980501)))                                                                                                  //Natural: IF #NEXT-CHK-DT-N < 19980501
        {
            pnd_Next_Chk_Dt.setValue(19980501);                                                                                                                           //Natural: ASSIGN #NEXT-CHK-DT := 19980501
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Nazn6032.class , getCurrentProcessState(), pnd_Next_Pay_Dte, pnd_Next_Chk_Dt, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode());                     //Natural: CALLNAT 'NAZN6032' #NEXT-PAY-DTE #NEXT-CHK-DT #AIAN013-LINKAGE.#MODE
        if (condition(Global.isEscape())) return;
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte().setValue(pnd_Next_Pay_Dte);                                                                               //Natural: ASSIGN #AIAN013-LINKAGE.#NEXT-PYMNT-DTE := #NEXT-PAY-DTE
        pdaIata002.getIata002_Pnd_Next_Pymnt_Dte().setValue(pnd_Next_Pay_Dte);                                                                                            //Natural: ASSIGN IATA002.#NEXT-PYMNT-DTE := #NEXT-PAY-DTE
        //*  LOAD AUDIT FILE
                                                                                                                                                                          //Natural: PERFORM #FILL-AUDIT-WITH-REQUEST-INFO
        sub_Pnd_Fill_Audit_With_Request_Info();
        if (condition(Global.isEscape())) {return;}
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  STORE BEFORE IMAGES
        DbsUtil.callnat(Iatn420a.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pnd_Iaa_New_Issue,      //Natural: CALLNAT 'IATN420A' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE #IAA-NEW-ISSUE IATL010.XFR-FRM-ACCT-CDE ( 1 ) IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE #SAVE-CHECK-DTE #TIME #W3-REJECT-CDE
            pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1), pdaIatl420z.getIatl010_Ia_To_Cntrct(), pdaIatl420z.getIatl010_Ia_To_Payee(), pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, 
            pnd_Time, pnd_W3_Reject_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        //*  STORE HISTORICAL RATE INFO FOR
        //*  TIAA AND CREF
        DbsUtil.callnat(Iatn420b.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pnd_Iaa_New_Issue,      //Natural: CALLNAT 'IATN420B' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE #IAA-NEW-ISSUE IATL010.XFR-FRM-ACCT-CDE ( 1 ) IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE #SAVE-CHECK-DTE #LAST-CHK-DT #TIME #W3-REJECT-CDE
            pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1), pdaIatl420z.getIatl010_Ia_To_Cntrct(), pdaIatl420z.getIatl010_Ia_To_Payee(), pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, 
            pnd_Last_Chk_Dt, pnd_Time, pnd_W3_Reject_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FILL AIAN013 LINKAGE
                                                                                                                                                                          //Natural: PERFORM #FILL-AIAN013-LINKAGE
        sub_Pnd_Fill_Aian013_Linkage();
        if (condition(Global.isEscape())) {return;}
        FZ:                                                                                                                                                               //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals(" ")))                                                                         //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) EQ ' '
            {
                if (true) break FZ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FZ. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                                        //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-FRM-ACCT-CDE ( #T )
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
            sub_Pnd_Convert_One_Byte_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIatl171.getIatl171_Rcrd_Type_Cde().equals("2")))                                                                                             //Natural: IF IATL171.RCRD-TYPE-CDE = '2'
            {
                                                                                                                                                                          //Natural: PERFORM #SWITCHING-CHECK
                sub_Pnd_Switching_Check();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Switch_Fund_Found.equals(" ")))                                                                                                         //Natural: IF #SWITCH-FUND-FOUND = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                       //Natural: ASSIGN #FUND-CODE := IATL010.XFR-FRM-ACCT-CDE ( #T )
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().setValue(pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T));                           //Natural: ASSIGN #REVALUATION-INDICATOR := IATL010.XFR-FRM-UNIT-TYP ( #T )
            pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                                        //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-FRM-ACCT-CDE ( #T )
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
            sub_Pnd_Convert_One_Byte_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T).setValue(pnd_Two_Byte_Fund);                                                                      //Natural: ASSIGN IATL171.IAXFR-FROM-FUND-CDE ( #T ) := #TWO-BYTE-FUND
            ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                //Natural: ASSIGN IATL171.IAXFR-FRM-ACCT-CD ( #T ) := IATL010.XFR-FRM-ACCT-CDE ( #T )
            ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T));                               //Natural: ASSIGN IATL171.IAXFR-FRM-UNIT-TYP ( #T ) := IATL010.XFR-FRM-UNIT-TYP ( #T )
            ldaIatl171.getIatl171_Iaxfr_From_Typ().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T));                                        //Natural: ASSIGN IATL171.IAXFR-FROM-TYP ( #T ) := IATL010.XFR-FRM-TYP ( #T )
            ldaIatl171.getIatl171_Iaxfr_From_Qty().getValue(pnd_T).setValue(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                        //Natural: ASSIGN IATL171.IAXFR-FROM-QTY ( #T ) := IATL010.XFR-FRM-QTY ( #T )
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("G")))                                                                         //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) = 'G'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W3_Reject_Cde.reset();                                                                                                                                //Natural: RESET #W3-REJECT-CDE
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                sub_Pnd_Get_Unit_Value();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                          //Natural: IF #W3-REJECT-CDE NE ' '
                {
                    pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                         //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*  FLD NOT FILLED
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Unit_Val().getValue(pnd_T).setValue(0);                                                                          //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-UNIT-VAL ( #T ) := 0
            ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(pnd_T).setValue(pnd_A26_Fields_Pnd_Auv);                                                           //Natural: ASSIGN IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( #T ) := #AUV
            if (condition(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T).equals("A")))                                                                        //Natural: IF IATL171.IAXFR-FRM-UNIT-TYP ( #T ) = 'A'
            {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
                sub_Pnd_Annual_Fund_Cnv();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                sub_Pnd_Monthly_Cnv_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*      #FROM-FUND-H := IATL010.XFR-FRM-ACCT-CD(#T)
            //*      PERFORM #CNV-FUND-FOR-FUND-REC
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
            //*  LOAD AUDIT FILE AND
                                                                                                                                                                          //Natural: PERFORM #FILL-AUDIT-WITH-FUND-INFO
            sub_Pnd_Fill_Audit_With_Fund_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  AIAN014 LINKAGE
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                             //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("G")))                                                                         //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) = 'G'
            {
                                                                                                                                                                          //Natural: PERFORM #CHECK-TEACHERS-PRESENT-VALUE
                sub_Pnd_Check_Teachers_Present_Value();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #CHECK-CREFS-PRESENT-VALUE
                sub_Pnd_Check_Crefs_Present_Value();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                             //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("G")))                                                                         //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) = 'G'
            {
                //*  FILL AIAN014 LINKAGE
                                                                                                                                                                          //Natural: PERFORM #FILL-AIAN014-LINKAGE
                sub_Pnd_Fill_Aian014_Linkage();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*     WRITE 'BEFORE CALLING AIAN014'
                //*     CALLING THE LINKAGE
                DbsUtil.callnat(Aian014.class , getCurrentProcessState(), pdaIata002.getIata002());                                                                       //Natural: CALLNAT 'AIAN014' IATA002
                if (condition(Global.isEscape())) return;
                if (condition(pls_Debug.getBoolean()))                                                                                                                    //Natural: IF +DEBUG
                {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-AIAN014-LINKAGE
                    sub_Display_Aian014_Linkage();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  040612
                if (condition(pdaIata002.getIata002_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                                          //Natural: IF IATA002.#RETURN-CODE-NBR NE 0
                {
                    //*  040612
                    getReports().write(0, NEWLINE,"ERROR IN LINK AIAN014 - ERROR CD =>",pdaIata002.getIata002_Pnd_Return_Code_Nbr());                                     //Natural: WRITE / 'ERROR IN LINK AIAN014 - ERROR CD =>' IATA002.#RETURN-CODE-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "CONTRACT NUMBER =>",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()); //Natural: WRITE 'CONTRACT NUMBER =>' #CONTRACT-NUMBER #PAYEE-CODE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue("C2");                                                                                      //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := 'C2'
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  FILL REST OF AIAN013 LINK
                                                                                                                                                                          //Natural: PERFORM #FILL-REST-OF-AIAN013-LINKAGE
                sub_Pnd_Fill_Rest_Of_Aian013_Linkage();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "PROCESSING =>",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code());      //Natural: WRITE 'PROCESSING =>' #CONTRACT-NUMBER #PAYEE-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PERFORM WRITE-AIAN013-LINKAGE
                DbsUtil.callnat(Aian013.class , getCurrentProcessState(), pdaAial0130.getPnd_Aian013_Linkage());                                                          //Natural: CALLNAT 'AIAN013' #AIAN013-LINKAGE
                if (condition(Global.isEscape())) return;
                if (condition(pls_Debug.getBoolean()))                                                                                                                    //Natural: IF +DEBUG
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-AIAN013-LINKAGE
                    sub_Write_Aian013_Linkage();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  040612
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                             //Natural: IF #AIAN013-LINKAGE.#RETURN-CODE-NBR NE 0
                {
                    //*  040612
                    getReports().write(0, NEWLINE,"ERROR IN LINK AIAN013 - ERROR CD =>",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr());                        //Natural: WRITE / 'ERROR IN LINK AIAN013 - ERROR CD =>' #AIAN013-LINKAGE.#RETURN-CODE-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "CONTRACT NUMBER =>",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()); //Natural: WRITE 'CONTRACT NUMBER =>' #CONTRACT-NUMBER #PAYEE-CODE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue("C3");                                                                                      //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := 'C3'
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("G")))                                                                         //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) = 'G'
            {
                ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T).nadd(pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue("*"));                            //Natural: ADD IATA002.#GTD-PMT-GRD ( * ) TO IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T )
                ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Divid().getValue(pnd_T).nadd(pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue("*"));                           //Natural: ADD IATA002.#DVD-PMT-GRD ( * ) TO IATL171.IAXFR-FROM-RQSTD-XFR-DIVID ( #T )
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( #T ) = IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) - IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T )
                    ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).subtract(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T)));
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Divid().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Divid().getValue(pnd_T)),  //Natural: COMPUTE IATL171.IAXFR-FROM-AFTR-XFR-DIVID ( #T ) = IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T ) - IATL171.IAXFR-FROM-RQSTD-XFR-DIVID ( #T )
                    ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T).subtract(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Divid().getValue(pnd_T)));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Units().getValue(pnd_T).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units());                     //Natural: ASSIGN IATL171.IAXFR-FROM-RQSTD-XFR-UNITS ( #T ) := #TRANSFER-UNITS
                pnd_Acc.reset();                                                                                                                                          //Natural: RESET #ACC
                pnd_Acc.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue("*"));                                                                     //Natural: ADD #PMTS-TO-RECEIVE ( * ) TO #ACC
                ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T).setValue(pnd_Acc);                                                                      //Natural: ASSIGN IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T ) := #ACC
                //*   IATL171.IAXFR-FROM-RQSTD-XFR-GUAR(#T)  := #PMTS-TO-RECEIVE(*)
            }                                                                                                                                                             //Natural: END-IF
            //*  FILL CALC AND CONTROL FILES
                                                                                                                                                                          //Natural: PERFORM #FILLING-TO-SIDE
            sub_Pnd_Filling_To_Side();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).notEquals("G")))                                                                      //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) NOT = 'G'
            {
                pnd_Xfr_Eff_Dte_Out.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Eff_Date_Out());                                                             //Natural: ASSIGN #XFR-EFF-DTE-OUT := #TRANSFER-EFF-DATE-OUT
                ldaIatl171.getIatl171_Iaxfr_Calc_Unit_Val_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Xfr_Eff_Dte_Out_Pnd_Trnsfr_Eff_Date_Out_A);             //Natural: MOVE EDITED #TRNSFR-EFF-DATE-OUT-A TO IATL171.IAXFR-CALC-UNIT-VAL-DTE ( EM = YYYYMMDD )
                //*    COMPUTE ROUNDED IATL171.IAXFR-FROM-RQSTD-XFR-AMT(#T) =
                //*          IATL171.IAXFR-FROM-REVAL-UNIT-VAL(#T) * #TRANSFER-UNITS
                ldaIatl171.getIatl171_Iaxfr_From_Asset_Xfr_Amt().getValue(pnd_T).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out());               //Natural: ASSIGN IATL171.IAXFR-FROM-ASSET-XFR-AMT ( #T ) := #TOTAL-TRANSFER-AMT-OUT
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_T)),  //Natural: COMPUTE IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( #T ) = IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T ) - IATL171.IAXFR-FROM-RQSTD-XFR-UNITS ( #T )
                    ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T).subtract(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Units().getValue(pnd_T)));
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( #T ) = IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) - IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T )
                    ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).subtract(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T)));
                ldaIatl171.getIatl171_Iaxfr_From_Pmt_Aftr_Xfr().getValue(pnd_T).compute(new ComputeParameters(true, ldaIatl171.getIatl171_Iaxfr_From_Pmt_Aftr_Xfr().getValue(pnd_T)),  //Natural: COMPUTE ROUNDED IATL171.IAXFR-FROM-PMT-AFTR-XFR ( #T ) = IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( #T ) * IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( #T )
                    ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(pnd_T)));
            }                                                                                                                                                             //Natural: END-IF
            //*  GRADED TO STANDARD IS NOW LEDGERED             /* 122308 START
            //*  IF IATL010.XFR-FRM-ACCT-CDE(1) = 'T' OR = 'G'
            //*    IGNORE
            //*  ELSE                                           /* 122308 END
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
                                                                                                                                                                          //Natural: PERFORM #FILL-LEDGER-PDA
            sub_Pnd_Fill_Ledger_Pda();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().notEquals(" ")))                                                                              //Natural: IF #IATA422-RETURN-CD NE ' '
            {
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd());                                             //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #IATA422-RETURN-CD
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  END-IF                                         /* 122308
                                                                                                                                                                          //Natural: PERFORM #RESET-FUND-INFO
            sub_Pnd_Reset_Fund_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        if (condition(ldaIatl171.getIatl171_Rcrd_Type_Cde().equals("2")))                                                                                                 //Natural: IF IATL171.RCRD-TYPE-CDE = '2'
        {
            if (condition(pnd_At_Least_One_Switch.equals(" ")))                                                                                                           //Natural: IF #AT-LEAST-ONE-SWITCH = ' '
            {
                pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue("S1");                                                                                          //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := 'S1'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FK:                                                                                                                                                               //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(" ")))                                                                          //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = ' '
            {
                if (true) break FK;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FK. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)),  //Natural: COMPUTE IATL171.IAXFR-TO-AFTR-XFR-UNITS ( #I ) = IATL171.IAXFR-TO-BFR-XFR-UNITS ( #I ) + IATL171.IAXFR-TO-XFR-UNITS ( #I ) - #SAME-FUND-UNITS ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).add(ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(pnd_I)).subtract(pnd_Same_Fund_Units.getValue(pnd_I)));
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_I).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_I)),  //Natural: COMPUTE IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #I ) = IATL171.IAXFR-TO-BFR-XFR-GUAR ( #I ) + IATL171.IAXFR-TO-XFR-GUAR ( #I ) - #SAME-FUND-DOLLARS ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_I).add(ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_I)).subtract(pnd_Same_Fund_Dollars.getValue(pnd_I)));
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(pnd_I).compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(pnd_I)),  //Natural: COMPUTE IATL171.IAXFR-TO-AFTR-XFR-DIVID ( #I ) = IATL171.IAXFR-TO-BFR-XFR-DIVID ( #I ) + IATL171.IAXFR-TO-XFR-DIVID ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Divid().getValue(pnd_I).add(ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(pnd_I)));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaIatl420x.getPnd_Iaa_Xfr_Audit().setValuesByName(ldaIatl171.getVw_iatl171());                                                                                   //Natural: MOVE BY NAME IATL171 TO #IAA-XFR-AUDIT
        pnd_W3_Reject_Cde.reset();                                                                                                                                        //Natural: RESET #W3-REJECT-CDE
        if (condition(pdaIatl420z.getIatl010_Ia_To_Cntrct().notEquals(" ")))                                                                                              //Natural: IF IATL010.IA-TO-CNTRCT NOT = ' '
        {
            DbsUtil.callnat(Iatn420d.class , getCurrentProcessState(), pdaIatl420x.getPnd_Iatn420x_In(), pdaIatl420x.getPnd_Iaa_Xfr_Audit(), pnd_Ivc_Pro_Adj,             //Natural: CALLNAT 'IATN420D' #IATN420X-IN #IAA-XFR-AUDIT #IVC-PRO-ADJ #PER-IVC-PRO-ADJ #IVC-IND #W3-REJECT-CDE
                pnd_Per_Ivc_Pro_Adj, pnd_Ivc_Ind, pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Iatn420c.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(), pdaIatl420z.getIatl010_Ia_To_Cntrct(),  //Natural: CALLNAT 'IATN420C' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE IATL010.IA-TO-CNTRCT IATL010.IA-TO-PAYEE #TIME #IVC-IND #IAA-NEW-ISSUE IATL010.#CHECK-DTE IATL010.#TODAYS-DTE IATL010.RQST-EFFCTV-DTE IATL010.RCRD-TYPE-CDE IATL010.RQST-XFR-TYPE
            pdaIatl420z.getIatl010_Ia_To_Payee(), pnd_Time, pnd_Ivc_Ind, pnd_Iaa_New_Issue, pdaIatl420z.getIatl010_Pnd_Check_Dte(), pdaIatl420z.getIatl010_Pnd_Todays_Dte(), 
            pdaIatl420z.getIatl010_Rqst_Effctv_Dte(), pdaIatl420z.getIatl010_Rcrd_Type_Cde(), pdaIatl420z.getIatl010_Rqst_Xfr_Type());
        if (condition(Global.isEscape())) return;
        //*         IATL010.#NEXT-BUS-DTE
        //*  6/99 KN
        //* *IF (#EFF-DATE-MM = '03' AND #EFF-DATE-DD = '31')
        if (condition((pnd_Eff_Date_Pnd_Eff_Date_Mm.equals("03") && pnd_Eff_Date_Pnd_Eff_Date_Dd.equals(pnd_Reval_Dte_Pnd_Reval_Dd))))                                    //Natural: IF ( #EFF-DATE-MM = '03' AND #EFF-DATE-DD = #REVAL-DD )
        {
                                                                                                                                                                          //Natural: PERFORM #REVALUE-03-31
            sub_Pnd_Revalue_03_31();
            if (condition(Global.isEscape())) {return;}
            pnd_Eff_Dte_03_31.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #EFF-DTE-03-31
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue("*").equals("M") || ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue("*").equals("M")))       //Natural: IF IATL171.IAXFR-FRM-UNIT-TYP ( * ) = 'M' OR IATL171.IAXFR-TO-UNIT-TYP ( * ) = 'M'
        {
            //*   AND IATL171.RCRD-TYPE-CDE = '1'
                                                                                                                                                                          //Natural: PERFORM #FILL-MONTHLY-DOLLARS
            sub_Pnd_Fill_Monthly_Dollars();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #UPDATE-MASTER
        sub_Pnd_Update_Master();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                                  //Natural: IF #W3-REJECT-CDE NE ' '
        {
            getReports().write(0, " UPDATE MASTER ERROR ","=",pnd_W3_Reject_Cde);                                                                                         //Natural: WRITE ' UPDATE MASTER ERROR ' '=' #W3-REJECT-CDE
            if (Global.isEscape()) return;
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().setValue(pnd_W3_Reject_Cde);                                                                                 //Natural: ASSIGN #IATN420-OUT.#RETURN-CODE := #W3-REJECT-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl171.getVw_iatl171().insertDBRow();                                                                                                                         //Natural: STORE IATL171
        pdaIatl420x.getPnd_Iaa_Xfr_Audit().setValuesByName(ldaIatl171.getVw_iatl171());                                                                                   //Natural: MOVE BY NAME IATL171 TO #IAA-XFR-AUDIT
        DbsUtil.callnat(Iatn420x.class , getCurrentProcessState(), pdaIatl420x.getPnd_Iatn420x_In(), pdaIatl420x.getPnd_Iaa_Xfr_Audit());                                 //Natural: CALLNAT 'IATN420X' #IATN420X-IN #IAA-XFR-AUDIT
        if (condition(Global.isEscape())) return;
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-AUDIT-WITH-REQUEST-INFO
        //* *******************************************************************
        //*     IATL171.IAXFR-CALC-SSN            := IATL010.RQST-SSN
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-CONTRACT-INFO
        //* ******************************************************************
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONVERT-ONE-BYTE-FUND
        //* *0. FOR #II = 1 TO 40
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-AUDIT-WITH-FUND-INFO
        //*    (#FROM-FUND-TOT NE 'U09' AND #FROM-FUND-TOT NE 'W09')
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TIAA-FUND-FROM
        //* **********************************************************************
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-LEDGER-PDA
        //* *******************************************************************
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CREF-FUND-FROM
        //* **********************************************************************
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CPR-INFO
        //* ******************************************************************
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FROM-FUND-TABLE
        //* *F6. FOR #I = 1 TO 3
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-T-TABLE-1
        //* *F7A. FOR #J = 1 TO 3
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-C-TABLE-1
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #COMPARE-SAME-FUND-1
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #LOAD-TABLES
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #UPDATE-MASTER
        //*  WHEN #INTRA-FUND = 'Y' AND #FROM-REAL = 'Y'
        //*  WHEN #INTRA-FUND = ' ' AND #FROM-REAL = 'Y'
        //*    WHEN #INTRA-FUND = 'Y' AND #FROM-TIAA = 'Y'
        //*            #PARTIAL-TRANSFER
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-CONTROL-FILE-FIELDS
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILLING-TO-SIDE
        //*  FS. FOR #I = 1 TO 20
        //* *    ADD 1 TO #CTL-TO-NEW-FUND-RCRDS(#SUB-TO)
        //* *                                                ANYWHERE
        //*        MOVE '1' TO #TEACHERS-FUND-CONV
        //*        #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*        PERFORM GET-FUND-TO-RECEIVE
        //*        #TEACHERS-FUND-CONV := #FUND-RCVD
        //*        MOVE '2' TO #TEACHERS-FUND-CONV
        //*        #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*        PERFORM GET-FUND-TO-RECEIVE
        //*        #TEACHERS-FUND-CONV := #FUND-RCVD
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-UNITS-LINKAGE
        //* *********************************************************************
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-TEACHER-LINKAGE
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #LOAD-EXTERNALIZE-TABLE-CODES
        //* *#W-RATE-CDE (*)           := IAAA051Z.#V2-RT-1(*)
        //* *F. FOR #F = 1 TO 20
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-AIAN013-LINKAGE
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-REST-OF-AIAN013-LINKAGE
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-NEW-FUND
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-CREF-NEW-FUND
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-TIAA-NEW-FUND
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-AIAN014-LINKAGE
        //* *FOR #I 1 TO 99
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-CREFS-PRESENT-VALUE
        //* ********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-TEACHERS-PRESENT-VALUE
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-UNIT-VALUE
        //* ******************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-FUND-CNV
        //* ******************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-CNV-FUND
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-TRNSFR-UNITS
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-TRNSFR-UNITS-AND-DOLLARS
        //*   WRITE '=' #XFR-UNITS '=' #TRANSFER-DOLLARS
        //* ********************************************************122308*********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-TO-RECEIVE
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-TRANSFER-TO-ARRAYS
        //*     FOR #I = 1 TO 20
        //*      #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*      PERFORM GET-FUND-TO-RECEIVE
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-TRANSFER-TO-ARRAYS-SWITCH
        //*     FOR #I = 1 TO 20
        //*      #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*      PERFORM GET-FUND-TO-RECEIVE
        //*       #TO-UNITS := #UNITS-TO-RECEIVE(1)
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-TRANSFER-TO-ARRAYS
        //* *********************************************************************
        //*     FOR #I = 1 TO 20
        //*      #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*      PERFORM GET-FUND-TO-RECEIVE
        //*  KN     #TO-UNITS := #UNITS-TO-RECEIVE(#I)
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-TRANSFER-TO-ARRAYS-SWITCH
        //* *********************************************************************
        //*     FOR #I = 1 TO 20
        //*      #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*      PERFORM GET-FUND-TO-RECEIVE
        //*  KN     #TO-UNITS := #UNITS-TO-RECEIVE(1)
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-TO-SIDE
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-TO-SIDE-SWITCH
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ROUNDING-UNITS
        //* ******************************************************************
        //* **     RESET #TO-UNITS
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ROUNDING-DOLLARS
        //* ******************************************************************
        //* **     RESET #TO-PYMTS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FROM-FUND-ONLY-PARA
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-FULL-TIAA-REAL-OUT
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-FULL-6M-REAL-OUT
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-FULL-CREF-OUT
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-IF-REAL-OUT
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-IF-CREF-OUT
        //* *********************************************************************
        //* ******************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-TIAA-TO-SIDE
        //* *********************************************************************
        //* *********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-6M-TO-SIDE
        //* *********************************************************************
        //* *********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-CREF-TO-SIDE
        //* *********************************************************************
        //* *********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SWITCHING-CHECK
        //* *********************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-FUND-INFO
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #REVALUE-03-31
        //* ******************************************************************
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-MONTHLY-DOLLARS
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AIAN013-LINKAGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-AIAN079-LINKAGE
        //* *FOR #A 1 99
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-AIAN014-LINKAGE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-AIAN026-LINKAGE
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pdaAial0130.getPnd_Aian013_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN013-LINKAGE #J #TO-UNITS #D-UNITS #W-UNITS #E-UNITS
        pnd_J.reset();
        pnd_To_Units.reset();
        pnd_D_Units.reset();
        pnd_W_Units.reset();
        pnd_E_Units.reset();
    }
    private void sub_Pnd_Fill_Audit_With_Request_Info() throws Exception                                                                                                  //Natural: #FILL-AUDIT-WITH-REQUEST-INFO
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl171.getIatl171_Rqst_Id().setValue(pdaIatl420z.getIatl010_Rqst_Id());                                                                                       //Natural: ASSIGN IATL171.RQST-ID := IATL010.RQST-ID
        ldaIatl171.getIatl171_Rcrd_Type_Cde().setValue(pdaIatl420z.getIatl010_Rcrd_Type_Cde());                                                                           //Natural: ASSIGN IATL171.RCRD-TYPE-CDE := IATL010.RCRD-TYPE-CDE
        ldaIatl171.getIatl171_Iaxfr_Entry_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Entry_Dte());                                                                        //Natural: ASSIGN IATL171.IAXFR-ENTRY-DTE := IATL010.RQST-ENTRY-DTE
        ldaIatl171.getIatl171_Iaxfr_Entry_Tme().setValue(pdaIatl420z.getIatl010_Rqst_Entry_Tme());                                                                        //Natural: ASSIGN IATL171.IAXFR-ENTRY-TME := IATL010.RQST-ENTRY-TME
        ldaIatl171.getIatl171_Iaxfr_Opn_Clsd_Ind().setValue(pdaIatl420z.getIatl010_Rqst_Opn_Clsd_Ind());                                                                  //Natural: ASSIGN IATL171.IAXFR-OPN-CLSD-IND := IATL010.RQST-OPN-CLSD-IND
        ldaIatl171.getIatl171_Iaxfr_In_Progress_Ind().setValue(pdaIatl420z.getIatl010_Xfr_In_Progress_Ind().getBoolean());                                                //Natural: ASSIGN IATL171.IAXFR-IN-PROGRESS-IND := IATL010.XFR-IN-PROGRESS-IND
        ldaIatl171.getIatl171_Iaxfr_Retry_Cnt().setValue(pdaIatl420z.getIatl010_Xfr_Retry_Cnt());                                                                         //Natural: ASSIGN IATL171.IAXFR-RETRY-CNT := IATL010.XFR-RETRY-CNT
        ldaIatl171.getIatl171_Iaxfr_Calc_Unique_Id().setValue(pdaIatl420z.getIatl010_Ia_Unique_Id());                                                                     //Natural: ASSIGN IATL171.IAXFR-CALC-UNIQUE-ID := IATL010.IA-UNIQUE-ID
        ldaIatl171.getIatl171_Iaxfr_Effctve_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Effctv_Dte());                                                                     //Natural: ASSIGN IATL171.IAXFR-EFFCTVE-DTE := IATL010.RQST-EFFCTV-DTE
        ldaIatl171.getIatl171_Iaxfr_Rqst_Rcvd_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Rcvd_Dte());                                                                     //Natural: ASSIGN IATL171.IAXFR-RQST-RCVD-DTE := IATL010.RQST-RCVD-DTE
        ldaIatl171.getIatl171_Iaxfr_Rqst_Rcvd_Tme().setValue(pdaIatl420z.getIatl010_Rqst_Rcvd_Tme());                                                                     //Natural: ASSIGN IATL171.IAXFR-RQST-RCVD-TME := IATL010.RQST-RCVD-TME
        ldaIatl171.getIatl171_Iaxfr_Invrse_Rcvd_Tme().setValue(0);                                                                                                        //Natural: ASSIGN IATL171.IAXFR-INVRSE-RCVD-TME := 0
        ldaIatl171.getIatl171_Iaxfr_Invrse_Effctve_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Rcprcl_Dte());                                                              //Natural: ASSIGN IATL171.IAXFR-INVRSE-EFFCTVE-DTE := IATL010.RQST-RCPRCL-DTE
        ldaIatl171.getIatl171_Iaxfr_Calc_Sttmnt_Indctr().setValue(pdaIatl420z.getIatl010_Rqst_Sttmnt_Ind());                                                              //Natural: ASSIGN IATL171.IAXFR-CALC-STTMNT-INDCTR := IATL010.RQST-STTMNT-IND
        ldaIatl171.getIatl171_Iaxfr_Calc_Status_Cde().setValue("M0");                                                                                                     //Natural: ASSIGN IATL171.IAXFR-CALC-STATUS-CDE := 'M0'
        ldaIatl171.getIatl171_Iaxfr_Cwf_Wpid().setValue(pdaIatl420z.getIatl010_Xfr_Work_Prcss_Id());                                                                      //Natural: ASSIGN IATL171.IAXFR-CWF-WPID := IATL010.XFR-WORK-PRCSS-ID
        ldaIatl171.getIatl171_Iaxfr_Cwf_Log_Dte_Time().setValue(pdaIatl420z.getIatl010_Xfr_Mit_Log_Dte_Tme());                                                            //Natural: ASSIGN IATL171.IAXFR-CWF-LOG-DTE-TIME := IATL010.XFR-MIT-LOG-DTE-TME
        ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                     //Natural: ASSIGN IATL171.IAXFR-FROM-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        if (condition(pdaIatl420z.getIatl010_Ia_Frm_Payee().equals(" ")))                                                                                                 //Natural: IF IATL010.IA-FRM-PAYEE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde().compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde()), pdaIatl420z.getIatl010_Ia_Frm_Payee().val()); //Natural: ASSIGN IATL171.IAXFR-FROM-PAYEE-CDE := VAL ( IATL010.IA-FRM-PAYEE )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl171.getIatl171_Iaxfr_Calc_Reject_Cde().setValue(pdaIatl420z.getIatl010_Xfr_Rjctn_Cde());                                                                   //Natural: ASSIGN IATL171.IAXFR-CALC-REJECT-CDE := IATL010.XFR-RJCTN-CDE
        ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                                   //Natural: ASSIGN IATL171.IAXFR-CALC-TO-PPCN-NBR := IATL010.IA-TO-CNTRCT
        if (condition(pdaIatl420z.getIatl010_Ia_To_Payee().equals(" ")))                                                                                                  //Natural: IF IATL010.IA-TO-PAYEE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde().compute(new ComputeParameters(false, ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde()), pdaIatl420z.getIatl010_Ia_To_Payee().val()); //Natural: ASSIGN IATL171.IAXFR-CALC-TO-PAYEE-CDE := VAL ( IATL010.IA-TO-PAYEE )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_New_Issue_Ind().setValue(pnd_Iaa_New_Issue_Phys);                                                                        //Natural: ASSIGN IATL171.IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND := #IAA-NEW-ISSUE-PHYS
        ldaIatl171.getIatl171_Iaxfr_Cycle_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Todays_Dte());                                                                        //Natural: ASSIGN IATL171.IAXFR-CYCLE-DTE := IATL010.#TODAYS-DTE
        ldaIatl171.getIatl171_Iaxfr_Acctng_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte());                                                                     //Natural: ASSIGN IATL171.IAXFR-ACCTNG-DTE := IATL010.#NEXT-BUS-DTE
        ldaIatl171.getIatl171_Lst_Chnge_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Sys_Time());                                                                            //Natural: ASSIGN IATL171.LST-CHNGE-DTE := IATL010.#SYS-TIME
        ldaIatl171.getIatl171_Rqst_Xfr_Type().setValue(pdaIatl420z.getIatl010_Rqst_Xfr_Type());                                                                           //Natural: ASSIGN IATL171.RQST-XFR-TYPE := IATL010.RQST-XFR-TYPE
        ldaIatl171.getIatl171_Rqst_Unit_Cde().setValue(pdaIatl420z.getIatl010_Rqst_Unit_Cde());                                                                           //Natural: ASSIGN IATL171.RQST-UNIT-CDE := IATL010.RQST-UNIT-CDE
        ldaIatl171.getIatl171_Ia_New_Iss_Prt_Pull().setValue(pdaIatl420z.getIatl010_Ia_New_Iss_Prt_Pull());                                                               //Natural: ASSIGN IATL171.IA-NEW-ISS-PRT-PULL := IATL010.IA-NEW-ISS-PRT-PULL
    }
    private void sub_Pnd_Get_Contract_Info() throws Exception                                                                                                             //Natural: #GET-CONTRACT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number().setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                        //Natural: ASSIGN #CONTRACT-NUMBER := IATL010.IA-FRM-CNTRCT
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().compute(new ComputeParameters(false, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()),                    //Natural: ASSIGN #PAYEE-CODE := VAL ( IATL010.IA-FRM-PAYEE )
            pdaIatl420z.getIatl010_Ia_Frm_Payee().val());
        ldaIatl420.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CONTRACT-NUMBER
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(), WcType.WITH) },
        1
        );
        F1:
        while (condition(ldaIatl420.getVw_iaa_Cntrct().readNextRow("F1", true)))
        {
            ldaIatl420.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIatl420.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                       //Natural: IF NO RECORDS FOUND
            {
                pnd_W3_Reject_Cde.setValue("M1");                                                                                                                         //Natural: MOVE 'M1' TO #W3-REJECT-CDE
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
                //*  02/10
            }                                                                                                                                                             //Natural: END-NOREC
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Origin().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                         //Natural: ASSIGN #AIAN013-LINKAGE.#ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Optn_Cde());                                                         //Natural: ASSIGN #AIAN013-LINKAGE.#OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
            pdaIata002.getIata002_Pnd_Origin().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                      //Natural: ASSIGN IATA002.#ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Issue_Dte());                                                    //Natural: ASSIGN #ISSUE-DATE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            pnd_Total_Issue_Date_Pnd_Issue_Date_6.setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Issue_Dte());                                                                  //Natural: ASSIGN #ISSUE-DATE-6 := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            pnd_Total_Issue_Date_Pnd_Issue_Date_2.setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                               //Natural: ASSIGN #ISSUE-DATE-2 := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            if (condition(pnd_Total_Issue_Date_Pnd_Issue_Date_2.equals(getZero())))                                                                                       //Natural: IF #ISSUE-DATE-2 = 0
            {
                pnd_Total_Issue_Date_Pnd_Issue_Date_2_A.setValue("01");                                                                                                   //Natural: MOVE '01' TO #ISSUE-DATE-2-A
            }                                                                                                                                                             //Natural: END-IF
            pdaIata002.getIata002_Pnd_Issue_Date_8().setValue(pnd_Total_Issue_Date);                                                                                      //Natural: ASSIGN IATA002.#ISSUE-DATE-8 := #TOTAL-ISSUE-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                                        //Natural: ASSIGN #AIAN013-LINKAGE.#FIRST-ANN-DOB := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde());                                        //Natural: ASSIGN #AIAN013-LINKAGE.#FIRST-ANN-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte());                                        //Natural: ASSIGN #AIAN013-LINKAGE.#FIRST-ANN-DOD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte());                                        //Natural: ASSIGN #AIAN013-LINKAGE.#SECOND-ANN-DOB := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde());                                        //Natural: ASSIGN #AIAN013-LINKAGE.#SECOND-ANN-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dod().setValue(ldaIatl420.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte());                                        //Natural: ASSIGN #AIAN013-LINKAGE.#SECOND-ANN-DOD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Convert_One_Byte_Fund() throws Exception                                                                                                         //Natural: #CONVERT-ONE-BYTE-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*  123008
        pnd_Two_Byte_Fund.reset();                                                                                                                                        //Natural: RESET #TWO-BYTE-FUND
        F30:                                                                                                                                                              //Natural: FOR #II = 1 TO #MAX-PRODS
        for (pnd_Ii.setValue(1); condition(pnd_Ii.lessOrEqual(pnd_Max_Prods)); pnd_Ii.nadd(1))
        {
            if (condition(pnd_Alpha_Cd_Table.getValue(pnd_Ii).equals(" ")))                                                                                               //Natural: IF #ALPHA-CD-TABLE ( #II ) EQ ' '
            {
                if (true) break F30;                                                                                                                                      //Natural: ESCAPE BOTTOM ( F30. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Alpha_Cd_Table.getValue(pnd_Ii).equals(pnd_One_Byte_Fund)))                                                                             //Natural: IF #ALPHA-CD-TABLE ( #II ) = #ONE-BYTE-FUND
                {
                    pnd_Two_Byte_Fund.setValue(pnd_Ctl_Fund_Cde.getValue(pnd_Ii));                                                                                        //Natural: MOVE #CTL-FUND-CDE ( #II ) TO #TWO-BYTE-FUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Audit_With_Fund_Info() throws Exception                                                                                                     //Natural: #FILL-AUDIT-WITH-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  122308
        if (condition((pnd_Contract_Type.equals("T") && ! ((pnd_From_Fund_Tot.getSubstring(1,1).equals("U") || pnd_From_Fund_Tot.getSubstring(1,1).equals("W"))))))       //Natural: IF #CONTRACT-TYPE = 'T' AND NOT SUBSTR ( #FROM-FUND-TOT,1,1 ) EQ 'U' OR EQ 'W'
        {
            pnd_W3_Reject_Cde.setValue("T3");                                                                                                                             //Natural: ASSIGN #W3-REJECT-CDE = 'T3'
                                                                                                                                                                          //Natural: PERFORM #TIAA-FUND-FROM
            sub_Pnd_Tiaa_Fund_From();
            if (condition(Global.isEscape())) {return;}
            //*  WRITE '=' #W3-REJECT-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W3_Reject_Cde.setValue("T4");                                                                                                                             //Natural: ASSIGN #W3-REJECT-CDE = 'T4'
                                                                                                                                                                          //Natural: PERFORM #CREF-FUND-FROM
            sub_Pnd_Cref_Fund_From();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Tiaa_Fund_From() throws Exception                                                                                                                //Natural: #TIAA-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := IATL010.#IA-FRM-PAYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE := #FROM-FUND-TOT
        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1A",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1A:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1A")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt());                             //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) := IAA-TIAA-FUND.TIAA-TOT-PER-AMT
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T).setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt());                            //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T ) := IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
                pnd_Tiaa_Rates_Pnd_Rate_Code.getValue("*").setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Cde().getValue("*"));                                           //Natural: ASSIGN #RATE-CODE ( * ) := IAA-TIAA-FUND.TIAA-RATE-CDE ( * )
                pnd_Tiaa_Rates_Pnd_Rate_Date.getValue("*").setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Rate_Dte().getValue("*"));                                           //Natural: ASSIGN #RATE-DATE ( * ) := IAA-TIAA-FUND.TIAA-RATE-DTE ( * )
                pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue("*").setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Pay_Amt().getValue("*"));                                          //Natural: ASSIGN #GTD-PMT ( * ) := IAA-TIAA-FUND.TIAA-PER-PAY-AMT ( * )
                pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue("*").setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Per_Div_Amt().getValue("*"));                                          //Natural: ASSIGN #DVD-PMT ( * ) := IAA-TIAA-FUND.TIAA-PER-DIV-AMT ( * )
                pnd_W3_Reject_Cde.reset();                                                                                                                                //Natural: RESET #W3-REJECT-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1A;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1A. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Ledger_Pda() throws Exception                                                                                                               //Natural: #FILL-LEDGER-PDA
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata422.getPnd_Iata422_Pnd_Iata422_Rqst_Id().setValue(pdaIatl420z.getIatl010_Rqst_Id());                                                                       //Natural: ASSIGN #IATA422-RQST-ID := IATL010.RQST-ID
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Todays_Dte());                                                         //Natural: ASSIGN #IATA422-LDGR-CYCLE-DTE := IATL010.#TODAYS-DTE
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte().setValue(pdaIatl420z.getIatl010_Rqst_Effctv_Dte());                                                       //Natural: ASSIGN #IATA422-LDGR-EFFCTV-DTE := IATL010.RQST-EFFCTV-DTE
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Accntng_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte());                                                     //Natural: ASSIGN #IATA422-LDGR-ACCNTNG-DTE := IATL010.#NEXT-BUS-DTE
        pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                           //Natural: ASSIGN #IATA422-FROM-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Payee_Cde().compute(new ComputeParameters(false, pdaIata422.getPnd_Iata422_Pnd_Iata422_From_Payee_Cde()),              //Natural: ASSIGN #IATA422-FROM-PAYEE-CDE := VAL ( IATL010.IA-FRM-PAYEE )
            pdaIatl420z.getIatl010_Ia_Frm_Payee().val());
        pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr().setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                              //Natural: ASSIGN #IATA422-TO-PPCN-NBR := IATL010.IA-TO-CNTRCT
        if (condition(pdaIatl420z.getIatl010_Ia_To_Payee().equals(" ")))                                                                                                  //Natural: IF IATL010.IA-TO-PAYEE = ' '
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Pye_Cde().setValue(0);                                                                                               //Natural: ASSIGN #IATA422-TO-PYE-CDE := 0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Pye_Cde().compute(new ComputeParameters(false, pdaIata422.getPnd_Iata422_Pnd_Iata422_To_Pye_Cde()),                  //Natural: ASSIGN #IATA422-TO-PYE-CDE := VAL ( IATL010.IA-TO-PAYEE )
                pdaIatl420z.getIatl010_Ia_To_Payee().val());
        }                                                                                                                                                                 //Natural: END-IF
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde().setValue(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T));                                 //Natural: ASSIGN #IATA422-LDGR-FROM-FUND-CDE := IATL171.IAXFR-FROM-FUND-CDE ( #T )
        //*  122308
        pnd_To_Acct_Cd.getValue("*").reset();                                                                                                                             //Natural: RESET #TO-ACCT-CD ( * )
        //*  122308
        if (condition(pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("2")))                                                                                                //Natural: IF IATL010.RCRD-TYPE-CDE = '2'
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Switch_Ind().setValue("S");                                                                                             //Natural: ASSIGN #IATA422-SWITCH-IND := 'S'
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(1).setValue(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T));                     //Natural: ASSIGN #IATA422-LDGR-TO-FUND-CDE ( 1 ) := IATL171.IAXFR-TO-FUND-CDE ( #T )
            pnd_To_Acct_Cd.getValue(1).setValue(ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(pnd_T));                                                                //Natural: ASSIGN #TO-ACCT-CD ( 1 ) := IATL171.IAXFR-TO-ACCT-CD ( #T )
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Amt().getValue(1).setValue(pnd_To_Asset_Amt.getValue(pnd_T));                                                      //Natural: ASSIGN #IATA422-LDGR-AMT ( 1 ) := #TO-ASSET-AMT ( #T )
            //*  122308
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Switch_Ind().setValue(" ");                                                                                             //Natural: ASSIGN #IATA422-SWITCH-IND := ' '
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde().getValue(1,":",20).setValue(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,                    //Natural: ASSIGN #IATA422-LDGR-TO-FUND-CDE ( 1:20 ) := IATL171.IAXFR-TO-FUND-CDE ( 1:20 )
                ":",20));
            pnd_To_Acct_Cd.getValue("*").setValue(ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue("*"));                                                                //Natural: ASSIGN #TO-ACCT-CD ( * ) := IATL171.IAXFR-TO-ACCT-CD ( * )
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ().getValue(1,":",20).setValue(ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,                    //Natural: ASSIGN #IATA422-LDGR-TO-UNIT-TYP ( 1:20 ) := IATL171.IAXFR-TO-UNIT-TYP ( 1:20 )
                ":",20));
            pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Amt().getValue(1,":",20).setValue(pnd_To_Asset_Amt.getValue(1,":",20));                                            //Natural: ASSIGN #IATA422-LDGR-AMT ( 1:20 ) := #TO-ASSET-AMT ( 1:20 )
        }                                                                                                                                                                 //Natural: END-IF
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd().setValue(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T));                                        //Natural: ASSIGN #IATA422-LDGR-ACCT-CD := IATL171.IAXFR-FRM-UNIT-TYP ( #T )
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Lst_Chnge_Dte().setValue(pdaIatl420z.getIatl010_Pnd_Sys_Time());                                                            //Natural: ASSIGN #IATA422-LST-CHNGE-DTE := IATL010.#SYS-TIME
        pdaIata422.getPnd_Iata422_Pnd_Iata422_Return_Cd().setValue("C4");                                                                                                 //Natural: ASSIGN #IATA422-RETURN-CD := 'C4'
        //* *LLNAT 'IATN420V' #IATA422                           /* 122308 START
        //*  122308 END
        DbsUtil.callnat(Iatn421v.class , getCurrentProcessState(), pdaIata422.getPnd_Iata422(), pdaIatl420z.getIatl010_Ia_Unique_Id(), ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(pnd_T),  //Natural: CALLNAT 'IATN421V' #IATA422 IATL010.IA-UNIQUE-ID IATL171.IAXFR-FRM-ACCT-CD ( #T ) IATL171.IAXFR-FRM-UNIT-TYP ( #T ) #TO-ACCT-CD ( * )
            ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T), pnd_To_Acct_Cd.getValue("*"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Cref_Fund_From() throws Exception                                                                                                                //Natural: #CREF-FUND-FROM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := IATL010.#IA-FRM-PAYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE := #FROM-FUND-TOT
        ldaIaal420.getVw_iaa_Cref_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-CREF-FUND BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1B",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1B:
        while (condition(ldaIaal420.getVw_iaa_Cref_Fund().readNextRow("R1B")))
        {
            if (condition(ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Cref_Fund_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T).setValue(ldaIaal420.getIaa_Cref_Fund_Cref_Units_Cnt().getValue(1));                  //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T ) := IAA-CREF-FUND.CREF-UNITS-CNT ( 1 )
                ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).setValue(ldaIaal420.getIaa_Cref_Fund_Cref_Tot_Per_Amt());                             //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) := IAA-CREF-FUND.CREF-TOT-PER-AMT
                pnd_W3_Reject_Cde.reset();                                                                                                                                //Natural: RESET #W3-REJECT-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1B;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1B. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Process_Cpr_Info() throws Exception                                                                                                              //Natural: #PROCESS-CPR-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number());                                                             //Natural: ASSIGN #PPCN-NBR := #CONTRACT-NUMBER
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code());                                                                 //Natural: ASSIGN #PAYEE-CDE := #PAYEE-CODE
        ldaIatl420.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseRead                                                                                                      //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R1:
        while (condition(ldaIatl420.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("R1")))
        {
            if (condition(ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().notEquals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number())                //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE #CONTRACT-NUMBER OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE #PAYEE-CDE
                || ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().notEquals(pnd_Cntrct_Payee_Key_Pnd_Payee_Cde)))
            {
                pnd_W3_Reject_Cde.setValue("M2");                                                                                                                         //Natural: MOVE 'M2' TO #W3-REJECT-CDE
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9)))                                                                           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
            {
                pnd_W3_Reject_Cde.setValue("D1");                                                                                                                         //Natural: MOVE 'D1' TO #W3-REJECT-CDE
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Type.reset();                                                                                                                                    //Natural: RESET #CONTRACT-TYPE
            if (condition(ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).equals(" ")))                                                             //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = ' '
            {
                pnd_Contract_Type.setValue("C");                                                                                                                          //Natural: MOVE 'C' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Contract_Type.setValue("T");                                                                                                                          //Natural: MOVE 'T' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: END-IF
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode().setValue(ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                              //Natural: ASSIGN #AIAN013-LINKAGE.#MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date().setValue(ldaIatl420.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                       //Natural: ASSIGN #AIAN013-LINKAGE.#FINAL-PER-PAY-DATE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_From_Fund_Table() throws Exception                                                                                                               //Natural: #FROM-FUND-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  122308
        pnd_Check_Teachers.reset();                                                                                                                                       //Natural: RESET #CHECK-TEACHERS
        F6:                                                                                                                                                               //Natural: FOR #I = 1 TO #MAX-T-OCCURS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_T_Occurs)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals(pnd_T_Table.getValue(pnd_I))))                                                 //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) = #T-TABLE ( #I )
            {
                pnd_Check_Teachers.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #CHECK-TEACHERS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_T_Table_1() throws Exception                                                                                                               //Natural: #CHECK-T-TABLE-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  122308
        pnd_Same_From_Fund.reset();                                                                                                                                       //Natural: RESET #SAME-FROM-FUND
        F7A:                                                                                                                                                              //Natural: FOR #J = 1 TO #MAX-T-OCCURS
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_T_Occurs)); pnd_J.nadd(1))
        {
            //*      WRITE '=' IATA010.XFR-TO-ACCT-CD(#I) '=' #T-TABLE(#J)
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pnd_T_Table.getValue(pnd_J))))                                                  //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = #T-TABLE ( #J )
            {
                pnd_Same_From_Fund.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #SAME-FROM-FUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_C_Table_1() throws Exception                                                                                                               //Natural: #CHECK-C-TABLE-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Same_From_Fund.reset();                                                                                                                                       //Natural: RESET #SAME-FROM-FUND
        F9A:                                                                                                                                                              //Natural: FOR #J = 1 TO 8
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(8)); pnd_J.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pnd_C_Table.getValue(pnd_J))))                                                  //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = #C-TABLE ( #J )
            {
                pnd_Same_From_Fund.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #SAME-FROM-FUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Compare_Same_Fund_1() throws Exception                                                                                                           //Natural: #COMPARE-SAME-FUND-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Check_Teachers.equals("Y")))                                                                                                                    //Natural: IF #CHECK-TEACHERS = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-T-TABLE-1
            sub_Pnd_Check_T_Table_1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-C-TABLE-1
            sub_Pnd_Check_C_Table_1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Load_Tables() throws Exception                                                                                                                   //Natural: #LOAD-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_T_Table.getValue(1).setValue("T");                                                                                                                            //Natural: MOVE 'T' TO #T-TABLE ( 1 )
        pnd_T_Table.getValue(2).setValue("G");                                                                                                                            //Natural: MOVE 'G' TO #T-TABLE ( 2 )
        pnd_T_Table.getValue(3).setValue("R");                                                                                                                            //Natural: MOVE 'R' TO #T-TABLE ( 3 )
        //*  122308
        pnd_T_Table.getValue(4).setValue("D");                                                                                                                            //Natural: MOVE 'D' TO #T-TABLE ( 4 )
        pnd_C_Table.getValue(1).setValue("C");                                                                                                                            //Natural: MOVE 'C' TO #C-TABLE ( 1 )
        pnd_C_Table.getValue(2).setValue("M");                                                                                                                            //Natural: MOVE 'M' TO #C-TABLE ( 2 )
        pnd_C_Table.getValue(3).setValue("S");                                                                                                                            //Natural: MOVE 'S' TO #C-TABLE ( 3 )
        pnd_C_Table.getValue(4).setValue("B");                                                                                                                            //Natural: MOVE 'B' TO #C-TABLE ( 4 )
        pnd_C_Table.getValue(5).setValue("W");                                                                                                                            //Natural: MOVE 'W' TO #C-TABLE ( 5 )
        pnd_C_Table.getValue(6).setValue("L");                                                                                                                            //Natural: MOVE 'L' TO #C-TABLE ( 6 )
        pnd_C_Table.getValue(7).setValue("E");                                                                                                                            //Natural: MOVE 'E' TO #C-TABLE ( 7 )
        pnd_C_Table.getValue(8).setValue("I");                                                                                                                            //Natural: MOVE 'I' TO #C-TABLE ( 8 )
    }
    private void sub_Pnd_Update_Master() throws Exception                                                                                                                 //Natural: #UPDATE-MASTER
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  122308
        ldaIatl420.getPnd_Teachers_To_Teachers().reset();                                                                                                                 //Natural: RESET #TEACHERS-TO-TEACHERS #CREF-TO-CREF #INTRA-FUND #FROM-CREF #FULL-CREF-CONTRACT-OUT #FULL-TIAA-CONTRACT-OUT #FROM-TIAA #FROM-REAL #FROM-REAL-6 #FULL-CONTRACT-OUT #FROM-ACC
        ldaIatl420.getPnd_Cref_To_Cref().reset();
        pnd_Intra_Fund.reset();
        pnd_From_Cref.reset();
        pnd_Full_Cref_Contract_Out.reset();
        pnd_Full_Tiaa_Contract_Out.reset();
        pnd_From_Tiaa.reset();
        pnd_From_Real.reset();
        pnd_From_Real_6.reset();
        pnd_Full_Contract_Out.reset();
        pnd_From_Acc.reset();
        if (condition(pdaIatl420z.getIatl010_Ia_To_Cntrct().equals(" ")))                                                                                                 //Natural: IF IATL010.IA-TO-CNTRCT = ' '
        {
            pnd_Intra_Fund.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #INTRA-FUND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).equals("T") || pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).equals("G")))            //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( 1 ) = 'T' OR = 'G'
        {
            pnd_From_Tiaa.setValue("Y");                                                                                                                                  //Natural: MOVE 'Y' TO #FROM-TIAA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IF IATL010.XFR-FRM-ACCT-CDE(1) NOT = 'R'            /* 122308
            //*  122308
            if (condition(! (pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).equals("R") || pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).equals("D"))))    //Natural: IF NOT IATL010.XFR-FRM-ACCT-CDE ( 1 ) = 'R' OR = 'D'
            {
                pnd_From_Cref.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #FROM-CREF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  122308
                if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).equals("D")))                                                                         //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( 1 ) = 'D'
                {
                    //*  122308
                    pnd_From_Acc.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #FROM-ACC
                    //*  122308
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_From_Real.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #FROM-REAL
                    if (condition(DbsUtil.maskMatches(pdaIatl420z.getIatl010_Ia_Frm_Cntrct(),"'6L7'") || DbsUtil.maskMatches(pdaIatl420z.getIatl010_Ia_Frm_Cntrct(),"'6M7'")  //Natural: IF IATL010.IA-FRM-CNTRCT = MASK ( '6L7' ) OR IATL010.IA-FRM-CNTRCT = MASK ( '6M7' ) OR IATL010.IA-FRM-CNTRCT = MASK ( '6N7' )
                        || DbsUtil.maskMatches(pdaIatl420z.getIatl010_Ia_Frm_Cntrct(),"'6N7'")))
                    {
                        pnd_From_Real_6.setValue("Y");                                                                                                                    //Natural: MOVE 'Y' TO #FROM-REAL-6
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2720 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #INTRA-FUND = 'Y' AND #FROM-CREF = 'Y' ) OR ( IATL010.RCRD-TYPE-CDE = '2' )
        if (condition(((pnd_Intra_Fund.equals("Y") && pnd_From_Cref.equals("Y")) || pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("2"))))
        {
            decideConditionsMet2720++;
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE #FROM-FUND-ONLY ( 1:20 )
            pnd_From_Fund_Only.getValue(1,":",20).reset();
                                                                                                                                                                          //Natural: PERFORM #FROM-FUND-ONLY-PARA
            sub_Pnd_From_Fund_Only_Para();
            if (condition(Global.isEscape())) {return;}
            //*  ADD & UPDATE CREF FUND RECORDS
            //*  UPDATE CONTRACT & CPR RECORD
            //*  CREATE AFTER IMAGES FOR ALL
            //*  123008
            //*  051112
            DbsUtil.callnat(Iatn420g.class , getCurrentProcessState(), ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde(),         //Natural: CALLNAT 'IATN420G' IATL171.IAXFR-FROM-PPCN-NBR IATL171.IAXFR-FROM-PAYEE-CDE IATL171.IAXFR-FROM-FUND-CDE ( 1:20 ) IATL171.IAXFR-FRM-ACCT-CD ( 1:20 ) IATL171.IAXFR-FRM-UNIT-TYP ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( 1:20 ) #RATE-CODE-TABLE ( 1:#MAX-PRODS ) IATL171.IAXFR-TO-FUND-CDE ( 1:20 ) IATL171.IAXFR-TO-ACCT-CD ( 1:20 ) IATL171.IAXFR-TO-UNIT-TYP ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-REVAL-UNIT-VAL ( 1:20 ) #FROM-FUND-ONLY ( 1:20 ) IATL010.RCRD-TYPE-CDE #SAVE-CHECK-DTE IATL010.#TODAYS-DTE #TIME #EFF-DTE-03-31 #W3-REJECT-CDE
                ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(1,":",20), pnd_Rate_Code_Table.getValue(1,":",pnd_Max_Prods), ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(1,":",20), pnd_From_Fund_Only.getValue(1,":",20), 
                pdaIatl420z.getIatl010_Rcrd_Type_Cde(), pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, pdaIatl420z.getIatl010_Pnd_Todays_Dte(), pnd_Time, pnd_Eff_Dte_03_31, 
                pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  122308
                //*  122308
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #INTRA-FUND = 'Y' AND ( #FROM-REAL = 'Y' OR #FROM-ACC = 'Y' )
        else if (condition((pnd_Intra_Fund.equals("Y") && (pnd_From_Real.equals("Y") || pnd_From_Acc.equals("Y")))))
        {
            decideConditionsMet2720++;
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
            //*  FROM REAL TO GRADED/STANDARD
            //*  UPDATE CONTRACT & CPR RECORD
            //*  CREATE AFTER IMAGES FOR ALL
            //*  123008
            DbsUtil.callnat(Iatn420i.class , getCurrentProcessState(), ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde(),         //Natural: CALLNAT 'IATN420I' IATL171.IAXFR-FROM-PPCN-NBR IATL171.IAXFR-FROM-PAYEE-CDE IATL171.IAXFR-FROM-FUND-CDE ( 1 ) IATL171.IAXFR-FRM-ACCT-CD ( 1 ) IATL171.IAXFR-FRM-UNIT-TYP ( 1 ) IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1 ) IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1 ) IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( 1 ) IATL171.IAXFR-CALC-TO-PPCN-NBR IATL171.IAXFR-CALC-TO-PAYEE-CDE #RATE-CODE-TABLE ( 1:#MAX-PRODS ) IATL171.IAXFR-TO-FUND-CDE ( 1:20 ) IATL171.IAXFR-TO-ACCT-CD ( 1:20 ) IATL171.IAXFR-TO-UNIT-TYP ( 1:20 ) IATL171.IAXFR-TO-RATE-CDE ( 1:20 ) IATL171.IAXFR-TO-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-REVAL-UNIT-VAL ( 1:20 ) #SAVE-CHECK-DTE IATL010.#TODAYS-DTE IATL171.IAXFR-EFFCTVE-DTE IATL010.#NEXT-BUS-DTE #NEXT-PAY-DTE #TIME #EFF-DTE-03-31 #W3-REJECT-CDE
                ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(1), ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(1), ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(1), 
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1), ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(1), 
                ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde(), pnd_Rate_Code_Table.getValue(1,":",pnd_Max_Prods), 
                ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(1,":",20), 
                pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, pdaIatl420z.getIatl010_Pnd_Todays_Dte(), ldaIatl171.getIatl171_Iaxfr_Effctve_Dte(), pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte(), 
                pnd_Next_Pay_Dte, pnd_Time, pnd_Eff_Dte_03_31, pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #INTRA-FUND = ' ' AND #FROM-REAL = 'Y' AND #FROM-REAL-6 = 'Y'
        else if (condition(pnd_Intra_Fund.equals(" ") && pnd_From_Real.equals("Y") && pnd_From_Real_6.equals("Y")))
        {
            decideConditionsMet2720++;
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-FULL-6M-REAL-OUT
            sub_Pnd_Determine_Full_6m_Real_Out();
            if (condition(Global.isEscape())) {return;}
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
            //*     WRITE 'IATN420E'
            //*  ADD & UPDATE CREF FUND RECORDS
            //*  UPDATE CONTRACT & CPR RECORD
            //*  CREATE AFTER IMAGES FOR ALL
            //*  123008
            DbsUtil.callnat(Iatn420e.class , getCurrentProcessState(), ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde(),         //Natural: CALLNAT 'IATN420E' IATL171.IAXFR-FROM-PPCN-NBR IATL171.IAXFR-FROM-PAYEE-CDE IATL171.IAXFR-CALC-TO-PPCN-NBR IATL171.IAXFR-CALC-TO-PAYEE-CDE IATL171.IAXFR-FROM-FUND-CDE ( 1:20 ) IATL171.IAXFR-FRM-ACCT-CD ( 1:20 ) IATL171.IAXFR-FRM-UNIT-TYP ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( 1:20 ) #RATE-CODE-TABLE ( 1:#MAX-PRODS ) IATL171.IAXFR-TO-FUND-CDE ( 1:20 ) IATL171.IAXFR-TO-ACCT-CD ( 1:20 ) IATL171.IAXFR-TO-UNIT-TYP ( 1:20 ) IATL171.IAXFR-TO-RATE-CDE ( 1:20 ) IATL171.IAXFR-TO-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-REVAL-UNIT-VAL ( 1:20 ) #IVC-PRO-ADJ #PER-IVC-PRO-ADJ #IVC-IND #IAA-NEW-ISSUE #FULL-CONTRACT-OUT #SAVE-CHECK-DTE IATL010.#TODAYS-DTE IATL171.IAXFR-EFFCTVE-DTE IATL010.#NEXT-BUS-DTE #NEXT-PAY-DTE #TIME #EFF-DTE-03-31 #W3-REJECT-CDE
                ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde(), ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(1,":",20), 
                pnd_Rate_Code_Table.getValue(1,":",pnd_Max_Prods), ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(1,":",20), 
                pnd_Ivc_Pro_Adj, pnd_Per_Ivc_Pro_Adj, pnd_Ivc_Ind, pnd_Iaa_New_Issue, pnd_Full_Contract_Out, pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, pdaIatl420z.getIatl010_Pnd_Todays_Dte(), 
                ldaIatl171.getIatl171_Iaxfr_Effctve_Dte(), pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte(), pnd_Next_Pay_Dte, pnd_Time, pnd_Eff_Dte_03_31, pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  122308
                //*  122308
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #INTRA-FUND = ' ' AND ( #FROM-REAL = 'Y' OR #FROM-ACC = 'Y' ) AND #FROM-REAL-6 = ' '
        else if (condition(((pnd_Intra_Fund.equals(" ") && (pnd_From_Real.equals("Y") || pnd_From_Acc.equals("Y"))) && pnd_From_Real_6.equals(" "))))
        {
            decideConditionsMet2720++;
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-FULL-TIAA-REAL-OUT
            sub_Pnd_Determine_Full_Tiaa_Real_Out();
            if (condition(Global.isEscape())) {return;}
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
            //*  ADD & UPDATE CREF FUND RECORDS
            //*  UPDATE CONTRACT & CPR RECORD
            //*  CREATE AFTER IMAGES FOR ALL
            //*  123008
            DbsUtil.callnat(Iatn420j.class , getCurrentProcessState(), ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde(),         //Natural: CALLNAT 'IATN420J' IATL171.IAXFR-FROM-PPCN-NBR IATL171.IAXFR-FROM-PAYEE-CDE IATL171.IAXFR-CALC-TO-PPCN-NBR IATL171.IAXFR-CALC-TO-PAYEE-CDE IATL171.IAXFR-FROM-FUND-CDE ( 1:20 ) IATL171.IAXFR-FRM-ACCT-CD ( 1:20 ) IATL171.IAXFR-FRM-UNIT-TYP ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( 1:20 ) #RATE-CODE-TABLE ( 1:#MAX-PRODS ) IATL171.IAXFR-TO-FUND-CDE ( 1:20 ) IATL171.IAXFR-TO-ACCT-CD ( 1:20 ) IATL171.IAXFR-TO-UNIT-TYP ( 1:20 ) IATL171.IAXFR-TO-RATE-CDE ( 1:20 ) IATL171.IAXFR-TO-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-REVAL-UNIT-VAL ( 1:20 ) #IVC-PRO-ADJ #PER-IVC-PRO-ADJ #IVC-IND #IAA-NEW-ISSUE #FULL-CONTRACT-OUT #SAVE-CHECK-DTE IATL010.#TODAYS-DTE IATL171.IAXFR-EFFCTVE-DTE IATL010.#NEXT-BUS-DTE #NEXT-PAY-DTE #TIME #EFF-DTE-03-31 #W3-REJECT-CDE
                ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde(), ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(1,":",20), 
                pnd_Rate_Code_Table.getValue(1,":",pnd_Max_Prods), ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(1,":",20), 
                pnd_Ivc_Pro_Adj, pnd_Per_Ivc_Pro_Adj, pnd_Ivc_Ind, pnd_Iaa_New_Issue, pnd_Full_Contract_Out, pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, pdaIatl420z.getIatl010_Pnd_Todays_Dte(), 
                ldaIatl171.getIatl171_Iaxfr_Effctve_Dte(), pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte(), pnd_Next_Pay_Dte, pnd_Time, pnd_Eff_Dte_03_31, pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #INTRA-FUND = ' ' AND #FROM-CREF = 'Y'
        else if (condition(pnd_Intra_Fund.equals(" ") && pnd_From_Cref.equals("Y")))
        {
            decideConditionsMet2720++;
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE #FROM-FUND-ONLY ( 1:20 )
            pnd_From_Fund_Only.getValue(1,":",20).reset();
                                                                                                                                                                          //Natural: PERFORM #FROM-FUND-ONLY-PARA
            sub_Pnd_From_Fund_Only_Para();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_From_On_To_Side.equals(" ")))                                                                                                               //Natural: IF #FROM-ON-TO-SIDE = ' '
            {
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-FULL-CREF-OUT
                sub_Pnd_Determine_Full_Cref_Out();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  ADD & UPDATE CREF FUND RECORDS
            //*  UPDATE CONTRACT & CPR RECORD
            //*  CREATE AFTER IMAGES FOR ALL
            //*  123008
            DbsUtil.callnat(Iatn420h.class , getCurrentProcessState(), ldaIatl171.getIatl171_Iaxfr_From_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_From_Payee_Cde(),         //Natural: CALLNAT 'IATN420H' IATL171.IAXFR-FROM-PPCN-NBR IATL171.IAXFR-FROM-PAYEE-CDE IATL171.IAXFR-CALC-TO-PPCN-NBR IATL171.IAXFR-CALC-TO-PAYEE-CDE #FROM-FUND-ONLY ( 1:20 ) IATL171.IAXFR-FROM-FUND-CDE ( 1:20 ) IATL171.IAXFR-FRM-ACCT-CD ( 1:20 ) IATL171.IAXFR-FRM-UNIT-TYP ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( 1:20 ) #RATE-CODE-TABLE ( 1:#MAX-PRODS ) IATL171.IAXFR-TO-FUND-CDE ( 1:20 ) IATL171.IAXFR-TO-ACCT-CD ( 1:20 ) IATL171.IAXFR-TO-UNIT-TYP ( 1:20 ) IATL171.IAXFR-TO-RATE-CDE ( 1:20 ) IATL171.IAXFR-TO-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-UNITS ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-REVAL-UNIT-VAL ( 1:20 ) #IVC-PRO-ADJ #PER-IVC-PRO-ADJ #IVC-IND #IAA-NEW-ISSUE #IAA-NEW-ISSUE-RE #FULL-CONTRACT-OUT #SAVE-CHECK-DTE IATL010.#TODAYS-DTE IATL171.IAXFR-EFFCTVE-DTE IATL010.#NEXT-BUS-DTE #NEXT-PAY-DTE #TIME #EFF-DTE-03-31 #W3-REJECT-CDE
                ldaIatl171.getIatl171_Iaxfr_Calc_To_Ppcn_Nbr(), ldaIatl171.getIatl171_Iaxfr_Calc_To_Payee_Cde(), pnd_From_Fund_Only.getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_Frm_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(1,":",20), pnd_Rate_Code_Table.getValue(1,":",pnd_Max_Prods), ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(1,":",20), pnd_Ivc_Pro_Adj, pnd_Per_Ivc_Pro_Adj, pnd_Ivc_Ind, pnd_Iaa_New_Issue, 
                pnd_Iaa_New_Issue_Re, pnd_Full_Contract_Out, pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, pdaIatl420z.getIatl010_Pnd_Todays_Dte(), ldaIatl171.getIatl171_Iaxfr_Effctve_Dte(), 
                pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte(), pnd_Next_Pay_Dte, pnd_Time, pnd_Eff_Dte_03_31, pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN IATL010.XFR-FRM-ACCT-CDE ( 1 ) = 'G'
        else if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).equals("G")))
        {
            decideConditionsMet2720++;
            pnd_W3_Reject_Cde.reset();                                                                                                                                    //Natural: RESET #W3-REJECT-CDE
            //*  ADD & UPDATE STANDARD TO
            //*  GRADED RECORDS
            //*  CREATE AFTER IMAGES FOR ALL
            //*  040612 CHANGED FROM :99
            //*  040612 END
            DbsUtil.callnat(Iatn420f.class , getCurrentProcessState(), pdaIatl420z.getIatl010_Ia_Frm_Cntrct(), pdaIatl420z.getIatl010_Ia_Frm_Payee(),                     //Natural: CALLNAT 'IATN420F' IATL010.IA-FRM-CNTRCT IATL010.IA-FRM-PAYEE IATL010.XFR-FRM-ACCT-CDE ( 1 ) IATA002.#RATE-CODE-GRD ( 1:#MAX-RATE ) IATA002.#GTD-PMT-GRD ( 1:#MAX-RATE ) IATA002.#DVD-PMT-GRD ( 1:#MAX-RATE ) IATA002.#RATE-CODE-STD ( 1:#MAX-RATE ) IATA002.#GTD-PMT-STD ( 1:#MAX-RATE ) IATA002.#DVD-PMT-STD ( 1:#MAX-RATE ) IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-FROM-AFTR-XFR-DIVID ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-GUAR ( 1:20 ) IATL171.IAXFR-TO-AFTR-XFR-DIVID ( 1:20 ) #SAVE-CHECK-DTE IATL010.#TODAYS-DTE IATL171.IAXFR-EFFCTVE-DTE IATL010.#NEXT-BUS-DTE #TIME #W3-REJECT-CDE
                pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1), pdaIata002.getIata002_Pnd_Rate_Code_Grd().getValue(1,":",pnd_Max_Rate), pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(1,":",pnd_Max_Rate), 
                pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(1,":",pnd_Max_Rate), pdaIata002.getIata002_Pnd_Rate_Code_Std().getValue(1,":",pnd_Max_Rate), 
                pdaIata002.getIata002_Pnd_Gtd_Pmt_Std().getValue(1,":",pnd_Max_Rate), pdaIata002.getIata002_Pnd_Dvd_Pmt_Std().getValue(1,":",pnd_Max_Rate), 
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Divid().getValue(1,":",20), 
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(1,":",20), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Divid().getValue(1,":",20), pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte, 
                pdaIatl420z.getIatl010_Pnd_Todays_Dte(), ldaIatl171.getIatl171_Iaxfr_Effctve_Dte(), pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte(), pnd_Time, 
                pnd_W3_Reject_Cde);
            if (condition(Global.isEscape())) return;
            if (condition(pnd_W3_Reject_Cde.notEquals(" ")))                                                                                                              //Natural: IF #W3-REJECT-CDE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Fill_Control_File_Fields() throws Exception                                                                                                      //Natural: #FILL-CONTROL-FILE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Iaa_New_Issue.equals("Y") && pdaIatl420z.getIatl010_Ia_To_Cntrct().notEquals(" ")))                                                             //Natural: IF #IAA-NEW-ISSUE = 'Y' AND IATL010.IA-TO-CNTRCT NOT = ' '
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).equals("R") && DbsUtil.maskMatches(pdaIatl420z.getIatl010_Ia_To_Cntrct(),                 //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( 1 ) = 'R' AND IATL010.IA-TO-CNTRCT = MASK ( N )
                "N")))
            {
                pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Cref_Payee().setValue("Y");                                                                                        //Natural: MOVE 'Y' TO #IATN420X-IN.#NEW-CREF-PAYEE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Tiaa_Payee().setValue("Y");                                                                                        //Natural: MOVE 'Y' TO #IATN420X-IN.#NEW-TIAA-PAYEE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*     PERFORM #CONVERT-CTL-FROM-FUND
    }
    private void sub_Pnd_Filling_To_Side() throws Exception                                                                                                               //Natural: #FILLING-TO-SIDE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
                                                                                                                                                                          //Natural: PERFORM #FROM-FUND-TABLE
        sub_Pnd_From_Fund_Table();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaIatl171.getIatl171_Rcrd_Type_Cde().equals("2")))                                                                                                 //Natural: IF IATL171.RCRD-TYPE-CDE = '2'
        {
            pnd_Var3.setValue(pnd_T);                                                                                                                                     //Natural: ASSIGN #VAR3 := #T
            pnd_Var4.setValue(pnd_T);                                                                                                                                     //Natural: ASSIGN #VAR4 := #T
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Var3.setValue(1);                                                                                                                                         //Natural: ASSIGN #VAR3 := 1
            pnd_Var4.setValue(20);                                                                                                                                        //Natural: ASSIGN #VAR4 := 20
        }                                                                                                                                                                 //Natural: END-IF
        FS:                                                                                                                                                               //Natural: FOR #I = #VAR3 TO #VAR4
        for (pnd_I.setValue(pnd_Var3); condition(pnd_I.lessOrEqual(pnd_Var4)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(" ")))                                                                          //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = ' '
            {
                if (true) break FS;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FS. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ctl_To_Fund_Hold.setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                                                                  //Natural: MOVE IATL010.XFR-TO-ACCT-CDE ( #I ) TO #CTL-TO-FUND-HOLD
                //* *  PERFORM #CONVERT-CTL-TO-FUND                 /* 123008
                if (condition(pdaIatl420z.getIatl010_Ia_To_Cntrct().equals(" ")))                                                                                         //Natural: IF IATL010.IA-TO-CNTRCT = ' '
                {
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                           //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = IATL010.IA-FRM-CNTRCT
                    pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                         //Natural: ASSIGN #W-CNTRCT-PAYEE = IATL010.#IA-FRM-PAYEE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #COMPARE-SAME-FUND-1
                    sub_Pnd_Compare_Same_Fund_1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Same_From_Fund.equals("Y")))                                                                                                        //Natural: IF #SAME-FROM-FUND = 'Y'
                    {
                        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = IATL010.IA-FRM-CNTRCT
                        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE = IATL010.#IA-FRM-PAYEE-N
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                        //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = IATL010.IA-TO-CNTRCT
                        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_To_Payee_N());                                                      //Natural: ASSIGN #W-CNTRCT-PAYEE = IATL010.#IA-TO-PAYEE-N
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).setValue(0);                                                                               //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-UNITS ( #I ) := 0
                pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                                                                     //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-TO-ACCT-CDE ( #I )
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
                sub_Pnd_Convert_One_Byte_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                                      //Natural: IF IATL010.XFR-TO-UNIT-TYP ( #I ) = 'A'
                {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
                    sub_Pnd_Annual_Fund_Cnv();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
                    sub_Pnd_Monthly_Cnv_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #CHECK-NEW-FUND
                sub_Pnd_Check_New_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_On_File.equals("Y")))                                                                                                                   //Natural: IF #ON-FILE = 'Y'
                {
                    ldaIatl171.getIatl171_Iaxfr_To_New_Fund_Rec().getValue(pnd_I).setValue(" ");                                                                          //Natural: ASSIGN IATL171.IAXFR-TO-NEW-FUND-REC ( #I ) := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl171.getIatl171_Iaxfr_To_New_Fund_Rec().getValue(pnd_I).setValue("Y");                                                                          //Natural: ASSIGN IATL171.IAXFR-TO-NEW-FUND-REC ( #I ) := 'Y'
                }                                                                                                                                                         //Natural: END-IF
                pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                                                                     //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-TO-ACCT-CDE ( #I )
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
                sub_Pnd_Convert_One_Byte_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_I).setValue(pnd_Two_Byte_Fund);                                                                    //Natural: ASSIGN IATL171.IAXFR-TO-FUND-CDE ( #I ) := #TWO-BYTE-FUND
                ldaIatl171.getIatl171_Iaxfr_To_Acct_Cd().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                              //Natural: ASSIGN IATL171.IAXFR-TO-ACCT-CD ( #I ) := IATL010.XFR-TO-ACCT-CDE ( #I )
                ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_I));                             //Natural: ASSIGN IATL171.IAXFR-TO-UNIT-TYP ( #I ) := IATL010.XFR-TO-UNIT-TYP ( #I )
                ldaIatl171.getIatl171_Iaxfr_To_Typ().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Typ().getValue(pnd_I));                                       //Natural: ASSIGN IATL171.IAXFR-TO-TYP ( #I ) := IATL010.XFR-TO-TYP ( #I )
                ldaIatl171.getIatl171_Iaxfr_To_Qty().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I));                                       //Natural: ASSIGN IATL171.IAXFR-TO-QTY ( #I ) := IATL010.XFR-TO-QTY ( #I )
                //*  062810
                short decideConditionsMet2868 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IATL010.XFR-TO-ACCT-CDE ( #I ) = 'T'
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("T")))
                {
                    decideConditionsMet2868++;
                    ldaIatl420.getPnd_Teachers_Fund_Conv().setValue("T");                                                                                                 //Natural: ASSIGN #TEACHERS-FUND-CONV := 'T'
                    //*  062810
                                                                                                                                                                          //Natural: PERFORM #CHECK-TEACHER-LINKAGE
                    sub_Pnd_Check_Teacher_Linkage();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G'
                else if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))
                {
                    decideConditionsMet2868++;
                    ldaIatl420.getPnd_Teachers_Fund_Conv().setValue("2");                                                                                                 //Natural: ASSIGN #TEACHERS-FUND-CONV := '2'
                                                                                                                                                                          //Natural: PERFORM #CHECK-TEACHER-LINKAGE
                    sub_Pnd_Check_Teacher_Linkage();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #CHECK-UNITS-LINKAGE
                    sub_Pnd_Check_Units_Linkage();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Units_Linkage() throws Exception                                                                                                           //Natural: #CHECK-UNITS-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        FQ:                                                                                                                                                               //Natural: FOR #J = 1 TO 19
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(19)); pnd_J.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_J))))       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = #FUND-CODE-OUT ( #J )
            {
                ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_J));               //Natural: ASSIGN IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #I ) := #AUV-OUT ( #J )
                if (condition((pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I)))                //Natural: IF ( IATL010.XFR-FRM-ACCT-CDE ( #T ) = IATL010.XFR-TO-ACCT-CDE ( #I ) ) AND ( IATL010.XFR-FRM-UNIT-TYP ( #T ) = IATL010.XFR-TO-UNIT-TYP ( #I ) )
                    && (pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T).equals(pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_I)))))
                {
                    pnd_Same_Fund_Units.getValue(pnd_I).setValue(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Units().getValue(pnd_T));                                     //Natural: MOVE IATL171.IAXFR-FROM-RQSTD-XFR-UNITS ( #T ) TO #SAME-FUND-UNITS ( #I )
                    pnd_Same_Fund_Dollars.getValue(pnd_I).setValue(ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T));                                    //Natural: MOVE IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T ) TO #SAME-FUND-DOLLARS ( #I )
                }                                                                                                                                                         //Natural: END-IF
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Units().getValue(pnd_I).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_J));                      //Natural: ADD #UNITS-OUT ( #J ) TO IATL171.IAXFR-TO-XFR-UNITS ( #I )
                ldaIatl171.getIatl171_Iaxfr_To_Asset_Amt().getValue(pnd_I).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_J));          //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #J ) TO IATL171.IAXFR-TO-ASSET-AMT ( #I )
                pnd_To_Asset_Amt.getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_J));                                //Natural: ASSIGN #TO-ASSET-AMT ( #I ) := #TRANSFER-AMT-OUT-CREF ( #J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F:                                                                                                                                                                //Natural: FOR #O = 1 TO 21
        for (pnd_O.setValue(1); condition(pnd_O.lessOrEqual(21)); pnd_O.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_O))))     //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = #FUND-TO-RECEIVE ( #O )
            {
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_I).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_O));                 //Natural: ADD #PMTS-TO-RECEIVE ( #O ) TO IATL171.IAXFR-TO-XFR-GUAR ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Teacher_Linkage() throws Exception                                                                                                         //Natural: #CHECK-TEACHER-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(1).notEquals("G")))                                                                              //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( 1 ) NOT = 'G'
        {
            FQS:                                                                                                                                                          //Natural: FOR #J = 1 TO 2
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(2)); pnd_J.nadd(1))
            {
                if (condition(ldaIatl420.getPnd_Teachers_Fund_Conv().equals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(pnd_J))))               //Natural: IF #TEACHERS-FUND-CONV = #PMT-METHOD-CODE-OUT ( #J )
                {
                    ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_I).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(pnd_J));                 //Natural: ADD #GTD-PMT-OUT ( #J ) TO IATL171.IAXFR-TO-XFR-GUAR ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(pnd_I).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(pnd_J));                //Natural: ADD #DVD-PMT-OUT ( #J ) TO IATL171.IAXFR-TO-XFR-DIVID ( #I )
                    ldaIatl171.getIatl171_Iaxfr_To_Rate_Cde().getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(pnd_J));           //Natural: ASSIGN IATL171.IAXFR-TO-RATE-CDE ( #I ) := #RATE-CODE-OUT ( #J )
                    ldaIatl171.getIatl171_Iaxfr_To_Asset_Amt().getValue(pnd_I).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_J));      //Natural: ADD #TRANSFER-AMT-OUT-TIAA ( #J ) TO IATL171.IAXFR-TO-ASSET-AMT ( #I )
                    pnd_To_Asset_Amt.getValue(pnd_I).setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(pnd_J));                            //Natural: ASSIGN #TO-ASSET-AMT ( #I ) := #TRANSFER-AMT-OUT-TIAA ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_I).nadd(pdaIata002.getIata002_Pnd_Gtd_Pmt_Std().getValue("*"));                                        //Natural: ADD IATA002.#GTD-PMT-STD ( * ) TO IATL171.IAXFR-TO-XFR-GUAR ( #I )
            //*  122308
            ldaIatl171.getIatl171_Iaxfr_To_Xfr_Divid().getValue(pnd_I).nadd(pdaIata002.getIata002_Pnd_Dvd_Pmt_Std().getValue("*"));                                       //Natural: ADD IATA002.#DVD-PMT-STD ( * ) TO IATL171.IAXFR-TO-XFR-DIVID ( #I )
            pnd_To_Asset_Amt.getValue(pnd_I).setValue(pdaIata002.getIata002_Pnd_Tot_Trnsfr_Amt());                                                                        //Natural: ASSIGN #TO-ASSET-AMT ( #I ) := IATA002.#TOT-TRNSFR-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_One_Byte_Fund.setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));                                                                             //Natural: ASSIGN #ONE-BYTE-FUND := IATL010.XFR-TO-ACCT-CDE ( #I )
                                                                                                                                                                          //Natural: PERFORM #CONVERT-ONE-BYTE-FUND
        sub_Pnd_Convert_One_Byte_Fund();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                                             //Natural: IF IATL171.IAXFR-TO-UNIT-TYP ( #I ) = 'A'
        {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
            sub_Pnd_Annual_Fund_Cnv();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
            sub_Pnd_Monthly_Cnv_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  122308
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("R") || pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("G")      //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) = 'R' OR = 'G' OR = 'D'
            || pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("D")))
        {
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                   //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = IATL010.IA-FRM-CNTRCT
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                                 //Natural: ASSIGN #W-CNTRCT-PAYEE = IATL010.#IA-FRM-PAYEE-N
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_To_Cntrct());                                                                    //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = IATL010.IA-TO-CNTRCT
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_To_Payee_N());                                                                  //Natural: ASSIGN #W-CNTRCT-PAYEE = IATL010.#IA-TO-PAYEE-N
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1H",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1H:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1H")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_I).setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Per_Amt());                                   //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-GUAR ( #I ) := IAA-TIAA-FUND.TIAA-TOT-PER-AMT
                ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Divid().getValue(pnd_I).setValue(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Tot_Div_Amt());                                  //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-DIVID ( #I ) := IAA-TIAA-FUND.TIAA-TOT-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Load_Externalize_Table_Codes() throws Exception                                                                                                  //Natural: #LOAD-EXTERNALIZE-TABLE-CODES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_G.reset();                                                                                                                                                    //Natural: RESET #G #GG #GGG
        pnd_Gg.reset();
        pnd_Ggg.reset();
        //*  CALLNAT 'IAAN050F' #W-ALPHA-CDE(1:40) #W-RATE-CDE(1:40) #W-NM-CD(1:40)
        //*  #W-IA-STD-ALPHA-CD(1:40)
        pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Nm_Cd().getValue(1,":",80).reset();                                                                                            //Natural: RESET #IA-RPT-NM-CD ( 1:80 ) #IA-RPT-CMPNY-CD-A ( 1:80 )
        pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Cmpny_Cd_A().getValue(1,":",80).reset();
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_W_Alpha_Cde.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue("*"));                                                              //Natural: ASSIGN #W-ALPHA-CDE ( * ) := IAAA051Z.#IA-STD-ALPHA-CD ( * )
        pnd_W_Ia_Std_Alpha_Cd.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue("*"));                                                        //Natural: ASSIGN #W-IA-STD-ALPHA-CD ( * ) := IAAA051Z.#IA-STD-ALPHA-CD ( * )
        pnd_W_Nm_Cd.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"));                                                                     //Natural: ASSIGN #W-NM-CD ( * ) := IAAA051Z.#IA-STD-NM-CD ( * )
        pnd_W_Ia_Rpt_Cmpny_Cd_A.getValue("*").setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Cmpny_Cd_A().getValue("*"));                                                    //Natural: ASSIGN #W-IA-RPT-CMPNY-CD-A ( * ) := IAAA051Z.#IA-RPT-CMPNY-CD-A ( * )
        FOR01:                                                                                                                                                            //Natural: FOR #F 1 #MAX-PRODS
        for (pnd_F.setValue(1); condition(pnd_F.lessOrEqual(pnd_Max_Prods)); pnd_F.nadd(1))
        {
            if (condition(pnd_W_Alpha_Cde.getValue(pnd_F).equals(" ")))                                                                                                   //Natural: IF #W-ALPHA-CDE ( #F ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Rate_Cde.getValue(pnd_F).setValueEdited(pdaIaaa051z.getIaaa051z_Pnd_V2_Rt_1().getValue(pnd_F),new ReportEditMask("99"));                                //Natural: MOVE EDITED IAAA051Z.#V2-RT-1 ( #F ) ( EM = 99 ) TO #W-RATE-CDE ( #F )
            if (condition((pnd_W_Rate_Cde.getValue(pnd_F).equals("00") && ! ((pnd_W_Alpha_Cde.getValue(pnd_F).equals("T") || pnd_W_Alpha_Cde.getValue(pnd_F).equals("G")))))) //Natural: IF #W-RATE-CDE ( #F ) = '00' AND NOT #W-ALPHA-CDE ( #F ) = 'T' OR = 'G'
            {
                DbsUtil.callnat(Iatn421.class , getCurrentProcessState(), pnd_W_Nm_Cd.getValue(pnd_F), pnd_W_Rate_Cde.getValue(pnd_F));                                   //Natural: CALLNAT 'IATN421' #W-NM-CD ( #F ) #W-RATE-CDE ( #F )
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*                                                      /* 122308 END
        getReports().write(0, "=",pnd_W_Alpha_Cde.getValue(1,":",40));                                                                                                    //Natural: WRITE '=' #W-ALPHA-CDE ( 1:40 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W_Ia_Std_Alpha_Cd.getValue(1,":",40));                                                                                              //Natural: WRITE '=' #W-IA-STD-ALPHA-CD ( 1:40 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W_Nm_Cd.getValue(1,":",40));                                                                                                        //Natural: WRITE '=' #W-NM-CD ( 1:40 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W_Ia_Rpt_Cmpny_Cd_A.getValue(1,":",40));                                                                                            //Natural: WRITE '=' #W-IA-RPT-CMPNY-CD-A ( 1:40 )
        if (Global.isEscape()) return;
        //*  122308
        getReports().write(0, "=",pnd_W_Rate_Cde.getValue(1,":",40));                                                                                                     //Natural: WRITE '=' #W-RATE-CDE ( 1:40 )
        if (Global.isEscape()) return;
        FFF:                                                                                                                                                              //Natural: FOR #F = 1 TO #MAX-PRODS
        for (pnd_F.setValue(1); condition(pnd_F.lessOrEqual(pnd_Max_Prods)); pnd_F.nadd(1))
        {
            if (condition(pnd_W_Rate_Cde.getValue(pnd_F).notEquals(" ")))                                                                                                 //Natural: IF #W-RATE-CDE ( #F ) NE ' '
            {
                pnd_G.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #G
                pnd_Rate_Code_Table.getValue(pnd_G).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Alpha_Cde.getValue(pnd_F), pnd_W_Rate_Cde.getValue(pnd_F))); //Natural: COMPRESS #W-ALPHA-CDE ( #F ) #W-RATE-CDE ( #F ) INTO #RATE-CODE-TABLE ( #G ) LEAVING NO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W_Nm_Cd.getValue(pnd_F).equals("07")))                                                                                                      //Natural: IF #W-NM-CD ( #F ) = '07'
            {
                pnd_W_Nm_Cd.getValue(pnd_F).setValue("08");                                                                                                               //Natural: MOVE '08' TO #W-NM-CD ( #F )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_W_Nm_Cd.getValue(pnd_F).equals("08")))                                                                                                  //Natural: IF #W-NM-CD ( #F ) = '08'
                {
                    pnd_W_Nm_Cd.getValue(pnd_F).setValue("07");                                                                                                           //Natural: MOVE '07' TO #W-NM-CD ( #F )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W_Nm_Cd.getValue(pnd_F).equals("1 ")))                                                                                                      //Natural: IF #W-NM-CD ( #F ) = '1 '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Gg.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #GG
                pnd_Ctl_Fund_Cde.getValue(pnd_Gg).setValue(pnd_W_Nm_Cd.getValue(pnd_F));                                                                                  //Natural: MOVE #W-NM-CD ( #F ) TO #CTL-FUND-CDE ( #GG )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_F.greater(1)))                                                                                                                              //Natural: IF #F > 1
            {
                pnd_Ggg.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #GGG
                if (condition(pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).equals("L")))                                                                                         //Natural: IF #W-IA-STD-ALPHA-CD ( #F ) = 'L'
                {
                    pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).setValue("E");                                                                                                  //Natural: MOVE 'E' TO #W-IA-STD-ALPHA-CD ( #F )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).equals("E")))                                                                                     //Natural: IF #W-IA-STD-ALPHA-CD ( #F ) = 'E'
                    {
                        pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F).setValue("L");                                                                                              //Natural: MOVE 'L' TO #W-IA-STD-ALPHA-CD ( #F )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Alpha_Cd_Table.getValue(pnd_Ggg).setValue(pnd_W_Ia_Std_Alpha_Cd.getValue(pnd_F));                                                                     //Natural: MOVE #W-IA-STD-ALPHA-CD ( #F ) TO #ALPHA-CD-TABLE ( #GGG )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Aian013_Linkage() throws Exception                                                                                                          //Natural: #FILL-AIAN013-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //* * #TRANSFER-UNITS := IATA010.XFR-FRM-UNITS(1)
        pnd_Todays_Bus_Date_A.setValueEdited(pdaIatl420z.getIatl010_Pnd_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED IATL010.#TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-BUS-DATE-A
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date().setValue(pnd_Todays_Bus_Date_A_Pnd_Todays_Bus_Date);                                                     //Natural: ASSIGN #AIAN013-LINKAGE.#PROCESSING-DATE := #TODAYS-BUS-DATE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().setValue(pnd_Eff_Date_Pnd_Eff_Date_N);                                                             //Natural: ASSIGN #ILLUSTRATION-EFF-DATE := #EFF-DATE-N
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().setValue(pnd_Eff_Date_Pnd_Eff_Date_N);                                                           //Natural: ASSIGN #AIAN013-LINKAGE.#TRANSFER-EFFECTIVE-DATE := #EFF-DATE-N
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().setValue("A");                                                                                               //Natural: ASSIGN #AIAN013-LINKAGE.#TYPE-OF-RUN := 'A'
        short decideConditionsMet3028 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IATL010.RCRD-TYPE-CDE = '1'
        if (condition(pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("1")))
        {
            decideConditionsMet3028++;
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue("0");                                                                                 //Natural: ASSIGN #TRANSFER-REVAL-SWITCH := '0'
        }                                                                                                                                                                 //Natural: WHEN IATL010.RCRD-TYPE-CDE = '2' AND IATL010.XFR-FRM-UNIT-TYP ( 1 ) = 'M'
        else if (condition(pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("2") && pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(1).equals("M")))
        {
            decideConditionsMet3028++;
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue("1");                                                                                 //Natural: ASSIGN #TRANSFER-REVAL-SWITCH := '1'
        }                                                                                                                                                                 //Natural: WHEN IATL010.RCRD-TYPE-CDE = '2' AND IATL010.XFR-FRM-UNIT-TYP ( 1 ) = 'A'
        else if (condition(pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("2") && pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(1).equals("A")))
        {
            decideConditionsMet3028++;
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue("2");                                                                                 //Natural: ASSIGN #TRANSFER-REVAL-SWITCH := '2'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Fill_Rest_Of_Aian013_Linkage() throws Exception                                                                                                  //Natural: #FILL-REST-OF-AIAN013-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  FILLING FIELDS #TRANSFER-UNITS AND  #TRANSFER-TO-ARRAYS ON N013 LINKAGE
        pnd_To_Qty_Total.reset();                                                                                                                                         //Natural: RESET #TO-QTY-TOTAL
        pnd_Tiaa_Units_Cnt.setValue(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T));                                                                //Natural: ASSIGN #TIAA-UNITS-CNT := IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T )
        pnd_Tiaa_Fund_Pymt.setValue(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T));                                                                 //Natural: ASSIGN #TIAA-FUND-PYMT := IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T )
        pnd_To_Qty_Total.nadd(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue("*"));                                                                                         //Natural: ADD IATL010.XFR-TO-QTY ( * ) TO #TO-QTY-TOTAL
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T).equals("M")))                                                                             //Natural: IF IATL010.XFR-FRM-UNIT-TYP ( #T ) = 'M'
        {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-TRNSFR-UNITS
            sub_Pnd_Monthly_Trnsfr_Units();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIatl171.getIatl171_Rcrd_Type_Cde().equals("2")))                                                                                             //Natural: IF IATL171.RCRD-TYPE-CDE = '2'
            {
                pnd_Var1.setValue(pnd_T);                                                                                                                                 //Natural: ASSIGN #VAR1 := #T
                pnd_Var2.setValue(pnd_T);                                                                                                                                 //Natural: ASSIGN #VAR2 := #T
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-TRANSFER-TO-ARRAYS-SWITCH
                sub_Pnd_Monthly_Transfer_To_Arrays_Switch();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Var1.setValue(1);                                                                                                                                     //Natural: ASSIGN #VAR1 := 1
                pnd_Var2.setValue(20);                                                                                                                                    //Natural: ASSIGN #VAR2 := 20
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-TRANSFER-TO-ARRAYS
                sub_Pnd_Monthly_Transfer_To_Arrays();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #ROUNDING-UNITS
                sub_Pnd_Rounding_Units();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-TRNSFR-UNITS-AND-DOLLARS
            sub_Pnd_Annual_Trnsfr_Units_And_Dollars();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIatl171.getIatl171_Rcrd_Type_Cde().equals("2")))                                                                                             //Natural: IF IATL171.RCRD-TYPE-CDE = '2'
            {
                pnd_Var1.setValue(pnd_T);                                                                                                                                 //Natural: ASSIGN #VAR1 := #T
                pnd_Var2.setValue(pnd_T);                                                                                                                                 //Natural: ASSIGN #VAR2 := #T
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-TRANSFER-TO-ARRAYS-SWITCH
                sub_Pnd_Annual_Transfer_To_Arrays_Switch();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Var1.setValue(1);                                                                                                                                     //Natural: ASSIGN #VAR1 := 1
                pnd_Var2.setValue(20);                                                                                                                                    //Natural: ASSIGN #VAR2 := 20
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-TRANSFER-TO-ARRAYS
                sub_Pnd_Annual_Transfer_To_Arrays();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #ROUNDING-UNITS
                sub_Pnd_Rounding_Units();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #ROUNDING-DOLLARS
                sub_Pnd_Rounding_Dollars();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*      WRITE '=' #TRANSFER-UNITS '=' #TO-UNITS
    }
    private void sub_Pnd_Check_New_Fund() throws Exception                                                                                                                //Natural: #CHECK-NEW-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("T") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))      //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'T' OR = 'G'
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-TIAA-NEW-FUND
            sub_Pnd_Check_Tiaa_New_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-CREF-NEW-FUND
            sub_Pnd_Check_Cref_New_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_Cref_New_Fund() throws Exception                                                                                                           //Natural: #CHECK-CREF-NEW-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File.reset();                                                                                                                                              //Natural: RESET #ON-FILE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIaal420.getVw_iaa_Cref_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-CREF-FUND BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Y",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1Y:
        while (condition(ldaIaal420.getVw_iaa_Cref_Fund().readNextRow("R1Y")))
        {
            //*       WRITE '=' CREF-CNTRCT-PPCN-NBR '=' CREF-CMPNY-FUND-CDE
            if (condition(ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Cref_Fund_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_On_File.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #ON-FILE
                ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_I).setValue(ldaIaal420.getIaa_Cref_Fund_Cref_Units_Cnt().getValue(1));                        //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-UNITS ( #I ) := IAA-CREF-FUND.CREF-UNITS-CNT ( 1 )
                ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_I).setValue(ldaIaal420.getIaa_Cref_Fund_Cref_Tot_Per_Amt());                                   //Natural: ASSIGN IATL171.IAXFR-TO-BFR-XFR-GUAR ( #I ) := IAA-CREF-FUND.CREF-TOT-PER-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Tiaa_New_Fund() throws Exception                                                                                                           //Natural: #CHECK-TIAA-NEW-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_On_File.reset();                                                                                                                                              //Natural: RESET #ON-FILE
        //*  ASSIGN #W-CNTRCT-PPCN-NBR = IATA010.IAA-TO-CNTRCT
        //*  ASSIGN #W-CNTRCT-PAYEE = IATA010.#IAA-TO-PYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE = #FROM-FUND-TOT
        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1Q",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R1Q:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R1Q")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_On_File.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #ON-FILE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Aian014_Linkage() throws Exception                                                                                                          //Natural: #FILL-AIAN014-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_W_Gtd_Pmt.getValue("*").reset();                                                                                                                              //Natural: RESET #W-GTD-PMT ( * ) #W-DVD-PMT ( * ) #TO-GTD #TO-DIV #J #W-DIV #W-GTD #D-DIV #D-GTD #E-DIV #E-GTD #XFR-FRM-PER-AMT #XFR-FRM-DIV-AMT
        pnd_W_Dvd_Pmt.getValue("*").reset();
        pnd_To_Gtd.reset();
        pnd_To_Div.reset();
        pnd_J.reset();
        pnd_W_Div.reset();
        pnd_W_Gtd.reset();
        pnd_D_Div.reset();
        pnd_D_Gtd.reset();
        pnd_E_Div.reset();
        pnd_E_Gtd.reset();
        pnd_Xfr_Frm_Per_Amt.reset();
        pnd_Xfr_Frm_Div_Amt.reset();
        pdaIata002.getIata002().setValuesByName(pdaAial0130.getPnd_Aian013_Linkage());                                                                                    //Natural: MOVE BY NAME #AIAN013-LINKAGE TO IATA002
        pnd_Curr_Frm_Amt.compute(new ComputeParameters(false, pnd_Curr_Frm_Amt), ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).add(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T))); //Natural: COMPUTE #CURR-FRM-AMT = IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) + IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T )
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("P")))                                                                                  //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'P'
        {
            pnd_Xfr_Frm_Per_Amt.compute(new ComputeParameters(true, pnd_Xfr_Frm_Per_Amt), ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #XFR-FRM-PER-AMT = IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) * IATL010.XFR-FRM-QTY ( #T ) / 100
            pnd_Xfr_Frm_Div_Amt.compute(new ComputeParameters(true, pnd_Xfr_Frm_Div_Amt), ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #XFR-FRM-DIV-AMT = IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T ) * IATL010.XFR-FRM-QTY ( #T ) / 100
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("D")))                                                                                  //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'D'
        {
            pnd_Xfr_Frm_Per_Amt.compute(new ComputeParameters(true, pnd_Xfr_Frm_Per_Amt), ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(pnd_Curr_Frm_Amt)); //Natural: COMPUTE ROUNDED #XFR-FRM-PER-AMT = IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) * IATL010.XFR-FRM-QTY ( #T ) / #CURR-FRM-AMT
            pnd_Xfr_Frm_Div_Amt.compute(new ComputeParameters(true, pnd_Xfr_Frm_Div_Amt), ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(pnd_Curr_Frm_Amt)); //Natural: COMPUTE ROUNDED #XFR-FRM-DIV-AMT = IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T ) * IATL010.XFR-FRM-QTY ( #T ) / #CURR-FRM-AMT
            //*  040612
        }                                                                                                                                                                 //Natural: END-IF
        pnd_To_Gtd.setValue(pnd_Xfr_Frm_Per_Amt);                                                                                                                         //Natural: ASSIGN #TO-GTD := #XFR-FRM-PER-AMT
        pnd_To_Div.setValue(pnd_Xfr_Frm_Div_Amt);                                                                                                                         //Natural: ASSIGN #TO-DIV := #XFR-FRM-DIV-AMT
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO #MAX-RATE
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rate)); pnd_I.nadd(1))
        {
            if (condition(pnd_Tiaa_Rates_Pnd_Rate_Code.getValue(pnd_I).greater(" ")))                                                                                     //Natural: IF #TIAA-RATES.#RATE-CODE ( #I ) GT ' '
            {
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("P")))                                                                          //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'P'
                {
                    pnd_W_Gtd_Pmt.getValue(pnd_I).compute(new ComputeParameters(true, pnd_W_Gtd_Pmt.getValue(pnd_I)), pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #W-GTD-PMT ( #I ) = #TIAA-RATES.#GTD-PMT ( #I ) * IATL010.XFR-FRM-QTY ( #T ) / 100
                    pnd_W_Dvd_Pmt.getValue(pnd_I).compute(new ComputeParameters(true, pnd_W_Dvd_Pmt.getValue(pnd_I)), pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #W-DVD-PMT ( #I ) = #TIAA-RATES.#DVD-PMT ( #I ) * IATL010.XFR-FRM-QTY ( #T ) / 100
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Gtd_Pmt.getValue(pnd_I).compute(new ComputeParameters(true, pnd_W_Gtd_Pmt.getValue(pnd_I)), pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(pnd_Curr_Frm_Amt)); //Natural: COMPUTE ROUNDED #W-GTD-PMT ( #I ) = #TIAA-RATES.#GTD-PMT ( #I ) * IATL010.XFR-FRM-QTY ( #T ) / #CURR-FRM-AMT
                    pnd_W_Dvd_Pmt.getValue(pnd_I).compute(new ComputeParameters(true, pnd_W_Dvd_Pmt.getValue(pnd_I)), pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(pnd_Curr_Frm_Amt)); //Natural: COMPUTE ROUNDED #W-DVD-PMT ( #I ) = #TIAA-RATES.#DVD-PMT ( #I ) * IATL010.XFR-FRM-QTY ( #T ) / #CURR-FRM-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Typ().getValue(pnd_T).equals("P")))                                                                           //Natural: IF IATL010.XFR-TO-TYP ( #T ) = 'P'
                {
                    pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_I).compute(new ComputeParameters(true, pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_I)), //Natural: COMPUTE ROUNDED IATA002.#GTD-PMT-GRD ( #I ) = #W-GTD-PMT ( #I ) * IATL010.XFR-TO-QTY ( #T ) / 100
                        pnd_W_Gtd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_T)).divide(100));
                    pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_I).compute(new ComputeParameters(true, pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_I)), //Natural: COMPUTE ROUNDED IATA002.#DVD-PMT-GRD ( #I ) = #W-DVD-PMT ( #I ) * IATL010.XFR-TO-QTY ( #T ) / 100
                        pnd_W_Dvd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_T)).divide(100));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_I).compute(new ComputeParameters(true, pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_I)), //Natural: COMPUTE ROUNDED IATA002.#GTD-PMT-GRD ( #I ) = #W-GTD-PMT ( #I ) * IATL010.XFR-TO-QTY ( #T ) / #CURR-FRM-AMT
                        pnd_W_Gtd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_T)).divide(pnd_Curr_Frm_Amt));
                    pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_I).compute(new ComputeParameters(true, pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_I)), //Natural: COMPUTE ROUNDED IATA002.#DVD-PMT-GRD ( #I ) = #W-DVD-PMT ( #I ) * IATL010.XFR-TO-QTY ( #T ) / #CURR-FRM-AMT
                        pnd_W_Dvd_Pmt.getValue(pnd_I).multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_T)).divide(pnd_Curr_Frm_Amt));
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Div.nadd(pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_I));                                                                                  //Natural: ADD IATA002.#DVD-PMT-GRD ( #I ) TO #W-DIV
                pnd_W_Gtd.nadd(pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_I));                                                                                  //Natural: ADD IATA002.#GTD-PMT-GRD ( #I ) TO #W-GTD
                pdaIata002.getIata002_Pnd_Rate_Code_Grd().getValue(pnd_I).setValue(pnd_Tiaa_Rates_Pnd_Rate_Code.getValue(pnd_I));                                         //Natural: ASSIGN IATA002.#RATE-CODE-GRD ( #I ) := #TIAA-RATES.#RATE-CODE ( #I )
                pdaIata002.getIata002_Pnd_Rate_Date_Grd().getValue(pnd_I).setValue(pnd_Tiaa_Rates_Pnd_Rate_Date.getValue(pnd_I));                                         //Natural: ASSIGN IATA002.#RATE-DATE-GRD ( #I ) := #TIAA-RATES.#RATE-DATE ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_D_Div.compute(new ComputeParameters(false, pnd_D_Div), pnd_To_Div.subtract(pnd_W_Div));                                                                       //Natural: ASSIGN #D-DIV := #TO-DIV - #W-DIV
        pnd_D_Gtd.compute(new ComputeParameters(false, pnd_D_Gtd), pnd_To_Gtd.subtract(pnd_W_Gtd));                                                                       //Natural: ASSIGN #D-GTD := #TO-GTD - #W-GTD
        if (condition(pnd_D_Div.greater(getZero())))                                                                                                                      //Natural: IF #D-DIV GT 0
        {
            pnd_E_Div.compute(new ComputeParameters(true, pnd_E_Div), pnd_D_Div.divide(pnd_J));                                                                           //Natural: COMPUTE ROUNDED #E-DIV = #D-DIV / #J
            if (condition(pnd_E_Div.less(getZero())))                                                                                                                     //Natural: IF #E-DIV LT 0
            {
                pnd_E_Div.setValue(0);                                                                                                                                    //Natural: ASSIGN #E-DIV := .01
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 TO #J
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
            {
                pnd_W_Div.nadd(pnd_E_Div);                                                                                                                                //Natural: ADD #E-DIV TO #W-DIV
                pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_I).nadd(pnd_E_Div);                                                                                  //Natural: ADD #E-DIV TO IATA002.#DVD-PMT-GRD ( #I )
                if (condition(pnd_W_Div.greaterOrEqual(pnd_To_Div)))                                                                                                      //Natural: IF #W-DIV GE #TO-DIV
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_D_Div.compute(new ComputeParameters(false, pnd_D_Div), pnd_To_Div.subtract(pnd_W_Div));                                                                   //Natural: ASSIGN #D-DIV := #TO-DIV - #W-DIV
            pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_J).nadd(pnd_D_Div);                                                                                      //Natural: ADD #D-DIV TO IATA002.#DVD-PMT-GRD ( #J )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_J).nadd(pnd_D_Div);                                                                                      //Natural: ADD #D-DIV TO IATA002.#DVD-PMT-GRD ( #J )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_D_Gtd.greater(getZero())))                                                                                                                      //Natural: IF #D-GTD GT 0
        {
            pnd_E_Gtd.compute(new ComputeParameters(true, pnd_E_Gtd), pnd_D_Gtd.divide(pnd_J));                                                                           //Natural: COMPUTE ROUNDED #E-GTD = #D-GTD / #J
            if (condition(pnd_E_Gtd.less(getZero())))                                                                                                                     //Natural: IF #E-GTD LT 0
            {
                pnd_E_Gtd.setValue(0);                                                                                                                                    //Natural: ASSIGN #E-GTD := .01
            }                                                                                                                                                             //Natural: END-IF
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO #J
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
            {
                pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_I).nadd(pnd_E_Gtd);                                                                                  //Natural: ADD #E-GTD TO IATA002.#GTD-PMT-GRD ( #I )
                pnd_W_Gtd.nadd(pnd_E_Gtd);                                                                                                                                //Natural: ADD #E-GTD TO #W-GTD
                if (condition(pnd_W_Gtd.greaterOrEqual(pnd_To_Gtd)))                                                                                                      //Natural: IF #W-GTD GE #TO-GTD
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_D_Gtd.compute(new ComputeParameters(false, pnd_D_Gtd), pnd_To_Gtd.subtract(pnd_W_Gtd));                                                                   //Natural: ASSIGN #D-GTD := #TO-GTD - #W-GTD
            pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_J).nadd(pnd_D_Gtd);                                                                                      //Natural: ADD #D-GTD TO IATA002.#GTD-PMT-GRD ( #J )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_J).nadd(pnd_D_Gtd);                                                                                      //Natural: ADD #D-GTD TO IATA002.#GTD-PMT-GRD ( #J )
            //*  092109
        }                                                                                                                                                                 //Natural: END-IF
        pdaIata002.getIata002_Pnd_Type_Of_Run().setValue("A");                                                                                                            //Natural: ASSIGN IATA002.#TYPE-OF-RUN := 'A'
    }
    private void sub_Pnd_Check_Crefs_Present_Value() throws Exception                                                                                                     //Natural: #CHECK-CREFS-PRESENT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_New_Val_Dollar.reset();                                                                                                                                       //Natural: RESET #NEW-VAL-DOLLAR #ORIGINAL-TRN-AMT
        pnd_Original_Trn_Amt.reset();
        pnd_New_Val_Dollar.compute(new ComputeParameters(true, pnd_New_Val_Dollar), ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(pnd_T))); //Natural: COMPUTE ROUNDED #NEW-VAL-DOLLAR = IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T ) * IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( #T )
        if (condition(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T).equals("M")))                                                                            //Natural: IF IATL171.IAXFR-FRM-UNIT-TYP ( #T ) = 'M'
        {
            ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).setValue(pnd_New_Val_Dollar);                                                             //Natural: ASSIGN IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) := #NEW-VAL-DOLLAR
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet3209 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IATL010.XFR-FRM-TYP ( #T ) = 'D'
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("D")))
        {
            decideConditionsMet3209++;
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T))))             //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T )
            {
                pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).setValue(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T));                       //Natural: ASSIGN IATL010.XFR-FRM-QTY ( #T ) := IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T )
                ldaIatl171.getIatl171_Iaxfr_From_Qty().getValue(pnd_T).setValue(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T));                     //Natural: ASSIGN IATL171.IAXFR-FROM-QTY ( #T ) := IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN IATL010.XFR-FRM-TYP ( #T ) = 'U'
        else if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("U")))
        {
            decideConditionsMet3209++;
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T))))            //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T )
            {
                pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).setValue(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T));                      //Natural: ASSIGN IATL010.XFR-FRM-QTY ( #T ) := IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T )
                ldaIatl171.getIatl171_Iaxfr_From_Qty().getValue(pnd_T).setValue(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T));                    //Natural: ASSIGN IATL171.IAXFR-FROM-QTY ( #T ) := IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Check_Teachers_Present_Value() throws Exception                                                                                                  //Natural: #CHECK-TEACHERS-PRESENT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Tot_Guar_Divid.reset();                                                                                                                                       //Natural: RESET #TOT-GUAR-DIVID
        short decideConditionsMet3227 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IATL010.XFR-FRM-TYP ( #T ) = 'D'
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("D")))
        {
            decideConditionsMet3227++;
            pnd_Tot_Guar_Divid.compute(new ComputeParameters(false, pnd_Tot_Guar_Divid), ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).add(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Divid().getValue(pnd_T))); //Natural: COMPUTE #TOT-GUAR-DIVID = IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) + IATL171.IAXFR-FROM-CURRENT-PMT-DIVID ( #T )
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(pnd_Tot_Guar_Divid)))                                                              //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TOT-GUAR-DIVID
            {
                pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).setValue(pnd_Tot_Guar_Divid);                                                                        //Natural: ASSIGN IATL010.XFR-FRM-QTY ( #T ) := #TOT-GUAR-DIVID
                ldaIatl171.getIatl171_Iaxfr_From_Qty().getValue(pnd_T).setValue(pnd_Tot_Guar_Divid);                                                                      //Natural: ASSIGN IATL171.IAXFR-FROM-QTY ( #T ) := #TOT-GUAR-DIVID
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Get_Unit_Value() throws Exception                                                                                                                //Natural: #GET-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_A26_Fields_Pnd_Auv.reset();                                                                                                                                   //Natural: RESET #AUV #AUV-RET-DTE #RC #A26-PRTC-DTE
        pnd_A26_Fields_Pnd_Auv_Ret_Dte.reset();
        pnd_A26_Fields_Pnd_Rc.reset();
        pnd_A26_Fields_Pnd_A26_Prtc_Dte.reset();
        pnd_A26_Fields_Pnd_A26_Call_Type.setValue("F");                                                                                                                   //Natural: ASSIGN #A26-CALL-TYPE := 'F'
        pnd_A26_Fields_Pnd_A26_Fnd_Cde.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                               //Natural: ASSIGN #A26-FND-CDE := IATL010.XFR-FRM-ACCT-CDE ( #T )
        pnd_A26_Fields_Pnd_A26_Reval_Meth.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T));                                                            //Natural: ASSIGN #A26-REVAL-METH := IATL010.XFR-FRM-UNIT-TYP ( #T )
        pnd_A26_Fields_Pnd_A26_Req_Dte.setValue(pnd_Eff_Date_Pnd_Eff_Date_N);                                                                                             //Natural: ASSIGN #A26-REQ-DTE := #EFF-DATE-N
        //*  040612
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pnd_A26_Fields);                                                                                        //Natural: CALLNAT 'AIAN026' #A26-FIELDS
        if (condition(Global.isEscape())) return;
        //*  #RC #AUV #AUV-RET-DTE         /* 040612
        //*  #DAYS-IN-REQUEST-MONTH        /* 040612
        //*  #DAYS-IN-PARTICIP-MONTH       /* 040612
        if (condition(pls_Debug.getBoolean()))                                                                                                                            //Natural: IF +DEBUG
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-AIAN026-LINKAGE
            sub_Display_Aian026_Linkage();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero())))                                                                                                          //Natural: IF #RC > 0
        {
            getReports().write(0, NEWLINE,"ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ",pnd_A26_Fields_Pnd_Rc);                                                           //Natural: WRITE / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RC
            if (Global.isEscape()) return;
            getReports().write(0, "CONTRACT NUMBER =>",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code());     //Natural: WRITE 'CONTRACT NUMBER =>' #CONTRACT-NUMBER #PAYEE-CODE
            if (Global.isEscape()) return;
            pnd_W3_Reject_Cde.setValue("C1");                                                                                                                             //Natural: ASSIGN #W3-REJECT-CDE := 'C1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Annual_Fund_Cnv() throws Exception                                                                                                               //Natural: #ANNUAL-FUND-CNV
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet3265 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet3265++;
            //*  122308
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet3265++;
            pnd_Fund_Cd_1.setValue("U");                                                                                                                                  //Natural: MOVE 'U' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("2");                                                                                                                                  //Natural: MOVE '2' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_From_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                    //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FROM-FUND-TOT LEAVING NO
    }
    private void sub_Pnd_Monthly_Cnv_Fund() throws Exception                                                                                                              //Natural: #MONTHLY-CNV-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Fund_Cd_1.reset();                                                                                                                                            //Natural: RESET #FUND-CD-1
        short decideConditionsMet3280 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ONE-BYTE-FUND = 'T' OR = 'G'
        if (condition(pnd_One_Byte_Fund.equals("T") || pnd_One_Byte_Fund.equals("G")))
        {
            decideConditionsMet3280++;
            //*  122308
            pnd_Fund_Cd_1.setValue("T");                                                                                                                                  //Natural: MOVE 'T' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN #ONE-BYTE-FUND = 'R' OR = 'D'
        else if (condition(pnd_One_Byte_Fund.equals("R") || pnd_One_Byte_Fund.equals("D")))
        {
            decideConditionsMet3280++;
            pnd_Fund_Cd_1.setValue("W");                                                                                                                                  //Natural: MOVE 'W' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fund_Cd_1.setValue("4");                                                                                                                                  //Natural: MOVE '4' TO #FUND-CD-1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_From_Fund_Tot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Cd_1, pnd_Two_Byte_Fund));                                                    //Natural: COMPRESS #FUND-CD-1 #TWO-BYTE-FUND INTO #FROM-FUND-TOT LEAVING NO
    }
    private void sub_Pnd_Monthly_Trnsfr_Units() throws Exception                                                                                                          //Natural: #MONTHLY-TRNSFR-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("D")))                                                                                  //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'D'
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(pnd_Tiaa_Fund_Pymt)))                                                              //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-FUND-PYMT
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()),     //Natural: COMPUTE ROUNDED #TRANSFER-UNITS = #TIAA-FUND-PYMT / #AUV
                    pnd_Tiaa_Fund_Pymt.divide(pnd_A26_Fields_Pnd_Auv));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()),     //Natural: COMPUTE ROUNDED #TRANSFER-UNITS = IATL010.XFR-FRM-QTY ( #T ) / #AUV
                    pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).divide(pnd_A26_Fields_Pnd_Auv));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("P")))                                                                              //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'P'
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()),     //Natural: COMPUTE ROUNDED #TRANSFER-UNITS = #TIAA-UNITS-CNT * IATL010.XFR-FRM-QTY ( #T ) / 100
                    pnd_Tiaa_Units_Cnt.multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(pnd_Tiaa_Units_Cnt)))                                                          //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-UNITS-CNT
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(pnd_Tiaa_Units_Cnt);                                                                 //Natural: ASSIGN #TRANSFER-UNITS := #TIAA-UNITS-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                               //Natural: ASSIGN #TRANSFER-UNITS := IATL010.XFR-FRM-QTY ( #T )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Annual_Trnsfr_Units_And_Dollars() throws Exception                                                                                               //Natural: #ANNUAL-TRNSFR-UNITS-AND-DOLLARS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Per.reset();                                                                                                                                                  //Natural: RESET #PER #TRANSFER-DOLLARS #XFR-UNITS
        pnd_Transfer_Dollars.reset();
        pnd_Xfr_Units.reset();
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("D")))                                                                                  //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'D'
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(pnd_Tiaa_Fund_Pymt)))                                                              //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-FUND-PYMT
            {
                pnd_Transfer_Dollars.setValue(pnd_Tiaa_Fund_Pymt);                                                                                                        //Natural: MOVE #TIAA-FUND-PYMT TO #TRANSFER-DOLLARS
                pnd_Xfr_Units.setValue(pnd_Tiaa_Units_Cnt);                                                                                                               //Natural: MOVE #TIAA-UNITS-CNT TO #XFR-UNITS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Per.compute(new ComputeParameters(true, pnd_Per), pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).divide(pnd_Tiaa_Fund_Pymt).multiply(100));     //Natural: COMPUTE ROUNDED #PER = IATL010.XFR-FRM-QTY ( #T ) / #TIAA-FUND-PYMT * 100
                pnd_Xfr_Units.compute(new ComputeParameters(false, pnd_Xfr_Units), pnd_Per.multiply(pnd_Tiaa_Units_Cnt).divide(100));                                     //Natural: COMPUTE #XFR-UNITS = #PER * #TIAA-UNITS-CNT / 100
                pnd_Transfer_Dollars.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                                                      //Natural: MOVE IATL010.XFR-FRM-QTY ( #T ) TO #TRANSFER-DOLLARS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("P")))                                                                              //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'P'
            {
                pnd_Xfr_Units.compute(new ComputeParameters(true, pnd_Xfr_Units), pnd_Tiaa_Units_Cnt.multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #XFR-UNITS = #TIAA-UNITS-CNT * IATL010.XFR-FRM-QTY ( #T ) / 100
                pnd_Transfer_Dollars.compute(new ComputeParameters(true, pnd_Transfer_Dollars), pnd_Tiaa_Fund_Pymt.multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #TRANSFER-DOLLARS = #TIAA-FUND-PYMT * IATL010.XFR-FRM-QTY ( #T ) / 100
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(pnd_Tiaa_Units_Cnt)))                                                          //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-UNITS-CNT
                {
                    pnd_Transfer_Dollars.setValue(pnd_Tiaa_Fund_Pymt);                                                                                                    //Natural: MOVE #TIAA-FUND-PYMT TO #TRANSFER-DOLLARS
                    pnd_Xfr_Units.setValue(pnd_Tiaa_Units_Cnt);                                                                                                           //Natural: MOVE #TIAA-UNITS-CNT TO #XFR-UNITS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Per.compute(new ComputeParameters(true, pnd_Per), pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).divide(pnd_Tiaa_Units_Cnt).multiply(100)); //Natural: COMPUTE ROUNDED #PER = IATL010.XFR-FRM-QTY ( #T ) / #TIAA-UNITS-CNT * 100
                    pnd_Transfer_Dollars.compute(new ComputeParameters(false, pnd_Transfer_Dollars), pnd_Per.multiply(pnd_Tiaa_Fund_Pymt).divide(100));                   //Natural: COMPUTE #TRANSFER-DOLLARS = #PER * #TIAA-FUND-PYMT / 100
                    pnd_Xfr_Units.setValue(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                                                         //Natural: ASSIGN #XFR-UNITS := IATL010.XFR-FRM-QTY ( #T )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(pnd_Xfr_Units);                                                                                  //Natural: ASSIGN #TRANSFER-UNITS := #XFR-UNITS
    }
    private void sub_Get_Fund_To_Receive() throws Exception                                                                                                               //Natural: GET-FUND-TO-RECEIVE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iatn261.class , getCurrentProcessState(), pnd_Fund_Cde, ldaIatl420.getIaa_Cntrct_Cntrct_Orgn_Cde(), pnd_Fund_Rcvd);                               //Natural: CALLNAT 'IATN261' #FUND-CDE IAA-CNTRCT.CNTRCT-ORGN-CDE #FUND-RCVD
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Monthly_Transfer_To_Arrays() throws Exception                                                                                                    //Natural: #MONTHLY-TRANSFER-TO-ARRAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_W_Per.reset();                                                                                                                                                //Natural: RESET #W-PER
        FOR05:                                                                                                                                                            //Natural: FOR #I = #VAR1 TO #VAR2
        for (pnd_I.setValue(pnd_Var1); condition(pnd_I.lessOrEqual(pnd_Var2)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) NE ' '
            {
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Typ().getValue(pnd_I).equals("P")))                                                                           //Natural: IF IATL010.XFR-TO-TYP ( #I ) = 'P'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #TRANSFER-UNITS * IATL010.XFR-TO-QTY ( #I ) / 100
                        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Per.compute(new ComputeParameters(true, pnd_W_Per), pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I).divide(pnd_To_Qty_Total).multiply(100)); //Natural: COMPUTE ROUNDED #W-PER = IATL010.XFR-TO-QTY ( #I ) / #TO-QTY-TOTAL * 100
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #W-PER * #TRANSFER-UNITS / 100
                        pnd_W_Per.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()).divide(100));
                    pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I).compute(new ComputeParameters(true, pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)),         //Natural: COMPUTE ROUNDED IATL010.XFR-TO-QTY ( #I ) = #W-PER * IATL010.XFR-FRM-QTY ( #T ) / 100
                        pnd_W_Per.multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100));
                }                                                                                                                                                         //Natural: END-IF
                //*  122308 START
                //*    IF IATL010.XFR-TO-ACCT-CDE(#I) = 'T'
                //*      #FUND-TO-RECEIVE(#I) := '1'
                //*    ELSE
                //*      IF IATL010.XFR-TO-ACCT-CDE(#I) = 'G'
                //*        #FUND-TO-RECEIVE(#I) := '2'
                //*  = 'G'
                //*  02/10
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))                                                                      //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue("2");                                                               //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := '2'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));          //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := IATL010.XFR-TO-ACCT-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                pnd_To_Units.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I));                                                             //Natural: ASSIGN #TO-UNITS := #TO-UNITS + #UNITS-TO-RECEIVE ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Monthly_Transfer_To_Arrays_Switch() throws Exception                                                                                             //Natural: #MONTHLY-TRANSFER-TO-ARRAYS-SWITCH
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_W_Per.reset();                                                                                                                                                //Natural: RESET #W-PER
        FOR06:                                                                                                                                                            //Natural: FOR #I = #VAR1 TO #VAR2
        for (pnd_I.setValue(pnd_Var1); condition(pnd_I.lessOrEqual(pnd_Var2)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) NE ' '
            {
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                pdaIatl420z.getIatl010_Xfr_To_Typ().getValue(pnd_I).setValue("P");                                                                                        //Natural: ASSIGN IATL010.XFR-TO-TYP ( #I ) := 'P'
                pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I).setValue(100);                                                                                        //Natural: ASSIGN IATL010.XFR-TO-QTY ( #I ) := 100.00
                //*    IF IATL010.XFR-TO-TYP(#I) = 'P'
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( 1 ) = #TRANSFER-UNITS * IATL010.XFR-TO-QTY ( #I ) / 100
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
                //*    ELSE
                //*      COMPUTE ROUNDED #W-PER =
                //*        IATL010.XFR-TO-QTY(#I) / #TO-QTY-TOTAL * 100
                //*      COMPUTE ROUNDED #UNITS-TO-RECEIVE(1) =
                //*        #W-PER * #TRANSFER-UNITS  / 100
                //*      COMPUTE ROUNDED IATL010.XFR-TO-QTY(#I) =
                //*        #W-PER * IATL010.XFR-FRM-QTY(#T)  / 100
                //*    END-IF
                //*  122308 START
                //*    IF IATL010.XFR-TO-ACCT-CDE(#I) = 'T'
                //*      #FUND-TO-RECEIVE(1) := '1'
                //*    ELSE
                //*      IF IATL010.XFR-TO-ACCT-CDE(#I) = 'G'
                //*        #FUND-TO-RECEIVE(1) := '2'
                //*  = 'G'  02/10
                //*  02/10
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))                                                                      //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1).setValue("2");                                                                   //Natural: ASSIGN #FUND-TO-RECEIVE ( 1 ) := '2'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1).setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));              //Natural: ASSIGN #FUND-TO-RECEIVE ( 1 ) := IATL010.XFR-TO-ACCT-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                pnd_To_Units.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1));                                                                 //Natural: ASSIGN #TO-UNITS := #TO-UNITS + #UNITS-TO-RECEIVE ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Annual_Transfer_To_Arrays() throws Exception                                                                                                     //Natural: #ANNUAL-TRANSFER-TO-ARRAYS
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #I = #VAR1 TO #VAR2
        for (pnd_I.setValue(pnd_Var1); condition(pnd_I.lessOrEqual(pnd_Var2)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) NE ' '
            {
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                                                                                                                                                                          //Natural: PERFORM #FILL-TO-SIDE
                sub_Pnd_Fill_To_Side();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  122308 START
                //*    IF IATL010.XFR-TO-ACCT-CDE(#I) = 'T'
                //*      #FUND-TO-RECEIVE(#I) := '1'
                //*    ELSE
                //*      IF IATL010.XFR-TO-ACCT-CDE(#I) = 'G'
                //*        #FUND-TO-RECEIVE(#I) := '2'
                //*      ELSE
                //*  = 'G'        02/10
                //*  02/10
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))                                                                      //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue("2");                                                               //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := '2'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));          //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := IATL010.XFR-TO-ACCT-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                pnd_To_Units.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I));                                                             //Natural: ASSIGN #TO-UNITS := #TO-UNITS + #UNITS-TO-RECEIVE ( #I )
                pnd_To_Pymts.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I));                                                              //Natural: ASSIGN #TO-PYMTS := #TO-PYMTS + #PMTS-TO-RECEIVE ( #I )
                //*  KN     #TO-PYMTS := #PMTS-TO-RECEIVE(#I)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Annual_Transfer_To_Arrays_Switch() throws Exception                                                                                              //Natural: #ANNUAL-TRANSFER-TO-ARRAYS-SWITCH
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #I = #VAR1 TO #VAR2
        for (pnd_I.setValue(pnd_Var1); condition(pnd_I.lessOrEqual(pnd_Var2)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) NE ' '
            {
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                pdaIatl420z.getIatl010_Xfr_To_Typ().getValue(pnd_I).setValue("P");                                                                                        //Natural: ASSIGN IATL010.XFR-TO-TYP ( #I ) := 'P'
                pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I).setValue(100);                                                                                        //Natural: ASSIGN IATL010.XFR-TO-QTY ( #I ) := 100.00
                                                                                                                                                                          //Natural: PERFORM #FILL-TO-SIDE-SWITCH
                sub_Pnd_Fill_To_Side_Switch();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  122308 START
                //*    IF IATL010.XFR-TO-ACCT-CDE(#I) = 'T'
                //*      #FUND-TO-RECEIVE(1) := '1'
                //*    ELSE
                //*      IF IATL010.XFR-TO-ACCT-CDE(#I) = 'G'
                //*        #FUND-TO-RECEIVE(1) := '2'
                //*      ELSE
                //*  = 'G'           02/10
                //*  02/10
                if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))                                                                      //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1).setValue("2");                                                                   //Natural: ASSIGN #FUND-TO-RECEIVE ( 1 ) := '2'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1).setValue(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));              //Natural: ASSIGN #FUND-TO-RECEIVE ( 1 ) := IATL010.XFR-TO-ACCT-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                pnd_To_Units.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1));                                                                 //Natural: ASSIGN #TO-UNITS := #TO-UNITS + #UNITS-TO-RECEIVE ( 1 )
                pnd_To_Pymts.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1));                                                                  //Natural: ASSIGN #TO-PYMTS := #TO-PYMTS + #PMTS-TO-RECEIVE ( 1 )
                //*  KN     #TO-PYMTS := #PMTS-TO-RECEIVE(1)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_To_Side() throws Exception                                                                                                                  //Natural: #FILL-TO-SIDE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Per.reset();                                                                                                                                                  //Natural: RESET #PER
        if (condition(pdaIatl420z.getIatl010_Xfr_To_Typ().getValue(pnd_I).equals("P")))                                                                                   //Natural: IF IATL010.XFR-TO-TYP ( #I ) = 'P'
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #XFR-UNITS * IATL010.XFR-TO-QTY ( #I ) / 100
                pnd_Xfr_Units.multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #PMTS-TO-RECEIVE ( #I ) = #TRANSFER-DOLLARS * IATL010.XFR-TO-QTY ( #I ) / 100
                pnd_Transfer_Dollars.multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Per.compute(new ComputeParameters(true, pnd_Per), pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I).divide(pnd_To_Qty_Total).multiply(100));            //Natural: COMPUTE ROUNDED #PER = IATL010.XFR-TO-QTY ( #I ) / #TO-QTY-TOTAL * 100
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #PER * #TRANSFER-UNITS / 100
                pnd_Per.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()).divide(100));
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #PMTS-TO-RECEIVE ( #I ) = #PER * IATL010.XFR-FRM-QTY ( #T ) / 100
                pnd_Per.multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100));
            pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I).compute(new ComputeParameters(true, pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)),                 //Natural: COMPUTE ROUNDED IATL010.XFR-TO-QTY ( #I ) = #PER * IATL010.XFR-FRM-QTY ( #T ) / 100
                pnd_Per.multiply(pdaIatl420z.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100));
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Fill_To_Side_Switch() throws Exception                                                                                                           //Natural: #FILL-TO-SIDE-SWITCH
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Per.reset();                                                                                                                                                  //Natural: RESET #PER
        //*  IF IATL010.XFR-TO-TYP(#I) = 'P'
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( 1 ) = #XFR-UNITS * IATL010.XFR-TO-QTY ( #I ) / 100
            pnd_Xfr_Units.multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1)),  //Natural: COMPUTE ROUNDED #PMTS-TO-RECEIVE ( 1 ) = #TRANSFER-DOLLARS * IATL010.XFR-TO-QTY ( #I ) / 100
            pnd_Transfer_Dollars.multiply(pdaIatl420z.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
        //*  ELSE
        //*   COMPUTE ROUNDED #PER =
        //*         IATL010.XFR-TO-QTY(#I) / #TO-QTY-TOTAL * 100
        //*   COMPUTE ROUNDED #UNITS-TO-RECEIVE(1) =
        //*         #PER * #TRANSFER-UNITS  / 100
        //*   COMPUTE ROUNDED #PMTS-TO-RECEIVE(1) =
        //*         #PER * IATL010.XFR-FRM-QTY(#T)  / 100
        //*   COMPUTE ROUNDED IATL010.XFR-TO-QTY(#I) =
        //*         #PER * IATL010.XFR-FRM-QTY(#T)  / 100
        //*  END-IF
    }
    private void sub_Pnd_Rounding_Units() throws Exception                                                                                                                //Natural: #ROUNDING-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_D_Units.compute(new ComputeParameters(false, pnd_D_Units), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().subtract(pnd_To_Units));                   //Natural: ASSIGN #D-UNITS := #TRANSFER-UNITS - #TO-UNITS
        if (condition(pnd_D_Units.greater(getZero())))                                                                                                                    //Natural: IF #D-UNITS GT 0
        {
            if (condition(pnd_J.greater(2)))                                                                                                                              //Natural: IF #J GT 2
            {
                pnd_W_Units.compute(new ComputeParameters(false, pnd_W_Units), pnd_J.multiply(new DbsDecimal(".001")));                                                   //Natural: ASSIGN #W-UNITS := #J * .001
                short decideConditionsMet3508 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #D-UNITS LE #W-UNITS
                if (condition(pnd_D_Units.lessOrEqual(pnd_W_Units)))
                {
                    decideConditionsMet3508++;
                    pnd_E_Units.setValue(0);                                                                                                                              //Natural: ASSIGN #E-UNITS := .001
                }                                                                                                                                                         //Natural: WHEN #D-UNITS > #W-UNITS
                else if (condition(pnd_D_Units.greater(pnd_W_Units)))
                {
                    decideConditionsMet3508++;
                    pnd_E_Units.compute(new ComputeParameters(true, pnd_E_Units), pnd_D_Units.divide(pnd_J));                                                             //Natural: COMPUTE ROUNDED #E-UNITS = #D-UNITS / #J
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet3508 > 0))
                {
                    F8:                                                                                                                                                   //Natural: FOR #I = 1 TO #J
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
                    {
                        if (condition((pnd_To_Units.add(pnd_E_Units)).less(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units())))                                     //Natural: IF ( #TO-UNITS + #E-UNITS ) LT #TRANSFER-UNITS
                        {
                            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).nadd(pnd_E_Units);                                                  //Natural: ADD #E-UNITS TO #UNITS-TO-RECEIVE ( #I )
                            //* ***         ADD #UNITS-TO-RECEIVE(#I) TO #TO-UNITS
                            pnd_To_Units.nadd(pnd_E_Units);                                                                                                               //Natural: ADD #E-UNITS TO #TO-UNITS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().notEquals(pnd_To_Units)))                                                       //Natural: IF #TRANSFER-UNITS NE #TO-UNITS
                    {
                        pnd_E_Units.compute(new ComputeParameters(false, pnd_E_Units), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().subtract(pnd_To_Units));   //Natural: ASSIGN #E-UNITS := #TRANSFER-UNITS - #TO-UNITS
                        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_J).nadd(pnd_E_Units);                                                      //Natural: ADD #E-UNITS TO #UNITS-TO-RECEIVE ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(2).nadd(pnd_D_Units);                                                                  //Natural: ADD #D-UNITS TO #UNITS-TO-RECEIVE ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_J).nadd(pnd_D_Units);                                                                  //Natural: ADD #D-UNITS TO #UNITS-TO-RECEIVE ( #J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Rounding_Dollars() throws Exception                                                                                                              //Natural: #ROUNDING-DOLLARS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_D_Pymts.compute(new ComputeParameters(false, pnd_D_Pymts), pnd_Transfer_Dollars.subtract(pnd_To_Pymts));                                                      //Natural: ASSIGN #D-PYMTS := #TRANSFER-DOLLARS - #TO-PYMTS
        //*  WRITE '=' #J '=' #D-PYMTS
        if (condition(pnd_D_Pymts.greater(getZero())))                                                                                                                    //Natural: IF #D-PYMTS GT 0
        {
            if (condition(pnd_J.greater(2)))                                                                                                                              //Natural: IF #J GT 2
            {
                pnd_W_Pymts.compute(new ComputeParameters(false, pnd_W_Pymts), pnd_J.multiply(new DbsDecimal(".01")));                                                    //Natural: ASSIGN #W-PYMTS := #J * .01
                short decideConditionsMet3544 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #D-PYMTS LE #W-PYMTS
                if (condition(pnd_D_Pymts.lessOrEqual(pnd_W_Pymts)))
                {
                    decideConditionsMet3544++;
                    pnd_E_Pymts.setValue(0);                                                                                                                              //Natural: ASSIGN #E-PYMTS := .01
                }                                                                                                                                                         //Natural: WHEN #D-PYMTS > #W-PYMTS
                else if (condition(pnd_D_Pymts.greater(pnd_W_Pymts)))
                {
                    decideConditionsMet3544++;
                    pnd_E_Pymts.compute(new ComputeParameters(true, pnd_E_Pymts), pnd_D_Pymts.divide(pnd_J));                                                             //Natural: COMPUTE ROUNDED #E-PYMTS = #D-PYMTS / #J
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet3544 > 0))
                {
                    F9:                                                                                                                                                   //Natural: FOR #I = 1 TO #J
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
                    {
                        if (condition((pnd_To_Pymts.add(pnd_E_Pymts)).less(pnd_Transfer_Dollars)))                                                                        //Natural: IF ( #TO-PYMTS + #E-PYMTS ) LT #TRANSFER-DOLLARS
                        {
                            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).nadd(pnd_E_Pymts);                                                   //Natural: ADD #E-PYMTS TO #PMTS-TO-RECEIVE ( #I )
                            //* ***         ADD #PYMTS-TO-RECEIVE(#I) TO #TO-PYMTS
                            pnd_To_Pymts.nadd(pnd_E_Pymts);                                                                                                               //Natural: ADD #E-PYMTS TO #TO-PYMTS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    if (condition(pnd_Transfer_Dollars.notEquals(pnd_To_Pymts)))                                                                                          //Natural: IF #TRANSFER-DOLLARS NE #TO-PYMTS
                    {
                        pnd_E_Pymts.compute(new ComputeParameters(false, pnd_E_Pymts), pnd_Transfer_Dollars.subtract(pnd_To_Pymts));                                      //Natural: ASSIGN #E-PYMTS := #TRANSFER-DOLLARS - #TO-PYMTS
                        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_J).nadd(pnd_E_Pymts);                                                       //Natural: ADD #E-PYMTS TO #PMTS-TO-RECEIVE ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(2).nadd(pnd_D_Pymts);                                                                   //Natural: ADD #D-PYMTS TO #PMTS-TO-RECEIVE ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_J).nadd(pnd_D_Pymts);                                                                   //Natural: ADD #D-PYMTS TO #PMTS-TO-RECEIVE ( #J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_From_Fund_Only_Para() throws Exception                                                                                                           //Natural: #FROM-FUND-ONLY-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_From_On_To_Side.reset();                                                                                                                                      //Natural: RESET #FROM-ON-TO-SIDE
        Q:                                                                                                                                                                //Natural: FOR #QQ = 1 TO 20
        for (pnd_Qq.setValue(1); condition(pnd_Qq.lessOrEqual(20)); pnd_Qq.nadd(1))
        {
            if (condition(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_Qq).equals(" ")))                                                                      //Natural: IF IATL171.IAXFR-FROM-FUND-CDE ( #QQ ) = ' '
            {
                //*    ESCAPE BOTTOM(Q.)
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_From_Fund_Only.getValue(pnd_Qq).setValue("Y");                                                                                                        //Natural: ASSIGN #FROM-FUND-ONLY ( #QQ ) := 'Y'
                if (condition(ldaIatl171.getIatl171_Rcrd_Type_Cde().equals("2")))                                                                                         //Natural: IF IATL171.RCRD-TYPE-CDE = '2'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    Z:                                                                                                                                                    //Natural: FOR #NN = 1 TO 20
                    for (pnd_Nn.setValue(1); condition(pnd_Nn.lessOrEqual(20)); pnd_Nn.nadd(1))
                    {
                        if (condition(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_Qq).equals(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_Nn))))   //Natural: IF IATL171.IAXFR-FROM-FUND-CDE ( #QQ ) = IATL171.IAXFR-TO-FUND-CDE ( #NN )
                        {
                            pnd_From_Fund_Only.getValue(pnd_Qq).setValue(" ");                                                                                            //Natural: ASSIGN #FROM-FUND-ONLY ( #QQ ) := ' '
                            pnd_From_On_To_Side.setValue("Y");                                                                                                            //Natural: MOVE 'Y' TO #FROM-ON-TO-SIDE
                            if (true) break Z;                                                                                                                            //Natural: ESCAPE BOTTOM ( Z. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("Q"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("Q"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Determine_Full_Tiaa_Real_Out() throws Exception                                                                                                  //Natural: #DETERMINE-FULL-TIAA-REAL-OUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Not_Full_Out.reset();                                                                                                                                         //Natural: RESET #NOT-FULL-OUT
        if (condition(ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1).equals(getZero()) && ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1).equals(getZero()))) //Natural: IF IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1 ) = 0 AND IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1 ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-TIAA-TO-SIDE
            sub_Pnd_Check_Tiaa_To_Side();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Not_Full_Out.equals(" ")))                                                                                                                  //Natural: IF #NOT-FULL-OUT = ' '
            {
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-REAL-OUT
                sub_Pnd_Check_If_Real_Out();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Not_Full_Out.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #NOT-FULL-OUT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Not_Full_Out.equals(" ")))                                                                                                                      //Natural: IF #NOT-FULL-OUT = ' '
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue("Y");                                                                                          //Natural: MOVE 'Y' TO #IATN420X-IN.#NEW-INACTIVE-IND
            pnd_Full_Contract_Out.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #FULL-CONTRACT-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #IATN420X-IN.#NEW-INACTIVE-IND
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Determine_Full_6m_Real_Out() throws Exception                                                                                                    //Natural: #DETERMINE-FULL-6M-REAL-OUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Not_Full_Out.reset();                                                                                                                                         //Natural: RESET #NOT-FULL-OUT
        if (condition(ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(1).equals(getZero()) && ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(1).equals(getZero()))) //Natural: IF IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( 1 ) = 0 AND IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( 1 ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-6M-TO-SIDE
            sub_Pnd_Check_6m_To_Side();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Not_Full_Out.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #NOT-FULL-OUT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Not_Full_Out.equals(" ")))                                                                                                                      //Natural: IF #NOT-FULL-OUT = ' '
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue("Y");                                                                                          //Natural: MOVE 'Y' TO #IATN420X-IN.#NEW-INACTIVE-IND
            pnd_Full_Contract_Out.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #FULL-CONTRACT-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #IATN420X-IN.#NEW-INACTIVE-IND
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Determine_Full_Cref_Out() throws Exception                                                                                                       //Natural: #DETERMINE-FULL-CREF-OUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Not_Full_Out.reset();                                                                                                                                         //Natural: RESET #NOT-FULL-OUT #NOT-ALL-FROM-TRANSFER-OUT
        pnd_Not_All_From_Transfer_Out.reset();
        TQ:                                                                                                                                                               //Natural: FOR #QQ = 1 TO 20
        for (pnd_Qq.setValue(1); condition(pnd_Qq.lessOrEqual(20)); pnd_Qq.nadd(1))
        {
            if (condition(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_Qq).equals(" ")))                                                                      //Natural: IF IATL171.IAXFR-FROM-FUND-CDE ( #QQ ) = ' '
            {
                if (true) break TQ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( TQ. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_Qq).equals(getZero()) && ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_Qq).equals(getZero()))) //Natural: IF IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( #QQ ) = 0 AND IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( #QQ ) = 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Not_Full_Out.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #NOT-FULL-OUT
                if (true) break TQ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( TQ. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Not_Full_Out.equals(" ")))                                                                                                                      //Natural: IF #NOT-FULL-OUT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM #CHECK-CREF-TO-SIDE
            sub_Pnd_Check_Cref_To_Side();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Not_Full_Out.equals(" ")))                                                                                                                  //Natural: IF #NOT-FULL-OUT = ' '
            {
                                                                                                                                                                          //Natural: PERFORM #CHECK-IF-CREF-OUT
                sub_Pnd_Check_If_Cref_Out();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Not_Full_Out.equals(" ")))                                                                                                                      //Natural: IF #NOT-FULL-OUT = ' '
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue("Y");                                                                                          //Natural: MOVE 'Y' TO #IATN420X-IN.#NEW-INACTIVE-IND
            pnd_Full_Contract_Out.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #FULL-CONTRACT-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIatl420x.getPnd_Iatn420x_In_Pnd_New_Inactive_Ind().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #IATN420X-IN.#NEW-INACTIVE-IND
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_If_Real_Out() throws Exception                                                                                                             //Natural: #CHECK-IF-REAL-OUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_W_Other_Funds.reset();                                                                                                                                        //Natural: RESET #W-OTHER-FUNDS
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := IATL010.#IA-FRM-PAYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal420.getVw_iaa_Tiaa_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R11",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R11:
        while (condition(ldaIaal420.getVw_iaa_Tiaa_Fund().readNextRow("R11")))
        {
            if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                //*  122309
                if (condition(ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("U09") || ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("W09")          //Natural: REJECT IF IAA-TIAA-FUND.TIAA-CMPNY-FUND-CDE = 'U09' OR = 'W09' OR = 'U11' OR = 'W11'
                    || ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("U11") || ldaIaal420.getIaa_Tiaa_Fund_Tiaa_Cmpny_Fund_Cde().equals("W11")))
                {
                    continue;
                }
                pnd_W_Other_Funds.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #W-OTHER-FUNDS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R11;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R11. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_W_Other_Funds.equals("Y")))                                                                                                                     //Natural: IF #W-OTHER-FUNDS = 'Y'
        {
            pnd_Not_Full_Out.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #NOT-FULL-OUT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Check_If_Cref_Out() throws Exception                                                                                                             //Natural: #CHECK-IF-CREF-OUT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := IATL010.#IA-FRM-PAYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #W-FUND-CODE := ' '
        ldaIaal420.getVw_iaa_Cref_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-CREF-FUND BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R15",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R15:
        while (condition(ldaIaal420.getVw_iaa_Cref_Fund().readNextRow("R15")))
        {
            if (condition(ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                pnd_Found_Fund.reset();                                                                                                                                   //Natural: RESET #FOUND-FUND
                AZ:                                                                                                                                                       //Natural: FOR #ZZ = 1 TO 20
                for (pnd_Zz.setValue(1); condition(pnd_Zz.lessOrEqual(20)); pnd_Zz.nadd(1))
                {
                    if (condition(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_Zz).equals(" ")))                                                              //Natural: IF IATL171.IAXFR-FROM-FUND-CDE ( #ZZ ) = ' '
                    {
                        if (true) break AZ;                                                                                                                               //Natural: ESCAPE BOTTOM ( AZ. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal420.getIaa_Cref_Fund_Cref_Fund_Cde().equals(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_Zz))))                      //Natural: IF IAA-CREF-FUND.CREF-FUND-CDE = IATL171.IAXFR-FROM-FUND-CDE ( #ZZ )
                    {
                        pnd_Found_Fund.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #FOUND-FUND
                        if (true) break AZ;                                                                                                                               //Natural: ESCAPE BOTTOM ( AZ. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R15"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R15"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Found_Fund.equals(" ")))                                                                                                                //Natural: IF #FOUND-FUND = ' '
                {
                    pnd_Not_Full_Out.setValue("Y");                                                                                                                       //Natural: MOVE 'Y' TO #NOT-FULL-OUT
                    if (true) break R15;                                                                                                                                  //Natural: ESCAPE BOTTOM ( R15. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R15;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R15. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Tiaa_To_Side() throws Exception                                                                                                            //Natural: #CHECK-TIAA-TO-SIDE
    {
        if (BLNatReinput.isReinput()) return;

        FQK:                                                                                                                                                              //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  122308
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("T") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")    //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'T' OR = 'G' OR = 'R' OR = 'D'
                || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("R") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("D")))
            {
                pnd_Not_Full_Out.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #NOT-FULL-OUT
                if (true) break FQK;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FQK. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_6m_To_Side() throws Exception                                                                                                              //Natural: #CHECK-6M-TO-SIDE
    {
        if (BLNatReinput.isReinput()) return;

        FKQ:                                                                                                                                                              //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("R")))                                                                          //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'R'
            {
                pnd_Not_Full_Out.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #NOT-FULL-OUT
                if (true) break FKQ;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FKQ. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Cref_To_Side() throws Exception                                                                                                            //Natural: #CHECK-CREF-TO-SIDE
    {
        if (BLNatReinput.isReinput()) return;

        FFK:                                                                                                                                                              //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals(" ")))                                                                          //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = ' '
            {
                if (true) break FFK;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FFK. )
            }                                                                                                                                                             //Natural: END-IF
            //*  122308
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("T") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")    //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'T' OR = 'G' OR = 'R' OR = 'D'
                || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("R") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("D")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Not_Full_Out.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #NOT-FULL-OUT
                if (true) break FFK;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FFK. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Switching_Check() throws Exception                                                                                                               //Natural: #SWITCHING-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Switch_Fund_Found.reset();                                                                                                                                    //Natural: RESET #SWITCH-FUND-FOUND
        if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T).equals("A")))                                                                             //Natural: IF IATL010.XFR-FRM-UNIT-TYP ( #T ) = 'A'
        {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-FUND-CNV
            sub_Pnd_Annual_Fund_Cnv();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-CNV-FUND
            sub_Pnd_Monthly_Cnv_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaIatl420z.getIatl010_Ia_Frm_Cntrct());                                                                       //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := IATL010.IA-FRM-CNTRCT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaIatl420z.getIatl010_Pnd_Ia_Frm_Payee_N());                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := IATL010.#IA-FRM-PAYEE-N
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue(pnd_From_Fund_Tot);                                                                                                  //Natural: ASSIGN #W-FUND-CODE := #FROM-FUND-TOT
        ldaIaal420.getVw_iaa_Cref_Fund().startDatabaseRead                                                                                                                //Natural: READ IAA-CREF-FUND BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R13",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R13:
        while (condition(ldaIaal420.getVw_iaa_Cref_Fund().readNextRow("R13")))
        {
            if (condition(ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal420.getIaa_Cref_Fund_Cref_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-CREF-FUND.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-CREF-FUND.CREF-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal420.getIaa_Cref_Fund_Cref_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_At_Least_One_Switch.setValue("Y");                                                                                                                    //Natural: MOVE 'Y' TO #AT-LEAST-ONE-SWITCH
                pnd_Switch_Fund_Found.setValue("Y");                                                                                                                      //Natural: MOVE 'Y' TO #SWITCH-FUND-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R13;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R13. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Fund_Info() throws Exception                                                                                                               //Natural: #RESET-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().reset();                                                                                                  //Natural: RESET #TRANSFER-UNITS #FUND-CODE #FUND-TO-RECEIVE ( 1:21 ) #UNITS-TO-RECEIVE ( 1:21 ) #PMTS-TO-RECEIVE ( 1:21 ) #AIAN013-LINKAGE.#RETURN-CODE #FUND-CODE-OUT ( 1:19 ) #UNITS-OUT ( 1:19 ) #AUV-OUT ( 1:19 ) #TRANSFER-AMT-OUT-CREF ( 1:19 ) #PMT-METHOD-CODE-OUT ( 1:2 ) #RATE-CODE-OUT ( 1:2 ) #GTD-PMT-OUT ( 1:2 ) #DVD-PMT-OUT ( 1:2 ) #TRANSFER-AMT-OUT-TIAA ( 1:2 ) #TOTAL-TRANSFER-AMT-OUT #J #TO-UNITS #D-UNITS #W-UNITS #E-UNITS #TO-PYMTS #D-PYMTS #W-PYMTS #E-PYMTS
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().reset();
        pnd_J.reset();
        pnd_To_Units.reset();
        pnd_D_Units.reset();
        pnd_W_Units.reset();
        pnd_E_Units.reset();
        pnd_To_Pymts.reset();
        pnd_D_Pymts.reset();
        pnd_W_Pymts.reset();
        pnd_E_Pymts.reset();
    }
    private void sub_Pnd_Revalue_03_31() throws Exception                                                                                                                 //Natural: #REVALUE-03-31
    {
        if (BLNatReinput.isReinput()) return;

        JD:                                                                                                                                                               //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T).equals(" ")))                                                                       //Natural: IF IATL171.IAXFR-FROM-FUND-CDE ( #T ) = ' '
            {
                if (true) break JD;                                                                                                                                       //Natural: ESCAPE BOTTOM ( JD. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaIatl420z.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals("G") || (pdaIatl420z.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T).equals("M") //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) = 'G' OR ( IATL010.XFR-FRM-UNIT-TYP ( #T ) = 'M' AND IATL010.RCRD-TYPE-CDE = '1' )
                && pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("1"))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Diff.compute(new ComputeParameters(true, pnd_W_Diff), ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(pnd_T))); //Natural: COMPUTE ROUNDED #W-DIFF = IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( #T ) * IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( #T )
                pnd_W_Diff2.compute(new ComputeParameters(false, pnd_W_Diff2), pnd_W_Diff.subtract(ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T)));    //Natural: COMPUTE #W-DIFF2 = #W-DIFF - IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( #T )
                ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T).nadd(pnd_W_Diff2);                                                                       //Natural: ADD #W-DIFF2 TO IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( #T )
                //*       ADD #W-DIFF2 TO IATL171.IAXFR-FROM-RQSTD-XFR-GUAR(#T)
                ldaIatl171.getIatl171_Iaxfr_From_Rqstd_Xfr_Guar().getValue(pnd_T).nsubtract(pnd_W_Diff2);                                                                 //Natural: SUBTRACT #W-DIFF2 FROM IATL171.IAXFR-FROM-RQSTD-XFR-GUAR ( #T )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DJ:                                                                                                                                                               //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_T).equals(" ")))                                                                          //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #T ) = ' '
            {
                if (true) break DJ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( DJ. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_T).equals("G") || pdaIatl420z.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_T).equals("T")) //Natural: IF ( IATL010.XFR-TO-ACCT-CDE ( #T ) = 'G' OR = 'T' ) OR ( IATL010.XFR-TO-UNIT-TYP ( #T ) = 'M' AND IATL010.RCRD-TYPE-CDE = '1' )
                || (pdaIatl420z.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_T).equals("M") && pdaIatl420z.getIatl010_Rcrd_Type_Cde().equals("1")))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Diff.compute(new ComputeParameters(true, pnd_W_Diff), ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_T))); //Natural: COMPUTE ROUNDED #W-DIFF = IATL171.IAXFR-TO-AFTR-XFR-UNITS ( #T ) * IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #T )
                pnd_W_Diff2.compute(new ComputeParameters(false, pnd_W_Diff2), pnd_W_Diff.subtract(ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_T)));      //Natural: COMPUTE #W-DIFF2 = #W-DIFF - IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #T )
                ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_T).nadd(pnd_W_Diff2);                                                                         //Natural: ADD #W-DIFF2 TO IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #T )
                ldaIatl171.getIatl171_Iaxfr_To_Xfr_Guar().getValue(pnd_T).nadd(pnd_W_Diff2);                                                                              //Natural: ADD #W-DIFF2 TO IATL171.IAXFR-TO-XFR-GUAR ( #T )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Monthly_Dollars() throws Exception                                                                                                          //Natural: #FILL-MONTHLY-DOLLARS
    {
        if (BLNatReinput.isReinput()) return;

        LZ:                                                                                                                                                               //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T).equals(" ")))                                                                       //Natural: IF IATL171.IAXFR-FROM-FUND-CDE ( #T ) = ' '
            {
                if (true) break LZ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( LZ. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T).equals("1G") || ldaIatl171.getIatl171_Iaxfr_From_Fund_Cde().getValue(pnd_T).equals("1S"))) //Natural: IF IATL171.IAXFR-FROM-FUND-CDE ( #T ) = '1G' OR = '1S'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIatl171.getIatl171_Iaxfr_Frm_Unit_Typ().getValue(pnd_T).equals("M")))                                                                    //Natural: IF IATL171.IAXFR-FRM-UNIT-TYP ( #T ) = 'M'
                {
                    ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T).compute(new ComputeParameters(true, ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Guar().getValue(pnd_T)),  //Natural: COMPUTE ROUNDED IATL171.IAXFR-FROM-CURRENT-PMT-GUAR ( #T ) = IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( #T ) * IATL171.IAXFR-FROM-CURRENT-PMT-UNITS ( #T )
                        ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_From_Current_Pmt_Units().getValue(pnd_T)));
                    ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(true, ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE ROUNDED IATL171.IAXFR-FROM-AFTR-XFR-GUAR ( #T ) = IATL171.IAXFR-FROM-REVAL-UNIT-VAL ( #T ) * IATL171.IAXFR-FROM-AFTR-XFR-UNITS ( #T )
                        ldaIatl171.getIatl171_Iaxfr_From_Reval_Unit_Val().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_From_Aftr_Xfr_Units().getValue(pnd_T)));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ZL:                                                                                                                                                               //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T).equals(" ")))                                                                         //Natural: IF IATL171.IAXFR-TO-FUND-CDE ( #T ) = ' '
            {
                if (true) break ZL;                                                                                                                                       //Natural: ESCAPE BOTTOM ( ZL. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T).equals("1G") || ldaIatl171.getIatl171_Iaxfr_To_Fund_Cde().getValue(pnd_T).equals("1S"))) //Natural: IF IATL171.IAXFR-TO-FUND-CDE ( #T ) = '1G' OR = '1S'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIatl171.getIatl171_Iaxfr_To_Unit_Typ().getValue(pnd_T).equals("M")))                                                                     //Natural: IF IATL171.IAXFR-TO-UNIT-TYP ( #T ) = 'M'
                {
                    ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(true, ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE ROUNDED IATL171.IAXFR-TO-BFR-XFR-GUAR ( #T ) = IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #T ) * IATL171.IAXFR-TO-BFR-XFR-UNITS ( #T )
                        ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_To_Bfr_Xfr_Units().getValue(pnd_T)));
                    ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_T).compute(new ComputeParameters(true, ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Guar().getValue(pnd_T)),  //Natural: COMPUTE ROUNDED IATL171.IAXFR-TO-AFTR-XFR-GUAR ( #T ) = IATL171.IAXFR-TO-REVAL-UNIT-VAL ( #T ) * IATL171.IAXFR-TO-AFTR-XFR-UNITS ( #T )
                        ldaIatl171.getIatl171_Iaxfr_To_Reval_Unit_Val().getValue(pnd_T).multiply(ldaIatl171.getIatl171_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_T)));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Aian013_Linkage() throws Exception                                                                                                             //Natural: WRITE-AIAN013-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "*** AIAN013 LINKAGE VALUES ***");                                                                                                          //Natural: WRITE '*** AIAN013 LINKAGE VALUES ***'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),    //Natural: WRITE '=' #TRANSFER-REVAL-SWITCH / '=' #CONTRACT-NUMBER '=' #PAYEE-CODE / '=' #FUND-CODE / '=' #AIAN013-LINKAGE.#MODE / '=' #AIAN013-LINKAGE.#OPTION / '=' #AIAN013-LINKAGE.#ORIGIN / '=' #ISSUE-DATE / '=' #AIAN013-LINKAGE.#FINAL-PER-PAY-DATE / '=' #AIAN013-LINKAGE.#FIRST-ANN-DOB / '=' #AIAN013-LINKAGE.#FIRST-ANN-SEX / '=' #AIAN013-LINKAGE.#SECOND-ANN-DOB / '=' #AIAN013-LINKAGE.#SECOND-ANN-SEX / '=' #AIAN013-LINKAGE.#TRANSFER-EFFECTIVE-DATE / '=' #AIAN013-LINKAGE.#TYPE-OF-RUN / '=' #REVALUATION-INDICATOR / '=' #AIAN013-LINKAGE.#PROCESSING-DATE / '=' #ILLUSTRATION-EFF-DATE / '=' #TRANSFER-UNITS /
            "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode(),
            NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Origin(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date(),
            NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob(),NEWLINE,
            "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex(),
            NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run(),
            NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date(),
            NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units(),
            NEWLINE);
        if (Global.isEscape()) return;
        FOR09:                                                                                                                                                            //Natural: FOR #A 1 21
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(21)); pnd_A.nadd(1))
        {
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_A).equals(" ")))                                                          //Natural: IF #FUND-TO-RECEIVE ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_A),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_A), //Natural: WRITE '=' #FUND-TO-RECEIVE ( #A ) / '=' #UNITS-TO-RECEIVE ( #A ) / '=' #PMTS-TO-RECEIVE ( 1:3 ) /
                NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1,":",3),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Eff_Date_Out(),     //Natural: WRITE '=' #AIAN013-LINKAGE.#NEXT-PYMNT-DTE / '=' #TRANSFER-EFF-DATE-OUT /
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        FOR10:                                                                                                                                                            //Natural: FOR #A 1 19
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(19)); pnd_A.nadd(1))
        {
            if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_A).equals(" ")))                                                            //Natural: IF #FUND-CODE-OUT ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(pnd_A),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(pnd_A), //Natural: WRITE '=' #FUND-CODE-OUT ( #A ) / '=' #UNITS-OUT ( #A ) / '=' #AUV-OUT ( #A ) / '=' #TRANSFER-AMT-OUT-CREF ( #A )
                NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(pnd_A),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(pnd_A));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  040612
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(1,":",2),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(1,":",2),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(1,":",2),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(1,":",2),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(1,":",2),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out(),NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr(),  //Natural: WRITE '=' #PMT-METHOD-CODE-OUT ( 1:2 ) / '=' #RATE-CODE-OUT ( 1:2 ) / '=' #GTD-PMT-OUT ( 1:2 ) / '=' #DVD-PMT-OUT ( 1:2 ) / '=' #TRANSFER-AMT-OUT-TIAA ( 1:2 ) / '=' #TOTAL-TRANSFER-AMT-OUT / '=' #AIAN013-LINKAGE.#RETURN-CODE-NBR ( EM = Z99 )
            new ReportEditMask ("Z99"));
        if (Global.isEscape()) return;
    }
    private void sub_Display_Aian079_Linkage() throws Exception                                                                                                           //Natural: DISPLAY-AIAN079-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, NEWLINE,"*** AIAN079 LINKAGE VALUES ***");                                                                                                  //Natural: WRITE / '*** AIAN079 LINKAGE VALUES ***'
        if (Global.isEscape()) return;
        //*  040612
        getReports().write(0, "=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Contract(),NEWLINE,"=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Origin(),            //Natural: WRITE '=' #AIAA079-CONTRACT / '=' #AIAA079-ORIGIN ( EM = 99 ) / '=' #AIAA079-OPTION ( EM = 99 ) / '=' #AIAA079-YOUNGEST-DOB / '=' #AIAA079-FINAL-PAYT-PRD / '=' #AIAA079-CALL-TYPE / '=' #AIAA079-EFFECTIVE-DATE /
            new ReportEditMask ("99"),NEWLINE,"=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Option(), new ReportEditMask ("99"),NEWLINE,"=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Youngest_Dob(),
            NEWLINE,"=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Final_Payt_Prd(),NEWLINE,"=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Call_Type(),
            NEWLINE,"=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Effective_Date(),NEWLINE);
        if (Global.isEscape()) return;
        FOR11:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATE
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rate)); pnd_A.nadd(1))
        {
            if (condition(pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Rate_In().getValue(pnd_A).equals(" ")))                                                           //Natural: IF #AIAA079-RATE-IN ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Rate_In().getValue(pnd_A));                                                           //Natural: WRITE '=' #AIAA079-RATE-IN ( #A )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  040612
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR12:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATE
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rate)); pnd_A.nadd(1))
        {
            if (condition(pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Rate_Out().getValue(pnd_A).equals(" ")))                                                          //Natural: IF #AIAA079-RATE-OUT ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Rate_Out().getValue(pnd_A),NEWLINE,"=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_S_G_Code().getValue(pnd_A)); //Natural: WRITE #AIAA079-RATE-OUT ( #A ) / '=' #AIAA079-S-G-CODE ( #A )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Return_Code_Nbr(), new ReportEditMask ("999"));                                           //Natural: WRITE '=' #AIAA079-RETURN-CODE-NBR ( EM = 999 )
        if (Global.isEscape()) return;
    }
    private void sub_Display_Aian014_Linkage() throws Exception                                                                                                           //Natural: DISPLAY-AIAN014-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, NEWLINE,"*** AIAN014 LINKAGE VALUES ***");                                                                                                  //Natural: WRITE / '*** AIAN014 LINKAGE VALUES ***'
        if (Global.isEscape()) return;
        //*  040612
        getReports().write(0, "=",pdaIata002.getIata002_Pnd_Contract_Payee(),NEWLINE,"=",pdaIata002.getIata002_Pnd_Mode(), new ReportEditMask ("999"),NEWLINE,"=",pdaIata002.getIata002_Pnd_Origin(),  //Natural: WRITE '=' IATA002.#CONTRACT-PAYEE / '=' IATA002.#MODE ( EM = 999 ) / '=' IATA002.#ORIGIN ( EM = 99 ) / '=' IATA002.#OPTION ( EM = 99 ) / '=' #ISSUE-DATE-8 / '=' IATA002.#FINAL-PER-PAY-DATE / '=' IATA002.#FIRST-ANN-DOB / '=' IATA002.#FIRST-ANN-SEX / '=' IATA002.#FIRST-ANN-DOD / '=' IATA002.#SECOND-ANN-DOB / '=' IATA002.#SECOND-ANN-SEX / '=' IATA002.#SECOND-ANN-DOD / '=' IATA002.#TRANSFER-EFFECTIVE-DATE / '=' IATA002.#TYPE-OF-RUN / '=' IATA002.#PROCESSING-DATE /
            new ReportEditMask ("99"),NEWLINE,"=",pdaIata002.getIata002_Pnd_Option(), new ReportEditMask ("99"),NEWLINE,"=",pdaIata002.getIata002_Pnd_Issue_Date_8(),
            NEWLINE,"=",pdaIata002.getIata002_Pnd_Final_Per_Pay_Date(),NEWLINE,"=",pdaIata002.getIata002_Pnd_First_Ann_Dob(),NEWLINE,"=",pdaIata002.getIata002_Pnd_First_Ann_Sex(),
            NEWLINE,"=",pdaIata002.getIata002_Pnd_First_Ann_Dod(),NEWLINE,"=",pdaIata002.getIata002_Pnd_Second_Ann_Dob(),NEWLINE,"=",pdaIata002.getIata002_Pnd_Second_Ann_Sex(),
            NEWLINE,"=",pdaIata002.getIata002_Pnd_Second_Ann_Dod(),NEWLINE,"=",pdaIata002.getIata002_Pnd_Transfer_Effective_Date(),NEWLINE,"=",pdaIata002.getIata002_Pnd_Type_Of_Run(),
            NEWLINE,"=",pdaIata002.getIata002_Pnd_Processing_Date(),NEWLINE);
        if (Global.isEscape()) return;
        FOR13:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATE
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rate)); pnd_A.nadd(1))
        {
            if (condition(pdaIata002.getIata002_Pnd_Rate_Code_Grd().getValue(pnd_A).equals(" ")))                                                                         //Natural: IF #RATE-CODE-GRD ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaIata002.getIata002_Pnd_Rate_Code_Grd().getValue(pnd_A),NEWLINE,"=",pdaIata002.getIata002_Pnd_Gtd_Pmt_Grd().getValue(pnd_A),NEWLINE,"=",pdaIata002.getIata002_Pnd_Dvd_Pmt_Grd().getValue(pnd_A),NEWLINE,"=",pdaIata002.getIata002_Pnd_Rate_Date_Grd().getValue(pnd_A),  //Natural: WRITE '=' #RATE-CODE-GRD ( #A ) / '=' #GTD-PMT-GRD ( #A ) / '=' #DVD-PMT-GRD ( #A ) / '=' #RATE-DATE-GRD ( #A ) ( EM = YYYYMMDD )
                new ReportEditMask ("YYYYMMDD"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  040612
        getReports().write(0, "=",pdaIata002.getIata002_Pnd_Next_Pymnt_Dte(), new ReportEditMask ("YYYYMMDD"),NEWLINE);                                                   //Natural: WRITE '=' IATA002.#NEXT-PYMNT-DTE ( EM = YYYYMMDD ) /
        if (Global.isEscape()) return;
        FOR14:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATE
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rate)); pnd_A.nadd(1))
        {
            if (condition(pdaIata002.getIata002_Pnd_Rate_Code_Std().getValue(pnd_A).equals(" ")))                                                                         //Natural: IF #RATE-CODE-STD ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaIata002.getIata002_Pnd_Rate_Code_Std().getValue(pnd_A),NEWLINE,"=",pdaIata002.getIata002_Pnd_Gtd_Pmt_Std().getValue(pnd_A),      //Natural: WRITE '=' #RATE-CODE-STD ( #A ) / '=' #GTD-PMT-STD ( #A ) / '=' #DVD-PMT-STD ( #A ) /
                NEWLINE,"=",pdaIata002.getIata002_Pnd_Dvd_Pmt_Std().getValue(pnd_A),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaIata002.getIata002_Pnd_Tot_Trnsfr_Amt());                                                                                            //Natural: WRITE '=' #TOT-TRNSFR-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaIata002.getIata002_Pnd_Return_Code_Nbr(), new ReportEditMask ("999"),NEWLINE);                                                       //Natural: WRITE '=' IATA002.#RETURN-CODE-NBR ( EM = 999 ) /
        if (Global.isEscape()) return;
    }
    private void sub_Display_Aian026_Linkage() throws Exception                                                                                                           //Natural: DISPLAY-AIAN026-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "******* AIAN026 LINKAGE ************");                                                                                                    //Natural: WRITE '******* AIAN026 LINKAGE ************'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_A26_Fields_Pnd_A26_Call_Type,NEWLINE,"=",pnd_A26_Fields_Pnd_A26_Fnd_Cde,NEWLINE,"=",pnd_A26_Fields_Pnd_A26_Reval_Meth,              //Natural: WRITE '=' #A26-CALL-TYPE / '=' #A26-FND-CDE / '=' #A26-REVAL-METH / '=' #A26-REQ-DTE / '=' #A26-PRTC-DTE / '=' #RC / '=' #AUV / '=' #AUV-RET-DTE / '=' #DAYS-IN-REQUEST-MONTH / '=' #DAYS-IN-PARTICIP-MONTH
            NEWLINE,"=",pnd_A26_Fields_Pnd_A26_Req_Dte,NEWLINE,"=",pnd_A26_Fields_Pnd_A26_Prtc_Dte,NEWLINE,"=",pnd_A26_Fields_Pnd_Rc,NEWLINE,"=",pnd_A26_Fields_Pnd_Auv,
            NEWLINE,"=",pnd_A26_Fields_Pnd_Auv_Ret_Dte,NEWLINE,"=",pnd_A26_Fields_Pnd_Days_In_Request_Month,NEWLINE,"=",pnd_A26_Fields_Pnd_Days_In_Particip_Month);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
                                                                                                                                                                          //Natural: PERFORM WRITE-AIAN013-LINKAGE
        sub_Write_Aian013_Linkage();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
        Global.format(0, "LS=133 PS=56");
    }
}
