/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:41:45 AM
**        * FROM NATURAL SUBPROGRAM : Iaap110c
************************************************************
**        * FILE NAME            : Iaap110c.java
**        * CLASS NAME           : Iaap110c
**        * INSTANCE NAME        : Iaap110c
************************************************************
***********************************************************************
*                                                                     *
*   PROGRAM     -  IAAP110C    PRINT ROLLOVER MODE REPORTS CALLED BY  *
*      DATE     -  10/01       IAAP110C                               *
*    AUTHOR     -  J.B                                                *
*                                                                     *
*                                                                     *
*   HISTORY     -                                                     *
*     6/03         ADDED NEW ROLL DEST CODES 57BT & 57BC              *
*                  DO SCAN ON 6/03                                    *
***********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap110c extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Eof_Sw;
    private DbsField pnd_Mode_1;
    private DbsField pnd_I_Cnt_Nbr;
    private DbsField pnd_I_Cnt_Curr_Dist_Cde;
    private DbsField pnd_I_Cntrct_Optn_Code;
    private DbsField pnd_I_Tiaa_Per_Pay_Amt;
    private DbsField pnd_I_Tiaa_Per_Div_Amt;
    private DbsField pnd_I_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_I_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Mode_Table_2;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Tiaa_Title;
    private DbsField pnd_I;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot;
    private DbsField pnd_Tiaa_Per_Div_Tot;
    private DbsField pnd_Tiaa_Fin_Pay_Tot;
    private DbsField pnd_Tiaa_Fin_Div_Tot;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_G;
    private DbsField pnd_Tiaa_Per_Div_Tot_G;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_G;
    private DbsField pnd_Tiaa_Fin_Div_Tot_G;
    private DbsField pnd_Rollover_Title;
    private DbsField pnd_Heading1;
    private DbsField pls_Pnd_Tiaa_Table_4_R;
    private DbsField pls_Pnd_Tiaa_Table_Tot_4_R;
    private DbsField pls_Pnd_Tiaa_Table_5_R;
    private DbsField pls_Pnd_Tiaa_Table_Tot_5_R;
    private DbsField pls_Pnd_Tiaa_Table_6_R;
    private DbsField pls_Pnd_Tiaa_Table_Tot_6_R;
    private DbsField pls_Pnd_Tiaa_Table_7_R;
    private DbsField pls_Pnd_Tiaa_Table_Tot_7_R;
    private DbsField pls_Pnd_Tiaa_Table_8_R;
    private DbsField pls_Pnd_Tiaa_Table_Tot_8_R;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Eof_Sw = parameters.newFieldInRecord("pnd_Eof_Sw", "#EOF-SW", FieldType.STRING, 1);
        pnd_Eof_Sw.setParameterOption(ParameterOption.ByReference);
        pnd_Mode_1 = parameters.newFieldInRecord("pnd_Mode_1", "#MODE-1", FieldType.NUMERIC, 2);
        pnd_Mode_1.setParameterOption(ParameterOption.ByReference);
        pnd_I_Cnt_Nbr = parameters.newFieldInRecord("pnd_I_Cnt_Nbr", "#I-CNT-NBR", FieldType.STRING, 10);
        pnd_I_Cnt_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_I_Cnt_Curr_Dist_Cde = parameters.newFieldInRecord("pnd_I_Cnt_Curr_Dist_Cde", "#I-CNT-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_I_Cnt_Curr_Dist_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_I_Cntrct_Optn_Code = parameters.newFieldInRecord("pnd_I_Cntrct_Optn_Code", "#I-CNTRCT-OPTN-CODE", FieldType.NUMERIC, 2);
        pnd_I_Cntrct_Optn_Code.setParameterOption(ParameterOption.ByReference);
        pnd_I_Tiaa_Per_Pay_Amt = parameters.newFieldInRecord("pnd_I_Tiaa_Per_Pay_Amt", "#I-TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_I_Tiaa_Per_Pay_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_I_Tiaa_Per_Div_Amt = parameters.newFieldInRecord("pnd_I_Tiaa_Per_Div_Amt", "#I-TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_I_Tiaa_Per_Div_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_I_Tiaa_Rate_Final_Pay_Amt = parameters.newFieldInRecord("pnd_I_Tiaa_Rate_Final_Pay_Amt", "#I-TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_I_Tiaa_Rate_Final_Pay_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_I_Tiaa_Rate_Final_Div_Amt = parameters.newFieldInRecord("pnd_I_Tiaa_Rate_Final_Div_Amt", "#I-TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_I_Tiaa_Rate_Final_Div_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Mode_Table_2 = parameters.newFieldArrayInRecord("pnd_Mode_Table_2", "#MODE-TABLE-2", FieldType.STRING, 3, new DbsArrayController(1, 22));
        pnd_Mode_Table_2.setParameterOption(ParameterOption.ByReference);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 4);
        pnd_Page_Number.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Tiaa_Title = localVariables.newFieldInRecord("pnd_Tiaa_Title", "#TIAA-TITLE", FieldType.STRING, 30);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte = localVariables.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 10);

        pnd_Payment_Due_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_1", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy = pnd_Payment_Due_Dte__R_Field_1.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy", "#PAYMENT-DUE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Tiaa_Cnt_Amt_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot", "#TIAA-CNT-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Per_Div_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot", "#TIAA-PER-DIV-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Fin_Pay_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot", "#TIAA-FIN-PAY-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Fin_Div_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot", "#TIAA-FIN-DIV-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Cnt_Amt_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_G", "#TIAA-CNT-AMT-TOT-G", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Per_Div_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot_G", "#TIAA-PER-DIV-TOT-G", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Fin_Pay_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_G", "#TIAA-FIN-PAY-TOT-G", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Fin_Div_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_G", "#TIAA-FIN-DIV-TOT-G", FieldType.NUMERIC, 11, 2);
        pnd_Rollover_Title = localVariables.newFieldInRecord("pnd_Rollover_Title", "#ROLLOVER-TITLE", FieldType.STRING, 20);
        pnd_Heading1 = localVariables.newFieldInRecord("pnd_Heading1", "#HEADING1", FieldType.STRING, 69);
        pls_Pnd_Tiaa_Table_4_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_4_R", "+#TIAA-TABLE-4-R", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_Tot_4_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_Tot_4_R", "+#TIAA-TABLE-TOT-4-R", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_5_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_5_R", "+#TIAA-TABLE-5-R", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_Tot_5_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_Tot_5_R", "+#TIAA-TABLE-TOT-5-R", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_6_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_6_R", "+#TIAA-TABLE-6-R", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_Tot_6_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_Tot_6_R", "+#TIAA-TABLE-TOT-6-R", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_7_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_7_R", "+#TIAA-TABLE-7-R", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_Tot_7_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_Tot_7_R", "+#TIAA-TABLE-TOT-7-R", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_8_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_8_R", "+#TIAA-TABLE-8-R", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 22, 1, 4));
        pls_Pnd_Tiaa_Table_Tot_8_R = WsIndependent.getInstance().newFieldArrayInRecord("pls_Pnd_Tiaa_Table_Tot_8_R", "+#TIAA-TABLE-TOT-8-R", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 22, 1, 4));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Heading1.setInitialValue("IA ADMINISTRATION - ANNUAL REVALUATION MODE CONTROL FOR PAYMENTS DUE ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaap110c() throws Exception
    {
        super("Iaap110c");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP110C", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *=====================================================================
        //*                     SET AT TOP OF PAGE
        //* *=====================================================================
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(pnd_Eof_Sw.equals("B")))                                                                                                                            //Natural: IF #EOF-SW = 'B'
        {
                                                                                                                                                                          //Natural: PERFORM #CALCULATE-TOTALS
            sub_Pnd_Calculate_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Eof_Sw.equals("E")))                                                                                                                        //Natural: IF #EOF-SW = 'E'
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
                sub_Pnd_Write_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CALCULATE-TOTALS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT
        //* ****************** ON ERROR **************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Pnd_Calculate_Totals() throws Exception                                                                                                              //Natural: #CALCULATE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  ADDED 6/03
        if (condition(pnd_I_Cnt_Curr_Dist_Cde.equals("IRAT") || pnd_I_Cnt_Curr_Dist_Cde.equals("IRAC") || pnd_I_Cnt_Curr_Dist_Cde.equals("03BT") || pnd_I_Cnt_Curr_Dist_Cde.equals("03BC")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BC' OR = '57BT'
            || pnd_I_Cnt_Curr_Dist_Cde.equals("QPLT") || pnd_I_Cnt_Curr_Dist_Cde.equals("QPLC") || pnd_I_Cnt_Curr_Dist_Cde.equals("57BC") || pnd_I_Cnt_Curr_Dist_Cde.equals("57BT")))
        {
            //*  TPA
            short decideConditionsMet137 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #I-CNTRCT-OPTN-CODE;//Natural: VALUE 28,30
            if (condition((pnd_I_Cntrct_Optn_Code.equals(28) || pnd_I_Cntrct_Optn_Code.equals(30))))
            {
                decideConditionsMet137++;
                pls_Pnd_Tiaa_Table_4_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-4-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_4_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-4-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_4_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-4-R ( #MODE-1,3 )
                pls_Pnd_Tiaa_Table_4_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-4-R ( #MODE-1,4 )
                pls_Pnd_Tiaa_Table_Tot_4_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                           //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-TOT-4-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_Tot_4_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                           //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-TOT-4-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_Tot_4_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-TOT-4-R ( #MODE-1,3 )
                //*  IPRO
                pls_Pnd_Tiaa_Table_Tot_4_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-TOT-4-R ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: VALUE 25,27
            else if (condition((pnd_I_Cntrct_Optn_Code.equals(25) || pnd_I_Cntrct_Optn_Code.equals(27))))
            {
                decideConditionsMet137++;
                pls_Pnd_Tiaa_Table_5_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-5-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_5_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-5-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_5_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-5-R ( #MODE-1,3 )
                pls_Pnd_Tiaa_Table_5_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-5-R ( #MODE-1,4 )
                pls_Pnd_Tiaa_Table_Tot_5_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                           //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-TOT-5-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_Tot_5_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                           //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-TOT-5-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_Tot_5_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-TOT-5-R ( #MODE-1,3 )
                //*  P&I
                pls_Pnd_Tiaa_Table_Tot_5_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-TOT-5-R ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: VALUE 22
            else if (condition((pnd_I_Cntrct_Optn_Code.equals(22))))
            {
                decideConditionsMet137++;
                pls_Pnd_Tiaa_Table_6_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-6-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_6_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-6-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_6_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-6-R ( #MODE-1,3 )
                pls_Pnd_Tiaa_Table_6_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-6-R ( #MODE-1,4 )
                pls_Pnd_Tiaa_Table_Tot_6_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                           //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-TOT-6-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_Tot_6_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                           //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-TOT-6-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_Tot_6_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-TOT-6-R ( #MODE-1,3 )
                //*  ANNUITY CERTAIN
                pls_Pnd_Tiaa_Table_Tot_6_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-TOT-6-R ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pls_Pnd_Tiaa_Table_8_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-8-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_8_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-8-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_8_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-8-R ( #MODE-1,3 )
                pls_Pnd_Tiaa_Table_8_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-8-R ( #MODE-1,4 )
                pls_Pnd_Tiaa_Table_Tot_8_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                           //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-TOT-8-R ( #MODE-1,1 )
                pls_Pnd_Tiaa_Table_Tot_8_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                           //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-TOT-8-R ( #MODE-1,2 )
                pls_Pnd_Tiaa_Table_Tot_8_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-TOT-8-R ( #MODE-1,3 )
                pls_Pnd_Tiaa_Table_Tot_8_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                    //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-TOT-8-R ( #MODE-1,4 )
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  TPA REINVESTMENTS
        if (condition(((pnd_I_Cntrct_Optn_Code.equals(28) || pnd_I_Cntrct_Optn_Code.equals(30)) && pnd_I_Cnt_Curr_Dist_Cde.equals("RINV"))))                              //Natural: IF #I-CNTRCT-OPTN-CODE = 28 OR = 30 AND #I-CNT-CURR-DIST-CDE = 'RINV'
        {
            pls_Pnd_Tiaa_Table_7_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                                   //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-7-R ( #MODE-1,1 )
            pls_Pnd_Tiaa_Table_7_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                                   //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-7-R ( #MODE-1,2 )
            pls_Pnd_Tiaa_Table_7_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                            //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-7-R ( #MODE-1,3 )
            pls_Pnd_Tiaa_Table_7_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                            //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-7-R ( #MODE-1,4 )
            pls_Pnd_Tiaa_Table_Tot_7_R.getValue(pnd_Mode_1,1).nadd(pnd_I_Tiaa_Per_Pay_Amt);                                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO +#TIAA-TABLE-TOT-7-R ( #MODE-1,1 )
            pls_Pnd_Tiaa_Table_Tot_7_R.getValue(pnd_Mode_1,2).nadd(pnd_I_Tiaa_Per_Div_Amt);                                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO +#TIAA-TABLE-TOT-7-R ( #MODE-1,2 )
            pls_Pnd_Tiaa_Table_Tot_7_R.getValue(pnd_Mode_1,3).nadd(pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO +#TIAA-TABLE-TOT-7-R ( #MODE-1,3 )
            pls_Pnd_Tiaa_Table_Tot_7_R.getValue(pnd_Mode_1,4).nadd(pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO +#TIAA-TABLE-TOT-7-R ( #MODE-1,4 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Write_Report() throws Exception                                                                                                                  //Natural: #WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Tiaa_Title.setValue("TIAA TPA ROLLOVER");                                                                                                                     //Natural: MOVE 'TIAA TPA ROLLOVER' TO #TIAA-TITLE
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(13),pls_Pnd_Tiaa_Table_4_R.getValue(pnd_I,1), //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 13X +#TIAA-TABLE-4-R ( #I,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-4-R ( #I,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-4-R ( #I,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X +#TIAA-TABLE-4-R ( #I,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pls_Pnd_Tiaa_Table_4_R.getValue(pnd_I,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(14),pls_Pnd_Tiaa_Table_4_R.getValue(pnd_I,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pls_Pnd_Tiaa_Table_4_R.getValue(pnd_I,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pls_Pnd_Tiaa_Table_4_R.getValue("*",1));                                                                                                //Natural: ADD +#TIAA-TABLE-4-R ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pls_Pnd_Tiaa_Table_4_R.getValue("*",2));                                                                                                //Natural: ADD +#TIAA-TABLE-4-R ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pls_Pnd_Tiaa_Table_4_R.getValue("*",3));                                                                                                //Natural: ADD +#TIAA-TABLE-4-R ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pls_Pnd_Tiaa_Table_4_R.getValue("*",4));                                                                                                //Natural: ADD +#TIAA-TABLE-4-R ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(11),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 11X #TIAA-CNT-AMT-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-PER-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-PAY-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X #TIAA-FIN-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(14),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Tiaa_Title.setValue("TIAA IPRO ROLLOVER");                                                                                                                    //Natural: MOVE 'TIAA IPRO ROLLOVER' TO #TIAA-TITLE
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(13),pls_Pnd_Tiaa_Table_5_R.getValue(pnd_I,1), //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 13X +#TIAA-TABLE-5-R ( #I,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-5-R ( #I,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-5-R ( #I,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X +#TIAA-TABLE-5-R ( #I,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pls_Pnd_Tiaa_Table_5_R.getValue(pnd_I,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(14),pls_Pnd_Tiaa_Table_5_R.getValue(pnd_I,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pls_Pnd_Tiaa_Table_5_R.getValue(pnd_I,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pls_Pnd_Tiaa_Table_5_R.getValue("*",1));                                                                                                //Natural: ADD +#TIAA-TABLE-5-R ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pls_Pnd_Tiaa_Table_5_R.getValue("*",2));                                                                                                //Natural: ADD +#TIAA-TABLE-5-R ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pls_Pnd_Tiaa_Table_5_R.getValue("*",3));                                                                                                //Natural: ADD +#TIAA-TABLE-5-R ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pls_Pnd_Tiaa_Table_5_R.getValue("*",4));                                                                                                //Natural: ADD +#TIAA-TABLE-5-R ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(11),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 11X #TIAA-CNT-AMT-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-PER-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-PAY-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X #TIAA-FIN-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(14),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        pnd_Tiaa_Title.setValue("TIAA P&I ROLLOVER");                                                                                                                     //Natural: MOVE 'TIAA P&I ROLLOVER' TO #TIAA-TITLE
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(13),pls_Pnd_Tiaa_Table_6_R.getValue(pnd_I,1), //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 13X +#TIAA-TABLE-6-R ( #I,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-6-R ( #I,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-6-R ( #I,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X +#TIAA-TABLE-6-R ( #I,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pls_Pnd_Tiaa_Table_6_R.getValue(pnd_I,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(14),pls_Pnd_Tiaa_Table_6_R.getValue(pnd_I,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pls_Pnd_Tiaa_Table_6_R.getValue(pnd_I,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pls_Pnd_Tiaa_Table_6_R.getValue("*",1));                                                                                                //Natural: ADD +#TIAA-TABLE-6-R ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pls_Pnd_Tiaa_Table_6_R.getValue("*",2));                                                                                                //Natural: ADD +#TIAA-TABLE-6-R ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pls_Pnd_Tiaa_Table_6_R.getValue("*",3));                                                                                                //Natural: ADD +#TIAA-TABLE-6-R ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pls_Pnd_Tiaa_Table_6_R.getValue("*",4));                                                                                                //Natural: ADD +#TIAA-TABLE-6-R ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(11),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 11X #TIAA-CNT-AMT-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-PER-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-PAY-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X #TIAA-FIN-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(14),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        pnd_Tiaa_Title.setValue("TIAA TPA REINVESTMENTS");                                                                                                                //Natural: MOVE 'TIAA TPA REINVESTMENTS' TO #TIAA-TITLE
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(13),pls_Pnd_Tiaa_Table_7_R.getValue(pnd_I,1), //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 13X +#TIAA-TABLE-7-R ( #I,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-7-R ( #I,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-7-R ( #I,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X +#TIAA-TABLE-7-R ( #I,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pls_Pnd_Tiaa_Table_7_R.getValue(pnd_I,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(14),pls_Pnd_Tiaa_Table_7_R.getValue(pnd_I,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pls_Pnd_Tiaa_Table_7_R.getValue(pnd_I,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pls_Pnd_Tiaa_Table_7_R.getValue("*",1));                                                                                                //Natural: ADD +#TIAA-TABLE-7-R ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pls_Pnd_Tiaa_Table_7_R.getValue("*",2));                                                                                                //Natural: ADD +#TIAA-TABLE-7-R ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pls_Pnd_Tiaa_Table_7_R.getValue("*",3));                                                                                                //Natural: ADD +#TIAA-TABLE-7-R ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pls_Pnd_Tiaa_Table_7_R.getValue("*",4));                                                                                                //Natural: ADD +#TIAA-TABLE-7-R ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(11),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 11X #TIAA-CNT-AMT-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-PER-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-PAY-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X #TIAA-FIN-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(14),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        pnd_Tiaa_Title.setValue("TIAA AC ROLLOVERS");                                                                                                                     //Natural: MOVE 'TIAA AC ROLLOVERS' TO #TIAA-TITLE
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(13),pls_Pnd_Tiaa_Table_8_R.getValue(pnd_I,1), //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 13X +#TIAA-TABLE-8-R ( #I,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-8-R ( #I,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X +#TIAA-TABLE-8-R ( #I,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X +#TIAA-TABLE-8-R ( #I,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pls_Pnd_Tiaa_Table_8_R.getValue(pnd_I,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(14),pls_Pnd_Tiaa_Table_8_R.getValue(pnd_I,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pls_Pnd_Tiaa_Table_8_R.getValue(pnd_I,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pls_Pnd_Tiaa_Table_8_R.getValue("*",1));                                                                                                //Natural: ADD +#TIAA-TABLE-8-R ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pls_Pnd_Tiaa_Table_8_R.getValue("*",2));                                                                                                //Natural: ADD +#TIAA-TABLE-8-R ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pls_Pnd_Tiaa_Table_8_R.getValue("*",3));                                                                                                //Natural: ADD +#TIAA-TABLE-8-R ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pls_Pnd_Tiaa_Table_8_R.getValue("*",4));                                                                                                //Natural: ADD +#TIAA-TABLE-8-R ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(11),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 11X #TIAA-CNT-AMT-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-PER-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-PAY-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X #TIAA-FIN-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(14),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' #PAGE-NUMBER
                        TabSetting(121),"PAGE ",pnd_Page_Number);
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(62),pnd_Tiaa_Title);         //Natural: WRITE ( 1 ) '   DATE ' *DATX ( EM = MM/DD/YYYY ) 62T #TIAA-TITLE
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(21),"CONTRACTUAL ",new TabSetting(53),"PERIODIC",new TabSetting(82)," FINAL",new            //Natural: WRITE ( 1 ) 021T 'CONTRACTUAL ' 053T 'PERIODIC' 082T ' FINAL' 112T '  FINAL'
                        TabSetting(112),"  FINAL");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(21),"  AMOUNT    ",new TabSetting(53),"DIVIDEND",new               //Natural: WRITE ( 1 ) 003T 'MODE' 021T '  AMOUNT    ' 053T 'DIVIDEND' 082T 'PAYMENT' 112T 'DIVIDEND'
                        TabSetting(82),"PAYMENT",new TabSetting(112),"DIVIDEND");
                    //*  MODE
                    //*  CONTRACTUAL AMOUNT
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(19),"--------------",new TabSetting(47),"--------------",new       //Natural: WRITE ( 1 ) 003T '----' 019T '--------------' 047T '--------------' 075T '--------------' 106T '--------------'
                        TabSetting(75),"--------------",new TabSetting(106),"--------------");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    //*  PAGE 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
