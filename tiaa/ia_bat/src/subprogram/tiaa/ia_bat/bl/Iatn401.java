/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:44:24 AM
**        * FROM NATURAL SUBPROGRAM : Iatn401
************************************************************
**        * FILE NAME            : Iatn401.java
**        * CLASS NAME           : Iatn401
**        * INSTANCE NAME        : Iatn401
************************************************************
***********************************************************************
*                PROCESSING OF TRANSFER/SWITCH REQUESTS
*
*   PROGRAM    :- IATN401
*   SYSTEM     :- IA
*   AUTHOR     :- LEN BERNSTEIN
*   DATE       :- 11/26/1997
*
*   DESCRIPTION
*   -----------
*   THIS SUB-PROGRAM WILL TRY TO COMPLETE A TRANSFER OR SWITCH REQUEST
*   USING A RQST ID.
*   THIS SUB-PROGRAM CALLS IATN420 TO PERFORM ALL DB03 CHANGES.
*   ALL NECESSARY DB045 CHANGES ARE PERFORMED IN THIS SUB-PROGRAM.
*   THIS SUB-PROGRAM PERFORMS THE ET AND BT.
*
*   ERROR NBRS:-
*   ------------
*   '102' RQST RETRY COUNT >= NUM RETRY
*   '150' RETURNED FROM IATN420 WITH AN ERROR
*
*   HISTORY
*   -------
*   08/01/99 - ARI GROSSMAN
*      STORING "NEXT YEARS" REQUEST RECORD FOR REPETITVE TEACHER TO
*      CREF TRANSFERS.
*
*   01/23/01 - JUN TINIO
*      INCREASED THE NUMBER OF TIMES TO ATTEMPT TO FIND A VALID
*      BUSINESS DATE WHEN CREATING THE NEXT SUBSEQUENT REQUEST FOR
*      REPETITIVE TRANSFERS. THE LIMIT IS SET TO 10 DAYS FROM THE
*      THE ORIGINAL REQUEST's effective date. The reason for the limit
*      IS TO PREVENT A PERPETUAL LOOP IF THE CALENDAR FILE IS EMPTY OR
*      NOT CURRENT. THE INCREASE IN THE LOOP IS TO ENSURE THAT IF THE
*      EFFECTIVE DATE FALLS ON A HOLIDAY MONDAY, IT WILL STILL FIND A
*      VALID BUSINESS DATE.
*
*   05/24/02 - T. DIAZ
*      CHANGE FIND TO A READ TO PROCESS RECORDS WITH SAME MIT LOG DTE
*
*   07/24/02 - T. DIAZ
*      CHANGE ROUTINE TO JUST GET ISN OF CONTRACTS PASSED BY IATP401
*
*   07/27/02 - J. TINIO - SEE JT COMMENT
*      FIX BUG CREATED BY 07/24/02 CHANGE
*   05/31/11 - J. TINIO - MEOW CHANGES
*   04/06/12 - O. SOTTO - RATE BASE EXPANSION CHANGES. SC 040612.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* JUL 2017 O SOTTO ADDITIONAL PIN EXP CHANGES MARKED 082017.
*   02/26/21 - J. TINIO - ENSURE RQST-ID IS UNIQUE FOR THE NEXT
*                         TIAA TO CREF TRANSFER REQUEST - 022021
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn401 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaIatl401p pdaIatl401p;
    private PdaIata700 pdaIata700;
    private PdaIatl400p pdaIatl400p;
    private PdaIatl420z pdaIatl420z;
    private PdaIatl403p pdaIatl403p;
    private PdaAiaa026 pdaAiaa026;
    private PdaIaapda_M pdaIaapda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Fnd1_Isn;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Srce;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Ssn;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_New_Issue;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull;

    private DataAccessProgramView vw_hist_View;
    private DbsField hist_View_Rqst_Id;

    private DataAccessProgramView vw_dup_Rqst;
    private DbsField dup_Rqst_Rcrd_Type_Cde;
    private DbsField dup_Rqst_Rqst_Effctv_Dte;
    private DbsField dup_Rqst_Ia_Frm_Cntrct;
    private DbsField dup_Rqst_Ia_Frm_Payee;
    private DbsField dup_Rqst_Xfr_Stts_Cde;
    private DbsField pnd_Dup_Super_De_04;

    private DbsGroup pnd_Dup_Super_De_04__R_Field_1;
    private DbsField pnd_Dup_Super_De_04__Filler1;
    private DbsField pnd_Dup_Super_De_04_Pnd_Dup_Frm_Cntrct;
    private DbsField pnd_Dup_Super_De_04_Pnd_Dup_Frm_Payee;
    private DbsField pnd_Dup_Super_De_04_Pnd_Dup_Effctv_Dte;

    private DataAccessProgramView vw_rqst_First;
    private DbsField rqst_First_Rcrd_Type_Cde;
    private DbsField rqst_First_Rqst_Id;
    private DbsField rqst_First_Rqst_Effctv_Dte;
    private DbsField rqst_First_Rqst_Entry_Dte;
    private DbsField rqst_First_Rqst_Entry_Tme;
    private DbsField rqst_First_Rqst_Lst_Actvty_Dte;
    private DbsField rqst_First_Rqst_Rcvd_Dte;
    private DbsField rqst_First_Rqst_Rcvd_Tme;
    private DbsField rqst_First_Rqst_Rcvd_User_Id;
    private DbsField rqst_First_Rqst_Entry_User_Id;
    private DbsField rqst_First_Rqst_Cntct_Mde;
    private DbsField rqst_First_Rqst_Srce;
    private DbsField rqst_First_Rqst_Rep_Nme;
    private DbsField rqst_First_Rqst_Sttmnt_Ind;
    private DbsField rqst_First_Rqst_Opn_Clsd_Ind;
    private DbsField rqst_First_Rqst_Rcprcl_Dte;
    private DbsField rqst_First_Rqst_Ssn;
    private DbsField rqst_First_Xfr_Work_Prcss_Id;
    private DbsField rqst_First_Xfr_Mit_Log_Dte_Tme;
    private DbsField rqst_First_Xfr_Stts_Cde;
    private DbsField rqst_First_Xfr_Rjctn_Cde;
    private DbsField rqst_First_Xfr_In_Progress_Ind;
    private DbsField rqst_First_Xfr_Retry_Cnt;
    private DbsField rqst_First_Ia_Frm_Cntrct;
    private DbsField rqst_First_Ia_Frm_Payee;
    private DbsField rqst_First_Ia_Unique_Id;
    private DbsField rqst_First_Ia_Hold_Cde;
    private DbsField rqst_First_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup rqst_First_Xfr_Frm_Acct_Dta;
    private DbsField rqst_First_Xfr_Frm_Acct_Cde;
    private DbsField rqst_First_Xfr_Frm_Unit_Typ;
    private DbsField rqst_First_Xfr_Frm_Typ;
    private DbsField rqst_First_Xfr_Frm_Qty;
    private DbsField rqst_First_Xfr_Frm_Est_Amt;
    private DbsField rqst_First_Xfr_Frm_Asset_Amt;
    private DbsField rqst_First_Count_Castxfr_To_Acct_Dta;

    private DbsGroup rqst_First_Xfr_To_Acct_Dta;
    private DbsField rqst_First_Xfr_To_Acct_Cde;
    private DbsField rqst_First_Xfr_To_Unit_Typ;
    private DbsField rqst_First_Xfr_To_Typ;
    private DbsField rqst_First_Xfr_To_Qty;
    private DbsField rqst_First_Xfr_To_Est_Amt;
    private DbsField rqst_First_Xfr_To_Asset_Amt;
    private DbsField rqst_First_Ia_To_Cntrct;
    private DbsField rqst_First_Ia_To_Payee;
    private DbsField rqst_First_Ia_Appl_Rcvd_Dte;
    private DbsField rqst_First_Ia_New_Issue;
    private DbsField rqst_First_Ia_Rsn_For_Ovrrde;
    private DbsField rqst_First_Ia_Ovrrde_User_Id;
    private DbsField rqst_First_Ia_Gnrl_Pend_Cde;
    private DbsField rqst_First_Rqst_Xfr_Type;
    private DbsField rqst_First_Rqst_Unit_Cde;
    private DbsField rqst_First_Ia_New_Iss_Prt_Pull;

    private DataAccessProgramView vw_rqst_Future;
    private DbsField rqst_Future_Rcrd_Type_Cde;
    private DbsField rqst_Future_Rqst_Id;
    private DbsField rqst_Future_Rqst_Effctv_Dte;
    private DbsField rqst_Future_Rqst_Entry_Dte;
    private DbsField rqst_Future_Rqst_Entry_Tme;
    private DbsField rqst_Future_Rqst_Lst_Actvty_Dte;
    private DbsField rqst_Future_Rqst_Rcvd_Dte;
    private DbsField rqst_Future_Rqst_Rcvd_Tme;
    private DbsField rqst_Future_Rqst_Rcvd_User_Id;
    private DbsField rqst_Future_Rqst_Entry_User_Id;
    private DbsField rqst_Future_Rqst_Cntct_Mde;
    private DbsField rqst_Future_Rqst_Srce;
    private DbsField rqst_Future_Rqst_Rep_Nme;
    private DbsField rqst_Future_Rqst_Sttmnt_Ind;
    private DbsField rqst_Future_Rqst_Opn_Clsd_Ind;
    private DbsField rqst_Future_Rqst_Rcprcl_Dte;
    private DbsField rqst_Future_Rqst_Ssn;
    private DbsField rqst_Future_Xfr_Work_Prcss_Id;
    private DbsField rqst_Future_Xfr_Mit_Log_Dte_Tme;
    private DbsField rqst_Future_Xfr_Stts_Cde;
    private DbsField rqst_Future_Xfr_Rjctn_Cde;
    private DbsField rqst_Future_Xfr_In_Progress_Ind;
    private DbsField rqst_Future_Xfr_Retry_Cnt;
    private DbsField rqst_Future_Ia_Frm_Cntrct;
    private DbsField rqst_Future_Ia_Frm_Payee;
    private DbsField rqst_Future_Ia_Unique_Id;
    private DbsField rqst_Future_Ia_Hold_Cde;
    private DbsField rqst_Future_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup rqst_Future_Xfr_Frm_Acct_Dta;
    private DbsField rqst_Future_Xfr_Frm_Acct_Cde;
    private DbsField rqst_Future_Xfr_Frm_Unit_Typ;
    private DbsField rqst_Future_Xfr_Frm_Typ;
    private DbsField rqst_Future_Xfr_Frm_Qty;
    private DbsField rqst_Future_Xfr_Frm_Est_Amt;
    private DbsField rqst_Future_Xfr_Frm_Asset_Amt;
    private DbsField rqst_Future_Count_Castxfr_To_Acct_Dta;

    private DbsGroup rqst_Future_Xfr_To_Acct_Dta;
    private DbsField rqst_Future_Xfr_To_Acct_Cde;
    private DbsField rqst_Future_Xfr_To_Unit_Typ;
    private DbsField rqst_Future_Xfr_To_Typ;
    private DbsField rqst_Future_Xfr_To_Qty;
    private DbsField rqst_Future_Xfr_To_Est_Amt;
    private DbsField rqst_Future_Xfr_To_Asset_Amt;
    private DbsField rqst_Future_Ia_To_Cntrct;
    private DbsField rqst_Future_Ia_To_Payee;
    private DbsField rqst_Future_Ia_Appl_Rcvd_Dte;
    private DbsField rqst_Future_Ia_New_Issue;
    private DbsField rqst_Future_Ia_Rsn_For_Ovrrde;
    private DbsField rqst_Future_Ia_Ovrrde_User_Id;
    private DbsField rqst_Future_Ia_Gnrl_Pend_Cde;
    private DbsField rqst_Future_Rqst_Xfr_Type;
    private DbsField rqst_Future_Rqst_Unit_Cde;
    private DbsField rqst_Future_Ia_New_Iss_Prt_Pull;
    private DbsField pnd_Ia_Super_De_04;

    private DbsGroup pnd_Ia_Super_De_04__R_Field_2;
    private DbsField pnd_Ia_Super_De_04_Pnd_De_04_Type_Cde;
    private DbsField pnd_Ia_Super_De_04_Pnd_De_04_Frm_Cntrct;
    private DbsField pnd_Ia_Super_De_04_Pnd_De_04_Frm_Payee;
    private DbsField pnd_Ia_Super_De_04_Pnd_De_04_Effctv_Dte;
    private DbsField pnd_Save_Rqst_Id;
    private DbsField pnd_S_Pin_Nbr;
    private DbsField pnd_S_Rcprcl_Dte;
    private DbsField pnd_S_Rcvd_Dte;
    private DbsField pnd_S_Rcvd_Tme;

    private DbsGroup pnd_S_Rcvd_Tme__R_Field_3;
    private DbsField pnd_S_Rcvd_Tme_Pnd_S_Rcvd_Tme_A;
    private DbsField pnd_Non_Repetitive;
    private DbsField pnd_Timex;
    private DbsField pnd_Timn;

    private DbsGroup pnd_Timn__R_Field_4;
    private DbsField pnd_Timn_Pnd_Time;

    private DbsGroup pnd_Timn__R_Field_5;
    private DbsField pnd_Timn_Pnd_Time_A;
    private DbsField pnd_Timn_Pnd_Time_Fill;
    private DbsField pnd_Percent;
    private DbsField pnd_Sub;
    private DbsField pnd_Work_Date_A;

    private DbsGroup pnd_Work_Date_A__R_Field_6;
    private DbsField pnd_Work_Date_A_Pnd_Work_Date_N;
    private DbsField pnd_Hold_Effective_Dte;
    private DbsField pnd_H_Effective_Dte;
    private DbsField pnd_Rc;
    private DbsField pnd_Present_Date;

    private DbsGroup pnd_Present_Date__R_Field_7;
    private DbsField pnd_Present_Date_Pnd_Present_Date_Cc;
    private DbsField pnd_Present_Date_Pnd_Present_Date_Yy;

    private DbsGroup pnd_Present_Date__R_Field_8;
    private DbsField pnd_Present_Date_Pnd_Present_Date_Yy_A;
    private DbsField pnd_Present_Date_Pnd_Present_Date_Mm;
    private DbsField pnd_Present_Date_Pnd_Present_Date_Dd;

    private DbsGroup pnd_Present_Date__R_Field_9;
    private DbsField pnd_Present_Date_Pnd_Present_Date_A;
    private DbsField pnd_Passed_Date_D;
    private DbsField pnd_Passed_Date;

    private DbsGroup pnd_Passed_Date__R_Field_10;
    private DbsField pnd_Passed_Date_Pnd_Passed_Date_Cc;
    private DbsField pnd_Passed_Date_Pnd_Passed_Date_Yy;

    private DbsGroup pnd_Passed_Date__R_Field_11;
    private DbsField pnd_Passed_Date_Pnd_Passed_Date_Yy_A;
    private DbsField pnd_Passed_Date_Pnd_Passed_Date_Mm;
    private DbsField pnd_Passed_Date_Pnd_Passed_Date_Dd;

    private DbsGroup pnd_Passed_Date__R_Field_12;
    private DbsField pnd_Passed_Date_Pnd_Passed_Date_A;
    private DbsField pnd_Work_Field;

    private DataAccessProgramView vw_iaa_Xfr_Audit;
    private DbsField iaa_Xfr_Audit_Rqst_Id;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Awaiting_Factor_A1;
    private DbsField pnd_Const_Pnd_Stat_Complete_M0;
    private DbsField pnd_Const_Pnd_Stat_Rjctd_P0;
    private DbsField pnd_Const_Pnd_Stat_Rjctd_P4;
    private DbsField pnd_Const_Pnd_Active;
    private DbsField pnd_Const_Pnd_Closed;
    private DbsField pnd_Const_Pnd_Transfer_T;
    private DbsField pnd_Const_Pnd_Switch_S;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Trnsfr;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Switch;
    private DbsField pnd_Const_Pnd_Num_Retry;
    private DbsField pnd_Datea;

    private DbsGroup pnd_Datea__R_Field_13;
    private DbsField pnd_Datea_Pnd_Daten;

    private DbsGroup pnd_Datea__R_Field_14;
    private DbsField pnd_Datea_Pnd_Date_Yyyy;
    private DbsField pnd_Datea_Pnd_Date_Mm;
    private DbsField pnd_Datea_Pnd_Date_Dd;
    private DbsField pnd_Todays_Datea;

    private DbsGroup pnd_Todays_Datea__R_Field_15;
    private DbsField pnd_Todays_Datea_Pnd_Todays_Daten;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_Audit_Rec_Exists;
    private DbsField pnd_L_Pnd_Debug;
    private DbsField pnd_Stat_1_2;

    private DbsGroup pnd_Stat_1_2__R_Field_16;
    private DbsField pnd_Stat_1_2_Pnd_Stat_1;
    private DbsField pnd_Stat_1_2_Pnd_Stat_2;
    private DbsField pnd_Rqst_Status_Cde;
    private DbsField pnd_Xfr_Rjctn_Cde;
    private DbsField pnd_I;
    private DbsField pnd_T;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIata700 = new PdaIata700(localVariables);
        pdaIatl400p = new PdaIatl400p(localVariables);
        pdaIatl420z = new PdaIatl420z(localVariables);
        pdaIatl403p = new PdaIatl403p(localVariables);
        pdaAiaa026 = new PdaAiaa026(localVariables);
        pdaIaapda_M = new PdaIaapda_M(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaIatl401p = new PdaIatl401p(parameters);
        pnd_Fnd1_Isn = parameters.newFieldInRecord("pnd_Fnd1_Isn", "#FND1-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Fnd1_Isn.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_iaa_Trnsfr_Sw_Rqst = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst", "IAA-TRNSFR-SW-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_ENTRY_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_RCVD_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_CNTCT_MDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Srce = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_SRCE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "RQST_REP_NME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_STTMNT_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RQST_RCPRCL_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Ssn = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RQST_SSN");
        iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_RJCTN_CDE");
        iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "XFR_RETRY_CNT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "IA_HOLD_CDE");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta", 
            "C*XFR-FRM-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta", 
            "C*XFR-TO-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IA_APPL_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_Ia_New_Issue = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISSUE");
        iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_RSN_FOR_OVRRDE");
        iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "IA_OVRRDE_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_GNRL_PEND_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst);

        vw_hist_View = new DataAccessProgramView(new NameInfo("vw_hist_View", "HIST-VIEW"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO");
        hist_View_Rqst_Id = vw_hist_View.getRecord().newFieldInGroup("hist_View_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        registerRecord(vw_hist_View);

        vw_dup_Rqst = new DataAccessProgramView(new NameInfo("vw_dup_Rqst", "DUP-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO");
        dup_Rqst_Rcrd_Type_Cde = vw_dup_Rqst.getRecord().newFieldInGroup("dup_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        dup_Rqst_Rqst_Effctv_Dte = vw_dup_Rqst.getRecord().newFieldInGroup("dup_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_EFFCTV_DTE");
        dup_Rqst_Ia_Frm_Cntrct = vw_dup_Rqst.getRecord().newFieldInGroup("dup_Rqst_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_FRM_CNTRCT");
        dup_Rqst_Ia_Frm_Payee = vw_dup_Rqst.getRecord().newFieldInGroup("dup_Rqst_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_FRM_PAYEE");
        dup_Rqst_Xfr_Stts_Cde = vw_dup_Rqst.getRecord().newFieldInGroup("dup_Rqst_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_STTS_CDE");
        registerRecord(vw_dup_Rqst);

        pnd_Dup_Super_De_04 = localVariables.newFieldInRecord("pnd_Dup_Super_De_04", "#DUP-SUPER-DE-04", FieldType.STRING, 17);

        pnd_Dup_Super_De_04__R_Field_1 = localVariables.newGroupInRecord("pnd_Dup_Super_De_04__R_Field_1", "REDEFINE", pnd_Dup_Super_De_04);
        pnd_Dup_Super_De_04__Filler1 = pnd_Dup_Super_De_04__R_Field_1.newFieldInGroup("pnd_Dup_Super_De_04__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Dup_Super_De_04_Pnd_Dup_Frm_Cntrct = pnd_Dup_Super_De_04__R_Field_1.newFieldInGroup("pnd_Dup_Super_De_04_Pnd_Dup_Frm_Cntrct", "#DUP-FRM-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Dup_Super_De_04_Pnd_Dup_Frm_Payee = pnd_Dup_Super_De_04__R_Field_1.newFieldInGroup("pnd_Dup_Super_De_04_Pnd_Dup_Frm_Payee", "#DUP-FRM-PAYEE", 
            FieldType.STRING, 2);
        pnd_Dup_Super_De_04_Pnd_Dup_Effctv_Dte = pnd_Dup_Super_De_04__R_Field_1.newFieldInGroup("pnd_Dup_Super_De_04_Pnd_Dup_Effctv_Dte", "#DUP-EFFCTV-DTE", 
            FieldType.DATE);

        vw_rqst_First = new DataAccessProgramView(new NameInfo("vw_rqst_First", "RQST-FIRST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        rqst_First_Rcrd_Type_Cde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        rqst_First_Rqst_Id = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        rqst_First_Rqst_Effctv_Dte = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_EFFCTV_DTE");
        rqst_First_Rqst_Entry_Dte = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_DTE");
        rqst_First_Rqst_Entry_Tme = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_TME");
        rqst_First_Rqst_Lst_Actvty_Dte = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        rqst_First_Rqst_Rcvd_Dte = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_RCVD_DTE");
        rqst_First_Rqst_Rcvd_Tme = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RQST_RCVD_TME");
        rqst_First_Rqst_Rcvd_User_Id = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        rqst_First_Rqst_Entry_User_Id = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        rqst_First_Rqst_Cntct_Mde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_CNTCT_MDE");
        rqst_First_Rqst_Srce = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_SRCE");
        rqst_First_Rqst_Rep_Nme = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RQST_REP_NME");
        rqst_First_Rqst_Sttmnt_Ind = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_STTMNT_IND");
        rqst_First_Rqst_Opn_Clsd_Ind = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        rqst_First_Rqst_Rcprcl_Dte = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "RQST_RCPRCL_DTE");
        rqst_First_Rqst_Ssn = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "RQST_SSN");
        rqst_First_Xfr_Work_Prcss_Id = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        rqst_First_Xfr_Mit_Log_Dte_Tme = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        rqst_First_Xfr_Stts_Cde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_STTS_CDE");
        rqst_First_Xfr_Rjctn_Cde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_RJCTN_CDE");
        rqst_First_Xfr_In_Progress_Ind = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", FieldType.BOOLEAN, 
            1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        rqst_First_Xfr_Retry_Cnt = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "XFR_RETRY_CNT");
        rqst_First_Ia_Frm_Cntrct = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_FRM_CNTRCT");
        rqst_First_Ia_Frm_Payee = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_FRM_PAYEE");
        rqst_First_Ia_Unique_Id = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "IA_UNIQUE_ID");
        rqst_First_Ia_Hold_Cde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IA_HOLD_CDE");
        rqst_First_Count_Castxfr_Frm_Acct_Dta = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Count_Castxfr_Frm_Acct_Dta", "C*XFR-FRM-ACCT-DTA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        rqst_First_Xfr_Frm_Acct_Dta = vw_rqst_First.getRecord().newGroupInGroup("rqst_First_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_First_Xfr_Frm_Acct_Cde = rqst_First_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_First_Xfr_Frm_Unit_Typ = rqst_First_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_First_Xfr_Frm_Typ = rqst_First_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_First_Xfr_Frm_Qty = rqst_First_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_First_Xfr_Frm_Est_Amt = rqst_First_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_First_Xfr_Frm_Asset_Amt = rqst_First_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_First_Count_Castxfr_To_Acct_Dta = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Count_Castxfr_To_Acct_Dta", "C*XFR-TO-ACCT-DTA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        rqst_First_Xfr_To_Acct_Dta = vw_rqst_First.getRecord().newGroupInGroup("rqst_First_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_First_Xfr_To_Acct_Cde = rqst_First_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_First_Xfr_To_Unit_Typ = rqst_First_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_First_Xfr_To_Typ = rqst_First_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_First_Xfr_To_Qty = rqst_First_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_First_Xfr_To_Est_Amt = rqst_First_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_First_Xfr_To_Asset_Amt = rqst_First_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_First_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_First_Ia_To_Cntrct = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_TO_CNTRCT");
        rqst_First_Ia_To_Payee = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_TO_PAYEE");
        rqst_First_Ia_Appl_Rcvd_Dte = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "IA_APPL_RCVD_DTE");
        rqst_First_Ia_New_Issue = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "IA_NEW_ISSUE");
        rqst_First_Ia_Rsn_For_Ovrrde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_RSN_FOR_OVRRDE");
        rqst_First_Ia_Ovrrde_User_Id = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "IA_OVRRDE_USER_ID");
        rqst_First_Ia_Gnrl_Pend_Cde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "IA_GNRL_PEND_CDE");
        rqst_First_Rqst_Xfr_Type = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_XFR_TYPE");
        rqst_First_Rqst_Unit_Cde = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_UNIT_CDE");
        rqst_First_Ia_New_Iss_Prt_Pull = vw_rqst_First.getRecord().newFieldInGroup("rqst_First_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_rqst_First);

        vw_rqst_Future = new DataAccessProgramView(new NameInfo("vw_rqst_Future", "RQST-FUTURE"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        rqst_Future_Rcrd_Type_Cde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        rqst_Future_Rqst_Id = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        rqst_Future_Rqst_Effctv_Dte = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_EFFCTV_DTE");
        rqst_Future_Rqst_Entry_Dte = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_DTE");
        rqst_Future_Rqst_Entry_Tme = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 7, 
            RepeatingFieldStrategy.None, "RQST_ENTRY_TME");
        rqst_Future_Rqst_Lst_Actvty_Dte = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        rqst_Future_Rqst_Rcvd_Dte = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_RCVD_DTE");
        rqst_Future_Rqst_Rcvd_Tme = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RQST_RCVD_TME");
        rqst_Future_Rqst_Rcvd_User_Id = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        rqst_Future_Rqst_Entry_User_Id = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        rqst_Future_Rqst_Cntct_Mde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_CNTCT_MDE");
        rqst_Future_Rqst_Srce = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_SRCE");
        rqst_Future_Rqst_Rep_Nme = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RQST_REP_NME");
        rqst_Future_Rqst_Sttmnt_Ind = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RQST_STTMNT_IND");
        rqst_Future_Rqst_Opn_Clsd_Ind = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        rqst_Future_Rqst_Rcprcl_Dte = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RQST_RCPRCL_DTE");
        rqst_Future_Rqst_Ssn = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "RQST_SSN");
        rqst_Future_Xfr_Work_Prcss_Id = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        rqst_Future_Xfr_Mit_Log_Dte_Tme = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        rqst_Future_Xfr_Stts_Cde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_STTS_CDE");
        rqst_Future_Xfr_Rjctn_Cde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_RJCTN_CDE");
        rqst_Future_Xfr_In_Progress_Ind = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", FieldType.BOOLEAN, 
            1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        rqst_Future_Xfr_Retry_Cnt = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "XFR_RETRY_CNT");
        rqst_Future_Ia_Frm_Cntrct = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_FRM_CNTRCT");
        rqst_Future_Ia_Frm_Payee = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_FRM_PAYEE");
        rqst_Future_Ia_Unique_Id = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "IA_UNIQUE_ID");
        rqst_Future_Ia_Hold_Cde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IA_HOLD_CDE");
        rqst_Future_Count_Castxfr_Frm_Acct_Dta = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Count_Castxfr_Frm_Acct_Dta", "C*XFR-FRM-ACCT-DTA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        rqst_Future_Xfr_Frm_Acct_Dta = vw_rqst_Future.getRecord().newGroupInGroup("rqst_Future_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_Future_Xfr_Frm_Acct_Cde = rqst_Future_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_Future_Xfr_Frm_Unit_Typ = rqst_Future_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_Future_Xfr_Frm_Typ = rqst_Future_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1, new 
            DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_Future_Xfr_Frm_Qty = rqst_Future_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_Future_Xfr_Frm_Est_Amt = rqst_Future_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_Future_Xfr_Frm_Asset_Amt = rqst_Future_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        rqst_Future_Count_Castxfr_To_Acct_Dta = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Count_Castxfr_To_Acct_Dta", "C*XFR-TO-ACCT-DTA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        rqst_Future_Xfr_To_Acct_Dta = vw_rqst_Future.getRecord().newGroupInGroup("rqst_Future_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_Future_Xfr_To_Acct_Cde = rqst_Future_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_Future_Xfr_To_Unit_Typ = rqst_Future_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_Future_Xfr_To_Typ = rqst_Future_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_Future_Xfr_To_Qty = rqst_Future_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_Future_Xfr_To_Est_Amt = rqst_Future_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_Future_Xfr_To_Asset_Amt = rqst_Future_Xfr_To_Acct_Dta.newFieldArrayInGroup("rqst_Future_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        rqst_Future_Ia_To_Cntrct = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_TO_CNTRCT");
        rqst_Future_Ia_To_Payee = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_TO_PAYEE");
        rqst_Future_Ia_Appl_Rcvd_Dte = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IA_APPL_RCVD_DTE");
        rqst_Future_Ia_New_Issue = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "IA_NEW_ISSUE");
        rqst_Future_Ia_Rsn_For_Ovrrde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_RSN_FOR_OVRRDE");
        rqst_Future_Ia_Ovrrde_User_Id = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "IA_OVRRDE_USER_ID");
        rqst_Future_Ia_Gnrl_Pend_Cde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_GNRL_PEND_CDE");
        rqst_Future_Rqst_Xfr_Type = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_XFR_TYPE");
        rqst_Future_Rqst_Unit_Cde = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_UNIT_CDE");
        rqst_Future_Ia_New_Iss_Prt_Pull = vw_rqst_Future.getRecord().newFieldInGroup("rqst_Future_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_rqst_Future);

        pnd_Ia_Super_De_04 = localVariables.newFieldInRecord("pnd_Ia_Super_De_04", "#IA-SUPER-DE-04", FieldType.STRING, 17);

        pnd_Ia_Super_De_04__R_Field_2 = localVariables.newGroupInRecord("pnd_Ia_Super_De_04__R_Field_2", "REDEFINE", pnd_Ia_Super_De_04);
        pnd_Ia_Super_De_04_Pnd_De_04_Type_Cde = pnd_Ia_Super_De_04__R_Field_2.newFieldInGroup("pnd_Ia_Super_De_04_Pnd_De_04_Type_Cde", "#DE-04-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Ia_Super_De_04_Pnd_De_04_Frm_Cntrct = pnd_Ia_Super_De_04__R_Field_2.newFieldInGroup("pnd_Ia_Super_De_04_Pnd_De_04_Frm_Cntrct", "#DE-04-FRM-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Ia_Super_De_04_Pnd_De_04_Frm_Payee = pnd_Ia_Super_De_04__R_Field_2.newFieldInGroup("pnd_Ia_Super_De_04_Pnd_De_04_Frm_Payee", "#DE-04-FRM-PAYEE", 
            FieldType.STRING, 2);
        pnd_Ia_Super_De_04_Pnd_De_04_Effctv_Dte = pnd_Ia_Super_De_04__R_Field_2.newFieldInGroup("pnd_Ia_Super_De_04_Pnd_De_04_Effctv_Dte", "#DE-04-EFFCTV-DTE", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Save_Rqst_Id = localVariables.newFieldInRecord("pnd_Save_Rqst_Id", "#SAVE-RQST-ID", FieldType.STRING, 34);
        pnd_S_Pin_Nbr = localVariables.newFieldInRecord("pnd_S_Pin_Nbr", "#S-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_S_Rcprcl_Dte = localVariables.newFieldInRecord("pnd_S_Rcprcl_Dte", "#S-RCPRCL-DTE", FieldType.STRING, 8);
        pnd_S_Rcvd_Dte = localVariables.newFieldInRecord("pnd_S_Rcvd_Dte", "#S-RCVD-DTE", FieldType.STRING, 8);
        pnd_S_Rcvd_Tme = localVariables.newFieldInRecord("pnd_S_Rcvd_Tme", "#S-RCVD-TME", FieldType.NUMERIC, 6);

        pnd_S_Rcvd_Tme__R_Field_3 = localVariables.newGroupInRecord("pnd_S_Rcvd_Tme__R_Field_3", "REDEFINE", pnd_S_Rcvd_Tme);
        pnd_S_Rcvd_Tme_Pnd_S_Rcvd_Tme_A = pnd_S_Rcvd_Tme__R_Field_3.newFieldInGroup("pnd_S_Rcvd_Tme_Pnd_S_Rcvd_Tme_A", "#S-RCVD-TME-A", FieldType.STRING, 
            6);
        pnd_Non_Repetitive = localVariables.newFieldInRecord("pnd_Non_Repetitive", "#NON-REPETITIVE", FieldType.STRING, 1);
        pnd_Timex = localVariables.newFieldInRecord("pnd_Timex", "#TIMEX", FieldType.TIME);
        pnd_Timn = localVariables.newFieldInRecord("pnd_Timn", "#TIMN", FieldType.NUMERIC, 7);

        pnd_Timn__R_Field_4 = localVariables.newGroupInRecord("pnd_Timn__R_Field_4", "REDEFINE", pnd_Timn);
        pnd_Timn_Pnd_Time = pnd_Timn__R_Field_4.newFieldInGroup("pnd_Timn_Pnd_Time", "#TIME", FieldType.NUMERIC, 6);

        pnd_Timn__R_Field_5 = pnd_Timn__R_Field_4.newGroupInGroup("pnd_Timn__R_Field_5", "REDEFINE", pnd_Timn_Pnd_Time);
        pnd_Timn_Pnd_Time_A = pnd_Timn__R_Field_5.newFieldInGroup("pnd_Timn_Pnd_Time_A", "#TIME-A", FieldType.STRING, 6);
        pnd_Timn_Pnd_Time_Fill = pnd_Timn__R_Field_4.newFieldInGroup("pnd_Timn_Pnd_Time_Fill", "#TIME-FILL", FieldType.NUMERIC, 1);
        pnd_Percent = localVariables.newFieldInRecord("pnd_Percent", "#PERCENT", FieldType.NUMERIC, 3);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 2);
        pnd_Work_Date_A = localVariables.newFieldInRecord("pnd_Work_Date_A", "#WORK-DATE-A", FieldType.STRING, 8);

        pnd_Work_Date_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_Date_A__R_Field_6", "REDEFINE", pnd_Work_Date_A);
        pnd_Work_Date_A_Pnd_Work_Date_N = pnd_Work_Date_A__R_Field_6.newFieldInGroup("pnd_Work_Date_A_Pnd_Work_Date_N", "#WORK-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Hold_Effective_Dte = localVariables.newFieldInRecord("pnd_Hold_Effective_Dte", "#HOLD-EFFECTIVE-DTE", FieldType.DATE);
        pnd_H_Effective_Dte = localVariables.newFieldInRecord("pnd_H_Effective_Dte", "#H-EFFECTIVE-DTE", FieldType.DATE);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Present_Date = localVariables.newFieldInRecord("pnd_Present_Date", "#PRESENT-DATE", FieldType.NUMERIC, 8);

        pnd_Present_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Present_Date__R_Field_7", "REDEFINE", pnd_Present_Date);
        pnd_Present_Date_Pnd_Present_Date_Cc = pnd_Present_Date__R_Field_7.newFieldInGroup("pnd_Present_Date_Pnd_Present_Date_Cc", "#PRESENT-DATE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Present_Date_Pnd_Present_Date_Yy = pnd_Present_Date__R_Field_7.newFieldInGroup("pnd_Present_Date_Pnd_Present_Date_Yy", "#PRESENT-DATE-YY", 
            FieldType.NUMERIC, 2);

        pnd_Present_Date__R_Field_8 = pnd_Present_Date__R_Field_7.newGroupInGroup("pnd_Present_Date__R_Field_8", "REDEFINE", pnd_Present_Date_Pnd_Present_Date_Yy);
        pnd_Present_Date_Pnd_Present_Date_Yy_A = pnd_Present_Date__R_Field_8.newFieldInGroup("pnd_Present_Date_Pnd_Present_Date_Yy_A", "#PRESENT-DATE-YY-A", 
            FieldType.STRING, 2);
        pnd_Present_Date_Pnd_Present_Date_Mm = pnd_Present_Date__R_Field_7.newFieldInGroup("pnd_Present_Date_Pnd_Present_Date_Mm", "#PRESENT-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Present_Date_Pnd_Present_Date_Dd = pnd_Present_Date__R_Field_7.newFieldInGroup("pnd_Present_Date_Pnd_Present_Date_Dd", "#PRESENT-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Present_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Present_Date__R_Field_9", "REDEFINE", pnd_Present_Date);
        pnd_Present_Date_Pnd_Present_Date_A = pnd_Present_Date__R_Field_9.newFieldInGroup("pnd_Present_Date_Pnd_Present_Date_A", "#PRESENT-DATE-A", FieldType.STRING, 
            8);
        pnd_Passed_Date_D = localVariables.newFieldInRecord("pnd_Passed_Date_D", "#PASSED-DATE-D", FieldType.DATE);
        pnd_Passed_Date = localVariables.newFieldInRecord("pnd_Passed_Date", "#PASSED-DATE", FieldType.NUMERIC, 8);

        pnd_Passed_Date__R_Field_10 = localVariables.newGroupInRecord("pnd_Passed_Date__R_Field_10", "REDEFINE", pnd_Passed_Date);
        pnd_Passed_Date_Pnd_Passed_Date_Cc = pnd_Passed_Date__R_Field_10.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Date_Cc", "#PASSED-DATE-CC", FieldType.NUMERIC, 
            2);
        pnd_Passed_Date_Pnd_Passed_Date_Yy = pnd_Passed_Date__R_Field_10.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Date_Yy", "#PASSED-DATE-YY", FieldType.NUMERIC, 
            2);

        pnd_Passed_Date__R_Field_11 = pnd_Passed_Date__R_Field_10.newGroupInGroup("pnd_Passed_Date__R_Field_11", "REDEFINE", pnd_Passed_Date_Pnd_Passed_Date_Yy);
        pnd_Passed_Date_Pnd_Passed_Date_Yy_A = pnd_Passed_Date__R_Field_11.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Date_Yy_A", "#PASSED-DATE-YY-A", 
            FieldType.STRING, 2);
        pnd_Passed_Date_Pnd_Passed_Date_Mm = pnd_Passed_Date__R_Field_10.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Date_Mm", "#PASSED-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Passed_Date_Pnd_Passed_Date_Dd = pnd_Passed_Date__R_Field_10.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Date_Dd", "#PASSED-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Passed_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_Passed_Date__R_Field_12", "REDEFINE", pnd_Passed_Date);
        pnd_Passed_Date_Pnd_Passed_Date_A = pnd_Passed_Date__R_Field_12.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Date_A", "#PASSED-DATE-A", FieldType.STRING, 
            8);
        pnd_Work_Field = localVariables.newFieldInRecord("pnd_Work_Field", "#WORK-FIELD", FieldType.STRING, 20);

        vw_iaa_Xfr_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Audit", "IAA-XFR-AUDIT"), "IAA_XFR_AUDIT", "IA_TRANSFERS");
        iaa_Xfr_Audit_Rqst_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        registerRecord(vw_iaa_Xfr_Audit);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Awaiting_Factor_A1 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Awaiting_Factor_A1", "#AWAITING-FACTOR-A1", FieldType.STRING, 1);
        pnd_Const_Pnd_Stat_Complete_M0 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Complete_M0", "#STAT-COMPLETE-M0", FieldType.STRING, 2);
        pnd_Const_Pnd_Stat_Rjctd_P0 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Rjctd_P0", "#STAT-RJCTD-P0", FieldType.STRING, 2);
        pnd_Const_Pnd_Stat_Rjctd_P4 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Rjctd_P4", "#STAT-RJCTD-P4", FieldType.STRING, 2);
        pnd_Const_Pnd_Active = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Active", "#ACTIVE", FieldType.STRING, 1);
        pnd_Const_Pnd_Closed = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Closed", "#CLOSED", FieldType.STRING, 1);
        pnd_Const_Pnd_Transfer_T = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Transfer_T", "#TRANSFER-T", FieldType.STRING, 1);
        pnd_Const_Pnd_Switch_S = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Switch_S", "#SWITCH-S", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Trnsfr = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Trnsfr", "#RCRD-TYPE-TRNSFR", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Switch = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Switch", "#RCRD-TYPE-SWITCH", FieldType.STRING, 1);
        pnd_Const_Pnd_Num_Retry = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Num_Retry", "#NUM-RETRY", FieldType.NUMERIC, 1);
        pnd_Datea = localVariables.newFieldInRecord("pnd_Datea", "#DATEA", FieldType.STRING, 8);

        pnd_Datea__R_Field_13 = localVariables.newGroupInRecord("pnd_Datea__R_Field_13", "REDEFINE", pnd_Datea);
        pnd_Datea_Pnd_Daten = pnd_Datea__R_Field_13.newFieldInGroup("pnd_Datea_Pnd_Daten", "#DATEN", FieldType.NUMERIC, 8);

        pnd_Datea__R_Field_14 = localVariables.newGroupInRecord("pnd_Datea__R_Field_14", "REDEFINE", pnd_Datea);
        pnd_Datea_Pnd_Date_Yyyy = pnd_Datea__R_Field_14.newFieldInGroup("pnd_Datea_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Datea_Pnd_Date_Mm = pnd_Datea__R_Field_14.newFieldInGroup("pnd_Datea_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Datea_Pnd_Date_Dd = pnd_Datea__R_Field_14.newFieldInGroup("pnd_Datea_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Todays_Datea = localVariables.newFieldInRecord("pnd_Todays_Datea", "#TODAYS-DATEA", FieldType.STRING, 8);

        pnd_Todays_Datea__R_Field_15 = localVariables.newGroupInRecord("pnd_Todays_Datea__R_Field_15", "REDEFINE", pnd_Todays_Datea);
        pnd_Todays_Datea_Pnd_Todays_Daten = pnd_Todays_Datea__R_Field_15.newFieldInGroup("pnd_Todays_Datea_Pnd_Todays_Daten", "#TODAYS-DATEN", FieldType.NUMERIC, 
            8);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_Audit_Rec_Exists = pnd_L.newFieldInGroup("pnd_L_Pnd_Audit_Rec_Exists", "#AUDIT-REC-EXISTS", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Debug = pnd_L.newFieldInGroup("pnd_L_Pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Stat_1_2 = localVariables.newFieldInRecord("pnd_Stat_1_2", "#STAT-1-2", FieldType.STRING, 2);

        pnd_Stat_1_2__R_Field_16 = localVariables.newGroupInRecord("pnd_Stat_1_2__R_Field_16", "REDEFINE", pnd_Stat_1_2);
        pnd_Stat_1_2_Pnd_Stat_1 = pnd_Stat_1_2__R_Field_16.newFieldInGroup("pnd_Stat_1_2_Pnd_Stat_1", "#STAT-1", FieldType.STRING, 1);
        pnd_Stat_1_2_Pnd_Stat_2 = pnd_Stat_1_2__R_Field_16.newFieldInGroup("pnd_Stat_1_2_Pnd_Stat_2", "#STAT-2", FieldType.STRING, 1);
        pnd_Rqst_Status_Cde = localVariables.newFieldInRecord("pnd_Rqst_Status_Cde", "#RQST-STATUS-CDE", FieldType.STRING, 2);
        pnd_Xfr_Rjctn_Cde = localVariables.newFieldInRecord("pnd_Xfr_Rjctn_Cde", "#XFR-RJCTN-CDE", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst.reset();
        vw_hist_View.reset();
        vw_dup_Rqst.reset();
        vw_rqst_First.reset();
        vw_rqst_Future.reset();
        vw_iaa_Xfr_Audit.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Dup_Super_De_04.setInitialValue("1");
        pnd_Const_Pnd_Awaiting_Factor_A1.setInitialValue("F");
        pnd_Const_Pnd_Stat_Complete_M0.setInitialValue("M0");
        pnd_Const_Pnd_Stat_Rjctd_P0.setInitialValue("P0");
        pnd_Const_Pnd_Stat_Rjctd_P4.setInitialValue("P4");
        pnd_Const_Pnd_Active.setInitialValue("A");
        pnd_Const_Pnd_Closed.setInitialValue("C");
        pnd_Const_Pnd_Transfer_T.setInitialValue("T");
        pnd_Const_Pnd_Switch_S.setInitialValue("S");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr.setInitialValue("1");
        pnd_Const_Pnd_Rcrd_Type_Switch.setInitialValue("2");
        pnd_Const_Pnd_Num_Retry.setInitialValue(2);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn401() throws Exception
    {
        super("Iatn401");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *======================================================================
        //*                       START OF PROGRAM
        //* *======================================================================
        //*  #DEBUG := TRUE
        pdaIatl401p.getPnd_Iatn401_Out().reset();                                                                                                                         //Natural: RESET #IATN401-OUT
        RPT1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM GET-RQST-AND-VALIDATE-INPUT-PARAMS
            sub_Get_Rqst_And_Validate_Input_Params();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHECK-RETRY-COUNT
            sub_Check_Retry_Count();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHECK STATUS OF CONTROL RECORD
                                                                                                                                                                          //Natural: PERFORM CHECK-CONTROL-RECORD
            sub_Check_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHECK-FACTOR-AVAILABLE
            sub_Check_Factor_Available();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FOR ALL AWAITING-FACTOR REQUESTS
            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind.getBoolean()))                                                                                           //Natural: IF IAA-TRNSFR-SW-RQST.XFR-IN-PROGRESS-IND
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-AUDIT-FILE-UPADATED
                sub_Check_If_Audit_File_Upadated();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_L_Pnd_Audit_Rec_Exists.getBoolean()))                                                                                                   //Natural: IF #AUDIT-REC-EXISTS
                {
                    pnd_Rqst_Status_Cde.setValue(pnd_Const_Pnd_Stat_Complete_M0);                                                                                         //Natural: ASSIGN #RQST-STATUS-CDE := #STAT-COMPLETE-M0
                    //*  TO COMPLETED OR REJECTED
                                                                                                                                                                          //Natural: PERFORM CHANGE-RQST-STATUS
                    sub_Change_Rqst_Status();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RPT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  END
                                                                                                                                                                          //Natural: PERFORM #ESCAPE-SUBPROG
                    sub_Pnd_Escape_Subprog();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RPT1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-RQST-RETRY-COUNT
                sub_Update_Rqst_Retry_Count();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  RQST NOT IN PROGRESS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-RQST-TO-IN-PROGRESS
                sub_Update_Rqst_To_In_Progress();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals(" ")))                                                                                                  //Natural: IF IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = ' '
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-STATE-APPROVAL
                sub_Check_State_Approval();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  MASTER FILE AND CHECK RETRY COUNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-DB03-CHANGES
            sub_Process_Db03_Changes();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  TO COMPLETED OR REJECTED
                                                                                                                                                                          //Natural: PERFORM CHANGE-RQST-STATUS
            sub_Change_Rqst_Status();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END
                                                                                                                                                                          //Natural: PERFORM #ESCAPE-SUBPROG
            sub_Pnd_Escape_Subprog();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *******************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ESCAPE-SUBPROG
            //* ******************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-ERROR
            //*  RPT1.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* *======================================================================
        //*                       START OF SUBROUTINES
        //* *======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RQST-AND-VALIDATE-INPUT-PARAMS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RETRY-COUNT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CONTROL-RECORD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FACTOR-AVAILABLE
        //* ***********************************************************************
        //*  WE ASSUME THAT IF ONE FACTOR IS AVAILABLE THEN ALL ARE AVAILABLE
        //* *IA-CALL-TYPE    := 'L'
        //* *IA-FUND-CODE    := 'C'
        //* *IA-REVAL-METHD  := 'A'
        //* *IA-CHECK-DATE   := 99999999
        //*  IA-AIAN026-LINKAGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-AUDIT-FILE-UPADATED
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RQST-TO-IN-PROGRESS
        //* ******************* END OF FIX FOR 3.1.4
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RQST-RETRY-COUNT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHANGE-RQST-STATUS
        //* ***************** END FOR FIX FOR 3.1.4
        //* *BACKOUT TRANSACTION
        //*  SET RETURN STATUS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-STATE-APPROVAL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DB03-CHANGES
        //* * BACKOUT TRANSACTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-TO-QTY-OF-SW
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-MIT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #NEXT-YEARS-REQUEST
        //* ***********************************************************************
        //*   RQST-FUTURE.RQST-UNIT-CDE        := 'BPSRB1'
        //* **   FILLING REQUEST ID ***
        //*  022021 - START
        //*  INTO RQST-FUTURE.RQST-ID LEAVING NO
        //*  CHECK IF A RQST ALREADY EXISTS WITH SAME EFFECTIVE DATE & STATUS
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-MULTI
        //* ****************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUTURE-REQUEST
    }
    private void sub_Pnd_Escape_Subprog() throws Exception                                                                                                                //Natural: #ESCAPE-SUBPROG
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( RPT1. )
        Global.setEscapeCode(EscapeType.Bottom, "RPT1");
        if (true) return;
    }
    private void sub_Pnd_Process_Error() throws Exception                                                                                                                 //Natural: #PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pdaIatl401p.getPnd_Iatn401_Out_Pnd_Return_Code().setValue("E");                                                                                                   //Natural: MOVE 'E' TO #IATN401-OUT.#RETURN-CODE
        pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg(), Global.getPROGRAM()));                               //Natural: COMPRESS #IATN401-OUT.#MSG *PROGRAM INTO #IATN401-OUT.#MSG
        //*  BT
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( RPT1. )
        Global.setEscapeCode(EscapeType.Bottom, "RPT1");
        if (true) return;
        //*  #PROCESS-ERROR
    }
    private void sub_Get_Rqst_And_Validate_Input_Params() throws Exception                                                                                                //Natural: GET-RQST-AND-VALIDATE-INPUT-PARAMS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  FOR DEBUGGING
        if (condition(pnd_L_Pnd_Debug.getBoolean()))                                                                                                                      //Natural: IF #DEBUG
        {
            getReports().write(0, "Processing #RQST-ID :-",pdaIatl401p.getPnd_Iatn401_In_Pnd_Rqst_Id());                                                                  //Natural: WRITE 'Processing #RQST-ID :-' #IATN401-IN.#RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK THAT #IANT401-IN.#RQST-ID EXISTS
        //*  FIND(1) IAA-TRNSFR-SW-RQST WITH RQST-ID = #IATN401-IN.#RQST-ID
        //*  IF NO RECORDS FOUND
        //*    COMPRESS 'Request id (' #IATN401-IN.#RQST-ID ') is not on file.'
        //*      INTO #IATN401-OUT.#MSG
        //*    PERFORM #PROCESS-ERROR
        //*  END-NOREC
        //*     #FND1-ISN := *ISN(FND1.)
        //*  END-FIND   /* FND1.
        //* *
        //* * TD 05/2002
        //*  READ IAA-TRNSFR-SW-RQST WITH IAA-TRNSFR-SW-RQST.RQST-ID
        //*        = #IATN401-IN.#RQST-ID
        //*   IF IAA-TRNSFR-SW-RQST.RQST-ID = #IATN401-IN.#RQST-ID
        //*    /*
        //*     #STAT-1-2  := IAA-TRNSFR-SW-RQST.XFR-STTS-CDE
        //*     IF #STAT-1 NE #AWAITING-FACTOR-A1
        //*       ESCAPE TOP
        //*     ELSE
        //*        XFR-STATUS := IAA-TRNSFR-SW-RQST.XFR-STTS-CDE
        //*        #FND1-ISN := *ISN(FND1.)
        //*      END-IF
        //*    ELSE
        //*      ESCAPE BOTTOM (FND1.)
        //*    END-IF
        //*   END-READ
        //* ***
        //*  JT 07/27/02
        FND1:                                                                                                                                                             //Natural: GET IAA-TRNSFR-SW-RQST #FND1-ISN
        vw_iaa_Trnsfr_Sw_Rqst.readByID(pnd_Fnd1_Isn.getLong(), "FND1");
        pnd_Stat_1_2.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde);                                                                                                           //Natural: ASSIGN #STAT-1-2 := IAA-TRNSFR-SW-RQST.XFR-STTS-CDE
        //*  WRITE '=' #FND1-ISN
        //*  WRITE '=' XFR-STATUS
        //*  WRITE '=' IAA-TRNSFR-SW-RQST.RQST-OPN-CLSD-IND
        //*  VALIDATE THAT THE STATUS OF THIS TRANSFER IS EITHER 'F*' FOR AWAITING
        //*  FACTOR OR 'M0' FOR COMPLETE  OR 'P0' FOR REJECTED
        //*  #STAT-1-2  := XFR-STATUS /* JT - COMMENTED OUT 07/27/02
        if (condition(pnd_Stat_1_2_Pnd_Stat_1.notEquals(pnd_Const_Pnd_Awaiting_Factor_A1)))                                                                               //Natural: IF #STAT-1 NE #AWAITING-FACTOR-A1
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Transfer RQST must be in Awaiting Factor status (F*).",    //Natural: COMPRESS 'Transfer RQST must be in Awaiting Factor status (F*).' 'RQST status is (' IAA-TRNSFR-SW-RQST.XFR-STTS-CDE ').' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                "RQST status is (", iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde, ")."));
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK THAT THIS TRANSFER/SWITCH REQUEST IS OPEN
        if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind.equals(pnd_Const_Pnd_Closed)))                                                                                 //Natural: IF IAA-TRNSFR-SW-RQST.RQST-OPN-CLSD-IND EQ #CLOSED
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Transfer Request closed."));                               //Natural: COMPRESS 'Transfer Request closed.' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE TRANSFER DATE
        pnd_Datea.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DATEA
        pnd_Todays_Datea.setValueEdited(pdaIatl401p.getPnd_Iatn401_In_Pnd_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #IATN401-IN.#TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DATEA
        //*  WRITE '=' #DATEA '=' #DATEN
        //*  WRITE '=' #TODAYS-DATEA
        if (condition(pnd_Datea_Pnd_Daten.greater(pnd_Todays_Datea_Pnd_Todays_Daten)))                                                                                    //Natural: IF #DATEN GT #TODAYS-DATEN
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Effective date '", pnd_Datea, "' must be before Todays Date. '",  //Natural: COMPRESS 'Effective date "' #DATEA '" must be before Todays Date. "'#TODAYS-DATEA '"' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                pnd_Todays_Datea, "'"));
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE THE #SYS-TIME
        if (condition(pdaIatl401p.getPnd_Iatn401_In_Pnd_Sys_Time().equals(getZero())))                                                                                    //Natural: IF #IATN401-IN.#SYS-TIME = 0
        {
            pdaIatl401p.getPnd_Iatn401_In_Pnd_Sys_Time().setValue(Global.getTIMX());                                                                                      //Natural: ASSIGN #IATN401-IN.#SYS-TIME := *TIMX
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE TODAYS DATE
        if (condition(pdaIatl401p.getPnd_Iatn401_In_Pnd_Todays_Dte().equals(getZero())))                                                                                  //Natural: IF #IATN401-IN.#TODAYS-DTE EQ 0
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Todays date is required."));                               //Natural: COMPRESS 'Todays date is required.' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE NEXT BUS DATE
        if (condition(pdaIatl401p.getPnd_Iatn401_In_Pnd_Next_Bus_Dte().equals(getZero())))                                                                                //Natural: IF #IATN401-IN.#NEXT-BUS-DTE EQ 0
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Next Busness date is required."));                         //Natural: COMPRESS 'Next Busness date is required.' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-RQST-AND-VALIDATE-INPUT-PARAMS
    }
    private void sub_Check_Retry_Count() throws Exception                                                                                                                 //Natural: CHECK-RETRY-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.greaterOrEqual(pnd_Const_Pnd_Num_Retry)))                                                                          //Natural: IF IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT GE #NUM-RETRY
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Return_Code().setValue("E");                                                                                               //Natural: MOVE 'E' TO #IATN401-OUT.#RETURN-CODE
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Error_Nbr().setValue("102");                                                                                               //Natural: MOVE '102' TO #IATN401-OUT.#ERROR-NBR
            //*  CHNGE STATUS REJECT
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Retry Count was exceeded for this RQST id. (",             //Natural: COMPRESS 'Retry Count was exceeded for this RQST id. (' *PROGRAM ')' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                Global.getPROGRAM(), ")"));
            pnd_Xfr_Rjctn_Cde.setValue("R0");                                                                                                                             //Natural: ASSIGN #XFR-RJCTN-CDE := 'R0'
            pnd_Rqst_Status_Cde.setValue(pnd_Const_Pnd_Stat_Rjctd_P4);                                                                                                    //Natural: ASSIGN #RQST-STATUS-CDE := #STAT-RJCTD-P4
            //*  TO REJECTED
                                                                                                                                                                          //Natural: PERFORM CHANGE-RQST-STATUS
            sub_Change_Rqst_Status();
            if (condition(Global.isEscape())) {return;}
            //*  END
                                                                                                                                                                          //Natural: PERFORM #ESCAPE-SUBPROG
            sub_Pnd_Escape_Subprog();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-RETRY-COUNT
    }
    private void sub_Check_Control_Record() throws Exception                                                                                                              //Natural: CHECK-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("AA");                                                                                                         //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'AA'
        //*  FETCH THE LATEST CONTROL RECORD
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Actvty_Cde().equals(pnd_Const_Pnd_Active) || pdaIatl400p.getPnd_Iatn400_Out_Pnd_Return_Code().equals(" ")))) //Natural: IF NOT ( #IATN400-OUT.CNTRL-ACTVTY-CDE EQ #CONST.#ACTIVE OR #IATN400-OUT.#RETURN-CODE EQ ' ' )
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(pdaIatl400p.getPnd_Iatn400_Out_Pnd_Msg());                                                                  //Natural: MOVE #IATN400-OUT.#MSG TO #IATN401-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   CHECK-CONTROL-RECORD
    }
    private void sub_Check_Factor_Available() throws Exception                                                                                                            //Natural: CHECK-FACTOR-AVAILABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("L");                                                                                          //Natural: ASSIGN #AIAN026-CALL-TYPE := 'L'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue("C");                                                                                       //Natural: ASSIGN #AIAN026-IA-FUND-CODE := 'C'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("A");                                                                                 //Natural: ASSIGN #AIAN026-REVALUATION-METHOD := 'A'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(99999999);                                                                             //Natural: ASSIGN #AIAN026-IAUV-REQUEST-DATE := 99999999
        //*  FETCH THE ANNUITY UNIT VALUE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        //* *IF RETURN-CODE NE 0 OR AUV-RETURNED = 0
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().notEquals(getZero()) || pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va().equals(getZero()))) //Natural: IF #AIAN026-RETURN-CODE-NBR NE 0 OR #AIAN026-INTERIM-ANNUITY-UNIT-VA = 0
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Error after call to (AIAN026), which retrieves the annuity",  //Natural: COMPRESS 'Error after call to (AIAN026), which retrieves the annuity' 'unit value.' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                "unit value."));
            //*  COMPRESS 'Return code (' RETURN-CODE ') AUV Returned (' AUV-RETURNED
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg_2().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Return code (", pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr(),  //Natural: COMPRESS 'Return code (' #AIAN026-RETURN-CODE-NBR ') AUV RETURNED (' #AIAN026-INTERIM-ANNUITY-UNIT-VA ')' INTO #IATN401-OUT.#MSG-2 LEAVING NO SPACE
                ") AUV RETURNED (", pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va(), ")"));
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE THE FACTOR DATE
        pnd_Datea.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DATEA
        //* *IF  #DATEN GT AUV-DATE-RETURN
        if (condition(pnd_Datea_Pnd_Daten.greater(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Date_Returned())))                                                   //Natural: IF #DATEN GT #AIAN026-IAUV-DATE-RETURNED
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "The Factor is not available for this Request."));          //Natural: COMPRESS 'The Factor is not available for this Request.' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
            //*  COMPRESS 'Latest available factor (' AUV-DATE-RETURN
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg_2().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "Latest available factor (", pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Date_Returned(),  //Natural: COMPRESS 'Latest available factor (' #AIAN026-IAUV-DATE-RETURNED '). Eff date is for (' #DATEN ').' INTO #IATN401-OUT.#MSG-2 LEAVING NO SPACE
                "). Eff date is for (", pnd_Datea_Pnd_Daten, ")."));
            //*  040612 END
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-FACTOR-AVAILABLE
    }
    private void sub_Check_If_Audit_File_Upadated() throws Exception                                                                                                      //Natural: CHECK-IF-AUDIT-FILE-UPADATED
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_iaa_Xfr_Audit.getTotalRowCount                                                                                                                                 //Natural: FIND NUMBER IAA-XFR-AUDIT RQST-ID = #IATN401-IN.#RQST-ID
        (
        new Wc[] { new Wc("RQST_ID", "=", pdaIatl401p.getPnd_Iatn401_In_Pnd_Rqst_Id(), WcType.WITH) }
        );
        if (condition(vw_iaa_Xfr_Audit.getAstNUMBER().notEquals(getZero())))                                                                                              //Natural: IF *NUMBER ( FND2. ) NE 0
        {
            pnd_L_Pnd_Audit_Rec_Exists.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #AUDIT-REC-EXISTS
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-AUDIT-FILE-UPADATED
    }
    private void sub_Update_Rqst_To_In_Progress() throws Exception                                                                                                        //Natural: UPDATE-RQST-TO-IN-PROGRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* * GET-REC1.
        //* /* GET IAA-TRNSFR-SW-RQST *ISN(FND1.)
        //* *GET IAA-TRNSFR-SW-RQST #FND1-ISN
        //* *IAA-TRNSFR-SW-RQST.XFR-IN-PROGRESS-IND := TRUE
        //* *IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT       := 1
        //* *IAA-TRNSFR-SW-RQST.RQST-LST-ACTVTY-DTE := #IATN401-IN.#TODAYS-DTE
        //*  IF THERE IS NO MIT LOG DTE AND TIME WE NEED TO ADD THE MIT REC IN
        //*  AWAITING FACTOR STATUS.
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme.equals(" ")))                                                                                                //Natural: IF IAA-TRNSFR-SW-RQST.XFR-MIT-LOG-DTE-TME = ' '
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-MIT
            sub_Update_Mit();
            if (condition(Global.isEscape())) {return;}
            //*  IAA-TRNSFR-SW-RQST.XFR-MIT-LOG-DTE-TME := IATA700.XFR-MIT-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************** FIX FOR 3.1.4
        //*  GET IAA-TRNSFR-SW-RQST *ISN(FND1.)
        GET_REC1:                                                                                                                                                         //Natural: GET IAA-TRNSFR-SW-RQST #FND1-ISN
        vw_iaa_Trnsfr_Sw_Rqst.readByID(pnd_Fnd1_Isn.getLong(), "GET_REC1");
        iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind.setValue(true);                                                                                                            //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-IN-PROGRESS-IND := TRUE
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.setValue(1);                                                                                                                     //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT := 1
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte.setValue(pdaIatl401p.getPnd_Iatn401_In_Pnd_Todays_Dte());                                                                  //Natural: ASSIGN IAA-TRNSFR-SW-RQST.RQST-LST-ACTVTY-DTE := #IATN401-IN.#TODAYS-DTE
        //*  IF THERE IS NO MIT LOG DTE AND TIME WE NEED TO ADD THE MIT REC IN
        //*  AWAITING FACTOR STATUS.
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme.equals(" ")))                                                                                                //Natural: IF IAA-TRNSFR-SW-RQST.XFR-MIT-LOG-DTE-TME = ' '
        {
            iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme.setValue(pdaIata700.getIata700_Xfr_Mit_Log_Dte_Tme());                                                                 //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-MIT-LOG-DTE-TME := IATA700.XFR-MIT-LOG-DTE-TME
            //* UPDATE
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Trnsfr_Sw_Rqst.updateDBRow("GET_REC1");                                                                                                                    //Natural: UPDATE ( GET-REC1. )
        //*  ET
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *BACKOUT TRANSACTION    /* ET
        //*  UPDATE-RQST-TO-IN-PROGRESS
    }
    private void sub_Update_Rqst_Retry_Count() throws Exception                                                                                                           //Natural: UPDATE-RQST-RETRY-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET IAA-TRNSFR-SW-RQST *ISN(FND1.)
        GET_REC3:                                                                                                                                                         //Natural: GET IAA-TRNSFR-SW-RQST #FND1-ISN
        vw_iaa_Trnsfr_Sw_Rqst.readByID(pnd_Fnd1_Isn.getLong(), "GET_REC3");
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.less(9)))                                                                                                          //Natural: IF IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT LT 9
        {
            //*  UPDATE
            iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.nadd(1);                                                                                                                     //Natural: COMPUTE IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT = IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT + 1
            iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte.setValue(pdaIatl401p.getPnd_Iatn401_In_Pnd_Todays_Dte());                                                              //Natural: ASSIGN IAA-TRNSFR-SW-RQST.RQST-LST-ACTVTY-DTE := #IATN401-IN.#TODAYS-DTE
            vw_iaa_Trnsfr_Sw_Rqst.updateDBRow("GET_REC3");                                                                                                                //Natural: UPDATE ( GET-REC3. )
            //*  ET
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //* * BACKOUT TRANSACTION    /* ET
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress("Retry Count = 9. Fatal Error. "));                                                        //Natural: COMPRESS 'Retry Count = 9. Fatal Error. ' INTO #IATN401-OUT.#MSG
            //*  INCLUDES BT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
            sub_Pnd_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-RQST-RETRY-COUNT
    }
    private void sub_Change_Rqst_Status() throws Exception                                                                                                                //Natural: CHANGE-RQST-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHANGE THE STATUS OF THE RQST
        //*  GET-REC4.
        //*  GET IAA-TRNSFR-SW-RQST *ISN(FND1.)
        GET01:                                                                                                                                                            //Natural: GET IAA-TRNSFR-SW-RQST #FND1-ISN
        vw_iaa_Trnsfr_Sw_Rqst.readByID(pnd_Fnd1_Isn.getLong(), "GET01");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.setValue(pnd_Rqst_Status_Cde);                                                                                                    //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE := #RQST-STATUS-CDE
        iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde.setValue(pnd_Xfr_Rjctn_Cde);                                                                                                     //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-RJCTN-CDE := #XFR-RJCTN-CDE
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.setValue(0);                                                                                                                     //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT := 0
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte.setValue(pdaIatl401p.getPnd_Iatn401_In_Pnd_Todays_Dte());                                                                  //Natural: ASSIGN IAA-TRNSFR-SW-RQST.RQST-LST-ACTVTY-DTE := #IATN401-IN.#TODAYS-DTE
        //*  REJECT THE TRANSFER/SWITCH
        if (condition(pnd_Rqst_Status_Cde.equals(pnd_Const_Pnd_Stat_Rjctd_P4)))                                                                                           //Natural: IF #RQST-STATUS-CDE = #STAT-RJCTD-P4
        {
            iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind.setValue(pnd_Const_Pnd_Closed);                                                                                          //Natural: ASSIGN IAA-TRNSFR-SW-RQST.RQST-OPN-CLSD-IND := #CLOSED
            iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind.setValue(false);                                                                                                       //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-IN-PROGRESS-IND := FALSE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-MIT
        sub_Update_Mit();
        if (condition(Global.isEscape())) {return;}
        //* ************* FIX FOR 3.1.4
        //*  GET IAA-TRNSFR-SW-RQST *ISN(FND1.)
        GET_REC4:                                                                                                                                                         //Natural: GET IAA-TRNSFR-SW-RQST #FND1-ISN
        vw_iaa_Trnsfr_Sw_Rqst.readByID(pnd_Fnd1_Isn.getLong(), "GET_REC4");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.setValue(pnd_Rqst_Status_Cde);                                                                                                    //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-STTS-CDE := #RQST-STATUS-CDE
        iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde.setValue(pnd_Xfr_Rjctn_Cde);                                                                                                     //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-RJCTN-CDE := #XFR-RJCTN-CDE
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt.setValue(0);                                                                                                                     //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-RETRY-CNT := 0
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte.setValue(pdaIatl401p.getPnd_Iatn401_In_Pnd_Todays_Dte());                                                                  //Natural: ASSIGN IAA-TRNSFR-SW-RQST.RQST-LST-ACTVTY-DTE := #IATN401-IN.#TODAYS-DTE
        //*  REJECT THE TRANSFER/SWITCH
        if (condition(pnd_Rqst_Status_Cde.equals(pnd_Const_Pnd_Stat_Rjctd_P4)))                                                                                           //Natural: IF #RQST-STATUS-CDE = #STAT-RJCTD-P4
        {
            iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind.setValue(pnd_Const_Pnd_Closed);                                                                                          //Natural: ASSIGN IAA-TRNSFR-SW-RQST.RQST-OPN-CLSD-IND := #CLOSED
            iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind.setValue(false);                                                                                                       //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-IN-PROGRESS-IND := FALSE
            //* UPDATE
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Trnsfr_Sw_Rqst.updateDBRow("GET_REC4");                                                                                                                    //Natural: UPDATE ( GET-REC4. )
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals(pnd_Const_Pnd_Stat_Complete_M0)))                                                                            //Natural: IF IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = #STAT-COMPLETE-M0
        {
            pnd_Non_Repetitive.reset();                                                                                                                                   //Natural: RESET #NON-REPETITIVE
            vw_rqst_Future.setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                                        //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO RQST-FUTURE
            short decideConditionsMet1141 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = '0' OR = ' ' ) OR ( IAA-TRNSFR-SW-RQST.XFR-WORK-PRCSS-ID = 'TAITG ' )
            if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals("0") || iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals(" ") || iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id.equals("TAITG ")))
            {
                decideConditionsMet1141++;
                pnd_Non_Repetitive.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #NON-REPETITIVE
            }                                                                                                                                                             //Natural: WHEN IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = '1'
            else if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals("1")))
            {
                decideConditionsMet1141++;
                rqst_Future_Rqst_Xfr_Type.setValue("2");                                                                                                                  //Natural: ASSIGN RQST-FUTURE.RQST-XFR-TYPE := '2'
                pnd_Percent.setValue(25);                                                                                                                                 //Natural: MOVE 25 TO #PERCENT
                                                                                                                                                                          //Natural: PERFORM #FILL-MULTI
                sub_Pnd_Fill_Multi();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = '2'
            else if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals("2")))
            {
                decideConditionsMet1141++;
                rqst_Future_Rqst_Xfr_Type.setValue("3");                                                                                                                  //Natural: ASSIGN RQST-FUTURE.RQST-XFR-TYPE := '3'
                pnd_Percent.setValue(33);                                                                                                                                 //Natural: MOVE 33 TO #PERCENT
                                                                                                                                                                          //Natural: PERFORM #FILL-MULTI
                sub_Pnd_Fill_Multi();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = '3'
            else if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals("3")))
            {
                decideConditionsMet1141++;
                rqst_Future_Rqst_Xfr_Type.setValue("4");                                                                                                                  //Natural: ASSIGN RQST-FUTURE.RQST-XFR-TYPE := '4'
                pnd_Percent.setValue(50);                                                                                                                                 //Natural: MOVE 50 TO #PERCENT
                                                                                                                                                                          //Natural: PERFORM #FILL-MULTI
                sub_Pnd_Fill_Multi();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = '4'
            else if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals("4")))
            {
                decideConditionsMet1141++;
                rqst_Future_Rqst_Xfr_Type.setValue("5");                                                                                                                  //Natural: ASSIGN RQST-FUTURE.RQST-XFR-TYPE := '5'
                pnd_Percent.setValue(100);                                                                                                                                //Natural: MOVE 100 TO #PERCENT
                                                                                                                                                                          //Natural: PERFORM #FILL-MULTI
                sub_Pnd_Fill_Multi();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE = '5'
            else if (condition(iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type.equals("5")))
            {
                decideConditionsMet1141++;
                pnd_Non_Repetitive.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #NON-REPETITIVE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(0, "ERROR IN RQST-XFR-TYPE","=",iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type);                                                                     //Natural: WRITE 'ERROR IN RQST-XFR-TYPE' '=' IAA-TRNSFR-SW-RQST.RQST-XFR-TYPE
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Non_Repetitive.equals(" ")))                                                                                                                //Natural: IF #NON-REPETITIVE = ' '
            {
                                                                                                                                                                          //Natural: PERFORM #NEXT-YEARS-REQUEST
                sub_Pnd_Next_Years_Request();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ET
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pdaIatl401p.getPnd_Iatn401_Out_Pnd_Xfr_Stts_Cde().setValue(pnd_Rqst_Status_Cde);                                                                                  //Natural: ASSIGN #IATN401-OUT.#XFR-STTS-CDE := #RQST-STATUS-CDE
        //*  CHANGE-RQST-STATUS
    }
    private void sub_Check_State_Approval() throws Exception                                                                                                              //Natural: CHECK-STATE-APPROVAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ANNUAL TRANSFERS WITH EFFECTIVE DATE 03/31 DO NOT HAVE TO BE APPROVED
        //*  BY THE STATE.
        pnd_Datea.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DATEA
        if (condition((! (((pnd_Datea_Pnd_Date_Dd.equals(31) && pnd_Datea_Pnd_Date_Mm.equals(3)) && iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ.getValue(1).equals("A")))         //Natural: IF NOT ( #DATE-DD EQ 31 AND #DATE-MM EQ 3 AND IAA-TRNSFR-SW-RQST.XFR-FRM-UNIT-TYP ( 1 ) EQ 'A' ) OR IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE EQ #RCRD-TYPE-SWITCH
            || iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch))))
        {
            if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch)))                                                                       //Natural: IF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE EQ #RCRD-TYPE-SWITCH
            {
                pdaIatl403p.getPnd_Iatn403_In_Pnd_Transfer_Switch().setValue(pnd_Const_Pnd_Switch_S);                                                                     //Natural: ASSIGN #IATN403-IN.#TRANSFER-SWITCH := #SWITCH-S
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIatl403p.getPnd_Iatn403_In_Pnd_Transfer_Switch().setValue(pnd_Const_Pnd_Transfer_T);                                                                   //Natural: ASSIGN #IATN403-IN.#TRANSFER-SWITCH := #TRANSFER-T
            }                                                                                                                                                             //Natural: END-IF
            pdaIatl403p.getPnd_Iatn403_In().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                       //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO #IATN403-IN
            DbsUtil.callnat(Iatn403.class , getCurrentProcessState(), pdaIatl403p.getPnd_Iatn403_In(), pdaIatl403p.getPnd_Iatn403_Out());                                 //Natural: CALLNAT 'IATN403' #IATN403-IN #IATN403-OUT
            if (condition(Global.isEscape())) return;
            if (condition(pdaIatl403p.getPnd_Iatn403_Out_Pnd_Return_Code().equals("E")))                                                                                  //Natural: IF #IATN403-OUT.#RETURN-CODE = 'E'
            {
                pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(pdaIatl403p.getPnd_Iatn403_Out_Pnd_Msg());                                                              //Natural: MOVE #IATN403-OUT.#MSG TO #IATN401-OUT.#MSG
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
                sub_Pnd_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pdaIatl403p.getPnd_Iatn403_Out_Pnd_State_Approved().getBoolean())))                                                                          //Natural: IF NOT #STATE-APPROVED
            {
                pnd_Xfr_Rjctn_Cde.setValue("ST");                                                                                                                         //Natural: ASSIGN #XFR-RJCTN-CDE := 'ST'
                //*  REJECT SWITCH / AWAIT CONFIR
                if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch)))                                                                   //Natural: IF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE EQ #RCRD-TYPE-SWITCH
                {
                    pnd_Rqst_Status_Cde.setValue(pnd_Const_Pnd_Stat_Rjctd_P0);                                                                                            //Natural: ASSIGN #RQST-STATUS-CDE := #STAT-RJCTD-P0
                    //*  REJECT AND CLOSE TRANSFER
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rqst_Status_Cde.setValue(pnd_Const_Pnd_Stat_Rjctd_P4);                                                                                            //Natural: ASSIGN #RQST-STATUS-CDE := #STAT-RJCTD-P4
                }                                                                                                                                                         //Natural: END-IF
                //*  TO REJECTED
                                                                                                                                                                          //Natural: PERFORM CHANGE-RQST-STATUS
                sub_Change_Rqst_Status();
                if (condition(Global.isEscape())) {return;}
                pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress("Transfer RQST Rejected. Error with State approval.", Global.getPROGRAM()));           //Natural: COMPRESS 'Transfer RQST Rejected. Error with State approval.' *PROGRAM INTO #IATN401-OUT.#MSG
                pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg_2().setValue(DbsUtil.compress("Residence Sate = ", pdaIatl403p.getPnd_Iatn403_Out_Pnd_Residence_State(),           //Natural: COMPRESS 'Residence Sate = ' #RESIDENCE-STATE 'FROM Issue State = ' #ISSUE-STATE 'TO Issue State = ' #TO-ISSUE-STATE INTO #MSG-2
                    "FROM Issue State = ", pdaIatl403p.getPnd_Iatn403_Out_Pnd_Issue_State(), "TO Issue State = ", pdaIatl403p.getPnd_Iatn403_Out_Pnd_To_Issue_State()));
                //*  END
                                                                                                                                                                          //Natural: PERFORM #ESCAPE-SUBPROG
                sub_Pnd_Escape_Subprog();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-STATE-APPROVAL
    }
    private void sub_Process_Db03_Changes() throws Exception                                                                                                              //Natural: PROCESS-DB03-CHANGES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  PROCESS ALL THE CHANGES IN DB03 AS ONE LOGICAL TRANSACTION
                                                                                                                                                                          //Natural: PERFORM FILL-TO-QTY-OF-SW
        sub_Fill_To_Qty_Of_Sw();
        if (condition(Global.isEscape())) {return;}
        pdaIatl420z.getIatl010().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                                  //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO IATL010
        pdaIatl420z.getIatl010_Pnd_Sys_Time().setValue(pdaIatl401p.getPnd_Iatn401_In_Pnd_Sys_Time());                                                                     //Natural: MOVE #IATN401-IN.#SYS-TIME TO IATL010.#SYS-TIME
        pdaIatl420z.getIatl010_Pnd_Todays_Dte().setValue(pdaIatl401p.getPnd_Iatn401_In_Pnd_Todays_Dte());                                                                 //Natural: MOVE #IATN401-IN.#TODAYS-DTE TO IATL010.#TODAYS-DTE
        pdaIatl420z.getIatl010_Pnd_Next_Bus_Dte().setValue(pdaIatl401p.getPnd_Iatn401_In_Pnd_Next_Bus_Dte());                                                             //Natural: MOVE #IATN401-IN.#NEXT-BUS-DTE TO IATL010.#NEXT-BUS-DTE
        pdaIatl420z.getIatl010_Pnd_Check_Dte().setValue(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Check_Dte());                                                                //Natural: MOVE #IATN400-OUT.CNTRL-CHECK-DTE TO IATL010.#CHECK-DTE
        //*  6/99 KN
        if (condition(pdaIatl420z.getIatl010_Rqst_Xfr_Type().equals(" ")))                                                                                                //Natural: IF IATL010.RQST-XFR-TYPE = ' '
        {
            DbsUtil.callnat(Iatn420.class , getCurrentProcessState(), pdaIatl420z.getIatl010(), pdaIatl420z.getPnd_Iatn420_Out());                                        //Natural: CALLNAT 'IATN420' IATL010 #IATN420-OUT
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Iatn422.class , getCurrentProcessState(), pdaIatl420z.getIatl010(), pdaIatl420z.getPnd_Iatn420_Out());                                        //Natural: CALLNAT 'IATN422' IATL010 #IATN420-OUT
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaIatl401p.getPnd_Iatn401_Out_Pnd_Return_Code_Iatn420().setValue(pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code());                                              //Natural: ASSIGN #IATN401-OUT.#RETURN-CODE-IATN420 := #IATN420-OUT.#RETURN-CODE
        //*  IF ONLY AN IVC ERROR OCCURED, SAVE THE REJECT CODE AND CONTINUE
        //*  THE PROCESS AS IF NO ERRORS OCCURED.
        //*  REJECT CODE
        if (condition(pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().equals("IV")))                                                                                     //Natural: IF #IATN420-OUT.#RETURN-CODE EQ 'IV'
        {
            pnd_Xfr_Rjctn_Cde.setValue(pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code());                                                                                 //Natural: ASSIGN #XFR-RJCTN-CDE := #IATN420-OUT.#RETURN-CODE
            pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().reset();                                                                                                     //Natural: RESET #IATN420-OUT.#RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  AN ERROR OCCURED
        if (condition(pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code().notEquals(" ")))                                                                                   //Natural: IF #IATN420-OUT.#RETURN-CODE NE ' '
        {
            //*  BT DATA BASE 03
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Error_Nbr().setValue("150");                                                                                               //Natural: ASSIGN #IATN401-OUT.#ERROR-NBR := '150'
            //*  REJECT CODE
            //*  CHNGE STATUS REJECT
            pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RQST Rejected. Error returned from 'IATN420'.",            //Natural: COMPRESS 'RQST Rejected. Error returned from "IATN420".' *PROGRAM INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                Global.getPROGRAM()));
            pnd_Xfr_Rjctn_Cde.setValue(pdaIatl420z.getPnd_Iatn420_Out_Pnd_Return_Code());                                                                                 //Natural: ASSIGN #XFR-RJCTN-CDE := #IATN420-OUT.#RETURN-CODE
            pnd_Rqst_Status_Cde.setValue(pnd_Const_Pnd_Stat_Rjctd_P4);                                                                                                    //Natural: ASSIGN #RQST-STATUS-CDE := #STAT-RJCTD-P4
            //*  TRANSFER COMPLETE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_L_Pnd_Debug.getBoolean()))                                                                                                                  //Natural: IF #DEBUG
            {
                getReports().write(0, "END TRANSACTION");                                                                                                                 //Natural: WRITE 'END TRANSACTION'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  ET DATA BASE 03
            //*  CHNGE STATUS TO COMPLETE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Rqst_Status_Cde.setValue(pnd_Const_Pnd_Stat_Complete_M0);                                                                                                 //Natural: ASSIGN #RQST-STATUS-CDE := #STAT-COMPLETE-M0
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-DB03-CHANGES
    }
    private void sub_Fill_To_Qty_Of_Sw() throws Exception                                                                                                                 //Natural: FILL-TO-QTY-OF-SW
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHANGE THE TO QTY TO BE 100%
        if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch)))                                                                           //Natural: IF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE = #RCRD-TYPE-SWITCH
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                //*    IF IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE(#I) NE ' '
                if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).notEquals(" ")))                                                                        //Natural: IF IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) NE ' '
                {
                    iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty.getValue(pnd_I).setValue(100);                                                                                          //Natural: ASSIGN IAA-TRNSFR-SW-RQST.XFR-TO-QTY ( #I ) := 100
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PERFORM FILL-TO-QTY-OF-SW
    }
    private void sub_Update_Mit() throws Exception                                                                                                                        //Natural: UPDATE-MIT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHANGE THE STATUS OF MIT
        pdaIata700.getIata700().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                                   //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO IATA700
        pdaIata700.getIata700_Type_Of_Process().setValue("BATCH");                                                                                                        //Natural: ASSIGN IATA700.TYPE-OF-PROCESS := 'BATCH'
        short decideConditionsMet1303 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE;//Natural: VALUE #RCRD-TYPE-TRNSFR
        if (condition((iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Trnsfr))))
        {
            decideConditionsMet1303++;
            pdaIata700.getIata700_Rqst_Type().setValue("T");                                                                                                              //Natural: ASSIGN IATA700.RQST-TYPE := 'T'
        }                                                                                                                                                                 //Natural: VALUE #RCRD-TYPE-SWITCH
        else if (condition((iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch))))
        {
            decideConditionsMet1303++;
            pdaIata700.getIata700_Rqst_Type().setValue("S");                                                                                                              //Natural: ASSIGN IATA700.RQST-TYPE := 'S'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pdaIatl401p.getPnd_Iatn401_Out_Mit_Msg().setValue("RCRD-TYPE-CDE is not valid. Could not update MIT.");                                                       //Natural: MOVE 'RCRD-TYPE-CDE is not valid. Could not update MIT.' TO #IATN401-OUT.MIT-MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.callnat(Iatn700.class , getCurrentProcessState(), pdaIata700.getIata700());                                                                               //Natural: CALLNAT 'IATN700' IATA700
        if (condition(Global.isEscape())) return;
        pdaIatl401p.getPnd_Iatn401_Out().setValuesByName(pdaIata700.getIata700());                                                                                        //Natural: MOVE BY NAME IATA700 TO #IATN401-OUT
        //*   UPDATE-MIT
    }
    private void sub_Pnd_Next_Years_Request() throws Exception                                                                                                            //Natural: #NEXT-YEARS-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Timn.setValue(Global.getTIMN());                                                                                                                              //Natural: ASSIGN #TIMN := *TIMN
        rqst_Future_Rqst_Entry_Tme.setValue(pnd_Timn);                                                                                                                    //Natural: ASSIGN RQST-FUTURE.RQST-ENTRY-TME := #TIMN
        rqst_Future_Rqst_Rcvd_Tme.setValue(pnd_Timn);                                                                                                                     //Natural: ASSIGN RQST-FUTURE.RQST-RCVD-TME := #TIMN
        rqst_Future_Rqst_Effctv_Dte.setValue(Global.getTIMX());                                                                                                           //Natural: ASSIGN RQST-FUTURE.RQST-EFFCTV-DTE := *TIMX
        rqst_Future_Rqst_Entry_Dte.setValue(Global.getTIMX());                                                                                                            //Natural: ASSIGN RQST-FUTURE.RQST-ENTRY-DTE := *TIMX
        rqst_Future_Rqst_Lst_Actvty_Dte.setValue(Global.getTIMX());                                                                                                       //Natural: ASSIGN RQST-FUTURE.RQST-LST-ACTVTY-DTE := *TIMX
        rqst_Future_Rqst_Rcvd_Dte.setValue(Global.getTIMX());                                                                                                             //Natural: ASSIGN RQST-FUTURE.RQST-RCVD-DTE := *TIMX
        rqst_Future_Rqst_Rcvd_User_Id.setValue("IAIQ");                                                                                                                   //Natural: ASSIGN RQST-FUTURE.RQST-RCVD-USER-ID := 'IAIQ'
        rqst_Future_Rqst_Entry_User_Id.setValue("IAIQ");                                                                                                                  //Natural: ASSIGN RQST-FUTURE.RQST-ENTRY-USER-ID := 'IAIQ'
        rqst_Future_Rqst_Cntct_Mde.setValue(" ");                                                                                                                         //Natural: ASSIGN RQST-FUTURE.RQST-CNTCT-MDE := ' '
        rqst_Future_Rqst_Srce.setValue(" ");                                                                                                                              //Natural: ASSIGN RQST-FUTURE.RQST-SRCE := ' '
        rqst_Future_Rqst_Rep_Nme.setValue(" ");                                                                                                                           //Natural: ASSIGN RQST-FUTURE.RQST-REP-NME := ' '
        rqst_Future_Xfr_In_Progress_Ind.setValue(false);                                                                                                                  //Natural: ASSIGN RQST-FUTURE.XFR-IN-PROGRESS-IND := FALSE
        rqst_Future_Rqst_Sttmnt_Ind.setValue("Y");                                                                                                                        //Natural: ASSIGN RQST-FUTURE.RQST-STTMNT-IND := 'Y'
        rqst_Future_Rqst_Opn_Clsd_Ind.setValue("O");                                                                                                                      //Natural: ASSIGN RQST-FUTURE.RQST-OPN-CLSD-IND := 'O'
        rqst_Future_Xfr_Mit_Log_Dte_Tme.setValue(" ");                                                                                                                    //Natural: ASSIGN RQST-FUTURE.XFR-MIT-LOG-DTE-TME := ' '
        rqst_Future_Xfr_Stts_Cde.setValue("F2");                                                                                                                          //Natural: ASSIGN RQST-FUTURE.XFR-STTS-CDE := 'F2'
        rqst_Future_Xfr_Rjctn_Cde.setValue(" ");                                                                                                                          //Natural: ASSIGN RQST-FUTURE.XFR-RJCTN-CDE := ' '
        rqst_Future_Xfr_Retry_Cnt.setValue(0);                                                                                                                            //Natural: ASSIGN RQST-FUTURE.XFR-RETRY-CNT := 0
        rqst_Future_Ia_Hold_Cde.setValue(" ");                                                                                                                            //Natural: ASSIGN RQST-FUTURE.IA-HOLD-CDE := ' '
        rqst_Future_Xfr_Frm_Asset_Amt.getValue("*").setValue(0);                                                                                                          //Natural: ASSIGN RQST-FUTURE.XFR-FRM-ASSET-AMT ( * ) := 0
        rqst_Future_Xfr_To_Asset_Amt.getValue("*").setValue(0);                                                                                                           //Natural: ASSIGN RQST-FUTURE.XFR-TO-ASSET-AMT ( * ) := 0
        rqst_Future_Ia_Appl_Rcvd_Dte.reset();                                                                                                                             //Natural: RESET RQST-FUTURE.IA-APPL-RCVD-DTE
        rqst_Future_Ia_New_Issue.setValue(" ");                                                                                                                           //Natural: ASSIGN RQST-FUTURE.IA-NEW-ISSUE := ' '
        rqst_Future_Ia_Rsn_For_Ovrrde.setValue(" ");                                                                                                                      //Natural: ASSIGN RQST-FUTURE.IA-RSN-FOR-OVRRDE := ' '
        rqst_Future_Ia_Ovrrde_User_Id.setValue(" ");                                                                                                                      //Natural: ASSIGN RQST-FUTURE.IA-OVRRDE-USER-ID := ' '
        rqst_Future_Ia_Gnrl_Pend_Cde.setValue(" ");                                                                                                                       //Natural: ASSIGN RQST-FUTURE.IA-GNRL-PEND-CDE := ' '
        rqst_Future_Ia_New_Iss_Prt_Pull.setValue(" ");                                                                                                                    //Natural: ASSIGN RQST-FUTURE.IA-NEW-ISS-PRT-PULL := ' '
        //* **   FILLING REQUEST EFFECTIVE DATE ***
        pnd_Hold_Effective_Dte.reset();                                                                                                                                   //Natural: RESET #HOLD-EFFECTIVE-DTE #H-EFFECTIVE-DTE
        pnd_H_Effective_Dte.reset();
        pnd_Ia_Super_De_04_Pnd_De_04_Type_Cde.setValue("1");                                                                                                              //Natural: MOVE '1' TO #DE-04-TYPE-CDE
        pnd_Ia_Super_De_04_Pnd_De_04_Frm_Cntrct.setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                               //Natural: MOVE IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT TO #DE-04-FRM-CNTRCT
        pnd_Ia_Super_De_04_Pnd_De_04_Frm_Payee.setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee);                                                                                 //Natural: MOVE IAA-TRNSFR-SW-RQST.IA-FRM-PAYEE TO #DE-04-FRM-PAYEE
        pnd_Ia_Super_De_04_Pnd_De_04_Effctv_Dte.setValue(0);                                                                                                              //Natural: MOVE 0 TO #DE-04-EFFCTV-DTE
        vw_rqst_First.startDatabaseRead                                                                                                                                   //Natural: READ RQST-FIRST BY IA-SUPER-DE-04 STARTING FROM #IA-SUPER-DE-04
        (
        "R1",
        new Wc[] { new Wc("IA_SUPER_DE_04", ">=", pnd_Ia_Super_De_04.getBinary(), WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_04", "ASC") }
        );
        R1:
        while (condition(vw_rqst_First.readNextRow("R1")))
        {
            if (condition(rqst_First_Ia_Frm_Cntrct.notEquals(pnd_Ia_Super_De_04_Pnd_De_04_Frm_Cntrct) && rqst_First_Ia_Frm_Payee.notEquals(pnd_Ia_Super_De_04_Pnd_De_04_Frm_Payee))) //Natural: IF RQST-FIRST.IA-FRM-CNTRCT NE #DE-04-FRM-CNTRCT AND RQST-FIRST.IA-FRM-PAYEE NE #DE-04-FRM-PAYEE
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rqst_First_Rqst_Xfr_Type.equals("1") && rqst_First_Ia_To_Cntrct.equals(iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct) && rqst_First_Ia_To_Payee.equals(iaa_Trnsfr_Sw_Rqst_Ia_To_Payee))) //Natural: IF RQST-FIRST.RQST-XFR-TYPE = '1' AND RQST-FIRST.IA-TO-CNTRCT = IAA-TRNSFR-SW-RQST.IA-TO-CNTRCT AND RQST-FIRST.IA-TO-PAYEE = IAA-TRNSFR-SW-RQST.IA-TO-PAYEE
            {
                pnd_Hold_Effective_Dte.setValue(rqst_First_Rqst_Effctv_Dte);                                                                                              //Natural: ASSIGN #HOLD-EFFECTIVE-DTE := RQST-FIRST.RQST-EFFCTV-DTE
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_H_Effective_Dte.equals(getZero())))                                                                                                         //Natural: IF #H-EFFECTIVE-DTE = 0
            {
                pnd_H_Effective_Dte.setValue(rqst_First_Rqst_Effctv_Dte);                                                                                                 //Natural: ASSIGN #H-EFFECTIVE-DTE := RQST-FIRST.RQST-EFFCTV-DTE
            }                                                                                                                                                             //Natural: END-IF
            //*  RQST-FIRST
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Hold_Effective_Dte.equals(getZero())))                                                                                                          //Natural: IF #HOLD-EFFECTIVE-DTE = 0
        {
            pnd_Hold_Effective_Dte.setValue(pnd_H_Effective_Dte);                                                                                                         //Natural: ASSIGN #HOLD-EFFECTIVE-DTE := #H-EFFECTIVE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Effective_Dte.equals(getZero())))                                                                                                          //Natural: IF #HOLD-EFFECTIVE-DTE = 0
        {
            getReports().write(0, "THERE IS NO FIRST YEAR EFFECTIVE DATE FOR",rqst_First_Ia_Frm_Cntrct,rqst_First_Ia_Frm_Payee);                                          //Natural: WRITE 'THERE IS NO FIRST YEAR EFFECTIVE DATE FOR' RQST-FIRST.IA-FRM-CNTRCT RQST-FIRST.IA-FRM-PAYEE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  THIS IS FIRST YEAR EFFECTIVE DATE
            pnd_Passed_Date_Pnd_Passed_Date_A.setValueEdited(pnd_Hold_Effective_Dte,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #HOLD-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #PASSED-DATE-A
            //*  THIS IS THE PRESENT YEAR EFFECTIVE DATE
            pnd_Present_Date_Pnd_Present_Date_A.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #PRESENT-DATE-A
            pnd_Passed_Date_Pnd_Passed_Date_Cc.setValue(pnd_Present_Date_Pnd_Present_Date_Cc);                                                                            //Natural: ASSIGN #PASSED-DATE-CC := #PRESENT-DATE-CC
            pnd_Passed_Date_Pnd_Passed_Date_Yy.setValue(pnd_Present_Date_Pnd_Present_Date_Yy);                                                                            //Natural: ASSIGN #PASSED-DATE-YY := #PRESENT-DATE-YY
            //*  THIS WILL GIVE THE VARIABLE #PASSED-DATE THE FIRST YEARS DAY AND MONTH
            //*  WITH THE PRESENT YEAR. NEXT STATEMENT WILL GO ONE YEAR IN FUTURE
            if (condition(pnd_Passed_Date_Pnd_Passed_Date_Yy.equals(99)))                                                                                                 //Natural: IF #PASSED-DATE-YY = 99
            {
                pnd_Passed_Date_Pnd_Passed_Date_Yy_A.setValue("00");                                                                                                      //Natural: MOVE '00' TO #PASSED-DATE-YY-A
                pnd_Passed_Date_Pnd_Passed_Date_Cc.nadd(1);                                                                                                               //Natural: ADD 1 TO #PASSED-DATE-CC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Passed_Date_Pnd_Passed_Date_Yy.nadd(1);                                                                                                               //Natural: ADD 1 TO #PASSED-DATE-YY
                if (condition(DbsUtil.maskMatches(pnd_Passed_Date_Pnd_Passed_Date_A,"....'0229'")))                                                                       //Natural: IF #PASSED-DATE-A = MASK ( ....'0229' )
                {
                    if (condition(DbsUtil.maskMatches(pnd_Passed_Date_Pnd_Passed_Date_A,"YYYYMMDD")))                                                                     //Natural: IF #PASSED-DATE-A = MASK ( YYYYMMDD )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Passed_Date_Pnd_Passed_Date_Dd.nsubtract(1);                                                                                                  //Natural: SUBTRACT 1 FROM #PASSED-DATE-DD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  MEOW
            pnd_Passed_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Passed_Date_Pnd_Passed_Date_A);                                                           //Natural: MOVE EDITED #PASSED-DATE-A TO #PASSED-DATE-D ( EM = YYYYMMDD )
            //*  MEOW
            DbsUtil.callnat(Adsn607.class , getCurrentProcessState(), pnd_Passed_Date_D, pnd_Rc);                                                                         //Natural: CALLNAT 'ADSN607' #PASSED-DATE-D #RC
            if (condition(Global.isEscape())) return;
            //*  RP. REPEAT
            //*    RESET #WORK-FIELD
            //*    ADD 1 TO #T
            //*  INCREASED COUNT FROM 4 TO 11 - JFT 01/23/01
            //*    IF #T = 11 ESCAPE BOTTOM END-IF
            //*    CALLNAT 'NAZN110A' MSG-INFO-SUB
            //*      #PASSED-DATE
            //*      #WORK-FIELD
            //*    IF MSG-INFO-SUB.##RETURN-CODE = 'Y'
            //*      MOVE EDITED #PASSED-DATE-A  TO
            //*        RQST-FUTURE.RQST-EFFCTV-DTE (EM=YYYYMMDD)
            //*      ESCAPE BOTTOM(RP.)
            //*    END-IF
            //*    MOVE EDITED #PASSED-DATE-A  TO #PASSED-DATE-D (EM=YYYYMMDD)
            //*    SUBTRACT 1 FROM #PASSED-DATE-D
            //*    MOVE EDITED #PASSED-DATE-D (EM=YYYYMMDD) TO #PASSED-DATE-A
            //*  END-REPEAT
            //*  ISSUE AN ERROR IF THERE IS NO VALID BUSINESS DATE FOR FUTURE REQ
            //*  JFT 01/23/01
            //*  IF MSG-INFO-SUB.##RETURN-CODE NE 'Y'
            if (condition(pnd_Rc.notEquals(getZero())))                                                                                                                   //Natural: IF #RC NE 0
            {
                pdaIatl401p.getPnd_Iatn401_Out_Pnd_Msg().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "The next effect date is invalid. Check the calendar file")); //Natural: COMPRESS 'The next effect date is invalid. Check the calendar file' INTO #IATN401-OUT.#MSG LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-ERROR
                sub_Pnd_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  MEOW
        }                                                                                                                                                                 //Natural: END-IF
        rqst_Future_Rqst_Effctv_Dte.setValue(pnd_Passed_Date_D);                                                                                                          //Natural: ASSIGN RQST-FUTURE.RQST-EFFCTV-DTE := #PASSED-DATE-D
        pnd_Work_Date_A.setValueEdited(rqst_Future_Rqst_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED RQST-FUTURE.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #WORK-DATE-A
        //*      #WORK-DATE-A := RQST-FUTURE.RQST-EFFCTV-DTE
        rqst_Future_Rqst_Rcprcl_Dte.compute(new ComputeParameters(false, rqst_Future_Rqst_Rcprcl_Dte), DbsField.subtract(99999999,pnd_Work_Date_A_Pnd_Work_Date_N));      //Natural: COMPUTE RQST-FUTURE.RQST-RCPRCL-DTE = 99999999 - #WORK-DATE-N
        pnd_S_Pin_Nbr.setValue(rqst_Future_Ia_Unique_Id);                                                                                                                 //Natural: ASSIGN #S-PIN-NBR := RQST-FUTURE.IA-UNIQUE-ID
        pnd_Work_Date_A.setValueEdited(rqst_Future_Rqst_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED RQST-FUTURE.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #WORK-DATE-A
        pnd_S_Rcprcl_Dte.compute(new ComputeParameters(false, pnd_S_Rcprcl_Dte), DbsField.subtract(99999999,pnd_Work_Date_A_Pnd_Work_Date_N));                            //Natural: ASSIGN #S-RCPRCL-DTE := 99999999 - #WORK-DATE-N
        pnd_S_Rcvd_Dte.setValueEdited(rqst_Future_Rqst_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                                          //Natural: MOVE EDITED RQST-FUTURE.RQST-RCVD-DTE ( EM = YYYYMMDD ) TO #S-RCVD-DTE
        pnd_S_Rcvd_Tme.setValue(pnd_Timn_Pnd_Time);                                                                                                                       //Natural: ASSIGN #S-RCVD-TME := #TIME
        pnd_Save_Rqst_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_S_Pin_Nbr, pnd_S_Rcprcl_Dte, pnd_S_Rcvd_Dte, pnd_S_Rcvd_Tme_Pnd_S_Rcvd_Tme_A));     //Natural: COMPRESS #S-PIN-NBR #S-RCPRCL-DTE #S-RCVD-DTE #S-RCVD-TME-A INTO #SAVE-RQST-ID LEAVING NO
        RN:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            vw_hist_View.createHistogram                                                                                                                                  //Natural: HISTOGRAM ( 1 ) HIST-VIEW FOR RQST-ID FROM #SAVE-RQST-ID THRU #SAVE-RQST-ID
            (
            "CHECK_IF_EXISTS",
            "RQST_ID",
            new Wc[] { new Wc("RQST_ID", ">=", pnd_Save_Rqst_Id, "And", WcType.WITH) ,
            new Wc("RQST_ID", "<=", pnd_Save_Rqst_Id, WcType.WITH) },
            1
            );
            CHECK_IF_EXISTS:
            while (condition(vw_hist_View.readNextRow("CHECK_IF_EXISTS")))
            {
            }                                                                                                                                                             //Natural: END-HISTOGRAM
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RN"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RN"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(vw_hist_View.getAstNUMBER().greater(getZero())))                                                                                                //Natural: IF *NUMBER ( CHECK-IF-EXISTS. ) GT 0
            {
                getReports().write(0, "**** DUPLICATE RQST-ID CAUGHT:",pnd_Save_Rqst_Id);                                                                                 //Natural: WRITE '**** DUPLICATE RQST-ID CAUGHT:' #SAVE-RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RN"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RN"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Timn_Pnd_Time.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TIME
                pnd_S_Rcvd_Tme.setValue(pnd_Timn_Pnd_Time);                                                                                                               //Natural: ASSIGN #S-RCVD-TME := #TIME
                pnd_Save_Rqst_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_S_Pin_Nbr, pnd_S_Rcprcl_Dte, pnd_S_Rcvd_Dte, pnd_S_Rcvd_Tme_Pnd_S_Rcvd_Tme_A)); //Natural: COMPRESS #S-PIN-NBR #S-RCPRCL-DTE #S-RCVD-DTE #S-RCVD-TME-A INTO #SAVE-RQST-ID LEAVING NO
                getReports().write(0, "**** NEW RQST-ID TO REPLACE:",pnd_Save_Rqst_Id);                                                                                   //Natural: WRITE '**** NEW RQST-ID TO REPLACE:' #SAVE-RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RN"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RN"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RN;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RN. )
            }                                                                                                                                                             //Natural: END-IF
            //*  082017
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        rqst_Future_Rqst_Id.setValue(pnd_Save_Rqst_Id);                                                                                                                   //Natural: ASSIGN RQST-FUTURE.RQST-ID := #SAVE-RQST-ID
        pnd_Dup_Super_De_04_Pnd_Dup_Frm_Cntrct.setValue(rqst_Future_Ia_Frm_Cntrct);                                                                                       //Natural: ASSIGN #DUP-FRM-CNTRCT := RQST-FUTURE.IA-FRM-CNTRCT
        pnd_Dup_Super_De_04_Pnd_Dup_Frm_Payee.setValue(rqst_Future_Ia_Frm_Payee);                                                                                         //Natural: ASSIGN #DUP-FRM-PAYEE := RQST-FUTURE.IA-FRM-PAYEE
        pnd_Dup_Super_De_04_Pnd_Dup_Effctv_Dte.setValue(rqst_Future_Rqst_Effctv_Dte);                                                                                     //Natural: ASSIGN #DUP-EFFCTV-DTE := RQST-FUTURE.RQST-EFFCTV-DTE
        vw_dup_Rqst.startDatabaseFind                                                                                                                                     //Natural: FIND DUP-RQST WITH IA-SUPER-DE-04 = #DUP-SUPER-DE-04
        (
        "FIND01",
        new Wc[] { new Wc("IA_SUPER_DE_04", "=", pnd_Dup_Super_De_04.getBinary(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_dup_Rqst.readNextRow("FIND01")))
        {
            vw_dup_Rqst.setIfNotFoundControlFlag(false);
            //*  OPEN OR COMPLETED RQST WITH SAME
            //*  EFFECTIVE DATE EXISTS
            if (condition(dup_Rqst_Xfr_Stts_Cde.equals("F2") || DbsUtil.maskMatches(dup_Rqst_Xfr_Stts_Cde,"'M'")))                                                        //Natural: IF DUP-RQST.XFR-STTS-CDE = 'F2' OR DUP-RQST.XFR-STTS-CDE = MASK ( 'M' )
            {
                getReports().write(0, "DUPLICATE OPEN FUTURE OR COMPLETED REQUEST EXISTS",NEWLINE,pnd_Dup_Super_De_04_Pnd_Dup_Frm_Cntrct,pnd_Dup_Super_De_04_Pnd_Dup_Frm_Payee,pnd_Dup_Super_De_04_Pnd_Dup_Effctv_Dte,  //Natural: WRITE 'DUPLICATE OPEN FUTURE OR COMPLETED REQUEST EXISTS' / #DUP-FRM-CNTRCT #DUP-FRM-PAYEE #DUP-EFFCTV-DTE ( EM = YYYYMMDD )
                    new ReportEditMask ("YYYYMMDD"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-FUTURE-REQUEST
                sub_Write_Future_Request();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  DO NOT WRITE FUTURE RQST - DUP
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  022021 - END
                                                                                                                                                                          //Natural: PERFORM WRITE-FUTURE-REQUEST
        sub_Write_Future_Request();
        if (condition(Global.isEscape())) {return;}
        vw_rqst_Future.insertDBRow();                                                                                                                                     //Natural: STORE RQST-FUTURE
        //*   NEXT-YEARS-REQUEST
    }
    private void sub_Pnd_Fill_Multi() throws Exception                                                                                                                    //Natural: #FILL-MULTI
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        rqst_Future_Xfr_Frm_Qty.getValue("*").reset();                                                                                                                    //Natural: RESET RQST-FUTURE.XFR-FRM-QTY ( * )
        pnd_Sub.setValue(iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta);                                                                                                  //Natural: MOVE C*XFR-FRM-ACCT-DTA TO #SUB
        F8:                                                                                                                                                               //Natural: FOR #I = 1 TO #SUB
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Sub)); pnd_I.nadd(1))
        {
            rqst_Future_Xfr_Frm_Qty.getValue(pnd_I).setValue(pnd_Percent);                                                                                                //Natural: ASSIGN RQST-FUTURE.XFR-FRM-QTY ( #I ) := #PERCENT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Future_Request() throws Exception                                                                                                              //Natural: WRITE-FUTURE-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, NEWLINE,"--- FUTURE REQUEST RECORD ---",NEWLINE,NEWLINE,"=",rqst_Future_Rcrd_Type_Cde,NEWLINE,"=",rqst_Future_Rqst_Id,NEWLINE,              //Natural: WRITE / '--- FUTURE REQUEST RECORD ---' // '=' RQST-FUTURE.RCRD-TYPE-CDE / '=' RQST-FUTURE.RQST-ID / '=' RQST-FUTURE.RQST-EFFCTV-DTE / '=' RQST-FUTURE.RQST-ENTRY-DTE / '=' RQST-FUTURE.RQST-ENTRY-TME / '=' RQST-FUTURE.RQST-LST-ACTVTY-DTE / '=' RQST-FUTURE.RQST-RCVD-DTE / '=' RQST-FUTURE.RQST-RCVD-TME / '=' RQST-FUTURE.RQST-RCVD-USER-ID / '=' RQST-FUTURE.RQST-ENTRY-USER-ID / '=' RQST-FUTURE.RQST-CNTCT-MDE / '=' RQST-FUTURE.RQST-SRCE / '=' RQST-FUTURE.RQST-REP-NME / '=' RQST-FUTURE.RQST-STTMNT-IND / '=' RQST-FUTURE.RQST-OPN-CLSD-IND / '=' RQST-FUTURE.RQST-RCPRCL-DTE / '=' RQST-FUTURE.RQST-SSN / '=' RQST-FUTURE.XFR-WORK-PRCSS-ID / '=' RQST-FUTURE.XFR-MIT-LOG-DTE-TME /
            "=",rqst_Future_Rqst_Effctv_Dte,NEWLINE,"=",rqst_Future_Rqst_Entry_Dte,NEWLINE,"=",rqst_Future_Rqst_Entry_Tme,NEWLINE,"=",rqst_Future_Rqst_Lst_Actvty_Dte,
            NEWLINE,"=",rqst_Future_Rqst_Rcvd_Dte,NEWLINE,"=",rqst_Future_Rqst_Rcvd_Tme,NEWLINE,"=",rqst_Future_Rqst_Rcvd_User_Id,NEWLINE,"=",rqst_Future_Rqst_Entry_User_Id,
            NEWLINE,"=",rqst_Future_Rqst_Cntct_Mde,NEWLINE,"=",rqst_Future_Rqst_Srce,NEWLINE,"=",rqst_Future_Rqst_Rep_Nme,NEWLINE,"=",rqst_Future_Rqst_Sttmnt_Ind,
            NEWLINE,"=",rqst_Future_Rqst_Opn_Clsd_Ind,NEWLINE,"=",rqst_Future_Rqst_Rcprcl_Dte,NEWLINE,"=",rqst_Future_Rqst_Ssn,NEWLINE,"=",rqst_Future_Xfr_Work_Prcss_Id,
            NEWLINE,"=",rqst_Future_Xfr_Mit_Log_Dte_Tme,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(0, "=",rqst_Future_Xfr_Stts_Cde,NEWLINE,"=",rqst_Future_Xfr_Rjctn_Cde,NEWLINE,"=",rqst_Future_Xfr_In_Progress_Ind,NEWLINE,"=",                 //Natural: WRITE '=' RQST-FUTURE.XFR-STTS-CDE / '=' RQST-FUTURE.XFR-RJCTN-CDE / '=' RQST-FUTURE.XFR-IN-PROGRESS-IND / '=' RQST-FUTURE.XFR-RETRY-CNT / '=' RQST-FUTURE.IA-FRM-CNTRCT / '=' RQST-FUTURE.IA-FRM-PAYEE / '=' RQST-FUTURE.IA-UNIQUE-ID / '=' RQST-FUTURE.IA-HOLD-CDE / '=' RQST-FUTURE.C*XFR-FRM-ACCT-DTA / '=' RQST-FUTURE.XFR-FRM-ACCT-CDE ( 1:08 ) / '=' RQST-FUTURE.XFR-FRM-UNIT-TYP ( 1:8 ) / '=' RQST-FUTURE.XFR-FRM-TYP ( 1:8 ) / '=' RQST-FUTURE.XFR-FRM-QTY ( 1:8 ) / '=' RQST-FUTURE.XFR-FRM-EST-AMT ( 1:8 ) / '=' RQST-FUTURE.XFR-FRM-ASSET-AMT ( 1:8 ) / '=' RQST-FUTURE.C*XFR-TO-ACCT-DTA /
            rqst_Future_Xfr_Retry_Cnt,NEWLINE,"=",rqst_Future_Ia_Frm_Cntrct,NEWLINE,"=",rqst_Future_Ia_Frm_Payee,NEWLINE,"=",rqst_Future_Ia_Unique_Id,NEWLINE,
            "=",rqst_Future_Ia_Hold_Cde,NEWLINE,"=",rqst_Future_Count_Castxfr_Frm_Acct_Dta,NEWLINE,"=",rqst_Future_Xfr_Frm_Acct_Cde.getValue(1,":",8),NEWLINE,
            "=",rqst_Future_Xfr_Frm_Unit_Typ.getValue(1,":",8),NEWLINE,"=",rqst_Future_Xfr_Frm_Typ.getValue(1,":",8),NEWLINE,"=",rqst_Future_Xfr_Frm_Qty.getValue(1,
            ":",8),NEWLINE,"=",rqst_Future_Xfr_Frm_Est_Amt.getValue(1,":",8),NEWLINE,"=",rqst_Future_Xfr_Frm_Asset_Amt.getValue(1,":",8),NEWLINE,"=",rqst_Future_Count_Castxfr_To_Acct_Dta,
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(0, "=",rqst_Future_Xfr_To_Acct_Cde.getValue(1,":",8),NEWLINE,"=",rqst_Future_Xfr_To_Unit_Typ.getValue(1,":",8),NEWLINE,"=",                    //Natural: WRITE '=' RQST-FUTURE.XFR-TO-ACCT-CDE ( 1:8 ) / '=' RQST-FUTURE.XFR-TO-UNIT-TYP ( 1:8 ) / '=' RQST-FUTURE.XFR-TO-TYP ( 1:8 ) / '=' RQST-FUTURE.XFR-TO-QTY ( 1:8 ) / '=' RQST-FUTURE.XFR-TO-EST-AMT ( 1:8 ) / '=' RQST-FUTURE.XFR-TO-ASSET-AMT ( 1:8 ) / '=' RQST-FUTURE.IA-TO-CNTRCT / '=' RQST-FUTURE.IA-TO-PAYEE / '=' RQST-FUTURE.IA-APPL-RCVD-DTE / '=' RQST-FUTURE.IA-NEW-ISSUE / '=' RQST-FUTURE.IA-RSN-FOR-OVRRDE / '=' RQST-FUTURE.IA-OVRRDE-USER-ID / '=' RQST-FUTURE.IA-GNRL-PEND-CDE / '=' RQST-FUTURE.RQST-XFR-TYPE / '=' RQST-FUTURE.RQST-UNIT-CDE / '=' RQST-FUTURE.IA-NEW-ISS-PRT-PULL /
            rqst_Future_Xfr_To_Typ.getValue(1,":",8),NEWLINE,"=",rqst_Future_Xfr_To_Qty.getValue(1,":",8),NEWLINE,"=",rqst_Future_Xfr_To_Est_Amt.getValue(1,
            ":",8),NEWLINE,"=",rqst_Future_Xfr_To_Asset_Amt.getValue(1,":",8),NEWLINE,"=",rqst_Future_Ia_To_Cntrct,NEWLINE,"=",rqst_Future_Ia_To_Payee,NEWLINE,
            "=",rqst_Future_Ia_Appl_Rcvd_Dte,NEWLINE,"=",rqst_Future_Ia_New_Issue,NEWLINE,"=",rqst_Future_Ia_Rsn_For_Ovrrde,NEWLINE,"=",rqst_Future_Ia_Ovrrde_User_Id,
            NEWLINE,"=",rqst_Future_Ia_Gnrl_Pend_Cde,NEWLINE,"=",rqst_Future_Rqst_Xfr_Type,NEWLINE,"=",rqst_Future_Rqst_Unit_Cde,NEWLINE,"=",rqst_Future_Ia_New_Iss_Prt_Pull,
            NEWLINE);
        if (Global.isEscape()) return;
        //*   #WRITE-NEXT-YEARS-REQUEST
    }

    //
}
