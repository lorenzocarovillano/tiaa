/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:40:48 AM
**        * FROM NATURAL SUBPROGRAM : Iaan760
************************************************************
**        * FILE NAME            : Iaan760.java
**        * CLASS NAME           : Iaan760
**        * INSTANCE NAME        : Iaan760
************************************************************
************************************************************************
* PROGRAM  : IAAN760
* SYSTEM   : IA
* TITLE    : GET SHORT NAMES USING TICKER SYMBOL
* CREATED  : JUNE 30,2017
* FUNCTION : GET SHORT NAME OF A TICKER SYMBOL PASSED FROM CALLING
*          : PROGRAM.
*
************************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaan760 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_I_Tkr;
    private DbsField pnd_O_Shrt_Nme;
    private DbsField pnd_Rc;
    private DbsField pnd_Rc_Msg;

    private DbsGroup pnd_Tkr_Shrt_Nme;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd1;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme1;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd2;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme2;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd3;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme3;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd4;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme4;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd5;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme5;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd6;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme6;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd7;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme7;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd8;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme8;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd9;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme9;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd10;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme10;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd11;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme11;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd12;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme12;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd13;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme13;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd14;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme14;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd15;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme15;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd16;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme16;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd17;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme17;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd18;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme18;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd19;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme19;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd20;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme20;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd21;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme21;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd22;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme22;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd23;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme23;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd24;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme24;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd25;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme25;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd26;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme26;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd27;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme27;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd28;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme28;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd29;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme29;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd30;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme30;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd31;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme31;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd32;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme32;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd33;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme33;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd34;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme34;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Fnd35;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Nme35;

    private DbsGroup pnd_Tkr_Shrt_Nme__R_Field_1;

    private DbsGroup pnd_Tkr_Shrt_Nme_Pnd_Fnd_Nme_Tbl;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Ticker;
    private DbsField pnd_Tkr_Shrt_Nme_Pnd_Shortname;
    private DbsField pnd_A;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_I_Tkr = parameters.newFieldInRecord("pnd_I_Tkr", "#I-TKR", FieldType.STRING, 10);
        pnd_I_Tkr.setParameterOption(ParameterOption.ByReference);
        pnd_O_Shrt_Nme = parameters.newFieldInRecord("pnd_O_Shrt_Nme", "#O-SHRT-NME", FieldType.STRING, 5);
        pnd_O_Shrt_Nme.setParameterOption(ParameterOption.ByReference);
        pnd_Rc = parameters.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Rc.setParameterOption(ParameterOption.ByReference);
        pnd_Rc_Msg = parameters.newFieldInRecord("pnd_Rc_Msg", "#RC-MSG", FieldType.STRING, 79);
        pnd_Rc_Msg.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Tkr_Shrt_Nme = localVariables.newGroupInRecord("pnd_Tkr_Shrt_Nme", "#TKR-SHRT-NME");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd1 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd1", "#FND1", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme1 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme1", "#NME1", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd2 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd2", "#FND2", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme2 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme2", "#NME2", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd3 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd3", "#FND3", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme3 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme3", "#NME3", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd4 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd4", "#FND4", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme4 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme4", "#NME4", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd5 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd5", "#FND5", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme5 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme5", "#NME5", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd6 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd6", "#FND6", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme6 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme6", "#NME6", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd7 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd7", "#FND7", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme7 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme7", "#NME7", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd8 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd8", "#FND8", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme8 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme8", "#NME8", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd9 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd9", "#FND9", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme9 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme9", "#NME9", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd10 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd10", "#FND10", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme10 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme10", "#NME10", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd11 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd11", "#FND11", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme11 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme11", "#NME11", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd12 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd12", "#FND12", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme12 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme12", "#NME12", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd13 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd13", "#FND13", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme13 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme13", "#NME13", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd14 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd14", "#FND14", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme14 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme14", "#NME14", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd15 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd15", "#FND15", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme15 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme15", "#NME15", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd16 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd16", "#FND16", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme16 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme16", "#NME16", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd17 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd17", "#FND17", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme17 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme17", "#NME17", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd18 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd18", "#FND18", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme18 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme18", "#NME18", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd19 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd19", "#FND19", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme19 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme19", "#NME19", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd20 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd20", "#FND20", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme20 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme20", "#NME20", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd21 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd21", "#FND21", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme21 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme21", "#NME21", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd22 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd22", "#FND22", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme22 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme22", "#NME22", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd23 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd23", "#FND23", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme23 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme23", "#NME23", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd24 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd24", "#FND24", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme24 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme24", "#NME24", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd25 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd25", "#FND25", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme25 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme25", "#NME25", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd26 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd26", "#FND26", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme26 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme26", "#NME26", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd27 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd27", "#FND27", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme27 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme27", "#NME27", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd28 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd28", "#FND28", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme28 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme28", "#NME28", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd29 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd29", "#FND29", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme29 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme29", "#NME29", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd30 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd30", "#FND30", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme30 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme30", "#NME30", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd31 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd31", "#FND31", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme31 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme31", "#NME31", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd32 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd32", "#FND32", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme32 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme32", "#NME32", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd33 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd33", "#FND33", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme33 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme33", "#NME33", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd34 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd34", "#FND34", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme34 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme34", "#NME34", FieldType.STRING, 5);
        pnd_Tkr_Shrt_Nme_Pnd_Fnd35 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd35", "#FND35", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Nme35 = pnd_Tkr_Shrt_Nme.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Nme35", "#NME35", FieldType.STRING, 5);

        pnd_Tkr_Shrt_Nme__R_Field_1 = localVariables.newGroupInRecord("pnd_Tkr_Shrt_Nme__R_Field_1", "REDEFINE", pnd_Tkr_Shrt_Nme);

        pnd_Tkr_Shrt_Nme_Pnd_Fnd_Nme_Tbl = pnd_Tkr_Shrt_Nme__R_Field_1.newGroupArrayInGroup("pnd_Tkr_Shrt_Nme_Pnd_Fnd_Nme_Tbl", "#FND-NME-TBL", new DbsArrayController(1, 
            35));
        pnd_Tkr_Shrt_Nme_Pnd_Ticker = pnd_Tkr_Shrt_Nme_Pnd_Fnd_Nme_Tbl.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Ticker", "#TICKER", FieldType.STRING, 10);
        pnd_Tkr_Shrt_Nme_Pnd_Shortname = pnd_Tkr_Shrt_Nme_Pnd_Fnd_Nme_Tbl.newFieldInGroup("pnd_Tkr_Shrt_Nme_Pnd_Shortname", "#SHORTNAME", FieldType.STRING, 
            5);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Tkr_Shrt_Nme_Pnd_Fnd1.setInitialValue("CBND#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme1.setInitialValue("CBN3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd2.setInitialValue("CEQX#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme2.setInitialValue("CEQ3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd3.setInitialValue("CGLB#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme3.setInitialValue("CGL3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd4.setInitialValue("CGRW#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme4.setInitialValue("CGR3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd5.setInitialValue("CILB#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme5.setInitialValue("CIL3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd6.setInitialValue("CMMA#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme6.setInitialValue("CMM3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd7.setInitialValue("CSCL#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme7.setInitialValue("CSC3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd8.setInitialValue("CSTK#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme8.setInitialValue("CST3#");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd9.setInitialValue("QCBMIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme9.setInitialValue("CBNR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd10.setInitialValue("QCBMPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme10.setInitialValue("CBNR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd11.setInitialValue("QCBMRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme11.setInitialValue("CBND");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd12.setInitialValue("QCEQIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme12.setInitialValue("CEQR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd13.setInitialValue("QCEQPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme13.setInitialValue("CEQR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd14.setInitialValue("QCEQRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme14.setInitialValue("CEQX");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd15.setInitialValue("QCGLIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme15.setInitialValue("CGLR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd16.setInitialValue("QCGLPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme16.setInitialValue("CGLR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd17.setInitialValue("QCGLRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme17.setInitialValue("CGLB");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd18.setInitialValue("QCGRIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme18.setInitialValue("CGRR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd19.setInitialValue("QCGRPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme19.setInitialValue("CGRR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd20.setInitialValue("QCGRRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme20.setInitialValue("CGRW");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd21.setInitialValue("QCILIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme21.setInitialValue("CILR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd22.setInitialValue("QCILPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme22.setInitialValue("CILR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd23.setInitialValue("QCILRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme23.setInitialValue("CILB");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd24.setInitialValue("QCMMIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme24.setInitialValue("CMMR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd25.setInitialValue("QCMMPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme25.setInitialValue("CMMR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd26.setInitialValue("QCMMRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme26.setInitialValue("CMMA");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd27.setInitialValue("QCSCIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme27.setInitialValue("CSCR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd28.setInitialValue("QCSCPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme28.setInitialValue("CSCR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd29.setInitialValue("QCSCRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme29.setInitialValue("CSCL");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd30.setInitialValue("QCSTIX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme30.setInitialValue("CSTR3");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd31.setInitialValue("QCSTPX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme31.setInitialValue("CSTR2");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd32.setInitialValue("QCSTRX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme32.setInitialValue("CSTK");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd33.setInitialValue("QREARX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme33.setInitialValue("TREA");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd34.setInitialValue("QREARX");
        pnd_Tkr_Shrt_Nme_Pnd_Nme34.setInitialValue("TREX");
        pnd_Tkr_Shrt_Nme_Pnd_Fnd35.setInitialValue("WA51#");
        pnd_Tkr_Shrt_Nme_Pnd_Nme35.setInitialValue("WA51#");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iaan760() throws Exception
    {
        super("Iaan760");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Rc.reset();                                                                                                                                                   //Natural: RESET #RC #RC-MSG #O-SHRT-NME
        pnd_Rc_Msg.reset();
        pnd_O_Shrt_Nme.reset();
        if (condition(pnd_I_Tkr.equals(pnd_Tkr_Shrt_Nme_Pnd_Ticker.getValue("*"))))                                                                                       //Natural: IF #I-TKR = #TICKER ( * )
        {
            DbsUtil.examine(new ExamineSource(pnd_Tkr_Shrt_Nme_Pnd_Ticker.getValue("*")), new ExamineSearch(pnd_I_Tkr), new ExamineGivingIndex(pnd_A));                   //Natural: EXAMINE #TICKER ( * ) FOR #I-TKR GIVING INDEX #A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_A.equals(getZero())))                                                                                                                           //Natural: IF #A = 0
        {
            pnd_Rc.setValue(99);                                                                                                                                          //Natural: ASSIGN #RC := 99
            pnd_Rc_Msg.setValue(DbsUtil.compress("TICKER", pnd_I_Tkr, "NOT FOUND"));                                                                                      //Natural: COMPRESS 'TICKER' #I-TKR 'NOT FOUND' INTO #RC-MSG
            getReports().write(0, pnd_Rc_Msg);                                                                                                                            //Natural: WRITE #RC-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_O_Shrt_Nme.setValue(pnd_Tkr_Shrt_Nme_Pnd_Shortname.getValue(pnd_A));                                                                                      //Natural: ASSIGN #O-SHRT-NME := #SHORTNAME ( #A )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
