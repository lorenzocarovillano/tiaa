/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:45:19 AM
**        * FROM NATURAL SUBPROGRAM : Iatn420c
************************************************************
**        * FILE NAME            : Iatn420c.java
**        * CLASS NAME           : Iatn420c
**        * INSTANCE NAME        : Iatn420c
************************************************************
************************************************************************
* PROGRAM : IATN420C
* FUNCTION: CREATE TRANSACTION RECORD FOR IA TRANSFERS
* DATE    : FEB 13, 1996
*
* HISTORY : 6/99 KN ADDED TRAANS CODE 512-518
* 04/2017 OS RE-STOWED FOR PIN EXPANSION.
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatn420c extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Iaa_From_Cntrct;
    private DbsField pnd_Iaa_From_Pyee;
    private DbsField pnd_Iaa_To_Cntrct;
    private DbsField pnd_Iaa_To_Pyee;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Ivc_Ind;
    private DbsField pnd_New_Issue;
    private DbsField pnd_Check_Dte;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Effective_Dte;
    private DbsField pnd_Rcrd_Type_Cde;
    private DbsField pnd_Rqst_Xfr_Type;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;

    private DbsGroup iaa_Trans_Rcrd__R_Field_1;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte_A;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;

    private DbsGroup iaa_Trans_Rcrd__R_Field_2;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte_A;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Iaa_From_Cntrct = parameters.newFieldInRecord("pnd_Iaa_From_Cntrct", "#IAA-FROM-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_From_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_From_Pyee = parameters.newFieldInRecord("pnd_Iaa_From_Pyee", "#IAA-FROM-PYEE", FieldType.STRING, 2);
        pnd_Iaa_From_Pyee.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_To_Cntrct = parameters.newFieldInRecord("pnd_Iaa_To_Cntrct", "#IAA-TO-CNTRCT", FieldType.STRING, 10);
        pnd_Iaa_To_Cntrct.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_To_Pyee = parameters.newFieldInRecord("pnd_Iaa_To_Pyee", "#IAA-TO-PYEE", FieldType.STRING, 2);
        pnd_Iaa_To_Pyee.setParameterOption(ParameterOption.ByReference);
        pnd_Trans_Dte = parameters.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Trans_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Ivc_Ind = parameters.newFieldInRecord("pnd_Ivc_Ind", "#IVC-IND", FieldType.STRING, 1);
        pnd_Ivc_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_New_Issue = parameters.newFieldInRecord("pnd_New_Issue", "#NEW-ISSUE", FieldType.STRING, 1);
        pnd_New_Issue.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Dte = parameters.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.DATE);
        pnd_Check_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Todays_Dte = parameters.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.DATE);
        pnd_Todays_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Effective_Dte = parameters.newFieldInRecord("pnd_Effective_Dte", "#EFFECTIVE-DTE", FieldType.DATE);
        pnd_Effective_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Rcrd_Type_Cde = parameters.newFieldInRecord("pnd_Rcrd_Type_Cde", "#RCRD-TYPE-CDE", FieldType.STRING, 1);
        pnd_Rcrd_Type_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Rqst_Xfr_Type = parameters.newFieldInRecord("pnd_Rqst_Xfr_Type", "#RQST-XFR-TYPE", FieldType.STRING, 1);
        pnd_Rqst_Xfr_Type.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");

        iaa_Trans_Rcrd__R_Field_1 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_1", "REDEFINE", iaa_Trans_Rcrd_Trans_Check_Dte);
        iaa_Trans_Rcrd_Trans_Check_Dte_A = iaa_Trans_Rcrd__R_Field_1.newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte_A", "TRANS-CHECK-DTE-A", FieldType.STRING, 
            8);
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");

        iaa_Trans_Rcrd__R_Field_2 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_2", "REDEFINE", iaa_Trans_Rcrd_Trans_Todays_Dte);
        iaa_Trans_Rcrd_Trans_Todays_Dte_A = iaa_Trans_Rcrd__R_Field_2.newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte_A", "TRANS-TODAYS-DTE-A", FieldType.STRING, 
            8);
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trans_Rcrd.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Iatn420c() throws Exception
    {
        super("Iatn420c");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        iaa_Trans_Rcrd_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE
        iaa_Trans_Rcrd_Invrse_Trans_Dte.compute(new ComputeParameters(false, iaa_Trans_Rcrd_Invrse_Trans_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));  //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := 1000000000000 - #TRANS-DTE
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr.setValue(pnd_Iaa_From_Cntrct);                                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #IAA-FROM-CNTRCT
        iaa_Trans_Rcrd_Trans_Payee_Cde.compute(new ComputeParameters(false, iaa_Trans_Rcrd_Trans_Payee_Cde), pnd_Iaa_From_Pyee.val());                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := VAL ( #IAA-FROM-PYEE )
        iaa_Trans_Rcrd_Trans_Actvty_Cde.setValue("A");                                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        iaa_Trans_Rcrd_Trans_Check_Dte_A.setValueEdited(pnd_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED #CHECK-DTE ( EM = YYYYMMDD ) TO IAA-TRANS-RCRD.TRANS-CHECK-DTE-A
        iaa_Trans_Rcrd_Trans_Todays_Dte_A.setValueEdited(pnd_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED #TODAYS-DTE ( EM = YYYYMMDD ) TO IAA-TRANS-RCRD.TRANS-TODAYS-DTE-A
        iaa_Trans_Rcrd_Trans_User_Id.setValue("XFR SYS");                                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'XFR SYS'
        iaa_Trans_Rcrd_Trans_Sub_Cde.setValue(" ");                                                                                                                       //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := ' '
        iaa_Trans_Rcrd_Trans_User_Area.setValue(" ");                                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := ' '
        iaa_Trans_Rcrd_Trans_Verify_Cde.setValue(" ");                                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-CDE := ' '
        iaa_Trans_Rcrd_Trans_Verify_Id.setValue(" ");                                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-ID := ' '
        iaa_Trans_Rcrd_Trans_Cwf_Wpid.setValue(" ");                                                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-WPID := ' '
        iaa_Trans_Rcrd_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := #TRANS-DTE
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr.setValue(0);                                                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-ID-NBR := 0
        iaa_Trans_Rcrd_Trans_Effective_Dte.setValue(pnd_Effective_Dte);                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE := #EFFECTIVE-DTE
        if (condition(pnd_Iaa_To_Cntrct.equals(" ")))                                                                                                                     //Natural: IF #IAA-TO-CNTRCT = ' '
        {
            if (condition(pnd_Rcrd_Type_Cde.equals("1")))                                                                                                                 //Natural: IF #RCRD-TYPE-CDE = '1'
            {
                iaa_Trans_Rcrd_Trans_Cde.setValue(500);                                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 500
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                iaa_Trans_Rcrd_Trans_Cde.setValue(520);                                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 520
            }                                                                                                                                                             //Natural: END-IF
            vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                              //Natural: STORE IAA-TRANS-RCRD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ivc_Ind.equals("Y")))                                                                                                                       //Natural: IF #IVC-IND = 'Y'
            {
                //*  6/99 KN
                if (condition(pnd_Rqst_Xfr_Type.equals(" ")))                                                                                                             //Natural: IF #RQST-XFR-TYPE = ' '
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(504);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 504
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(514);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 514
                }                                                                                                                                                         //Natural: END-IF
                iaa_Trans_Rcrd_Trans_Cmbne_Cde.setValue(pnd_Iaa_To_Cntrct);                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #IAA-TO-CNTRCT
                setValueToSubstring(pnd_Iaa_To_Pyee,iaa_Trans_Rcrd_Trans_Cmbne_Cde,11,2);                                                                                 //Natural: MOVE #IAA-TO-PYEE TO SUBSTR ( IAA-TRANS-RCRD.TRANS-CMBNE-CDE,11,2 )
                vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                          //Natural: STORE IAA-TRANS-RCRD
                iaa_Trans_Rcrd_Trans_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #IAA-TO-CNTRCT
                iaa_Trans_Rcrd_Trans_Payee_Cde.compute(new ComputeParameters(false, iaa_Trans_Rcrd_Trans_Payee_Cde), pnd_Iaa_To_Pyee.val());                              //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := VAL ( #IAA-TO-PYEE )
                //*  6/99 KN
                if (condition(pnd_Rqst_Xfr_Type.equals(" ")))                                                                                                             //Natural: IF #RQST-XFR-TYPE = ' '
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(508);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 508
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(518);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 518
                }                                                                                                                                                         //Natural: END-IF
                iaa_Trans_Rcrd_Trans_Cmbne_Cde.setValue(pnd_Iaa_From_Cntrct);                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #IAA-FROM-CNTRCT
                setValueToSubstring(pnd_Iaa_From_Pyee,iaa_Trans_Rcrd_Trans_Cmbne_Cde,11,2);                                                                               //Natural: MOVE #IAA-FROM-PYEE TO SUBSTR ( IAA-TRANS-RCRD.TRANS-CMBNE-CDE,11,2 )
                if (condition(pnd_New_Issue.equals("Y")))                                                                                                                 //Natural: IF #NEW-ISSUE = 'Y'
                {
                    iaa_Trans_Rcrd_Trans_Sub_Cde.setValue("033");                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := '033'
                }                                                                                                                                                         //Natural: END-IF
                vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                          //Natural: STORE IAA-TRANS-RCRD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  6/99 KN
                if (condition(pnd_Rqst_Xfr_Type.equals(" ")))                                                                                                             //Natural: IF #RQST-XFR-TYPE = ' '
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(502);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 502
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(512);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 512
                }                                                                                                                                                         //Natural: END-IF
                iaa_Trans_Rcrd_Trans_Cmbne_Cde.setValue(pnd_Iaa_To_Cntrct);                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #IAA-TO-CNTRCT
                setValueToSubstring(pnd_Iaa_To_Pyee,iaa_Trans_Rcrd_Trans_Cmbne_Cde,11,2);                                                                                 //Natural: MOVE #IAA-TO-PYEE TO SUBSTR ( IAA-TRANS-RCRD.TRANS-CMBNE-CDE,11,2 )
                vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                          //Natural: STORE IAA-TRANS-RCRD
                iaa_Trans_Rcrd_Trans_Ppcn_Nbr.setValue(pnd_Iaa_To_Cntrct);                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #IAA-TO-CNTRCT
                iaa_Trans_Rcrd_Trans_Payee_Cde.compute(new ComputeParameters(false, iaa_Trans_Rcrd_Trans_Payee_Cde), pnd_Iaa_To_Pyee.val());                              //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := VAL ( #IAA-TO-PYEE )
                //*  6/99 KN
                if (condition(pnd_Rqst_Xfr_Type.equals(" ")))                                                                                                             //Natural: IF #RQST-XFR-TYPE = ' '
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(506);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 506
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Trans_Rcrd_Trans_Cde.setValue(516);                                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 516
                }                                                                                                                                                         //Natural: END-IF
                iaa_Trans_Rcrd_Trans_Cmbne_Cde.setValue(pnd_Iaa_From_Cntrct);                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #IAA-FROM-CNTRCT
                setValueToSubstring(pnd_Iaa_From_Pyee,iaa_Trans_Rcrd_Trans_Cmbne_Cde,11,2);                                                                               //Natural: MOVE #IAA-FROM-PYEE TO SUBSTR ( IAA-TRANS-RCRD.TRANS-CMBNE-CDE,11,2 )
                if (condition(pnd_New_Issue.equals("Y")))                                                                                                                 //Natural: IF #NEW-ISSUE = 'Y'
                {
                    iaa_Trans_Rcrd_Trans_Sub_Cde.setValue("033");                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := '033'
                }                                                                                                                                                         //Natural: END-IF
                vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                          //Natural: STORE IAA-TRANS-RCRD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
