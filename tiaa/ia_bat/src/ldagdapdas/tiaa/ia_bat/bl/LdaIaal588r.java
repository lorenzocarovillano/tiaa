/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:22 PM
**        * FROM NATURAL LDA     : IAAL588R
************************************************************
**        * FILE NAME            : LdaIaal588r.java
**        * CLASS NAME           : LdaIaal588r
**        * INSTANCE NAME        : LdaIaal588r
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal588r extends DbsRecord
{
    // Properties
    private DbsGroup iaal588r;
    private DbsGroup iaal588r_Msg_Info_Sub;
    private DbsField iaal588r_Pnd_Pnd_Msg;
    private DbsField iaal588r_Pnd_Pnd_Msg_Nr;
    private DbsField iaal588r_Pnd_Pnd_Msg_Data;
    private DbsGroup iaal588r_Pnd_Pnd_Msg_DataRedef1;
    private DbsGroup iaal588r_Pnd_Pnd_Msg_Data_Struct;
    private DbsField iaal588r_Pnd_Pnd_Msg_Data_Char;
    private DbsField iaal588r_Pnd_Pnd_Return_Code;
    private DbsField iaal588r_Pnd_Pnd_Error_Field;
    private DbsField iaal588r_Pnd_Pnd_Error_Field_Index1;
    private DbsField iaal588r_Pnd_Pnd_Error_Field_Index2;
    private DbsField iaal588r_Pnd_Pnd_Error_Field_Index3;
    private DbsField iaal588r_Pnd_Parm_Cntrct;
    private DbsField iaal588r_Pnd_Parm_Payee;
    private DbsField iaal588r_Pnd_Parm_Fund_File;
    private DbsField iaal588r_Pnd_Parm_Fund_Inverse_Date;
    private DbsField iaal588r_Pnd_Parm_Fund;
    private DbsField iaal588r_Pnd_Parm_Com_Val_Amt;
    private DbsField iaal588r_Pnd_Rqst_Dci_Amt;
    private DbsGroup pnd_Iaan425b;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Rqst_Est_Amt;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Amt;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Dci;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date;
    private DbsGroup pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_DateRedef2;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date_N;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Tab_Per_Amt;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Tab_Div_Amt;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Tab_Cmpny_Cde;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Date_Of_Death;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Number_Of_Payments;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Method_1_Or_2;
    private DbsField pnd_Iaan425b_Pnd_Iaan425b_Rqst_Dci_Amount;
    private DbsField pnd_Audit_Reads;
    private DbsField pnd_Audit_Time_Selects;
    private DbsField pnd_Payment_Records;
    private DbsField pnd_Diff_Accounting_Date;
    private DbsField pnd_M;
    private DbsField pnd_N;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Num;
    private DbsField pnd_W_Title_Variable1;
    private DbsField pnd_W_Title_Variable2;
    private DbsField pnd_Account_Error;
    private DbsField pnd_Eft_Hold;
    private DbsField pnd_Eft;
    private DbsField pnd_Check;
    private DbsField pnd_Tab_Total_Num_Of_Pay;
    private DbsField pnd_Tab_Total_Gross_Amt;
    private DbsField pnd_Tab_Cref_Gross;
    private DbsField pnd_Grand_Total_Num_Of_Pay;
    private DbsField pnd_Grand_Total_Gross_Amt;
    private DbsField pnd_Grand_Total_Tiaa_Gross_Amt;
    private DbsField pnd_Grand_Total_Cref_Gross_Amt;
    private DbsField pnd_Grand_Total_Real_Est_Gross_Amt;
    private DbsField pnd_Title_Variable1;
    private DbsField pnd_Title_Variable2;
    private DbsField pnd_Tab_Divd;
    private DbsField pnd_Tab_Guar;
    private DbsField pnd_Tab_Gross;
    private DbsField pnd_Tab_Gross_Eft_Hold;
    private DbsField pnd_Tab_Gross_Eft;
    private DbsField pnd_Tab_Gross_Chk;
    private DbsField pnd_Tab_Dci;
    private DbsField pnd_Tab_Ovrpy;
    private DbsField pnd_Tab_Num_Paym;
    private DbsField pnd_Tab_Num_Eft;
    private DbsField pnd_Tab_Num_Eft_Hold;
    private DbsField pnd_Tab_Num_Check;
    private DbsField pnd_Tab_Header;
    private DbsField pnd_Date_Ccyymmdd;
    private DbsGroup pnd_Date_CcyymmddRedef3;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Cc;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Dd;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef4;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Date_NRedef5;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_AlphRedef6;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd;
    private DbsField pnd_Fl_Time_Hhiiss_Alph;
    private DbsGroup pnd_Fl_Time_Hhiiss_AlphRedef8;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num;
    private DbsGroup pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef10;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef12;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsGroup pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;

    public DbsGroup getIaal588r() { return iaal588r; }

    public DbsGroup getIaal588r_Msg_Info_Sub() { return iaal588r_Msg_Info_Sub; }

    public DbsField getIaal588r_Pnd_Pnd_Msg() { return iaal588r_Pnd_Pnd_Msg; }

    public DbsField getIaal588r_Pnd_Pnd_Msg_Nr() { return iaal588r_Pnd_Pnd_Msg_Nr; }

    public DbsField getIaal588r_Pnd_Pnd_Msg_Data() { return iaal588r_Pnd_Pnd_Msg_Data; }

    public DbsGroup getIaal588r_Pnd_Pnd_Msg_DataRedef1() { return iaal588r_Pnd_Pnd_Msg_DataRedef1; }

    public DbsGroup getIaal588r_Pnd_Pnd_Msg_Data_Struct() { return iaal588r_Pnd_Pnd_Msg_Data_Struct; }

    public DbsField getIaal588r_Pnd_Pnd_Msg_Data_Char() { return iaal588r_Pnd_Pnd_Msg_Data_Char; }

    public DbsField getIaal588r_Pnd_Pnd_Return_Code() { return iaal588r_Pnd_Pnd_Return_Code; }

    public DbsField getIaal588r_Pnd_Pnd_Error_Field() { return iaal588r_Pnd_Pnd_Error_Field; }

    public DbsField getIaal588r_Pnd_Pnd_Error_Field_Index1() { return iaal588r_Pnd_Pnd_Error_Field_Index1; }

    public DbsField getIaal588r_Pnd_Pnd_Error_Field_Index2() { return iaal588r_Pnd_Pnd_Error_Field_Index2; }

    public DbsField getIaal588r_Pnd_Pnd_Error_Field_Index3() { return iaal588r_Pnd_Pnd_Error_Field_Index3; }

    public DbsField getIaal588r_Pnd_Parm_Cntrct() { return iaal588r_Pnd_Parm_Cntrct; }

    public DbsField getIaal588r_Pnd_Parm_Payee() { return iaal588r_Pnd_Parm_Payee; }

    public DbsField getIaal588r_Pnd_Parm_Fund_File() { return iaal588r_Pnd_Parm_Fund_File; }

    public DbsField getIaal588r_Pnd_Parm_Fund_Inverse_Date() { return iaal588r_Pnd_Parm_Fund_Inverse_Date; }

    public DbsField getIaal588r_Pnd_Parm_Fund() { return iaal588r_Pnd_Parm_Fund; }

    public DbsField getIaal588r_Pnd_Parm_Com_Val_Amt() { return iaal588r_Pnd_Parm_Com_Val_Amt; }

    public DbsField getIaal588r_Pnd_Rqst_Dci_Amt() { return iaal588r_Pnd_Rqst_Dci_Amt; }

    public DbsGroup getPnd_Iaan425b() { return pnd_Iaan425b; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Rqst_Est_Amt() { return pnd_Iaan425b_Pnd_Iaan425b_Rqst_Est_Amt; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Com_Val_Amt() { return pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Amt; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Com_Val_Dci() { return pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Dci; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date() { return pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date; }

    public DbsGroup getPnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_DateRedef2() { return pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_DateRedef2; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date_N() { return pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date_N; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Tab_Per_Amt() { return pnd_Iaan425b_Pnd_Iaan425b_Tab_Per_Amt; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Tab_Div_Amt() { return pnd_Iaan425b_Pnd_Iaan425b_Tab_Div_Amt; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Tab_Cmpny_Cde() { return pnd_Iaan425b_Pnd_Iaan425b_Tab_Cmpny_Cde; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Date_Of_Death() { return pnd_Iaan425b_Pnd_Iaan425b_Date_Of_Death; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Number_Of_Payments() { return pnd_Iaan425b_Pnd_Iaan425b_Number_Of_Payments; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Method_1_Or_2() { return pnd_Iaan425b_Pnd_Iaan425b_Method_1_Or_2; }

    public DbsField getPnd_Iaan425b_Pnd_Iaan425b_Rqst_Dci_Amount() { return pnd_Iaan425b_Pnd_Iaan425b_Rqst_Dci_Amount; }

    public DbsField getPnd_Audit_Reads() { return pnd_Audit_Reads; }

    public DbsField getPnd_Audit_Time_Selects() { return pnd_Audit_Time_Selects; }

    public DbsField getPnd_Payment_Records() { return pnd_Payment_Records; }

    public DbsField getPnd_Diff_Accounting_Date() { return pnd_Diff_Accounting_Date; }

    public DbsField getPnd_M() { return pnd_M; }

    public DbsField getPnd_N() { return pnd_N; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_J() { return pnd_J; }

    public DbsField getPnd_Num() { return pnd_Num; }

    public DbsField getPnd_W_Title_Variable1() { return pnd_W_Title_Variable1; }

    public DbsField getPnd_W_Title_Variable2() { return pnd_W_Title_Variable2; }

    public DbsField getPnd_Account_Error() { return pnd_Account_Error; }

    public DbsField getPnd_Eft_Hold() { return pnd_Eft_Hold; }

    public DbsField getPnd_Eft() { return pnd_Eft; }

    public DbsField getPnd_Check() { return pnd_Check; }

    public DbsField getPnd_Tab_Total_Num_Of_Pay() { return pnd_Tab_Total_Num_Of_Pay; }

    public DbsField getPnd_Tab_Total_Gross_Amt() { return pnd_Tab_Total_Gross_Amt; }

    public DbsField getPnd_Tab_Cref_Gross() { return pnd_Tab_Cref_Gross; }

    public DbsField getPnd_Grand_Total_Num_Of_Pay() { return pnd_Grand_Total_Num_Of_Pay; }

    public DbsField getPnd_Grand_Total_Gross_Amt() { return pnd_Grand_Total_Gross_Amt; }

    public DbsField getPnd_Grand_Total_Tiaa_Gross_Amt() { return pnd_Grand_Total_Tiaa_Gross_Amt; }

    public DbsField getPnd_Grand_Total_Cref_Gross_Amt() { return pnd_Grand_Total_Cref_Gross_Amt; }

    public DbsField getPnd_Grand_Total_Real_Est_Gross_Amt() { return pnd_Grand_Total_Real_Est_Gross_Amt; }

    public DbsField getPnd_Title_Variable1() { return pnd_Title_Variable1; }

    public DbsField getPnd_Title_Variable2() { return pnd_Title_Variable2; }

    public DbsField getPnd_Tab_Divd() { return pnd_Tab_Divd; }

    public DbsField getPnd_Tab_Guar() { return pnd_Tab_Guar; }

    public DbsField getPnd_Tab_Gross() { return pnd_Tab_Gross; }

    public DbsField getPnd_Tab_Gross_Eft_Hold() { return pnd_Tab_Gross_Eft_Hold; }

    public DbsField getPnd_Tab_Gross_Eft() { return pnd_Tab_Gross_Eft; }

    public DbsField getPnd_Tab_Gross_Chk() { return pnd_Tab_Gross_Chk; }

    public DbsField getPnd_Tab_Dci() { return pnd_Tab_Dci; }

    public DbsField getPnd_Tab_Ovrpy() { return pnd_Tab_Ovrpy; }

    public DbsField getPnd_Tab_Num_Paym() { return pnd_Tab_Num_Paym; }

    public DbsField getPnd_Tab_Num_Eft() { return pnd_Tab_Num_Eft; }

    public DbsField getPnd_Tab_Num_Eft_Hold() { return pnd_Tab_Num_Eft_Hold; }

    public DbsField getPnd_Tab_Num_Check() { return pnd_Tab_Num_Check; }

    public DbsField getPnd_Tab_Header() { return pnd_Tab_Header; }

    public DbsField getPnd_Date_Ccyymmdd() { return pnd_Date_Ccyymmdd; }

    public DbsGroup getPnd_Date_CcyymmddRedef3() { return pnd_Date_CcyymmddRedef3; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Cc() { return pnd_Date_Ccyymmdd_Pnd_Date_Cc; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Yy() { return pnd_Date_Ccyymmdd_Pnd_Date_Yy; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Mm() { return pnd_Date_Ccyymmdd_Pnd_Date_Mm; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Dd() { return pnd_Date_Ccyymmdd_Pnd_Date_Dd; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef4() { return iaa_Parm_Card_Pnd_Parm_DateRedef4; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Date_NRedef5() { return iaa_Parm_Card_Pnd_Parm_Date_NRedef5; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph() { return pnd_Fl_Date_Yyyymmdd_Alph; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_AlphRedef6() { return pnd_Fl_Date_Yyyymmdd_AlphRedef6; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph() { return pnd_Fl_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_AlphRedef8() { return pnd_Fl_Time_Hhiiss_AlphRedef8; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef10() { return pnd_Sy_Date_Yymmdd_AlphRedef10; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef12() { return pnd_Sy_Time_Hhiiss_AlphRedef12; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsField getPnd_Sys_Time() { return pnd_Sys_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaal588r = newGroupInRecord("iaal588r", "IAAL588R");
        iaal588r_Msg_Info_Sub = iaal588r.newGroupInGroup("iaal588r_Msg_Info_Sub", "MSG-INFO-SUB");
        iaal588r_Pnd_Pnd_Msg = iaal588r_Msg_Info_Sub.newFieldInGroup("iaal588r_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        iaal588r_Pnd_Pnd_Msg_Nr = iaal588r_Msg_Info_Sub.newFieldInGroup("iaal588r_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        iaal588r_Pnd_Pnd_Msg_Data = iaal588r_Msg_Info_Sub.newFieldArrayInGroup("iaal588r_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1,
            3));
        iaal588r_Pnd_Pnd_Msg_DataRedef1 = iaal588r_Msg_Info_Sub.newGroupInGroup("iaal588r_Pnd_Pnd_Msg_DataRedef1", "Redefines", iaal588r_Pnd_Pnd_Msg_Data);
        iaal588r_Pnd_Pnd_Msg_Data_Struct = iaal588r_Pnd_Pnd_Msg_DataRedef1.newGroupArrayInGroup("iaal588r_Pnd_Pnd_Msg_Data_Struct", "##MSG-DATA-STRUCT", 
            new DbsArrayController(1,3));
        iaal588r_Pnd_Pnd_Msg_Data_Char = iaal588r_Pnd_Pnd_Msg_Data_Struct.newFieldArrayInGroup("iaal588r_Pnd_Pnd_Msg_Data_Char", "##MSG-DATA-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1,32));
        iaal588r_Pnd_Pnd_Return_Code = iaal588r_Msg_Info_Sub.newFieldInGroup("iaal588r_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        iaal588r_Pnd_Pnd_Error_Field = iaal588r_Msg_Info_Sub.newFieldInGroup("iaal588r_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        iaal588r_Pnd_Pnd_Error_Field_Index1 = iaal588r_Msg_Info_Sub.newFieldInGroup("iaal588r_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        iaal588r_Pnd_Pnd_Error_Field_Index2 = iaal588r_Msg_Info_Sub.newFieldInGroup("iaal588r_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        iaal588r_Pnd_Pnd_Error_Field_Index3 = iaal588r_Msg_Info_Sub.newFieldInGroup("iaal588r_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);
        iaal588r_Pnd_Parm_Cntrct = iaal588r.newFieldInGroup("iaal588r_Pnd_Parm_Cntrct", "#PARM-CNTRCT", FieldType.STRING, 10);
        iaal588r_Pnd_Parm_Payee = iaal588r.newFieldInGroup("iaal588r_Pnd_Parm_Payee", "#PARM-PAYEE", FieldType.NUMERIC, 2);
        iaal588r_Pnd_Parm_Fund_File = iaal588r.newFieldInGroup("iaal588r_Pnd_Parm_Fund_File", "#PARM-FUND-FILE", FieldType.STRING, 1);
        iaal588r_Pnd_Parm_Fund_Inverse_Date = iaal588r.newFieldInGroup("iaal588r_Pnd_Parm_Fund_Inverse_Date", "#PARM-FUND-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        iaal588r_Pnd_Parm_Fund = iaal588r.newFieldArrayInGroup("iaal588r_Pnd_Parm_Fund", "#PARM-FUND", FieldType.STRING, 3, new DbsArrayController(1,40));
        iaal588r_Pnd_Parm_Com_Val_Amt = iaal588r.newFieldArrayInGroup("iaal588r_Pnd_Parm_Com_Val_Amt", "#PARM-COM-VAL-AMT", FieldType.DECIMAL, 11,2, new 
            DbsArrayController(1,40));
        iaal588r_Pnd_Rqst_Dci_Amt = iaal588r.newFieldInGroup("iaal588r_Pnd_Rqst_Dci_Amt", "#RQST-DCI-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Iaan425b = newGroupInRecord("pnd_Iaan425b", "#IAAN425B");
        pnd_Iaan425b_Pnd_Iaan425b_Rqst_Est_Amt = pnd_Iaan425b.newFieldInGroup("pnd_Iaan425b_Pnd_Iaan425b_Rqst_Est_Amt", "#IAAN425B-RQST-EST-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Amt = pnd_Iaan425b.newFieldArrayInGroup("pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Amt", "#IAAN425B-COM-VAL-AMT", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,40));
        pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Dci = pnd_Iaan425b.newFieldArrayInGroup("pnd_Iaan425b_Pnd_Iaan425b_Com_Val_Dci", "#IAAN425B-COM-VAL-DCI", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,40));
        pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date = pnd_Iaan425b.newFieldArrayInGroup("pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date", "#IAAN425B-TAB-INST-DATE", 
            FieldType.STRING, 8, new DbsArrayController(1,120));
        pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_DateRedef2 = pnd_Iaan425b.newGroupInGroup("pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_DateRedef2", "Redefines", pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date);
        pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date_N = pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_DateRedef2.newFieldArrayInGroup("pnd_Iaan425b_Pnd_Iaan425b_Tab_Inst_Date_N", 
            "#IAAN425B-TAB-INST-DATE-N", FieldType.NUMERIC, 8, new DbsArrayController(1,120));
        pnd_Iaan425b_Pnd_Iaan425b_Tab_Per_Amt = pnd_Iaan425b.newFieldArrayInGroup("pnd_Iaan425b_Pnd_Iaan425b_Tab_Per_Amt", "#IAAN425B-TAB-PER-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,120));
        pnd_Iaan425b_Pnd_Iaan425b_Tab_Div_Amt = pnd_Iaan425b.newFieldArrayInGroup("pnd_Iaan425b_Pnd_Iaan425b_Tab_Div_Amt", "#IAAN425B-TAB-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,120));
        pnd_Iaan425b_Pnd_Iaan425b_Tab_Cmpny_Cde = pnd_Iaan425b.newFieldArrayInGroup("pnd_Iaan425b_Pnd_Iaan425b_Tab_Cmpny_Cde", "#IAAN425B-TAB-CMPNY-CDE", 
            FieldType.STRING, 3, new DbsArrayController(1,120));
        pnd_Iaan425b_Pnd_Iaan425b_Date_Of_Death = pnd_Iaan425b.newFieldInGroup("pnd_Iaan425b_Pnd_Iaan425b_Date_Of_Death", "#IAAN425B-DATE-OF-DEATH", FieldType.NUMERIC, 
            8);
        pnd_Iaan425b_Pnd_Iaan425b_Number_Of_Payments = pnd_Iaan425b.newFieldInGroup("pnd_Iaan425b_Pnd_Iaan425b_Number_Of_Payments", "#IAAN425B-NUMBER-OF-PAYMENTS", 
            FieldType.NUMERIC, 3);
        pnd_Iaan425b_Pnd_Iaan425b_Method_1_Or_2 = pnd_Iaan425b.newFieldInGroup("pnd_Iaan425b_Pnd_Iaan425b_Method_1_Or_2", "#IAAN425B-METHOD-1-OR-2", FieldType.STRING, 
            1);
        pnd_Iaan425b_Pnd_Iaan425b_Rqst_Dci_Amount = pnd_Iaan425b.newFieldInGroup("pnd_Iaan425b_Pnd_Iaan425b_Rqst_Dci_Amount", "#IAAN425B-RQST-DCI-AMOUNT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Audit_Reads = newFieldInRecord("pnd_Audit_Reads", "#AUDIT-READS", FieldType.NUMERIC, 6);

        pnd_Audit_Time_Selects = newFieldInRecord("pnd_Audit_Time_Selects", "#AUDIT-TIME-SELECTS", FieldType.NUMERIC, 6);

        pnd_Payment_Records = newFieldInRecord("pnd_Payment_Records", "#PAYMENT-RECORDS", FieldType.NUMERIC, 6);

        pnd_Diff_Accounting_Date = newFieldInRecord("pnd_Diff_Accounting_Date", "#DIFF-ACCOUNTING-DATE", FieldType.STRING, 1);

        pnd_M = newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);

        pnd_N = newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 3);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);

        pnd_J = newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);

        pnd_Num = newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 3);

        pnd_W_Title_Variable1 = newFieldInRecord("pnd_W_Title_Variable1", "#W-TITLE-VARIABLE1", FieldType.STRING, 8);

        pnd_W_Title_Variable2 = newFieldInRecord("pnd_W_Title_Variable2", "#W-TITLE-VARIABLE2", FieldType.STRING, 8);

        pnd_Account_Error = newFieldInRecord("pnd_Account_Error", "#ACCOUNT-ERROR", FieldType.STRING, 1);

        pnd_Eft_Hold = newFieldInRecord("pnd_Eft_Hold", "#EFT-HOLD", FieldType.STRING, 1);

        pnd_Eft = newFieldInRecord("pnd_Eft", "#EFT", FieldType.STRING, 1);

        pnd_Check = newFieldInRecord("pnd_Check", "#CHECK", FieldType.STRING, 1);

        pnd_Tab_Total_Num_Of_Pay = newFieldArrayInRecord("pnd_Tab_Total_Num_Of_Pay", "#TAB-TOTAL-NUM-OF-PAY", FieldType.NUMERIC, 4, new DbsArrayController(1,
            2));

        pnd_Tab_Total_Gross_Amt = newFieldArrayInRecord("pnd_Tab_Total_Gross_Amt", "#TAB-TOTAL-GROSS-AMT", FieldType.DECIMAL, 12,2, new DbsArrayController(1,
            2));

        pnd_Tab_Cref_Gross = newFieldArrayInRecord("pnd_Tab_Cref_Gross", "#TAB-CREF-GROSS", FieldType.DECIMAL, 12,2, new DbsArrayController(1,2));

        pnd_Grand_Total_Num_Of_Pay = newFieldInRecord("pnd_Grand_Total_Num_Of_Pay", "#GRAND-TOTAL-NUM-OF-PAY", FieldType.NUMERIC, 4);

        pnd_Grand_Total_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Gross_Amt", "#GRAND-TOTAL-GROSS-AMT", FieldType.DECIMAL, 12,2);

        pnd_Grand_Total_Tiaa_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Tiaa_Gross_Amt", "#GRAND-TOTAL-TIAA-GROSS-AMT", FieldType.DECIMAL, 12,2);

        pnd_Grand_Total_Cref_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Cref_Gross_Amt", "#GRAND-TOTAL-CREF-GROSS-AMT", FieldType.DECIMAL, 12,2);

        pnd_Grand_Total_Real_Est_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Real_Est_Gross_Amt", "#GRAND-TOTAL-REAL-EST-GROSS-AMT", FieldType.DECIMAL, 
            12,2);

        pnd_Title_Variable1 = newFieldArrayInRecord("pnd_Title_Variable1", "#TITLE-VARIABLE1", FieldType.NUMERIC, 8, new DbsArrayController(1,2));

        pnd_Title_Variable2 = newFieldArrayInRecord("pnd_Title_Variable2", "#TITLE-VARIABLE2", FieldType.NUMERIC, 8, new DbsArrayController(1,2));

        pnd_Tab_Divd = newFieldArrayInRecord("pnd_Tab_Divd", "#TAB-DIVD", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Guar = newFieldArrayInRecord("pnd_Tab_Guar", "#TAB-GUAR", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Gross = newFieldArrayInRecord("pnd_Tab_Gross", "#TAB-GROSS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Gross_Eft_Hold = newFieldArrayInRecord("pnd_Tab_Gross_Eft_Hold", "#TAB-GROSS-EFT-HOLD", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            2,1,60));

        pnd_Tab_Gross_Eft = newFieldArrayInRecord("pnd_Tab_Gross_Eft", "#TAB-GROSS-EFT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Gross_Chk = newFieldArrayInRecord("pnd_Tab_Gross_Chk", "#TAB-GROSS-CHK", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Dci = newFieldArrayInRecord("pnd_Tab_Dci", "#TAB-DCI", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Ovrpy = newFieldArrayInRecord("pnd_Tab_Ovrpy", "#TAB-OVRPY", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Num_Paym = newFieldArrayInRecord("pnd_Tab_Num_Paym", "#TAB-NUM-PAYM", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Num_Eft = newFieldArrayInRecord("pnd_Tab_Num_Eft", "#TAB-NUM-EFT", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Num_Eft_Hold = newFieldArrayInRecord("pnd_Tab_Num_Eft_Hold", "#TAB-NUM-EFT-HOLD", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Num_Check = newFieldArrayInRecord("pnd_Tab_Num_Check", "#TAB-NUM-CHECK", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Header = newFieldArrayInRecord("pnd_Tab_Header", "#TAB-HEADER", FieldType.STRING, 12, new DbsArrayController(1,12));

        pnd_Date_Ccyymmdd = newFieldInRecord("pnd_Date_Ccyymmdd", "#DATE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Date_CcyymmddRedef3 = newGroupInRecord("pnd_Date_CcyymmddRedef3", "Redefines", pnd_Date_Ccyymmdd);
        pnd_Date_Ccyymmdd_Pnd_Date_Cc = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Yy = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Mm = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Dd = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef4 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef4", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_Date_NRedef5 = iaa_Parm_Card_Pnd_Parm_DateRedef4.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Date_NRedef5", "Redefines", iaa_Parm_Card_Pnd_Parm_Date_N);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Fl_Date_Yyyymmdd_Alph = newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Fl_Date_Yyyymmdd_AlphRedef6 = newGroupInRecord("pnd_Fl_Date_Yyyymmdd_AlphRedef6", "Redefines", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7 = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7", 
            "Redefines", pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd", 
            "#FL-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Fl_Time_Hhiiss_Alph = newFieldInRecord("pnd_Fl_Time_Hhiiss_Alph", "#FL-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Fl_Time_Hhiiss_AlphRedef8 = newGroupInRecord("pnd_Fl_Time_Hhiiss_AlphRedef8", "Redefines", pnd_Fl_Time_Hhiiss_Alph);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num = pnd_Fl_Time_Hhiiss_AlphRedef8.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num", 
            "#FL-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9 = pnd_Fl_Time_Hhiiss_AlphRedef8.newGroupInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9", 
            "Redefines", pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh", 
            "#FL-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii", 
            "#FL-TIME-II", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss", 
            "#FL-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef10 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef10", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef10.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11 = pnd_Sy_Date_Yymmdd_AlphRedef10.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef12 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef12", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef12.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13 = pnd_Sy_Time_Hhiiss_AlphRedef12.newGroupInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13", 
            "Redefines", pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh", 
            "#SY-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii", 
            "#SY-TIME-II", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss", 
            "#SY-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        pnd_Sys_Time = newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);

        this.setRecordName("LdaIaal588r");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal588r() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
