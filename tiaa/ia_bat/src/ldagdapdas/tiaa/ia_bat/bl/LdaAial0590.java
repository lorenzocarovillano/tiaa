/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:33 PM
**        * FROM NATURAL LDA     : AIAL0590
************************************************************
**        * FILE NAME            : LdaAial0590.java
**        * CLASS NAME           : LdaAial0590
**        * INSTANCE NAME        : LdaAial0590
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0590 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Trans_Rcrd_View;
    private DbsField iaa_Trans_Rcrd_View_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_View_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_View_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_View_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_View_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_View_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Effective_Dte;
    private DbsField iaa_Trans_Rcrd_View_Trans_Cntrct_Key;
    private DbsField iaa_Trans_Rcrd_View_Trans_Chck_Dte_Key;

    public DataAccessProgramView getVw_iaa_Trans_Rcrd_View() { return vw_iaa_Trans_Rcrd_View; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Dte() { return iaa_Trans_Rcrd_View_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_View_Invrse_Trans_Dte() { return iaa_Trans_Rcrd_View_Invrse_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr() { return iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Payee_Cde() { return iaa_Trans_Rcrd_View_Trans_Payee_Cde; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Cde() { return iaa_Trans_Rcrd_View_Trans_Cde; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Sub_Cde() { return iaa_Trans_Rcrd_View_Trans_Sub_Cde; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_User_Id() { return iaa_Trans_Rcrd_View_Trans_User_Id; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Actvty_Cde() { return iaa_Trans_Rcrd_View_Trans_Actvty_Cde; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Check_Dte() { return iaa_Trans_Rcrd_View_Trans_Check_Dte; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Todays_Dte() { return iaa_Trans_Rcrd_View_Trans_Todays_Dte; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Verify_Cde() { return iaa_Trans_Rcrd_View_Trans_Verify_Cde; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Effective_Dte() { return iaa_Trans_Rcrd_View_Trans_Effective_Dte; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Cntrct_Key() { return iaa_Trans_Rcrd_View_Trans_Cntrct_Key; }

    public DbsField getIaa_Trans_Rcrd_View_Trans_Chck_Dte_Key() { return iaa_Trans_Rcrd_View_Trans_Chck_Dte_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Trans_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd_View", "IAA-TRANS-RCRD-VIEW"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_View_Trans_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Trans_Rcrd_View_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_View_Trans_Payee_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Payee_Cde", "TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_View_Trans_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TRANS_CDE");
        iaa_Trans_Rcrd_View_Trans_Sub_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_View_Trans_User_Id = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_View_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_View_Trans_Check_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_View_Trans_Todays_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Todays_Dte", "TRANS-TODAYS-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_View_Trans_Verify_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Verify_Cde", "TRANS-VERIFY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_View_Trans_Effective_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        iaa_Trans_Rcrd_View_Trans_Cntrct_Key = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Cntrct_Key", "TRANS-CNTRCT-KEY", 
            FieldType.STRING, 27, RepeatingFieldStrategy.None, "TRANS_CNTRCT_KEY");
        iaa_Trans_Rcrd_View_Trans_Chck_Dte_Key = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Chck_Dte_Key", "TRANS-CHCK-DTE-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TRANS_CHCK_DTE_KEY");

        this.setRecordName("LdaAial0590");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Trans_Rcrd_View.reset();
    }

    // Constructor
    public LdaAial0590() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
