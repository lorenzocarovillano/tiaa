/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:51 PM
**        * FROM NATURAL PDA     : IATL402P
************************************************************
**        * FILE NAME            : PdaIatl402p.java
**        * CLASS NAME           : PdaIatl402p
**        * INSTANCE NAME        : PdaIatl402p
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIatl402p extends PdaBase
{
    // Properties
    private DbsGroup pnd_Iatn402_In;
    private DbsField pnd_Iatn402_In_Pnd_Rqst_Id;
    private DbsField pnd_Iatn402_In_Pnd_Isn;
    private DbsField pnd_Iatn402_In_Pnd_Sys_Time;
    private DbsField pnd_Iatn402_In_Pnd_Todays_Dte;
    private DbsGroup pnd_Iatn402_In_Pnd_Todays_DteRedef1;
    private DbsField pnd_Iatn402_In_Pnd_Todays_Dte_N;
    private DbsField pnd_Iatn402_In_Pnd_Next_Bus_Dte;
    private DbsField pnd_Iatn402_In_Pnd_Check_Control_Rec;
    private DbsGroup pnd_Iatn402_Out;
    private DbsField pnd_Iatn402_Out_Pnd_Return_Code;
    private DbsField pnd_Iatn402_Out_Pnd_Error_Nbr;
    private DbsField pnd_Iatn402_Out_Pnd_Xfr_Stts_Cde;
    private DbsField pnd_Iatn402_Out_Pnd_Msg;
    private DbsField pnd_Iatn402_Out_Pnd_Msg_2;

    public DbsGroup getPnd_Iatn402_In() { return pnd_Iatn402_In; }

    public DbsField getPnd_Iatn402_In_Pnd_Rqst_Id() { return pnd_Iatn402_In_Pnd_Rqst_Id; }

    public DbsField getPnd_Iatn402_In_Pnd_Isn() { return pnd_Iatn402_In_Pnd_Isn; }

    public DbsField getPnd_Iatn402_In_Pnd_Sys_Time() { return pnd_Iatn402_In_Pnd_Sys_Time; }

    public DbsField getPnd_Iatn402_In_Pnd_Todays_Dte() { return pnd_Iatn402_In_Pnd_Todays_Dte; }

    public DbsGroup getPnd_Iatn402_In_Pnd_Todays_DteRedef1() { return pnd_Iatn402_In_Pnd_Todays_DteRedef1; }

    public DbsField getPnd_Iatn402_In_Pnd_Todays_Dte_N() { return pnd_Iatn402_In_Pnd_Todays_Dte_N; }

    public DbsField getPnd_Iatn402_In_Pnd_Next_Bus_Dte() { return pnd_Iatn402_In_Pnd_Next_Bus_Dte; }

    public DbsField getPnd_Iatn402_In_Pnd_Check_Control_Rec() { return pnd_Iatn402_In_Pnd_Check_Control_Rec; }

    public DbsGroup getPnd_Iatn402_Out() { return pnd_Iatn402_Out; }

    public DbsField getPnd_Iatn402_Out_Pnd_Return_Code() { return pnd_Iatn402_Out_Pnd_Return_Code; }

    public DbsField getPnd_Iatn402_Out_Pnd_Error_Nbr() { return pnd_Iatn402_Out_Pnd_Error_Nbr; }

    public DbsField getPnd_Iatn402_Out_Pnd_Xfr_Stts_Cde() { return pnd_Iatn402_Out_Pnd_Xfr_Stts_Cde; }

    public DbsField getPnd_Iatn402_Out_Pnd_Msg() { return pnd_Iatn402_Out_Pnd_Msg; }

    public DbsField getPnd_Iatn402_Out_Pnd_Msg_2() { return pnd_Iatn402_Out_Pnd_Msg_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iatn402_In = dbsRecord.newGroupInRecord("pnd_Iatn402_In", "#IATN402-IN");
        pnd_Iatn402_In.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn402_In_Pnd_Rqst_Id = pnd_Iatn402_In.newFieldInGroup("pnd_Iatn402_In_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 34);
        pnd_Iatn402_In_Pnd_Isn = pnd_Iatn402_In.newFieldInGroup("pnd_Iatn402_In_Pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Iatn402_In_Pnd_Sys_Time = pnd_Iatn402_In.newFieldInGroup("pnd_Iatn402_In_Pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        pnd_Iatn402_In_Pnd_Todays_Dte = pnd_Iatn402_In.newFieldInGroup("pnd_Iatn402_In_Pnd_Todays_Dte", "#TODAYS-DTE", FieldType.STRING, 8);
        pnd_Iatn402_In_Pnd_Todays_DteRedef1 = pnd_Iatn402_In.newGroupInGroup("pnd_Iatn402_In_Pnd_Todays_DteRedef1", "Redefines", pnd_Iatn402_In_Pnd_Todays_Dte);
        pnd_Iatn402_In_Pnd_Todays_Dte_N = pnd_Iatn402_In_Pnd_Todays_DteRedef1.newFieldInGroup("pnd_Iatn402_In_Pnd_Todays_Dte_N", "#TODAYS-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Iatn402_In_Pnd_Next_Bus_Dte = pnd_Iatn402_In.newFieldInGroup("pnd_Iatn402_In_Pnd_Next_Bus_Dte", "#NEXT-BUS-DTE", FieldType.STRING, 8);
        pnd_Iatn402_In_Pnd_Check_Control_Rec = pnd_Iatn402_In.newFieldInGroup("pnd_Iatn402_In_Pnd_Check_Control_Rec", "#CHECK-CONTROL-REC", FieldType.BOOLEAN);

        pnd_Iatn402_Out = dbsRecord.newGroupInRecord("pnd_Iatn402_Out", "#IATN402-OUT");
        pnd_Iatn402_Out.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn402_Out_Pnd_Return_Code = pnd_Iatn402_Out.newFieldInGroup("pnd_Iatn402_Out_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Iatn402_Out_Pnd_Error_Nbr = pnd_Iatn402_Out.newFieldInGroup("pnd_Iatn402_Out_Pnd_Error_Nbr", "#ERROR-NBR", FieldType.STRING, 3);
        pnd_Iatn402_Out_Pnd_Xfr_Stts_Cde = pnd_Iatn402_Out.newFieldInGroup("pnd_Iatn402_Out_Pnd_Xfr_Stts_Cde", "#XFR-STTS-CDE", FieldType.STRING, 2);
        pnd_Iatn402_Out_Pnd_Msg = pnd_Iatn402_Out.newFieldInGroup("pnd_Iatn402_Out_Pnd_Msg", "#MSG", FieldType.STRING, 79);
        pnd_Iatn402_Out_Pnd_Msg_2 = pnd_Iatn402_Out.newFieldInGroup("pnd_Iatn402_Out_Pnd_Msg_2", "#MSG-2", FieldType.STRING, 79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIatl402p(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

