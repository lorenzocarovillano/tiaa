/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:14 PM
**        * FROM NATURAL LDA     : IAAL162G
************************************************************
**        * FILE NAME            : LdaIaal162g.java
**        * CLASS NAME           : LdaIaal162g
**        * INSTANCE NAME        : LdaIaal162g
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal162g extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct_1;
    private DbsField iaa_Cntrct_1_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_1_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_1_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_1_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_1_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_1_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_1_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_1_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_1_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_1_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_1_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_1_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Type;
    private DbsField iaa_Cntrct_1_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_1_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_1_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_1_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_1_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_1_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_1_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_1_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_1_Plan_Nmbr;
    private DbsField iaa_Cntrct_1_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_1_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_1_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_1_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_1_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_1_Orgntng_Cntrct_Nmbr;
    private DataAccessProgramView vw_iaa_Cntrct_2;
    private DbsField iaa_Cntrct_2_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_2_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_2_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_2_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_2_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_2_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_2_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_2_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_2_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_2_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_2_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_2_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Type;
    private DbsField iaa_Cntrct_2_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_2_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_2_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_2_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_2_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_2_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_2_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_2_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_2_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_2_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_2_Plan_Nmbr;
    private DbsField iaa_Cntrct_2_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_2_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_2_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_2_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_2_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_2_Orgntng_Cntrct_Nmbr;
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Trans_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr;
    private DataAccessProgramView vw_iaa_Cpr_2;
    private DbsField iaa_Cpr_2_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_2_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_2_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_2_Lst_Trans_Dte;
    private DbsField iaa_Cpr_2_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_2_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_2_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_2_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_2_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_2_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_2_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_2_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup iaa_Cpr_2_Cntrct_Company_Data;
    private DbsField iaa_Cpr_2_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_2_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_2_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_2_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_2_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_2_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_2_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_2_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_2_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_2_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_2_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_2_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_2_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_2_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_2_Cntrct_State_Cde;
    private DbsField iaa_Cpr_2_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_2_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_2_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_2_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cpr_2_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cpr_2_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cpr_2_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Cpr_2_Rllvr_Ivc_Ind;
    private DbsField iaa_Cpr_2_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Cpr_2_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Cpr_2_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Cpr_2_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Cpr_2_Rllvr_Pln_Admn_Ind;
    private DbsField iaa_Cpr_2_Roth_Dsblty_Dte;

    public DataAccessProgramView getVw_iaa_Cntrct_1() { return vw_iaa_Cntrct_1; }

    public DbsField getIaa_Cntrct_1_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_1_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_1_Cntrct_Optn_Cde() { return iaa_Cntrct_1_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Orgn_Cde() { return iaa_Cntrct_1_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Acctng_Cde() { return iaa_Cntrct_1_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Issue_Dte() { return iaa_Cntrct_1_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_1_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_1_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Crrncy_Cde() { return iaa_Cntrct_1_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Type_Cde() { return iaa_Cntrct_1_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_1_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_1_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_1_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_1_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_1_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_1_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_1_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_1_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_1_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_1_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_1_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_1_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_1_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_1_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_1_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_1_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_1_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_1_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_1_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_1_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_1_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_1_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_1_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_1_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_1_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_1_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_1_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_1_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_1_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_1_Lst_Trans_Dte() { return iaa_Cntrct_1_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Type() { return iaa_Cntrct_1_Cntrct_Type; }

    public DbsField getIaa_Cntrct_1_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_1_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_1_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_1_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_1_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_1_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_1_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_1_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_1_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_1_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_1_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_1_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_1_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_1_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_1_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_1_Cntrct_Ssnng_Dte() { return iaa_Cntrct_1_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_1_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_1_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_1_Roth_Ssnng_Dte() { return iaa_Cntrct_1_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_1_Plan_Nmbr() { return iaa_Cntrct_1_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_1_Tax_Exmpt_Ind() { return iaa_Cntrct_1_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_1_Orig_Ownr_Dob() { return iaa_Cntrct_1_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_1_Orig_Ownr_Dod() { return iaa_Cntrct_1_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_1_Sub_Plan_Nmbr() { return iaa_Cntrct_1_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_1_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_1_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_1_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_1_Orgntng_Cntrct_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cntrct_2() { return vw_iaa_Cntrct_2; }

    public DbsField getIaa_Cntrct_2_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_2_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_2_Cntrct_Optn_Cde() { return iaa_Cntrct_2_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Orgn_Cde() { return iaa_Cntrct_2_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Acctng_Cde() { return iaa_Cntrct_2_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Issue_Dte() { return iaa_Cntrct_2_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_2_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_2_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Crrncy_Cde() { return iaa_Cntrct_2_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Type_Cde() { return iaa_Cntrct_2_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_2_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_2_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_2_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_2_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_2_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_2_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_2_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_2_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_2_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_2_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_2_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_2_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_2_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_2_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_2_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_2_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_2_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_2_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_2_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_2_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_2_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_2_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_2_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_2_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_2_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_2_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_2_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_2_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_2_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_2_Lst_Trans_Dte() { return iaa_Cntrct_2_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Type() { return iaa_Cntrct_2_Cntrct_Type; }

    public DbsField getIaa_Cntrct_2_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_2_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_2_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_2_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_2_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_2_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_2_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_2_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_2_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_2_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_2_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_2_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_2_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_2_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_2_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_2_Cntrct_Ssnng_Dte() { return iaa_Cntrct_2_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_2_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_2_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_2_Roth_Ssnng_Dte() { return iaa_Cntrct_2_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_2_Plan_Nmbr() { return iaa_Cntrct_2_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_2_Tax_Exmpt_Ind() { return iaa_Cntrct_2_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_2_Orig_Ownr_Dob() { return iaa_Cntrct_2_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_2_Orig_Ownr_Dod() { return iaa_Cntrct_2_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_2_Sub_Plan_Nmbr() { return iaa_Cntrct_2_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_2_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_2_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_2_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_2_Orgntng_Cntrct_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Trans_Dte() { return iaa_Cntrct_Trans_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Invrse_Trans_Dte() { return iaa_Cntrct_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Lst_Trans_Dte() { return iaa_Cntrct_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Optn_Cde() { return iaa_Cntrct_Trans_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orgn_Cde() { return iaa_Cntrct_Trans_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Acctng_Cde() { return iaa_Cntrct_Trans_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Trans_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type_Cde() { return iaa_Cntrct_Trans_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Trans_Trans_Check_Dte() { return iaa_Cntrct_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cntrct_Trans_Bfre_Imge_Id() { return iaa_Cntrct_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Aftr_Imge_Id() { return iaa_Cntrct_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type() { return iaa_Cntrct_Trans_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Trans_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Ssnng_Dte() { return iaa_Cntrct_Trans_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Plan_Nmbr() { return iaa_Cntrct_Trans_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Tax_Exmpt_Ind() { return iaa_Cntrct_Trans_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dob() { return iaa_Cntrct_Trans_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dod() { return iaa_Cntrct_Trans_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Trans_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cpr_2() { return vw_iaa_Cpr_2; }

    public DbsField getIaa_Cpr_2_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_2_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_2_Cntrct_Part_Payee_Cde() { return iaa_Cpr_2_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_2_Cpr_Id_Nbr() { return iaa_Cpr_2_Cpr_Id_Nbr; }

    public DbsField getIaa_Cpr_2_Lst_Trans_Dte() { return iaa_Cpr_2_Lst_Trans_Dte; }

    public DbsField getIaa_Cpr_2_Prtcpnt_Ctznshp_Cde() { return iaa_Cpr_2_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cpr_2_Prtcpnt_Rsdncy_Cde() { return iaa_Cpr_2_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cpr_2_Prtcpnt_Rsdncy_Sw() { return iaa_Cpr_2_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cpr_2_Prtcpnt_Tax_Id_Nbr() { return iaa_Cpr_2_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cpr_2_Prtcpnt_Tax_Id_Typ() { return iaa_Cpr_2_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Cpr_2_Cntrct_Actvty_Cde() { return iaa_Cpr_2_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Trmnte_Rsn() { return iaa_Cpr_2_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Cpr_2_Cntrct_Rwrttn_Ind() { return iaa_Cpr_2_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Cpr_2_Cntrct_Cash_Cde() { return iaa_Cpr_2_Cntrct_Cash_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cpr_2_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getIaa_Cpr_2_Cntrct_Company_Data() { return iaa_Cpr_2_Cntrct_Company_Data; }

    public DbsField getIaa_Cpr_2_Cntrct_Company_Cd() { return iaa_Cpr_2_Cntrct_Company_Cd; }

    public DbsField getIaa_Cpr_2_Cntrct_Rcvry_Type_Ind() { return iaa_Cpr_2_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Cpr_2_Cntrct_Per_Ivc_Amt() { return iaa_Cpr_2_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Resdl_Ivc_Amt() { return iaa_Cpr_2_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Ivc_Amt() { return iaa_Cpr_2_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Ivc_Used_Amt() { return iaa_Cpr_2_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Rtb_Amt() { return iaa_Cpr_2_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Rtb_Percent() { return iaa_Cpr_2_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Cpr_2_Cntrct_Mode_Ind() { return iaa_Cpr_2_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cpr_2_Cntrct_Wthdrwl_Dte() { return iaa_Cpr_2_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cpr_2_Cntrct_Final_Per_Pay_Dte() { return iaa_Cpr_2_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cpr_2_Cntrct_Final_Pay_Dte() { return iaa_Cpr_2_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cpr_2_Bnfcry_Xref_Ind() { return iaa_Cpr_2_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cpr_2_Bnfcry_Dod_Dte() { return iaa_Cpr_2_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cpr_2_Cntrct_Pend_Cde() { return iaa_Cpr_2_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Hold_Cde() { return iaa_Cpr_2_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Pend_Dte() { return iaa_Cpr_2_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cpr_2_Cntrct_Prev_Dist_Cde() { return iaa_Cpr_2_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Curr_Dist_Cde() { return iaa_Cpr_2_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Cmbne_Cde() { return iaa_Cpr_2_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Spirt_Cde() { return iaa_Cpr_2_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Spirt_Amt() { return iaa_Cpr_2_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Spirt_Srce() { return iaa_Cpr_2_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cpr_2_Cntrct_Spirt_Arr_Dte() { return iaa_Cpr_2_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Cpr_2_Cntrct_Spirt_Prcss_Dte() { return iaa_Cpr_2_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Cpr_2_Cntrct_Fed_Tax_Amt() { return iaa_Cpr_2_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_State_Cde() { return iaa_Cpr_2_Cntrct_State_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_State_Tax_Amt() { return iaa_Cpr_2_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Local_Cde() { return iaa_Cpr_2_Cntrct_Local_Cde; }

    public DbsField getIaa_Cpr_2_Cntrct_Local_Tax_Amt() { return iaa_Cpr_2_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Cpr_2_Cntrct_Lst_Chnge_Dte() { return iaa_Cpr_2_Cntrct_Lst_Chnge_Dte; }

    public DbsField getIaa_Cpr_2_Cpr_Xfr_Term_Cde() { return iaa_Cpr_2_Cpr_Xfr_Term_Cde; }

    public DbsField getIaa_Cpr_2_Cpr_Lgl_Res_Cde() { return iaa_Cpr_2_Cpr_Lgl_Res_Cde; }

    public DbsField getIaa_Cpr_2_Cpr_Xfr_Iss_Dte() { return iaa_Cpr_2_Cpr_Xfr_Iss_Dte; }

    public DbsField getIaa_Cpr_2_Rllvr_Cntrct_Nbr() { return iaa_Cpr_2_Rllvr_Cntrct_Nbr; }

    public DbsField getIaa_Cpr_2_Rllvr_Ivc_Ind() { return iaa_Cpr_2_Rllvr_Ivc_Ind; }

    public DbsField getIaa_Cpr_2_Rllvr_Elgble_Ind() { return iaa_Cpr_2_Rllvr_Elgble_Ind; }

    public DbsGroup getIaa_Cpr_2_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return iaa_Cpr_2_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getIaa_Cpr_2_Rllvr_Dstrbtng_Irc_Cde() { return iaa_Cpr_2_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getIaa_Cpr_2_Rllvr_Accptng_Irc_Cde() { return iaa_Cpr_2_Rllvr_Accptng_Irc_Cde; }

    public DbsField getIaa_Cpr_2_Rllvr_Pln_Admn_Ind() { return iaa_Cpr_2_Rllvr_Pln_Admn_Ind; }

    public DbsField getIaa_Cpr_2_Roth_Dsblty_Dte() { return iaa_Cpr_2_Roth_Dsblty_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_1", "IAA-CNTRCT-1"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_1_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_1_Cntrct_Optn_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_1_Cntrct_Orgn_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_1_Cntrct_Acctng_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_1_Cntrct_Issue_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_1_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_1_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_1_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_1_Cntrct_Type_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_1_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_1_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_1_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_1_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_1_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_1_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_1_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_1_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_1_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_1_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_1_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_1_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_1_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_1_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_1_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_1_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_1_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_1_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_1_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_1_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_1_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_1_Lst_Trans_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_1_Cntrct_Type = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_1_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_1_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_1.getRecord().newGroupInGroup("iaa_Cntrct_1_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_1_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_1_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_1_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_1_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_1_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_1_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_1_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_1_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_1_Cntrct_Ssnng_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_1_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_1_Roth_Ssnng_Dte = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_SSNNG_DTE");
        iaa_Cntrct_1_Plan_Nmbr = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_1_Tax_Exmpt_Ind = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_EXMPT_IND");
        iaa_Cntrct_1_Orig_Ownr_Dob = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOB");
        iaa_Cntrct_1_Orig_Ownr_Dod = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOD");
        iaa_Cntrct_1_Sub_Plan_Nmbr = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NMBR");
        iaa_Cntrct_1_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_1_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_1.getRecord().newFieldInGroup("iaa_Cntrct_1_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");

        vw_iaa_Cntrct_2 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_2", "IAA-CNTRCT-2"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_2_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_2_Cntrct_Optn_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_2_Cntrct_Orgn_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_2_Cntrct_Acctng_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_2_Cntrct_Issue_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_2_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_2_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_2_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_2_Cntrct_Type_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_2_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_2_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_2_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_2_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_2_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_2_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_2_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_2_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_2_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_2_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_2_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_2_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_2_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_2_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_2_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_2_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_2_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_2_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_2_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_2_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_2_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_2_Lst_Trans_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_2_Cntrct_Type = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_2_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_2_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_2.getRecord().newGroupInGroup("iaa_Cntrct_2_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_2_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_2_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_2_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_2_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_2_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_2_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_2_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_2_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_2_Cntrct_Ssnng_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_2_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_2_Roth_Ssnng_Dte = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_SSNNG_DTE");
        iaa_Cntrct_2_Plan_Nmbr = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_2_Tax_Exmpt_Ind = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_EXMPT_IND");
        iaa_Cntrct_2_Orig_Ownr_Dob = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOB");
        iaa_Cntrct_2_Orig_Ownr_Dod = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOD");
        iaa_Cntrct_2_Sub_Plan_Nmbr = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NMBR");
        iaa_Cntrct_2_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_2_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_2.getRecord().newFieldInGroup("iaa_Cntrct_2_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_Cntrct_Acctng_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_Trans_Check_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Cntrct_Type = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Trans_Roth_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_SSNNG_DTE");
        iaa_Cntrct_Trans_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Trans_Tax_Exmpt_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TAX_EXMPT_IND");
        iaa_Cntrct_Trans_Orig_Ownr_Dob = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOB");
        iaa_Cntrct_Trans_Orig_Ownr_Dod = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOD");
        iaa_Cntrct_Trans_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");

        vw_iaa_Cpr_2 = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_2", "IAA-CPR-2"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cpr_2_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_2_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_2_Cpr_Id_Nbr = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_2_Lst_Trans_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_2_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_2_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_2_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_2_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_2_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_2_Cntrct_Actvty_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_2_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_2_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_2_Cntrct_Cash_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_CASH_CDE");
        iaa_Cpr_2_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cpr_2_Cntrct_Company_Data = vw_iaa_Cpr_2.getRecord().newGroupInGroup("iaa_Cpr_2_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Company_Cd = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", FieldType.STRING, 
            1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Rcvry_Type_Ind = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Per_Ivc_Amt = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Ivc_Amt = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Ivc_Used_Amt = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Rtb_Amt = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Rtb_Percent = iaa_Cpr_2_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_2_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 
            7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_2_Cntrct_Mode_Ind = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        iaa_Cpr_2_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_2_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_2_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_2_Bnfcry_Xref_Ind = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        iaa_Cpr_2_Bnfcry_Dod_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        iaa_Cpr_2_Cntrct_Pend_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        iaa_Cpr_2_Cntrct_Hold_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        iaa_Cpr_2_Cntrct_Pend_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        iaa_Cpr_2_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_2_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_2_Cntrct_Cmbne_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_2_Cntrct_Spirt_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_CDE");
        iaa_Cpr_2_Cntrct_Spirt_Amt = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_2_Cntrct_Spirt_Srce = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_2_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_2_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_2_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_2_Cntrct_State_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_STATE_CDE");
        iaa_Cpr_2_Cntrct_State_Tax_Amt = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_2_Cntrct_Local_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_LOCAL_CDE");
        iaa_Cpr_2_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_2_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_2_Cpr_Xfr_Term_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CPR_XFR_TERM_CDE");
        iaa_Cpr_2_Cpr_Lgl_Res_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CPR_LGL_RES_CDE");
        iaa_Cpr_2_Cpr_Xfr_Iss_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CPR_XFR_ISS_DTE");
        iaa_Cpr_2_Rllvr_Cntrct_Nbr = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Cpr_2_Rllvr_Ivc_Ind = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_IVC_IND");
        iaa_Cpr_2_Rllvr_Elgble_Ind = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Rllvr_Elgble_Ind", "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_ELGBLE_IND");
        iaa_Cpr_2_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Cpr_2.getRecord().newGroupInGroup("iaa_Cpr_2_Rllvr_Dstrbtng_Irc_CdeMuGroup", "RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cpr_2_Rllvr_Dstrbtng_Irc_Cde = iaa_Cpr_2_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Cpr_2_Rllvr_Dstrbtng_Irc_Cde", "RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cpr_2_Rllvr_Accptng_Irc_Cde = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Rllvr_Accptng_Irc_Cde", "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Cpr_2_Rllvr_Pln_Admn_Ind = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Rllvr_Pln_Admn_Ind", "RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        iaa_Cpr_2_Roth_Dsblty_Dte = vw_iaa_Cpr_2.getRecord().newFieldInGroup("iaa_Cpr_2_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_DSBLTY_DTE");
        vw_iaa_Cpr_2.setUniquePeList();

        this.setRecordName("LdaIaal162g");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct_1.reset();
        vw_iaa_Cntrct_2.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_2.reset();
    }

    // Constructor
    public LdaIaal162g() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
