/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:51 PM
**        * FROM NATURAL PDA     : IATL420X
************************************************************
**        * FILE NAME            : PdaIatl420x.java
**        * CLASS NAME           : PdaIatl420x
**        * INSTANCE NAME        : PdaIatl420x
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIatl420x extends PdaBase
{
    // Properties
    private DbsGroup pnd_Iatn420x_In;
    private DbsField pnd_Iatn420x_In_Pnd_New_Tiaa_Payee;
    private DbsField pnd_Iatn420x_In_Pnd_New_Cref_Payee;
    private DbsField pnd_Iatn420x_In_Pnd_New_Inactive_Ind;
    private DbsField pnd_Iatn420x_In_Pnd_Return_Code;
    private DbsField pnd_Iatn420x_In_Pnd_Msg;
    private DbsGroup pnd_Iaa_Xfr_Audit;
    private DbsField pnd_Iaa_Xfr_Audit_Rcrd_Type_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Rqst_Id;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Dte;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Tme;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Wpid;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Payee_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_In_Progress_Ind;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Retry_Cnt;
    private DbsField pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_From_Acct_Data;
    private DbsGroup pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Typ;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Qty;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid;
    private DbsField pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_To_Acct_Data;
    private DbsGroup pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Acct_Cd;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Typ;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Qty;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Rate_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte;
    private DbsField pnd_Iaa_Xfr_Audit_Iaxfr_Acctng_Dte;
    private DbsField pnd_Iaa_Xfr_Audit_Lst_Chnge_Dte;

    public DbsGroup getPnd_Iatn420x_In() { return pnd_Iatn420x_In; }

    public DbsField getPnd_Iatn420x_In_Pnd_New_Tiaa_Payee() { return pnd_Iatn420x_In_Pnd_New_Tiaa_Payee; }

    public DbsField getPnd_Iatn420x_In_Pnd_New_Cref_Payee() { return pnd_Iatn420x_In_Pnd_New_Cref_Payee; }

    public DbsField getPnd_Iatn420x_In_Pnd_New_Inactive_Ind() { return pnd_Iatn420x_In_Pnd_New_Inactive_Ind; }

    public DbsField getPnd_Iatn420x_In_Pnd_Return_Code() { return pnd_Iatn420x_In_Pnd_Return_Code; }

    public DbsField getPnd_Iatn420x_In_Pnd_Msg() { return pnd_Iatn420x_In_Pnd_Msg; }

    public DbsGroup getPnd_Iaa_Xfr_Audit() { return pnd_Iaa_Xfr_Audit; }

    public DbsField getPnd_Iaa_Xfr_Audit_Rcrd_Type_Cde() { return pnd_Iaa_Xfr_Audit_Rcrd_Type_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Rqst_Id() { return pnd_Iaa_Xfr_Audit_Rqst_Id; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte() { return pnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte() { return pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme() { return pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte() { return pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme() { return pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Entry_Dte() { return pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Dte; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Entry_Tme() { return pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Tme; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Wpid() { return pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Wpid; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time() { return pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Payee_Cde() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Payee_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind() { return pnd_Iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_In_Progress_Ind() { return pnd_Iaa_Xfr_Audit_Iaxfr_In_Progress_Ind; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Retry_Cnt() { return pnd_Iaa_Xfr_Audit_Iaxfr_Retry_Cnt; }

    public DbsField getPnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_From_Acct_Data() { return pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_From_Acct_Data; }

    public DbsGroup getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd() { return pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ() { return pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Typ() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Typ; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Qty() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Qty; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid() { return pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid; }

    public DbsField getPnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_To_Acct_Data() { return pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_To_Acct_Data; }

    public DbsGroup getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Acct_Cd() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Acct_Cd; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Typ() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Typ; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Qty() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Qty; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Rate_Cde() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Rate_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid() { return pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte() { return pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte() { return pnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte; }

    public DbsField getPnd_Iaa_Xfr_Audit_Iaxfr_Acctng_Dte() { return pnd_Iaa_Xfr_Audit_Iaxfr_Acctng_Dte; }

    public DbsField getPnd_Iaa_Xfr_Audit_Lst_Chnge_Dte() { return pnd_Iaa_Xfr_Audit_Lst_Chnge_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iatn420x_In = dbsRecord.newGroupInRecord("pnd_Iatn420x_In", "#IATN420X-IN");
        pnd_Iatn420x_In.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn420x_In_Pnd_New_Tiaa_Payee = pnd_Iatn420x_In.newFieldInGroup("pnd_Iatn420x_In_Pnd_New_Tiaa_Payee", "#NEW-TIAA-PAYEE", FieldType.STRING, 
            1);
        pnd_Iatn420x_In_Pnd_New_Cref_Payee = pnd_Iatn420x_In.newFieldInGroup("pnd_Iatn420x_In_Pnd_New_Cref_Payee", "#NEW-CREF-PAYEE", FieldType.STRING, 
            1);
        pnd_Iatn420x_In_Pnd_New_Inactive_Ind = pnd_Iatn420x_In.newFieldInGroup("pnd_Iatn420x_In_Pnd_New_Inactive_Ind", "#NEW-INACTIVE-IND", FieldType.STRING, 
            1);
        pnd_Iatn420x_In_Pnd_Return_Code = pnd_Iatn420x_In.newFieldInGroup("pnd_Iatn420x_In_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Iatn420x_In_Pnd_Msg = pnd_Iatn420x_In.newFieldInGroup("pnd_Iatn420x_In_Pnd_Msg", "#MSG", FieldType.STRING, 79);

        pnd_Iaa_Xfr_Audit = dbsRecord.newGroupInRecord("pnd_Iaa_Xfr_Audit", "#IAA-XFR-AUDIT");
        pnd_Iaa_Xfr_Audit.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_Xfr_Audit_Rcrd_Type_Cde = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Audit_Rqst_Id = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id", "IAXFR-CALC-UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Effctve_Dte", "IAXFR-EFFCTVE-DTE", FieldType.DATE);
        pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte", "IAXFR-RQST-RCVD-DTE", FieldType.DATE);
        pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme", "IAXFR-RQST-RCVD-TME", FieldType.NUMERIC, 
            7);
        pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte", "IAXFR-INVRSE-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme", "IAXFR-INVRSE-RCVD-TME", 
            FieldType.NUMERIC, 14);
        pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Dte", "IAXFR-ENTRY-DTE", FieldType.DATE);
        pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Tme = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Entry_Tme", "IAXFR-ENTRY-TME", FieldType.NUMERIC, 
            7);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr", "IAXFR-CALC-STTMNT-INDCTR", 
            FieldType.STRING, 1);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde", "IAXFR-CALC-STATUS-CDE", 
            FieldType.STRING, 2);
        pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Wpid = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Wpid", "IAXFR-CWF-WPID", FieldType.STRING, 6);
        pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time", "IAXFR-CWF-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr", "IAXFR-FROM-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Payee_Cde = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Payee_Cde", "IAXFR-FROM-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde", "IAXFR-CALC-REJECT-CDE", 
            FieldType.STRING, 2);
        pnd_Iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind", "IAXFR-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Iaa_Xfr_Audit_Iaxfr_In_Progress_Ind = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_In_Progress_Ind", "IAXFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN);
        pnd_Iaa_Xfr_Audit_Iaxfr_Retry_Cnt = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Retry_Cnt", "IAXFR-RETRY-CNT", FieldType.NUMERIC, 
            1);
        pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_From_Acct_Data = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_From_Acct_Data", "C#IAXFR-CALC-FROM-ACCT-DATA", 
            FieldType.NUMERIC, 3);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data = pnd_Iaa_Xfr_Audit.newGroupInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data", "IAXFR-CALC-FROM-ACCT-DATA");
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Fund_Cde", 
            "IAXFR-FROM-FUND-CDE", FieldType.STRING, 2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd", 
            "IAXFR-FRM-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ", 
            "IAXFR-FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Typ = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Typ", "IAXFR-FROM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Qty = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Qty", "IAXFR-FROM-QTY", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar", 
            "IAXFR-FROM-CURRENT-PMT-GUAR", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid", 
            "IAXFR-FROM-CURRENT-PMT-DIVID", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units", 
            "IAXFR-FROM-CURRENT-PMT-UNITS", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val", 
            "IAXFR-FROM-CURRENT-PMT-UNIT-VAL", FieldType.PACKED_DECIMAL, 9,4, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val", 
            "IAXFR-FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9,4, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt", 
            "IAXFR-FROM-RQSTD-XFR-AMT", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units", 
            "IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar", 
            "IAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid", 
            "IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt", 
            "IAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr", 
            "IAXFR-FROM-PMT-AFTR-XFR", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units", 
            "IAXFR-FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar", 
            "IAXFR-FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid", 
            "IAXFR-FROM-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_To_Acct_Data = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Cpnd_Iaxfr_Calc_To_Acct_Data", "C#IAXFR-CALC-TO-ACCT-DATA", 
            FieldType.NUMERIC, 3);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data = pnd_Iaa_Xfr_Audit.newGroupInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA");
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Fund_Cde", "IAXFR-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Acct_Cd = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Unit_Typ", "IAXFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec", 
            "IAXFR-TO-NEW-FUND-REC", FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue", 
            "IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE", FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Typ = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Typ", "IAXFR-TO-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Qty = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Qty", "IAXFR-TO-QTY", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar", 
            "IAXFR-TO-BFR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid", 
            "IAXFR-TO-BFR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units", 
            "IAXFR-TO-BFR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val", 
            "IAXFR-TO-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9,4, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Units", 
            "IAXFR-TO-XFR-UNITS", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Rate_Cde = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Rate_Cde", "IAXFR-TO-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar", "IAXFR-TO-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid", 
            "IAXFR-TO-XFR-DIVID", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Asset_Amt", 
            "IAXFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units", 
            "IAXFR-TO-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar", 
            "IAXFR-TO-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid = pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid", 
            "IAXFR-TO-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,20));
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr", "IAXFR-CALC-TO-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde", "IAXFR-CALC-TO-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind", "IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND", 
            FieldType.STRING, 1);
        pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte", "IAXFR-CALC-UNIT-VAL-DTE", 
            FieldType.DATE);
        pnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Cycle_Dte", "IAXFR-CYCLE-DTE", FieldType.DATE);
        pnd_Iaa_Xfr_Audit_Iaxfr_Acctng_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Iaxfr_Acctng_Dte", "IAXFR-ACCTNG-DTE", FieldType.DATE);
        pnd_Iaa_Xfr_Audit_Lst_Chnge_Dte = pnd_Iaa_Xfr_Audit.newFieldInGroup("pnd_Iaa_Xfr_Audit_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIatl420x(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

