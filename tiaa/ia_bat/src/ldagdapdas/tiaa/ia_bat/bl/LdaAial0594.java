/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:36 PM
**        * FROM NATURAL LDA     : AIAL0594
************************************************************
**        * FILE NAME            : LdaAial0594.java
**        * CLASS NAME           : LdaAial0594
**        * INSTANCE NAME        : LdaAial0594
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0594 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct_View;
    private DbsField iaa_Cntrct_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_View_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_View_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_View_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Issue_Dte_Dd;

    public DataAccessProgramView getVw_iaa_Cntrct_View() { return vw_iaa_Cntrct_View; }

    public DbsField getIaa_Cntrct_View_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_View_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_View_Cntrct_Optn_Cde() { return iaa_Cntrct_View_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_View_Cntrct_Orgn_Cde() { return iaa_Cntrct_View_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_View_Cntrct_Issue_Dte() { return iaa_Cntrct_View_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_View_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_View_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_View_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_View_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_View_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_View_Cntrct_Issue_Dte_Dd; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_View", "IAA-CNTRCT-VIEW"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_View_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_View_Cntrct_Optn_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_View_Cntrct_Orgn_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_View_Cntrct_Issue_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_View_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_View.getRecord().newGroupInGroup("iaa_Cntrct_View_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_View_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_View_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_View_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_View_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");

        this.setRecordName("LdaAial0594");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct_View.reset();
    }

    // Constructor
    public LdaAial0594() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
