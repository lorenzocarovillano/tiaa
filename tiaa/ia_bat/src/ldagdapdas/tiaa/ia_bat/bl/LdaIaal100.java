/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:13 PM
**        * FROM NATURAL LDA     : IAAL100
************************************************************
**        * FILE NAME            : LdaIaal100.java
**        * CLASS NAME           : LdaIaal100
**        * INSTANCE NAME        : LdaIaal100
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal100 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr;
    private DbsGroup pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Record_Cde;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef2;
    private DbsField pnd_Input_Record_Pnd_W_Check_Date;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Input_Record_Pnd_Filler_1z;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef4;
    private DbsField pnd_Input_Record_Pnd_Filler_1a;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Actvty_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_2a;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Mode_Ind;
    private DbsField pnd_Input_Record_Pnd_Filler_3a;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_Filler_4a;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef5;
    private DbsField pnd_Input_Record_Pnd_Filler_1b;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Per_Amt;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef6;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Fund_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_1ca;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Tot_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Filler_1c;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Units_Cnt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record_2;
    private DbsGroup pnd_Output_Record;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_Tot;
    private DbsGroup pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_TotRedef7;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr;
    private DbsGroup pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Ddctn_Per_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Tot_Pay_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde;
    private DbsField pnd_W_Rec_3;

    public DbsGroup getPnd_Input_Record() { return pnd_Input_Record; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsGroup getPnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Payee_Cde() { return pnd_Input_Record_Pnd_Cntrct_Payee_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Record_Cde() { return pnd_Input_Record_Pnd_Record_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Rest_Of_Record() { return pnd_Input_Record_Pnd_Rest_Of_Record; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef2() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef2; }

    public DbsField getPnd_Input_Record_Pnd_W_Check_Date() { return pnd_Input_Record_Pnd_W_Check_Date; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef3() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef3; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Optn_Cde() { return pnd_Input_Record_Pnd_Cntrct_Optn_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1() { return pnd_Input_Record_Pnd_Filler_1; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Issue_Dte() { return pnd_Input_Record_Pnd_Cntrct_Issue_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1z() { return pnd_Input_Record_Pnd_Filler_1z; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Crrncy_Cde() { return pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef4() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef4; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1a() { return pnd_Input_Record_Pnd_Filler_1a; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde() { return pnd_Input_Record_Pnd_Cntrct_Actvty_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Filler_2a() { return pnd_Input_Record_Pnd_Filler_2a; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Mode_Ind() { return pnd_Input_Record_Pnd_Cntrct_Mode_Ind; }

    public DbsField getPnd_Input_Record_Pnd_Filler_3a() { return pnd_Input_Record_Pnd_Filler_3a; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte() { return pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Filler_4a() { return pnd_Input_Record_Pnd_Filler_4a; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde() { return pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef5() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef5; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1b() { return pnd_Input_Record_Pnd_Filler_1b; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Per_Amt() { return pnd_Input_Record_Pnd_Ddctn_Per_Amt; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef6() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef6; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde() { return pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Fund_Cde() { return pnd_Input_Record_Pnd_Tiaa_Fund_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1ca() { return pnd_Input_Record_Pnd_Filler_1ca; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Tot_Pay_Amt() { return pnd_Input_Record_Pnd_Tiaa_Tot_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt() { return pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1c() { return pnd_Input_Record_Pnd_Filler_1c; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt() { return pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt() { return pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Units_Cnt() { return pnd_Input_Record_Pnd_Tiaa_Units_Cnt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt() { return pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt() { return pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Rest_Of_Record_2() { return pnd_Input_Record_Pnd_Rest_Of_Record_2; }

    public DbsGroup getPnd_Output_Record() { return pnd_Output_Record; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_Tot() { return pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_Tot; }

    public DbsGroup getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_TotRedef7() { return pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_TotRedef7; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde() { return pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Fund_Cde() { return pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr; }

    public DbsGroup getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Payee_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Issue_Dte() { return pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Mode_Ind() { return pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte() { return pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt() { return pnd_Output_Record_Pnd_O_Ddctn_Per_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Tot_Pay_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Tot_Pay_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Tot_Div_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Tot_Div_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Units_Cnt() { return pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Optn_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde; }

    public DbsField getPnd_W_Rec_3() { return pnd_W_Rec_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Input_Record = newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1", "Redefines", pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1 = pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1", 
            "#CNTRCT-PPCN-NBR-1", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9 = pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9", 
            "#CNTRCT-PPCN-NBR-9", FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_Record_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Record_Cde", "#RECORD-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            251);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef2 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef2", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_W_Check_Date = pnd_Input_Record_Pnd_Rest_Of_RecordRedef2.newFieldInGroup("pnd_Input_Record_Pnd_W_Check_Date", "#W-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef3", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Cntrct_Optn_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Filler_1 = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 
            4);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Filler_1z = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1z", "#FILLER-1Z", FieldType.STRING, 
            12);
        pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef4", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Filler_1a = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1a", "#FILLER-1A", FieldType.STRING, 
            36);
        pnd_Input_Record_Pnd_Cntrct_Actvty_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Actvty_Cde", "#CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Filler_2a = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_2a", "#FILLER-2A", FieldType.STRING, 
            160);
        pnd_Input_Record_Pnd_Cntrct_Mode_Ind = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Mode_Ind", "#CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3);
        pnd_Input_Record_Pnd_Filler_3a = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_3a", "#FILLER-3A", FieldType.STRING, 
            12);
        pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte", 
            "#CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Filler_4a = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_4a", "#FILLER-4A", FieldType.STRING, 
            27);
        pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde", 
            "#CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef5 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef5", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Filler_1b = pnd_Input_Record_Pnd_Rest_Of_RecordRedef5.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1b", "#FILLER-1B", FieldType.STRING, 
            23);
        pnd_Input_Record_Pnd_Ddctn_Per_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef5.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", 
            FieldType.NUMERIC, 5);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef6", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde", "#TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Tiaa_Fund_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Fund_Cde", "#TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Filler_1ca = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1ca", "#FILLER-1CA", 
            FieldType.STRING, 10);
        pnd_Input_Record_Pnd_Tiaa_Tot_Pay_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Tot_Pay_Amt", "#TIAA-TOT-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt", "#TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Filler_1c = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1c", "#FILLER-1C", FieldType.STRING, 
            16);
        pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt", "#TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt", "#TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Units_Cnt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Units_Cnt", "#TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9,3);
        pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt", 
            "#TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt", 
            "#TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Rest_Of_Record_2 = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Rest_Of_Record_2", "#REST-OF-RECORD-2", FieldType.STRING, 
            55);

        pnd_Output_Record = newGroupInRecord("pnd_Output_Record", "#OUTPUT-RECORD");
        pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_Tot = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_Tot", "#O-TIAA-CMPNY-CDE-TOT", 
            FieldType.STRING, 3);
        pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_TotRedef7 = pnd_Output_Record.newGroupInGroup("pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_TotRedef7", "Redefines", 
            pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_Tot);
        pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde = pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_TotRedef7.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde", 
            "#O-TIAA-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde = pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde_TotRedef7.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde", 
            "#O-TIAA-FUND-CDE", FieldType.STRING, 2);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr", "#O-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8 = pnd_Output_Record.newGroupInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8", "Redefines", 
            pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3 = pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3", 
            "#O-CNTRCT-PPCN-NBR-1-3", FieldType.STRING, 3);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6 = pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6", 
            "#O-CNTRCT-PPCN-NBR-6", FieldType.STRING, 6);
        pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde", "#O-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde", "#O-CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte", "#O-CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind", "#O-CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte", "#O-CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde", "#O-CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Output_Record_Pnd_O_Ddctn_Per_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Ddctn_Per_Amt", "#O-DDCTN-PER-AMT", FieldType.NUMERIC, 
            5);
        pnd_Output_Record_Pnd_O_Tiaa_Tot_Pay_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Tot_Pay_Amt", "#O-TIAA-TOT-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Tot_Div_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Tot_Div_Amt", "#O-TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt", "#O-TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt", "#O-TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt", "#O-TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt", "#O-TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt", "#O-TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde", "#O-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2);

        pnd_W_Rec_3 = newFieldInRecord("pnd_W_Rec_3", "#W-REC-3", FieldType.NUMERIC, 8);

        this.setRecordName("LdaIaal100");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal100() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
