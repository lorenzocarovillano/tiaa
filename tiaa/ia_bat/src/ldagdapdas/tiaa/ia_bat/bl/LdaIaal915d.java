/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:03:43 PM
**        * FROM NATURAL LDA     : IAAL915D
************************************************************
**        * FILE NAME            : LdaIaal915d.java
**        * CLASS NAME           : LdaIaal915d
**        * INSTANCE NAME        : LdaIaal915d
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal915d extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1;
    private DbsField iaa_Cntrct_Filler1;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Yy;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Mm;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dd;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3;
    private DbsField iaa_Cntrct_Filler2;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsGroup ws_Trans_304_Rcrd;
    private DbsField ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Dte;
    private DbsField ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr;
    private DbsGroup ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_NbrRedef5;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr_8;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Cde;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte;
    private DbsGroup ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Cc;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Yy;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Mm;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Dd;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte;
    private DbsGroup ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Cc;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Yy;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Mm;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Dd;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde;
    private DbsGroup ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_CdeRedef8;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_Cc_Payee;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_User_Area;
    private DbsField ws_Trans_304_Rcrd_Pnd_Trans_User_Id;
    private DbsGroup pnd_Comb_Chk_Trans;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Check_DteRedef9;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy;
    private DbsField pnd_Comb_Chk_Trans_Pnd_User_Area;
    private DbsField pnd_Comb_Chk_Trans_Pnd_User_Id;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef10;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Rcrd_Status;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Intent_Code;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Currency;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_AnntRedef11;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Ssn;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_AnntRedef12;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef13;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Sex;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy;
    private DbsField pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef15;
    private DbsField pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct;
    private DbsField pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef16;
    private DbsField pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef17;
    private DbsField pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct;
    private DbsField pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef18;
    private DbsField pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct;
    private DbsField pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Filler1;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Multiple;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Seq_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Verify;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Filler;
    private DbsField pnd_Iaa_Cntrct_Key;
    private DbsGroup pnd_Iaa_Cntrct_KeyRedef19;
    private DbsField pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key;
    private DbsGroup pnd_Iaa_Cntrct_Prtcpnt_KeyRedef20;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cntrct_Trans_Key;
    private DbsGroup pnd_Iaa_Cntrct_Trans_KeyRedef21;
    private DbsField pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id;
    private DbsField pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte;
    private DbsGroup pnd_Miscellaneous_Variables;
    private DbsField pnd_Miscellaneous_Variables_Pnd_Save_Cntrct_Nbr;
    private DbsField pnd_Miscellaneous_Variables_Pnd_Save_Payee_Cde;
    private DbsField pnd_Miscellaneous_Variables_Pnd_Cmbne_Rcrd;
    private DbsField pnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr;
    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_No_Cntrct_Rec;
    private DbsField pnd_Logical_Variables_Pnd_Rcrd_In_Progress;
    private DbsField pnd_Logical_Variables_Pnd_Trans_Sw;

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Type_Cde() { return iaa_Cntrct_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1() { return iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1; }

    public DbsField getIaa_Cntrct_Filler1() { return iaa_Cntrct_Filler1; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2() { return iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Cc() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Yy() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Mm() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3; }

    public DbsField getIaa_Cntrct_Filler2() { return iaa_Cntrct_Filler2; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Lst_Trans_Dte() { return iaa_Cntrct_Lst_Trans_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct_Prtcpnt_Role() { return vw_iaa_Cntrct_Prtcpnt_Role; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte() { return iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind() { return iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Lst_Trans_Dte() { return iaa_Cntrct_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Invrse_Trans_Dte() { return iaa_Cntrct_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Aftr_Imge_Id() { return iaa_Cntrct_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Bfre_Imge_Id() { return iaa_Cntrct_Trans_Bfre_Imge_Id; }

    public DbsGroup getWs_Trans_304_Rcrd() { return ws_Trans_304_Rcrd; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte() { return ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Dte() { return ws_Trans_304_Rcrd_Pnd_Trans_Dte; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Lst_Trans_Dte() { return ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr() { return ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr; }

    public DbsGroup getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_NbrRedef5() { return ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_NbrRedef5; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr_8() { return ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr_8; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Payee_Cde() { return ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Sub_Cde() { return ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Cde() { return ws_Trans_304_Rcrd_Pnd_Trans_Cde; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde() { return ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte() { return ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte; }

    public DbsGroup getWs_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6() { return ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Cc() { return ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Cc; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Yy() { return ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Yy; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Mm() { return ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Mm; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Dd() { return ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Dd; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte() { return ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte; }

    public DbsGroup getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7() { return ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Cc() { return ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Cc; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Yy() { return ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Yy; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Mm() { return ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Mm; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Dd() { return ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Dd; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde() { return ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde; }

    public DbsGroup getWs_Trans_304_Rcrd_Pnd_Trans_Cmbne_CdeRedef8() { return ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_CdeRedef8; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn() { return ws_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_Cc_Payee() { return ws_Trans_304_Rcrd_Pnd_Trans_Cc_Payee; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_User_Area() { return ws_Trans_304_Rcrd_Pnd_Trans_User_Area; }

    public DbsField getWs_Trans_304_Rcrd_Pnd_Trans_User_Id() { return ws_Trans_304_Rcrd_Pnd_Trans_User_Id; }

    public DbsGroup getPnd_Comb_Chk_Trans() { return pnd_Comb_Chk_Trans; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Check_DteRedef9() { return pnd_Comb_Chk_Trans_Pnd_Check_DteRedef9; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_User_Area() { return pnd_Comb_Chk_Trans_Pnd_User_Area; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_User_Id() { return pnd_Comb_Chk_Trans_Pnd_User_Id; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Trans_DteRedef10() { return pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef10; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_N() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Trans_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status() { return pnd_Comb_Chk_Trans_Pnd_Rcrd_Status; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Intent_Code() { return pnd_Comb_Chk_Trans_Pnd_Intent_Code; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Currency() { return pnd_Comb_Chk_Trans_Pnd_Currency; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt() { return pnd_Comb_Chk_Trans_Pnd_Annt; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_AnntRedef11() { return pnd_Comb_Chk_Trans_Pnd_AnntRedef11; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Ssn() { return pnd_Comb_Chk_Trans_Pnd_Ssn; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_AnntRedef12() { return pnd_Comb_Chk_Trans_Pnd_AnntRedef12; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_A() { return pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef13() { return pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef13; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Sex() { return pnd_Comb_Chk_Trans_Pnd_Annt_Sex; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_A() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef15() { return pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef15; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef16() { return pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef16; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef17() { return pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef17; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef18() { return pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef18; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Filler1() { return pnd_Comb_Chk_Trans_Pnd_Filler1; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Multiple() { return pnd_Comb_Chk_Trans_Pnd_Multiple; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Seq_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Seq_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Verify() { return pnd_Comb_Chk_Trans_Pnd_Verify; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Filler() { return pnd_Comb_Chk_Trans_Pnd_Filler; }

    public DbsField getPnd_Iaa_Cntrct_Key() { return pnd_Iaa_Cntrct_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_KeyRedef19() { return pnd_Iaa_Cntrct_KeyRedef19; }

    public DbsField getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte() { return pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key() { return pnd_Iaa_Cntrct_Prtcpnt_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Prtcpnt_KeyRedef20() { return pnd_Iaa_Cntrct_Prtcpnt_KeyRedef20; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key() { return pnd_Iaa_Cntrct_Trans_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Trans_KeyRedef21() { return pnd_Iaa_Cntrct_Trans_KeyRedef21; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id() { return pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte() { return pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Miscellaneous_Variables() { return pnd_Miscellaneous_Variables; }

    public DbsField getPnd_Miscellaneous_Variables_Pnd_Save_Cntrct_Nbr() { return pnd_Miscellaneous_Variables_Pnd_Save_Cntrct_Nbr; }

    public DbsField getPnd_Miscellaneous_Variables_Pnd_Save_Payee_Cde() { return pnd_Miscellaneous_Variables_Pnd_Save_Payee_Cde; }

    public DbsField getPnd_Miscellaneous_Variables_Pnd_Cmbne_Rcrd() { return pnd_Miscellaneous_Variables_Pnd_Cmbne_Rcrd; }

    public DbsField getPnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr() { return pnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr; }

    public DbsGroup getPnd_Logical_Variables() { return pnd_Logical_Variables; }

    public DbsField getPnd_Logical_Variables_Pnd_No_Cntrct_Rec() { return pnd_Logical_Variables_Pnd_No_Cntrct_Rec; }

    public DbsField getPnd_Logical_Variables_Pnd_Rcrd_In_Progress() { return pnd_Logical_Variables_Pnd_Rcrd_In_Progress; }

    public DbsField getPnd_Logical_Variables_Pnd_Trans_Sw() { return pnd_Logical_Variables_Pnd_Trans_Sw; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1", "Redefines", 
            iaa_Cntrct_Cntrct_First_Annt_Dob_Dte);
        iaa_Cntrct_Filler1 = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Filler1", "FILLER1", FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Yy = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Yy", "CNTRCT-FIRST-ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Mm = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Mm", "CNTRCT-FIRST-ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dd = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dd", "CNTRCT-FIRST-ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2", "Redefines", 
            iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Cc = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Cc", "CNTRCT-FIRST-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Yy = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Yy", "CNTRCT-FIRST-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Mm = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Mm", "CNTRCT-FIRST-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3", "Redefines", 
            iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte);
        iaa_Cntrct_Filler2 = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Filler2", "FILLER2", FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy", "CNTRCT-SCND-ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm", "CNTRCT-SCND-ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd", "CNTRCT-SCND-ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4", "Redefines", 
            iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc", "CNTRCT-SCND-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy", "CNTRCT-SCND-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm", "CNTRCT-SCND-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");

        ws_Trans_304_Rcrd = newGroupInRecord("ws_Trans_304_Rcrd", "WS-TRANS-304-RCRD");
        ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte", "#CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        ws_Trans_304_Rcrd_Pnd_Trans_Dte = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 
            10);
        ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_NbrRedef5 = ws_Trans_304_Rcrd.newGroupInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_NbrRedef5", "Redefines", ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr);
        ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr_8 = ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_NbrRedef5.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr_8", 
            "#TRANS-PPCN-NBR-8", FieldType.STRING, 8);
        ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde", "#TRANS-SUB-CDE", FieldType.STRING, 
            3);
        ws_Trans_304_Rcrd_Pnd_Trans_Cde = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 3);
        ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde", "#TRANS-ACTVTY-CDE", FieldType.STRING, 
            1);
        ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);
        ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6 = ws_Trans_304_Rcrd.newGroupInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6", "Redefines", ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte);
        ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Cc = ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Cc", 
            "#TRANS-CHECK-DTE-CC", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Yy = ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Yy", 
            "#TRANS-CHECK-DTE-YY", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Mm = ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Mm", 
            "#TRANS-CHECK-DTE-MM", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Dd = ws_Trans_304_Rcrd_Pnd_Trans_Check_DteRedef6.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte_Dd", 
            "#TRANS-CHECK-DTE-DD", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 
            8);
        ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7 = ws_Trans_304_Rcrd.newGroupInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7", "Redefines", 
            ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte);
        ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Cc = ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Cc", 
            "#TRANS-EFFCTVE-DTE-CC", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Yy = ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Yy", 
            "#TRANS-EFFCTVE-DTE-YY", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Mm = ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Mm", 
            "#TRANS-EFFCTVE-DTE-MM", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Dd = ws_Trans_304_Rcrd_Pnd_Trans_Effctve_DteRedef7.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte_Dd", 
            "#TRANS-EFFCTVE-DTE-DD", FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde", "#TRANS-CMBNE-CDE", FieldType.STRING, 
            12);
        ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_CdeRedef8 = ws_Trans_304_Rcrd.newGroupInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_CdeRedef8", "Redefines", ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde);
        ws_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn = ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_CdeRedef8.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Cc_Ppcn", "#TRANS-CC-PPCN", 
            FieldType.STRING, 10);
        ws_Trans_304_Rcrd_Pnd_Trans_Cc_Payee = ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_CdeRedef8.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_Cc_Payee", "#TRANS-CC-PAYEE", 
            FieldType.NUMERIC, 2);
        ws_Trans_304_Rcrd_Pnd_Trans_User_Area = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_User_Area", "#TRANS-USER-AREA", FieldType.STRING, 
            6);
        ws_Trans_304_Rcrd_Pnd_Trans_User_Id = ws_Trans_304_Rcrd.newFieldInGroup("ws_Trans_304_Rcrd_Pnd_Trans_User_Id", "#TRANS-USER-ID", FieldType.STRING, 
            8);

        pnd_Comb_Chk_Trans = newGroupInRecord("pnd_Comb_Chk_Trans", "#COMB-CHK-TRANS");
        pnd_Comb_Chk_Trans_Pnd_Check_Dte = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 6);
        pnd_Comb_Chk_Trans_Pnd_Check_DteRedef9 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Check_DteRedef9", "Redefines", pnd_Comb_Chk_Trans_Pnd_Check_Dte);
        pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm = pnd_Comb_Chk_Trans_Pnd_Check_DteRedef9.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd = pnd_Comb_Chk_Trans_Pnd_Check_DteRedef9.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy = pnd_Comb_Chk_Trans_Pnd_Check_DteRedef9.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_User_Area = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 6);
        pnd_Comb_Chk_Trans_Pnd_User_Id = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef10 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef10", "Redefines", pnd_Comb_Chk_Trans_Pnd_Trans_Dte);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N = pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef10.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N", "#TRANS-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Comb_Chk_Trans_Pnd_Trans_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 3);
        pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_Rcrd_Status = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Rcrd_Status", "#RCRD-STATUS", FieldType.STRING, 
            2);
        pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_Comb_Chk_Trans_Pnd_Intent_Code = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr", "#RCRD-NBR", FieldType.STRING, 1);
        pnd_Comb_Chk_Trans_Pnd_Currency = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Currency", "#CURRENCY", FieldType.NUMERIC, 1);
        pnd_Comb_Chk_Trans_Pnd_Annt = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt", "#ANNT", FieldType.STRING, 9);
        pnd_Comb_Chk_Trans_Pnd_AnntRedef11 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_AnntRedef11", "Redefines", pnd_Comb_Chk_Trans_Pnd_Annt);
        pnd_Comb_Chk_Trans_Pnd_Ssn = pnd_Comb_Chk_Trans_Pnd_AnntRedef11.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Comb_Chk_Trans_Pnd_AnntRedef12 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_AnntRedef12", "Redefines", pnd_Comb_Chk_Trans_Pnd_Annt);
        pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A = pnd_Comb_Chk_Trans_Pnd_AnntRedef12.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A", "#ANNT-SEX-A", FieldType.STRING, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef13 = pnd_Comb_Chk_Trans_Pnd_AnntRedef12.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef13", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A);
        pnd_Comb_Chk_Trans_Pnd_Annt_Sex = pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef13.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Sex", "#ANNT-SEX", FieldType.NUMERIC, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A = pnd_Comb_Chk_Trans_Pnd_AnntRedef12.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A", "#ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14 = pnd_Comb_Chk_Trans_Pnd_AnntRedef12.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm = pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm", "#ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd = pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd", "#ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy = pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef14.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy", "#ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct", "#2ND-COMB-CNTRCT", FieldType.STRING, 
            8);
        pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef15 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef15", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct);
        pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct = pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef15.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct", 
            "#5TH-COMB-CNTRCT", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr", "#2ND-STA-NBR", FieldType.STRING, 
            2);
        pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef16 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef16", "Redefines", pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr);
        pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr = pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef16.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr", "#5TH-STA-NBR", 
            FieldType.STRING, 2);
        pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct", "#3RD-COMB-CNTRCT", FieldType.STRING, 
            8);
        pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef17 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef17", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct);
        pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct = pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef17.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct", 
            "#6TH-COMB-CNTRCT", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr", "#3RD-STA-NBR", FieldType.STRING, 
            2);
        pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef18 = pnd_Comb_Chk_Trans.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef18", "Redefines", pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr);
        pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr = pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef18.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr", "#6TH-STA-NBR", 
            FieldType.STRING, 2);
        pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct", "#4TH-COMB-CNTRCT", FieldType.STRING, 
            8);
        pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr", "#4TH-STA-NBR", FieldType.STRING, 
            2);
        pnd_Comb_Chk_Trans_Pnd_Filler1 = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Filler1", "#FILLER1", FieldType.STRING, 18);
        pnd_Comb_Chk_Trans_Pnd_Multiple = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_Comb_Chk_Trans_Pnd_Seq_Nbr = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Comb_Chk_Trans_Pnd_Verify = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Comb_Chk_Trans_Pnd_Filler = pnd_Comb_Chk_Trans.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Filler", "#FILLER", FieldType.STRING, 4);

        pnd_Iaa_Cntrct_Key = newFieldInRecord("pnd_Iaa_Cntrct_Key", "#IAA-CNTRCT-KEY", FieldType.STRING, 17);
        pnd_Iaa_Cntrct_KeyRedef19 = newGroupInRecord("pnd_Iaa_Cntrct_KeyRedef19", "Redefines", pnd_Iaa_Cntrct_Key);
        pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_KeyRedef19.newFieldInGroup("pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte = pnd_Iaa_Cntrct_KeyRedef19.newFieldInGroup("pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.PACKED_DECIMAL, 
            7);

        pnd_Iaa_Cntrct_Prtcpnt_Key = newFieldInRecord("pnd_Iaa_Cntrct_Prtcpnt_Key", "#IAA-CNTRCT-PRTCPNT-KEY", FieldType.STRING, 12);
        pnd_Iaa_Cntrct_Prtcpnt_KeyRedef20 = newGroupInRecord("pnd_Iaa_Cntrct_Prtcpnt_KeyRedef20", "Redefines", pnd_Iaa_Cntrct_Prtcpnt_Key);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef20.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef20.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Iaa_Cntrct_Trans_Key = newFieldInRecord("pnd_Iaa_Cntrct_Trans_Key", "#IAA-CNTRCT-TRANS-KEY", FieldType.STRING, 18);
        pnd_Iaa_Cntrct_Trans_KeyRedef21 = newGroupInRecord("pnd_Iaa_Cntrct_Trans_KeyRedef21", "Redefines", pnd_Iaa_Cntrct_Trans_Key);
        pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id = pnd_Iaa_Cntrct_Trans_KeyRedef21.newFieldInGroup("pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id", "#BFRE-IMGR-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_Trans_KeyRedef21.newFieldInGroup("pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr", 
            "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte = pnd_Iaa_Cntrct_Trans_KeyRedef21.newFieldInGroup("pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte", "#TRANS-DTE", 
            FieldType.TIME);

        pnd_Miscellaneous_Variables = newGroupInRecord("pnd_Miscellaneous_Variables", "#MISCELLANEOUS-VARIABLES");
        pnd_Miscellaneous_Variables_Pnd_Save_Cntrct_Nbr = pnd_Miscellaneous_Variables.newFieldInGroup("pnd_Miscellaneous_Variables_Pnd_Save_Cntrct_Nbr", 
            "#SAVE-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Miscellaneous_Variables_Pnd_Save_Payee_Cde = pnd_Miscellaneous_Variables.newFieldInGroup("pnd_Miscellaneous_Variables_Pnd_Save_Payee_Cde", 
            "#SAVE-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Miscellaneous_Variables_Pnd_Cmbne_Rcrd = pnd_Miscellaneous_Variables.newFieldInGroup("pnd_Miscellaneous_Variables_Pnd_Cmbne_Rcrd", "#CMBNE-RCRD", 
            FieldType.NUMERIC, 1);
        pnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr = pnd_Miscellaneous_Variables.newFieldInGroup("pnd_Miscellaneous_Variables_Pnd_Cmbne_Ctr", "#CMBNE-CTR", 
            FieldType.NUMERIC, 2);

        pnd_Logical_Variables = newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_No_Cntrct_Rec = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_No_Cntrct_Rec", "#NO-CNTRCT-REC", FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Rcrd_In_Progress = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Rcrd_In_Progress", "#RCRD-IN-PROGRESS", 
            FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Trans_Sw = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Trans_Sw", "#TRANS-SW", FieldType.BOOLEAN);

        this.setRecordName("LdaIaal915d");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct_Trans.reset();
        pnd_Comb_Chk_Trans_Pnd_Check_Dte.setInitialValue(0);
        pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr.setInitialValue("1");
    }

    // Constructor
    public LdaIaal915d() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
