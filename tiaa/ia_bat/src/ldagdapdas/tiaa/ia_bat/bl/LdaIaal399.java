/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:01:00 PM
**        * FROM NATURAL LDA     : IAAL399
************************************************************
**        * FILE NAME            : LdaIaal399.java
**        * CLASS NAME           : LdaIaal399
**        * INSTANCE NAME        : LdaIaal399
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal399 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Work_Record_In;
    private DbsField pnd_Work_Record_In_Pnd_W_Record_Contract;
    private DbsField pnd_Work_Record_In_Pnd_W_Record_Payee;
    private DbsField pnd_Work_Record_In_Pnd_W_Record_Code;
    private DbsField pnd_Work_Record_In_Pnd_Rest_Of_Record;
    private DbsGroup pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef1;
    private DbsGroup pnd_Work_Record_In_Pnd_Record_Type_10;
    private DbsField pnd_Work_Record_In_Pnd_W1_Optn_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Orgn_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Acctng_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Issue_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Pymnt_Due_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Pymnt_Pd_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Crrncy_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Type_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Pymnt_Mthd;
    private DbsField pnd_Work_Record_In_Pnd_W1_Pnsn_Pln_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W1_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Work_Record_In_Pnd_W1_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_In_Pnd_W1_First_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Ssn;
    private DbsField pnd_Work_Record_In_Pnd_W1_Div_Payee_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Div_Coll_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Inst_Iss_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Type;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Work_Record_In_Pnd_W1_Filler;
    private DbsField pnd_Work_Record_In_Pnd_W2_Filler;
    private DbsGroup pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef2;
    private DbsGroup pnd_Work_Record_In_Pnd_Record_Type_20;
    private DbsField pnd_Work_Record_In_Pnd_W2_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Work_Record_In_Pnd_W2_Actvty_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Trmnte_Rsn;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rwrttn_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W2_Cash_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Emplymnt_Trmnt_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Company_Cd;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W2_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Resdl_Ivc_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Ivc_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Ivc_Used_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rtb_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rtb_Percent;
    private DbsField pnd_Work_Record_In_Pnd_W2_Mode_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W2_Wthdrwl_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Final_Pay_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Bnfcry_Xref_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W2_Bnfcry_Dod_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Pend_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Hold_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Pend_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Prev_Dist_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Curr_Dist_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Cmbne_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Spirt_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Spirt_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Spirt_Srce;
    private DbsField pnd_Work_Record_In_Pnd_W2_Spirt_Arr_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Spirt_Prcss_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Fed_Tax_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_State_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_State_Tax_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Local_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Local_Tax_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W2_Lst_Chnge_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rllvr_Ivc_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rllvr_Elgble_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W2_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte_N8;
    private DbsField pnd_Work_Record_In_Pnd_W2_Roth_Dsblty_Dte;
    private DbsGroup pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef3;
    private DbsGroup pnd_Work_Record_In_Pnd_Record_Type_30;
    private DbsField pnd_Work_Record_In_Pnd_W3_Cmpny_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W3_Fund_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W3_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Rtb_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Tot_Per_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Tot_Div_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Old_Per_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Old_Div_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Rate_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W3_Rate_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W3_Per_Pay_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Per_Div_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Units_Cnt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W3_Filler;
    private DbsField pnd_Work_Record_In_Pnd_W3_Filler2;
    private DbsField pnd_Work_Record_In_Pnd_W3_Filler3;
    private DbsField pnd_Work_Record_In_Pnd_W3_Filler4;
    private DbsGroup pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef4;
    private DbsGroup pnd_Work_Record_In_Pnd_Record_Type_40;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Id_Nbr;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Seq_Nbr;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Payee;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Per_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Ytd_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Pd_To_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Tot_Amt;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Intent_Cde;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Strt_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Stp_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W4_Ddctn_Final_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_In_Pnd_W4_Filler;
    private DbsGroup pnd_Work_Record_Out;
    private DbsField pnd_Work_Record_Out_Pnd_W_Record_Contract;
    private DbsField pnd_Work_Record_Out_Pnd_W_Record_Payee;
    private DbsField pnd_Work_Record_Out_Pnd_W_Record_Code;
    private DbsField pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out;
    private DbsGroup pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef5;
    private DbsGroup pnd_Work_Record_Out_Pnd_Record_Type_10_Out;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Optn_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Orgn_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Acctng_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Issue_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Due_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Pd_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Crrncy_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Type_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Pymnt_Mthd;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Pnsn_Pln_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_First_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Ssn;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Div_Payee_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Div_Coll_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Inst_Iss_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Type;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Filler;
    private DbsGroup pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef6;
    private DbsGroup pnd_Work_Record_Out_Pnd_Record_Type_20_Out;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Actvty_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Trmnte_Rsn;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rwrttn_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Cash_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Emplymnt_Trmnt_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Company_Cd;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Resdl_Ivc_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Ivc_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Ivc_Used_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rtb_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rtb_Percent;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Mode_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Wthdrwl_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Final_Pay_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Bnfcry_Xref_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Bnfcry_Dod_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Pend_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Hold_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Pend_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Prev_Dist_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Curr_Dist_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Cmbne_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Spirt_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Spirt_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Spirt_Srce;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Spirt_Arr_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Spirt_Prcss_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Fed_Tax_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_State_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_State_Tax_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Local_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Local_Tax_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Lst_Chnge_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rllvr_Ivc_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rllvr_Elgble_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N8;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Roth_Dsblty_Dte;
    private DbsGroup pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef7;
    private DbsGroup pnd_Work_Record_Out_Pnd_Record_Type_30_Out;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Cmpny_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Fund_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Rtb_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Tot_Per_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Tot_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Old_Per_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Old_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Rate_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Rate_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Units_Cnt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Filler;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Filler2;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Filler3;
    private DbsGroup pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef8;
    private DbsGroup pnd_Work_Record_Out_Pnd_Record_Type_40_Out;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Id_Nbr;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Seq_Nbr;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Payee;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Per_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Ytd_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Pd_To_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Tot_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Intent_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Strt_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Stp_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Ddctn_Final_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N;
    private DbsField pnd_Work_Record_Out_Pnd_W4_Filler;

    public DbsGroup getPnd_Work_Record_In() { return pnd_Work_Record_In; }

    public DbsField getPnd_Work_Record_In_Pnd_W_Record_Contract() { return pnd_Work_Record_In_Pnd_W_Record_Contract; }

    public DbsField getPnd_Work_Record_In_Pnd_W_Record_Payee() { return pnd_Work_Record_In_Pnd_W_Record_Payee; }

    public DbsField getPnd_Work_Record_In_Pnd_W_Record_Code() { return pnd_Work_Record_In_Pnd_W_Record_Code; }

    public DbsField getPnd_Work_Record_In_Pnd_Rest_Of_Record() { return pnd_Work_Record_In_Pnd_Rest_Of_Record; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Rest_Of_RecordRedef1() { return pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef1; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Record_Type_10() { return pnd_Work_Record_In_Pnd_Record_Type_10; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Optn_Cde() { return pnd_Work_Record_In_Pnd_W1_Optn_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Orgn_Cde() { return pnd_Work_Record_In_Pnd_W1_Orgn_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Acctng_Cde() { return pnd_Work_Record_In_Pnd_W1_Acctng_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Issue_Dte() { return pnd_Work_Record_In_Pnd_W1_Issue_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Pymnt_Due_Dte() { return pnd_Work_Record_In_Pnd_W1_First_Pymnt_Due_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Pymnt_Pd_Dte() { return pnd_Work_Record_In_Pnd_W1_First_Pymnt_Pd_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Crrncy_Cde() { return pnd_Work_Record_In_Pnd_W1_Crrncy_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Type_Cde() { return pnd_Work_Record_In_Pnd_W1_Type_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Pymnt_Mthd() { return pnd_Work_Record_In_Pnd_W1_Pymnt_Mthd; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Pnsn_Pln_Cde() { return pnd_Work_Record_In_Pnd_W1_Pnsn_Pln_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind() { return pnd_Work_Record_In_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Orig_Da_Cntrct_Nbr() { return pnd_Work_Record_In_Pnd_W1_Orig_Da_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Rsdncy_At_Issue_Cde() { return pnd_Work_Record_In_Pnd_W1_Rsdncy_At_Issue_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Annt_Xref_Ind() { return pnd_Work_Record_In_Pnd_W1_First_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Annt_Dob_Dte() { return pnd_Work_Record_In_Pnd_W1_First_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_In_Pnd_W1_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Annt_Sex_Cde() { return pnd_Work_Record_In_Pnd_W1_First_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Annt_Life_Cnt() { return pnd_Work_Record_In_Pnd_W1_First_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_First_Annt_Dod_Dte() { return pnd_Work_Record_In_Pnd_W1_First_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Scnd_Annt_Xref_Ind() { return pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dob_Dte() { return pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Scnd_Annt_Sex_Cde() { return pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dod_Dte() { return pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Scnd_Annt_Life_Cnt() { return pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Scnd_Annt_Ssn() { return pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Ssn; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Div_Payee_Cde() { return pnd_Work_Record_In_Pnd_W1_Div_Payee_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Div_Coll_Cde() { return pnd_Work_Record_In_Pnd_W1_Div_Coll_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Inst_Iss_Cde() { return pnd_Work_Record_In_Pnd_W1_Inst_Iss_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte() { return pnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Type() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Type; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Mtch_Ppcn() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Mtch_Ppcn; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Due_Dte_Dd() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd() { return pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getPnd_Work_Record_In_Pnd_W1_Filler() { return pnd_Work_Record_In_Pnd_W1_Filler; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Filler() { return pnd_Work_Record_In_Pnd_W2_Filler; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Rest_Of_RecordRedef2() { return pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef2; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Record_Type_20() { return pnd_Work_Record_In_Pnd_Record_Type_20; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Cpr_Id_Nbr() { return pnd_Work_Record_In_Pnd_W2_Cpr_Id_Nbr; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte() { return pnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Prtcpnt_Ctznshp_Cde() { return pnd_Work_Record_In_Pnd_W2_Prtcpnt_Ctznshp_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Cde() { return pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Sw() { return pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Sw; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Nbr() { return pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Typ() { return pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Typ; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Actvty_Cde() { return pnd_Work_Record_In_Pnd_W2_Actvty_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Trmnte_Rsn() { return pnd_Work_Record_In_Pnd_W2_Trmnte_Rsn; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rwrttn_Ind() { return pnd_Work_Record_In_Pnd_W2_Rwrttn_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Cash_Cde() { return pnd_Work_Record_In_Pnd_W2_Cash_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Emplymnt_Trmnt_Cde() { return pnd_Work_Record_In_Pnd_W2_Emplymnt_Trmnt_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Company_Cd() { return pnd_Work_Record_In_Pnd_W2_Company_Cd; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rcvry_Type_Ind() { return pnd_Work_Record_In_Pnd_W2_Rcvry_Type_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Per_Ivc_Amt() { return pnd_Work_Record_In_Pnd_W2_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Resdl_Ivc_Amt() { return pnd_Work_Record_In_Pnd_W2_Resdl_Ivc_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Ivc_Amt() { return pnd_Work_Record_In_Pnd_W2_Ivc_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Ivc_Used_Amt() { return pnd_Work_Record_In_Pnd_W2_Ivc_Used_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rtb_Amt() { return pnd_Work_Record_In_Pnd_W2_Rtb_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rtb_Percent() { return pnd_Work_Record_In_Pnd_W2_Rtb_Percent; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Mode_Ind() { return pnd_Work_Record_In_Pnd_W2_Mode_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Wthdrwl_Dte() { return pnd_Work_Record_In_Pnd_W2_Wthdrwl_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Final_Per_Pay_Dte() { return pnd_Work_Record_In_Pnd_W2_Final_Per_Pay_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Final_Pay_Dte() { return pnd_Work_Record_In_Pnd_W2_Final_Pay_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Bnfcry_Xref_Ind() { return pnd_Work_Record_In_Pnd_W2_Bnfcry_Xref_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Bnfcry_Dod_Dte() { return pnd_Work_Record_In_Pnd_W2_Bnfcry_Dod_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Pend_Cde() { return pnd_Work_Record_In_Pnd_W2_Pend_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Hold_Cde() { return pnd_Work_Record_In_Pnd_W2_Hold_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Pend_Dte() { return pnd_Work_Record_In_Pnd_W2_Pend_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Prev_Dist_Cde() { return pnd_Work_Record_In_Pnd_W2_Prev_Dist_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Curr_Dist_Cde() { return pnd_Work_Record_In_Pnd_W2_Curr_Dist_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Cmbne_Cde() { return pnd_Work_Record_In_Pnd_W2_Cmbne_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Spirt_Cde() { return pnd_Work_Record_In_Pnd_W2_Spirt_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Spirt_Amt() { return pnd_Work_Record_In_Pnd_W2_Spirt_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Spirt_Srce() { return pnd_Work_Record_In_Pnd_W2_Spirt_Srce; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Spirt_Arr_Dte() { return pnd_Work_Record_In_Pnd_W2_Spirt_Arr_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Spirt_Prcss_Dte() { return pnd_Work_Record_In_Pnd_W2_Spirt_Prcss_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Fed_Tax_Amt() { return pnd_Work_Record_In_Pnd_W2_Fed_Tax_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_State_Cde() { return pnd_Work_Record_In_Pnd_W2_State_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_State_Tax_Amt() { return pnd_Work_Record_In_Pnd_W2_State_Tax_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Local_Cde() { return pnd_Work_Record_In_Pnd_W2_Local_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Local_Tax_Amt() { return pnd_Work_Record_In_Pnd_W2_Local_Tax_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Lst_Chnge_Dte() { return pnd_Work_Record_In_Pnd_W2_Lst_Chnge_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Term_Cde() { return pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Term_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Cpr_Lgl_Res_Cde() { return pnd_Work_Record_In_Pnd_W2_Cpr_Lgl_Res_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte() { return pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rllvr_Cntrct_Nbr() { return pnd_Work_Record_In_Pnd_W2_Rllvr_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rllvr_Ivc_Ind() { return pnd_Work_Record_In_Pnd_W2_Rllvr_Ivc_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rllvr_Elgble_Ind() { return pnd_Work_Record_In_Pnd_W2_Rllvr_Elgble_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde() { return pnd_Work_Record_In_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rllvr_Accptng_Irc_Cde() { return pnd_Work_Record_In_Pnd_W2_Rllvr_Accptng_Irc_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Rllvr_Pln_Admn_Ind() { return pnd_Work_Record_In_Pnd_W2_Rllvr_Pln_Admn_Ind; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte_N8() { return pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte_N8; }

    public DbsField getPnd_Work_Record_In_Pnd_W2_Roth_Dsblty_Dte() { return pnd_Work_Record_In_Pnd_W2_Roth_Dsblty_Dte; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Rest_Of_RecordRedef3() { return pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef3; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Record_Type_30() { return pnd_Work_Record_In_Pnd_Record_Type_30; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Cmpny_Cde() { return pnd_Work_Record_In_Pnd_W3_Cmpny_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Fund_Cde() { return pnd_Work_Record_In_Pnd_W3_Fund_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Per_Ivc_Amt() { return pnd_Work_Record_In_Pnd_W3_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Rtb_Amt() { return pnd_Work_Record_In_Pnd_W3_Rtb_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Tot_Per_Amt() { return pnd_Work_Record_In_Pnd_W3_Tot_Per_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Tot_Div_Amt() { return pnd_Work_Record_In_Pnd_W3_Tot_Div_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Old_Per_Amt() { return pnd_Work_Record_In_Pnd_W3_Old_Per_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Old_Div_Amt() { return pnd_Work_Record_In_Pnd_W3_Old_Div_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Rate_Cde() { return pnd_Work_Record_In_Pnd_W3_Rate_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Rate_Dte() { return pnd_Work_Record_In_Pnd_W3_Rate_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Per_Pay_Amt() { return pnd_Work_Record_In_Pnd_W3_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Per_Div_Amt() { return pnd_Work_Record_In_Pnd_W3_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Units_Cnt() { return pnd_Work_Record_In_Pnd_W3_Units_Cnt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Rate_Final_Pay_Amt() { return pnd_Work_Record_In_Pnd_W3_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Rate_Final_Div_Amt() { return pnd_Work_Record_In_Pnd_W3_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte() { return pnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Filler() { return pnd_Work_Record_In_Pnd_W3_Filler; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Filler2() { return pnd_Work_Record_In_Pnd_W3_Filler2; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Filler3() { return pnd_Work_Record_In_Pnd_W3_Filler3; }

    public DbsField getPnd_Work_Record_In_Pnd_W3_Filler4() { return pnd_Work_Record_In_Pnd_W3_Filler4; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Rest_Of_RecordRedef4() { return pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef4; }

    public DbsGroup getPnd_Work_Record_In_Pnd_Record_Type_40() { return pnd_Work_Record_In_Pnd_Record_Type_40; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Id_Nbr() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Id_Nbr; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Cde() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Seq_Nbr() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Seq_Nbr; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Payee() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Payee; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Per_Amt() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Per_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Ytd_Amt() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Ytd_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Pd_To_Dte() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Pd_To_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Tot_Amt() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Tot_Amt; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Intent_Cde() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Intent_Cde; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Strt_Dte() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Strt_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Stp_Dte() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Stp_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Ddctn_Final_Dte() { return pnd_Work_Record_In_Pnd_W4_Ddctn_Final_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte() { return pnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_In_Pnd_W4_Filler() { return pnd_Work_Record_In_Pnd_W4_Filler; }

    public DbsGroup getPnd_Work_Record_Out() { return pnd_Work_Record_Out; }

    public DbsField getPnd_Work_Record_Out_Pnd_W_Record_Contract() { return pnd_Work_Record_Out_Pnd_W_Record_Contract; }

    public DbsField getPnd_Work_Record_Out_Pnd_W_Record_Payee() { return pnd_Work_Record_Out_Pnd_W_Record_Payee; }

    public DbsField getPnd_Work_Record_Out_Pnd_W_Record_Code() { return pnd_Work_Record_Out_Pnd_W_Record_Code; }

    public DbsField getPnd_Work_Record_Out_Pnd_Rest_Of_Record_Out() { return pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef5() { return pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef5; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Record_Type_10_Out() { return pnd_Work_Record_Out_Pnd_Record_Type_10_Out; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Optn_Cde() { return pnd_Work_Record_Out_Pnd_W1_Optn_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Orgn_Cde() { return pnd_Work_Record_Out_Pnd_W1_Orgn_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Acctng_Cde() { return pnd_Work_Record_Out_Pnd_W1_Acctng_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Issue_Dte() { return pnd_Work_Record_Out_Pnd_W1_Issue_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Pymnt_Due_Dte() { return pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Due_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Pymnt_Pd_Dte() { return pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Pd_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Crrncy_Cde() { return pnd_Work_Record_Out_Pnd_W1_Crrncy_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Type_Cde() { return pnd_Work_Record_Out_Pnd_W1_Type_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Pymnt_Mthd() { return pnd_Work_Record_Out_Pnd_W1_Pymnt_Mthd; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Pnsn_Pln_Cde() { return pnd_Work_Record_Out_Pnd_W1_Pnsn_Pln_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind() { return pnd_Work_Record_Out_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Orig_Da_Cntrct_Nbr() { return pnd_Work_Record_Out_Pnd_W1_Orig_Da_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Rsdncy_At_Issue_Cde() { return pnd_Work_Record_Out_Pnd_W1_Rsdncy_At_Issue_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Annt_Xref_Ind() { return pnd_Work_Record_Out_Pnd_W1_First_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Annt_Dob_Dte() { return pnd_Work_Record_Out_Pnd_W1_First_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_Out_Pnd_W1_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Annt_Sex_Cde() { return pnd_Work_Record_Out_Pnd_W1_First_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Annt_Life_Cnt() { return pnd_Work_Record_Out_Pnd_W1_First_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_First_Annt_Dod_Dte() { return pnd_Work_Record_Out_Pnd_W1_First_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Xref_Ind() { return pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dob_Dte() { return pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Sex_Cde() { return pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dod_Dte() { return pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Life_Cnt() { return pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Ssn() { return pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Ssn; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Div_Payee_Cde() { return pnd_Work_Record_Out_Pnd_W1_Div_Payee_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Div_Coll_Cde() { return pnd_Work_Record_Out_Pnd_W1_Div_Coll_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Inst_Iss_Cde() { return pnd_Work_Record_Out_Pnd_W1_Inst_Iss_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N() { return pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Type() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Type; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Mtch_Ppcn() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Mtch_Ppcn; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Issue_Dte_Dd() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Issue_Dte_Dd; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Due_Dte_Dd() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Filler() { return pnd_Work_Record_Out_Pnd_W1_Filler; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef6() { return pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef6; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Record_Type_20_Out() { return pnd_Work_Record_Out_Pnd_Record_Type_20_Out; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Cpr_Id_Nbr() { return pnd_Work_Record_Out_Pnd_W2_Cpr_Id_Nbr; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N() { return pnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Prtcpnt_Ctznshp_Cde() { return pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Ctznshp_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Cde() { return pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Sw() { return pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Sw; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Nbr() { return pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Typ() { return pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Typ; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Actvty_Cde() { return pnd_Work_Record_Out_Pnd_W2_Actvty_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Trmnte_Rsn() { return pnd_Work_Record_Out_Pnd_W2_Trmnte_Rsn; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rwrttn_Ind() { return pnd_Work_Record_Out_Pnd_W2_Rwrttn_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Cash_Cde() { return pnd_Work_Record_Out_Pnd_W2_Cash_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Emplymnt_Trmnt_Cde() { return pnd_Work_Record_Out_Pnd_W2_Emplymnt_Trmnt_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Company_Cd() { return pnd_Work_Record_Out_Pnd_W2_Company_Cd; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rcvry_Type_Ind() { return pnd_Work_Record_Out_Pnd_W2_Rcvry_Type_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Per_Ivc_Amt() { return pnd_Work_Record_Out_Pnd_W2_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Resdl_Ivc_Amt() { return pnd_Work_Record_Out_Pnd_W2_Resdl_Ivc_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Ivc_Amt() { return pnd_Work_Record_Out_Pnd_W2_Ivc_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Ivc_Used_Amt() { return pnd_Work_Record_Out_Pnd_W2_Ivc_Used_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rtb_Amt() { return pnd_Work_Record_Out_Pnd_W2_Rtb_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rtb_Percent() { return pnd_Work_Record_Out_Pnd_W2_Rtb_Percent; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Mode_Ind() { return pnd_Work_Record_Out_Pnd_W2_Mode_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Wthdrwl_Dte() { return pnd_Work_Record_Out_Pnd_W2_Wthdrwl_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Final_Per_Pay_Dte() { return pnd_Work_Record_Out_Pnd_W2_Final_Per_Pay_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Final_Pay_Dte() { return pnd_Work_Record_Out_Pnd_W2_Final_Pay_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Bnfcry_Xref_Ind() { return pnd_Work_Record_Out_Pnd_W2_Bnfcry_Xref_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Bnfcry_Dod_Dte() { return pnd_Work_Record_Out_Pnd_W2_Bnfcry_Dod_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Pend_Cde() { return pnd_Work_Record_Out_Pnd_W2_Pend_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Hold_Cde() { return pnd_Work_Record_Out_Pnd_W2_Hold_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Pend_Dte() { return pnd_Work_Record_Out_Pnd_W2_Pend_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Prev_Dist_Cde() { return pnd_Work_Record_Out_Pnd_W2_Prev_Dist_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Curr_Dist_Cde() { return pnd_Work_Record_Out_Pnd_W2_Curr_Dist_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Cmbne_Cde() { return pnd_Work_Record_Out_Pnd_W2_Cmbne_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Spirt_Cde() { return pnd_Work_Record_Out_Pnd_W2_Spirt_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Spirt_Amt() { return pnd_Work_Record_Out_Pnd_W2_Spirt_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Spirt_Srce() { return pnd_Work_Record_Out_Pnd_W2_Spirt_Srce; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Spirt_Arr_Dte() { return pnd_Work_Record_Out_Pnd_W2_Spirt_Arr_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Spirt_Prcss_Dte() { return pnd_Work_Record_Out_Pnd_W2_Spirt_Prcss_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Fed_Tax_Amt() { return pnd_Work_Record_Out_Pnd_W2_Fed_Tax_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_State_Cde() { return pnd_Work_Record_Out_Pnd_W2_State_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_State_Tax_Amt() { return pnd_Work_Record_Out_Pnd_W2_State_Tax_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Local_Cde() { return pnd_Work_Record_Out_Pnd_W2_Local_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Local_Tax_Amt() { return pnd_Work_Record_Out_Pnd_W2_Local_Tax_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Lst_Chnge_Dte() { return pnd_Work_Record_Out_Pnd_W2_Lst_Chnge_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Term_Cde() { return pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Term_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Cpr_Lgl_Res_Cde() { return pnd_Work_Record_Out_Pnd_W2_Cpr_Lgl_Res_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N() { return pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rllvr_Cntrct_Nbr() { return pnd_Work_Record_Out_Pnd_W2_Rllvr_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rllvr_Ivc_Ind() { return pnd_Work_Record_Out_Pnd_W2_Rllvr_Ivc_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rllvr_Elgble_Ind() { return pnd_Work_Record_Out_Pnd_W2_Rllvr_Elgble_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde() { return pnd_Work_Record_Out_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rllvr_Accptng_Irc_Cde() { return pnd_Work_Record_Out_Pnd_W2_Rllvr_Accptng_Irc_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rllvr_Pln_Admn_Ind() { return pnd_Work_Record_Out_Pnd_W2_Rllvr_Pln_Admn_Ind; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N8() { return pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N8; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Roth_Dsblty_Dte() { return pnd_Work_Record_Out_Pnd_W2_Roth_Dsblty_Dte; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef7() { return pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef7; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Record_Type_30_Out() { return pnd_Work_Record_Out_Pnd_Record_Type_30_Out; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Cmpny_Cde() { return pnd_Work_Record_Out_Pnd_W3_Cmpny_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Fund_Cde() { return pnd_Work_Record_Out_Pnd_W3_Fund_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Per_Ivc_Amt() { return pnd_Work_Record_Out_Pnd_W3_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Rtb_Amt() { return pnd_Work_Record_Out_Pnd_W3_Rtb_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Tot_Per_Amt() { return pnd_Work_Record_Out_Pnd_W3_Tot_Per_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Tot_Div_Amt() { return pnd_Work_Record_Out_Pnd_W3_Tot_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Old_Per_Amt() { return pnd_Work_Record_Out_Pnd_W3_Old_Per_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Old_Div_Amt() { return pnd_Work_Record_Out_Pnd_W3_Old_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Rate_Cde() { return pnd_Work_Record_Out_Pnd_W3_Rate_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Rate_Dte() { return pnd_Work_Record_Out_Pnd_W3_Rate_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Per_Div_Amt() { return pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Units_Cnt() { return pnd_Work_Record_Out_Pnd_W3_Units_Cnt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt() { return pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N() { return pnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Filler() { return pnd_Work_Record_Out_Pnd_W3_Filler; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Filler2() { return pnd_Work_Record_Out_Pnd_W3_Filler2; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Filler3() { return pnd_Work_Record_Out_Pnd_W3_Filler3; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef8() { return pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef8; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_Record_Type_40_Out() { return pnd_Work_Record_Out_Pnd_Record_Type_40_Out; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Id_Nbr() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Id_Nbr; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Cde() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Seq_Nbr() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Seq_Nbr; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Payee() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Payee; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Per_Amt() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Per_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Ytd_Amt() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Ytd_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Pd_To_Dte() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Pd_To_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Tot_Amt() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Tot_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Intent_Cde() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Intent_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Strt_Dte() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Strt_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Stp_Dte() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Stp_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Ddctn_Final_Dte() { return pnd_Work_Record_Out_Pnd_W4_Ddctn_Final_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N() { return pnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N; }

    public DbsField getPnd_Work_Record_Out_Pnd_W4_Filler() { return pnd_Work_Record_Out_Pnd_W4_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Work_Record_In = newGroupInRecord("pnd_Work_Record_In", "#WORK-RECORD-IN");
        pnd_Work_Record_In_Pnd_W_Record_Contract = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_W_Record_Contract", "#W-RECORD-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Work_Record_In_Pnd_W_Record_Payee = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_W_Record_Payee", "#W-RECORD-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_In_Pnd_W_Record_Code = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_W_Record_Code", "#W-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_In_Pnd_Rest_Of_Record = pnd_Work_Record_In.newFieldInGroup("pnd_Work_Record_In_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            355);
        pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef1 = pnd_Work_Record_In.newGroupInGroup("pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef1", "Redefines", pnd_Work_Record_In_Pnd_Rest_Of_Record);
        pnd_Work_Record_In_Pnd_Record_Type_10 = pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef1.newGroupInGroup("pnd_Work_Record_In_Pnd_Record_Type_10", "#RECORD-TYPE-10");
        pnd_Work_Record_In_Pnd_W1_Optn_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Optn_Cde", "#W1-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_In_Pnd_W1_Orgn_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_In_Pnd_W1_Acctng_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Acctng_Cde", "#W1-ACCTNG-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_In_Pnd_W1_Issue_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Issue_Dte", "#W1-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W1_First_Pymnt_Due_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Pymnt_Due_Dte", 
            "#W1-FIRST-PYMNT-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W1_First_Pymnt_Pd_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Pymnt_Pd_Dte", 
            "#W1-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W1_Crrncy_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Crrncy_Cde", "#W1-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W1_Type_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Type_Cde", "#W1-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W1_Pymnt_Mthd = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Pymnt_Mthd", "#W1-PYMNT-MTHD", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W1_Pnsn_Pln_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Pnsn_Pln_Cde", "#W1-PNSN-PLN-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind", 
            "#W1-JOINT-CNVRT-RCRCD-IND", FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W1_Orig_Da_Cntrct_Nbr = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Orig_Da_Cntrct_Nbr", 
            "#W1-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Work_Record_In_Pnd_W1_Rsdncy_At_Issue_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Rsdncy_At_Issue_Cde", 
            "#W1-RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 3);
        pnd_Work_Record_In_Pnd_W1_First_Annt_Xref_Ind = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Annt_Xref_Ind", 
            "#W1-FIRST-ANNT-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_In_Pnd_W1_First_Annt_Dob_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Annt_Dob_Dte", 
            "#W1-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_W1_First_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Annt_Mrtlty_Yob_Dte", 
            "#W1-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_In_Pnd_W1_First_Annt_Sex_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Annt_Sex_Cde", 
            "#W1-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W1_First_Annt_Life_Cnt = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Annt_Life_Cnt", 
            "#W1-FIRST-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W1_First_Annt_Dod_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_First_Annt_Dod_Dte", 
            "#W1-FIRST-ANNT-DOD-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Xref_Ind = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Xref_Ind", 
            "#W1-SCND-ANNT-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dob_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dob_Dte", 
            "#W1-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte", 
            "#W1-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Sex_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Sex_Cde", 
            "#W1-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dod_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Dod_Dte", 
            "#W1-SCND-ANNT-DOD-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Life_Cnt = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Life_Cnt", 
            "#W1-SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Ssn = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Scnd_Annt_Ssn", "#W1-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record_In_Pnd_W1_Div_Payee_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Div_Payee_Cde", "#W1-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W1_Div_Coll_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Div_Coll_Cde", "#W1-DIV-COLL-CDE", 
            FieldType.STRING, 5);
        pnd_Work_Record_In_Pnd_W1_Inst_Iss_Cde = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Inst_Iss_Cde", "#W1-INST-ISS-CDE", 
            FieldType.STRING, 5);
        pnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte", "#W1-LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Work_Record_In_Pnd_W1_Cntrct_Type = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Type", "#W1-CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re", 
            "#W1-CNTRCT-RSDNCY-AT-ISS-RE", FieldType.STRING, 3);
        pnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte", 
            "#W1-CNTRCT-FNL-PRM-DTE", FieldType.DATE, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W1_Cntrct_Mtch_Ppcn = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Mtch_Ppcn", 
            "#W1-CNTRCT-MTCH-PPCN", FieldType.STRING, 10);
        pnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte", 
            "#W1-CNTRCT-ANNTY-STRT-DTE", FieldType.DATE);
        pnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd", 
            "#W1-CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", 
            "#W1-CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", 
            "#W1-CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_In_Pnd_W1_Filler = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W1_Filler", "#W1-FILLER", FieldType.STRING, 
            167);
        pnd_Work_Record_In_Pnd_W2_Filler = pnd_Work_Record_In_Pnd_Record_Type_10.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Filler", "#W2-FILLER", FieldType.STRING, 
            5);
        pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef2 = pnd_Work_Record_In.newGroupInGroup("pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef2", "Redefines", pnd_Work_Record_In_Pnd_Rest_Of_Record);
        pnd_Work_Record_In_Pnd_Record_Type_20 = pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef2.newGroupInGroup("pnd_Work_Record_In_Pnd_Record_Type_20", "#RECORD-TYPE-20");
        pnd_Work_Record_In_Pnd_W2_Cpr_Id_Nbr = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Cpr_Id_Nbr", "#W2-CPR-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte", "#W2-LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Work_Record_In_Pnd_W2_Prtcpnt_Ctznshp_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Prtcpnt_Ctznshp_Cde", 
            "#W2-PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Cde", 
            "#W2-PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Sw = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Prtcpnt_Rsdncy_Sw", 
            "#W2-PRTCPNT-RSDNCY-SW", FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Nbr = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Nbr", 
            "#W2-PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Typ = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Prtcpnt_Tax_Id_Typ", 
            "#W2-PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Actvty_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Actvty_Cde", "#W2-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W2_Trmnte_Rsn = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Trmnte_Rsn", "#W2-TRMNTE-RSN", 
            FieldType.STRING, 2);
        pnd_Work_Record_In_Pnd_W2_Rwrttn_Ind = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Rwrttn_Ind", "#W2-RWRTTN-IND", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W2_Cash_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Cash_Cde", "#W2-CASH-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Emplymnt_Trmnt_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Emplymnt_Trmnt_Cde", 
            "#W2-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Company_Cd = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Company_Cd", "#W2-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Rcvry_Type_Ind = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Rcvry_Type_Ind", 
            "#W2-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Per_Ivc_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Per_Ivc_Amt", "#W2-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Resdl_Ivc_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Resdl_Ivc_Amt", 
            "#W2-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Ivc_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Ivc_Amt", "#W2-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Ivc_Used_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Ivc_Used_Amt", 
            "#W2-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Rtb_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Rtb_Amt", "#W2-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Rtb_Percent = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Rtb_Percent", "#W2-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7,4, new DbsArrayController(1,5));
        pnd_Work_Record_In_Pnd_W2_Mode_Ind = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Mode_Ind", "#W2-MODE-IND", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_In_Pnd_W2_Wthdrwl_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Wthdrwl_Dte", "#W2-WTHDRWL-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W2_Final_Per_Pay_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Final_Per_Pay_Dte", 
            "#W2-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W2_Final_Pay_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Final_Pay_Dte", "#W2-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_W2_Bnfcry_Xref_Ind = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Bnfcry_Xref_Ind", 
            "#W2-BNFCRY-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_In_Pnd_W2_Bnfcry_Dod_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Bnfcry_Dod_Dte", "#W2-BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W2_Pend_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Pend_Cde", "#W2-PEND-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Hold_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Hold_Cde", "#W2-HOLD-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Pend_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Pend_Dte", "#W2-PEND-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W2_Prev_Dist_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Prev_Dist_Cde", "#W2-PREV-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Work_Record_In_Pnd_W2_Curr_Dist_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Curr_Dist_Cde", "#W2-CURR-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Work_Record_In_Pnd_W2_Cmbne_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Cmbne_Cde", "#W2-CMBNE-CDE", 
            FieldType.STRING, 12);
        pnd_Work_Record_In_Pnd_W2_Spirt_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Spirt_Cde", "#W2-SPIRT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Spirt_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Spirt_Amt", "#W2-SPIRT-AMT", 
            FieldType.PACKED_DECIMAL, 7,2);
        pnd_Work_Record_In_Pnd_W2_Spirt_Srce = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Spirt_Srce", "#W2-SPIRT-SRCE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Spirt_Arr_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Spirt_Arr_Dte", "#W2-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_In_Pnd_W2_Spirt_Prcss_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Spirt_Prcss_Dte", 
            "#W2-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W2_Fed_Tax_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Fed_Tax_Amt", "#W2-FED-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W2_State_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_State_Cde", "#W2-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_In_Pnd_W2_State_Tax_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_State_Tax_Amt", "#W2-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W2_Local_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Local_Cde", "#W2-LOCAL-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_In_Pnd_W2_Local_Tax_Amt = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Local_Tax_Amt", "#W2-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W2_Lst_Chnge_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Lst_Chnge_Dte", "#W2-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Term_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Term_Cde", 
            "#W2-CPR-XFR-TERM-CDE", FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Cpr_Lgl_Res_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Cpr_Lgl_Res_Cde", 
            "#W2-CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte", 
            "#W2-CPR-XFR-ISS-DTE", FieldType.DATE);
        pnd_Work_Record_In_Pnd_W2_Rllvr_Cntrct_Nbr = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Rllvr_Cntrct_Nbr", 
            "#W2-RLLVR-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Work_Record_In_Pnd_W2_Rllvr_Ivc_Ind = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Rllvr_Ivc_Ind", "#W2-RLLVR-IVC-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Rllvr_Elgble_Ind = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Rllvr_Elgble_Ind", 
            "#W2-RLLVR-ELGBLE-IND", FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldArrayInGroup("pnd_Work_Record_In_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde", 
            "#W2-RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4));
        pnd_Work_Record_In_Pnd_W2_Rllvr_Accptng_Irc_Cde = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Rllvr_Accptng_Irc_Cde", 
            "#W2-RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2);
        pnd_Work_Record_In_Pnd_W2_Rllvr_Pln_Admn_Ind = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Rllvr_Pln_Admn_Ind", 
            "#W2-RLLVR-PLN-ADMN-IND", FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte_N8 = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte_N8", 
            "#W2-CPR-XFR-ISS-DTE-N8", FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_W2_Roth_Dsblty_Dte = pnd_Work_Record_In_Pnd_Record_Type_20.newFieldInGroup("pnd_Work_Record_In_Pnd_W2_Roth_Dsblty_Dte", 
            "#W2-ROTH-DSBLTY-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef3 = pnd_Work_Record_In.newGroupInGroup("pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef3", "Redefines", pnd_Work_Record_In_Pnd_Rest_Of_Record);
        pnd_Work_Record_In_Pnd_Record_Type_30 = pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef3.newGroupInGroup("pnd_Work_Record_In_Pnd_Record_Type_30", "#RECORD-TYPE-30");
        pnd_Work_Record_In_Pnd_W3_Cmpny_Cde = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Cmpny_Cde", "#W3-CMPNY-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_In_Pnd_W3_Fund_Cde = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Fund_Cde", "#W3-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_In_Pnd_W3_Per_Ivc_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Per_Ivc_Amt", "#W3-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Rtb_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Rtb_Amt", "#W3-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Tot_Per_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Tot_Per_Amt", "#W3-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Tot_Div_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Tot_Div_Amt", "#W3-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Old_Per_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Old_Per_Amt", "#W3-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Old_Div_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Old_Div_Amt", "#W3-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Rate_Cde = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Rate_Cde", "#W3-RATE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_In_Pnd_W3_Rate_Dte = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Rate_Dte", "#W3-RATE-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_In_Pnd_W3_Per_Pay_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Per_Pay_Amt", "#W3-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Per_Div_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Per_Div_Amt", "#W3-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Units_Cnt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Units_Cnt", "#W3-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9,3);
        pnd_Work_Record_In_Pnd_W3_Rate_Final_Pay_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Rate_Final_Pay_Amt", 
            "#W3-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Rate_Final_Div_Amt = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Rate_Final_Div_Amt", 
            "#W3-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte", "#W3-LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Work_Record_In_Pnd_W3_Filler = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Filler", "#W3-FILLER", FieldType.STRING, 
            231);
        pnd_Work_Record_In_Pnd_W3_Filler2 = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Filler2", "#W3-FILLER2", 
            FieldType.STRING, 31);
        pnd_Work_Record_In_Pnd_W3_Filler3 = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Filler3", "#W3-FILLER3", 
            FieldType.STRING, 16);
        pnd_Work_Record_In_Pnd_W3_Filler4 = pnd_Work_Record_In_Pnd_Record_Type_30.newFieldInGroup("pnd_Work_Record_In_Pnd_W3_Filler4", "#W3-FILLER4", 
            FieldType.STRING, 5);
        pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef4 = pnd_Work_Record_In.newGroupInGroup("pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef4", "Redefines", pnd_Work_Record_In_Pnd_Rest_Of_Record);
        pnd_Work_Record_In_Pnd_Record_Type_40 = pnd_Work_Record_In_Pnd_Rest_Of_RecordRedef4.newGroupInGroup("pnd_Work_Record_In_Pnd_Record_Type_40", "#RECORD-TYPE-40");
        pnd_Work_Record_In_Pnd_W4_Ddctn_Id_Nbr = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Id_Nbr", "#W4-DDCTN-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Cde = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Cde", "#W4-DDCTN-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Seq_Nbr = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Seq_Nbr", "#W4-DDCTN-SEQ-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Payee = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Payee", "#W4-DDCTN-PAYEE", 
            FieldType.STRING, 5);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Per_Amt = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Per_Amt", "#W4-DDCTN-PER-AMT", 
            FieldType.DECIMAL, 7,2);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Ytd_Amt = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Ytd_Amt", "#W4-DDCTN-YTD-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Pd_To_Dte = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Pd_To_Dte", 
            "#W4-DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Tot_Amt = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Tot_Amt", "#W4-DDCTN-TOT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Intent_Cde = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Intent_Cde", 
            "#W4-DDCTN-INTENT-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Strt_Dte = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Strt_Dte", "#W4-DDCTN-STRT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Stp_Dte = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Stp_Dte", "#W4-DDCTN-STP-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_W4_Ddctn_Final_Dte = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Ddctn_Final_Dte", 
            "#W4-DDCTN-FINAL-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte", "#W4-LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Work_Record_In_Pnd_W4_Filler = pnd_Work_Record_In_Pnd_Record_Type_40.newFieldInGroup("pnd_Work_Record_In_Pnd_W4_Filler", "#W4-FILLER", FieldType.STRING, 
            270);

        pnd_Work_Record_Out = newGroupInRecord("pnd_Work_Record_Out", "#WORK-RECORD-OUT");
        pnd_Work_Record_Out_Pnd_W_Record_Contract = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W_Record_Contract", "#W-RECORD-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Work_Record_Out_Pnd_W_Record_Payee = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W_Record_Payee", "#W-RECORD-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Out_Pnd_W_Record_Code = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W_Record_Code", "#W-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out", "#REST-OF-RECORD-OUT", 
            FieldType.STRING, 358);
        pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef5 = pnd_Work_Record_Out.newGroupInGroup("pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef5", "Redefines", 
            pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out);
        pnd_Work_Record_Out_Pnd_Record_Type_10_Out = pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef5.newGroupInGroup("pnd_Work_Record_Out_Pnd_Record_Type_10_Out", 
            "#RECORD-TYPE-10-OUT");
        pnd_Work_Record_Out_Pnd_W1_Optn_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Optn_Cde", "#W1-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Out_Pnd_W1_Orgn_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Out_Pnd_W1_Acctng_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Acctng_Cde", "#W1-ACCTNG-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_Out_Pnd_W1_Issue_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Issue_Dte", "#W1-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Due_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Due_Dte", 
            "#W1-FIRST-PYMNT-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Pd_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Pymnt_Pd_Dte", 
            "#W1-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W1_Crrncy_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Crrncy_Cde", "#W1-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W1_Type_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Type_Cde", "#W1-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W1_Pymnt_Mthd = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Pymnt_Mthd", "#W1-PYMNT-MTHD", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W1_Pnsn_Pln_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Pnsn_Pln_Cde", 
            "#W1-PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind", 
            "#W1-JOINT-CNVRT-RCRCD-IND", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W1_Orig_Da_Cntrct_Nbr = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Orig_Da_Cntrct_Nbr", 
            "#W1-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Work_Record_Out_Pnd_W1_Rsdncy_At_Issue_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Rsdncy_At_Issue_Cde", 
            "#W1-RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 3);
        pnd_Work_Record_Out_Pnd_W1_First_Annt_Xref_Ind = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Annt_Xref_Ind", 
            "#W1-FIRST-ANNT-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_Out_Pnd_W1_First_Annt_Dob_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Annt_Dob_Dte", 
            "#W1-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W1_First_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Annt_Mrtlty_Yob_Dte", 
            "#W1-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_Out_Pnd_W1_First_Annt_Sex_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Annt_Sex_Cde", 
            "#W1-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W1_First_Annt_Life_Cnt = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Annt_Life_Cnt", 
            "#W1-FIRST-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W1_First_Annt_Dod_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_First_Annt_Dod_Dte", 
            "#W1-FIRST-ANNT-DOD-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Xref_Ind = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Xref_Ind", 
            "#W1-SCND-ANNT-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dob_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dob_Dte", 
            "#W1-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte", 
            "#W1-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Sex_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Sex_Cde", 
            "#W1-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dod_Dte = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Dod_Dte", 
            "#W1-SCND-ANNT-DOD-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Life_Cnt = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Life_Cnt", 
            "#W1-SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Ssn = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Scnd_Annt_Ssn", 
            "#W1-SCND-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Work_Record_Out_Pnd_W1_Div_Payee_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Div_Payee_Cde", 
            "#W1-DIV-PAYEE-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W1_Div_Coll_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Div_Coll_Cde", 
            "#W1-DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Work_Record_Out_Pnd_W1_Inst_Iss_Cde = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Inst_Iss_Cde", 
            "#W1-INST-ISS-CDE", FieldType.STRING, 5);
        pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N", 
            "#W1-LST-TRANS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Type = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Type", 
            "#W1-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re", 
            "#W1-CNTRCT-RSDNCY-AT-ISS-RE", FieldType.STRING, 3);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N", 
            "#W1-CNTRCT-FNL-PRM-DTE-N", FieldType.NUMERIC, 8, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Mtch_Ppcn = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Mtch_Ppcn", 
            "#W1-CNTRCT-MTCH-PPCN", FieldType.STRING, 10);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N", 
            "#W1-CNTRCT-ANNTY-STRT-DTE-N", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Issue_Dte_Dd", 
            "#W1-CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", 
            "#W1-CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", 
            "#W1-CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_Out_Pnd_W1_Filler = pnd_Work_Record_Out_Pnd_Record_Type_10_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Filler", "#W1-FILLER", 
            FieldType.STRING, 150);
        pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef6 = pnd_Work_Record_Out.newGroupInGroup("pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef6", "Redefines", 
            pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out);
        pnd_Work_Record_Out_Pnd_Record_Type_20_Out = pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef6.newGroupInGroup("pnd_Work_Record_Out_Pnd_Record_Type_20_Out", 
            "#RECORD-TYPE-20-OUT");
        pnd_Work_Record_Out_Pnd_W2_Cpr_Id_Nbr = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Cpr_Id_Nbr", "#W2-CPR-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N", 
            "#W2-LST-TRANS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Ctznshp_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Ctznshp_Cde", 
            "#W2-PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Cde", 
            "#W2-PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Sw = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Rsdncy_Sw", 
            "#W2-PRTCPNT-RSDNCY-SW", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Nbr = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Nbr", 
            "#W2-PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Typ = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Prtcpnt_Tax_Id_Typ", 
            "#W2-PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Actvty_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Actvty_Cde", "#W2-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W2_Trmnte_Rsn = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Trmnte_Rsn", "#W2-TRMNTE-RSN", 
            FieldType.STRING, 2);
        pnd_Work_Record_Out_Pnd_W2_Rwrttn_Ind = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rwrttn_Ind", "#W2-RWRTTN-IND", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W2_Cash_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Cash_Cde", "#W2-CASH-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Emplymnt_Trmnt_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Emplymnt_Trmnt_Cde", 
            "#W2-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Company_Cd = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Company_Cd", 
            "#W2-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Rcvry_Type_Ind = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Rcvry_Type_Ind", 
            "#W2-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Per_Ivc_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Per_Ivc_Amt", 
            "#W2-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Resdl_Ivc_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Resdl_Ivc_Amt", 
            "#W2-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Ivc_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Ivc_Amt", "#W2-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Ivc_Used_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Ivc_Used_Amt", 
            "#W2-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Rtb_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Rtb_Amt", "#W2-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Rtb_Percent = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Rtb_Percent", 
            "#W2-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7,4, new DbsArrayController(1,5));
        pnd_Work_Record_Out_Pnd_W2_Mode_Ind = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Mode_Ind", "#W2-MODE-IND", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_Out_Pnd_W2_Wthdrwl_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Wthdrwl_Dte", 
            "#W2-WTHDRWL-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W2_Final_Per_Pay_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Final_Per_Pay_Dte", 
            "#W2-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W2_Final_Pay_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Final_Pay_Dte", 
            "#W2-FINAL-PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W2_Bnfcry_Xref_Ind = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Bnfcry_Xref_Ind", 
            "#W2-BNFCRY-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_Out_Pnd_W2_Bnfcry_Dod_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Bnfcry_Dod_Dte", 
            "#W2-BNFCRY-DOD-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W2_Pend_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Pend_Cde", "#W2-PEND-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Hold_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Hold_Cde", "#W2-HOLD-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Pend_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Pend_Dte", "#W2-PEND-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W2_Prev_Dist_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Prev_Dist_Cde", 
            "#W2-PREV-DIST-CDE", FieldType.STRING, 4);
        pnd_Work_Record_Out_Pnd_W2_Curr_Dist_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Curr_Dist_Cde", 
            "#W2-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Work_Record_Out_Pnd_W2_Cmbne_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Cmbne_Cde", "#W2-CMBNE-CDE", 
            FieldType.STRING, 12);
        pnd_Work_Record_Out_Pnd_W2_Spirt_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Spirt_Cde", "#W2-SPIRT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Spirt_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Spirt_Amt", "#W2-SPIRT-AMT", 
            FieldType.PACKED_DECIMAL, 7,2);
        pnd_Work_Record_Out_Pnd_W2_Spirt_Srce = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Spirt_Srce", "#W2-SPIRT-SRCE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Spirt_Arr_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Spirt_Arr_Dte", 
            "#W2-SPIRT-ARR-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_Out_Pnd_W2_Spirt_Prcss_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Spirt_Prcss_Dte", 
            "#W2-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W2_Fed_Tax_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Fed_Tax_Amt", 
            "#W2-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W2_State_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_State_Cde", "#W2-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_Out_Pnd_W2_State_Tax_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_State_Tax_Amt", 
            "#W2-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W2_Local_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Local_Cde", "#W2-LOCAL-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_Out_Pnd_W2_Local_Tax_Amt = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Local_Tax_Amt", 
            "#W2-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W2_Lst_Chnge_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Lst_Chnge_Dte", 
            "#W2-LST-CHNGE-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Term_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Term_Cde", 
            "#W2-CPR-XFR-TERM-CDE", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Cpr_Lgl_Res_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Cpr_Lgl_Res_Cde", 
            "#W2-CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N", 
            "#W2-CPR-XFR-ISS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W2_Rllvr_Cntrct_Nbr = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rllvr_Cntrct_Nbr", 
            "#W2-RLLVR-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Work_Record_Out_Pnd_W2_Rllvr_Ivc_Ind = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rllvr_Ivc_Ind", 
            "#W2-RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Rllvr_Elgble_Ind = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rllvr_Elgble_Ind", 
            "#W2-RLLVR-ELGBLE-IND", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldArrayInGroup("pnd_Work_Record_Out_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde", 
            "#W2-RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4));
        pnd_Work_Record_Out_Pnd_W2_Rllvr_Accptng_Irc_Cde = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rllvr_Accptng_Irc_Cde", 
            "#W2-RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2);
        pnd_Work_Record_Out_Pnd_W2_Rllvr_Pln_Admn_Ind = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rllvr_Pln_Admn_Ind", 
            "#W2-RLLVR-PLN-ADMN-IND", FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N8 = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N8", 
            "#W2-CPR-XFR-ISS-DTE-N8", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W2_Roth_Dsblty_Dte = pnd_Work_Record_Out_Pnd_Record_Type_20_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Roth_Dsblty_Dte", 
            "#W2-ROTH-DSBLTY-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef7 = pnd_Work_Record_Out.newGroupInGroup("pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef7", "Redefines", 
            pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out);
        pnd_Work_Record_Out_Pnd_Record_Type_30_Out = pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef7.newGroupInGroup("pnd_Work_Record_Out_Pnd_Record_Type_30_Out", 
            "#RECORD-TYPE-30-OUT");
        pnd_Work_Record_Out_Pnd_W3_Cmpny_Cde = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Cmpny_Cde", "#W3-CMPNY-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W3_Fund_Cde = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Fund_Cde", "#W3-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_Out_Pnd_W3_Per_Ivc_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Per_Ivc_Amt", 
            "#W3-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Rtb_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Rtb_Amt", "#W3-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Tot_Per_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Tot_Per_Amt", 
            "#W3-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Tot_Div_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Tot_Div_Amt", 
            "#W3-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Old_Per_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Old_Per_Amt", 
            "#W3-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Old_Div_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Old_Div_Amt", 
            "#W3-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Rate_Cde = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Rate_Cde", "#W3-RATE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_Out_Pnd_W3_Rate_Dte = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Rate_Dte", "#W3-RATE-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt", 
            "#W3-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt", 
            "#W3-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Units_Cnt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Units_Cnt", "#W3-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9,3);
        pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt", 
            "#W3-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt", 
            "#W3-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N", 
            "#W3-LST-TRANS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W3_Filler = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Filler", "#W3-FILLER", 
            FieldType.STRING, 231);
        pnd_Work_Record_Out_Pnd_W3_Filler2 = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Filler2", "#W3-FILLER2", 
            FieldType.STRING, 31);
        pnd_Work_Record_Out_Pnd_W3_Filler3 = pnd_Work_Record_Out_Pnd_Record_Type_30_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Filler3", "#W3-FILLER3", 
            FieldType.STRING, 16);
        pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef8 = pnd_Work_Record_Out.newGroupInGroup("pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef8", "Redefines", 
            pnd_Work_Record_Out_Pnd_Rest_Of_Record_Out);
        pnd_Work_Record_Out_Pnd_Record_Type_40_Out = pnd_Work_Record_Out_Pnd_Rest_Of_Record_OutRedef8.newGroupInGroup("pnd_Work_Record_Out_Pnd_Record_Type_40_Out", 
            "#RECORD-TYPE-40-OUT");
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Id_Nbr = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Id_Nbr", 
            "#W4-DDCTN-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Cde = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Cde", "#W4-DDCTN-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Seq_Nbr = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Seq_Nbr", 
            "#W4-DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Payee = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Payee", 
            "#W4-DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Per_Amt = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Per_Amt", 
            "#W4-DDCTN-PER-AMT", FieldType.DECIMAL, 7,2);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Ytd_Amt = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Ytd_Amt", 
            "#W4-DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Pd_To_Dte = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Pd_To_Dte", 
            "#W4-DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Tot_Amt = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Tot_Amt", 
            "#W4-DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Intent_Cde = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Intent_Cde", 
            "#W4-DDCTN-INTENT-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Strt_Dte = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Strt_Dte", 
            "#W4-DDCTN-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Stp_Dte = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Stp_Dte", 
            "#W4-DDCTN-STP-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W4_Ddctn_Final_Dte = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Ddctn_Final_Dte", 
            "#W4-DDCTN-FINAL-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N", 
            "#W4-LST-TRANS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Work_Record_Out_Pnd_W4_Filler = pnd_Work_Record_Out_Pnd_Record_Type_40_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W4_Filler", "#W4-FILLER", 
            FieldType.STRING, 270);

        this.setRecordName("LdaIaal399");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal399() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
