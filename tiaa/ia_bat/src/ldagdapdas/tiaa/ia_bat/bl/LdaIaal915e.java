/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:03:46 PM
**        * FROM NATURAL LDA     : IAAL915E
************************************************************
**        * FILE NAME            : LdaIaal915e.java
**        * CLASS NAME           : LdaIaal915e
**        * INSTANCE NAME        : LdaIaal915e
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal915e extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DataAccessProgramView vw_iaa_Deduction;
    private DbsField iaa_Deduction_Lst_Trans_Dte;
    private DbsField iaa_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_Ddctn_Cde;
    private DbsField iaa_Deduction_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee;
    private DbsField iaa_Deduction_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_Ddctn_Final_Dte;
    private DataAccessProgramView vw_iaa_Ddctn_Trans;
    private DbsField iaa_Ddctn_Trans_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Lst_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Cde;
    private DbsGroup iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee;
    private DbsField iaa_Ddctn_Trans_Ddctn_Per_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Tot_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Final_Dte;
    private DbsField iaa_Ddctn_Trans_Bfre_Imge_Id;
    private DbsField iaa_Ddctn_Trans_Aftr_Imge_Id;
    private DbsField iaa_Ddctn_Trans_Ddctn_Strt_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Stp_Dte;
    private DbsField pnd_Ddctn_From_Net;
    private DbsGroup pnd_Ddctn_From_NetRedef2;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Check_DteRedef3;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy;
    private DbsField pnd_Ddctn_From_Net_Pnd_User_Area;
    private DbsField pnd_Ddctn_From_Net_Pnd_User_Id;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Trans_DteRedef4;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Record_Status;
    private DbsField pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Intent_Code;
    private DbsField pnd_Ddctn_From_Net_Pnd_Sequence_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Ddctn_Cde;
    private DbsField pnd_Ddctn_From_Net_Pnd_Ddctn_Payee;
    private DbsField pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef5;
    private DbsField pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Tot_Ddctn;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef6;
    private DbsField pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef7;
    private DbsField pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm;
    private DbsField pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy;
    private DbsField pnd_Ddctn_From_Net_Pnd_Filler1;
    private DbsField pnd_Ddctn_From_Net_Pnd_Multiple;
    private DbsField pnd_Ddctn_From_Net_Pnd_Seq_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Verify;
    private DbsField pnd_Ddctn_From_Net_Pnd_Filler2;
    private DbsField pnd_His_Ytd_Ddctn;
    private DbsGroup pnd_His_Ytd_DdctnRedef8;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte;
    private DbsGroup pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_User_Area;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_User_Id;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte;
    private DbsGroup pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef10;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Record_Status;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Intent_Code;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt;
    private DbsGroup pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef11;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Filler1;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Multiple;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Verify;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Filler2;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key;
    private DbsGroup pnd_Iaa_Cntrct_Aftr_KeyRedef12;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Iaa_Cntrct_Bfre_Key;
    private DbsGroup pnd_Iaa_Cntrct_Bfre_KeyRedef13;
    private DbsField pnd_Iaa_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Iaa_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Iaa_Cpr_Bfre_Key;
    private DbsGroup pnd_Iaa_Cpr_Bfre_KeyRedef14;
    private DbsField pnd_Iaa_Cpr_Bfre_Key_Pnd_Bfre_Image_Id;
    private DbsField pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Iaa_Cpr_Aftr_Key;
    private DbsGroup pnd_Iaa_Cpr_Aftr_KeyRedef15;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key;
    private DbsGroup pnd_Cntrct_Payee_Ddctn_KeyRedef16;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Cde;
    private DbsField pnd_Ddctn_Aftr_Key;
    private DbsGroup pnd_Ddctn_Aftr_KeyRedef17;
    private DbsField pnd_Ddctn_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Seq_Cde;
    private DbsField pnd_Ddctn_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Ddctn_Bfre_Key;
    private DbsGroup pnd_Ddctn_Bfre_KeyRedef18;
    private DbsField pnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Seq_Cde;
    private DbsField pnd_Ddctn_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Ddctn_Final_Dte;
    private DbsGroup pnd_Ddctn_Final_DteRedef19;
    private DbsField pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Cc;
    private DbsField pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Yy;
    private DbsField pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Mm;
    private DbsField pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Dd;
    private DbsField pnd_Seq_Ddctn_Tbl;
    private DbsField pnd_Save_Trans_Ppcn_Nbr;
    private DbsField pnd_Save_Trans_Payee_Cde;
    private DbsField pnd_Save_Trans_Cde;
    private DbsField pnd_Indx;
    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_No_Cntrct_Rec;

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_Bnfcry_Xref_Ind; }

    public DataAccessProgramView getVw_iaa_Deduction() { return vw_iaa_Deduction; }

    public DbsField getIaa_Deduction_Lst_Trans_Dte() { return iaa_Deduction_Lst_Trans_Dte; }

    public DbsField getIaa_Deduction_Ddctn_Ppcn_Nbr() { return iaa_Deduction_Ddctn_Ppcn_Nbr; }

    public DbsField getIaa_Deduction_Ddctn_Payee_Cde() { return iaa_Deduction_Ddctn_Payee_Cde; }

    public DbsField getIaa_Deduction_Ddctn_Id_Nbr() { return iaa_Deduction_Ddctn_Id_Nbr; }

    public DbsField getIaa_Deduction_Ddctn_Cde() { return iaa_Deduction_Ddctn_Cde; }

    public DbsField getIaa_Deduction_Ddctn_Seq_Nbr() { return iaa_Deduction_Ddctn_Seq_Nbr; }

    public DbsField getIaa_Deduction_Ddctn_Payee() { return iaa_Deduction_Ddctn_Payee; }

    public DbsField getIaa_Deduction_Ddctn_Per_Amt() { return iaa_Deduction_Ddctn_Per_Amt; }

    public DbsField getIaa_Deduction_Ddctn_Tot_Amt() { return iaa_Deduction_Ddctn_Tot_Amt; }

    public DbsField getIaa_Deduction_Ddctn_Intent_Cde() { return iaa_Deduction_Ddctn_Intent_Cde; }

    public DbsField getIaa_Deduction_Ddctn_Strt_Dte() { return iaa_Deduction_Ddctn_Strt_Dte; }

    public DbsField getIaa_Deduction_Ddctn_Stp_Dte() { return iaa_Deduction_Ddctn_Stp_Dte; }

    public DbsField getIaa_Deduction_Ddctn_Final_Dte() { return iaa_Deduction_Ddctn_Final_Dte; }

    public DataAccessProgramView getVw_iaa_Ddctn_Trans() { return vw_iaa_Ddctn_Trans; }

    public DbsField getIaa_Ddctn_Trans_Trans_Dte() { return iaa_Ddctn_Trans_Trans_Dte; }

    public DbsField getIaa_Ddctn_Trans_Invrse_Trans_Dte() { return iaa_Ddctn_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Ddctn_Trans_Lst_Trans_Dte() { return iaa_Ddctn_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Ppcn_Nbr() { return iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Payee_Cde() { return iaa_Ddctn_Trans_Ddctn_Payee_Cde; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Seq_Cde() { return iaa_Ddctn_Trans_Ddctn_Seq_Cde; }

    public DbsGroup getIaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1() { return iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Seq_Nbr() { return iaa_Ddctn_Trans_Ddctn_Seq_Nbr; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Cde() { return iaa_Ddctn_Trans_Ddctn_Cde; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Payee() { return iaa_Ddctn_Trans_Ddctn_Payee; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Per_Amt() { return iaa_Ddctn_Trans_Ddctn_Per_Amt; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Tot_Amt() { return iaa_Ddctn_Trans_Ddctn_Tot_Amt; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Final_Dte() { return iaa_Ddctn_Trans_Ddctn_Final_Dte; }

    public DbsField getIaa_Ddctn_Trans_Bfre_Imge_Id() { return iaa_Ddctn_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Ddctn_Trans_Aftr_Imge_Id() { return iaa_Ddctn_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Strt_Dte() { return iaa_Ddctn_Trans_Ddctn_Strt_Dte; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Stp_Dte() { return iaa_Ddctn_Trans_Ddctn_Stp_Dte; }

    public DbsField getPnd_Ddctn_From_Net() { return pnd_Ddctn_From_Net; }

    public DbsGroup getPnd_Ddctn_From_NetRedef2() { return pnd_Ddctn_From_NetRedef2; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte() { return pnd_Ddctn_From_Net_Pnd_Check_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Check_DteRedef3() { return pnd_Ddctn_From_Net_Pnd_Check_DteRedef3; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Mm() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Dd() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Yy() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_User_Area() { return pnd_Ddctn_From_Net_Pnd_User_Area; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_User_Id() { return pnd_Ddctn_From_Net_Pnd_User_Id; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Trans_DteRedef4() { return pnd_Ddctn_From_Net_Pnd_Trans_DteRedef4; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte_N() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Nbr() { return pnd_Ddctn_From_Net_Pnd_Trans_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Cntrct_Nbr() { return pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Record_Status() { return pnd_Ddctn_From_Net_Pnd_Record_Status; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr() { return pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Intent_Code() { return pnd_Ddctn_From_Net_Pnd_Intent_Code; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Sequence_Nbr() { return pnd_Ddctn_From_Net_Pnd_Sequence_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Ddctn_Cde() { return pnd_Ddctn_From_Net_Pnd_Ddctn_Cde; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Ddctn_Payee() { return pnd_Ddctn_From_Net_Pnd_Ddctn_Payee; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt() { return pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef5() { return pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef5; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N() { return pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn() { return pnd_Ddctn_From_Net_Pnd_Tot_Ddctn; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef6() { return pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef6; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N() { return pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef7() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef7; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Filler1() { return pnd_Ddctn_From_Net_Pnd_Filler1; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Multiple() { return pnd_Ddctn_From_Net_Pnd_Multiple; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Seq_Nbr() { return pnd_Ddctn_From_Net_Pnd_Seq_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Verify() { return pnd_Ddctn_From_Net_Pnd_Verify; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Filler2() { return pnd_Ddctn_From_Net_Pnd_Filler2; }

    public DbsField getPnd_His_Ytd_Ddctn() { return pnd_His_Ytd_Ddctn; }

    public DbsGroup getPnd_His_Ytd_DdctnRedef8() { return pnd_His_Ytd_DdctnRedef8; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte; }

    public DbsGroup getPnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9() { return pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_User_Area() { return pnd_His_Ytd_Ddctn_Pnd_User_Area; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_User_Id() { return pnd_His_Ytd_Ddctn_Pnd_User_Id; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte; }

    public DbsGroup getPnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef10() { return pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef10; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Record_Status() { return pnd_His_Ytd_Ddctn_Pnd_Record_Status; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Intent_Code() { return pnd_His_Ytd_Ddctn_Pnd_Intent_Code; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde() { return pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee() { return pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt() { return pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt; }

    public DbsGroup getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef11() { return pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef11; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N() { return pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Filler1() { return pnd_His_Ytd_Ddctn_Pnd_Filler1; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Multiple() { return pnd_His_Ytd_Ddctn_Pnd_Multiple; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Seq_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Verify() { return pnd_His_Ytd_Ddctn_Pnd_Verify; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Filler2() { return pnd_His_Ytd_Ddctn_Pnd_Filler2; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key() { return pnd_Iaa_Cntrct_Aftr_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Aftr_KeyRedef12() { return pnd_Iaa_Cntrct_Aftr_KeyRedef12; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Iaa_Cntrct_Bfre_Key() { return pnd_Iaa_Cntrct_Bfre_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Bfre_KeyRedef13() { return pnd_Iaa_Cntrct_Bfre_KeyRedef13; }

    public DbsField getPnd_Iaa_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Iaa_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Iaa_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Bfre_Key_Pnd_Trans_Dte() { return pnd_Iaa_Cntrct_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Iaa_Cpr_Bfre_Key() { return pnd_Iaa_Cpr_Bfre_Key; }

    public DbsGroup getPnd_Iaa_Cpr_Bfre_KeyRedef14() { return pnd_Iaa_Cpr_Bfre_KeyRedef14; }

    public DbsField getPnd_Iaa_Cpr_Bfre_Key_Pnd_Bfre_Image_Id() { return pnd_Iaa_Cpr_Bfre_Key_Pnd_Bfre_Image_Id; }

    public DbsField getPnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Bfre_Key_Pnd_Trans_Dte() { return pnd_Iaa_Cpr_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key() { return pnd_Iaa_Cpr_Aftr_Key; }

    public DbsGroup getPnd_Iaa_Cpr_Aftr_KeyRedef15() { return pnd_Iaa_Cpr_Aftr_KeyRedef15; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Cntrct_Payee_Ddctn_Key() { return pnd_Cntrct_Payee_Ddctn_Key; }

    public DbsGroup getPnd_Cntrct_Payee_Ddctn_KeyRedef16() { return pnd_Cntrct_Payee_Ddctn_KeyRedef16; }

    public DbsField getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr() { return pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde() { return pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde; }

    public DbsField getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr() { return pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr; }

    public DbsField getPnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Cde() { return pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Cde; }

    public DbsField getPnd_Ddctn_Aftr_Key() { return pnd_Ddctn_Aftr_Key; }

    public DbsGroup getPnd_Ddctn_Aftr_KeyRedef17() { return pnd_Ddctn_Aftr_KeyRedef17; }

    public DbsField getPnd_Ddctn_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Ddctn_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Ddctn_Aftr_Key_Pnd_Ddctn_Ppcn_Nbr() { return pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Ppcn_Nbr; }

    public DbsField getPnd_Ddctn_Aftr_Key_Pnd_Ddctn_Payee_Cde() { return pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Payee_Cde; }

    public DbsField getPnd_Ddctn_Aftr_Key_Pnd_Ddctn_Seq_Cde() { return pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Seq_Cde; }

    public DbsField getPnd_Ddctn_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Ddctn_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Ddctn_Bfre_Key() { return pnd_Ddctn_Bfre_Key; }

    public DbsGroup getPnd_Ddctn_Bfre_KeyRedef18() { return pnd_Ddctn_Bfre_KeyRedef18; }

    public DbsField getPnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr() { return pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr; }

    public DbsField getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Payee_Cde() { return pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Payee_Cde; }

    public DbsField getPnd_Ddctn_Bfre_Key_Pnd_Ddctn_Seq_Cde() { return pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Seq_Cde; }

    public DbsField getPnd_Ddctn_Bfre_Key_Pnd_Trans_Dte() { return pnd_Ddctn_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Ddctn_Final_Dte() { return pnd_Ddctn_Final_Dte; }

    public DbsGroup getPnd_Ddctn_Final_DteRedef19() { return pnd_Ddctn_Final_DteRedef19; }

    public DbsField getPnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Cc() { return pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Cc; }

    public DbsField getPnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Yy() { return pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Yy; }

    public DbsField getPnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Mm() { return pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Mm; }

    public DbsField getPnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Dd() { return pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Dd; }

    public DbsField getPnd_Seq_Ddctn_Tbl() { return pnd_Seq_Ddctn_Tbl; }

    public DbsField getPnd_Save_Trans_Ppcn_Nbr() { return pnd_Save_Trans_Ppcn_Nbr; }

    public DbsField getPnd_Save_Trans_Payee_Cde() { return pnd_Save_Trans_Payee_Cde; }

    public DbsField getPnd_Save_Trans_Cde() { return pnd_Save_Trans_Cde; }

    public DbsField getPnd_Indx() { return pnd_Indx; }

    public DbsGroup getPnd_Logical_Variables() { return pnd_Logical_Variables; }

    public DbsField getPnd_Logical_Variables_Pnd_No_Cntrct_Rec() { return pnd_Logical_Variables_Pnd_No_Cntrct_Rec; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");

        vw_iaa_Deduction = new DataAccessProgramView(new NameInfo("vw_iaa_Deduction", "IAA-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Deduction_Lst_Trans_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Deduction_Ddctn_Ppcn_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Deduction_Ddctn_Payee_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Deduction_Ddctn_Id_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Deduction_Ddctn_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Deduction_Ddctn_Seq_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Deduction_Ddctn_Payee = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Deduction_Ddctn_Per_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Deduction_Ddctn_Tot_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Deduction_Ddctn_Intent_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Deduction_Ddctn_Strt_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Deduction_Ddctn_Stp_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Deduction_Ddctn_Final_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");

        vw_iaa_Ddctn_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Ddctn_Trans", "IAA-DDCTN-TRANS"), "IAA_DDCTN_TRANS", "IA_TRANS_FILE");
        iaa_Ddctn_Trans_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Ddctn_Trans_Invrse_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Ddctn_Trans_Lst_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Ddctn_Trans_Ddctn_Payee_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Ddctn_Trans_Ddctn_Seq_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Cde", "DDCTN-SEQ-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "DDCTN_SEQ_CDE");
        iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1 = vw_iaa_Ddctn_Trans.getRecord().newGroupInGroup("iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1", "Redefines", iaa_Ddctn_Trans_Ddctn_Seq_Cde);
        iaa_Ddctn_Trans_Ddctn_Seq_Nbr = iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        iaa_Ddctn_Trans_Ddctn_Cde = iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3);
        iaa_Ddctn_Trans_Ddctn_Payee = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Ddctn_Trans_Ddctn_Per_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Ddctn_Trans_Ddctn_Tot_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Ddctn_Trans_Ddctn_Final_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Ddctn_Trans_Bfre_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Ddctn_Trans_Aftr_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Ddctn_Trans_Ddctn_Strt_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Ddctn_Trans_Ddctn_Stp_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");

        pnd_Ddctn_From_Net = newFieldInRecord("pnd_Ddctn_From_Net", "#DDCTN-FROM-NET", FieldType.STRING, 120);
        pnd_Ddctn_From_NetRedef2 = newGroupInRecord("pnd_Ddctn_From_NetRedef2", "Redefines", pnd_Ddctn_From_Net);
        pnd_Ddctn_From_Net_Pnd_Check_Dte = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 
            6);
        pnd_Ddctn_From_Net_Pnd_Check_DteRedef3 = pnd_Ddctn_From_NetRedef2.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Check_DteRedef3", "Redefines", pnd_Ddctn_From_Net_Pnd_Check_Dte);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm = pnd_Ddctn_From_Net_Pnd_Check_DteRedef3.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd = pnd_Ddctn_From_Net_Pnd_Check_DteRedef3.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy = pnd_Ddctn_From_Net_Pnd_Check_DteRedef3.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_User_Area = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 
            6);
        pnd_Ddctn_From_Net_Pnd_User_Id = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 
            8);
        pnd_Ddctn_From_Net_Pnd_Trans_DteRedef4 = pnd_Ddctn_From_NetRedef2.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Trans_DteRedef4", "Redefines", pnd_Ddctn_From_Net_Pnd_Trans_Dte);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte_N = pnd_Ddctn_From_Net_Pnd_Trans_DteRedef4.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte_N", "#TRANS-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Ddctn_From_Net_Pnd_Trans_Nbr = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 
            3);
        pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Ddctn_From_Net_Pnd_Record_Status = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Record_Status", "#RECORD-STATUS", FieldType.NUMERIC, 
            2);
        pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_Ddctn_From_Net_Pnd_Intent_Code = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_Ddctn_From_Net_Pnd_Sequence_Nbr = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Sequence_Nbr", "#SEQUENCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Ddctn_From_Net_Pnd_Ddctn_Cde = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_Ddctn_From_Net_Pnd_Ddctn_Payee = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt", "#PER-DDCTN-AMT", FieldType.STRING, 
            7);
        pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef5 = pnd_Ddctn_From_NetRedef2.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef5", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt);
        pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N = pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef5.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N", 
            "#PER-DDCTN-AMT-N", FieldType.DECIMAL, 7,2);
        pnd_Ddctn_From_Net_Pnd_Tot_Ddctn = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Tot_Ddctn", "#TOT-DDCTN", FieldType.STRING, 
            9);
        pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef6 = pnd_Ddctn_From_NetRedef2.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef6", "Redefines", pnd_Ddctn_From_Net_Pnd_Tot_Ddctn);
        pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N = pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef6.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N", "#TOT-DDCTN-N", 
            FieldType.DECIMAL, 9,2);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte", "#FINAL-DDCTN-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef7 = pnd_Ddctn_From_NetRedef2.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef7", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm = pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef7.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm", 
            "#FINAL-DDCTN-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy = pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef7.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy", 
            "#FINAL-DDCTN-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Filler1 = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Filler1", "#FILLER1", FieldType.STRING, 28);
        pnd_Ddctn_From_Net_Pnd_Multiple = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_Ddctn_From_Net_Pnd_Seq_Nbr = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Ddctn_From_Net_Pnd_Verify = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Ddctn_From_Net_Pnd_Filler2 = pnd_Ddctn_From_NetRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Filler2", "#FILLER2", FieldType.STRING, 4);

        pnd_His_Ytd_Ddctn = newFieldInRecord("pnd_His_Ytd_Ddctn", "#HIS-YTD-DDCTN", FieldType.STRING, 120);
        pnd_His_Ytd_DdctnRedef8 = newGroupInRecord("pnd_His_Ytd_DdctnRedef8", "Redefines", pnd_His_Ytd_Ddctn);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 
            6);
        pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9 = pnd_His_Ytd_DdctnRedef8.newGroupInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9", "Redefines", pnd_His_Ytd_Ddctn_Pnd_Check_Dte);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm = pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd = pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy = pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef9.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_User_Area = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 6);
        pnd_His_Ytd_Ddctn_Pnd_User_Id = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef10 = pnd_His_Ytd_DdctnRedef8.newGroupInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef10", "Redefines", pnd_His_Ytd_Ddctn_Pnd_Trans_Dte);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N = pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef10.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N", "#TRANS-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 
            3);
        pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_His_Ytd_Ddctn_Pnd_Record_Status = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Record_Status", "#RECORD-STATUS", FieldType.NUMERIC, 
            2);
        pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_His_Ytd_Ddctn_Pnd_Intent_Code = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr", "#SEQUENCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);
        pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt", "#HIS-DDCTN-AMT", FieldType.STRING, 
            9);
        pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef11 = pnd_His_Ytd_DdctnRedef8.newGroupInGroup("pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef11", "Redefines", 
            pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt);
        pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N = pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef11.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N", "#HIS-DDCTN-AMT-N", 
            FieldType.DECIMAL, 9,2);
        pnd_His_Ytd_Ddctn_Pnd_Filler1 = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Filler1", "#FILLER1", FieldType.STRING, 39);
        pnd_His_Ytd_Ddctn_Pnd_Multiple = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_His_Ytd_Ddctn_Pnd_Verify = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_His_Ytd_Ddctn_Pnd_Filler2 = pnd_His_Ytd_DdctnRedef8.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Filler2", "#FILLER2", FieldType.STRING, 4);

        pnd_Iaa_Cntrct_Aftr_Key = newFieldInRecord("pnd_Iaa_Cntrct_Aftr_Key", "#IAA-CNTRCT-AFTR-KEY", FieldType.STRING, 23);
        pnd_Iaa_Cntrct_Aftr_KeyRedef12 = newGroupInRecord("pnd_Iaa_Cntrct_Aftr_KeyRedef12", "Redefines", pnd_Iaa_Cntrct_Aftr_Key);
        pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Iaa_Cntrct_Aftr_KeyRedef12.newFieldInGroup("pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_Aftr_KeyRedef12.newFieldInGroup("pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Iaa_Cntrct_Aftr_KeyRedef12.newFieldInGroup("pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte", 
            "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);

        pnd_Iaa_Cntrct_Bfre_Key = newFieldInRecord("pnd_Iaa_Cntrct_Bfre_Key", "#IAA-CNTRCT-BFRE-KEY", FieldType.STRING, 18);
        pnd_Iaa_Cntrct_Bfre_KeyRedef13 = newGroupInRecord("pnd_Iaa_Cntrct_Bfre_KeyRedef13", "Redefines", pnd_Iaa_Cntrct_Bfre_Key);
        pnd_Iaa_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Iaa_Cntrct_Bfre_KeyRedef13.newFieldInGroup("pnd_Iaa_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_Bfre_KeyRedef13.newFieldInGroup("pnd_Iaa_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Bfre_Key_Pnd_Trans_Dte = pnd_Iaa_Cntrct_Bfre_KeyRedef13.newFieldInGroup("pnd_Iaa_Cntrct_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", 
            FieldType.TIME);

        pnd_Iaa_Cpr_Bfre_Key = newFieldInRecord("pnd_Iaa_Cpr_Bfre_Key", "#IAA-CPR-BFRE-KEY", FieldType.STRING, 20);
        pnd_Iaa_Cpr_Bfre_KeyRedef14 = newGroupInRecord("pnd_Iaa_Cpr_Bfre_KeyRedef14", "Redefines", pnd_Iaa_Cpr_Bfre_Key);
        pnd_Iaa_Cpr_Bfre_Key_Pnd_Bfre_Image_Id = pnd_Iaa_Cpr_Bfre_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Bfre_Key_Pnd_Bfre_Image_Id", "#BFRE-IMAGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Bfre_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Bfre_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Bfre_Key_Pnd_Trans_Dte = pnd_Iaa_Cpr_Bfre_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Iaa_Cpr_Aftr_Key = newFieldInRecord("pnd_Iaa_Cpr_Aftr_Key", "#IAA-CPR-AFTR-KEY", FieldType.STRING, 25);
        pnd_Iaa_Cpr_Aftr_KeyRedef15 = newGroupInRecord("pnd_Iaa_Cpr_Aftr_KeyRedef15", "Redefines", pnd_Iaa_Cpr_Aftr_Key);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Iaa_Cpr_Aftr_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Aftr_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Aftr_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Iaa_Cpr_Aftr_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        pnd_Cntrct_Payee_Ddctn_Key = newFieldInRecord("pnd_Cntrct_Payee_Ddctn_Key", "#CNTRCT-PAYEE-DDCTN-KEY", FieldType.STRING, 18);
        pnd_Cntrct_Payee_Ddctn_KeyRedef16 = newGroupInRecord("pnd_Cntrct_Payee_Ddctn_KeyRedef16", "Redefines", pnd_Cntrct_Payee_Ddctn_Key);
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Cntrct_Payee_Ddctn_KeyRedef16.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr", 
            "#DDCTN-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde = pnd_Cntrct_Payee_Ddctn_KeyRedef16.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde", 
            "#DDCTN-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr = pnd_Cntrct_Payee_Ddctn_KeyRedef16.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr", 
            "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Cde = pnd_Cntrct_Payee_Ddctn_KeyRedef16.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Cde", "#DDCTN-CDE", 
            FieldType.STRING, 3);

        pnd_Ddctn_Aftr_Key = newFieldInRecord("pnd_Ddctn_Aftr_Key", "#DDCTN-AFTR-KEY", FieldType.STRING, 31);
        pnd_Ddctn_Aftr_KeyRedef17 = newGroupInRecord("pnd_Ddctn_Aftr_KeyRedef17", "Redefines", pnd_Ddctn_Aftr_Key);
        pnd_Ddctn_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Ddctn_Aftr_KeyRedef17.newFieldInGroup("pnd_Ddctn_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Ddctn_Aftr_KeyRedef17.newFieldInGroup("pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Payee_Cde = pnd_Ddctn_Aftr_KeyRedef17.newFieldInGroup("pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Payee_Cde", "#DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Seq_Cde = pnd_Ddctn_Aftr_KeyRedef17.newFieldInGroup("pnd_Ddctn_Aftr_Key_Pnd_Ddctn_Seq_Cde", "#DDCTN-SEQ-CDE", FieldType.STRING, 
            6);
        pnd_Ddctn_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Ddctn_Aftr_KeyRedef17.newFieldInGroup("pnd_Ddctn_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        pnd_Ddctn_Bfre_Key = newFieldInRecord("pnd_Ddctn_Bfre_Key", "#DDCTN-BFRE-KEY", FieldType.STRING, 31);
        pnd_Ddctn_Bfre_KeyRedef18 = newGroupInRecord("pnd_Ddctn_Bfre_KeyRedef18", "Redefines", pnd_Ddctn_Bfre_Key);
        pnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Ddctn_Bfre_KeyRedef18.newFieldInGroup("pnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Ddctn_Bfre_KeyRedef18.newFieldInGroup("pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Payee_Cde = pnd_Ddctn_Bfre_KeyRedef18.newFieldInGroup("pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Payee_Cde", "#DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Seq_Cde = pnd_Ddctn_Bfre_KeyRedef18.newFieldInGroup("pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Seq_Cde", "#DDCTN-SEQ-CDE", FieldType.STRING, 
            6);
        pnd_Ddctn_Bfre_Key_Pnd_Trans_Dte = pnd_Ddctn_Bfre_KeyRedef18.newFieldInGroup("pnd_Ddctn_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Ddctn_Final_Dte = newFieldInRecord("pnd_Ddctn_Final_Dte", "#DDCTN-FINAL-DTE", FieldType.NUMERIC, 8);
        pnd_Ddctn_Final_DteRedef19 = newGroupInRecord("pnd_Ddctn_Final_DteRedef19", "Redefines", pnd_Ddctn_Final_Dte);
        pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Cc = pnd_Ddctn_Final_DteRedef19.newFieldInGroup("pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Cc", "#DDCTN-FINAL-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Yy = pnd_Ddctn_Final_DteRedef19.newFieldInGroup("pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Yy", "#DDCTN-FINAL-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Mm = pnd_Ddctn_Final_DteRedef19.newFieldInGroup("pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Mm", "#DDCTN-FINAL-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Dd = pnd_Ddctn_Final_DteRedef19.newFieldInGroup("pnd_Ddctn_Final_Dte_Pnd_Ddctn_Final_Dte_Dd", "#DDCTN-FINAL-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Seq_Ddctn_Tbl = newFieldArrayInRecord("pnd_Seq_Ddctn_Tbl", "#SEQ-DDCTN-TBL", FieldType.STRING, 6, new DbsArrayController(1,5));

        pnd_Save_Trans_Ppcn_Nbr = newFieldInRecord("pnd_Save_Trans_Ppcn_Nbr", "#SAVE-TRANS-PPCN-NBR", FieldType.STRING, 10);

        pnd_Save_Trans_Payee_Cde = newFieldInRecord("pnd_Save_Trans_Payee_Cde", "#SAVE-TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Save_Trans_Cde = newFieldInRecord("pnd_Save_Trans_Cde", "#SAVE-TRANS-CDE", FieldType.NUMERIC, 3);

        pnd_Indx = newFieldInRecord("pnd_Indx", "#INDX", FieldType.NUMERIC, 2);

        pnd_Logical_Variables = newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_No_Cntrct_Rec = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_No_Cntrct_Rec", "#NO-CNTRCT-REC", FieldType.BOOLEAN);

        this.setRecordName("LdaIaal915e");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Deduction.reset();
        vw_iaa_Ddctn_Trans.reset();
    }

    // Constructor
    public LdaIaal915e() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
