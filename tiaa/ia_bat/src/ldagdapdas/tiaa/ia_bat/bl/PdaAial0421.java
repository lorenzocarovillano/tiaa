/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:46 PM
**        * FROM NATURAL PDA     : AIAL0421
************************************************************
**        * FILE NAME            : PdaAial0421.java
**        * CLASS NAME           : PdaAial0421
**        * INSTANCE NAME        : PdaAial0421
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAial0421 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Aian042_Linkage;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Cmpny_Fund_Cde_Meth;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Option;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Contract_Origin;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Issue_Date;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dod;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dod;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Mode;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex;
    private DbsGroup pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Code;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Transfer_Eff_Dt;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ts_Guaranteed_Payment;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ts_Dividend_Payment;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Guaranteed_Pay;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Dividend_Pay;
    private DbsGroup pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Code;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Transfer_Eff_Dt;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Tg_Guaranteed_Payment;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Tg_Dividend_Payment;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Guaranteed_Pay;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Dividend_Pay;
    private DbsGroup pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Code;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Transfer_Eff_Dt;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ca_Guaranteed_Payment;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ca_Dividend_Payment;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Guaranteed_Pay;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Dividend_Pay;
    private DbsField pnd_Aian042_Linkage_Pnd_Aian042_Error;

    public DbsGroup getPnd_Aian042_Linkage() { return pnd_Aian042_Linkage; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr() { return pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde() { return pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Cmpny_Fund_Cde_Meth() { return pnd_Aian042_Linkage_Pnd_Aian042_Cmpny_Fund_Cde_Meth; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Option() { return pnd_Aian042_Linkage_Pnd_Aian042_Option; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Contract_Origin() { return pnd_Aian042_Linkage_Pnd_Aian042_Contract_Origin; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Issue_Date() { return pnd_Aian042_Linkage_Pnd_Aian042_Issue_Date; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob() { return pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dod() { return pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dod; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob() { return pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dod() { return pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dod; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex() { return pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Mode() { return pnd_Aian042_Linkage_Pnd_Aian042_Mode; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date() { return pnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex() { return pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex; }

    public DbsGroup getPnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard() { return pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Code() { return pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Code; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Transfer_Eff_Dt() { return pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Transfer_Eff_Dt; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ts_Guaranteed_Payment() { return pnd_Aian042_Linkage_Pnd_Aian042_Ts_Guaranteed_Payment; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ts_Dividend_Payment() { return pnd_Aian042_Linkage_Pnd_Aian042_Ts_Dividend_Payment; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Guaranteed_Pay() { return pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Guaranteed_Pay; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Dividend_Pay() { return pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Dividend_Pay; }

    public DbsGroup getPnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded() { return pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Code() { return pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Code; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Transfer_Eff_Dt() { return pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Transfer_Eff_Dt; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Tg_Guaranteed_Payment() { return pnd_Aian042_Linkage_Pnd_Aian042_Tg_Guaranteed_Payment; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Tg_Dividend_Payment() { return pnd_Aian042_Linkage_Pnd_Aian042_Tg_Dividend_Payment; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Guaranteed_Pay() { return pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Guaranteed_Pay; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Dividend_Pay() { return pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Dividend_Pay; }

    public DbsGroup getPnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual() { return pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Code() { return pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Code; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Transfer_Eff_Dt() { return pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Transfer_Eff_Dt; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ca_Guaranteed_Payment() { return pnd_Aian042_Linkage_Pnd_Aian042_Ca_Guaranteed_Payment; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ca_Dividend_Payment() { return pnd_Aian042_Linkage_Pnd_Aian042_Ca_Dividend_Payment; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Guaranteed_Pay() { return pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Guaranteed_Pay; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Dividend_Pay() { return pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Dividend_Pay; }

    public DbsField getPnd_Aian042_Linkage_Pnd_Aian042_Error() { return pnd_Aian042_Linkage_Pnd_Aian042_Error; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Aian042_Linkage = dbsRecord.newGroupInRecord("pnd_Aian042_Linkage", "#AIAN042-LINKAGE");
        pnd_Aian042_Linkage.setParameterOption(ParameterOption.ByReference);
        pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr", "#AIAN042-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 8);
        pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde", "#AIAN042-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Aian042_Linkage_Pnd_Aian042_Cmpny_Fund_Cde_Meth = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Cmpny_Fund_Cde_Meth", 
            "#AIAN042-CMPNY-FUND-CDE-METH", FieldType.STRING, 1);
        pnd_Aian042_Linkage_Pnd_Aian042_Option = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Option", "#AIAN042-OPTION", FieldType.NUMERIC, 
            2);
        pnd_Aian042_Linkage_Pnd_Aian042_Contract_Origin = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Contract_Origin", "#AIAN042-CONTRACT-ORIGIN", 
            FieldType.NUMERIC, 2);
        pnd_Aian042_Linkage_Pnd_Aian042_Issue_Date = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Issue_Date", "#AIAN042-ISSUE-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob", "#AIAN042-FIRST-ANN-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dod = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dod", "#AIAN042-FIRST-ANN-DOD", 
            FieldType.NUMERIC, 6);
        pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob", "#AIAN042-SECOND-ANN-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dod = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dod", "#AIAN042-SECOND-ANN-DOD", 
            FieldType.NUMERIC, 6);
        pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex", "#AIAN042-FIRST-ANN-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Aian042_Linkage_Pnd_Aian042_Mode = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Mode", "#AIAN042-MODE", FieldType.NUMERIC, 
            3);
        pnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date", 
            "#AIAN042-FINAL-PER-PAY-DATE", FieldType.NUMERIC, 6);
        pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex", "#AIAN042-SECOND-ANN-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard = pnd_Aian042_Linkage.newGroupInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard", "#AIAN042-TIAA-STANDARD");
        pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Code = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Code", 
            "#AIAN042-TS-RATE-CODE", FieldType.STRING, 2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Transfer_Eff_Dt = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ts_Rate_Transfer_Eff_Dt", 
            "#AIAN042-TS-RATE-TRANSFER-EFF-DT", FieldType.NUMERIC, 8, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ts_Guaranteed_Payment = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ts_Guaranteed_Payment", 
            "#AIAN042-TS-GUARANTEED-PAYMENT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ts_Dividend_Payment = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ts_Dividend_Payment", 
            "#AIAN042-TS-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Guaranteed_Pay = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Guaranteed_Pay", 
            "#AIAN042-TS-FINAL-GUARANTEED-PAY", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Dividend_Pay = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Standard.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ts_Final_Dividend_Pay", 
            "#AIAN042-TS-FINAL-DIVIDEND-PAY", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded = pnd_Aian042_Linkage.newGroupInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded", "#AIAN042-TIAA-GRADED");
        pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Code = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Code", 
            "#AIAN042-TG-RATE-CODE", FieldType.STRING, 2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Transfer_Eff_Dt = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tg_Rate_Transfer_Eff_Dt", 
            "#AIAN042-TG-RATE-TRANSFER-EFF-DT", FieldType.NUMERIC, 8, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Tg_Guaranteed_Payment = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tg_Guaranteed_Payment", 
            "#AIAN042-TG-GUARANTEED-PAYMENT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Tg_Dividend_Payment = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tg_Dividend_Payment", 
            "#AIAN042-TG-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Guaranteed_Pay = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Guaranteed_Pay", 
            "#AIAN042-TG-FINAL-GUARANTEED-PAY", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Dividend_Pay = pnd_Aian042_Linkage_Pnd_Aian042_Tiaa_Graded.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Tg_Final_Dividend_Pay", 
            "#AIAN042-TG-FINAL-DIVIDEND-PAY", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual = pnd_Aian042_Linkage.newGroupInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual", "#AIAN042-CREF-ANNUAL");
        pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Code = pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Code", 
            "#AIAN042-CA-RATE-CODE", FieldType.STRING, 2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Transfer_Eff_Dt = pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ca_Rate_Transfer_Eff_Dt", 
            "#AIAN042-CA-RATE-TRANSFER-EFF-DT", FieldType.NUMERIC, 8, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ca_Guaranteed_Payment = pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ca_Guaranteed_Payment", 
            "#AIAN042-CA-GUARANTEED-PAYMENT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ca_Dividend_Payment = pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ca_Dividend_Payment", 
            "#AIAN042-CA-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Guaranteed_Pay = pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Guaranteed_Pay", 
            "#AIAN042-CA-FINAL-GUARANTEED-PAY", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Dividend_Pay = pnd_Aian042_Linkage_Pnd_Aian042_Cref_Annual.newFieldArrayInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Ca_Final_Dividend_Pay", 
            "#AIAN042-CA-FINAL-DIVIDEND-PAY", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));
        pnd_Aian042_Linkage_Pnd_Aian042_Error = pnd_Aian042_Linkage.newFieldInGroup("pnd_Aian042_Linkage_Pnd_Aian042_Error", "#AIAN042-ERROR", FieldType.NUMERIC, 
            1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAial0421(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

