/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:37 PM
**        * FROM NATURAL PDA     : IAAA0510
************************************************************
**        * FILE NAME            : PdaIaaa0510.java
**        * CLASS NAME           : PdaIaaa0510
**        * INSTANCE NAME        : PdaIaaa0510
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIaaa0510 extends PdaBase
{
    // Properties
    private DbsGroup iaaa0510;
    private DbsField iaaa0510_Pec_Nbr_Active_Acct_Att;
    private DbsField iaaa0510_Pec_Nbr_Active_Acct;
    private DbsField iaaa0510_Pec_Acct_Name_Att;
    private DbsField iaaa0510_Pec_Acct_Name_1;
    private DbsField iaaa0510_Pec_Acct_Name_2;
    private DbsField iaaa0510_Pec_Acct_Name_3;
    private DbsField iaaa0510_Pec_Acct_Name_4;
    private DbsField iaaa0510_Pec_Acct_Name_5;
    private DbsField iaaa0510_Pec_Acct_Name_6;
    private DbsField iaaa0510_Pec_Acct_Name_7;
    private DbsField iaaa0510_Pec_Acct_Name_8;
    private DbsField iaaa0510_Pec_Acct_Name_9;
    private DbsField iaaa0510_Pec_Acct_Name_10;
    private DbsField iaaa0510_Pec_Acct_Name_11;
    private DbsField iaaa0510_Pec_Acct_Name_12;
    private DbsField iaaa0510_Pec_Acct_Std_Att;
    private DbsField iaaa0510_Pec_Acct_Eff_Date;
    private DbsField iaaa0510_Pec_Acct_Unit_Rate_Ind;
    private DbsField iaaa0510_Pec_Acct_Std_Nm_Cd;
    private DbsField iaaa0510_Pec_Acct_Std_Alpha_Cd;
    private DbsField iaaa0510_Pec_Acct_Std_Alpha_Seq;
    private DbsField iaaa0510_Pec_Acct_V2_Rt_1;
    private DbsField iaaa0510_Pec_Acct_V2_Rt_2;
    private DbsField iaaa0510_Pec_Acct_V2_Rt_3;
    private DbsField iaaa0510_Pec_Acct_V2_Active_Ind;
    private DbsField iaaa0510_Pec_Acct_V2_Srt_Cd;
    private DbsField iaaa0510_Pec_Acct_Cmpny_Cd;
    private DbsField iaaa0510_Pec_Acct_Rpt_Seq;
    private DbsField iaaa0510_Pnd_Ia_Std_Cmpny_Cd_A;
    private DbsField iaaa0510_Pnd_Ia_Std_Nm_Cd;
    private DbsField iaaa0510_Pnd_Ia_Std_Alpha_Cd;
    private DbsField iaaa0510_Pnd_Ia_Std_Srt_Seq;
    private DbsField iaaa0510_Pnd_Ia_Std_Cmpny_Cd;
    private DbsField iaaa0510_Pnd_Ia_Std_Actve_Acct;
    private DbsField iaaa0510_Pnd_Dc_Std_Nm_Cd;
    private DbsField iaaa0510_Pnd_Dc_Std_Srt_Seq;
    private DbsField iaaa0510_Pnd_Cm_Std_Srt_Seq;
    private DbsField iaaa0510_Pnd_Ia_Rpt_Nm_Cd;
    private DbsField iaaa0510_Pnd_Ia_Rpt_Cmpny_Cd_A;
    private DbsField iaaa0510_Pnd_Cref_Cnt;
    private DbsField iaaa0510_Pnd_Pa_Sel_Cnt;

    public DbsGroup getIaaa0510() { return iaaa0510; }

    public DbsField getIaaa0510_Pec_Nbr_Active_Acct_Att() { return iaaa0510_Pec_Nbr_Active_Acct_Att; }

    public DbsField getIaaa0510_Pec_Nbr_Active_Acct() { return iaaa0510_Pec_Nbr_Active_Acct; }

    public DbsField getIaaa0510_Pec_Acct_Name_Att() { return iaaa0510_Pec_Acct_Name_Att; }

    public DbsField getIaaa0510_Pec_Acct_Name_1() { return iaaa0510_Pec_Acct_Name_1; }

    public DbsField getIaaa0510_Pec_Acct_Name_2() { return iaaa0510_Pec_Acct_Name_2; }

    public DbsField getIaaa0510_Pec_Acct_Name_3() { return iaaa0510_Pec_Acct_Name_3; }

    public DbsField getIaaa0510_Pec_Acct_Name_4() { return iaaa0510_Pec_Acct_Name_4; }

    public DbsField getIaaa0510_Pec_Acct_Name_5() { return iaaa0510_Pec_Acct_Name_5; }

    public DbsField getIaaa0510_Pec_Acct_Name_6() { return iaaa0510_Pec_Acct_Name_6; }

    public DbsField getIaaa0510_Pec_Acct_Name_7() { return iaaa0510_Pec_Acct_Name_7; }

    public DbsField getIaaa0510_Pec_Acct_Name_8() { return iaaa0510_Pec_Acct_Name_8; }

    public DbsField getIaaa0510_Pec_Acct_Name_9() { return iaaa0510_Pec_Acct_Name_9; }

    public DbsField getIaaa0510_Pec_Acct_Name_10() { return iaaa0510_Pec_Acct_Name_10; }

    public DbsField getIaaa0510_Pec_Acct_Name_11() { return iaaa0510_Pec_Acct_Name_11; }

    public DbsField getIaaa0510_Pec_Acct_Name_12() { return iaaa0510_Pec_Acct_Name_12; }

    public DbsField getIaaa0510_Pec_Acct_Std_Att() { return iaaa0510_Pec_Acct_Std_Att; }

    public DbsField getIaaa0510_Pec_Acct_Eff_Date() { return iaaa0510_Pec_Acct_Eff_Date; }

    public DbsField getIaaa0510_Pec_Acct_Unit_Rate_Ind() { return iaaa0510_Pec_Acct_Unit_Rate_Ind; }

    public DbsField getIaaa0510_Pec_Acct_Std_Nm_Cd() { return iaaa0510_Pec_Acct_Std_Nm_Cd; }

    public DbsField getIaaa0510_Pec_Acct_Std_Alpha_Cd() { return iaaa0510_Pec_Acct_Std_Alpha_Cd; }

    public DbsField getIaaa0510_Pec_Acct_Std_Alpha_Seq() { return iaaa0510_Pec_Acct_Std_Alpha_Seq; }

    public DbsField getIaaa0510_Pec_Acct_V2_Rt_1() { return iaaa0510_Pec_Acct_V2_Rt_1; }

    public DbsField getIaaa0510_Pec_Acct_V2_Rt_2() { return iaaa0510_Pec_Acct_V2_Rt_2; }

    public DbsField getIaaa0510_Pec_Acct_V2_Rt_3() { return iaaa0510_Pec_Acct_V2_Rt_3; }

    public DbsField getIaaa0510_Pec_Acct_V2_Active_Ind() { return iaaa0510_Pec_Acct_V2_Active_Ind; }

    public DbsField getIaaa0510_Pec_Acct_V2_Srt_Cd() { return iaaa0510_Pec_Acct_V2_Srt_Cd; }

    public DbsField getIaaa0510_Pec_Acct_Cmpny_Cd() { return iaaa0510_Pec_Acct_Cmpny_Cd; }

    public DbsField getIaaa0510_Pec_Acct_Rpt_Seq() { return iaaa0510_Pec_Acct_Rpt_Seq; }

    public DbsField getIaaa0510_Pnd_Ia_Std_Cmpny_Cd_A() { return iaaa0510_Pnd_Ia_Std_Cmpny_Cd_A; }

    public DbsField getIaaa0510_Pnd_Ia_Std_Nm_Cd() { return iaaa0510_Pnd_Ia_Std_Nm_Cd; }

    public DbsField getIaaa0510_Pnd_Ia_Std_Alpha_Cd() { return iaaa0510_Pnd_Ia_Std_Alpha_Cd; }

    public DbsField getIaaa0510_Pnd_Ia_Std_Srt_Seq() { return iaaa0510_Pnd_Ia_Std_Srt_Seq; }

    public DbsField getIaaa0510_Pnd_Ia_Std_Cmpny_Cd() { return iaaa0510_Pnd_Ia_Std_Cmpny_Cd; }

    public DbsField getIaaa0510_Pnd_Ia_Std_Actve_Acct() { return iaaa0510_Pnd_Ia_Std_Actve_Acct; }

    public DbsField getIaaa0510_Pnd_Dc_Std_Nm_Cd() { return iaaa0510_Pnd_Dc_Std_Nm_Cd; }

    public DbsField getIaaa0510_Pnd_Dc_Std_Srt_Seq() { return iaaa0510_Pnd_Dc_Std_Srt_Seq; }

    public DbsField getIaaa0510_Pnd_Cm_Std_Srt_Seq() { return iaaa0510_Pnd_Cm_Std_Srt_Seq; }

    public DbsField getIaaa0510_Pnd_Ia_Rpt_Nm_Cd() { return iaaa0510_Pnd_Ia_Rpt_Nm_Cd; }

    public DbsField getIaaa0510_Pnd_Ia_Rpt_Cmpny_Cd_A() { return iaaa0510_Pnd_Ia_Rpt_Cmpny_Cd_A; }

    public DbsField getIaaa0510_Pnd_Cref_Cnt() { return iaaa0510_Pnd_Cref_Cnt; }

    public DbsField getIaaa0510_Pnd_Pa_Sel_Cnt() { return iaaa0510_Pnd_Pa_Sel_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaaa0510 = dbsRecord.newGroupInRecord("iaaa0510", "IAAA0510");
        iaaa0510.setParameterOption(ParameterOption.ByReference);
        iaaa0510_Pec_Nbr_Active_Acct_Att = iaaa0510.newFieldInGroup("iaaa0510_Pec_Nbr_Active_Acct_Att", "PEC-NBR-ACTIVE-ACCT-ATT", FieldType.STRING, 1);
        iaaa0510_Pec_Nbr_Active_Acct = iaaa0510.newFieldInGroup("iaaa0510_Pec_Nbr_Active_Acct", "PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        iaaa0510_Pec_Acct_Name_Att = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_Att", "PEC-ACCT-NAME-ATT", FieldType.STRING, 1, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_1 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_1", "PEC-ACCT-NAME-1", FieldType.STRING, 1, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_2 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_2", "PEC-ACCT-NAME-2", FieldType.STRING, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_3 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_3", "PEC-ACCT-NAME-3", FieldType.STRING, 3, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_4 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_4", "PEC-ACCT-NAME-4", FieldType.STRING, 4, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_5 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_5", "PEC-ACCT-NAME-5", FieldType.STRING, 6, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_6 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_6", "PEC-ACCT-NAME-6", FieldType.STRING, 10, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_7 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_7", "PEC-ACCT-NAME-7", FieldType.STRING, 11, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_8 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_8", "PEC-ACCT-NAME-8", FieldType.STRING, 11, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_9 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_9", "PEC-ACCT-NAME-9", FieldType.STRING, 25, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_10 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_10", "PEC-ACCT-NAME-10", FieldType.STRING, 35, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_11 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_11", "PEC-ACCT-NAME-11", FieldType.STRING, 1, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Name_12 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Name_12", "PEC-ACCT-NAME-12", FieldType.STRING, 1, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Std_Att = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Std_Att", "PEC-ACCT-STD-ATT", FieldType.STRING, 1, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Eff_Date = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Eff_Date", "PEC-ACCT-EFF-DATE", FieldType.PACKED_DECIMAL, 8, new 
            DbsArrayController(1,80));
        iaaa0510_Pec_Acct_Unit_Rate_Ind = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Unit_Rate_Ind", "PEC-ACCT-UNIT-RATE-IND", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1,80));
        iaaa0510_Pec_Acct_Std_Nm_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Std_Nm_Cd", "PEC-ACCT-STD-NM-CD", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Std_Alpha_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Std_Alpha_Cd", "PEC-ACCT-STD-ALPHA-CD", FieldType.STRING, 1, 
            new DbsArrayController(1,80));
        iaaa0510_Pec_Acct_Std_Alpha_Seq = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Std_Alpha_Seq", "PEC-ACCT-STD-ALPHA-SEQ", FieldType.STRING, 
            1, new DbsArrayController(1,80));
        iaaa0510_Pec_Acct_V2_Rt_1 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_V2_Rt_1", "PEC-ACCT-V2-RT-1", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_V2_Rt_2 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_V2_Rt_2", "PEC-ACCT-V2-RT-2", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_V2_Rt_3 = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_V2_Rt_3", "PEC-ACCT-V2-RT-3", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_V2_Active_Ind = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_V2_Active_Ind", "PEC-ACCT-V2-ACTIVE-IND", FieldType.STRING, 
            1, new DbsArrayController(1,80));
        iaaa0510_Pec_Acct_V2_Srt_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_V2_Srt_Cd", "PEC-ACCT-V2-SRT-CD", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Cmpny_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Cmpny_Cd", "PEC-ACCT-CMPNY-CD", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pec_Acct_Rpt_Seq = iaaa0510.newFieldArrayInGroup("iaaa0510_Pec_Acct_Rpt_Seq", "PEC-ACCT-RPT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Ia_Std_Cmpny_Cd_A = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Std_Cmpny_Cd_A", "#IA-STD-CMPNY-CD-A", FieldType.STRING, 1, new 
            DbsArrayController(1,80));
        iaaa0510_Pnd_Ia_Std_Nm_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Std_Nm_Cd", "#IA-STD-NM-CD", FieldType.STRING, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Ia_Std_Alpha_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Std_Alpha_Cd", "#IA-STD-ALPHA-CD", FieldType.STRING, 1, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Ia_Std_Srt_Seq = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Std_Srt_Seq", "#IA-STD-SRT-SEQ", FieldType.STRING, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Ia_Std_Cmpny_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Std_Cmpny_Cd", "#IA-STD-CMPNY-CD", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Ia_Std_Actve_Acct = iaaa0510.newFieldInGroup("iaaa0510_Pnd_Ia_Std_Actve_Acct", "#IA-STD-ACTVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        iaaa0510_Pnd_Dc_Std_Nm_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Dc_Std_Nm_Cd", "#DC-STD-NM-CD", FieldType.STRING, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Dc_Std_Srt_Seq = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Dc_Std_Srt_Seq", "#DC-STD-SRT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Cm_Std_Srt_Seq = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Cm_Std_Srt_Seq", "#CM-STD-SRT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Ia_Rpt_Nm_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Rpt_Nm_Cd", "#IA-RPT-NM-CD", FieldType.STRING, 2, new DbsArrayController(1,
            80));
        iaaa0510_Pnd_Ia_Rpt_Cmpny_Cd_A = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Rpt_Cmpny_Cd_A", "#IA-RPT-CMPNY-CD-A", FieldType.STRING, 1, new 
            DbsArrayController(1,80));
        iaaa0510_Pnd_Cref_Cnt = iaaa0510.newFieldInGroup("iaaa0510_Pnd_Cref_Cnt", "#CREF-CNT", FieldType.PACKED_DECIMAL, 3);
        iaaa0510_Pnd_Pa_Sel_Cnt = iaaa0510.newFieldInGroup("iaaa0510_Pnd_Pa_Sel_Cnt", "#PA-SEL-CNT", FieldType.PACKED_DECIMAL, 3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIaaa0510(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

