/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:15 PM
**        * FROM NATURAL LDA     : IAAL585R
************************************************************
**        * FILE NAME            : LdaIaal585r.java
**        * CLASS NAME           : LdaIaal585r
**        * INSTANCE NAME        : LdaIaal585r
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal585r extends DbsRecord
{
    // Properties
    private DbsField pnd_Page_Ctr;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Tot_Cntrct;
    private DbsField pnd_Tot_Current;
    private DbsField pnd_Tot_Future;
    private DbsField pnd_Tot_Cr_Gross;
    private DbsField pnd_Tot_Cr_Dci;
    private DbsField pnd_Tot_Cr_Ovp;
    private DbsField pnd_Tot_Ft_Gross;
    private DbsField pnd_Tot_Ft_Ovp;
    private DbsField pnd_Tot_Past_Due;
    private DbsField pnd_Work_Record_1;
    private DbsGroup pnd_Work_Record_1Redef1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Contract;
    private DbsGroup pnd_Work_Record_1_Pnd_W1_ContractRedef2;
    private DbsField pnd_Work_Record_1_Pnd_W1_Contract_1_7;
    private DbsField pnd_Work_Record_1_Pnd_W1_Contract_8;
    private DbsField pnd_Work_Record_1_Pnd_W1_Payee_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cr_Gross;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cr_Dci;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cr_Ovp;
    private DbsField pnd_Work_Record_1_Pnd_W1_Ft_Gross;
    private DbsField pnd_Work_Record_1_Pnd_W1_Ft_Ovp;
    private DbsField pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ;
    private DbsField pnd_Work_Record_1_Pnd_W1_Past_Due;
    private DbsField pnd_W_Contract;
    private DbsField pnd_Date_Ccyymmdd;
    private DbsGroup pnd_Date_CcyymmddRedef3;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Cc;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Dd;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef4;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Date_NRedef5;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_W_Parm_Date;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_AlphRedef6;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd;
    private DbsField pnd_Fl_Time_Hhiiss_Alph;
    private DbsGroup pnd_Fl_Time_Hhiiss_AlphRedef8;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num;
    private DbsGroup pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef10;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef12;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsGroup pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;

    public DbsField getPnd_Page_Ctr() { return pnd_Page_Ctr; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_J() { return pnd_J; }

    public DbsField getPnd_Tot_Cntrct() { return pnd_Tot_Cntrct; }

    public DbsField getPnd_Tot_Current() { return pnd_Tot_Current; }

    public DbsField getPnd_Tot_Future() { return pnd_Tot_Future; }

    public DbsField getPnd_Tot_Cr_Gross() { return pnd_Tot_Cr_Gross; }

    public DbsField getPnd_Tot_Cr_Dci() { return pnd_Tot_Cr_Dci; }

    public DbsField getPnd_Tot_Cr_Ovp() { return pnd_Tot_Cr_Ovp; }

    public DbsField getPnd_Tot_Ft_Gross() { return pnd_Tot_Ft_Gross; }

    public DbsField getPnd_Tot_Ft_Ovp() { return pnd_Tot_Ft_Ovp; }

    public DbsField getPnd_Tot_Past_Due() { return pnd_Tot_Past_Due; }

    public DbsField getPnd_Work_Record_1() { return pnd_Work_Record_1; }

    public DbsGroup getPnd_Work_Record_1Redef1() { return pnd_Work_Record_1Redef1; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Contract() { return pnd_Work_Record_1_Pnd_W1_Contract; }

    public DbsGroup getPnd_Work_Record_1_Pnd_W1_ContractRedef2() { return pnd_Work_Record_1_Pnd_W1_ContractRedef2; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Contract_1_7() { return pnd_Work_Record_1_Pnd_W1_Contract_1_7; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Contract_8() { return pnd_Work_Record_1_Pnd_W1_Contract_8; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Payee_Cde() { return pnd_Work_Record_1_Pnd_W1_Payee_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cr_Gross() { return pnd_Work_Record_1_Pnd_W1_Cr_Gross; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cr_Dci() { return pnd_Work_Record_1_Pnd_W1_Cr_Dci; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cr_Ovp() { return pnd_Work_Record_1_Pnd_W1_Cr_Ovp; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Ft_Gross() { return pnd_Work_Record_1_Pnd_W1_Ft_Gross; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Ft_Ovp() { return pnd_Work_Record_1_Pnd_W1_Ft_Ovp; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ() { return pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Past_Due() { return pnd_Work_Record_1_Pnd_W1_Past_Due; }

    public DbsField getPnd_W_Contract() { return pnd_W_Contract; }

    public DbsField getPnd_Date_Ccyymmdd() { return pnd_Date_Ccyymmdd; }

    public DbsGroup getPnd_Date_CcyymmddRedef3() { return pnd_Date_CcyymmddRedef3; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Cc() { return pnd_Date_Ccyymmdd_Pnd_Date_Cc; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Yy() { return pnd_Date_Ccyymmdd_Pnd_Date_Yy; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Mm() { return pnd_Date_Ccyymmdd_Pnd_Date_Mm; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Dd() { return pnd_Date_Ccyymmdd_Pnd_Date_Dd; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef4() { return iaa_Parm_Card_Pnd_Parm_DateRedef4; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Date_NRedef5() { return iaa_Parm_Card_Pnd_Parm_Date_NRedef5; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DbsField getPnd_W_Parm_Date() { return pnd_W_Parm_Date; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph() { return pnd_Fl_Date_Yyyymmdd_Alph; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_AlphRedef6() { return pnd_Fl_Date_Yyyymmdd_AlphRedef6; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph() { return pnd_Fl_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_AlphRedef8() { return pnd_Fl_Time_Hhiiss_AlphRedef8; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef10() { return pnd_Sy_Date_Yymmdd_AlphRedef10; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef12() { return pnd_Sy_Time_Hhiiss_AlphRedef12; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsField getPnd_Sys_Time() { return pnd_Sys_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Page_Ctr = newFieldInRecord("pnd_Page_Ctr", "#PAGE-CTR", FieldType.NUMERIC, 3);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        pnd_J = newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);

        pnd_Tot_Cntrct = newFieldInRecord("pnd_Tot_Cntrct", "#TOT-CNTRCT", FieldType.NUMERIC, 4);

        pnd_Tot_Current = newFieldInRecord("pnd_Tot_Current", "#TOT-CURRENT", FieldType.NUMERIC, 4);

        pnd_Tot_Future = newFieldInRecord("pnd_Tot_Future", "#TOT-FUTURE", FieldType.NUMERIC, 4);

        pnd_Tot_Cr_Gross = newFieldInRecord("pnd_Tot_Cr_Gross", "#TOT-CR-GROSS", FieldType.DECIMAL, 12,2);

        pnd_Tot_Cr_Dci = newFieldInRecord("pnd_Tot_Cr_Dci", "#TOT-CR-DCI", FieldType.DECIMAL, 12,2);

        pnd_Tot_Cr_Ovp = newFieldInRecord("pnd_Tot_Cr_Ovp", "#TOT-CR-OVP", FieldType.DECIMAL, 12,2);

        pnd_Tot_Ft_Gross = newFieldInRecord("pnd_Tot_Ft_Gross", "#TOT-FT-GROSS", FieldType.DECIMAL, 12,2);

        pnd_Tot_Ft_Ovp = newFieldInRecord("pnd_Tot_Ft_Ovp", "#TOT-FT-OVP", FieldType.DECIMAL, 12,2);

        pnd_Tot_Past_Due = newFieldInRecord("pnd_Tot_Past_Due", "#TOT-PAST-DUE", FieldType.DECIMAL, 12,2);

        pnd_Work_Record_1 = newFieldInRecord("pnd_Work_Record_1", "#WORK-RECORD-1", FieldType.STRING, 80);
        pnd_Work_Record_1Redef1 = newGroupInRecord("pnd_Work_Record_1Redef1", "Redefines", pnd_Work_Record_1);
        pnd_Work_Record_1_Pnd_W1_Contract = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Contract", "#W1-CONTRACT", FieldType.STRING, 
            10);
        pnd_Work_Record_1_Pnd_W1_ContractRedef2 = pnd_Work_Record_1Redef1.newGroupInGroup("pnd_Work_Record_1_Pnd_W1_ContractRedef2", "Redefines", pnd_Work_Record_1_Pnd_W1_Contract);
        pnd_Work_Record_1_Pnd_W1_Contract_1_7 = pnd_Work_Record_1_Pnd_W1_ContractRedef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Contract_1_7", "#W1-CONTRACT-1-7", 
            FieldType.STRING, 7);
        pnd_Work_Record_1_Pnd_W1_Contract_8 = pnd_Work_Record_1_Pnd_W1_ContractRedef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Contract_8", "#W1-CONTRACT-8", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Payee_Cde = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Payee_Cde", "#W1-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record_1_Pnd_W1_Cr_Gross = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cr_Gross", "#W1-CR-GROSS", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Cr_Dci = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cr_Dci", "#W1-CR-DCI", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Cr_Ovp = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cr_Ovp", "#W1-CR-OVP", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Ft_Gross = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Ft_Gross", "#W1-FT-GROSS", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Ft_Ovp = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Ft_Ovp", "#W1-FT-OVP", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ", "#W1-INSTLLMNT-TYP", 
            FieldType.STRING, 4);
        pnd_Work_Record_1_Pnd_W1_Past_Due = pnd_Work_Record_1Redef1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Past_Due", "#W1-PAST-DUE", FieldType.DECIMAL, 
            9,2);

        pnd_W_Contract = newFieldInRecord("pnd_W_Contract", "#W-CONTRACT", FieldType.STRING, 9);

        pnd_Date_Ccyymmdd = newFieldInRecord("pnd_Date_Ccyymmdd", "#DATE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Date_CcyymmddRedef3 = newGroupInRecord("pnd_Date_CcyymmddRedef3", "Redefines", pnd_Date_Ccyymmdd);
        pnd_Date_Ccyymmdd_Pnd_Date_Cc = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Yy = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Mm = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Dd = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef4 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef4", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_Date_NRedef5 = iaa_Parm_Card_Pnd_Parm_DateRedef4.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Date_NRedef5", "Redefines", iaa_Parm_Card_Pnd_Parm_Date_N);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_W_Parm_Date = newFieldInRecord("pnd_W_Parm_Date", "#W-PARM-DATE", FieldType.STRING, 8);

        pnd_Fl_Date_Yyyymmdd_Alph = newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Fl_Date_Yyyymmdd_AlphRedef6 = newGroupInRecord("pnd_Fl_Date_Yyyymmdd_AlphRedef6", "Redefines", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7 = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7", 
            "Redefines", pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd", 
            "#FL-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Fl_Time_Hhiiss_Alph = newFieldInRecord("pnd_Fl_Time_Hhiiss_Alph", "#FL-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Fl_Time_Hhiiss_AlphRedef8 = newGroupInRecord("pnd_Fl_Time_Hhiiss_AlphRedef8", "Redefines", pnd_Fl_Time_Hhiiss_Alph);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num = pnd_Fl_Time_Hhiiss_AlphRedef8.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num", 
            "#FL-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9 = pnd_Fl_Time_Hhiiss_AlphRedef8.newGroupInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9", 
            "Redefines", pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh", 
            "#FL-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii", 
            "#FL-TIME-II", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss", 
            "#FL-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef10 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef10", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef10.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11 = pnd_Sy_Date_Yymmdd_AlphRedef10.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef12 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef12", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef12.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13 = pnd_Sy_Time_Hhiiss_AlphRedef12.newGroupInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13", 
            "Redefines", pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh", 
            "#SY-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii", 
            "#SY-TIME-II", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss", 
            "#SY-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        pnd_Sys_Time = newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);

        this.setRecordName("LdaIaal585r");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal585r() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
