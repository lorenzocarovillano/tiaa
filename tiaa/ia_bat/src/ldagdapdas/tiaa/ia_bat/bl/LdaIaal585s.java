/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:16 PM
**        * FROM NATURAL LDA     : IAAL585S
************************************************************
**        * FILE NAME            : LdaIaal585s.java
**        * CLASS NAME           : LdaIaal585s
**        * INSTANCE NAME        : LdaIaal585s
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal585s extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_audit_View;
    private DbsField audit_View_Paudit_Id_Nbr;
    private DbsField audit_View_Paudit_Tax_Id_Nbr;
    private DbsField audit_View_Paudit_Ppcn_Nbr;
    private DbsField audit_View_Paudit_Payee_Cde;
    private DbsGroup audit_View_Paudit_Payee_CdeRedef1;
    private DbsField audit_View_Pnd_Paudit_Payee_Cde_A;
    private DbsField audit_View_Paudit_Timestamp;
    private DbsField audit_View_Paudit_Status_Timestamp;
    private DbsField audit_View_Paudit_Pymnt_Dte;
    private DbsField audit_View_Paudit_Type_Req_Ind;
    private DbsField audit_View_Paudit_Hold_Cde;
    private DbsField audit_View_Count_Castpaudit_Installments;
    private DbsGroup audit_View_Paudit_Installments;
    private DbsField audit_View_Paudit_Instllmnt_Typ;
    private DbsField audit_View_Paudit_Instllmnt_Gross;
    private DbsField audit_View_Paudit_Instllmnt_Guar;
    private DbsField audit_View_Paudit_Instllmnt_Divd;
    private DbsField audit_View_Paudit_Instllmnt_Ivc;
    private DbsField audit_View_Paudit_Instllmnt_Dci;
    private DbsField audit_View_Paudit_Instllmnt_Units;
    private DbsField audit_View_Paudit_Instllmnt_Ovrpymnt;
    private DbsField audit_View_Paudit_Instllmnt_Futr_Ind;
    private DbsField audit_View_Paudit_Inv_Acct_Cde;
    private DbsField pnd_Audit_Reads;
    private DbsField pnd_Audit_Time_Selects;
    private DbsField pnd_Work_Records_Written;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Num;
    private DbsField pnd_Num_Minus_One;
    private DbsField pnd_Cr_Gross;
    private DbsField pnd_Cr_Dci;
    private DbsField pnd_Cr_Ovp;
    private DbsField pnd_Work_Record_1;
    private DbsGroup pnd_Work_Record_1Redef2;
    private DbsField pnd_Work_Record_1_Pnd_W1_Contract;
    private DbsField pnd_Work_Record_1_Pnd_W1_Payee_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cr_Gross;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cr_Dci;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cr_Ovp;
    private DbsField pnd_Work_Record_1_Pnd_W1_Ft_Gross;
    private DbsField pnd_Work_Record_1_Pnd_W1_Ft_Ovp;
    private DbsField pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ;
    private DbsField pnd_Work_Record_1_Pnd_W1_Past_Due;
    private DbsField pnd_Date_Ccyymmdd;
    private DbsGroup pnd_Date_CcyymmddRedef3;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Cc;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Dd;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef4;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Date_NRedef5;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_AlphRedef6;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd;
    private DbsField pnd_Fl_Time_Hhiiss_Alph;
    private DbsGroup pnd_Fl_Time_Hhiiss_AlphRedef8;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num;
    private DbsGroup pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef10;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef12;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsGroup pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;

    public DataAccessProgramView getVw_audit_View() { return vw_audit_View; }

    public DbsField getAudit_View_Paudit_Id_Nbr() { return audit_View_Paudit_Id_Nbr; }

    public DbsField getAudit_View_Paudit_Tax_Id_Nbr() { return audit_View_Paudit_Tax_Id_Nbr; }

    public DbsField getAudit_View_Paudit_Ppcn_Nbr() { return audit_View_Paudit_Ppcn_Nbr; }

    public DbsField getAudit_View_Paudit_Payee_Cde() { return audit_View_Paudit_Payee_Cde; }

    public DbsGroup getAudit_View_Paudit_Payee_CdeRedef1() { return audit_View_Paudit_Payee_CdeRedef1; }

    public DbsField getAudit_View_Pnd_Paudit_Payee_Cde_A() { return audit_View_Pnd_Paudit_Payee_Cde_A; }

    public DbsField getAudit_View_Paudit_Timestamp() { return audit_View_Paudit_Timestamp; }

    public DbsField getAudit_View_Paudit_Status_Timestamp() { return audit_View_Paudit_Status_Timestamp; }

    public DbsField getAudit_View_Paudit_Pymnt_Dte() { return audit_View_Paudit_Pymnt_Dte; }

    public DbsField getAudit_View_Paudit_Type_Req_Ind() { return audit_View_Paudit_Type_Req_Ind; }

    public DbsField getAudit_View_Paudit_Hold_Cde() { return audit_View_Paudit_Hold_Cde; }

    public DbsField getAudit_View_Count_Castpaudit_Installments() { return audit_View_Count_Castpaudit_Installments; }

    public DbsGroup getAudit_View_Paudit_Installments() { return audit_View_Paudit_Installments; }

    public DbsField getAudit_View_Paudit_Instllmnt_Typ() { return audit_View_Paudit_Instllmnt_Typ; }

    public DbsField getAudit_View_Paudit_Instllmnt_Gross() { return audit_View_Paudit_Instllmnt_Gross; }

    public DbsField getAudit_View_Paudit_Instllmnt_Guar() { return audit_View_Paudit_Instllmnt_Guar; }

    public DbsField getAudit_View_Paudit_Instllmnt_Divd() { return audit_View_Paudit_Instllmnt_Divd; }

    public DbsField getAudit_View_Paudit_Instllmnt_Ivc() { return audit_View_Paudit_Instllmnt_Ivc; }

    public DbsField getAudit_View_Paudit_Instllmnt_Dci() { return audit_View_Paudit_Instllmnt_Dci; }

    public DbsField getAudit_View_Paudit_Instllmnt_Units() { return audit_View_Paudit_Instllmnt_Units; }

    public DbsField getAudit_View_Paudit_Instllmnt_Ovrpymnt() { return audit_View_Paudit_Instllmnt_Ovrpymnt; }

    public DbsField getAudit_View_Paudit_Instllmnt_Futr_Ind() { return audit_View_Paudit_Instllmnt_Futr_Ind; }

    public DbsField getAudit_View_Paudit_Inv_Acct_Cde() { return audit_View_Paudit_Inv_Acct_Cde; }

    public DbsField getPnd_Audit_Reads() { return pnd_Audit_Reads; }

    public DbsField getPnd_Audit_Time_Selects() { return pnd_Audit_Time_Selects; }

    public DbsField getPnd_Work_Records_Written() { return pnd_Work_Records_Written; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_J() { return pnd_J; }

    public DbsField getPnd_Num() { return pnd_Num; }

    public DbsField getPnd_Num_Minus_One() { return pnd_Num_Minus_One; }

    public DbsField getPnd_Cr_Gross() { return pnd_Cr_Gross; }

    public DbsField getPnd_Cr_Dci() { return pnd_Cr_Dci; }

    public DbsField getPnd_Cr_Ovp() { return pnd_Cr_Ovp; }

    public DbsField getPnd_Work_Record_1() { return pnd_Work_Record_1; }

    public DbsGroup getPnd_Work_Record_1Redef2() { return pnd_Work_Record_1Redef2; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Contract() { return pnd_Work_Record_1_Pnd_W1_Contract; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Payee_Cde() { return pnd_Work_Record_1_Pnd_W1_Payee_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cr_Gross() { return pnd_Work_Record_1_Pnd_W1_Cr_Gross; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cr_Dci() { return pnd_Work_Record_1_Pnd_W1_Cr_Dci; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cr_Ovp() { return pnd_Work_Record_1_Pnd_W1_Cr_Ovp; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Ft_Gross() { return pnd_Work_Record_1_Pnd_W1_Ft_Gross; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Ft_Ovp() { return pnd_Work_Record_1_Pnd_W1_Ft_Ovp; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ() { return pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Past_Due() { return pnd_Work_Record_1_Pnd_W1_Past_Due; }

    public DbsField getPnd_Date_Ccyymmdd() { return pnd_Date_Ccyymmdd; }

    public DbsGroup getPnd_Date_CcyymmddRedef3() { return pnd_Date_CcyymmddRedef3; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Cc() { return pnd_Date_Ccyymmdd_Pnd_Date_Cc; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Yy() { return pnd_Date_Ccyymmdd_Pnd_Date_Yy; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Mm() { return pnd_Date_Ccyymmdd_Pnd_Date_Mm; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Dd() { return pnd_Date_Ccyymmdd_Pnd_Date_Dd; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef4() { return iaa_Parm_Card_Pnd_Parm_DateRedef4; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Date_NRedef5() { return iaa_Parm_Card_Pnd_Parm_Date_NRedef5; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph() { return pnd_Fl_Date_Yyyymmdd_Alph; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_AlphRedef6() { return pnd_Fl_Date_Yyyymmdd_AlphRedef6; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph() { return pnd_Fl_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_AlphRedef8() { return pnd_Fl_Time_Hhiiss_AlphRedef8; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef10() { return pnd_Sy_Date_Yymmdd_AlphRedef10; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef12() { return pnd_Sy_Time_Hhiiss_AlphRedef12; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsField getPnd_Sys_Time() { return pnd_Sys_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_audit_View = new DataAccessProgramView(new NameInfo("vw_audit_View", "AUDIT-VIEW"), "IAA_DC_PMT_AUDIT", "IA_DEATH_CLAIMS", DdmPeriodicGroups.getInstance().getGroups("IAA_DC_PMT_AUDIT"));
        audit_View_Paudit_Id_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Id_Nbr", "PAUDIT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PAUDIT_ID_NBR");
        audit_View_Paudit_Tax_Id_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Tax_Id_Nbr", "PAUDIT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PAUDIT_TAX_ID_NBR");
        audit_View_Paudit_Ppcn_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ppcn_Nbr", "PAUDIT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "PAUDIT_PPCN_NBR");
        audit_View_Paudit_Payee_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Payee_Cde", "PAUDIT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PAUDIT_PAYEE_CDE");
        audit_View_Paudit_Payee_CdeRedef1 = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Payee_CdeRedef1", "Redefines", audit_View_Paudit_Payee_Cde);
        audit_View_Pnd_Paudit_Payee_Cde_A = audit_View_Paudit_Payee_CdeRedef1.newFieldInGroup("audit_View_Pnd_Paudit_Payee_Cde_A", "#PAUDIT-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        audit_View_Paudit_Timestamp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Timestamp", "PAUDIT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "PAUDIT_TIMESTAMP");
        audit_View_Paudit_Status_Timestamp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Status_Timestamp", "PAUDIT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PAUDIT_STATUS_TIMESTAMP");
        audit_View_Paudit_Pymnt_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Pymnt_Dte", "PAUDIT-PYMNT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_PYMNT_DTE");
        audit_View_Paudit_Type_Req_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Type_Req_Ind", "PAUDIT-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PAUDIT_TYPE_REQ_IND");
        audit_View_Paudit_Hold_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Hold_Cde", "PAUDIT-HOLD-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "PAUDIT_HOLD_CDE");
        audit_View_Count_Castpaudit_Installments = vw_audit_View.getRecord().newFieldInGroup("audit_View_Count_Castpaudit_Installments", "C*PAUDIT-INSTALLMENTS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Installments = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Installments", "PAUDIT-INSTALLMENTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Typ = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Typ", "PAUDIT-INSTLLMNT-TYP", 
            FieldType.STRING, 4, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_TYP", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Gross = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Gross", "PAUDIT-INSTLLMNT-GROSS", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_GROSS", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Guar = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Guar", "PAUDIT-INSTLLMNT-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_GUAR", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Divd = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Divd", "PAUDIT-INSTLLMNT-DIVD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DIVD", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Ivc = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Ivc", "PAUDIT-INSTLLMNT-IVC", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_IVC", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Dci = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Dci", "PAUDIT-INSTLLMNT-DCI", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DCI", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Units = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Units", "PAUDIT-INSTLLMNT-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_UNITS", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Ovrpymnt = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Ovrpymnt", "PAUDIT-INSTLLMNT-OVRPYMNT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_OVRPYMNT", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Futr_Ind = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Futr_Ind", "PAUDIT-INSTLLMNT-FUTR-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_FUTR_IND", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Inv_Acct_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Inv_Acct_Cde", "PAUDIT-INV-ACCT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_INV_ACCT_CDE");

        pnd_Audit_Reads = newFieldInRecord("pnd_Audit_Reads", "#AUDIT-READS", FieldType.NUMERIC, 9);

        pnd_Audit_Time_Selects = newFieldInRecord("pnd_Audit_Time_Selects", "#AUDIT-TIME-SELECTS", FieldType.NUMERIC, 9);

        pnd_Work_Records_Written = newFieldInRecord("pnd_Work_Records_Written", "#WORK-RECORDS-WRITTEN", FieldType.NUMERIC, 9);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        pnd_J = newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);

        pnd_Num = newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 2);

        pnd_Num_Minus_One = newFieldInRecord("pnd_Num_Minus_One", "#NUM-MINUS-ONE", FieldType.NUMERIC, 2);

        pnd_Cr_Gross = newFieldInRecord("pnd_Cr_Gross", "#CR-GROSS", FieldType.DECIMAL, 11,2);

        pnd_Cr_Dci = newFieldInRecord("pnd_Cr_Dci", "#CR-DCI", FieldType.DECIMAL, 11,2);

        pnd_Cr_Ovp = newFieldInRecord("pnd_Cr_Ovp", "#CR-OVP", FieldType.DECIMAL, 11,2);

        pnd_Work_Record_1 = newFieldInRecord("pnd_Work_Record_1", "#WORK-RECORD-1", FieldType.STRING, 80);
        pnd_Work_Record_1Redef2 = newGroupInRecord("pnd_Work_Record_1Redef2", "Redefines", pnd_Work_Record_1);
        pnd_Work_Record_1_Pnd_W1_Contract = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Contract", "#W1-CONTRACT", FieldType.STRING, 
            10);
        pnd_Work_Record_1_Pnd_W1_Payee_Cde = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Payee_Cde", "#W1-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record_1_Pnd_W1_Cr_Gross = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cr_Gross", "#W1-CR-GROSS", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Cr_Dci = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cr_Dci", "#W1-CR-DCI", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Cr_Ovp = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cr_Ovp", "#W1-CR-OVP", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Ft_Gross = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Ft_Gross", "#W1-FT-GROSS", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Ft_Ovp = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Ft_Ovp", "#W1-FT-OVP", FieldType.DECIMAL, 
            11,2);
        pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Instllmnt_Typ", "#W1-INSTLLMNT-TYP", 
            FieldType.STRING, 4);
        pnd_Work_Record_1_Pnd_W1_Past_Due = pnd_Work_Record_1Redef2.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Past_Due", "#W1-PAST-DUE", FieldType.DECIMAL, 
            9,2);

        pnd_Date_Ccyymmdd = newFieldInRecord("pnd_Date_Ccyymmdd", "#DATE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Date_CcyymmddRedef3 = newGroupInRecord("pnd_Date_CcyymmddRedef3", "Redefines", pnd_Date_Ccyymmdd);
        pnd_Date_Ccyymmdd_Pnd_Date_Cc = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Yy = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Mm = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Dd = pnd_Date_CcyymmddRedef3.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef4 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef4", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_Date_NRedef5 = iaa_Parm_Card_Pnd_Parm_DateRedef4.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Date_NRedef5", "Redefines", iaa_Parm_Card_Pnd_Parm_Date_N);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Fl_Date_Yyyymmdd_Alph = newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Fl_Date_Yyyymmdd_AlphRedef6 = newGroupInRecord("pnd_Fl_Date_Yyyymmdd_AlphRedef6", "Redefines", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7 = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7", 
            "Redefines", pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd", 
            "#FL-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Fl_Time_Hhiiss_Alph = newFieldInRecord("pnd_Fl_Time_Hhiiss_Alph", "#FL-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Fl_Time_Hhiiss_AlphRedef8 = newGroupInRecord("pnd_Fl_Time_Hhiiss_AlphRedef8", "Redefines", pnd_Fl_Time_Hhiiss_Alph);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num = pnd_Fl_Time_Hhiiss_AlphRedef8.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num", 
            "#FL-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9 = pnd_Fl_Time_Hhiiss_AlphRedef8.newGroupInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9", 
            "Redefines", pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh", 
            "#FL-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii", 
            "#FL-TIME-II", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss", 
            "#FL-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef10 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef10", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef10.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11 = pnd_Sy_Date_Yymmdd_AlphRedef10.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef12 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef12", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef12.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13 = pnd_Sy_Time_Hhiiss_AlphRedef12.newGroupInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13", 
            "Redefines", pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh", 
            "#SY-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii", 
            "#SY-TIME-II", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss", 
            "#SY-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        pnd_Sys_Time = newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        vw_audit_View.setUniquePeList();

        this.setRecordName("LdaIaal585s");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_audit_View.reset();
    }

    // Constructor
    public LdaIaal585s() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
