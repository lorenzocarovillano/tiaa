/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:08:26 PM
**        * FROM NATURAL LDA     : NAZLDARC
************************************************************
**        * FILE NAME            : LdaNazldarc.java
**        * CLASS NAME           : LdaNazldarc
**        * INSTANCE NAME        : LdaNazldarc
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaNazldarc extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_naz_Da_Rslt_Ddm_View;
    private DbsField naz_Da_Rslt_Ddm_View_Rqst_Id;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Sqnce_Nbr;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Stts_Cde;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Da_Tiaa_Nbr;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Da_Cref_Nbr;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Da_Gd_Cntrct_Ind;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Lst_Actvty_Dte;
    private DbsGroup naz_Da_Rslt_Ddm_View_Nad_Rqst_Info;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Cde;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Stts_Cde;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Typ;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Qty;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Nbr_Units;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Typ;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Qty;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Nbr_Units;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Typ;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Qty;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Stndrd_Annl_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Accum_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Nbr_Units;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Accum_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Nbr_Units;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Acct_Cref_Rate_Cde;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Tiaa_Ivc_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Tiaa_Rtb_Ivc_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Cref_Ivc_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Cref_Rtb_Ivc_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Dte;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Tme;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Dte;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Tme;
    private DbsGroup naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rate_Cde;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rtb_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Grd_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Stndrd_Actl_Amt;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Ia_Rate_Cde;
    private DbsField naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Opn_Accum_Amt;

    public DataAccessProgramView getVw_naz_Da_Rslt_Ddm_View() { return vw_naz_Da_Rslt_Ddm_View; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Rqst_Id() { return naz_Da_Rslt_Ddm_View_Rqst_Id; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Sqnce_Nbr() { return naz_Da_Rslt_Ddm_View_Nad_Sqnce_Nbr; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Stts_Cde() { return naz_Da_Rslt_Ddm_View_Nad_Stts_Cde; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Da_Tiaa_Nbr() { return naz_Da_Rslt_Ddm_View_Nad_Da_Tiaa_Nbr; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Da_Cref_Nbr() { return naz_Da_Rslt_Ddm_View_Nad_Da_Cref_Nbr; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Da_Gd_Cntrct_Ind() { return naz_Da_Rslt_Ddm_View_Nad_Da_Gd_Cntrct_Ind; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Lst_Actvty_Dte() { return naz_Da_Rslt_Ddm_View_Nad_Lst_Actvty_Dte; }

    public DbsGroup getNaz_Da_Rslt_Ddm_View_Nad_Rqst_Info() { return naz_Da_Rslt_Ddm_View_Nad_Rqst_Info; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Cde() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Cde; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Stts_Cde() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Stts_Cde; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Typ() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Typ; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Qty() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Qty; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Nbr_Units() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Nbr_Units; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Typ() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Typ; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Qty() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Qty; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Nbr_Units() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Nbr_Units; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Typ() { return naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Typ; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Qty() { return naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Qty; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Stndrd_Annl_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Stndrd_Annl_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Accum_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Accum_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Nbr_Units() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Nbr_Units; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Accum_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Accum_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Nbr_Units() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Nbr_Units; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Acct_Cref_Rate_Cde() { return naz_Da_Rslt_Ddm_View_Nad_Acct_Cref_Rate_Cde; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Tiaa_Ivc_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Tiaa_Ivc_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Tiaa_Rtb_Ivc_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Tiaa_Rtb_Ivc_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Cref_Ivc_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Cref_Ivc_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Cref_Rtb_Ivc_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Cref_Rtb_Ivc_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Dte() { return naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Dte; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Tme() { return naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Tme; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Dte() { return naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Dte; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Tme() { return naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Tme; }

    public DbsGroup getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rate_Cde() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rate_Cde; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rtb_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rtb_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Grd_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Grd_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Stndrd_Actl_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Stndrd_Actl_Amt; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Ia_Rate_Cde() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Ia_Rate_Cde; }

    public DbsField getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Opn_Accum_Amt() { return naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Opn_Accum_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_naz_Da_Rslt_Ddm_View = new DataAccessProgramView(new NameInfo("vw_naz_Da_Rslt_Ddm_View", "NAZ-DA-RSLT-DDM-VIEW"), "NAZ_DA_RSLT_DDM", "NAZ_DA_RSLT_RCRD", 
            DdmPeriodicGroups.getInstance().getGroups("NAZ_DA_RSLT_DDM"));
        naz_Da_Rslt_Ddm_View_Rqst_Id = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "RQST_ID");
        naz_Da_Rslt_Ddm_View_Nad_Sqnce_Nbr = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Sqnce_Nbr", "NAD-SQNCE-NBR", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NAD_SQNCE_NBR");
        naz_Da_Rslt_Ddm_View_Nad_Stts_Cde = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Stts_Cde", "NAD-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "NAD_STTS_CDE");
        naz_Da_Rslt_Ddm_View_Nad_Da_Tiaa_Nbr = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Da_Tiaa_Nbr", "NAD-DA-TIAA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NAD_DA_TIAA_NBR");
        naz_Da_Rslt_Ddm_View_Nad_Da_Cref_Nbr = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Da_Cref_Nbr", "NAD-DA-CREF-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NAD_DA_CREF_NBR");
        naz_Da_Rslt_Ddm_View_Nad_Da_Gd_Cntrct_Ind = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Da_Gd_Cntrct_Ind", "NAD-DA-GD-CNTRCT-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "NAD_DA_GD_CNTRCT_IND");
        naz_Da_Rslt_Ddm_View_Nad_Lst_Actvty_Dte = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Lst_Actvty_Dte", "NAD-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAD_LST_ACTVTY_DTE");
        naz_Da_Rslt_Ddm_View_Nad_Rqst_Info = vw_naz_Da_Rslt_Ddm_View.getRecord().newGroupInGroup("naz_Da_Rslt_Ddm_View_Nad_Rqst_Info", "NAD-RQST-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Cde = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Cde", "NAD-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_CDE", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Stts_Cde = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Stts_Cde", "NAD-ACCT-STTS-CDE", 
            FieldType.STRING, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_STTS_CDE", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Typ = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Typ", "NAD-ACCT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_TYP", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Qty = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Qty", "NAD-ACCT-QTY", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_QTY", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Amt", "NAD-ACCT-ACTL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_ACTL_AMT", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Nbr_Units = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Nbr_Units", 
            "NAD-ACCT-ACTL-NBR-UNITS", FieldType.NUMERIC, 10, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_ACTL_NBR_UNITS", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Typ = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Typ", "NAD-ACCT-RTB-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_RTB_TYP", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Qty = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Qty", "NAD-ACCT-RTB-QTY", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_RTB_QTY", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Amt", 
            "NAD-ACCT-RTB-ACTL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_RTB_ACTL_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Nbr_Units = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Nbr_Units", 
            "NAD-ACCT-RTB-ACTL-NBR-UNITS", FieldType.NUMERIC, 10, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_RTB_ACTL_NBR_UNITS", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Typ = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Typ", "NAD-GRD-MNTHLY-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_GRD_MNTHLY_TYP", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Qty = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Qty", "NAD-GRD-MNTHLY-QTY", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_GRD_MNTHLY_QTY", "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Actl_Amt", 
            "NAD-GRD-MNTHLY-ACTL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_GRD_MNTHLY_ACTL_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Stndrd_Annl_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Stndrd_Annl_Actl_Amt", 
            "NAD-STNDRD-ANNL-ACTL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_STNDRD_ANNL_ACTL_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Accum_Amt = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Accum_Amt", 
            "NAD-ACCT-OPN-ACCUM-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_OPN_ACCUM_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Nbr_Units = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Opn_Nbr_Units", 
            "NAD-ACCT-OPN-NBR-UNITS", FieldType.NUMERIC, 10, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_OPN_NBR_UNITS", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Accum_Amt = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Accum_Amt", 
            "NAD-ACCT-CLO-ACCUM-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_CLO_ACCUM_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Nbr_Units = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Clo_Nbr_Units", 
            "NAD-ACCT-CLO-NBR-UNITS", FieldType.NUMERIC, 10, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_CLO_NBR_UNITS", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Acct_Cref_Rate_Cde = naz_Da_Rslt_Ddm_View_Nad_Rqst_Info.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Acct_Cref_Rate_Cde", 
            "NAD-ACCT-CREF-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_ACCT_CREF_RATE_CDE", 
            "NAZ_DA_RSLT_RCRD_NAD_RQST_INFO");
        naz_Da_Rslt_Ddm_View_Nad_Tiaa_Ivc_Amt = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Tiaa_Ivc_Amt", "NAD-TIAA-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "NAD_TIAA_IVC_AMT");
        naz_Da_Rslt_Ddm_View_Nad_Tiaa_Rtb_Ivc_Amt = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Tiaa_Rtb_Ivc_Amt", "NAD-TIAA-RTB-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "NAD_TIAA_RTB_IVC_AMT");
        naz_Da_Rslt_Ddm_View_Nad_Cref_Ivc_Amt = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Cref_Ivc_Amt", "NAD-CREF-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "NAD_CREF_IVC_AMT");
        naz_Da_Rslt_Ddm_View_Nad_Cref_Rtb_Ivc_Amt = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Cref_Rtb_Ivc_Amt", "NAD-CREF-RTB-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "NAD_CREF_RTB_IVC_AMT");
        naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Dte = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Dte", 
            "NAD-MTRTY-NON-PRM-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "NAD_MTRTY_NON_PRM_DTE");
        naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Tme = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Mtrty_Non_Prm_Tme", 
            "NAD-MTRTY-NON-PRM-TME", FieldType.TIME, RepeatingFieldStrategy.None, "NAD_MTRTY_NON_PRM_TME");
        naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Dte = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Dte", "NAD-RTB-NON-PRM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAD_RTB_NON_PRM_DTE");
        naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Tme = vw_naz_Da_Rslt_Ddm_View.getRecord().newFieldInGroup("naz_Da_Rslt_Ddm_View_Nad_Rtb_Non_Prm_Tme", "NAD-RTB-NON-PRM-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "NAD_RTB_NON_PRM_TME");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data = vw_naz_Da_Rslt_Ddm_View.getRecord().newGroupInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data", "NAD-DTL-TIAA-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rate_Cde = naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rate_Cde", 
            "NAD-DTL-TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_DTL_TIAA_RATE_CDE", 
            "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Actl_Amt", 
            "NAD-DTL-TIAA-ACTL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_DTL_TIAA_ACTL_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rtb_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rtb_Actl_Amt", 
            "NAD-DTL-TIAA-RTB-ACTL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_DTL_TIAA_RTB_ACTL_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Grd_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Grd_Actl_Amt", 
            "NAD-DTL-TIAA-GRD-ACTL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_DTL_TIAA_GRD_ACTL_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Stndrd_Actl_Amt = naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Stndrd_Actl_Amt", 
            "NAD-DTL-TIAA-STNDRD-ACTL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_DTL_TIAA_STNDRD_ACTL_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Ia_Rate_Cde = naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Ia_Rate_Cde", 
            "NAD-DTL-TIAA-IA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_DTL_TIAA_IA_RATE_CDE", 
            "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Opn_Accum_Amt = naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Data.newFieldArrayInGroup("naz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Opn_Accum_Amt", 
            "NAD-DTL-TIAA-OPN-ACCUM-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NAD_DTL_TIAA_OPN_ACCUM_AMT", 
            "NAZ_DA_RSLT_RCRD_NAD_DTL_TIAA_DATA");
        vw_naz_Da_Rslt_Ddm_View.setUniquePeList();

        this.setRecordName("LdaNazldarc");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_naz_Da_Rslt_Ddm_View.reset();
    }

    // Constructor
    public LdaNazldarc() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
