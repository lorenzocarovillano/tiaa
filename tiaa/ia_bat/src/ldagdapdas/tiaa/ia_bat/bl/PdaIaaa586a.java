/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:44 PM
**        * FROM NATURAL PDA     : IAAA586A
************************************************************
**        * FILE NAME            : PdaIaaa586a.java
**        * CLASS NAME           : PdaIaaa586a
**        * INSTANCE NAME        : PdaIaaa586a
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIaaa586a extends PdaBase
{
    // Properties
    private DbsGroup iaaa460a;
    private DbsField iaaa460a_Pnd_W_Cntrct;
    private DbsGroup iaaa460a_Pnd_W_CntrctRedef1;
    private DbsField iaaa460a_Pnd_Ppcn_Nbr_A3;
    private DbsGroup iaaa460a_Pnd_Ppcn_Nbr_A3Redef2;
    private DbsField iaaa460a_Pnd_Ppcn_Nbr_A1;
    private DbsField iaaa460a_Pnd_W_Payee_Cde_1;
    private DbsField iaaa460a_Pnd_W_Payee_Cde_2;
    private DbsField iaaa460a_Pnd_Table_Instl_Date;
    private DbsField iaaa460a_Pnd_Table_File;
    private DbsField iaaa460a_Pnd_Table_Inv_Date;
    private DbsField iaaa460a_Pnd_Month_Field;
    private DbsField iaaa460a_Pnd_Delete_Field;
    private DbsField iaaa460a_Pnd_Year_Num;
    private DbsField iaaa460a_Pnd_Instlmnts;
    private DbsField iaaa460a_Pnd_Year;
    private DbsField iaaa460a_Pnd_Option_Cde;
    private DbsField iaaa460a_Pnd_Mode;
    private DbsField iaaa460a_Pnd_Msg_1;
    private DbsField iaaa460a_Pnd_Ov_Tot_Amt;
    private DbsField iaaa460a_Pnd_Ov_Per_Amt;
    private DbsField iaaa460a_Pnd_Ov_Div_Amt;
    private DbsField iaaa460a_Pnd_Ov_Unt_Amt;
    private DbsField iaaa460a_Pnd_Ov_Instl_Date;
    private DbsField iaaa460a_Pnd_Ov_Fund;

    public DbsGroup getIaaa460a() { return iaaa460a; }

    public DbsField getIaaa460a_Pnd_W_Cntrct() { return iaaa460a_Pnd_W_Cntrct; }

    public DbsGroup getIaaa460a_Pnd_W_CntrctRedef1() { return iaaa460a_Pnd_W_CntrctRedef1; }

    public DbsField getIaaa460a_Pnd_Ppcn_Nbr_A3() { return iaaa460a_Pnd_Ppcn_Nbr_A3; }

    public DbsGroup getIaaa460a_Pnd_Ppcn_Nbr_A3Redef2() { return iaaa460a_Pnd_Ppcn_Nbr_A3Redef2; }

    public DbsField getIaaa460a_Pnd_Ppcn_Nbr_A1() { return iaaa460a_Pnd_Ppcn_Nbr_A1; }

    public DbsField getIaaa460a_Pnd_W_Payee_Cde_1() { return iaaa460a_Pnd_W_Payee_Cde_1; }

    public DbsField getIaaa460a_Pnd_W_Payee_Cde_2() { return iaaa460a_Pnd_W_Payee_Cde_2; }

    public DbsField getIaaa460a_Pnd_Table_Instl_Date() { return iaaa460a_Pnd_Table_Instl_Date; }

    public DbsField getIaaa460a_Pnd_Table_File() { return iaaa460a_Pnd_Table_File; }

    public DbsField getIaaa460a_Pnd_Table_Inv_Date() { return iaaa460a_Pnd_Table_Inv_Date; }

    public DbsField getIaaa460a_Pnd_Month_Field() { return iaaa460a_Pnd_Month_Field; }

    public DbsField getIaaa460a_Pnd_Delete_Field() { return iaaa460a_Pnd_Delete_Field; }

    public DbsField getIaaa460a_Pnd_Year_Num() { return iaaa460a_Pnd_Year_Num; }

    public DbsField getIaaa460a_Pnd_Instlmnts() { return iaaa460a_Pnd_Instlmnts; }

    public DbsField getIaaa460a_Pnd_Year() { return iaaa460a_Pnd_Year; }

    public DbsField getIaaa460a_Pnd_Option_Cde() { return iaaa460a_Pnd_Option_Cde; }

    public DbsField getIaaa460a_Pnd_Mode() { return iaaa460a_Pnd_Mode; }

    public DbsField getIaaa460a_Pnd_Msg_1() { return iaaa460a_Pnd_Msg_1; }

    public DbsField getIaaa460a_Pnd_Ov_Tot_Amt() { return iaaa460a_Pnd_Ov_Tot_Amt; }

    public DbsField getIaaa460a_Pnd_Ov_Per_Amt() { return iaaa460a_Pnd_Ov_Per_Amt; }

    public DbsField getIaaa460a_Pnd_Ov_Div_Amt() { return iaaa460a_Pnd_Ov_Div_Amt; }

    public DbsField getIaaa460a_Pnd_Ov_Unt_Amt() { return iaaa460a_Pnd_Ov_Unt_Amt; }

    public DbsField getIaaa460a_Pnd_Ov_Instl_Date() { return iaaa460a_Pnd_Ov_Instl_Date; }

    public DbsField getIaaa460a_Pnd_Ov_Fund() { return iaaa460a_Pnd_Ov_Fund; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaaa460a = dbsRecord.newGroupInRecord("iaaa460a", "IAAA460A");
        iaaa460a.setParameterOption(ParameterOption.ByReference);
        iaaa460a_Pnd_W_Cntrct = iaaa460a.newFieldInGroup("iaaa460a_Pnd_W_Cntrct", "#W-CNTRCT", FieldType.STRING, 10);
        iaaa460a_Pnd_W_CntrctRedef1 = iaaa460a.newGroupInGroup("iaaa460a_Pnd_W_CntrctRedef1", "Redefines", iaaa460a_Pnd_W_Cntrct);
        iaaa460a_Pnd_Ppcn_Nbr_A3 = iaaa460a_Pnd_W_CntrctRedef1.newFieldInGroup("iaaa460a_Pnd_Ppcn_Nbr_A3", "#PPCN-NBR-A3", FieldType.STRING, 3);
        iaaa460a_Pnd_Ppcn_Nbr_A3Redef2 = iaaa460a_Pnd_W_CntrctRedef1.newGroupInGroup("iaaa460a_Pnd_Ppcn_Nbr_A3Redef2", "Redefines", iaaa460a_Pnd_Ppcn_Nbr_A3);
        iaaa460a_Pnd_Ppcn_Nbr_A1 = iaaa460a_Pnd_Ppcn_Nbr_A3Redef2.newFieldInGroup("iaaa460a_Pnd_Ppcn_Nbr_A1", "#PPCN-NBR-A1", FieldType.STRING, 1);
        iaaa460a_Pnd_W_Payee_Cde_1 = iaaa460a.newFieldInGroup("iaaa460a_Pnd_W_Payee_Cde_1", "#W-PAYEE-CDE-1", FieldType.NUMERIC, 2);
        iaaa460a_Pnd_W_Payee_Cde_2 = iaaa460a.newFieldInGroup("iaaa460a_Pnd_W_Payee_Cde_2", "#W-PAYEE-CDE-2", FieldType.NUMERIC, 2);
        iaaa460a_Pnd_Table_Instl_Date = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Table_Instl_Date", "#TABLE-INSTL-DATE", FieldType.NUMERIC, 8, new 
            DbsArrayController(1,12));
        iaaa460a_Pnd_Table_File = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Table_File", "#TABLE-FILE", FieldType.STRING, 1, new DbsArrayController(1,
            12));
        iaaa460a_Pnd_Table_Inv_Date = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Table_Inv_Date", "#TABLE-INV-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1,
            12));
        iaaa460a_Pnd_Month_Field = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Month_Field", "#MONTH-FIELD", FieldType.NUMERIC, 2, new DbsArrayController(1,
            7,1,12));
        iaaa460a_Pnd_Delete_Field = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Delete_Field", "#DELETE-FIELD", FieldType.STRING, 1, new DbsArrayController(1,
            7,1,12));
        iaaa460a_Pnd_Year_Num = iaaa460a.newFieldInGroup("iaaa460a_Pnd_Year_Num", "#YEAR-NUM", FieldType.NUMERIC, 1);
        iaaa460a_Pnd_Instlmnts = iaaa460a.newFieldInGroup("iaaa460a_Pnd_Instlmnts", "#INSTLMNTS", FieldType.NUMERIC, 2);
        iaaa460a_Pnd_Year = iaaa460a.newFieldInGroup("iaaa460a_Pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        iaaa460a_Pnd_Option_Cde = iaaa460a.newFieldInGroup("iaaa460a_Pnd_Option_Cde", "#OPTION-CDE", FieldType.NUMERIC, 2);
        iaaa460a_Pnd_Mode = iaaa460a.newFieldInGroup("iaaa460a_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        iaaa460a_Pnd_Msg_1 = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Msg_1", "#MSG-1", FieldType.STRING, 3, new DbsArrayController(1,208));
        iaaa460a_Pnd_Ov_Tot_Amt = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Ov_Tot_Amt", "#OV-TOT-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            208));
        iaaa460a_Pnd_Ov_Per_Amt = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Ov_Per_Amt", "#OV-PER-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            208));
        iaaa460a_Pnd_Ov_Div_Amt = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Ov_Div_Amt", "#OV-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            208));
        iaaa460a_Pnd_Ov_Unt_Amt = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Ov_Unt_Amt", "#OV-UNT-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            208));
        iaaa460a_Pnd_Ov_Instl_Date = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Ov_Instl_Date", "#OV-INSTL-DATE", FieldType.STRING, 8, new DbsArrayController(1,
            208));
        iaaa460a_Pnd_Ov_Fund = iaaa460a.newFieldArrayInGroup("iaaa460a_Pnd_Ov_Fund", "#OV-FUND", FieldType.STRING, 5, new DbsArrayController(1,208));

        dbsRecord.reset();
    }

    // Constructors
    public PdaIaaa586a(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

