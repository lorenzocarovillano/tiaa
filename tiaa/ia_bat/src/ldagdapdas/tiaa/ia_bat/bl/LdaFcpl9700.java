/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:20 PM
**        * FROM NATURAL LDA     : FCPL9700
************************************************************
**        * FILE NAME            : LdaFcpl9700.java
**        * CLASS NAME           : LdaFcpl9700
**        * INSTANCE NAME        : LdaFcpl9700
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl9700 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;
    private DbsGroup pstl9700_Data;
    private DbsField pstl9700_Data_Pstl9700_Rec_Id;
    private DbsField pstl9700_Data_Pstl9700_Data_Lgth;
    private DbsField pstl9700_Data_Pstl9700_Data_Occurs;
    private DbsField pstl9700_Data_Pstl9700_Data_Array;
    private DbsGroup pstl9700_Data_Pstl9700_Data_ArrayRedef1;
    private DbsGroup pstl9700_Data_Psta3005_Tax;
    private DbsField pstl9700_Data_Opening_Paragraph;
    private DbsField pstl9700_Data_Effective_Paragraph;
    private DbsField pstl9700_Data_Closing_Paragraph;
    private DbsField pstl9700_Data_Phone_Call_Date;
    private DbsField pstl9700_Data_Projected_Receive_Date;
    private DbsField pstl9700_Data_Incmplte_Hdr_Type;
    private DbsField pstl9700_Data_Incmplte_Cdes_Fed;
    private DbsField pstl9700_Data_Incmplte_Fed_Hdr;
    private DbsField pstl9700_Data_Incmplte_Cdes_State;
    private DbsField pstl9700_Data_Incmplte_State_Hdr;
    private DbsField pstl9700_Data_Tax_Addr_Ltr;
    private DbsField pstl9700_Data_From_State_Nme;
    private DbsField pstl9700_Data_To_State_Nme;
    private DbsField pstl9700_Data_Cntrct_Desc;
    private DbsField pstl9700_Data_C_Contract_Sets;
    private DbsField pstl9700_Data_This_Set_Index;
    private DbsGroup pstl9700_Data_Psta3005_Set_Tax;
    private DbsField pstl9700_Data_C_Contracts;
    private DbsField pstl9700_Data_Cntrct_Nbr;
    private DbsField pstl9700_Data_Cref_Nbr;
    private DbsField pstl9700_Data_Nbr_Of_Copies;
    private DbsField pstl9700_Data_Addrss_Lne;
    private DbsField pstl9700_Data_Addrss_Type_Cde;
    private DbsField pstl9700_Data_Addrss_State_Cde;
    private DbsField pstl9700_Data_Addrss_Postal_Data;
    private DbsGroup pstl9700_Data_Addrss_Postal_DataRedef2;
    private DbsField pstl9700_Data_Addrss_Zip_Plus_4;
    private DbsGroup pstl9700_Data_Addrss_Zip_Plus_4Redef3;
    private DbsField pstl9700_Data_Addrss_Zip_5;
    private DbsField pstl9700_Data_Addrss_Zip_4;
    private DbsGroup pstl9700_Data_Federal_Data;
    private DbsField pstl9700_Data_New_Election_In_Words;
    private DbsField pstl9700_Data_Effective_Date_Month;
    private DbsField pstl9700_Data_Withhold_Status_No;
    private DbsField pstl9700_Data_Withhold_Status_Yes;
    private DbsGroup pstl9700_Data_Marital_Tax_Status;
    private DbsField pstl9700_Data_Marital_Single;
    private DbsField pstl9700_Data_Marital_Married;
    private DbsField pstl9700_Data_Marital_Higher;
    private DbsField pstl9700_Data_Nmbr_Allowances;
    private DbsGroup pstl9700_Data_Withhold_Info;
    private DbsField pstl9700_Data_Withhold_Addl;
    private DbsField pstl9700_Data_Addl_Tiaa_Amount;
    private DbsGroup pstl9700_Data_Tiaa_Frmt_Info;
    private DbsField pstl9700_Data_Tiaa_Decimals;
    private DbsField pstl9700_Data_Tiaa_Type;
    private DbsField pstl9700_Data_Tiaa_Right_Just;
    private DbsField pstl9700_Data_Tiaa_Zero_Print;
    private DbsField pstl9700_Data_Addl_Cref_Amount;
    private DbsGroup pstl9700_Data_Cref_Frmt_Info;
    private DbsField pstl9700_Data_Cref_Decimals;
    private DbsField pstl9700_Data_Cref_Type;
    private DbsField pstl9700_Data_Cref_Right_Just;
    private DbsField pstl9700_Data_Cref_Zero_Print;
    private DbsField pstl9700_Data_Data_Type_Flat;
    private DbsField pstl9700_Data_Flat_Tiaa_Amount;
    private DbsGroup pstl9700_Data_Flatt_Tiaa_Frmt_Info;
    private DbsField pstl9700_Data_Flat_Tiaa_Decimals;
    private DbsField pstl9700_Data_Flat_Tiaa_Type;
    private DbsField pstl9700_Data_Flat_Tiaa_Right_Just;
    private DbsField pstl9700_Data_Flat_Tiaa_Zero_Print;
    private DbsField pstl9700_Data_Flat_Cref_Amount;
    private DbsGroup pstl9700_Data_Flat_Cref_Frmt_Info;
    private DbsField pstl9700_Data_Flat_Cref_Decimals;
    private DbsField pstl9700_Data_Flat_Cref_Type;
    private DbsField pstl9700_Data_Flat_Cref_Right_Just;
    private DbsField pstl9700_Data_Flat_Cref_Zero_Print;
    private DbsField pstl9700_Data_Data_Type_Fixd;
    private DbsField pstl9700_Data_Fixd_Tiaa_Percent;
    private DbsGroup pstl9700_Data_Fixd_Tiaa_Frmt_Info;
    private DbsField pstl9700_Data_Fixd_Tiaa_Decimals;
    private DbsField pstl9700_Data_Fixd_Tiaa_Type;
    private DbsField pstl9700_Data_Fixd_Tiaa_Right_Just;
    private DbsField pstl9700_Data_Fixd_Tiaa_Zero_Print;
    private DbsField pstl9700_Data_Fixd_Cref_Percent;
    private DbsGroup pstl9700_Data_Fixd_Cref_Frmt_Info;
    private DbsField pstl9700_Data_Fixd_Cref_Decimals;
    private DbsField pstl9700_Data_Fixd_Cref_Type;
    private DbsField pstl9700_Data_Fixd_Cref_Right_Just;
    private DbsField pstl9700_Data_Fixd_Cref_Zero_Print;
    private DbsGroup pstl9700_Data_State_Data;
    private DbsField pstl9700_Data_No_Wthhld_Ind;
    private DbsField pstl9700_Data_Wthhld_Ind;
    private DbsField pstl9700_Data_Wthhld_Amnt;
    private DbsGroup pstl9700_Data_Wthhld_Amnt_Frmt;
    private DbsField pstl9700_Data_Wthhld_Decimals;
    private DbsField pstl9700_Data_Wthhld_Type;
    private DbsField pstl9700_Data_Wthhld_Right_Just;
    private DbsField pstl9700_Data_Wthhld_Zero_Print;
    private DbsField pstl9700_Data_Wthhld_Addl_Ind;
    private DbsField pstl9700_Data_Wthhld_Addl_Amnt;
    private DbsGroup pstl9700_Data_Wthhld_Addl_Amnt_Frmt;
    private DbsField pstl9700_Data_Addl_Decimals;
    private DbsField pstl9700_Data_Addl_Type;
    private DbsField pstl9700_Data_Addl_Right_Just;
    private DbsField pstl9700_Data_Addl_Zero_Print;
    private DbsField pstl9700_Data_Rvke_Prev_Elctn;
    private DbsField pstl9700_Data_Exempt;
    private DbsField pstl9700_Data_Not_Subject_To;
    private DbsField pstl9700_Data_Redce_Wthhldng;
    private DbsField pstl9700_Data_Nbr_Exmptns;
    private DbsField pstl9700_Data_Spse_Exmptn;
    private DbsField pstl9700_Data_Blind_Exmptn;
    private DbsField pstl9700_Data_Blind_Spse_Exmptn;
    private DbsField pstl9700_Data_Married;
    private DbsField pstl9700_Data_Single;
    private DbsField pstl9700_Data_Married_Higher;
    private DbsField pstl9700_Data_Married_Separate;
    private DbsField pstl9700_Data_Over_65;
    private DbsField pstl9700_Data_Spse_Over_65;
    private DbsField pstl9700_Data_Wthhld_Flat_Ind;
    private DbsField pstl9700_Data_State_Elctn_In_Words;
    private DbsGroup pstl9700_Data_Psta3006_Tax;
    private DbsGroup pstl9700_Data_User_Selected_Options;
    private DbsField pstl9700_Data_Cover_Ltr_Req;
    private DbsField pstl9700_Data_Fed_Form_Req;
    private DbsField pstl9700_Data_State_Form_Req;
    private DbsField pstl9700_Data_Prefill_Fed;
    private DbsField pstl9700_Data_Prefill_State;
    private DbsGroup pstl9700_Data_Package_Selected;
    private DbsField pstl9700_Data_Initial_Sel;
    private DbsField pstl9700_Data_Incmplte_Sel;
    private DbsField pstl9700_Data_Phonecf_Sel;
    private DbsGroup pstl9700_Data_Incmplte_Selected;
    private DbsField pstl9700_Data_Incmplte_Federal;
    private DbsField pstl9700_Data_Incmplte_State;
    private DbsGroup pstl9700_Data_Mandatory_Tax_Form_Indicators;
    private DbsField pstl9700_Data_Rollover_Ind;
    private DbsField pstl9700_Data_Periodic_Ind;

    public DbsGroup getSort_Key() { return sort_Key; }

    public DbsField getSort_Key_Sort_Key_Lgth() { return sort_Key_Sort_Key_Lgth; }

    public DbsField getSort_Key_Sort_Key_Array() { return sort_Key_Sort_Key_Array; }

    public DbsGroup getPstl9700_Data() { return pstl9700_Data; }

    public DbsField getPstl9700_Data_Pstl9700_Rec_Id() { return pstl9700_Data_Pstl9700_Rec_Id; }

    public DbsField getPstl9700_Data_Pstl9700_Data_Lgth() { return pstl9700_Data_Pstl9700_Data_Lgth; }

    public DbsField getPstl9700_Data_Pstl9700_Data_Occurs() { return pstl9700_Data_Pstl9700_Data_Occurs; }

    public DbsField getPstl9700_Data_Pstl9700_Data_Array() { return pstl9700_Data_Pstl9700_Data_Array; }

    public DbsGroup getPstl9700_Data_Pstl9700_Data_ArrayRedef1() { return pstl9700_Data_Pstl9700_Data_ArrayRedef1; }

    public DbsGroup getPstl9700_Data_Psta3005_Tax() { return pstl9700_Data_Psta3005_Tax; }

    public DbsField getPstl9700_Data_Opening_Paragraph() { return pstl9700_Data_Opening_Paragraph; }

    public DbsField getPstl9700_Data_Effective_Paragraph() { return pstl9700_Data_Effective_Paragraph; }

    public DbsField getPstl9700_Data_Closing_Paragraph() { return pstl9700_Data_Closing_Paragraph; }

    public DbsField getPstl9700_Data_Phone_Call_Date() { return pstl9700_Data_Phone_Call_Date; }

    public DbsField getPstl9700_Data_Projected_Receive_Date() { return pstl9700_Data_Projected_Receive_Date; }

    public DbsField getPstl9700_Data_Incmplte_Hdr_Type() { return pstl9700_Data_Incmplte_Hdr_Type; }

    public DbsField getPstl9700_Data_Incmplte_Cdes_Fed() { return pstl9700_Data_Incmplte_Cdes_Fed; }

    public DbsField getPstl9700_Data_Incmplte_Fed_Hdr() { return pstl9700_Data_Incmplte_Fed_Hdr; }

    public DbsField getPstl9700_Data_Incmplte_Cdes_State() { return pstl9700_Data_Incmplte_Cdes_State; }

    public DbsField getPstl9700_Data_Incmplte_State_Hdr() { return pstl9700_Data_Incmplte_State_Hdr; }

    public DbsField getPstl9700_Data_Tax_Addr_Ltr() { return pstl9700_Data_Tax_Addr_Ltr; }

    public DbsField getPstl9700_Data_From_State_Nme() { return pstl9700_Data_From_State_Nme; }

    public DbsField getPstl9700_Data_To_State_Nme() { return pstl9700_Data_To_State_Nme; }

    public DbsField getPstl9700_Data_Cntrct_Desc() { return pstl9700_Data_Cntrct_Desc; }

    public DbsField getPstl9700_Data_C_Contract_Sets() { return pstl9700_Data_C_Contract_Sets; }

    public DbsField getPstl9700_Data_This_Set_Index() { return pstl9700_Data_This_Set_Index; }

    public DbsGroup getPstl9700_Data_Psta3005_Set_Tax() { return pstl9700_Data_Psta3005_Set_Tax; }

    public DbsField getPstl9700_Data_C_Contracts() { return pstl9700_Data_C_Contracts; }

    public DbsField getPstl9700_Data_Cntrct_Nbr() { return pstl9700_Data_Cntrct_Nbr; }

    public DbsField getPstl9700_Data_Cref_Nbr() { return pstl9700_Data_Cref_Nbr; }

    public DbsField getPstl9700_Data_Nbr_Of_Copies() { return pstl9700_Data_Nbr_Of_Copies; }

    public DbsField getPstl9700_Data_Addrss_Lne() { return pstl9700_Data_Addrss_Lne; }

    public DbsField getPstl9700_Data_Addrss_Type_Cde() { return pstl9700_Data_Addrss_Type_Cde; }

    public DbsField getPstl9700_Data_Addrss_State_Cde() { return pstl9700_Data_Addrss_State_Cde; }

    public DbsField getPstl9700_Data_Addrss_Postal_Data() { return pstl9700_Data_Addrss_Postal_Data; }

    public DbsGroup getPstl9700_Data_Addrss_Postal_DataRedef2() { return pstl9700_Data_Addrss_Postal_DataRedef2; }

    public DbsField getPstl9700_Data_Addrss_Zip_Plus_4() { return pstl9700_Data_Addrss_Zip_Plus_4; }

    public DbsGroup getPstl9700_Data_Addrss_Zip_Plus_4Redef3() { return pstl9700_Data_Addrss_Zip_Plus_4Redef3; }

    public DbsField getPstl9700_Data_Addrss_Zip_5() { return pstl9700_Data_Addrss_Zip_5; }

    public DbsField getPstl9700_Data_Addrss_Zip_4() { return pstl9700_Data_Addrss_Zip_4; }

    public DbsGroup getPstl9700_Data_Federal_Data() { return pstl9700_Data_Federal_Data; }

    public DbsField getPstl9700_Data_New_Election_In_Words() { return pstl9700_Data_New_Election_In_Words; }

    public DbsField getPstl9700_Data_Effective_Date_Month() { return pstl9700_Data_Effective_Date_Month; }

    public DbsField getPstl9700_Data_Withhold_Status_No() { return pstl9700_Data_Withhold_Status_No; }

    public DbsField getPstl9700_Data_Withhold_Status_Yes() { return pstl9700_Data_Withhold_Status_Yes; }

    public DbsGroup getPstl9700_Data_Marital_Tax_Status() { return pstl9700_Data_Marital_Tax_Status; }

    public DbsField getPstl9700_Data_Marital_Single() { return pstl9700_Data_Marital_Single; }

    public DbsField getPstl9700_Data_Marital_Married() { return pstl9700_Data_Marital_Married; }

    public DbsField getPstl9700_Data_Marital_Higher() { return pstl9700_Data_Marital_Higher; }

    public DbsField getPstl9700_Data_Nmbr_Allowances() { return pstl9700_Data_Nmbr_Allowances; }

    public DbsGroup getPstl9700_Data_Withhold_Info() { return pstl9700_Data_Withhold_Info; }

    public DbsField getPstl9700_Data_Withhold_Addl() { return pstl9700_Data_Withhold_Addl; }

    public DbsField getPstl9700_Data_Addl_Tiaa_Amount() { return pstl9700_Data_Addl_Tiaa_Amount; }

    public DbsGroup getPstl9700_Data_Tiaa_Frmt_Info() { return pstl9700_Data_Tiaa_Frmt_Info; }

    public DbsField getPstl9700_Data_Tiaa_Decimals() { return pstl9700_Data_Tiaa_Decimals; }

    public DbsField getPstl9700_Data_Tiaa_Type() { return pstl9700_Data_Tiaa_Type; }

    public DbsField getPstl9700_Data_Tiaa_Right_Just() { return pstl9700_Data_Tiaa_Right_Just; }

    public DbsField getPstl9700_Data_Tiaa_Zero_Print() { return pstl9700_Data_Tiaa_Zero_Print; }

    public DbsField getPstl9700_Data_Addl_Cref_Amount() { return pstl9700_Data_Addl_Cref_Amount; }

    public DbsGroup getPstl9700_Data_Cref_Frmt_Info() { return pstl9700_Data_Cref_Frmt_Info; }

    public DbsField getPstl9700_Data_Cref_Decimals() { return pstl9700_Data_Cref_Decimals; }

    public DbsField getPstl9700_Data_Cref_Type() { return pstl9700_Data_Cref_Type; }

    public DbsField getPstl9700_Data_Cref_Right_Just() { return pstl9700_Data_Cref_Right_Just; }

    public DbsField getPstl9700_Data_Cref_Zero_Print() { return pstl9700_Data_Cref_Zero_Print; }

    public DbsField getPstl9700_Data_Data_Type_Flat() { return pstl9700_Data_Data_Type_Flat; }

    public DbsField getPstl9700_Data_Flat_Tiaa_Amount() { return pstl9700_Data_Flat_Tiaa_Amount; }

    public DbsGroup getPstl9700_Data_Flatt_Tiaa_Frmt_Info() { return pstl9700_Data_Flatt_Tiaa_Frmt_Info; }

    public DbsField getPstl9700_Data_Flat_Tiaa_Decimals() { return pstl9700_Data_Flat_Tiaa_Decimals; }

    public DbsField getPstl9700_Data_Flat_Tiaa_Type() { return pstl9700_Data_Flat_Tiaa_Type; }

    public DbsField getPstl9700_Data_Flat_Tiaa_Right_Just() { return pstl9700_Data_Flat_Tiaa_Right_Just; }

    public DbsField getPstl9700_Data_Flat_Tiaa_Zero_Print() { return pstl9700_Data_Flat_Tiaa_Zero_Print; }

    public DbsField getPstl9700_Data_Flat_Cref_Amount() { return pstl9700_Data_Flat_Cref_Amount; }

    public DbsGroup getPstl9700_Data_Flat_Cref_Frmt_Info() { return pstl9700_Data_Flat_Cref_Frmt_Info; }

    public DbsField getPstl9700_Data_Flat_Cref_Decimals() { return pstl9700_Data_Flat_Cref_Decimals; }

    public DbsField getPstl9700_Data_Flat_Cref_Type() { return pstl9700_Data_Flat_Cref_Type; }

    public DbsField getPstl9700_Data_Flat_Cref_Right_Just() { return pstl9700_Data_Flat_Cref_Right_Just; }

    public DbsField getPstl9700_Data_Flat_Cref_Zero_Print() { return pstl9700_Data_Flat_Cref_Zero_Print; }

    public DbsField getPstl9700_Data_Data_Type_Fixd() { return pstl9700_Data_Data_Type_Fixd; }

    public DbsField getPstl9700_Data_Fixd_Tiaa_Percent() { return pstl9700_Data_Fixd_Tiaa_Percent; }

    public DbsGroup getPstl9700_Data_Fixd_Tiaa_Frmt_Info() { return pstl9700_Data_Fixd_Tiaa_Frmt_Info; }

    public DbsField getPstl9700_Data_Fixd_Tiaa_Decimals() { return pstl9700_Data_Fixd_Tiaa_Decimals; }

    public DbsField getPstl9700_Data_Fixd_Tiaa_Type() { return pstl9700_Data_Fixd_Tiaa_Type; }

    public DbsField getPstl9700_Data_Fixd_Tiaa_Right_Just() { return pstl9700_Data_Fixd_Tiaa_Right_Just; }

    public DbsField getPstl9700_Data_Fixd_Tiaa_Zero_Print() { return pstl9700_Data_Fixd_Tiaa_Zero_Print; }

    public DbsField getPstl9700_Data_Fixd_Cref_Percent() { return pstl9700_Data_Fixd_Cref_Percent; }

    public DbsGroup getPstl9700_Data_Fixd_Cref_Frmt_Info() { return pstl9700_Data_Fixd_Cref_Frmt_Info; }

    public DbsField getPstl9700_Data_Fixd_Cref_Decimals() { return pstl9700_Data_Fixd_Cref_Decimals; }

    public DbsField getPstl9700_Data_Fixd_Cref_Type() { return pstl9700_Data_Fixd_Cref_Type; }

    public DbsField getPstl9700_Data_Fixd_Cref_Right_Just() { return pstl9700_Data_Fixd_Cref_Right_Just; }

    public DbsField getPstl9700_Data_Fixd_Cref_Zero_Print() { return pstl9700_Data_Fixd_Cref_Zero_Print; }

    public DbsGroup getPstl9700_Data_State_Data() { return pstl9700_Data_State_Data; }

    public DbsField getPstl9700_Data_No_Wthhld_Ind() { return pstl9700_Data_No_Wthhld_Ind; }

    public DbsField getPstl9700_Data_Wthhld_Ind() { return pstl9700_Data_Wthhld_Ind; }

    public DbsField getPstl9700_Data_Wthhld_Amnt() { return pstl9700_Data_Wthhld_Amnt; }

    public DbsGroup getPstl9700_Data_Wthhld_Amnt_Frmt() { return pstl9700_Data_Wthhld_Amnt_Frmt; }

    public DbsField getPstl9700_Data_Wthhld_Decimals() { return pstl9700_Data_Wthhld_Decimals; }

    public DbsField getPstl9700_Data_Wthhld_Type() { return pstl9700_Data_Wthhld_Type; }

    public DbsField getPstl9700_Data_Wthhld_Right_Just() { return pstl9700_Data_Wthhld_Right_Just; }

    public DbsField getPstl9700_Data_Wthhld_Zero_Print() { return pstl9700_Data_Wthhld_Zero_Print; }

    public DbsField getPstl9700_Data_Wthhld_Addl_Ind() { return pstl9700_Data_Wthhld_Addl_Ind; }

    public DbsField getPstl9700_Data_Wthhld_Addl_Amnt() { return pstl9700_Data_Wthhld_Addl_Amnt; }

    public DbsGroup getPstl9700_Data_Wthhld_Addl_Amnt_Frmt() { return pstl9700_Data_Wthhld_Addl_Amnt_Frmt; }

    public DbsField getPstl9700_Data_Addl_Decimals() { return pstl9700_Data_Addl_Decimals; }

    public DbsField getPstl9700_Data_Addl_Type() { return pstl9700_Data_Addl_Type; }

    public DbsField getPstl9700_Data_Addl_Right_Just() { return pstl9700_Data_Addl_Right_Just; }

    public DbsField getPstl9700_Data_Addl_Zero_Print() { return pstl9700_Data_Addl_Zero_Print; }

    public DbsField getPstl9700_Data_Rvke_Prev_Elctn() { return pstl9700_Data_Rvke_Prev_Elctn; }

    public DbsField getPstl9700_Data_Exempt() { return pstl9700_Data_Exempt; }

    public DbsField getPstl9700_Data_Not_Subject_To() { return pstl9700_Data_Not_Subject_To; }

    public DbsField getPstl9700_Data_Redce_Wthhldng() { return pstl9700_Data_Redce_Wthhldng; }

    public DbsField getPstl9700_Data_Nbr_Exmptns() { return pstl9700_Data_Nbr_Exmptns; }

    public DbsField getPstl9700_Data_Spse_Exmptn() { return pstl9700_Data_Spse_Exmptn; }

    public DbsField getPstl9700_Data_Blind_Exmptn() { return pstl9700_Data_Blind_Exmptn; }

    public DbsField getPstl9700_Data_Blind_Spse_Exmptn() { return pstl9700_Data_Blind_Spse_Exmptn; }

    public DbsField getPstl9700_Data_Married() { return pstl9700_Data_Married; }

    public DbsField getPstl9700_Data_Single() { return pstl9700_Data_Single; }

    public DbsField getPstl9700_Data_Married_Higher() { return pstl9700_Data_Married_Higher; }

    public DbsField getPstl9700_Data_Married_Separate() { return pstl9700_Data_Married_Separate; }

    public DbsField getPstl9700_Data_Over_65() { return pstl9700_Data_Over_65; }

    public DbsField getPstl9700_Data_Spse_Over_65() { return pstl9700_Data_Spse_Over_65; }

    public DbsField getPstl9700_Data_Wthhld_Flat_Ind() { return pstl9700_Data_Wthhld_Flat_Ind; }

    public DbsField getPstl9700_Data_State_Elctn_In_Words() { return pstl9700_Data_State_Elctn_In_Words; }

    public DbsGroup getPstl9700_Data_Psta3006_Tax() { return pstl9700_Data_Psta3006_Tax; }

    public DbsGroup getPstl9700_Data_User_Selected_Options() { return pstl9700_Data_User_Selected_Options; }

    public DbsField getPstl9700_Data_Cover_Ltr_Req() { return pstl9700_Data_Cover_Ltr_Req; }

    public DbsField getPstl9700_Data_Fed_Form_Req() { return pstl9700_Data_Fed_Form_Req; }

    public DbsField getPstl9700_Data_State_Form_Req() { return pstl9700_Data_State_Form_Req; }

    public DbsField getPstl9700_Data_Prefill_Fed() { return pstl9700_Data_Prefill_Fed; }

    public DbsField getPstl9700_Data_Prefill_State() { return pstl9700_Data_Prefill_State; }

    public DbsGroup getPstl9700_Data_Package_Selected() { return pstl9700_Data_Package_Selected; }

    public DbsField getPstl9700_Data_Initial_Sel() { return pstl9700_Data_Initial_Sel; }

    public DbsField getPstl9700_Data_Incmplte_Sel() { return pstl9700_Data_Incmplte_Sel; }

    public DbsField getPstl9700_Data_Phonecf_Sel() { return pstl9700_Data_Phonecf_Sel; }

    public DbsGroup getPstl9700_Data_Incmplte_Selected() { return pstl9700_Data_Incmplte_Selected; }

    public DbsField getPstl9700_Data_Incmplte_Federal() { return pstl9700_Data_Incmplte_Federal; }

    public DbsField getPstl9700_Data_Incmplte_State() { return pstl9700_Data_Incmplte_State; }

    public DbsGroup getPstl9700_Data_Mandatory_Tax_Form_Indicators() { return pstl9700_Data_Mandatory_Tax_Form_Indicators; }

    public DbsField getPstl9700_Data_Rollover_Ind() { return pstl9700_Data_Rollover_Ind; }

    public DbsField getPstl9700_Data_Periodic_Ind() { return pstl9700_Data_Periodic_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key = newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            1));

        pstl9700_Data = newGroupInRecord("pstl9700_Data", "PSTL9700-DATA");
        pstl9700_Data_Pstl9700_Rec_Id = pstl9700_Data.newFieldInGroup("pstl9700_Data_Pstl9700_Rec_Id", "PSTL9700-REC-ID", FieldType.STRING, 2);
        pstl9700_Data_Pstl9700_Data_Lgth = pstl9700_Data.newFieldInGroup("pstl9700_Data_Pstl9700_Data_Lgth", "PSTL9700-DATA-LGTH", FieldType.NUMERIC, 
            5);
        pstl9700_Data_Pstl9700_Data_Occurs = pstl9700_Data.newFieldInGroup("pstl9700_Data_Pstl9700_Data_Occurs", "PSTL9700-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        pstl9700_Data_Pstl9700_Data_Array = pstl9700_Data.newFieldArrayInGroup("pstl9700_Data_Pstl9700_Data_Array", "PSTL9700-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,910));
        pstl9700_Data_Pstl9700_Data_ArrayRedef1 = pstl9700_Data.newGroupInGroup("pstl9700_Data_Pstl9700_Data_ArrayRedef1", "Redefines", pstl9700_Data_Pstl9700_Data_Array);
        pstl9700_Data_Psta3005_Tax = pstl9700_Data_Pstl9700_Data_ArrayRedef1.newGroupInGroup("pstl9700_Data_Psta3005_Tax", "PSTA3005-TAX");
        pstl9700_Data_Opening_Paragraph = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Opening_Paragraph", "OPENING-PARAGRAPH", FieldType.STRING, 
            1);
        pstl9700_Data_Effective_Paragraph = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Effective_Paragraph", "EFFECTIVE-PARAGRAPH", FieldType.STRING, 
            1);
        pstl9700_Data_Closing_Paragraph = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Closing_Paragraph", "CLOSING-PARAGRAPH", FieldType.STRING, 
            1);
        pstl9700_Data_Phone_Call_Date = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Phone_Call_Date", "PHONE-CALL-DATE", FieldType.DATE);
        pstl9700_Data_Projected_Receive_Date = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Projected_Receive_Date", "PROJECTED-RECEIVE-DATE", 
            FieldType.DATE);
        pstl9700_Data_Incmplte_Hdr_Type = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Incmplte_Hdr_Type", "INCMPLTE-HDR-TYPE", FieldType.STRING, 
            1);
        pstl9700_Data_Incmplte_Cdes_Fed = pstl9700_Data_Psta3005_Tax.newFieldArrayInGroup("pstl9700_Data_Incmplte_Cdes_Fed", "INCMPLTE-CDES-FED", FieldType.STRING, 
            10, new DbsArrayController(1,10));
        pstl9700_Data_Incmplte_Fed_Hdr = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Incmplte_Fed_Hdr", "INCMPLTE-FED-HDR", FieldType.STRING, 
            16);
        pstl9700_Data_Incmplte_Cdes_State = pstl9700_Data_Psta3005_Tax.newFieldArrayInGroup("pstl9700_Data_Incmplte_Cdes_State", "INCMPLTE-CDES-STATE", 
            FieldType.STRING, 10, new DbsArrayController(1,10));
        pstl9700_Data_Incmplte_State_Hdr = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Incmplte_State_Hdr", "INCMPLTE-STATE-HDR", FieldType.STRING, 
            16);
        pstl9700_Data_Tax_Addr_Ltr = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Tax_Addr_Ltr", "TAX-ADDR-LTR", FieldType.STRING, 1);
        pstl9700_Data_From_State_Nme = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_From_State_Nme", "FROM-STATE-NME", FieldType.STRING, 
            14);
        pstl9700_Data_To_State_Nme = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_To_State_Nme", "TO-STATE-NME", FieldType.STRING, 14);
        pstl9700_Data_Cntrct_Desc = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_Cntrct_Desc", "CNTRCT-DESC", FieldType.STRING, 21);
        pstl9700_Data_C_Contract_Sets = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_C_Contract_Sets", "C-CONTRACT-SETS", FieldType.NUMERIC, 
            2);
        pstl9700_Data_This_Set_Index = pstl9700_Data_Psta3005_Tax.newFieldInGroup("pstl9700_Data_This_Set_Index", "THIS-SET-INDEX", FieldType.NUMERIC, 
            2);
        pstl9700_Data_Psta3005_Set_Tax = pstl9700_Data_Pstl9700_Data_ArrayRedef1.newGroupInGroup("pstl9700_Data_Psta3005_Set_Tax", "PSTA3005-SET-TAX");
        pstl9700_Data_C_Contracts = pstl9700_Data_Psta3005_Set_Tax.newFieldInGroup("pstl9700_Data_C_Contracts", "C-CONTRACTS", FieldType.NUMERIC, 2);
        pstl9700_Data_Cntrct_Nbr = pstl9700_Data_Psta3005_Set_Tax.newFieldArrayInGroup("pstl9700_Data_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 9, 
            new DbsArrayController(1,10));
        pstl9700_Data_Cref_Nbr = pstl9700_Data_Psta3005_Set_Tax.newFieldArrayInGroup("pstl9700_Data_Cref_Nbr", "CREF-NBR", FieldType.STRING, 9, new DbsArrayController(1,
            10));
        pstl9700_Data_Nbr_Of_Copies = pstl9700_Data_Psta3005_Set_Tax.newFieldInGroup("pstl9700_Data_Nbr_Of_Copies", "NBR-OF-COPIES", FieldType.NUMERIC, 
            1);
        pstl9700_Data_Addrss_Lne = pstl9700_Data_Psta3005_Set_Tax.newFieldArrayInGroup("pstl9700_Data_Addrss_Lne", "ADDRSS-LNE", FieldType.STRING, 35, 
            new DbsArrayController(1,6));
        pstl9700_Data_Addrss_Type_Cde = pstl9700_Data_Psta3005_Set_Tax.newFieldInGroup("pstl9700_Data_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pstl9700_Data_Addrss_State_Cde = pstl9700_Data_Psta3005_Set_Tax.newFieldInGroup("pstl9700_Data_Addrss_State_Cde", "ADDRSS-STATE-CDE", FieldType.STRING, 
            2);
        pstl9700_Data_Addrss_Postal_Data = pstl9700_Data_Psta3005_Set_Tax.newFieldInGroup("pstl9700_Data_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);
        pstl9700_Data_Addrss_Postal_DataRedef2 = pstl9700_Data_Psta3005_Set_Tax.newGroupInGroup("pstl9700_Data_Addrss_Postal_DataRedef2", "Redefines", 
            pstl9700_Data_Addrss_Postal_Data);
        pstl9700_Data_Addrss_Zip_Plus_4 = pstl9700_Data_Addrss_Postal_DataRedef2.newFieldInGroup("pstl9700_Data_Addrss_Zip_Plus_4", "ADDRSS-ZIP-PLUS-4", 
            FieldType.STRING, 9);
        pstl9700_Data_Addrss_Zip_Plus_4Redef3 = pstl9700_Data_Addrss_Postal_DataRedef2.newGroupInGroup("pstl9700_Data_Addrss_Zip_Plus_4Redef3", "Redefines", 
            pstl9700_Data_Addrss_Zip_Plus_4);
        pstl9700_Data_Addrss_Zip_5 = pstl9700_Data_Addrss_Zip_Plus_4Redef3.newFieldInGroup("pstl9700_Data_Addrss_Zip_5", "ADDRSS-ZIP-5", FieldType.STRING, 
            5);
        pstl9700_Data_Addrss_Zip_4 = pstl9700_Data_Addrss_Zip_Plus_4Redef3.newFieldInGroup("pstl9700_Data_Addrss_Zip_4", "ADDRSS-ZIP-4", FieldType.STRING, 
            4);
        pstl9700_Data_Federal_Data = pstl9700_Data_Psta3005_Set_Tax.newGroupInGroup("pstl9700_Data_Federal_Data", "FEDERAL-DATA");
        pstl9700_Data_New_Election_In_Words = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_New_Election_In_Words", "NEW-ELECTION-IN-WORDS", 
            FieldType.STRING, 35);
        pstl9700_Data_Effective_Date_Month = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Effective_Date_Month", "EFFECTIVE-DATE-MONTH", 
            FieldType.STRING, 5);
        pstl9700_Data_Withhold_Status_No = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Withhold_Status_No", "WITHHOLD-STATUS-NO", FieldType.BOOLEAN);
        pstl9700_Data_Withhold_Status_Yes = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Withhold_Status_Yes", "WITHHOLD-STATUS-YES", FieldType.BOOLEAN);
        pstl9700_Data_Marital_Tax_Status = pstl9700_Data_Federal_Data.newGroupInGroup("pstl9700_Data_Marital_Tax_Status", "MARITAL-TAX-STATUS");
        pstl9700_Data_Marital_Single = pstl9700_Data_Marital_Tax_Status.newFieldInGroup("pstl9700_Data_Marital_Single", "MARITAL-SINGLE", FieldType.BOOLEAN);
        pstl9700_Data_Marital_Married = pstl9700_Data_Marital_Tax_Status.newFieldInGroup("pstl9700_Data_Marital_Married", "MARITAL-MARRIED", FieldType.BOOLEAN);
        pstl9700_Data_Marital_Higher = pstl9700_Data_Marital_Tax_Status.newFieldInGroup("pstl9700_Data_Marital_Higher", "MARITAL-HIGHER", FieldType.BOOLEAN);
        pstl9700_Data_Nmbr_Allowances = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Nmbr_Allowances", "NMBR-ALLOWANCES", FieldType.STRING, 
            2);
        pstl9700_Data_Withhold_Info = pstl9700_Data_Federal_Data.newGroupInGroup("pstl9700_Data_Withhold_Info", "WITHHOLD-INFO");
        pstl9700_Data_Withhold_Addl = pstl9700_Data_Withhold_Info.newFieldInGroup("pstl9700_Data_Withhold_Addl", "WITHHOLD-ADDL", FieldType.BOOLEAN);
        pstl9700_Data_Addl_Tiaa_Amount = pstl9700_Data_Withhold_Info.newFieldInGroup("pstl9700_Data_Addl_Tiaa_Amount", "ADDL-TIAA-AMOUNT", FieldType.PACKED_DECIMAL, 
            9,2);
        pstl9700_Data_Tiaa_Frmt_Info = pstl9700_Data_Withhold_Info.newGroupInGroup("pstl9700_Data_Tiaa_Frmt_Info", "TIAA-FRMT-INFO");
        pstl9700_Data_Tiaa_Decimals = pstl9700_Data_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Tiaa_Decimals", "TIAA-DECIMALS", FieldType.NUMERIC, 
            1);
        pstl9700_Data_Tiaa_Type = pstl9700_Data_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Tiaa_Type", "TIAA-TYPE", FieldType.STRING, 1);
        pstl9700_Data_Tiaa_Right_Just = pstl9700_Data_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Tiaa_Right_Just", "TIAA-RIGHT-JUST", FieldType.BOOLEAN);
        pstl9700_Data_Tiaa_Zero_Print = pstl9700_Data_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Tiaa_Zero_Print", "TIAA-ZERO-PRINT", FieldType.BOOLEAN);
        pstl9700_Data_Addl_Cref_Amount = pstl9700_Data_Withhold_Info.newFieldInGroup("pstl9700_Data_Addl_Cref_Amount", "ADDL-CREF-AMOUNT", FieldType.PACKED_DECIMAL, 
            9,2);
        pstl9700_Data_Cref_Frmt_Info = pstl9700_Data_Withhold_Info.newGroupInGroup("pstl9700_Data_Cref_Frmt_Info", "CREF-FRMT-INFO");
        pstl9700_Data_Cref_Decimals = pstl9700_Data_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Cref_Decimals", "CREF-DECIMALS", FieldType.NUMERIC, 
            1);
        pstl9700_Data_Cref_Type = pstl9700_Data_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Cref_Type", "CREF-TYPE", FieldType.STRING, 1);
        pstl9700_Data_Cref_Right_Just = pstl9700_Data_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Cref_Right_Just", "CREF-RIGHT-JUST", FieldType.BOOLEAN);
        pstl9700_Data_Cref_Zero_Print = pstl9700_Data_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Cref_Zero_Print", "CREF-ZERO-PRINT", FieldType.BOOLEAN);
        pstl9700_Data_Data_Type_Flat = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Data_Type_Flat", "DATA-TYPE-FLAT", FieldType.BOOLEAN);
        pstl9700_Data_Flat_Tiaa_Amount = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Flat_Tiaa_Amount", "FLAT-TIAA-AMOUNT", FieldType.PACKED_DECIMAL, 
            9,2);
        pstl9700_Data_Flatt_Tiaa_Frmt_Info = pstl9700_Data_Federal_Data.newGroupInGroup("pstl9700_Data_Flatt_Tiaa_Frmt_Info", "FLATT-TIAA-FRMT-INFO");
        pstl9700_Data_Flat_Tiaa_Decimals = pstl9700_Data_Flatt_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Tiaa_Decimals", "FLAT-TIAA-DECIMALS", 
            FieldType.NUMERIC, 1);
        pstl9700_Data_Flat_Tiaa_Type = pstl9700_Data_Flatt_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Tiaa_Type", "FLAT-TIAA-TYPE", FieldType.STRING, 
            1);
        pstl9700_Data_Flat_Tiaa_Right_Just = pstl9700_Data_Flatt_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Tiaa_Right_Just", "FLAT-TIAA-RIGHT-JUST", 
            FieldType.BOOLEAN);
        pstl9700_Data_Flat_Tiaa_Zero_Print = pstl9700_Data_Flatt_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Tiaa_Zero_Print", "FLAT-TIAA-ZERO-PRINT", 
            FieldType.BOOLEAN);
        pstl9700_Data_Flat_Cref_Amount = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Flat_Cref_Amount", "FLAT-CREF-AMOUNT", FieldType.PACKED_DECIMAL, 
            9,2);
        pstl9700_Data_Flat_Cref_Frmt_Info = pstl9700_Data_Federal_Data.newGroupInGroup("pstl9700_Data_Flat_Cref_Frmt_Info", "FLAT-CREF-FRMT-INFO");
        pstl9700_Data_Flat_Cref_Decimals = pstl9700_Data_Flat_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Cref_Decimals", "FLAT-CREF-DECIMALS", 
            FieldType.NUMERIC, 1);
        pstl9700_Data_Flat_Cref_Type = pstl9700_Data_Flat_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Cref_Type", "FLAT-CREF-TYPE", FieldType.STRING, 
            1);
        pstl9700_Data_Flat_Cref_Right_Just = pstl9700_Data_Flat_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Cref_Right_Just", "FLAT-CREF-RIGHT-JUST", 
            FieldType.BOOLEAN);
        pstl9700_Data_Flat_Cref_Zero_Print = pstl9700_Data_Flat_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Flat_Cref_Zero_Print", "FLAT-CREF-ZERO-PRINT", 
            FieldType.BOOLEAN);
        pstl9700_Data_Data_Type_Fixd = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Data_Type_Fixd", "DATA-TYPE-FIXD", FieldType.BOOLEAN);
        pstl9700_Data_Fixd_Tiaa_Percent = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Fixd_Tiaa_Percent", "FIXD-TIAA-PERCENT", FieldType.PACKED_DECIMAL, 
            5,2);
        pstl9700_Data_Fixd_Tiaa_Frmt_Info = pstl9700_Data_Federal_Data.newGroupInGroup("pstl9700_Data_Fixd_Tiaa_Frmt_Info", "FIXD-TIAA-FRMT-INFO");
        pstl9700_Data_Fixd_Tiaa_Decimals = pstl9700_Data_Fixd_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Tiaa_Decimals", "FIXD-TIAA-DECIMALS", 
            FieldType.NUMERIC, 1);
        pstl9700_Data_Fixd_Tiaa_Type = pstl9700_Data_Fixd_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Tiaa_Type", "FIXD-TIAA-TYPE", FieldType.STRING, 
            1);
        pstl9700_Data_Fixd_Tiaa_Right_Just = pstl9700_Data_Fixd_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Tiaa_Right_Just", "FIXD-TIAA-RIGHT-JUST", 
            FieldType.BOOLEAN);
        pstl9700_Data_Fixd_Tiaa_Zero_Print = pstl9700_Data_Fixd_Tiaa_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Tiaa_Zero_Print", "FIXD-TIAA-ZERO-PRINT", 
            FieldType.BOOLEAN);
        pstl9700_Data_Fixd_Cref_Percent = pstl9700_Data_Federal_Data.newFieldInGroup("pstl9700_Data_Fixd_Cref_Percent", "FIXD-CREF-PERCENT", FieldType.PACKED_DECIMAL, 
            5,2);
        pstl9700_Data_Fixd_Cref_Frmt_Info = pstl9700_Data_Federal_Data.newGroupInGroup("pstl9700_Data_Fixd_Cref_Frmt_Info", "FIXD-CREF-FRMT-INFO");
        pstl9700_Data_Fixd_Cref_Decimals = pstl9700_Data_Fixd_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Cref_Decimals", "FIXD-CREF-DECIMALS", 
            FieldType.NUMERIC, 1);
        pstl9700_Data_Fixd_Cref_Type = pstl9700_Data_Fixd_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Cref_Type", "FIXD-CREF-TYPE", FieldType.STRING, 
            1);
        pstl9700_Data_Fixd_Cref_Right_Just = pstl9700_Data_Fixd_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Cref_Right_Just", "FIXD-CREF-RIGHT-JUST", 
            FieldType.BOOLEAN);
        pstl9700_Data_Fixd_Cref_Zero_Print = pstl9700_Data_Fixd_Cref_Frmt_Info.newFieldInGroup("pstl9700_Data_Fixd_Cref_Zero_Print", "FIXD-CREF-ZERO-PRINT", 
            FieldType.BOOLEAN);
        pstl9700_Data_State_Data = pstl9700_Data_Psta3005_Set_Tax.newGroupInGroup("pstl9700_Data_State_Data", "STATE-DATA");
        pstl9700_Data_No_Wthhld_Ind = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_No_Wthhld_Ind", "NO-WTHHLD-IND", FieldType.BOOLEAN);
        pstl9700_Data_Wthhld_Ind = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Wthhld_Ind", "WTHHLD-IND", FieldType.BOOLEAN);
        pstl9700_Data_Wthhld_Amnt = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Wthhld_Amnt", "WTHHLD-AMNT", FieldType.PACKED_DECIMAL, 9,2);
        pstl9700_Data_Wthhld_Amnt_Frmt = pstl9700_Data_State_Data.newGroupInGroup("pstl9700_Data_Wthhld_Amnt_Frmt", "WTHHLD-AMNT-FRMT");
        pstl9700_Data_Wthhld_Decimals = pstl9700_Data_Wthhld_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Wthhld_Decimals", "WTHHLD-DECIMALS", FieldType.NUMERIC, 
            1);
        pstl9700_Data_Wthhld_Type = pstl9700_Data_Wthhld_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Wthhld_Type", "WTHHLD-TYPE", FieldType.STRING, 1);
        pstl9700_Data_Wthhld_Right_Just = pstl9700_Data_Wthhld_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Wthhld_Right_Just", "WTHHLD-RIGHT-JUST", FieldType.BOOLEAN);
        pstl9700_Data_Wthhld_Zero_Print = pstl9700_Data_Wthhld_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Wthhld_Zero_Print", "WTHHLD-ZERO-PRINT", FieldType.BOOLEAN);
        pstl9700_Data_Wthhld_Addl_Ind = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Wthhld_Addl_Ind", "WTHHLD-ADDL-IND", FieldType.BOOLEAN);
        pstl9700_Data_Wthhld_Addl_Amnt = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Wthhld_Addl_Amnt", "WTHHLD-ADDL-AMNT", FieldType.PACKED_DECIMAL, 
            9,2);
        pstl9700_Data_Wthhld_Addl_Amnt_Frmt = pstl9700_Data_State_Data.newGroupInGroup("pstl9700_Data_Wthhld_Addl_Amnt_Frmt", "WTHHLD-ADDL-AMNT-FRMT");
        pstl9700_Data_Addl_Decimals = pstl9700_Data_Wthhld_Addl_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Addl_Decimals", "ADDL-DECIMALS", FieldType.NUMERIC, 
            1);
        pstl9700_Data_Addl_Type = pstl9700_Data_Wthhld_Addl_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Addl_Type", "ADDL-TYPE", FieldType.STRING, 1);
        pstl9700_Data_Addl_Right_Just = pstl9700_Data_Wthhld_Addl_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Addl_Right_Just", "ADDL-RIGHT-JUST", FieldType.BOOLEAN);
        pstl9700_Data_Addl_Zero_Print = pstl9700_Data_Wthhld_Addl_Amnt_Frmt.newFieldInGroup("pstl9700_Data_Addl_Zero_Print", "ADDL-ZERO-PRINT", FieldType.BOOLEAN);
        pstl9700_Data_Rvke_Prev_Elctn = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Rvke_Prev_Elctn", "RVKE-PREV-ELCTN", FieldType.BOOLEAN);
        pstl9700_Data_Exempt = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Exempt", "EXEMPT", FieldType.BOOLEAN);
        pstl9700_Data_Not_Subject_To = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Not_Subject_To", "NOT-SUBJECT-TO", FieldType.BOOLEAN);
        pstl9700_Data_Redce_Wthhldng = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Redce_Wthhldng", "REDCE-WTHHLDNG", FieldType.BOOLEAN);
        pstl9700_Data_Nbr_Exmptns = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Nbr_Exmptns", "NBR-EXMPTNS", FieldType.STRING, 2);
        pstl9700_Data_Spse_Exmptn = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Spse_Exmptn", "SPSE-EXMPTN", FieldType.BOOLEAN);
        pstl9700_Data_Blind_Exmptn = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Blind_Exmptn", "BLIND-EXMPTN", FieldType.BOOLEAN);
        pstl9700_Data_Blind_Spse_Exmptn = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Blind_Spse_Exmptn", "BLIND-SPSE-EXMPTN", FieldType.BOOLEAN);
        pstl9700_Data_Married = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Married", "MARRIED", FieldType.BOOLEAN);
        pstl9700_Data_Single = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Single", "SINGLE", FieldType.BOOLEAN);
        pstl9700_Data_Married_Higher = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Married_Higher", "MARRIED-HIGHER", FieldType.BOOLEAN);
        pstl9700_Data_Married_Separate = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Married_Separate", "MARRIED-SEPARATE", FieldType.BOOLEAN);
        pstl9700_Data_Over_65 = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Over_65", "OVER-65", FieldType.BOOLEAN);
        pstl9700_Data_Spse_Over_65 = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Spse_Over_65", "SPSE-OVER-65", FieldType.BOOLEAN);
        pstl9700_Data_Wthhld_Flat_Ind = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_Wthhld_Flat_Ind", "WTHHLD-FLAT-IND", FieldType.BOOLEAN);
        pstl9700_Data_State_Elctn_In_Words = pstl9700_Data_State_Data.newFieldInGroup("pstl9700_Data_State_Elctn_In_Words", "STATE-ELCTN-IN-WORDS", FieldType.STRING, 
            35);
        pstl9700_Data_Psta3006_Tax = pstl9700_Data_Pstl9700_Data_ArrayRedef1.newGroupInGroup("pstl9700_Data_Psta3006_Tax", "PSTA3006-TAX");
        pstl9700_Data_User_Selected_Options = pstl9700_Data_Psta3006_Tax.newGroupInGroup("pstl9700_Data_User_Selected_Options", "USER-SELECTED-OPTIONS");
        pstl9700_Data_Cover_Ltr_Req = pstl9700_Data_User_Selected_Options.newFieldInGroup("pstl9700_Data_Cover_Ltr_Req", "COVER-LTR-REQ", FieldType.BOOLEAN);
        pstl9700_Data_Fed_Form_Req = pstl9700_Data_User_Selected_Options.newFieldInGroup("pstl9700_Data_Fed_Form_Req", "FED-FORM-REQ", FieldType.BOOLEAN);
        pstl9700_Data_State_Form_Req = pstl9700_Data_User_Selected_Options.newFieldInGroup("pstl9700_Data_State_Form_Req", "STATE-FORM-REQ", FieldType.BOOLEAN);
        pstl9700_Data_Prefill_Fed = pstl9700_Data_User_Selected_Options.newFieldInGroup("pstl9700_Data_Prefill_Fed", "PREFILL-FED", FieldType.BOOLEAN);
        pstl9700_Data_Prefill_State = pstl9700_Data_User_Selected_Options.newFieldInGroup("pstl9700_Data_Prefill_State", "PREFILL-STATE", FieldType.BOOLEAN);
        pstl9700_Data_Package_Selected = pstl9700_Data_Psta3006_Tax.newGroupInGroup("pstl9700_Data_Package_Selected", "PACKAGE-SELECTED");
        pstl9700_Data_Initial_Sel = pstl9700_Data_Package_Selected.newFieldInGroup("pstl9700_Data_Initial_Sel", "INITIAL-SEL", FieldType.BOOLEAN);
        pstl9700_Data_Incmplte_Sel = pstl9700_Data_Package_Selected.newFieldInGroup("pstl9700_Data_Incmplte_Sel", "INCMPLTE-SEL", FieldType.BOOLEAN);
        pstl9700_Data_Phonecf_Sel = pstl9700_Data_Package_Selected.newFieldInGroup("pstl9700_Data_Phonecf_Sel", "PHONECF-SEL", FieldType.BOOLEAN);
        pstl9700_Data_Incmplte_Selected = pstl9700_Data_Psta3006_Tax.newGroupInGroup("pstl9700_Data_Incmplte_Selected", "INCMPLTE-SELECTED");
        pstl9700_Data_Incmplte_Federal = pstl9700_Data_Incmplte_Selected.newFieldInGroup("pstl9700_Data_Incmplte_Federal", "INCMPLTE-FEDERAL", FieldType.BOOLEAN);
        pstl9700_Data_Incmplte_State = pstl9700_Data_Incmplte_Selected.newFieldInGroup("pstl9700_Data_Incmplte_State", "INCMPLTE-STATE", FieldType.BOOLEAN);
        pstl9700_Data_Mandatory_Tax_Form_Indicators = pstl9700_Data_Pstl9700_Data_ArrayRedef1.newGroupInGroup("pstl9700_Data_Mandatory_Tax_Form_Indicators", 
            "MANDATORY-TAX-FORM-INDICATORS");
        pstl9700_Data_Rollover_Ind = pstl9700_Data_Mandatory_Tax_Form_Indicators.newFieldInGroup("pstl9700_Data_Rollover_Ind", "ROLLOVER-IND", FieldType.STRING, 
            1);
        pstl9700_Data_Periodic_Ind = pstl9700_Data_Mandatory_Tax_Form_Indicators.newFieldInGroup("pstl9700_Data_Periodic_Ind", "PERIODIC-IND", FieldType.STRING, 
            1);

        this.setRecordName("LdaFcpl9700");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        pstl9700_Data_Pstl9700_Rec_Id.setInitialValue("a");
        pstl9700_Data_Pstl9700_Data_Lgth.setInitialValue(910);
        pstl9700_Data_Pstl9700_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaFcpl9700() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
