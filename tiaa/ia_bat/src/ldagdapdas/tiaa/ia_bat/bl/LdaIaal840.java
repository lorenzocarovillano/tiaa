/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:33 PM
**        * FROM NATURAL LDA     : IAAL840
************************************************************
**        * FILE NAME            : LdaIaal840.java
**        * CLASS NAME           : LdaIaal840
**        * INSTANCE NAME        : LdaIaal840
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal840 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Pnd_State_01;
    private DbsField pnd_State_Table_Pnd_State_02;
    private DbsField pnd_State_Table_Pnd_State_03;
    private DbsField pnd_State_Table_Pnd_State_04;
    private DbsField pnd_State_Table_Pnd_State_05;
    private DbsField pnd_State_Table_Pnd_State_06;
    private DbsField pnd_State_Table_Pnd_State_07;
    private DbsField pnd_State_Table_Pnd_State_08;
    private DbsField pnd_State_Table_Pnd_State_09;
    private DbsField pnd_State_Table_Pnd_State_10;
    private DbsField pnd_State_Table_Pnd_State_11;
    private DbsField pnd_State_Table_Pnd_State_12;
    private DbsField pnd_State_Table_Pnd_State_13;
    private DbsField pnd_State_Table_Pnd_State_14;
    private DbsField pnd_State_Table_Pnd_State_15;
    private DbsField pnd_State_Table_Pnd_State_16;
    private DbsField pnd_State_Table_Pnd_State_17;
    private DbsField pnd_State_Table_Pnd_State_18;
    private DbsField pnd_State_Table_Pnd_State_19;
    private DbsField pnd_State_Table_Pnd_State_20;
    private DbsField pnd_State_Table_Pnd_State_21;
    private DbsField pnd_State_Table_Pnd_State_22;
    private DbsField pnd_State_Table_Pnd_State_23;
    private DbsField pnd_State_Table_Pnd_State_24;
    private DbsField pnd_State_Table_Pnd_State_25;
    private DbsField pnd_State_Table_Pnd_State_26;
    private DbsField pnd_State_Table_Pnd_State_27;
    private DbsField pnd_State_Table_Pnd_State_28;
    private DbsField pnd_State_Table_Pnd_State_29;
    private DbsField pnd_State_Table_Pnd_State_30;
    private DbsField pnd_State_Table_Pnd_State_31;
    private DbsField pnd_State_Table_Pnd_State_32;
    private DbsField pnd_State_Table_Pnd_State_33;
    private DbsField pnd_State_Table_Pnd_State_34;
    private DbsField pnd_State_Table_Pnd_State_35;
    private DbsField pnd_State_Table_Pnd_State_36;
    private DbsField pnd_State_Table_Pnd_State_37;
    private DbsField pnd_State_Table_Pnd_State_38;
    private DbsField pnd_State_Table_Pnd_State_39;
    private DbsField pnd_State_Table_Pnd_State_40;
    private DbsField pnd_State_Table_Pnd_State_41;
    private DbsField pnd_State_Table_Pnd_State_42;
    private DbsField pnd_State_Table_Pnd_State_43;
    private DbsField pnd_State_Table_Pnd_State_44;
    private DbsField pnd_State_Table_Pnd_State_45;
    private DbsField pnd_State_Table_Pnd_State_46;
    private DbsField pnd_State_Table_Pnd_State_47;
    private DbsField pnd_State_Table_Pnd_State_48;
    private DbsField pnd_State_Table_Pnd_State_49;
    private DbsField pnd_State_Table_Pnd_State_50;
    private DbsField pnd_State_Table_Pnd_State_51;
    private DbsField pnd_State_Table_Pnd_State_52;
    private DbsField pnd_State_Table_Pnd_State_53;
    private DbsField pnd_State_Table_Pnd_State_54;
    private DbsField pnd_State_Table_Pnd_State_55;
    private DbsField pnd_State_Table_Pnd_State_56;
    private DbsField pnd_State_Table_Pnd_State_57;
    private DbsField pnd_State_Table_Pnd_State_58;
    private DbsField pnd_State_Table_Pnd_State_59;
    private DbsField pnd_State_Table_Pnd_State_60;
    private DbsGroup pnd_State_TableRedef1;
    private DbsField pnd_State_Table_Pnd_Days;

    public DbsGroup getPnd_State_Table() { return pnd_State_Table; }

    public DbsField getPnd_State_Table_Pnd_State_01() { return pnd_State_Table_Pnd_State_01; }

    public DbsField getPnd_State_Table_Pnd_State_02() { return pnd_State_Table_Pnd_State_02; }

    public DbsField getPnd_State_Table_Pnd_State_03() { return pnd_State_Table_Pnd_State_03; }

    public DbsField getPnd_State_Table_Pnd_State_04() { return pnd_State_Table_Pnd_State_04; }

    public DbsField getPnd_State_Table_Pnd_State_05() { return pnd_State_Table_Pnd_State_05; }

    public DbsField getPnd_State_Table_Pnd_State_06() { return pnd_State_Table_Pnd_State_06; }

    public DbsField getPnd_State_Table_Pnd_State_07() { return pnd_State_Table_Pnd_State_07; }

    public DbsField getPnd_State_Table_Pnd_State_08() { return pnd_State_Table_Pnd_State_08; }

    public DbsField getPnd_State_Table_Pnd_State_09() { return pnd_State_Table_Pnd_State_09; }

    public DbsField getPnd_State_Table_Pnd_State_10() { return pnd_State_Table_Pnd_State_10; }

    public DbsField getPnd_State_Table_Pnd_State_11() { return pnd_State_Table_Pnd_State_11; }

    public DbsField getPnd_State_Table_Pnd_State_12() { return pnd_State_Table_Pnd_State_12; }

    public DbsField getPnd_State_Table_Pnd_State_13() { return pnd_State_Table_Pnd_State_13; }

    public DbsField getPnd_State_Table_Pnd_State_14() { return pnd_State_Table_Pnd_State_14; }

    public DbsField getPnd_State_Table_Pnd_State_15() { return pnd_State_Table_Pnd_State_15; }

    public DbsField getPnd_State_Table_Pnd_State_16() { return pnd_State_Table_Pnd_State_16; }

    public DbsField getPnd_State_Table_Pnd_State_17() { return pnd_State_Table_Pnd_State_17; }

    public DbsField getPnd_State_Table_Pnd_State_18() { return pnd_State_Table_Pnd_State_18; }

    public DbsField getPnd_State_Table_Pnd_State_19() { return pnd_State_Table_Pnd_State_19; }

    public DbsField getPnd_State_Table_Pnd_State_20() { return pnd_State_Table_Pnd_State_20; }

    public DbsField getPnd_State_Table_Pnd_State_21() { return pnd_State_Table_Pnd_State_21; }

    public DbsField getPnd_State_Table_Pnd_State_22() { return pnd_State_Table_Pnd_State_22; }

    public DbsField getPnd_State_Table_Pnd_State_23() { return pnd_State_Table_Pnd_State_23; }

    public DbsField getPnd_State_Table_Pnd_State_24() { return pnd_State_Table_Pnd_State_24; }

    public DbsField getPnd_State_Table_Pnd_State_25() { return pnd_State_Table_Pnd_State_25; }

    public DbsField getPnd_State_Table_Pnd_State_26() { return pnd_State_Table_Pnd_State_26; }

    public DbsField getPnd_State_Table_Pnd_State_27() { return pnd_State_Table_Pnd_State_27; }

    public DbsField getPnd_State_Table_Pnd_State_28() { return pnd_State_Table_Pnd_State_28; }

    public DbsField getPnd_State_Table_Pnd_State_29() { return pnd_State_Table_Pnd_State_29; }

    public DbsField getPnd_State_Table_Pnd_State_30() { return pnd_State_Table_Pnd_State_30; }

    public DbsField getPnd_State_Table_Pnd_State_31() { return pnd_State_Table_Pnd_State_31; }

    public DbsField getPnd_State_Table_Pnd_State_32() { return pnd_State_Table_Pnd_State_32; }

    public DbsField getPnd_State_Table_Pnd_State_33() { return pnd_State_Table_Pnd_State_33; }

    public DbsField getPnd_State_Table_Pnd_State_34() { return pnd_State_Table_Pnd_State_34; }

    public DbsField getPnd_State_Table_Pnd_State_35() { return pnd_State_Table_Pnd_State_35; }

    public DbsField getPnd_State_Table_Pnd_State_36() { return pnd_State_Table_Pnd_State_36; }

    public DbsField getPnd_State_Table_Pnd_State_37() { return pnd_State_Table_Pnd_State_37; }

    public DbsField getPnd_State_Table_Pnd_State_38() { return pnd_State_Table_Pnd_State_38; }

    public DbsField getPnd_State_Table_Pnd_State_39() { return pnd_State_Table_Pnd_State_39; }

    public DbsField getPnd_State_Table_Pnd_State_40() { return pnd_State_Table_Pnd_State_40; }

    public DbsField getPnd_State_Table_Pnd_State_41() { return pnd_State_Table_Pnd_State_41; }

    public DbsField getPnd_State_Table_Pnd_State_42() { return pnd_State_Table_Pnd_State_42; }

    public DbsField getPnd_State_Table_Pnd_State_43() { return pnd_State_Table_Pnd_State_43; }

    public DbsField getPnd_State_Table_Pnd_State_44() { return pnd_State_Table_Pnd_State_44; }

    public DbsField getPnd_State_Table_Pnd_State_45() { return pnd_State_Table_Pnd_State_45; }

    public DbsField getPnd_State_Table_Pnd_State_46() { return pnd_State_Table_Pnd_State_46; }

    public DbsField getPnd_State_Table_Pnd_State_47() { return pnd_State_Table_Pnd_State_47; }

    public DbsField getPnd_State_Table_Pnd_State_48() { return pnd_State_Table_Pnd_State_48; }

    public DbsField getPnd_State_Table_Pnd_State_49() { return pnd_State_Table_Pnd_State_49; }

    public DbsField getPnd_State_Table_Pnd_State_50() { return pnd_State_Table_Pnd_State_50; }

    public DbsField getPnd_State_Table_Pnd_State_51() { return pnd_State_Table_Pnd_State_51; }

    public DbsField getPnd_State_Table_Pnd_State_52() { return pnd_State_Table_Pnd_State_52; }

    public DbsField getPnd_State_Table_Pnd_State_53() { return pnd_State_Table_Pnd_State_53; }

    public DbsField getPnd_State_Table_Pnd_State_54() { return pnd_State_Table_Pnd_State_54; }

    public DbsField getPnd_State_Table_Pnd_State_55() { return pnd_State_Table_Pnd_State_55; }

    public DbsField getPnd_State_Table_Pnd_State_56() { return pnd_State_Table_Pnd_State_56; }

    public DbsField getPnd_State_Table_Pnd_State_57() { return pnd_State_Table_Pnd_State_57; }

    public DbsField getPnd_State_Table_Pnd_State_58() { return pnd_State_Table_Pnd_State_58; }

    public DbsField getPnd_State_Table_Pnd_State_59() { return pnd_State_Table_Pnd_State_59; }

    public DbsField getPnd_State_Table_Pnd_State_60() { return pnd_State_Table_Pnd_State_60; }

    public DbsGroup getPnd_State_TableRedef1() { return pnd_State_TableRedef1; }

    public DbsField getPnd_State_Table_Pnd_Days() { return pnd_State_Table_Pnd_Days; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_State_Table = newGroupInRecord("pnd_State_Table", "#STATE-TABLE");
        pnd_State_Table_Pnd_State_01 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_01", "#STATE-01", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_02 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_02", "#STATE-02", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_03 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_03", "#STATE-03", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_04 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_04", "#STATE-04", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_05 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_05", "#STATE-05", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_06 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_06", "#STATE-06", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_07 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_07", "#STATE-07", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_08 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_08", "#STATE-08", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_09 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_09", "#STATE-09", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_10 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_10", "#STATE-10", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_11 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_11", "#STATE-11", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_12 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_12", "#STATE-12", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_13 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_13", "#STATE-13", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_14 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_14", "#STATE-14", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_15 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_15", "#STATE-15", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_16 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_16", "#STATE-16", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_17 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_17", "#STATE-17", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_18 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_18", "#STATE-18", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_19 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_19", "#STATE-19", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_20 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_20", "#STATE-20", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_21 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_21", "#STATE-21", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_22 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_22", "#STATE-22", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_23 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_23", "#STATE-23", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_24 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_24", "#STATE-24", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_25 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_25", "#STATE-25", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_26 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_26", "#STATE-26", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_27 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_27", "#STATE-27", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_28 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_28", "#STATE-28", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_29 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_29", "#STATE-29", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_30 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_30", "#STATE-30", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_31 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_31", "#STATE-31", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_32 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_32", "#STATE-32", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_33 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_33", "#STATE-33", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_34 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_34", "#STATE-34", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_35 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_35", "#STATE-35", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_36 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_36", "#STATE-36", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_37 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_37", "#STATE-37", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_38 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_38", "#STATE-38", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_39 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_39", "#STATE-39", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_40 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_40", "#STATE-40", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_41 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_41", "#STATE-41", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_42 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_42", "#STATE-42", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_43 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_43", "#STATE-43", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_44 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_44", "#STATE-44", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_45 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_45", "#STATE-45", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_46 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_46", "#STATE-46", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_47 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_47", "#STATE-47", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_48 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_48", "#STATE-48", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_49 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_49", "#STATE-49", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_50 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_50", "#STATE-50", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_51 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_51", "#STATE-51", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_52 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_52", "#STATE-52", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_53 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_53", "#STATE-53", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_54 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_54", "#STATE-54", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_55 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_55", "#STATE-55", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_56 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_56", "#STATE-56", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_57 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_57", "#STATE-57", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_58 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_58", "#STATE-58", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_59 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_59", "#STATE-59", FieldType.STRING, 2);
        pnd_State_Table_Pnd_State_60 = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_State_60", "#STATE-60", FieldType.STRING, 2);
        pnd_State_TableRedef1 = newGroupInRecord("pnd_State_TableRedef1", "Redefines", pnd_State_Table);
        pnd_State_Table_Pnd_Days = pnd_State_TableRedef1.newFieldArrayInGroup("pnd_State_Table_Pnd_Days", "#DAYS", FieldType.STRING, 2, new DbsArrayController(1,
            60));

        this.setRecordName("LdaIaal840");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_State_Table_Pnd_State_01.setInitialValue("  ");
        pnd_State_Table_Pnd_State_02.setInitialValue("  ");
        pnd_State_Table_Pnd_State_03.setInitialValue("  ");
        pnd_State_Table_Pnd_State_04.setInitialValue("  ");
        pnd_State_Table_Pnd_State_05.setInitialValue("  ");
        pnd_State_Table_Pnd_State_06.setInitialValue("  ");
        pnd_State_Table_Pnd_State_07.setInitialValue("  ");
        pnd_State_Table_Pnd_State_08.setInitialValue("  ");
        pnd_State_Table_Pnd_State_09.setInitialValue("  ");
        pnd_State_Table_Pnd_State_10.setInitialValue("  ");
        pnd_State_Table_Pnd_State_11.setInitialValue("  ");
        pnd_State_Table_Pnd_State_12.setInitialValue("10");
        pnd_State_Table_Pnd_State_13.setInitialValue("  ");
        pnd_State_Table_Pnd_State_14.setInitialValue("10");
        pnd_State_Table_Pnd_State_15.setInitialValue("20");
        pnd_State_Table_Pnd_State_16.setInitialValue("  ");
        pnd_State_Table_Pnd_State_17.setInitialValue("  ");
        pnd_State_Table_Pnd_State_18.setInitialValue("10");
        pnd_State_Table_Pnd_State_19.setInitialValue("  ");
        pnd_State_Table_Pnd_State_20.setInitialValue("  ");
        pnd_State_Table_Pnd_State_21.setInitialValue("17");
        pnd_State_Table_Pnd_State_22.setInitialValue("  ");
        pnd_State_Table_Pnd_State_23.setInitialValue("  ");
        pnd_State_Table_Pnd_State_24.setInitialValue("20");
        pnd_State_Table_Pnd_State_25.setInitialValue("10");
        pnd_State_Table_Pnd_State_26.setInitialValue("  ");
        pnd_State_Table_Pnd_State_27.setInitialValue("  ");
        pnd_State_Table_Pnd_State_28.setInitialValue("10");
        pnd_State_Table_Pnd_State_29.setInitialValue("  ");
        pnd_State_Table_Pnd_State_30.setInitialValue("10");
        pnd_State_Table_Pnd_State_31.setInitialValue("  ");
        pnd_State_Table_Pnd_State_32.setInitialValue("  ");
        pnd_State_Table_Pnd_State_33.setInitialValue("  ");
        pnd_State_Table_Pnd_State_34.setInitialValue("  ");
        pnd_State_Table_Pnd_State_35.setInitialValue("  ");
        pnd_State_Table_Pnd_State_36.setInitialValue("10");
        pnd_State_Table_Pnd_State_37.setInitialValue("  ");
        pnd_State_Table_Pnd_State_38.setInitialValue("  ");
        pnd_State_Table_Pnd_State_39.setInitialValue("10");
        pnd_State_Table_Pnd_State_40.setInitialValue("  ");
        pnd_State_Table_Pnd_State_41.setInitialValue("  ");
        pnd_State_Table_Pnd_State_42.setInitialValue("  ");
        pnd_State_Table_Pnd_State_43.setInitialValue("10");
        pnd_State_Table_Pnd_State_44.setInitialValue("  ");
        pnd_State_Table_Pnd_State_45.setInitialValue("31");
        pnd_State_Table_Pnd_State_46.setInitialValue("  ");
        pnd_State_Table_Pnd_State_47.setInitialValue("  ");
        pnd_State_Table_Pnd_State_48.setInitialValue("  ");
        pnd_State_Table_Pnd_State_49.setInitialValue("10");
        pnd_State_Table_Pnd_State_50.setInitialValue("  ");
        pnd_State_Table_Pnd_State_51.setInitialValue("  ");
        pnd_State_Table_Pnd_State_52.setInitialValue("  ");
        pnd_State_Table_Pnd_State_53.setInitialValue("  ");
        pnd_State_Table_Pnd_State_54.setInitialValue("10");
        pnd_State_Table_Pnd_State_55.setInitialValue("10");
        pnd_State_Table_Pnd_State_56.setInitialValue("20");
        pnd_State_Table_Pnd_State_57.setInitialValue("  ");
        pnd_State_Table_Pnd_State_58.setInitialValue("  ");
        pnd_State_Table_Pnd_State_59.setInitialValue("  ");
        pnd_State_Table_Pnd_State_60.setInitialValue("  ");
    }

    // Constructor
    public LdaIaal840() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
