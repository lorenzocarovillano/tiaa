/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:49 PM
**        * FROM NATURAL LDA     : IAAL300
************************************************************
**        * FILE NAME            : LdaIaal300.java
**        * CLASS NAME           : LdaIaal300
**        * INSTANCE NAME        : LdaIaal300
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal300 extends DbsRecord
{
    // Properties
    private DbsField pnd_Count_Fund_Record_Reads;
    private DbsField pnd_Count_Master_File_Reads;
    private DbsField pnd_Count_Master_Record_Error;
    private DbsField pnd_Count_Fund_Extract;
    private DbsField pnd_Count_Fund_Extract_2;
    private DbsField pnd_Count;
    private DbsField pnd_Count2;
    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Record_Code;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cmpny_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Fund_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rtb_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Old_Per_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Old_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Per_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Units_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte;
    private DbsGroup pnd_Work_Record_2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Record_Code;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cmpny_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Fund_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Old_Per_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Old_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Units_Cnt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Filler;
    private DbsGroup pnd_Work_Record_2_Pnd_W2_FillerRedef1;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Filler2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Filler3;
    private DbsField pnd_Work_Record_2_Pnd_W2_Filler4;
    private DbsGroup pnd_Work_Record_Header;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Record_Code;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Check_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Process_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_1;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_2;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_3;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_4;
    private DbsGroup pnd_Work_Record_Trailer;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Record_Code;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Check_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Process_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_1;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_2;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_3;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_4;
    private DbsField pnd_Per_Tot;
    private DbsField pnd_I;
    private DbsField pnd_All_Ok;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd_View;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef2;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key;
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_View;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key;
    private DbsField pnd_Check_Date_Ccyymmdd;
    private DbsGroup pnd_Check_Date_CcyymmddRedef3;
    private DbsField pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N;

    public DbsField getPnd_Count_Fund_Record_Reads() { return pnd_Count_Fund_Record_Reads; }

    public DbsField getPnd_Count_Master_File_Reads() { return pnd_Count_Master_File_Reads; }

    public DbsField getPnd_Count_Master_Record_Error() { return pnd_Count_Master_Record_Error; }

    public DbsField getPnd_Count_Fund_Extract() { return pnd_Count_Fund_Extract; }

    public DbsField getPnd_Count_Fund_Extract_2() { return pnd_Count_Fund_Extract_2; }

    public DbsField getPnd_Count() { return pnd_Count; }

    public DbsField getPnd_Count2() { return pnd_Count2; }

    public DbsGroup getPnd_Work_Record_1() { return pnd_Work_Record_1; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Record_Code() { return pnd_Work_Record_1_Pnd_W1_Record_Code; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cmpny_Cde() { return pnd_Work_Record_1_Pnd_W1_Cmpny_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Fund_Cde() { return pnd_Work_Record_1_Pnd_W1_Fund_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt() { return pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Rtb_Amt() { return pnd_Work_Record_1_Pnd_W1_Rtb_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Tot_Per_Amt() { return pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Tot_Div_Amt() { return pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Old_Per_Amt() { return pnd_Work_Record_1_Pnd_W1_Old_Per_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Old_Div_Amt() { return pnd_Work_Record_1_Pnd_W1_Old_Div_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Rate_Cde() { return pnd_Work_Record_1_Pnd_W1_Rate_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Rate_Dte() { return pnd_Work_Record_1_Pnd_W1_Rate_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Per_Pay_Amt() { return pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Per_Div_Amt() { return pnd_Work_Record_1_Pnd_W1_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Units_Cnt() { return pnd_Work_Record_1_Pnd_W1_Units_Cnt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt() { return pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt() { return pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte() { return pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte; }

    public DbsGroup getPnd_Work_Record_2() { return pnd_Work_Record_2; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde() { return pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Record_Code() { return pnd_Work_Record_2_Pnd_W2_Record_Code; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cmpny_Cde() { return pnd_Work_Record_2_Pnd_W2_Cmpny_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Fund_Cde() { return pnd_Work_Record_2_Pnd_W2_Fund_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt() { return pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rtb_Amt() { return pnd_Work_Record_2_Pnd_W2_Rtb_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Tot_Per_Amt() { return pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Tot_Div_Amt() { return pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Old_Per_Amt() { return pnd_Work_Record_2_Pnd_W2_Old_Per_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Old_Div_Amt() { return pnd_Work_Record_2_Pnd_W2_Old_Div_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rate_Cde() { return pnd_Work_Record_2_Pnd_W2_Rate_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rate_Dte() { return pnd_Work_Record_2_Pnd_W2_Rate_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Per_Pay_Amt() { return pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Per_Div_Amt() { return pnd_Work_Record_2_Pnd_W2_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Units_Cnt() { return pnd_Work_Record_2_Pnd_W2_Units_Cnt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt() { return pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt() { return pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte() { return pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Filler() { return pnd_Work_Record_2_Pnd_W2_Filler; }

    public DbsGroup getPnd_Work_Record_2_Pnd_W2_FillerRedef1() { return pnd_Work_Record_2_Pnd_W2_FillerRedef1; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte() { return pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Filler2() { return pnd_Work_Record_2_Pnd_W2_Filler2; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Filler3() { return pnd_Work_Record_2_Pnd_W2_Filler3; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Filler4() { return pnd_Work_Record_2_Pnd_W2_Filler4; }

    public DbsGroup getPnd_Work_Record_Header() { return pnd_Work_Record_Header; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde() { return pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Record_Code() { return pnd_Work_Record_Header_Pnd_Wh_Record_Code; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Check_Date() { return pnd_Work_Record_Header_Pnd_Wh_Check_Date; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Process_Date() { return pnd_Work_Record_Header_Pnd_Wh_Process_Date; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_1() { return pnd_Work_Record_Header_Pnd_Wh_Filler_1; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_2() { return pnd_Work_Record_Header_Pnd_Wh_Filler_2; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_3() { return pnd_Work_Record_Header_Pnd_Wh_Filler_3; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_4() { return pnd_Work_Record_Header_Pnd_Wh_Filler_4; }

    public DbsGroup getPnd_Work_Record_Trailer() { return pnd_Work_Record_Trailer; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde() { return pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Record_Code() { return pnd_Work_Record_Trailer_Pnd_Wt_Record_Code; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Check_Date() { return pnd_Work_Record_Trailer_Pnd_Wt_Check_Date; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Process_Date() { return pnd_Work_Record_Trailer_Pnd_Wt_Process_Date; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records() { return pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_1() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_1; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_2() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_2; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_3() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_3; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_4() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_4; }

    public DbsField getPnd_Per_Tot() { return pnd_Per_Tot; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_All_Ok() { return pnd_All_Ok; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd_View() { return vw_iaa_Tiaa_Fund_Rcrd_View; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef2() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef2; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key; }

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd_View() { return vw_iaa_Cntrl_Rcrd_View; }

    public DbsField getIaa_Cntrl_Rcrd_View_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key() { return iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key; }

    public DbsField getPnd_Check_Date_Ccyymmdd() { return pnd_Check_Date_Ccyymmdd; }

    public DbsGroup getPnd_Check_Date_CcyymmddRedef3() { return pnd_Check_Date_CcyymmddRedef3; }

    public DbsField getPnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N() { return pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Count_Fund_Record_Reads = newFieldInRecord("pnd_Count_Fund_Record_Reads", "#COUNT-FUND-RECORD-READS", FieldType.NUMERIC, 8);

        pnd_Count_Master_File_Reads = newFieldInRecord("pnd_Count_Master_File_Reads", "#COUNT-MASTER-FILE-READS", FieldType.NUMERIC, 8);

        pnd_Count_Master_Record_Error = newFieldInRecord("pnd_Count_Master_Record_Error", "#COUNT-MASTER-RECORD-ERROR", FieldType.NUMERIC, 8);

        pnd_Count_Fund_Extract = newFieldInRecord("pnd_Count_Fund_Extract", "#COUNT-FUND-EXTRACT", FieldType.NUMERIC, 8);

        pnd_Count_Fund_Extract_2 = newFieldInRecord("pnd_Count_Fund_Extract_2", "#COUNT-FUND-EXTRACT-2", FieldType.NUMERIC, 8);

        pnd_Count = newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 8);

        pnd_Count2 = newFieldInRecord("pnd_Count2", "#COUNT2", FieldType.NUMERIC, 8);

        pnd_Work_Record_1 = newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr", "#W1-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde", "#W1-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Record_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Record_Code", "#W1-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Cmpny_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cmpny_Cde", "#W1-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Fund_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Fund_Cde", "#W1-FUND-CDE", FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt", "#W1-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_1_Pnd_W1_Rtb_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Rtb_Amt", "#W1-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt", "#W1-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt", "#W1-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_1_Pnd_W1_Old_Per_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Old_Per_Amt", "#W1-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_1_Pnd_W1_Old_Div_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Old_Div_Amt", "#W1-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_1_Pnd_W1_Rate_Cde = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Cde", "#W1-RATE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,250));
        pnd_Work_Record_1_Pnd_W1_Rate_Dte = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Dte", "#W1-RATE-DTE", FieldType.DATE, 
            new DbsArrayController(1,250));
        pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt", "#W1-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,250));
        pnd_Work_Record_1_Pnd_W1_Per_Div_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Per_Div_Amt", "#W1-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,250));
        pnd_Work_Record_1_Pnd_W1_Units_Cnt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Units_Cnt", "#W1-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9,3, new DbsArrayController(1,250));
        pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt", "#W1-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,250));
        pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt", "#W1-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,250));
        pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte", "#W1-LST-TRANS-DTE", FieldType.TIME);

        pnd_Work_Record_2 = newGroupInRecord("pnd_Work_Record_2", "#WORK-RECORD-2");
        pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr", "#W2-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde", "#W2-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_2_Pnd_W2_Record_Code = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Record_Code", "#W2-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_2_Pnd_W2_Cmpny_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cmpny_Cde", "#W2-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Fund_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Fund_Cde", "#W2-FUND-CDE", FieldType.STRING, 2);
        pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt", "#W2-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Rtb_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Amt", "#W2-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt", "#W2-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt", "#W2-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Old_Per_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Old_Per_Amt", "#W2-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Old_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Old_Div_Amt", "#W2-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Rate_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Cde", "#W2-RATE-CDE", FieldType.STRING, 2);
        pnd_Work_Record_2_Pnd_W2_Rate_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Dte", "#W2-RATE-DTE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt", "#W2-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Per_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Per_Div_Amt", "#W2-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Units_Cnt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Units_Cnt", "#W2-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt", "#W2-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt", "#W2-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte", "#W2-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_2_Pnd_W2_Filler = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Filler", "#W2-FILLER", FieldType.STRING, 231);
        pnd_Work_Record_2_Pnd_W2_FillerRedef1 = pnd_Work_Record_2.newGroupInGroup("pnd_Work_Record_2_Pnd_W2_FillerRedef1", "Redefines", pnd_Work_Record_2_Pnd_W2_Filler);
        pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte = pnd_Work_Record_2_Pnd_W2_FillerRedef1.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte", 
            "#W2-TIAA-LST-XFR-IN-DTE", FieldType.STRING, 8);
        pnd_Work_Record_2_Pnd_W2_Filler2 = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Filler2", "#W2-FILLER2", FieldType.STRING, 31);
        pnd_Work_Record_2_Pnd_W2_Filler3 = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Filler3", "#W2-FILLER3", FieldType.STRING, 15);
        pnd_Work_Record_2_Pnd_W2_Filler4 = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Filler4", "#W2-FILLER4", FieldType.STRING, 5);

        pnd_Work_Record_Header = newGroupInRecord("pnd_Work_Record_Header", "#WORK-RECORD-HEADER");
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr", "#WH-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde", "#WH-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Record_Code = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Record_Code", "#WH-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Check_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Check_Date", "#WH-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Process_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Process_Date", "#WH-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Filler_1 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_1", "#WH-FILLER-1", FieldType.STRING, 
            225);
        pnd_Work_Record_Header_Pnd_Wh_Filler_2 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_2", "#WH-FILLER-2", FieldType.STRING, 
            91);
        pnd_Work_Record_Header_Pnd_Wh_Filler_3 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_3", "#WH-FILLER-3", FieldType.STRING, 
            16);
        pnd_Work_Record_Header_Pnd_Wh_Filler_4 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_4", "#WH-FILLER-4", FieldType.STRING, 
            5);

        pnd_Work_Record_Trailer = newGroupInRecord("pnd_Work_Record_Trailer", "#WORK-RECORD-TRAILER");
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr", "#WT-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde", "#WT-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Record_Code = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Record_Code", "#WT-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Check_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Check_Date", "#WT-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Process_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Process_Date", "#WT-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records", "#WT-TOT-FUND-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_1 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_1", "#WT-FILLER-1", FieldType.STRING, 
            193);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_2 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_2", "#WT-FILLER-2", FieldType.STRING, 
            115);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_3 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_3", "#WT-FILLER-3", FieldType.STRING, 
            16);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_4 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_4", "#WT-FILLER-4", FieldType.STRING, 
            5);

        pnd_Per_Tot = newFieldInRecord("pnd_Per_Tot", "#PER-TOT", FieldType.NUMERIC, 3);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);

        pnd_All_Ok = newFieldInRecord("pnd_All_Ok", "#ALL-OK", FieldType.STRING, 1);

        vw_iaa_Tiaa_Fund_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd_View", "IAA-TIAA-FUND-RCRD-VIEW"), "IAA_TIAA_FUND_RCRD", 
            "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr", 
            "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde", 
            "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde", 
            "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef2 = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef2", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde", 
            "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde", 
            "TIAA-FUND-CDE", FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt", 
            "TIAA-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt", 
            "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt", 
            "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt", 
            "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt", 
            "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp", 
            "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde", 
            "TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte", 
            "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt", 
            "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt", 
            "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt", 
            "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte", 
            "TIAA-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key", 
            "TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");

        vw_iaa_Cntrl_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_View", "IAA-CNTRL-RCRD-VIEW"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key", "CNTRL-RCRD-KEY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRL_RCRD_KEY");

        pnd_Check_Date_Ccyymmdd = newFieldInRecord("pnd_Check_Date_Ccyymmdd", "#CHECK-DATE-CCYYMMDD", FieldType.STRING, 8);
        pnd_Check_Date_CcyymmddRedef3 = newGroupInRecord("pnd_Check_Date_CcyymmddRedef3", "Redefines", pnd_Check_Date_Ccyymmdd);
        pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N = pnd_Check_Date_CcyymmddRedef3.newFieldInGroup("pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N", 
            "#CHECK-DATE-CCYYMMDD-N", FieldType.NUMERIC, 8);
        vw_iaa_Tiaa_Fund_Rcrd_View.setUniquePeList();

        this.setRecordName("LdaIaal300");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Tiaa_Fund_Rcrd_View.reset();
        vw_iaa_Cntrl_Rcrd_View.reset();
    }

    // Constructor
    public LdaIaal300() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
