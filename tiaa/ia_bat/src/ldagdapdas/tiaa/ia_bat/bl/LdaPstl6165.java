/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:30 PM
**        * FROM NATURAL LDA     : PSTL6165
************************************************************
**        * FILE NAME            : LdaPstl6165.java
**        * CLASS NAME           : LdaPstl6165
**        * INSTANCE NAME        : LdaPstl6165
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl6165 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key_Struct;
    private DbsField sort_Key_Struct_Sort_Key_Lgth;
    private DbsField sort_Key_Struct_Sort_Key_Data;
    private DbsGroup sort_Key_Struct_Sort_Key_DataRedef1;
    private DbsGroup sort_Key_Struct_Sort_Key;
    private DbsGroup sort_Key_Struct_Level_1_Sort_Flds;
    private DbsField sort_Key_Struct_Rsk_L1_Value;
    private DbsField sort_Key_Struct_Id_L1_Record;
    private DbsGroup sort_Key_Struct_Level_2_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L2_Record;
    private DbsField sort_Key_Struct_Key_L2_Post_Req_Id;
    private DbsGroup sort_Key_Struct_Level_3_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L3_Record;
    private DbsField sort_Key_Struct_Key_L3_Req_Date_Time;
    private DbsField sort_Key_Struct_Key_L3_From_Cont_Number;
    private DbsGroup sort_Key_Struct_Level_3_Sort_FldsRedef2;
    private DbsField sort_Key_Struct_Level_3_Sort_Flds_Reset;
    private DbsGroup sort_Key_Struct_Level_4_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L4_Record;
    private DbsField sort_Key_Struct_Key_L4_Account_Type;
    private DbsField sort_Key_Struct_Key_L4_Fyi_Mes_Num;
    private DbsField sort_Key_Struct_Key_L4_Quest_Mes_Num;
    private DbsGroup sort_Key_Struct_Level_4_Sort_FldsRedef3;
    private DbsField sort_Key_Struct_Level_4_Sort_Flds_Reset;
    private DbsGroup cn_Contract_Struct;
    private DbsField cn_Contract_Struct_Cn_Record_Id;
    private DbsField cn_Contract_Struct_Cn_Data_Lgth;
    private DbsField cn_Contract_Struct_Cn_Data_Occurs;
    private DbsField cn_Contract_Struct_Cn_Data;
    private DbsGroup cn_Contract_Struct_Cn_DataRedef4;
    private DbsGroup cn_Contract_Struct_Cn_Contract;
    private DbsGroup cn_Contract_Struct_Cn_Contract_Numbers;
    private DbsGroup cn_Contract_Struct_Cn_Tiaa_Contract_Num;
    private DbsField cn_Contract_Struct_Cn_Tiaa_Cont_Num;
    private DbsField cn_Contract_Struct_Cn_Tiaa_Frmto_Ind;
    private DbsField cn_Contract_Struct_Cn_Tiaa_Grdstd_Ind;
    private DbsField cn_Contract_Struct_Cn_Tiaa_Cont_Tbd;
    private DbsGroup cn_Contract_Struct_Cn_Cref_Contract_Num;
    private DbsField cn_Contract_Struct_Cn_Cref_Cont_Num;
    private DbsField cn_Contract_Struct_Cn_Cref_Frmto_Ind;
    private DbsField cn_Contract_Struct_Cn_Cref_Cont_Tbd;
    private DbsGroup cn_Contract_Struct_Cn_Real_Estate_Contract;
    private DbsField cn_Contract_Struct_Cn_Real_Estate_Cont;
    private DbsField cn_Contract_Struct_Cn_Real_Frmto_Ind;
    private DbsField cn_Contract_Struct_Cn_Real_Estate_Tbd;
    private DbsField cn_Contract_Struct_Cn_Req_Source;
    private DbsField cn_Contract_Struct_Cn_Service_Rep;
    private DbsField cn_Contract_Struct_Cn_Switch_Ind;
    private DbsField cn_Contract_Struct_Cn_Effective_Date_F;
    private DbsGroup cn_Contract_Struct_Cn_Effective_Date_FRedef5;
    private DbsField cn_Contract_Struct_Cn_Effective_Mm;
    private DbsField cn_Contract_Struct_Cn_Effective_Dd;
    private DbsField cn_Contract_Struct_Cn_Effective_Yy;
    private DbsField cn_Contract_Struct_Cn_From_Tran_Type;
    private DbsField cn_Contract_Struct_Cn_To_Tran_Type;
    private DbsField cn_Contract_Struct_Cn_Mod_Date_Time_F;
    private DbsGroup cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6;
    private DbsField cn_Contract_Struct_Cn_Mod_Mm;
    private DbsField cn_Contract_Struct_Cn_Mod_Dd;
    private DbsField cn_Contract_Struct_Cn_Mod_Yy;
    private DbsField cn_Contract_Struct_Cn_Mod_Hh;
    private DbsField cn_Contract_Struct_Cn_Mod_Min;
    private DbsField cn_Contract_Struct_Cn_Mod_Sec;
    private DbsField cn_Contract_Struct_Cn_Mod_Ampm;
    private DbsField cn_Contract_Struct_Cn_Mach_Func_1;
    private DbsField cn_Contract_Struct_Cn_Image_Ind;
    private DbsField cn_Contract_Struct_Cn_Processing_Date;
    private DbsGroup cn_Frm_Rec_Struct;
    private DbsField cn_Frm_Rec_Struct_Cn_Record_Id;
    private DbsField cn_Frm_Rec_Struct_Cn_Data_Lgth;
    private DbsField cn_Frm_Rec_Struct_Cn_Data_Occurs;
    private DbsField cn_Frm_Rec_Struct_Cn_Data;
    private DbsGroup cn_Frm_Rec_Struct_Cn_DataRedef7;
    private DbsGroup cn_Frm_Rec_Struct_Cn_Frm_Rec;
    private DbsField cn_Frm_Rec_Struct_Cn_Frm_Account_Type;
    private DbsField cn_Frm_Rec_Struct_Cn_Frm_Myi;
    private DbsField cn_Frm_Rec_Struct_Cn_Frm_Trn_Typ;
    private DbsField cn_Frm_Rec_Struct_Cn_Frm_Percent;
    private DbsField cn_Frm_Rec_Struct_Cn_Frm_Units;
    private DbsField cn_Frm_Rec_Struct_Cn_Frm_Dollars;
    private DbsGroup cn_To_Acct_Struct;
    private DbsField cn_To_Acct_Struct_Cn_Record_Id;
    private DbsField cn_To_Acct_Struct_Cn_Data_Lgth;
    private DbsField cn_To_Acct_Struct_Cn_Data_Occurs;
    private DbsField cn_To_Acct_Struct_Cn_Data;
    private DbsGroup cn_To_Acct_Struct_Cn_DataRedef8;
    private DbsGroup cn_To_Acct_Struct_Cn_To_Acct;
    private DbsField cn_To_Acct_Struct_Cn_To_Account_Type;
    private DbsField cn_To_Acct_Struct_Cn_To_Myi;
    private DbsField cn_To_Acct_Struct_Cn_To_Trn_Typ;
    private DbsField cn_To_Acct_Struct_Cn_To_Percent;
    private DbsField cn_To_Acct_Struct_Cn_To_Units;
    private DbsField cn_To_Acct_Struct_Cn_To_Dollars;
    private DbsGroup cn_Etf_Struct;
    private DbsField cn_Etf_Struct_Cn_Record_Id;
    private DbsField cn_Etf_Struct_Cn_Data_Lgth;
    private DbsField cn_Etf_Struct_Cn_Data_Occurs;
    private DbsField cn_Etf_Struct_Cn_Data;
    private DbsGroup cn_Etf_Struct_Cn_DataRedef9;
    private DbsGroup cn_Etf_Struct_Cn_Etf;
    private DbsField cn_Etf_Struct_Cn_Etf_Myi;
    private DbsField cn_Etf_Struct_Cn_Etf_Vtb;
    private DbsField cn_Etf_Struct_Cn_Etf_Datvp1f;
    private DbsGroup cn_Etf_Struct_Cn_Etf_Datvp1fRedef10;
    private DbsField cn_Etf_Struct_Cn_Etf_Dd_P1v;
    private DbsField cn_Etf_Struct_Cn_Etf_Mm_P1v;
    private DbsField cn_Etf_Struct_Cn_Etf_Yy_P1v;
    private DbsField cn_Etf_Struct_Cn_Etf_Var_P1v;
    private DbsField cn_Etf_Struct_Cn_Etf_Datvp2f;
    private DbsGroup cn_Etf_Struct_Cn_Etf_Datvp2fRedef11;
    private DbsField cn_Etf_Struct_Cn_Etf_Dd_P2v;
    private DbsField cn_Etf_Struct_Cn_Etf_Mm_P2v;
    private DbsField cn_Etf_Struct_Cn_Etf_Yy_P2v;
    private DbsField cn_Etf_Struct_Cn_Etf_Var_P2v;
    private DbsField cn_Etf_Struct_Cn_Etf_Datvp3f;
    private DbsGroup cn_Etf_Struct_Cn_Etf_Datvp3fRedef12;
    private DbsField cn_Etf_Struct_Cn_Etf_Dd_P3v;
    private DbsField cn_Etf_Struct_Cn_Etf_Mm_P3v;
    private DbsField cn_Etf_Struct_Cn_Etf_Yy_P3v;
    private DbsField cn_Etf_Struct_Cn_Etf_Var_P3v;
    private DbsField cn_Etf_Struct_Cn_Etf_Payhddt;
    private DbsGroup cn_Etf_Struct_Cn_Etf_PayhddtRedef13;
    private DbsField cn_Etf_Struct_Cn_Etf_Dd_Phd;
    private DbsField cn_Etf_Struct_Cn_Etf_Mm_Phd;
    private DbsField cn_Etf_Struct_Cn_Etf_Yy_Phd;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Grded_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Mma_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Social_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Stock_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Bond_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Growth_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Reale_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Eqi_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Infb_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Global_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Mutual_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Std_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Mma_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Social_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Stock_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Bond_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Growth_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Reale_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Eqi_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Infb_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Global_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Mutual_U;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Std_Con;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Std_Div;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Grd_Con;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Grd_Div;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Access_P;
    private DbsField cn_Etf_Struct_Cn_Etf_Accnt_Access_U;
    private DbsGroup cn_Ett_Struct;
    private DbsField cn_Ett_Struct_Cn_Record_Id;
    private DbsField cn_Ett_Struct_Cn_Data_Lgth;
    private DbsField cn_Ett_Struct_Cn_Data_Occurs;
    private DbsField cn_Ett_Struct_Cn_Data;
    private DbsGroup cn_Ett_Struct_Cn_DataRedef14;
    private DbsGroup cn_Ett_Struct_Cn_Ett;
    private DbsField cn_Ett_Struct_Cn_Ett_Myi;
    private DbsField cn_Ett_Struct_Cn_Ett_Vtb;
    private DbsField cn_Ett_Struct_Cn_Ett_Datvp1f;
    private DbsGroup cn_Ett_Struct_Cn_Ett_Datvp1fRedef15;
    private DbsField cn_Ett_Struct_Cn_Ett_Dd_P1v;
    private DbsField cn_Ett_Struct_Cn_Ett_Mm_P1v;
    private DbsField cn_Ett_Struct_Cn_Ett_Yy_P1v;
    private DbsField cn_Ett_Struct_Cn_Ett_Var_P1v;
    private DbsField cn_Ett_Struct_Cn_Ett_Datvp2f;
    private DbsGroup cn_Ett_Struct_Cn_Ett_Datvp2fRedef16;
    private DbsField cn_Ett_Struct_Cn_Ett_Dd_P2v;
    private DbsField cn_Ett_Struct_Cn_Ett_Mm_P2v;
    private DbsField cn_Ett_Struct_Cn_Ett_Yy_P2v;
    private DbsField cn_Ett_Struct_Cn_Ett_Var_P2v;
    private DbsField cn_Ett_Struct_Cn_Ett_Datvp3f;
    private DbsGroup cn_Ett_Struct_Cn_Ett_Datvp3fRedef17;
    private DbsField cn_Ett_Struct_Cn_Ett_Dd_P3v;
    private DbsField cn_Ett_Struct_Cn_Ett_Mm_P3v;
    private DbsField cn_Ett_Struct_Cn_Ett_Yy_P3v;
    private DbsField cn_Ett_Struct_Cn_Ett_Var_P3v;
    private DbsField cn_Ett_Struct_Cn_Ett_Payhddt;
    private DbsGroup cn_Ett_Struct_Cn_Ett_PayhddtRedef18;
    private DbsField cn_Ett_Struct_Cn_Ett_Dd_Phd;
    private DbsField cn_Ett_Struct_Cn_Ett_Mm_Phd;
    private DbsField cn_Ett_Struct_Cn_Ett_Yy_Phd;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Grded_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Mma_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Social_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Stock_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Bond_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Growth_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Reale_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Eqi_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Infb_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Global_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Mutual_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Std_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Mma_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Social_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Stock_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Bond_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Growth_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Reale_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Eqi_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Infb_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Global_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Mutual_U;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Std_Con;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Std_Div;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Grd_Con;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Grd_Div;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Access_P;
    private DbsField cn_Ett_Struct_Cn_Ett_Accnt_Access_U;
    private DbsGroup cn_Anv_Struct;
    private DbsField cn_Anv_Struct_Cn_Record_Id;
    private DbsField cn_Anv_Struct_Cn_Data_Lgth;
    private DbsField cn_Anv_Struct_Cn_Data_Occurs;
    private DbsField cn_Anv_Struct_Cn_Data;
    private DbsGroup cn_Anv_Struct_Cn_DataRedef19;
    private DbsGroup cn_Anv_Struct_Cn_Anv;
    private DbsField cn_Anv_Struct_Cn_Anv_Myi;
    private DbsField cn_Anv_Struct_Cn_Anv_Datvp1f;
    private DbsGroup cn_Anv_Struct_Cn_Anv_Datvp1fRedef20;
    private DbsField cn_Anv_Struct_Cn_Anv_Dd_P1v;
    private DbsField cn_Anv_Struct_Cn_Anv_Mm_P1v;
    private DbsField cn_Anv_Struct_Cn_Anv_Yy_P1v;
    private DbsField cn_Anv_Struct_Cn_Anv_Var_P1v;
    private DbsField cn_Anv_Struct_Cn_Anv_Datvp2f;
    private DbsGroup cn_Anv_Struct_Cn_Anv_Datvp2fRedef21;
    private DbsField cn_Anv_Struct_Cn_Anv_Dd_P2v;
    private DbsField cn_Anv_Struct_Cn_Anv_Mm_P2v;
    private DbsField cn_Anv_Struct_Cn_Anv_Yy_P2v;
    private DbsField cn_Anv_Struct_Cn_Anv_Var_P2v;
    private DbsField cn_Anv_Struct_Cn_Anv_Datvp3f;
    private DbsGroup cn_Anv_Struct_Cn_Anv_Datvp3fRedef22;
    private DbsField cn_Anv_Struct_Cn_Anv_Dd_P3v;
    private DbsField cn_Anv_Struct_Cn_Anv_Mm_P3v;
    private DbsField cn_Anv_Struct_Cn_Anv_Yy_P3v;
    private DbsField cn_Anv_Struct_Cn_Anv_Var_P3v;
    private DbsField cn_Anv_Struct_Cn_Anv_Payhddt;
    private DbsGroup cn_Anv_Struct_Cn_Anv_PayhddtRedef23;
    private DbsField cn_Anv_Struct_Cn_Anv_Dd_Phd;
    private DbsField cn_Anv_Struct_Cn_Anv_Mm_Phd;
    private DbsField cn_Anv_Struct_Cn_Anv_Yy_Phd;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Mma_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Social_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Stock_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Bond_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Growth_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Reale_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Eqi_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Infb_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Global_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Mutual_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Mma_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Social_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Stock_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Bond_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Growth_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Reale_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Eqi_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Infb_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Global_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Mutual_A;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Access_M;
    private DbsField cn_Anv_Struct_Cn_Anv_Accnt_Access_A;
    private DbsGroup cn_Fyi_Struct;
    private DbsField cn_Fyi_Struct_Cn_Record_Id;
    private DbsField cn_Fyi_Struct_Cn_Data_Lgth;
    private DbsField cn_Fyi_Struct_Cn_Data_Occurs;
    private DbsField cn_Fyi_Struct_Cn_Data;
    private DbsGroup cn_Fyi_Struct_Cn_DataRedef24;
    private DbsGroup cn_Fyi_Struct_Cn_Fyi;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Msg_Number;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date1_F;
    private DbsGroup cn_Fyi_Struct_Cn_Fyi_Date1_FRedef25;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date1_Mm;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date1_Dd;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date1_Yy;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date2_F;
    private DbsGroup cn_Fyi_Struct_Cn_Fyi_Date2_FRedef26;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date2_Mm;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date2_Dd;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date2_Yy;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Vartxt;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Srt_Num;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date3_F;
    private DbsGroup cn_Fyi_Struct_Cn_Fyi_Date3_FRedef27;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date3_Mm;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date3_Dd;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date3_Yy;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date4_F;
    private DbsGroup cn_Fyi_Struct_Cn_Fyi_Date4_FRedef28;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date4_Mm;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date4_Dd;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date4_Yy;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date5_F;
    private DbsGroup cn_Fyi_Struct_Cn_Fyi_Date5_FRedef29;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date5_Mm;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date5_Dd;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date5_Yy;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date6_F;
    private DbsGroup cn_Fyi_Struct_Cn_Fyi_Date6_FRedef30;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date6_Mm;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date6_Dd;
    private DbsField cn_Fyi_Struct_Cn_Fyi_Date6_Yy;

    public DbsGroup getSort_Key_Struct() { return sort_Key_Struct; }

    public DbsField getSort_Key_Struct_Sort_Key_Lgth() { return sort_Key_Struct_Sort_Key_Lgth; }

    public DbsField getSort_Key_Struct_Sort_Key_Data() { return sort_Key_Struct_Sort_Key_Data; }

    public DbsGroup getSort_Key_Struct_Sort_Key_DataRedef1() { return sort_Key_Struct_Sort_Key_DataRedef1; }

    public DbsGroup getSort_Key_Struct_Sort_Key() { return sort_Key_Struct_Sort_Key; }

    public DbsGroup getSort_Key_Struct_Level_1_Sort_Flds() { return sort_Key_Struct_Level_1_Sort_Flds; }

    public DbsField getSort_Key_Struct_Rsk_L1_Value() { return sort_Key_Struct_Rsk_L1_Value; }

    public DbsField getSort_Key_Struct_Id_L1_Record() { return sort_Key_Struct_Id_L1_Record; }

    public DbsGroup getSort_Key_Struct_Level_2_Sort_Flds() { return sort_Key_Struct_Level_2_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L2_Record() { return sort_Key_Struct_Id_L2_Record; }

    public DbsField getSort_Key_Struct_Key_L2_Post_Req_Id() { return sort_Key_Struct_Key_L2_Post_Req_Id; }

    public DbsGroup getSort_Key_Struct_Level_3_Sort_Flds() { return sort_Key_Struct_Level_3_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L3_Record() { return sort_Key_Struct_Id_L3_Record; }

    public DbsField getSort_Key_Struct_Key_L3_Req_Date_Time() { return sort_Key_Struct_Key_L3_Req_Date_Time; }

    public DbsField getSort_Key_Struct_Key_L3_From_Cont_Number() { return sort_Key_Struct_Key_L3_From_Cont_Number; }

    public DbsGroup getSort_Key_Struct_Level_3_Sort_FldsRedef2() { return sort_Key_Struct_Level_3_Sort_FldsRedef2; }

    public DbsField getSort_Key_Struct_Level_3_Sort_Flds_Reset() { return sort_Key_Struct_Level_3_Sort_Flds_Reset; }

    public DbsGroup getSort_Key_Struct_Level_4_Sort_Flds() { return sort_Key_Struct_Level_4_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L4_Record() { return sort_Key_Struct_Id_L4_Record; }

    public DbsField getSort_Key_Struct_Key_L4_Account_Type() { return sort_Key_Struct_Key_L4_Account_Type; }

    public DbsField getSort_Key_Struct_Key_L4_Fyi_Mes_Num() { return sort_Key_Struct_Key_L4_Fyi_Mes_Num; }

    public DbsField getSort_Key_Struct_Key_L4_Quest_Mes_Num() { return sort_Key_Struct_Key_L4_Quest_Mes_Num; }

    public DbsGroup getSort_Key_Struct_Level_4_Sort_FldsRedef3() { return sort_Key_Struct_Level_4_Sort_FldsRedef3; }

    public DbsField getSort_Key_Struct_Level_4_Sort_Flds_Reset() { return sort_Key_Struct_Level_4_Sort_Flds_Reset; }

    public DbsGroup getCn_Contract_Struct() { return cn_Contract_Struct; }

    public DbsField getCn_Contract_Struct_Cn_Record_Id() { return cn_Contract_Struct_Cn_Record_Id; }

    public DbsField getCn_Contract_Struct_Cn_Data_Lgth() { return cn_Contract_Struct_Cn_Data_Lgth; }

    public DbsField getCn_Contract_Struct_Cn_Data_Occurs() { return cn_Contract_Struct_Cn_Data_Occurs; }

    public DbsField getCn_Contract_Struct_Cn_Data() { return cn_Contract_Struct_Cn_Data; }

    public DbsGroup getCn_Contract_Struct_Cn_DataRedef4() { return cn_Contract_Struct_Cn_DataRedef4; }

    public DbsGroup getCn_Contract_Struct_Cn_Contract() { return cn_Contract_Struct_Cn_Contract; }

    public DbsGroup getCn_Contract_Struct_Cn_Contract_Numbers() { return cn_Contract_Struct_Cn_Contract_Numbers; }

    public DbsGroup getCn_Contract_Struct_Cn_Tiaa_Contract_Num() { return cn_Contract_Struct_Cn_Tiaa_Contract_Num; }

    public DbsField getCn_Contract_Struct_Cn_Tiaa_Cont_Num() { return cn_Contract_Struct_Cn_Tiaa_Cont_Num; }

    public DbsField getCn_Contract_Struct_Cn_Tiaa_Frmto_Ind() { return cn_Contract_Struct_Cn_Tiaa_Frmto_Ind; }

    public DbsField getCn_Contract_Struct_Cn_Tiaa_Grdstd_Ind() { return cn_Contract_Struct_Cn_Tiaa_Grdstd_Ind; }

    public DbsField getCn_Contract_Struct_Cn_Tiaa_Cont_Tbd() { return cn_Contract_Struct_Cn_Tiaa_Cont_Tbd; }

    public DbsGroup getCn_Contract_Struct_Cn_Cref_Contract_Num() { return cn_Contract_Struct_Cn_Cref_Contract_Num; }

    public DbsField getCn_Contract_Struct_Cn_Cref_Cont_Num() { return cn_Contract_Struct_Cn_Cref_Cont_Num; }

    public DbsField getCn_Contract_Struct_Cn_Cref_Frmto_Ind() { return cn_Contract_Struct_Cn_Cref_Frmto_Ind; }

    public DbsField getCn_Contract_Struct_Cn_Cref_Cont_Tbd() { return cn_Contract_Struct_Cn_Cref_Cont_Tbd; }

    public DbsGroup getCn_Contract_Struct_Cn_Real_Estate_Contract() { return cn_Contract_Struct_Cn_Real_Estate_Contract; }

    public DbsField getCn_Contract_Struct_Cn_Real_Estate_Cont() { return cn_Contract_Struct_Cn_Real_Estate_Cont; }

    public DbsField getCn_Contract_Struct_Cn_Real_Frmto_Ind() { return cn_Contract_Struct_Cn_Real_Frmto_Ind; }

    public DbsField getCn_Contract_Struct_Cn_Real_Estate_Tbd() { return cn_Contract_Struct_Cn_Real_Estate_Tbd; }

    public DbsField getCn_Contract_Struct_Cn_Req_Source() { return cn_Contract_Struct_Cn_Req_Source; }

    public DbsField getCn_Contract_Struct_Cn_Service_Rep() { return cn_Contract_Struct_Cn_Service_Rep; }

    public DbsField getCn_Contract_Struct_Cn_Switch_Ind() { return cn_Contract_Struct_Cn_Switch_Ind; }

    public DbsField getCn_Contract_Struct_Cn_Effective_Date_F() { return cn_Contract_Struct_Cn_Effective_Date_F; }

    public DbsGroup getCn_Contract_Struct_Cn_Effective_Date_FRedef5() { return cn_Contract_Struct_Cn_Effective_Date_FRedef5; }

    public DbsField getCn_Contract_Struct_Cn_Effective_Mm() { return cn_Contract_Struct_Cn_Effective_Mm; }

    public DbsField getCn_Contract_Struct_Cn_Effective_Dd() { return cn_Contract_Struct_Cn_Effective_Dd; }

    public DbsField getCn_Contract_Struct_Cn_Effective_Yy() { return cn_Contract_Struct_Cn_Effective_Yy; }

    public DbsField getCn_Contract_Struct_Cn_From_Tran_Type() { return cn_Contract_Struct_Cn_From_Tran_Type; }

    public DbsField getCn_Contract_Struct_Cn_To_Tran_Type() { return cn_Contract_Struct_Cn_To_Tran_Type; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Date_Time_F() { return cn_Contract_Struct_Cn_Mod_Date_Time_F; }

    public DbsGroup getCn_Contract_Struct_Cn_Mod_Date_Time_FRedef6() { return cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Mm() { return cn_Contract_Struct_Cn_Mod_Mm; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Dd() { return cn_Contract_Struct_Cn_Mod_Dd; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Yy() { return cn_Contract_Struct_Cn_Mod_Yy; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Hh() { return cn_Contract_Struct_Cn_Mod_Hh; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Min() { return cn_Contract_Struct_Cn_Mod_Min; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Sec() { return cn_Contract_Struct_Cn_Mod_Sec; }

    public DbsField getCn_Contract_Struct_Cn_Mod_Ampm() { return cn_Contract_Struct_Cn_Mod_Ampm; }

    public DbsField getCn_Contract_Struct_Cn_Mach_Func_1() { return cn_Contract_Struct_Cn_Mach_Func_1; }

    public DbsField getCn_Contract_Struct_Cn_Image_Ind() { return cn_Contract_Struct_Cn_Image_Ind; }

    public DbsField getCn_Contract_Struct_Cn_Processing_Date() { return cn_Contract_Struct_Cn_Processing_Date; }

    public DbsGroup getCn_Frm_Rec_Struct() { return cn_Frm_Rec_Struct; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Record_Id() { return cn_Frm_Rec_Struct_Cn_Record_Id; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Data_Lgth() { return cn_Frm_Rec_Struct_Cn_Data_Lgth; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Data_Occurs() { return cn_Frm_Rec_Struct_Cn_Data_Occurs; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Data() { return cn_Frm_Rec_Struct_Cn_Data; }

    public DbsGroup getCn_Frm_Rec_Struct_Cn_DataRedef7() { return cn_Frm_Rec_Struct_Cn_DataRedef7; }

    public DbsGroup getCn_Frm_Rec_Struct_Cn_Frm_Rec() { return cn_Frm_Rec_Struct_Cn_Frm_Rec; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Frm_Account_Type() { return cn_Frm_Rec_Struct_Cn_Frm_Account_Type; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Frm_Myi() { return cn_Frm_Rec_Struct_Cn_Frm_Myi; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Frm_Trn_Typ() { return cn_Frm_Rec_Struct_Cn_Frm_Trn_Typ; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Frm_Percent() { return cn_Frm_Rec_Struct_Cn_Frm_Percent; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Frm_Units() { return cn_Frm_Rec_Struct_Cn_Frm_Units; }

    public DbsField getCn_Frm_Rec_Struct_Cn_Frm_Dollars() { return cn_Frm_Rec_Struct_Cn_Frm_Dollars; }

    public DbsGroup getCn_To_Acct_Struct() { return cn_To_Acct_Struct; }

    public DbsField getCn_To_Acct_Struct_Cn_Record_Id() { return cn_To_Acct_Struct_Cn_Record_Id; }

    public DbsField getCn_To_Acct_Struct_Cn_Data_Lgth() { return cn_To_Acct_Struct_Cn_Data_Lgth; }

    public DbsField getCn_To_Acct_Struct_Cn_Data_Occurs() { return cn_To_Acct_Struct_Cn_Data_Occurs; }

    public DbsField getCn_To_Acct_Struct_Cn_Data() { return cn_To_Acct_Struct_Cn_Data; }

    public DbsGroup getCn_To_Acct_Struct_Cn_DataRedef8() { return cn_To_Acct_Struct_Cn_DataRedef8; }

    public DbsGroup getCn_To_Acct_Struct_Cn_To_Acct() { return cn_To_Acct_Struct_Cn_To_Acct; }

    public DbsField getCn_To_Acct_Struct_Cn_To_Account_Type() { return cn_To_Acct_Struct_Cn_To_Account_Type; }

    public DbsField getCn_To_Acct_Struct_Cn_To_Myi() { return cn_To_Acct_Struct_Cn_To_Myi; }

    public DbsField getCn_To_Acct_Struct_Cn_To_Trn_Typ() { return cn_To_Acct_Struct_Cn_To_Trn_Typ; }

    public DbsField getCn_To_Acct_Struct_Cn_To_Percent() { return cn_To_Acct_Struct_Cn_To_Percent; }

    public DbsField getCn_To_Acct_Struct_Cn_To_Units() { return cn_To_Acct_Struct_Cn_To_Units; }

    public DbsField getCn_To_Acct_Struct_Cn_To_Dollars() { return cn_To_Acct_Struct_Cn_To_Dollars; }

    public DbsGroup getCn_Etf_Struct() { return cn_Etf_Struct; }

    public DbsField getCn_Etf_Struct_Cn_Record_Id() { return cn_Etf_Struct_Cn_Record_Id; }

    public DbsField getCn_Etf_Struct_Cn_Data_Lgth() { return cn_Etf_Struct_Cn_Data_Lgth; }

    public DbsField getCn_Etf_Struct_Cn_Data_Occurs() { return cn_Etf_Struct_Cn_Data_Occurs; }

    public DbsField getCn_Etf_Struct_Cn_Data() { return cn_Etf_Struct_Cn_Data; }

    public DbsGroup getCn_Etf_Struct_Cn_DataRedef9() { return cn_Etf_Struct_Cn_DataRedef9; }

    public DbsGroup getCn_Etf_Struct_Cn_Etf() { return cn_Etf_Struct_Cn_Etf; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Myi() { return cn_Etf_Struct_Cn_Etf_Myi; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Vtb() { return cn_Etf_Struct_Cn_Etf_Vtb; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Datvp1f() { return cn_Etf_Struct_Cn_Etf_Datvp1f; }

    public DbsGroup getCn_Etf_Struct_Cn_Etf_Datvp1fRedef10() { return cn_Etf_Struct_Cn_Etf_Datvp1fRedef10; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Dd_P1v() { return cn_Etf_Struct_Cn_Etf_Dd_P1v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Mm_P1v() { return cn_Etf_Struct_Cn_Etf_Mm_P1v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Yy_P1v() { return cn_Etf_Struct_Cn_Etf_Yy_P1v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Var_P1v() { return cn_Etf_Struct_Cn_Etf_Var_P1v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Datvp2f() { return cn_Etf_Struct_Cn_Etf_Datvp2f; }

    public DbsGroup getCn_Etf_Struct_Cn_Etf_Datvp2fRedef11() { return cn_Etf_Struct_Cn_Etf_Datvp2fRedef11; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Dd_P2v() { return cn_Etf_Struct_Cn_Etf_Dd_P2v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Mm_P2v() { return cn_Etf_Struct_Cn_Etf_Mm_P2v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Yy_P2v() { return cn_Etf_Struct_Cn_Etf_Yy_P2v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Var_P2v() { return cn_Etf_Struct_Cn_Etf_Var_P2v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Datvp3f() { return cn_Etf_Struct_Cn_Etf_Datvp3f; }

    public DbsGroup getCn_Etf_Struct_Cn_Etf_Datvp3fRedef12() { return cn_Etf_Struct_Cn_Etf_Datvp3fRedef12; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Dd_P3v() { return cn_Etf_Struct_Cn_Etf_Dd_P3v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Mm_P3v() { return cn_Etf_Struct_Cn_Etf_Mm_P3v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Yy_P3v() { return cn_Etf_Struct_Cn_Etf_Yy_P3v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Var_P3v() { return cn_Etf_Struct_Cn_Etf_Var_P3v; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Payhddt() { return cn_Etf_Struct_Cn_Etf_Payhddt; }

    public DbsGroup getCn_Etf_Struct_Cn_Etf_PayhddtRedef13() { return cn_Etf_Struct_Cn_Etf_PayhddtRedef13; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Dd_Phd() { return cn_Etf_Struct_Cn_Etf_Dd_Phd; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Mm_Phd() { return cn_Etf_Struct_Cn_Etf_Mm_Phd; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Yy_Phd() { return cn_Etf_Struct_Cn_Etf_Yy_Phd; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Grded_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Grded_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Mma_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Mma_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Social_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Social_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Stock_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Stock_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Bond_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Bond_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Growth_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Growth_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Reale_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Reale_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Eqi_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Infb_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Infb_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Global_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Global_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Mutual_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Mutual_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Std_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Std_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Mma_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Mma_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Social_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Social_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Stock_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Stock_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Bond_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Bond_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Growth_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Growth_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Reale_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Reale_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Eqi_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Eqi_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Infb_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Infb_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Global_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Global_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Mutual_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Mutual_U; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Std_Con() { return cn_Etf_Struct_Cn_Etf_Accnt_Std_Con; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Std_Div() { return cn_Etf_Struct_Cn_Etf_Accnt_Std_Div; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Grd_Con() { return cn_Etf_Struct_Cn_Etf_Accnt_Grd_Con; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Grd_Div() { return cn_Etf_Struct_Cn_Etf_Accnt_Grd_Div; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Access_P() { return cn_Etf_Struct_Cn_Etf_Accnt_Access_P; }

    public DbsField getCn_Etf_Struct_Cn_Etf_Accnt_Access_U() { return cn_Etf_Struct_Cn_Etf_Accnt_Access_U; }

    public DbsGroup getCn_Ett_Struct() { return cn_Ett_Struct; }

    public DbsField getCn_Ett_Struct_Cn_Record_Id() { return cn_Ett_Struct_Cn_Record_Id; }

    public DbsField getCn_Ett_Struct_Cn_Data_Lgth() { return cn_Ett_Struct_Cn_Data_Lgth; }

    public DbsField getCn_Ett_Struct_Cn_Data_Occurs() { return cn_Ett_Struct_Cn_Data_Occurs; }

    public DbsField getCn_Ett_Struct_Cn_Data() { return cn_Ett_Struct_Cn_Data; }

    public DbsGroup getCn_Ett_Struct_Cn_DataRedef14() { return cn_Ett_Struct_Cn_DataRedef14; }

    public DbsGroup getCn_Ett_Struct_Cn_Ett() { return cn_Ett_Struct_Cn_Ett; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Myi() { return cn_Ett_Struct_Cn_Ett_Myi; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Vtb() { return cn_Ett_Struct_Cn_Ett_Vtb; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Datvp1f() { return cn_Ett_Struct_Cn_Ett_Datvp1f; }

    public DbsGroup getCn_Ett_Struct_Cn_Ett_Datvp1fRedef15() { return cn_Ett_Struct_Cn_Ett_Datvp1fRedef15; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Dd_P1v() { return cn_Ett_Struct_Cn_Ett_Dd_P1v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Mm_P1v() { return cn_Ett_Struct_Cn_Ett_Mm_P1v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Yy_P1v() { return cn_Ett_Struct_Cn_Ett_Yy_P1v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Var_P1v() { return cn_Ett_Struct_Cn_Ett_Var_P1v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Datvp2f() { return cn_Ett_Struct_Cn_Ett_Datvp2f; }

    public DbsGroup getCn_Ett_Struct_Cn_Ett_Datvp2fRedef16() { return cn_Ett_Struct_Cn_Ett_Datvp2fRedef16; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Dd_P2v() { return cn_Ett_Struct_Cn_Ett_Dd_P2v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Mm_P2v() { return cn_Ett_Struct_Cn_Ett_Mm_P2v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Yy_P2v() { return cn_Ett_Struct_Cn_Ett_Yy_P2v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Var_P2v() { return cn_Ett_Struct_Cn_Ett_Var_P2v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Datvp3f() { return cn_Ett_Struct_Cn_Ett_Datvp3f; }

    public DbsGroup getCn_Ett_Struct_Cn_Ett_Datvp3fRedef17() { return cn_Ett_Struct_Cn_Ett_Datvp3fRedef17; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Dd_P3v() { return cn_Ett_Struct_Cn_Ett_Dd_P3v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Mm_P3v() { return cn_Ett_Struct_Cn_Ett_Mm_P3v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Yy_P3v() { return cn_Ett_Struct_Cn_Ett_Yy_P3v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Var_P3v() { return cn_Ett_Struct_Cn_Ett_Var_P3v; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Payhddt() { return cn_Ett_Struct_Cn_Ett_Payhddt; }

    public DbsGroup getCn_Ett_Struct_Cn_Ett_PayhddtRedef18() { return cn_Ett_Struct_Cn_Ett_PayhddtRedef18; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Dd_Phd() { return cn_Ett_Struct_Cn_Ett_Dd_Phd; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Mm_Phd() { return cn_Ett_Struct_Cn_Ett_Mm_Phd; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Yy_Phd() { return cn_Ett_Struct_Cn_Ett_Yy_Phd; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Grded_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Grded_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Mma_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Mma_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Social_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Social_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Stock_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Stock_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Bond_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Bond_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Growth_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Growth_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Reale_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Reale_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Eqi_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Infb_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Infb_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Global_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Global_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Mutual_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Mutual_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Std_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Std_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Mma_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Mma_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Social_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Social_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Stock_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Stock_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Bond_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Bond_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Growth_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Growth_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Reale_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Reale_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Eqi_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Eqi_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Infb_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Infb_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Global_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Global_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Mutual_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Mutual_U; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Std_Con() { return cn_Ett_Struct_Cn_Ett_Accnt_Std_Con; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Std_Div() { return cn_Ett_Struct_Cn_Ett_Accnt_Std_Div; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Grd_Con() { return cn_Ett_Struct_Cn_Ett_Accnt_Grd_Con; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Grd_Div() { return cn_Ett_Struct_Cn_Ett_Accnt_Grd_Div; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Access_P() { return cn_Ett_Struct_Cn_Ett_Accnt_Access_P; }

    public DbsField getCn_Ett_Struct_Cn_Ett_Accnt_Access_U() { return cn_Ett_Struct_Cn_Ett_Accnt_Access_U; }

    public DbsGroup getCn_Anv_Struct() { return cn_Anv_Struct; }

    public DbsField getCn_Anv_Struct_Cn_Record_Id() { return cn_Anv_Struct_Cn_Record_Id; }

    public DbsField getCn_Anv_Struct_Cn_Data_Lgth() { return cn_Anv_Struct_Cn_Data_Lgth; }

    public DbsField getCn_Anv_Struct_Cn_Data_Occurs() { return cn_Anv_Struct_Cn_Data_Occurs; }

    public DbsField getCn_Anv_Struct_Cn_Data() { return cn_Anv_Struct_Cn_Data; }

    public DbsGroup getCn_Anv_Struct_Cn_DataRedef19() { return cn_Anv_Struct_Cn_DataRedef19; }

    public DbsGroup getCn_Anv_Struct_Cn_Anv() { return cn_Anv_Struct_Cn_Anv; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Myi() { return cn_Anv_Struct_Cn_Anv_Myi; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Datvp1f() { return cn_Anv_Struct_Cn_Anv_Datvp1f; }

    public DbsGroup getCn_Anv_Struct_Cn_Anv_Datvp1fRedef20() { return cn_Anv_Struct_Cn_Anv_Datvp1fRedef20; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Dd_P1v() { return cn_Anv_Struct_Cn_Anv_Dd_P1v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Mm_P1v() { return cn_Anv_Struct_Cn_Anv_Mm_P1v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Yy_P1v() { return cn_Anv_Struct_Cn_Anv_Yy_P1v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Var_P1v() { return cn_Anv_Struct_Cn_Anv_Var_P1v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Datvp2f() { return cn_Anv_Struct_Cn_Anv_Datvp2f; }

    public DbsGroup getCn_Anv_Struct_Cn_Anv_Datvp2fRedef21() { return cn_Anv_Struct_Cn_Anv_Datvp2fRedef21; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Dd_P2v() { return cn_Anv_Struct_Cn_Anv_Dd_P2v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Mm_P2v() { return cn_Anv_Struct_Cn_Anv_Mm_P2v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Yy_P2v() { return cn_Anv_Struct_Cn_Anv_Yy_P2v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Var_P2v() { return cn_Anv_Struct_Cn_Anv_Var_P2v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Datvp3f() { return cn_Anv_Struct_Cn_Anv_Datvp3f; }

    public DbsGroup getCn_Anv_Struct_Cn_Anv_Datvp3fRedef22() { return cn_Anv_Struct_Cn_Anv_Datvp3fRedef22; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Dd_P3v() { return cn_Anv_Struct_Cn_Anv_Dd_P3v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Mm_P3v() { return cn_Anv_Struct_Cn_Anv_Mm_P3v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Yy_P3v() { return cn_Anv_Struct_Cn_Anv_Yy_P3v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Var_P3v() { return cn_Anv_Struct_Cn_Anv_Var_P3v; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Payhddt() { return cn_Anv_Struct_Cn_Anv_Payhddt; }

    public DbsGroup getCn_Anv_Struct_Cn_Anv_PayhddtRedef23() { return cn_Anv_Struct_Cn_Anv_PayhddtRedef23; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Dd_Phd() { return cn_Anv_Struct_Cn_Anv_Dd_Phd; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Mm_Phd() { return cn_Anv_Struct_Cn_Anv_Mm_Phd; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Yy_Phd() { return cn_Anv_Struct_Cn_Anv_Yy_Phd; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Mma_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Mma_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Social_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Social_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Stock_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Stock_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Bond_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Bond_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Growth_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Growth_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Reale_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Reale_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Eqi_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Infb_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Infb_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Global_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Global_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Mutual_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Mutual_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Mma_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Mma_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Social_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Social_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Stock_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Stock_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Bond_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Bond_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Growth_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Growth_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Reale_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Reale_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Eqi_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Eqi_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Infb_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Infb_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Global_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Global_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Mutual_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Mutual_A; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Access_M() { return cn_Anv_Struct_Cn_Anv_Accnt_Access_M; }

    public DbsField getCn_Anv_Struct_Cn_Anv_Accnt_Access_A() { return cn_Anv_Struct_Cn_Anv_Accnt_Access_A; }

    public DbsGroup getCn_Fyi_Struct() { return cn_Fyi_Struct; }

    public DbsField getCn_Fyi_Struct_Cn_Record_Id() { return cn_Fyi_Struct_Cn_Record_Id; }

    public DbsField getCn_Fyi_Struct_Cn_Data_Lgth() { return cn_Fyi_Struct_Cn_Data_Lgth; }

    public DbsField getCn_Fyi_Struct_Cn_Data_Occurs() { return cn_Fyi_Struct_Cn_Data_Occurs; }

    public DbsField getCn_Fyi_Struct_Cn_Data() { return cn_Fyi_Struct_Cn_Data; }

    public DbsGroup getCn_Fyi_Struct_Cn_DataRedef24() { return cn_Fyi_Struct_Cn_DataRedef24; }

    public DbsGroup getCn_Fyi_Struct_Cn_Fyi() { return cn_Fyi_Struct_Cn_Fyi; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Msg_Number() { return cn_Fyi_Struct_Cn_Fyi_Msg_Number; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date1_F() { return cn_Fyi_Struct_Cn_Fyi_Date1_F; }

    public DbsGroup getCn_Fyi_Struct_Cn_Fyi_Date1_FRedef25() { return cn_Fyi_Struct_Cn_Fyi_Date1_FRedef25; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date1_Mm() { return cn_Fyi_Struct_Cn_Fyi_Date1_Mm; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date1_Dd() { return cn_Fyi_Struct_Cn_Fyi_Date1_Dd; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date1_Yy() { return cn_Fyi_Struct_Cn_Fyi_Date1_Yy; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date2_F() { return cn_Fyi_Struct_Cn_Fyi_Date2_F; }

    public DbsGroup getCn_Fyi_Struct_Cn_Fyi_Date2_FRedef26() { return cn_Fyi_Struct_Cn_Fyi_Date2_FRedef26; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date2_Mm() { return cn_Fyi_Struct_Cn_Fyi_Date2_Mm; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date2_Dd() { return cn_Fyi_Struct_Cn_Fyi_Date2_Dd; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date2_Yy() { return cn_Fyi_Struct_Cn_Fyi_Date2_Yy; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Vartxt() { return cn_Fyi_Struct_Cn_Fyi_Vartxt; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Srt_Num() { return cn_Fyi_Struct_Cn_Fyi_Srt_Num; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date3_F() { return cn_Fyi_Struct_Cn_Fyi_Date3_F; }

    public DbsGroup getCn_Fyi_Struct_Cn_Fyi_Date3_FRedef27() { return cn_Fyi_Struct_Cn_Fyi_Date3_FRedef27; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date3_Mm() { return cn_Fyi_Struct_Cn_Fyi_Date3_Mm; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date3_Dd() { return cn_Fyi_Struct_Cn_Fyi_Date3_Dd; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date3_Yy() { return cn_Fyi_Struct_Cn_Fyi_Date3_Yy; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date4_F() { return cn_Fyi_Struct_Cn_Fyi_Date4_F; }

    public DbsGroup getCn_Fyi_Struct_Cn_Fyi_Date4_FRedef28() { return cn_Fyi_Struct_Cn_Fyi_Date4_FRedef28; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date4_Mm() { return cn_Fyi_Struct_Cn_Fyi_Date4_Mm; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date4_Dd() { return cn_Fyi_Struct_Cn_Fyi_Date4_Dd; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date4_Yy() { return cn_Fyi_Struct_Cn_Fyi_Date4_Yy; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date5_F() { return cn_Fyi_Struct_Cn_Fyi_Date5_F; }

    public DbsGroup getCn_Fyi_Struct_Cn_Fyi_Date5_FRedef29() { return cn_Fyi_Struct_Cn_Fyi_Date5_FRedef29; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date5_Mm() { return cn_Fyi_Struct_Cn_Fyi_Date5_Mm; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date5_Dd() { return cn_Fyi_Struct_Cn_Fyi_Date5_Dd; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date5_Yy() { return cn_Fyi_Struct_Cn_Fyi_Date5_Yy; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date6_F() { return cn_Fyi_Struct_Cn_Fyi_Date6_F; }

    public DbsGroup getCn_Fyi_Struct_Cn_Fyi_Date6_FRedef30() { return cn_Fyi_Struct_Cn_Fyi_Date6_FRedef30; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date6_Mm() { return cn_Fyi_Struct_Cn_Fyi_Date6_Mm; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date6_Dd() { return cn_Fyi_Struct_Cn_Fyi_Date6_Dd; }

    public DbsField getCn_Fyi_Struct_Cn_Fyi_Date6_Yy() { return cn_Fyi_Struct_Cn_Fyi_Date6_Yy; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key_Struct = newGroupInRecord("sort_Key_Struct", "SORT-KEY-STRUCT");
        sort_Key_Struct_Sort_Key_Lgth = sort_Key_Struct.newFieldInGroup("sort_Key_Struct_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Struct_Sort_Key_Data = sort_Key_Struct.newFieldArrayInGroup("sort_Key_Struct_Sort_Key_Data", "SORT-KEY-DATA", FieldType.STRING, 1, new 
            DbsArrayController(1,55));
        sort_Key_Struct_Sort_Key_DataRedef1 = sort_Key_Struct.newGroupInGroup("sort_Key_Struct_Sort_Key_DataRedef1", "Redefines", sort_Key_Struct_Sort_Key_Data);
        sort_Key_Struct_Sort_Key = sort_Key_Struct_Sort_Key_DataRedef1.newGroupInGroup("sort_Key_Struct_Sort_Key", "SORT-KEY");
        sort_Key_Struct_Level_1_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_1_Sort_Flds", "LEVEL-1-SORT-FLDS");
        sort_Key_Struct_Rsk_L1_Value = sort_Key_Struct_Level_1_Sort_Flds.newFieldInGroup("sort_Key_Struct_Rsk_L1_Value", "RSK-L1-VALUE", FieldType.NUMERIC, 
            2);
        sort_Key_Struct_Id_L1_Record = sort_Key_Struct_Level_1_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L1_Record", "ID-L1-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Level_2_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_2_Sort_Flds", "LEVEL-2-SORT-FLDS");
        sort_Key_Struct_Id_L2_Record = sort_Key_Struct_Level_2_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L2_Record", "ID-L2-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L2_Post_Req_Id = sort_Key_Struct_Level_2_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L2_Post_Req_Id", "KEY-L2-POST-REQ-ID", 
            FieldType.STRING, 11);
        sort_Key_Struct_Level_3_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_3_Sort_Flds", "LEVEL-3-SORT-FLDS");
        sort_Key_Struct_Id_L3_Record = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L3_Record", "ID-L3-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L3_Req_Date_Time = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L3_Req_Date_Time", "KEY-L3-REQ-DATE-TIME", 
            FieldType.STRING, 15);
        sort_Key_Struct_Key_L3_From_Cont_Number = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L3_From_Cont_Number", "KEY-L3-FROM-CONT-NUMBER", 
            FieldType.STRING, 15);
        sort_Key_Struct_Level_3_Sort_FldsRedef2 = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_3_Sort_FldsRedef2", "Redefines", sort_Key_Struct_Level_3_Sort_Flds);
        sort_Key_Struct_Level_3_Sort_Flds_Reset = sort_Key_Struct_Level_3_Sort_FldsRedef2.newFieldInGroup("sort_Key_Struct_Level_3_Sort_Flds_Reset", "LEVEL-3-SORT-FLDS-RESET", 
            FieldType.STRING, 31);
        sort_Key_Struct_Level_4_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_4_Sort_Flds", "LEVEL-4-SORT-FLDS");
        sort_Key_Struct_Id_L4_Record = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L4_Record", "ID-L4-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L4_Account_Type = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L4_Account_Type", "KEY-L4-ACCOUNT-TYPE", 
            FieldType.NUMERIC, 2);
        sort_Key_Struct_Key_L4_Fyi_Mes_Num = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L4_Fyi_Mes_Num", "KEY-L4-FYI-MES-NUM", 
            FieldType.NUMERIC, 3);
        sort_Key_Struct_Key_L4_Quest_Mes_Num = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L4_Quest_Mes_Num", "KEY-L4-QUEST-MES-NUM", 
            FieldType.NUMERIC, 3);
        sort_Key_Struct_Level_4_Sort_FldsRedef3 = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_4_Sort_FldsRedef3", "Redefines", sort_Key_Struct_Level_4_Sort_Flds);
        sort_Key_Struct_Level_4_Sort_Flds_Reset = sort_Key_Struct_Level_4_Sort_FldsRedef3.newFieldInGroup("sort_Key_Struct_Level_4_Sort_Flds_Reset", "LEVEL-4-SORT-FLDS-RESET", 
            FieldType.STRING, 9);

        cn_Contract_Struct = newGroupInRecord("cn_Contract_Struct", "CN-CONTRACT-STRUCT");
        cn_Contract_Struct_Cn_Record_Id = cn_Contract_Struct.newFieldInGroup("cn_Contract_Struct_Cn_Record_Id", "CN-RECORD-ID", FieldType.STRING, 2);
        cn_Contract_Struct_Cn_Data_Lgth = cn_Contract_Struct.newFieldInGroup("cn_Contract_Struct_Cn_Data_Lgth", "CN-DATA-LGTH", FieldType.NUMERIC, 5);
        cn_Contract_Struct_Cn_Data_Occurs = cn_Contract_Struct.newFieldInGroup("cn_Contract_Struct_Cn_Data_Occurs", "CN-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        cn_Contract_Struct_Cn_Data = cn_Contract_Struct.newFieldArrayInGroup("cn_Contract_Struct_Cn_Data", "CN-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            117));
        cn_Contract_Struct_Cn_DataRedef4 = cn_Contract_Struct.newGroupInGroup("cn_Contract_Struct_Cn_DataRedef4", "Redefines", cn_Contract_Struct_Cn_Data);
        cn_Contract_Struct_Cn_Contract = cn_Contract_Struct_Cn_DataRedef4.newGroupInGroup("cn_Contract_Struct_Cn_Contract", "CN-CONTRACT");
        cn_Contract_Struct_Cn_Contract_Numbers = cn_Contract_Struct_Cn_Contract.newGroupInGroup("cn_Contract_Struct_Cn_Contract_Numbers", "CN-CONTRACT-NUMBERS");
        cn_Contract_Struct_Cn_Tiaa_Contract_Num = cn_Contract_Struct_Cn_Contract_Numbers.newGroupInGroup("cn_Contract_Struct_Cn_Tiaa_Contract_Num", "CN-TIAA-CONTRACT-NUM");
        cn_Contract_Struct_Cn_Tiaa_Cont_Num = cn_Contract_Struct_Cn_Tiaa_Contract_Num.newFieldInGroup("cn_Contract_Struct_Cn_Tiaa_Cont_Num", "CN-TIAA-CONT-NUM", 
            FieldType.STRING, 8);
        cn_Contract_Struct_Cn_Tiaa_Frmto_Ind = cn_Contract_Struct_Cn_Tiaa_Contract_Num.newFieldInGroup("cn_Contract_Struct_Cn_Tiaa_Frmto_Ind", "CN-TIAA-FRMTO-IND", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_Tiaa_Grdstd_Ind = cn_Contract_Struct_Cn_Tiaa_Contract_Num.newFieldInGroup("cn_Contract_Struct_Cn_Tiaa_Grdstd_Ind", "CN-TIAA-GRDSTD-IND", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_Tiaa_Cont_Tbd = cn_Contract_Struct_Cn_Tiaa_Contract_Num.newFieldInGroup("cn_Contract_Struct_Cn_Tiaa_Cont_Tbd", "CN-TIAA-CONT-TBD", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_Cref_Contract_Num = cn_Contract_Struct_Cn_Contract_Numbers.newGroupInGroup("cn_Contract_Struct_Cn_Cref_Contract_Num", "CN-CREF-CONTRACT-NUM");
        cn_Contract_Struct_Cn_Cref_Cont_Num = cn_Contract_Struct_Cn_Cref_Contract_Num.newFieldInGroup("cn_Contract_Struct_Cn_Cref_Cont_Num", "CN-CREF-CONT-NUM", 
            FieldType.STRING, 8);
        cn_Contract_Struct_Cn_Cref_Frmto_Ind = cn_Contract_Struct_Cn_Cref_Contract_Num.newFieldInGroup("cn_Contract_Struct_Cn_Cref_Frmto_Ind", "CN-CREF-FRMTO-IND", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_Cref_Cont_Tbd = cn_Contract_Struct_Cn_Cref_Contract_Num.newFieldInGroup("cn_Contract_Struct_Cn_Cref_Cont_Tbd", "CN-CREF-CONT-TBD", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_Real_Estate_Contract = cn_Contract_Struct_Cn_Contract_Numbers.newGroupInGroup("cn_Contract_Struct_Cn_Real_Estate_Contract", 
            "CN-REAL-ESTATE-CONTRACT");
        cn_Contract_Struct_Cn_Real_Estate_Cont = cn_Contract_Struct_Cn_Real_Estate_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Real_Estate_Cont", 
            "CN-REAL-ESTATE-CONT", FieldType.STRING, 15);
        cn_Contract_Struct_Cn_Real_Frmto_Ind = cn_Contract_Struct_Cn_Real_Estate_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Real_Frmto_Ind", "CN-REAL-FRMTO-IND", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_Real_Estate_Tbd = cn_Contract_Struct_Cn_Real_Estate_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Real_Estate_Tbd", "CN-REAL-ESTATE-TBD", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_Req_Source = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Req_Source", "CN-REQ-SOURCE", FieldType.STRING, 
            1);
        cn_Contract_Struct_Cn_Service_Rep = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Service_Rep", "CN-SERVICE-REP", FieldType.STRING, 
            40);
        cn_Contract_Struct_Cn_Switch_Ind = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Switch_Ind", "CN-SWITCH-IND", FieldType.STRING, 
            1);
        cn_Contract_Struct_Cn_Effective_Date_F = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Effective_Date_F", "CN-EFFECTIVE-DATE-F", 
            FieldType.NUMERIC, 8);
        cn_Contract_Struct_Cn_Effective_Date_FRedef5 = cn_Contract_Struct_Cn_Contract.newGroupInGroup("cn_Contract_Struct_Cn_Effective_Date_FRedef5", 
            "Redefines", cn_Contract_Struct_Cn_Effective_Date_F);
        cn_Contract_Struct_Cn_Effective_Mm = cn_Contract_Struct_Cn_Effective_Date_FRedef5.newFieldInGroup("cn_Contract_Struct_Cn_Effective_Mm", "CN-EFFECTIVE-MM", 
            FieldType.NUMERIC, 2);
        cn_Contract_Struct_Cn_Effective_Dd = cn_Contract_Struct_Cn_Effective_Date_FRedef5.newFieldInGroup("cn_Contract_Struct_Cn_Effective_Dd", "CN-EFFECTIVE-DD", 
            FieldType.NUMERIC, 2);
        cn_Contract_Struct_Cn_Effective_Yy = cn_Contract_Struct_Cn_Effective_Date_FRedef5.newFieldInGroup("cn_Contract_Struct_Cn_Effective_Yy", "CN-EFFECTIVE-YY", 
            FieldType.NUMERIC, 4);
        cn_Contract_Struct_Cn_From_Tran_Type = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_From_Tran_Type", "CN-FROM-TRAN-TYPE", 
            FieldType.STRING, 1);
        cn_Contract_Struct_Cn_To_Tran_Type = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_To_Tran_Type", "CN-TO-TRAN-TYPE", FieldType.STRING, 
            1);
        cn_Contract_Struct_Cn_Mod_Date_Time_F = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Date_Time_F", "CN-MOD-DATE-TIME-F", 
            FieldType.NUMERIC, 15);
        cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6 = cn_Contract_Struct_Cn_Contract.newGroupInGroup("cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6", "Redefines", 
            cn_Contract_Struct_Cn_Mod_Date_Time_F);
        cn_Contract_Struct_Cn_Mod_Mm = cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Mm", "CN-MOD-MM", FieldType.NUMERIC, 
            2);
        cn_Contract_Struct_Cn_Mod_Dd = cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Dd", "CN-MOD-DD", FieldType.NUMERIC, 
            2);
        cn_Contract_Struct_Cn_Mod_Yy = cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Yy", "CN-MOD-YY", FieldType.NUMERIC, 
            4);
        cn_Contract_Struct_Cn_Mod_Hh = cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Hh", "CN-MOD-HH", FieldType.NUMERIC, 
            2);
        cn_Contract_Struct_Cn_Mod_Min = cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Min", "CN-MOD-MIN", FieldType.NUMERIC, 
            2);
        cn_Contract_Struct_Cn_Mod_Sec = cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Sec", "CN-MOD-SEC", FieldType.NUMERIC, 
            2);
        cn_Contract_Struct_Cn_Mod_Ampm = cn_Contract_Struct_Cn_Mod_Date_Time_FRedef6.newFieldInGroup("cn_Contract_Struct_Cn_Mod_Ampm", "CN-MOD-AMPM", 
            FieldType.NUMERIC, 1);
        cn_Contract_Struct_Cn_Mach_Func_1 = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Mach_Func_1", "CN-MACH-FUNC-1", FieldType.STRING, 
            1);
        cn_Contract_Struct_Cn_Image_Ind = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Image_Ind", "CN-IMAGE-IND", FieldType.STRING, 
            1);
        cn_Contract_Struct_Cn_Processing_Date = cn_Contract_Struct_Cn_Contract.newFieldInGroup("cn_Contract_Struct_Cn_Processing_Date", "CN-PROCESSING-DATE", 
            FieldType.STRING, 10);

        cn_Frm_Rec_Struct = newGroupInRecord("cn_Frm_Rec_Struct", "CN-FRM-REC-STRUCT");
        cn_Frm_Rec_Struct_Cn_Record_Id = cn_Frm_Rec_Struct.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Record_Id", "CN-RECORD-ID", FieldType.STRING, 2);
        cn_Frm_Rec_Struct_Cn_Data_Lgth = cn_Frm_Rec_Struct.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Data_Lgth", "CN-DATA-LGTH", FieldType.NUMERIC, 5);
        cn_Frm_Rec_Struct_Cn_Data_Occurs = cn_Frm_Rec_Struct.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Data_Occurs", "CN-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        cn_Frm_Rec_Struct_Cn_Data = cn_Frm_Rec_Struct.newFieldArrayInGroup("cn_Frm_Rec_Struct_Cn_Data", "CN-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            340));
        cn_Frm_Rec_Struct_Cn_DataRedef7 = cn_Frm_Rec_Struct.newGroupInGroup("cn_Frm_Rec_Struct_Cn_DataRedef7", "Redefines", cn_Frm_Rec_Struct_Cn_Data);
        cn_Frm_Rec_Struct_Cn_Frm_Rec = cn_Frm_Rec_Struct_Cn_DataRedef7.newGroupArrayInGroup("cn_Frm_Rec_Struct_Cn_Frm_Rec", "CN-FRM-REC", new DbsArrayController(1,
            20));
        cn_Frm_Rec_Struct_Cn_Frm_Account_Type = cn_Frm_Rec_Struct_Cn_Frm_Rec.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Frm_Account_Type", "CN-FRM-ACCOUNT-TYPE", 
            FieldType.NUMERIC, 2);
        cn_Frm_Rec_Struct_Cn_Frm_Myi = cn_Frm_Rec_Struct_Cn_Frm_Rec.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Frm_Myi", "CN-FRM-MYI", FieldType.STRING, 1);
        cn_Frm_Rec_Struct_Cn_Frm_Trn_Typ = cn_Frm_Rec_Struct_Cn_Frm_Rec.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Frm_Trn_Typ", "CN-FRM-TRN-TYP", FieldType.STRING, 
            1);
        cn_Frm_Rec_Struct_Cn_Frm_Percent = cn_Frm_Rec_Struct_Cn_Frm_Rec.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Frm_Percent", "CN-FRM-PERCENT", FieldType.PACKED_DECIMAL, 
            3);
        cn_Frm_Rec_Struct_Cn_Frm_Units = cn_Frm_Rec_Struct_Cn_Frm_Rec.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Frm_Units", "CN-FRM-UNITS", FieldType.PACKED_DECIMAL, 
            10,3);
        cn_Frm_Rec_Struct_Cn_Frm_Dollars = cn_Frm_Rec_Struct_Cn_Frm_Rec.newFieldInGroup("cn_Frm_Rec_Struct_Cn_Frm_Dollars", "CN-FRM-DOLLARS", FieldType.PACKED_DECIMAL, 
            9,2);

        cn_To_Acct_Struct = newGroupInRecord("cn_To_Acct_Struct", "CN-TO-ACCT-STRUCT");
        cn_To_Acct_Struct_Cn_Record_Id = cn_To_Acct_Struct.newFieldInGroup("cn_To_Acct_Struct_Cn_Record_Id", "CN-RECORD-ID", FieldType.STRING, 2);
        cn_To_Acct_Struct_Cn_Data_Lgth = cn_To_Acct_Struct.newFieldInGroup("cn_To_Acct_Struct_Cn_Data_Lgth", "CN-DATA-LGTH", FieldType.NUMERIC, 5);
        cn_To_Acct_Struct_Cn_Data_Occurs = cn_To_Acct_Struct.newFieldInGroup("cn_To_Acct_Struct_Cn_Data_Occurs", "CN-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        cn_To_Acct_Struct_Cn_Data = cn_To_Acct_Struct.newFieldArrayInGroup("cn_To_Acct_Struct_Cn_Data", "CN-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            340));
        cn_To_Acct_Struct_Cn_DataRedef8 = cn_To_Acct_Struct.newGroupInGroup("cn_To_Acct_Struct_Cn_DataRedef8", "Redefines", cn_To_Acct_Struct_Cn_Data);
        cn_To_Acct_Struct_Cn_To_Acct = cn_To_Acct_Struct_Cn_DataRedef8.newGroupArrayInGroup("cn_To_Acct_Struct_Cn_To_Acct", "CN-TO-ACCT", new DbsArrayController(1,
            20));
        cn_To_Acct_Struct_Cn_To_Account_Type = cn_To_Acct_Struct_Cn_To_Acct.newFieldInGroup("cn_To_Acct_Struct_Cn_To_Account_Type", "CN-TO-ACCOUNT-TYPE", 
            FieldType.NUMERIC, 2);
        cn_To_Acct_Struct_Cn_To_Myi = cn_To_Acct_Struct_Cn_To_Acct.newFieldInGroup("cn_To_Acct_Struct_Cn_To_Myi", "CN-TO-MYI", FieldType.STRING, 1);
        cn_To_Acct_Struct_Cn_To_Trn_Typ = cn_To_Acct_Struct_Cn_To_Acct.newFieldInGroup("cn_To_Acct_Struct_Cn_To_Trn_Typ", "CN-TO-TRN-TYP", FieldType.STRING, 
            1);
        cn_To_Acct_Struct_Cn_To_Percent = cn_To_Acct_Struct_Cn_To_Acct.newFieldInGroup("cn_To_Acct_Struct_Cn_To_Percent", "CN-TO-PERCENT", FieldType.PACKED_DECIMAL, 
            3);
        cn_To_Acct_Struct_Cn_To_Units = cn_To_Acct_Struct_Cn_To_Acct.newFieldInGroup("cn_To_Acct_Struct_Cn_To_Units", "CN-TO-UNITS", FieldType.PACKED_DECIMAL, 
            10,3);
        cn_To_Acct_Struct_Cn_To_Dollars = cn_To_Acct_Struct_Cn_To_Acct.newFieldInGroup("cn_To_Acct_Struct_Cn_To_Dollars", "CN-TO-DOLLARS", FieldType.PACKED_DECIMAL, 
            9,2);

        cn_Etf_Struct = newGroupInRecord("cn_Etf_Struct", "CN-ETF-STRUCT");
        cn_Etf_Struct_Cn_Record_Id = cn_Etf_Struct.newFieldInGroup("cn_Etf_Struct_Cn_Record_Id", "CN-RECORD-ID", FieldType.STRING, 2);
        cn_Etf_Struct_Cn_Data_Lgth = cn_Etf_Struct.newFieldInGroup("cn_Etf_Struct_Cn_Data_Lgth", "CN-DATA-LGTH", FieldType.NUMERIC, 5);
        cn_Etf_Struct_Cn_Data_Occurs = cn_Etf_Struct.newFieldInGroup("cn_Etf_Struct_Cn_Data_Occurs", "CN-DATA-OCCURS", FieldType.NUMERIC, 5);
        cn_Etf_Struct_Cn_Data = cn_Etf_Struct.newFieldArrayInGroup("cn_Etf_Struct_Cn_Data", "CN-DATA", FieldType.STRING, 1, new DbsArrayController(1,528));
        cn_Etf_Struct_Cn_DataRedef9 = cn_Etf_Struct.newGroupInGroup("cn_Etf_Struct_Cn_DataRedef9", "Redefines", cn_Etf_Struct_Cn_Data);
        cn_Etf_Struct_Cn_Etf = cn_Etf_Struct_Cn_DataRedef9.newGroupArrayInGroup("cn_Etf_Struct_Cn_Etf", "CN-ETF", new DbsArrayController(1,2));
        cn_Etf_Struct_Cn_Etf_Myi = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Myi", "CN-ETF-MYI", FieldType.STRING, 1);
        cn_Etf_Struct_Cn_Etf_Vtb = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Vtb", "CN-ETF-VTB", FieldType.STRING, 1);
        cn_Etf_Struct_Cn_Etf_Datvp1f = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Datvp1f", "CN-ETF-DATVP1F", FieldType.NUMERIC, 8);
        cn_Etf_Struct_Cn_Etf_Datvp1fRedef10 = cn_Etf_Struct_Cn_Etf.newGroupInGroup("cn_Etf_Struct_Cn_Etf_Datvp1fRedef10", "Redefines", cn_Etf_Struct_Cn_Etf_Datvp1f);
        cn_Etf_Struct_Cn_Etf_Dd_P1v = cn_Etf_Struct_Cn_Etf_Datvp1fRedef10.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Dd_P1v", "CN-ETF-DD-P1V", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Mm_P1v = cn_Etf_Struct_Cn_Etf_Datvp1fRedef10.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Mm_P1v", "CN-ETF-MM-P1V", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Yy_P1v = cn_Etf_Struct_Cn_Etf_Datvp1fRedef10.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Yy_P1v", "CN-ETF-YY-P1V", FieldType.NUMERIC, 
            4);
        cn_Etf_Struct_Cn_Etf_Var_P1v = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Var_P1v", "CN-ETF-VAR-P1V", FieldType.STRING, 30);
        cn_Etf_Struct_Cn_Etf_Datvp2f = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Datvp2f", "CN-ETF-DATVP2F", FieldType.NUMERIC, 8);
        cn_Etf_Struct_Cn_Etf_Datvp2fRedef11 = cn_Etf_Struct_Cn_Etf.newGroupInGroup("cn_Etf_Struct_Cn_Etf_Datvp2fRedef11", "Redefines", cn_Etf_Struct_Cn_Etf_Datvp2f);
        cn_Etf_Struct_Cn_Etf_Dd_P2v = cn_Etf_Struct_Cn_Etf_Datvp2fRedef11.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Dd_P2v", "CN-ETF-DD-P2V", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Mm_P2v = cn_Etf_Struct_Cn_Etf_Datvp2fRedef11.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Mm_P2v", "CN-ETF-MM-P2V", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Yy_P2v = cn_Etf_Struct_Cn_Etf_Datvp2fRedef11.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Yy_P2v", "CN-ETF-YY-P2V", FieldType.NUMERIC, 
            4);
        cn_Etf_Struct_Cn_Etf_Var_P2v = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Var_P2v", "CN-ETF-VAR-P2V", FieldType.STRING, 30);
        cn_Etf_Struct_Cn_Etf_Datvp3f = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Datvp3f", "CN-ETF-DATVP3F", FieldType.NUMERIC, 8);
        cn_Etf_Struct_Cn_Etf_Datvp3fRedef12 = cn_Etf_Struct_Cn_Etf.newGroupInGroup("cn_Etf_Struct_Cn_Etf_Datvp3fRedef12", "Redefines", cn_Etf_Struct_Cn_Etf_Datvp3f);
        cn_Etf_Struct_Cn_Etf_Dd_P3v = cn_Etf_Struct_Cn_Etf_Datvp3fRedef12.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Dd_P3v", "CN-ETF-DD-P3V", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Mm_P3v = cn_Etf_Struct_Cn_Etf_Datvp3fRedef12.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Mm_P3v", "CN-ETF-MM-P3V", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Yy_P3v = cn_Etf_Struct_Cn_Etf_Datvp3fRedef12.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Yy_P3v", "CN-ETF-YY-P3V", FieldType.NUMERIC, 
            4);
        cn_Etf_Struct_Cn_Etf_Var_P3v = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Var_P3v", "CN-ETF-VAR-P3V", FieldType.STRING, 30);
        cn_Etf_Struct_Cn_Etf_Payhddt = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Payhddt", "CN-ETF-PAYHDDT", FieldType.NUMERIC, 8);
        cn_Etf_Struct_Cn_Etf_PayhddtRedef13 = cn_Etf_Struct_Cn_Etf.newGroupInGroup("cn_Etf_Struct_Cn_Etf_PayhddtRedef13", "Redefines", cn_Etf_Struct_Cn_Etf_Payhddt);
        cn_Etf_Struct_Cn_Etf_Dd_Phd = cn_Etf_Struct_Cn_Etf_PayhddtRedef13.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Dd_Phd", "CN-ETF-DD-PHD", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Mm_Phd = cn_Etf_Struct_Cn_Etf_PayhddtRedef13.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Mm_Phd", "CN-ETF-MM-PHD", FieldType.NUMERIC, 
            2);
        cn_Etf_Struct_Cn_Etf_Yy_Phd = cn_Etf_Struct_Cn_Etf_PayhddtRedef13.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Yy_Phd", "CN-ETF-YY-PHD", FieldType.NUMERIC, 
            4);
        cn_Etf_Struct_Cn_Etf_Accnt_Grded_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Grded_P", "CN-ETF-ACCNT-GRDED-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Mma_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Mma_P", "CN-ETF-ACCNT-MMA-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Social_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Social_P", "CN-ETF-ACCNT-SOCIAL-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Stock_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Stock_P", "CN-ETF-ACCNT-STOCK-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Bond_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Bond_P", "CN-ETF-ACCNT-BOND-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Growth_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Growth_P", "CN-ETF-ACCNT-GROWTH-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Reale_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Reale_P", "CN-ETF-ACCNT-REALE-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Eqi_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Eqi_P", "CN-ETF-ACCNT-EQI-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Infb_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Infb_P", "CN-ETF-ACCNT-INFB-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Global_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Global_P", "CN-ETF-ACCNT-GLOBAL-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Mutual_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Mutual_P", "CN-ETF-ACCNT-MUTUAL-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Std_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Std_P", "CN-ETF-ACCNT-STD-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Mma_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Mma_U", "CN-ETF-ACCNT-MMA-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Social_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Social_U", "CN-ETF-ACCNT-SOCIAL-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Stock_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Stock_U", "CN-ETF-ACCNT-STOCK-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Bond_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Bond_U", "CN-ETF-ACCNT-BOND-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Growth_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Growth_U", "CN-ETF-ACCNT-GROWTH-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Reale_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Reale_U", "CN-ETF-ACCNT-REALE-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Eqi_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Eqi_U", "CN-ETF-ACCNT-EQI-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Infb_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Infb_U", "CN-ETF-ACCNT-INFB-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Global_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Global_U", "CN-ETF-ACCNT-GLOBAL-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Mutual_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Mutual_U", "CN-ETF-ACCNT-MUTUAL-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Etf_Struct_Cn_Etf_Accnt_Std_Con = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Std_Con", "CN-ETF-ACCNT-STD-CON", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Std_Div = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Std_Div", "CN-ETF-ACCNT-STD-DIV", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Grd_Con = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Grd_Con", "CN-ETF-ACCNT-GRD-CON", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Grd_Div = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Grd_Div", "CN-ETF-ACCNT-GRD-DIV", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Access_P = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Access_P", "CN-ETF-ACCNT-ACCESS-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Etf_Struct_Cn_Etf_Accnt_Access_U = cn_Etf_Struct_Cn_Etf.newFieldInGroup("cn_Etf_Struct_Cn_Etf_Accnt_Access_U", "CN-ETF-ACCNT-ACCESS-U", FieldType.PACKED_DECIMAL, 
            8,3);

        cn_Ett_Struct = newGroupInRecord("cn_Ett_Struct", "CN-ETT-STRUCT");
        cn_Ett_Struct_Cn_Record_Id = cn_Ett_Struct.newFieldInGroup("cn_Ett_Struct_Cn_Record_Id", "CN-RECORD-ID", FieldType.STRING, 2);
        cn_Ett_Struct_Cn_Data_Lgth = cn_Ett_Struct.newFieldInGroup("cn_Ett_Struct_Cn_Data_Lgth", "CN-DATA-LGTH", FieldType.NUMERIC, 5);
        cn_Ett_Struct_Cn_Data_Occurs = cn_Ett_Struct.newFieldInGroup("cn_Ett_Struct_Cn_Data_Occurs", "CN-DATA-OCCURS", FieldType.NUMERIC, 5);
        cn_Ett_Struct_Cn_Data = cn_Ett_Struct.newFieldArrayInGroup("cn_Ett_Struct_Cn_Data", "CN-DATA", FieldType.STRING, 1, new DbsArrayController(1,528));
        cn_Ett_Struct_Cn_DataRedef14 = cn_Ett_Struct.newGroupInGroup("cn_Ett_Struct_Cn_DataRedef14", "Redefines", cn_Ett_Struct_Cn_Data);
        cn_Ett_Struct_Cn_Ett = cn_Ett_Struct_Cn_DataRedef14.newGroupArrayInGroup("cn_Ett_Struct_Cn_Ett", "CN-ETT", new DbsArrayController(1,2));
        cn_Ett_Struct_Cn_Ett_Myi = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Myi", "CN-ETT-MYI", FieldType.STRING, 1);
        cn_Ett_Struct_Cn_Ett_Vtb = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Vtb", "CN-ETT-VTB", FieldType.STRING, 1);
        cn_Ett_Struct_Cn_Ett_Datvp1f = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Datvp1f", "CN-ETT-DATVP1F", FieldType.NUMERIC, 8);
        cn_Ett_Struct_Cn_Ett_Datvp1fRedef15 = cn_Ett_Struct_Cn_Ett.newGroupInGroup("cn_Ett_Struct_Cn_Ett_Datvp1fRedef15", "Redefines", cn_Ett_Struct_Cn_Ett_Datvp1f);
        cn_Ett_Struct_Cn_Ett_Dd_P1v = cn_Ett_Struct_Cn_Ett_Datvp1fRedef15.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Dd_P1v", "CN-ETT-DD-P1V", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Mm_P1v = cn_Ett_Struct_Cn_Ett_Datvp1fRedef15.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Mm_P1v", "CN-ETT-MM-P1V", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Yy_P1v = cn_Ett_Struct_Cn_Ett_Datvp1fRedef15.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Yy_P1v", "CN-ETT-YY-P1V", FieldType.NUMERIC, 
            4);
        cn_Ett_Struct_Cn_Ett_Var_P1v = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Var_P1v", "CN-ETT-VAR-P1V", FieldType.STRING, 30);
        cn_Ett_Struct_Cn_Ett_Datvp2f = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Datvp2f", "CN-ETT-DATVP2F", FieldType.NUMERIC, 8);
        cn_Ett_Struct_Cn_Ett_Datvp2fRedef16 = cn_Ett_Struct_Cn_Ett.newGroupInGroup("cn_Ett_Struct_Cn_Ett_Datvp2fRedef16", "Redefines", cn_Ett_Struct_Cn_Ett_Datvp2f);
        cn_Ett_Struct_Cn_Ett_Dd_P2v = cn_Ett_Struct_Cn_Ett_Datvp2fRedef16.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Dd_P2v", "CN-ETT-DD-P2V", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Mm_P2v = cn_Ett_Struct_Cn_Ett_Datvp2fRedef16.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Mm_P2v", "CN-ETT-MM-P2V", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Yy_P2v = cn_Ett_Struct_Cn_Ett_Datvp2fRedef16.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Yy_P2v", "CN-ETT-YY-P2V", FieldType.NUMERIC, 
            4);
        cn_Ett_Struct_Cn_Ett_Var_P2v = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Var_P2v", "CN-ETT-VAR-P2V", FieldType.STRING, 30);
        cn_Ett_Struct_Cn_Ett_Datvp3f = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Datvp3f", "CN-ETT-DATVP3F", FieldType.NUMERIC, 8);
        cn_Ett_Struct_Cn_Ett_Datvp3fRedef17 = cn_Ett_Struct_Cn_Ett.newGroupInGroup("cn_Ett_Struct_Cn_Ett_Datvp3fRedef17", "Redefines", cn_Ett_Struct_Cn_Ett_Datvp3f);
        cn_Ett_Struct_Cn_Ett_Dd_P3v = cn_Ett_Struct_Cn_Ett_Datvp3fRedef17.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Dd_P3v", "CN-ETT-DD-P3V", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Mm_P3v = cn_Ett_Struct_Cn_Ett_Datvp3fRedef17.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Mm_P3v", "CN-ETT-MM-P3V", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Yy_P3v = cn_Ett_Struct_Cn_Ett_Datvp3fRedef17.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Yy_P3v", "CN-ETT-YY-P3V", FieldType.NUMERIC, 
            4);
        cn_Ett_Struct_Cn_Ett_Var_P3v = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Var_P3v", "CN-ETT-VAR-P3V", FieldType.STRING, 30);
        cn_Ett_Struct_Cn_Ett_Payhddt = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Payhddt", "CN-ETT-PAYHDDT", FieldType.NUMERIC, 8);
        cn_Ett_Struct_Cn_Ett_PayhddtRedef18 = cn_Ett_Struct_Cn_Ett.newGroupInGroup("cn_Ett_Struct_Cn_Ett_PayhddtRedef18", "Redefines", cn_Ett_Struct_Cn_Ett_Payhddt);
        cn_Ett_Struct_Cn_Ett_Dd_Phd = cn_Ett_Struct_Cn_Ett_PayhddtRedef18.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Dd_Phd", "CN-ETT-DD-PHD", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Mm_Phd = cn_Ett_Struct_Cn_Ett_PayhddtRedef18.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Mm_Phd", "CN-ETT-MM-PHD", FieldType.NUMERIC, 
            2);
        cn_Ett_Struct_Cn_Ett_Yy_Phd = cn_Ett_Struct_Cn_Ett_PayhddtRedef18.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Yy_Phd", "CN-ETT-YY-PHD", FieldType.NUMERIC, 
            4);
        cn_Ett_Struct_Cn_Ett_Accnt_Grded_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Grded_P", "CN-ETT-ACCNT-GRDED-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Mma_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Mma_P", "CN-ETT-ACCNT-MMA-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Social_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Social_P", "CN-ETT-ACCNT-SOCIAL-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Stock_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Stock_P", "CN-ETT-ACCNT-STOCK-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Bond_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Bond_P", "CN-ETT-ACCNT-BOND-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Growth_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Growth_P", "CN-ETT-ACCNT-GROWTH-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Reale_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Reale_P", "CN-ETT-ACCNT-REALE-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Eqi_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Eqi_P", "CN-ETT-ACCNT-EQI-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Infb_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Infb_P", "CN-ETT-ACCNT-INFB-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Global_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Global_P", "CN-ETT-ACCNT-GLOBAL-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Mutual_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Mutual_P", "CN-ETT-ACCNT-MUTUAL-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Std_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Std_P", "CN-ETT-ACCNT-STD-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Mma_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Mma_U", "CN-ETT-ACCNT-MMA-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Social_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Social_U", "CN-ETT-ACCNT-SOCIAL-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Stock_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Stock_U", "CN-ETT-ACCNT-STOCK-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Bond_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Bond_U", "CN-ETT-ACCNT-BOND-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Growth_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Growth_U", "CN-ETT-ACCNT-GROWTH-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Reale_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Reale_U", "CN-ETT-ACCNT-REALE-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Eqi_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Eqi_U", "CN-ETT-ACCNT-EQI-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Infb_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Infb_U", "CN-ETT-ACCNT-INFB-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Global_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Global_U", "CN-ETT-ACCNT-GLOBAL-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Mutual_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Mutual_U", "CN-ETT-ACCNT-MUTUAL-U", FieldType.PACKED_DECIMAL, 
            8,3);
        cn_Ett_Struct_Cn_Ett_Accnt_Std_Con = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Std_Con", "CN-ETT-ACCNT-STD-CON", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Std_Div = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Std_Div", "CN-ETT-ACCNT-STD-DIV", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Grd_Con = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Grd_Con", "CN-ETT-ACCNT-GRD-CON", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Grd_Div = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Grd_Div", "CN-ETT-ACCNT-GRD-DIV", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Access_P = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Access_P", "CN-ETT-ACCNT-ACCESS-P", FieldType.PACKED_DECIMAL, 
            9,2);
        cn_Ett_Struct_Cn_Ett_Accnt_Access_U = cn_Ett_Struct_Cn_Ett.newFieldInGroup("cn_Ett_Struct_Cn_Ett_Accnt_Access_U", "CN-ETT-ACCNT-ACCESS-U", FieldType.PACKED_DECIMAL, 
            8,3);

        cn_Anv_Struct = newGroupInRecord("cn_Anv_Struct", "CN-ANV-STRUCT");
        cn_Anv_Struct_Cn_Record_Id = cn_Anv_Struct.newFieldInGroup("cn_Anv_Struct_Cn_Record_Id", "CN-RECORD-ID", FieldType.STRING, 2);
        cn_Anv_Struct_Cn_Data_Lgth = cn_Anv_Struct.newFieldInGroup("cn_Anv_Struct_Cn_Data_Lgth", "CN-DATA-LGTH", FieldType.NUMERIC, 5);
        cn_Anv_Struct_Cn_Data_Occurs = cn_Anv_Struct.newFieldInGroup("cn_Anv_Struct_Cn_Data_Occurs", "CN-DATA-OCCURS", FieldType.NUMERIC, 5);
        cn_Anv_Struct_Cn_Data = cn_Anv_Struct.newFieldArrayInGroup("cn_Anv_Struct_Cn_Data", "CN-DATA", FieldType.STRING, 1, new DbsArrayController(1,241));
        cn_Anv_Struct_Cn_DataRedef19 = cn_Anv_Struct.newGroupInGroup("cn_Anv_Struct_Cn_DataRedef19", "Redefines", cn_Anv_Struct_Cn_Data);
        cn_Anv_Struct_Cn_Anv = cn_Anv_Struct_Cn_DataRedef19.newGroupInGroup("cn_Anv_Struct_Cn_Anv", "CN-ANV");
        cn_Anv_Struct_Cn_Anv_Myi = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Myi", "CN-ANV-MYI", FieldType.STRING, 1);
        cn_Anv_Struct_Cn_Anv_Datvp1f = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Datvp1f", "CN-ANV-DATVP1F", FieldType.NUMERIC, 8);
        cn_Anv_Struct_Cn_Anv_Datvp1fRedef20 = cn_Anv_Struct_Cn_Anv.newGroupInGroup("cn_Anv_Struct_Cn_Anv_Datvp1fRedef20", "Redefines", cn_Anv_Struct_Cn_Anv_Datvp1f);
        cn_Anv_Struct_Cn_Anv_Dd_P1v = cn_Anv_Struct_Cn_Anv_Datvp1fRedef20.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Dd_P1v", "CN-ANV-DD-P1V", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Mm_P1v = cn_Anv_Struct_Cn_Anv_Datvp1fRedef20.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Mm_P1v", "CN-ANV-MM-P1V", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Yy_P1v = cn_Anv_Struct_Cn_Anv_Datvp1fRedef20.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Yy_P1v", "CN-ANV-YY-P1V", FieldType.NUMERIC, 
            4);
        cn_Anv_Struct_Cn_Anv_Var_P1v = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Var_P1v", "CN-ANV-VAR-P1V", FieldType.STRING, 30);
        cn_Anv_Struct_Cn_Anv_Datvp2f = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Datvp2f", "CN-ANV-DATVP2F", FieldType.NUMERIC, 8);
        cn_Anv_Struct_Cn_Anv_Datvp2fRedef21 = cn_Anv_Struct_Cn_Anv.newGroupInGroup("cn_Anv_Struct_Cn_Anv_Datvp2fRedef21", "Redefines", cn_Anv_Struct_Cn_Anv_Datvp2f);
        cn_Anv_Struct_Cn_Anv_Dd_P2v = cn_Anv_Struct_Cn_Anv_Datvp2fRedef21.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Dd_P2v", "CN-ANV-DD-P2V", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Mm_P2v = cn_Anv_Struct_Cn_Anv_Datvp2fRedef21.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Mm_P2v", "CN-ANV-MM-P2V", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Yy_P2v = cn_Anv_Struct_Cn_Anv_Datvp2fRedef21.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Yy_P2v", "CN-ANV-YY-P2V", FieldType.NUMERIC, 
            4);
        cn_Anv_Struct_Cn_Anv_Var_P2v = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Var_P2v", "CN-ANV-VAR-P2V", FieldType.STRING, 30);
        cn_Anv_Struct_Cn_Anv_Datvp3f = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Datvp3f", "CN-ANV-DATVP3F", FieldType.NUMERIC, 8);
        cn_Anv_Struct_Cn_Anv_Datvp3fRedef22 = cn_Anv_Struct_Cn_Anv.newGroupInGroup("cn_Anv_Struct_Cn_Anv_Datvp3fRedef22", "Redefines", cn_Anv_Struct_Cn_Anv_Datvp3f);
        cn_Anv_Struct_Cn_Anv_Dd_P3v = cn_Anv_Struct_Cn_Anv_Datvp3fRedef22.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Dd_P3v", "CN-ANV-DD-P3V", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Mm_P3v = cn_Anv_Struct_Cn_Anv_Datvp3fRedef22.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Mm_P3v", "CN-ANV-MM-P3V", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Yy_P3v = cn_Anv_Struct_Cn_Anv_Datvp3fRedef22.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Yy_P3v", "CN-ANV-YY-P3V", FieldType.NUMERIC, 
            4);
        cn_Anv_Struct_Cn_Anv_Var_P3v = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Var_P3v", "CN-ANV-VAR-P3V", FieldType.STRING, 30);
        cn_Anv_Struct_Cn_Anv_Payhddt = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Payhddt", "CN-ANV-PAYHDDT", FieldType.NUMERIC, 8);
        cn_Anv_Struct_Cn_Anv_PayhddtRedef23 = cn_Anv_Struct_Cn_Anv.newGroupInGroup("cn_Anv_Struct_Cn_Anv_PayhddtRedef23", "Redefines", cn_Anv_Struct_Cn_Anv_Payhddt);
        cn_Anv_Struct_Cn_Anv_Dd_Phd = cn_Anv_Struct_Cn_Anv_PayhddtRedef23.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Dd_Phd", "CN-ANV-DD-PHD", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Mm_Phd = cn_Anv_Struct_Cn_Anv_PayhddtRedef23.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Mm_Phd", "CN-ANV-MM-PHD", FieldType.NUMERIC, 
            2);
        cn_Anv_Struct_Cn_Anv_Yy_Phd = cn_Anv_Struct_Cn_Anv_PayhddtRedef23.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Yy_Phd", "CN-ANV-YY-PHD", FieldType.NUMERIC, 
            4);
        cn_Anv_Struct_Cn_Anv_Accnt_Mma_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Mma_M", "CN-ANV-ACCNT-MMA-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Social_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Social_M", "CN-ANV-ACCNT-SOCIAL-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Stock_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Stock_M", "CN-ANV-ACCNT-STOCK-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Bond_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Bond_M", "CN-ANV-ACCNT-BOND-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Growth_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Growth_M", "CN-ANV-ACCNT-GROWTH-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Reale_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Reale_M", "CN-ANV-ACCNT-REALE-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Eqi_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Eqi_M", "CN-ANV-ACCNT-EQI-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Infb_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Infb_M", "CN-ANV-ACCNT-INFB-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Global_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Global_M", "CN-ANV-ACCNT-GLOBAL-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Mutual_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Mutual_M", "CN-ANV-ACCNT-MUTUAL-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Mma_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Mma_A", "CN-ANV-ACCNT-MMA-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Social_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Social_A", "CN-ANV-ACCNT-SOCIAL-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Stock_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Stock_A", "CN-ANV-ACCNT-STOCK-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Bond_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Bond_A", "CN-ANV-ACCNT-BOND-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Growth_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Growth_A", "CN-ANV-ACCNT-GROWTH-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Reale_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Reale_A", "CN-ANV-ACCNT-REALE-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Eqi_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Eqi_A", "CN-ANV-ACCNT-EQI-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Infb_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Infb_A", "CN-ANV-ACCNT-INFB-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Global_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Global_A", "CN-ANV-ACCNT-GLOBAL-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Mutual_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Mutual_A", "CN-ANV-ACCNT-MUTUAL-A", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Access_M = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Access_M", "CN-ANV-ACCNT-ACCESS-M", FieldType.PACKED_DECIMAL, 
            9,4);
        cn_Anv_Struct_Cn_Anv_Accnt_Access_A = cn_Anv_Struct_Cn_Anv.newFieldInGroup("cn_Anv_Struct_Cn_Anv_Accnt_Access_A", "CN-ANV-ACCNT-ACCESS-A", FieldType.PACKED_DECIMAL, 
            9,4);

        cn_Fyi_Struct = newGroupInRecord("cn_Fyi_Struct", "CN-FYI-STRUCT");
        cn_Fyi_Struct_Cn_Record_Id = cn_Fyi_Struct.newFieldInGroup("cn_Fyi_Struct_Cn_Record_Id", "CN-RECORD-ID", FieldType.STRING, 2);
        cn_Fyi_Struct_Cn_Data_Lgth = cn_Fyi_Struct.newFieldInGroup("cn_Fyi_Struct_Cn_Data_Lgth", "CN-DATA-LGTH", FieldType.NUMERIC, 5);
        cn_Fyi_Struct_Cn_Data_Occurs = cn_Fyi_Struct.newFieldInGroup("cn_Fyi_Struct_Cn_Data_Occurs", "CN-DATA-OCCURS", FieldType.NUMERIC, 5);
        cn_Fyi_Struct_Cn_Data = cn_Fyi_Struct.newFieldArrayInGroup("cn_Fyi_Struct_Cn_Data", "CN-DATA", FieldType.STRING, 1, new DbsArrayController(1,1880));
        cn_Fyi_Struct_Cn_DataRedef24 = cn_Fyi_Struct.newGroupInGroup("cn_Fyi_Struct_Cn_DataRedef24", "Redefines", cn_Fyi_Struct_Cn_Data);
        cn_Fyi_Struct_Cn_Fyi = cn_Fyi_Struct_Cn_DataRedef24.newGroupArrayInGroup("cn_Fyi_Struct_Cn_Fyi", "CN-FYI", new DbsArrayController(1,20));
        cn_Fyi_Struct_Cn_Fyi_Msg_Number = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Msg_Number", "CN-FYI-MSG-NUMBER", FieldType.NUMERIC, 
            3);
        cn_Fyi_Struct_Cn_Fyi_Date1_F = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date1_F", "CN-FYI-DATE1-F", FieldType.NUMERIC, 8);
        cn_Fyi_Struct_Cn_Fyi_Date1_FRedef25 = cn_Fyi_Struct_Cn_Fyi.newGroupInGroup("cn_Fyi_Struct_Cn_Fyi_Date1_FRedef25", "Redefines", cn_Fyi_Struct_Cn_Fyi_Date1_F);
        cn_Fyi_Struct_Cn_Fyi_Date1_Mm = cn_Fyi_Struct_Cn_Fyi_Date1_FRedef25.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date1_Mm", "CN-FYI-DATE1-MM", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date1_Dd = cn_Fyi_Struct_Cn_Fyi_Date1_FRedef25.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date1_Dd", "CN-FYI-DATE1-DD", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date1_Yy = cn_Fyi_Struct_Cn_Fyi_Date1_FRedef25.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date1_Yy", "CN-FYI-DATE1-YY", FieldType.NUMERIC, 
            4);
        cn_Fyi_Struct_Cn_Fyi_Date2_F = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date2_F", "CN-FYI-DATE2-F", FieldType.NUMERIC, 8);
        cn_Fyi_Struct_Cn_Fyi_Date2_FRedef26 = cn_Fyi_Struct_Cn_Fyi.newGroupInGroup("cn_Fyi_Struct_Cn_Fyi_Date2_FRedef26", "Redefines", cn_Fyi_Struct_Cn_Fyi_Date2_F);
        cn_Fyi_Struct_Cn_Fyi_Date2_Mm = cn_Fyi_Struct_Cn_Fyi_Date2_FRedef26.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date2_Mm", "CN-FYI-DATE2-MM", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date2_Dd = cn_Fyi_Struct_Cn_Fyi_Date2_FRedef26.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date2_Dd", "CN-FYI-DATE2-DD", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date2_Yy = cn_Fyi_Struct_Cn_Fyi_Date2_FRedef26.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date2_Yy", "CN-FYI-DATE2-YY", FieldType.NUMERIC, 
            4);
        cn_Fyi_Struct_Cn_Fyi_Vartxt = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Vartxt", "CN-FYI-VARTXT", FieldType.STRING, 40);
        cn_Fyi_Struct_Cn_Fyi_Srt_Num = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Srt_Num", "CN-FYI-SRT-NUM", FieldType.NUMERIC, 3);
        cn_Fyi_Struct_Cn_Fyi_Date3_F = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date3_F", "CN-FYI-DATE3-F", FieldType.NUMERIC, 8);
        cn_Fyi_Struct_Cn_Fyi_Date3_FRedef27 = cn_Fyi_Struct_Cn_Fyi.newGroupInGroup("cn_Fyi_Struct_Cn_Fyi_Date3_FRedef27", "Redefines", cn_Fyi_Struct_Cn_Fyi_Date3_F);
        cn_Fyi_Struct_Cn_Fyi_Date3_Mm = cn_Fyi_Struct_Cn_Fyi_Date3_FRedef27.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date3_Mm", "CN-FYI-DATE3-MM", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date3_Dd = cn_Fyi_Struct_Cn_Fyi_Date3_FRedef27.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date3_Dd", "CN-FYI-DATE3-DD", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date3_Yy = cn_Fyi_Struct_Cn_Fyi_Date3_FRedef27.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date3_Yy", "CN-FYI-DATE3-YY", FieldType.NUMERIC, 
            4);
        cn_Fyi_Struct_Cn_Fyi_Date4_F = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date4_F", "CN-FYI-DATE4-F", FieldType.NUMERIC, 8);
        cn_Fyi_Struct_Cn_Fyi_Date4_FRedef28 = cn_Fyi_Struct_Cn_Fyi.newGroupInGroup("cn_Fyi_Struct_Cn_Fyi_Date4_FRedef28", "Redefines", cn_Fyi_Struct_Cn_Fyi_Date4_F);
        cn_Fyi_Struct_Cn_Fyi_Date4_Mm = cn_Fyi_Struct_Cn_Fyi_Date4_FRedef28.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date4_Mm", "CN-FYI-DATE4-MM", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date4_Dd = cn_Fyi_Struct_Cn_Fyi_Date4_FRedef28.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date4_Dd", "CN-FYI-DATE4-DD", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date4_Yy = cn_Fyi_Struct_Cn_Fyi_Date4_FRedef28.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date4_Yy", "CN-FYI-DATE4-YY", FieldType.NUMERIC, 
            4);
        cn_Fyi_Struct_Cn_Fyi_Date5_F = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date5_F", "CN-FYI-DATE5-F", FieldType.NUMERIC, 8);
        cn_Fyi_Struct_Cn_Fyi_Date5_FRedef29 = cn_Fyi_Struct_Cn_Fyi.newGroupInGroup("cn_Fyi_Struct_Cn_Fyi_Date5_FRedef29", "Redefines", cn_Fyi_Struct_Cn_Fyi_Date5_F);
        cn_Fyi_Struct_Cn_Fyi_Date5_Mm = cn_Fyi_Struct_Cn_Fyi_Date5_FRedef29.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date5_Mm", "CN-FYI-DATE5-MM", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date5_Dd = cn_Fyi_Struct_Cn_Fyi_Date5_FRedef29.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date5_Dd", "CN-FYI-DATE5-DD", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date5_Yy = cn_Fyi_Struct_Cn_Fyi_Date5_FRedef29.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date5_Yy", "CN-FYI-DATE5-YY", FieldType.NUMERIC, 
            4);
        cn_Fyi_Struct_Cn_Fyi_Date6_F = cn_Fyi_Struct_Cn_Fyi.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date6_F", "CN-FYI-DATE6-F", FieldType.NUMERIC, 8);
        cn_Fyi_Struct_Cn_Fyi_Date6_FRedef30 = cn_Fyi_Struct_Cn_Fyi.newGroupInGroup("cn_Fyi_Struct_Cn_Fyi_Date6_FRedef30", "Redefines", cn_Fyi_Struct_Cn_Fyi_Date6_F);
        cn_Fyi_Struct_Cn_Fyi_Date6_Mm = cn_Fyi_Struct_Cn_Fyi_Date6_FRedef30.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date6_Mm", "CN-FYI-DATE6-MM", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date6_Dd = cn_Fyi_Struct_Cn_Fyi_Date6_FRedef30.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date6_Dd", "CN-FYI-DATE6-DD", FieldType.NUMERIC, 
            2);
        cn_Fyi_Struct_Cn_Fyi_Date6_Yy = cn_Fyi_Struct_Cn_Fyi_Date6_FRedef30.newFieldInGroup("cn_Fyi_Struct_Cn_Fyi_Date6_Yy", "CN-FYI-DATE6-YY", FieldType.NUMERIC, 
            4);

        this.setRecordName("LdaPstl6165");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Struct_Sort_Key_Lgth.setInitialValue(55);
        cn_Contract_Struct_Cn_Record_Id.setInitialValue("19");
        cn_Contract_Struct_Cn_Data_Lgth.setInitialValue(117);
        cn_Contract_Struct_Cn_Data_Occurs.setInitialValue(1);
        cn_Frm_Rec_Struct_Cn_Record_Id.setInitialValue("20");
        cn_Frm_Rec_Struct_Cn_Data_Lgth.setInitialValue(17);
        cn_Frm_Rec_Struct_Cn_Data_Occurs.setInitialValue(1);
        cn_To_Acct_Struct_Cn_Record_Id.setInitialValue("21");
        cn_To_Acct_Struct_Cn_Data_Lgth.setInitialValue(17);
        cn_To_Acct_Struct_Cn_Data_Occurs.setInitialValue(1);
        cn_Etf_Struct_Cn_Record_Id.setInitialValue("24");
        cn_Etf_Struct_Cn_Data_Lgth.setInitialValue(264);
        cn_Etf_Struct_Cn_Data_Occurs.setInitialValue(1);
        cn_Ett_Struct_Cn_Record_Id.setInitialValue("25");
        cn_Ett_Struct_Cn_Data_Lgth.setInitialValue(264);
        cn_Ett_Struct_Cn_Data_Occurs.setInitialValue(1);
        cn_Anv_Struct_Cn_Record_Id.setInitialValue("26");
        cn_Anv_Struct_Cn_Data_Lgth.setInitialValue(241);
        cn_Anv_Struct_Cn_Data_Occurs.setInitialValue(1);
        cn_Fyi_Struct_Cn_Record_Id.setInitialValue("27");
        cn_Fyi_Struct_Cn_Data_Lgth.setInitialValue(94);
        cn_Fyi_Struct_Cn_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaPstl6165() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
