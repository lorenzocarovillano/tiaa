/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:38 PM
**        * FROM NATURAL PDA     : IAAA090F
************************************************************
**        * FILE NAME            : PdaIaaa090f.java
**        * CLASS NAME           : PdaIaaa090f
**        * INSTANCE NAME        : PdaIaaa090f
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIaaa090f extends PdaBase
{
    // Properties
    private DbsGroup iaaa090f;
    private DbsField iaaa090f_Cntrl_Cde;
    private DbsField iaaa090f_Cntrl_Frst_Trans_Dte;
    private DbsField iaaa090f_Cntrl_Actvty_Cde;
    private DbsField iaaa090f_Cntrl_Tiaa_Payees;
    private DbsField iaaa090f_Cntrl_Per_Pay_Amt;
    private DbsField iaaa090f_Cntrl_Per_Div_Amt;
    private DbsField iaaa090f_Cntrl_Units_Cnt;
    private DbsField iaaa090f_Cntrl_Final_Pay_Amt;
    private DbsField iaaa090f_Cntrl_Final_Div_Amt;
    private DbsField iaaa090f_Cntrl_Todays_Dte;
    private DbsGroup iaaa090f_Cntrl_Fund_Cnts;
    private DbsField iaaa090f_Cntrl_Fund_Cde;
    private DbsField iaaa090f_Cntrl_Fund_Payees;
    private DbsField iaaa090f_Cntrl_Units;
    private DbsField iaaa090f_Cntrl_Amt;
    private DbsField iaaa090f_Cntrl_Ddctn_Cnt;
    private DbsField iaaa090f_Cntrl_Ddctn_Amt;
    private DbsField iaaa090f_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaaa090f_Cntrl_Actve_Cref_Pys;
    private DbsField iaaa090f_Cntrl_Inactve_Payees;

    public DbsGroup getIaaa090f() { return iaaa090f; }

    public DbsField getIaaa090f_Cntrl_Cde() { return iaaa090f_Cntrl_Cde; }

    public DbsField getIaaa090f_Cntrl_Frst_Trans_Dte() { return iaaa090f_Cntrl_Frst_Trans_Dte; }

    public DbsField getIaaa090f_Cntrl_Actvty_Cde() { return iaaa090f_Cntrl_Actvty_Cde; }

    public DbsField getIaaa090f_Cntrl_Tiaa_Payees() { return iaaa090f_Cntrl_Tiaa_Payees; }

    public DbsField getIaaa090f_Cntrl_Per_Pay_Amt() { return iaaa090f_Cntrl_Per_Pay_Amt; }

    public DbsField getIaaa090f_Cntrl_Per_Div_Amt() { return iaaa090f_Cntrl_Per_Div_Amt; }

    public DbsField getIaaa090f_Cntrl_Units_Cnt() { return iaaa090f_Cntrl_Units_Cnt; }

    public DbsField getIaaa090f_Cntrl_Final_Pay_Amt() { return iaaa090f_Cntrl_Final_Pay_Amt; }

    public DbsField getIaaa090f_Cntrl_Final_Div_Amt() { return iaaa090f_Cntrl_Final_Div_Amt; }

    public DbsField getIaaa090f_Cntrl_Todays_Dte() { return iaaa090f_Cntrl_Todays_Dte; }

    public DbsGroup getIaaa090f_Cntrl_Fund_Cnts() { return iaaa090f_Cntrl_Fund_Cnts; }

    public DbsField getIaaa090f_Cntrl_Fund_Cde() { return iaaa090f_Cntrl_Fund_Cde; }

    public DbsField getIaaa090f_Cntrl_Fund_Payees() { return iaaa090f_Cntrl_Fund_Payees; }

    public DbsField getIaaa090f_Cntrl_Units() { return iaaa090f_Cntrl_Units; }

    public DbsField getIaaa090f_Cntrl_Amt() { return iaaa090f_Cntrl_Amt; }

    public DbsField getIaaa090f_Cntrl_Ddctn_Cnt() { return iaaa090f_Cntrl_Ddctn_Cnt; }

    public DbsField getIaaa090f_Cntrl_Ddctn_Amt() { return iaaa090f_Cntrl_Ddctn_Amt; }

    public DbsField getIaaa090f_Cntrl_Actve_Tiaa_Pys() { return iaaa090f_Cntrl_Actve_Tiaa_Pys; }

    public DbsField getIaaa090f_Cntrl_Actve_Cref_Pys() { return iaaa090f_Cntrl_Actve_Cref_Pys; }

    public DbsField getIaaa090f_Cntrl_Inactve_Payees() { return iaaa090f_Cntrl_Inactve_Payees; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaaa090f = dbsRecord.newGroupInRecord("iaaa090f", "IAAA090F");
        iaaa090f.setParameterOption(ParameterOption.ByReference);
        iaaa090f_Cntrl_Cde = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2);
        iaaa090f_Cntrl_Frst_Trans_Dte = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", FieldType.TIME);
        iaaa090f_Cntrl_Actvty_Cde = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 1);
        iaaa090f_Cntrl_Tiaa_Payees = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Tiaa_Payees", "CNTRL-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        iaaa090f_Cntrl_Per_Pay_Amt = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090f_Cntrl_Per_Div_Amt = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090f_Cntrl_Units_Cnt = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 13,3);
        iaaa090f_Cntrl_Final_Pay_Amt = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090f_Cntrl_Final_Div_Amt = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090f_Cntrl_Todays_Dte = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE);
        iaaa090f_Cntrl_Fund_Cnts = iaaa090f.newGroupArrayInGroup("iaaa090f_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", new DbsArrayController(1,80));
        iaaa090f_Cntrl_Fund_Cde = iaaa090f_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090f_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 3);
        iaaa090f_Cntrl_Fund_Payees = iaaa090f_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090f_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", FieldType.PACKED_DECIMAL, 
            9);
        iaaa090f_Cntrl_Units = iaaa090f_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090f_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 13,3);
        iaaa090f_Cntrl_Amt = iaaa090f_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090f_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090f_Cntrl_Ddctn_Cnt = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 9);
        iaaa090f_Cntrl_Ddctn_Amt = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090f_Cntrl_Actve_Tiaa_Pys = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", FieldType.PACKED_DECIMAL, 9);
        iaaa090f_Cntrl_Actve_Cref_Pys = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", FieldType.PACKED_DECIMAL, 9);
        iaaa090f_Cntrl_Inactve_Payees = iaaa090f.newFieldInGroup("iaaa090f_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", FieldType.PACKED_DECIMAL, 9);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIaaa090f(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

