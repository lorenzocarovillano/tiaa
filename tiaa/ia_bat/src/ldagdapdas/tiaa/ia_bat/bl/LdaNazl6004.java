/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:07:58 PM
**        * FROM NATURAL LDA     : NAZL6004
************************************************************
**        * FILE NAME            : LdaNazl6004.java
**        * CLASS NAME           : LdaNazl6004
**        * INSTANCE NAME        : LdaNazl6004
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaNazl6004 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Nazl6004;
    private DbsField pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key;
    private DbsField pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key_Old;
    private DbsField pnd_Nazl6004_Pnd_Cis_Requestor;
    private DbsField pnd_Nazl6004_Pnd_Cis_Frst_Nme;
    private DbsField pnd_Nazl6004_Pnd_Cis_Mid_Nme;
    private DbsField pnd_Nazl6004_Pnd_Cis_Lst_Nme;
    private DbsField pnd_Nazl6004_Pnd_Cis_Prfx;
    private DbsField pnd_Nazl6004_Pnd_Cis_Sufx;
    private DbsField pnd_Nazl6004_Pnd_Cis_Sex_Cde;
    private DbsField pnd_Nazl6004_Pnd_Cis_Ownership_Cde;
    private DbsField pnd_Nazl6004_Pnd_Cis_Pin_Number;
    private DbsField pnd_Nazl6004_Pnd_Cis_Ssn;
    private DbsField pnd_Nazl6004_Pnd_Cis_Dob;
    private DbsField pnd_Nazl6004_Pnd_Cis_Tiaa_No;
    private DbsField pnd_Nazl6004_Pnd_Cis_Cref_No;
    private DbsField pnd_Nazl6004_Pnd_Cis_Rqst_Log_Dte_Tme;
    private DbsField pnd_Nazl6004_Pnd_Cis_Product_Cde;
    private DbsField pnd_Nazl6004_Pnd_Cis_Surv_Retr_Ind;
    private DbsField pnd_Nazl6004_Pnd_Cis_Cntrct_Type;
    private DbsField pnd_Nazl6004_Pnd_Cis_Assign_Issue_Cntrct_Ind;
    private DbsField pnd_Nazl6004_Pnd_Cis_Mit_Function_Code;
    private DbsField pnd_Nazl6004_Pnd_Cis_Org_Issue_St;
    private DbsField pnd_Nazl6004_Pnd_Cis_Residence_Issue_St;
    private DbsField pnd_Nazl6004_Pnd_Cis_Issue_Dt;
    private DbsField pnd_Nazl6004_Pnd_Cis_Cntrct_Apprvl_Ind;
    private DbsField pnd_Nazl6004_Pnd_Cis_Scnd_Site_Verification;
    private DbsField pnd_Nazl6004_Pnd_Cis_Msg_Text;
    private DbsField pnd_Nazl6004_Pnd_Cis_Msg_Number;
    private DbsField pnd_Nazl6004_Pnd_Cis_Functions;
    private DbsGroup pnd_Nazl6004_Pnd_Cis_FunctionsRedef1;
    private DbsField pnd_Nazl6004_Pnd_Cis_Function;
    private DbsField pnd_Nazl6004_Pnd_Cis_Misc;

    public DbsGroup getPnd_Nazl6004() { return pnd_Nazl6004; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key() { return pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key_Old() { return pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key_Old; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Requestor() { return pnd_Nazl6004_Pnd_Cis_Requestor; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Frst_Nme() { return pnd_Nazl6004_Pnd_Cis_Frst_Nme; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Mid_Nme() { return pnd_Nazl6004_Pnd_Cis_Mid_Nme; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Lst_Nme() { return pnd_Nazl6004_Pnd_Cis_Lst_Nme; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Prfx() { return pnd_Nazl6004_Pnd_Cis_Prfx; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Sufx() { return pnd_Nazl6004_Pnd_Cis_Sufx; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Sex_Cde() { return pnd_Nazl6004_Pnd_Cis_Sex_Cde; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Ownership_Cde() { return pnd_Nazl6004_Pnd_Cis_Ownership_Cde; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Pin_Number() { return pnd_Nazl6004_Pnd_Cis_Pin_Number; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Ssn() { return pnd_Nazl6004_Pnd_Cis_Ssn; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Dob() { return pnd_Nazl6004_Pnd_Cis_Dob; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Tiaa_No() { return pnd_Nazl6004_Pnd_Cis_Tiaa_No; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Cref_No() { return pnd_Nazl6004_Pnd_Cis_Cref_No; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Rqst_Log_Dte_Tme() { return pnd_Nazl6004_Pnd_Cis_Rqst_Log_Dte_Tme; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Product_Cde() { return pnd_Nazl6004_Pnd_Cis_Product_Cde; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Surv_Retr_Ind() { return pnd_Nazl6004_Pnd_Cis_Surv_Retr_Ind; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Cntrct_Type() { return pnd_Nazl6004_Pnd_Cis_Cntrct_Type; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Assign_Issue_Cntrct_Ind() { return pnd_Nazl6004_Pnd_Cis_Assign_Issue_Cntrct_Ind; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Mit_Function_Code() { return pnd_Nazl6004_Pnd_Cis_Mit_Function_Code; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Org_Issue_St() { return pnd_Nazl6004_Pnd_Cis_Org_Issue_St; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Residence_Issue_St() { return pnd_Nazl6004_Pnd_Cis_Residence_Issue_St; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Issue_Dt() { return pnd_Nazl6004_Pnd_Cis_Issue_Dt; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Cntrct_Apprvl_Ind() { return pnd_Nazl6004_Pnd_Cis_Cntrct_Apprvl_Ind; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Scnd_Site_Verification() { return pnd_Nazl6004_Pnd_Cis_Scnd_Site_Verification; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Msg_Text() { return pnd_Nazl6004_Pnd_Cis_Msg_Text; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Msg_Number() { return pnd_Nazl6004_Pnd_Cis_Msg_Number; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Functions() { return pnd_Nazl6004_Pnd_Cis_Functions; }

    public DbsGroup getPnd_Nazl6004_Pnd_Cis_FunctionsRedef1() { return pnd_Nazl6004_Pnd_Cis_FunctionsRedef1; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Function() { return pnd_Nazl6004_Pnd_Cis_Function; }

    public DbsField getPnd_Nazl6004_Pnd_Cis_Misc() { return pnd_Nazl6004_Pnd_Cis_Misc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Nazl6004 = newGroupInRecord("pnd_Nazl6004", "#NAZL6004");
        pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key", "#CIS-ENTRY-DATE-TIME-KEY", 
            FieldType.STRING, 35);
        pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key_Old = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key_Old", "#CIS-ENTRY-DATE-TIME-KEY-OLD", 
            FieldType.STRING, 35);
        pnd_Nazl6004_Pnd_Cis_Requestor = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Requestor", "#CIS-REQUESTOR", FieldType.STRING, 8);
        pnd_Nazl6004_Pnd_Cis_Frst_Nme = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Frst_Nme", "#CIS-FRST-NME", FieldType.STRING, 30);
        pnd_Nazl6004_Pnd_Cis_Mid_Nme = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Mid_Nme", "#CIS-MID-NME", FieldType.STRING, 30);
        pnd_Nazl6004_Pnd_Cis_Lst_Nme = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Lst_Nme", "#CIS-LST-NME", FieldType.STRING, 30);
        pnd_Nazl6004_Pnd_Cis_Prfx = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Prfx", "#CIS-PRFX", FieldType.STRING, 8);
        pnd_Nazl6004_Pnd_Cis_Sufx = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Sufx", "#CIS-SUFX", FieldType.STRING, 8);
        pnd_Nazl6004_Pnd_Cis_Sex_Cde = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Sex_Cde", "#CIS-SEX-CDE", FieldType.STRING, 1);
        pnd_Nazl6004_Pnd_Cis_Ownership_Cde = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Ownership_Cde", "#CIS-OWNERSHIP-CDE", FieldType.STRING, 
            1);
        pnd_Nazl6004_Pnd_Cis_Pin_Number = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Pin_Number", "#CIS-PIN-NUMBER", FieldType.NUMERIC, 12);
        pnd_Nazl6004_Pnd_Cis_Ssn = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Ssn", "#CIS-SSN", FieldType.NUMERIC, 9);
        pnd_Nazl6004_Pnd_Cis_Dob = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Dob", "#CIS-DOB", FieldType.NUMERIC, 8);
        pnd_Nazl6004_Pnd_Cis_Tiaa_No = pnd_Nazl6004.newFieldArrayInGroup("pnd_Nazl6004_Pnd_Cis_Tiaa_No", "#CIS-TIAA-NO", FieldType.STRING, 10, new DbsArrayController(1,
            20));
        pnd_Nazl6004_Pnd_Cis_Cref_No = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Cref_No", "#CIS-CREF-NO", FieldType.STRING, 10);
        pnd_Nazl6004_Pnd_Cis_Rqst_Log_Dte_Tme = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Rqst_Log_Dte_Tme", "#CIS-RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Nazl6004_Pnd_Cis_Product_Cde = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Product_Cde", "#CIS-PRODUCT-CDE", FieldType.STRING, 10);
        pnd_Nazl6004_Pnd_Cis_Surv_Retr_Ind = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Surv_Retr_Ind", "#CIS-SURV-RETR-IND", FieldType.STRING, 
            1);
        pnd_Nazl6004_Pnd_Cis_Cntrct_Type = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Cntrct_Type", "#CIS-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Nazl6004_Pnd_Cis_Assign_Issue_Cntrct_Ind = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Assign_Issue_Cntrct_Ind", "#CIS-ASSIGN-ISSUE-CNTRCT-IND", 
            FieldType.STRING, 1);
        pnd_Nazl6004_Pnd_Cis_Mit_Function_Code = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Mit_Function_Code", "#CIS-MIT-FUNCTION-CODE", FieldType.STRING, 
            2);
        pnd_Nazl6004_Pnd_Cis_Org_Issue_St = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Org_Issue_St", "#CIS-ORG-ISSUE-ST", FieldType.STRING, 2);
        pnd_Nazl6004_Pnd_Cis_Residence_Issue_St = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Residence_Issue_St", "#CIS-RESIDENCE-ISSUE-ST", FieldType.STRING, 
            2);
        pnd_Nazl6004_Pnd_Cis_Issue_Dt = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Issue_Dt", "#CIS-ISSUE-DT", FieldType.NUMERIC, 8);
        pnd_Nazl6004_Pnd_Cis_Cntrct_Apprvl_Ind = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Cntrct_Apprvl_Ind", "#CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        pnd_Nazl6004_Pnd_Cis_Scnd_Site_Verification = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Scnd_Site_Verification", "#CIS-SCND-SITE-VERIFICATION", 
            FieldType.STRING, 1);
        pnd_Nazl6004_Pnd_Cis_Msg_Text = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Msg_Text", "#CIS-MSG-TEXT", FieldType.STRING, 72);
        pnd_Nazl6004_Pnd_Cis_Msg_Number = pnd_Nazl6004.newFieldArrayInGroup("pnd_Nazl6004_Pnd_Cis_Msg_Number", "#CIS-MSG-NUMBER", FieldType.STRING, 4, 
            new DbsArrayController(1,20));
        pnd_Nazl6004_Pnd_Cis_Functions = pnd_Nazl6004.newFieldArrayInGroup("pnd_Nazl6004_Pnd_Cis_Functions", "#CIS-FUNCTIONS", FieldType.STRING, 2, new 
            DbsArrayController(1,20));
        pnd_Nazl6004_Pnd_Cis_FunctionsRedef1 = pnd_Nazl6004.newGroupInGroup("pnd_Nazl6004_Pnd_Cis_FunctionsRedef1", "Redefines", pnd_Nazl6004_Pnd_Cis_Functions);
        pnd_Nazl6004_Pnd_Cis_Function = pnd_Nazl6004_Pnd_Cis_FunctionsRedef1.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Function", "#CIS-FUNCTION", FieldType.STRING, 
            40);
        pnd_Nazl6004_Pnd_Cis_Misc = pnd_Nazl6004.newFieldInGroup("pnd_Nazl6004_Pnd_Cis_Misc", "#CIS-MISC", FieldType.STRING, 253);

        this.setRecordName("LdaNazl6004");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaNazl6004() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
