/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:13 PM
**        * FROM NATURAL LDA     : IAAL584W
************************************************************
**        * FILE NAME            : LdaIaal584w.java
**        * CLASS NAME           : LdaIaal584w
**        * INSTANCE NAME        : LdaIaal584w
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal584w extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct_View;
    private DbsField iaa_Cntrct_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_View_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Issue_Dte;
    private DataAccessProgramView vw_new_Tiaa_Rates;
    private DbsField new_Tiaa_Rates_Tiaa_Tot_Per_Amt;
    private DbsField new_Tiaa_Rates_Tiaa_Tot_Div_Amt;
    private DbsField new_Tiaa_Rates_Tiaa_Cntrct_Fund_Key;
    private DbsField pnd_Ovrpymnt_Time_Selects;
    private DbsField pnd_Ovrpymnt_Reads;
    private DbsField pnd_A1;
    private DbsField pnd_A6;
    private DbsField pnd_A7;
    private DbsField pnd_C1;
    private DbsField pnd_C6;
    private DbsField pnd_C7;
    private DbsField pnd_T1;
    private DbsField pnd_T6;
    private DbsField pnd_T7;
    private DbsField pnd_A;
    private DbsGroup pnd_ARedef1;
    private DbsField pnd_A_Pnd_A_Alph;
    private DbsField pnd_B;
    private DbsField pnd_C;
    private DbsGroup pnd_CRedef2;
    private DbsField pnd_C_Pnd_C_Alph;
    private DbsField pnd_Contract_Type_Field;
    private DbsField pnd_W_Contract_Issue_Dte;
    private DbsField pnd_W_Contract;
    private DbsGroup pnd_W_ContractRedef3;
    private DbsField pnd_W_Contract_Pnd_W_Contract_1_7;
    private DbsField pnd_W_Contract_Pnd_W_Contract_8;
    private DbsGroup pnd_W_ContractRedef4;
    private DbsField pnd_W_Contract_Pnd_W_Cntrct_1_2;
    private DbsGroup pnd_W_Contract_Pnd_W_Cntrct_1_2Redef5;
    private DbsField pnd_W_Contract_Pnd_W_Cntrct_1;
    private DbsField pnd_W_Contract_Pnd_W_Cntrct_2;
    private DbsField pnd_W_Contract_Pnd_W_Cntrct_Nbr;
    private DbsField pnd_W_Pymnt_Mode;
    private DbsGroup pnd_W_Pymnt_ModeRedef6;
    private DbsField pnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A;
    private DbsGroup pnd_W_Pymnt_ModeRedef7;
    private DbsField pnd_W_Pymnt_Mode_Pnd_Contract_Mode_1;
    private DbsField pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3;
    private DbsGroup pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3Redef8;
    private DbsField pnd_W_Pymnt_Mode_Pnd_Filler_2;
    private DbsField pnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N;
    private DbsField pnd_W_Payee_Cde;
    private DbsGroup pnd_W_Payee_CdeRedef9;
    private DbsField pnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num;
    private DbsField pnd_Option_Cde;
    private DbsField pnd_W_Option_Cde_Desc;
    private DbsField pnd_Num_Ovrpy_Yrs;
    private DbsField pnd_Lowest_Year_Yyyymm;
    private DbsGroup pnd_Lowest_Year_YyyymmRedef10;
    private DbsField pnd_Lowest_Year_Yyyymm_Pnd_Lowest_Year_Yyyymm_A;
    private DbsField pnd_T;
    private DbsField pnd_P;
    private DbsField pnd_H;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_R;
    private DbsField pnd_X;
    private DbsField pnd_Ovrpymnt_Years_Count;
    private DbsField pnd_Cref_Sub;
    private DbsField pnd_Page_Ctr_1;
    private DbsField pnd_Page_Ctr_2;
    private DbsField pnd_Page_Ctr_3;
    private DbsField pnd_Page_Ctr_4;
    private DbsField pnd_Page_Ctr_5;
    private DbsField pnd_Page_Ctr_6;
    private DbsField pnd_Page_Ctr_7;
    private DbsField pnd_Page_Ctr_8;
    private DbsField pnd_Page_Ctr_9;
    private DbsField pnd_Found;
    private DbsField pnd_Cnt_Frq;
    private DbsField pnd_Pay_Install;
    private DbsField pnd_Gen_Sub;
    private DbsField pnd_W_Pay_Date;
    private DbsField pnd_W_Rate_Gross;
    private DbsField pnd_W_Rate_Ovrpy;
    private DbsField pnd_W_Rate_Guar;
    private DbsField pnd_W_Rate_Divd;
    private DbsField pnd_Rate_Guar_Ovp;
    private DbsField pnd_Rate_Divd_Ovp;
    private DbsField pnd_Tot_Guar_Divd;
    private DbsField pnd_Calc_Hold;
    private DbsField pnd_W_Rate_Guar_Ovp;
    private DbsField pnd_W_Rate_Divd_Ovp;
    private DbsField pnd_Ovrpy_Tot;
    private DbsField pnd_Cont_Stat_Fund;
    private DbsGroup pnd_Cont_Stat_FundRedef11;
    private DbsField pnd_Cont_Stat_Fund_Pnd_Cont;
    private DbsField pnd_Cont_Stat_Fund_Pnd_Stat;
    private DbsField pnd_Cont_Stat_Fund_Pnd_Fund;
    private DbsField pnd_Tiaa_Rate_Guar;
    private DbsField pnd_Tiaa_Rate_Div;
    private DbsField pnd_Tiaa_Rate_Date;
    private DbsField pnd_Old_Tiaa_Key_S;
    private DbsGroup pnd_Old_Tiaa_Key_SRedef12;
    private DbsField pnd_Old_Tiaa_Key_S_Pnd_Cont_S;
    private DbsField pnd_Old_Tiaa_Key_S_Pnd_Stat_S;
    private DbsField pnd_Old_Tiaa_Key_S_Pnd_Fund_Invrse_Lpd_S;
    private DbsField pnd_Old_Tiaa_Key_S_Pnd_Comp_Fund_Cde_S;
    private DbsField pnd_Old_Tiaa_Key_E;
    private DbsGroup pnd_Old_Tiaa_Key_ERedef13;
    private DbsField pnd_Old_Tiaa_Key_E_Pnd_Cont_E;
    private DbsField pnd_Old_Tiaa_Key_E_Pnd_Stat_E;
    private DbsField pnd_Old_Tiaa_Key_E_Pnd_Fund_Invrse_Lpd_E;
    private DbsField pnd_Old_Tiaa_Key_E_Pnd_Comp_Fund_Cde_E;
    private DbsField pnd_Fund_Lst_Pd_Dte_A;
    private DbsGroup pnd_Fund_Lst_Pd_Dte_ARedef14;
    private DbsField pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte;
    private DbsGroup pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15;
    private DbsField pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm;
    private DbsGroup pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16;
    private DbsField pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyy;
    private DbsField pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm;
    private DbsGroup pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_MmRedef17;
    private DbsField pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm_Alpha;
    private DbsField pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd;
    private DbsGroup pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_DdRedef18;
    private DbsField pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd_Alpha;
    private DbsField pnd_Enter_Old_Rates_File;
    private DbsField pnd_More_Old_Rates;
    private DbsField pnd_Year_Of_Death;
    private DbsField pnd_Dt_Yyyy;
    private DbsField pnd_Dt_Mm;
    private DbsField pnd_Pay_Date_Yr;
    private DbsField pnd_Ovrpymnt_Begin_Fiscal_Year;
    private DbsGroup pnd_Ovrpymnt_Begin_Fiscal_YearRedef19;
    private DbsField pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Cc;
    private DbsField pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Yy;
    private DbsField pnd_Ovrpymnt_End_Fiscal_Year;
    private DbsGroup pnd_Ovrpymnt_End_Fiscal_YearRedef20;
    private DbsField pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Cc;
    private DbsField pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Yy;
    private DbsField pnd_Ovrpymnt_Begin_Fiscal_Mo;
    private DbsField pnd_Ovrpymnt_End_Fiscal_Mo;
    private DbsField pnd_Ovrpymnt_Nbr_Per_Year;
    private DbsField pnd_Ovrpymnt_Dcdnt_Per_Amt;
    private DbsField pnd_Ovrpymnt_Ovr_Per_Amt;
    private DbsField pnd_Py_Dt;
    private DbsGroup pnd_Py_DtRedef21;
    private DbsField pnd_Py_Dt_Pnd_Py_Dt_Mm;
    private DbsField pnd_Py_Dt_Pnd_Filler_3;
    private DbsField pnd_Py_Dt_Pnd_Py_Dt_Dd;
    private DbsField pnd_Py_Dt_Pnd_Filler_4;
    private DbsField pnd_Py_Dt_Pnd_Py_Dt_Yyyy;
    private DbsField pnd_Py_Dt_Yyyymm_A;
    private DbsGroup pnd_Py_Dt_Yyyymm_ARedef22;
    private DbsField pnd_Py_Dt_Yyyymm_A_Pnd_Py_Dt_Yyyymm;
    private DbsField pnd_T_Record;
    private DbsGroup pnd_T_RecordRedef23;
    private DbsField pnd_T_Record_Pnd_T_Contract;
    private DbsField pnd_T_Record_Pnd_T_Filler_1;
    private DbsField pnd_T_Record_Pnd_T_Payee_Cde;
    private DbsField pnd_T_Record_Pnd_T_Filler_2;
    private DbsField pnd_T_Record_Pnd_T_Option_Cde_Desc;
    private DbsField pnd_T_Record_Pnd_T_Filler_3;
    private DbsField pnd_T_Record_Pnd_T_Pymnt_Mode;
    private DbsField pnd_T_Record_Pnd_T_Filler_4;
    private DbsField pnd_T_Record_Pnd_T_Pay_Date;
    private DbsField pnd_T_Record_Pnd_T_Filler_5;
    private DbsField pnd_T_Record_Pnd_T_Rate_Guar;
    private DbsField pnd_T_Record_Pnd_T_Filler_6;
    private DbsField pnd_T_Record_Pnd_T_Rate_Divd;
    private DbsField pnd_T_Record_Pnd_T_Filler_7;
    private DbsField pnd_T_Record_Pnd_T_Rate_Guar_Ovp;
    private DbsField pnd_T_Record_Pnd_T_Filler_8;
    private DbsField pnd_T_Record_Pnd_T_Rate_Divd_Ovp;
    private DbsField pnd_T_Record_Pnd_T_Filler_9;
    private DbsField pnd_T_Record_Pnd_T_Rate_Ovrpy;
    private DbsField pnd_C_Rate_Guar;
    private DbsField pnd_C_Rate_Divd;
    private DbsField pnd_C_Rate_Guar_Ovp;
    private DbsField pnd_C_Rate_Divd_Ovp;
    private DbsField pnd_C_Rate_Ovrpymnt;
    private DbsField pnd_C_Record;
    private DbsGroup pnd_C_RecordRedef24;
    private DbsField pnd_C_Record_Pnd_C_Contract;
    private DbsField pnd_C_Record_Pnd_C_Filler_1;
    private DbsField pnd_C_Record_Pnd_C_Payee_Cde;
    private DbsField pnd_C_Record_Pnd_C_Filler_2;
    private DbsField pnd_C_Record_Pnd_C_Option_Cde_Desc;
    private DbsField pnd_C_Record_Pnd_C_Filler_3;
    private DbsField pnd_C_Record_Pnd_C_Pymnt_Mode;
    private DbsField pnd_C_Record_Pnd_C_Filler_4;
    private DbsField pnd_C_Record_Pnd_C_Pay_Date;
    private DbsField pnd_C_Record_Pnd_C_Filler_5;
    private DbsField pnd_C_Record_Pnd_C_Rate_Gross;
    private DbsField pnd_C_Record_Pnd_C_Filler_6;
    private DbsField pnd_C_Record_Pnd_C_Rate_Ovrpy;
    private DbsField pnd_Cnt_Rate_Gross;
    private DbsField pnd_Cnt_Rate_Ovrpy;
    private DbsField pnd_W_Parm_Date;
    private DbsField pnd_Grand_Tot_Ovrpy_Tiaa;
    private DbsField pnd_Grand_Tot_Ovrpy_Stock;
    private DbsField pnd_Grand_Tot_Ovrpy_Money_Market;
    private DbsField pnd_Grand_Tot_Ovrpy_Social_Choice;
    private DbsField pnd_Grand_Tot_Ovrpy_Global_Equity;
    private DbsField pnd_Grand_Tot_Ovrpy_Growth;
    private DbsField pnd_Grand_Tot_Ovrpy_Equity_Index;
    private DbsField pnd_Grand_Tot_Ovrpy_Real_Estate;
    private DbsField pnd_Grand_Tot_Ovrpy_Bond;
    private DbsField pnd_Grand_Rate_Guar_Ovp;
    private DbsField pnd_Grand_Rate_Divd_Ovp;
    private DbsField pnd_Num_Of_Tiaa;
    private DbsField pnd_Num_Of_Stock;
    private DbsField pnd_Num_Of_Money_Market;
    private DbsField pnd_Num_Of_Social_Choice;
    private DbsField pnd_Num_Of_Global_Equity;
    private DbsField pnd_Num_Of_Growth;
    private DbsField pnd_Num_Of_Equity_Index;
    private DbsField pnd_Num_Of_Real_Estate;
    private DbsField pnd_Num_Of_Bond;
    private DbsField pnd_W_Last_Pay_Date;
    private DbsGroup pnd_W_Last_Pay_DateRedef25;
    private DbsField pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Yyyy;
    private DbsField pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm;
    private DbsGroup pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_MmRedef26;
    private DbsField pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A;
    private DbsField pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Dd;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef27;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Date_NRedef28;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_AlphRedef29;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd;
    private DbsField pnd_Fl_Time_Hhiiss_Alph;
    private DbsGroup pnd_Fl_Time_Hhiiss_AlphRedef31;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num;
    private DbsGroup pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef33;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef35;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsGroup pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;
    private DataAccessProgramView vw_iaa_Old_Tiaa_Rates_View;
    private DbsField iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Cmpny_Fund_CdeRedef37;
    private DbsField iaa_Old_Tiaa_Rates_View_Cmpny_Cde;
    private DbsField iaa_Old_Tiaa_Rates_View_Fund_Cde;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Py_Dte_Key;
    private DbsField pnd_Temp_Occur;
    private DbsField pnd_D;
    private DbsField pnd_E;
    private DbsField pnd_Temp_Occur_2;
    private DbsField pnd_Temp_Occur_3;
    private DbsField pnd_Q;
    private DbsField pnd_X1;
    private DbsField pnd_M1;
    private DbsField pnd_Ovp_Cnt;
    private DbsField pnd_Earlier_Death;

    public DataAccessProgramView getVw_iaa_Cntrct_View() { return vw_iaa_Cntrct_View; }

    public DbsField getIaa_Cntrct_View_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_View_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_View_Cntrct_Optn_Cde() { return iaa_Cntrct_View_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_View_Cntrct_Issue_Dte() { return iaa_Cntrct_View_Cntrct_Issue_Dte; }

    public DataAccessProgramView getVw_new_Tiaa_Rates() { return vw_new_Tiaa_Rates; }

    public DbsField getNew_Tiaa_Rates_Tiaa_Tot_Per_Amt() { return new_Tiaa_Rates_Tiaa_Tot_Per_Amt; }

    public DbsField getNew_Tiaa_Rates_Tiaa_Tot_Div_Amt() { return new_Tiaa_Rates_Tiaa_Tot_Div_Amt; }

    public DbsField getNew_Tiaa_Rates_Tiaa_Cntrct_Fund_Key() { return new_Tiaa_Rates_Tiaa_Cntrct_Fund_Key; }

    public DbsField getPnd_Ovrpymnt_Time_Selects() { return pnd_Ovrpymnt_Time_Selects; }

    public DbsField getPnd_Ovrpymnt_Reads() { return pnd_Ovrpymnt_Reads; }

    public DbsField getPnd_A1() { return pnd_A1; }

    public DbsField getPnd_A6() { return pnd_A6; }

    public DbsField getPnd_A7() { return pnd_A7; }

    public DbsField getPnd_C1() { return pnd_C1; }

    public DbsField getPnd_C6() { return pnd_C6; }

    public DbsField getPnd_C7() { return pnd_C7; }

    public DbsField getPnd_T1() { return pnd_T1; }

    public DbsField getPnd_T6() { return pnd_T6; }

    public DbsField getPnd_T7() { return pnd_T7; }

    public DbsField getPnd_A() { return pnd_A; }

    public DbsGroup getPnd_ARedef1() { return pnd_ARedef1; }

    public DbsField getPnd_A_Pnd_A_Alph() { return pnd_A_Pnd_A_Alph; }

    public DbsField getPnd_B() { return pnd_B; }

    public DbsField getPnd_C() { return pnd_C; }

    public DbsGroup getPnd_CRedef2() { return pnd_CRedef2; }

    public DbsField getPnd_C_Pnd_C_Alph() { return pnd_C_Pnd_C_Alph; }

    public DbsField getPnd_Contract_Type_Field() { return pnd_Contract_Type_Field; }

    public DbsField getPnd_W_Contract_Issue_Dte() { return pnd_W_Contract_Issue_Dte; }

    public DbsField getPnd_W_Contract() { return pnd_W_Contract; }

    public DbsGroup getPnd_W_ContractRedef3() { return pnd_W_ContractRedef3; }

    public DbsField getPnd_W_Contract_Pnd_W_Contract_1_7() { return pnd_W_Contract_Pnd_W_Contract_1_7; }

    public DbsField getPnd_W_Contract_Pnd_W_Contract_8() { return pnd_W_Contract_Pnd_W_Contract_8; }

    public DbsGroup getPnd_W_ContractRedef4() { return pnd_W_ContractRedef4; }

    public DbsField getPnd_W_Contract_Pnd_W_Cntrct_1_2() { return pnd_W_Contract_Pnd_W_Cntrct_1_2; }

    public DbsGroup getPnd_W_Contract_Pnd_W_Cntrct_1_2Redef5() { return pnd_W_Contract_Pnd_W_Cntrct_1_2Redef5; }

    public DbsField getPnd_W_Contract_Pnd_W_Cntrct_1() { return pnd_W_Contract_Pnd_W_Cntrct_1; }

    public DbsField getPnd_W_Contract_Pnd_W_Cntrct_2() { return pnd_W_Contract_Pnd_W_Cntrct_2; }

    public DbsField getPnd_W_Contract_Pnd_W_Cntrct_Nbr() { return pnd_W_Contract_Pnd_W_Cntrct_Nbr; }

    public DbsField getPnd_W_Pymnt_Mode() { return pnd_W_Pymnt_Mode; }

    public DbsGroup getPnd_W_Pymnt_ModeRedef6() { return pnd_W_Pymnt_ModeRedef6; }

    public DbsField getPnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A() { return pnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A; }

    public DbsGroup getPnd_W_Pymnt_ModeRedef7() { return pnd_W_Pymnt_ModeRedef7; }

    public DbsField getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_1() { return pnd_W_Pymnt_Mode_Pnd_Contract_Mode_1; }

    public DbsField getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3() { return pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3; }

    public DbsGroup getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3Redef8() { return pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3Redef8; }

    public DbsField getPnd_W_Pymnt_Mode_Pnd_Filler_2() { return pnd_W_Pymnt_Mode_Pnd_Filler_2; }

    public DbsField getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N() { return pnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N; }

    public DbsField getPnd_W_Payee_Cde() { return pnd_W_Payee_Cde; }

    public DbsGroup getPnd_W_Payee_CdeRedef9() { return pnd_W_Payee_CdeRedef9; }

    public DbsField getPnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num() { return pnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num; }

    public DbsField getPnd_Option_Cde() { return pnd_Option_Cde; }

    public DbsField getPnd_W_Option_Cde_Desc() { return pnd_W_Option_Cde_Desc; }

    public DbsField getPnd_Num_Ovrpy_Yrs() { return pnd_Num_Ovrpy_Yrs; }

    public DbsField getPnd_Lowest_Year_Yyyymm() { return pnd_Lowest_Year_Yyyymm; }

    public DbsGroup getPnd_Lowest_Year_YyyymmRedef10() { return pnd_Lowest_Year_YyyymmRedef10; }

    public DbsField getPnd_Lowest_Year_Yyyymm_Pnd_Lowest_Year_Yyyymm_A() { return pnd_Lowest_Year_Yyyymm_Pnd_Lowest_Year_Yyyymm_A; }

    public DbsField getPnd_T() { return pnd_T; }

    public DbsField getPnd_P() { return pnd_P; }

    public DbsField getPnd_H() { return pnd_H; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_J() { return pnd_J; }

    public DbsField getPnd_R() { return pnd_R; }

    public DbsField getPnd_X() { return pnd_X; }

    public DbsField getPnd_Ovrpymnt_Years_Count() { return pnd_Ovrpymnt_Years_Count; }

    public DbsField getPnd_Cref_Sub() { return pnd_Cref_Sub; }

    public DbsField getPnd_Page_Ctr_1() { return pnd_Page_Ctr_1; }

    public DbsField getPnd_Page_Ctr_2() { return pnd_Page_Ctr_2; }

    public DbsField getPnd_Page_Ctr_3() { return pnd_Page_Ctr_3; }

    public DbsField getPnd_Page_Ctr_4() { return pnd_Page_Ctr_4; }

    public DbsField getPnd_Page_Ctr_5() { return pnd_Page_Ctr_5; }

    public DbsField getPnd_Page_Ctr_6() { return pnd_Page_Ctr_6; }

    public DbsField getPnd_Page_Ctr_7() { return pnd_Page_Ctr_7; }

    public DbsField getPnd_Page_Ctr_8() { return pnd_Page_Ctr_8; }

    public DbsField getPnd_Page_Ctr_9() { return pnd_Page_Ctr_9; }

    public DbsField getPnd_Found() { return pnd_Found; }

    public DbsField getPnd_Cnt_Frq() { return pnd_Cnt_Frq; }

    public DbsField getPnd_Pay_Install() { return pnd_Pay_Install; }

    public DbsField getPnd_Gen_Sub() { return pnd_Gen_Sub; }

    public DbsField getPnd_W_Pay_Date() { return pnd_W_Pay_Date; }

    public DbsField getPnd_W_Rate_Gross() { return pnd_W_Rate_Gross; }

    public DbsField getPnd_W_Rate_Ovrpy() { return pnd_W_Rate_Ovrpy; }

    public DbsField getPnd_W_Rate_Guar() { return pnd_W_Rate_Guar; }

    public DbsField getPnd_W_Rate_Divd() { return pnd_W_Rate_Divd; }

    public DbsField getPnd_Rate_Guar_Ovp() { return pnd_Rate_Guar_Ovp; }

    public DbsField getPnd_Rate_Divd_Ovp() { return pnd_Rate_Divd_Ovp; }

    public DbsField getPnd_Tot_Guar_Divd() { return pnd_Tot_Guar_Divd; }

    public DbsField getPnd_Calc_Hold() { return pnd_Calc_Hold; }

    public DbsField getPnd_W_Rate_Guar_Ovp() { return pnd_W_Rate_Guar_Ovp; }

    public DbsField getPnd_W_Rate_Divd_Ovp() { return pnd_W_Rate_Divd_Ovp; }

    public DbsField getPnd_Ovrpy_Tot() { return pnd_Ovrpy_Tot; }

    public DbsField getPnd_Cont_Stat_Fund() { return pnd_Cont_Stat_Fund; }

    public DbsGroup getPnd_Cont_Stat_FundRedef11() { return pnd_Cont_Stat_FundRedef11; }

    public DbsField getPnd_Cont_Stat_Fund_Pnd_Cont() { return pnd_Cont_Stat_Fund_Pnd_Cont; }

    public DbsField getPnd_Cont_Stat_Fund_Pnd_Stat() { return pnd_Cont_Stat_Fund_Pnd_Stat; }

    public DbsField getPnd_Cont_Stat_Fund_Pnd_Fund() { return pnd_Cont_Stat_Fund_Pnd_Fund; }

    public DbsField getPnd_Tiaa_Rate_Guar() { return pnd_Tiaa_Rate_Guar; }

    public DbsField getPnd_Tiaa_Rate_Div() { return pnd_Tiaa_Rate_Div; }

    public DbsField getPnd_Tiaa_Rate_Date() { return pnd_Tiaa_Rate_Date; }

    public DbsField getPnd_Old_Tiaa_Key_S() { return pnd_Old_Tiaa_Key_S; }

    public DbsGroup getPnd_Old_Tiaa_Key_SRedef12() { return pnd_Old_Tiaa_Key_SRedef12; }

    public DbsField getPnd_Old_Tiaa_Key_S_Pnd_Cont_S() { return pnd_Old_Tiaa_Key_S_Pnd_Cont_S; }

    public DbsField getPnd_Old_Tiaa_Key_S_Pnd_Stat_S() { return pnd_Old_Tiaa_Key_S_Pnd_Stat_S; }

    public DbsField getPnd_Old_Tiaa_Key_S_Pnd_Fund_Invrse_Lpd_S() { return pnd_Old_Tiaa_Key_S_Pnd_Fund_Invrse_Lpd_S; }

    public DbsField getPnd_Old_Tiaa_Key_S_Pnd_Comp_Fund_Cde_S() { return pnd_Old_Tiaa_Key_S_Pnd_Comp_Fund_Cde_S; }

    public DbsField getPnd_Old_Tiaa_Key_E() { return pnd_Old_Tiaa_Key_E; }

    public DbsGroup getPnd_Old_Tiaa_Key_ERedef13() { return pnd_Old_Tiaa_Key_ERedef13; }

    public DbsField getPnd_Old_Tiaa_Key_E_Pnd_Cont_E() { return pnd_Old_Tiaa_Key_E_Pnd_Cont_E; }

    public DbsField getPnd_Old_Tiaa_Key_E_Pnd_Stat_E() { return pnd_Old_Tiaa_Key_E_Pnd_Stat_E; }

    public DbsField getPnd_Old_Tiaa_Key_E_Pnd_Fund_Invrse_Lpd_E() { return pnd_Old_Tiaa_Key_E_Pnd_Fund_Invrse_Lpd_E; }

    public DbsField getPnd_Old_Tiaa_Key_E_Pnd_Comp_Fund_Cde_E() { return pnd_Old_Tiaa_Key_E_Pnd_Comp_Fund_Cde_E; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A() { return pnd_Fund_Lst_Pd_Dte_A; }

    public DbsGroup getPnd_Fund_Lst_Pd_Dte_ARedef14() { return pnd_Fund_Lst_Pd_Dte_ARedef14; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte; }

    public DbsGroup getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm; }

    public DbsGroup getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyy() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyy; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm; }

    public DbsGroup getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_MmRedef17() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_MmRedef17; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm_Alpha() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm_Alpha; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd; }

    public DbsGroup getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_DdRedef18() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_DdRedef18; }

    public DbsField getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd_Alpha() { return pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd_Alpha; }

    public DbsField getPnd_Enter_Old_Rates_File() { return pnd_Enter_Old_Rates_File; }

    public DbsField getPnd_More_Old_Rates() { return pnd_More_Old_Rates; }

    public DbsField getPnd_Year_Of_Death() { return pnd_Year_Of_Death; }

    public DbsField getPnd_Dt_Yyyy() { return pnd_Dt_Yyyy; }

    public DbsField getPnd_Dt_Mm() { return pnd_Dt_Mm; }

    public DbsField getPnd_Pay_Date_Yr() { return pnd_Pay_Date_Yr; }

    public DbsField getPnd_Ovrpymnt_Begin_Fiscal_Year() { return pnd_Ovrpymnt_Begin_Fiscal_Year; }

    public DbsGroup getPnd_Ovrpymnt_Begin_Fiscal_YearRedef19() { return pnd_Ovrpymnt_Begin_Fiscal_YearRedef19; }

    public DbsField getPnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Cc() { return pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Cc; 
        }

    public DbsField getPnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Yy() { return pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Yy; 
        }

    public DbsField getPnd_Ovrpymnt_End_Fiscal_Year() { return pnd_Ovrpymnt_End_Fiscal_Year; }

    public DbsGroup getPnd_Ovrpymnt_End_Fiscal_YearRedef20() { return pnd_Ovrpymnt_End_Fiscal_YearRedef20; }

    public DbsField getPnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Cc() { return pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Cc; 
        }

    public DbsField getPnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Yy() { return pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Yy; 
        }

    public DbsField getPnd_Ovrpymnt_Begin_Fiscal_Mo() { return pnd_Ovrpymnt_Begin_Fiscal_Mo; }

    public DbsField getPnd_Ovrpymnt_End_Fiscal_Mo() { return pnd_Ovrpymnt_End_Fiscal_Mo; }

    public DbsField getPnd_Ovrpymnt_Nbr_Per_Year() { return pnd_Ovrpymnt_Nbr_Per_Year; }

    public DbsField getPnd_Ovrpymnt_Dcdnt_Per_Amt() { return pnd_Ovrpymnt_Dcdnt_Per_Amt; }

    public DbsField getPnd_Ovrpymnt_Ovr_Per_Amt() { return pnd_Ovrpymnt_Ovr_Per_Amt; }

    public DbsField getPnd_Py_Dt() { return pnd_Py_Dt; }

    public DbsGroup getPnd_Py_DtRedef21() { return pnd_Py_DtRedef21; }

    public DbsField getPnd_Py_Dt_Pnd_Py_Dt_Mm() { return pnd_Py_Dt_Pnd_Py_Dt_Mm; }

    public DbsField getPnd_Py_Dt_Pnd_Filler_3() { return pnd_Py_Dt_Pnd_Filler_3; }

    public DbsField getPnd_Py_Dt_Pnd_Py_Dt_Dd() { return pnd_Py_Dt_Pnd_Py_Dt_Dd; }

    public DbsField getPnd_Py_Dt_Pnd_Filler_4() { return pnd_Py_Dt_Pnd_Filler_4; }

    public DbsField getPnd_Py_Dt_Pnd_Py_Dt_Yyyy() { return pnd_Py_Dt_Pnd_Py_Dt_Yyyy; }

    public DbsField getPnd_Py_Dt_Yyyymm_A() { return pnd_Py_Dt_Yyyymm_A; }

    public DbsGroup getPnd_Py_Dt_Yyyymm_ARedef22() { return pnd_Py_Dt_Yyyymm_ARedef22; }

    public DbsField getPnd_Py_Dt_Yyyymm_A_Pnd_Py_Dt_Yyyymm() { return pnd_Py_Dt_Yyyymm_A_Pnd_Py_Dt_Yyyymm; }

    public DbsField getPnd_T_Record() { return pnd_T_Record; }

    public DbsGroup getPnd_T_RecordRedef23() { return pnd_T_RecordRedef23; }

    public DbsField getPnd_T_Record_Pnd_T_Contract() { return pnd_T_Record_Pnd_T_Contract; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_1() { return pnd_T_Record_Pnd_T_Filler_1; }

    public DbsField getPnd_T_Record_Pnd_T_Payee_Cde() { return pnd_T_Record_Pnd_T_Payee_Cde; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_2() { return pnd_T_Record_Pnd_T_Filler_2; }

    public DbsField getPnd_T_Record_Pnd_T_Option_Cde_Desc() { return pnd_T_Record_Pnd_T_Option_Cde_Desc; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_3() { return pnd_T_Record_Pnd_T_Filler_3; }

    public DbsField getPnd_T_Record_Pnd_T_Pymnt_Mode() { return pnd_T_Record_Pnd_T_Pymnt_Mode; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_4() { return pnd_T_Record_Pnd_T_Filler_4; }

    public DbsField getPnd_T_Record_Pnd_T_Pay_Date() { return pnd_T_Record_Pnd_T_Pay_Date; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_5() { return pnd_T_Record_Pnd_T_Filler_5; }

    public DbsField getPnd_T_Record_Pnd_T_Rate_Guar() { return pnd_T_Record_Pnd_T_Rate_Guar; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_6() { return pnd_T_Record_Pnd_T_Filler_6; }

    public DbsField getPnd_T_Record_Pnd_T_Rate_Divd() { return pnd_T_Record_Pnd_T_Rate_Divd; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_7() { return pnd_T_Record_Pnd_T_Filler_7; }

    public DbsField getPnd_T_Record_Pnd_T_Rate_Guar_Ovp() { return pnd_T_Record_Pnd_T_Rate_Guar_Ovp; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_8() { return pnd_T_Record_Pnd_T_Filler_8; }

    public DbsField getPnd_T_Record_Pnd_T_Rate_Divd_Ovp() { return pnd_T_Record_Pnd_T_Rate_Divd_Ovp; }

    public DbsField getPnd_T_Record_Pnd_T_Filler_9() { return pnd_T_Record_Pnd_T_Filler_9; }

    public DbsField getPnd_T_Record_Pnd_T_Rate_Ovrpy() { return pnd_T_Record_Pnd_T_Rate_Ovrpy; }

    public DbsField getPnd_C_Rate_Guar() { return pnd_C_Rate_Guar; }

    public DbsField getPnd_C_Rate_Divd() { return pnd_C_Rate_Divd; }

    public DbsField getPnd_C_Rate_Guar_Ovp() { return pnd_C_Rate_Guar_Ovp; }

    public DbsField getPnd_C_Rate_Divd_Ovp() { return pnd_C_Rate_Divd_Ovp; }

    public DbsField getPnd_C_Rate_Ovrpymnt() { return pnd_C_Rate_Ovrpymnt; }

    public DbsField getPnd_C_Record() { return pnd_C_Record; }

    public DbsGroup getPnd_C_RecordRedef24() { return pnd_C_RecordRedef24; }

    public DbsField getPnd_C_Record_Pnd_C_Contract() { return pnd_C_Record_Pnd_C_Contract; }

    public DbsField getPnd_C_Record_Pnd_C_Filler_1() { return pnd_C_Record_Pnd_C_Filler_1; }

    public DbsField getPnd_C_Record_Pnd_C_Payee_Cde() { return pnd_C_Record_Pnd_C_Payee_Cde; }

    public DbsField getPnd_C_Record_Pnd_C_Filler_2() { return pnd_C_Record_Pnd_C_Filler_2; }

    public DbsField getPnd_C_Record_Pnd_C_Option_Cde_Desc() { return pnd_C_Record_Pnd_C_Option_Cde_Desc; }

    public DbsField getPnd_C_Record_Pnd_C_Filler_3() { return pnd_C_Record_Pnd_C_Filler_3; }

    public DbsField getPnd_C_Record_Pnd_C_Pymnt_Mode() { return pnd_C_Record_Pnd_C_Pymnt_Mode; }

    public DbsField getPnd_C_Record_Pnd_C_Filler_4() { return pnd_C_Record_Pnd_C_Filler_4; }

    public DbsField getPnd_C_Record_Pnd_C_Pay_Date() { return pnd_C_Record_Pnd_C_Pay_Date; }

    public DbsField getPnd_C_Record_Pnd_C_Filler_5() { return pnd_C_Record_Pnd_C_Filler_5; }

    public DbsField getPnd_C_Record_Pnd_C_Rate_Gross() { return pnd_C_Record_Pnd_C_Rate_Gross; }

    public DbsField getPnd_C_Record_Pnd_C_Filler_6() { return pnd_C_Record_Pnd_C_Filler_6; }

    public DbsField getPnd_C_Record_Pnd_C_Rate_Ovrpy() { return pnd_C_Record_Pnd_C_Rate_Ovrpy; }

    public DbsField getPnd_Cnt_Rate_Gross() { return pnd_Cnt_Rate_Gross; }

    public DbsField getPnd_Cnt_Rate_Ovrpy() { return pnd_Cnt_Rate_Ovrpy; }

    public DbsField getPnd_W_Parm_Date() { return pnd_W_Parm_Date; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Tiaa() { return pnd_Grand_Tot_Ovrpy_Tiaa; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Stock() { return pnd_Grand_Tot_Ovrpy_Stock; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Money_Market() { return pnd_Grand_Tot_Ovrpy_Money_Market; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Social_Choice() { return pnd_Grand_Tot_Ovrpy_Social_Choice; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Global_Equity() { return pnd_Grand_Tot_Ovrpy_Global_Equity; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Growth() { return pnd_Grand_Tot_Ovrpy_Growth; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Equity_Index() { return pnd_Grand_Tot_Ovrpy_Equity_Index; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Real_Estate() { return pnd_Grand_Tot_Ovrpy_Real_Estate; }

    public DbsField getPnd_Grand_Tot_Ovrpy_Bond() { return pnd_Grand_Tot_Ovrpy_Bond; }

    public DbsField getPnd_Grand_Rate_Guar_Ovp() { return pnd_Grand_Rate_Guar_Ovp; }

    public DbsField getPnd_Grand_Rate_Divd_Ovp() { return pnd_Grand_Rate_Divd_Ovp; }

    public DbsField getPnd_Num_Of_Tiaa() { return pnd_Num_Of_Tiaa; }

    public DbsField getPnd_Num_Of_Stock() { return pnd_Num_Of_Stock; }

    public DbsField getPnd_Num_Of_Money_Market() { return pnd_Num_Of_Money_Market; }

    public DbsField getPnd_Num_Of_Social_Choice() { return pnd_Num_Of_Social_Choice; }

    public DbsField getPnd_Num_Of_Global_Equity() { return pnd_Num_Of_Global_Equity; }

    public DbsField getPnd_Num_Of_Growth() { return pnd_Num_Of_Growth; }

    public DbsField getPnd_Num_Of_Equity_Index() { return pnd_Num_Of_Equity_Index; }

    public DbsField getPnd_Num_Of_Real_Estate() { return pnd_Num_Of_Real_Estate; }

    public DbsField getPnd_Num_Of_Bond() { return pnd_Num_Of_Bond; }

    public DbsField getPnd_W_Last_Pay_Date() { return pnd_W_Last_Pay_Date; }

    public DbsGroup getPnd_W_Last_Pay_DateRedef25() { return pnd_W_Last_Pay_DateRedef25; }

    public DbsField getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Yyyy() { return pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Yyyy; }

    public DbsField getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm() { return pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm; }

    public DbsGroup getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_MmRedef26() { return pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_MmRedef26; }

    public DbsField getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A() { return pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A; }

    public DbsField getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Dd() { return pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Dd; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef27() { return iaa_Parm_Card_Pnd_Parm_DateRedef27; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Date_NRedef28() { return iaa_Parm_Card_Pnd_Parm_Date_NRedef28; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph() { return pnd_Fl_Date_Yyyymmdd_Alph; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_AlphRedef29() { return pnd_Fl_Date_Yyyymmdd_AlphRedef29; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30; 
        }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph() { return pnd_Fl_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_AlphRedef31() { return pnd_Fl_Time_Hhiiss_AlphRedef31; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef33() { return pnd_Sy_Date_Yymmdd_AlphRedef33; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef35() { return pnd_Sy_Time_Hhiiss_AlphRedef35; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsField getPnd_Sys_Time() { return pnd_Sys_Time; }

    public DataAccessProgramView getVw_iaa_Old_Tiaa_Rates_View() { return vw_iaa_Old_Tiaa_Rates_View; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte() { return iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte() { return iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr() { return iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde() { return iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde() { return iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Cmpny_Fund_CdeRedef37() { return iaa_Old_Tiaa_Rates_View_Cmpny_Fund_CdeRedef37; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cmpny_Cde() { return iaa_Old_Tiaa_Rates_View_Cmpny_Cde; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Fund_Cde() { return iaa_Old_Tiaa_Rates_View_Fund_Cde; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt() { return iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt() { return iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Py_Dte_Key() { return iaa_Old_Tiaa_Rates_View_Cntrct_Py_Dte_Key; }

    public DbsField getPnd_Temp_Occur() { return pnd_Temp_Occur; }

    public DbsField getPnd_D() { return pnd_D; }

    public DbsField getPnd_E() { return pnd_E; }

    public DbsField getPnd_Temp_Occur_2() { return pnd_Temp_Occur_2; }

    public DbsField getPnd_Temp_Occur_3() { return pnd_Temp_Occur_3; }

    public DbsField getPnd_Q() { return pnd_Q; }

    public DbsField getPnd_X1() { return pnd_X1; }

    public DbsField getPnd_M1() { return pnd_M1; }

    public DbsField getPnd_Ovp_Cnt() { return pnd_Ovp_Cnt; }

    public DbsField getPnd_Earlier_Death() { return pnd_Earlier_Death; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_View", "IAA-CNTRCT-VIEW"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_View_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_View_Cntrct_Optn_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_View_Cntrct_Issue_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");

        vw_new_Tiaa_Rates = new DataAccessProgramView(new NameInfo("vw_new_Tiaa_Rates", "NEW-TIAA-RATES"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS");
        new_Tiaa_Rates_Tiaa_Tot_Per_Amt = vw_new_Tiaa_Rates.getRecord().newFieldInGroup("new_Tiaa_Rates_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        new_Tiaa_Rates_Tiaa_Tot_Div_Amt = vw_new_Tiaa_Rates.getRecord().newFieldInGroup("new_Tiaa_Rates_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        new_Tiaa_Rates_Tiaa_Cntrct_Fund_Key = vw_new_Tiaa_Rates.getRecord().newFieldInGroup("new_Tiaa_Rates_Tiaa_Cntrct_Fund_Key", "TIAA-CNTRCT-FUND-KEY", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");

        pnd_Ovrpymnt_Time_Selects = newFieldInRecord("pnd_Ovrpymnt_Time_Selects", "#OVRPYMNT-TIME-SELECTS", FieldType.NUMERIC, 6);

        pnd_Ovrpymnt_Reads = newFieldInRecord("pnd_Ovrpymnt_Reads", "#OVRPYMNT-READS", FieldType.NUMERIC, 6);

        pnd_A1 = newFieldArrayInRecord("pnd_A1", "#A1", FieldType.STRING, 2, new DbsArrayController(1,12));

        pnd_A6 = newFieldArrayInRecord("pnd_A6", "#A6", FieldType.STRING, 2, new DbsArrayController(1,3,1,4));

        pnd_A7 = newFieldArrayInRecord("pnd_A7", "#A7", FieldType.STRING, 2, new DbsArrayController(1,6,1,2));

        pnd_C1 = newFieldArrayInRecord("pnd_C1", "#C1", FieldType.STRING, 2, new DbsArrayController(1,12));

        pnd_C6 = newFieldArrayInRecord("pnd_C6", "#C6", FieldType.STRING, 2, new DbsArrayController(1,3,1,4));

        pnd_C7 = newFieldArrayInRecord("pnd_C7", "#C7", FieldType.STRING, 2, new DbsArrayController(1,6,1,2));

        pnd_T1 = newFieldArrayInRecord("pnd_T1", "#T1", FieldType.STRING, 2, new DbsArrayController(1,12));

        pnd_T6 = newFieldArrayInRecord("pnd_T6", "#T6", FieldType.STRING, 2, new DbsArrayController(1,3,1,4));

        pnd_T7 = newFieldArrayInRecord("pnd_T7", "#T7", FieldType.STRING, 2, new DbsArrayController(1,6,1,2));

        pnd_A = newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_ARedef1 = newGroupInRecord("pnd_ARedef1", "Redefines", pnd_A);
        pnd_A_Pnd_A_Alph = pnd_ARedef1.newFieldInGroup("pnd_A_Pnd_A_Alph", "#A-ALPH", FieldType.STRING, 2);

        pnd_B = newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);

        pnd_C = newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_CRedef2 = newGroupInRecord("pnd_CRedef2", "Redefines", pnd_C);
        pnd_C_Pnd_C_Alph = pnd_CRedef2.newFieldInGroup("pnd_C_Pnd_C_Alph", "#C-ALPH", FieldType.STRING, 2);

        pnd_Contract_Type_Field = newFieldInRecord("pnd_Contract_Type_Field", "#CONTRACT-TYPE-FIELD", FieldType.STRING, 1);

        pnd_W_Contract_Issue_Dte = newFieldInRecord("pnd_W_Contract_Issue_Dte", "#W-CONTRACT-ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_W_Contract = newFieldInRecord("pnd_W_Contract", "#W-CONTRACT", FieldType.STRING, 10);
        pnd_W_ContractRedef3 = newGroupInRecord("pnd_W_ContractRedef3", "Redefines", pnd_W_Contract);
        pnd_W_Contract_Pnd_W_Contract_1_7 = pnd_W_ContractRedef3.newFieldInGroup("pnd_W_Contract_Pnd_W_Contract_1_7", "#W-CONTRACT-1-7", FieldType.STRING, 
            7);
        pnd_W_Contract_Pnd_W_Contract_8 = pnd_W_ContractRedef3.newFieldInGroup("pnd_W_Contract_Pnd_W_Contract_8", "#W-CONTRACT-8", FieldType.STRING, 1);
        pnd_W_ContractRedef4 = newGroupInRecord("pnd_W_ContractRedef4", "Redefines", pnd_W_Contract);
        pnd_W_Contract_Pnd_W_Cntrct_1_2 = pnd_W_ContractRedef4.newFieldInGroup("pnd_W_Contract_Pnd_W_Cntrct_1_2", "#W-CNTRCT-1-2", FieldType.STRING, 2);
        pnd_W_Contract_Pnd_W_Cntrct_1_2Redef5 = pnd_W_ContractRedef4.newGroupInGroup("pnd_W_Contract_Pnd_W_Cntrct_1_2Redef5", "Redefines", pnd_W_Contract_Pnd_W_Cntrct_1_2);
        pnd_W_Contract_Pnd_W_Cntrct_1 = pnd_W_Contract_Pnd_W_Cntrct_1_2Redef5.newFieldInGroup("pnd_W_Contract_Pnd_W_Cntrct_1", "#W-CNTRCT-1", FieldType.STRING, 
            1);
        pnd_W_Contract_Pnd_W_Cntrct_2 = pnd_W_Contract_Pnd_W_Cntrct_1_2Redef5.newFieldInGroup("pnd_W_Contract_Pnd_W_Cntrct_2", "#W-CNTRCT-2", FieldType.STRING, 
            1);
        pnd_W_Contract_Pnd_W_Cntrct_Nbr = pnd_W_ContractRedef4.newFieldInGroup("pnd_W_Contract_Pnd_W_Cntrct_Nbr", "#W-CNTRCT-NBR", FieldType.NUMERIC, 
            5);

        pnd_W_Pymnt_Mode = newFieldInRecord("pnd_W_Pymnt_Mode", "#W-PYMNT-MODE", FieldType.NUMERIC, 3);
        pnd_W_Pymnt_ModeRedef6 = newGroupInRecord("pnd_W_Pymnt_ModeRedef6", "Redefines", pnd_W_Pymnt_Mode);
        pnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A = pnd_W_Pymnt_ModeRedef6.newFieldInGroup("pnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A", "#W-PYMNT-MODE-A", FieldType.STRING, 
            3);
        pnd_W_Pymnt_ModeRedef7 = newGroupInRecord("pnd_W_Pymnt_ModeRedef7", "Redefines", pnd_W_Pymnt_Mode);
        pnd_W_Pymnt_Mode_Pnd_Contract_Mode_1 = pnd_W_Pymnt_ModeRedef7.newFieldInGroup("pnd_W_Pymnt_Mode_Pnd_Contract_Mode_1", "#CONTRACT-MODE-1", FieldType.STRING, 
            1);
        pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3 = pnd_W_Pymnt_ModeRedef7.newFieldInGroup("pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3", "#CONTRACT-MODE-2-3", 
            FieldType.STRING, 2);
        pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3Redef8 = pnd_W_Pymnt_ModeRedef7.newGroupInGroup("pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3Redef8", "Redefines", 
            pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3);
        pnd_W_Pymnt_Mode_Pnd_Filler_2 = pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3Redef8.newFieldInGroup("pnd_W_Pymnt_Mode_Pnd_Filler_2", "#FILLER-2", FieldType.NUMERIC, 
            1);
        pnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N = pnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3Redef8.newFieldInGroup("pnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N", 
            "#CONTRACT-MODE-3-N", FieldType.NUMERIC, 1);

        pnd_W_Payee_Cde = newFieldInRecord("pnd_W_Payee_Cde", "#W-PAYEE-CDE", FieldType.STRING, 2);
        pnd_W_Payee_CdeRedef9 = newGroupInRecord("pnd_W_Payee_CdeRedef9", "Redefines", pnd_W_Payee_Cde);
        pnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num = pnd_W_Payee_CdeRedef9.newFieldInGroup("pnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num", "#W-PAYEE-CDE-NUM", FieldType.NUMERIC, 
            2);

        pnd_Option_Cde = newFieldInRecord("pnd_Option_Cde", "#OPTION-CDE", FieldType.NUMERIC, 2);

        pnd_W_Option_Cde_Desc = newFieldInRecord("pnd_W_Option_Cde_Desc", "#W-OPTION-CDE-DESC", FieldType.STRING, 20);

        pnd_Num_Ovrpy_Yrs = newFieldInRecord("pnd_Num_Ovrpy_Yrs", "#NUM-OVRPY-YRS", FieldType.NUMERIC, 1);

        pnd_Lowest_Year_Yyyymm = newFieldInRecord("pnd_Lowest_Year_Yyyymm", "#LOWEST-YEAR-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Lowest_Year_YyyymmRedef10 = newGroupInRecord("pnd_Lowest_Year_YyyymmRedef10", "Redefines", pnd_Lowest_Year_Yyyymm);
        pnd_Lowest_Year_Yyyymm_Pnd_Lowest_Year_Yyyymm_A = pnd_Lowest_Year_YyyymmRedef10.newFieldInGroup("pnd_Lowest_Year_Yyyymm_Pnd_Lowest_Year_Yyyymm_A", 
            "#LOWEST-YEAR-YYYYMM-A", FieldType.STRING, 6);

        pnd_T = newFieldInRecord("pnd_T", "#T", FieldType.NUMERIC, 2);

        pnd_P = newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 2);

        pnd_H = newFieldInRecord("pnd_H", "#H", FieldType.NUMERIC, 2);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 1);

        pnd_J = newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);

        pnd_R = newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 2);

        pnd_X = newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);

        pnd_Ovrpymnt_Years_Count = newFieldInRecord("pnd_Ovrpymnt_Years_Count", "#OVRPYMNT-YEARS-COUNT", FieldType.NUMERIC, 2);

        pnd_Cref_Sub = newFieldInRecord("pnd_Cref_Sub", "#CREF-SUB", FieldType.NUMERIC, 2);

        pnd_Page_Ctr_1 = newFieldInRecord("pnd_Page_Ctr_1", "#PAGE-CTR-1", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_2 = newFieldInRecord("pnd_Page_Ctr_2", "#PAGE-CTR-2", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_3 = newFieldInRecord("pnd_Page_Ctr_3", "#PAGE-CTR-3", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_4 = newFieldInRecord("pnd_Page_Ctr_4", "#PAGE-CTR-4", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_5 = newFieldInRecord("pnd_Page_Ctr_5", "#PAGE-CTR-5", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_6 = newFieldInRecord("pnd_Page_Ctr_6", "#PAGE-CTR-6", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_7 = newFieldInRecord("pnd_Page_Ctr_7", "#PAGE-CTR-7", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_8 = newFieldInRecord("pnd_Page_Ctr_8", "#PAGE-CTR-8", FieldType.NUMERIC, 4);

        pnd_Page_Ctr_9 = newFieldInRecord("pnd_Page_Ctr_9", "#PAGE-CTR-9", FieldType.NUMERIC, 4);

        pnd_Found = newFieldInRecord("pnd_Found", "#FOUND", FieldType.STRING, 1);

        pnd_Cnt_Frq = newFieldInRecord("pnd_Cnt_Frq", "#CNT-FRQ", FieldType.NUMERIC, 2);

        pnd_Pay_Install = newFieldInRecord("pnd_Pay_Install", "#PAY-INSTALL", FieldType.NUMERIC, 2);

        pnd_Gen_Sub = newFieldInRecord("pnd_Gen_Sub", "#GEN-SUB", FieldType.NUMERIC, 2);

        pnd_W_Pay_Date = newFieldArrayInRecord("pnd_W_Pay_Date", "#W-PAY-DATE", FieldType.STRING, 10, new DbsArrayController(1,84));

        pnd_W_Rate_Gross = newFieldArrayInRecord("pnd_W_Rate_Gross", "#W-RATE-GROSS", FieldType.DECIMAL, 9,2, new DbsArrayController(1,84));

        pnd_W_Rate_Ovrpy = newFieldArrayInRecord("pnd_W_Rate_Ovrpy", "#W-RATE-OVRPY", FieldType.DECIMAL, 9,2, new DbsArrayController(1,84));

        pnd_W_Rate_Guar = newFieldArrayInRecord("pnd_W_Rate_Guar", "#W-RATE-GUAR", FieldType.DECIMAL, 9,2, new DbsArrayController(1,84));

        pnd_W_Rate_Divd = newFieldArrayInRecord("pnd_W_Rate_Divd", "#W-RATE-DIVD", FieldType.DECIMAL, 9,2, new DbsArrayController(1,84));

        pnd_Rate_Guar_Ovp = newFieldInRecord("pnd_Rate_Guar_Ovp", "#RATE-GUAR-OVP", FieldType.DECIMAL, 9,2);

        pnd_Rate_Divd_Ovp = newFieldInRecord("pnd_Rate_Divd_Ovp", "#RATE-DIVD-OVP", FieldType.DECIMAL, 9,2);

        pnd_Tot_Guar_Divd = newFieldInRecord("pnd_Tot_Guar_Divd", "#TOT-GUAR-DIVD", FieldType.DECIMAL, 9,2);

        pnd_Calc_Hold = newFieldInRecord("pnd_Calc_Hold", "#CALC-HOLD", FieldType.DECIMAL, 13,6);

        pnd_W_Rate_Guar_Ovp = newFieldInRecord("pnd_W_Rate_Guar_Ovp", "#W-RATE-GUAR-OVP", FieldType.DECIMAL, 9,2);

        pnd_W_Rate_Divd_Ovp = newFieldInRecord("pnd_W_Rate_Divd_Ovp", "#W-RATE-DIVD-OVP", FieldType.DECIMAL, 9,2);

        pnd_Ovrpy_Tot = newFieldInRecord("pnd_Ovrpy_Tot", "#OVRPY-TOT", FieldType.DECIMAL, 9,2);

        pnd_Cont_Stat_Fund = newFieldInRecord("pnd_Cont_Stat_Fund", "#CONT-STAT-FUND", FieldType.STRING, 15);
        pnd_Cont_Stat_FundRedef11 = newGroupInRecord("pnd_Cont_Stat_FundRedef11", "Redefines", pnd_Cont_Stat_Fund);
        pnd_Cont_Stat_Fund_Pnd_Cont = pnd_Cont_Stat_FundRedef11.newFieldInGroup("pnd_Cont_Stat_Fund_Pnd_Cont", "#CONT", FieldType.STRING, 10);
        pnd_Cont_Stat_Fund_Pnd_Stat = pnd_Cont_Stat_FundRedef11.newFieldInGroup("pnd_Cont_Stat_Fund_Pnd_Stat", "#STAT", FieldType.NUMERIC, 2);
        pnd_Cont_Stat_Fund_Pnd_Fund = pnd_Cont_Stat_FundRedef11.newFieldInGroup("pnd_Cont_Stat_Fund_Pnd_Fund", "#FUND", FieldType.STRING, 3);

        pnd_Tiaa_Rate_Guar = newFieldArrayInRecord("pnd_Tiaa_Rate_Guar", "#TIAA-RATE-GUAR", FieldType.DECIMAL, 9,2, new DbsArrayController(1,15));

        pnd_Tiaa_Rate_Div = newFieldArrayInRecord("pnd_Tiaa_Rate_Div", "#TIAA-RATE-DIV", FieldType.DECIMAL, 9,2, new DbsArrayController(1,15));

        pnd_Tiaa_Rate_Date = newFieldArrayInRecord("pnd_Tiaa_Rate_Date", "#TIAA-RATE-DATE", FieldType.NUMERIC, 6, new DbsArrayController(1,15));

        pnd_Old_Tiaa_Key_S = newFieldInRecord("pnd_Old_Tiaa_Key_S", "#OLD-TIAA-KEY-S", FieldType.STRING, 23);
        pnd_Old_Tiaa_Key_SRedef12 = newGroupInRecord("pnd_Old_Tiaa_Key_SRedef12", "Redefines", pnd_Old_Tiaa_Key_S);
        pnd_Old_Tiaa_Key_S_Pnd_Cont_S = pnd_Old_Tiaa_Key_SRedef12.newFieldInGroup("pnd_Old_Tiaa_Key_S_Pnd_Cont_S", "#CONT-S", FieldType.STRING, 10);
        pnd_Old_Tiaa_Key_S_Pnd_Stat_S = pnd_Old_Tiaa_Key_SRedef12.newFieldInGroup("pnd_Old_Tiaa_Key_S_Pnd_Stat_S", "#STAT-S", FieldType.NUMERIC, 2);
        pnd_Old_Tiaa_Key_S_Pnd_Fund_Invrse_Lpd_S = pnd_Old_Tiaa_Key_SRedef12.newFieldInGroup("pnd_Old_Tiaa_Key_S_Pnd_Fund_Invrse_Lpd_S", "#FUND-INVRSE-LPD-S", 
            FieldType.NUMERIC, 8);
        pnd_Old_Tiaa_Key_S_Pnd_Comp_Fund_Cde_S = pnd_Old_Tiaa_Key_SRedef12.newFieldInGroup("pnd_Old_Tiaa_Key_S_Pnd_Comp_Fund_Cde_S", "#COMP-FUND-CDE-S", 
            FieldType.STRING, 3);

        pnd_Old_Tiaa_Key_E = newFieldInRecord("pnd_Old_Tiaa_Key_E", "#OLD-TIAA-KEY-E", FieldType.STRING, 23);
        pnd_Old_Tiaa_Key_ERedef13 = newGroupInRecord("pnd_Old_Tiaa_Key_ERedef13", "Redefines", pnd_Old_Tiaa_Key_E);
        pnd_Old_Tiaa_Key_E_Pnd_Cont_E = pnd_Old_Tiaa_Key_ERedef13.newFieldInGroup("pnd_Old_Tiaa_Key_E_Pnd_Cont_E", "#CONT-E", FieldType.STRING, 10);
        pnd_Old_Tiaa_Key_E_Pnd_Stat_E = pnd_Old_Tiaa_Key_ERedef13.newFieldInGroup("pnd_Old_Tiaa_Key_E_Pnd_Stat_E", "#STAT-E", FieldType.NUMERIC, 2);
        pnd_Old_Tiaa_Key_E_Pnd_Fund_Invrse_Lpd_E = pnd_Old_Tiaa_Key_ERedef13.newFieldInGroup("pnd_Old_Tiaa_Key_E_Pnd_Fund_Invrse_Lpd_E", "#FUND-INVRSE-LPD-E", 
            FieldType.NUMERIC, 8);
        pnd_Old_Tiaa_Key_E_Pnd_Comp_Fund_Cde_E = pnd_Old_Tiaa_Key_ERedef13.newFieldInGroup("pnd_Old_Tiaa_Key_E_Pnd_Comp_Fund_Cde_E", "#COMP-FUND-CDE-E", 
            FieldType.STRING, 3);

        pnd_Fund_Lst_Pd_Dte_A = newFieldInRecord("pnd_Fund_Lst_Pd_Dte_A", "#FUND-LST-PD-DTE-A", FieldType.STRING, 8);
        pnd_Fund_Lst_Pd_Dte_ARedef14 = newGroupInRecord("pnd_Fund_Lst_Pd_Dte_ARedef14", "Redefines", pnd_Fund_Lst_Pd_Dte_A);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte = pnd_Fund_Lst_Pd_Dte_ARedef14.newFieldInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte", "#FUND-LST-PD-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15 = pnd_Fund_Lst_Pd_Dte_ARedef14.newGroupInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15", 
            "Redefines", pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15.newFieldInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm", 
            "#FUND-LST-PD-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16 = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15.newGroupInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16", 
            "Redefines", pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyy = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16.newFieldInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyy", 
            "#FUND-LST-PD-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16.newFieldInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm", 
            "#FUND-LST-PD-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_MmRedef17 = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_YyyymmRedef16.newGroupInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_MmRedef17", 
            "Redefines", pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm_Alpha = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_MmRedef17.newFieldInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm_Alpha", 
            "#FUND-LST-PD-DTE-MM-ALPHA", FieldType.STRING, 2);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15.newFieldInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd", 
            "#FUND-LST-PD-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_DdRedef18 = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_DteRedef15.newGroupInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_DdRedef18", 
            "Redefines", pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd);
        pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd_Alpha = pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_DdRedef18.newFieldInGroup("pnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Dd_Alpha", 
            "#FUND-LST-PD-DTE-DD-ALPHA", FieldType.STRING, 2);

        pnd_Enter_Old_Rates_File = newFieldInRecord("pnd_Enter_Old_Rates_File", "#ENTER-OLD-RATES-FILE", FieldType.STRING, 1);

        pnd_More_Old_Rates = newFieldInRecord("pnd_More_Old_Rates", "#MORE-OLD-RATES", FieldType.STRING, 1);

        pnd_Year_Of_Death = newFieldInRecord("pnd_Year_Of_Death", "#YEAR-OF-DEATH", FieldType.STRING, 1);

        pnd_Dt_Yyyy = newFieldInRecord("pnd_Dt_Yyyy", "#DT-YYYY", FieldType.NUMERIC, 4);

        pnd_Dt_Mm = newFieldInRecord("pnd_Dt_Mm", "#DT-MM", FieldType.NUMERIC, 2);

        pnd_Pay_Date_Yr = newFieldInRecord("pnd_Pay_Date_Yr", "#PAY-DATE-YR", FieldType.NUMERIC, 4);

        pnd_Ovrpymnt_Begin_Fiscal_Year = newFieldInRecord("pnd_Ovrpymnt_Begin_Fiscal_Year", "#OVRPYMNT-BEGIN-FISCAL-YEAR", FieldType.NUMERIC, 4);
        pnd_Ovrpymnt_Begin_Fiscal_YearRedef19 = newGroupInRecord("pnd_Ovrpymnt_Begin_Fiscal_YearRedef19", "Redefines", pnd_Ovrpymnt_Begin_Fiscal_Year);
        pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Cc = pnd_Ovrpymnt_Begin_Fiscal_YearRedef19.newFieldInGroup("pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Cc", 
            "#OVRPYMNT-BEGIN-FISCAL-YEAR-CC", FieldType.NUMERIC, 2);
        pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Yy = pnd_Ovrpymnt_Begin_Fiscal_YearRedef19.newFieldInGroup("pnd_Ovrpymnt_Begin_Fiscal_Year_Pnd_Ovrpymnt_Begin_Fiscal_Year_Yy", 
            "#OVRPYMNT-BEGIN-FISCAL-YEAR-YY", FieldType.NUMERIC, 2);

        pnd_Ovrpymnt_End_Fiscal_Year = newFieldInRecord("pnd_Ovrpymnt_End_Fiscal_Year", "#OVRPYMNT-END-FISCAL-YEAR", FieldType.NUMERIC, 4);
        pnd_Ovrpymnt_End_Fiscal_YearRedef20 = newGroupInRecord("pnd_Ovrpymnt_End_Fiscal_YearRedef20", "Redefines", pnd_Ovrpymnt_End_Fiscal_Year);
        pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Cc = pnd_Ovrpymnt_End_Fiscal_YearRedef20.newFieldInGroup("pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Cc", 
            "#OVRPYMNT-END-FISCAL-YEAR-CC", FieldType.NUMERIC, 2);
        pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Yy = pnd_Ovrpymnt_End_Fiscal_YearRedef20.newFieldInGroup("pnd_Ovrpymnt_End_Fiscal_Year_Pnd_Ovrpymnt_End_Fiscal_Year_Yy", 
            "#OVRPYMNT-END-FISCAL-YEAR-YY", FieldType.NUMERIC, 2);

        pnd_Ovrpymnt_Begin_Fiscal_Mo = newFieldInRecord("pnd_Ovrpymnt_Begin_Fiscal_Mo", "#OVRPYMNT-BEGIN-FISCAL-MO", FieldType.NUMERIC, 2);

        pnd_Ovrpymnt_End_Fiscal_Mo = newFieldInRecord("pnd_Ovrpymnt_End_Fiscal_Mo", "#OVRPYMNT-END-FISCAL-MO", FieldType.NUMERIC, 2);

        pnd_Ovrpymnt_Nbr_Per_Year = newFieldInRecord("pnd_Ovrpymnt_Nbr_Per_Year", "#OVRPYMNT-NBR-PER-YEAR", FieldType.NUMERIC, 2);

        pnd_Ovrpymnt_Dcdnt_Per_Amt = newFieldInRecord("pnd_Ovrpymnt_Dcdnt_Per_Amt", "#OVRPYMNT-DCDNT-PER-AMT", FieldType.DECIMAL, 9,2);

        pnd_Ovrpymnt_Ovr_Per_Amt = newFieldInRecord("pnd_Ovrpymnt_Ovr_Per_Amt", "#OVRPYMNT-OVR-PER-AMT", FieldType.DECIMAL, 9,2);

        pnd_Py_Dt = newFieldInRecord("pnd_Py_Dt", "#PY-DT", FieldType.STRING, 10);
        pnd_Py_DtRedef21 = newGroupInRecord("pnd_Py_DtRedef21", "Redefines", pnd_Py_Dt);
        pnd_Py_Dt_Pnd_Py_Dt_Mm = pnd_Py_DtRedef21.newFieldInGroup("pnd_Py_Dt_Pnd_Py_Dt_Mm", "#PY-DT-MM", FieldType.STRING, 2);
        pnd_Py_Dt_Pnd_Filler_3 = pnd_Py_DtRedef21.newFieldInGroup("pnd_Py_Dt_Pnd_Filler_3", "#FILLER-3", FieldType.STRING, 1);
        pnd_Py_Dt_Pnd_Py_Dt_Dd = pnd_Py_DtRedef21.newFieldInGroup("pnd_Py_Dt_Pnd_Py_Dt_Dd", "#PY-DT-DD", FieldType.STRING, 2);
        pnd_Py_Dt_Pnd_Filler_4 = pnd_Py_DtRedef21.newFieldInGroup("pnd_Py_Dt_Pnd_Filler_4", "#FILLER-4", FieldType.STRING, 1);
        pnd_Py_Dt_Pnd_Py_Dt_Yyyy = pnd_Py_DtRedef21.newFieldInGroup("pnd_Py_Dt_Pnd_Py_Dt_Yyyy", "#PY-DT-YYYY", FieldType.STRING, 4);

        pnd_Py_Dt_Yyyymm_A = newFieldInRecord("pnd_Py_Dt_Yyyymm_A", "#PY-DT-YYYYMM-A", FieldType.STRING, 6);
        pnd_Py_Dt_Yyyymm_ARedef22 = newGroupInRecord("pnd_Py_Dt_Yyyymm_ARedef22", "Redefines", pnd_Py_Dt_Yyyymm_A);
        pnd_Py_Dt_Yyyymm_A_Pnd_Py_Dt_Yyyymm = pnd_Py_Dt_Yyyymm_ARedef22.newFieldInGroup("pnd_Py_Dt_Yyyymm_A_Pnd_Py_Dt_Yyyymm", "#PY-DT-YYYYMM", FieldType.NUMERIC, 
            6);

        pnd_T_Record = newFieldInRecord("pnd_T_Record", "#T-RECORD", FieldType.STRING, 132);
        pnd_T_RecordRedef23 = newGroupInRecord("pnd_T_RecordRedef23", "Redefines", pnd_T_Record);
        pnd_T_Record_Pnd_T_Contract = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Contract", "#T-CONTRACT", FieldType.STRING, 9);
        pnd_T_Record_Pnd_T_Filler_1 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_1", "#T-FILLER-1", FieldType.STRING, 2);
        pnd_T_Record_Pnd_T_Payee_Cde = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Payee_Cde", "#T-PAYEE-CDE", FieldType.STRING, 2);
        pnd_T_Record_Pnd_T_Filler_2 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_2", "#T-FILLER-2", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Option_Cde_Desc = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Option_Cde_Desc", "#T-OPTION-CDE-DESC", FieldType.STRING, 
            20);
        pnd_T_Record_Pnd_T_Filler_3 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_3", "#T-FILLER-3", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Pymnt_Mode = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Pymnt_Mode", "#T-PYMNT-MODE", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Filler_4 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_4", "#T-FILLER-4", FieldType.STRING, 4);
        pnd_T_Record_Pnd_T_Pay_Date = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Pay_Date", "#T-PAY-DATE", FieldType.STRING, 10);
        pnd_T_Record_Pnd_T_Filler_5 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_5", "#T-FILLER-5", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Rate_Guar = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Rate_Guar", "#T-RATE-GUAR", FieldType.DECIMAL, 9,2);
        pnd_T_Record_Pnd_T_Filler_6 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_6", "#T-FILLER-6", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Rate_Divd = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Rate_Divd", "#T-RATE-DIVD", FieldType.DECIMAL, 9,2);
        pnd_T_Record_Pnd_T_Filler_7 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_7", "#T-FILLER-7", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Rate_Guar_Ovp = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Rate_Guar_Ovp", "#T-RATE-GUAR-OVP", FieldType.DECIMAL, 
            9,2);
        pnd_T_Record_Pnd_T_Filler_8 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_8", "#T-FILLER-8", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Rate_Divd_Ovp = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Rate_Divd_Ovp", "#T-RATE-DIVD-OVP", FieldType.DECIMAL, 
            9,2);
        pnd_T_Record_Pnd_T_Filler_9 = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Filler_9", "#T-FILLER-9", FieldType.STRING, 3);
        pnd_T_Record_Pnd_T_Rate_Ovrpy = pnd_T_RecordRedef23.newFieldInGroup("pnd_T_Record_Pnd_T_Rate_Ovrpy", "#T-RATE-OVRPY", FieldType.DECIMAL, 9,2);

        pnd_C_Rate_Guar = newFieldInRecord("pnd_C_Rate_Guar", "#C-RATE-GUAR", FieldType.DECIMAL, 9,2);

        pnd_C_Rate_Divd = newFieldInRecord("pnd_C_Rate_Divd", "#C-RATE-DIVD", FieldType.DECIMAL, 9,2);

        pnd_C_Rate_Guar_Ovp = newFieldInRecord("pnd_C_Rate_Guar_Ovp", "#C-RATE-GUAR-OVP", FieldType.DECIMAL, 9,2);

        pnd_C_Rate_Divd_Ovp = newFieldInRecord("pnd_C_Rate_Divd_Ovp", "#C-RATE-DIVD-OVP", FieldType.DECIMAL, 9,2);

        pnd_C_Rate_Ovrpymnt = newFieldInRecord("pnd_C_Rate_Ovrpymnt", "#C-RATE-OVRPYMNT", FieldType.DECIMAL, 9,2);

        pnd_C_Record = newFieldInRecord("pnd_C_Record", "#C-RECORD", FieldType.STRING, 132);
        pnd_C_RecordRedef24 = newGroupInRecord("pnd_C_RecordRedef24", "Redefines", pnd_C_Record);
        pnd_C_Record_Pnd_C_Contract = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Contract", "#C-CONTRACT", FieldType.STRING, 9);
        pnd_C_Record_Pnd_C_Filler_1 = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Filler_1", "#C-FILLER-1", FieldType.STRING, 10);
        pnd_C_Record_Pnd_C_Payee_Cde = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Payee_Cde", "#C-PAYEE-CDE", FieldType.STRING, 2);
        pnd_C_Record_Pnd_C_Filler_2 = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Filler_2", "#C-FILLER-2", FieldType.STRING, 8);
        pnd_C_Record_Pnd_C_Option_Cde_Desc = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Option_Cde_Desc", "#C-OPTION-CDE-DESC", FieldType.STRING, 
            20);
        pnd_C_Record_Pnd_C_Filler_3 = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Filler_3", "#C-FILLER-3", FieldType.STRING, 6);
        pnd_C_Record_Pnd_C_Pymnt_Mode = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Pymnt_Mode", "#C-PYMNT-MODE", FieldType.STRING, 3);
        pnd_C_Record_Pnd_C_Filler_4 = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Filler_4", "#C-FILLER-4", FieldType.STRING, 12);
        pnd_C_Record_Pnd_C_Pay_Date = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Pay_Date", "#C-PAY-DATE", FieldType.STRING, 10);
        pnd_C_Record_Pnd_C_Filler_5 = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Filler_5", "#C-FILLER-5", FieldType.STRING, 11);
        pnd_C_Record_Pnd_C_Rate_Gross = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Rate_Gross", "#C-RATE-GROSS", FieldType.DECIMAL, 9,2);
        pnd_C_Record_Pnd_C_Filler_6 = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Filler_6", "#C-FILLER-6", FieldType.STRING, 10);
        pnd_C_Record_Pnd_C_Rate_Ovrpy = pnd_C_RecordRedef24.newFieldInGroup("pnd_C_Record_Pnd_C_Rate_Ovrpy", "#C-RATE-OVRPY", FieldType.DECIMAL, 9,2);

        pnd_Cnt_Rate_Gross = newFieldInRecord("pnd_Cnt_Rate_Gross", "#CNT-RATE-GROSS", FieldType.DECIMAL, 9,2);

        pnd_Cnt_Rate_Ovrpy = newFieldInRecord("pnd_Cnt_Rate_Ovrpy", "#CNT-RATE-OVRPY", FieldType.DECIMAL, 9,2);

        pnd_W_Parm_Date = newFieldInRecord("pnd_W_Parm_Date", "#W-PARM-DATE", FieldType.STRING, 8);

        pnd_Grand_Tot_Ovrpy_Tiaa = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Tiaa", "#GRAND-TOT-OVRPY-TIAA", FieldType.DECIMAL, 11,2);

        pnd_Grand_Tot_Ovrpy_Stock = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Stock", "#GRAND-TOT-OVRPY-STOCK", FieldType.DECIMAL, 11,2);

        pnd_Grand_Tot_Ovrpy_Money_Market = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Money_Market", "#GRAND-TOT-OVRPY-MONEY-MARKET", FieldType.DECIMAL, 11,
            2);

        pnd_Grand_Tot_Ovrpy_Social_Choice = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Social_Choice", "#GRAND-TOT-OVRPY-SOCIAL-CHOICE", FieldType.DECIMAL, 
            11,2);

        pnd_Grand_Tot_Ovrpy_Global_Equity = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Global_Equity", "#GRAND-TOT-OVRPY-GLOBAL-EQUITY", FieldType.DECIMAL, 
            11,2);

        pnd_Grand_Tot_Ovrpy_Growth = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Growth", "#GRAND-TOT-OVRPY-GROWTH", FieldType.DECIMAL, 11,2);

        pnd_Grand_Tot_Ovrpy_Equity_Index = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Equity_Index", "#GRAND-TOT-OVRPY-EQUITY-INDEX", FieldType.DECIMAL, 11,
            2);

        pnd_Grand_Tot_Ovrpy_Real_Estate = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Real_Estate", "#GRAND-TOT-OVRPY-REAL-ESTATE", FieldType.DECIMAL, 11,2);

        pnd_Grand_Tot_Ovrpy_Bond = newFieldInRecord("pnd_Grand_Tot_Ovrpy_Bond", "#GRAND-TOT-OVRPY-BOND", FieldType.DECIMAL, 11,2);

        pnd_Grand_Rate_Guar_Ovp = newFieldInRecord("pnd_Grand_Rate_Guar_Ovp", "#GRAND-RATE-GUAR-OVP", FieldType.DECIMAL, 11,2);

        pnd_Grand_Rate_Divd_Ovp = newFieldInRecord("pnd_Grand_Rate_Divd_Ovp", "#GRAND-RATE-DIVD-OVP", FieldType.DECIMAL, 11,2);

        pnd_Num_Of_Tiaa = newFieldInRecord("pnd_Num_Of_Tiaa", "#NUM-OF-TIAA", FieldType.NUMERIC, 6);

        pnd_Num_Of_Stock = newFieldInRecord("pnd_Num_Of_Stock", "#NUM-OF-STOCK", FieldType.NUMERIC, 6);

        pnd_Num_Of_Money_Market = newFieldInRecord("pnd_Num_Of_Money_Market", "#NUM-OF-MONEY-MARKET", FieldType.NUMERIC, 6);

        pnd_Num_Of_Social_Choice = newFieldInRecord("pnd_Num_Of_Social_Choice", "#NUM-OF-SOCIAL-CHOICE", FieldType.NUMERIC, 6);

        pnd_Num_Of_Global_Equity = newFieldInRecord("pnd_Num_Of_Global_Equity", "#NUM-OF-GLOBAL-EQUITY", FieldType.NUMERIC, 6);

        pnd_Num_Of_Growth = newFieldInRecord("pnd_Num_Of_Growth", "#NUM-OF-GROWTH", FieldType.NUMERIC, 6);

        pnd_Num_Of_Equity_Index = newFieldInRecord("pnd_Num_Of_Equity_Index", "#NUM-OF-EQUITY-INDEX", FieldType.NUMERIC, 6);

        pnd_Num_Of_Real_Estate = newFieldInRecord("pnd_Num_Of_Real_Estate", "#NUM-OF-REAL-ESTATE", FieldType.NUMERIC, 6);

        pnd_Num_Of_Bond = newFieldInRecord("pnd_Num_Of_Bond", "#NUM-OF-BOND", FieldType.NUMERIC, 6);

        pnd_W_Last_Pay_Date = newFieldInRecord("pnd_W_Last_Pay_Date", "#W-LAST-PAY-DATE", FieldType.NUMERIC, 8);
        pnd_W_Last_Pay_DateRedef25 = newGroupInRecord("pnd_W_Last_Pay_DateRedef25", "Redefines", pnd_W_Last_Pay_Date);
        pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Yyyy = pnd_W_Last_Pay_DateRedef25.newFieldInGroup("pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Yyyy", "#W-LAST-PAY-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm = pnd_W_Last_Pay_DateRedef25.newFieldInGroup("pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm", "#W-LAST-PAY-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_MmRedef26 = pnd_W_Last_Pay_DateRedef25.newGroupInGroup("pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_MmRedef26", 
            "Redefines", pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm);
        pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A = pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_MmRedef26.newFieldInGroup("pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A", 
            "#W-LAST-PAY-DATE-MM-A", FieldType.STRING, 2);
        pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Dd = pnd_W_Last_Pay_DateRedef25.newFieldInGroup("pnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Dd", "#W-LAST-PAY-DATE-DD", 
            FieldType.NUMERIC, 2);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef27 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef27", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef27.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_Date_NRedef28 = iaa_Parm_Card_Pnd_Parm_DateRedef27.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Date_NRedef28", "Redefines", 
            iaa_Parm_Card_Pnd_Parm_Date_N);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_Date_NRedef28.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_Date_NRedef28.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_Date_NRedef28.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_Date_NRedef28.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Fl_Date_Yyyymmdd_Alph = newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Fl_Date_Yyyymmdd_AlphRedef29 = newGroupInRecord("pnd_Fl_Date_Yyyymmdd_AlphRedef29", "Redefines", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_AlphRedef29.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30 = pnd_Fl_Date_Yyyymmdd_AlphRedef29.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30", 
            "Redefines", pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef30.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd", 
            "#FL-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Fl_Time_Hhiiss_Alph = newFieldInRecord("pnd_Fl_Time_Hhiiss_Alph", "#FL-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Fl_Time_Hhiiss_AlphRedef31 = newGroupInRecord("pnd_Fl_Time_Hhiiss_AlphRedef31", "Redefines", pnd_Fl_Time_Hhiiss_Alph);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num = pnd_Fl_Time_Hhiiss_AlphRedef31.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num", 
            "#FL-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32 = pnd_Fl_Time_Hhiiss_AlphRedef31.newGroupInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32", 
            "Redefines", pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh", 
            "#FL-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii", 
            "#FL-TIME-II", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef32.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss", 
            "#FL-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef33 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef33", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef33.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34 = pnd_Sy_Date_Yymmdd_AlphRedef33.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef34.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef35 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef35", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef35.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36 = pnd_Sy_Time_Hhiiss_AlphRedef35.newGroupInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36", 
            "Redefines", pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh", 
            "#SY-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii", 
            "#SY-TIME-II", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef36.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss", 
            "#SY-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        pnd_Sys_Time = newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);

        vw_iaa_Old_Tiaa_Rates_View = new DataAccessProgramView(new NameInfo("vw_iaa_Old_Tiaa_Rates_View", "IAA-OLD-TIAA-RATES-VIEW"), "IAA_OLD_TIAA_RATES", 
            "IA_OLD_RATES");
        iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");
        iaa_Old_Tiaa_Rates_View_Cmpny_Fund_CdeRedef37 = vw_iaa_Old_Tiaa_Rates_View.getRecord().newGroupInGroup("iaa_Old_Tiaa_Rates_View_Cmpny_Fund_CdeRedef37", 
            "Redefines", iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde);
        iaa_Old_Tiaa_Rates_View_Cmpny_Cde = iaa_Old_Tiaa_Rates_View_Cmpny_Fund_CdeRedef37.newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cmpny_Cde", "CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Old_Tiaa_Rates_View_Fund_Cde = iaa_Old_Tiaa_Rates_View_Cmpny_Fund_CdeRedef37.newFieldInGroup("iaa_Old_Tiaa_Rates_View_Fund_Cde", "FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt", 
            "CNTRCT-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt", 
            "CNTRCT-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        iaa_Old_Tiaa_Rates_View_Cntrct_Py_Dte_Key = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Py_Dte_Key", 
            "CNTRCT-PY-DTE-KEY", FieldType.STRING, 23, RepeatingFieldStrategy.None, "CNTRCT_PY_DTE_KEY");

        pnd_Temp_Occur = newFieldInRecord("pnd_Temp_Occur", "#TEMP-OCCUR", FieldType.NUMERIC, 1);

        pnd_D = newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 1);

        pnd_E = newFieldInRecord("pnd_E", "#E", FieldType.NUMERIC, 1);

        pnd_Temp_Occur_2 = newFieldInRecord("pnd_Temp_Occur_2", "#TEMP-OCCUR-2", FieldType.NUMERIC, 1);

        pnd_Temp_Occur_3 = newFieldInRecord("pnd_Temp_Occur_3", "#TEMP-OCCUR-3", FieldType.NUMERIC, 2);

        pnd_Q = newFieldInRecord("pnd_Q", "#Q", FieldType.NUMERIC, 1);

        pnd_X1 = newFieldInRecord("pnd_X1", "#X1", FieldType.NUMERIC, 1);

        pnd_M1 = newFieldInRecord("pnd_M1", "#M1", FieldType.NUMERIC, 2);

        pnd_Ovp_Cnt = newFieldInRecord("pnd_Ovp_Cnt", "#OVP-CNT", FieldType.NUMERIC, 2);

        pnd_Earlier_Death = newFieldInRecord("pnd_Earlier_Death", "#EARLIER-DEATH", FieldType.STRING, 1);

        this.setRecordName("LdaIaal584w");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct_View.reset();
        vw_new_Tiaa_Rates.reset();
        vw_iaa_Old_Tiaa_Rates_View.reset();
    }

    // Constructor
    public LdaIaal584w() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
