/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:47 PM
**        * FROM NATURAL PDA     : IATA002
************************************************************
**        * FILE NAME            : PdaIata002.java
**        * CLASS NAME           : PdaIata002
**        * INSTANCE NAME        : PdaIata002
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIata002 extends PdaBase
{
    // Properties
    private DbsGroup iata002;
    private DbsField iata002_Pnd_Contract_Payee;
    private DbsField iata002_Pnd_Mode;
    private DbsField iata002_Pnd_Origin;
    private DbsField iata002_Pnd_Option;
    private DbsField iata002_Pnd_Issue_Date_8;
    private DbsGroup iata002_Pnd_Issue_Date_8Redef1;
    private DbsField iata002_Pnd_Issue_Year;
    private DbsField iata002_Pnd_Issue_Month;
    private DbsField iata002_Pnd_Issue_Day;
    private DbsField iata002_Pnd_Final_Per_Pay_Date;
    private DbsGroup iata002_Pnd_Final_Per_Pay_DateRedef2;
    private DbsField iata002_Pnd_Final_Per_Pay_Year;
    private DbsField iata002_Pnd_Final_Per_Pay_Month;
    private DbsField iata002_Pnd_First_Ann_Dob;
    private DbsGroup iata002_Pnd_First_Ann_DobRedef3;
    private DbsField iata002_Pnd_First_Ann_Dob_Year;
    private DbsField iata002_Pnd_First_Ann_Dob_Month;
    private DbsField iata002_Pnd_First_Ann_Dob_Day;
    private DbsField iata002_Pnd_First_Ann_Sex;
    private DbsField iata002_Pnd_First_Ann_Dod;
    private DbsGroup iata002_Pnd_First_Ann_DodRedef4;
    private DbsField iata002_Pnd_First_Ann_Dod_Year;
    private DbsField iata002_Pnd_First_Ann_Dod_Month;
    private DbsField iata002_Pnd_Second_Ann_Dob;
    private DbsGroup iata002_Pnd_Second_Ann_DobRedef5;
    private DbsField iata002_Pnd_Second_Ann_Dob_Year;
    private DbsField iata002_Pnd_Second_Ann_Dob_Month;
    private DbsField iata002_Pnd_Second_Ann_Dob_Day;
    private DbsField iata002_Pnd_Second_Ann_Sex;
    private DbsField iata002_Pnd_Second_Ann_Dod;
    private DbsGroup iata002_Pnd_Second_Ann_DodRedef6;
    private DbsField iata002_Pnd_Second_Ann_Dod_Year;
    private DbsField iata002_Pnd_Second_Ann_Dod_Month;
    private DbsField iata002_Pnd_Transfer_Effective_Date;
    private DbsField iata002_Pnd_Type_Of_Run;
    private DbsField iata002_Pnd_Processing_Date;
    private DbsGroup iata002_Pnd_Tiaa_Grd_Array;
    private DbsField iata002_Pnd_Rate_Code_Grd;
    private DbsField iata002_Pnd_Gic_Cde_G;
    private DbsField iata002_Pnd_Gtd_Pmt_Grd;
    private DbsField iata002_Pnd_Dvd_Pmt_Grd;
    private DbsField iata002_Pnd_Rate_Date_Grd;
    private DbsField iata002_Pnd_Next_Pymnt_Dte;
    private DbsField iata002_Pnd_Return_Code;
    private DbsGroup iata002_Pnd_Return_CodeRedef7;
    private DbsField iata002_Pnd_Return_Code_Pgm;
    private DbsField iata002_Pnd_Return_Code_Nbr;
    private DbsGroup iata002_Pnd_Tiaa_Std_Array;
    private DbsField iata002_Pnd_Rate_Code_Std;
    private DbsField iata002_Pnd_Gic_Cde_S;
    private DbsField iata002_Pnd_Gtd_Pmt_Std;
    private DbsField iata002_Pnd_Dvd_Pmt_Std;
    private DbsField iata002_Pnd_Graded_Annuity_Factor;
    private DbsField iata002_Pnd_Graded_Trnsfr_Amt;
    private DbsField iata002_Pnd_Tot_Trnsfr_Amt;
    private DbsGroup iata002_Pnd_Filler;
    private DbsField iata002_Pnd_Modal_Adjustment;
    private DbsField iata002_Pnd_Standard_Annuity_Factor;
    private DbsField iata002_Pnd_Standard_Rate;
    private DbsField iata002_Pnd_Prorated_Std_Grd_Ratio;
    private DbsField iata002_Pnd_Graded_Interest_Rate;
    private DbsField iata002_Pnd_Mortality_Table;
    private DbsField iata002_Pnd_Setback;
    private DbsField iata002_Pnd_Base_Interest_Rate;
    private DbsField iata002_Pnd_Current_Option_Status;
    private DbsField iata002_Pnd_Current_Pct_To_Second_Ann;
    private DbsField iata002_Pnd_Standard_Deferred_Period;
    private DbsField iata002_Pnd_Standard_Certain_Period;
    private DbsField iata002_Pnd_Graded_Deferred_Period;
    private DbsField iata002_Pnd_Graded_Certain_Period;
    private DbsField iata002_Pnd_Age1;
    private DbsField iata002_Pnd_Age2;

    public DbsGroup getIata002() { return iata002; }

    public DbsField getIata002_Pnd_Contract_Payee() { return iata002_Pnd_Contract_Payee; }

    public DbsField getIata002_Pnd_Mode() { return iata002_Pnd_Mode; }

    public DbsField getIata002_Pnd_Origin() { return iata002_Pnd_Origin; }

    public DbsField getIata002_Pnd_Option() { return iata002_Pnd_Option; }

    public DbsField getIata002_Pnd_Issue_Date_8() { return iata002_Pnd_Issue_Date_8; }

    public DbsGroup getIata002_Pnd_Issue_Date_8Redef1() { return iata002_Pnd_Issue_Date_8Redef1; }

    public DbsField getIata002_Pnd_Issue_Year() { return iata002_Pnd_Issue_Year; }

    public DbsField getIata002_Pnd_Issue_Month() { return iata002_Pnd_Issue_Month; }

    public DbsField getIata002_Pnd_Issue_Day() { return iata002_Pnd_Issue_Day; }

    public DbsField getIata002_Pnd_Final_Per_Pay_Date() { return iata002_Pnd_Final_Per_Pay_Date; }

    public DbsGroup getIata002_Pnd_Final_Per_Pay_DateRedef2() { return iata002_Pnd_Final_Per_Pay_DateRedef2; }

    public DbsField getIata002_Pnd_Final_Per_Pay_Year() { return iata002_Pnd_Final_Per_Pay_Year; }

    public DbsField getIata002_Pnd_Final_Per_Pay_Month() { return iata002_Pnd_Final_Per_Pay_Month; }

    public DbsField getIata002_Pnd_First_Ann_Dob() { return iata002_Pnd_First_Ann_Dob; }

    public DbsGroup getIata002_Pnd_First_Ann_DobRedef3() { return iata002_Pnd_First_Ann_DobRedef3; }

    public DbsField getIata002_Pnd_First_Ann_Dob_Year() { return iata002_Pnd_First_Ann_Dob_Year; }

    public DbsField getIata002_Pnd_First_Ann_Dob_Month() { return iata002_Pnd_First_Ann_Dob_Month; }

    public DbsField getIata002_Pnd_First_Ann_Dob_Day() { return iata002_Pnd_First_Ann_Dob_Day; }

    public DbsField getIata002_Pnd_First_Ann_Sex() { return iata002_Pnd_First_Ann_Sex; }

    public DbsField getIata002_Pnd_First_Ann_Dod() { return iata002_Pnd_First_Ann_Dod; }

    public DbsGroup getIata002_Pnd_First_Ann_DodRedef4() { return iata002_Pnd_First_Ann_DodRedef4; }

    public DbsField getIata002_Pnd_First_Ann_Dod_Year() { return iata002_Pnd_First_Ann_Dod_Year; }

    public DbsField getIata002_Pnd_First_Ann_Dod_Month() { return iata002_Pnd_First_Ann_Dod_Month; }

    public DbsField getIata002_Pnd_Second_Ann_Dob() { return iata002_Pnd_Second_Ann_Dob; }

    public DbsGroup getIata002_Pnd_Second_Ann_DobRedef5() { return iata002_Pnd_Second_Ann_DobRedef5; }

    public DbsField getIata002_Pnd_Second_Ann_Dob_Year() { return iata002_Pnd_Second_Ann_Dob_Year; }

    public DbsField getIata002_Pnd_Second_Ann_Dob_Month() { return iata002_Pnd_Second_Ann_Dob_Month; }

    public DbsField getIata002_Pnd_Second_Ann_Dob_Day() { return iata002_Pnd_Second_Ann_Dob_Day; }

    public DbsField getIata002_Pnd_Second_Ann_Sex() { return iata002_Pnd_Second_Ann_Sex; }

    public DbsField getIata002_Pnd_Second_Ann_Dod() { return iata002_Pnd_Second_Ann_Dod; }

    public DbsGroup getIata002_Pnd_Second_Ann_DodRedef6() { return iata002_Pnd_Second_Ann_DodRedef6; }

    public DbsField getIata002_Pnd_Second_Ann_Dod_Year() { return iata002_Pnd_Second_Ann_Dod_Year; }

    public DbsField getIata002_Pnd_Second_Ann_Dod_Month() { return iata002_Pnd_Second_Ann_Dod_Month; }

    public DbsField getIata002_Pnd_Transfer_Effective_Date() { return iata002_Pnd_Transfer_Effective_Date; }

    public DbsField getIata002_Pnd_Type_Of_Run() { return iata002_Pnd_Type_Of_Run; }

    public DbsField getIata002_Pnd_Processing_Date() { return iata002_Pnd_Processing_Date; }

    public DbsGroup getIata002_Pnd_Tiaa_Grd_Array() { return iata002_Pnd_Tiaa_Grd_Array; }

    public DbsField getIata002_Pnd_Rate_Code_Grd() { return iata002_Pnd_Rate_Code_Grd; }

    public DbsField getIata002_Pnd_Gic_Cde_G() { return iata002_Pnd_Gic_Cde_G; }

    public DbsField getIata002_Pnd_Gtd_Pmt_Grd() { return iata002_Pnd_Gtd_Pmt_Grd; }

    public DbsField getIata002_Pnd_Dvd_Pmt_Grd() { return iata002_Pnd_Dvd_Pmt_Grd; }

    public DbsField getIata002_Pnd_Rate_Date_Grd() { return iata002_Pnd_Rate_Date_Grd; }

    public DbsField getIata002_Pnd_Next_Pymnt_Dte() { return iata002_Pnd_Next_Pymnt_Dte; }

    public DbsField getIata002_Pnd_Return_Code() { return iata002_Pnd_Return_Code; }

    public DbsGroup getIata002_Pnd_Return_CodeRedef7() { return iata002_Pnd_Return_CodeRedef7; }

    public DbsField getIata002_Pnd_Return_Code_Pgm() { return iata002_Pnd_Return_Code_Pgm; }

    public DbsField getIata002_Pnd_Return_Code_Nbr() { return iata002_Pnd_Return_Code_Nbr; }

    public DbsGroup getIata002_Pnd_Tiaa_Std_Array() { return iata002_Pnd_Tiaa_Std_Array; }

    public DbsField getIata002_Pnd_Rate_Code_Std() { return iata002_Pnd_Rate_Code_Std; }

    public DbsField getIata002_Pnd_Gic_Cde_S() { return iata002_Pnd_Gic_Cde_S; }

    public DbsField getIata002_Pnd_Gtd_Pmt_Std() { return iata002_Pnd_Gtd_Pmt_Std; }

    public DbsField getIata002_Pnd_Dvd_Pmt_Std() { return iata002_Pnd_Dvd_Pmt_Std; }

    public DbsField getIata002_Pnd_Graded_Annuity_Factor() { return iata002_Pnd_Graded_Annuity_Factor; }

    public DbsField getIata002_Pnd_Graded_Trnsfr_Amt() { return iata002_Pnd_Graded_Trnsfr_Amt; }

    public DbsField getIata002_Pnd_Tot_Trnsfr_Amt() { return iata002_Pnd_Tot_Trnsfr_Amt; }

    public DbsGroup getIata002_Pnd_Filler() { return iata002_Pnd_Filler; }

    public DbsField getIata002_Pnd_Modal_Adjustment() { return iata002_Pnd_Modal_Adjustment; }

    public DbsField getIata002_Pnd_Standard_Annuity_Factor() { return iata002_Pnd_Standard_Annuity_Factor; }

    public DbsField getIata002_Pnd_Standard_Rate() { return iata002_Pnd_Standard_Rate; }

    public DbsField getIata002_Pnd_Prorated_Std_Grd_Ratio() { return iata002_Pnd_Prorated_Std_Grd_Ratio; }

    public DbsField getIata002_Pnd_Graded_Interest_Rate() { return iata002_Pnd_Graded_Interest_Rate; }

    public DbsField getIata002_Pnd_Mortality_Table() { return iata002_Pnd_Mortality_Table; }

    public DbsField getIata002_Pnd_Setback() { return iata002_Pnd_Setback; }

    public DbsField getIata002_Pnd_Base_Interest_Rate() { return iata002_Pnd_Base_Interest_Rate; }

    public DbsField getIata002_Pnd_Current_Option_Status() { return iata002_Pnd_Current_Option_Status; }

    public DbsField getIata002_Pnd_Current_Pct_To_Second_Ann() { return iata002_Pnd_Current_Pct_To_Second_Ann; }

    public DbsField getIata002_Pnd_Standard_Deferred_Period() { return iata002_Pnd_Standard_Deferred_Period; }

    public DbsField getIata002_Pnd_Standard_Certain_Period() { return iata002_Pnd_Standard_Certain_Period; }

    public DbsField getIata002_Pnd_Graded_Deferred_Period() { return iata002_Pnd_Graded_Deferred_Period; }

    public DbsField getIata002_Pnd_Graded_Certain_Period() { return iata002_Pnd_Graded_Certain_Period; }

    public DbsField getIata002_Pnd_Age1() { return iata002_Pnd_Age1; }

    public DbsField getIata002_Pnd_Age2() { return iata002_Pnd_Age2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iata002 = dbsRecord.newGroupInRecord("iata002", "IATA002");
        iata002.setParameterOption(ParameterOption.ByReference);
        iata002_Pnd_Contract_Payee = iata002.newFieldInGroup("iata002_Pnd_Contract_Payee", "#CONTRACT-PAYEE", FieldType.STRING, 10);
        iata002_Pnd_Mode = iata002.newFieldInGroup("iata002_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        iata002_Pnd_Origin = iata002.newFieldInGroup("iata002_Pnd_Origin", "#ORIGIN", FieldType.NUMERIC, 2);
        iata002_Pnd_Option = iata002.newFieldInGroup("iata002_Pnd_Option", "#OPTION", FieldType.NUMERIC, 2);
        iata002_Pnd_Issue_Date_8 = iata002.newFieldInGroup("iata002_Pnd_Issue_Date_8", "#ISSUE-DATE-8", FieldType.NUMERIC, 8);
        iata002_Pnd_Issue_Date_8Redef1 = iata002.newGroupInGroup("iata002_Pnd_Issue_Date_8Redef1", "Redefines", iata002_Pnd_Issue_Date_8);
        iata002_Pnd_Issue_Year = iata002_Pnd_Issue_Date_8Redef1.newFieldInGroup("iata002_Pnd_Issue_Year", "#ISSUE-YEAR", FieldType.NUMERIC, 4);
        iata002_Pnd_Issue_Month = iata002_Pnd_Issue_Date_8Redef1.newFieldInGroup("iata002_Pnd_Issue_Month", "#ISSUE-MONTH", FieldType.NUMERIC, 2);
        iata002_Pnd_Issue_Day = iata002_Pnd_Issue_Date_8Redef1.newFieldInGroup("iata002_Pnd_Issue_Day", "#ISSUE-DAY", FieldType.NUMERIC, 2);
        iata002_Pnd_Final_Per_Pay_Date = iata002.newFieldInGroup("iata002_Pnd_Final_Per_Pay_Date", "#FINAL-PER-PAY-DATE", FieldType.NUMERIC, 6);
        iata002_Pnd_Final_Per_Pay_DateRedef2 = iata002.newGroupInGroup("iata002_Pnd_Final_Per_Pay_DateRedef2", "Redefines", iata002_Pnd_Final_Per_Pay_Date);
        iata002_Pnd_Final_Per_Pay_Year = iata002_Pnd_Final_Per_Pay_DateRedef2.newFieldInGroup("iata002_Pnd_Final_Per_Pay_Year", "#FINAL-PER-PAY-YEAR", 
            FieldType.NUMERIC, 4);
        iata002_Pnd_Final_Per_Pay_Month = iata002_Pnd_Final_Per_Pay_DateRedef2.newFieldInGroup("iata002_Pnd_Final_Per_Pay_Month", "#FINAL-PER-PAY-MONTH", 
            FieldType.NUMERIC, 2);
        iata002_Pnd_First_Ann_Dob = iata002.newFieldInGroup("iata002_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        iata002_Pnd_First_Ann_DobRedef3 = iata002.newGroupInGroup("iata002_Pnd_First_Ann_DobRedef3", "Redefines", iata002_Pnd_First_Ann_Dob);
        iata002_Pnd_First_Ann_Dob_Year = iata002_Pnd_First_Ann_DobRedef3.newFieldInGroup("iata002_Pnd_First_Ann_Dob_Year", "#FIRST-ANN-DOB-YEAR", FieldType.NUMERIC, 
            4);
        iata002_Pnd_First_Ann_Dob_Month = iata002_Pnd_First_Ann_DobRedef3.newFieldInGroup("iata002_Pnd_First_Ann_Dob_Month", "#FIRST-ANN-DOB-MONTH", FieldType.NUMERIC, 
            2);
        iata002_Pnd_First_Ann_Dob_Day = iata002_Pnd_First_Ann_DobRedef3.newFieldInGroup("iata002_Pnd_First_Ann_Dob_Day", "#FIRST-ANN-DOB-DAY", FieldType.NUMERIC, 
            2);
        iata002_Pnd_First_Ann_Sex = iata002.newFieldInGroup("iata002_Pnd_First_Ann_Sex", "#FIRST-ANN-SEX", FieldType.NUMERIC, 1);
        iata002_Pnd_First_Ann_Dod = iata002.newFieldInGroup("iata002_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.NUMERIC, 6);
        iata002_Pnd_First_Ann_DodRedef4 = iata002.newGroupInGroup("iata002_Pnd_First_Ann_DodRedef4", "Redefines", iata002_Pnd_First_Ann_Dod);
        iata002_Pnd_First_Ann_Dod_Year = iata002_Pnd_First_Ann_DodRedef4.newFieldInGroup("iata002_Pnd_First_Ann_Dod_Year", "#FIRST-ANN-DOD-YEAR", FieldType.NUMERIC, 
            4);
        iata002_Pnd_First_Ann_Dod_Month = iata002_Pnd_First_Ann_DodRedef4.newFieldInGroup("iata002_Pnd_First_Ann_Dod_Month", "#FIRST-ANN-DOD-MONTH", FieldType.NUMERIC, 
            2);
        iata002_Pnd_Second_Ann_Dob = iata002.newFieldInGroup("iata002_Pnd_Second_Ann_Dob", "#SECOND-ANN-DOB", FieldType.NUMERIC, 8);
        iata002_Pnd_Second_Ann_DobRedef5 = iata002.newGroupInGroup("iata002_Pnd_Second_Ann_DobRedef5", "Redefines", iata002_Pnd_Second_Ann_Dob);
        iata002_Pnd_Second_Ann_Dob_Year = iata002_Pnd_Second_Ann_DobRedef5.newFieldInGroup("iata002_Pnd_Second_Ann_Dob_Year", "#SECOND-ANN-DOB-YEAR", 
            FieldType.NUMERIC, 4);
        iata002_Pnd_Second_Ann_Dob_Month = iata002_Pnd_Second_Ann_DobRedef5.newFieldInGroup("iata002_Pnd_Second_Ann_Dob_Month", "#SECOND-ANN-DOB-MONTH", 
            FieldType.NUMERIC, 2);
        iata002_Pnd_Second_Ann_Dob_Day = iata002_Pnd_Second_Ann_DobRedef5.newFieldInGroup("iata002_Pnd_Second_Ann_Dob_Day", "#SECOND-ANN-DOB-DAY", FieldType.NUMERIC, 
            2);
        iata002_Pnd_Second_Ann_Sex = iata002.newFieldInGroup("iata002_Pnd_Second_Ann_Sex", "#SECOND-ANN-SEX", FieldType.NUMERIC, 1);
        iata002_Pnd_Second_Ann_Dod = iata002.newFieldInGroup("iata002_Pnd_Second_Ann_Dod", "#SECOND-ANN-DOD", FieldType.NUMERIC, 6);
        iata002_Pnd_Second_Ann_DodRedef6 = iata002.newGroupInGroup("iata002_Pnd_Second_Ann_DodRedef6", "Redefines", iata002_Pnd_Second_Ann_Dod);
        iata002_Pnd_Second_Ann_Dod_Year = iata002_Pnd_Second_Ann_DodRedef6.newFieldInGroup("iata002_Pnd_Second_Ann_Dod_Year", "#SECOND-ANN-DOD-YEAR", 
            FieldType.NUMERIC, 4);
        iata002_Pnd_Second_Ann_Dod_Month = iata002_Pnd_Second_Ann_DodRedef6.newFieldInGroup("iata002_Pnd_Second_Ann_Dod_Month", "#SECOND-ANN-DOD-MONTH", 
            FieldType.NUMERIC, 2);
        iata002_Pnd_Transfer_Effective_Date = iata002.newFieldInGroup("iata002_Pnd_Transfer_Effective_Date", "#TRANSFER-EFFECTIVE-DATE", FieldType.NUMERIC, 
            8);
        iata002_Pnd_Type_Of_Run = iata002.newFieldInGroup("iata002_Pnd_Type_Of_Run", "#TYPE-OF-RUN", FieldType.STRING, 1);
        iata002_Pnd_Processing_Date = iata002.newFieldInGroup("iata002_Pnd_Processing_Date", "#PROCESSING-DATE", FieldType.NUMERIC, 8);
        iata002_Pnd_Tiaa_Grd_Array = iata002.newGroupArrayInGroup("iata002_Pnd_Tiaa_Grd_Array", "#TIAA-GRD-ARRAY", new DbsArrayController(1,250));
        iata002_Pnd_Rate_Code_Grd = iata002_Pnd_Tiaa_Grd_Array.newFieldInGroup("iata002_Pnd_Rate_Code_Grd", "#RATE-CODE-GRD", FieldType.STRING, 2);
        iata002_Pnd_Gic_Cde_G = iata002_Pnd_Tiaa_Grd_Array.newFieldInGroup("iata002_Pnd_Gic_Cde_G", "#GIC-CDE-G", FieldType.NUMERIC, 11);
        iata002_Pnd_Gtd_Pmt_Grd = iata002_Pnd_Tiaa_Grd_Array.newFieldInGroup("iata002_Pnd_Gtd_Pmt_Grd", "#GTD-PMT-GRD", FieldType.DECIMAL, 9,2);
        iata002_Pnd_Dvd_Pmt_Grd = iata002_Pnd_Tiaa_Grd_Array.newFieldInGroup("iata002_Pnd_Dvd_Pmt_Grd", "#DVD-PMT-GRD", FieldType.DECIMAL, 9,2);
        iata002_Pnd_Rate_Date_Grd = iata002_Pnd_Tiaa_Grd_Array.newFieldInGroup("iata002_Pnd_Rate_Date_Grd", "#RATE-DATE-GRD", FieldType.DATE);
        iata002_Pnd_Next_Pymnt_Dte = iata002.newFieldInGroup("iata002_Pnd_Next_Pymnt_Dte", "#NEXT-PYMNT-DTE", FieldType.DATE);
        iata002_Pnd_Return_Code = iata002.newFieldInGroup("iata002_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 11);
        iata002_Pnd_Return_CodeRedef7 = iata002.newGroupInGroup("iata002_Pnd_Return_CodeRedef7", "Redefines", iata002_Pnd_Return_Code);
        iata002_Pnd_Return_Code_Pgm = iata002_Pnd_Return_CodeRedef7.newFieldInGroup("iata002_Pnd_Return_Code_Pgm", "#RETURN-CODE-PGM", FieldType.STRING, 
            8);
        iata002_Pnd_Return_Code_Nbr = iata002_Pnd_Return_CodeRedef7.newFieldInGroup("iata002_Pnd_Return_Code_Nbr", "#RETURN-CODE-NBR", FieldType.NUMERIC, 
            3);
        iata002_Pnd_Tiaa_Std_Array = iata002.newGroupArrayInGroup("iata002_Pnd_Tiaa_Std_Array", "#TIAA-STD-ARRAY", new DbsArrayController(1,250));
        iata002_Pnd_Rate_Code_Std = iata002_Pnd_Tiaa_Std_Array.newFieldInGroup("iata002_Pnd_Rate_Code_Std", "#RATE-CODE-STD", FieldType.STRING, 2);
        iata002_Pnd_Gic_Cde_S = iata002_Pnd_Tiaa_Std_Array.newFieldInGroup("iata002_Pnd_Gic_Cde_S", "#GIC-CDE-S", FieldType.NUMERIC, 11);
        iata002_Pnd_Gtd_Pmt_Std = iata002_Pnd_Tiaa_Std_Array.newFieldInGroup("iata002_Pnd_Gtd_Pmt_Std", "#GTD-PMT-STD", FieldType.DECIMAL, 9,2);
        iata002_Pnd_Dvd_Pmt_Std = iata002_Pnd_Tiaa_Std_Array.newFieldInGroup("iata002_Pnd_Dvd_Pmt_Std", "#DVD-PMT-STD", FieldType.DECIMAL, 9,2);
        iata002_Pnd_Graded_Annuity_Factor = iata002_Pnd_Tiaa_Std_Array.newFieldInGroup("iata002_Pnd_Graded_Annuity_Factor", "#GRADED-ANNUITY-FACTOR", 
            FieldType.DECIMAL, 8,5);
        iata002_Pnd_Graded_Trnsfr_Amt = iata002_Pnd_Tiaa_Std_Array.newFieldInGroup("iata002_Pnd_Graded_Trnsfr_Amt", "#GRADED-TRNSFR-AMT", FieldType.DECIMAL, 
            11,2);
        iata002_Pnd_Tot_Trnsfr_Amt = iata002.newFieldInGroup("iata002_Pnd_Tot_Trnsfr_Amt", "#TOT-TRNSFR-AMT", FieldType.DECIMAL, 13,2);
        iata002_Pnd_Filler = iata002.newGroupArrayInGroup("iata002_Pnd_Filler", "#FILLER", new DbsArrayController(1,250));
        iata002_Pnd_Modal_Adjustment = iata002_Pnd_Filler.newFieldInGroup("iata002_Pnd_Modal_Adjustment", "#MODAL-ADJUSTMENT", FieldType.FLOAT, 8);
        iata002_Pnd_Standard_Annuity_Factor = iata002_Pnd_Filler.newFieldInGroup("iata002_Pnd_Standard_Annuity_Factor", "#STANDARD-ANNUITY-FACTOR", FieldType.DECIMAL, 
            8,5);
        iata002_Pnd_Standard_Rate = iata002_Pnd_Filler.newFieldInGroup("iata002_Pnd_Standard_Rate", "#STANDARD-RATE", FieldType.DECIMAL, 6,5);
        iata002_Pnd_Prorated_Std_Grd_Ratio = iata002.newFieldInGroup("iata002_Pnd_Prorated_Std_Grd_Ratio", "#PRORATED-STD-GRD-RATIO", FieldType.DECIMAL, 
            6,5);
        iata002_Pnd_Graded_Interest_Rate = iata002.newFieldInGroup("iata002_Pnd_Graded_Interest_Rate", "#GRADED-INTEREST-RATE", FieldType.DECIMAL, 5,5);
        iata002_Pnd_Mortality_Table = iata002.newFieldInGroup("iata002_Pnd_Mortality_Table", "#MORTALITY-TABLE", FieldType.NUMERIC, 4);
        iata002_Pnd_Setback = iata002.newFieldInGroup("iata002_Pnd_Setback", "#SETBACK", FieldType.DECIMAL, 7,5);
        iata002_Pnd_Base_Interest_Rate = iata002.newFieldInGroup("iata002_Pnd_Base_Interest_Rate", "#BASE-INTEREST-RATE", FieldType.DECIMAL, 5,5);
        iata002_Pnd_Current_Option_Status = iata002.newFieldInGroup("iata002_Pnd_Current_Option_Status", "#CURRENT-OPTION-STATUS", FieldType.STRING, 2);
        iata002_Pnd_Current_Pct_To_Second_Ann = iata002.newFieldInGroup("iata002_Pnd_Current_Pct_To_Second_Ann", "#CURRENT-PCT-TO-SECOND-ANN", FieldType.DECIMAL, 
            8,5);
        iata002_Pnd_Standard_Deferred_Period = iata002.newFieldInGroup("iata002_Pnd_Standard_Deferred_Period", "#STANDARD-DEFERRED-PERIOD", FieldType.NUMERIC, 
            3);
        iata002_Pnd_Standard_Certain_Period = iata002.newFieldInGroup("iata002_Pnd_Standard_Certain_Period", "#STANDARD-CERTAIN-PERIOD", FieldType.DECIMAL, 
            7,5);
        iata002_Pnd_Graded_Deferred_Period = iata002.newFieldInGroup("iata002_Pnd_Graded_Deferred_Period", "#GRADED-DEFERRED-PERIOD", FieldType.NUMERIC, 
            3);
        iata002_Pnd_Graded_Certain_Period = iata002.newFieldInGroup("iata002_Pnd_Graded_Certain_Period", "#GRADED-CERTAIN-PERIOD", FieldType.DECIMAL, 
            7,5);
        iata002_Pnd_Age1 = iata002.newFieldInGroup("iata002_Pnd_Age1", "#AGE1", FieldType.DECIMAL, 9,5);
        iata002_Pnd_Age2 = iata002.newFieldInGroup("iata002_Pnd_Age2", "#AGE2", FieldType.DECIMAL, 9,5);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIata002(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

