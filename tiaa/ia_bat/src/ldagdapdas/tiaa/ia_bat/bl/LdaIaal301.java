/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:50 PM
**        * FROM NATURAL LDA     : IAAL301
************************************************************
**        * FILE NAME            : LdaIaal301.java
**        * CLASS NAME           : LdaIaal301
**        * INSTANCE NAME        : LdaIaal301
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal301 extends DbsRecord
{
    // Properties
    private DbsField pnd_Count_Fund_Record_Reads;
    private DbsField pnd_Count_Master_File_Reads;
    private DbsField pnd_Count_Master_Record_Error;
    private DbsField pnd_Count_Fund_Extract;
    private DbsField pnd_Count_Contract_Extract;
    private DbsField pnd_Count_Deduction_Extract;
    private DbsField pnd_Count_Cpr_Extract;
    private DbsField pnd_Count_Active_Payees;
    private DbsField pnd_Count_Inactive_Payees;
    private DbsField pnd_Count;
    private DbsField pnd_Count2;
    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Payee;
    private DbsField pnd_Work_Record_1_Pnd_W1_Record_Code;
    private DbsField pnd_Work_Record_1_Pnd_W1_Optn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orgn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Acctng_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Issue_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Crrncy_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Type_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn;
    private DbsField pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Type;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Plan_Nmbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tax_Exmpt_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dob;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dod;
    private DbsField pnd_Work_Record_1_Pnd_W1_Sub_Plan_Nmbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orgntng_Sub_Plan_Nmbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orgntng_Cntrct_Nmbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Filler;
    private DbsField pnd_Work_Record_1_Pnd_W1_Filler2;
    private DbsGroup pnd_Work_Record_2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Record_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Work_Record_2_Pnd_W2_Actvty_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cash_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Company_Cd;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Percent;
    private DbsField pnd_Work_Record_2_Pnd_W2_Mode_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Pend_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Hold_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Pend_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cmbne_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Srce;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_State_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_State_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Local_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8;
    private DbsField pnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte;
    private DbsGroup pnd_Work_Record_3;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Payee;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Filler;
    private DbsField pnd_Work_Record_3_Pnd_W3_Filler2;
    private DbsField pnd_Work_Record_3_Pnd_W3_Filler3;
    private DbsField pnd_Work_Record_3_Pnd_W3_Filler4;
    private DbsField pnd_Per_Tot;
    private DbsField pnd_I;
    private DbsField pnd_All_Ok;
    private DataAccessProgramView vw_iaa_Master_File_View;
    private DbsField iaa_Master_File_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Master_File_View_Cntrct_Optn_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Orgn_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Acctng_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Issue_Dte;
    private DbsField iaa_Master_File_View_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Master_File_View_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Crrncy_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Type_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Master_File_View_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Master_File_View_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Master_File_View_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Master_File_View_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Master_File_View_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Master_File_View_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Master_File_View_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Master_File_View_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Master_File_View_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Master_File_View_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Master_File_View_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Master_File_View_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Master_File_View_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Master_File_View_Cpr_Id_Nbr;
    private DbsField iaa_Master_File_View_Lst_Trans_Dte;
    private DbsField iaa_Master_File_View_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Master_File_View_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Master_File_View_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Master_File_View_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Master_File_View_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Master_File_View_Cntrct_Actvty_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Master_File_View_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Master_File_View_Cntrct_Cash_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup iaa_Master_File_View_Cntrct_Company_Data;
    private DbsField iaa_Master_File_View_Cntrct_Company_Cd;
    private DbsField iaa_Master_File_View_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Master_File_View_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Ivc_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Rtb_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Rtb_Percent;
    private DbsField iaa_Master_File_View_Cntrct_Mode_Ind;
    private DbsField iaa_Master_File_View_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Master_File_View_Bnfcry_Xref_Ind;
    private DbsField iaa_Master_File_View_Bnfcry_Dod_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Pend_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Hold_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Pend_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Cmbne_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Spirt_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Spirt_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Spirt_Srce;
    private DbsField iaa_Master_File_View_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Master_File_View_Cntrct_State_Cde;
    private DbsField iaa_Master_File_View_Cntrct_State_Tax_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Local_Cde;
    private DbsField iaa_Master_File_View_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Master_File_View_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Master_File_View_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Master_File_View_Ddctn_Payee_Cde;
    private DbsField iaa_Master_File_View_Ddctn_Id_Nbr;
    private DbsField iaa_Master_File_View_Ddctn_Cde;
    private DbsField iaa_Master_File_View_Ddctn_Seq_Nbr;
    private DbsField iaa_Master_File_View_Ddctn_Payee;
    private DbsField iaa_Master_File_View_Ddctn_Per_Amt;
    private DbsField iaa_Master_File_View_Ddctn_Ytd_Amt;
    private DbsField iaa_Master_File_View_Ddctn_Pd_To_Dte;
    private DbsField iaa_Master_File_View_Ddctn_Tot_Amt;
    private DbsField iaa_Master_File_View_Ddctn_Intent_Cde;
    private DbsField iaa_Master_File_View_Ddctn_Strt_Dte;
    private DbsField iaa_Master_File_View_Ddctn_Stp_Dte;
    private DbsField iaa_Master_File_View_Ddctn_Final_Dte;
    private DbsField iaa_Master_File_View_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Master_File_View_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Master_File_View_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Type;
    private DbsField iaa_Master_File_View_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Master_File_View_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Master_File_View_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Master_File_View_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Master_File_View_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Master_File_View_Rllvr_Ivc_Ind;
    private DbsField iaa_Master_File_View_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Master_File_View_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Master_File_View_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Master_File_View_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Master_File_View_Rllvr_Pln_Admn_Ind;
    private DbsField iaa_Master_File_View_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Master_File_View_Roth_Ssnng_Dte;
    private DbsField iaa_Master_File_View_Roth_Dsblty_Dte;
    private DbsField iaa_Master_File_View_Cntrct_Ssnng_Dte;
    private DbsField iaa_Master_File_View_Plan_Nmbr;
    private DbsField iaa_Master_File_View_Tax_Exmpt_Ind;
    private DbsField iaa_Master_File_View_Orig_Ownr_Dob;
    private DbsField iaa_Master_File_View_Orig_Ownr_Dod;
    private DbsField iaa_Master_File_View_Sub_Plan_Nmbr;
    private DbsField iaa_Master_File_View_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Master_File_View_Orgntng_Cntrct_Nmbr;
    private DbsGroup pnd_Work_Record_Header;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Record_Code;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Check_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Process_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_1;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_2;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_3;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_4;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_5;
    private DbsGroup pnd_Work_Record_Trailer;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Record_Code;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Check_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Process_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_1;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_2;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_3;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_4;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_5;
    private DbsField pnd_Check_Date_Ccyymmdd;
    private DbsGroup pnd_Check_Date_CcyymmddRedef1;
    private DbsField pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N;
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_View;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key;

    public DbsField getPnd_Count_Fund_Record_Reads() { return pnd_Count_Fund_Record_Reads; }

    public DbsField getPnd_Count_Master_File_Reads() { return pnd_Count_Master_File_Reads; }

    public DbsField getPnd_Count_Master_Record_Error() { return pnd_Count_Master_Record_Error; }

    public DbsField getPnd_Count_Fund_Extract() { return pnd_Count_Fund_Extract; }

    public DbsField getPnd_Count_Contract_Extract() { return pnd_Count_Contract_Extract; }

    public DbsField getPnd_Count_Deduction_Extract() { return pnd_Count_Deduction_Extract; }

    public DbsField getPnd_Count_Cpr_Extract() { return pnd_Count_Cpr_Extract; }

    public DbsField getPnd_Count_Active_Payees() { return pnd_Count_Active_Payees; }

    public DbsField getPnd_Count_Inactive_Payees() { return pnd_Count_Inactive_Payees; }

    public DbsField getPnd_Count() { return pnd_Count; }

    public DbsField getPnd_Count2() { return pnd_Count2; }

    public DbsGroup getPnd_Work_Record_1() { return pnd_Work_Record_1; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Ppcn_Nbr() { return pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Payee() { return pnd_Work_Record_1_Pnd_W1_Payee; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Record_Code() { return pnd_Work_Record_1_Pnd_W1_Record_Code; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Optn_Cde() { return pnd_Work_Record_1_Pnd_W1_Optn_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Orgn_Cde() { return pnd_Work_Record_1_Pnd_W1_Orgn_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Acctng_Cde() { return pnd_Work_Record_1_Pnd_W1_Acctng_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Issue_Dte() { return pnd_Work_Record_1_Pnd_W1_Issue_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte() { return pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte() { return pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Crrncy_Cde() { return pnd_Work_Record_1_Pnd_W1_Crrncy_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Type_Cde() { return pnd_Work_Record_1_Pnd_W1_Type_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Pymnt_Mthd() { return pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde() { return pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind() { return pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr() { return pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde() { return pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind() { return pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte() { return pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde() { return pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt() { return pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte() { return pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind() { return pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte() { return pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde() { return pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte() { return pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt() { return pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn() { return pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Div_Payee_Cde() { return pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Div_Coll_Cde() { return pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde() { return pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte() { return pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Type() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Type; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte() { return pnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte() { return pnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte() { return pnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Plan_Nmbr() { return pnd_Work_Record_1_Pnd_W1_Plan_Nmbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Tax_Exmpt_Ind() { return pnd_Work_Record_1_Pnd_W1_Tax_Exmpt_Ind; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dob() { return pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dob; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dod() { return pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dod; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Sub_Plan_Nmbr() { return pnd_Work_Record_1_Pnd_W1_Sub_Plan_Nmbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Orgntng_Sub_Plan_Nmbr() { return pnd_Work_Record_1_Pnd_W1_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Orgntng_Cntrct_Nmbr() { return pnd_Work_Record_1_Pnd_W1_Orgntng_Cntrct_Nmbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Filler() { return pnd_Work_Record_1_Pnd_W1_Filler; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Filler2() { return pnd_Work_Record_1_Pnd_W1_Filler2; }

    public DbsGroup getPnd_Work_Record_2() { return pnd_Work_Record_2; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr() { return pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Part_Payee_Cde() { return pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Record_Cde() { return pnd_Work_Record_2_Pnd_W2_Record_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr() { return pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte() { return pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde() { return pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde() { return pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw() { return pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr() { return pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ() { return pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Actvty_Cde() { return pnd_Work_Record_2_Pnd_W2_Actvty_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Trmnte_Rsn() { return pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rwrttn_Ind() { return pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cash_Cde() { return pnd_Work_Record_2_Pnd_W2_Cash_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde() { return pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Company_Cd() { return pnd_Work_Record_2_Pnd_W2_Company_Cd; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind() { return pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt() { return pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt() { return pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Ivc_Amt() { return pnd_Work_Record_2_Pnd_W2_Ivc_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt() { return pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rtb_Amt() { return pnd_Work_Record_2_Pnd_W2_Rtb_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rtb_Percent() { return pnd_Work_Record_2_Pnd_W2_Rtb_Percent; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Mode_Ind() { return pnd_Work_Record_2_Pnd_W2_Mode_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte() { return pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte() { return pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Final_Pay_Dte() { return pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind() { return pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte() { return pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Pend_Cde() { return pnd_Work_Record_2_Pnd_W2_Pend_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Hold_Cde() { return pnd_Work_Record_2_Pnd_W2_Hold_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Pend_Dte() { return pnd_Work_Record_2_Pnd_W2_Pend_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde() { return pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde() { return pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cmbne_Cde() { return pnd_Work_Record_2_Pnd_W2_Cmbne_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Spirt_Cde() { return pnd_Work_Record_2_Pnd_W2_Spirt_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Spirt_Amt() { return pnd_Work_Record_2_Pnd_W2_Spirt_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Spirt_Srce() { return pnd_Work_Record_2_Pnd_W2_Spirt_Srce; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte() { return pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte() { return pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt() { return pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_State_Cde() { return pnd_Work_Record_2_Pnd_W2_State_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_State_Tax_Amt() { return pnd_Work_Record_2_Pnd_W2_State_Tax_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Local_Cde() { return pnd_Work_Record_2_Pnd_W2_Local_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Local_Tax_Amt() { return pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte() { return pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde() { return pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde() { return pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte() { return pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr() { return pnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind() { return pnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind() { return pnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde() { return pnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde() { return pnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind() { return pnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8() { return pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte() { return pnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte; }

    public DbsGroup getPnd_Work_Record_3() { return pnd_Work_Record_3; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Cde() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Cde; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Payee() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Payee; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte() { return pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte() { return pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Filler() { return pnd_Work_Record_3_Pnd_W3_Filler; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Filler2() { return pnd_Work_Record_3_Pnd_W3_Filler2; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Filler3() { return pnd_Work_Record_3_Pnd_W3_Filler3; }

    public DbsField getPnd_Work_Record_3_Pnd_W3_Filler4() { return pnd_Work_Record_3_Pnd_W3_Filler4; }

    public DbsField getPnd_Per_Tot() { return pnd_Per_Tot; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_All_Ok() { return pnd_All_Ok; }

    public DataAccessProgramView getVw_iaa_Master_File_View() { return vw_iaa_Master_File_View; }

    public DbsField getIaa_Master_File_View_Cntrct_Ppcn_Nbr() { return iaa_Master_File_View_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Master_File_View_Cntrct_Optn_Cde() { return iaa_Master_File_View_Cntrct_Optn_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Orgn_Cde() { return iaa_Master_File_View_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Acctng_Cde() { return iaa_Master_File_View_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Issue_Dte() { return iaa_Master_File_View_Cntrct_Issue_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Pymnt_Due_Dte() { return iaa_Master_File_View_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Master_File_View_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Crrncy_Cde() { return iaa_Master_File_View_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Type_Cde() { return iaa_Master_File_View_Cntrct_Type_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Pymnt_Mthd() { return iaa_Master_File_View_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Master_File_View_Cntrct_Pnsn_Pln_Cde() { return iaa_Master_File_View_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Master_File_View_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Master_File_View_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Master_File_View_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Master_File_View_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Master_File_View_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Annt_Xref_Ind() { return iaa_Master_File_View_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Annt_Dob_Dte() { return iaa_Master_File_View_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Master_File_View_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Annt_Sex_Cde() { return iaa_Master_File_View_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Master_File_View_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Master_File_View_Cntrct_First_Annt_Dod_Dte() { return iaa_Master_File_View_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Master_File_View_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Master_File_View_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Master_File_View_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Master_File_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Master_File_View_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Master_File_View_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Master_File_View_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Master_File_View_Cntrct_Scnd_Annt_Ssn() { return iaa_Master_File_View_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Master_File_View_Cntrct_Div_Payee_Cde() { return iaa_Master_File_View_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Div_Coll_Cde() { return iaa_Master_File_View_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Inst_Iss_Cde() { return iaa_Master_File_View_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr() { return iaa_Master_File_View_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Master_File_View_Cntrct_Part_Payee_Cde() { return iaa_Master_File_View_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Master_File_View_Cpr_Id_Nbr() { return iaa_Master_File_View_Cpr_Id_Nbr; }

    public DbsField getIaa_Master_File_View_Lst_Trans_Dte() { return iaa_Master_File_View_Lst_Trans_Dte; }

    public DbsField getIaa_Master_File_View_Prtcpnt_Ctznshp_Cde() { return iaa_Master_File_View_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Master_File_View_Prtcpnt_Rsdncy_Cde() { return iaa_Master_File_View_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Master_File_View_Prtcpnt_Rsdncy_Sw() { return iaa_Master_File_View_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Master_File_View_Prtcpnt_Tax_Id_Nbr() { return iaa_Master_File_View_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Master_File_View_Prtcpnt_Tax_Id_Typ() { return iaa_Master_File_View_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Master_File_View_Cntrct_Actvty_Cde() { return iaa_Master_File_View_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Trmnte_Rsn() { return iaa_Master_File_View_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Master_File_View_Cntrct_Rwrttn_Ind() { return iaa_Master_File_View_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Master_File_View_Cntrct_Cash_Cde() { return iaa_Master_File_View_Cntrct_Cash_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Master_File_View_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getIaa_Master_File_View_Cntrct_Company_Data() { return iaa_Master_File_View_Cntrct_Company_Data; }

    public DbsField getIaa_Master_File_View_Cntrct_Company_Cd() { return iaa_Master_File_View_Cntrct_Company_Cd; }

    public DbsField getIaa_Master_File_View_Cntrct_Rcvry_Type_Ind() { return iaa_Master_File_View_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Master_File_View_Cntrct_Per_Ivc_Amt() { return iaa_Master_File_View_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Resdl_Ivc_Amt() { return iaa_Master_File_View_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Ivc_Amt() { return iaa_Master_File_View_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Ivc_Used_Amt() { return iaa_Master_File_View_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Rtb_Amt() { return iaa_Master_File_View_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Rtb_Percent() { return iaa_Master_File_View_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Master_File_View_Cntrct_Mode_Ind() { return iaa_Master_File_View_Cntrct_Mode_Ind; }

    public DbsField getIaa_Master_File_View_Cntrct_Wthdrwl_Dte() { return iaa_Master_File_View_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Final_Per_Pay_Dte() { return iaa_Master_File_View_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Final_Pay_Dte() { return iaa_Master_File_View_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Master_File_View_Bnfcry_Xref_Ind() { return iaa_Master_File_View_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Master_File_View_Bnfcry_Dod_Dte() { return iaa_Master_File_View_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Pend_Cde() { return iaa_Master_File_View_Cntrct_Pend_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Hold_Cde() { return iaa_Master_File_View_Cntrct_Hold_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Pend_Dte() { return iaa_Master_File_View_Cntrct_Pend_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Prev_Dist_Cde() { return iaa_Master_File_View_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Curr_Dist_Cde() { return iaa_Master_File_View_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Cmbne_Cde() { return iaa_Master_File_View_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Spirt_Cde() { return iaa_Master_File_View_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Spirt_Amt() { return iaa_Master_File_View_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Spirt_Srce() { return iaa_Master_File_View_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Master_File_View_Cntrct_Spirt_Arr_Dte() { return iaa_Master_File_View_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Spirt_Prcss_Dte() { return iaa_Master_File_View_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Fed_Tax_Amt() { return iaa_Master_File_View_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_State_Cde() { return iaa_Master_File_View_Cntrct_State_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_State_Tax_Amt() { return iaa_Master_File_View_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Local_Cde() { return iaa_Master_File_View_Cntrct_Local_Cde; }

    public DbsField getIaa_Master_File_View_Cntrct_Local_Tax_Amt() { return iaa_Master_File_View_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Master_File_View_Cntrct_Lst_Chnge_Dte() { return iaa_Master_File_View_Cntrct_Lst_Chnge_Dte; }

    public DbsField getIaa_Master_File_View_Ddctn_Ppcn_Nbr() { return iaa_Master_File_View_Ddctn_Ppcn_Nbr; }

    public DbsField getIaa_Master_File_View_Ddctn_Payee_Cde() { return iaa_Master_File_View_Ddctn_Payee_Cde; }

    public DbsField getIaa_Master_File_View_Ddctn_Id_Nbr() { return iaa_Master_File_View_Ddctn_Id_Nbr; }

    public DbsField getIaa_Master_File_View_Ddctn_Cde() { return iaa_Master_File_View_Ddctn_Cde; }

    public DbsField getIaa_Master_File_View_Ddctn_Seq_Nbr() { return iaa_Master_File_View_Ddctn_Seq_Nbr; }

    public DbsField getIaa_Master_File_View_Ddctn_Payee() { return iaa_Master_File_View_Ddctn_Payee; }

    public DbsField getIaa_Master_File_View_Ddctn_Per_Amt() { return iaa_Master_File_View_Ddctn_Per_Amt; }

    public DbsField getIaa_Master_File_View_Ddctn_Ytd_Amt() { return iaa_Master_File_View_Ddctn_Ytd_Amt; }

    public DbsField getIaa_Master_File_View_Ddctn_Pd_To_Dte() { return iaa_Master_File_View_Ddctn_Pd_To_Dte; }

    public DbsField getIaa_Master_File_View_Ddctn_Tot_Amt() { return iaa_Master_File_View_Ddctn_Tot_Amt; }

    public DbsField getIaa_Master_File_View_Ddctn_Intent_Cde() { return iaa_Master_File_View_Ddctn_Intent_Cde; }

    public DbsField getIaa_Master_File_View_Ddctn_Strt_Dte() { return iaa_Master_File_View_Ddctn_Strt_Dte; }

    public DbsField getIaa_Master_File_View_Ddctn_Stp_Dte() { return iaa_Master_File_View_Ddctn_Stp_Dte; }

    public DbsField getIaa_Master_File_View_Ddctn_Final_Dte() { return iaa_Master_File_View_Ddctn_Final_Dte; }

    public DbsField getIaa_Master_File_View_Cpr_Xfr_Term_Cde() { return iaa_Master_File_View_Cpr_Xfr_Term_Cde; }

    public DbsField getIaa_Master_File_View_Cpr_Lgl_Res_Cde() { return iaa_Master_File_View_Cpr_Lgl_Res_Cde; }

    public DbsField getIaa_Master_File_View_Cpr_Xfr_Iss_Dte() { return iaa_Master_File_View_Cpr_Xfr_Iss_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Type() { return iaa_Master_File_View_Cntrct_Type; }

    public DbsField getIaa_Master_File_View_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Master_File_View_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Master_File_View_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Master_File_View_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Master_File_View_Cntrct_Fnl_Prm_Dte() { return iaa_Master_File_View_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Mtch_Ppcn() { return iaa_Master_File_View_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Master_File_View_Cntrct_Annty_Strt_Dte() { return iaa_Master_File_View_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Issue_Dte_Dd() { return iaa_Master_File_View_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd() { return iaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Master_File_View_Rllvr_Cntrct_Nbr() { return iaa_Master_File_View_Rllvr_Cntrct_Nbr; }

    public DbsField getIaa_Master_File_View_Rllvr_Ivc_Ind() { return iaa_Master_File_View_Rllvr_Ivc_Ind; }

    public DbsField getIaa_Master_File_View_Rllvr_Elgble_Ind() { return iaa_Master_File_View_Rllvr_Elgble_Ind; }

    public DbsGroup getIaa_Master_File_View_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return iaa_Master_File_View_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getIaa_Master_File_View_Rllvr_Dstrbtng_Irc_Cde() { return iaa_Master_File_View_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getIaa_Master_File_View_Rllvr_Accptng_Irc_Cde() { return iaa_Master_File_View_Rllvr_Accptng_Irc_Cde; }

    public DbsField getIaa_Master_File_View_Rllvr_Pln_Admn_Ind() { return iaa_Master_File_View_Rllvr_Pln_Admn_Ind; }

    public DbsField getIaa_Master_File_View_Roth_Frst_Cntrb_Dte() { return iaa_Master_File_View_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Master_File_View_Roth_Ssnng_Dte() { return iaa_Master_File_View_Roth_Ssnng_Dte; }

    public DbsField getIaa_Master_File_View_Roth_Dsblty_Dte() { return iaa_Master_File_View_Roth_Dsblty_Dte; }

    public DbsField getIaa_Master_File_View_Cntrct_Ssnng_Dte() { return iaa_Master_File_View_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Master_File_View_Plan_Nmbr() { return iaa_Master_File_View_Plan_Nmbr; }

    public DbsField getIaa_Master_File_View_Tax_Exmpt_Ind() { return iaa_Master_File_View_Tax_Exmpt_Ind; }

    public DbsField getIaa_Master_File_View_Orig_Ownr_Dob() { return iaa_Master_File_View_Orig_Ownr_Dob; }

    public DbsField getIaa_Master_File_View_Orig_Ownr_Dod() { return iaa_Master_File_View_Orig_Ownr_Dod; }

    public DbsField getIaa_Master_File_View_Sub_Plan_Nmbr() { return iaa_Master_File_View_Sub_Plan_Nmbr; }

    public DbsField getIaa_Master_File_View_Orgntng_Sub_Plan_Nmbr() { return iaa_Master_File_View_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getIaa_Master_File_View_Orgntng_Cntrct_Nmbr() { return iaa_Master_File_View_Orgntng_Cntrct_Nmbr; }

    public DbsGroup getPnd_Work_Record_Header() { return pnd_Work_Record_Header; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde() { return pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Record_Code() { return pnd_Work_Record_Header_Pnd_Wh_Record_Code; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Check_Date() { return pnd_Work_Record_Header_Pnd_Wh_Check_Date; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Process_Date() { return pnd_Work_Record_Header_Pnd_Wh_Process_Date; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_1() { return pnd_Work_Record_Header_Pnd_Wh_Filler_1; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_2() { return pnd_Work_Record_Header_Pnd_Wh_Filler_2; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_3() { return pnd_Work_Record_Header_Pnd_Wh_Filler_3; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_4() { return pnd_Work_Record_Header_Pnd_Wh_Filler_4; }

    public DbsField getPnd_Work_Record_Header_Pnd_Wh_Filler_5() { return pnd_Work_Record_Header_Pnd_Wh_Filler_5; }

    public DbsGroup getPnd_Work_Record_Trailer() { return pnd_Work_Record_Trailer; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde() { return pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Record_Code() { return pnd_Work_Record_Trailer_Pnd_Wt_Record_Code; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Check_Date() { return pnd_Work_Record_Trailer_Pnd_Wt_Check_Date; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Process_Date() { return pnd_Work_Record_Trailer_Pnd_Wt_Process_Date; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records() { return pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records() { return pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records() { return pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_1() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_1; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_2() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_2; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_3() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_3; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_4() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_4; }

    public DbsField getPnd_Work_Record_Trailer_Pnd_Wt_Filler_5() { return pnd_Work_Record_Trailer_Pnd_Wt_Filler_5; }

    public DbsField getPnd_Check_Date_Ccyymmdd() { return pnd_Check_Date_Ccyymmdd; }

    public DbsGroup getPnd_Check_Date_CcyymmddRedef1() { return pnd_Check_Date_CcyymmddRedef1; }

    public DbsField getPnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N() { return pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N; }

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd_View() { return vw_iaa_Cntrl_Rcrd_View; }

    public DbsField getIaa_Cntrl_Rcrd_View_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key() { return iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Count_Fund_Record_Reads = newFieldInRecord("pnd_Count_Fund_Record_Reads", "#COUNT-FUND-RECORD-READS", FieldType.NUMERIC, 8);

        pnd_Count_Master_File_Reads = newFieldInRecord("pnd_Count_Master_File_Reads", "#COUNT-MASTER-FILE-READS", FieldType.NUMERIC, 8);

        pnd_Count_Master_Record_Error = newFieldInRecord("pnd_Count_Master_Record_Error", "#COUNT-MASTER-RECORD-ERROR", FieldType.NUMERIC, 8);

        pnd_Count_Fund_Extract = newFieldInRecord("pnd_Count_Fund_Extract", "#COUNT-FUND-EXTRACT", FieldType.NUMERIC, 8);

        pnd_Count_Contract_Extract = newFieldInRecord("pnd_Count_Contract_Extract", "#COUNT-CONTRACT-EXTRACT", FieldType.NUMERIC, 8);

        pnd_Count_Deduction_Extract = newFieldInRecord("pnd_Count_Deduction_Extract", "#COUNT-DEDUCTION-EXTRACT", FieldType.NUMERIC, 8);

        pnd_Count_Cpr_Extract = newFieldInRecord("pnd_Count_Cpr_Extract", "#COUNT-CPR-EXTRACT", FieldType.NUMERIC, 8);

        pnd_Count_Active_Payees = newFieldInRecord("pnd_Count_Active_Payees", "#COUNT-ACTIVE-PAYEES", FieldType.NUMERIC, 8);

        pnd_Count_Inactive_Payees = newFieldInRecord("pnd_Count_Inactive_Payees", "#COUNT-INACTIVE-PAYEES", FieldType.NUMERIC, 8);

        pnd_Count = newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 8);

        pnd_Count2 = newFieldInRecord("pnd_Count2", "#COUNT2", FieldType.NUMERIC, 8);

        pnd_Work_Record_1 = newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr", "#W1-PPCN-NBR", FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Payee = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Payee", "#W1-PAYEE", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Record_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Record_Code", "#W1-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Optn_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Optn_Cde", "#W1-OPTN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Orgn_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Acctng_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Acctng_Cde", "#W1-ACCTNG-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record_1_Pnd_W1_Issue_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Issue_Dte", "#W1-ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte", "#W1-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte", "#W1-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W1_Crrncy_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Crrncy_Cde", "#W1-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_1_Pnd_W1_Type_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Type_Cde", "#W1-TYPE-CDE", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd", "#W1-PYMNT-MTHD", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde", "#W1-PNSN-PLN-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind", "#W1-JOINT-CNVRT-RCRCD-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr", "#W1-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde", "#W1-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind", "#W1-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte", "#W1-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte", "#W1-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde", "#W1-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt", "#W1-FIRST-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte", "#W1-FIRST-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind", "#W1-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte", "#W1-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte", "#W1-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde", "#W1-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte", "#W1-SCND-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt", "#W1-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn", "#W1-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde", "#W1-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde", "#W1-DIV-COLL-CDE", FieldType.STRING, 
            5);
        pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde", "#W1-INST-ISS-CDE", FieldType.STRING, 
            5);
        pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte", "#W1-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Type = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Type", "#W1-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re", "#W1-CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte", "#W1-CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5));
        pnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn", "#W1-CNTRCT-MTCH-PPCN", 
            FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte", "#W1-CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte", "#W1-ROTH-FRST-CNTRB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte", "#W1-ROTH-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte", "#W1-CNTRCT-SSNNG-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_Plan_Nmbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Plan_Nmbr", "#W1-PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Work_Record_1_Pnd_W1_Tax_Exmpt_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tax_Exmpt_Ind", "#W1-TAX-EXMPT-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dob = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dob", "#W1-ORIG-OWNR-DOB", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dod = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dod", "#W1-ORIG-OWNR-DOD", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_1_Pnd_W1_Sub_Plan_Nmbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Sub_Plan_Nmbr", "#W1-SUB-PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Work_Record_1_Pnd_W1_Orgntng_Sub_Plan_Nmbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orgntng_Sub_Plan_Nmbr", "#W1-ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6);
        pnd_Work_Record_1_Pnd_W1_Orgntng_Cntrct_Nmbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orgntng_Cntrct_Nmbr", "#W1-ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Filler = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Filler", "#W1-FILLER", FieldType.STRING, 114);
        pnd_Work_Record_1_Pnd_W1_Filler2 = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Filler2", "#W1-FILLER2", FieldType.STRING, 5);

        pnd_Work_Record_2 = newGroupInRecord("pnd_Work_Record_2", "#WORK-RECORD-2");
        pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr", "#W2-PART-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde", "#W2-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_2_Pnd_W2_Record_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Record_Cde", "#W2-RECORD-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr", "#W2-CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte", "#W2-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde", "#W2-PRTCPNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde", "#W2-PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw", "#W2-PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr", "#W2-PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ", "#W2-PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Actvty_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Actvty_Cde", "#W2-ACTVTY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn", "#W2-TRMNTE-RSN", FieldType.STRING, 
            2);
        pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind", "#W2-RWRTTN-IND", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_2_Pnd_W2_Cash_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cash_Cde", "#W2-CASH-CDE", FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde", "#W2-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Company_Cd = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Company_Cd", "#W2-COMPANY-CD", FieldType.STRING, 
            1, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind", "#W2-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt", "#W2-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt", "#W2-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Ivc_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Ivc_Amt", "#W2-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt", "#W2-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Rtb_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Amt", "#W2-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Rtb_Percent = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Percent", "#W2-RTB-PERCENT", FieldType.PACKED_DECIMAL, 
            7,4, new DbsArrayController(1,5));
        pnd_Work_Record_2_Pnd_W2_Mode_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Mode_Ind", "#W2-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte", "#W2-WTHDRWL-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte", "#W2-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte", "#W2-FINAL-PAY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind", "#W2-BNFCRY-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte", "#W2-BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Pend_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Pend_Cde", "#W2-PEND-CDE", FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Hold_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Hold_Cde", "#W2-HOLD-CDE", FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Pend_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Pend_Dte", "#W2-PEND-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde", "#W2-PREV-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde", "#W2-CURR-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_2_Pnd_W2_Cmbne_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cmbne_Cde", "#W2-CMBNE-CDE", FieldType.STRING, 
            12);
        pnd_Work_Record_2_Pnd_W2_Spirt_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Cde", "#W2-SPIRT-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Spirt_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Amt", "#W2-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7,2);
        pnd_Work_Record_2_Pnd_W2_Spirt_Srce = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Srce", "#W2-SPIRT-SRCE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte", "#W2-SPIRT-ARR-DTE", FieldType.NUMERIC, 
            4);
        pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte", "#W2-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt", "#W2-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_State_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_State_Cde", "#W2-STATE-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_2_Pnd_W2_State_Tax_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_State_Tax_Amt", "#W2-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Local_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Local_Cde", "#W2-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt", "#W2-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte", "#W2-LST-CHNGE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde", "#W2-CPR-XFR-TERM-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde", "#W2-CPR-LGL-RES-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte", "#W2-CPR-XFR-ISS-DTE", 
            FieldType.DATE);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr", "#W2-RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind", "#W2-RLLVR-IVC-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind", "#W2-RLLVR-ELGBLE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde", "#W2-RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,4));
        pnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde", "#W2-RLLVR-ACCPTNG-IRC-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind", "#W2-RLLVR-PLN-ADMN-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8 = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8", "#W2-CPR-XFR-ISS-DTE-N8", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte", "#W2-ROTH-DSBLTY-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_Record_3 = newGroupInRecord("pnd_Work_Record_3", "#WORK-RECORD-3");
        pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr", "#W3-DDCTN-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde", "#W3-DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde", "#W3-DDCTN-RECORD-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr", "#W3-DDCTN-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Cde", "#W3-DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr", "#W3-DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Payee = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Payee", "#W3-DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt", "#W3-DDCTN-PER-AMT", FieldType.DECIMAL, 
            7,2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt", "#W3-DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte", "#W3-DDCTN-PD-TO-DTE", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt", "#W3-DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde", "#W3-DDCTN-INTENT-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte", "#W3-DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte", "#W3-DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte", "#W3-DDCTN-FINAL-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte", "#W3-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_3_Pnd_W3_Filler = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Filler", "#W3-FILLER", FieldType.STRING, 250);
        pnd_Work_Record_3_Pnd_W3_Filler2 = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Filler2", "#W3-FILLER2", FieldType.STRING, 10);
        pnd_Work_Record_3_Pnd_W3_Filler3 = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Filler3", "#W3-FILLER3", FieldType.STRING, 8);
        pnd_Work_Record_3_Pnd_W3_Filler4 = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Filler4", "#W3-FILLER4", FieldType.STRING, 8);

        pnd_Per_Tot = newFieldInRecord("pnd_Per_Tot", "#PER-TOT", FieldType.NUMERIC, 3);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);

        pnd_All_Ok = newFieldInRecord("pnd_All_Ok", "#ALL-OK", FieldType.STRING, 1);

        vw_iaa_Master_File_View = new DataAccessProgramView(new NameInfo("vw_iaa_Master_File_View", "IAA-MASTER-FILE-VIEW"), "IAA_MASTER_FILE", "IA_CONTRACT_PART", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_MASTER_FILE"));
        iaa_Master_File_View_Cntrct_Ppcn_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Master_File_View_Cntrct_Optn_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Master_File_View_Cntrct_Orgn_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Master_File_View_Cntrct_Acctng_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Master_File_View_Cntrct_Issue_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Master_File_View_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Pymnt_Due_Dte", 
            "CNTRCT-FIRST-PYMNT-DUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Master_File_View_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Pymnt_Pd_Dte", 
            "CNTRCT-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Master_File_View_Cntrct_Crrncy_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Master_File_View_Cntrct_Type_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Master_File_View_Cntrct_Pymnt_Mthd = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Master_File_View_Cntrct_Pnsn_Pln_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Master_File_View_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Master_File_View_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Orig_Da_Cntrct_Nbr", 
            "CNTRCT-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Master_File_View_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Rsdncy_At_Issue_Cde", 
            "CNTRCT-RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Master_File_View_Cntrct_First_Annt_Xref_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Annt_Xref_Ind", 
            "CNTRCT-FIRST-ANNT-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Master_File_View_Cntrct_First_Annt_Dob_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Annt_Dob_Dte", 
            "CNTRCT-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Master_File_View_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Master_File_View_Cntrct_First_Annt_Sex_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Annt_Sex_Cde", 
            "CNTRCT-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Master_File_View_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Annt_Lfe_Cnt", 
            "CNTRCT-FIRST-ANNT-LFE-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Master_File_View_Cntrct_First_Annt_Dod_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_First_Annt_Dod_Dte", 
            "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Master_File_View_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Scnd_Annt_Xref_Ind", 
            "CNTRCT-SCND-ANNT-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Master_File_View_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Scnd_Annt_Dob_Dte", 
            "CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Master_File_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Master_File_View_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Scnd_Annt_Sex_Cde", 
            "CNTRCT-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Master_File_View_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Scnd_Annt_Dod_Dte", 
            "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Master_File_View_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Scnd_Annt_Life_Cnt", 
            "CNTRCT-SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Master_File_View_Cntrct_Scnd_Annt_Ssn = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Master_File_View_Cntrct_Div_Payee_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Master_File_View_Cntrct_Div_Coll_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Master_File_View_Cntrct_Inst_Iss_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Master_File_View_Cntrct_Part_Ppcn_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Master_File_View_Cntrct_Part_Payee_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Master_File_View_Cpr_Id_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Master_File_View_Lst_Trans_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Master_File_View_Prtcpnt_Ctznshp_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Master_File_View_Prtcpnt_Rsdncy_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Master_File_View_Prtcpnt_Rsdncy_Sw = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Master_File_View_Prtcpnt_Tax_Id_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Master_File_View_Prtcpnt_Tax_Id_Typ = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Master_File_View_Cntrct_Actvty_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Master_File_View_Cntrct_Trmnte_Rsn = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Master_File_View_Cntrct_Rwrttn_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Master_File_View_Cntrct_Cash_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Master_File_View_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Master_File_View_Cntrct_Company_Data = vw_iaa_Master_File_View.getRecord().newGroupInGroup("iaa_Master_File_View_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Company_Cd = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Rcvry_Type_Ind = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Per_Ivc_Amt = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Resdl_Ivc_Amt = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Ivc_Amt = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Ivc_Used_Amt = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Rtb_Amt = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Rtb_Percent = iaa_Master_File_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_View_Cntrct_Mode_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Master_File_View_Cntrct_Wthdrwl_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Master_File_View_Cntrct_Final_Per_Pay_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Master_File_View_Cntrct_Final_Pay_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Master_File_View_Bnfcry_Xref_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Master_File_View_Bnfcry_Dod_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Master_File_View_Cntrct_Pend_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Master_File_View_Cntrct_Hold_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Master_File_View_Cntrct_Pend_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Master_File_View_Cntrct_Prev_Dist_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Master_File_View_Cntrct_Curr_Dist_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Master_File_View_Cntrct_Cmbne_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", 
            FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Master_File_View_Cntrct_Spirt_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Master_File_View_Cntrct_Spirt_Amt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Master_File_View_Cntrct_Spirt_Srce = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Master_File_View_Cntrct_Spirt_Arr_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Master_File_View_Cntrct_Spirt_Prcss_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Master_File_View_Cntrct_Fed_Tax_Amt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Master_File_View_Cntrct_State_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_State_Cde", "CNTRCT-STATE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Master_File_View_Cntrct_State_Tax_Amt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Master_File_View_Cntrct_Local_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Master_File_View_Cntrct_Local_Tax_Amt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Master_File_View_Cntrct_Lst_Chnge_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Master_File_View_Ddctn_Ppcn_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Master_File_View_Ddctn_Payee_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Master_File_View_Ddctn_Id_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Master_File_View_Ddctn_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DDCTN_CDE");
        iaa_Master_File_View_Ddctn_Seq_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Master_File_View_Ddctn_Payee = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Master_File_View_Ddctn_Per_Amt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Per_Amt", "DDCTN-PER-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Master_File_View_Ddctn_Ytd_Amt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Master_File_View_Ddctn_Pd_To_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Master_File_View_Ddctn_Tot_Amt = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Master_File_View_Ddctn_Intent_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Master_File_View_Ddctn_Strt_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Master_File_View_Ddctn_Stp_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Stp_Dte", "DDCTN-STP-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Master_File_View_Ddctn_Final_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Master_File_View_Cpr_Xfr_Term_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Master_File_View_Cpr_Lgl_Res_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Master_File_View_Cpr_Xfr_Iss_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Master_File_View_Cntrct_Type = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        iaa_Master_File_View_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Rsdncy_At_Iss_Re", 
            "CNTRCT-RSDNCY-AT-ISS-RE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Master_File_View_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Master_File_View.getRecord().newGroupInGroup("iaa_Master_File_View_Cntrct_Fnl_Prm_DteMuGroup", 
            "CNTRCT_FNL_PRM_DTEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Master_File_View_Cntrct_Fnl_Prm_Dte = iaa_Master_File_View_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Master_File_View_Cntrct_Fnl_Prm_Dte", 
            "CNTRCT-FNL-PRM-DTE", FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Master_File_View_Cntrct_Mtch_Ppcn = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Master_File_View_Cntrct_Annty_Strt_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Annty_Strt_Dte", 
            "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Master_File_View_Cntrct_Issue_Dte_Dd = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Master_File_View_Rllvr_Cntrct_Nbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Master_File_View_Rllvr_Ivc_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        iaa_Master_File_View_Rllvr_Elgble_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Rllvr_Elgble_Ind", "RLLVR-ELGBLE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        iaa_Master_File_View_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Master_File_View.getRecord().newGroupInGroup("iaa_Master_File_View_Rllvr_Dstrbtng_Irc_CdeMuGroup", 
            "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Master_File_View_Rllvr_Dstrbtng_Irc_Cde = iaa_Master_File_View_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Master_File_View_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Master_File_View_Rllvr_Accptng_Irc_Cde = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Rllvr_Accptng_Irc_Cde", 
            "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Master_File_View_Rllvr_Pln_Admn_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Rllvr_Pln_Admn_Ind", "RLLVR-PLN-ADMN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        iaa_Master_File_View_Roth_Frst_Cntrb_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Master_File_View_Roth_Ssnng_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_SSNNG_DTE");
        iaa_Master_File_View_Roth_Dsblty_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");
        iaa_Master_File_View_Cntrct_Ssnng_Dte = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Master_File_View_Plan_Nmbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "PLAN_NMBR");
        iaa_Master_File_View_Tax_Exmpt_Ind = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Tax_Exmpt_Ind", "TAX-EXMPT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TAX_EXMPT_IND");
        iaa_Master_File_View_Orig_Ownr_Dob = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Orig_Ownr_Dob", "ORIG-OWNR-DOB", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ORIG_OWNR_DOB");
        iaa_Master_File_View_Orig_Ownr_Dod = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Orig_Ownr_Dod", "ORIG-OWNR-DOD", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ORIG_OWNR_DOD");
        iaa_Master_File_View_Sub_Plan_Nmbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "SUB_PLAN_NMBR");
        iaa_Master_File_View_Orgntng_Sub_Plan_Nmbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Orgntng_Sub_Plan_Nmbr", 
            "ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Master_File_View_Orgntng_Cntrct_Nmbr = vw_iaa_Master_File_View.getRecord().newFieldInGroup("iaa_Master_File_View_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");

        pnd_Work_Record_Header = newGroupInRecord("pnd_Work_Record_Header", "#WORK-RECORD-HEADER");
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr", "#WH-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde", "#WH-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Record_Code = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Record_Code", "#WH-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Check_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Check_Date", "#WH-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Process_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Process_Date", "#WH-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Filler_1 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_1", "#WH-FILLER-1", FieldType.STRING, 
            193);
        pnd_Work_Record_Header_Pnd_Wh_Filler_2 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_2", "#WH-FILLER-2", FieldType.STRING, 
            123);
        pnd_Work_Record_Header_Pnd_Wh_Filler_3 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_3", "#WH-FILLER-3", FieldType.STRING, 
            8);
        pnd_Work_Record_Header_Pnd_Wh_Filler_4 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_4", "#WH-FILLER-4", FieldType.STRING, 
            8);
        pnd_Work_Record_Header_Pnd_Wh_Filler_5 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_5", "#WH-FILLER-5", FieldType.STRING, 
            5);

        pnd_Work_Record_Trailer = newGroupInRecord("pnd_Work_Record_Trailer", "#WORK-RECORD-TRAILER");
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr", "#WT-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde", "#WT-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Record_Code = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Record_Code", "#WT-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Check_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Check_Date", "#WT-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Process_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Process_Date", "#WT-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records", "#WT-TOT-CNT-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records", "#WT-TOT-CPR-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records", "#WT-TOT-DED-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_1 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_1", "#WT-FILLER-1", FieldType.STRING, 
            193);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_2 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_2", "#WT-FILLER-2", FieldType.STRING, 
            99);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_3 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_3", "#WT-FILLER-3", FieldType.STRING, 
            8);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_4 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_4", "#WT-FILLER-4", FieldType.STRING, 
            8);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_5 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_5", "#WT-FILLER-5", FieldType.STRING, 
            5);

        pnd_Check_Date_Ccyymmdd = newFieldInRecord("pnd_Check_Date_Ccyymmdd", "#CHECK-DATE-CCYYMMDD", FieldType.STRING, 8);
        pnd_Check_Date_CcyymmddRedef1 = newGroupInRecord("pnd_Check_Date_CcyymmddRedef1", "Redefines", pnd_Check_Date_Ccyymmdd);
        pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N = pnd_Check_Date_CcyymmddRedef1.newFieldInGroup("pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N", 
            "#CHECK-DATE-CCYYMMDD-N", FieldType.NUMERIC, 8);

        vw_iaa_Cntrl_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_View", "IAA-CNTRL-RCRD-VIEW"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key", "CNTRL-RCRD-KEY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRL_RCRD_KEY");
        vw_iaa_Master_File_View.setUniquePeList();

        this.setRecordName("LdaIaal301");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Master_File_View.reset();
        vw_iaa_Cntrl_Rcrd_View.reset();
    }

    // Constructor
    public LdaIaal301() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
