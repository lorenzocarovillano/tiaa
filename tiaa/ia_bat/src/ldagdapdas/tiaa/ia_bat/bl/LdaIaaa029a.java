/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:08 PM
**        * FROM NATURAL LDA     : IAAA029A
************************************************************
**        * FILE NAME            : LdaIaaa029a.java
**        * CLASS NAME           : LdaIaaa029a
**        * INSTANCE NAME        : LdaIaaa029a
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaaa029a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Sort_Key_Layout;
    private DbsField pnd_Sort_Key_Layout_Pnd_Sort_Key_Length;
    private DbsField pnd_Sort_Key_Layout_Pnd_Sort_Key_Array;
    private DbsGroup pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1;
    private DbsGroup pnd_Sort_Key_Layout_Pnd_Sort_Key;
    private DbsGroup pnd_Sort_Key_Layout_Pnd_L1_Sort_Flds;
    private DbsField pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_N;
    private DbsGroup pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_NRedef2;
    private DbsField pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value;
    private DbsField pnd_Sort_Key_Layout_Pnd_Id_L1;
    private DbsField pnd_Sort_Key_Layout_Pnd_L2_Sort_Flds;
    private DbsGroup pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3;
    private DbsField pnd_Sort_Key_Layout_Pnd_Id_L2;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L2_Area;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L2_Nbr_Of_Contracts;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L2_Pin;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L2_Payee_Cde;
    private DbsField pnd_Sort_Key_Layout_Pnd_L3_Sort_Flds;
    private DbsGroup pnd_Sort_Key_Layout_Pnd_L3_Sort_FldsRedef4;
    private DbsField pnd_Sort_Key_Layout_Pnd_Id_L3;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L3_Tiebreak;
    private DbsField pnd_Sort_Key_Layout_Pnd_L4_Sort_Flds;
    private DbsGroup pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5;
    private DbsField pnd_Sort_Key_Layout_Pnd_Id_L4;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2;
    private DbsField pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3;
    private DbsGroup co_Header_Record;
    private DbsField co_Header_Record_Co_Record_Id;
    private DbsField co_Header_Record_Co_Record_Length;
    private DbsField co_Header_Record_Co_Data_Occurs;
    private DbsField co_Header_Record_Co_Data;
    private DbsGroup co_Header_Record_Co_DataRedef6;
    private DbsField co_Header_Record_Co_Application_Record;
    private DbsField co_Header_Record_Co_Statement_Date;
    private DbsGroup co_Header_Record_Co_Statement_DateRedef7;
    private DbsField co_Header_Record_Co_Statement_Month;
    private DbsField co_Header_Record_Co_Statement_Day;
    private DbsField co_Header_Record_Co_Statement_Year;
    private DbsGroup mail_Item_Record;
    private DbsField mail_Item_Record_Mail_Record_Id;
    private DbsField mail_Item_Record_Mail_Data_Length;
    private DbsField mail_Item_Record_Mail_Data_Occurs;
    private DbsField mail_Item_Record_Mail_Data;
    private DbsGroup mail_Item_Record_Mail_DataRedef8;
    private DbsField mail_Item_Record_Mail_Pin_Number;
    private DbsField mail_Item_Record_Mail_Postnet_Barcode;
    private DbsGroup mail_Item_Record_Mail_Ph_Name_Address;
    private DbsField mail_Item_Record_Mail_Ph_Full_Name;
    private DbsField mail_Item_Record_Mail_Ph_Address1;
    private DbsField mail_Item_Record_Mail_Ph_Address2;
    private DbsField mail_Item_Record_Mail_Ph_Address3;
    private DbsField mail_Item_Record_Mail_Ph_Address4;
    private DbsField mail_Item_Record_Mail_Ph_Address5;
    private DbsField mail_Item_Record_Mail_Ph_Address6;
    private DbsField mail_Item_Record_Mail_Work_Prcss_Id;
    private DbsField mail_Item_Record_Mail_Image_Id;
    private DbsField mail_Item_Record_Mail_Rqst_Id;
    private DbsField mail_Item_Record_Mail_Environment;
    private DbsField mail_Item_Record_Mail_Mach_Func_2;
    private DbsField mail_Item_Record_Mail_Mach_Func_3;
    private DbsField mail_Item_Record_Mail_Mach_Func_4;
    private DbsField mail_Item_Record_Mail_Mach_Func_5;
    private DbsField mail_Item_Record_Mail_Package_Id;
    private DbsGroup mail_Item_Record_Mail_Package_IdRedef9;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_1;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_2;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_3;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_4;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_5;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_6;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_7;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_8;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_9;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_10;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_11;
    private DbsField mail_Item_Record_Mail_Pkid_Byte_12;
    private DbsField mail_Item_Record_Mail_Document_Count;
    private DbsField mail_Item_Record_Mail_Document_Bad_Ind;
    private DbsField mail_Item_Record_Mail_Finish_Profile;
    private DbsField mail_Item_Record_Mail_Postnet_Zip_Full;
    private DbsField mail_Item_Record_Mail_Package_Delivery_Type;
    private DbsField mail_Item_Record_Mail_Image_Source_Id;
    private DbsField mail_Item_Record_Mail_Summary_Prev_Total;
    private DbsField mail_Item_Record_Mail_Summary_Curr_Total;
    private DbsField mail_Item_Record_Mail_Fill_Num;
    private DbsField mail_Item_Record_Mail_Fill_Alpha;
    private DbsGroup contract_Record;
    private DbsField contract_Record_Contract_Record_Id;
    private DbsField contract_Record_Contract_Data_Length;
    private DbsField contract_Record_Contract_Data_Occurs;
    private DbsField contract_Record_Contract_Data;
    private DbsGroup contract_Record_Contract_DataRedef10;
    private DbsField contract_Record_Contract_Prev_Payment_Date;
    private DbsGroup contract_Record_Contract_Prev_Payment_DateRedef11;
    private DbsField contract_Record_Contract_Prev_Pay_Month;
    private DbsField contract_Record_Contract_Prev_Pay_Day;
    private DbsField contract_Record_Contract_Prev_Pay_Cc;
    private DbsField contract_Record_Contract_Prev_Pay_Yy;
    private DbsField contract_Record_Contract_Curr_Payment_Date;
    private DbsGroup contract_Record_Contract_Curr_Payment_DateRedef12;
    private DbsField contract_Record_Contract_Curr_Pay_Month;
    private DbsField contract_Record_Contract_Curr_Pay_Day;
    private DbsField contract_Record_Contract_Curr_Pay_Cc;
    private DbsField contract_Record_Contract_Curr_Pay_Yy;
    private DbsField contract_Record_Contract_Number;
    private DbsField contract_Record_Contract_Type;
    private DbsField contract_Record_Contract_Fill_Num;
    private DbsField contract_Record_Contract_Fill_Alpha;
    private DbsGroup fund_Account_Record;
    private DbsField fund_Account_Record_Fund_Record_Id;
    private DbsField fund_Account_Record_Fund_Data_Length;
    private DbsField fund_Account_Record_Fund_Data_Occurs;
    private DbsField fund_Account_Record_Fund_Data;
    private DbsGroup fund_Account_Record_Fund_DataRedef13;
    private DbsField fund_Account_Record_Fund_Account_Name;
    private DbsField fund_Account_Record_Fund_Account_Type;
    private DbsField fund_Account_Record_Fund_Payment_Method;
    private DbsField fund_Account_Record_Fund_Change_Method;
    private DbsField fund_Account_Record_Fund_Prev_Contract_Amt;
    private DbsField fund_Account_Record_Fund_Prev_Dividend;
    private DbsField fund_Account_Record_Fund_Prev_Units;
    private DbsField fund_Account_Record_Fund_Prev_Unit_Value;
    private DbsGroup fund_Account_Record_Fund_Prev_Unit_ValueRedef14;
    private DbsField fund_Account_Record_Fund_Prev_Unit_Value_Ask;
    private DbsField fund_Account_Record_Fund_Prev_Total_Payment;
    private DbsField fund_Account_Record_Fund_Prev_Total_Gross_Payment;
    private DbsField fund_Account_Record_Fund_Curr_Contract_Amt;
    private DbsField fund_Account_Record_Fund_Curr_Dividend;
    private DbsField fund_Account_Record_Fund_Curr_Units;
    private DbsField fund_Account_Record_Fund_Curr_Unit_Value;
    private DbsGroup fund_Account_Record_Fund_Curr_Unit_ValueRedef15;
    private DbsField fund_Account_Record_Fund_Curr_Unit_Value_Ask;
    private DbsField fund_Account_Record_Fund_Curr_Total_Payment;
    private DbsField fund_Account_Record_Fund_Curr_Total_Gross_Payment;
    private DbsField fund_Account_Record_Fund_Fill_Num;
    private DbsField fund_Account_Record_Fund_Fill_Alpha;
    private DbsGroup ded_Record;
    private DbsField ded_Record_Ded_Record_Id;
    private DbsField ded_Record_Ded_Data_Length;
    private DbsField ded_Record_Ded_Data_Occurs;
    private DbsField ded_Record_Ded_Data;
    private DbsGroup ded_Record_Ded_DataRedef16;
    private DbsField ded_Record_Ded_Description;
    private DbsField ded_Record_Ded_Prev_Amt;
    private DbsField ded_Record_Ded_Curr_Amt;
    private DbsField ded_Record_Ded_Prev_Net_Amt;
    private DbsField ded_Record_Ded_Curr_Net_Amt;
    private DbsField ded_Record_Ded_Fill_Num;
    private DbsField ded_Record_Ded_Fill_Alpha;
    private DbsGroup msg_Record;
    private DbsField msg_Record_Msg_Record_Id;
    private DbsField msg_Record_Msg_Data_Length;
    private DbsField msg_Record_Msg_Data_Occurs;
    private DbsField msg_Record_Msg_Data;
    private DbsGroup msg_Record_Msg_DataRedef17;
    private DbsField msg_Record_Msg_Id;
    private DbsField msg_Record_Msg_Date_1;
    private DbsGroup msg_Record_Msg_Date_1Redef18;
    private DbsField msg_Record_Msg_Date1_Mmdd;
    private DbsField msg_Record_Msg_Date1_Ccyy;
    private DbsGroup msg_Record_Msg_Date_1Redef19;
    private DbsField msg_Record_Msg_Date_Mm_1;
    private DbsField msg_Record_Msg_Date_Dd_1;
    private DbsField msg_Record_Msg_Date_Cc_1;
    private DbsField msg_Record_Msg_Date_Yy_1;
    private DbsField msg_Record_Msg_Date_2;
    private DbsGroup msg_Record_Msg_Date_2Redef20;
    private DbsField msg_Record_Msg_Date2_Mmdd;
    private DbsField msg_Record_Msg_Date2_Ccyy;
    private DbsGroup msg_Record_Msg_Date_2Redef21;
    private DbsField msg_Record_Msg_Date_Mm_2;
    private DbsField msg_Record_Msg_Date_Dd_2;
    private DbsField msg_Record_Msg_Date_Cc_2;
    private DbsField msg_Record_Msg_Date_Yy_2;
    private DbsField msg_Record_Msg_Increase_Percent;
    private DbsField msg_Record_Msg_Phone_Number;
    private DbsField msg_Record_Msg_Numeric_1;
    private DbsField msg_Record_Msg_Numeric_2;
    private DbsField msg_Record_Msg_Numeric_3;
    private DbsField msg_Record_Msg_Char_1;
    private DbsField msg_Record_Msg_Char_2;
    private DbsField msg_Record_Msg_Char_3;

    public DbsGroup getPnd_Sort_Key_Layout() { return pnd_Sort_Key_Layout; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length() { return pnd_Sort_Key_Layout_Pnd_Sort_Key_Length; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Sort_Key_Array() { return pnd_Sort_Key_Layout_Pnd_Sort_Key_Array; }

    public DbsGroup getPnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1() { return pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1; }

    public DbsGroup getPnd_Sort_Key_Layout_Pnd_Sort_Key() { return pnd_Sort_Key_Layout_Pnd_Sort_Key; }

    public DbsGroup getPnd_Sort_Key_Layout_Pnd_L1_Sort_Flds() { return pnd_Sort_Key_Layout_Pnd_L1_Sort_Flds; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_N() { return pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_N; }

    public DbsGroup getPnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_NRedef2() { return pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_NRedef2; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Rsk_L1_Value() { return pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Id_L1() { return pnd_Sort_Key_Layout_Pnd_Id_L1; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_L2_Sort_Flds() { return pnd_Sort_Key_Layout_Pnd_L2_Sort_Flds; }

    public DbsGroup getPnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3() { return pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Id_L2() { return pnd_Sort_Key_Layout_Pnd_Id_L2; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L2_Area() { return pnd_Sort_Key_Layout_Pnd_Key_L2_Area; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L2_Nbr_Of_Contracts() { return pnd_Sort_Key_Layout_Pnd_Key_L2_Nbr_Of_Contracts; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L2_Pin() { return pnd_Sort_Key_Layout_Pnd_Key_L2_Pin; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L2_Payee_Cde() { return pnd_Sort_Key_Layout_Pnd_Key_L2_Payee_Cde; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_L3_Sort_Flds() { return pnd_Sort_Key_Layout_Pnd_L3_Sort_Flds; }

    public DbsGroup getPnd_Sort_Key_Layout_Pnd_L3_Sort_FldsRedef4() { return pnd_Sort_Key_Layout_Pnd_L3_Sort_FldsRedef4; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Id_L3() { return pnd_Sort_Key_Layout_Pnd_Id_L3; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L3_Tiebreak() { return pnd_Sort_Key_Layout_Pnd_Key_L3_Tiebreak; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_L4_Sort_Flds() { return pnd_Sort_Key_Layout_Pnd_L4_Sort_Flds; }

    public DbsGroup getPnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5() { return pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Id_L4() { return pnd_Sort_Key_Layout_Pnd_Id_L4; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak() { return pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2() { return pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2; }

    public DbsField getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3() { return pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3; }

    public DbsGroup getCo_Header_Record() { return co_Header_Record; }

    public DbsField getCo_Header_Record_Co_Record_Id() { return co_Header_Record_Co_Record_Id; }

    public DbsField getCo_Header_Record_Co_Record_Length() { return co_Header_Record_Co_Record_Length; }

    public DbsField getCo_Header_Record_Co_Data_Occurs() { return co_Header_Record_Co_Data_Occurs; }

    public DbsField getCo_Header_Record_Co_Data() { return co_Header_Record_Co_Data; }

    public DbsGroup getCo_Header_Record_Co_DataRedef6() { return co_Header_Record_Co_DataRedef6; }

    public DbsField getCo_Header_Record_Co_Application_Record() { return co_Header_Record_Co_Application_Record; }

    public DbsField getCo_Header_Record_Co_Statement_Date() { return co_Header_Record_Co_Statement_Date; }

    public DbsGroup getCo_Header_Record_Co_Statement_DateRedef7() { return co_Header_Record_Co_Statement_DateRedef7; }

    public DbsField getCo_Header_Record_Co_Statement_Month() { return co_Header_Record_Co_Statement_Month; }

    public DbsField getCo_Header_Record_Co_Statement_Day() { return co_Header_Record_Co_Statement_Day; }

    public DbsField getCo_Header_Record_Co_Statement_Year() { return co_Header_Record_Co_Statement_Year; }

    public DbsGroup getMail_Item_Record() { return mail_Item_Record; }

    public DbsField getMail_Item_Record_Mail_Record_Id() { return mail_Item_Record_Mail_Record_Id; }

    public DbsField getMail_Item_Record_Mail_Data_Length() { return mail_Item_Record_Mail_Data_Length; }

    public DbsField getMail_Item_Record_Mail_Data_Occurs() { return mail_Item_Record_Mail_Data_Occurs; }

    public DbsField getMail_Item_Record_Mail_Data() { return mail_Item_Record_Mail_Data; }

    public DbsGroup getMail_Item_Record_Mail_DataRedef8() { return mail_Item_Record_Mail_DataRedef8; }

    public DbsField getMail_Item_Record_Mail_Pin_Number() { return mail_Item_Record_Mail_Pin_Number; }

    public DbsField getMail_Item_Record_Mail_Postnet_Barcode() { return mail_Item_Record_Mail_Postnet_Barcode; }

    public DbsGroup getMail_Item_Record_Mail_Ph_Name_Address() { return mail_Item_Record_Mail_Ph_Name_Address; }

    public DbsField getMail_Item_Record_Mail_Ph_Full_Name() { return mail_Item_Record_Mail_Ph_Full_Name; }

    public DbsField getMail_Item_Record_Mail_Ph_Address1() { return mail_Item_Record_Mail_Ph_Address1; }

    public DbsField getMail_Item_Record_Mail_Ph_Address2() { return mail_Item_Record_Mail_Ph_Address2; }

    public DbsField getMail_Item_Record_Mail_Ph_Address3() { return mail_Item_Record_Mail_Ph_Address3; }

    public DbsField getMail_Item_Record_Mail_Ph_Address4() { return mail_Item_Record_Mail_Ph_Address4; }

    public DbsField getMail_Item_Record_Mail_Ph_Address5() { return mail_Item_Record_Mail_Ph_Address5; }

    public DbsField getMail_Item_Record_Mail_Ph_Address6() { return mail_Item_Record_Mail_Ph_Address6; }

    public DbsField getMail_Item_Record_Mail_Work_Prcss_Id() { return mail_Item_Record_Mail_Work_Prcss_Id; }

    public DbsField getMail_Item_Record_Mail_Image_Id() { return mail_Item_Record_Mail_Image_Id; }

    public DbsField getMail_Item_Record_Mail_Rqst_Id() { return mail_Item_Record_Mail_Rqst_Id; }

    public DbsField getMail_Item_Record_Mail_Environment() { return mail_Item_Record_Mail_Environment; }

    public DbsField getMail_Item_Record_Mail_Mach_Func_2() { return mail_Item_Record_Mail_Mach_Func_2; }

    public DbsField getMail_Item_Record_Mail_Mach_Func_3() { return mail_Item_Record_Mail_Mach_Func_3; }

    public DbsField getMail_Item_Record_Mail_Mach_Func_4() { return mail_Item_Record_Mail_Mach_Func_4; }

    public DbsField getMail_Item_Record_Mail_Mach_Func_5() { return mail_Item_Record_Mail_Mach_Func_5; }

    public DbsField getMail_Item_Record_Mail_Package_Id() { return mail_Item_Record_Mail_Package_Id; }

    public DbsGroup getMail_Item_Record_Mail_Package_IdRedef9() { return mail_Item_Record_Mail_Package_IdRedef9; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_1() { return mail_Item_Record_Mail_Pkid_Byte_1; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_2() { return mail_Item_Record_Mail_Pkid_Byte_2; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_3() { return mail_Item_Record_Mail_Pkid_Byte_3; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_4() { return mail_Item_Record_Mail_Pkid_Byte_4; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_5() { return mail_Item_Record_Mail_Pkid_Byte_5; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_6() { return mail_Item_Record_Mail_Pkid_Byte_6; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_7() { return mail_Item_Record_Mail_Pkid_Byte_7; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_8() { return mail_Item_Record_Mail_Pkid_Byte_8; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_9() { return mail_Item_Record_Mail_Pkid_Byte_9; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_10() { return mail_Item_Record_Mail_Pkid_Byte_10; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_11() { return mail_Item_Record_Mail_Pkid_Byte_11; }

    public DbsField getMail_Item_Record_Mail_Pkid_Byte_12() { return mail_Item_Record_Mail_Pkid_Byte_12; }

    public DbsField getMail_Item_Record_Mail_Document_Count() { return mail_Item_Record_Mail_Document_Count; }

    public DbsField getMail_Item_Record_Mail_Document_Bad_Ind() { return mail_Item_Record_Mail_Document_Bad_Ind; }

    public DbsField getMail_Item_Record_Mail_Finish_Profile() { return mail_Item_Record_Mail_Finish_Profile; }

    public DbsField getMail_Item_Record_Mail_Postnet_Zip_Full() { return mail_Item_Record_Mail_Postnet_Zip_Full; }

    public DbsField getMail_Item_Record_Mail_Package_Delivery_Type() { return mail_Item_Record_Mail_Package_Delivery_Type; }

    public DbsField getMail_Item_Record_Mail_Image_Source_Id() { return mail_Item_Record_Mail_Image_Source_Id; }

    public DbsField getMail_Item_Record_Mail_Summary_Prev_Total() { return mail_Item_Record_Mail_Summary_Prev_Total; }

    public DbsField getMail_Item_Record_Mail_Summary_Curr_Total() { return mail_Item_Record_Mail_Summary_Curr_Total; }

    public DbsField getMail_Item_Record_Mail_Fill_Num() { return mail_Item_Record_Mail_Fill_Num; }

    public DbsField getMail_Item_Record_Mail_Fill_Alpha() { return mail_Item_Record_Mail_Fill_Alpha; }

    public DbsGroup getContract_Record() { return contract_Record; }

    public DbsField getContract_Record_Contract_Record_Id() { return contract_Record_Contract_Record_Id; }

    public DbsField getContract_Record_Contract_Data_Length() { return contract_Record_Contract_Data_Length; }

    public DbsField getContract_Record_Contract_Data_Occurs() { return contract_Record_Contract_Data_Occurs; }

    public DbsField getContract_Record_Contract_Data() { return contract_Record_Contract_Data; }

    public DbsGroup getContract_Record_Contract_DataRedef10() { return contract_Record_Contract_DataRedef10; }

    public DbsField getContract_Record_Contract_Prev_Payment_Date() { return contract_Record_Contract_Prev_Payment_Date; }

    public DbsGroup getContract_Record_Contract_Prev_Payment_DateRedef11() { return contract_Record_Contract_Prev_Payment_DateRedef11; }

    public DbsField getContract_Record_Contract_Prev_Pay_Month() { return contract_Record_Contract_Prev_Pay_Month; }

    public DbsField getContract_Record_Contract_Prev_Pay_Day() { return contract_Record_Contract_Prev_Pay_Day; }

    public DbsField getContract_Record_Contract_Prev_Pay_Cc() { return contract_Record_Contract_Prev_Pay_Cc; }

    public DbsField getContract_Record_Contract_Prev_Pay_Yy() { return contract_Record_Contract_Prev_Pay_Yy; }

    public DbsField getContract_Record_Contract_Curr_Payment_Date() { return contract_Record_Contract_Curr_Payment_Date; }

    public DbsGroup getContract_Record_Contract_Curr_Payment_DateRedef12() { return contract_Record_Contract_Curr_Payment_DateRedef12; }

    public DbsField getContract_Record_Contract_Curr_Pay_Month() { return contract_Record_Contract_Curr_Pay_Month; }

    public DbsField getContract_Record_Contract_Curr_Pay_Day() { return contract_Record_Contract_Curr_Pay_Day; }

    public DbsField getContract_Record_Contract_Curr_Pay_Cc() { return contract_Record_Contract_Curr_Pay_Cc; }

    public DbsField getContract_Record_Contract_Curr_Pay_Yy() { return contract_Record_Contract_Curr_Pay_Yy; }

    public DbsField getContract_Record_Contract_Number() { return contract_Record_Contract_Number; }

    public DbsField getContract_Record_Contract_Type() { return contract_Record_Contract_Type; }

    public DbsField getContract_Record_Contract_Fill_Num() { return contract_Record_Contract_Fill_Num; }

    public DbsField getContract_Record_Contract_Fill_Alpha() { return contract_Record_Contract_Fill_Alpha; }

    public DbsGroup getFund_Account_Record() { return fund_Account_Record; }

    public DbsField getFund_Account_Record_Fund_Record_Id() { return fund_Account_Record_Fund_Record_Id; }

    public DbsField getFund_Account_Record_Fund_Data_Length() { return fund_Account_Record_Fund_Data_Length; }

    public DbsField getFund_Account_Record_Fund_Data_Occurs() { return fund_Account_Record_Fund_Data_Occurs; }

    public DbsField getFund_Account_Record_Fund_Data() { return fund_Account_Record_Fund_Data; }

    public DbsGroup getFund_Account_Record_Fund_DataRedef13() { return fund_Account_Record_Fund_DataRedef13; }

    public DbsField getFund_Account_Record_Fund_Account_Name() { return fund_Account_Record_Fund_Account_Name; }

    public DbsField getFund_Account_Record_Fund_Account_Type() { return fund_Account_Record_Fund_Account_Type; }

    public DbsField getFund_Account_Record_Fund_Payment_Method() { return fund_Account_Record_Fund_Payment_Method; }

    public DbsField getFund_Account_Record_Fund_Change_Method() { return fund_Account_Record_Fund_Change_Method; }

    public DbsField getFund_Account_Record_Fund_Prev_Contract_Amt() { return fund_Account_Record_Fund_Prev_Contract_Amt; }

    public DbsField getFund_Account_Record_Fund_Prev_Dividend() { return fund_Account_Record_Fund_Prev_Dividend; }

    public DbsField getFund_Account_Record_Fund_Prev_Units() { return fund_Account_Record_Fund_Prev_Units; }

    public DbsField getFund_Account_Record_Fund_Prev_Unit_Value() { return fund_Account_Record_Fund_Prev_Unit_Value; }

    public DbsGroup getFund_Account_Record_Fund_Prev_Unit_ValueRedef14() { return fund_Account_Record_Fund_Prev_Unit_ValueRedef14; }

    public DbsField getFund_Account_Record_Fund_Prev_Unit_Value_Ask() { return fund_Account_Record_Fund_Prev_Unit_Value_Ask; }

    public DbsField getFund_Account_Record_Fund_Prev_Total_Payment() { return fund_Account_Record_Fund_Prev_Total_Payment; }

    public DbsField getFund_Account_Record_Fund_Prev_Total_Gross_Payment() { return fund_Account_Record_Fund_Prev_Total_Gross_Payment; }

    public DbsField getFund_Account_Record_Fund_Curr_Contract_Amt() { return fund_Account_Record_Fund_Curr_Contract_Amt; }

    public DbsField getFund_Account_Record_Fund_Curr_Dividend() { return fund_Account_Record_Fund_Curr_Dividend; }

    public DbsField getFund_Account_Record_Fund_Curr_Units() { return fund_Account_Record_Fund_Curr_Units; }

    public DbsField getFund_Account_Record_Fund_Curr_Unit_Value() { return fund_Account_Record_Fund_Curr_Unit_Value; }

    public DbsGroup getFund_Account_Record_Fund_Curr_Unit_ValueRedef15() { return fund_Account_Record_Fund_Curr_Unit_ValueRedef15; }

    public DbsField getFund_Account_Record_Fund_Curr_Unit_Value_Ask() { return fund_Account_Record_Fund_Curr_Unit_Value_Ask; }

    public DbsField getFund_Account_Record_Fund_Curr_Total_Payment() { return fund_Account_Record_Fund_Curr_Total_Payment; }

    public DbsField getFund_Account_Record_Fund_Curr_Total_Gross_Payment() { return fund_Account_Record_Fund_Curr_Total_Gross_Payment; }

    public DbsField getFund_Account_Record_Fund_Fill_Num() { return fund_Account_Record_Fund_Fill_Num; }

    public DbsField getFund_Account_Record_Fund_Fill_Alpha() { return fund_Account_Record_Fund_Fill_Alpha; }

    public DbsGroup getDed_Record() { return ded_Record; }

    public DbsField getDed_Record_Ded_Record_Id() { return ded_Record_Ded_Record_Id; }

    public DbsField getDed_Record_Ded_Data_Length() { return ded_Record_Ded_Data_Length; }

    public DbsField getDed_Record_Ded_Data_Occurs() { return ded_Record_Ded_Data_Occurs; }

    public DbsField getDed_Record_Ded_Data() { return ded_Record_Ded_Data; }

    public DbsGroup getDed_Record_Ded_DataRedef16() { return ded_Record_Ded_DataRedef16; }

    public DbsField getDed_Record_Ded_Description() { return ded_Record_Ded_Description; }

    public DbsField getDed_Record_Ded_Prev_Amt() { return ded_Record_Ded_Prev_Amt; }

    public DbsField getDed_Record_Ded_Curr_Amt() { return ded_Record_Ded_Curr_Amt; }

    public DbsField getDed_Record_Ded_Prev_Net_Amt() { return ded_Record_Ded_Prev_Net_Amt; }

    public DbsField getDed_Record_Ded_Curr_Net_Amt() { return ded_Record_Ded_Curr_Net_Amt; }

    public DbsField getDed_Record_Ded_Fill_Num() { return ded_Record_Ded_Fill_Num; }

    public DbsField getDed_Record_Ded_Fill_Alpha() { return ded_Record_Ded_Fill_Alpha; }

    public DbsGroup getMsg_Record() { return msg_Record; }

    public DbsField getMsg_Record_Msg_Record_Id() { return msg_Record_Msg_Record_Id; }

    public DbsField getMsg_Record_Msg_Data_Length() { return msg_Record_Msg_Data_Length; }

    public DbsField getMsg_Record_Msg_Data_Occurs() { return msg_Record_Msg_Data_Occurs; }

    public DbsField getMsg_Record_Msg_Data() { return msg_Record_Msg_Data; }

    public DbsGroup getMsg_Record_Msg_DataRedef17() { return msg_Record_Msg_DataRedef17; }

    public DbsField getMsg_Record_Msg_Id() { return msg_Record_Msg_Id; }

    public DbsField getMsg_Record_Msg_Date_1() { return msg_Record_Msg_Date_1; }

    public DbsGroup getMsg_Record_Msg_Date_1Redef18() { return msg_Record_Msg_Date_1Redef18; }

    public DbsField getMsg_Record_Msg_Date1_Mmdd() { return msg_Record_Msg_Date1_Mmdd; }

    public DbsField getMsg_Record_Msg_Date1_Ccyy() { return msg_Record_Msg_Date1_Ccyy; }

    public DbsGroup getMsg_Record_Msg_Date_1Redef19() { return msg_Record_Msg_Date_1Redef19; }

    public DbsField getMsg_Record_Msg_Date_Mm_1() { return msg_Record_Msg_Date_Mm_1; }

    public DbsField getMsg_Record_Msg_Date_Dd_1() { return msg_Record_Msg_Date_Dd_1; }

    public DbsField getMsg_Record_Msg_Date_Cc_1() { return msg_Record_Msg_Date_Cc_1; }

    public DbsField getMsg_Record_Msg_Date_Yy_1() { return msg_Record_Msg_Date_Yy_1; }

    public DbsField getMsg_Record_Msg_Date_2() { return msg_Record_Msg_Date_2; }

    public DbsGroup getMsg_Record_Msg_Date_2Redef20() { return msg_Record_Msg_Date_2Redef20; }

    public DbsField getMsg_Record_Msg_Date2_Mmdd() { return msg_Record_Msg_Date2_Mmdd; }

    public DbsField getMsg_Record_Msg_Date2_Ccyy() { return msg_Record_Msg_Date2_Ccyy; }

    public DbsGroup getMsg_Record_Msg_Date_2Redef21() { return msg_Record_Msg_Date_2Redef21; }

    public DbsField getMsg_Record_Msg_Date_Mm_2() { return msg_Record_Msg_Date_Mm_2; }

    public DbsField getMsg_Record_Msg_Date_Dd_2() { return msg_Record_Msg_Date_Dd_2; }

    public DbsField getMsg_Record_Msg_Date_Cc_2() { return msg_Record_Msg_Date_Cc_2; }

    public DbsField getMsg_Record_Msg_Date_Yy_2() { return msg_Record_Msg_Date_Yy_2; }

    public DbsField getMsg_Record_Msg_Increase_Percent() { return msg_Record_Msg_Increase_Percent; }

    public DbsField getMsg_Record_Msg_Phone_Number() { return msg_Record_Msg_Phone_Number; }

    public DbsField getMsg_Record_Msg_Numeric_1() { return msg_Record_Msg_Numeric_1; }

    public DbsField getMsg_Record_Msg_Numeric_2() { return msg_Record_Msg_Numeric_2; }

    public DbsField getMsg_Record_Msg_Numeric_3() { return msg_Record_Msg_Numeric_3; }

    public DbsField getMsg_Record_Msg_Char_1() { return msg_Record_Msg_Char_1; }

    public DbsField getMsg_Record_Msg_Char_2() { return msg_Record_Msg_Char_2; }

    public DbsField getMsg_Record_Msg_Char_3() { return msg_Record_Msg_Char_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Sort_Key_Layout = newGroupInRecord("pnd_Sort_Key_Layout", "#SORT-KEY-LAYOUT");
        pnd_Sort_Key_Layout_Pnd_Sort_Key_Length = pnd_Sort_Key_Layout.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Sort_Key_Length", "#SORT-KEY-LENGTH", FieldType.NUMERIC, 
            3);
        pnd_Sort_Key_Layout_Pnd_Sort_Key_Array = pnd_Sort_Key_Layout.newFieldArrayInGroup("pnd_Sort_Key_Layout_Pnd_Sort_Key_Array", "#SORT-KEY-ARRAY", 
            FieldType.STRING, 1, new DbsArrayController(1,41));
        pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1 = pnd_Sort_Key_Layout.newGroupInGroup("pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1", "Redefines", 
            pnd_Sort_Key_Layout_Pnd_Sort_Key_Array);
        pnd_Sort_Key_Layout_Pnd_Sort_Key = pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1.newGroupInGroup("pnd_Sort_Key_Layout_Pnd_Sort_Key", "#SORT-KEY");
        pnd_Sort_Key_Layout_Pnd_L1_Sort_Flds = pnd_Sort_Key_Layout_Pnd_Sort_Key.newGroupInGroup("pnd_Sort_Key_Layout_Pnd_L1_Sort_Flds", "#L1-SORT-FLDS");
        pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_N = pnd_Sort_Key_Layout_Pnd_L1_Sort_Flds.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_N", "#RSK-L1-VALUE-N", 
            FieldType.NUMERIC, 2);
        pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_NRedef2 = pnd_Sort_Key_Layout_Pnd_L1_Sort_Flds.newGroupInGroup("pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_NRedef2", 
            "Redefines", pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_N);
        pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value = pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value_NRedef2.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Rsk_L1_Value", "#RSK-L1-VALUE", 
            FieldType.STRING, 2);
        pnd_Sort_Key_Layout_Pnd_Id_L1 = pnd_Sort_Key_Layout_Pnd_L1_Sort_Flds.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Id_L1", "#ID-L1", FieldType.STRING, 
            1);
        pnd_Sort_Key_Layout_Pnd_L2_Sort_Flds = pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1.newFieldArrayInGroup("pnd_Sort_Key_Layout_Pnd_L2_Sort_Flds", 
            "#L2-SORT-FLDS", FieldType.STRING, 1, new DbsArrayController(1,21));
        pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3 = pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1.newGroupInGroup("pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3", 
            "Redefines", pnd_Sort_Key_Layout_Pnd_L2_Sort_Flds);
        pnd_Sort_Key_Layout_Pnd_Id_L2 = pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Id_L2", "#ID-L2", FieldType.STRING, 
            1);
        pnd_Sort_Key_Layout_Pnd_Key_L2_Area = pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L2_Area", "#KEY-L2-AREA", 
            FieldType.STRING, 3);
        pnd_Sort_Key_Layout_Pnd_Key_L2_Nbr_Of_Contracts = pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L2_Nbr_Of_Contracts", 
            "#KEY-L2-NBR-OF-CONTRACTS", FieldType.NUMERIC, 3);
        pnd_Sort_Key_Layout_Pnd_Key_L2_Pin = pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L2_Pin", "#KEY-L2-PIN", 
            FieldType.NUMERIC, 12);
        pnd_Sort_Key_Layout_Pnd_Key_L2_Payee_Cde = pnd_Sort_Key_Layout_Pnd_L2_Sort_FldsRedef3.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L2_Payee_Cde", 
            "#KEY-L2-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Sort_Key_Layout_Pnd_L3_Sort_Flds = pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1.newFieldArrayInGroup("pnd_Sort_Key_Layout_Pnd_L3_Sort_Flds", 
            "#L3-SORT-FLDS", FieldType.STRING, 1, new DbsArrayController(1,11));
        pnd_Sort_Key_Layout_Pnd_L3_Sort_FldsRedef4 = pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1.newGroupInGroup("pnd_Sort_Key_Layout_Pnd_L3_Sort_FldsRedef4", 
            "Redefines", pnd_Sort_Key_Layout_Pnd_L3_Sort_Flds);
        pnd_Sort_Key_Layout_Pnd_Id_L3 = pnd_Sort_Key_Layout_Pnd_L3_Sort_FldsRedef4.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Id_L3", "#ID-L3", FieldType.STRING, 
            1);
        pnd_Sort_Key_Layout_Pnd_Key_L3_Tiebreak = pnd_Sort_Key_Layout_Pnd_L3_Sort_FldsRedef4.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L3_Tiebreak", 
            "#KEY-L3-TIEBREAK", FieldType.STRING, 10);
        pnd_Sort_Key_Layout_Pnd_L4_Sort_Flds = pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1.newFieldArrayInGroup("pnd_Sort_Key_Layout_Pnd_L4_Sort_Flds", 
            "#L4-SORT-FLDS", FieldType.STRING, 1, new DbsArrayController(1,6));
        pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5 = pnd_Sort_Key_Layout_Pnd_Sort_Key_ArrayRedef1.newGroupInGroup("pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5", 
            "Redefines", pnd_Sort_Key_Layout_Pnd_L4_Sort_Flds);
        pnd_Sort_Key_Layout_Pnd_Id_L4 = pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Id_L4", "#ID-L4", FieldType.STRING, 
            1);
        pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak = pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak", 
            "#KEY-L4-TIEBREAK", FieldType.NUMERIC, 1);
        pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2 = pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2", 
            "#KEY-L4-TIEBREAK2", FieldType.STRING, 1);
        pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3 = pnd_Sort_Key_Layout_Pnd_L4_Sort_FldsRedef5.newFieldInGroup("pnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3", 
            "#KEY-L4-TIEBREAK3", FieldType.NUMERIC, 3);

        co_Header_Record = newGroupInRecord("co_Header_Record", "CO-HEADER-RECORD");
        co_Header_Record_Co_Record_Id = co_Header_Record.newFieldInGroup("co_Header_Record_Co_Record_Id", "CO-RECORD-ID", FieldType.STRING, 2);
        co_Header_Record_Co_Record_Length = co_Header_Record.newFieldInGroup("co_Header_Record_Co_Record_Length", "CO-RECORD-LENGTH", FieldType.NUMERIC, 
            5);
        co_Header_Record_Co_Data_Occurs = co_Header_Record.newFieldInGroup("co_Header_Record_Co_Data_Occurs", "CO-DATA-OCCURS", FieldType.NUMERIC, 5);
        co_Header_Record_Co_Data = co_Header_Record.newFieldArrayInGroup("co_Header_Record_Co_Data", "CO-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            16));
        co_Header_Record_Co_DataRedef6 = co_Header_Record.newGroupInGroup("co_Header_Record_Co_DataRedef6", "Redefines", co_Header_Record_Co_Data);
        co_Header_Record_Co_Application_Record = co_Header_Record_Co_DataRedef6.newFieldInGroup("co_Header_Record_Co_Application_Record", "CO-APPLICATION-RECORD", 
            FieldType.STRING, 8);
        co_Header_Record_Co_Statement_Date = co_Header_Record_Co_DataRedef6.newFieldInGroup("co_Header_Record_Co_Statement_Date", "CO-STATEMENT-DATE", 
            FieldType.NUMERIC, 8);
        co_Header_Record_Co_Statement_DateRedef7 = co_Header_Record_Co_DataRedef6.newGroupInGroup("co_Header_Record_Co_Statement_DateRedef7", "Redefines", 
            co_Header_Record_Co_Statement_Date);
        co_Header_Record_Co_Statement_Month = co_Header_Record_Co_Statement_DateRedef7.newFieldInGroup("co_Header_Record_Co_Statement_Month", "CO-STATEMENT-MONTH", 
            FieldType.NUMERIC, 2);
        co_Header_Record_Co_Statement_Day = co_Header_Record_Co_Statement_DateRedef7.newFieldInGroup("co_Header_Record_Co_Statement_Day", "CO-STATEMENT-DAY", 
            FieldType.NUMERIC, 2);
        co_Header_Record_Co_Statement_Year = co_Header_Record_Co_Statement_DateRedef7.newFieldInGroup("co_Header_Record_Co_Statement_Year", "CO-STATEMENT-YEAR", 
            FieldType.NUMERIC, 4);

        mail_Item_Record = newGroupInRecord("mail_Item_Record", "MAIL-ITEM-RECORD");
        mail_Item_Record_Mail_Record_Id = mail_Item_Record.newFieldInGroup("mail_Item_Record_Mail_Record_Id", "MAIL-RECORD-ID", FieldType.STRING, 2);
        mail_Item_Record_Mail_Data_Length = mail_Item_Record.newFieldInGroup("mail_Item_Record_Mail_Data_Length", "MAIL-DATA-LENGTH", FieldType.NUMERIC, 
            5);
        mail_Item_Record_Mail_Data_Occurs = mail_Item_Record.newFieldInGroup("mail_Item_Record_Mail_Data_Occurs", "MAIL-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        mail_Item_Record_Mail_Data = mail_Item_Record.newFieldArrayInGroup("mail_Item_Record_Mail_Data", "MAIL-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            394));
        mail_Item_Record_Mail_DataRedef8 = mail_Item_Record.newGroupInGroup("mail_Item_Record_Mail_DataRedef8", "Redefines", mail_Item_Record_Mail_Data);
        mail_Item_Record_Mail_Pin_Number = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Pin_Number", "MAIL-PIN-NUMBER", FieldType.STRING, 
            12);
        mail_Item_Record_Mail_Postnet_Barcode = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Postnet_Barcode", "MAIL-POSTNET-BARCODE", 
            FieldType.STRING, 12);
        mail_Item_Record_Mail_Ph_Name_Address = mail_Item_Record_Mail_DataRedef8.newGroupInGroup("mail_Item_Record_Mail_Ph_Name_Address", "MAIL-PH-NAME-ADDRESS");
        mail_Item_Record_Mail_Ph_Full_Name = mail_Item_Record_Mail_Ph_Name_Address.newFieldInGroup("mail_Item_Record_Mail_Ph_Full_Name", "MAIL-PH-FULL-NAME", 
            FieldType.STRING, 35);
        mail_Item_Record_Mail_Ph_Address1 = mail_Item_Record_Mail_Ph_Name_Address.newFieldInGroup("mail_Item_Record_Mail_Ph_Address1", "MAIL-PH-ADDRESS1", 
            FieldType.STRING, 35);
        mail_Item_Record_Mail_Ph_Address2 = mail_Item_Record_Mail_Ph_Name_Address.newFieldInGroup("mail_Item_Record_Mail_Ph_Address2", "MAIL-PH-ADDRESS2", 
            FieldType.STRING, 35);
        mail_Item_Record_Mail_Ph_Address3 = mail_Item_Record_Mail_Ph_Name_Address.newFieldInGroup("mail_Item_Record_Mail_Ph_Address3", "MAIL-PH-ADDRESS3", 
            FieldType.STRING, 35);
        mail_Item_Record_Mail_Ph_Address4 = mail_Item_Record_Mail_Ph_Name_Address.newFieldInGroup("mail_Item_Record_Mail_Ph_Address4", "MAIL-PH-ADDRESS4", 
            FieldType.STRING, 35);
        mail_Item_Record_Mail_Ph_Address5 = mail_Item_Record_Mail_Ph_Name_Address.newFieldInGroup("mail_Item_Record_Mail_Ph_Address5", "MAIL-PH-ADDRESS5", 
            FieldType.STRING, 35);
        mail_Item_Record_Mail_Ph_Address6 = mail_Item_Record_Mail_Ph_Name_Address.newFieldInGroup("mail_Item_Record_Mail_Ph_Address6", "MAIL-PH-ADDRESS6", 
            FieldType.STRING, 35);
        mail_Item_Record_Mail_Work_Prcss_Id = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Work_Prcss_Id", "MAIL-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        mail_Item_Record_Mail_Image_Id = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Image_Id", "MAIL-IMAGE-ID", FieldType.STRING, 
            11);
        mail_Item_Record_Mail_Rqst_Id = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Rqst_Id", "MAIL-RQST-ID", FieldType.STRING, 
            11);
        mail_Item_Record_Mail_Environment = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Environment", "MAIL-ENVIRONMENT", 
            FieldType.STRING, 4);
        mail_Item_Record_Mail_Mach_Func_2 = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Mach_Func_2", "MAIL-MACH-FUNC-2", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Mach_Func_3 = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Mach_Func_3", "MAIL-MACH-FUNC-3", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Mach_Func_4 = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Mach_Func_4", "MAIL-MACH-FUNC-4", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Mach_Func_5 = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Mach_Func_5", "MAIL-MACH-FUNC-5", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Package_Id = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Package_Id", "MAIL-PACKAGE-ID", FieldType.STRING, 
            12);
        mail_Item_Record_Mail_Package_IdRedef9 = mail_Item_Record_Mail_DataRedef8.newGroupInGroup("mail_Item_Record_Mail_Package_IdRedef9", "Redefines", 
            mail_Item_Record_Mail_Package_Id);
        mail_Item_Record_Mail_Pkid_Byte_1 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_1", "MAIL-PKID-BYTE-1", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_2 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_2", "MAIL-PKID-BYTE-2", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_3 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_3", "MAIL-PKID-BYTE-3", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_4 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_4", "MAIL-PKID-BYTE-4", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_5 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_5", "MAIL-PKID-BYTE-5", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_6 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_6", "MAIL-PKID-BYTE-6", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_7 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_7", "MAIL-PKID-BYTE-7", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_8 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_8", "MAIL-PKID-BYTE-8", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_9 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_9", "MAIL-PKID-BYTE-9", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_10 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_10", "MAIL-PKID-BYTE-10", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_11 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_11", "MAIL-PKID-BYTE-11", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Pkid_Byte_12 = mail_Item_Record_Mail_Package_IdRedef9.newFieldInGroup("mail_Item_Record_Mail_Pkid_Byte_12", "MAIL-PKID-BYTE-12", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Document_Count = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Document_Count", "MAIL-DOCUMENT-COUNT", 
            FieldType.NUMERIC, 3);
        mail_Item_Record_Mail_Document_Bad_Ind = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Document_Bad_Ind", "MAIL-DOCUMENT-BAD-IND", 
            FieldType.STRING, 1);
        mail_Item_Record_Mail_Finish_Profile = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Finish_Profile", "MAIL-FINISH-PROFILE", 
            FieldType.STRING, 2);
        mail_Item_Record_Mail_Postnet_Zip_Full = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Postnet_Zip_Full", "MAIL-POSTNET-ZIP-FULL", 
            FieldType.STRING, 14);
        mail_Item_Record_Mail_Package_Delivery_Type = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Package_Delivery_Type", 
            "MAIL-PACKAGE-DELIVERY-TYPE", FieldType.STRING, 3);
        mail_Item_Record_Mail_Image_Source_Id = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Image_Source_Id", "MAIL-IMAGE-SOURCE-ID", 
            FieldType.STRING, 6);
        mail_Item_Record_Mail_Summary_Prev_Total = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Summary_Prev_Total", "MAIL-SUMMARY-PREV-TOTAL", 
            FieldType.DECIMAL, 9,2);
        mail_Item_Record_Mail_Summary_Curr_Total = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Summary_Curr_Total", "MAIL-SUMMARY-CURR-TOTAL", 
            FieldType.DECIMAL, 9,2);
        mail_Item_Record_Mail_Fill_Num = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Fill_Num", "MAIL-FILL-NUM", FieldType.NUMERIC, 
            20);
        mail_Item_Record_Mail_Fill_Alpha = mail_Item_Record_Mail_DataRedef8.newFieldInGroup("mail_Item_Record_Mail_Fill_Alpha", "MAIL-FILL-ALPHA", FieldType.STRING, 
            10);

        contract_Record = newGroupInRecord("contract_Record", "CONTRACT-RECORD");
        contract_Record_Contract_Record_Id = contract_Record.newFieldInGroup("contract_Record_Contract_Record_Id", "CONTRACT-RECORD-ID", FieldType.STRING, 
            2);
        contract_Record_Contract_Data_Length = contract_Record.newFieldInGroup("contract_Record_Contract_Data_Length", "CONTRACT-DATA-LENGTH", FieldType.NUMERIC, 
            5);
        contract_Record_Contract_Data_Occurs = contract_Record.newFieldInGroup("contract_Record_Contract_Data_Occurs", "CONTRACT-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        contract_Record_Contract_Data = contract_Record.newFieldArrayInGroup("contract_Record_Contract_Data", "CONTRACT-DATA", FieldType.STRING, 1, new 
            DbsArrayController(1,70));
        contract_Record_Contract_DataRedef10 = contract_Record.newGroupInGroup("contract_Record_Contract_DataRedef10", "Redefines", contract_Record_Contract_Data);
        contract_Record_Contract_Prev_Payment_Date = contract_Record_Contract_DataRedef10.newFieldInGroup("contract_Record_Contract_Prev_Payment_Date", 
            "CONTRACT-PREV-PAYMENT-DATE", FieldType.NUMERIC, 8);
        contract_Record_Contract_Prev_Payment_DateRedef11 = contract_Record_Contract_DataRedef10.newGroupInGroup("contract_Record_Contract_Prev_Payment_DateRedef11", 
            "Redefines", contract_Record_Contract_Prev_Payment_Date);
        contract_Record_Contract_Prev_Pay_Month = contract_Record_Contract_Prev_Payment_DateRedef11.newFieldInGroup("contract_Record_Contract_Prev_Pay_Month", 
            "CONTRACT-PREV-PAY-MONTH", FieldType.NUMERIC, 2);
        contract_Record_Contract_Prev_Pay_Day = contract_Record_Contract_Prev_Payment_DateRedef11.newFieldInGroup("contract_Record_Contract_Prev_Pay_Day", 
            "CONTRACT-PREV-PAY-DAY", FieldType.NUMERIC, 2);
        contract_Record_Contract_Prev_Pay_Cc = contract_Record_Contract_Prev_Payment_DateRedef11.newFieldInGroup("contract_Record_Contract_Prev_Pay_Cc", 
            "CONTRACT-PREV-PAY-CC", FieldType.NUMERIC, 2);
        contract_Record_Contract_Prev_Pay_Yy = contract_Record_Contract_Prev_Payment_DateRedef11.newFieldInGroup("contract_Record_Contract_Prev_Pay_Yy", 
            "CONTRACT-PREV-PAY-YY", FieldType.NUMERIC, 2);
        contract_Record_Contract_Curr_Payment_Date = contract_Record_Contract_DataRedef10.newFieldInGroup("contract_Record_Contract_Curr_Payment_Date", 
            "CONTRACT-CURR-PAYMENT-DATE", FieldType.NUMERIC, 8);
        contract_Record_Contract_Curr_Payment_DateRedef12 = contract_Record_Contract_DataRedef10.newGroupInGroup("contract_Record_Contract_Curr_Payment_DateRedef12", 
            "Redefines", contract_Record_Contract_Curr_Payment_Date);
        contract_Record_Contract_Curr_Pay_Month = contract_Record_Contract_Curr_Payment_DateRedef12.newFieldInGroup("contract_Record_Contract_Curr_Pay_Month", 
            "CONTRACT-CURR-PAY-MONTH", FieldType.NUMERIC, 2);
        contract_Record_Contract_Curr_Pay_Day = contract_Record_Contract_Curr_Payment_DateRedef12.newFieldInGroup("contract_Record_Contract_Curr_Pay_Day", 
            "CONTRACT-CURR-PAY-DAY", FieldType.NUMERIC, 2);
        contract_Record_Contract_Curr_Pay_Cc = contract_Record_Contract_Curr_Payment_DateRedef12.newFieldInGroup("contract_Record_Contract_Curr_Pay_Cc", 
            "CONTRACT-CURR-PAY-CC", FieldType.NUMERIC, 2);
        contract_Record_Contract_Curr_Pay_Yy = contract_Record_Contract_Curr_Payment_DateRedef12.newFieldInGroup("contract_Record_Contract_Curr_Pay_Yy", 
            "CONTRACT-CURR-PAY-YY", FieldType.NUMERIC, 2);
        contract_Record_Contract_Number = contract_Record_Contract_DataRedef10.newFieldInGroup("contract_Record_Contract_Number", "CONTRACT-NUMBER", FieldType.STRING, 
            13);
        contract_Record_Contract_Type = contract_Record_Contract_DataRedef10.newFieldInGroup("contract_Record_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 
            1);
        contract_Record_Contract_Fill_Num = contract_Record_Contract_DataRedef10.newFieldInGroup("contract_Record_Contract_Fill_Num", "CONTRACT-FILL-NUM", 
            FieldType.NUMERIC, 20);
        contract_Record_Contract_Fill_Alpha = contract_Record_Contract_DataRedef10.newFieldInGroup("contract_Record_Contract_Fill_Alpha", "CONTRACT-FILL-ALPHA", 
            FieldType.STRING, 20);

        fund_Account_Record = newGroupInRecord("fund_Account_Record", "FUND-ACCOUNT-RECORD");
        fund_Account_Record_Fund_Record_Id = fund_Account_Record.newFieldInGroup("fund_Account_Record_Fund_Record_Id", "FUND-RECORD-ID", FieldType.STRING, 
            2);
        fund_Account_Record_Fund_Data_Length = fund_Account_Record.newFieldInGroup("fund_Account_Record_Fund_Data_Length", "FUND-DATA-LENGTH", FieldType.NUMERIC, 
            5);
        fund_Account_Record_Fund_Data_Occurs = fund_Account_Record.newFieldInGroup("fund_Account_Record_Fund_Data_Occurs", "FUND-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        fund_Account_Record_Fund_Data = fund_Account_Record.newFieldArrayInGroup("fund_Account_Record_Fund_Data", "FUND-DATA", FieldType.STRING, 1, new 
            DbsArrayController(1,199));
        fund_Account_Record_Fund_DataRedef13 = fund_Account_Record.newGroupInGroup("fund_Account_Record_Fund_DataRedef13", "Redefines", fund_Account_Record_Fund_Data);
        fund_Account_Record_Fund_Account_Name = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Account_Name", "FUND-ACCOUNT-NAME", 
            FieldType.STRING, 40);
        fund_Account_Record_Fund_Account_Type = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Account_Type", "FUND-ACCOUNT-TYPE", 
            FieldType.STRING, 1);
        fund_Account_Record_Fund_Payment_Method = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Payment_Method", "FUND-PAYMENT-METHOD", 
            FieldType.STRING, 1);
        fund_Account_Record_Fund_Change_Method = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Change_Method", "FUND-CHANGE-METHOD", 
            FieldType.STRING, 1);
        fund_Account_Record_Fund_Prev_Contract_Amt = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Prev_Contract_Amt", 
            "FUND-PREV-CONTRACT-AMT", FieldType.DECIMAL, 9,2);
        fund_Account_Record_Fund_Prev_Dividend = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Prev_Dividend", "FUND-PREV-DIVIDEND", 
            FieldType.DECIMAL, 9,2);
        fund_Account_Record_Fund_Prev_Units = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Prev_Units", "FUND-PREV-UNITS", 
            FieldType.DECIMAL, 9,3);
        fund_Account_Record_Fund_Prev_Unit_Value = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Prev_Unit_Value", "FUND-PREV-UNIT-VALUE", 
            FieldType.DECIMAL, 9,4);
        fund_Account_Record_Fund_Prev_Unit_ValueRedef14 = fund_Account_Record_Fund_DataRedef13.newGroupInGroup("fund_Account_Record_Fund_Prev_Unit_ValueRedef14", 
            "Redefines", fund_Account_Record_Fund_Prev_Unit_Value);
        fund_Account_Record_Fund_Prev_Unit_Value_Ask = fund_Account_Record_Fund_Prev_Unit_ValueRedef14.newFieldInGroup("fund_Account_Record_Fund_Prev_Unit_Value_Ask", 
            "FUND-PREV-UNIT-VALUE-ASK", FieldType.STRING, 9);
        fund_Account_Record_Fund_Prev_Total_Payment = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Prev_Total_Payment", 
            "FUND-PREV-TOTAL-PAYMENT", FieldType.DECIMAL, 11,2);
        fund_Account_Record_Fund_Prev_Total_Gross_Payment = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Prev_Total_Gross_Payment", 
            "FUND-PREV-TOTAL-GROSS-PAYMENT", FieldType.DECIMAL, 11,2);
        fund_Account_Record_Fund_Curr_Contract_Amt = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Curr_Contract_Amt", 
            "FUND-CURR-CONTRACT-AMT", FieldType.DECIMAL, 9,2);
        fund_Account_Record_Fund_Curr_Dividend = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Curr_Dividend", "FUND-CURR-DIVIDEND", 
            FieldType.DECIMAL, 9,2);
        fund_Account_Record_Fund_Curr_Units = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Curr_Units", "FUND-CURR-UNITS", 
            FieldType.DECIMAL, 9,3);
        fund_Account_Record_Fund_Curr_Unit_Value = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Curr_Unit_Value", "FUND-CURR-UNIT-VALUE", 
            FieldType.DECIMAL, 9,4);
        fund_Account_Record_Fund_Curr_Unit_ValueRedef15 = fund_Account_Record_Fund_DataRedef13.newGroupInGroup("fund_Account_Record_Fund_Curr_Unit_ValueRedef15", 
            "Redefines", fund_Account_Record_Fund_Curr_Unit_Value);
        fund_Account_Record_Fund_Curr_Unit_Value_Ask = fund_Account_Record_Fund_Curr_Unit_ValueRedef15.newFieldInGroup("fund_Account_Record_Fund_Curr_Unit_Value_Ask", 
            "FUND-CURR-UNIT-VALUE-ASK", FieldType.STRING, 9);
        fund_Account_Record_Fund_Curr_Total_Payment = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Curr_Total_Payment", 
            "FUND-CURR-TOTAL-PAYMENT", FieldType.DECIMAL, 11,2);
        fund_Account_Record_Fund_Curr_Total_Gross_Payment = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Curr_Total_Gross_Payment", 
            "FUND-CURR-TOTAL-GROSS-PAYMENT", FieldType.DECIMAL, 11,2);
        fund_Account_Record_Fund_Fill_Num = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Fill_Num", "FUND-FILL-NUM", 
            FieldType.NUMERIC, 20);
        fund_Account_Record_Fund_Fill_Alpha = fund_Account_Record_Fund_DataRedef13.newFieldInGroup("fund_Account_Record_Fund_Fill_Alpha", "FUND-FILL-ALPHA", 
            FieldType.STRING, 20);

        ded_Record = newGroupInRecord("ded_Record", "DED-RECORD");
        ded_Record_Ded_Record_Id = ded_Record.newFieldInGroup("ded_Record_Ded_Record_Id", "DED-RECORD-ID", FieldType.STRING, 2);
        ded_Record_Ded_Data_Length = ded_Record.newFieldInGroup("ded_Record_Ded_Data_Length", "DED-DATA-LENGTH", FieldType.NUMERIC, 5);
        ded_Record_Ded_Data_Occurs = ded_Record.newFieldInGroup("ded_Record_Ded_Data_Occurs", "DED-DATA-OCCURS", FieldType.NUMERIC, 5);
        ded_Record_Ded_Data = ded_Record.newFieldArrayInGroup("ded_Record_Ded_Data", "DED-DATA", FieldType.STRING, 1, new DbsArrayController(1,106));
        ded_Record_Ded_DataRedef16 = ded_Record.newGroupInGroup("ded_Record_Ded_DataRedef16", "Redefines", ded_Record_Ded_Data);
        ded_Record_Ded_Description = ded_Record_Ded_DataRedef16.newFieldInGroup("ded_Record_Ded_Description", "DED-DESCRIPTION", FieldType.STRING, 30);
        ded_Record_Ded_Prev_Amt = ded_Record_Ded_DataRedef16.newFieldInGroup("ded_Record_Ded_Prev_Amt", "DED-PREV-AMT", FieldType.DECIMAL, 7,2);
        ded_Record_Ded_Curr_Amt = ded_Record_Ded_DataRedef16.newFieldInGroup("ded_Record_Ded_Curr_Amt", "DED-CURR-AMT", FieldType.DECIMAL, 7,2);
        ded_Record_Ded_Prev_Net_Amt = ded_Record_Ded_DataRedef16.newFieldInGroup("ded_Record_Ded_Prev_Net_Amt", "DED-PREV-NET-AMT", FieldType.DECIMAL, 
            11,2);
        ded_Record_Ded_Curr_Net_Amt = ded_Record_Ded_DataRedef16.newFieldInGroup("ded_Record_Ded_Curr_Net_Amt", "DED-CURR-NET-AMT", FieldType.DECIMAL, 
            11,2);
        ded_Record_Ded_Fill_Num = ded_Record_Ded_DataRedef16.newFieldInGroup("ded_Record_Ded_Fill_Num", "DED-FILL-NUM", FieldType.NUMERIC, 20);
        ded_Record_Ded_Fill_Alpha = ded_Record_Ded_DataRedef16.newFieldInGroup("ded_Record_Ded_Fill_Alpha", "DED-FILL-ALPHA", FieldType.STRING, 20);

        msg_Record = newGroupInRecord("msg_Record", "MSG-RECORD");
        msg_Record_Msg_Record_Id = msg_Record.newFieldInGroup("msg_Record_Msg_Record_Id", "MSG-RECORD-ID", FieldType.STRING, 2);
        msg_Record_Msg_Data_Length = msg_Record.newFieldInGroup("msg_Record_Msg_Data_Length", "MSG-DATA-LENGTH", FieldType.NUMERIC, 5);
        msg_Record_Msg_Data_Occurs = msg_Record.newFieldInGroup("msg_Record_Msg_Data_Occurs", "MSG-DATA-OCCURS", FieldType.NUMERIC, 5);
        msg_Record_Msg_Data = msg_Record.newFieldArrayInGroup("msg_Record_Msg_Data", "MSG-DATA", FieldType.STRING, 1, new DbsArrayController(1,124));
        msg_Record_Msg_DataRedef17 = msg_Record.newGroupInGroup("msg_Record_Msg_DataRedef17", "Redefines", msg_Record_Msg_Data);
        msg_Record_Msg_Id = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Id", "MSG-ID", FieldType.STRING, 2);
        msg_Record_Msg_Date_1 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Date_1", "MSG-DATE-1", FieldType.NUMERIC, 8);
        msg_Record_Msg_Date_1Redef18 = msg_Record_Msg_DataRedef17.newGroupInGroup("msg_Record_Msg_Date_1Redef18", "Redefines", msg_Record_Msg_Date_1);
        msg_Record_Msg_Date1_Mmdd = msg_Record_Msg_Date_1Redef18.newFieldInGroup("msg_Record_Msg_Date1_Mmdd", "MSG-DATE1-MMDD", FieldType.NUMERIC, 4);
        msg_Record_Msg_Date1_Ccyy = msg_Record_Msg_Date_1Redef18.newFieldInGroup("msg_Record_Msg_Date1_Ccyy", "MSG-DATE1-CCYY", FieldType.NUMERIC, 4);
        msg_Record_Msg_Date_1Redef19 = msg_Record_Msg_DataRedef17.newGroupInGroup("msg_Record_Msg_Date_1Redef19", "Redefines", msg_Record_Msg_Date_1);
        msg_Record_Msg_Date_Mm_1 = msg_Record_Msg_Date_1Redef19.newFieldInGroup("msg_Record_Msg_Date_Mm_1", "MSG-DATE-MM-1", FieldType.NUMERIC, 2);
        msg_Record_Msg_Date_Dd_1 = msg_Record_Msg_Date_1Redef19.newFieldInGroup("msg_Record_Msg_Date_Dd_1", "MSG-DATE-DD-1", FieldType.NUMERIC, 2);
        msg_Record_Msg_Date_Cc_1 = msg_Record_Msg_Date_1Redef19.newFieldInGroup("msg_Record_Msg_Date_Cc_1", "MSG-DATE-CC-1", FieldType.NUMERIC, 2);
        msg_Record_Msg_Date_Yy_1 = msg_Record_Msg_Date_1Redef19.newFieldInGroup("msg_Record_Msg_Date_Yy_1", "MSG-DATE-YY-1", FieldType.NUMERIC, 2);
        msg_Record_Msg_Date_2 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Date_2", "MSG-DATE-2", FieldType.NUMERIC, 8);
        msg_Record_Msg_Date_2Redef20 = msg_Record_Msg_DataRedef17.newGroupInGroup("msg_Record_Msg_Date_2Redef20", "Redefines", msg_Record_Msg_Date_2);
        msg_Record_Msg_Date2_Mmdd = msg_Record_Msg_Date_2Redef20.newFieldInGroup("msg_Record_Msg_Date2_Mmdd", "MSG-DATE2-MMDD", FieldType.NUMERIC, 4);
        msg_Record_Msg_Date2_Ccyy = msg_Record_Msg_Date_2Redef20.newFieldInGroup("msg_Record_Msg_Date2_Ccyy", "MSG-DATE2-CCYY", FieldType.NUMERIC, 4);
        msg_Record_Msg_Date_2Redef21 = msg_Record_Msg_DataRedef17.newGroupInGroup("msg_Record_Msg_Date_2Redef21", "Redefines", msg_Record_Msg_Date_2);
        msg_Record_Msg_Date_Mm_2 = msg_Record_Msg_Date_2Redef21.newFieldInGroup("msg_Record_Msg_Date_Mm_2", "MSG-DATE-MM-2", FieldType.NUMERIC, 2);
        msg_Record_Msg_Date_Dd_2 = msg_Record_Msg_Date_2Redef21.newFieldInGroup("msg_Record_Msg_Date_Dd_2", "MSG-DATE-DD-2", FieldType.NUMERIC, 2);
        msg_Record_Msg_Date_Cc_2 = msg_Record_Msg_Date_2Redef21.newFieldInGroup("msg_Record_Msg_Date_Cc_2", "MSG-DATE-CC-2", FieldType.NUMERIC, 2);
        msg_Record_Msg_Date_Yy_2 = msg_Record_Msg_Date_2Redef21.newFieldInGroup("msg_Record_Msg_Date_Yy_2", "MSG-DATE-YY-2", FieldType.NUMERIC, 2);
        msg_Record_Msg_Increase_Percent = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Increase_Percent", "MSG-INCREASE-PERCENT", FieldType.DECIMAL, 
            5,3);
        msg_Record_Msg_Phone_Number = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Phone_Number", "MSG-PHONE-NUMBER", FieldType.STRING, 
            14);
        msg_Record_Msg_Numeric_1 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Numeric_1", "MSG-NUMERIC-1", FieldType.NUMERIC, 6);
        msg_Record_Msg_Numeric_2 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Numeric_2", "MSG-NUMERIC-2", FieldType.NUMERIC, 12);
        msg_Record_Msg_Numeric_3 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Numeric_3", "MSG-NUMERIC-3", FieldType.NUMERIC, 9);
        msg_Record_Msg_Char_1 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Char_1", "MSG-CHAR-1", FieldType.STRING, 20);
        msg_Record_Msg_Char_2 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Char_2", "MSG-CHAR-2", FieldType.STRING, 20);
        msg_Record_Msg_Char_3 = msg_Record_Msg_DataRedef17.newFieldInGroup("msg_Record_Msg_Char_3", "MSG-CHAR-3", FieldType.STRING, 20);

        this.setRecordName("LdaIaaa029a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Sort_Key_Layout_Pnd_Sort_Key_Length.setInitialValue(36);
        co_Header_Record_Co_Record_Id.setInitialValue("01");
        co_Header_Record_Co_Record_Length.setInitialValue(16);
        co_Header_Record_Co_Data_Occurs.setInitialValue(1);
        mail_Item_Record_Mail_Record_Id.setInitialValue("02");
        mail_Item_Record_Mail_Data_Length.setInitialValue(394);
        mail_Item_Record_Mail_Data_Occurs.setInitialValue(1);
        contract_Record_Contract_Record_Id.setInitialValue("03");
        contract_Record_Contract_Data_Length.setInitialValue(70);
        contract_Record_Contract_Data_Occurs.setInitialValue(1);
        fund_Account_Record_Fund_Record_Id.setInitialValue("04");
        fund_Account_Record_Fund_Data_Length.setInitialValue(199);
        fund_Account_Record_Fund_Data_Occurs.setInitialValue(1);
        ded_Record_Ded_Record_Id.setInitialValue("05");
        ded_Record_Ded_Data_Length.setInitialValue(106);
        ded_Record_Ded_Data_Occurs.setInitialValue(1);
        msg_Record_Msg_Record_Id.setInitialValue("06");
        msg_Record_Msg_Data_Length.setInitialValue(124);
        msg_Record_Msg_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaIaaa029a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
