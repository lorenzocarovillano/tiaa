/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:41 PM
**        * FROM NATURAL LDA     : PSTL6365
************************************************************
**        * FILE NAME            : LdaPstl6365.java
**        * CLASS NAME           : LdaPstl6365
**        * INSTANCE NAME        : LdaPstl6365
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl6365 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key_Struct;
    private DbsField sort_Key_Struct_Sort_Key_Lgth;
    private DbsField sort_Key_Struct_Sort_Key_Data;
    private DbsGroup sort_Key_Struct_Sort_Key_DataRedef1;
    private DbsGroup sort_Key_Struct_Sort_Key;
    private DbsGroup sort_Key_Struct_Level_1_Sort_Flds;
    private DbsField sort_Key_Struct_Rsk_L1_Value;
    private DbsField sort_Key_Struct_Id_L1_Record;
    private DbsGroup sort_Key_Struct_Level_2_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L2_Record;
    private DbsField sort_Key_Struct_Key_L2_Post_Req_Id;
    private DbsGroup sort_Key_Struct_Level_3_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L3_Record;
    private DbsField sort_Key_Struct_Key_L3_Req_Date_Time;
    private DbsField sort_Key_Struct_Key_L3_From_Cont_Number;
    private DbsGroup sort_Key_Struct_Level_3_Sort_FldsRedef2;
    private DbsField sort_Key_Struct_Level_3_Sort_Flds_Reset;
    private DbsGroup sort_Key_Struct_Level_4_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L4_Record;
    private DbsField sort_Key_Struct_Key_L4_Account_Type;
    private DbsField sort_Key_Struct_Key_L4_Fyi_Mes_Num;
    private DbsField sort_Key_Struct_Key_L4_Quest_Mes_Num;
    private DbsGroup sort_Key_Struct_Level_4_Sort_FldsRedef3;
    private DbsField sort_Key_Struct_Level_4_Sort_Flds_Reset;
    private DbsGroup ac_Contract_Struct;
    private DbsField ac_Contract_Struct_Ac_Record_Id;
    private DbsField ac_Contract_Struct_Ac_Data_Lgth;
    private DbsField ac_Contract_Struct_Ac_Data_Occurs;
    private DbsField ac_Contract_Struct_Ac_Data;
    private DbsGroup ac_Contract_Struct_Ac_DataRedef4;
    private DbsGroup ac_Contract_Struct_Ac_Contract;
    private DbsGroup ac_Contract_Struct_Ac_Contract_Numbers;
    private DbsField ac_Contract_Struct_Ac_Tiaa_Contract_Num;
    private DbsField ac_Contract_Struct_Ac_Tiaa_Contract_Tbd;
    private DbsField ac_Contract_Struct_Ac_Cref_Contract_Num;
    private DbsField ac_Contract_Struct_Ac_Cref_Contract_Tbd;
    private DbsField ac_Contract_Struct_Ac_Real_Estate_Contract;
    private DbsField ac_Contract_Struct_Ac_Real_Estate_Tbd;
    private DbsField ac_Contract_Struct_Ac_Req_Source;
    private DbsField ac_Contract_Struct_Ac_Service_Rep;
    private DbsField ac_Contract_Struct_Ac_Ack_Type;
    private DbsField ac_Contract_Struct_Ac_Frm_Switch;
    private DbsField ac_Contract_Struct_Ac_Effective_Date;
    private DbsGroup ac_Contract_Struct_Ac_Effective_DateRedef5;
    private DbsField ac_Contract_Struct_Ac_Effective_Mm;
    private DbsField ac_Contract_Struct_Ac_Effective_Dd;
    private DbsField ac_Contract_Struct_Ac_Effective_Yy;
    private DbsField ac_Contract_Struct_Ac_From_Tran_Type;
    private DbsField ac_Contract_Struct_Ac_To_Tran_Type;
    private DbsField ac_Contract_Struct_Ac_Mod_Date_Time;
    private DbsGroup ac_Contract_Struct_Ac_Mod_Date_TimeRedef6;
    private DbsField ac_Contract_Struct_Ac_Mod_Mm;
    private DbsField ac_Contract_Struct_Ac_Mod_Dd;
    private DbsField ac_Contract_Struct_Ac_Mod_Yy;
    private DbsField ac_Contract_Struct_Ac_Mod_Hh;
    private DbsField ac_Contract_Struct_Ac_Mod_Min;
    private DbsField ac_Contract_Struct_Ac_Mod_Sec;
    private DbsField ac_Contract_Struct_Ac_Mod_Ampm;
    private DbsField ac_Contract_Struct_Ac_Mach_Func_1;
    private DbsField ac_Contract_Struct_Ac_Image_Ind;
    private DbsGroup ac_Frm_Rec_Struct;
    private DbsField ac_Frm_Rec_Struct_Ac_Record_Id;
    private DbsField ac_Frm_Rec_Struct_Ac_Data_Lgth;
    private DbsField ac_Frm_Rec_Struct_Ac_Data_Occurs;
    private DbsField ac_Frm_Rec_Struct_Ac_Data;
    private DbsGroup ac_Frm_Rec_Struct_Ac_DataRedef7;
    private DbsGroup ac_Frm_Rec_Struct_Ac_Frm_Rec;
    private DbsField ac_Frm_Rec_Struct_Ac_Frm_Account_Type;
    private DbsField ac_Frm_Rec_Struct_Ac_Frm_Myi;
    private DbsField ac_Frm_Rec_Struct_Ac_Frm_Trn_Typ;
    private DbsField ac_Frm_Rec_Struct_Ac_Frm_Percent;
    private DbsField ac_Frm_Rec_Struct_Ac_Frm_Units;
    private DbsField ac_Frm_Rec_Struct_Ac_Frm_Dollars;
    private DbsGroup ac_To_Rec_Struct;
    private DbsField ac_To_Rec_Struct_Ac_Record_Id;
    private DbsField ac_To_Rec_Struct_Ac_Data_Lgth;
    private DbsField ac_To_Rec_Struct_Ac_Data_Occurs;
    private DbsField ac_To_Rec_Struct_Ac_Data;
    private DbsGroup ac_To_Rec_Struct_Ac_DataRedef8;
    private DbsGroup ac_To_Rec_Struct_Ac_To_Rec;
    private DbsField ac_To_Rec_Struct_Ac_To_Account_Type;
    private DbsField ac_To_Rec_Struct_Ac_To_Myi;
    private DbsField ac_To_Rec_Struct_Ac_To_Trn_Typ;
    private DbsField ac_To_Rec_Struct_Ac_To_Percent;
    private DbsField ac_To_Rec_Struct_Ac_To_Units;
    private DbsField ac_To_Rec_Struct_Ac_To_Dollars;
    private DbsGroup ac_Fyi_Struct;
    private DbsField ac_Fyi_Struct_Ac_Record_Id;
    private DbsField ac_Fyi_Struct_Ac_Data_Lgth;
    private DbsField ac_Fyi_Struct_Ac_Data_Occurs;
    private DbsField ac_Fyi_Struct_Ac_Data;
    private DbsGroup ac_Fyi_Struct_Ac_DataRedef9;
    private DbsGroup ac_Fyi_Struct_Ac_Fyi;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Msg_Number;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Srt_Num;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date1_F;
    private DbsGroup ac_Fyi_Struct_Ac_Fyi_Date1_FRedef10;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date1_Mm;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date1_Dd;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date1_Yy;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date2_F;
    private DbsGroup ac_Fyi_Struct_Ac_Fyi_Date2_FRedef11;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date2_Mm;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date2_Dd;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Date2_Yy;
    private DbsField ac_Fyi_Struct_Ac_Fyi_Vartxt;

    public DbsGroup getSort_Key_Struct() { return sort_Key_Struct; }

    public DbsField getSort_Key_Struct_Sort_Key_Lgth() { return sort_Key_Struct_Sort_Key_Lgth; }

    public DbsField getSort_Key_Struct_Sort_Key_Data() { return sort_Key_Struct_Sort_Key_Data; }

    public DbsGroup getSort_Key_Struct_Sort_Key_DataRedef1() { return sort_Key_Struct_Sort_Key_DataRedef1; }

    public DbsGroup getSort_Key_Struct_Sort_Key() { return sort_Key_Struct_Sort_Key; }

    public DbsGroup getSort_Key_Struct_Level_1_Sort_Flds() { return sort_Key_Struct_Level_1_Sort_Flds; }

    public DbsField getSort_Key_Struct_Rsk_L1_Value() { return sort_Key_Struct_Rsk_L1_Value; }

    public DbsField getSort_Key_Struct_Id_L1_Record() { return sort_Key_Struct_Id_L1_Record; }

    public DbsGroup getSort_Key_Struct_Level_2_Sort_Flds() { return sort_Key_Struct_Level_2_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L2_Record() { return sort_Key_Struct_Id_L2_Record; }

    public DbsField getSort_Key_Struct_Key_L2_Post_Req_Id() { return sort_Key_Struct_Key_L2_Post_Req_Id; }

    public DbsGroup getSort_Key_Struct_Level_3_Sort_Flds() { return sort_Key_Struct_Level_3_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L3_Record() { return sort_Key_Struct_Id_L3_Record; }

    public DbsField getSort_Key_Struct_Key_L3_Req_Date_Time() { return sort_Key_Struct_Key_L3_Req_Date_Time; }

    public DbsField getSort_Key_Struct_Key_L3_From_Cont_Number() { return sort_Key_Struct_Key_L3_From_Cont_Number; }

    public DbsGroup getSort_Key_Struct_Level_3_Sort_FldsRedef2() { return sort_Key_Struct_Level_3_Sort_FldsRedef2; }

    public DbsField getSort_Key_Struct_Level_3_Sort_Flds_Reset() { return sort_Key_Struct_Level_3_Sort_Flds_Reset; }

    public DbsGroup getSort_Key_Struct_Level_4_Sort_Flds() { return sort_Key_Struct_Level_4_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L4_Record() { return sort_Key_Struct_Id_L4_Record; }

    public DbsField getSort_Key_Struct_Key_L4_Account_Type() { return sort_Key_Struct_Key_L4_Account_Type; }

    public DbsField getSort_Key_Struct_Key_L4_Fyi_Mes_Num() { return sort_Key_Struct_Key_L4_Fyi_Mes_Num; }

    public DbsField getSort_Key_Struct_Key_L4_Quest_Mes_Num() { return sort_Key_Struct_Key_L4_Quest_Mes_Num; }

    public DbsGroup getSort_Key_Struct_Level_4_Sort_FldsRedef3() { return sort_Key_Struct_Level_4_Sort_FldsRedef3; }

    public DbsField getSort_Key_Struct_Level_4_Sort_Flds_Reset() { return sort_Key_Struct_Level_4_Sort_Flds_Reset; }

    public DbsGroup getAc_Contract_Struct() { return ac_Contract_Struct; }

    public DbsField getAc_Contract_Struct_Ac_Record_Id() { return ac_Contract_Struct_Ac_Record_Id; }

    public DbsField getAc_Contract_Struct_Ac_Data_Lgth() { return ac_Contract_Struct_Ac_Data_Lgth; }

    public DbsField getAc_Contract_Struct_Ac_Data_Occurs() { return ac_Contract_Struct_Ac_Data_Occurs; }

    public DbsField getAc_Contract_Struct_Ac_Data() { return ac_Contract_Struct_Ac_Data; }

    public DbsGroup getAc_Contract_Struct_Ac_DataRedef4() { return ac_Contract_Struct_Ac_DataRedef4; }

    public DbsGroup getAc_Contract_Struct_Ac_Contract() { return ac_Contract_Struct_Ac_Contract; }

    public DbsGroup getAc_Contract_Struct_Ac_Contract_Numbers() { return ac_Contract_Struct_Ac_Contract_Numbers; }

    public DbsField getAc_Contract_Struct_Ac_Tiaa_Contract_Num() { return ac_Contract_Struct_Ac_Tiaa_Contract_Num; }

    public DbsField getAc_Contract_Struct_Ac_Tiaa_Contract_Tbd() { return ac_Contract_Struct_Ac_Tiaa_Contract_Tbd; }

    public DbsField getAc_Contract_Struct_Ac_Cref_Contract_Num() { return ac_Contract_Struct_Ac_Cref_Contract_Num; }

    public DbsField getAc_Contract_Struct_Ac_Cref_Contract_Tbd() { return ac_Contract_Struct_Ac_Cref_Contract_Tbd; }

    public DbsField getAc_Contract_Struct_Ac_Real_Estate_Contract() { return ac_Contract_Struct_Ac_Real_Estate_Contract; }

    public DbsField getAc_Contract_Struct_Ac_Real_Estate_Tbd() { return ac_Contract_Struct_Ac_Real_Estate_Tbd; }

    public DbsField getAc_Contract_Struct_Ac_Req_Source() { return ac_Contract_Struct_Ac_Req_Source; }

    public DbsField getAc_Contract_Struct_Ac_Service_Rep() { return ac_Contract_Struct_Ac_Service_Rep; }

    public DbsField getAc_Contract_Struct_Ac_Ack_Type() { return ac_Contract_Struct_Ac_Ack_Type; }

    public DbsField getAc_Contract_Struct_Ac_Frm_Switch() { return ac_Contract_Struct_Ac_Frm_Switch; }

    public DbsField getAc_Contract_Struct_Ac_Effective_Date() { return ac_Contract_Struct_Ac_Effective_Date; }

    public DbsGroup getAc_Contract_Struct_Ac_Effective_DateRedef5() { return ac_Contract_Struct_Ac_Effective_DateRedef5; }

    public DbsField getAc_Contract_Struct_Ac_Effective_Mm() { return ac_Contract_Struct_Ac_Effective_Mm; }

    public DbsField getAc_Contract_Struct_Ac_Effective_Dd() { return ac_Contract_Struct_Ac_Effective_Dd; }

    public DbsField getAc_Contract_Struct_Ac_Effective_Yy() { return ac_Contract_Struct_Ac_Effective_Yy; }

    public DbsField getAc_Contract_Struct_Ac_From_Tran_Type() { return ac_Contract_Struct_Ac_From_Tran_Type; }

    public DbsField getAc_Contract_Struct_Ac_To_Tran_Type() { return ac_Contract_Struct_Ac_To_Tran_Type; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Date_Time() { return ac_Contract_Struct_Ac_Mod_Date_Time; }

    public DbsGroup getAc_Contract_Struct_Ac_Mod_Date_TimeRedef6() { return ac_Contract_Struct_Ac_Mod_Date_TimeRedef6; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Mm() { return ac_Contract_Struct_Ac_Mod_Mm; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Dd() { return ac_Contract_Struct_Ac_Mod_Dd; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Yy() { return ac_Contract_Struct_Ac_Mod_Yy; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Hh() { return ac_Contract_Struct_Ac_Mod_Hh; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Min() { return ac_Contract_Struct_Ac_Mod_Min; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Sec() { return ac_Contract_Struct_Ac_Mod_Sec; }

    public DbsField getAc_Contract_Struct_Ac_Mod_Ampm() { return ac_Contract_Struct_Ac_Mod_Ampm; }

    public DbsField getAc_Contract_Struct_Ac_Mach_Func_1() { return ac_Contract_Struct_Ac_Mach_Func_1; }

    public DbsField getAc_Contract_Struct_Ac_Image_Ind() { return ac_Contract_Struct_Ac_Image_Ind; }

    public DbsGroup getAc_Frm_Rec_Struct() { return ac_Frm_Rec_Struct; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Record_Id() { return ac_Frm_Rec_Struct_Ac_Record_Id; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Data_Lgth() { return ac_Frm_Rec_Struct_Ac_Data_Lgth; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Data_Occurs() { return ac_Frm_Rec_Struct_Ac_Data_Occurs; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Data() { return ac_Frm_Rec_Struct_Ac_Data; }

    public DbsGroup getAc_Frm_Rec_Struct_Ac_DataRedef7() { return ac_Frm_Rec_Struct_Ac_DataRedef7; }

    public DbsGroup getAc_Frm_Rec_Struct_Ac_Frm_Rec() { return ac_Frm_Rec_Struct_Ac_Frm_Rec; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Frm_Account_Type() { return ac_Frm_Rec_Struct_Ac_Frm_Account_Type; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Frm_Myi() { return ac_Frm_Rec_Struct_Ac_Frm_Myi; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Frm_Trn_Typ() { return ac_Frm_Rec_Struct_Ac_Frm_Trn_Typ; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Frm_Percent() { return ac_Frm_Rec_Struct_Ac_Frm_Percent; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Frm_Units() { return ac_Frm_Rec_Struct_Ac_Frm_Units; }

    public DbsField getAc_Frm_Rec_Struct_Ac_Frm_Dollars() { return ac_Frm_Rec_Struct_Ac_Frm_Dollars; }

    public DbsGroup getAc_To_Rec_Struct() { return ac_To_Rec_Struct; }

    public DbsField getAc_To_Rec_Struct_Ac_Record_Id() { return ac_To_Rec_Struct_Ac_Record_Id; }

    public DbsField getAc_To_Rec_Struct_Ac_Data_Lgth() { return ac_To_Rec_Struct_Ac_Data_Lgth; }

    public DbsField getAc_To_Rec_Struct_Ac_Data_Occurs() { return ac_To_Rec_Struct_Ac_Data_Occurs; }

    public DbsField getAc_To_Rec_Struct_Ac_Data() { return ac_To_Rec_Struct_Ac_Data; }

    public DbsGroup getAc_To_Rec_Struct_Ac_DataRedef8() { return ac_To_Rec_Struct_Ac_DataRedef8; }

    public DbsGroup getAc_To_Rec_Struct_Ac_To_Rec() { return ac_To_Rec_Struct_Ac_To_Rec; }

    public DbsField getAc_To_Rec_Struct_Ac_To_Account_Type() { return ac_To_Rec_Struct_Ac_To_Account_Type; }

    public DbsField getAc_To_Rec_Struct_Ac_To_Myi() { return ac_To_Rec_Struct_Ac_To_Myi; }

    public DbsField getAc_To_Rec_Struct_Ac_To_Trn_Typ() { return ac_To_Rec_Struct_Ac_To_Trn_Typ; }

    public DbsField getAc_To_Rec_Struct_Ac_To_Percent() { return ac_To_Rec_Struct_Ac_To_Percent; }

    public DbsField getAc_To_Rec_Struct_Ac_To_Units() { return ac_To_Rec_Struct_Ac_To_Units; }

    public DbsField getAc_To_Rec_Struct_Ac_To_Dollars() { return ac_To_Rec_Struct_Ac_To_Dollars; }

    public DbsGroup getAc_Fyi_Struct() { return ac_Fyi_Struct; }

    public DbsField getAc_Fyi_Struct_Ac_Record_Id() { return ac_Fyi_Struct_Ac_Record_Id; }

    public DbsField getAc_Fyi_Struct_Ac_Data_Lgth() { return ac_Fyi_Struct_Ac_Data_Lgth; }

    public DbsField getAc_Fyi_Struct_Ac_Data_Occurs() { return ac_Fyi_Struct_Ac_Data_Occurs; }

    public DbsField getAc_Fyi_Struct_Ac_Data() { return ac_Fyi_Struct_Ac_Data; }

    public DbsGroup getAc_Fyi_Struct_Ac_DataRedef9() { return ac_Fyi_Struct_Ac_DataRedef9; }

    public DbsGroup getAc_Fyi_Struct_Ac_Fyi() { return ac_Fyi_Struct_Ac_Fyi; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Msg_Number() { return ac_Fyi_Struct_Ac_Fyi_Msg_Number; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Srt_Num() { return ac_Fyi_Struct_Ac_Fyi_Srt_Num; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date1_F() { return ac_Fyi_Struct_Ac_Fyi_Date1_F; }

    public DbsGroup getAc_Fyi_Struct_Ac_Fyi_Date1_FRedef10() { return ac_Fyi_Struct_Ac_Fyi_Date1_FRedef10; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date1_Mm() { return ac_Fyi_Struct_Ac_Fyi_Date1_Mm; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date1_Dd() { return ac_Fyi_Struct_Ac_Fyi_Date1_Dd; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date1_Yy() { return ac_Fyi_Struct_Ac_Fyi_Date1_Yy; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date2_F() { return ac_Fyi_Struct_Ac_Fyi_Date2_F; }

    public DbsGroup getAc_Fyi_Struct_Ac_Fyi_Date2_FRedef11() { return ac_Fyi_Struct_Ac_Fyi_Date2_FRedef11; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date2_Mm() { return ac_Fyi_Struct_Ac_Fyi_Date2_Mm; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date2_Dd() { return ac_Fyi_Struct_Ac_Fyi_Date2_Dd; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Date2_Yy() { return ac_Fyi_Struct_Ac_Fyi_Date2_Yy; }

    public DbsField getAc_Fyi_Struct_Ac_Fyi_Vartxt() { return ac_Fyi_Struct_Ac_Fyi_Vartxt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key_Struct = newGroupInRecord("sort_Key_Struct", "SORT-KEY-STRUCT");
        sort_Key_Struct_Sort_Key_Lgth = sort_Key_Struct.newFieldInGroup("sort_Key_Struct_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Struct_Sort_Key_Data = sort_Key_Struct.newFieldArrayInGroup("sort_Key_Struct_Sort_Key_Data", "SORT-KEY-DATA", FieldType.STRING, 1, new 
            DbsArrayController(1,55));
        sort_Key_Struct_Sort_Key_DataRedef1 = sort_Key_Struct.newGroupInGroup("sort_Key_Struct_Sort_Key_DataRedef1", "Redefines", sort_Key_Struct_Sort_Key_Data);
        sort_Key_Struct_Sort_Key = sort_Key_Struct_Sort_Key_DataRedef1.newGroupInGroup("sort_Key_Struct_Sort_Key", "SORT-KEY");
        sort_Key_Struct_Level_1_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_1_Sort_Flds", "LEVEL-1-SORT-FLDS");
        sort_Key_Struct_Rsk_L1_Value = sort_Key_Struct_Level_1_Sort_Flds.newFieldInGroup("sort_Key_Struct_Rsk_L1_Value", "RSK-L1-VALUE", FieldType.NUMERIC, 
            2);
        sort_Key_Struct_Id_L1_Record = sort_Key_Struct_Level_1_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L1_Record", "ID-L1-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Level_2_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_2_Sort_Flds", "LEVEL-2-SORT-FLDS");
        sort_Key_Struct_Id_L2_Record = sort_Key_Struct_Level_2_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L2_Record", "ID-L2-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L2_Post_Req_Id = sort_Key_Struct_Level_2_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L2_Post_Req_Id", "KEY-L2-POST-REQ-ID", 
            FieldType.STRING, 11);
        sort_Key_Struct_Level_3_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_3_Sort_Flds", "LEVEL-3-SORT-FLDS");
        sort_Key_Struct_Id_L3_Record = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L3_Record", "ID-L3-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L3_Req_Date_Time = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L3_Req_Date_Time", "KEY-L3-REQ-DATE-TIME", 
            FieldType.STRING, 15);
        sort_Key_Struct_Key_L3_From_Cont_Number = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L3_From_Cont_Number", "KEY-L3-FROM-CONT-NUMBER", 
            FieldType.STRING, 15);
        sort_Key_Struct_Level_3_Sort_FldsRedef2 = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_3_Sort_FldsRedef2", "Redefines", sort_Key_Struct_Level_3_Sort_Flds);
        sort_Key_Struct_Level_3_Sort_Flds_Reset = sort_Key_Struct_Level_3_Sort_FldsRedef2.newFieldInGroup("sort_Key_Struct_Level_3_Sort_Flds_Reset", "LEVEL-3-SORT-FLDS-RESET", 
            FieldType.STRING, 31);
        sort_Key_Struct_Level_4_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_4_Sort_Flds", "LEVEL-4-SORT-FLDS");
        sort_Key_Struct_Id_L4_Record = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L4_Record", "ID-L4-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L4_Account_Type = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L4_Account_Type", "KEY-L4-ACCOUNT-TYPE", 
            FieldType.NUMERIC, 2);
        sort_Key_Struct_Key_L4_Fyi_Mes_Num = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L4_Fyi_Mes_Num", "KEY-L4-FYI-MES-NUM", 
            FieldType.NUMERIC, 3);
        sort_Key_Struct_Key_L4_Quest_Mes_Num = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L4_Quest_Mes_Num", "KEY-L4-QUEST-MES-NUM", 
            FieldType.NUMERIC, 3);
        sort_Key_Struct_Level_4_Sort_FldsRedef3 = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_4_Sort_FldsRedef3", "Redefines", sort_Key_Struct_Level_4_Sort_Flds);
        sort_Key_Struct_Level_4_Sort_Flds_Reset = sort_Key_Struct_Level_4_Sort_FldsRedef3.newFieldInGroup("sort_Key_Struct_Level_4_Sort_Flds_Reset", "LEVEL-4-SORT-FLDS-RESET", 
            FieldType.STRING, 9);

        ac_Contract_Struct = newGroupInRecord("ac_Contract_Struct", "AC-CONTRACT-STRUCT");
        ac_Contract_Struct_Ac_Record_Id = ac_Contract_Struct.newFieldInGroup("ac_Contract_Struct_Ac_Record_Id", "AC-RECORD-ID", FieldType.STRING, 2);
        ac_Contract_Struct_Ac_Data_Lgth = ac_Contract_Struct.newFieldInGroup("ac_Contract_Struct_Ac_Data_Lgth", "AC-DATA-LGTH", FieldType.NUMERIC, 5);
        ac_Contract_Struct_Ac_Data_Occurs = ac_Contract_Struct.newFieldInGroup("ac_Contract_Struct_Ac_Data_Occurs", "AC-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        ac_Contract_Struct_Ac_Data = ac_Contract_Struct.newFieldArrayInGroup("ac_Contract_Struct_Ac_Data", "AC-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            104));
        ac_Contract_Struct_Ac_DataRedef4 = ac_Contract_Struct.newGroupInGroup("ac_Contract_Struct_Ac_DataRedef4", "Redefines", ac_Contract_Struct_Ac_Data);
        ac_Contract_Struct_Ac_Contract = ac_Contract_Struct_Ac_DataRedef4.newGroupInGroup("ac_Contract_Struct_Ac_Contract", "AC-CONTRACT");
        ac_Contract_Struct_Ac_Contract_Numbers = ac_Contract_Struct_Ac_Contract.newGroupInGroup("ac_Contract_Struct_Ac_Contract_Numbers", "AC-CONTRACT-NUMBERS");
        ac_Contract_Struct_Ac_Tiaa_Contract_Num = ac_Contract_Struct_Ac_Contract_Numbers.newFieldInGroup("ac_Contract_Struct_Ac_Tiaa_Contract_Num", "AC-TIAA-CONTRACT-NUM", 
            FieldType.STRING, 8);
        ac_Contract_Struct_Ac_Tiaa_Contract_Tbd = ac_Contract_Struct_Ac_Contract_Numbers.newFieldInGroup("ac_Contract_Struct_Ac_Tiaa_Contract_Tbd", "AC-TIAA-CONTRACT-TBD", 
            FieldType.STRING, 1);
        ac_Contract_Struct_Ac_Cref_Contract_Num = ac_Contract_Struct_Ac_Contract_Numbers.newFieldInGroup("ac_Contract_Struct_Ac_Cref_Contract_Num", "AC-CREF-CONTRACT-NUM", 
            FieldType.STRING, 8);
        ac_Contract_Struct_Ac_Cref_Contract_Tbd = ac_Contract_Struct_Ac_Contract_Numbers.newFieldInGroup("ac_Contract_Struct_Ac_Cref_Contract_Tbd", "AC-CREF-CONTRACT-TBD", 
            FieldType.STRING, 1);
        ac_Contract_Struct_Ac_Real_Estate_Contract = ac_Contract_Struct_Ac_Contract_Numbers.newFieldInGroup("ac_Contract_Struct_Ac_Real_Estate_Contract", 
            "AC-REAL-ESTATE-CONTRACT", FieldType.STRING, 15);
        ac_Contract_Struct_Ac_Real_Estate_Tbd = ac_Contract_Struct_Ac_Contract_Numbers.newFieldInGroup("ac_Contract_Struct_Ac_Real_Estate_Tbd", "AC-REAL-ESTATE-TBD", 
            FieldType.STRING, 1);
        ac_Contract_Struct_Ac_Req_Source = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Req_Source", "AC-REQ-SOURCE", FieldType.STRING, 
            1);
        ac_Contract_Struct_Ac_Service_Rep = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Service_Rep", "AC-SERVICE-REP", FieldType.STRING, 
            40);
        ac_Contract_Struct_Ac_Ack_Type = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Ack_Type", "AC-ACK-TYPE", FieldType.STRING, 
            1);
        ac_Contract_Struct_Ac_Frm_Switch = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Frm_Switch", "AC-FRM-SWITCH", FieldType.STRING, 
            1);
        ac_Contract_Struct_Ac_Effective_Date = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Effective_Date", "AC-EFFECTIVE-DATE", 
            FieldType.NUMERIC, 8);
        ac_Contract_Struct_Ac_Effective_DateRedef5 = ac_Contract_Struct_Ac_Contract.newGroupInGroup("ac_Contract_Struct_Ac_Effective_DateRedef5", "Redefines", 
            ac_Contract_Struct_Ac_Effective_Date);
        ac_Contract_Struct_Ac_Effective_Mm = ac_Contract_Struct_Ac_Effective_DateRedef5.newFieldInGroup("ac_Contract_Struct_Ac_Effective_Mm", "AC-EFFECTIVE-MM", 
            FieldType.NUMERIC, 2);
        ac_Contract_Struct_Ac_Effective_Dd = ac_Contract_Struct_Ac_Effective_DateRedef5.newFieldInGroup("ac_Contract_Struct_Ac_Effective_Dd", "AC-EFFECTIVE-DD", 
            FieldType.NUMERIC, 2);
        ac_Contract_Struct_Ac_Effective_Yy = ac_Contract_Struct_Ac_Effective_DateRedef5.newFieldInGroup("ac_Contract_Struct_Ac_Effective_Yy", "AC-EFFECTIVE-YY", 
            FieldType.NUMERIC, 4);
        ac_Contract_Struct_Ac_From_Tran_Type = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_From_Tran_Type", "AC-FROM-TRAN-TYPE", 
            FieldType.STRING, 1);
        ac_Contract_Struct_Ac_To_Tran_Type = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_To_Tran_Type", "AC-TO-TRAN-TYPE", FieldType.STRING, 
            1);
        ac_Contract_Struct_Ac_Mod_Date_Time = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Date_Time", "AC-MOD-DATE-TIME", 
            FieldType.NUMERIC, 15);
        ac_Contract_Struct_Ac_Mod_Date_TimeRedef6 = ac_Contract_Struct_Ac_Contract.newGroupInGroup("ac_Contract_Struct_Ac_Mod_Date_TimeRedef6", "Redefines", 
            ac_Contract_Struct_Ac_Mod_Date_Time);
        ac_Contract_Struct_Ac_Mod_Mm = ac_Contract_Struct_Ac_Mod_Date_TimeRedef6.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Mm", "AC-MOD-MM", FieldType.NUMERIC, 
            2);
        ac_Contract_Struct_Ac_Mod_Dd = ac_Contract_Struct_Ac_Mod_Date_TimeRedef6.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Dd", "AC-MOD-DD", FieldType.NUMERIC, 
            2);
        ac_Contract_Struct_Ac_Mod_Yy = ac_Contract_Struct_Ac_Mod_Date_TimeRedef6.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Yy", "AC-MOD-YY", FieldType.NUMERIC, 
            4);
        ac_Contract_Struct_Ac_Mod_Hh = ac_Contract_Struct_Ac_Mod_Date_TimeRedef6.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Hh", "AC-MOD-HH", FieldType.NUMERIC, 
            2);
        ac_Contract_Struct_Ac_Mod_Min = ac_Contract_Struct_Ac_Mod_Date_TimeRedef6.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Min", "AC-MOD-MIN", FieldType.NUMERIC, 
            2);
        ac_Contract_Struct_Ac_Mod_Sec = ac_Contract_Struct_Ac_Mod_Date_TimeRedef6.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Sec", "AC-MOD-SEC", FieldType.NUMERIC, 
            2);
        ac_Contract_Struct_Ac_Mod_Ampm = ac_Contract_Struct_Ac_Mod_Date_TimeRedef6.newFieldInGroup("ac_Contract_Struct_Ac_Mod_Ampm", "AC-MOD-AMPM", FieldType.NUMERIC, 
            1);
        ac_Contract_Struct_Ac_Mach_Func_1 = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Mach_Func_1", "AC-MACH-FUNC-1", FieldType.STRING, 
            1);
        ac_Contract_Struct_Ac_Image_Ind = ac_Contract_Struct_Ac_Contract.newFieldInGroup("ac_Contract_Struct_Ac_Image_Ind", "AC-IMAGE-IND", FieldType.STRING, 
            1);

        ac_Frm_Rec_Struct = newGroupInRecord("ac_Frm_Rec_Struct", "AC-FRM-REC-STRUCT");
        ac_Frm_Rec_Struct_Ac_Record_Id = ac_Frm_Rec_Struct.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Record_Id", "AC-RECORD-ID", FieldType.STRING, 2);
        ac_Frm_Rec_Struct_Ac_Data_Lgth = ac_Frm_Rec_Struct.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Data_Lgth", "AC-DATA-LGTH", FieldType.NUMERIC, 5);
        ac_Frm_Rec_Struct_Ac_Data_Occurs = ac_Frm_Rec_Struct.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Data_Occurs", "AC-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        ac_Frm_Rec_Struct_Ac_Data = ac_Frm_Rec_Struct.newFieldArrayInGroup("ac_Frm_Rec_Struct_Ac_Data", "AC-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            340));
        ac_Frm_Rec_Struct_Ac_DataRedef7 = ac_Frm_Rec_Struct.newGroupInGroup("ac_Frm_Rec_Struct_Ac_DataRedef7", "Redefines", ac_Frm_Rec_Struct_Ac_Data);
        ac_Frm_Rec_Struct_Ac_Frm_Rec = ac_Frm_Rec_Struct_Ac_DataRedef7.newGroupArrayInGroup("ac_Frm_Rec_Struct_Ac_Frm_Rec", "AC-FRM-REC", new DbsArrayController(1,
            20));
        ac_Frm_Rec_Struct_Ac_Frm_Account_Type = ac_Frm_Rec_Struct_Ac_Frm_Rec.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Frm_Account_Type", "AC-FRM-ACCOUNT-TYPE", 
            FieldType.NUMERIC, 2);
        ac_Frm_Rec_Struct_Ac_Frm_Myi = ac_Frm_Rec_Struct_Ac_Frm_Rec.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Frm_Myi", "AC-FRM-MYI", FieldType.STRING, 1);
        ac_Frm_Rec_Struct_Ac_Frm_Trn_Typ = ac_Frm_Rec_Struct_Ac_Frm_Rec.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Frm_Trn_Typ", "AC-FRM-TRN-TYP", FieldType.STRING, 
            1);
        ac_Frm_Rec_Struct_Ac_Frm_Percent = ac_Frm_Rec_Struct_Ac_Frm_Rec.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Frm_Percent", "AC-FRM-PERCENT", FieldType.PACKED_DECIMAL, 
            3);
        ac_Frm_Rec_Struct_Ac_Frm_Units = ac_Frm_Rec_Struct_Ac_Frm_Rec.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Frm_Units", "AC-FRM-UNITS", FieldType.PACKED_DECIMAL, 
            10,3);
        ac_Frm_Rec_Struct_Ac_Frm_Dollars = ac_Frm_Rec_Struct_Ac_Frm_Rec.newFieldInGroup("ac_Frm_Rec_Struct_Ac_Frm_Dollars", "AC-FRM-DOLLARS", FieldType.PACKED_DECIMAL, 
            9,2);

        ac_To_Rec_Struct = newGroupInRecord("ac_To_Rec_Struct", "AC-TO-REC-STRUCT");
        ac_To_Rec_Struct_Ac_Record_Id = ac_To_Rec_Struct.newFieldInGroup("ac_To_Rec_Struct_Ac_Record_Id", "AC-RECORD-ID", FieldType.STRING, 2);
        ac_To_Rec_Struct_Ac_Data_Lgth = ac_To_Rec_Struct.newFieldInGroup("ac_To_Rec_Struct_Ac_Data_Lgth", "AC-DATA-LGTH", FieldType.NUMERIC, 5);
        ac_To_Rec_Struct_Ac_Data_Occurs = ac_To_Rec_Struct.newFieldInGroup("ac_To_Rec_Struct_Ac_Data_Occurs", "AC-DATA-OCCURS", FieldType.NUMERIC, 5);
        ac_To_Rec_Struct_Ac_Data = ac_To_Rec_Struct.newFieldArrayInGroup("ac_To_Rec_Struct_Ac_Data", "AC-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            340));
        ac_To_Rec_Struct_Ac_DataRedef8 = ac_To_Rec_Struct.newGroupInGroup("ac_To_Rec_Struct_Ac_DataRedef8", "Redefines", ac_To_Rec_Struct_Ac_Data);
        ac_To_Rec_Struct_Ac_To_Rec = ac_To_Rec_Struct_Ac_DataRedef8.newGroupArrayInGroup("ac_To_Rec_Struct_Ac_To_Rec", "AC-TO-REC", new DbsArrayController(1,
            20));
        ac_To_Rec_Struct_Ac_To_Account_Type = ac_To_Rec_Struct_Ac_To_Rec.newFieldInGroup("ac_To_Rec_Struct_Ac_To_Account_Type", "AC-TO-ACCOUNT-TYPE", 
            FieldType.NUMERIC, 2);
        ac_To_Rec_Struct_Ac_To_Myi = ac_To_Rec_Struct_Ac_To_Rec.newFieldInGroup("ac_To_Rec_Struct_Ac_To_Myi", "AC-TO-MYI", FieldType.STRING, 1);
        ac_To_Rec_Struct_Ac_To_Trn_Typ = ac_To_Rec_Struct_Ac_To_Rec.newFieldInGroup("ac_To_Rec_Struct_Ac_To_Trn_Typ", "AC-TO-TRN-TYP", FieldType.STRING, 
            1);
        ac_To_Rec_Struct_Ac_To_Percent = ac_To_Rec_Struct_Ac_To_Rec.newFieldInGroup("ac_To_Rec_Struct_Ac_To_Percent", "AC-TO-PERCENT", FieldType.PACKED_DECIMAL, 
            3);
        ac_To_Rec_Struct_Ac_To_Units = ac_To_Rec_Struct_Ac_To_Rec.newFieldInGroup("ac_To_Rec_Struct_Ac_To_Units", "AC-TO-UNITS", FieldType.PACKED_DECIMAL, 
            10,3);
        ac_To_Rec_Struct_Ac_To_Dollars = ac_To_Rec_Struct_Ac_To_Rec.newFieldInGroup("ac_To_Rec_Struct_Ac_To_Dollars", "AC-TO-DOLLARS", FieldType.PACKED_DECIMAL, 
            9,2);

        ac_Fyi_Struct = newGroupInRecord("ac_Fyi_Struct", "AC-FYI-STRUCT");
        ac_Fyi_Struct_Ac_Record_Id = ac_Fyi_Struct.newFieldInGroup("ac_Fyi_Struct_Ac_Record_Id", "AC-RECORD-ID", FieldType.STRING, 2);
        ac_Fyi_Struct_Ac_Data_Lgth = ac_Fyi_Struct.newFieldInGroup("ac_Fyi_Struct_Ac_Data_Lgth", "AC-DATA-LGTH", FieldType.NUMERIC, 5);
        ac_Fyi_Struct_Ac_Data_Occurs = ac_Fyi_Struct.newFieldInGroup("ac_Fyi_Struct_Ac_Data_Occurs", "AC-DATA-OCCURS", FieldType.NUMERIC, 5);
        ac_Fyi_Struct_Ac_Data = ac_Fyi_Struct.newFieldArrayInGroup("ac_Fyi_Struct_Ac_Data", "AC-DATA", FieldType.STRING, 1, new DbsArrayController(1,1240));
        ac_Fyi_Struct_Ac_DataRedef9 = ac_Fyi_Struct.newGroupInGroup("ac_Fyi_Struct_Ac_DataRedef9", "Redefines", ac_Fyi_Struct_Ac_Data);
        ac_Fyi_Struct_Ac_Fyi = ac_Fyi_Struct_Ac_DataRedef9.newGroupArrayInGroup("ac_Fyi_Struct_Ac_Fyi", "AC-FYI", new DbsArrayController(1,20));
        ac_Fyi_Struct_Ac_Fyi_Msg_Number = ac_Fyi_Struct_Ac_Fyi.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Msg_Number", "AC-FYI-MSG-NUMBER", FieldType.NUMERIC, 
            3);
        ac_Fyi_Struct_Ac_Fyi_Srt_Num = ac_Fyi_Struct_Ac_Fyi.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Srt_Num", "AC-FYI-SRT-NUM", FieldType.NUMERIC, 3);
        ac_Fyi_Struct_Ac_Fyi_Date1_F = ac_Fyi_Struct_Ac_Fyi.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date1_F", "AC-FYI-DATE1-F", FieldType.NUMERIC, 8);
        ac_Fyi_Struct_Ac_Fyi_Date1_FRedef10 = ac_Fyi_Struct_Ac_Fyi.newGroupInGroup("ac_Fyi_Struct_Ac_Fyi_Date1_FRedef10", "Redefines", ac_Fyi_Struct_Ac_Fyi_Date1_F);
        ac_Fyi_Struct_Ac_Fyi_Date1_Mm = ac_Fyi_Struct_Ac_Fyi_Date1_FRedef10.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date1_Mm", "AC-FYI-DATE1-MM", FieldType.NUMERIC, 
            2);
        ac_Fyi_Struct_Ac_Fyi_Date1_Dd = ac_Fyi_Struct_Ac_Fyi_Date1_FRedef10.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date1_Dd", "AC-FYI-DATE1-DD", FieldType.NUMERIC, 
            2);
        ac_Fyi_Struct_Ac_Fyi_Date1_Yy = ac_Fyi_Struct_Ac_Fyi_Date1_FRedef10.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date1_Yy", "AC-FYI-DATE1-YY", FieldType.NUMERIC, 
            4);
        ac_Fyi_Struct_Ac_Fyi_Date2_F = ac_Fyi_Struct_Ac_Fyi.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date2_F", "AC-FYI-DATE2-F", FieldType.NUMERIC, 8);
        ac_Fyi_Struct_Ac_Fyi_Date2_FRedef11 = ac_Fyi_Struct_Ac_Fyi.newGroupInGroup("ac_Fyi_Struct_Ac_Fyi_Date2_FRedef11", "Redefines", ac_Fyi_Struct_Ac_Fyi_Date2_F);
        ac_Fyi_Struct_Ac_Fyi_Date2_Mm = ac_Fyi_Struct_Ac_Fyi_Date2_FRedef11.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date2_Mm", "AC-FYI-DATE2-MM", FieldType.NUMERIC, 
            2);
        ac_Fyi_Struct_Ac_Fyi_Date2_Dd = ac_Fyi_Struct_Ac_Fyi_Date2_FRedef11.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date2_Dd", "AC-FYI-DATE2-DD", FieldType.NUMERIC, 
            2);
        ac_Fyi_Struct_Ac_Fyi_Date2_Yy = ac_Fyi_Struct_Ac_Fyi_Date2_FRedef11.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Date2_Yy", "AC-FYI-DATE2-YY", FieldType.NUMERIC, 
            4);
        ac_Fyi_Struct_Ac_Fyi_Vartxt = ac_Fyi_Struct_Ac_Fyi.newFieldInGroup("ac_Fyi_Struct_Ac_Fyi_Vartxt", "AC-FYI-VARTXT", FieldType.STRING, 40);

        this.setRecordName("LdaPstl6365");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Struct_Sort_Key_Lgth.setInitialValue(55);
        ac_Contract_Struct_Ac_Record_Id.setInitialValue("15");
        ac_Contract_Struct_Ac_Data_Lgth.setInitialValue(104);
        ac_Contract_Struct_Ac_Data_Occurs.setInitialValue(1);
        ac_Frm_Rec_Struct_Ac_Record_Id.setInitialValue("16");
        ac_Frm_Rec_Struct_Ac_Data_Lgth.setInitialValue(17);
        ac_Frm_Rec_Struct_Ac_Data_Occurs.setInitialValue(1);
        ac_To_Rec_Struct_Ac_Record_Id.setInitialValue("17");
        ac_To_Rec_Struct_Ac_Data_Lgth.setInitialValue(17);
        ac_To_Rec_Struct_Ac_Data_Occurs.setInitialValue(1);
        ac_Fyi_Struct_Ac_Record_Id.setInitialValue("18");
        ac_Fyi_Struct_Ac_Data_Lgth.setInitialValue(62);
        ac_Fyi_Struct_Ac_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaPstl6365() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
