/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:57 PM
**        * FROM NATURAL LDA     : IAAL384
************************************************************
**        * FILE NAME            : LdaIaal384.java
**        * CLASS NAME           : LdaIaal384
**        * INSTANCE NAME        : LdaIaal384
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal384 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd_View;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef1;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_CdeMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_DteMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_AmtMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_AmtMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_AmtMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_AmtMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund;

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd_View() { return vw_iaa_Tiaa_Fund_Rcrd_View; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef1() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef1; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_CdeMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_CdeMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_DteMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_DteMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_AmtMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_AmtMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_AmtMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_AmtMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_AmtMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_AmtMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_AmtMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_AmtMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Tiaa_Fund_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd_View", "IAA-TIAA-FUND-RCRD-VIEW"), "IAA_TIAA_FUND_RCRD", 
            "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr", 
            "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde", 
            "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde", 
            "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef1 = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef1", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde", 
            "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde", 
            "TIAA-FUND-CDE", FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt", 
            "TIAA-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt", 
            "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt", 
            "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt", 
            "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt", 
            "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt", 
            "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt", 
            "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt", 
            "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte", 
            "TIAA-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte", 
            "TIAA-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte", 
            "TIAA-LST-XFR-OUT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup", 
            "TIAA_RATE_GICMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic", 
            "TIAA-RATE-GIC", FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind", "TIAA-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund", 
            "TIAA-OLD-CMPNY-FUND", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        vw_iaa_Tiaa_Fund_Rcrd_View.setUniquePeList();

        this.setRecordName("LdaIaal384");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Tiaa_Fund_Rcrd_View.reset();
    }

    // Constructor
    public LdaIaal384() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
