/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:03:43 PM
**        * FROM NATURAL LDA     : IAAL915C
************************************************************
**        * FILE NAME            : LdaIaal915c.java
**        * CLASS NAME           : LdaIaal915c
**        * INSTANCE NAME        : LdaIaal915c
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal915c extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsGroup pnd_Ddctn_From_Net;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Check_DteRedef1;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy;
    private DbsField pnd_Ddctn_From_Net_Pnd_User_Area;
    private DbsField pnd_Ddctn_From_Net_Pnd_User_Id;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Trans_DteRedef2;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Record_Status;
    private DbsField pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Intent_Code;
    private DbsField pnd_Ddctn_From_Net_Pnd_Filler1;
    private DbsField pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_A;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_ARedef3;
    private DbsField pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Filler2;
    private DbsField pnd_Ddctn_From_Net_Pnd_Multiple;
    private DbsField pnd_Ddctn_From_Net_Pnd_Seq_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Verify;
    private DbsField pnd_Ddctn_From_Net_Pnd_Filler3;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key;
    private DbsGroup pnd_Iaa_Cntrct_Aftr_KeyRedef4;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Iaa_Cntrct_Key;
    private DbsGroup pnd_Iaa_Cntrct_KeyRedef5;
    private DbsField pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Iaa_Cpr_Aftr_Key;
    private DbsGroup pnd_Iaa_Cpr_Aftr_KeyRedef6;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key;
    private DbsGroup pnd_Iaa_Cntrct_Prtcpnt_KeyRedef7;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_No_Cntrct_Rec;

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsGroup getIaa_Cpr_Trans_Cntrct_Company_Data() { return iaa_Cpr_Trans_Cntrct_Company_Data; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Company_Cd() { return iaa_Cpr_Trans_Cntrct_Company_Cd; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_Bnfcry_Xref_Ind; }

    public DbsGroup getPnd_Ddctn_From_Net() { return pnd_Ddctn_From_Net; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte() { return pnd_Ddctn_From_Net_Pnd_Check_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Check_DteRedef1() { return pnd_Ddctn_From_Net_Pnd_Check_DteRedef1; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Mm() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Dd() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Yy() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_User_Area() { return pnd_Ddctn_From_Net_Pnd_User_Area; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_User_Id() { return pnd_Ddctn_From_Net_Pnd_User_Id; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Trans_DteRedef2() { return pnd_Ddctn_From_Net_Pnd_Trans_DteRedef2; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte_N() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Nbr() { return pnd_Ddctn_From_Net_Pnd_Trans_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Cntrct_Nbr() { return pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Record_Status() { return pnd_Ddctn_From_Net_Pnd_Record_Status; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr() { return pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Intent_Code() { return pnd_Ddctn_From_Net_Pnd_Intent_Code; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Filler1() { return pnd_Ddctn_From_Net_Pnd_Filler1; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_A() { return pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_A; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_ARedef3() { return pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_ARedef3; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_N() { return pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Filler2() { return pnd_Ddctn_From_Net_Pnd_Filler2; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Multiple() { return pnd_Ddctn_From_Net_Pnd_Multiple; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Seq_Nbr() { return pnd_Ddctn_From_Net_Pnd_Seq_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Verify() { return pnd_Ddctn_From_Net_Pnd_Verify; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Filler3() { return pnd_Ddctn_From_Net_Pnd_Filler3; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key() { return pnd_Iaa_Cntrct_Aftr_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Aftr_KeyRedef4() { return pnd_Iaa_Cntrct_Aftr_KeyRedef4; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Iaa_Cntrct_Key() { return pnd_Iaa_Cntrct_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_KeyRedef5() { return pnd_Iaa_Cntrct_KeyRedef5; }

    public DbsField getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte() { return pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key() { return pnd_Iaa_Cpr_Aftr_Key; }

    public DbsGroup getPnd_Iaa_Cpr_Aftr_KeyRedef6() { return pnd_Iaa_Cpr_Aftr_KeyRedef6; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key() { return pnd_Iaa_Cntrct_Prtcpnt_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Prtcpnt_KeyRedef7() { return pnd_Iaa_Cntrct_Prtcpnt_KeyRedef7; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsGroup getPnd_Logical_Variables() { return pnd_Logical_Variables; }

    public DbsField getPnd_Logical_Variables_Pnd_No_Cntrct_Rec() { return pnd_Logical_Variables_Pnd_No_Cntrct_Rec; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");

        pnd_Ddctn_From_Net = newGroupInRecord("pnd_Ddctn_From_Net", "#DDCTN-FROM-NET");
        pnd_Ddctn_From_Net_Pnd_Check_Dte = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 6);
        pnd_Ddctn_From_Net_Pnd_Check_DteRedef1 = pnd_Ddctn_From_Net.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Check_DteRedef1", "Redefines", pnd_Ddctn_From_Net_Pnd_Check_Dte);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm = pnd_Ddctn_From_Net_Pnd_Check_DteRedef1.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd = pnd_Ddctn_From_Net_Pnd_Check_DteRedef1.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy = pnd_Ddctn_From_Net_Pnd_Check_DteRedef1.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_User_Area = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 6);
        pnd_Ddctn_From_Net_Pnd_User_Id = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Ddctn_From_Net_Pnd_Trans_DteRedef2 = pnd_Ddctn_From_Net.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Trans_DteRedef2", "Redefines", pnd_Ddctn_From_Net_Pnd_Trans_Dte);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte_N = pnd_Ddctn_From_Net_Pnd_Trans_DteRedef2.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte_N", "#TRANS-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Ddctn_From_Net_Pnd_Trans_Nbr = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 3);
        pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Ddctn_From_Net_Pnd_Record_Status = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Record_Status", "#RECORD-STATUS", FieldType.NUMERIC, 
            2);
        pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_Ddctn_From_Net_Pnd_Intent_Code = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_Ddctn_From_Net_Pnd_Filler1 = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Filler1", "#FILLER1", FieldType.STRING, 30);
        pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_A = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_A", "#INVEST-IN-CNTRCT-A", 
            FieldType.STRING, 9);
        pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_ARedef3 = pnd_Ddctn_From_Net.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_ARedef3", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_A);
        pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_N = pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_ARedef3.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Invest_In_Cntrct_N", 
            "#INVEST-IN-CNTRCT-N", FieldType.DECIMAL, 9,2);
        pnd_Ddctn_From_Net_Pnd_Filler2 = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Filler2", "#FILLER2", FieldType.STRING, 20);
        pnd_Ddctn_From_Net_Pnd_Multiple = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_Ddctn_From_Net_Pnd_Seq_Nbr = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Ddctn_From_Net_Pnd_Verify = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Ddctn_From_Net_Pnd_Filler3 = pnd_Ddctn_From_Net.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Filler3", "#FILLER3", FieldType.STRING, 4);

        pnd_Iaa_Cntrct_Aftr_Key = newFieldInRecord("pnd_Iaa_Cntrct_Aftr_Key", "#IAA-CNTRCT-AFTR-KEY", FieldType.STRING, 23);
        pnd_Iaa_Cntrct_Aftr_KeyRedef4 = newGroupInRecord("pnd_Iaa_Cntrct_Aftr_KeyRedef4", "Redefines", pnd_Iaa_Cntrct_Aftr_Key);
        pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Iaa_Cntrct_Aftr_KeyRedef4.newFieldInGroup("pnd_Iaa_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_Aftr_KeyRedef4.newFieldInGroup("pnd_Iaa_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Iaa_Cntrct_Aftr_KeyRedef4.newFieldInGroup("pnd_Iaa_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        pnd_Iaa_Cntrct_Key = newFieldInRecord("pnd_Iaa_Cntrct_Key", "#IAA-CNTRCT-KEY", FieldType.STRING, 17);
        pnd_Iaa_Cntrct_KeyRedef5 = newGroupInRecord("pnd_Iaa_Cntrct_KeyRedef5", "Redefines", pnd_Iaa_Cntrct_Key);
        pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_KeyRedef5.newFieldInGroup("pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte = pnd_Iaa_Cntrct_KeyRedef5.newFieldInGroup("pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.PACKED_DECIMAL, 
            7);

        pnd_Iaa_Cpr_Aftr_Key = newFieldInRecord("pnd_Iaa_Cpr_Aftr_Key", "#IAA-CPR-AFTR-KEY", FieldType.STRING, 25);
        pnd_Iaa_Cpr_Aftr_KeyRedef6 = newGroupInRecord("pnd_Iaa_Cpr_Aftr_KeyRedef6", "Redefines", pnd_Iaa_Cpr_Aftr_Key);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Iaa_Cpr_Aftr_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Aftr_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Aftr_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Iaa_Cpr_Aftr_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        pnd_Iaa_Cntrct_Prtcpnt_Key = newFieldInRecord("pnd_Iaa_Cntrct_Prtcpnt_Key", "#IAA-CNTRCT-PRTCPNT-KEY", FieldType.STRING, 12);
        pnd_Iaa_Cntrct_Prtcpnt_KeyRedef7 = newGroupInRecord("pnd_Iaa_Cntrct_Prtcpnt_KeyRedef7", "Redefines", pnd_Iaa_Cntrct_Prtcpnt_Key);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef7.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef7.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Logical_Variables = newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_No_Cntrct_Rec = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_No_Cntrct_Rec", "#NO-CNTRCT-REC", FieldType.BOOLEAN);
        vw_iaa_Cpr_Trans.setUniquePeList();

        this.setRecordName("LdaIaal915c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();
        pnd_Ddctn_From_Net_Pnd_Check_Dte.setInitialValue(0);
        pnd_Ddctn_From_Net_Pnd_Record_Status.setInitialValue(0);
    }

    // Constructor
    public LdaIaal915c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
