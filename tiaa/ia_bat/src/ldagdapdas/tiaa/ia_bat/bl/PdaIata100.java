/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:48 PM
**        * FROM NATURAL PDA     : IATA100
************************************************************
**        * FILE NAME            : PdaIata100.java
**        * CLASS NAME           : PdaIata100
**        * INSTANCE NAME        : PdaIata100
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIata100 extends PdaBase
{
    // Properties
    private DbsGroup iata100;
    private DbsField iata100_Rcrd_Type_Cde;
    private DbsField iata100_Rqst_Id;
    private DbsField iata100_Rqst_Effctv_Dte;
    private DbsField iata100_Rqst_Entry_Dte;
    private DbsField iata100_Rqst_Entry_Tme;
    private DbsField iata100_Rqst_Lst_Actvty_Dte;
    private DbsField iata100_Rqst_Rcvd_Dte;
    private DbsField iata100_Rqst_Rcvd_Tme;
    private DbsField iata100_Rqst_Rcvd_User_Id;
    private DbsField iata100_Rqst_Entry_User_Id;
    private DbsField iata100_Rqst_Cntct_Mde;
    private DbsField iata100_Rqst_Srce;
    private DbsField iata100_Rqst_Rep_Nme;
    private DbsField iata100_Rqst_Sttmnt_Ind;
    private DbsField iata100_Rqst_Opn_Clsd_Ind;
    private DbsField iata100_Rqst_Rcprcl_Dte;
    private DbsField iata100_Rqst_Ssn;
    private DbsField iata100_Xfr_Work_Prcss_Id;
    private DbsField iata100_Xfr_Mit_Log_Dte_Tme;
    private DbsField iata100_Xfr_Stts_Cde;
    private DbsField iata100_Xfr_Rjctn_Cde;
    private DbsField iata100_Xfr_In_Progress_Ind;
    private DbsField iata100_Xfr_Retry_Cnt;
    private DbsField iata100_Ia_Frm_Cntrct;
    private DbsField iata100_Ia_Frm_Payee;
    private DbsField iata100_Ia_Unique_Id;
    private DbsField iata100_Ia_Hold_Cde;
    private DbsField iata100_Cpnd_Xfr_Frm_Acct_Dta;
    private DbsGroup iata100_Xfr_Frm_Acct_Dta;
    private DbsField iata100_Xfr_Frm_Acct_Cde;
    private DbsField iata100_Xfr_Frm_Unit_Typ;
    private DbsField iata100_Xfr_Frm_Typ;
    private DbsField iata100_Xfr_Frm_Qty;
    private DbsField iata100_Xfr_Frm_Est_Amt;
    private DbsField iata100_Xfr_Frm_Asset_Amt;
    private DbsField iata100_Cpnd_Xfr_To_Acct_Dta;
    private DbsGroup iata100_Xfr_To_Acct_Dta;
    private DbsField iata100_Xfr_To_Acct_Cde;
    private DbsField iata100_Xfr_To_Unit_Typ;
    private DbsField iata100_Xfr_To_Typ;
    private DbsField iata100_Xfr_To_Qty;
    private DbsField iata100_Xfr_To_Est_Amt;
    private DbsField iata100_Xfr_To_Asset_Amt;
    private DbsField iata100_Ia_To_Cntrct;
    private DbsField iata100_Ia_To_Payee;
    private DbsField iata100_Ia_Appl_Rcvd_Dte;
    private DbsField iata100_Ia_New_Issue;
    private DbsField iata100_Ia_Rsn_For_Ovrrde;
    private DbsField iata100_Ia_Ovrrde_User_Id;
    private DbsField iata100_Ia_Gnrl_Pend_Cde;
    private DbsField iata100_Rqst_Xfr_Type;
    private DbsField iata100_Rqst_Unit_Cde;
    private DbsField iata100_Ia_New_Iss_Prt_Pull;

    public DbsGroup getIata100() { return iata100; }

    public DbsField getIata100_Rcrd_Type_Cde() { return iata100_Rcrd_Type_Cde; }

    public DbsField getIata100_Rqst_Id() { return iata100_Rqst_Id; }

    public DbsField getIata100_Rqst_Effctv_Dte() { return iata100_Rqst_Effctv_Dte; }

    public DbsField getIata100_Rqst_Entry_Dte() { return iata100_Rqst_Entry_Dte; }

    public DbsField getIata100_Rqst_Entry_Tme() { return iata100_Rqst_Entry_Tme; }

    public DbsField getIata100_Rqst_Lst_Actvty_Dte() { return iata100_Rqst_Lst_Actvty_Dte; }

    public DbsField getIata100_Rqst_Rcvd_Dte() { return iata100_Rqst_Rcvd_Dte; }

    public DbsField getIata100_Rqst_Rcvd_Tme() { return iata100_Rqst_Rcvd_Tme; }

    public DbsField getIata100_Rqst_Rcvd_User_Id() { return iata100_Rqst_Rcvd_User_Id; }

    public DbsField getIata100_Rqst_Entry_User_Id() { return iata100_Rqst_Entry_User_Id; }

    public DbsField getIata100_Rqst_Cntct_Mde() { return iata100_Rqst_Cntct_Mde; }

    public DbsField getIata100_Rqst_Srce() { return iata100_Rqst_Srce; }

    public DbsField getIata100_Rqst_Rep_Nme() { return iata100_Rqst_Rep_Nme; }

    public DbsField getIata100_Rqst_Sttmnt_Ind() { return iata100_Rqst_Sttmnt_Ind; }

    public DbsField getIata100_Rqst_Opn_Clsd_Ind() { return iata100_Rqst_Opn_Clsd_Ind; }

    public DbsField getIata100_Rqst_Rcprcl_Dte() { return iata100_Rqst_Rcprcl_Dte; }

    public DbsField getIata100_Rqst_Ssn() { return iata100_Rqst_Ssn; }

    public DbsField getIata100_Xfr_Work_Prcss_Id() { return iata100_Xfr_Work_Prcss_Id; }

    public DbsField getIata100_Xfr_Mit_Log_Dte_Tme() { return iata100_Xfr_Mit_Log_Dte_Tme; }

    public DbsField getIata100_Xfr_Stts_Cde() { return iata100_Xfr_Stts_Cde; }

    public DbsField getIata100_Xfr_Rjctn_Cde() { return iata100_Xfr_Rjctn_Cde; }

    public DbsField getIata100_Xfr_In_Progress_Ind() { return iata100_Xfr_In_Progress_Ind; }

    public DbsField getIata100_Xfr_Retry_Cnt() { return iata100_Xfr_Retry_Cnt; }

    public DbsField getIata100_Ia_Frm_Cntrct() { return iata100_Ia_Frm_Cntrct; }

    public DbsField getIata100_Ia_Frm_Payee() { return iata100_Ia_Frm_Payee; }

    public DbsField getIata100_Ia_Unique_Id() { return iata100_Ia_Unique_Id; }

    public DbsField getIata100_Ia_Hold_Cde() { return iata100_Ia_Hold_Cde; }

    public DbsField getIata100_Cpnd_Xfr_Frm_Acct_Dta() { return iata100_Cpnd_Xfr_Frm_Acct_Dta; }

    public DbsGroup getIata100_Xfr_Frm_Acct_Dta() { return iata100_Xfr_Frm_Acct_Dta; }

    public DbsField getIata100_Xfr_Frm_Acct_Cde() { return iata100_Xfr_Frm_Acct_Cde; }

    public DbsField getIata100_Xfr_Frm_Unit_Typ() { return iata100_Xfr_Frm_Unit_Typ; }

    public DbsField getIata100_Xfr_Frm_Typ() { return iata100_Xfr_Frm_Typ; }

    public DbsField getIata100_Xfr_Frm_Qty() { return iata100_Xfr_Frm_Qty; }

    public DbsField getIata100_Xfr_Frm_Est_Amt() { return iata100_Xfr_Frm_Est_Amt; }

    public DbsField getIata100_Xfr_Frm_Asset_Amt() { return iata100_Xfr_Frm_Asset_Amt; }

    public DbsField getIata100_Cpnd_Xfr_To_Acct_Dta() { return iata100_Cpnd_Xfr_To_Acct_Dta; }

    public DbsGroup getIata100_Xfr_To_Acct_Dta() { return iata100_Xfr_To_Acct_Dta; }

    public DbsField getIata100_Xfr_To_Acct_Cde() { return iata100_Xfr_To_Acct_Cde; }

    public DbsField getIata100_Xfr_To_Unit_Typ() { return iata100_Xfr_To_Unit_Typ; }

    public DbsField getIata100_Xfr_To_Typ() { return iata100_Xfr_To_Typ; }

    public DbsField getIata100_Xfr_To_Qty() { return iata100_Xfr_To_Qty; }

    public DbsField getIata100_Xfr_To_Est_Amt() { return iata100_Xfr_To_Est_Amt; }

    public DbsField getIata100_Xfr_To_Asset_Amt() { return iata100_Xfr_To_Asset_Amt; }

    public DbsField getIata100_Ia_To_Cntrct() { return iata100_Ia_To_Cntrct; }

    public DbsField getIata100_Ia_To_Payee() { return iata100_Ia_To_Payee; }

    public DbsField getIata100_Ia_Appl_Rcvd_Dte() { return iata100_Ia_Appl_Rcvd_Dte; }

    public DbsField getIata100_Ia_New_Issue() { return iata100_Ia_New_Issue; }

    public DbsField getIata100_Ia_Rsn_For_Ovrrde() { return iata100_Ia_Rsn_For_Ovrrde; }

    public DbsField getIata100_Ia_Ovrrde_User_Id() { return iata100_Ia_Ovrrde_User_Id; }

    public DbsField getIata100_Ia_Gnrl_Pend_Cde() { return iata100_Ia_Gnrl_Pend_Cde; }

    public DbsField getIata100_Rqst_Xfr_Type() { return iata100_Rqst_Xfr_Type; }

    public DbsField getIata100_Rqst_Unit_Cde() { return iata100_Rqst_Unit_Cde; }

    public DbsField getIata100_Ia_New_Iss_Prt_Pull() { return iata100_Ia_New_Iss_Prt_Pull; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iata100 = dbsRecord.newGroupInRecord("iata100", "IATA100");
        iata100.setParameterOption(ParameterOption.ByReference);
        iata100_Rcrd_Type_Cde = iata100.newFieldInGroup("iata100_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1);
        iata100_Rqst_Id = iata100.newFieldInGroup("iata100_Rqst_Id", "RQST-ID", FieldType.STRING, 34);
        iata100_Rqst_Effctv_Dte = iata100.newFieldInGroup("iata100_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE);
        iata100_Rqst_Entry_Dte = iata100.newFieldInGroup("iata100_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE);
        iata100_Rqst_Entry_Tme = iata100.newFieldInGroup("iata100_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 7);
        iata100_Rqst_Lst_Actvty_Dte = iata100.newFieldInGroup("iata100_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", FieldType.DATE);
        iata100_Rqst_Rcvd_Dte = iata100.newFieldInGroup("iata100_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE);
        iata100_Rqst_Rcvd_Tme = iata100.newFieldInGroup("iata100_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 7);
        iata100_Rqst_Rcvd_User_Id = iata100.newFieldInGroup("iata100_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", FieldType.STRING, 8);
        iata100_Rqst_Entry_User_Id = iata100.newFieldInGroup("iata100_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 8);
        iata100_Rqst_Cntct_Mde = iata100.newFieldInGroup("iata100_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 1);
        iata100_Rqst_Srce = iata100.newFieldInGroup("iata100_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 1);
        iata100_Rqst_Rep_Nme = iata100.newFieldInGroup("iata100_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 40);
        iata100_Rqst_Sttmnt_Ind = iata100.newFieldInGroup("iata100_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", FieldType.STRING, 1);
        iata100_Rqst_Opn_Clsd_Ind = iata100.newFieldInGroup("iata100_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", FieldType.STRING, 1);
        iata100_Rqst_Rcprcl_Dte = iata100.newFieldInGroup("iata100_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", FieldType.NUMERIC, 8);
        iata100_Rqst_Ssn = iata100.newFieldInGroup("iata100_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 9);
        iata100_Xfr_Work_Prcss_Id = iata100.newFieldInGroup("iata100_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 6);
        iata100_Xfr_Mit_Log_Dte_Tme = iata100.newFieldInGroup("iata100_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        iata100_Xfr_Stts_Cde = iata100.newFieldInGroup("iata100_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2);
        iata100_Xfr_Rjctn_Cde = iata100.newFieldInGroup("iata100_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2);
        iata100_Xfr_In_Progress_Ind = iata100.newFieldInGroup("iata100_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", FieldType.BOOLEAN);
        iata100_Xfr_Retry_Cnt = iata100.newFieldInGroup("iata100_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 1);
        iata100_Ia_Frm_Cntrct = iata100.newFieldInGroup("iata100_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10);
        iata100_Ia_Frm_Payee = iata100.newFieldInGroup("iata100_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2);
        iata100_Ia_Unique_Id = iata100.newFieldInGroup("iata100_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12);
        iata100_Ia_Hold_Cde = iata100.newFieldInGroup("iata100_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 8);
        iata100_Cpnd_Xfr_Frm_Acct_Dta = iata100.newFieldInGroup("iata100_Cpnd_Xfr_Frm_Acct_Dta", "C#XFR-FRM-ACCT-DTA", FieldType.NUMERIC, 3);
        iata100_Xfr_Frm_Acct_Dta = iata100.newGroupArrayInGroup("iata100_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", new DbsArrayController(1,20));
        iata100_Xfr_Frm_Acct_Cde = iata100_Xfr_Frm_Acct_Dta.newFieldInGroup("iata100_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 1);
        iata100_Xfr_Frm_Unit_Typ = iata100_Xfr_Frm_Acct_Dta.newFieldInGroup("iata100_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 1);
        iata100_Xfr_Frm_Typ = iata100_Xfr_Frm_Acct_Dta.newFieldInGroup("iata100_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1);
        iata100_Xfr_Frm_Qty = iata100_Xfr_Frm_Acct_Dta.newFieldInGroup("iata100_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 9,2);
        iata100_Xfr_Frm_Est_Amt = iata100_Xfr_Frm_Acct_Dta.newFieldInGroup("iata100_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 9,2);
        iata100_Xfr_Frm_Asset_Amt = iata100_Xfr_Frm_Acct_Dta.newFieldInGroup("iata100_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        iata100_Cpnd_Xfr_To_Acct_Dta = iata100.newFieldInGroup("iata100_Cpnd_Xfr_To_Acct_Dta", "C#XFR-TO-ACCT-DTA", FieldType.NUMERIC, 3);
        iata100_Xfr_To_Acct_Dta = iata100.newGroupArrayInGroup("iata100_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", new DbsArrayController(1,40));
        iata100_Xfr_To_Acct_Cde = iata100_Xfr_To_Acct_Dta.newFieldInGroup("iata100_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 1);
        iata100_Xfr_To_Unit_Typ = iata100_Xfr_To_Acct_Dta.newFieldInGroup("iata100_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 1);
        iata100_Xfr_To_Typ = iata100_Xfr_To_Acct_Dta.newFieldInGroup("iata100_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1);
        iata100_Xfr_To_Qty = iata100_Xfr_To_Acct_Dta.newFieldInGroup("iata100_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9,2);
        iata100_Xfr_To_Est_Amt = iata100_Xfr_To_Acct_Dta.newFieldInGroup("iata100_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 9,2);
        iata100_Xfr_To_Asset_Amt = iata100_Xfr_To_Acct_Dta.newFieldInGroup("iata100_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        iata100_Ia_To_Cntrct = iata100.newFieldInGroup("iata100_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10);
        iata100_Ia_To_Payee = iata100.newFieldInGroup("iata100_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2);
        iata100_Ia_Appl_Rcvd_Dte = iata100.newFieldInGroup("iata100_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", FieldType.DATE);
        iata100_Ia_New_Issue = iata100.newFieldInGroup("iata100_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 1);
        iata100_Ia_Rsn_For_Ovrrde = iata100.newFieldInGroup("iata100_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", FieldType.STRING, 2);
        iata100_Ia_Ovrrde_User_Id = iata100.newFieldInGroup("iata100_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", FieldType.STRING, 8);
        iata100_Ia_Gnrl_Pend_Cde = iata100.newFieldInGroup("iata100_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", FieldType.STRING, 1);
        iata100_Rqst_Xfr_Type = iata100.newFieldInGroup("iata100_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1);
        iata100_Rqst_Unit_Cde = iata100.newFieldInGroup("iata100_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8);
        iata100_Ia_New_Iss_Prt_Pull = iata100.newFieldInGroup("iata100_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIata100(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

