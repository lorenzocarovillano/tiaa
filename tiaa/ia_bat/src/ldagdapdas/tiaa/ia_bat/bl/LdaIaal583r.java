/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:04 PM
**        * FROM NATURAL LDA     : IAAL583R
************************************************************
**        * FILE NAME            : LdaIaal583r.java
**        * CLASS NAME           : LdaIaal583r
**        * INSTANCE NAME        : LdaIaal583r
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal583r extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField pnd_Check_Date_Ccyymmdd;
    private DbsField pnd_Page_Ctr;
    private DbsField pnd_Lines_To_Print;
    private DbsField pnd_Lines_Left;
    private DbsField pnd_Other_Lines;
    private DbsField pnd_Corr_Count;
    private DbsField pnd_Work_Records_Read;
    private DbsField pnd_Date_Ccyymmdd;
    private DbsGroup pnd_Date_CcyymmddRedef1;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Cc;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Dd;
    private DbsField pnd_Date_Ccyymm_1;
    private DbsGroup pnd_Date_Ccyymm_1Redef2;
    private DbsField pnd_Date_Ccyymm_1_Pnd_Date_Ccyy_1;
    private DbsField pnd_Date_Ccyymm_1_Pnd_Date_Mm_1;
    private DbsField pnd_Line_1;
    private DbsGroup pnd_Line_1Redef3;
    private DbsField pnd_Line_1_Pnd_Ln1_Name;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler1;
    private DbsField pnd_Line_1_Pnd_Ln1_Pin;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler2;
    private DbsField pnd_Line_1_Pnd_Ln1_Ssn;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler3;
    private DbsField pnd_Line_1_Pnd_Ln1_Cont_Stat;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler4;
    private DbsField pnd_Line_1_Pnd_Ln1_Annu_Type;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler5;
    private DbsField pnd_Line_1_Pnd_Ln1_Cont_Mode;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler6;
    private DbsField pnd_Line_1_Pnd_Ln1_Doi;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler7;
    private DbsField pnd_Line_1_Pnd_Ln1_Citz;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler8;
    private DbsField pnd_Line_1_Pnd_Ln1_Resd;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler9;
    private DbsField pnd_Line_1_Pnd_Ln1_Dod;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler10;
    private DbsField pnd_Line_1_Pnd_Ln1_Nfp;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler11;
    private DbsField pnd_City_State;
    private DbsField pnd_Total_Zip;
    private DbsField pnd_Zip_Breakdown;
    private DbsGroup pnd_Zip_BreakdownRedef4;
    private DbsField pnd_Zip_Breakdown_Pnd_Zip_1_5;
    private DbsField pnd_Zip_Breakdown_Pnd_Zip_6_9;
    private DbsGroup pnd_Table_Other_Lines;
    private DbsGroup pnd_Table_Other_Lines_Pnd_Table_Group;
    private DbsField pnd_Table_Other_Lines_Pnd_Tab_Filler1;
    private DbsField pnd_Table_Other_Lines_Pnd_Tab_Notify_Address;
    private DbsField pnd_I;
    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Pnd_W_Name;
    private DbsGroup pnd_Work_Record_Pnd_W_NameRedef5;
    private DbsField pnd_Work_Record_Pnd_W_Last_Name;
    private DbsField pnd_Work_Record_Pnd_W_First_Name;
    private DbsField pnd_Work_Record_Pnd_W_Middle_Name;
    private DbsField pnd_Work_Record_Pnd_W_Cont_Stat;
    private DbsField pnd_Work_Record_Pnd_W_Pin;
    private DbsField pnd_Work_Record_Pnd_W_Ssn;
    private DbsGroup pnd_Work_Record_Pnd_W_SsnRedef6;
    private DbsField pnd_Work_Record_Pnd_Ssn_1_3;
    private DbsField pnd_Work_Record_Pnd_Ssn_4_5;
    private DbsField pnd_Work_Record_Pnd_Ssn_6_9;
    private DbsField pnd_Work_Record_Pnd_W_Annu_Type;
    private DbsField pnd_Work_Record_Pnd_W_Contract_Mode;
    private DbsField pnd_Work_Record_Pnd_W_Dod_Dte;
    private DbsField pnd_Work_Record_Pnd_W_Nfp;
    private DbsField pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte;
    private DbsField pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_W_Parm_Date;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef7;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd() { return vw_iaa_Cntrl_Rcrd; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Check_Dte; }

    public DbsField getPnd_Check_Date_Ccyymmdd() { return pnd_Check_Date_Ccyymmdd; }

    public DbsField getPnd_Page_Ctr() { return pnd_Page_Ctr; }

    public DbsField getPnd_Lines_To_Print() { return pnd_Lines_To_Print; }

    public DbsField getPnd_Lines_Left() { return pnd_Lines_Left; }

    public DbsField getPnd_Other_Lines() { return pnd_Other_Lines; }

    public DbsField getPnd_Corr_Count() { return pnd_Corr_Count; }

    public DbsField getPnd_Work_Records_Read() { return pnd_Work_Records_Read; }

    public DbsField getPnd_Date_Ccyymmdd() { return pnd_Date_Ccyymmdd; }

    public DbsGroup getPnd_Date_CcyymmddRedef1() { return pnd_Date_CcyymmddRedef1; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Cc() { return pnd_Date_Ccyymmdd_Pnd_Date_Cc; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Yy() { return pnd_Date_Ccyymmdd_Pnd_Date_Yy; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Mm() { return pnd_Date_Ccyymmdd_Pnd_Date_Mm; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Dd() { return pnd_Date_Ccyymmdd_Pnd_Date_Dd; }

    public DbsField getPnd_Date_Ccyymm_1() { return pnd_Date_Ccyymm_1; }

    public DbsGroup getPnd_Date_Ccyymm_1Redef2() { return pnd_Date_Ccyymm_1Redef2; }

    public DbsField getPnd_Date_Ccyymm_1_Pnd_Date_Ccyy_1() { return pnd_Date_Ccyymm_1_Pnd_Date_Ccyy_1; }

    public DbsField getPnd_Date_Ccyymm_1_Pnd_Date_Mm_1() { return pnd_Date_Ccyymm_1_Pnd_Date_Mm_1; }

    public DbsField getPnd_Line_1() { return pnd_Line_1; }

    public DbsGroup getPnd_Line_1Redef3() { return pnd_Line_1Redef3; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Name() { return pnd_Line_1_Pnd_Ln1_Name; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler1() { return pnd_Line_1_Pnd_Ln1_Filler1; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Pin() { return pnd_Line_1_Pnd_Ln1_Pin; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler2() { return pnd_Line_1_Pnd_Ln1_Filler2; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Ssn() { return pnd_Line_1_Pnd_Ln1_Ssn; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler3() { return pnd_Line_1_Pnd_Ln1_Filler3; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Cont_Stat() { return pnd_Line_1_Pnd_Ln1_Cont_Stat; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler4() { return pnd_Line_1_Pnd_Ln1_Filler4; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Annu_Type() { return pnd_Line_1_Pnd_Ln1_Annu_Type; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler5() { return pnd_Line_1_Pnd_Ln1_Filler5; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Cont_Mode() { return pnd_Line_1_Pnd_Ln1_Cont_Mode; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler6() { return pnd_Line_1_Pnd_Ln1_Filler6; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Doi() { return pnd_Line_1_Pnd_Ln1_Doi; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler7() { return pnd_Line_1_Pnd_Ln1_Filler7; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Citz() { return pnd_Line_1_Pnd_Ln1_Citz; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler8() { return pnd_Line_1_Pnd_Ln1_Filler8; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Resd() { return pnd_Line_1_Pnd_Ln1_Resd; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler9() { return pnd_Line_1_Pnd_Ln1_Filler9; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Dod() { return pnd_Line_1_Pnd_Ln1_Dod; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler10() { return pnd_Line_1_Pnd_Ln1_Filler10; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Nfp() { return pnd_Line_1_Pnd_Ln1_Nfp; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler11() { return pnd_Line_1_Pnd_Ln1_Filler11; }

    public DbsField getPnd_City_State() { return pnd_City_State; }

    public DbsField getPnd_Total_Zip() { return pnd_Total_Zip; }

    public DbsField getPnd_Zip_Breakdown() { return pnd_Zip_Breakdown; }

    public DbsGroup getPnd_Zip_BreakdownRedef4() { return pnd_Zip_BreakdownRedef4; }

    public DbsField getPnd_Zip_Breakdown_Pnd_Zip_1_5() { return pnd_Zip_Breakdown_Pnd_Zip_1_5; }

    public DbsField getPnd_Zip_Breakdown_Pnd_Zip_6_9() { return pnd_Zip_Breakdown_Pnd_Zip_6_9; }

    public DbsGroup getPnd_Table_Other_Lines() { return pnd_Table_Other_Lines; }

    public DbsGroup getPnd_Table_Other_Lines_Pnd_Table_Group() { return pnd_Table_Other_Lines_Pnd_Table_Group; }

    public DbsField getPnd_Table_Other_Lines_Pnd_Tab_Filler1() { return pnd_Table_Other_Lines_Pnd_Tab_Filler1; }

    public DbsField getPnd_Table_Other_Lines_Pnd_Tab_Notify_Address() { return pnd_Table_Other_Lines_Pnd_Tab_Notify_Address; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsGroup getPnd_Work_Record() { return pnd_Work_Record; }

    public DbsField getPnd_Work_Record_Pnd_W_Name() { return pnd_Work_Record_Pnd_W_Name; }

    public DbsGroup getPnd_Work_Record_Pnd_W_NameRedef5() { return pnd_Work_Record_Pnd_W_NameRedef5; }

    public DbsField getPnd_Work_Record_Pnd_W_Last_Name() { return pnd_Work_Record_Pnd_W_Last_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_First_Name() { return pnd_Work_Record_Pnd_W_First_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_Middle_Name() { return pnd_Work_Record_Pnd_W_Middle_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_Cont_Stat() { return pnd_Work_Record_Pnd_W_Cont_Stat; }

    public DbsField getPnd_Work_Record_Pnd_W_Pin() { return pnd_Work_Record_Pnd_W_Pin; }

    public DbsField getPnd_Work_Record_Pnd_W_Ssn() { return pnd_Work_Record_Pnd_W_Ssn; }

    public DbsGroup getPnd_Work_Record_Pnd_W_SsnRedef6() { return pnd_Work_Record_Pnd_W_SsnRedef6; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_1_3() { return pnd_Work_Record_Pnd_Ssn_1_3; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_4_5() { return pnd_Work_Record_Pnd_Ssn_4_5; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_6_9() { return pnd_Work_Record_Pnd_Ssn_6_9; }

    public DbsField getPnd_Work_Record_Pnd_W_Annu_Type() { return pnd_Work_Record_Pnd_W_Annu_Type; }

    public DbsField getPnd_Work_Record_Pnd_W_Contract_Mode() { return pnd_Work_Record_Pnd_W_Contract_Mode; }

    public DbsField getPnd_Work_Record_Pnd_W_Dod_Dte() { return pnd_Work_Record_Pnd_W_Dod_Dte; }

    public DbsField getPnd_Work_Record_Pnd_W_Nfp() { return pnd_Work_Record_Pnd_W_Nfp; }

    public DbsField getPnd_Work_Record_Pnd_W_Cntrct_Issue_Dte() { return pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte; }

    public DbsField getPnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde() { return pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde; }

    public DbsField getPnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde() { return pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde; }

    public DbsField getPnd_W_Parm_Date() { return pnd_W_Parm_Date; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef7() { return iaa_Parm_Card_Pnd_Parm_DateRedef7; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");

        pnd_Check_Date_Ccyymmdd = newFieldInRecord("pnd_Check_Date_Ccyymmdd", "#CHECK-DATE-CCYYMMDD", FieldType.STRING, 8);

        pnd_Page_Ctr = newFieldInRecord("pnd_Page_Ctr", "#PAGE-CTR", FieldType.NUMERIC, 4);

        pnd_Lines_To_Print = newFieldInRecord("pnd_Lines_To_Print", "#LINES-TO-PRINT", FieldType.NUMERIC, 2);

        pnd_Lines_Left = newFieldInRecord("pnd_Lines_Left", "#LINES-LEFT", FieldType.NUMERIC, 2);

        pnd_Other_Lines = newFieldInRecord("pnd_Other_Lines", "#OTHER-LINES", FieldType.NUMERIC, 2);

        pnd_Corr_Count = newFieldInRecord("pnd_Corr_Count", "#CORR-COUNT", FieldType.NUMERIC, 1);

        pnd_Work_Records_Read = newFieldInRecord("pnd_Work_Records_Read", "#WORK-RECORDS-READ", FieldType.NUMERIC, 4);

        pnd_Date_Ccyymmdd = newFieldInRecord("pnd_Date_Ccyymmdd", "#DATE-CCYYMMDD", FieldType.STRING, 8);
        pnd_Date_CcyymmddRedef1 = newGroupInRecord("pnd_Date_CcyymmddRedef1", "Redefines", pnd_Date_Ccyymmdd);
        pnd_Date_Ccyymmdd_Pnd_Date_Cc = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Cc", "#DATE-CC", FieldType.STRING, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Yy = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.STRING, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Mm = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.STRING, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Dd = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.STRING, 2);

        pnd_Date_Ccyymm_1 = newFieldInRecord("pnd_Date_Ccyymm_1", "#DATE-CCYYMM-1", FieldType.STRING, 6);
        pnd_Date_Ccyymm_1Redef2 = newGroupInRecord("pnd_Date_Ccyymm_1Redef2", "Redefines", pnd_Date_Ccyymm_1);
        pnd_Date_Ccyymm_1_Pnd_Date_Ccyy_1 = pnd_Date_Ccyymm_1Redef2.newFieldInGroup("pnd_Date_Ccyymm_1_Pnd_Date_Ccyy_1", "#DATE-CCYY-1", FieldType.STRING, 
            4);
        pnd_Date_Ccyymm_1_Pnd_Date_Mm_1 = pnd_Date_Ccyymm_1Redef2.newFieldInGroup("pnd_Date_Ccyymm_1_Pnd_Date_Mm_1", "#DATE-MM-1", FieldType.STRING, 2);

        pnd_Line_1 = newFieldInRecord("pnd_Line_1", "#LINE-1", FieldType.STRING, 132);
        pnd_Line_1Redef3 = newGroupInRecord("pnd_Line_1Redef3", "Redefines", pnd_Line_1);
        pnd_Line_1_Pnd_Ln1_Name = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Name", "#LN1-NAME", FieldType.STRING, 40);
        pnd_Line_1_Pnd_Ln1_Filler1 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler1", "#LN1-FILLER1", FieldType.STRING, 1);
        pnd_Line_1_Pnd_Ln1_Pin = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Pin", "#LN1-PIN", FieldType.NUMERIC, 12);
        pnd_Line_1_Pnd_Ln1_Filler2 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler2", "#LN1-FILLER2", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Ssn = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Ssn", "#LN1-SSN", FieldType.STRING, 11);
        pnd_Line_1_Pnd_Ln1_Filler3 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler3", "#LN1-FILLER3", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Cont_Stat = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Cont_Stat", "#LN1-CONT-STAT", FieldType.STRING, 11);
        pnd_Line_1_Pnd_Ln1_Filler4 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler4", "#LN1-FILLER4", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Annu_Type = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Annu_Type", "#LN1-ANNU-TYPE", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Filler5 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler5", "#LN1-FILLER5", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Cont_Mode = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Cont_Mode", "#LN1-CONT-MODE", FieldType.STRING, 4);
        pnd_Line_1_Pnd_Ln1_Filler6 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler6", "#LN1-FILLER6", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Doi = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Doi", "#LN1-DOI", FieldType.STRING, 7);
        pnd_Line_1_Pnd_Ln1_Filler7 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler7", "#LN1-FILLER7", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Citz = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Citz", "#LN1-CITZ", FieldType.STRING, 3);
        pnd_Line_1_Pnd_Ln1_Filler8 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler8", "#LN1-FILLER8", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Resd = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Resd", "#LN1-RESD", FieldType.STRING, 3);
        pnd_Line_1_Pnd_Ln1_Filler9 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler9", "#LN1-FILLER9", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Dod = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Dod", "#LN1-DOD", FieldType.STRING, 8);
        pnd_Line_1_Pnd_Ln1_Filler10 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler10", "#LN1-FILLER10", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Nfp = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Nfp", "#LN1-NFP", FieldType.STRING, 1);
        pnd_Line_1_Pnd_Ln1_Filler11 = pnd_Line_1Redef3.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler11", "#LN1-FILLER11", FieldType.STRING, 11);

        pnd_City_State = newFieldInRecord("pnd_City_State", "#CITY-STATE", FieldType.STRING, 24);

        pnd_Total_Zip = newFieldInRecord("pnd_Total_Zip", "#TOTAL-ZIP", FieldType.STRING, 10);

        pnd_Zip_Breakdown = newFieldInRecord("pnd_Zip_Breakdown", "#ZIP-BREAKDOWN", FieldType.STRING, 9);
        pnd_Zip_BreakdownRedef4 = newGroupInRecord("pnd_Zip_BreakdownRedef4", "Redefines", pnd_Zip_Breakdown);
        pnd_Zip_Breakdown_Pnd_Zip_1_5 = pnd_Zip_BreakdownRedef4.newFieldInGroup("pnd_Zip_Breakdown_Pnd_Zip_1_5", "#ZIP-1-5", FieldType.STRING, 5);
        pnd_Zip_Breakdown_Pnd_Zip_6_9 = pnd_Zip_BreakdownRedef4.newFieldInGroup("pnd_Zip_Breakdown_Pnd_Zip_6_9", "#ZIP-6-9", FieldType.STRING, 4);

        pnd_Table_Other_Lines = newGroupInRecord("pnd_Table_Other_Lines", "#TABLE-OTHER-LINES");
        pnd_Table_Other_Lines_Pnd_Table_Group = pnd_Table_Other_Lines.newGroupArrayInGroup("pnd_Table_Other_Lines_Pnd_Table_Group", "#TABLE-GROUP", new 
            DbsArrayController(1,5));
        pnd_Table_Other_Lines_Pnd_Tab_Filler1 = pnd_Table_Other_Lines_Pnd_Table_Group.newFieldInGroup("pnd_Table_Other_Lines_Pnd_Tab_Filler1", "#TAB-FILLER1", 
            FieldType.STRING, 96);
        pnd_Table_Other_Lines_Pnd_Tab_Notify_Address = pnd_Table_Other_Lines_Pnd_Table_Group.newFieldInGroup("pnd_Table_Other_Lines_Pnd_Tab_Notify_Address", 
            "#TAB-NOTIFY-ADDRESS", FieldType.STRING, 35);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        pnd_Work_Record = newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Pnd_W_Name = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Name", "#W-NAME", FieldType.STRING, 38);
        pnd_Work_Record_Pnd_W_NameRedef5 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_W_NameRedef5", "Redefines", pnd_Work_Record_Pnd_W_Name);
        pnd_Work_Record_Pnd_W_Last_Name = pnd_Work_Record_Pnd_W_NameRedef5.newFieldInGroup("pnd_Work_Record_Pnd_W_Last_Name", "#W-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Work_Record_Pnd_W_First_Name = pnd_Work_Record_Pnd_W_NameRedef5.newFieldInGroup("pnd_Work_Record_Pnd_W_First_Name", "#W-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Work_Record_Pnd_W_Middle_Name = pnd_Work_Record_Pnd_W_NameRedef5.newFieldInGroup("pnd_Work_Record_Pnd_W_Middle_Name", "#W-MIDDLE-NAME", FieldType.STRING, 
            12);
        pnd_Work_Record_Pnd_W_Cont_Stat = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cont_Stat", "#W-CONT-STAT", FieldType.STRING, 11);
        pnd_Work_Record_Pnd_W_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Pnd_W_Ssn = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Ssn", "#W-SSN", FieldType.STRING, 9);
        pnd_Work_Record_Pnd_W_SsnRedef6 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_W_SsnRedef6", "Redefines", pnd_Work_Record_Pnd_W_Ssn);
        pnd_Work_Record_Pnd_Ssn_1_3 = pnd_Work_Record_Pnd_W_SsnRedef6.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_1_3", "#SSN-1-3", FieldType.STRING, 3);
        pnd_Work_Record_Pnd_Ssn_4_5 = pnd_Work_Record_Pnd_W_SsnRedef6.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_4_5", "#SSN-4-5", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Ssn_6_9 = pnd_Work_Record_Pnd_W_SsnRedef6.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_6_9", "#SSN-6-9", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_W_Annu_Type = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Annu_Type", "#W-ANNU-TYPE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_W_Contract_Mode = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Contract_Mode", "#W-CONTRACT-MODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Pnd_W_Dod_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Dod_Dte", "#W-DOD-DTE", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_W_Nfp = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Nfp", "#W-NFP", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte", "#W-CNTRCT-ISSUE-DTE", FieldType.STRING, 
            6);
        pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde", "#W-PRTCPNT-CTZNSHP-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde", "#W-PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3);

        pnd_W_Parm_Date = newFieldInRecord("pnd_W_Parm_Date", "#W-PARM-DATE", FieldType.STRING, 8);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef7 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef7", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.STRING, 
            2);

        this.setRecordName("LdaIaal583r");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrl_Rcrd.reset();
    }

    // Constructor
    public LdaIaal583r() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
