/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:51 PM
**        * FROM NATURAL PDA     : IATL410P
************************************************************
**        * FILE NAME            : PdaIatl410p.java
**        * CLASS NAME           : PdaIatl410p
**        * INSTANCE NAME        : PdaIatl410p
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIatl410p extends PdaBase
{
    // Properties
    private DbsGroup pnd_Iatn410_In;
    private DbsField pnd_Iatn410_In_Pnd_Printer_Id;
    private DbsGroup pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst;
    private DbsField pnd_Iatn410_In_Rqst_Id;
    private DbsField pnd_Iatn410_In_Rqst_Entry_Tme;
    private DbsField pnd_Iatn410_In_Rqst_Lst_Actvty_Dte;
    private DbsField pnd_Iatn410_In_Rqst_Cntct_Mde;
    private DbsField pnd_Iatn410_In_Rqst_Rep_Nme;
    private DbsField pnd_Iatn410_In_Pnd_Todays_Dte;
    private DbsGroup pnd_Iatn410_In_Pnd_Todays_DteRedef1;
    private DbsField pnd_Iatn410_In_Pnd_Todays_Dte_N;
    private DbsGroup pnd_Iatn410_Out;
    private DbsField pnd_Iatn410_Out_Pnd_Return_Code;
    private DbsField pnd_Iatn410_Out_Pnd_Msg;

    public DbsGroup getPnd_Iatn410_In() { return pnd_Iatn410_In; }

    public DbsField getPnd_Iatn410_In_Pnd_Printer_Id() { return pnd_Iatn410_In_Pnd_Printer_Id; }

    public DbsGroup getPnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst() { return pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst; }

    public DbsField getPnd_Iatn410_In_Rqst_Id() { return pnd_Iatn410_In_Rqst_Id; }

    public DbsField getPnd_Iatn410_In_Rqst_Entry_Tme() { return pnd_Iatn410_In_Rqst_Entry_Tme; }

    public DbsField getPnd_Iatn410_In_Rqst_Lst_Actvty_Dte() { return pnd_Iatn410_In_Rqst_Lst_Actvty_Dte; }

    public DbsField getPnd_Iatn410_In_Rqst_Cntct_Mde() { return pnd_Iatn410_In_Rqst_Cntct_Mde; }

    public DbsField getPnd_Iatn410_In_Rqst_Rep_Nme() { return pnd_Iatn410_In_Rqst_Rep_Nme; }

    public DbsField getPnd_Iatn410_In_Pnd_Todays_Dte() { return pnd_Iatn410_In_Pnd_Todays_Dte; }

    public DbsGroup getPnd_Iatn410_In_Pnd_Todays_DteRedef1() { return pnd_Iatn410_In_Pnd_Todays_DteRedef1; }

    public DbsField getPnd_Iatn410_In_Pnd_Todays_Dte_N() { return pnd_Iatn410_In_Pnd_Todays_Dte_N; }

    public DbsGroup getPnd_Iatn410_Out() { return pnd_Iatn410_Out; }

    public DbsField getPnd_Iatn410_Out_Pnd_Return_Code() { return pnd_Iatn410_Out_Pnd_Return_Code; }

    public DbsField getPnd_Iatn410_Out_Pnd_Msg() { return pnd_Iatn410_Out_Pnd_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iatn410_In = dbsRecord.newGroupInRecord("pnd_Iatn410_In", "#IATN410-IN");
        pnd_Iatn410_In.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn410_In_Pnd_Printer_Id = pnd_Iatn410_In.newFieldInGroup("pnd_Iatn410_In_Pnd_Printer_Id", "#PRINTER-ID", FieldType.STRING, 4);
        pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst = pnd_Iatn410_In.newGroupInGroup("pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst", "#IAA-TRNSFR-SW-RQST");
        pnd_Iatn410_In_Rqst_Id = pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("pnd_Iatn410_In_Rqst_Id", "RQST-ID", FieldType.STRING, 34);
        pnd_Iatn410_In_Rqst_Entry_Tme = pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("pnd_Iatn410_In_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 
            7);
        pnd_Iatn410_In_Rqst_Lst_Actvty_Dte = pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("pnd_Iatn410_In_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", 
            FieldType.DATE);
        pnd_Iatn410_In_Rqst_Cntct_Mde = pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("pnd_Iatn410_In_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 
            1);
        pnd_Iatn410_In_Rqst_Rep_Nme = pnd_Iatn410_In_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("pnd_Iatn410_In_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 
            40);
        pnd_Iatn410_In_Pnd_Todays_Dte = pnd_Iatn410_In.newFieldInGroup("pnd_Iatn410_In_Pnd_Todays_Dte", "#TODAYS-DTE", FieldType.STRING, 8);
        pnd_Iatn410_In_Pnd_Todays_DteRedef1 = pnd_Iatn410_In.newGroupInGroup("pnd_Iatn410_In_Pnd_Todays_DteRedef1", "Redefines", pnd_Iatn410_In_Pnd_Todays_Dte);
        pnd_Iatn410_In_Pnd_Todays_Dte_N = pnd_Iatn410_In_Pnd_Todays_DteRedef1.newFieldInGroup("pnd_Iatn410_In_Pnd_Todays_Dte_N", "#TODAYS-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Iatn410_Out = dbsRecord.newGroupInRecord("pnd_Iatn410_Out", "#IATN410-OUT");
        pnd_Iatn410_Out.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn410_Out_Pnd_Return_Code = pnd_Iatn410_Out.newFieldInGroup("pnd_Iatn410_Out_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Iatn410_Out_Pnd_Msg = pnd_Iatn410_Out.newFieldInGroup("pnd_Iatn410_Out_Pnd_Msg", "#MSG", FieldType.STRING, 79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIatl410p(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

