/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:40 PM
**        * FROM NATURAL LDA     : AIAL2000
************************************************************
**        * FILE NAME            : LdaAial2000.java
**        * CLASS NAME           : LdaAial2000
**        * INSTANCE NAME        : LdaAial2000
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial2000 extends DbsRecord
{
    // Properties
    private DbsField pnd_Out_Record;
    private DbsGroup pnd_Out_RecordRedef1;
    private DbsField pnd_Out_Record_Pnd_Out_Key;
    private DbsField pnd_Out_Record_Pnd_Out_Filler1;
    private DbsField pnd_Out_Record_Pnd_Out_Contract;
    private DbsField pnd_Out_Record_Pnd_Out_Filler_1;
    private DbsField pnd_Out_Record_Pnd_Out_Payee_Code;
    private DbsGroup pnd_Out_Record_Pnd_Out_Payee_CodeRedef2;
    private DbsField pnd_Out_Record_Pnd_Out_Pay_Code;
    private DbsField pnd_Out_Record_Pnd_Out_Filler2;
    private DbsField pnd_Out_Record_Pnd_Out_Da_Contract;
    private DbsField pnd_Out_Record_Pnd_Out_Filler3;
    private DbsField pnd_Out_Record_Pnd_Out_Transaction_Code;
    private DbsGroup pnd_Out_Record_Pnd_Out_Transaction_CodeRedef3;
    private DbsField pnd_Out_Record_Pnd_Out_Transaction;
    private DbsField pnd_Out_Record_Pnd_Out_Filler4;
    private DbsField pnd_Out_Record_Pnd_Out_Transaction_Date;
    private DbsGroup pnd_Out_Record_Pnd_Out_Transaction_DateRedef4;
    private DbsField pnd_Out_Record_Pnd_Out_Transaction_Date_N;
    private DbsField pnd_Out_Record_Pnd_Out_Filler5;
    private DbsField pnd_Out_Record_Pnd_Out_Effective_Date;
    private DbsGroup pnd_Out_Record_Pnd_Out_Effective_DateRedef5;
    private DbsField pnd_Out_Record_Pnd_Out_Effective_Date_N;
    private DbsGroup pnd_Out_Record_Pnd_Out_Effective_DateRedef6;
    private DbsField pnd_Out_Record_Pnd_Out_Effective_Date_C;
    private DbsField pnd_Out_Record_Pnd_Out_Effective_Date_Y;
    private DbsField pnd_Out_Record_Pnd_Out_Effective_Date_M;
    private DbsField pnd_Out_Record_Pnd_Out_Effective_Date_D;
    private DbsField pnd_Out_Record_Pnd_Out_Filler6;
    private DbsField pnd_Out_Record_Pnd_Out_Check_Date;
    private DbsField pnd_Out_Record_Pnd_Out_Filler7;
    private DbsField pnd_Out_Record_Pnd_Out_Sequence_Code;
    private DbsField pnd_Out_Record_Pnd_Out_Filler8;
    private DbsField pnd_Out_Record_Pnd_Out_Fund_Code;
    private DbsField pnd_Out_Record_Pnd_Out_Filler81;
    private DbsField pnd_Out_Record_Pnd_Out_Rate_Basis;
    private DbsField pnd_Out_Record_Pnd_Out_Filler9;
    private DbsField pnd_Out_Record_Pnd_Out_Reval_Ind;
    private DbsField pnd_Out_Record_Pnd_Out_Filler10;
    private DbsField pnd_Out_Record_Pnd_Out_Per_Units;
    private DbsField pnd_Out_Record_Pnd_Out_Filler11;
    private DbsField pnd_Out_Record_Pnd_Out_Per_Payment;
    private DbsField pnd_Out_Record_Pnd_Out_Filler12;
    private DbsField pnd_Out_Record_Pnd_Out_Option;
    private DbsField pnd_Out_Record_Pnd_Out_Filler13;
    private DbsField pnd_Out_Record_Pnd_Out_Mode;
    private DbsField pnd_Out_Record_Pnd_Out_Filler14;
    private DbsField pnd_Out_Record_Pnd_Out_Origin;
    private DbsField pnd_Out_Record_Pnd_Out_Filler15;
    private DbsField pnd_Out_Record_Pnd_Out_Issue_Date;
    private DbsGroup pnd_Out_Record_Pnd_Out_Issue_DateRedef7;
    private DbsField pnd_Out_Record_Pnd_Out_Issue_Date_N;
    private DbsField pnd_Out_Record_Pnd_Out_Filler16;
    private DbsField pnd_Out_Record_Pnd_Out_Final_Pay_Date;
    private DbsGroup pnd_Out_Record_Pnd_Out_Final_Pay_DateRedef8;
    private DbsField pnd_Out_Record_Pnd_Out_Final_Pay_Date_N;
    private DbsField pnd_Out_Record_Pnd_Out_Filler17;
    private DbsField pnd_Out_Record_Pnd_Out_First_Ann_Xref;
    private DbsField pnd_Out_Record_Pnd_Out_Filler18;
    private DbsField pnd_Out_Record_Pnd_Out_First_Ann_Dob;
    private DbsGroup pnd_Out_Record_Pnd_Out_First_Ann_DobRedef9;
    private DbsField pnd_Out_Record_Pnd_Out_First_Ann_Dob_N;
    private DbsField pnd_Out_Record_Pnd_Out_Filler19;
    private DbsField pnd_Out_Record_Pnd_Out_First_Ann_Sex;
    private DbsField pnd_Out_Record_Pnd_Out_Filler20;
    private DbsField pnd_Out_Record_Pnd_Out_First_Ann_Dod;
    private DbsGroup pnd_Out_Record_Pnd_Out_First_Ann_DodRedef10;
    private DbsField pnd_Out_Record_Pnd_Out_First_Ann_Dod_Yymm;
    private DbsField pnd_Out_Record_Pnd_Out_First_Ann_Dod_Dd;
    private DbsField pnd_Out_Record_Pnd_Out_Filler21;
    private DbsField pnd_Out_Record_Pnd_Out_Sec_Ann_Xref;
    private DbsField pnd_Out_Record_Pnd_Out_Filler22;
    private DbsField pnd_Out_Record_Pnd_Out_Sec_Ann_Dob;
    private DbsGroup pnd_Out_Record_Pnd_Out_Sec_Ann_DobRedef11;
    private DbsField pnd_Out_Record_Pnd_Out_Sec_Ann_Dob_N;
    private DbsField pnd_Out_Record_Pnd_Out_Filler23;
    private DbsField pnd_Out_Record_Pnd_Out_Sec_Ann_Sex;
    private DbsField pnd_Out_Record_Pnd_Out_Filler24;
    private DbsField pnd_Out_Record_Pnd_Out_Sec_Ann_Dod;
    private DbsGroup pnd_Out_Record_Pnd_Out_Sec_Ann_DodRedef12;
    private DbsField pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Yymm;
    private DbsField pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Dd;
    private DbsField pnd_Out_Record_Pnd_Out_Filler25;
    private DbsField pnd_Out_Record_Pnd_Out_Beneficiary_Xref;
    private DbsField pnd_Out_Record_Pnd_Out_Filler26;
    private DbsField pnd_Out_Record_Pnd_Out_Mode_Change_Ind;
    private DbsField pnd_Out_Record_Pnd_Out_Filler27;
    private DbsField pnd_Out_Record_Pnd_Out_Calculated_Asset;
    private DbsField pnd_Out_Record_Pnd_Out_Filler28;

    public DbsField getPnd_Out_Record() { return pnd_Out_Record; }

    public DbsGroup getPnd_Out_RecordRedef1() { return pnd_Out_RecordRedef1; }

    public DbsField getPnd_Out_Record_Pnd_Out_Key() { return pnd_Out_Record_Pnd_Out_Key; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler1() { return pnd_Out_Record_Pnd_Out_Filler1; }

    public DbsField getPnd_Out_Record_Pnd_Out_Contract() { return pnd_Out_Record_Pnd_Out_Contract; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler_1() { return pnd_Out_Record_Pnd_Out_Filler_1; }

    public DbsField getPnd_Out_Record_Pnd_Out_Payee_Code() { return pnd_Out_Record_Pnd_Out_Payee_Code; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Payee_CodeRedef2() { return pnd_Out_Record_Pnd_Out_Payee_CodeRedef2; }

    public DbsField getPnd_Out_Record_Pnd_Out_Pay_Code() { return pnd_Out_Record_Pnd_Out_Pay_Code; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler2() { return pnd_Out_Record_Pnd_Out_Filler2; }

    public DbsField getPnd_Out_Record_Pnd_Out_Da_Contract() { return pnd_Out_Record_Pnd_Out_Da_Contract; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler3() { return pnd_Out_Record_Pnd_Out_Filler3; }

    public DbsField getPnd_Out_Record_Pnd_Out_Transaction_Code() { return pnd_Out_Record_Pnd_Out_Transaction_Code; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Transaction_CodeRedef3() { return pnd_Out_Record_Pnd_Out_Transaction_CodeRedef3; }

    public DbsField getPnd_Out_Record_Pnd_Out_Transaction() { return pnd_Out_Record_Pnd_Out_Transaction; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler4() { return pnd_Out_Record_Pnd_Out_Filler4; }

    public DbsField getPnd_Out_Record_Pnd_Out_Transaction_Date() { return pnd_Out_Record_Pnd_Out_Transaction_Date; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Transaction_DateRedef4() { return pnd_Out_Record_Pnd_Out_Transaction_DateRedef4; }

    public DbsField getPnd_Out_Record_Pnd_Out_Transaction_Date_N() { return pnd_Out_Record_Pnd_Out_Transaction_Date_N; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler5() { return pnd_Out_Record_Pnd_Out_Filler5; }

    public DbsField getPnd_Out_Record_Pnd_Out_Effective_Date() { return pnd_Out_Record_Pnd_Out_Effective_Date; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Effective_DateRedef5() { return pnd_Out_Record_Pnd_Out_Effective_DateRedef5; }

    public DbsField getPnd_Out_Record_Pnd_Out_Effective_Date_N() { return pnd_Out_Record_Pnd_Out_Effective_Date_N; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Effective_DateRedef6() { return pnd_Out_Record_Pnd_Out_Effective_DateRedef6; }

    public DbsField getPnd_Out_Record_Pnd_Out_Effective_Date_C() { return pnd_Out_Record_Pnd_Out_Effective_Date_C; }

    public DbsField getPnd_Out_Record_Pnd_Out_Effective_Date_Y() { return pnd_Out_Record_Pnd_Out_Effective_Date_Y; }

    public DbsField getPnd_Out_Record_Pnd_Out_Effective_Date_M() { return pnd_Out_Record_Pnd_Out_Effective_Date_M; }

    public DbsField getPnd_Out_Record_Pnd_Out_Effective_Date_D() { return pnd_Out_Record_Pnd_Out_Effective_Date_D; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler6() { return pnd_Out_Record_Pnd_Out_Filler6; }

    public DbsField getPnd_Out_Record_Pnd_Out_Check_Date() { return pnd_Out_Record_Pnd_Out_Check_Date; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler7() { return pnd_Out_Record_Pnd_Out_Filler7; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sequence_Code() { return pnd_Out_Record_Pnd_Out_Sequence_Code; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler8() { return pnd_Out_Record_Pnd_Out_Filler8; }

    public DbsField getPnd_Out_Record_Pnd_Out_Fund_Code() { return pnd_Out_Record_Pnd_Out_Fund_Code; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler81() { return pnd_Out_Record_Pnd_Out_Filler81; }

    public DbsField getPnd_Out_Record_Pnd_Out_Rate_Basis() { return pnd_Out_Record_Pnd_Out_Rate_Basis; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler9() { return pnd_Out_Record_Pnd_Out_Filler9; }

    public DbsField getPnd_Out_Record_Pnd_Out_Reval_Ind() { return pnd_Out_Record_Pnd_Out_Reval_Ind; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler10() { return pnd_Out_Record_Pnd_Out_Filler10; }

    public DbsField getPnd_Out_Record_Pnd_Out_Per_Units() { return pnd_Out_Record_Pnd_Out_Per_Units; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler11() { return pnd_Out_Record_Pnd_Out_Filler11; }

    public DbsField getPnd_Out_Record_Pnd_Out_Per_Payment() { return pnd_Out_Record_Pnd_Out_Per_Payment; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler12() { return pnd_Out_Record_Pnd_Out_Filler12; }

    public DbsField getPnd_Out_Record_Pnd_Out_Option() { return pnd_Out_Record_Pnd_Out_Option; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler13() { return pnd_Out_Record_Pnd_Out_Filler13; }

    public DbsField getPnd_Out_Record_Pnd_Out_Mode() { return pnd_Out_Record_Pnd_Out_Mode; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler14() { return pnd_Out_Record_Pnd_Out_Filler14; }

    public DbsField getPnd_Out_Record_Pnd_Out_Origin() { return pnd_Out_Record_Pnd_Out_Origin; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler15() { return pnd_Out_Record_Pnd_Out_Filler15; }

    public DbsField getPnd_Out_Record_Pnd_Out_Issue_Date() { return pnd_Out_Record_Pnd_Out_Issue_Date; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Issue_DateRedef7() { return pnd_Out_Record_Pnd_Out_Issue_DateRedef7; }

    public DbsField getPnd_Out_Record_Pnd_Out_Issue_Date_N() { return pnd_Out_Record_Pnd_Out_Issue_Date_N; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler16() { return pnd_Out_Record_Pnd_Out_Filler16; }

    public DbsField getPnd_Out_Record_Pnd_Out_Final_Pay_Date() { return pnd_Out_Record_Pnd_Out_Final_Pay_Date; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Final_Pay_DateRedef8() { return pnd_Out_Record_Pnd_Out_Final_Pay_DateRedef8; }

    public DbsField getPnd_Out_Record_Pnd_Out_Final_Pay_Date_N() { return pnd_Out_Record_Pnd_Out_Final_Pay_Date_N; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler17() { return pnd_Out_Record_Pnd_Out_Filler17; }

    public DbsField getPnd_Out_Record_Pnd_Out_First_Ann_Xref() { return pnd_Out_Record_Pnd_Out_First_Ann_Xref; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler18() { return pnd_Out_Record_Pnd_Out_Filler18; }

    public DbsField getPnd_Out_Record_Pnd_Out_First_Ann_Dob() { return pnd_Out_Record_Pnd_Out_First_Ann_Dob; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_First_Ann_DobRedef9() { return pnd_Out_Record_Pnd_Out_First_Ann_DobRedef9; }

    public DbsField getPnd_Out_Record_Pnd_Out_First_Ann_Dob_N() { return pnd_Out_Record_Pnd_Out_First_Ann_Dob_N; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler19() { return pnd_Out_Record_Pnd_Out_Filler19; }

    public DbsField getPnd_Out_Record_Pnd_Out_First_Ann_Sex() { return pnd_Out_Record_Pnd_Out_First_Ann_Sex; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler20() { return pnd_Out_Record_Pnd_Out_Filler20; }

    public DbsField getPnd_Out_Record_Pnd_Out_First_Ann_Dod() { return pnd_Out_Record_Pnd_Out_First_Ann_Dod; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_First_Ann_DodRedef10() { return pnd_Out_Record_Pnd_Out_First_Ann_DodRedef10; }

    public DbsField getPnd_Out_Record_Pnd_Out_First_Ann_Dod_Yymm() { return pnd_Out_Record_Pnd_Out_First_Ann_Dod_Yymm; }

    public DbsField getPnd_Out_Record_Pnd_Out_First_Ann_Dod_Dd() { return pnd_Out_Record_Pnd_Out_First_Ann_Dod_Dd; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler21() { return pnd_Out_Record_Pnd_Out_Filler21; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sec_Ann_Xref() { return pnd_Out_Record_Pnd_Out_Sec_Ann_Xref; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler22() { return pnd_Out_Record_Pnd_Out_Filler22; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sec_Ann_Dob() { return pnd_Out_Record_Pnd_Out_Sec_Ann_Dob; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Sec_Ann_DobRedef11() { return pnd_Out_Record_Pnd_Out_Sec_Ann_DobRedef11; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sec_Ann_Dob_N() { return pnd_Out_Record_Pnd_Out_Sec_Ann_Dob_N; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler23() { return pnd_Out_Record_Pnd_Out_Filler23; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sec_Ann_Sex() { return pnd_Out_Record_Pnd_Out_Sec_Ann_Sex; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler24() { return pnd_Out_Record_Pnd_Out_Filler24; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sec_Ann_Dod() { return pnd_Out_Record_Pnd_Out_Sec_Ann_Dod; }

    public DbsGroup getPnd_Out_Record_Pnd_Out_Sec_Ann_DodRedef12() { return pnd_Out_Record_Pnd_Out_Sec_Ann_DodRedef12; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Yymm() { return pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Yymm; }

    public DbsField getPnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Dd() { return pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Dd; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler25() { return pnd_Out_Record_Pnd_Out_Filler25; }

    public DbsField getPnd_Out_Record_Pnd_Out_Beneficiary_Xref() { return pnd_Out_Record_Pnd_Out_Beneficiary_Xref; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler26() { return pnd_Out_Record_Pnd_Out_Filler26; }

    public DbsField getPnd_Out_Record_Pnd_Out_Mode_Change_Ind() { return pnd_Out_Record_Pnd_Out_Mode_Change_Ind; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler27() { return pnd_Out_Record_Pnd_Out_Filler27; }

    public DbsField getPnd_Out_Record_Pnd_Out_Calculated_Asset() { return pnd_Out_Record_Pnd_Out_Calculated_Asset; }

    public DbsField getPnd_Out_Record_Pnd_Out_Filler28() { return pnd_Out_Record_Pnd_Out_Filler28; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Out_Record = newFieldInRecord("pnd_Out_Record", "#OUT-RECORD", FieldType.STRING, 228);
        pnd_Out_RecordRedef1 = newGroupInRecord("pnd_Out_RecordRedef1", "Redefines", pnd_Out_Record);
        pnd_Out_Record_Pnd_Out_Key = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Key", "#OUT-KEY", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Filler1 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler1", "#OUT-FILLER1", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Contract = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Contract", "#OUT-CONTRACT", FieldType.STRING, 8);
        pnd_Out_Record_Pnd_Out_Filler_1 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler_1", "#OUT-FILLER-1", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Payee_Code = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Payee_Code", "#OUT-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Out_Record_Pnd_Out_Payee_CodeRedef2 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Payee_CodeRedef2", "Redefines", pnd_Out_Record_Pnd_Out_Payee_Code);
        pnd_Out_Record_Pnd_Out_Pay_Code = pnd_Out_Record_Pnd_Out_Payee_CodeRedef2.newFieldInGroup("pnd_Out_Record_Pnd_Out_Pay_Code", "#OUT-PAY-CODE", 
            FieldType.STRING, 2);
        pnd_Out_Record_Pnd_Out_Filler2 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler2", "#OUT-FILLER2", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Da_Contract = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Da_Contract", "#OUT-DA-CONTRACT", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_Filler3 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler3", "#OUT-FILLER3", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Transaction_Code = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Transaction_Code", "#OUT-TRANSACTION-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Out_Record_Pnd_Out_Transaction_CodeRedef3 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Transaction_CodeRedef3", "Redefines", 
            pnd_Out_Record_Pnd_Out_Transaction_Code);
        pnd_Out_Record_Pnd_Out_Transaction = pnd_Out_Record_Pnd_Out_Transaction_CodeRedef3.newFieldInGroup("pnd_Out_Record_Pnd_Out_Transaction", "#OUT-TRANSACTION", 
            FieldType.NUMERIC, 2);
        pnd_Out_Record_Pnd_Out_Filler4 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler4", "#OUT-FILLER4", FieldType.STRING, 13);
        pnd_Out_Record_Pnd_Out_Transaction_Date = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Transaction_Date", "#OUT-TRANSACTION-DATE", 
            FieldType.STRING, 8);
        pnd_Out_Record_Pnd_Out_Transaction_DateRedef4 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Transaction_DateRedef4", "Redefines", 
            pnd_Out_Record_Pnd_Out_Transaction_Date);
        pnd_Out_Record_Pnd_Out_Transaction_Date_N = pnd_Out_Record_Pnd_Out_Transaction_DateRedef4.newFieldInGroup("pnd_Out_Record_Pnd_Out_Transaction_Date_N", 
            "#OUT-TRANSACTION-DATE-N", FieldType.NUMERIC, 8);
        pnd_Out_Record_Pnd_Out_Filler5 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler5", "#OUT-FILLER5", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Effective_Date = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Effective_Date", "#OUT-EFFECTIVE-DATE", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_Effective_DateRedef5 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Effective_DateRedef5", "Redefines", 
            pnd_Out_Record_Pnd_Out_Effective_Date);
        pnd_Out_Record_Pnd_Out_Effective_Date_N = pnd_Out_Record_Pnd_Out_Effective_DateRedef5.newFieldInGroup("pnd_Out_Record_Pnd_Out_Effective_Date_N", 
            "#OUT-EFFECTIVE-DATE-N", FieldType.NUMERIC, 8);
        pnd_Out_Record_Pnd_Out_Effective_DateRedef6 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Effective_DateRedef6", "Redefines", 
            pnd_Out_Record_Pnd_Out_Effective_Date);
        pnd_Out_Record_Pnd_Out_Effective_Date_C = pnd_Out_Record_Pnd_Out_Effective_DateRedef6.newFieldInGroup("pnd_Out_Record_Pnd_Out_Effective_Date_C", 
            "#OUT-EFFECTIVE-DATE-C", FieldType.STRING, 2);
        pnd_Out_Record_Pnd_Out_Effective_Date_Y = pnd_Out_Record_Pnd_Out_Effective_DateRedef6.newFieldInGroup("pnd_Out_Record_Pnd_Out_Effective_Date_Y", 
            "#OUT-EFFECTIVE-DATE-Y", FieldType.STRING, 2);
        pnd_Out_Record_Pnd_Out_Effective_Date_M = pnd_Out_Record_Pnd_Out_Effective_DateRedef6.newFieldInGroup("pnd_Out_Record_Pnd_Out_Effective_Date_M", 
            "#OUT-EFFECTIVE-DATE-M", FieldType.STRING, 2);
        pnd_Out_Record_Pnd_Out_Effective_Date_D = pnd_Out_Record_Pnd_Out_Effective_DateRedef6.newFieldInGroup("pnd_Out_Record_Pnd_Out_Effective_Date_D", 
            "#OUT-EFFECTIVE-DATE-D", FieldType.STRING, 2);
        pnd_Out_Record_Pnd_Out_Filler6 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler6", "#OUT-FILLER6", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Check_Date = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Check_Date", "#OUT-CHECK-DATE", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_Filler7 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler7", "#OUT-FILLER7", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Sequence_Code = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sequence_Code", "#OUT-SEQUENCE-CODE", FieldType.STRING, 
            2);
        pnd_Out_Record_Pnd_Out_Filler8 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler8", "#OUT-FILLER8", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Fund_Code = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Fund_Code", "#OUT-FUND-CODE", FieldType.STRING, 
            1);
        pnd_Out_Record_Pnd_Out_Filler81 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler81", "#OUT-FILLER81", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Rate_Basis = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Rate_Basis", "#OUT-RATE-BASIS", FieldType.NUMERIC, 
            2);
        pnd_Out_Record_Pnd_Out_Filler9 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler9", "#OUT-FILLER9", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Reval_Ind = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Reval_Ind", "#OUT-REVAL-IND", FieldType.STRING, 
            1);
        pnd_Out_Record_Pnd_Out_Filler10 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler10", "#OUT-FILLER10", FieldType.STRING, 15);
        pnd_Out_Record_Pnd_Out_Per_Units = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Per_Units", "#OUT-PER-UNITS", FieldType.STRING, 
            12);
        pnd_Out_Record_Pnd_Out_Filler11 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler11", "#OUT-FILLER11", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Per_Payment = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Per_Payment", "#OUT-PER-PAYMENT", FieldType.STRING, 
            11);
        pnd_Out_Record_Pnd_Out_Filler12 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler12", "#OUT-FILLER12", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Option = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Option", "#OUT-OPTION", FieldType.NUMERIC, 2);
        pnd_Out_Record_Pnd_Out_Filler13 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler13", "#OUT-FILLER13", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Mode = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Mode", "#OUT-MODE", FieldType.NUMERIC, 3);
        pnd_Out_Record_Pnd_Out_Filler14 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler14", "#OUT-FILLER14", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Origin = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Origin", "#OUT-ORIGIN", FieldType.NUMERIC, 2);
        pnd_Out_Record_Pnd_Out_Filler15 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler15", "#OUT-FILLER15", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Issue_Date = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Issue_Date", "#OUT-ISSUE-DATE", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_Issue_DateRedef7 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Issue_DateRedef7", "Redefines", pnd_Out_Record_Pnd_Out_Issue_Date);
        pnd_Out_Record_Pnd_Out_Issue_Date_N = pnd_Out_Record_Pnd_Out_Issue_DateRedef7.newFieldInGroup("pnd_Out_Record_Pnd_Out_Issue_Date_N", "#OUT-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Out_Record_Pnd_Out_Filler16 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler16", "#OUT-FILLER16", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Final_Pay_Date = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Final_Pay_Date", "#OUT-FINAL-PAY-DATE", FieldType.STRING, 
            6);
        pnd_Out_Record_Pnd_Out_Final_Pay_DateRedef8 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Final_Pay_DateRedef8", "Redefines", 
            pnd_Out_Record_Pnd_Out_Final_Pay_Date);
        pnd_Out_Record_Pnd_Out_Final_Pay_Date_N = pnd_Out_Record_Pnd_Out_Final_Pay_DateRedef8.newFieldInGroup("pnd_Out_Record_Pnd_Out_Final_Pay_Date_N", 
            "#OUT-FINAL-PAY-DATE-N", FieldType.NUMERIC, 6);
        pnd_Out_Record_Pnd_Out_Filler17 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler17", "#OUT-FILLER17", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_First_Ann_Xref = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_First_Ann_Xref", "#OUT-FIRST-ANN-XREF", FieldType.STRING, 
            9);
        pnd_Out_Record_Pnd_Out_Filler18 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler18", "#OUT-FILLER18", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_First_Ann_Dob = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_First_Ann_Dob", "#OUT-FIRST-ANN-DOB", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_First_Ann_DobRedef9 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_First_Ann_DobRedef9", "Redefines", pnd_Out_Record_Pnd_Out_First_Ann_Dob);
        pnd_Out_Record_Pnd_Out_First_Ann_Dob_N = pnd_Out_Record_Pnd_Out_First_Ann_DobRedef9.newFieldInGroup("pnd_Out_Record_Pnd_Out_First_Ann_Dob_N", 
            "#OUT-FIRST-ANN-DOB-N", FieldType.NUMERIC, 8);
        pnd_Out_Record_Pnd_Out_Filler19 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler19", "#OUT-FILLER19", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_First_Ann_Sex = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_First_Ann_Sex", "#OUT-FIRST-ANN-SEX", FieldType.NUMERIC, 
            1);
        pnd_Out_Record_Pnd_Out_Filler20 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler20", "#OUT-FILLER20", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_First_Ann_Dod = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_First_Ann_Dod", "#OUT-FIRST-ANN-DOD", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_First_Ann_DodRedef10 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_First_Ann_DodRedef10", "Redefines", 
            pnd_Out_Record_Pnd_Out_First_Ann_Dod);
        pnd_Out_Record_Pnd_Out_First_Ann_Dod_Yymm = pnd_Out_Record_Pnd_Out_First_Ann_DodRedef10.newFieldInGroup("pnd_Out_Record_Pnd_Out_First_Ann_Dod_Yymm", 
            "#OUT-FIRST-ANN-DOD-YYMM", FieldType.NUMERIC, 6);
        pnd_Out_Record_Pnd_Out_First_Ann_Dod_Dd = pnd_Out_Record_Pnd_Out_First_Ann_DodRedef10.newFieldInGroup("pnd_Out_Record_Pnd_Out_First_Ann_Dod_Dd", 
            "#OUT-FIRST-ANN-DOD-DD", FieldType.NUMERIC, 2);
        pnd_Out_Record_Pnd_Out_Filler21 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler21", "#OUT-FILLER21", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Sec_Ann_Xref = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_Xref", "#OUT-SEC-ANN-XREF", FieldType.STRING, 
            9);
        pnd_Out_Record_Pnd_Out_Filler22 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler22", "#OUT-FILLER22", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Sec_Ann_Dob = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_Dob", "#OUT-SEC-ANN-DOB", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_Sec_Ann_DobRedef11 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_DobRedef11", "Redefines", pnd_Out_Record_Pnd_Out_Sec_Ann_Dob);
        pnd_Out_Record_Pnd_Out_Sec_Ann_Dob_N = pnd_Out_Record_Pnd_Out_Sec_Ann_DobRedef11.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_Dob_N", "#OUT-SEC-ANN-DOB-N", 
            FieldType.NUMERIC, 8);
        pnd_Out_Record_Pnd_Out_Filler23 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler23", "#OUT-FILLER23", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Sec_Ann_Sex = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_Sex", "#OUT-SEC-ANN-SEX", FieldType.NUMERIC, 
            1);
        pnd_Out_Record_Pnd_Out_Filler24 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler24", "#OUT-FILLER24", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Sec_Ann_Dod = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_Dod", "#OUT-SEC-ANN-DOD", FieldType.STRING, 
            8);
        pnd_Out_Record_Pnd_Out_Sec_Ann_DodRedef12 = pnd_Out_RecordRedef1.newGroupInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_DodRedef12", "Redefines", pnd_Out_Record_Pnd_Out_Sec_Ann_Dod);
        pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Yymm = pnd_Out_Record_Pnd_Out_Sec_Ann_DodRedef12.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Yymm", 
            "#OUT-SEC-ANN-DOD-YYMM", FieldType.NUMERIC, 6);
        pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Dd = pnd_Out_Record_Pnd_Out_Sec_Ann_DodRedef12.newFieldInGroup("pnd_Out_Record_Pnd_Out_Sec_Ann_Dod_Dd", "#OUT-SEC-ANN-DOD-DD", 
            FieldType.NUMERIC, 2);
        pnd_Out_Record_Pnd_Out_Filler25 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler25", "#OUT-FILLER25", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Beneficiary_Xref = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Beneficiary_Xref", "#OUT-BENEFICIARY-XREF", 
            FieldType.STRING, 9);
        pnd_Out_Record_Pnd_Out_Filler26 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler26", "#OUT-FILLER26", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Mode_Change_Ind = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Mode_Change_Ind", "#OUT-MODE-CHANGE-IND", 
            FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Filler27 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler27", "#OUT-FILLER27", FieldType.STRING, 1);
        pnd_Out_Record_Pnd_Out_Calculated_Asset = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Calculated_Asset", "#OUT-CALCULATED-ASSET", 
            FieldType.STRING, 13);
        pnd_Out_Record_Pnd_Out_Filler28 = pnd_Out_RecordRedef1.newFieldInGroup("pnd_Out_Record_Pnd_Out_Filler28", "#OUT-FILLER28", FieldType.STRING, 3);

        this.setRecordName("LdaAial2000");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAial2000() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
