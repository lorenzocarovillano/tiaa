/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:09 PM
**        * FROM NATURAL LDA     : IAAA029C
************************************************************
**        * FILE NAME            : LdaIaaa029c.java
**        * CLASS NAME           : LdaIaaa029c
**        * INSTANCE NAME        : LdaIaaa029c
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaaa029c extends DbsRecord
{
    // Properties
    private DbsGroup afaplint_Function_Codes;
    private DbsField afaplint_Function_Codes_Afaplint_Open;
    private DbsField afaplint_Function_Codes_Afaplint_Write;
    private DbsField afaplint_Function_Codes_Afaplint_Close;
    private DbsGroup afaplint_Control_Fields;
    private DbsField afaplint_Control_Fields_Afaplint_Return_Code;
    private DbsField afaplint_Control_Fields_Afaplint_File_Name;
    private DbsField afaplint_Control_Fields_Afaplint_File_Occurrence;
    private DbsField afaplint_Control_Fields_Afaplint_Ddname;
    private DbsField afaplint_Control_Fields_Afaplint_Record_Id;
    private DbsField afaplint_Control_Fields_Afaplint_Record_Len;
    private DbsGroup afaplint_Control_FieldsRedef1;
    private DbsField afaplint_Control_Fields_Afaplint_Control_Fields_Alpha;
    private DbsField pnd_Csf_Data;
    private DbsGroup pnd_Csf_DataRedef2;
    private DbsField pnd_Csf_Data_Pnd_Csf_Sort_Key;
    private DbsGroup pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3;
    private DbsGroup pnd_Csf_Data_Pnd_L1_Sort_Flds;
    private DbsField pnd_Csf_Data_Pnd_Rsk_L1_Value_N;
    private DbsGroup pnd_Csf_Data_Pnd_Rsk_L1_Value_NRedef4;
    private DbsField pnd_Csf_Data_Pnd_Rsk_L1_Value;
    private DbsField pnd_Csf_Data_Pnd_Id_Li;
    private DbsGroup pnd_Csf_Data_Pnd_L2_Sort_Flds;
    private DbsField pnd_Csf_Data_Pnd_Id_L2;
    private DbsField pnd_Csf_Data_Pnd_Key_L2_Area;
    private DbsField pnd_Csf_Data_Pnd_Key_L2_Nbr_Of_Contracts;
    private DbsField pnd_Csf_Data_Pnd_Key_L2_Pin;
    private DbsField pnd_Csf_Data_Pnd_Key_L2_Payee_Cde;
    private DbsGroup pnd_Csf_Data_Pnd_L3_Sort_Flds;
    private DbsField pnd_Csf_Data_Pnd_Id_L3;
    private DbsField pnd_Csf_Data_Pnd_Key_L3_Tiebreak;
    private DbsGroup pnd_Csf_Data_Pnd_L4_Sort_Flds;
    private DbsField pnd_Csf_Data_Pnd_Id_L4;
    private DbsField pnd_Csf_Data_Pnd_Key_L4_Tiebreak;
    private DbsField pnd_Csf_Data_Pnd_Key_L4_Tiebreak2;
    private DbsField pnd_Csf_Data_Pnd_Key_L4_Tiebreak3;
    private DbsField pnd_Csf_Data_Pnd_Application_Data;

    public DbsGroup getAfaplint_Function_Codes() { return afaplint_Function_Codes; }

    public DbsField getAfaplint_Function_Codes_Afaplint_Open() { return afaplint_Function_Codes_Afaplint_Open; }

    public DbsField getAfaplint_Function_Codes_Afaplint_Write() { return afaplint_Function_Codes_Afaplint_Write; }

    public DbsField getAfaplint_Function_Codes_Afaplint_Close() { return afaplint_Function_Codes_Afaplint_Close; }

    public DbsGroup getAfaplint_Control_Fields() { return afaplint_Control_Fields; }

    public DbsField getAfaplint_Control_Fields_Afaplint_Return_Code() { return afaplint_Control_Fields_Afaplint_Return_Code; }

    public DbsField getAfaplint_Control_Fields_Afaplint_File_Name() { return afaplint_Control_Fields_Afaplint_File_Name; }

    public DbsField getAfaplint_Control_Fields_Afaplint_File_Occurrence() { return afaplint_Control_Fields_Afaplint_File_Occurrence; }

    public DbsField getAfaplint_Control_Fields_Afaplint_Ddname() { return afaplint_Control_Fields_Afaplint_Ddname; }

    public DbsField getAfaplint_Control_Fields_Afaplint_Record_Id() { return afaplint_Control_Fields_Afaplint_Record_Id; }

    public DbsField getAfaplint_Control_Fields_Afaplint_Record_Len() { return afaplint_Control_Fields_Afaplint_Record_Len; }

    public DbsGroup getAfaplint_Control_FieldsRedef1() { return afaplint_Control_FieldsRedef1; }

    public DbsField getAfaplint_Control_Fields_Afaplint_Control_Fields_Alpha() { return afaplint_Control_Fields_Afaplint_Control_Fields_Alpha; }

    public DbsField getPnd_Csf_Data() { return pnd_Csf_Data; }

    public DbsGroup getPnd_Csf_DataRedef2() { return pnd_Csf_DataRedef2; }

    public DbsField getPnd_Csf_Data_Pnd_Csf_Sort_Key() { return pnd_Csf_Data_Pnd_Csf_Sort_Key; }

    public DbsGroup getPnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3() { return pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3; }

    public DbsGroup getPnd_Csf_Data_Pnd_L1_Sort_Flds() { return pnd_Csf_Data_Pnd_L1_Sort_Flds; }

    public DbsField getPnd_Csf_Data_Pnd_Rsk_L1_Value_N() { return pnd_Csf_Data_Pnd_Rsk_L1_Value_N; }

    public DbsGroup getPnd_Csf_Data_Pnd_Rsk_L1_Value_NRedef4() { return pnd_Csf_Data_Pnd_Rsk_L1_Value_NRedef4; }

    public DbsField getPnd_Csf_Data_Pnd_Rsk_L1_Value() { return pnd_Csf_Data_Pnd_Rsk_L1_Value; }

    public DbsField getPnd_Csf_Data_Pnd_Id_Li() { return pnd_Csf_Data_Pnd_Id_Li; }

    public DbsGroup getPnd_Csf_Data_Pnd_L2_Sort_Flds() { return pnd_Csf_Data_Pnd_L2_Sort_Flds; }

    public DbsField getPnd_Csf_Data_Pnd_Id_L2() { return pnd_Csf_Data_Pnd_Id_L2; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L2_Area() { return pnd_Csf_Data_Pnd_Key_L2_Area; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L2_Nbr_Of_Contracts() { return pnd_Csf_Data_Pnd_Key_L2_Nbr_Of_Contracts; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L2_Pin() { return pnd_Csf_Data_Pnd_Key_L2_Pin; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L2_Payee_Cde() { return pnd_Csf_Data_Pnd_Key_L2_Payee_Cde; }

    public DbsGroup getPnd_Csf_Data_Pnd_L3_Sort_Flds() { return pnd_Csf_Data_Pnd_L3_Sort_Flds; }

    public DbsField getPnd_Csf_Data_Pnd_Id_L3() { return pnd_Csf_Data_Pnd_Id_L3; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L3_Tiebreak() { return pnd_Csf_Data_Pnd_Key_L3_Tiebreak; }

    public DbsGroup getPnd_Csf_Data_Pnd_L4_Sort_Flds() { return pnd_Csf_Data_Pnd_L4_Sort_Flds; }

    public DbsField getPnd_Csf_Data_Pnd_Id_L4() { return pnd_Csf_Data_Pnd_Id_L4; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L4_Tiebreak() { return pnd_Csf_Data_Pnd_Key_L4_Tiebreak; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L4_Tiebreak2() { return pnd_Csf_Data_Pnd_Key_L4_Tiebreak2; }

    public DbsField getPnd_Csf_Data_Pnd_Key_L4_Tiebreak3() { return pnd_Csf_Data_Pnd_Key_L4_Tiebreak3; }

    public DbsField getPnd_Csf_Data_Pnd_Application_Data() { return pnd_Csf_Data_Pnd_Application_Data; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        afaplint_Function_Codes = newGroupInRecord("afaplint_Function_Codes", "AFAPLINT-FUNCTION-CODES");
        afaplint_Function_Codes_Afaplint_Open = afaplint_Function_Codes.newFieldInGroup("afaplint_Function_Codes_Afaplint_Open", "AFAPLINT-OPEN", FieldType.STRING, 
            8);
        afaplint_Function_Codes_Afaplint_Write = afaplint_Function_Codes.newFieldInGroup("afaplint_Function_Codes_Afaplint_Write", "AFAPLINT-WRITE", FieldType.STRING, 
            8);
        afaplint_Function_Codes_Afaplint_Close = afaplint_Function_Codes.newFieldInGroup("afaplint_Function_Codes_Afaplint_Close", "AFAPLINT-CLOSE", FieldType.STRING, 
            8);

        afaplint_Control_Fields = newGroupInRecord("afaplint_Control_Fields", "AFAPLINT-CONTROL-FIELDS");
        afaplint_Control_Fields_Afaplint_Return_Code = afaplint_Control_Fields.newFieldInGroup("afaplint_Control_Fields_Afaplint_Return_Code", "AFAPLINT-RETURN-CODE", 
            FieldType.INTEGER, 2);
        afaplint_Control_Fields_Afaplint_File_Name = afaplint_Control_Fields.newFieldInGroup("afaplint_Control_Fields_Afaplint_File_Name", "AFAPLINT-FILE-NAME", 
            FieldType.STRING, 16);
        afaplint_Control_Fields_Afaplint_File_Occurrence = afaplint_Control_Fields.newFieldInGroup("afaplint_Control_Fields_Afaplint_File_Occurrence", 
            "AFAPLINT-FILE-OCCURRENCE", FieldType.INTEGER, 2);
        afaplint_Control_Fields_Afaplint_Ddname = afaplint_Control_Fields.newFieldInGroup("afaplint_Control_Fields_Afaplint_Ddname", "AFAPLINT-DDNAME", 
            FieldType.STRING, 8);
        afaplint_Control_Fields_Afaplint_Record_Id = afaplint_Control_Fields.newFieldInGroup("afaplint_Control_Fields_Afaplint_Record_Id", "AFAPLINT-RECORD-ID", 
            FieldType.STRING, 2);
        afaplint_Control_Fields_Afaplint_Record_Len = afaplint_Control_Fields.newFieldInGroup("afaplint_Control_Fields_Afaplint_Record_Len", "AFAPLINT-RECORD-LEN", 
            FieldType.INTEGER, 2);
        afaplint_Control_FieldsRedef1 = newGroupInRecord("afaplint_Control_FieldsRedef1", "Redefines", afaplint_Control_Fields);
        afaplint_Control_Fields_Afaplint_Control_Fields_Alpha = afaplint_Control_FieldsRedef1.newFieldInGroup("afaplint_Control_Fields_Afaplint_Control_Fields_Alpha", 
            "AFAPLINT-CONTROL-FIELDS-ALPHA", FieldType.STRING, 32);

        pnd_Csf_Data = newFieldArrayInRecord("pnd_Csf_Data", "#CSF-DATA", FieldType.STRING, 218, new DbsArrayController(1,2));
        pnd_Csf_DataRedef2 = newGroupInRecord("pnd_Csf_DataRedef2", "Redefines", pnd_Csf_Data);
        pnd_Csf_Data_Pnd_Csf_Sort_Key = pnd_Csf_DataRedef2.newFieldArrayInGroup("pnd_Csf_Data_Pnd_Csf_Sort_Key", "#CSF-SORT-KEY", FieldType.STRING, 1, 
            new DbsArrayController(1,41));
        pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3 = pnd_Csf_DataRedef2.newGroupInGroup("pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3", "Redefines", pnd_Csf_Data_Pnd_Csf_Sort_Key);
        pnd_Csf_Data_Pnd_L1_Sort_Flds = pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3.newGroupInGroup("pnd_Csf_Data_Pnd_L1_Sort_Flds", "#L1-SORT-FLDS");
        pnd_Csf_Data_Pnd_Rsk_L1_Value_N = pnd_Csf_Data_Pnd_L1_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Rsk_L1_Value_N", "#RSK-L1-VALUE-N", FieldType.NUMERIC, 
            2);
        pnd_Csf_Data_Pnd_Rsk_L1_Value_NRedef4 = pnd_Csf_Data_Pnd_L1_Sort_Flds.newGroupInGroup("pnd_Csf_Data_Pnd_Rsk_L1_Value_NRedef4", "Redefines", pnd_Csf_Data_Pnd_Rsk_L1_Value_N);
        pnd_Csf_Data_Pnd_Rsk_L1_Value = pnd_Csf_Data_Pnd_Rsk_L1_Value_NRedef4.newFieldInGroup("pnd_Csf_Data_Pnd_Rsk_L1_Value", "#RSK-L1-VALUE", FieldType.STRING, 
            2);
        pnd_Csf_Data_Pnd_Id_Li = pnd_Csf_Data_Pnd_L1_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Id_Li", "#ID-LI", FieldType.STRING, 1);
        pnd_Csf_Data_Pnd_L2_Sort_Flds = pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3.newGroupInGroup("pnd_Csf_Data_Pnd_L2_Sort_Flds", "#L2-SORT-FLDS");
        pnd_Csf_Data_Pnd_Id_L2 = pnd_Csf_Data_Pnd_L2_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Id_L2", "#ID-L2", FieldType.STRING, 1);
        pnd_Csf_Data_Pnd_Key_L2_Area = pnd_Csf_Data_Pnd_L2_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L2_Area", "#KEY-L2-AREA", FieldType.STRING, 
            3);
        pnd_Csf_Data_Pnd_Key_L2_Nbr_Of_Contracts = pnd_Csf_Data_Pnd_L2_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L2_Nbr_Of_Contracts", "#KEY-L2-NBR-OF-CONTRACTS", 
            FieldType.NUMERIC, 3);
        pnd_Csf_Data_Pnd_Key_L2_Pin = pnd_Csf_Data_Pnd_L2_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L2_Pin", "#KEY-L2-PIN", FieldType.NUMERIC, 12);
        pnd_Csf_Data_Pnd_Key_L2_Payee_Cde = pnd_Csf_Data_Pnd_L2_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L2_Payee_Cde", "#KEY-L2-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Csf_Data_Pnd_L3_Sort_Flds = pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3.newGroupInGroup("pnd_Csf_Data_Pnd_L3_Sort_Flds", "#L3-SORT-FLDS");
        pnd_Csf_Data_Pnd_Id_L3 = pnd_Csf_Data_Pnd_L3_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Id_L3", "#ID-L3", FieldType.STRING, 1);
        pnd_Csf_Data_Pnd_Key_L3_Tiebreak = pnd_Csf_Data_Pnd_L3_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L3_Tiebreak", "#KEY-L3-TIEBREAK", FieldType.STRING, 
            10);
        pnd_Csf_Data_Pnd_L4_Sort_Flds = pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3.newGroupInGroup("pnd_Csf_Data_Pnd_L4_Sort_Flds", "#L4-SORT-FLDS");
        pnd_Csf_Data_Pnd_Id_L4 = pnd_Csf_Data_Pnd_L4_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Id_L4", "#ID-L4", FieldType.STRING, 1);
        pnd_Csf_Data_Pnd_Key_L4_Tiebreak = pnd_Csf_Data_Pnd_L4_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L4_Tiebreak", "#KEY-L4-TIEBREAK", FieldType.NUMERIC, 
            1);
        pnd_Csf_Data_Pnd_Key_L4_Tiebreak2 = pnd_Csf_Data_Pnd_L4_Sort_Flds.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L4_Tiebreak2", "#KEY-L4-TIEBREAK2", FieldType.STRING, 
            1);
        pnd_Csf_Data_Pnd_Key_L4_Tiebreak3 = pnd_Csf_Data_Pnd_Csf_Sort_KeyRedef3.newFieldInGroup("pnd_Csf_Data_Pnd_Key_L4_Tiebreak3", "#KEY-L4-TIEBREAK3", 
            FieldType.NUMERIC, 3);
        pnd_Csf_Data_Pnd_Application_Data = pnd_Csf_DataRedef2.newFieldArrayInGroup("pnd_Csf_Data_Pnd_Application_Data", "#APPLICATION-DATA", FieldType.STRING, 
            1, new DbsArrayController(1,395));

        this.setRecordName("LdaIaaa029c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        afaplint_Function_Codes_Afaplint_Open.setInitialValue("OPEN");
        afaplint_Function_Codes_Afaplint_Write.setInitialValue("WRITE");
        afaplint_Function_Codes_Afaplint_Close.setInitialValue("CLOSE");
    }

    // Constructor
    public LdaIaaa029c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
