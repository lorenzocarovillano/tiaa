/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:45 PM
**        * FROM NATURAL LDA     : IAAL204A
************************************************************
**        * FILE NAME            : LdaIaal204a.java
**        * CLASS NAME           : LdaIaal204a
**        * INSTANCE NAME        : LdaIaal204a
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal204a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Xfr_Audit_View;
    private DbsField iaa_Xfr_Audit_View_Rcrd_Type_Cde;
    private DbsField iaa_Xfr_Audit_View_Rqst_Id;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_Unique_Id;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Dte;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Invrse_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Invrse_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Entry_Dte;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Entry_Tme;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_Sttmnt_Indctr;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_Status_Cde;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Cwf_Wpid;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Cwf_Log_Dte_Time;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_Reject_Cde;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Opn_Clsd_Ind;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_In_Progress_Ind;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Retry_Cnt;
    private DbsField iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_From_Acct_Data;
    private DbsGroup iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Fund_Cde;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Frm_Acct_Cd;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Frm_Unit_Typ;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Typ;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Qty;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Guar;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Divid;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Units;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Unit_Val;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Pmt_Aftr_Xfr;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_To_Acct_Data;
    private DbsGroup iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Fund_Cde;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_New_Fund_Rec;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_New_Phys_Cntrct_Issue;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Typ;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Qty;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Units;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Rate_Cde;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Asset_Amt;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_To_Payee_Cde;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_New_Issue_Ind;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Calc_Unit_Val_Dte;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Cycle_Dte;
    private DbsField iaa_Xfr_Audit_View_Iaxfr_Acctng_Dte;
    private DbsField iaa_Xfr_Audit_View_Lst_Chnge_Dte;
    private DbsField iaa_Xfr_Audit_View_Rqst_Xfr_Type;
    private DbsField iaa_Xfr_Audit_View_Rqst_Unit_Cde;
    private DbsField iaa_Xfr_Audit_View_Ia_New_Iss_Prt_Pull;

    public DataAccessProgramView getVw_iaa_Xfr_Audit_View() { return vw_iaa_Xfr_Audit_View; }

    public DbsField getIaa_Xfr_Audit_View_Rcrd_Type_Cde() { return iaa_Xfr_Audit_View_Rcrd_Type_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Rqst_Id() { return iaa_Xfr_Audit_View_Rqst_Id; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_Unique_Id() { return iaa_Xfr_Audit_View_Iaxfr_Calc_Unique_Id; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Effctve_Dte() { return iaa_Xfr_Audit_View_Iaxfr_Effctve_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Dte() { return iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Tme() { return iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Tme; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Invrse_Effctve_Dte() { return iaa_Xfr_Audit_View_Iaxfr_Invrse_Effctve_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Invrse_Rcvd_Tme() { return iaa_Xfr_Audit_View_Iaxfr_Invrse_Rcvd_Tme; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Entry_Dte() { return iaa_Xfr_Audit_View_Iaxfr_Entry_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Entry_Tme() { return iaa_Xfr_Audit_View_Iaxfr_Entry_Tme; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_Sttmnt_Indctr() { return iaa_Xfr_Audit_View_Iaxfr_Calc_Sttmnt_Indctr; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_Status_Cde() { return iaa_Xfr_Audit_View_Iaxfr_Calc_Status_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Cwf_Wpid() { return iaa_Xfr_Audit_View_Iaxfr_Cwf_Wpid; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Cwf_Log_Dte_Time() { return iaa_Xfr_Audit_View_Iaxfr_Cwf_Log_Dte_Time; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr() { return iaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde() { return iaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_Reject_Cde() { return iaa_Xfr_Audit_View_Iaxfr_Calc_Reject_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Opn_Clsd_Ind() { return iaa_Xfr_Audit_View_Iaxfr_Opn_Clsd_Ind; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_In_Progress_Ind() { return iaa_Xfr_Audit_View_Iaxfr_In_Progress_Ind; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Retry_Cnt() { return iaa_Xfr_Audit_View_Iaxfr_Retry_Cnt; }

    public DbsField getIaa_Xfr_Audit_View_Count_Castiaxfr_Calc_From_Acct_Data() { return iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_From_Acct_Data; }

    public DbsGroup getIaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data() { return iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Fund_Cde() { return iaa_Xfr_Audit_View_Iaxfr_From_Fund_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Frm_Acct_Cd() { return iaa_Xfr_Audit_View_Iaxfr_Frm_Acct_Cd; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Frm_Unit_Typ() { return iaa_Xfr_Audit_View_Iaxfr_Frm_Unit_Typ; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Typ() { return iaa_Xfr_Audit_View_Iaxfr_From_Typ; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Qty() { return iaa_Xfr_Audit_View_Iaxfr_From_Qty; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Guar() { return iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Guar; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Divid() { return iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Divid; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Units() { return iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Units; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Unit_Val() { return iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Unit_Val; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Reval_Unit_Val() { return iaa_Xfr_Audit_View_Iaxfr_From_Reval_Unit_Val; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Amt() { return iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Amt; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Units() { return iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Units; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Guar() { return iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Guar; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Divid() { return iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Divid; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Asset_Xfr_Amt() { return iaa_Xfr_Audit_View_Iaxfr_From_Asset_Xfr_Amt; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Pmt_Aftr_Xfr() { return iaa_Xfr_Audit_View_Iaxfr_From_Pmt_Aftr_Xfr; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Units() { return iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Units; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Guar() { return iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Guar; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Divid() { return iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Divid; }

    public DbsField getIaa_Xfr_Audit_View_Count_Castiaxfr_Calc_To_Acct_Data() { return iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_To_Acct_Data; }

    public DbsGroup getIaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data() { return iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Fund_Cde() { return iaa_Xfr_Audit_View_Iaxfr_To_Fund_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd() { return iaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ() { return iaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_New_Fund_Rec() { return iaa_Xfr_Audit_View_Iaxfr_To_New_Fund_Rec; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_New_Phys_Cntrct_Issue() { return iaa_Xfr_Audit_View_Iaxfr_To_New_Phys_Cntrct_Issue; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Typ() { return iaa_Xfr_Audit_View_Iaxfr_To_Typ; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Qty() { return iaa_Xfr_Audit_View_Iaxfr_To_Qty; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Guar() { return iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Guar; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Divid() { return iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Divid; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Units() { return iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Units; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Reval_Unit_Val() { return iaa_Xfr_Audit_View_Iaxfr_To_Reval_Unit_Val; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Xfr_Units() { return iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Units; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Rate_Cde() { return iaa_Xfr_Audit_View_Iaxfr_To_Rate_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Xfr_Guar() { return iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Guar; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Xfr_Divid() { return iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Divid; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Asset_Amt() { return iaa_Xfr_Audit_View_Iaxfr_To_Asset_Amt; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units() { return iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Guar() { return iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Guar; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Divid() { return iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Divid; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_Nbr() { return iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_Nbr; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_To_Payee_Cde() { return iaa_Xfr_Audit_View_Iaxfr_Calc_To_Payee_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_New_Issue_Ind() { return iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_New_Issue_Ind; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Calc_Unit_Val_Dte() { return iaa_Xfr_Audit_View_Iaxfr_Calc_Unit_Val_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Cycle_Dte() { return iaa_Xfr_Audit_View_Iaxfr_Cycle_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Iaxfr_Acctng_Dte() { return iaa_Xfr_Audit_View_Iaxfr_Acctng_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Lst_Chnge_Dte() { return iaa_Xfr_Audit_View_Lst_Chnge_Dte; }

    public DbsField getIaa_Xfr_Audit_View_Rqst_Xfr_Type() { return iaa_Xfr_Audit_View_Rqst_Xfr_Type; }

    public DbsField getIaa_Xfr_Audit_View_Rqst_Unit_Cde() { return iaa_Xfr_Audit_View_Rqst_Unit_Cde; }

    public DbsField getIaa_Xfr_Audit_View_Ia_New_Iss_Prt_Pull() { return iaa_Xfr_Audit_View_Ia_New_Iss_Prt_Pull; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Xfr_Audit_View = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Audit_View", "IAA-XFR-AUDIT-VIEW"), "IAA_XFR_AUDIT", "IA_TRANSFERS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_AUDIT"));
        iaa_Xfr_Audit_View_Rcrd_Type_Cde = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Xfr_Audit_View_Rqst_Id = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Xfr_Audit_View_Iaxfr_Calc_Unique_Id = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_Unique_Id", "IAXFR-CALC-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIQUE_ID");
        iaa_Xfr_Audit_View_Iaxfr_Effctve_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Effctve_Dte", "IAXFR-EFFCTVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_EFFCTVE_DTE");
        iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Dte", "IAXFR-RQST-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_DTE");
        iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Tme = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Rqst_Rcvd_Tme", "IAXFR-RQST-RCVD-TME", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_TME");
        iaa_Xfr_Audit_View_Iaxfr_Invrse_Effctve_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Invrse_Effctve_Dte", 
            "IAXFR-INVRSE-EFFCTVE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "IAXFR_INVRSE_EFFCTVE_DTE");
        iaa_Xfr_Audit_View_Iaxfr_Invrse_Rcvd_Tme = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Invrse_Rcvd_Tme", "IAXFR-INVRSE-RCVD-TME", 
            FieldType.NUMERIC, 14, RepeatingFieldStrategy.None, "IAXFR_INVRSE_RCVD_TME");
        iaa_Xfr_Audit_View_Iaxfr_Entry_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Entry_Dte", "IAXFR-ENTRY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_ENTRY_DTE");
        iaa_Xfr_Audit_View_Iaxfr_Entry_Tme = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Entry_Tme", "IAXFR-ENTRY-TME", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_ENTRY_TME");
        iaa_Xfr_Audit_View_Iaxfr_Calc_Sttmnt_Indctr = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_Sttmnt_Indctr", 
            "IAXFR-CALC-STTMNT-INDCTR", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_STTMNT_INDCTR");
        iaa_Xfr_Audit_View_Iaxfr_Calc_Status_Cde = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_Status_Cde", "IAXFR-CALC-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_STATUS_CDE");
        iaa_Xfr_Audit_View_Iaxfr_Cwf_Wpid = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Cwf_Wpid", "IAXFR-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IAXFR_CWF_WPID");
        iaa_Xfr_Audit_View_Iaxfr_Cwf_Log_Dte_Time = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Cwf_Log_Dte_Time", "IAXFR-CWF-LOG-DTE-TIME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "IAXFR_CWF_LOG_DTE_TIME");
        iaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr", "IAXFR-FROM-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IAXFR_FROM_PPCN_NBR");
        iaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde", "IAXFR-FROM-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_FROM_PAYEE_CDE");
        iaa_Xfr_Audit_View_Iaxfr_Calc_Reject_Cde = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_Reject_Cde", "IAXFR-CALC-REJECT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_REJECT_CDE");
        iaa_Xfr_Audit_View_Iaxfr_Opn_Clsd_Ind = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Opn_Clsd_Ind", "IAXFR-OPN-CLSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_OPN_CLSD_IND");
        iaa_Xfr_Audit_View_Iaxfr_In_Progress_Ind = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_In_Progress_Ind", "IAXFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "IAXFR_IN_PROGRESS_IND");
        iaa_Xfr_Audit_View_Iaxfr_Retry_Cnt = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Retry_Cnt", "IAXFR-RETRY-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "IAXFR_RETRY_CNT");
        iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_From_Acct_Data", 
            "C*IAXFR-CALC-FROM-ACCT-DATA", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit_View.getRecord().newGroupInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data", 
            "IAXFR-CALC-FROM-ACCT-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Fund_Cde = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Fund_Cde", 
            "IAXFR-FROM-FUND-CDE", FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_FUND_CDE", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_Frm_Acct_Cd = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_Frm_Acct_Cd", 
            "IAXFR-FRM-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_ACCT_CD", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_Frm_Unit_Typ = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_Frm_Unit_Typ", 
            "IAXFR-FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_UNIT_TYP", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Typ = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Typ", "IAXFR-FROM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Qty = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Qty", "IAXFR-FROM-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_QTY", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Guar = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Guar", 
            "IAXFR-FROM-CURRENT-PMT-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Divid = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Divid", 
            "IAXFR-FROM-CURRENT-PMT-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Units = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Units", 
            "IAXFR-FROM-CURRENT-PMT-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Unit_Val = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Current_Pmt_Unit_Val", 
            "IAXFR-FROM-CURRENT-PMT-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Reval_Unit_Val = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Reval_Unit_Val", 
            "IAXFR-FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_REVAL_UNIT_VAL", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Amt = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Amt", 
            "IAXFR-FROM-RQSTD-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_RQSTD_XFR_AMT", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Units = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Units", 
            "IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Guar = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Guar", 
            "IAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Divid = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Divid", 
            "IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Asset_Xfr_Amt = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Asset_Xfr_Amt", 
            "IAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_ASSET_XFR_AMT", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Pmt_Aftr_Xfr = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Pmt_Aftr_Xfr", 
            "IAXFR-FROM-PMT-AFTR-XFR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_PMT_AFTR_XFR", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Units = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Units", 
            "IAXFR-FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_AFTR_XFR_UNITS", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Guar = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Guar", 
            "IAXFR-FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_AFTR_XFR_GUAR", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Divid = iaa_Xfr_Audit_View_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_From_Aftr_Xfr_Divid", 
            "IAXFR-FROM-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Count_Castiaxfr_Calc_To_Acct_Data", 
            "C*IAXFR-CALC-TO-ACCT-DATA", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit_View.getRecord().newGroupInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Fund_Cde = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Fund_Cde", 
            "IAXFR-TO-FUND-CDE", FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_FUND_CDE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ", 
            "IAXFR-TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_UNIT_TYP", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_New_Fund_Rec = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_New_Fund_Rec", 
            "IAXFR-TO-NEW-FUND-REC", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_FUND_REC", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_New_Phys_Cntrct_Issue = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_New_Phys_Cntrct_Issue", 
            "IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_PHYS_CNTRCT_ISSUE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Typ = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Typ", "IAXFR-TO-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Qty = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Qty", "IAXFR-TO-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_QTY", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Guar = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Guar", 
            "IAXFR-TO-BFR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_GUAR", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Divid = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Divid", 
            "IAXFR-TO-BFR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_DIVID", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Units = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Bfr_Xfr_Units", 
            "IAXFR-TO-BFR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_UNITS", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Reval_Unit_Val = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Reval_Unit_Val", 
            "IAXFR-TO-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_REVAL_UNIT_VAL", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Units = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Units", 
            "IAXFR-TO-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_UNITS", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Rate_Cde = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Rate_Cde", 
            "IAXFR-TO-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_RATE_CDE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Guar = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Guar", 
            "IAXFR-TO-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_GUAR", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Divid = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Xfr_Divid", 
            "IAXFR-TO-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_DIVID", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Asset_Amt = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Asset_Amt", 
            "IAXFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ASSET_AMT", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units", 
            "IAXFR-TO-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_UNITS", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Guar = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Guar", 
            "IAXFR-TO-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_GUAR", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Divid = iaa_Xfr_Audit_View_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Divid", 
            "IAXFR-TO-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_DIVID", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_Nbr = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_Nbr", "IAXFR-CALC-TO-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NBR");
        iaa_Xfr_Audit_View_Iaxfr_Calc_To_Payee_Cde = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_To_Payee_Cde", "IAXFR-CALC-TO-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PAYEE_CDE");
        iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_New_Issue_Ind = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_New_Issue_Ind", 
            "IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NEW_ISSUE_IND");
        iaa_Xfr_Audit_View_Iaxfr_Calc_Unit_Val_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Calc_Unit_Val_Dte", "IAXFR-CALC-UNIT-VAL-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIT_VAL_DTE");
        iaa_Xfr_Audit_View_Iaxfr_Cycle_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Cycle_Dte", "IAXFR-CYCLE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CYCLE_DTE");
        iaa_Xfr_Audit_View_Iaxfr_Acctng_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Iaxfr_Acctng_Dte", "IAXFR-ACCTNG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_ACCTNG_DTE");
        iaa_Xfr_Audit_View_Lst_Chnge_Dte = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_CHNGE_DTE");
        iaa_Xfr_Audit_View_Rqst_Xfr_Type = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Xfr_Audit_View_Rqst_Unit_Cde = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Xfr_Audit_View_Ia_New_Iss_Prt_Pull = vw_iaa_Xfr_Audit_View.getRecord().newFieldInGroup("iaa_Xfr_Audit_View_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        vw_iaa_Xfr_Audit_View.setUniquePeList();

        this.setRecordName("LdaIaal204a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Xfr_Audit_View.reset();
    }

    // Constructor
    public LdaIaal204a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
