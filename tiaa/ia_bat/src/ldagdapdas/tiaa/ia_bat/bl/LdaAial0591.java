/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:34 PM
**        * FROM NATURAL LDA     : AIAL0591
************************************************************
**        * FILE NAME            : LdaAial0591.java
**        * CLASS NAME           : LdaAial0591
**        * INSTANCE NAME        : LdaAial0591
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0591 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cpr_Trans_View;
    private DbsField iaa_Cpr_Trans_View_Trans_Dte;
    private DbsField iaa_Cpr_Trans_View_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_View_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_View_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_View_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_View_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_View_Cpr_Bfre_Key;
    private DbsField iaa_Cpr_Trans_View_Cpr_Aftr_Key;

    public DataAccessProgramView getVw_iaa_Cpr_Trans_View() { return vw_iaa_Cpr_Trans_View; }

    public DbsField getIaa_Cpr_Trans_View_Trans_Dte() { return iaa_Cpr_Trans_View_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_View_Invrse_Trans_Dte() { return iaa_Cpr_Trans_View_Invrse_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_View_Lst_Trans_Dte() { return iaa_Cpr_Trans_View_Lst_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_View_Cntrct_Actvty_Cde() { return iaa_Cpr_Trans_View_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cpr_Trans_View_Cntrct_Mode_Ind() { return iaa_Cpr_Trans_View_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte() { return iaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte() { return iaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_View_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_View_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cpr_Trans_View_Bfre_Imge_Id() { return iaa_Cpr_Trans_View_Bfre_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_View_Aftr_Imge_Id() { return iaa_Cpr_Trans_View_Aftr_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_View_Cpr_Bfre_Key() { return iaa_Cpr_Trans_View_Cpr_Bfre_Key; }

    public DbsField getIaa_Cpr_Trans_View_Cpr_Aftr_Key() { return iaa_Cpr_Trans_View_Cpr_Aftr_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cpr_Trans_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans_View", "IAA-CPR-TRANS-VIEW"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        iaa_Cpr_Trans_View_Trans_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cpr_Trans_View_Invrse_Trans_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_View_Lst_Trans_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_View_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_View_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_View_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_View_Bfre_Imge_Id = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cpr_Trans_View_Aftr_Imge_Id = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cpr_Trans_View_Cpr_Bfre_Key = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cpr_Bfre_Key", "CPR-BFRE-KEY", FieldType.BINARY, 
            20, RepeatingFieldStrategy.None, "CPR_BFRE_KEY");
        iaa_Cpr_Trans_View_Cpr_Aftr_Key = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cpr_Aftr_Key", "CPR-AFTR-KEY", FieldType.STRING, 
            25, RepeatingFieldStrategy.None, "CPR_AFTR_KEY");

        this.setRecordName("LdaAial0591");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cpr_Trans_View.reset();
    }

    // Constructor
    public LdaAial0591() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
