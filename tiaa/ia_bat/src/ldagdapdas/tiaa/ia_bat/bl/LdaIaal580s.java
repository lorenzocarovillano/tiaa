/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:01:57 PM
**        * FROM NATURAL LDA     : IAAL580S
************************************************************
**        * FILE NAME            : LdaIaal580s.java
**        * CLASS NAME           : LdaIaal580s
**        * INSTANCE NAME        : LdaIaal580s
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal580s extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_audit_View;
    private DbsField audit_View_Paudit_Ppcn_Nbr;
    private DbsField audit_View_Paudit_Payee_Cde;
    private DbsField audit_View_Paudit_Timestamp;
    private DbsField audit_View_Paudit_Status_Timestamp;
    private DbsField audit_View_Paudit_Pyee_Tax_Id_Nbr;
    private DbsGroup audit_View_Paudit_Pyee_Tax_Id_NbrRedef1;
    private DbsField audit_View_Pnd_Paudit_Pyee_Tax_Id_Nbr_A;
    private DbsField audit_View_Paudit_Ph_Name_First;
    private DbsField audit_View_Paudit_Ph_Name_Middle;
    private DbsField audit_View_Paudit_Ph_Name_Last;
    private DbsField audit_View_Paudit_Dob_Dte;
    private DbsGroup audit_View_Paudit_Dob_DteRedef2;
    private DbsField audit_View_Pnd_Paudit_Dob_Dte_A;
    private DbsField audit_View_Count_Castpaudit_Name_Addrss_Grp;
    private DbsGroup audit_View_Paudit_Name_Addrss_Grp;
    private DbsField audit_View_Paudit_Name;
    private DbsField audit_View_Paudit_Addrss_Lne_1;
    private DbsField audit_View_Paudit_Addrss_Lne_2;
    private DbsField audit_View_Paudit_Addrss_Lne_3;
    private DbsField audit_View_Paudit_Addrss_Lne_4;
    private DbsField audit_View_Paudit_Addrss_Lne_5;
    private DbsField audit_View_Paudit_Addrss_Lne_6;
    private DbsField audit_View_Paudit_Addrss_City;
    private DbsField audit_View_Paudit_Addrss_State;
    private DbsField audit_View_Paudit_Addrss_Zip;
    private DbsField audit_View_Paudit_Type_Req_Ind;
    private DbsField audit_View_Paudit_Type_Ind;
    private DbsField audit_View_Paudit_Roll_Dest_Cde;
    private DbsField audit_View_Paudit_Eft_Acct_Nbr;
    private DbsField audit_View_Paudit_Eft_Transit_Id;
    private DbsGroup audit_View_Paudit_Eft_Transit_IdRedef3;
    private DbsField audit_View_Pnd_Paudit_Eft_Transit_Id_A;
    private DbsField audit_View_Paudit_Chk_Sav_Ind;
    private DbsField audit_View_Paudit_Oper_Id;
    private DbsField audit_View_Paudit_Oper_User_Grp;
    private DbsField audit_View_Paudit_Ph_Name_Prefix;
    private DbsField audit_View_Paudit_Surv_Bene_Sex_Cde;
    private DbsField audit_View_Paudit_New_Acct_Ind;
    private DbsField audit_View_Paudit_Per_Pymnt;
    private DbsField audit_View_Paudit_Per_Dvdnd;
    private DbsField audit_View_Paudit_Instllmnt_Gross;
    private DbsField pnd_Audit_Reads;
    private DbsField pnd_Audit_Selects;
    private DbsField pnd_Work_Record_Writes_1;
    private DbsField pnd_Work_Record_Writes_2;
    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Pyee_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Payee_Code;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Ppcn_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Dob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Acct_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Chk_Sav_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Transit_Id;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_First;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Middle;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Last;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp_Num;
    private DbsGroup pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Name;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_2;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_3;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_4;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_5;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_6;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_City;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_State;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Zip;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Oper_Id;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Oper_User_Grp;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Sex_Code;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Type_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Paudit_Type_Req_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_New_Acct_Id;
    private DbsGroup pnd_Work_Record_2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Dob_Ph_Name_Last;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Payee_Code;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Ppcn_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Pyee_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Dob_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Acct_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Chk_Sav_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Transit_Id;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_First;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Middle;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Last;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp_Num;
    private DbsGroup pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Name;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_1;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_3;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_4;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_5;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_6;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_City;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_State;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Zip;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Oper_Id;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Oper_User_Grp;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Sex_Code;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Type_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Paudit_Type_Req_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_New_Acct_Id;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef4;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Date_NRedef5;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_AlphRedef6;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd;
    private DbsField pnd_Fl_Time_Hhiiss_Alph;
    private DbsGroup pnd_Fl_Time_Hhiiss_AlphRedef8;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num;
    private DbsGroup pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef10;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef12;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsGroup pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;

    public DataAccessProgramView getVw_audit_View() { return vw_audit_View; }

    public DbsField getAudit_View_Paudit_Ppcn_Nbr() { return audit_View_Paudit_Ppcn_Nbr; }

    public DbsField getAudit_View_Paudit_Payee_Cde() { return audit_View_Paudit_Payee_Cde; }

    public DbsField getAudit_View_Paudit_Timestamp() { return audit_View_Paudit_Timestamp; }

    public DbsField getAudit_View_Paudit_Status_Timestamp() { return audit_View_Paudit_Status_Timestamp; }

    public DbsField getAudit_View_Paudit_Pyee_Tax_Id_Nbr() { return audit_View_Paudit_Pyee_Tax_Id_Nbr; }

    public DbsGroup getAudit_View_Paudit_Pyee_Tax_Id_NbrRedef1() { return audit_View_Paudit_Pyee_Tax_Id_NbrRedef1; }

    public DbsField getAudit_View_Pnd_Paudit_Pyee_Tax_Id_Nbr_A() { return audit_View_Pnd_Paudit_Pyee_Tax_Id_Nbr_A; }

    public DbsField getAudit_View_Paudit_Ph_Name_First() { return audit_View_Paudit_Ph_Name_First; }

    public DbsField getAudit_View_Paudit_Ph_Name_Middle() { return audit_View_Paudit_Ph_Name_Middle; }

    public DbsField getAudit_View_Paudit_Ph_Name_Last() { return audit_View_Paudit_Ph_Name_Last; }

    public DbsField getAudit_View_Paudit_Dob_Dte() { return audit_View_Paudit_Dob_Dte; }

    public DbsGroup getAudit_View_Paudit_Dob_DteRedef2() { return audit_View_Paudit_Dob_DteRedef2; }

    public DbsField getAudit_View_Pnd_Paudit_Dob_Dte_A() { return audit_View_Pnd_Paudit_Dob_Dte_A; }

    public DbsField getAudit_View_Count_Castpaudit_Name_Addrss_Grp() { return audit_View_Count_Castpaudit_Name_Addrss_Grp; }

    public DbsGroup getAudit_View_Paudit_Name_Addrss_Grp() { return audit_View_Paudit_Name_Addrss_Grp; }

    public DbsField getAudit_View_Paudit_Name() { return audit_View_Paudit_Name; }

    public DbsField getAudit_View_Paudit_Addrss_Lne_1() { return audit_View_Paudit_Addrss_Lne_1; }

    public DbsField getAudit_View_Paudit_Addrss_Lne_2() { return audit_View_Paudit_Addrss_Lne_2; }

    public DbsField getAudit_View_Paudit_Addrss_Lne_3() { return audit_View_Paudit_Addrss_Lne_3; }

    public DbsField getAudit_View_Paudit_Addrss_Lne_4() { return audit_View_Paudit_Addrss_Lne_4; }

    public DbsField getAudit_View_Paudit_Addrss_Lne_5() { return audit_View_Paudit_Addrss_Lne_5; }

    public DbsField getAudit_View_Paudit_Addrss_Lne_6() { return audit_View_Paudit_Addrss_Lne_6; }

    public DbsField getAudit_View_Paudit_Addrss_City() { return audit_View_Paudit_Addrss_City; }

    public DbsField getAudit_View_Paudit_Addrss_State() { return audit_View_Paudit_Addrss_State; }

    public DbsField getAudit_View_Paudit_Addrss_Zip() { return audit_View_Paudit_Addrss_Zip; }

    public DbsField getAudit_View_Paudit_Type_Req_Ind() { return audit_View_Paudit_Type_Req_Ind; }

    public DbsField getAudit_View_Paudit_Type_Ind() { return audit_View_Paudit_Type_Ind; }

    public DbsField getAudit_View_Paudit_Roll_Dest_Cde() { return audit_View_Paudit_Roll_Dest_Cde; }

    public DbsField getAudit_View_Paudit_Eft_Acct_Nbr() { return audit_View_Paudit_Eft_Acct_Nbr; }

    public DbsField getAudit_View_Paudit_Eft_Transit_Id() { return audit_View_Paudit_Eft_Transit_Id; }

    public DbsGroup getAudit_View_Paudit_Eft_Transit_IdRedef3() { return audit_View_Paudit_Eft_Transit_IdRedef3; }

    public DbsField getAudit_View_Pnd_Paudit_Eft_Transit_Id_A() { return audit_View_Pnd_Paudit_Eft_Transit_Id_A; }

    public DbsField getAudit_View_Paudit_Chk_Sav_Ind() { return audit_View_Paudit_Chk_Sav_Ind; }

    public DbsField getAudit_View_Paudit_Oper_Id() { return audit_View_Paudit_Oper_Id; }

    public DbsField getAudit_View_Paudit_Oper_User_Grp() { return audit_View_Paudit_Oper_User_Grp; }

    public DbsField getAudit_View_Paudit_Ph_Name_Prefix() { return audit_View_Paudit_Ph_Name_Prefix; }

    public DbsField getAudit_View_Paudit_Surv_Bene_Sex_Cde() { return audit_View_Paudit_Surv_Bene_Sex_Cde; }

    public DbsField getAudit_View_Paudit_New_Acct_Ind() { return audit_View_Paudit_New_Acct_Ind; }

    public DbsField getAudit_View_Paudit_Per_Pymnt() { return audit_View_Paudit_Per_Pymnt; }

    public DbsField getAudit_View_Paudit_Per_Dvdnd() { return audit_View_Paudit_Per_Dvdnd; }

    public DbsField getAudit_View_Paudit_Instllmnt_Gross() { return audit_View_Paudit_Instllmnt_Gross; }

    public DbsField getPnd_Audit_Reads() { return pnd_Audit_Reads; }

    public DbsField getPnd_Audit_Selects() { return pnd_Audit_Selects; }

    public DbsField getPnd_Work_Record_Writes_1() { return pnd_Work_Record_Writes_1; }

    public DbsField getPnd_Work_Record_Writes_2() { return pnd_Work_Record_Writes_2; }

    public DbsGroup getPnd_Work_Record_1() { return pnd_Work_Record_1; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Pyee_Tax_Id_Nbr() { return pnd_Work_Record_1_Pnd_W1_Paudit_Pyee_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Payee_Code() { return pnd_Work_Record_1_Pnd_W1_Paudit_Payee_Code; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Ppcn_Nbr() { return pnd_Work_Record_1_Pnd_W1_Paudit_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Dob_Dte() { return pnd_Work_Record_1_Pnd_W1_Paudit_Dob_Dte; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Eft_Acct_Nbr() { return pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Acct_Nbr; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Chk_Sav_Ind() { return pnd_Work_Record_1_Pnd_W1_Paudit_Chk_Sav_Ind; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Eft_Transit_Id() { return pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Transit_Id; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_First() { return pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_First; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Middle() { return pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Middle; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Last() { return pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Last; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp_Num() { return pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp_Num; }

    public DbsGroup getPnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp() { return pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Name() { return pnd_Work_Record_1_Pnd_W1_Paudit_Name; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_1() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_1; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_2() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_2; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_3() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_3; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_4() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_4; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_5() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_5; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_6() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_6; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_City() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_City; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_State() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_State; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Zip() { return pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Zip; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Oper_Id() { return pnd_Work_Record_1_Pnd_W1_Paudit_Oper_Id; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Oper_User_Grp() { return pnd_Work_Record_1_Pnd_W1_Paudit_Oper_User_Grp; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Sex_Code() { return pnd_Work_Record_1_Pnd_W1_Paudit_Sex_Code; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Type_Ind() { return pnd_Work_Record_1_Pnd_W1_Paudit_Type_Ind; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_Paudit_Type_Req_Ind() { return pnd_Work_Record_1_Pnd_W1_Paudit_Type_Req_Ind; }

    public DbsField getPnd_Work_Record_1_Pnd_W1_New_Acct_Id() { return pnd_Work_Record_1_Pnd_W1_New_Acct_Id; }

    public DbsGroup getPnd_Work_Record_2() { return pnd_Work_Record_2; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Dob_Ph_Name_Last() { return pnd_Work_Record_2_Pnd_W2_Dob_Ph_Name_Last; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Payee_Code() { return pnd_Work_Record_2_Pnd_W2_Paudit_Payee_Code; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Ppcn_Nbr() { return pnd_Work_Record_2_Pnd_W2_Paudit_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Pyee_Tax_Id_Nbr() { return pnd_Work_Record_2_Pnd_W2_Paudit_Pyee_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Dob_Dte() { return pnd_Work_Record_2_Pnd_W2_Paudit_Dob_Dte; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Eft_Acct_Nbr() { return pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Acct_Nbr; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Chk_Sav_Ind() { return pnd_Work_Record_2_Pnd_W2_Paudit_Chk_Sav_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Eft_Transit_Id() { return pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Transit_Id; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_First() { return pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_First; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Middle() { return pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Middle; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Last() { return pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Last; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp_Num() { return pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp_Num; }

    public DbsGroup getPnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp() { return pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Name() { return pnd_Work_Record_2_Pnd_W2_Paudit_Name; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_1() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_1; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_2() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_2; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_3() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_3; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_4() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_4; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_5() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_5; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_6() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_6; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_City() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_City; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_State() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_State; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Zip() { return pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Zip; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Oper_Id() { return pnd_Work_Record_2_Pnd_W2_Paudit_Oper_Id; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Oper_User_Grp() { return pnd_Work_Record_2_Pnd_W2_Paudit_Oper_User_Grp; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Sex_Code() { return pnd_Work_Record_2_Pnd_W2_Paudit_Sex_Code; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Type_Ind() { return pnd_Work_Record_2_Pnd_W2_Paudit_Type_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_Paudit_Type_Req_Ind() { return pnd_Work_Record_2_Pnd_W2_Paudit_Type_Req_Ind; }

    public DbsField getPnd_Work_Record_2_Pnd_W2_New_Acct_Id() { return pnd_Work_Record_2_Pnd_W2_New_Acct_Id; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef4() { return iaa_Parm_Card_Pnd_Parm_DateRedef4; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Date_NRedef5() { return iaa_Parm_Card_Pnd_Parm_Date_NRedef5; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph() { return pnd_Fl_Date_Yyyymmdd_Alph; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_AlphRedef6() { return pnd_Fl_Date_Yyyymmdd_AlphRedef6; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph() { return pnd_Fl_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_AlphRedef8() { return pnd_Fl_Time_Hhiiss_AlphRedef8; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef10() { return pnd_Sy_Date_Yymmdd_AlphRedef10; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef12() { return pnd_Sy_Time_Hhiiss_AlphRedef12; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsField getPnd_Sys_Time() { return pnd_Sys_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_audit_View = new DataAccessProgramView(new NameInfo("vw_audit_View", "AUDIT-VIEW"), "IAA_DC_PMT_AUDIT", "IA_DEATH_CLAIMS", DdmPeriodicGroups.getInstance().getGroups("IAA_DC_PMT_AUDIT"));
        audit_View_Paudit_Ppcn_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ppcn_Nbr", "PAUDIT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "PAUDIT_PPCN_NBR");
        audit_View_Paudit_Payee_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Payee_Cde", "PAUDIT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PAUDIT_PAYEE_CDE");
        audit_View_Paudit_Timestamp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Timestamp", "PAUDIT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "PAUDIT_TIMESTAMP");
        audit_View_Paudit_Status_Timestamp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Status_Timestamp", "PAUDIT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PAUDIT_STATUS_TIMESTAMP");
        audit_View_Paudit_Pyee_Tax_Id_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Pyee_Tax_Id_Nbr", "PAUDIT-PYEE-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PAUDIT_PYEE_TAX_ID_NBR");
        audit_View_Paudit_Pyee_Tax_Id_NbrRedef1 = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Pyee_Tax_Id_NbrRedef1", "Redefines", audit_View_Paudit_Pyee_Tax_Id_Nbr);
        audit_View_Pnd_Paudit_Pyee_Tax_Id_Nbr_A = audit_View_Paudit_Pyee_Tax_Id_NbrRedef1.newFieldInGroup("audit_View_Pnd_Paudit_Pyee_Tax_Id_Nbr_A", "#PAUDIT-PYEE-TAX-ID-NBR-A", 
            FieldType.STRING, 9);
        audit_View_Paudit_Ph_Name_First = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ph_Name_First", "PAUDIT-PH-NAME-FIRST", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_FIRST");
        audit_View_Paudit_Ph_Name_Middle = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ph_Name_Middle", "PAUDIT-PH-NAME-MIDDLE", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_MIDDLE");
        audit_View_Paudit_Ph_Name_Last = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ph_Name_Last", "PAUDIT-PH-NAME-LAST", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_LAST");
        audit_View_Paudit_Dob_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Dob_Dte", "PAUDIT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "PAUDIT_DOB_DTE");
        audit_View_Paudit_Dob_DteRedef2 = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Dob_DteRedef2", "Redefines", audit_View_Paudit_Dob_Dte);
        audit_View_Pnd_Paudit_Dob_Dte_A = audit_View_Paudit_Dob_DteRedef2.newFieldInGroup("audit_View_Pnd_Paudit_Dob_Dte_A", "#PAUDIT-DOB-DTE-A", FieldType.STRING, 
            8);
        audit_View_Count_Castpaudit_Name_Addrss_Grp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Count_Castpaudit_Name_Addrss_Grp", "C*PAUDIT-NAME-ADDRSS-GRP", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Name_Addrss_Grp = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Name_Addrss_Grp", "PAUDIT-NAME-ADDRSS-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Name = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Name", "PAUDIT-NAME", FieldType.STRING, 35, 
            new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_NAME", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_Lne_1 = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_Lne_1", "PAUDIT-ADDRSS-LNE-1", 
            FieldType.STRING, 35, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_1", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_Lne_2 = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_Lne_2", "PAUDIT-ADDRSS-LNE-2", 
            FieldType.STRING, 35, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_2", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_Lne_3 = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_Lne_3", "PAUDIT-ADDRSS-LNE-3", 
            FieldType.STRING, 35, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_3", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_Lne_4 = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_Lne_4", "PAUDIT-ADDRSS-LNE-4", 
            FieldType.STRING, 35, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_4", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_Lne_5 = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_Lne_5", "PAUDIT-ADDRSS-LNE-5", 
            FieldType.STRING, 35, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_5", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_Lne_6 = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_Lne_6", "PAUDIT-ADDRSS-LNE-6", 
            FieldType.STRING, 35, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_6", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_City = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_City", "PAUDIT-ADDRSS-CITY", 
            FieldType.STRING, 21, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_CITY", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_State = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_State", "PAUDIT-ADDRSS-STATE", 
            FieldType.STRING, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_STATE", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Addrss_Zip = audit_View_Paudit_Name_Addrss_Grp.newFieldArrayInGroup("audit_View_Paudit_Addrss_Zip", "PAUDIT-ADDRSS-ZIP", FieldType.STRING, 
            9, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_ZIP", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        audit_View_Paudit_Type_Req_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Type_Req_Ind", "PAUDIT-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PAUDIT_TYPE_REQ_IND");
        audit_View_Paudit_Type_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Type_Ind", "PAUDIT-TYPE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PAUDIT_TYPE_IND");
        audit_View_Paudit_Roll_Dest_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Roll_Dest_Cde", "PAUDIT-ROLL-DEST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PAUDIT_ROLL_DEST_CDE");
        audit_View_Paudit_Eft_Acct_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Eft_Acct_Nbr", "PAUDIT-EFT-ACCT-NBR", FieldType.STRING, 
            21, RepeatingFieldStrategy.None, "PAUDIT_EFT_ACCT_NBR");
        audit_View_Paudit_Eft_Transit_Id = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Eft_Transit_Id", "PAUDIT-EFT-TRANSIT-ID", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PAUDIT_EFT_TRANSIT_ID");
        audit_View_Paudit_Eft_Transit_IdRedef3 = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Eft_Transit_IdRedef3", "Redefines", audit_View_Paudit_Eft_Transit_Id);
        audit_View_Pnd_Paudit_Eft_Transit_Id_A = audit_View_Paudit_Eft_Transit_IdRedef3.newFieldInGroup("audit_View_Pnd_Paudit_Eft_Transit_Id_A", "#PAUDIT-EFT-TRANSIT-ID-A", 
            FieldType.STRING, 9);
        audit_View_Paudit_Chk_Sav_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Chk_Sav_Ind", "PAUDIT-CHK-SAV-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_CHK_SAV_IND");
        audit_View_Paudit_Oper_Id = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Oper_Id", "PAUDIT-OPER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OVRPYMNT_OPER_ID");
        audit_View_Paudit_Oper_User_Grp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Oper_User_Grp", "PAUDIT-OPER-USER-GRP", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "OVRPYMNT_OPER_USER_GRP");
        audit_View_Paudit_Ph_Name_Prefix = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ph_Name_Prefix", "PAUDIT-PH-NAME-PREFIX", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_PREFIX");
        audit_View_Paudit_Surv_Bene_Sex_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Surv_Bene_Sex_Cde", "PAUDIT-SURV-BENE-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "PAYMNT_SURV_BENE_SEX_CDE");
        audit_View_Paudit_New_Acct_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_New_Acct_Ind", "PAUDIT-NEW-ACCT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_NEW_ACCT_IND");
        audit_View_Paudit_Per_Pymnt = vw_audit_View.getRecord().newFieldArrayInGroup("audit_View_Paudit_Per_Pymnt", "PAUDIT-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_PYMNT", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Paudit_Per_Dvdnd = vw_audit_View.getRecord().newFieldArrayInGroup("audit_View_Paudit_Per_Dvdnd", "PAUDIT-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_DVDND", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Paudit_Instllmnt_Gross = vw_audit_View.getRecord().newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Gross", "PAUDIT-INSTLLMNT-GROSS", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_GROSS", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");

        pnd_Audit_Reads = newFieldInRecord("pnd_Audit_Reads", "#AUDIT-READS", FieldType.NUMERIC, 9);

        pnd_Audit_Selects = newFieldInRecord("pnd_Audit_Selects", "#AUDIT-SELECTS", FieldType.NUMERIC, 9);

        pnd_Work_Record_Writes_1 = newFieldInRecord("pnd_Work_Record_Writes_1", "#WORK-RECORD-WRITES-1", FieldType.NUMERIC, 9);

        pnd_Work_Record_Writes_2 = newFieldInRecord("pnd_Work_Record_Writes_2", "#WORK-RECORD-WRITES-2", FieldType.NUMERIC, 9);

        pnd_Work_Record_1 = newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_W1_Paudit_Pyee_Tax_Id_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Pyee_Tax_Id_Nbr", "#W1-PAUDIT-PYEE-TAX-ID-NBR", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_Paudit_Payee_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Payee_Code", "#W1-PAUDIT-PAYEE-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Paudit_Ppcn_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Ppcn_Nbr", "#W1-PAUDIT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Paudit_Dob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Dob_Dte", "#W1-PAUDIT-DOB-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Acct_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Acct_Nbr", "#W1-PAUDIT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Work_Record_1_Pnd_W1_Paudit_Chk_Sav_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Chk_Sav_Ind", "#W1-PAUDIT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Transit_Id = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Eft_Transit_Id", "#W1-PAUDIT-EFT-TRANSIT-ID", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_First = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_First", "#W1-PAUDIT-PH-NAME-FIRST", 
            FieldType.STRING, 30);
        pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Middle = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Middle", "#W1-PAUDIT-PH-NAME-MIDDLE", 
            FieldType.STRING, 30);
        pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Last = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Last", "#W1-PAUDIT-PH-NAME-LAST", 
            FieldType.STRING, 30);
        pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp_Num = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp_Num", 
            "#W1-PAUDIT-NAME-ADDRESS-GRP-NUM", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp = pnd_Work_Record_1.newGroupArrayInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp", 
            "#W1-PAUDIT-NAME-ADDRESS-GRP", new DbsArrayController(1,2));
        pnd_Work_Record_1_Pnd_W1_Paudit_Name = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Name", 
            "#W1-PAUDIT-NAME", FieldType.STRING, 35);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_1 = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_1", 
            "#W1-PAUDIT-ADDRSS-LNE-1", FieldType.STRING, 35);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_2 = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_2", 
            "#W1-PAUDIT-ADDRSS-LNE-2", FieldType.STRING, 35);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_3 = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_3", 
            "#W1-PAUDIT-ADDRSS-LNE-3", FieldType.STRING, 35);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_4 = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_4", 
            "#W1-PAUDIT-ADDRSS-LNE-4", FieldType.STRING, 35);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_5 = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_5", 
            "#W1-PAUDIT-ADDRSS-LNE-5", FieldType.STRING, 35);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_6 = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_6", 
            "#W1-PAUDIT-ADDRSS-LNE-6", FieldType.STRING, 35);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_City = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_City", 
            "#W1-PAUDIT-ADDRSS-CITY", FieldType.STRING, 21);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_State = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_State", 
            "#W1-PAUDIT-ADDRSS-STATE", FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Zip = pnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Zip", 
            "#W1-PAUDIT-ADDRSS-ZIP", FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_Paudit_Oper_Id = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Oper_Id", "#W1-PAUDIT-OPER-ID", FieldType.STRING, 
            8);
        pnd_Work_Record_1_Pnd_W1_Paudit_Oper_User_Grp = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Oper_User_Grp", "#W1-PAUDIT-OPER-USER-GRP", 
            FieldType.STRING, 6);
        pnd_Work_Record_1_Pnd_W1_Paudit_Sex_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Sex_Code", "#W1-PAUDIT-SEX-CODE", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Paudit_Type_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Type_Ind", "#W1-PAUDIT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Paudit_Type_Req_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Paudit_Type_Req_Ind", "#W1-PAUDIT-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_New_Acct_Id = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_New_Acct_Id", "#W1-NEW-ACCT-ID", FieldType.STRING, 
            1);

        pnd_Work_Record_2 = newGroupInRecord("pnd_Work_Record_2", "#WORK-RECORD-2");
        pnd_Work_Record_2_Pnd_W2_Dob_Ph_Name_Last = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Dob_Ph_Name_Last", "#W2-DOB-PH-NAME-LAST", 
            FieldType.STRING, 38);
        pnd_Work_Record_2_Pnd_W2_Paudit_Payee_Code = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Payee_Code", "#W2-PAUDIT-PAYEE-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_2_Pnd_W2_Paudit_Ppcn_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Ppcn_Nbr", "#W2-PAUDIT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_2_Pnd_W2_Paudit_Pyee_Tax_Id_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Pyee_Tax_Id_Nbr", "#W2-PAUDIT-PYEE-TAX-ID-NBR", 
            FieldType.STRING, 9);
        pnd_Work_Record_2_Pnd_W2_Paudit_Dob_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Dob_Dte", "#W2-PAUDIT-DOB-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Acct_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Acct_Nbr", "#W2-PAUDIT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Work_Record_2_Pnd_W2_Paudit_Chk_Sav_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Chk_Sav_Ind", "#W2-PAUDIT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Transit_Id = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Eft_Transit_Id", "#W2-PAUDIT-EFT-TRANSIT-ID", 
            FieldType.STRING, 9);
        pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_First = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_First", "#W2-PAUDIT-PH-NAME-FIRST", 
            FieldType.STRING, 30);
        pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Middle = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Middle", "#W2-PAUDIT-PH-NAME-MIDDLE", 
            FieldType.STRING, 30);
        pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Last = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Last", "#W2-PAUDIT-PH-NAME-LAST", 
            FieldType.STRING, 30);
        pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp_Num = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp_Num", 
            "#W2-PAUDIT-NAME-ADDRESS-GRP-NUM", FieldType.NUMERIC, 2);
        pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp = pnd_Work_Record_2.newGroupArrayInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp", 
            "#W2-PAUDIT-NAME-ADDRESS-GRP", new DbsArrayController(1,2));
        pnd_Work_Record_2_Pnd_W2_Paudit_Name = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Name", 
            "#W2-PAUDIT-NAME", FieldType.STRING, 35);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_1 = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_1", 
            "#W2-PAUDIT-ADDRSS-LNE-1", FieldType.STRING, 35);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_2 = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_2", 
            "#W2-PAUDIT-ADDRSS-LNE-2", FieldType.STRING, 35);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_3 = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_3", 
            "#W2-PAUDIT-ADDRSS-LNE-3", FieldType.STRING, 35);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_4 = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_4", 
            "#W2-PAUDIT-ADDRSS-LNE-4", FieldType.STRING, 35);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_5 = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_5", 
            "#W2-PAUDIT-ADDRSS-LNE-5", FieldType.STRING, 35);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_6 = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_6", 
            "#W2-PAUDIT-ADDRSS-LNE-6", FieldType.STRING, 35);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_City = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_City", 
            "#W2-PAUDIT-ADDRSS-CITY", FieldType.STRING, 21);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_State = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_State", 
            "#W2-PAUDIT-ADDRSS-STATE", FieldType.STRING, 2);
        pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Zip = pnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Zip", 
            "#W2-PAUDIT-ADDRSS-ZIP", FieldType.STRING, 9);
        pnd_Work_Record_2_Pnd_W2_Paudit_Oper_Id = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Oper_Id", "#W2-PAUDIT-OPER-ID", FieldType.STRING, 
            8);
        pnd_Work_Record_2_Pnd_W2_Paudit_Oper_User_Grp = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Oper_User_Grp", "#W2-PAUDIT-OPER-USER-GRP", 
            FieldType.STRING, 6);
        pnd_Work_Record_2_Pnd_W2_Paudit_Sex_Code = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Sex_Code", "#W2-PAUDIT-SEX-CODE", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Paudit_Type_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Type_Ind", "#W2-PAUDIT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Paudit_Type_Req_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Paudit_Type_Req_Ind", "#W2-PAUDIT-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_2_Pnd_W2_New_Acct_Id = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_New_Acct_Id", "#W2-NEW-ACCT-ID", FieldType.STRING, 
            1);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef4 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef4", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_Date_NRedef5 = iaa_Parm_Card_Pnd_Parm_DateRedef4.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Date_NRedef5", "Redefines", iaa_Parm_Card_Pnd_Parm_Date_N);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_Date_NRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Fl_Date_Yyyymmdd_Alph = newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Fl_Date_Yyyymmdd_AlphRedef6 = newGroupInRecord("pnd_Fl_Date_Yyyymmdd_AlphRedef6", "Redefines", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7 = pnd_Fl_Date_Yyyymmdd_AlphRedef6.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7", 
            "Redefines", pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef7.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd", 
            "#FL-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Fl_Time_Hhiiss_Alph = newFieldInRecord("pnd_Fl_Time_Hhiiss_Alph", "#FL-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Fl_Time_Hhiiss_AlphRedef8 = newGroupInRecord("pnd_Fl_Time_Hhiiss_AlphRedef8", "Redefines", pnd_Fl_Time_Hhiiss_Alph);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num = pnd_Fl_Time_Hhiiss_AlphRedef8.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num", 
            "#FL-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9 = pnd_Fl_Time_Hhiiss_AlphRedef8.newGroupInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9", 
            "Redefines", pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh", 
            "#FL-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii", 
            "#FL-TIME-II", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef9.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss", 
            "#FL-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef10 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef10", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef10.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11 = pnd_Sy_Date_Yymmdd_AlphRedef10.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef11.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef12 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef12", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef12.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13 = pnd_Sy_Time_Hhiiss_AlphRedef12.newGroupInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13", 
            "Redefines", pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh", 
            "#SY-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii", 
            "#SY-TIME-II", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef13.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss", 
            "#SY-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        pnd_Sys_Time = newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        vw_audit_View.setUniquePeList();

        this.setRecordName("LdaIaal580s");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_audit_View.reset();
    }

    // Constructor
    public LdaIaal580s() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
