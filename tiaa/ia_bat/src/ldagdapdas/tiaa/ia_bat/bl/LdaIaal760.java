/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:32 PM
**        * FROM NATURAL LDA     : IAAL760
************************************************************
**        * FILE NAME            : LdaIaal760.java
**        * CLASS NAME           : LdaIaal760
**        * INSTANCE NAME        : LdaIaal760
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal760 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_In_Opt_Out;
    private DbsField pnd_In_Opt_Out_Pnd_Orchstrtn_Id;
    private DbsField pnd_In_Opt_Out_Pnd_Process_Code;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Payee_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Optn_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Orgn_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Issue_Dte;
    private DbsGroup pnd_In_Opt_Out_Pnd_Issue_DteRedef1;
    private DbsField pnd_In_Opt_Out_Pnd_Issue_Dte_N6;
    private DbsField pnd_In_Opt_Out_Pnd_Issue_Dte_Dd;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte;
    private DbsGroup pnd_In_Opt_Out_Pnd_1st_Pmt_Due_DteRedef2;
    private DbsField pnd_In_Opt_Out_Pnd_1s_Pmt_Due_N6;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dd;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte;
    private DbsGroup pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_DteRedef3;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_N6;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dd;
    private DbsField pnd_In_Opt_Out_Pnd_Crrncy_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Issue_Stte;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Annt_Xref;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Annt_Dob;
    private DbsGroup pnd_In_Opt_Out_Pnd_1st_Annt_DobRedef4;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Annt_Yyyy;
    private DbsField pnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_2nd_Annt_Xref;
    private DbsField pnd_In_Opt_Out_Pnd_2nd_Annt_Dob;
    private DbsGroup pnd_In_Opt_Out_Pnd_2nd_Annt_DobRedef5;
    private DbsField pnd_In_Opt_Out_Pnd_2nd_Annt_Yyyy;
    private DbsField pnd_In_Opt_Out_Pnd_2nd_Annt_Sex_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_2nd_Annt_Ssn;
    private DbsField pnd_In_Opt_Out_Pnd_Finl_Prem_Dtes;
    private DbsField pnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Annty_Strt_Dte;
    private DbsField pnd_In_Opt_Out_Pnd_Roth_1st_Cntrb_Dte;
    private DbsField pnd_In_Opt_Out_Pnd_Pln_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Sub_Pln_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Tax_Exempt;
    private DbsField pnd_In_Opt_Out_Pnd_Frst_Annt_Frst_Nme;
    private DbsField pnd_In_Opt_Out_Pnd_Frst_Annt_Mddle_Nme;
    private DbsField pnd_In_Opt_Out_Pnd_Frst_Annt_Lst_Nme;
    private DbsField pnd_In_Opt_Out_Pnd_To_Lob;
    private DbsField pnd_In_Opt_Out_Pnd_To_Sub_Lob;
    private DbsField pnd_In_Opt_Out_Pnd_Sttlmnt_Ind;
    private DbsField pnd_In_Opt_Out_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind;
    private DbsField pnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte;
    private DbsField pnd_In_Opt_Out_Pnd_Finl_Pymnt_Dte;
    private DbsField pnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Emplymnt_Term_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Rcvry_Type_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Rsdl_Ivc_Amt;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Amt;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Pct;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Hold_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Cntrct_Dest_Cde;
    private DbsField pnd_In_Opt_Out_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_In_Opt_Out_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_In_Opt_Out_Pnd_Variable_Funds;

    public DbsGroup getPnd_In_Opt_Out() { return pnd_In_Opt_Out; }

    public DbsField getPnd_In_Opt_Out_Pnd_Orchstrtn_Id() { return pnd_In_Opt_Out_Pnd_Orchstrtn_Id; }

    public DbsField getPnd_In_Opt_Out_Pnd_Process_Code() { return pnd_In_Opt_Out_Pnd_Process_Code; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Nbr() { return pnd_In_Opt_Out_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Payee_Cde() { return pnd_In_Opt_Out_Pnd_Payee_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Optn_Cde() { return pnd_In_Opt_Out_Pnd_Optn_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Orgn_Cde() { return pnd_In_Opt_Out_Pnd_Orgn_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Issue_Dte() { return pnd_In_Opt_Out_Pnd_Issue_Dte; }

    public DbsGroup getPnd_In_Opt_Out_Pnd_Issue_DteRedef1() { return pnd_In_Opt_Out_Pnd_Issue_DteRedef1; }

    public DbsField getPnd_In_Opt_Out_Pnd_Issue_Dte_N6() { return pnd_In_Opt_Out_Pnd_Issue_Dte_N6; }

    public DbsField getPnd_In_Opt_Out_Pnd_Issue_Dte_Dd() { return pnd_In_Opt_Out_Pnd_Issue_Dte_Dd; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte() { return pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte; }

    public DbsGroup getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_DteRedef2() { return pnd_In_Opt_Out_Pnd_1st_Pmt_Due_DteRedef2; }

    public DbsField getPnd_In_Opt_Out_Pnd_1s_Pmt_Due_N6() { return pnd_In_Opt_Out_Pnd_1s_Pmt_Due_N6; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dd() { return pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dd; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte() { return pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte; }

    public DbsGroup getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_DteRedef3() { return pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_DteRedef3; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_N6() { return pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_N6; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dd() { return pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dd; }

    public DbsField getPnd_In_Opt_Out_Pnd_Crrncy_Cde() { return pnd_In_Opt_Out_Pnd_Crrncy_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr() { return pnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr() { return pnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Issue_Stte() { return pnd_In_Opt_Out_Pnd_Issue_Stte; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Annt_Xref() { return pnd_In_Opt_Out_Pnd_1st_Annt_Xref; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Annt_Dob() { return pnd_In_Opt_Out_Pnd_1st_Annt_Dob; }

    public DbsGroup getPnd_In_Opt_Out_Pnd_1st_Annt_DobRedef4() { return pnd_In_Opt_Out_Pnd_1st_Annt_DobRedef4; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Annt_Yyyy() { return pnd_In_Opt_Out_Pnd_1st_Annt_Yyyy; }

    public DbsField getPnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde() { return pnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_2nd_Annt_Xref() { return pnd_In_Opt_Out_Pnd_2nd_Annt_Xref; }

    public DbsField getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob() { return pnd_In_Opt_Out_Pnd_2nd_Annt_Dob; }

    public DbsGroup getPnd_In_Opt_Out_Pnd_2nd_Annt_DobRedef5() { return pnd_In_Opt_Out_Pnd_2nd_Annt_DobRedef5; }

    public DbsField getPnd_In_Opt_Out_Pnd_2nd_Annt_Yyyy() { return pnd_In_Opt_Out_Pnd_2nd_Annt_Yyyy; }

    public DbsField getPnd_In_Opt_Out_Pnd_2nd_Annt_Sex_Cde() { return pnd_In_Opt_Out_Pnd_2nd_Annt_Sex_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_2nd_Annt_Ssn() { return pnd_In_Opt_Out_Pnd_2nd_Annt_Ssn; }

    public DbsField getPnd_In_Opt_Out_Pnd_Finl_Prem_Dtes() { return pnd_In_Opt_Out_Pnd_Finl_Prem_Dtes; }

    public DbsField getPnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr() { return pnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Annty_Strt_Dte() { return pnd_In_Opt_Out_Pnd_Annty_Strt_Dte; }

    public DbsField getPnd_In_Opt_Out_Pnd_Roth_1st_Cntrb_Dte() { return pnd_In_Opt_Out_Pnd_Roth_1st_Cntrb_Dte; }

    public DbsField getPnd_In_Opt_Out_Pnd_Pln_Nbr() { return pnd_In_Opt_Out_Pnd_Pln_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Sub_Pln_Nbr() { return pnd_In_Opt_Out_Pnd_Sub_Pln_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Tax_Exempt() { return pnd_In_Opt_Out_Pnd_Tax_Exempt; }

    public DbsField getPnd_In_Opt_Out_Pnd_Frst_Annt_Frst_Nme() { return pnd_In_Opt_Out_Pnd_Frst_Annt_Frst_Nme; }

    public DbsField getPnd_In_Opt_Out_Pnd_Frst_Annt_Mddle_Nme() { return pnd_In_Opt_Out_Pnd_Frst_Annt_Mddle_Nme; }

    public DbsField getPnd_In_Opt_Out_Pnd_Frst_Annt_Lst_Nme() { return pnd_In_Opt_Out_Pnd_Frst_Annt_Lst_Nme; }

    public DbsField getPnd_In_Opt_Out_Pnd_To_Lob() { return pnd_In_Opt_Out_Pnd_To_Lob; }

    public DbsField getPnd_In_Opt_Out_Pnd_To_Sub_Lob() { return pnd_In_Opt_Out_Pnd_To_Sub_Lob; }

    public DbsField getPnd_In_Opt_Out_Pnd_Sttlmnt_Ind() { return pnd_In_Opt_Out_Pnd_Sttlmnt_Ind; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cpr_Id_Nbr() { return pnd_In_Opt_Out_Pnd_Cpr_Id_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind() { return pnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind; }

    public DbsField getPnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte() { return pnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte; }

    public DbsField getPnd_In_Opt_Out_Pnd_Finl_Pymnt_Dte() { return pnd_In_Opt_Out_Pnd_Finl_Pymnt_Dte; }

    public DbsField getPnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde() { return pnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr() { return pnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde() { return pnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Emplymnt_Term_Cde() { return pnd_In_Opt_Out_Pnd_Cntrct_Emplymnt_Term_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde() { return pnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Rcvry_Type_Cde() { return pnd_In_Opt_Out_Pnd_Rcvry_Type_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt() { return pnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Rsdl_Ivc_Amt() { return pnd_In_Opt_Out_Pnd_Cntrct_Rsdl_Ivc_Amt; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt() { return pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Used_Amt() { return pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Used_Amt; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Rtb_Amt() { return pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Amt; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Rtb_Pct() { return pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Pct; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde() { return pnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte() { return pnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Hold_Cde() { return pnd_In_Opt_Out_Pnd_Cntrct_Hold_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Cntrct_Dest_Cde() { return pnd_In_Opt_Out_Pnd_Cntrct_Dest_Cde; }

    public DbsField getPnd_In_Opt_Out_Pnd_Rllvr_Cntrct_Nbr() { return pnd_In_Opt_Out_Pnd_Rllvr_Cntrct_Nbr; }

    public DbsField getPnd_In_Opt_Out_Pnd_Roth_Dsblty_Dte() { return pnd_In_Opt_Out_Pnd_Roth_Dsblty_Dte; }

    public DbsField getPnd_In_Opt_Out_Pnd_Variable_Funds() { return pnd_In_Opt_Out_Pnd_Variable_Funds; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_In_Opt_Out = newGroupInRecord("pnd_In_Opt_Out", "#IN-OPT-OUT");
        pnd_In_Opt_Out_Pnd_Orchstrtn_Id = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Orchstrtn_Id", "#ORCHSTRTN-ID", FieldType.STRING, 9);
        pnd_In_Opt_Out_Pnd_Process_Code = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Process_Code", "#PROCESS-CODE", FieldType.STRING, 2);
        pnd_In_Opt_Out_Pnd_Cntrct_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_In_Opt_Out_Pnd_Payee_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);
        pnd_In_Opt_Out_Pnd_Optn_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.STRING, 2);
        pnd_In_Opt_Out_Pnd_Orgn_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_In_Opt_Out_Pnd_Issue_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.STRING, 8);
        pnd_In_Opt_Out_Pnd_Issue_DteRedef1 = pnd_In_Opt_Out.newGroupInGroup("pnd_In_Opt_Out_Pnd_Issue_DteRedef1", "Redefines", pnd_In_Opt_Out_Pnd_Issue_Dte);
        pnd_In_Opt_Out_Pnd_Issue_Dte_N6 = pnd_In_Opt_Out_Pnd_Issue_DteRedef1.newFieldInGroup("pnd_In_Opt_Out_Pnd_Issue_Dte_N6", "#ISSUE-DTE-N6", FieldType.NUMERIC, 
            6);
        pnd_In_Opt_Out_Pnd_Issue_Dte_Dd = pnd_In_Opt_Out_Pnd_Issue_DteRedef1.newFieldInGroup("pnd_In_Opt_Out_Pnd_Issue_Dte_Dd", "#ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte", "#1ST-PMT-DUE-DTE", FieldType.STRING, 
            8);
        pnd_In_Opt_Out_Pnd_1st_Pmt_Due_DteRedef2 = pnd_In_Opt_Out.newGroupInGroup("pnd_In_Opt_Out_Pnd_1st_Pmt_Due_DteRedef2", "Redefines", pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte);
        pnd_In_Opt_Out_Pnd_1s_Pmt_Due_N6 = pnd_In_Opt_Out_Pnd_1st_Pmt_Due_DteRedef2.newFieldInGroup("pnd_In_Opt_Out_Pnd_1s_Pmt_Due_N6", "#1S-PMT-DUE-N6", 
            FieldType.NUMERIC, 6);
        pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dd = pnd_In_Opt_Out_Pnd_1st_Pmt_Due_DteRedef2.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dd", "#1ST-PMT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte", "#1ST-PMT-PD-DTE", FieldType.STRING, 8);
        pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_DteRedef3 = pnd_In_Opt_Out.newGroupInGroup("pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_DteRedef3", "Redefines", pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte);
        pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_N6 = pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_DteRedef3.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_N6", "#1ST-PMT-PD-N6", 
            FieldType.NUMERIC, 6);
        pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dd = pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_DteRedef3.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dd", "#1ST-PMT-PD-DD", 
            FieldType.NUMERIC, 2);
        pnd_In_Opt_Out_Pnd_Crrncy_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.STRING, 1);
        pnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr", "#ORIG-TIAA-DA-NBR", FieldType.STRING, 
            8);
        pnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr", "#ORIG-CREF-DA-NBR", FieldType.STRING, 
            8);
        pnd_In_Opt_Out_Pnd_Issue_Stte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Issue_Stte", "#ISSUE-STTE", FieldType.STRING, 3);
        pnd_In_Opt_Out_Pnd_1st_Annt_Xref = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Annt_Xref", "#1ST-ANNT-XREF", FieldType.STRING, 9);
        pnd_In_Opt_Out_Pnd_1st_Annt_Dob = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Annt_Dob", "#1ST-ANNT-DOB", FieldType.STRING, 8);
        pnd_In_Opt_Out_Pnd_1st_Annt_DobRedef4 = pnd_In_Opt_Out.newGroupInGroup("pnd_In_Opt_Out_Pnd_1st_Annt_DobRedef4", "Redefines", pnd_In_Opt_Out_Pnd_1st_Annt_Dob);
        pnd_In_Opt_Out_Pnd_1st_Annt_Yyyy = pnd_In_Opt_Out_Pnd_1st_Annt_DobRedef4.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Annt_Yyyy", "#1ST-ANNT-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde", "#1ST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_In_Opt_Out_Pnd_2nd_Annt_Xref = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_2nd_Annt_Xref", "#2ND-ANNT-XREF", FieldType.STRING, 9);
        pnd_In_Opt_Out_Pnd_2nd_Annt_Dob = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_2nd_Annt_Dob", "#2ND-ANNT-DOB", FieldType.STRING, 8);
        pnd_In_Opt_Out_Pnd_2nd_Annt_DobRedef5 = pnd_In_Opt_Out.newGroupInGroup("pnd_In_Opt_Out_Pnd_2nd_Annt_DobRedef5", "Redefines", pnd_In_Opt_Out_Pnd_2nd_Annt_Dob);
        pnd_In_Opt_Out_Pnd_2nd_Annt_Yyyy = pnd_In_Opt_Out_Pnd_2nd_Annt_DobRedef5.newFieldInGroup("pnd_In_Opt_Out_Pnd_2nd_Annt_Yyyy", "#2ND-ANNT-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_In_Opt_Out_Pnd_2nd_Annt_Sex_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_2nd_Annt_Sex_Cde", "#2ND-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_In_Opt_Out_Pnd_2nd_Annt_Ssn = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_2nd_Annt_Ssn", "#2ND-ANNT-SSN", FieldType.STRING, 9);
        pnd_In_Opt_Out_Pnd_Finl_Prem_Dtes = pnd_In_Opt_Out.newFieldArrayInGroup("pnd_In_Opt_Out_Pnd_Finl_Prem_Dtes", "#FINL-PREM-DTES", FieldType.STRING, 
            8, new DbsArrayController(1,2));
        pnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr", "#MTCHNG-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_In_Opt_Out_Pnd_Annty_Strt_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Annty_Strt_Dte", "#ANNTY-STRT-DTE", FieldType.STRING, 8);
        pnd_In_Opt_Out_Pnd_Roth_1st_Cntrb_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Roth_1st_Cntrb_Dte", "#ROTH-1ST-CNTRB-DTE", FieldType.STRING, 
            8);
        pnd_In_Opt_Out_Pnd_Pln_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Pln_Nbr", "#PLN-NBR", FieldType.STRING, 6);
        pnd_In_Opt_Out_Pnd_Sub_Pln_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Sub_Pln_Nbr", "#SUB-PLN-NBR", FieldType.STRING, 6);
        pnd_In_Opt_Out_Pnd_Tax_Exempt = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Tax_Exempt", "#TAX-EXEMPT", FieldType.STRING, 10);
        pnd_In_Opt_Out_Pnd_Frst_Annt_Frst_Nme = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Frst_Annt_Frst_Nme", "#FRST-ANNT-FRST-NME", FieldType.STRING, 
            35);
        pnd_In_Opt_Out_Pnd_Frst_Annt_Mddle_Nme = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Frst_Annt_Mddle_Nme", "#FRST-ANNT-MDDLE-NME", FieldType.STRING, 
            35);
        pnd_In_Opt_Out_Pnd_Frst_Annt_Lst_Nme = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Frst_Annt_Lst_Nme", "#FRST-ANNT-LST-NME", FieldType.STRING, 
            35);
        pnd_In_Opt_Out_Pnd_To_Lob = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_To_Lob", "#TO-LOB", FieldType.STRING, 10);
        pnd_In_Opt_Out_Pnd_To_Sub_Lob = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_To_Sub_Lob", "#TO-SUB-LOB", FieldType.STRING, 12);
        pnd_In_Opt_Out_Pnd_Sttlmnt_Ind = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Sttlmnt_Ind", "#STTLMNT-IND", FieldType.STRING, 1);
        pnd_In_Opt_Out_Pnd_Cpr_Id_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.STRING, 12);
        pnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind", "#CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte", "#FINL-PER-PAY-DTE", FieldType.STRING, 
            6);
        pnd_In_Opt_Out_Pnd_Finl_Pymnt_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Finl_Pymnt_Dte", "#FINL-PYMNT-DTE", FieldType.STRING, 8);
        pnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde", "#PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3);
        pnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr", "#PRTCPNT-TAX-ID-NBR", FieldType.STRING, 
            9);
        pnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde", "#PRTCPNT-CTZNSHP-CDE", FieldType.STRING, 
            1);
        pnd_In_Opt_Out_Pnd_Cntrct_Emplymnt_Term_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Emplymnt_Term_Cde", "#CNTRCT-EMPLYMNT-TERM-CDE", 
            FieldType.STRING, 1);
        pnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde", "#CNTRCT-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_In_Opt_Out_Pnd_Rcvry_Type_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Rcvry_Type_Cde", "#RCVRY-TYPE-CDE", FieldType.STRING, 1);
        pnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_In_Opt_Out_Pnd_Cntrct_Rsdl_Ivc_Amt = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Rsdl_Ivc_Amt", "#CNTRCT-RSDL-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Used_Amt = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Amt = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Amt", "#CNTRCT-RTB-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Pct = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Rtb_Pct", "#CNTRCT-RTB-PCT", FieldType.DECIMAL, 
            7,4);
        pnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde", "#CNTRCT-PEND-CDE", FieldType.STRING, 
            1);
        pnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte", "#CNTRCT-PEND-DTE", FieldType.STRING, 
            8);
        pnd_In_Opt_Out_Pnd_Cntrct_Hold_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Hold_Cde", "#CNTRCT-HOLD-CDE", FieldType.STRING, 
            1);
        pnd_In_Opt_Out_Pnd_Cntrct_Dest_Cde = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Cntrct_Dest_Cde", "#CNTRCT-DEST-CDE", FieldType.STRING, 
            4);
        pnd_In_Opt_Out_Pnd_Rllvr_Cntrct_Nbr = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_In_Opt_Out_Pnd_Roth_Dsblty_Dte = pnd_In_Opt_Out.newFieldInGroup("pnd_In_Opt_Out_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.STRING, 
            8);
        pnd_In_Opt_Out_Pnd_Variable_Funds = pnd_In_Opt_Out.newFieldArrayInGroup("pnd_In_Opt_Out_Pnd_Variable_Funds", "#VARIABLE-FUNDS", FieldType.STRING, 
            80, new DbsArrayController(1,20));

        this.setRecordName("LdaIaal760");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal760() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
