/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:26 PM
**        * FROM NATURAL LDA     : IAALIMRA
************************************************************
**        * FILE NAME            : LdaIaalimra.java
**        * CLASS NAME           : LdaIaalimra
**        * INSTANCE NAME        : LdaIaalimra
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaalimra extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Xml_Id;
    private DbsField pnd_Output_Pnd_Pmry_Obj_Id;
    private DbsField pnd_Output_Pnd_Trans_Ref_Guid;
    private DbsField pnd_Output_Pnd_Trans_Type;
    private DbsField pnd_Output_Pnd_Trans_Subtype;
    private DbsField pnd_Output_Pnd_Trans_Exe_Dte;
    private DbsGroup pnd_Output_Pnd_Trans_Exe_DteRedef1;
    private DbsField pnd_Output_Pnd_Trans_Start_Dte;
    private DbsField pnd_Output_Pnd_Trans_End_Dte;
    private DbsField pnd_Output_Pnd_Trans_Exe_Tme;
    private DbsField pnd_Output_Pnd_Trans_Mode;
    private DbsField pnd_Output_Pnd_Inq_Level;
    private DbsField pnd_Output_Pnd_Pend_Response;
    private DbsField pnd_Output_Pnd_No_Response;
    private DbsField pnd_Output_Pnd_Test_Ind;
    private DbsField pnd_Output_Pnd_Primary_Object_Typ;
    private DbsField pnd_Output_Pnd_Creat_Dte;
    private DbsField pnd_Output_Pnd_Creat_Tme;
    private DbsField pnd_Output_Pnd_Source_Info_Nme;
    private DbsField pnd_Output_Pnd_Source_Info_Desc;
    private DbsField pnd_Output_Pnd_Source_Info_Comment;
    private DbsField pnd_Output_Pnd_File_Control_Id;
    private DbsField pnd_Output_Pnd_Holding_Id;
    private DbsField pnd_Output_Pnd_Holding_Type_Cde;
    private DbsField pnd_Output_Pnd_Holding_Stat;
    private DbsField pnd_Output_Pnd_As_Of_Dte;
    private DbsField pnd_Output_Pnd_Share_Class;
    private DbsField pnd_Output_Pnd_Policy_Nbr;
    private DbsField pnd_Output_Pnd_Cert_Nbr;
    private DbsField pnd_Output_Pnd_Lob;
    private DbsField pnd_Output_Pnd_Product_Type;
    private DbsField pnd_Output_Pnd_Product_Cde;
    private DbsField pnd_Output_Pnd_Carrier_Cde;
    private DbsField pnd_Output_Pnd_Plan_Name;
    private DbsField pnd_Output_Pnd_Policy_Stat;
    private DbsField pnd_Output_Pnd_Issue_Nation;
    private DbsField pnd_Output_Pnd_Issue_Type;
    private DbsField pnd_Output_Pnd_Jurisdiction;
    private DbsField pnd_Output_Pnd_Replacement_Type;
    private DbsField pnd_Output_Pnd_Policy_Value;
    private DbsField pnd_Output_Pnd_Eff_Date;
    private DbsField pnd_Output_Pnd_Out_Issue_Dte;
    private DbsField pnd_Output_Pnd_Status_Change_Dte;
    private DbsField pnd_Output_Pnd_Prior_Policy_Nbr;
    private DbsField pnd_Output_Pnd_Payout_Type;
    private DbsField pnd_Output_Pnd_Qual_Plan_Type;
    private DbsField pnd_Output_Pnd_Qual_Plan_Sub_Type;
    private DbsField pnd_Output_Pnd_Cum_Wthdrwl_Amtitd;
    private DbsField pnd_Output_Pnd_Death_Benefit_Amt;
    private DbsField pnd_Output_Pnd_Surrender_Value;
    private DbsField pnd_Output_Pnd_Guarint_Rate;
    private DbsField pnd_Output_Pnd_Gross_Prem_Amtitd;
    private DbsField pnd_Output_Pnd_Cum_Prem_Amt_Yr1;
    private DbsField pnd_Output_Pnd_Payout_Id;
    private DbsField pnd_Output_Pnd_Income_Option;
    private DbsField pnd_Output_Pnd_Payout_Amt;
    private DbsField pnd_Output_Pnd_Payout_Mode;
    private DbsField pnd_Output_Pnd_Start_Date;
    private DbsField pnd_Output_Pnd_Annuity_Start_Dte;
    private DbsField pnd_Output_Pnd_Lives_Type;
    private DbsField pnd_Output_Pnd_Annual_Indx_Type;
    private DbsField pnd_Output_Pnd_Period_Certain_Mode;
    private DbsField pnd_Output_Pnd_Period_Certain_Periods;
    private DbsField pnd_Output_Pnd_Payout_Change_Id;
    private DbsField pnd_Output_Pnd_Change_Mode;
    private DbsField pnd_Output_Pnd_Payout_Change_Amt;
    private DbsField pnd_Output_Pnd_Amt_Qualifier;
    private DbsField pnd_Output_Pnd_Payout_Pct_Type;
    private DbsField pnd_Output_Pnd_Description;
    private DbsField pnd_Output_Pnd_Out_Start_Dte;
    private DbsField pnd_Output_Pnd_Out_End_Dte;
    private DbsField pnd_Output_Pnd_Next_Activity_Dte;
    private DbsField pnd_Output_Pnd_Timing_Basis;
    private DbsField pnd_Output_Pnd_Payout_Pct;
    private DbsField pnd_Output_Pnd_Rider_Id;
    private DbsField pnd_Output_Pnd_Rider_Type_Cde;
    private DbsField pnd_Output_Pnd_Rider_Type_Sub_Cde;
    private DbsField pnd_Output_Pnd_Rider_Cde;
    private DbsField pnd_Output_Pnd_Eff_Dte;
    private DbsField pnd_Output_Pnd_Term_Date;
    private DbsField pnd_Output_Pnd_Tot_Amt;
    private DbsField pnd_Output_Pnd_Tot_Amt_Last_Ann;
    private DbsField pnd_Output_Pnd_Lives_Typ;
    private DbsField pnd_Output_Pnd_Benefit_Pct;
    private DbsField pnd_Output_Pnd_Event_Type;
    private DbsField pnd_Output_Pnd_Event_Year;
    private DbsField pnd_Output_Pnd_Rider_Free_Amt;
    private DbsField pnd_Output_Pnd_Financial_Act_Id;
    private DbsField pnd_Output_Pnd_Applies_To_Overage_Id;
    private DbsField pnd_Output_Pnd_Finact_Type;
    private DbsField pnd_Output_Pnd_Fin_Act_Sub_Type;
    private DbsField pnd_Output_Pnd_Reversal_Ind;
    private DbsField pnd_Output_Pnd_Fin_Act_Gross_Amt;
    private DbsField pnd_Output_Pnd_Acc_Value_Last_Ann;
    private DbsField pnd_Output_Pnd_Sub_Acct_Id;
    private DbsField pnd_Output_Pnd_Asset_Class;
    private DbsField pnd_Output_Pnd_Tot_Value;
    private DbsField pnd_Output_Pnd_Base_Rate;
    private DbsField pnd_Output_Pnd_Dur_Qualifier;
    private DbsField pnd_Output_Pnd_Guarantee_Duration;
    private DbsField pnd_Output_Pnd_Cap_Rate;
    private DbsField pnd_Output_Pnd_Part_Rate;
    private DbsField pnd_Output_Pnd_Market_Val_Adjust_Ind;
    private DbsField pnd_Output_Pnd_Bonus_Rate;
    private DbsField pnd_Output_Pnd_Acct_Value_Last_Ann;
    private DbsField pnd_Output_Pnd_Financial_Actid;
    private DbsField pnd_Output_Pnd_Applies_To_Coverage_Id;
    private DbsField pnd_Output_Pnd_Fin_Act_Type;
    private DbsField pnd_Output_Pnd_Fin_Act_Subtype;
    private DbsField pnd_Output_Pnd_Rev_Ind;
    private DbsField pnd_Output_Pnd_Fin_Activity_Gross_Amt;
    private DbsField pnd_Output_Pnd_Arrangement_Id;
    private DbsField pnd_Output_Pnd_Arr_Type;
    private DbsField pnd_Output_Pnd_Modal_Amt;
    private DbsField pnd_Output_Pnd_Party_Id1;
    private DbsField pnd_Output_Pnd_Party_Type_Cde1;
    private DbsField pnd_Output_Pnd_Party_Key1;
    private DbsField pnd_Output_Pnd_Res_State1;
    private DbsField pnd_Output_Pnd_Res_Country1;
    private DbsField pnd_Output_Pnd_Res_Zip1;
    private DbsField pnd_Output_Pnd_Nipr1;
    private DbsField pnd_Output_Pnd_Co_Prod_Id1;
    private DbsField pnd_Output_Pnd_Carrier_Appt_Stat1;
    private DbsField pnd_Output_Pnd_Dist_Channel1;
    private DbsField pnd_Output_Pnd_Dist_Channel_Sub_Type1;
    private DbsField pnd_Output_Pnd_Dist_Channel_Name1;
    private DbsField pnd_Output_Pnd_Dist_Channel_Cde1;
    private DbsField pnd_Output_Pnd_Party_Id2;
    private DbsField pnd_Output_Pnd_Party_Type_Cde2;
    private DbsField pnd_Output_Pnd_Party_Key2;
    private DbsField pnd_Output_Pnd_Res_State2;
    private DbsField pnd_Output_Pnd_Res_Country2;
    private DbsField pnd_Output_Pnd_Res_Zip2;
    private DbsField pnd_Output_Pnd_Nipr2;
    private DbsField pnd_Output_Pnd_Co_Prod_Id2;
    private DbsField pnd_Output_Pnd_Carrier_Appt_Stat2;
    private DbsField pnd_Output_Pnd_Dist_Channel2;
    private DbsField pnd_Output_Pnd_Dist_Channel_Sub_Type2;
    private DbsField pnd_Output_Pnd_Dist_Channel_Name2;
    private DbsField pnd_Output_Pnd_Dist_Channel_Cde2;
    private DbsField pnd_Output_Pnd_Party_Id3;
    private DbsField pnd_Output_Pnd_Party_Type_Cde3;
    private DbsField pnd_Output_Pnd_Party_Key3;
    private DbsField pnd_Output_Pnd_Res_State3;
    private DbsField pnd_Output_Pnd_Res_Country3;
    private DbsField pnd_Output_Pnd_Res_Zip3;
    private DbsField pnd_Output_Pnd_Party_Id4;
    private DbsField pnd_Output_Pnd_Party_Type_Cde4;
    private DbsField pnd_Output_Pnd_Party_Key4;
    private DbsField pnd_Output_Pnd_Res_State4;
    private DbsField pnd_Output_Pnd_Res_Country4;
    private DbsField pnd_Output_Pnd_Res_Zip4;
    private DbsField pnd_Output_Pnd_Party_Id5;
    private DbsField pnd_Output_Pnd_Party_Type_Cde5;
    private DbsField pnd_Output_Pnd_Party_Key5;
    private DbsField pnd_Output_Pnd_Res_State5;
    private DbsField pnd_Output_Pnd_Res_Country5;
    private DbsField pnd_Output_Pnd_Res_Zip5;
    private DbsField pnd_Output_Pnd_Party_Id6;
    private DbsField pnd_Output_Pnd_Party_Type_Cde6;
    private DbsField pnd_Output_Pnd_Party_Key6;
    private DbsField pnd_Output_Pnd_Full_Nme6;
    private DbsField pnd_Output_Pnd_Gov_Id6;
    private DbsField pnd_Output_Pnd_Gov_Id_Type_Cde6;
    private DbsField pnd_Output_Pnd_Res_State6;
    private DbsField pnd_Output_Pnd_Res_Country6;
    private DbsField pnd_Output_Pnd_Res_Zip6;
    private DbsField pnd_Output_Pnd_Party_Id7;
    private DbsField pnd_Output_Pnd_Party_Type_Cde7;
    private DbsField pnd_Output_Pnd_Party_Key7;
    private DbsField pnd_Output_Pnd_Full_Nme7;
    private DbsField pnd_Output_Pnd_Gov_Id7;
    private DbsField pnd_Output_Pnd_Gov_Id_Type_Cde7;
    private DbsField pnd_Output_Pnd_Res_State7;
    private DbsField pnd_Output_Pnd_Res_Country7;
    private DbsField pnd_Output_Pnd_Res_Zip7;
    private DbsField pnd_Output_Pnd_Party_Type_Cde8;
    private DbsField pnd_Output_Pnd_Party_Key8;
    private DbsField pnd_Output_Pnd_Full_Nme8;
    private DbsField pnd_Output_Pnd_Res_State8;
    private DbsField pnd_Output_Pnd_Res_Country8;
    private DbsField pnd_Output_Pnd_Res_Zip8;
    private DbsField pnd_Output_Pnd_Party_Type_Cde9;
    private DbsField pnd_Output_Pnd_Party_Key9;
    private DbsField pnd_Output_Pnd_Full_Nme9;
    private DbsField pnd_Output_Pnd_Res_State9;
    private DbsField pnd_Output_Pnd_Res_Country9;
    private DbsField pnd_Output_Pnd_Res_Zip9;
    private DbsField pnd_Output_Pnd_Rel_Id1;
    private DbsField pnd_Output_Pnd_Orig_Object_Id1;
    private DbsField pnd_Output_Pnd_Rel_Object_Id1;
    private DbsField pnd_Output_Pnd_Orig_Object_Type1;
    private DbsField pnd_Output_Pnd_Rel_Object_Type1;
    private DbsField pnd_Output_Pnd_Rel_Role_Code1;
    private DbsField pnd_Output_Pnd_Int_Pct1;
    private DbsField pnd_Output_Pnd_Rel_Id2;
    private DbsField pnd_Output_Pnd_Orig_Object_Id2;
    private DbsField pnd_Output_Pnd_Rel_Object_Id2;
    private DbsField pnd_Output_Pnd_Orig_Object_Type2;
    private DbsField pnd_Output_Pnd_Rel_Object_Type2;
    private DbsField pnd_Output_Pnd_Rel_Role_Code2;
    private DbsField pnd_Output_Pnd_Int_Pct2;
    private DbsField pnd_Output_Pnd_Rel_Id3;
    private DbsField pnd_Output_Pnd_Orig_Object_Id3;
    private DbsField pnd_Output_Pnd_Rel_Object_Id3;
    private DbsField pnd_Output_Pnd_Orig_Object_Type3;
    private DbsField pnd_Output_Pnd_Rel_Object_Type3;
    private DbsField pnd_Output_Pnd_Rel_Role_Code3;
    private DbsField pnd_Output_Pnd_Rel_Id4;
    private DbsField pnd_Output_Pnd_Orig_Object_Id4;
    private DbsField pnd_Output_Pnd_Rel_Object_Id4;
    private DbsField pnd_Output_Pnd_Orig_Object_Type4;
    private DbsField pnd_Output_Pnd_Rel_Object_Type4;
    private DbsField pnd_Output_Pnd_Rel_Role_Code4;
    private DbsField pnd_Output_Pnd_Rel_Id5;
    private DbsField pnd_Output_Pnd_Orig_Object_Id5;
    private DbsField pnd_Output_Pnd_Rel_Object_Id5;
    private DbsField pnd_Output_Pnd_Orig_Object_Type5;
    private DbsField pnd_Output_Pnd_Rel_Object_Type5;
    private DbsField pnd_Output_Pnd_Rel_Role_Code5;
    private DbsField pnd_Output_Pnd_Rel_Id6;
    private DbsField pnd_Output_Pnd_Orig_Object_Id6;
    private DbsField pnd_Output_Pnd_Rel_Object_Id6;
    private DbsField pnd_Output_Pnd_Orig_Object_Type6;
    private DbsField pnd_Output_Pnd_Rel_Object_Type6;
    private DbsField pnd_Output_Pnd_Rel_Role_Code6;
    private DbsField pnd_Output_Pnd_Rel_Id7;
    private DbsField pnd_Output_Pnd_Orig_Object_Id7;
    private DbsField pnd_Output_Pnd_Rel_Object_Id7;
    private DbsField pnd_Output_Pnd_Orig_Object_Type7;
    private DbsField pnd_Output_Pnd_Rel_Object_Type7;
    private DbsField pnd_Output_Pnd_Rel_Role_Code7;
    private DbsField pnd_Output_Pnd_Owner_Dob;
    private DbsField pnd_Output_Pnd_Jnt_Owner_Dob;
    private DbsField pnd_Output_Pnd_Txn_Type;
    private DbsField pnd_Output_Pnd_Txn_Nbr;
    private DbsField pnd_Output_Pnd_Premium_Amt;

    public DbsGroup getPnd_Output() { return pnd_Output; }

    public DbsField getPnd_Output_Pnd_Xml_Id() { return pnd_Output_Pnd_Xml_Id; }

    public DbsField getPnd_Output_Pnd_Pmry_Obj_Id() { return pnd_Output_Pnd_Pmry_Obj_Id; }

    public DbsField getPnd_Output_Pnd_Trans_Ref_Guid() { return pnd_Output_Pnd_Trans_Ref_Guid; }

    public DbsField getPnd_Output_Pnd_Trans_Type() { return pnd_Output_Pnd_Trans_Type; }

    public DbsField getPnd_Output_Pnd_Trans_Subtype() { return pnd_Output_Pnd_Trans_Subtype; }

    public DbsField getPnd_Output_Pnd_Trans_Exe_Dte() { return pnd_Output_Pnd_Trans_Exe_Dte; }

    public DbsGroup getPnd_Output_Pnd_Trans_Exe_DteRedef1() { return pnd_Output_Pnd_Trans_Exe_DteRedef1; }

    public DbsField getPnd_Output_Pnd_Trans_Start_Dte() { return pnd_Output_Pnd_Trans_Start_Dte; }

    public DbsField getPnd_Output_Pnd_Trans_End_Dte() { return pnd_Output_Pnd_Trans_End_Dte; }

    public DbsField getPnd_Output_Pnd_Trans_Exe_Tme() { return pnd_Output_Pnd_Trans_Exe_Tme; }

    public DbsField getPnd_Output_Pnd_Trans_Mode() { return pnd_Output_Pnd_Trans_Mode; }

    public DbsField getPnd_Output_Pnd_Inq_Level() { return pnd_Output_Pnd_Inq_Level; }

    public DbsField getPnd_Output_Pnd_Pend_Response() { return pnd_Output_Pnd_Pend_Response; }

    public DbsField getPnd_Output_Pnd_No_Response() { return pnd_Output_Pnd_No_Response; }

    public DbsField getPnd_Output_Pnd_Test_Ind() { return pnd_Output_Pnd_Test_Ind; }

    public DbsField getPnd_Output_Pnd_Primary_Object_Typ() { return pnd_Output_Pnd_Primary_Object_Typ; }

    public DbsField getPnd_Output_Pnd_Creat_Dte() { return pnd_Output_Pnd_Creat_Dte; }

    public DbsField getPnd_Output_Pnd_Creat_Tme() { return pnd_Output_Pnd_Creat_Tme; }

    public DbsField getPnd_Output_Pnd_Source_Info_Nme() { return pnd_Output_Pnd_Source_Info_Nme; }

    public DbsField getPnd_Output_Pnd_Source_Info_Desc() { return pnd_Output_Pnd_Source_Info_Desc; }

    public DbsField getPnd_Output_Pnd_Source_Info_Comment() { return pnd_Output_Pnd_Source_Info_Comment; }

    public DbsField getPnd_Output_Pnd_File_Control_Id() { return pnd_Output_Pnd_File_Control_Id; }

    public DbsField getPnd_Output_Pnd_Holding_Id() { return pnd_Output_Pnd_Holding_Id; }

    public DbsField getPnd_Output_Pnd_Holding_Type_Cde() { return pnd_Output_Pnd_Holding_Type_Cde; }

    public DbsField getPnd_Output_Pnd_Holding_Stat() { return pnd_Output_Pnd_Holding_Stat; }

    public DbsField getPnd_Output_Pnd_As_Of_Dte() { return pnd_Output_Pnd_As_Of_Dte; }

    public DbsField getPnd_Output_Pnd_Share_Class() { return pnd_Output_Pnd_Share_Class; }

    public DbsField getPnd_Output_Pnd_Policy_Nbr() { return pnd_Output_Pnd_Policy_Nbr; }

    public DbsField getPnd_Output_Pnd_Cert_Nbr() { return pnd_Output_Pnd_Cert_Nbr; }

    public DbsField getPnd_Output_Pnd_Lob() { return pnd_Output_Pnd_Lob; }

    public DbsField getPnd_Output_Pnd_Product_Type() { return pnd_Output_Pnd_Product_Type; }

    public DbsField getPnd_Output_Pnd_Product_Cde() { return pnd_Output_Pnd_Product_Cde; }

    public DbsField getPnd_Output_Pnd_Carrier_Cde() { return pnd_Output_Pnd_Carrier_Cde; }

    public DbsField getPnd_Output_Pnd_Plan_Name() { return pnd_Output_Pnd_Plan_Name; }

    public DbsField getPnd_Output_Pnd_Policy_Stat() { return pnd_Output_Pnd_Policy_Stat; }

    public DbsField getPnd_Output_Pnd_Issue_Nation() { return pnd_Output_Pnd_Issue_Nation; }

    public DbsField getPnd_Output_Pnd_Issue_Type() { return pnd_Output_Pnd_Issue_Type; }

    public DbsField getPnd_Output_Pnd_Jurisdiction() { return pnd_Output_Pnd_Jurisdiction; }

    public DbsField getPnd_Output_Pnd_Replacement_Type() { return pnd_Output_Pnd_Replacement_Type; }

    public DbsField getPnd_Output_Pnd_Policy_Value() { return pnd_Output_Pnd_Policy_Value; }

    public DbsField getPnd_Output_Pnd_Eff_Date() { return pnd_Output_Pnd_Eff_Date; }

    public DbsField getPnd_Output_Pnd_Out_Issue_Dte() { return pnd_Output_Pnd_Out_Issue_Dte; }

    public DbsField getPnd_Output_Pnd_Status_Change_Dte() { return pnd_Output_Pnd_Status_Change_Dte; }

    public DbsField getPnd_Output_Pnd_Prior_Policy_Nbr() { return pnd_Output_Pnd_Prior_Policy_Nbr; }

    public DbsField getPnd_Output_Pnd_Payout_Type() { return pnd_Output_Pnd_Payout_Type; }

    public DbsField getPnd_Output_Pnd_Qual_Plan_Type() { return pnd_Output_Pnd_Qual_Plan_Type; }

    public DbsField getPnd_Output_Pnd_Qual_Plan_Sub_Type() { return pnd_Output_Pnd_Qual_Plan_Sub_Type; }

    public DbsField getPnd_Output_Pnd_Cum_Wthdrwl_Amtitd() { return pnd_Output_Pnd_Cum_Wthdrwl_Amtitd; }

    public DbsField getPnd_Output_Pnd_Death_Benefit_Amt() { return pnd_Output_Pnd_Death_Benefit_Amt; }

    public DbsField getPnd_Output_Pnd_Surrender_Value() { return pnd_Output_Pnd_Surrender_Value; }

    public DbsField getPnd_Output_Pnd_Guarint_Rate() { return pnd_Output_Pnd_Guarint_Rate; }

    public DbsField getPnd_Output_Pnd_Gross_Prem_Amtitd() { return pnd_Output_Pnd_Gross_Prem_Amtitd; }

    public DbsField getPnd_Output_Pnd_Cum_Prem_Amt_Yr1() { return pnd_Output_Pnd_Cum_Prem_Amt_Yr1; }

    public DbsField getPnd_Output_Pnd_Payout_Id() { return pnd_Output_Pnd_Payout_Id; }

    public DbsField getPnd_Output_Pnd_Income_Option() { return pnd_Output_Pnd_Income_Option; }

    public DbsField getPnd_Output_Pnd_Payout_Amt() { return pnd_Output_Pnd_Payout_Amt; }

    public DbsField getPnd_Output_Pnd_Payout_Mode() { return pnd_Output_Pnd_Payout_Mode; }

    public DbsField getPnd_Output_Pnd_Start_Date() { return pnd_Output_Pnd_Start_Date; }

    public DbsField getPnd_Output_Pnd_Annuity_Start_Dte() { return pnd_Output_Pnd_Annuity_Start_Dte; }

    public DbsField getPnd_Output_Pnd_Lives_Type() { return pnd_Output_Pnd_Lives_Type; }

    public DbsField getPnd_Output_Pnd_Annual_Indx_Type() { return pnd_Output_Pnd_Annual_Indx_Type; }

    public DbsField getPnd_Output_Pnd_Period_Certain_Mode() { return pnd_Output_Pnd_Period_Certain_Mode; }

    public DbsField getPnd_Output_Pnd_Period_Certain_Periods() { return pnd_Output_Pnd_Period_Certain_Periods; }

    public DbsField getPnd_Output_Pnd_Payout_Change_Id() { return pnd_Output_Pnd_Payout_Change_Id; }

    public DbsField getPnd_Output_Pnd_Change_Mode() { return pnd_Output_Pnd_Change_Mode; }

    public DbsField getPnd_Output_Pnd_Payout_Change_Amt() { return pnd_Output_Pnd_Payout_Change_Amt; }

    public DbsField getPnd_Output_Pnd_Amt_Qualifier() { return pnd_Output_Pnd_Amt_Qualifier; }

    public DbsField getPnd_Output_Pnd_Payout_Pct_Type() { return pnd_Output_Pnd_Payout_Pct_Type; }

    public DbsField getPnd_Output_Pnd_Description() { return pnd_Output_Pnd_Description; }

    public DbsField getPnd_Output_Pnd_Out_Start_Dte() { return pnd_Output_Pnd_Out_Start_Dte; }

    public DbsField getPnd_Output_Pnd_Out_End_Dte() { return pnd_Output_Pnd_Out_End_Dte; }

    public DbsField getPnd_Output_Pnd_Next_Activity_Dte() { return pnd_Output_Pnd_Next_Activity_Dte; }

    public DbsField getPnd_Output_Pnd_Timing_Basis() { return pnd_Output_Pnd_Timing_Basis; }

    public DbsField getPnd_Output_Pnd_Payout_Pct() { return pnd_Output_Pnd_Payout_Pct; }

    public DbsField getPnd_Output_Pnd_Rider_Id() { return pnd_Output_Pnd_Rider_Id; }

    public DbsField getPnd_Output_Pnd_Rider_Type_Cde() { return pnd_Output_Pnd_Rider_Type_Cde; }

    public DbsField getPnd_Output_Pnd_Rider_Type_Sub_Cde() { return pnd_Output_Pnd_Rider_Type_Sub_Cde; }

    public DbsField getPnd_Output_Pnd_Rider_Cde() { return pnd_Output_Pnd_Rider_Cde; }

    public DbsField getPnd_Output_Pnd_Eff_Dte() { return pnd_Output_Pnd_Eff_Dte; }

    public DbsField getPnd_Output_Pnd_Term_Date() { return pnd_Output_Pnd_Term_Date; }

    public DbsField getPnd_Output_Pnd_Tot_Amt() { return pnd_Output_Pnd_Tot_Amt; }

    public DbsField getPnd_Output_Pnd_Tot_Amt_Last_Ann() { return pnd_Output_Pnd_Tot_Amt_Last_Ann; }

    public DbsField getPnd_Output_Pnd_Lives_Typ() { return pnd_Output_Pnd_Lives_Typ; }

    public DbsField getPnd_Output_Pnd_Benefit_Pct() { return pnd_Output_Pnd_Benefit_Pct; }

    public DbsField getPnd_Output_Pnd_Event_Type() { return pnd_Output_Pnd_Event_Type; }

    public DbsField getPnd_Output_Pnd_Event_Year() { return pnd_Output_Pnd_Event_Year; }

    public DbsField getPnd_Output_Pnd_Rider_Free_Amt() { return pnd_Output_Pnd_Rider_Free_Amt; }

    public DbsField getPnd_Output_Pnd_Financial_Act_Id() { return pnd_Output_Pnd_Financial_Act_Id; }

    public DbsField getPnd_Output_Pnd_Applies_To_Overage_Id() { return pnd_Output_Pnd_Applies_To_Overage_Id; }

    public DbsField getPnd_Output_Pnd_Finact_Type() { return pnd_Output_Pnd_Finact_Type; }

    public DbsField getPnd_Output_Pnd_Fin_Act_Sub_Type() { return pnd_Output_Pnd_Fin_Act_Sub_Type; }

    public DbsField getPnd_Output_Pnd_Reversal_Ind() { return pnd_Output_Pnd_Reversal_Ind; }

    public DbsField getPnd_Output_Pnd_Fin_Act_Gross_Amt() { return pnd_Output_Pnd_Fin_Act_Gross_Amt; }

    public DbsField getPnd_Output_Pnd_Acc_Value_Last_Ann() { return pnd_Output_Pnd_Acc_Value_Last_Ann; }

    public DbsField getPnd_Output_Pnd_Sub_Acct_Id() { return pnd_Output_Pnd_Sub_Acct_Id; }

    public DbsField getPnd_Output_Pnd_Asset_Class() { return pnd_Output_Pnd_Asset_Class; }

    public DbsField getPnd_Output_Pnd_Tot_Value() { return pnd_Output_Pnd_Tot_Value; }

    public DbsField getPnd_Output_Pnd_Base_Rate() { return pnd_Output_Pnd_Base_Rate; }

    public DbsField getPnd_Output_Pnd_Dur_Qualifier() { return pnd_Output_Pnd_Dur_Qualifier; }

    public DbsField getPnd_Output_Pnd_Guarantee_Duration() { return pnd_Output_Pnd_Guarantee_Duration; }

    public DbsField getPnd_Output_Pnd_Cap_Rate() { return pnd_Output_Pnd_Cap_Rate; }

    public DbsField getPnd_Output_Pnd_Part_Rate() { return pnd_Output_Pnd_Part_Rate; }

    public DbsField getPnd_Output_Pnd_Market_Val_Adjust_Ind() { return pnd_Output_Pnd_Market_Val_Adjust_Ind; }

    public DbsField getPnd_Output_Pnd_Bonus_Rate() { return pnd_Output_Pnd_Bonus_Rate; }

    public DbsField getPnd_Output_Pnd_Acct_Value_Last_Ann() { return pnd_Output_Pnd_Acct_Value_Last_Ann; }

    public DbsField getPnd_Output_Pnd_Financial_Actid() { return pnd_Output_Pnd_Financial_Actid; }

    public DbsField getPnd_Output_Pnd_Applies_To_Coverage_Id() { return pnd_Output_Pnd_Applies_To_Coverage_Id; }

    public DbsField getPnd_Output_Pnd_Fin_Act_Type() { return pnd_Output_Pnd_Fin_Act_Type; }

    public DbsField getPnd_Output_Pnd_Fin_Act_Subtype() { return pnd_Output_Pnd_Fin_Act_Subtype; }

    public DbsField getPnd_Output_Pnd_Rev_Ind() { return pnd_Output_Pnd_Rev_Ind; }

    public DbsField getPnd_Output_Pnd_Fin_Activity_Gross_Amt() { return pnd_Output_Pnd_Fin_Activity_Gross_Amt; }

    public DbsField getPnd_Output_Pnd_Arrangement_Id() { return pnd_Output_Pnd_Arrangement_Id; }

    public DbsField getPnd_Output_Pnd_Arr_Type() { return pnd_Output_Pnd_Arr_Type; }

    public DbsField getPnd_Output_Pnd_Modal_Amt() { return pnd_Output_Pnd_Modal_Amt; }

    public DbsField getPnd_Output_Pnd_Party_Id1() { return pnd_Output_Pnd_Party_Id1; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde1() { return pnd_Output_Pnd_Party_Type_Cde1; }

    public DbsField getPnd_Output_Pnd_Party_Key1() { return pnd_Output_Pnd_Party_Key1; }

    public DbsField getPnd_Output_Pnd_Res_State1() { return pnd_Output_Pnd_Res_State1; }

    public DbsField getPnd_Output_Pnd_Res_Country1() { return pnd_Output_Pnd_Res_Country1; }

    public DbsField getPnd_Output_Pnd_Res_Zip1() { return pnd_Output_Pnd_Res_Zip1; }

    public DbsField getPnd_Output_Pnd_Nipr1() { return pnd_Output_Pnd_Nipr1; }

    public DbsField getPnd_Output_Pnd_Co_Prod_Id1() { return pnd_Output_Pnd_Co_Prod_Id1; }

    public DbsField getPnd_Output_Pnd_Carrier_Appt_Stat1() { return pnd_Output_Pnd_Carrier_Appt_Stat1; }

    public DbsField getPnd_Output_Pnd_Dist_Channel1() { return pnd_Output_Pnd_Dist_Channel1; }

    public DbsField getPnd_Output_Pnd_Dist_Channel_Sub_Type1() { return pnd_Output_Pnd_Dist_Channel_Sub_Type1; }

    public DbsField getPnd_Output_Pnd_Dist_Channel_Name1() { return pnd_Output_Pnd_Dist_Channel_Name1; }

    public DbsField getPnd_Output_Pnd_Dist_Channel_Cde1() { return pnd_Output_Pnd_Dist_Channel_Cde1; }

    public DbsField getPnd_Output_Pnd_Party_Id2() { return pnd_Output_Pnd_Party_Id2; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde2() { return pnd_Output_Pnd_Party_Type_Cde2; }

    public DbsField getPnd_Output_Pnd_Party_Key2() { return pnd_Output_Pnd_Party_Key2; }

    public DbsField getPnd_Output_Pnd_Res_State2() { return pnd_Output_Pnd_Res_State2; }

    public DbsField getPnd_Output_Pnd_Res_Country2() { return pnd_Output_Pnd_Res_Country2; }

    public DbsField getPnd_Output_Pnd_Res_Zip2() { return pnd_Output_Pnd_Res_Zip2; }

    public DbsField getPnd_Output_Pnd_Nipr2() { return pnd_Output_Pnd_Nipr2; }

    public DbsField getPnd_Output_Pnd_Co_Prod_Id2() { return pnd_Output_Pnd_Co_Prod_Id2; }

    public DbsField getPnd_Output_Pnd_Carrier_Appt_Stat2() { return pnd_Output_Pnd_Carrier_Appt_Stat2; }

    public DbsField getPnd_Output_Pnd_Dist_Channel2() { return pnd_Output_Pnd_Dist_Channel2; }

    public DbsField getPnd_Output_Pnd_Dist_Channel_Sub_Type2() { return pnd_Output_Pnd_Dist_Channel_Sub_Type2; }

    public DbsField getPnd_Output_Pnd_Dist_Channel_Name2() { return pnd_Output_Pnd_Dist_Channel_Name2; }

    public DbsField getPnd_Output_Pnd_Dist_Channel_Cde2() { return pnd_Output_Pnd_Dist_Channel_Cde2; }

    public DbsField getPnd_Output_Pnd_Party_Id3() { return pnd_Output_Pnd_Party_Id3; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde3() { return pnd_Output_Pnd_Party_Type_Cde3; }

    public DbsField getPnd_Output_Pnd_Party_Key3() { return pnd_Output_Pnd_Party_Key3; }

    public DbsField getPnd_Output_Pnd_Res_State3() { return pnd_Output_Pnd_Res_State3; }

    public DbsField getPnd_Output_Pnd_Res_Country3() { return pnd_Output_Pnd_Res_Country3; }

    public DbsField getPnd_Output_Pnd_Res_Zip3() { return pnd_Output_Pnd_Res_Zip3; }

    public DbsField getPnd_Output_Pnd_Party_Id4() { return pnd_Output_Pnd_Party_Id4; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde4() { return pnd_Output_Pnd_Party_Type_Cde4; }

    public DbsField getPnd_Output_Pnd_Party_Key4() { return pnd_Output_Pnd_Party_Key4; }

    public DbsField getPnd_Output_Pnd_Res_State4() { return pnd_Output_Pnd_Res_State4; }

    public DbsField getPnd_Output_Pnd_Res_Country4() { return pnd_Output_Pnd_Res_Country4; }

    public DbsField getPnd_Output_Pnd_Res_Zip4() { return pnd_Output_Pnd_Res_Zip4; }

    public DbsField getPnd_Output_Pnd_Party_Id5() { return pnd_Output_Pnd_Party_Id5; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde5() { return pnd_Output_Pnd_Party_Type_Cde5; }

    public DbsField getPnd_Output_Pnd_Party_Key5() { return pnd_Output_Pnd_Party_Key5; }

    public DbsField getPnd_Output_Pnd_Res_State5() { return pnd_Output_Pnd_Res_State5; }

    public DbsField getPnd_Output_Pnd_Res_Country5() { return pnd_Output_Pnd_Res_Country5; }

    public DbsField getPnd_Output_Pnd_Res_Zip5() { return pnd_Output_Pnd_Res_Zip5; }

    public DbsField getPnd_Output_Pnd_Party_Id6() { return pnd_Output_Pnd_Party_Id6; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde6() { return pnd_Output_Pnd_Party_Type_Cde6; }

    public DbsField getPnd_Output_Pnd_Party_Key6() { return pnd_Output_Pnd_Party_Key6; }

    public DbsField getPnd_Output_Pnd_Full_Nme6() { return pnd_Output_Pnd_Full_Nme6; }

    public DbsField getPnd_Output_Pnd_Gov_Id6() { return pnd_Output_Pnd_Gov_Id6; }

    public DbsField getPnd_Output_Pnd_Gov_Id_Type_Cde6() { return pnd_Output_Pnd_Gov_Id_Type_Cde6; }

    public DbsField getPnd_Output_Pnd_Res_State6() { return pnd_Output_Pnd_Res_State6; }

    public DbsField getPnd_Output_Pnd_Res_Country6() { return pnd_Output_Pnd_Res_Country6; }

    public DbsField getPnd_Output_Pnd_Res_Zip6() { return pnd_Output_Pnd_Res_Zip6; }

    public DbsField getPnd_Output_Pnd_Party_Id7() { return pnd_Output_Pnd_Party_Id7; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde7() { return pnd_Output_Pnd_Party_Type_Cde7; }

    public DbsField getPnd_Output_Pnd_Party_Key7() { return pnd_Output_Pnd_Party_Key7; }

    public DbsField getPnd_Output_Pnd_Full_Nme7() { return pnd_Output_Pnd_Full_Nme7; }

    public DbsField getPnd_Output_Pnd_Gov_Id7() { return pnd_Output_Pnd_Gov_Id7; }

    public DbsField getPnd_Output_Pnd_Gov_Id_Type_Cde7() { return pnd_Output_Pnd_Gov_Id_Type_Cde7; }

    public DbsField getPnd_Output_Pnd_Res_State7() { return pnd_Output_Pnd_Res_State7; }

    public DbsField getPnd_Output_Pnd_Res_Country7() { return pnd_Output_Pnd_Res_Country7; }

    public DbsField getPnd_Output_Pnd_Res_Zip7() { return pnd_Output_Pnd_Res_Zip7; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde8() { return pnd_Output_Pnd_Party_Type_Cde8; }

    public DbsField getPnd_Output_Pnd_Party_Key8() { return pnd_Output_Pnd_Party_Key8; }

    public DbsField getPnd_Output_Pnd_Full_Nme8() { return pnd_Output_Pnd_Full_Nme8; }

    public DbsField getPnd_Output_Pnd_Res_State8() { return pnd_Output_Pnd_Res_State8; }

    public DbsField getPnd_Output_Pnd_Res_Country8() { return pnd_Output_Pnd_Res_Country8; }

    public DbsField getPnd_Output_Pnd_Res_Zip8() { return pnd_Output_Pnd_Res_Zip8; }

    public DbsField getPnd_Output_Pnd_Party_Type_Cde9() { return pnd_Output_Pnd_Party_Type_Cde9; }

    public DbsField getPnd_Output_Pnd_Party_Key9() { return pnd_Output_Pnd_Party_Key9; }

    public DbsField getPnd_Output_Pnd_Full_Nme9() { return pnd_Output_Pnd_Full_Nme9; }

    public DbsField getPnd_Output_Pnd_Res_State9() { return pnd_Output_Pnd_Res_State9; }

    public DbsField getPnd_Output_Pnd_Res_Country9() { return pnd_Output_Pnd_Res_Country9; }

    public DbsField getPnd_Output_Pnd_Res_Zip9() { return pnd_Output_Pnd_Res_Zip9; }

    public DbsField getPnd_Output_Pnd_Rel_Id1() { return pnd_Output_Pnd_Rel_Id1; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Id1() { return pnd_Output_Pnd_Orig_Object_Id1; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Id1() { return pnd_Output_Pnd_Rel_Object_Id1; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Type1() { return pnd_Output_Pnd_Orig_Object_Type1; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Type1() { return pnd_Output_Pnd_Rel_Object_Type1; }

    public DbsField getPnd_Output_Pnd_Rel_Role_Code1() { return pnd_Output_Pnd_Rel_Role_Code1; }

    public DbsField getPnd_Output_Pnd_Int_Pct1() { return pnd_Output_Pnd_Int_Pct1; }

    public DbsField getPnd_Output_Pnd_Rel_Id2() { return pnd_Output_Pnd_Rel_Id2; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Id2() { return pnd_Output_Pnd_Orig_Object_Id2; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Id2() { return pnd_Output_Pnd_Rel_Object_Id2; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Type2() { return pnd_Output_Pnd_Orig_Object_Type2; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Type2() { return pnd_Output_Pnd_Rel_Object_Type2; }

    public DbsField getPnd_Output_Pnd_Rel_Role_Code2() { return pnd_Output_Pnd_Rel_Role_Code2; }

    public DbsField getPnd_Output_Pnd_Int_Pct2() { return pnd_Output_Pnd_Int_Pct2; }

    public DbsField getPnd_Output_Pnd_Rel_Id3() { return pnd_Output_Pnd_Rel_Id3; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Id3() { return pnd_Output_Pnd_Orig_Object_Id3; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Id3() { return pnd_Output_Pnd_Rel_Object_Id3; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Type3() { return pnd_Output_Pnd_Orig_Object_Type3; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Type3() { return pnd_Output_Pnd_Rel_Object_Type3; }

    public DbsField getPnd_Output_Pnd_Rel_Role_Code3() { return pnd_Output_Pnd_Rel_Role_Code3; }

    public DbsField getPnd_Output_Pnd_Rel_Id4() { return pnd_Output_Pnd_Rel_Id4; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Id4() { return pnd_Output_Pnd_Orig_Object_Id4; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Id4() { return pnd_Output_Pnd_Rel_Object_Id4; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Type4() { return pnd_Output_Pnd_Orig_Object_Type4; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Type4() { return pnd_Output_Pnd_Rel_Object_Type4; }

    public DbsField getPnd_Output_Pnd_Rel_Role_Code4() { return pnd_Output_Pnd_Rel_Role_Code4; }

    public DbsField getPnd_Output_Pnd_Rel_Id5() { return pnd_Output_Pnd_Rel_Id5; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Id5() { return pnd_Output_Pnd_Orig_Object_Id5; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Id5() { return pnd_Output_Pnd_Rel_Object_Id5; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Type5() { return pnd_Output_Pnd_Orig_Object_Type5; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Type5() { return pnd_Output_Pnd_Rel_Object_Type5; }

    public DbsField getPnd_Output_Pnd_Rel_Role_Code5() { return pnd_Output_Pnd_Rel_Role_Code5; }

    public DbsField getPnd_Output_Pnd_Rel_Id6() { return pnd_Output_Pnd_Rel_Id6; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Id6() { return pnd_Output_Pnd_Orig_Object_Id6; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Id6() { return pnd_Output_Pnd_Rel_Object_Id6; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Type6() { return pnd_Output_Pnd_Orig_Object_Type6; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Type6() { return pnd_Output_Pnd_Rel_Object_Type6; }

    public DbsField getPnd_Output_Pnd_Rel_Role_Code6() { return pnd_Output_Pnd_Rel_Role_Code6; }

    public DbsField getPnd_Output_Pnd_Rel_Id7() { return pnd_Output_Pnd_Rel_Id7; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Id7() { return pnd_Output_Pnd_Orig_Object_Id7; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Id7() { return pnd_Output_Pnd_Rel_Object_Id7; }

    public DbsField getPnd_Output_Pnd_Orig_Object_Type7() { return pnd_Output_Pnd_Orig_Object_Type7; }

    public DbsField getPnd_Output_Pnd_Rel_Object_Type7() { return pnd_Output_Pnd_Rel_Object_Type7; }

    public DbsField getPnd_Output_Pnd_Rel_Role_Code7() { return pnd_Output_Pnd_Rel_Role_Code7; }

    public DbsField getPnd_Output_Pnd_Owner_Dob() { return pnd_Output_Pnd_Owner_Dob; }

    public DbsField getPnd_Output_Pnd_Jnt_Owner_Dob() { return pnd_Output_Pnd_Jnt_Owner_Dob; }

    public DbsField getPnd_Output_Pnd_Txn_Type() { return pnd_Output_Pnd_Txn_Type; }

    public DbsField getPnd_Output_Pnd_Txn_Nbr() { return pnd_Output_Pnd_Txn_Nbr; }

    public DbsField getPnd_Output_Pnd_Premium_Amt() { return pnd_Output_Pnd_Premium_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Output = newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Xml_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Xml_Id", "#XML-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Pmry_Obj_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pmry_Obj_Id", "#PMRY-OBJ-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Trans_Ref_Guid = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Trans_Ref_Guid", "#TRANS-REF-GUID", FieldType.STRING, 32);
        pnd_Output_Pnd_Trans_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Trans_Type", "#TRANS-TYPE", FieldType.STRING, 3);
        pnd_Output_Pnd_Trans_Subtype = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Trans_Subtype", "#TRANS-SUBTYPE", FieldType.STRING, 5);
        pnd_Output_Pnd_Trans_Exe_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Trans_Exe_Dte", "#TRANS-EXE-DTE", FieldType.STRING, 16);
        pnd_Output_Pnd_Trans_Exe_DteRedef1 = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Trans_Exe_DteRedef1", "Redefines", pnd_Output_Pnd_Trans_Exe_Dte);
        pnd_Output_Pnd_Trans_Start_Dte = pnd_Output_Pnd_Trans_Exe_DteRedef1.newFieldInGroup("pnd_Output_Pnd_Trans_Start_Dte", "#TRANS-START-DTE", FieldType.STRING, 
            8);
        pnd_Output_Pnd_Trans_End_Dte = pnd_Output_Pnd_Trans_Exe_DteRedef1.newFieldInGroup("pnd_Output_Pnd_Trans_End_Dte", "#TRANS-END-DTE", FieldType.STRING, 
            8);
        pnd_Output_Pnd_Trans_Exe_Tme = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Trans_Exe_Tme", "#TRANS-EXE-TME", FieldType.STRING, 8);
        pnd_Output_Pnd_Trans_Mode = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Trans_Mode", "#TRANS-MODE", FieldType.STRING, 1);
        pnd_Output_Pnd_Inq_Level = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Inq_Level", "#INQ-LEVEL", FieldType.STRING, 1);
        pnd_Output_Pnd_Pend_Response = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pend_Response", "#PEND-RESPONSE", FieldType.STRING, 1);
        pnd_Output_Pnd_No_Response = pnd_Output.newFieldInGroup("pnd_Output_Pnd_No_Response", "#NO-RESPONSE", FieldType.STRING, 1);
        pnd_Output_Pnd_Test_Ind = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 1);
        pnd_Output_Pnd_Primary_Object_Typ = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Primary_Object_Typ", "#PRIMARY-OBJECT-TYP", FieldType.STRING, 1);
        pnd_Output_Pnd_Creat_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Creat_Dte", "#CREAT-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Creat_Tme = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Creat_Tme", "#CREAT-TME", FieldType.STRING, 8);
        pnd_Output_Pnd_Source_Info_Nme = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Source_Info_Nme", "#SOURCE-INFO-NME", FieldType.STRING, 12);
        pnd_Output_Pnd_Source_Info_Desc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Source_Info_Desc", "#SOURCE-INFO-DESC", FieldType.STRING, 15);
        pnd_Output_Pnd_Source_Info_Comment = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Source_Info_Comment", "#SOURCE-INFO-COMMENT", FieldType.STRING, 
            15);
        pnd_Output_Pnd_File_Control_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_File_Control_Id", "#FILE-CONTROL-ID", FieldType.STRING, 8);
        pnd_Output_Pnd_Holding_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Holding_Id", "#HOLDING-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Holding_Type_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Holding_Type_Cde", "#HOLDING-TYPE-CDE", FieldType.STRING, 1);
        pnd_Output_Pnd_Holding_Stat = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Holding_Stat", "#HOLDING-STAT", FieldType.STRING, 1);
        pnd_Output_Pnd_As_Of_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_As_Of_Dte", "#AS-OF-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Share_Class = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Share_Class", "#SHARE-CLASS", FieldType.STRING, 12);
        pnd_Output_Pnd_Policy_Nbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Policy_Nbr", "#POLICY-NBR", FieldType.STRING, 10);
        pnd_Output_Pnd_Cert_Nbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cert_Nbr", "#CERT-NBR", FieldType.STRING, 10);
        pnd_Output_Pnd_Lob = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Lob", "#LOB", FieldType.STRING, 10);
        pnd_Output_Pnd_Product_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Product_Type", "#PRODUCT-TYPE", FieldType.STRING, 2);
        pnd_Output_Pnd_Product_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Product_Cde", "#PRODUCT-CDE", FieldType.STRING, 8);
        pnd_Output_Pnd_Carrier_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Carrier_Cde", "#CARRIER-CDE", FieldType.STRING, 8);
        pnd_Output_Pnd_Plan_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Plan_Name", "#PLAN-NAME", FieldType.STRING, 15);
        pnd_Output_Pnd_Policy_Stat = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Policy_Stat", "#POLICY-STAT", FieldType.STRING, 2);
        pnd_Output_Pnd_Issue_Nation = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Issue_Nation", "#ISSUE-NATION", FieldType.STRING, 1);
        pnd_Output_Pnd_Issue_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Issue_Type", "#ISSUE-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Jurisdiction = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Jurisdiction", "#JURISDICTION", FieldType.STRING, 2);
        pnd_Output_Pnd_Replacement_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Replacement_Type", "#REPLACEMENT-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Policy_Value = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Policy_Value", "#POLICY-VALUE", FieldType.STRING, 9);
        pnd_Output_Pnd_Eff_Date = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);
        pnd_Output_Pnd_Out_Issue_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Out_Issue_Dte", "#OUT-ISSUE-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Status_Change_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Status_Change_Dte", "#STATUS-CHANGE-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Prior_Policy_Nbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Prior_Policy_Nbr", "#PRIOR-POLICY-NBR", FieldType.STRING, 8);
        pnd_Output_Pnd_Payout_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Type", "#PAYOUT-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Qual_Plan_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Qual_Plan_Type", "#QUAL-PLAN-TYPE", FieldType.STRING, 2);
        pnd_Output_Pnd_Qual_Plan_Sub_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Qual_Plan_Sub_Type", "#QUAL-PLAN-SUB-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Cum_Wthdrwl_Amtitd = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cum_Wthdrwl_Amtitd", "#CUM-WTHDRWL-AMTITD", FieldType.STRING, 12);
        pnd_Output_Pnd_Death_Benefit_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Death_Benefit_Amt", "#DEATH-BENEFIT-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_Surrender_Value = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Surrender_Value", "#SURRENDER-VALUE", FieldType.STRING, 10);
        pnd_Output_Pnd_Guarint_Rate = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Guarint_Rate", "#GUARINT-RATE", FieldType.STRING, 6);
        pnd_Output_Pnd_Gross_Prem_Amtitd = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Gross_Prem_Amtitd", "#GROSS-PREM-AMTITD", FieldType.STRING, 10);
        pnd_Output_Pnd_Cum_Prem_Amt_Yr1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cum_Prem_Amt_Yr1", "#CUM-PREM-AMT-YR1", FieldType.STRING, 12);
        pnd_Output_Pnd_Payout_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Id", "#PAYOUT-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Income_Option = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Income_Option", "#INCOME-OPTION", FieldType.STRING, 2);
        pnd_Output_Pnd_Payout_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Amt", "#PAYOUT-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_Payout_Mode = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Mode", "#PAYOUT-MODE", FieldType.STRING, 2);
        pnd_Output_Pnd_Start_Date = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Output_Pnd_Annuity_Start_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Annuity_Start_Dte", "#ANNUITY-START-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Lives_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Lives_Type", "#LIVES-TYPE", FieldType.STRING, 2);
        pnd_Output_Pnd_Annual_Indx_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Annual_Indx_Type", "#ANNUAL-INDX-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Period_Certain_Mode = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Period_Certain_Mode", "#PERIOD-CERTAIN-MODE", FieldType.STRING, 
            1);
        pnd_Output_Pnd_Period_Certain_Periods = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Period_Certain_Periods", "#PERIOD-CERTAIN-PERIODS", FieldType.STRING, 
            1);
        pnd_Output_Pnd_Payout_Change_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Change_Id", "#PAYOUT-CHANGE-ID", FieldType.STRING, 1);
        pnd_Output_Pnd_Change_Mode = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Change_Mode", "#CHANGE-MODE", FieldType.STRING, 1);
        pnd_Output_Pnd_Payout_Change_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Change_Amt", "#PAYOUT-CHANGE-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_Amt_Qualifier = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Amt_Qualifier", "#AMT-QUALIFIER", FieldType.STRING, 1);
        pnd_Output_Pnd_Payout_Pct_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Pct_Type", "#PAYOUT-PCT-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Description = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Description", "#DESCRIPTION", FieldType.STRING, 15);
        pnd_Output_Pnd_Out_Start_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Out_Start_Dte", "#OUT-START-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Out_End_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Out_End_Dte", "#OUT-END-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Next_Activity_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Next_Activity_Dte", "#NEXT-ACTIVITY-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Timing_Basis = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Timing_Basis", "#TIMING-BASIS", FieldType.STRING, 1);
        pnd_Output_Pnd_Payout_Pct = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Payout_Pct", "#PAYOUT-PCT", FieldType.STRING, 6);
        pnd_Output_Pnd_Rider_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rider_Id", "#RIDER-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Rider_Type_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rider_Type_Cde", "#RIDER-TYPE-CDE", FieldType.STRING, 3);
        pnd_Output_Pnd_Rider_Type_Sub_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rider_Type_Sub_Cde", "#RIDER-TYPE-SUB-CDE", FieldType.STRING, 3);
        pnd_Output_Pnd_Rider_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rider_Cde", "#RIDER-CDE", FieldType.STRING, 3);
        pnd_Output_Pnd_Eff_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Eff_Dte", "#EFF-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Term_Date = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Term_Date", "#TERM-DATE", FieldType.STRING, 8);
        pnd_Output_Pnd_Tot_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Tot_Amt", "#TOT-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_Tot_Amt_Last_Ann = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Tot_Amt_Last_Ann", "#TOT-AMT-LAST-ANN", FieldType.STRING, 10);
        pnd_Output_Pnd_Lives_Typ = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Lives_Typ", "#LIVES-TYP", FieldType.STRING, 2);
        pnd_Output_Pnd_Benefit_Pct = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Benefit_Pct", "#BENEFIT-PCT", FieldType.STRING, 6);
        pnd_Output_Pnd_Event_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Event_Type", "#EVENT-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Event_Year = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Event_Year", "#EVENT-YEAR", FieldType.STRING, 4);
        pnd_Output_Pnd_Rider_Free_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rider_Free_Amt", "#RIDER-FREE-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_Financial_Act_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Financial_Act_Id", "#FINANCIAL-ACT-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Applies_To_Overage_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Applies_To_Overage_Id", "#APPLIES-TO-OVERAGE-ID", FieldType.STRING, 
            10);
        pnd_Output_Pnd_Finact_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Finact_Type", "#FINACT-TYPE", FieldType.STRING, 3);
        pnd_Output_Pnd_Fin_Act_Sub_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fin_Act_Sub_Type", "#FIN-ACT-SUB-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Reversal_Ind = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Reversal_Ind", "#REVERSAL-IND", FieldType.STRING, 1);
        pnd_Output_Pnd_Fin_Act_Gross_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fin_Act_Gross_Amt", "#FIN-ACT-GROSS-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_Acc_Value_Last_Ann = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Acc_Value_Last_Ann", "#ACC-VALUE-LAST-ANN", FieldType.STRING, 10);
        pnd_Output_Pnd_Sub_Acct_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Sub_Acct_Id", "#SUB-ACCT-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Asset_Class = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Asset_Class", "#ASSET-CLASS", FieldType.STRING, 1);
        pnd_Output_Pnd_Tot_Value = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Tot_Value", "#TOT-VALUE", FieldType.STRING, 10);
        pnd_Output_Pnd_Base_Rate = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Base_Rate", "#BASE-RATE", FieldType.STRING, 6);
        pnd_Output_Pnd_Dur_Qualifier = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dur_Qualifier", "#DUR-QUALIFIER", FieldType.STRING, 2);
        pnd_Output_Pnd_Guarantee_Duration = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Guarantee_Duration", "#GUARANTEE-DURATION", FieldType.STRING, 2);
        pnd_Output_Pnd_Cap_Rate = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cap_Rate", "#CAP-RATE", FieldType.STRING, 6);
        pnd_Output_Pnd_Part_Rate = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Part_Rate", "#PART-RATE", FieldType.STRING, 6);
        pnd_Output_Pnd_Market_Val_Adjust_Ind = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Market_Val_Adjust_Ind", "#MARKET-VAL-ADJUST-IND", FieldType.STRING, 
            1);
        pnd_Output_Pnd_Bonus_Rate = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Bonus_Rate", "#BONUS-RATE", FieldType.STRING, 6);
        pnd_Output_Pnd_Acct_Value_Last_Ann = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Acct_Value_Last_Ann", "#ACCT-VALUE-LAST-ANN", FieldType.STRING, 
            10);
        pnd_Output_Pnd_Financial_Actid = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Financial_Actid", "#FINANCIAL-ACTID", FieldType.STRING, 1);
        pnd_Output_Pnd_Applies_To_Coverage_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Applies_To_Coverage_Id", "#APPLIES-TO-COVERAGE-ID", FieldType.STRING, 
            1);
        pnd_Output_Pnd_Fin_Act_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fin_Act_Type", "#FIN-ACT-TYPE", FieldType.STRING, 3);
        pnd_Output_Pnd_Fin_Act_Subtype = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fin_Act_Subtype", "#FIN-ACT-SUBTYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Rev_Ind = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rev_Ind", "#REV-IND", FieldType.STRING, 1);
        pnd_Output_Pnd_Fin_Activity_Gross_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fin_Activity_Gross_Amt", "#FIN-ACTIVITY-GROSS-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Output_Pnd_Arrangement_Id = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Arrangement_Id", "#ARRANGEMENT-ID", FieldType.STRING, 12);
        pnd_Output_Pnd_Arr_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Arr_Type", "#ARR-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Modal_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Modal_Amt", "#MODAL-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_Party_Id1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Id1", "#PARTY-ID1", FieldType.STRING, 12);
        pnd_Output_Pnd_Party_Type_Cde1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde1", "#PARTY-TYPE-CDE1", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key1", "#PARTY-KEY1", FieldType.STRING, 12);
        pnd_Output_Pnd_Res_State1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State1", "#RES-STATE1", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country1", "#RES-COUNTRY1", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip1", "#RES-ZIP1", FieldType.STRING, 9);
        pnd_Output_Pnd_Nipr1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Nipr1", "#NIPR1", FieldType.STRING, 7);
        pnd_Output_Pnd_Co_Prod_Id1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Co_Prod_Id1", "#CO-PROD-ID1", FieldType.STRING, 12);
        pnd_Output_Pnd_Carrier_Appt_Stat1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Carrier_Appt_Stat1", "#CARRIER-APPT-STAT1", FieldType.STRING, 1);
        pnd_Output_Pnd_Dist_Channel1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel1", "#DIST-CHANNEL1", FieldType.STRING, 2);
        pnd_Output_Pnd_Dist_Channel_Sub_Type1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel_Sub_Type1", "#DIST-CHANNEL-SUB-TYPE1", FieldType.STRING, 
            10);
        pnd_Output_Pnd_Dist_Channel_Name1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel_Name1", "#DIST-CHANNEL-NAME1", FieldType.STRING, 20);
        pnd_Output_Pnd_Dist_Channel_Cde1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel_Cde1", "#DIST-CHANNEL-CDE1", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Id2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Id2", "#PARTY-ID2", FieldType.STRING, 12);
        pnd_Output_Pnd_Party_Type_Cde2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde2", "#PARTY-TYPE-CDE2", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key2", "#PARTY-KEY2", FieldType.STRING, 12);
        pnd_Output_Pnd_Res_State2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State2", "#RES-STATE2", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country2", "#RES-COUNTRY2", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip2", "#RES-ZIP2", FieldType.STRING, 9);
        pnd_Output_Pnd_Nipr2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Nipr2", "#NIPR2", FieldType.STRING, 7);
        pnd_Output_Pnd_Co_Prod_Id2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Co_Prod_Id2", "#CO-PROD-ID2", FieldType.STRING, 12);
        pnd_Output_Pnd_Carrier_Appt_Stat2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Carrier_Appt_Stat2", "#CARRIER-APPT-STAT2", FieldType.STRING, 1);
        pnd_Output_Pnd_Dist_Channel2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel2", "#DIST-CHANNEL2", FieldType.STRING, 2);
        pnd_Output_Pnd_Dist_Channel_Sub_Type2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel_Sub_Type2", "#DIST-CHANNEL-SUB-TYPE2", FieldType.STRING, 
            10);
        pnd_Output_Pnd_Dist_Channel_Name2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel_Name2", "#DIST-CHANNEL-NAME2", FieldType.STRING, 20);
        pnd_Output_Pnd_Dist_Channel_Cde2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Dist_Channel_Cde2", "#DIST-CHANNEL-CDE2", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Id3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Id3", "#PARTY-ID3", FieldType.STRING, 12);
        pnd_Output_Pnd_Party_Type_Cde3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde3", "#PARTY-TYPE-CDE3", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key3", "#PARTY-KEY3", FieldType.STRING, 12);
        pnd_Output_Pnd_Res_State3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State3", "#RES-STATE3", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country3", "#RES-COUNTRY3", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip3", "#RES-ZIP3", FieldType.STRING, 9);
        pnd_Output_Pnd_Party_Id4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Id4", "#PARTY-ID4", FieldType.STRING, 12);
        pnd_Output_Pnd_Party_Type_Cde4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde4", "#PARTY-TYPE-CDE4", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key4", "#PARTY-KEY4", FieldType.STRING, 12);
        pnd_Output_Pnd_Res_State4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State4", "#RES-STATE4", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country4", "#RES-COUNTRY4", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip4", "#RES-ZIP4", FieldType.STRING, 9);
        pnd_Output_Pnd_Party_Id5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Id5", "#PARTY-ID5", FieldType.STRING, 12);
        pnd_Output_Pnd_Party_Type_Cde5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde5", "#PARTY-TYPE-CDE5", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key5", "#PARTY-KEY5", FieldType.STRING, 12);
        pnd_Output_Pnd_Res_State5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State5", "#RES-STATE5", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country5", "#RES-COUNTRY5", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip5", "#RES-ZIP5", FieldType.STRING, 9);
        pnd_Output_Pnd_Party_Id6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Id6", "#PARTY-ID6", FieldType.STRING, 12);
        pnd_Output_Pnd_Party_Type_Cde6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde6", "#PARTY-TYPE-CDE6", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key6", "#PARTY-KEY6", FieldType.STRING, 12);
        pnd_Output_Pnd_Full_Nme6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Full_Nme6", "#FULL-NME6", FieldType.STRING, 12);
        pnd_Output_Pnd_Gov_Id6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Gov_Id6", "#GOV-ID6", FieldType.STRING, 10);
        pnd_Output_Pnd_Gov_Id_Type_Cde6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Gov_Id_Type_Cde6", "#GOV-ID-TYPE-CDE6", FieldType.STRING, 1);
        pnd_Output_Pnd_Res_State6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State6", "#RES-STATE6", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country6", "#RES-COUNTRY6", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip6", "#RES-ZIP6", FieldType.STRING, 9);
        pnd_Output_Pnd_Party_Id7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Id7", "#PARTY-ID7", FieldType.STRING, 12);
        pnd_Output_Pnd_Party_Type_Cde7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde7", "#PARTY-TYPE-CDE7", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key7", "#PARTY-KEY7", FieldType.STRING, 12);
        pnd_Output_Pnd_Full_Nme7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Full_Nme7", "#FULL-NME7", FieldType.STRING, 12);
        pnd_Output_Pnd_Gov_Id7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Gov_Id7", "#GOV-ID7", FieldType.STRING, 10);
        pnd_Output_Pnd_Gov_Id_Type_Cde7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Gov_Id_Type_Cde7", "#GOV-ID-TYPE-CDE7", FieldType.STRING, 1);
        pnd_Output_Pnd_Res_State7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State7", "#RES-STATE7", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country7", "#RES-COUNTRY7", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip7", "#RES-ZIP7", FieldType.STRING, 9);
        pnd_Output_Pnd_Party_Type_Cde8 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde8", "#PARTY-TYPE-CDE8", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key8 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key8", "#PARTY-KEY8", FieldType.STRING, 12);
        pnd_Output_Pnd_Full_Nme8 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Full_Nme8", "#FULL-NME8", FieldType.STRING, 12);
        pnd_Output_Pnd_Res_State8 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State8", "#RES-STATE8", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country8 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country8", "#RES-COUNTRY8", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip8 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip8", "#RES-ZIP8", FieldType.STRING, 9);
        pnd_Output_Pnd_Party_Type_Cde9 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Type_Cde9", "#PARTY-TYPE-CDE9", FieldType.STRING, 1);
        pnd_Output_Pnd_Party_Key9 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Party_Key9", "#PARTY-KEY9", FieldType.STRING, 12);
        pnd_Output_Pnd_Full_Nme9 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Full_Nme9", "#FULL-NME9", FieldType.STRING, 12);
        pnd_Output_Pnd_Res_State9 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_State9", "#RES-STATE9", FieldType.STRING, 2);
        pnd_Output_Pnd_Res_Country9 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Country9", "#RES-COUNTRY9", FieldType.STRING, 9);
        pnd_Output_Pnd_Res_Zip9 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Res_Zip9", "#RES-ZIP9", FieldType.STRING, 9);
        pnd_Output_Pnd_Rel_Id1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Id1", "#REL-ID1", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Id1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Id1", "#ORIG-OBJECT-ID1", FieldType.STRING, 12);
        pnd_Output_Pnd_Rel_Object_Id1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Id1", "#REL-OBJECT-ID1", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Type1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Type1", "#ORIG-OBJECT-TYPE1", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Object_Type1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Type1", "#REL-OBJECT-TYPE1", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Role_Code1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Role_Code1", "#REL-ROLE-CODE1", FieldType.STRING, 2);
        pnd_Output_Pnd_Int_Pct1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Int_Pct1", "#INT-PCT1", FieldType.STRING, 6);
        pnd_Output_Pnd_Rel_Id2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Id2", "#REL-ID2", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Id2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Id2", "#ORIG-OBJECT-ID2", FieldType.STRING, 12);
        pnd_Output_Pnd_Rel_Object_Id2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Id2", "#REL-OBJECT-ID2", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Type2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Type2", "#ORIG-OBJECT-TYPE2", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Object_Type2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Type2", "#REL-OBJECT-TYPE2", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Role_Code2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Role_Code2", "#REL-ROLE-CODE2", FieldType.STRING, 2);
        pnd_Output_Pnd_Int_Pct2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Int_Pct2", "#INT-PCT2", FieldType.STRING, 6);
        pnd_Output_Pnd_Rel_Id3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Id3", "#REL-ID3", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Id3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Id3", "#ORIG-OBJECT-ID3", FieldType.STRING, 12);
        pnd_Output_Pnd_Rel_Object_Id3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Id3", "#REL-OBJECT-ID3", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Type3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Type3", "#ORIG-OBJECT-TYPE3", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Object_Type3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Type3", "#REL-OBJECT-TYPE3", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Role_Code3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Role_Code3", "#REL-ROLE-CODE3", FieldType.STRING, 2);
        pnd_Output_Pnd_Rel_Id4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Id4", "#REL-ID4", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Id4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Id4", "#ORIG-OBJECT-ID4", FieldType.STRING, 12);
        pnd_Output_Pnd_Rel_Object_Id4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Id4", "#REL-OBJECT-ID4", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Type4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Type4", "#ORIG-OBJECT-TYPE4", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Object_Type4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Type4", "#REL-OBJECT-TYPE4", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Role_Code4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Role_Code4", "#REL-ROLE-CODE4", FieldType.STRING, 2);
        pnd_Output_Pnd_Rel_Id5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Id5", "#REL-ID5", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Id5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Id5", "#ORIG-OBJECT-ID5", FieldType.STRING, 12);
        pnd_Output_Pnd_Rel_Object_Id5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Id5", "#REL-OBJECT-ID5", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Type5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Type5", "#ORIG-OBJECT-TYPE5", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Object_Type5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Type5", "#REL-OBJECT-TYPE5", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Role_Code5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Role_Code5", "#REL-ROLE-CODE5", FieldType.STRING, 2);
        pnd_Output_Pnd_Rel_Id6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Id6", "#REL-ID6", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Id6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Id6", "#ORIG-OBJECT-ID6", FieldType.STRING, 12);
        pnd_Output_Pnd_Rel_Object_Id6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Id6", "#REL-OBJECT-ID6", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Type6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Type6", "#ORIG-OBJECT-TYPE6", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Object_Type6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Type6", "#REL-OBJECT-TYPE6", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Role_Code6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Role_Code6", "#REL-ROLE-CODE6", FieldType.STRING, 2);
        pnd_Output_Pnd_Rel_Id7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Id7", "#REL-ID7", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Id7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Id7", "#ORIG-OBJECT-ID7", FieldType.STRING, 12);
        pnd_Output_Pnd_Rel_Object_Id7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Id7", "#REL-OBJECT-ID7", FieldType.STRING, 12);
        pnd_Output_Pnd_Orig_Object_Type7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Object_Type7", "#ORIG-OBJECT-TYPE7", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Object_Type7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Object_Type7", "#REL-OBJECT-TYPE7", FieldType.STRING, 1);
        pnd_Output_Pnd_Rel_Role_Code7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rel_Role_Code7", "#REL-ROLE-CODE7", FieldType.STRING, 2);
        pnd_Output_Pnd_Owner_Dob = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Owner_Dob", "#OWNER-DOB", FieldType.STRING, 8);
        pnd_Output_Pnd_Jnt_Owner_Dob = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Jnt_Owner_Dob", "#JNT-OWNER-DOB", FieldType.STRING, 8);
        pnd_Output_Pnd_Txn_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Txn_Type", "#TXN-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Txn_Nbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Txn_Nbr", "#TXN-NBR", FieldType.NUMERIC, 3);
        pnd_Output_Pnd_Premium_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Premium_Amt", "#PREMIUM-AMT", FieldType.STRING, 10);

        this.setRecordName("LdaIaalimra");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Output_Pnd_Trans_Type.setInitialValue("180");
        pnd_Output_Pnd_Trans_Subtype.setInitialValue("18002");
        pnd_Output_Pnd_Trans_Exe_Tme.setInitialValue(Global.getTIME());
        pnd_Output_Pnd_Trans_Mode.setInitialValue("2");
        pnd_Output_Pnd_Inq_Level.setInitialValue("3");
        pnd_Output_Pnd_Pend_Response.setInitialValue("0");
        pnd_Output_Pnd_No_Response.setInitialValue("1");
        pnd_Output_Pnd_Test_Ind.setInitialValue("0");
        pnd_Output_Pnd_Primary_Object_Typ.setInitialValue("4");
        pnd_Output_Pnd_Creat_Tme.setInitialValue(Global.getTIME());
        pnd_Output_Pnd_Holding_Type_Cde.setInitialValue("2");
        pnd_Output_Pnd_Lob.setInitialValue("2");
        pnd_Output_Pnd_Issue_Nation.setInitialValue("1");
    }

    // Constructor
    public LdaIaalimra() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
