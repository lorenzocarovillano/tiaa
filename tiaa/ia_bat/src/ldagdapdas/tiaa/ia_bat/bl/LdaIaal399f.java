/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:01:01 PM
**        * FROM NATURAL LDA     : IAAL399F
************************************************************
**        * FILE NAME            : LdaIaal399f.java
**        * CLASS NAME           : LdaIaal399f
**        * INSTANCE NAME        : LdaIaal399f
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal399f extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tckr_Symbl;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind;
    private DbsGroup pnd_Work_Record_Out;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Record_Code;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Cmpny_Cde;
    private DbsGroup pnd_Work_Record_Out_Pnd_W1_Cmpny_CdeRedef2;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Fund_Type;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Fund_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Rtb_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Rate_Cde;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Rate_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Per_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Units_Cnt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Xfr_Iss_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Lst_Pd_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_Out_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_In_Dte;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Rate_Count;
    private DbsField pnd_Work_Record_Out_Pnd_W1_Original_Payment;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Units_Cnt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Per_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Per_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Prior_Payment;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W2_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Units_Cnt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_Out_Pnd_W3_Prior_Payment;

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd() { return vw_iaa_Tiaa_Fund_Rcrd; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tckr_Symbl() { return iaa_Tiaa_Fund_Rcrd_Tckr_Symbl; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind; }

    public DbsGroup getPnd_Work_Record_Out() { return pnd_Work_Record_Out; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde() { return pnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Record_Code() { return pnd_Work_Record_Out_Pnd_W1_Record_Code; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Cmpny_Cde() { return pnd_Work_Record_Out_Pnd_W1_Cmpny_Cde; }

    public DbsGroup getPnd_Work_Record_Out_Pnd_W1_Cmpny_CdeRedef2() { return pnd_Work_Record_Out_Pnd_W1_Cmpny_CdeRedef2; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Fund_Type() { return pnd_Work_Record_Out_Pnd_W1_Fund_Type; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Fund_Cde() { return pnd_Work_Record_Out_Pnd_W1_Fund_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Per_Ivc_Amt() { return pnd_Work_Record_Out_Pnd_W1_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Rtb_Amt() { return pnd_Work_Record_Out_Pnd_W1_Rtb_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Rate_Cde() { return pnd_Work_Record_Out_Pnd_W1_Rate_Cde; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Rate_Dte() { return pnd_Work_Record_Out_Pnd_W1_Rate_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Per_Div_Amt() { return pnd_Work_Record_Out_Pnd_W1_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Units_Cnt() { return pnd_Work_Record_Out_Pnd_W1_Units_Cnt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Rate_Final_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W1_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Rate_Final_Div_Amt() { return pnd_Work_Record_Out_Pnd_W1_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte() { return pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Xfr_Iss_Dte() { return pnd_Work_Record_Out_Pnd_W1_Xfr_Iss_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Lst_Pd_Dte() { return pnd_Work_Record_Out_Pnd_W2_Lst_Pd_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Lst_Xfr_Out_Dte() { return pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_Out_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Lst_Xfr_In_Dte() { return pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_In_Dte; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Rate_Count() { return pnd_Work_Record_Out_Pnd_W1_Rate_Count; }

    public DbsField getPnd_Work_Record_Out_Pnd_W1_Original_Payment() { return pnd_Work_Record_Out_Pnd_W1_Original_Payment; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Units_Cnt() { return pnd_Work_Record_Out_Pnd_W2_Units_Cnt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Per_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W2_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Per_Div_Amt() { return pnd_Work_Record_Out_Pnd_W2_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Prior_Payment() { return pnd_Work_Record_Out_Pnd_W2_Prior_Payment; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rate_Final_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W2_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W2_Rate_Final_Div_Amt() { return pnd_Work_Record_Out_Pnd_W2_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Units_Cnt() { return pnd_Work_Record_Out_Pnd_W3_Units_Cnt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Per_Div_Amt() { return pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt() { return pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt() { return pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_Out_Pnd_W3_Prior_Payment() { return pnd_Work_Record_Out_Pnd_W3_Prior_Payment; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_Tckr_Symbl = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");

        pnd_Work_Record_Out = newGroupInRecord("pnd_Work_Record_Out", "#WORK-RECORD-OUT");
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr", "#W1-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde", "#W1-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Out_Pnd_W1_Record_Code = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Record_Code", "#W1-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Out_Pnd_W1_Cmpny_Cde = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Cmpny_Cde", "#W1-CMPNY-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_Out_Pnd_W1_Cmpny_CdeRedef2 = pnd_Work_Record_Out.newGroupInGroup("pnd_Work_Record_Out_Pnd_W1_Cmpny_CdeRedef2", "Redefines", pnd_Work_Record_Out_Pnd_W1_Cmpny_Cde);
        pnd_Work_Record_Out_Pnd_W1_Fund_Type = pnd_Work_Record_Out_Pnd_W1_Cmpny_CdeRedef2.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Fund_Type", "#W1-FUND-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Out_Pnd_W1_Fund_Cde = pnd_Work_Record_Out_Pnd_W1_Cmpny_CdeRedef2.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Fund_Cde", "#W1-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Work_Record_Out_Pnd_W1_Per_Ivc_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Per_Ivc_Amt", "#W1-PER-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W1_Rtb_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Rtb_Amt", "#W1-RTB-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W1_Rate_Cde = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Rate_Cde", "#W1-RATE-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record_Out_Pnd_W1_Rate_Dte = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Rate_Dte", "#W1-RATE-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt", "#W1-PER-PAY-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W1_Per_Div_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Per_Div_Amt", "#W1-PER-DIV-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W1_Units_Cnt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Units_Cnt", "#W1-UNITS-CNT", FieldType.DECIMAL, 
            9,3);
        pnd_Work_Record_Out_Pnd_W1_Rate_Final_Pay_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Rate_Final_Pay_Amt", "#W1-RATE-FINAL-PAY-AMT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W1_Rate_Final_Div_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Rate_Final_Div_Amt", "#W1-RATE-FINAL-DIV-AMT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte", "#W1-LST-TRANS-DTE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Out_Pnd_W1_Xfr_Iss_Dte = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Xfr_Iss_Dte", "#W1-XFR-ISS-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Out_Pnd_W2_Lst_Pd_Dte = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Lst_Pd_Dte", "#W2-LST-PD-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_Out_Dte = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_Out_Dte", "#W1-LST-XFR-OUT-DTE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_In_Dte = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Lst_Xfr_In_Dte", "#W1-LST-XFR-IN-DTE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Out_Pnd_W1_Rate_Count = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Rate_Count", "#W1-RATE-COUNT", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Out_Pnd_W1_Original_Payment = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W1_Original_Payment", "#W1-ORIGINAL-PAYMENT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W2_Units_Cnt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Units_Cnt", "#W2-UNITS-CNT", FieldType.DECIMAL, 
            9,3);
        pnd_Work_Record_Out_Pnd_W2_Per_Pay_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Per_Pay_Amt", "#W2-PER-PAY-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W2_Per_Div_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Per_Div_Amt", "#W2-PER-DIV-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W2_Prior_Payment = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Prior_Payment", "#W2-PRIOR-PAYMENT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W2_Rate_Final_Pay_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rate_Final_Pay_Amt", "#W2-RATE-FINAL-PAY-AMT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W2_Rate_Final_Div_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W2_Rate_Final_Div_Amt", "#W2-RATE-FINAL-DIV-AMT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Units_Cnt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Units_Cnt", "#W3-UNITS-CNT", FieldType.DECIMAL, 
            9,3);
        pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Per_Pay_Amt", "#W3-PER-PAY-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Per_Div_Amt", "#W3-PER-DIV-AMT", FieldType.DECIMAL, 
            9,2);
        pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Rate_Final_Pay_Amt", "#W3-RATE-FINAL-PAY-AMT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Rate_Final_Div_Amt", "#W3-RATE-FINAL-DIV-AMT", 
            FieldType.DECIMAL, 9,2);
        pnd_Work_Record_Out_Pnd_W3_Prior_Payment = pnd_Work_Record_Out.newFieldInGroup("pnd_Work_Record_Out_Pnd_W3_Prior_Payment", "#W3-PRIOR-PAYMENT", 
            FieldType.DECIMAL, 9,2);
        vw_iaa_Tiaa_Fund_Rcrd.setUniquePeList();

        this.setRecordName("LdaIaal399f");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
    }

    // Constructor
    public LdaIaal399f() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
