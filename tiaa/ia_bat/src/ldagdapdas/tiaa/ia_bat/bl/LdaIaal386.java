/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:59 PM
**        * FROM NATURAL LDA     : IAAL386
************************************************************
**        * FILE NAME            : LdaIaal386.java
**        * CLASS NAME           : LdaIaal386
**        * INSTANCE NAME        : LdaIaal386
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal386 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Old_Tiaa_Rates_View;
    private DbsField iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_View_Trans_Dte;
    private DbsField iaa_Old_Tiaa_Rates_View_Trans_User_Area;
    private DbsField iaa_Old_Tiaa_Rates_View_Trans_User_Id;
    private DbsField iaa_Old_Tiaa_Rates_View_Trans_Verify_Id;
    private DbsField iaa_Old_Tiaa_Rates_View_Trans_Verify_Dte;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde;
    private DbsField iaa_Old_Tiaa_Rates_View_Rcrd_Srce;
    private DbsField iaa_Old_Tiaa_Rates_View_Rcrd_Status;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Tiaa_Rate_CdeMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Cde;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Tiaa_Rate_DteMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Dte;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_AmtMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_Amt;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_AmtMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_Amt;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_AmtMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_Amt;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_AmtMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Old_Tiaa_Rates_View_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Gic;
    private DbsField iaa_Old_Tiaa_Rates_View_Lst_Trans_Dte;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Mode_Ind;
    private DbsField iaa_Old_Tiaa_Rates_View_Tiaa_Old_Cmpny_Fund;

    public DataAccessProgramView getVw_iaa_Old_Tiaa_Rates_View() { return vw_iaa_Old_Tiaa_Rates_View; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte() { return iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte() { return iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Trans_Dte() { return iaa_Old_Tiaa_Rates_View_Trans_Dte; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Trans_User_Area() { return iaa_Old_Tiaa_Rates_View_Trans_User_Area; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Trans_User_Id() { return iaa_Old_Tiaa_Rates_View_Trans_User_Id; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Trans_Verify_Id() { return iaa_Old_Tiaa_Rates_View_Trans_Verify_Id; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Trans_Verify_Dte() { return iaa_Old_Tiaa_Rates_View_Trans_Verify_Dte; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr() { return iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde() { return iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Mode_Ind() { return iaa_Old_Tiaa_Rates_View_Cntrct_Mode_Ind; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde() { return iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Rcrd_Srce() { return iaa_Old_Tiaa_Rates_View_Rcrd_Srce; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Rcrd_Status() { return iaa_Old_Tiaa_Rates_View_Rcrd_Status; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt() { return iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt() { return iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_CdeMuGroup() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_CdeMuGroup; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_Cde() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Cde; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_DteMuGroup() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_DteMuGroup; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_Dte() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Dte; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_AmtMuGroup() { return iaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_AmtMuGroup; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_Amt() { return iaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_Amt; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_AmtMuGroup() { return iaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_AmtMuGroup; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_Amt() { return iaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_Amt; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_AmtMuGroup() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_AmtMuGroup; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_Amt() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_Amt; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_AmtMuGroup() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_AmtMuGroup; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_Amt() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_Amt; }

    public DbsGroup getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_GicMuGroup() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Rate_Gic() { return iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Gic; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Lst_Trans_Dte() { return iaa_Old_Tiaa_Rates_View_Lst_Trans_Dte; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Mode_Ind() { return iaa_Old_Tiaa_Rates_View_Tiaa_Mode_Ind; }

    public DbsField getIaa_Old_Tiaa_Rates_View_Tiaa_Old_Cmpny_Fund() { return iaa_Old_Tiaa_Rates_View_Tiaa_Old_Cmpny_Fund; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Old_Tiaa_Rates_View = new DataAccessProgramView(new NameInfo("vw_iaa_Old_Tiaa_Rates_View", "IAA-OLD-TIAA-RATES-VIEW"), "IAA_OLD_TIAA_RATES", 
            "IA_OLD_RATES", DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_TIAA_RATES"));
        iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_View_Trans_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Old_Tiaa_Rates_View_Trans_User_Area = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Trans_User_Area", "TRANS-USER-AREA", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Old_Tiaa_Rates_View_Trans_User_Id = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Trans_User_Id", "TRANS-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Old_Tiaa_Rates_View_Trans_Verify_Id = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Trans_Verify_Id", "TRANS-VERIFY-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Old_Tiaa_Rates_View_Trans_Verify_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Trans_Verify_Dte", 
            "TRANS-VERIFY-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Old_Tiaa_Rates_View_Cntrct_Mode_Ind = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");
        iaa_Old_Tiaa_Rates_View_Rcrd_Srce = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_SRCE");
        iaa_Old_Tiaa_Rates_View_Rcrd_Status = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Rcrd_Status", "RCRD-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RCRD_STATUS");
        iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt", 
            "CNTRCT-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt", 
            "CNTRCT-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Cde = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldArrayInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldArrayInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldArrayInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Per_Pay_Amt", 
            "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldArrayInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Per_Div_Amt", 
            "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldArrayInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_Amt = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldArrayInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_View_Tiaa_Rate_GicMuGroup = vw_iaa_Old_Tiaa_Rates_View.getRecord().newGroupInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Rate_GicMuGroup", 
            "TIAA_RATE_GICMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_OLD_RATES_TIAA_RATE_GIC");
        iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Gic = iaa_Old_Tiaa_Rates_View_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Rate_Gic", 
            "TIAA-RATE-GIC", FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Old_Tiaa_Rates_View_Lst_Trans_Dte = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Old_Tiaa_Rates_View_Tiaa_Mode_Ind = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Mode_Ind", "TIAA-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Old_Tiaa_Rates_View_Tiaa_Old_Cmpny_Fund = vw_iaa_Old_Tiaa_Rates_View.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_View_Tiaa_Old_Cmpny_Fund", 
            "TIAA-OLD-CMPNY-FUND", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        vw_iaa_Old_Tiaa_Rates_View.setUniquePeList();

        this.setRecordName("LdaIaal386");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Old_Tiaa_Rates_View.reset();
    }

    // Constructor
    public LdaIaal386() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
