/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:41 PM
**        * FROM NATURAL LDA     : IAAL902B
************************************************************
**        * FILE NAME            : LdaIaal902b.java
**        * CLASS NAME           : LdaIaal902b
**        * INSTANCE NAME        : LdaIaal902b
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal902b extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsGroup iaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos1;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos2;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos3;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2;
    private DbsField iaa_Cntrct_Filler1;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Yy;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Mm;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dd;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4;
    private DbsField iaa_Cntrct_Filler2;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Plan_Nmbr;
    private DbsField iaa_Cntrct_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Cntrct_Nmbr;
    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte;
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Trans_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef6;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef7;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund;
    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd_1;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde;
    private DbsGroup iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef8;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp;
    private DbsGroup iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Mode_Ind;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Fund_Key;
    private DataAccessProgramView vw_iaa_Cref_Fund_Trans_1;
    private DbsField iaa_Cref_Fund_Trans_1_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde;
    private DbsGroup iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef9;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp;
    private DbsGroup iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Mode_Ind;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Old_Cmpny_Fund;
    private DbsField iaa_Cref_Fund_Trans_1_Bfre_Imge_Id;
    private DbsField pnd_Coding_Sheets;
    private DbsGroup pnd_Coding_SheetsRedef10;
    private DbsField pnd_Coding_Sheets_Pnd_Manual_New_Issue1;
    private DbsGroup pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11;
    private DbsField pnd_Coding_Sheets_Pnd_Batch_Nbr1;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte1;
    private DbsGroup pnd_Coding_Sheets_Pnd_Check_Dte1Redef12;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Mm1;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Dd1;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Yy1;
    private DbsField pnd_Coding_Sheets_Pnd_Cntrct_Nbr1;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Status1;
    private DbsField pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr1;
    private DbsField pnd_Coding_Sheets_Pnd_Trans_Nbr1;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Type1;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Nbr1;
    private DbsField pnd_Coding_Sheets_Pnd_Rest_Of_Issue1;
    private DbsGroup pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13;
    private DbsField pnd_Coding_Sheets_Pnd_Product;
    private DbsField pnd_Coding_Sheets_Pnd_Currency;
    private DbsGroup pnd_Coding_Sheets_Pnd_CurrencyRedef14;
    private DbsField pnd_Coding_Sheets_Pnd_Currency_N;
    private DbsField pnd_Coding_Sheets_Pnd_Mode;
    private DbsGroup pnd_Coding_Sheets_Pnd_ModeRedef15;
    private DbsField pnd_Coding_Sheets_Pnd_Mode_N;
    private DbsField pnd_Coding_Sheets_Pnd_Pend_Code;
    private DbsField pnd_Coding_Sheets_Pnd_Hold_Check_Cde;
    private DbsField pnd_Coding_Sheets_Pnd_Pend_Dte;
    private DbsGroup pnd_Coding_Sheets_Pnd_Pend_DteRedef16;
    private DbsField pnd_Coding_Sheets_Pnd_Pend_Dte_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_Pend_Dte_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Option;
    private DbsGroup pnd_Coding_Sheets_Pnd_OptionRedef17;
    private DbsField pnd_Coding_Sheets_Pnd_Option_N;
    private DbsField pnd_Coding_Sheets_Pnd_Origin;
    private DbsField pnd_Coding_Sheets_Pnd_Iss_Dte;
    private DbsGroup pnd_Coding_Sheets_Pnd_Iss_DteRedef18;
    private DbsField pnd_Coding_Sheets_Pnd_Iss_Dte_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_Iss_Dte_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte;
    private DbsGroup pnd_Coding_Sheets_Pnd_1st_Pay_Due_DteRedef19;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte;
    private DbsGroup pnd_Coding_Sheets_Pnd_Lst_Man_Chk_DteRedef20;
    private DbsField pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte;
    private DbsGroup pnd_Coding_Sheets_Pnd_Fin_Per_Pay_DteRedef21;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte;
    private DbsGroup pnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Wdrawal_Dte;
    private DbsGroup pnd_Coding_Sheets_Pnd_Wdrawal_DteRedef23;
    private DbsField pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Filler;
    private DbsField pnd_Coding_Sheets_Pnd_User_Area1;
    private DbsGroup pnd_Coding_SheetsRedef24;
    private DbsField pnd_Coding_Sheets_Pnd_Manual_New_Issue2;
    private DbsGroup pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25;
    private DbsField pnd_Coding_Sheets_Pnd_Batch_Nbr2;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte2;
    private DbsGroup pnd_Coding_Sheets_Pnd_Check_Dte2Redef26;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Mm2;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Dd2;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Yy2;
    private DbsField pnd_Coding_Sheets_Pnd_Cntrct_Nbr2;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Status2;
    private DbsField pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr2;
    private DbsField pnd_Coding_Sheets_Pnd_Trans_Nbr2;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Type2;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Nbr2;
    private DbsField pnd_Coding_Sheets_Pnd_Rest_Of_Issue2;
    private DbsGroup pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27;
    private DbsField pnd_Coding_Sheets_Pnd_Citizen;
    private DbsField pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Iss;
    private DbsField pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Res;
    private DbsField pnd_Coding_Sheets_Pnd_Coll_Iss;
    private DbsField pnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt;
    private DbsField pnd_Coding_Sheets_Pnd_Ssn;
    private DbsField pnd_Coding_Sheets_Pnd_Joint_Cnvrt;
    private DbsField pnd_Coding_Sheets_Pnd_Spirt;
    private DbsField pnd_Coding_Sheets_Pnd_Pen_Pln_Cde;
    private DbsField pnd_Coding_Sheets_Pnd_Cntrct_Type;
    private DbsField pnd_Coding_Sheets_Pnd_Filler1;
    private DbsField pnd_Coding_Sheets_Pnd_User_Area2;
    private DbsGroup pnd_Coding_SheetsRedef28;
    private DbsField pnd_Coding_Sheets_Pnd_Manual_New_Issue3;
    private DbsGroup pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29;
    private DbsField pnd_Coding_Sheets_Pnd_Batch_Nbr3;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte3;
    private DbsGroup pnd_Coding_Sheets_Pnd_Check_Dte3Redef30;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Mm3;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Dd3;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Yy3;
    private DbsField pnd_Coding_Sheets_Pnd_Cntrct_Nbr3;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Status3;
    private DbsField pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr3;
    private DbsField pnd_Coding_Sheets_Pnd_Trans_Nbr3;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Type3;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Nbr3;
    private DbsField pnd_Coding_Sheets_Pnd_Rest_Of_Issue3;
    private DbsGroup pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Annt_X_Ref;
    private DbsGroup pnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Last_Name;
    private DbsField pnd_Coding_Sheets_Pnd_1st_First_Init;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Mid_Init;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Sex;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Dob;
    private DbsGroup pnd_Coding_Sheets_Pnd_1st_DobRedef33;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Dob_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Dob_Dd;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Dob_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Dod;
    private DbsGroup pnd_Coding_Sheets_Pnd_1st_DodRedef34;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Dod_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_1st_Dod_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Mort_Yob3;
    private DbsGroup pnd_Coding_Sheets_Pnd_Mort_Yob3Redef35;
    private DbsField pnd_Coding_Sheets_Pnd_Mort_Yob3_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Life_Cnt3;
    private DbsField pnd_Coding_Sheets_Pnd_Div_Payee;
    private DbsField pnd_Coding_Sheets_Pnd_Coll_Cde;
    private DbsField pnd_Coding_Sheets_Pnd_Orig_Cntrct;
    private DbsField pnd_Coding_Sheets_Pnd_Cash_Cde;
    private DbsField pnd_Coding_Sheets_Pnd_Emp_Term;
    private DbsField pnd_Coding_Sheets_Pnd_Filler2;
    private DbsField pnd_Coding_Sheets_Pnd_User_Area3;
    private DbsGroup pnd_Coding_SheetsRedef36;
    private DbsField pnd_Coding_Sheets_Pnd_Manual_New_Issue4;
    private DbsGroup pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37;
    private DbsField pnd_Coding_Sheets_Pnd_Batch_Nbr4;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte4;
    private DbsGroup pnd_Coding_Sheets_Pnd_Check_Dte4Redef38;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Mm4;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Dd4;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Yy4;
    private DbsField pnd_Coding_Sheets_Pnd_Cntrct_Nbr4;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Status4;
    private DbsField pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr4;
    private DbsField pnd_Coding_Sheets_Pnd_Trans_Nbr4;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Type4;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Nbr4;
    private DbsField pnd_Coding_Sheets_Pnd_Rest_Of_Issue4;
    private DbsGroup pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref;
    private DbsGroup pnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Last_Name;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_First_Init;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Mid_Init;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Sex;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Dob;
    private DbsGroup pnd_Coding_Sheets_Pnd_2nd_DobRedef41;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Dob_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Dob_Dd;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Dob_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Dod;
    private DbsGroup pnd_Coding_Sheets_Pnd_2nd_DodRedef42;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Dod_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_2nd_Dod_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Mort_Yob4;
    private DbsGroup pnd_Coding_Sheets_Pnd_Mort_Yob4Redef43;
    private DbsField pnd_Coding_Sheets_Pnd_Mort_Yob4_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Life_Cnt4;
    private DbsField pnd_Coding_Sheets_Pnd_Ben_Xref;
    private DbsField pnd_Coding_Sheets_Pnd_Ben_Dod;
    private DbsGroup pnd_Coding_Sheets_Pnd_Ben_DodRedef44;
    private DbsField pnd_Coding_Sheets_Pnd_Ben_Dod_A;
    private DbsGroup pnd_Coding_Sheets_Pnd_Ben_DodRedef45;
    private DbsField pnd_Coding_Sheets_Pnd_Ben_Dod_Mm;
    private DbsField pnd_Coding_Sheets_Pnd_Ben_Dod_Yy;
    private DbsField pnd_Coding_Sheets_Pnd_Dest_Prev;
    private DbsField pnd_Coding_Sheets_Pnd_Dest_Curr;
    private DbsField pnd_Coding_Sheets_Pnd_User_Area4;
    private DbsGroup pnd_Coding_SheetsRedef46;
    private DbsField pnd_Coding_Sheets_Pnd_Manual_New_Issue5;
    private DbsGroup pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47;
    private DbsField pnd_Coding_Sheets_Pnd_Batch_Nbr5;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte5;
    private DbsGroup pnd_Coding_Sheets_Pnd_Check_Dte5Redef48;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Mm5;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Dd5;
    private DbsField pnd_Coding_Sheets_Pnd_Check_Dte_Yy5;
    private DbsField pnd_Coding_Sheets_Pnd_Cntrct_Nbr5;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Status5;
    private DbsField pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr5;
    private DbsField pnd_Coding_Sheets_Pnd_Trans_Nbr5;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Type5;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Nbr5;
    private DbsGroup pnd_Coding_Sheets_Pnd_Record_Nbr5Redef49;
    private DbsField pnd_Coding_Sheets_Pnd_Record_Nbr5_N;
    private DbsField pnd_Coding_Sheets_Pnd_Rest_Of_Issue5;
    private DbsGroup pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50;
    private DbsField pnd_Coding_Sheets_Pnd_Rate;
    private DbsField pnd_Coding_Sheets_Pnd_Per_Pay;
    private DbsGroup pnd_Coding_Sheets_Pnd_Per_PayRedef51;
    private DbsField pnd_Coding_Sheets_Pnd_Cref_Units;
    private DbsField pnd_Coding_Sheets_Pnd_Per_Div;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Pay;
    private DbsGroup pnd_Coding_Sheets_Pnd_Fin_PayRedef52;
    private DbsField pnd_Coding_Sheets_Pnd_Fin_Pay_A;
    private DbsField pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div;
    private DbsGroup pnd_Coding_Sheets_Pnd_Tot_Old_Per_DivRedef53;
    private DbsField pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div_A;
    private DbsField pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay;
    private DbsGroup pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef54;
    private DbsField pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay_A;
    private DbsGroup pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef55;
    private DbsField pnd_Coding_Sheets_Pnd_Tot_Old_Cref_Units;
    private DbsField pnd_Coding_Sheets_Pnd_User_Area5;
    private DbsField pnd_Cntrct_Key;
    private DbsGroup pnd_Cntrct_KeyRedef56;
    private DbsField pnd_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Key_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Cntrct_Payee_Key;
    private DbsGroup pnd_Cntrct_Payee_KeyRedef57;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cntrct_Bfre_Key;
    private DbsGroup pnd_Cntrct_Bfre_KeyRedef58;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Cntrct_Aftr_Key;
    private DbsGroup pnd_Cntrct_Aftr_KeyRedef59;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Cpr_Bfre_Key;
    private DbsGroup pnd_Cpr_Bfre_KeyRedef60;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Cpr_Aftr_Key;
    private DbsGroup pnd_Cpr_Aftr_KeyRedef61;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Cref_Fund_Key;
    private DbsGroup pnd_Cref_Fund_KeyRedef62;
    private DbsField pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Payee_Cde;
    private DbsField pnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde;
    private DbsField pnd_Tiaa_Fund_Key;
    private DbsGroup pnd_Tiaa_Fund_KeyRedef63;
    private DbsField pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde;
    private DbsField pnd_Cref_Fund_Trans;
    private DbsGroup pnd_Cref_Fund_TransRedef64;
    private DbsField pnd_Cref_Fund_Trans_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Payee_Cde;
    private DbsField pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Fund_Cde;
    private DbsField pnd_Cref_Fund_Trans_Pnd_Trans_Dte;
    private DbsField pnd_Tiaa_Fund_Trans;
    private DbsGroup pnd_Tiaa_Fund_TransRedef65;
    private DbsField pnd_Tiaa_Fund_Trans_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Payee_Cde;
    private DbsField pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Fund_Cde;
    private DbsField pnd_Tiaa_Fund_Trans_Pnd_Trans_Dte;
    private DbsField pnd_Tiaa_Fund_Bfre_Key;
    private DbsGroup pnd_Tiaa_Fund_Bfre_KeyRedef66;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Fund_Cde;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Cref_Fund_Bfre_Key;
    private DbsGroup pnd_Cref_Fund_Bfre_KeyRedef67;
    private DbsField pnd_Cref_Fund_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Payee_Cde;
    private DbsField pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Fund_Cde;
    private DbsField pnd_Cref_Fund_Bfre_Key_Pnd_Trans_Dte;
    private DbsGroup pnd_Misc_Variables;
    private DbsField pnd_Misc_Variables_Pnd_Batch_Nbr;
    private DbsField pnd_Misc_Variables_Pnd_Check_Dte_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Check_Dte_Dd;
    private DbsField pnd_Misc_Variables_Pnd_Check_Dte_Yy;
    private DbsField pnd_Misc_Variables_Pnd_Cntrct_Nbr;
    private DbsField pnd_Misc_Variables_Pnd_Record_Status;
    private DbsField pnd_Misc_Variables_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Misc_Variables_Pnd_Rec_Type;
    private DbsField pnd_Misc_Variables_Pnd_Trans_Nbr;
    private DbsField pnd_Misc_Variables_Pnd_Ccyymm_Dte;
    private DbsGroup pnd_Misc_Variables_Pnd_Ccyymm_DteRedef68;
    private DbsField pnd_Misc_Variables_Pnd_Ccyymm_Dte_Cc;
    private DbsField pnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy;
    private DbsField pnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8;
    private DbsGroup pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69;
    private DbsField pnd_Misc_Variables_Pnd_Dte8_Cc;
    private DbsField pnd_Misc_Variables_Pnd_Dte8_Yy;
    private DbsField pnd_Misc_Variables_Pnd_Dte8_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Dte8_Dd;
    private DbsField pnd_Misc_Variables_Pnd_Ccyy_Dte4;
    private DbsGroup pnd_Misc_Variables_Pnd_Ccyy_Dte4Redef70;
    private DbsField pnd_Misc_Variables_Pnd_Dte4_Cc;
    private DbsField pnd_Misc_Variables_Pnd_Dte4_Yy;
    private DbsField pnd_Misc_Variables_Pnd_Pay_Dte;
    private DbsGroup pnd_Misc_Variables_Pnd_Pay_DteRedef71;
    private DbsField pnd_Misc_Variables_Pnd_Pay_Dte_Cc;
    private DbsField pnd_Misc_Variables_Pnd_Pay_Dte_Yy;
    private DbsField pnd_Misc_Variables_Pnd_Pay_Dte_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Pay_Dte_Dd;
    private DbsField pnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte;
    private DbsGroup pnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72;
    private DbsField pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Cc;
    private DbsField pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy;
    private DbsField pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Indx;
    private DbsField pnd_Misc_Variables_Pnd_Indx1;
    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_No_Cntrct_Rec;
    private DbsField pnd_Logical_Variables_Pnd_No_Record_Found;
    private DbsField pnd_Logical_Variables_Pnd_Fin_Pay_Gt_0;
    private DbsField pnd_Logical_Variables_Pnd_Rate_Changed;
    private DbsField pnd_Logical_Variables_Pnd_Issue_101_Written;

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Acctng_Cde() { return iaa_Cntrct_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte() { return iaa_Cntrct_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Type_Cde() { return iaa_Cntrct_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsGroup getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1() { return iaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos1() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos1; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos2() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos2; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos3() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos3; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2() { return iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2; }

    public DbsField getIaa_Cntrct_Filler1() { return iaa_Cntrct_Filler1; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3() { return iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Cc() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Yy() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Mm() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4; }

    public DbsField getIaa_Cntrct_Filler2() { return iaa_Cntrct_Filler2; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Lst_Trans_Dte() { return iaa_Cntrct_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Type() { return iaa_Cntrct_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Roth_Ssnng_Dte() { return iaa_Cntrct_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Plan_Nmbr() { return iaa_Cntrct_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Tax_Exmpt_Ind() { return iaa_Cntrct_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dob() { return iaa_Cntrct_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dod() { return iaa_Cntrct_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Sub_Plan_Nmbr() { return iaa_Cntrct_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Orgntng_Cntrct_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cntrct_Prtcpnt_Role() { return vw_iaa_Cntrct_Prtcpnt_Role; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte() { return iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind() { return iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte() { return iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind; }

    public DbsGroup getIaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte() { return iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Trans_Dte() { return iaa_Cntrct_Trans_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Invrse_Trans_Dte() { return iaa_Cntrct_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Lst_Trans_Dte() { return iaa_Cntrct_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Optn_Cde() { return iaa_Cntrct_Trans_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orgn_Cde() { return iaa_Cntrct_Trans_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Trans_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type_Cde() { return iaa_Cntrct_Trans_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Trans_Bfre_Imge_Id() { return iaa_Cntrct_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Aftr_Imge_Id() { return iaa_Cntrct_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type() { return iaa_Cntrct_Trans_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Trans_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Ssnng_Dte() { return iaa_Cntrct_Trans_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Plan_Nmbr() { return iaa_Cntrct_Trans_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Tax_Exmpt_Ind() { return iaa_Cntrct_Trans_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dob() { return iaa_Cntrct_Trans_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dod() { return iaa_Cntrct_Trans_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Trans_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Trans_Dte() { return iaa_Cpr_Trans_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Invrse_Trans_Dte() { return iaa_Cpr_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Lst_Trans_Dte() { return iaa_Cpr_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde() { return iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde() { return iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr() { return iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Cash_Cde() { return iaa_Cpr_Trans_Cntrct_Cash_Cde; }

    public DbsGroup getIaa_Cpr_Trans_Cntrct_Company_Data() { return iaa_Cpr_Trans_Cntrct_Company_Data; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Company_Cd() { return iaa_Cpr_Trans_Cntrct_Company_Cd; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rtb_Amt() { return iaa_Cpr_Trans_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Mode_Ind() { return iaa_Cpr_Trans_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Wthdrwl_Dte() { return iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Dod_Dte() { return iaa_Cpr_Trans_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Cde() { return iaa_Cpr_Trans_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Hold_Cde() { return iaa_Cpr_Trans_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte() { return iaa_Cpr_Trans_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Prev_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Curr_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Srce() { return iaa_Cpr_Trans_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cpr_Trans_Bfre_Imge_Id() { return iaa_Cpr_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd() { return vw_iaa_Tiaa_Fund_Rcrd; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef6() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef6; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Trans() { return vw_iaa_Tiaa_Fund_Trans; }

    public DbsField getIaa_Tiaa_Fund_Trans_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef7() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef7; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id() { return iaa_Tiaa_Fund_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund; }

    public DataAccessProgramView getVw_iaa_Cref_Fund_Rcrd_1() { return vw_iaa_Cref_Fund_Rcrd_1; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr() { return iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef8() { return iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef8; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt() { return iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Mode_Ind() { return iaa_Cref_Fund_Rcrd_1_Cref_Mode_Ind; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Fund_Key() { return iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Fund_Key; }

    public DataAccessProgramView getVw_iaa_Cref_Fund_Trans_1() { return vw_iaa_Cref_Fund_Trans_1; }

    public DbsField getIaa_Cref_Fund_Trans_1_Trans_Dte() { return iaa_Cref_Fund_Trans_1_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr() { return iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef9() { return iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef9; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Fund_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Fund_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp() { return iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getIaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp() { return iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Rate_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Rate_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Units_Cnt() { return iaa_Cref_Fund_Trans_1_Cref_Units_Cnt; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Mode_Ind() { return iaa_Cref_Fund_Trans_1_Cref_Mode_Ind; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Old_Cmpny_Fund() { return iaa_Cref_Fund_Trans_1_Cref_Old_Cmpny_Fund; }

    public DbsField getIaa_Cref_Fund_Trans_1_Bfre_Imge_Id() { return iaa_Cref_Fund_Trans_1_Bfre_Imge_Id; }

    public DbsField getPnd_Coding_Sheets() { return pnd_Coding_Sheets; }

    public DbsGroup getPnd_Coding_SheetsRedef10() { return pnd_Coding_SheetsRedef10; }

    public DbsField getPnd_Coding_Sheets_Pnd_Manual_New_Issue1() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue1; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11; }

    public DbsField getPnd_Coding_Sheets_Pnd_Batch_Nbr1() { return pnd_Coding_Sheets_Pnd_Batch_Nbr1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte1() { return pnd_Coding_Sheets_Pnd_Check_Dte1; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Check_Dte1Redef12() { return pnd_Coding_Sheets_Pnd_Check_Dte1Redef12; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Mm1() { return pnd_Coding_Sheets_Pnd_Check_Dte_Mm1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Dd1() { return pnd_Coding_Sheets_Pnd_Check_Dte_Dd1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Yy1() { return pnd_Coding_Sheets_Pnd_Check_Dte_Yy1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cntrct_Nbr1() { return pnd_Coding_Sheets_Pnd_Cntrct_Nbr1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Status1() { return pnd_Coding_Sheets_Pnd_Record_Status1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cross_Ref_Nbr1() { return pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Trans_Nbr1() { return pnd_Coding_Sheets_Pnd_Trans_Nbr1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Type1() { return pnd_Coding_Sheets_Pnd_Record_Type1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Nbr1() { return pnd_Coding_Sheets_Pnd_Record_Nbr1; }

    public DbsField getPnd_Coding_Sheets_Pnd_Rest_Of_Issue1() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue1; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13; }

    public DbsField getPnd_Coding_Sheets_Pnd_Product() { return pnd_Coding_Sheets_Pnd_Product; }

    public DbsField getPnd_Coding_Sheets_Pnd_Currency() { return pnd_Coding_Sheets_Pnd_Currency; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_CurrencyRedef14() { return pnd_Coding_Sheets_Pnd_CurrencyRedef14; }

    public DbsField getPnd_Coding_Sheets_Pnd_Currency_N() { return pnd_Coding_Sheets_Pnd_Currency_N; }

    public DbsField getPnd_Coding_Sheets_Pnd_Mode() { return pnd_Coding_Sheets_Pnd_Mode; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_ModeRedef15() { return pnd_Coding_Sheets_Pnd_ModeRedef15; }

    public DbsField getPnd_Coding_Sheets_Pnd_Mode_N() { return pnd_Coding_Sheets_Pnd_Mode_N; }

    public DbsField getPnd_Coding_Sheets_Pnd_Pend_Code() { return pnd_Coding_Sheets_Pnd_Pend_Code; }

    public DbsField getPnd_Coding_Sheets_Pnd_Hold_Check_Cde() { return pnd_Coding_Sheets_Pnd_Hold_Check_Cde; }

    public DbsField getPnd_Coding_Sheets_Pnd_Pend_Dte() { return pnd_Coding_Sheets_Pnd_Pend_Dte; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Pend_DteRedef16() { return pnd_Coding_Sheets_Pnd_Pend_DteRedef16; }

    public DbsField getPnd_Coding_Sheets_Pnd_Pend_Dte_Mm() { return pnd_Coding_Sheets_Pnd_Pend_Dte_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_Pend_Dte_Yy() { return pnd_Coding_Sheets_Pnd_Pend_Dte_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Option() { return pnd_Coding_Sheets_Pnd_Option; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_OptionRedef17() { return pnd_Coding_Sheets_Pnd_OptionRedef17; }

    public DbsField getPnd_Coding_Sheets_Pnd_Option_N() { return pnd_Coding_Sheets_Pnd_Option_N; }

    public DbsField getPnd_Coding_Sheets_Pnd_Origin() { return pnd_Coding_Sheets_Pnd_Origin; }

    public DbsField getPnd_Coding_Sheets_Pnd_Iss_Dte() { return pnd_Coding_Sheets_Pnd_Iss_Dte; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Iss_DteRedef18() { return pnd_Coding_Sheets_Pnd_Iss_DteRedef18; }

    public DbsField getPnd_Coding_Sheets_Pnd_Iss_Dte_Mm() { return pnd_Coding_Sheets_Pnd_Iss_Dte_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_Iss_Dte_Yy() { return pnd_Coding_Sheets_Pnd_Iss_Dte_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte() { return pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_1st_Pay_Due_DteRedef19() { return pnd_Coding_Sheets_Pnd_1st_Pay_Due_DteRedef19; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm() { return pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy() { return pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte() { return pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_DteRedef20() { return pnd_Coding_Sheets_Pnd_Lst_Man_Chk_DteRedef20; }

    public DbsField getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm() { return pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy() { return pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte() { return pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_DteRedef21() { return pnd_Coding_Sheets_Pnd_Fin_Per_Pay_DteRedef21; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm() { return pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy() { return pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte() { return pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22() { return pnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm() { return pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd() { return pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy() { return pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Wdrawal_Dte() { return pnd_Coding_Sheets_Pnd_Wdrawal_Dte; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Wdrawal_DteRedef23() { return pnd_Coding_Sheets_Pnd_Wdrawal_DteRedef23; }

    public DbsField getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm() { return pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy() { return pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Filler() { return pnd_Coding_Sheets_Pnd_Filler; }

    public DbsField getPnd_Coding_Sheets_Pnd_User_Area1() { return pnd_Coding_Sheets_Pnd_User_Area1; }

    public DbsGroup getPnd_Coding_SheetsRedef24() { return pnd_Coding_SheetsRedef24; }

    public DbsField getPnd_Coding_Sheets_Pnd_Manual_New_Issue2() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue2; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25; }

    public DbsField getPnd_Coding_Sheets_Pnd_Batch_Nbr2() { return pnd_Coding_Sheets_Pnd_Batch_Nbr2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte2() { return pnd_Coding_Sheets_Pnd_Check_Dte2; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Check_Dte2Redef26() { return pnd_Coding_Sheets_Pnd_Check_Dte2Redef26; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Mm2() { return pnd_Coding_Sheets_Pnd_Check_Dte_Mm2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Dd2() { return pnd_Coding_Sheets_Pnd_Check_Dte_Dd2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Yy2() { return pnd_Coding_Sheets_Pnd_Check_Dte_Yy2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cntrct_Nbr2() { return pnd_Coding_Sheets_Pnd_Cntrct_Nbr2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Status2() { return pnd_Coding_Sheets_Pnd_Record_Status2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cross_Ref_Nbr2() { return pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Trans_Nbr2() { return pnd_Coding_Sheets_Pnd_Trans_Nbr2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Type2() { return pnd_Coding_Sheets_Pnd_Record_Type2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Nbr2() { return pnd_Coding_Sheets_Pnd_Record_Nbr2; }

    public DbsField getPnd_Coding_Sheets_Pnd_Rest_Of_Issue2() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue2; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27; }

    public DbsField getPnd_Coding_Sheets_Pnd_Citizen() { return pnd_Coding_Sheets_Pnd_Citizen; }

    public DbsField getPnd_Coding_Sheets_Pnd_Stfslash_Cntry_Iss() { return pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Iss; }

    public DbsField getPnd_Coding_Sheets_Pnd_Stfslash_Cntry_Res() { return pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Res; }

    public DbsField getPnd_Coding_Sheets_Pnd_Coll_Iss() { return pnd_Coding_Sheets_Pnd_Coll_Iss; }

    public DbsField getPnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt() { return pnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt; }

    public DbsField getPnd_Coding_Sheets_Pnd_Ssn() { return pnd_Coding_Sheets_Pnd_Ssn; }

    public DbsField getPnd_Coding_Sheets_Pnd_Joint_Cnvrt() { return pnd_Coding_Sheets_Pnd_Joint_Cnvrt; }

    public DbsField getPnd_Coding_Sheets_Pnd_Spirt() { return pnd_Coding_Sheets_Pnd_Spirt; }

    public DbsField getPnd_Coding_Sheets_Pnd_Pen_Pln_Cde() { return pnd_Coding_Sheets_Pnd_Pen_Pln_Cde; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cntrct_Type() { return pnd_Coding_Sheets_Pnd_Cntrct_Type; }

    public DbsField getPnd_Coding_Sheets_Pnd_Filler1() { return pnd_Coding_Sheets_Pnd_Filler1; }

    public DbsField getPnd_Coding_Sheets_Pnd_User_Area2() { return pnd_Coding_Sheets_Pnd_User_Area2; }

    public DbsGroup getPnd_Coding_SheetsRedef28() { return pnd_Coding_SheetsRedef28; }

    public DbsField getPnd_Coding_Sheets_Pnd_Manual_New_Issue3() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue3; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29; }

    public DbsField getPnd_Coding_Sheets_Pnd_Batch_Nbr3() { return pnd_Coding_Sheets_Pnd_Batch_Nbr3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte3() { return pnd_Coding_Sheets_Pnd_Check_Dte3; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Check_Dte3Redef30() { return pnd_Coding_Sheets_Pnd_Check_Dte3Redef30; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Mm3() { return pnd_Coding_Sheets_Pnd_Check_Dte_Mm3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Dd3() { return pnd_Coding_Sheets_Pnd_Check_Dte_Dd3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Yy3() { return pnd_Coding_Sheets_Pnd_Check_Dte_Yy3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cntrct_Nbr3() { return pnd_Coding_Sheets_Pnd_Cntrct_Nbr3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Status3() { return pnd_Coding_Sheets_Pnd_Record_Status3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cross_Ref_Nbr3() { return pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Trans_Nbr3() { return pnd_Coding_Sheets_Pnd_Trans_Nbr3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Type3() { return pnd_Coding_Sheets_Pnd_Record_Type3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Nbr3() { return pnd_Coding_Sheets_Pnd_Record_Nbr3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Rest_Of_Issue3() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue3; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Annt_X_Ref() { return pnd_Coding_Sheets_Pnd_1st_Annt_X_Ref; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32() { return pnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Last_Name() { return pnd_Coding_Sheets_Pnd_1st_Last_Name; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_First_Init() { return pnd_Coding_Sheets_Pnd_1st_First_Init; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Mid_Init() { return pnd_Coding_Sheets_Pnd_1st_Mid_Init; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Sex() { return pnd_Coding_Sheets_Pnd_1st_Sex; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Dob() { return pnd_Coding_Sheets_Pnd_1st_Dob; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_1st_DobRedef33() { return pnd_Coding_Sheets_Pnd_1st_DobRedef33; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Dob_Mm() { return pnd_Coding_Sheets_Pnd_1st_Dob_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Dob_Dd() { return pnd_Coding_Sheets_Pnd_1st_Dob_Dd; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Dob_Yy() { return pnd_Coding_Sheets_Pnd_1st_Dob_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Dod() { return pnd_Coding_Sheets_Pnd_1st_Dod; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_1st_DodRedef34() { return pnd_Coding_Sheets_Pnd_1st_DodRedef34; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Dod_Mm() { return pnd_Coding_Sheets_Pnd_1st_Dod_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_1st_Dod_Yy() { return pnd_Coding_Sheets_Pnd_1st_Dod_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Mort_Yob3() { return pnd_Coding_Sheets_Pnd_Mort_Yob3; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Mort_Yob3Redef35() { return pnd_Coding_Sheets_Pnd_Mort_Yob3Redef35; }

    public DbsField getPnd_Coding_Sheets_Pnd_Mort_Yob3_Yy() { return pnd_Coding_Sheets_Pnd_Mort_Yob3_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Life_Cnt3() { return pnd_Coding_Sheets_Pnd_Life_Cnt3; }

    public DbsField getPnd_Coding_Sheets_Pnd_Div_Payee() { return pnd_Coding_Sheets_Pnd_Div_Payee; }

    public DbsField getPnd_Coding_Sheets_Pnd_Coll_Cde() { return pnd_Coding_Sheets_Pnd_Coll_Cde; }

    public DbsField getPnd_Coding_Sheets_Pnd_Orig_Cntrct() { return pnd_Coding_Sheets_Pnd_Orig_Cntrct; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cash_Cde() { return pnd_Coding_Sheets_Pnd_Cash_Cde; }

    public DbsField getPnd_Coding_Sheets_Pnd_Emp_Term() { return pnd_Coding_Sheets_Pnd_Emp_Term; }

    public DbsField getPnd_Coding_Sheets_Pnd_Filler2() { return pnd_Coding_Sheets_Pnd_Filler2; }

    public DbsField getPnd_Coding_Sheets_Pnd_User_Area3() { return pnd_Coding_Sheets_Pnd_User_Area3; }

    public DbsGroup getPnd_Coding_SheetsRedef36() { return pnd_Coding_SheetsRedef36; }

    public DbsField getPnd_Coding_Sheets_Pnd_Manual_New_Issue4() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue4; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37; }

    public DbsField getPnd_Coding_Sheets_Pnd_Batch_Nbr4() { return pnd_Coding_Sheets_Pnd_Batch_Nbr4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte4() { return pnd_Coding_Sheets_Pnd_Check_Dte4; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Check_Dte4Redef38() { return pnd_Coding_Sheets_Pnd_Check_Dte4Redef38; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Mm4() { return pnd_Coding_Sheets_Pnd_Check_Dte_Mm4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Dd4() { return pnd_Coding_Sheets_Pnd_Check_Dte_Dd4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Yy4() { return pnd_Coding_Sheets_Pnd_Check_Dte_Yy4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cntrct_Nbr4() { return pnd_Coding_Sheets_Pnd_Cntrct_Nbr4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Status4() { return pnd_Coding_Sheets_Pnd_Record_Status4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cross_Ref_Nbr4() { return pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Trans_Nbr4() { return pnd_Coding_Sheets_Pnd_Trans_Nbr4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Type4() { return pnd_Coding_Sheets_Pnd_Record_Type4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Nbr4() { return pnd_Coding_Sheets_Pnd_Record_Nbr4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Rest_Of_Issue4() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue4; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref() { return pnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40() { return pnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Last_Name() { return pnd_Coding_Sheets_Pnd_2nd_Last_Name; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_First_Init() { return pnd_Coding_Sheets_Pnd_2nd_First_Init; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Mid_Init() { return pnd_Coding_Sheets_Pnd_2nd_Mid_Init; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Sex() { return pnd_Coding_Sheets_Pnd_2nd_Sex; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Dob() { return pnd_Coding_Sheets_Pnd_2nd_Dob; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_2nd_DobRedef41() { return pnd_Coding_Sheets_Pnd_2nd_DobRedef41; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Dob_Mm() { return pnd_Coding_Sheets_Pnd_2nd_Dob_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Dob_Dd() { return pnd_Coding_Sheets_Pnd_2nd_Dob_Dd; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Dob_Yy() { return pnd_Coding_Sheets_Pnd_2nd_Dob_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Dod() { return pnd_Coding_Sheets_Pnd_2nd_Dod; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_2nd_DodRedef42() { return pnd_Coding_Sheets_Pnd_2nd_DodRedef42; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Dod_Mm() { return pnd_Coding_Sheets_Pnd_2nd_Dod_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_2nd_Dod_Yy() { return pnd_Coding_Sheets_Pnd_2nd_Dod_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Mort_Yob4() { return pnd_Coding_Sheets_Pnd_Mort_Yob4; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Mort_Yob4Redef43() { return pnd_Coding_Sheets_Pnd_Mort_Yob4Redef43; }

    public DbsField getPnd_Coding_Sheets_Pnd_Mort_Yob4_Yy() { return pnd_Coding_Sheets_Pnd_Mort_Yob4_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Life_Cnt4() { return pnd_Coding_Sheets_Pnd_Life_Cnt4; }

    public DbsField getPnd_Coding_Sheets_Pnd_Ben_Xref() { return pnd_Coding_Sheets_Pnd_Ben_Xref; }

    public DbsField getPnd_Coding_Sheets_Pnd_Ben_Dod() { return pnd_Coding_Sheets_Pnd_Ben_Dod; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Ben_DodRedef44() { return pnd_Coding_Sheets_Pnd_Ben_DodRedef44; }

    public DbsField getPnd_Coding_Sheets_Pnd_Ben_Dod_A() { return pnd_Coding_Sheets_Pnd_Ben_Dod_A; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Ben_DodRedef45() { return pnd_Coding_Sheets_Pnd_Ben_DodRedef45; }

    public DbsField getPnd_Coding_Sheets_Pnd_Ben_Dod_Mm() { return pnd_Coding_Sheets_Pnd_Ben_Dod_Mm; }

    public DbsField getPnd_Coding_Sheets_Pnd_Ben_Dod_Yy() { return pnd_Coding_Sheets_Pnd_Ben_Dod_Yy; }

    public DbsField getPnd_Coding_Sheets_Pnd_Dest_Prev() { return pnd_Coding_Sheets_Pnd_Dest_Prev; }

    public DbsField getPnd_Coding_Sheets_Pnd_Dest_Curr() { return pnd_Coding_Sheets_Pnd_Dest_Curr; }

    public DbsField getPnd_Coding_Sheets_Pnd_User_Area4() { return pnd_Coding_Sheets_Pnd_User_Area4; }

    public DbsGroup getPnd_Coding_SheetsRedef46() { return pnd_Coding_SheetsRedef46; }

    public DbsField getPnd_Coding_Sheets_Pnd_Manual_New_Issue5() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue5; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47() { return pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47; }

    public DbsField getPnd_Coding_Sheets_Pnd_Batch_Nbr5() { return pnd_Coding_Sheets_Pnd_Batch_Nbr5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte5() { return pnd_Coding_Sheets_Pnd_Check_Dte5; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Check_Dte5Redef48() { return pnd_Coding_Sheets_Pnd_Check_Dte5Redef48; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Mm5() { return pnd_Coding_Sheets_Pnd_Check_Dte_Mm5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Dd5() { return pnd_Coding_Sheets_Pnd_Check_Dte_Dd5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Check_Dte_Yy5() { return pnd_Coding_Sheets_Pnd_Check_Dte_Yy5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cntrct_Nbr5() { return pnd_Coding_Sheets_Pnd_Cntrct_Nbr5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Status5() { return pnd_Coding_Sheets_Pnd_Record_Status5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cross_Ref_Nbr5() { return pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Trans_Nbr5() { return pnd_Coding_Sheets_Pnd_Trans_Nbr5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Type5() { return pnd_Coding_Sheets_Pnd_Record_Type5; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Nbr5() { return pnd_Coding_Sheets_Pnd_Record_Nbr5; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Record_Nbr5Redef49() { return pnd_Coding_Sheets_Pnd_Record_Nbr5Redef49; }

    public DbsField getPnd_Coding_Sheets_Pnd_Record_Nbr5_N() { return pnd_Coding_Sheets_Pnd_Record_Nbr5_N; }

    public DbsField getPnd_Coding_Sheets_Pnd_Rest_Of_Issue5() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue5; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50() { return pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50; }

    public DbsField getPnd_Coding_Sheets_Pnd_Rate() { return pnd_Coding_Sheets_Pnd_Rate; }

    public DbsField getPnd_Coding_Sheets_Pnd_Per_Pay() { return pnd_Coding_Sheets_Pnd_Per_Pay; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Per_PayRedef51() { return pnd_Coding_Sheets_Pnd_Per_PayRedef51; }

    public DbsField getPnd_Coding_Sheets_Pnd_Cref_Units() { return pnd_Coding_Sheets_Pnd_Cref_Units; }

    public DbsField getPnd_Coding_Sheets_Pnd_Per_Div() { return pnd_Coding_Sheets_Pnd_Per_Div; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Pay() { return pnd_Coding_Sheets_Pnd_Fin_Pay; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Fin_PayRedef52() { return pnd_Coding_Sheets_Pnd_Fin_PayRedef52; }

    public DbsField getPnd_Coding_Sheets_Pnd_Fin_Pay_A() { return pnd_Coding_Sheets_Pnd_Fin_Pay_A; }

    public DbsField getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Div() { return pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Tot_Old_Per_DivRedef53() { return pnd_Coding_Sheets_Pnd_Tot_Old_Per_DivRedef53; }

    public DbsField getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Div_A() { return pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div_A; }

    public DbsField getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay() { return pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef54() { return pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef54; }

    public DbsField getPnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay_A() { return pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay_A; }

    public DbsGroup getPnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef55() { return pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef55; }

    public DbsField getPnd_Coding_Sheets_Pnd_Tot_Old_Cref_Units() { return pnd_Coding_Sheets_Pnd_Tot_Old_Cref_Units; }

    public DbsField getPnd_Coding_Sheets_Pnd_User_Area5() { return pnd_Coding_Sheets_Pnd_User_Area5; }

    public DbsField getPnd_Cntrct_Key() { return pnd_Cntrct_Key; }

    public DbsGroup getPnd_Cntrct_KeyRedef56() { return pnd_Cntrct_KeyRedef56; }

    public DbsField getPnd_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Key_Pnd_Lst_Trans_Dte() { return pnd_Cntrct_Key_Pnd_Lst_Trans_Dte; }

    public DbsField getPnd_Cntrct_Payee_Key() { return pnd_Cntrct_Payee_Key; }

    public DbsGroup getPnd_Cntrct_Payee_KeyRedef57() { return pnd_Cntrct_Payee_KeyRedef57; }

    public DbsField getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Cntrct_Bfre_Key() { return pnd_Cntrct_Bfre_Key; }

    public DbsGroup getPnd_Cntrct_Bfre_KeyRedef58() { return pnd_Cntrct_Bfre_KeyRedef58; }

    public DbsField getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte() { return pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Cntrct_Aftr_Key() { return pnd_Cntrct_Aftr_Key; }

    public DbsGroup getPnd_Cntrct_Aftr_KeyRedef59() { return pnd_Cntrct_Aftr_KeyRedef59; }

    public DbsField getPnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Cpr_Bfre_Key() { return pnd_Cpr_Bfre_Key; }

    public DbsGroup getPnd_Cpr_Bfre_KeyRedef60() { return pnd_Cpr_Bfre_KeyRedef60; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte() { return pnd_Cpr_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Cpr_Aftr_Key() { return pnd_Cpr_Aftr_Key; }

    public DbsGroup getPnd_Cpr_Aftr_KeyRedef61() { return pnd_Cpr_Aftr_KeyRedef61; }

    public DbsField getPnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cpr_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Cref_Fund_Key() { return pnd_Cref_Fund_Key; }

    public DbsGroup getPnd_Cref_Fund_KeyRedef62() { return pnd_Cref_Fund_KeyRedef62; }

    public DbsField getPnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Ppcn_Nbr() { return pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Payee_Cde() { return pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Payee_Cde; }

    public DbsField getPnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde() { return pnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde; }

    public DbsField getPnd_Tiaa_Fund_Key() { return pnd_Tiaa_Fund_Key; }

    public DbsGroup getPnd_Tiaa_Fund_KeyRedef63() { return pnd_Tiaa_Fund_KeyRedef63; }

    public DbsField getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr() { return pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde() { return pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getPnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde() { return pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde; }

    public DbsField getPnd_Cref_Fund_Trans() { return pnd_Cref_Fund_Trans; }

    public DbsGroup getPnd_Cref_Fund_TransRedef64() { return pnd_Cref_Fund_TransRedef64; }

    public DbsField getPnd_Cref_Fund_Trans_Pnd_Bfre_Imge_Id() { return pnd_Cref_Fund_Trans_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Ppcn_Nbr() { return pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Payee_Cde() { return pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Payee_Cde; }

    public DbsField getPnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Fund_Cde() { return pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Fund_Cde; }

    public DbsField getPnd_Cref_Fund_Trans_Pnd_Trans_Dte() { return pnd_Cref_Fund_Trans_Pnd_Trans_Dte; }

    public DbsField getPnd_Tiaa_Fund_Trans() { return pnd_Tiaa_Fund_Trans; }

    public DbsGroup getPnd_Tiaa_Fund_TransRedef65() { return pnd_Tiaa_Fund_TransRedef65; }

    public DbsField getPnd_Tiaa_Fund_Trans_Pnd_Bfre_Imge_Id() { return pnd_Tiaa_Fund_Trans_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Tiaa_Fund_Trans_Pnd_Tiaa_Ppcn_Nbr() { return pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Ppcn_Nbr; }

    public DbsField getPnd_Tiaa_Fund_Trans_Pnd_Tiaa_Payee_Cde() { return pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Payee_Cde; }

    public DbsField getPnd_Tiaa_Fund_Trans_Pnd_Tiaa_Fund_Cde() { return pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Fund_Cde; }

    public DbsField getPnd_Tiaa_Fund_Trans_Pnd_Trans_Dte() { return pnd_Tiaa_Fund_Trans_Pnd_Trans_Dte; }

    public DbsField getPnd_Tiaa_Fund_Bfre_Key() { return pnd_Tiaa_Fund_Bfre_Key; }

    public DbsGroup getPnd_Tiaa_Fund_Bfre_KeyRedef66() { return pnd_Tiaa_Fund_Bfre_KeyRedef66; }

    public DbsField getPnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr() { return pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde() { return pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getPnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Fund_Cde() { return pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Fund_Cde; }

    public DbsField getPnd_Tiaa_Fund_Bfre_Key_Pnd_Trans_Dte() { return pnd_Tiaa_Fund_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Cref_Fund_Bfre_Key() { return pnd_Cref_Fund_Bfre_Key; }

    public DbsGroup getPnd_Cref_Fund_Bfre_KeyRedef67() { return pnd_Cref_Fund_Bfre_KeyRedef67; }

    public DbsField getPnd_Cref_Fund_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Cref_Fund_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Ppcn_Nbr() { return pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Payee_Cde() { return pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Payee_Cde; }

    public DbsField getPnd_Cref_Fund_Bfre_Key_Pnd_Cref_Fund_Cde() { return pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Fund_Cde; }

    public DbsField getPnd_Cref_Fund_Bfre_Key_Pnd_Trans_Dte() { return pnd_Cref_Fund_Bfre_Key_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Misc_Variables() { return pnd_Misc_Variables; }

    public DbsField getPnd_Misc_Variables_Pnd_Batch_Nbr() { return pnd_Misc_Variables_Pnd_Batch_Nbr; }

    public DbsField getPnd_Misc_Variables_Pnd_Check_Dte_Mm() { return pnd_Misc_Variables_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Misc_Variables_Pnd_Check_Dte_Dd() { return pnd_Misc_Variables_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Misc_Variables_Pnd_Check_Dte_Yy() { return pnd_Misc_Variables_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Misc_Variables_Pnd_Cntrct_Nbr() { return pnd_Misc_Variables_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Misc_Variables_Pnd_Record_Status() { return pnd_Misc_Variables_Pnd_Record_Status; }

    public DbsField getPnd_Misc_Variables_Pnd_Cross_Ref_Nbr() { return pnd_Misc_Variables_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Misc_Variables_Pnd_Rec_Type() { return pnd_Misc_Variables_Pnd_Rec_Type; }

    public DbsField getPnd_Misc_Variables_Pnd_Trans_Nbr() { return pnd_Misc_Variables_Pnd_Trans_Nbr; }

    public DbsField getPnd_Misc_Variables_Pnd_Ccyymm_Dte() { return pnd_Misc_Variables_Pnd_Ccyymm_Dte; }

    public DbsGroup getPnd_Misc_Variables_Pnd_Ccyymm_DteRedef68() { return pnd_Misc_Variables_Pnd_Ccyymm_DteRedef68; }

    public DbsField getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Cc() { return pnd_Misc_Variables_Pnd_Ccyymm_Dte_Cc; }

    public DbsField getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy() { return pnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy; }

    public DbsField getPnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm() { return pnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm; }

    public DbsField getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8() { return pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8; }

    public DbsGroup getPnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69() { return pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69; }

    public DbsField getPnd_Misc_Variables_Pnd_Dte8_Cc() { return pnd_Misc_Variables_Pnd_Dte8_Cc; }

    public DbsField getPnd_Misc_Variables_Pnd_Dte8_Yy() { return pnd_Misc_Variables_Pnd_Dte8_Yy; }

    public DbsField getPnd_Misc_Variables_Pnd_Dte8_Mm() { return pnd_Misc_Variables_Pnd_Dte8_Mm; }

    public DbsField getPnd_Misc_Variables_Pnd_Dte8_Dd() { return pnd_Misc_Variables_Pnd_Dte8_Dd; }

    public DbsField getPnd_Misc_Variables_Pnd_Ccyy_Dte4() { return pnd_Misc_Variables_Pnd_Ccyy_Dte4; }

    public DbsGroup getPnd_Misc_Variables_Pnd_Ccyy_Dte4Redef70() { return pnd_Misc_Variables_Pnd_Ccyy_Dte4Redef70; }

    public DbsField getPnd_Misc_Variables_Pnd_Dte4_Cc() { return pnd_Misc_Variables_Pnd_Dte4_Cc; }

    public DbsField getPnd_Misc_Variables_Pnd_Dte4_Yy() { return pnd_Misc_Variables_Pnd_Dte4_Yy; }

    public DbsField getPnd_Misc_Variables_Pnd_Pay_Dte() { return pnd_Misc_Variables_Pnd_Pay_Dte; }

    public DbsGroup getPnd_Misc_Variables_Pnd_Pay_DteRedef71() { return pnd_Misc_Variables_Pnd_Pay_DteRedef71; }

    public DbsField getPnd_Misc_Variables_Pnd_Pay_Dte_Cc() { return pnd_Misc_Variables_Pnd_Pay_Dte_Cc; }

    public DbsField getPnd_Misc_Variables_Pnd_Pay_Dte_Yy() { return pnd_Misc_Variables_Pnd_Pay_Dte_Yy; }

    public DbsField getPnd_Misc_Variables_Pnd_Pay_Dte_Mm() { return pnd_Misc_Variables_Pnd_Pay_Dte_Mm; }

    public DbsField getPnd_Misc_Variables_Pnd_Pay_Dte_Dd() { return pnd_Misc_Variables_Pnd_Pay_Dte_Dd; }

    public DbsField getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte() { return pnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte; }

    public DbsGroup getPnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72() { return pnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72; }

    public DbsField getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Cc() { return pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Cc; }

    public DbsField getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy() { return pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy; }

    public DbsField getPnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm() { return pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm; }

    public DbsField getPnd_Misc_Variables_Pnd_Indx() { return pnd_Misc_Variables_Pnd_Indx; }

    public DbsField getPnd_Misc_Variables_Pnd_Indx1() { return pnd_Misc_Variables_Pnd_Indx1; }

    public DbsGroup getPnd_Logical_Variables() { return pnd_Logical_Variables; }

    public DbsField getPnd_Logical_Variables_Pnd_No_Cntrct_Rec() { return pnd_Logical_Variables_Pnd_No_Cntrct_Rec; }

    public DbsField getPnd_Logical_Variables_Pnd_No_Record_Found() { return pnd_Logical_Variables_Pnd_No_Record_Found; }

    public DbsField getPnd_Logical_Variables_Pnd_Fin_Pay_Gt_0() { return pnd_Logical_Variables_Pnd_Fin_Pay_Gt_0; }

    public DbsField getPnd_Logical_Variables_Pnd_Rate_Changed() { return pnd_Logical_Variables_Pnd_Rate_Changed; }

    public DbsField getPnd_Logical_Variables_Pnd_Issue_101_Written() { return pnd_Logical_Variables_Pnd_Issue_101_Written; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1", "Redefines", 
            iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde);
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos1 = iaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos1", "CNTRCT-RSDNCY-AT-ISS-POS1", 
            FieldType.NUMERIC, 1);
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos2 = iaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos2", "CNTRCT-RSDNCY-AT-ISS-POS2", 
            FieldType.NUMERIC, 1);
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos3 = iaa_Cntrct_Cntrct_Rsdncy_At_Issue_CdeRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Pos3", "CNTRCT-RSDNCY-AT-ISS-POS3", 
            FieldType.NUMERIC, 1);
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2", "Redefines", 
            iaa_Cntrct_Cntrct_First_Annt_Dob_Dte);
        iaa_Cntrct_Filler1 = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2.newFieldInGroup("iaa_Cntrct_Filler1", "FILLER1", FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Yy = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Yy", "CNTRCT-FIRST-ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Mm = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Mm", "CNTRCT-FIRST-ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dd = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dd", "CNTRCT-FIRST-ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3", "Redefines", 
            iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Cc = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Cc", "CNTRCT-FIRST-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Yy = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Yy", "CNTRCT-FIRST-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Mm = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Mm", "CNTRCT-FIRST-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4", "Redefines", 
            iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte);
        iaa_Cntrct_Filler2 = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4.newFieldInGroup("iaa_Cntrct_Filler2", "FILLER2", FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy", "CNTRCT-SCND-ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm", "CNTRCT-SCND-ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd", "CNTRCT-SCND-ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5", "Redefines", 
            iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc", "CNTRCT-SCND-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy", "CNTRCT-SCND-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef5.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm", "CNTRCT-SCND-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Roth_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_SSNNG_DTE");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Tax_Exmpt_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_EXMPT_IND");
        iaa_Cntrct_Orig_Ownr_Dob = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOB");
        iaa_Cntrct_Orig_Ownr_Dod = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOD");
        iaa_Cntrct_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr", 
            "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind", 
            "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup", 
            "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde = iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde", 
            "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind", 
            "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Cntrct_Type = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Trans_Roth_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_SSNNG_DTE");
        iaa_Cntrct_Trans_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Trans_Tax_Exmpt_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TAX_EXMPT_IND");
        iaa_Cntrct_Trans_Orig_Ownr_Dob = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOB");
        iaa_Cntrct_Trans_Orig_Ownr_Dod = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOD");
        iaa_Cntrct_Trans_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef6 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef6", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef6.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef6.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key", "TIAA-CNTRCT-FUND-KEY", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef7 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef7", 
            "Redefines", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef7.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef7.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");

        vw_iaa_Cref_Fund_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd_1", "IAA-CREF-FUND-RCRD-1"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef8 = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef8", 
            "Redefines", iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef8.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef8.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Mode_Ind = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Mode_Ind", "CREF-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Fund_Key = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Fund_Key", "CREF-CNTRCT-FUND-KEY", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");

        vw_iaa_Cref_Fund_Trans_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Trans_1", "IAA-CREF-FUND-TRANS-1"), "IAA_CREF_FUND_TRANS_1", 
            "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_TRANS_1"));
        iaa_Cref_Fund_Trans_1_Trans_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr", 
            "CREF-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde", 
            "CREF-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef9 = vw_iaa_Cref_Fund_Trans_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef9", 
            "Redefines", iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde = iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef9.newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde", 
            "CREF-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Cref_Fund_Trans_1_Cref_Fund_Cde = iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef9.newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CREF_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Rate_Cde = iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_1_Cref_Rate_Cde", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Units_Cnt = iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_1_Cref_Units_Cnt", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Mode_Ind = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Mode_Ind", "CREF-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Cref_Fund_Trans_1_Cref_Old_Cmpny_Fund = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Old_Cmpny_Fund", 
            "CREF-OLD-CMPNY-FUND", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        iaa_Cref_Fund_Trans_1_Bfre_Imge_Id = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Bfre_Imge_Id", "BFRE-IMGE-ID", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");

        pnd_Coding_Sheets = newFieldInRecord("pnd_Coding_Sheets", "#CODING-SHEETS", FieldType.STRING, 100);
        pnd_Coding_SheetsRedef10 = newGroupInRecord("pnd_Coding_SheetsRedef10", "Redefines", pnd_Coding_Sheets);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue1 = pnd_Coding_SheetsRedef10.newFieldInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue1", "#MANUAL-NEW-ISSUE1", 
            FieldType.STRING, 100);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11 = pnd_Coding_SheetsRedef10.newGroupInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11", "Redefines", 
            pnd_Coding_Sheets_Pnd_Manual_New_Issue1);
        pnd_Coding_Sheets_Pnd_Batch_Nbr1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Batch_Nbr1", "#BATCH-NBR1", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Coding_Sheets_Pnd_Check_Dte1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte1", "#CHECK-DTE1", 
            FieldType.NUMERIC, 6);
        pnd_Coding_Sheets_Pnd_Check_Dte1Redef12 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newGroupInGroup("pnd_Coding_Sheets_Pnd_Check_Dte1Redef12", 
            "Redefines", pnd_Coding_Sheets_Pnd_Check_Dte1);
        pnd_Coding_Sheets_Pnd_Check_Dte_Mm1 = pnd_Coding_Sheets_Pnd_Check_Dte1Redef12.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Mm1", "#CHECK-DTE-MM1", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Dd1 = pnd_Coding_Sheets_Pnd_Check_Dte1Redef12.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Dd1", "#CHECK-DTE-DD1", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Yy1 = pnd_Coding_Sheets_Pnd_Check_Dte1Redef12.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Yy1", "#CHECK-DTE-YY1", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cntrct_Nbr1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cntrct_Nbr1", "#CNTRCT-NBR1", 
            FieldType.STRING, 8);
        pnd_Coding_Sheets_Pnd_Record_Status1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Status1", 
            "#RECORD-STATUS1", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr1", 
            "#CROSS-REF-NBR1", FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_Trans_Nbr1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Trans_Nbr1", "#TRANS-NBR1", 
            FieldType.NUMERIC, 3);
        pnd_Coding_Sheets_Pnd_Record_Type1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Type1", "#RECORD-TYPE1", 
            FieldType.STRING, 2);
        pnd_Coding_Sheets_Pnd_Record_Nbr1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Nbr1", "#RECORD-NBR1", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue1", 
            "#REST-OF-ISSUE1", FieldType.STRING, 45);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newGroupInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13", 
            "Redefines", pnd_Coding_Sheets_Pnd_Rest_Of_Issue1);
        pnd_Coding_Sheets_Pnd_Product = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Product", "#PRODUCT", FieldType.STRING, 
            1);
        pnd_Coding_Sheets_Pnd_Currency = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Currency", "#CURRENCY", FieldType.STRING, 
            1);
        pnd_Coding_Sheets_Pnd_CurrencyRedef14 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_CurrencyRedef14", "Redefines", 
            pnd_Coding_Sheets_Pnd_Currency);
        pnd_Coding_Sheets_Pnd_Currency_N = pnd_Coding_Sheets_Pnd_CurrencyRedef14.newFieldInGroup("pnd_Coding_Sheets_Pnd_Currency_N", "#CURRENCY-N", FieldType.NUMERIC, 
            1);
        pnd_Coding_Sheets_Pnd_Mode = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Mode", "#MODE", FieldType.STRING, 
            3);
        pnd_Coding_Sheets_Pnd_ModeRedef15 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_ModeRedef15", "Redefines", 
            pnd_Coding_Sheets_Pnd_Mode);
        pnd_Coding_Sheets_Pnd_Mode_N = pnd_Coding_Sheets_Pnd_ModeRedef15.newFieldInGroup("pnd_Coding_Sheets_Pnd_Mode_N", "#MODE-N", FieldType.NUMERIC, 
            3);
        pnd_Coding_Sheets_Pnd_Pend_Code = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Pend_Code", "#PEND-CODE", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Hold_Check_Cde = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Hold_Check_Cde", "#HOLD-CHECK-CDE", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Pend_Dte = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Pend_Dte", "#PEND-DTE", FieldType.STRING, 
            4);
        pnd_Coding_Sheets_Pnd_Pend_DteRedef16 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_Pend_DteRedef16", "Redefines", 
            pnd_Coding_Sheets_Pnd_Pend_Dte);
        pnd_Coding_Sheets_Pnd_Pend_Dte_Mm = pnd_Coding_Sheets_Pnd_Pend_DteRedef16.newFieldInGroup("pnd_Coding_Sheets_Pnd_Pend_Dte_Mm", "#PEND-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Pend_Dte_Yy = pnd_Coding_Sheets_Pnd_Pend_DteRedef16.newFieldInGroup("pnd_Coding_Sheets_Pnd_Pend_Dte_Yy", "#PEND-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Option = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Option", "#OPTION", FieldType.STRING, 
            2);
        pnd_Coding_Sheets_Pnd_OptionRedef17 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_OptionRedef17", "Redefines", 
            pnd_Coding_Sheets_Pnd_Option);
        pnd_Coding_Sheets_Pnd_Option_N = pnd_Coding_Sheets_Pnd_OptionRedef17.newFieldInGroup("pnd_Coding_Sheets_Pnd_Option_N", "#OPTION-N", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_Origin = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Origin", "#ORIGIN", FieldType.STRING, 
            2);
        pnd_Coding_Sheets_Pnd_Iss_Dte = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Iss_Dte", "#ISS-DTE", FieldType.STRING, 
            4);
        pnd_Coding_Sheets_Pnd_Iss_DteRedef18 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_Iss_DteRedef18", "Redefines", 
            pnd_Coding_Sheets_Pnd_Iss_Dte);
        pnd_Coding_Sheets_Pnd_Iss_Dte_Mm = pnd_Coding_Sheets_Pnd_Iss_DteRedef18.newFieldInGroup("pnd_Coding_Sheets_Pnd_Iss_Dte_Mm", "#ISS-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_Iss_Dte_Yy = pnd_Coding_Sheets_Pnd_Iss_DteRedef18.newFieldInGroup("pnd_Coding_Sheets_Pnd_Iss_Dte_Yy", "#ISS-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte", "#1ST-PAY-DUE-DTE", 
            FieldType.STRING, 4);
        pnd_Coding_Sheets_Pnd_1st_Pay_Due_DteRedef19 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_1st_Pay_Due_DteRedef19", 
            "Redefines", pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte);
        pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm = pnd_Coding_Sheets_Pnd_1st_Pay_Due_DteRedef19.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Mm", 
            "#1ST-PAY-DUE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy = pnd_Coding_Sheets_Pnd_1st_Pay_Due_DteRedef19.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Pay_Due_Dte_Yy", 
            "#1ST-PAY-DUE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte", "#LST-MAN-CHK-DTE", 
            FieldType.STRING, 4);
        pnd_Coding_Sheets_Pnd_Lst_Man_Chk_DteRedef20 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_Lst_Man_Chk_DteRedef20", 
            "Redefines", pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte);
        pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm = pnd_Coding_Sheets_Pnd_Lst_Man_Chk_DteRedef20.newFieldInGroup("pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Mm", 
            "#LST-MAN-CHK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy = pnd_Coding_Sheets_Pnd_Lst_Man_Chk_DteRedef20.newFieldInGroup("pnd_Coding_Sheets_Pnd_Lst_Man_Chk_Dte_Yy", 
            "#LST-MAN-CHK-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte", "#FIN-PER-PAY-DTE", 
            FieldType.STRING, 4);
        pnd_Coding_Sheets_Pnd_Fin_Per_Pay_DteRedef21 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_Fin_Per_Pay_DteRedef21", 
            "Redefines", pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte);
        pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm = pnd_Coding_Sheets_Pnd_Fin_Per_Pay_DteRedef21.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Mm", 
            "#FIN-PER-PAY-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy = pnd_Coding_Sheets_Pnd_Fin_Per_Pay_DteRedef21.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Per_Pay_Dte_Yy", 
            "#FIN-PER-PAY-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte", "#FIN-PYMT-DTE", 
            FieldType.STRING, 6);
        pnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22", 
            "Redefines", pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte);
        pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm = pnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Mm", "#FIN-PYMT-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd = pnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Dd", "#FIN-PYMT-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy = pnd_Coding_Sheets_Pnd_Fin_Pymt_DteRedef22.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Pymt_Dte_Yy", "#FIN-PYMT-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Wdrawal_Dte = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Wdrawal_Dte", "#WDRAWAL-DTE", 
            FieldType.STRING, 4);
        pnd_Coding_Sheets_Pnd_Wdrawal_DteRedef23 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newGroupInGroup("pnd_Coding_Sheets_Pnd_Wdrawal_DteRedef23", 
            "Redefines", pnd_Coding_Sheets_Pnd_Wdrawal_Dte);
        pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm = pnd_Coding_Sheets_Pnd_Wdrawal_DteRedef23.newFieldInGroup("pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Mm", "#WDRAWAL-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy = pnd_Coding_Sheets_Pnd_Wdrawal_DteRedef23.newFieldInGroup("pnd_Coding_Sheets_Pnd_Wdrawal_Dte_Yy", "#WDRAWAL-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Filler = pnd_Coding_Sheets_Pnd_Rest_Of_Issue1Redef13.newFieldInGroup("pnd_Coding_Sheets_Pnd_Filler", "#FILLER", FieldType.STRING, 
            4);
        pnd_Coding_Sheets_Pnd_User_Area1 = pnd_Coding_Sheets_Pnd_Manual_New_Issue1Redef11.newFieldInGroup("pnd_Coding_Sheets_Pnd_User_Area1", "#USER-AREA1", 
            FieldType.STRING, 6);
        pnd_Coding_SheetsRedef24 = newGroupInRecord("pnd_Coding_SheetsRedef24", "Redefines", pnd_Coding_Sheets);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue2 = pnd_Coding_SheetsRedef24.newFieldInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue2", "#MANUAL-NEW-ISSUE2", 
            FieldType.STRING, 100);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25 = pnd_Coding_SheetsRedef24.newGroupInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25", "Redefines", 
            pnd_Coding_Sheets_Pnd_Manual_New_Issue2);
        pnd_Coding_Sheets_Pnd_Batch_Nbr2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Batch_Nbr2", "#BATCH-NBR2", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Coding_Sheets_Pnd_Check_Dte2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte2", "#CHECK-DTE2", 
            FieldType.NUMERIC, 6);
        pnd_Coding_Sheets_Pnd_Check_Dte2Redef26 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newGroupInGroup("pnd_Coding_Sheets_Pnd_Check_Dte2Redef26", 
            "Redefines", pnd_Coding_Sheets_Pnd_Check_Dte2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Mm2 = pnd_Coding_Sheets_Pnd_Check_Dte2Redef26.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Mm2", "#CHECK-DTE-MM2", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Dd2 = pnd_Coding_Sheets_Pnd_Check_Dte2Redef26.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Dd2", "#CHECK-DTE-DD2", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Yy2 = pnd_Coding_Sheets_Pnd_Check_Dte2Redef26.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Yy2", "#CHECK-DTE-YY2", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cntrct_Nbr2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cntrct_Nbr2", "#CNTRCT-NBR2", 
            FieldType.STRING, 8);
        pnd_Coding_Sheets_Pnd_Record_Status2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Status2", 
            "#RECORD-STATUS2", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr2", 
            "#CROSS-REF-NBR2", FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_Trans_Nbr2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Trans_Nbr2", "#TRANS-NBR2", 
            FieldType.NUMERIC, 3);
        pnd_Coding_Sheets_Pnd_Record_Type2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Type2", "#RECORD-TYPE2", 
            FieldType.STRING, 2);
        pnd_Coding_Sheets_Pnd_Record_Nbr2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Nbr2", "#RECORD-NBR2", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue2", 
            "#REST-OF-ISSUE2", FieldType.STRING, 45);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newGroupInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27", 
            "Redefines", pnd_Coding_Sheets_Pnd_Rest_Of_Issue2);
        pnd_Coding_Sheets_Pnd_Citizen = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Citizen", "#CITIZEN", FieldType.NUMERIC, 
            3);
        pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Iss = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Iss", 
            "#ST/CNTRY-ISS", FieldType.STRING, 3);
        pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Res = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Stfslash_Cntry_Res", 
            "#ST/CNTRY-RES", FieldType.STRING, 3);
        pnd_Coding_Sheets_Pnd_Coll_Iss = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Coll_Iss", "#COLL-ISS", FieldType.STRING, 
            5);
        pnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Rtbfslash_Ttb_Amt", 
            "#RTB/TTB-AMT", FieldType.DECIMAL, 9,2);
        pnd_Coding_Sheets_Pnd_Ssn = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 
            9);
        pnd_Coding_Sheets_Pnd_Joint_Cnvrt = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Joint_Cnvrt", "#JOINT-CNVRT", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Spirt = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Spirt", "#SPIRT", FieldType.STRING, 
            1);
        pnd_Coding_Sheets_Pnd_Pen_Pln_Cde = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Pen_Pln_Cde", "#PEN-PLN-CDE", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Cntrct_Type = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cntrct_Type", "#CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Filler1 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue2Redef27.newFieldInGroup("pnd_Coding_Sheets_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            9);
        pnd_Coding_Sheets_Pnd_User_Area2 = pnd_Coding_Sheets_Pnd_Manual_New_Issue2Redef25.newFieldInGroup("pnd_Coding_Sheets_Pnd_User_Area2", "#USER-AREA2", 
            FieldType.STRING, 6);
        pnd_Coding_SheetsRedef28 = newGroupInRecord("pnd_Coding_SheetsRedef28", "Redefines", pnd_Coding_Sheets);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue3 = pnd_Coding_SheetsRedef28.newFieldInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue3", "#MANUAL-NEW-ISSUE3", 
            FieldType.STRING, 100);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29 = pnd_Coding_SheetsRedef28.newGroupInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29", "Redefines", 
            pnd_Coding_Sheets_Pnd_Manual_New_Issue3);
        pnd_Coding_Sheets_Pnd_Batch_Nbr3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Batch_Nbr3", "#BATCH-NBR3", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Coding_Sheets_Pnd_Check_Dte3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte3", "#CHECK-DTE3", 
            FieldType.NUMERIC, 6);
        pnd_Coding_Sheets_Pnd_Check_Dte3Redef30 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newGroupInGroup("pnd_Coding_Sheets_Pnd_Check_Dte3Redef30", 
            "Redefines", pnd_Coding_Sheets_Pnd_Check_Dte3);
        pnd_Coding_Sheets_Pnd_Check_Dte_Mm3 = pnd_Coding_Sheets_Pnd_Check_Dte3Redef30.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Mm3", "#CHECK-DTE-MM3", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Dd3 = pnd_Coding_Sheets_Pnd_Check_Dte3Redef30.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Dd3", "#CHECK-DTE-DD3", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Yy3 = pnd_Coding_Sheets_Pnd_Check_Dte3Redef30.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Yy3", "#CHECK-DTE-YY3", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cntrct_Nbr3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cntrct_Nbr3", "#CNTRCT-NBR3", 
            FieldType.STRING, 8);
        pnd_Coding_Sheets_Pnd_Record_Status3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Status3", 
            "#RECORD-STATUS3", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr3", 
            "#CROSS-REF-NBR3", FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_Trans_Nbr3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Trans_Nbr3", "#TRANS-NBR3", 
            FieldType.NUMERIC, 3);
        pnd_Coding_Sheets_Pnd_Record_Type3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Type3", "#RECORD-TYPE3", 
            FieldType.STRING, 2);
        pnd_Coding_Sheets_Pnd_Record_Nbr3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Nbr3", "#RECORD-NBR3", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue3", 
            "#REST-OF-ISSUE3", FieldType.STRING, 45);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newGroupInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31", 
            "Redefines", pnd_Coding_Sheets_Pnd_Rest_Of_Issue3);
        pnd_Coding_Sheets_Pnd_1st_Annt_X_Ref = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Annt_X_Ref", "#1ST-ANNT-X-REF", 
            FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newGroupInGroup("pnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32", 
            "Redefines", pnd_Coding_Sheets_Pnd_1st_Annt_X_Ref);
        pnd_Coding_Sheets_Pnd_1st_Last_Name = pnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Last_Name", "#1ST-LAST-NAME", 
            FieldType.STRING, 7);
        pnd_Coding_Sheets_Pnd_1st_First_Init = pnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_First_Init", "#1ST-FIRST-INIT", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_1st_Mid_Init = pnd_Coding_Sheets_Pnd_1st_Annt_X_RefRedef32.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Mid_Init", "#1ST-MID-INIT", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_1st_Sex = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Sex", "#1ST-SEX", FieldType.STRING, 
            1);
        pnd_Coding_Sheets_Pnd_1st_Dob = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Dob", "#1ST-DOB", FieldType.NUMERIC, 
            6);
        pnd_Coding_Sheets_Pnd_1st_DobRedef33 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newGroupInGroup("pnd_Coding_Sheets_Pnd_1st_DobRedef33", "Redefines", 
            pnd_Coding_Sheets_Pnd_1st_Dob);
        pnd_Coding_Sheets_Pnd_1st_Dob_Mm = pnd_Coding_Sheets_Pnd_1st_DobRedef33.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Dob_Mm", "#1ST-DOB-MM", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_1st_Dob_Dd = pnd_Coding_Sheets_Pnd_1st_DobRedef33.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Dob_Dd", "#1ST-DOB-DD", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_1st_Dob_Yy = pnd_Coding_Sheets_Pnd_1st_DobRedef33.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Dob_Yy", "#1ST-DOB-YY", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_1st_Dod = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Dod", "#1ST-DOD", FieldType.NUMERIC, 
            4);
        pnd_Coding_Sheets_Pnd_1st_DodRedef34 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newGroupInGroup("pnd_Coding_Sheets_Pnd_1st_DodRedef34", "Redefines", 
            pnd_Coding_Sheets_Pnd_1st_Dod);
        pnd_Coding_Sheets_Pnd_1st_Dod_Mm = pnd_Coding_Sheets_Pnd_1st_DodRedef34.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Dod_Mm", "#1ST-DOD-MM", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_1st_Dod_Yy = pnd_Coding_Sheets_Pnd_1st_DodRedef34.newFieldInGroup("pnd_Coding_Sheets_Pnd_1st_Dod_Yy", "#1ST-DOD-YY", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_Mort_Yob3 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Mort_Yob3", "#MORT-YOB3", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Mort_Yob3Redef35 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newGroupInGroup("pnd_Coding_Sheets_Pnd_Mort_Yob3Redef35", 
            "Redefines", pnd_Coding_Sheets_Pnd_Mort_Yob3);
        pnd_Coding_Sheets_Pnd_Mort_Yob3_Yy = pnd_Coding_Sheets_Pnd_Mort_Yob3Redef35.newFieldInGroup("pnd_Coding_Sheets_Pnd_Mort_Yob3_Yy", "#MORT-YOB3-YY", 
            FieldType.STRING, 2);
        pnd_Coding_Sheets_Pnd_Life_Cnt3 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Life_Cnt3", "#LIFE-CNT3", 
            FieldType.NUMERIC, 1);
        pnd_Coding_Sheets_Pnd_Div_Payee = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Div_Payee", "#DIV-PAYEE", 
            FieldType.NUMERIC, 1);
        pnd_Coding_Sheets_Pnd_Coll_Cde = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Coll_Cde", "#COLL-CDE", FieldType.STRING, 
            5);
        pnd_Coding_Sheets_Pnd_Orig_Cntrct = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Orig_Cntrct", "#ORIG-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Coding_Sheets_Pnd_Cash_Cde = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 
            1);
        pnd_Coding_Sheets_Pnd_Emp_Term = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Emp_Term", "#EMP-TERM", FieldType.STRING, 
            1);
        pnd_Coding_Sheets_Pnd_Filler2 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue3Redef31.newFieldInGroup("pnd_Coding_Sheets_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            6);
        pnd_Coding_Sheets_Pnd_User_Area3 = pnd_Coding_Sheets_Pnd_Manual_New_Issue3Redef29.newFieldInGroup("pnd_Coding_Sheets_Pnd_User_Area3", "#USER-AREA3", 
            FieldType.STRING, 6);
        pnd_Coding_SheetsRedef36 = newGroupInRecord("pnd_Coding_SheetsRedef36", "Redefines", pnd_Coding_Sheets);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue4 = pnd_Coding_SheetsRedef36.newFieldInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue4", "#MANUAL-NEW-ISSUE4", 
            FieldType.STRING, 100);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37 = pnd_Coding_SheetsRedef36.newGroupInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37", "Redefines", 
            pnd_Coding_Sheets_Pnd_Manual_New_Issue4);
        pnd_Coding_Sheets_Pnd_Batch_Nbr4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Batch_Nbr4", "#BATCH-NBR4", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Coding_Sheets_Pnd_Check_Dte4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte4", "#CHECK-DTE4", 
            FieldType.NUMERIC, 6);
        pnd_Coding_Sheets_Pnd_Check_Dte4Redef38 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newGroupInGroup("pnd_Coding_Sheets_Pnd_Check_Dte4Redef38", 
            "Redefines", pnd_Coding_Sheets_Pnd_Check_Dte4);
        pnd_Coding_Sheets_Pnd_Check_Dte_Mm4 = pnd_Coding_Sheets_Pnd_Check_Dte4Redef38.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Mm4", "#CHECK-DTE-MM4", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Dd4 = pnd_Coding_Sheets_Pnd_Check_Dte4Redef38.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Dd4", "#CHECK-DTE-DD4", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Yy4 = pnd_Coding_Sheets_Pnd_Check_Dte4Redef38.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Yy4", "#CHECK-DTE-YY4", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cntrct_Nbr4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cntrct_Nbr4", "#CNTRCT-NBR4", 
            FieldType.STRING, 8);
        pnd_Coding_Sheets_Pnd_Record_Status4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Status4", 
            "#RECORD-STATUS4", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr4", 
            "#CROSS-REF-NBR4", FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_Trans_Nbr4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Trans_Nbr4", "#TRANS-NBR4", 
            FieldType.NUMERIC, 3);
        pnd_Coding_Sheets_Pnd_Record_Type4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Type4", "#RECORD-TYPE4", 
            FieldType.STRING, 2);
        pnd_Coding_Sheets_Pnd_Record_Nbr4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Nbr4", "#RECORD-NBR4", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue4", 
            "#REST-OF-ISSUE4", FieldType.STRING, 45);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newGroupInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39", 
            "Redefines", pnd_Coding_Sheets_Pnd_Rest_Of_Issue4);
        pnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref", "#2ND-ANNT-X-REF", 
            FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newGroupInGroup("pnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40", 
            "Redefines", pnd_Coding_Sheets_Pnd_2nd_Annt_X_Ref);
        pnd_Coding_Sheets_Pnd_2nd_Last_Name = pnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Last_Name", "#2ND-LAST-NAME", 
            FieldType.STRING, 7);
        pnd_Coding_Sheets_Pnd_2nd_First_Init = pnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_First_Init", "#2ND-FIRST-INIT", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_2nd_Mid_Init = pnd_Coding_Sheets_Pnd_2nd_Annt_X_RefRedef40.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Mid_Init", "#2ND-MID-INIT", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_2nd_Sex = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Sex", "#2ND-SEX", FieldType.STRING, 
            1);
        pnd_Coding_Sheets_Pnd_2nd_Dob = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Dob", "#2ND-DOB", FieldType.NUMERIC, 
            6);
        pnd_Coding_Sheets_Pnd_2nd_DobRedef41 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newGroupInGroup("pnd_Coding_Sheets_Pnd_2nd_DobRedef41", "Redefines", 
            pnd_Coding_Sheets_Pnd_2nd_Dob);
        pnd_Coding_Sheets_Pnd_2nd_Dob_Mm = pnd_Coding_Sheets_Pnd_2nd_DobRedef41.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Dob_Mm", "#2ND-DOB-MM", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_2nd_Dob_Dd = pnd_Coding_Sheets_Pnd_2nd_DobRedef41.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Dob_Dd", "#2ND-DOB-DD", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_2nd_Dob_Yy = pnd_Coding_Sheets_Pnd_2nd_DobRedef41.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Dob_Yy", "#2ND-DOB-YY", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_2nd_Dod = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Dod", "#2ND-DOD", FieldType.NUMERIC, 
            4);
        pnd_Coding_Sheets_Pnd_2nd_DodRedef42 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newGroupInGroup("pnd_Coding_Sheets_Pnd_2nd_DodRedef42", "Redefines", 
            pnd_Coding_Sheets_Pnd_2nd_Dod);
        pnd_Coding_Sheets_Pnd_2nd_Dod_Mm = pnd_Coding_Sheets_Pnd_2nd_DodRedef42.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Dod_Mm", "#2ND-DOD-MM", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_2nd_Dod_Yy = pnd_Coding_Sheets_Pnd_2nd_DodRedef42.newFieldInGroup("pnd_Coding_Sheets_Pnd_2nd_Dod_Yy", "#2ND-DOD-YY", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_Mort_Yob4 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_Mort_Yob4", "#MORT-YOB4", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Mort_Yob4Redef43 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newGroupInGroup("pnd_Coding_Sheets_Pnd_Mort_Yob4Redef43", 
            "Redefines", pnd_Coding_Sheets_Pnd_Mort_Yob4);
        pnd_Coding_Sheets_Pnd_Mort_Yob4_Yy = pnd_Coding_Sheets_Pnd_Mort_Yob4Redef43.newFieldInGroup("pnd_Coding_Sheets_Pnd_Mort_Yob4_Yy", "#MORT-YOB4-YY", 
            FieldType.STRING, 2);
        pnd_Coding_Sheets_Pnd_Life_Cnt4 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_Life_Cnt4", "#LIFE-CNT4", 
            FieldType.NUMERIC, 1);
        pnd_Coding_Sheets_Pnd_Ben_Xref = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_Ben_Xref", "#BEN-XREF", FieldType.STRING, 
            9);
        pnd_Coding_Sheets_Pnd_Ben_Dod = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_Ben_Dod", "#BEN-DOD", FieldType.NUMERIC, 
            4);
        pnd_Coding_Sheets_Pnd_Ben_DodRedef44 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newGroupInGroup("pnd_Coding_Sheets_Pnd_Ben_DodRedef44", "Redefines", 
            pnd_Coding_Sheets_Pnd_Ben_Dod);
        pnd_Coding_Sheets_Pnd_Ben_Dod_A = pnd_Coding_Sheets_Pnd_Ben_DodRedef44.newFieldInGroup("pnd_Coding_Sheets_Pnd_Ben_Dod_A", "#BEN-DOD-A", FieldType.STRING, 
            4);
        pnd_Coding_Sheets_Pnd_Ben_DodRedef45 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newGroupInGroup("pnd_Coding_Sheets_Pnd_Ben_DodRedef45", "Redefines", 
            pnd_Coding_Sheets_Pnd_Ben_Dod);
        pnd_Coding_Sheets_Pnd_Ben_Dod_Mm = pnd_Coding_Sheets_Pnd_Ben_DodRedef45.newFieldInGroup("pnd_Coding_Sheets_Pnd_Ben_Dod_Mm", "#BEN-DOD-MM", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_Ben_Dod_Yy = pnd_Coding_Sheets_Pnd_Ben_DodRedef45.newFieldInGroup("pnd_Coding_Sheets_Pnd_Ben_Dod_Yy", "#BEN-DOD-YY", FieldType.NUMERIC, 
            2);
        pnd_Coding_Sheets_Pnd_Dest_Prev = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_Dest_Prev", "#DEST-PREV", 
            FieldType.STRING, 4);
        pnd_Coding_Sheets_Pnd_Dest_Curr = pnd_Coding_Sheets_Pnd_Rest_Of_Issue4Redef39.newFieldInGroup("pnd_Coding_Sheets_Pnd_Dest_Curr", "#DEST-CURR", 
            FieldType.STRING, 4);
        pnd_Coding_Sheets_Pnd_User_Area4 = pnd_Coding_Sheets_Pnd_Manual_New_Issue4Redef37.newFieldInGroup("pnd_Coding_Sheets_Pnd_User_Area4", "#USER-AREA4", 
            FieldType.STRING, 6);
        pnd_Coding_SheetsRedef46 = newGroupInRecord("pnd_Coding_SheetsRedef46", "Redefines", pnd_Coding_Sheets);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue5 = pnd_Coding_SheetsRedef46.newFieldInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue5", "#MANUAL-NEW-ISSUE5", 
            FieldType.STRING, 100);
        pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47 = pnd_Coding_SheetsRedef46.newGroupInGroup("pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47", "Redefines", 
            pnd_Coding_Sheets_Pnd_Manual_New_Issue5);
        pnd_Coding_Sheets_Pnd_Batch_Nbr5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Batch_Nbr5", "#BATCH-NBR5", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Coding_Sheets_Pnd_Check_Dte5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte5", "#CHECK-DTE5", 
            FieldType.NUMERIC, 6);
        pnd_Coding_Sheets_Pnd_Check_Dte5Redef48 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newGroupInGroup("pnd_Coding_Sheets_Pnd_Check_Dte5Redef48", 
            "Redefines", pnd_Coding_Sheets_Pnd_Check_Dte5);
        pnd_Coding_Sheets_Pnd_Check_Dte_Mm5 = pnd_Coding_Sheets_Pnd_Check_Dte5Redef48.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Mm5", "#CHECK-DTE-MM5", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Dd5 = pnd_Coding_Sheets_Pnd_Check_Dte5Redef48.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Dd5", "#CHECK-DTE-DD5", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Check_Dte_Yy5 = pnd_Coding_Sheets_Pnd_Check_Dte5Redef48.newFieldInGroup("pnd_Coding_Sheets_Pnd_Check_Dte_Yy5", "#CHECK-DTE-YY5", 
            FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cntrct_Nbr5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cntrct_Nbr5", "#CNTRCT-NBR5", 
            FieldType.STRING, 8);
        pnd_Coding_Sheets_Pnd_Record_Status5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Status5", 
            "#RECORD-STATUS5", FieldType.NUMERIC, 2);
        pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cross_Ref_Nbr5", 
            "#CROSS-REF-NBR5", FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_Trans_Nbr5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Trans_Nbr5", "#TRANS-NBR5", 
            FieldType.NUMERIC, 3);
        pnd_Coding_Sheets_Pnd_Record_Type5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Type5", "#RECORD-TYPE5", 
            FieldType.STRING, 2);
        pnd_Coding_Sheets_Pnd_Record_Nbr5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Nbr5", "#RECORD-NBR5", 
            FieldType.STRING, 1);
        pnd_Coding_Sheets_Pnd_Record_Nbr5Redef49 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newGroupInGroup("pnd_Coding_Sheets_Pnd_Record_Nbr5Redef49", 
            "Redefines", pnd_Coding_Sheets_Pnd_Record_Nbr5);
        pnd_Coding_Sheets_Pnd_Record_Nbr5_N = pnd_Coding_Sheets_Pnd_Record_Nbr5Redef49.newFieldInGroup("pnd_Coding_Sheets_Pnd_Record_Nbr5_N", "#RECORD-NBR5-N", 
            FieldType.NUMERIC, 1);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue5", 
            "#REST-OF-ISSUE5", FieldType.STRING, 45);
        pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newGroupInGroup("pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50", 
            "Redefines", pnd_Coding_Sheets_Pnd_Rest_Of_Issue5);
        pnd_Coding_Sheets_Pnd_Rate = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newFieldInGroup("pnd_Coding_Sheets_Pnd_Rate", "#RATE", FieldType.STRING, 
            2);
        pnd_Coding_Sheets_Pnd_Per_Pay = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newFieldInGroup("pnd_Coding_Sheets_Pnd_Per_Pay", "#PER-PAY", FieldType.PACKED_DECIMAL, 
            13,2);
        pnd_Coding_Sheets_Pnd_Per_PayRedef51 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newGroupInGroup("pnd_Coding_Sheets_Pnd_Per_PayRedef51", "Redefines", 
            pnd_Coding_Sheets_Pnd_Per_Pay);
        pnd_Coding_Sheets_Pnd_Cref_Units = pnd_Coding_Sheets_Pnd_Per_PayRedef51.newFieldInGroup("pnd_Coding_Sheets_Pnd_Cref_Units", "#CREF-UNITS", FieldType.PACKED_DECIMAL, 
            11,3);
        pnd_Coding_Sheets_Pnd_Per_Div = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newFieldInGroup("pnd_Coding_Sheets_Pnd_Per_Div", "#PER-DIV", FieldType.DECIMAL, 
            9,2);
        pnd_Coding_Sheets_Pnd_Fin_Pay = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Pay", "#FIN-PAY", FieldType.DECIMAL, 
            9,2);
        pnd_Coding_Sheets_Pnd_Fin_PayRedef52 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newGroupInGroup("pnd_Coding_Sheets_Pnd_Fin_PayRedef52", "Redefines", 
            pnd_Coding_Sheets_Pnd_Fin_Pay);
        pnd_Coding_Sheets_Pnd_Fin_Pay_A = pnd_Coding_Sheets_Pnd_Fin_PayRedef52.newFieldInGroup("pnd_Coding_Sheets_Pnd_Fin_Pay_A", "#FIN-PAY-A", FieldType.STRING, 
            9);
        pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newFieldInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div", "#TOT-OLD-PER-DIV", 
            FieldType.DECIMAL, 9,2);
        pnd_Coding_Sheets_Pnd_Tot_Old_Per_DivRedef53 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newGroupInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Per_DivRedef53", 
            "Redefines", pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div);
        pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div_A = pnd_Coding_Sheets_Pnd_Tot_Old_Per_DivRedef53.newFieldInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Per_Div_A", 
            "#TOT-OLD-PER-DIV-A", FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newFieldInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay", "#TOT-OLD-PER-PAY", 
            FieldType.DECIMAL, 9,2);
        pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef54 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newGroupInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef54", 
            "Redefines", pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay);
        pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay_A = pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef54.newFieldInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay_A", 
            "#TOT-OLD-PER-PAY-A", FieldType.STRING, 9);
        pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef55 = pnd_Coding_Sheets_Pnd_Rest_Of_Issue5Redef50.newGroupInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef55", 
            "Redefines", pnd_Coding_Sheets_Pnd_Tot_Old_Per_Pay);
        pnd_Coding_Sheets_Pnd_Tot_Old_Cref_Units = pnd_Coding_Sheets_Pnd_Tot_Old_Per_PayRedef55.newFieldInGroup("pnd_Coding_Sheets_Pnd_Tot_Old_Cref_Units", 
            "#TOT-OLD-CREF-UNITS", FieldType.DECIMAL, 9,3);
        pnd_Coding_Sheets_Pnd_User_Area5 = pnd_Coding_Sheets_Pnd_Manual_New_Issue5Redef47.newFieldInGroup("pnd_Coding_Sheets_Pnd_User_Area5", "#USER-AREA5", 
            FieldType.STRING, 6);

        pnd_Cntrct_Key = newFieldInRecord("pnd_Cntrct_Key", "#CNTRCT-KEY", FieldType.STRING, 17);
        pnd_Cntrct_KeyRedef56 = newGroupInRecord("pnd_Cntrct_KeyRedef56", "Redefines", pnd_Cntrct_Key);
        pnd_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_KeyRedef56.newFieldInGroup("pnd_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Key_Pnd_Lst_Trans_Dte = pnd_Cntrct_KeyRedef56.newFieldInGroup("pnd_Cntrct_Key_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.PACKED_DECIMAL, 
            7);

        pnd_Cntrct_Payee_Key = newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);
        pnd_Cntrct_Payee_KeyRedef57 = newGroupInRecord("pnd_Cntrct_Payee_KeyRedef57", "Redefines", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_KeyRedef57.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_KeyRedef57.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Cntrct_Bfre_Key = newFieldInRecord("pnd_Cntrct_Bfre_Key", "#CNTRCT-BFRE-KEY", FieldType.STRING, 18);
        pnd_Cntrct_Bfre_KeyRedef58 = newGroupInRecord("pnd_Cntrct_Bfre_KeyRedef58", "Redefines", pnd_Cntrct_Bfre_Key);
        pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cntrct_Bfre_KeyRedef58.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Bfre_KeyRedef58.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte = pnd_Cntrct_Bfre_KeyRedef58.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Cntrct_Aftr_Key = newFieldInRecord("pnd_Cntrct_Aftr_Key", "#CNTRCT-AFTR-KEY", FieldType.STRING, 18);
        pnd_Cntrct_Aftr_KeyRedef59 = newGroupInRecord("pnd_Cntrct_Aftr_KeyRedef59", "Redefines", pnd_Cntrct_Aftr_Key);
        pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cntrct_Aftr_KeyRedef59.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Aftr_KeyRedef59.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Cntrct_Aftr_KeyRedef59.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.TIME);

        pnd_Cpr_Bfre_Key = newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);
        pnd_Cpr_Bfre_KeyRedef60 = newGroupInRecord("pnd_Cpr_Bfre_KeyRedef60", "Redefines", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cpr_Bfre_KeyRedef60.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Bfre_KeyRedef60.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Bfre_KeyRedef60.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_Trans_Dte = pnd_Cpr_Bfre_KeyRedef60.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Cpr_Aftr_Key = newFieldInRecord("pnd_Cpr_Aftr_Key", "#CPR-AFTR-KEY", FieldType.STRING, 20);
        pnd_Cpr_Aftr_KeyRedef61 = newGroupInRecord("pnd_Cpr_Aftr_KeyRedef61", "Redefines", pnd_Cpr_Aftr_Key);
        pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cpr_Aftr_KeyRedef61.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cpr_Aftr_KeyRedef61.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Aftr_KeyRedef61.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Cpr_Aftr_KeyRedef61.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.TIME);

        pnd_Cref_Fund_Key = newFieldInRecord("pnd_Cref_Fund_Key", "#CREF-FUND-KEY", FieldType.STRING, 15);
        pnd_Cref_Fund_KeyRedef62 = newGroupInRecord("pnd_Cref_Fund_KeyRedef62", "Redefines", pnd_Cref_Fund_Key);
        pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Ppcn_Nbr = pnd_Cref_Fund_KeyRedef62.newFieldInGroup("pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Ppcn_Nbr", "#CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Payee_Cde = pnd_Cref_Fund_KeyRedef62.newFieldInGroup("pnd_Cref_Fund_Key_Pnd_Cref_Cntrct_Payee_Cde", "#CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde = pnd_Cref_Fund_KeyRedef62.newFieldInGroup("pnd_Cref_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde", "#CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3);

        pnd_Tiaa_Fund_Key = newFieldInRecord("pnd_Tiaa_Fund_Key", "#TIAA-FUND-KEY", FieldType.STRING, 15);
        pnd_Tiaa_Fund_KeyRedef63 = newGroupInRecord("pnd_Tiaa_Fund_KeyRedef63", "Redefines", pnd_Tiaa_Fund_Key);
        pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr = pnd_Tiaa_Fund_KeyRedef63.newFieldInGroup("pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr", "#TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde = pnd_Tiaa_Fund_KeyRedef63.newFieldInGroup("pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde", "#TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde = pnd_Tiaa_Fund_KeyRedef63.newFieldInGroup("pnd_Tiaa_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde", "#TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3);

        pnd_Cref_Fund_Trans = newFieldInRecord("pnd_Cref_Fund_Trans", "#CREF-FUND-TRANS", FieldType.STRING, 22);
        pnd_Cref_Fund_TransRedef64 = newGroupInRecord("pnd_Cref_Fund_TransRedef64", "Redefines", pnd_Cref_Fund_Trans);
        pnd_Cref_Fund_Trans_Pnd_Bfre_Imge_Id = pnd_Cref_Fund_TransRedef64.newFieldInGroup("pnd_Cref_Fund_Trans_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Ppcn_Nbr = pnd_Cref_Fund_TransRedef64.newFieldInGroup("pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Ppcn_Nbr", "#CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Payee_Cde = pnd_Cref_Fund_TransRedef64.newFieldInGroup("pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Payee_Cde", "#CREF-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Fund_Cde = pnd_Cref_Fund_TransRedef64.newFieldInGroup("pnd_Cref_Fund_Trans_Pnd_Cref_Cntrct_Fund_Cde", "#CREF-CNTRCT-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Cref_Fund_Trans_Pnd_Trans_Dte = pnd_Cref_Fund_TransRedef64.newFieldInGroup("pnd_Cref_Fund_Trans_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Tiaa_Fund_Trans = newFieldInRecord("pnd_Tiaa_Fund_Trans", "#TIAA-FUND-TRANS", FieldType.STRING, 22);
        pnd_Tiaa_Fund_TransRedef65 = newGroupInRecord("pnd_Tiaa_Fund_TransRedef65", "Redefines", pnd_Tiaa_Fund_Trans);
        pnd_Tiaa_Fund_Trans_Pnd_Bfre_Imge_Id = pnd_Tiaa_Fund_TransRedef65.newFieldInGroup("pnd_Tiaa_Fund_Trans_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Ppcn_Nbr = pnd_Tiaa_Fund_TransRedef65.newFieldInGroup("pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Ppcn_Nbr", "#TIAA-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Payee_Cde = pnd_Tiaa_Fund_TransRedef65.newFieldInGroup("pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Payee_Cde", "#TIAA-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Fund_Cde = pnd_Tiaa_Fund_TransRedef65.newFieldInGroup("pnd_Tiaa_Fund_Trans_Pnd_Tiaa_Fund_Cde", "#TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Tiaa_Fund_Trans_Pnd_Trans_Dte = pnd_Tiaa_Fund_TransRedef65.newFieldInGroup("pnd_Tiaa_Fund_Trans_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Tiaa_Fund_Bfre_Key = newFieldInRecord("pnd_Tiaa_Fund_Bfre_Key", "#TIAA-FUND-BFRE-KEY", FieldType.STRING, 22);
        pnd_Tiaa_Fund_Bfre_KeyRedef66 = newGroupInRecord("pnd_Tiaa_Fund_Bfre_KeyRedef66", "Redefines", pnd_Tiaa_Fund_Bfre_Key);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Tiaa_Fund_Bfre_KeyRedef66.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr = pnd_Tiaa_Fund_Bfre_KeyRedef66.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr", 
            "#TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde = pnd_Tiaa_Fund_Bfre_KeyRedef66.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Cntrct_Payee_Cde", 
            "#TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Fund_Cde = pnd_Tiaa_Fund_Bfre_KeyRedef66.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Tiaa_Fund_Cde", "#TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Tiaa_Fund_Bfre_Key_Pnd_Trans_Dte = pnd_Tiaa_Fund_Bfre_KeyRedef66.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Cref_Fund_Bfre_Key = newFieldInRecord("pnd_Cref_Fund_Bfre_Key", "#CREF-FUND-BFRE-KEY", FieldType.STRING, 22);
        pnd_Cref_Fund_Bfre_KeyRedef67 = newGroupInRecord("pnd_Cref_Fund_Bfre_KeyRedef67", "Redefines", pnd_Cref_Fund_Bfre_Key);
        pnd_Cref_Fund_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cref_Fund_Bfre_KeyRedef67.newFieldInGroup("pnd_Cref_Fund_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Ppcn_Nbr = pnd_Cref_Fund_Bfre_KeyRedef67.newFieldInGroup("pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Ppcn_Nbr", 
            "#CREF-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Payee_Cde = pnd_Cref_Fund_Bfre_KeyRedef67.newFieldInGroup("pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Cntrct_Payee_Cde", 
            "#CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Fund_Cde = pnd_Cref_Fund_Bfre_KeyRedef67.newFieldInGroup("pnd_Cref_Fund_Bfre_Key_Pnd_Cref_Fund_Cde", "#CREF-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Cref_Fund_Bfre_Key_Pnd_Trans_Dte = pnd_Cref_Fund_Bfre_KeyRedef67.newFieldInGroup("pnd_Cref_Fund_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Misc_Variables = newGroupInRecord("pnd_Misc_Variables", "#MISC-VARIABLES");
        pnd_Misc_Variables_Pnd_Batch_Nbr = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Batch_Nbr", "#BATCH-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Variables_Pnd_Check_Dte_Mm = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Check_Dte_Dd = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Check_Dte_Yy = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Cntrct_Nbr = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Misc_Variables_Pnd_Record_Status = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Record_Status", "#RECORD-STATUS", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Cross_Ref_Nbr = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_Misc_Variables_Pnd_Rec_Type = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Trans_Nbr = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 3);
        pnd_Misc_Variables_Pnd_Ccyymm_Dte = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Ccyymm_Dte", "#CCYYMM-DTE", FieldType.NUMERIC, 
            6);
        pnd_Misc_Variables_Pnd_Ccyymm_DteRedef68 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables_Pnd_Ccyymm_DteRedef68", "Redefines", pnd_Misc_Variables_Pnd_Ccyymm_Dte);
        pnd_Misc_Variables_Pnd_Ccyymm_Dte_Cc = pnd_Misc_Variables_Pnd_Ccyymm_DteRedef68.newFieldInGroup("pnd_Misc_Variables_Pnd_Ccyymm_Dte_Cc", "#CCYYMM-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy = pnd_Misc_Variables_Pnd_Ccyymm_DteRedef68.newFieldInGroup("pnd_Misc_Variables_Pnd_Ccyymm_Dte_Yy", "#CCYYMM-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm = pnd_Misc_Variables_Pnd_Ccyymm_DteRedef68.newFieldInGroup("pnd_Misc_Variables_Pnd_Ccyymm_Dte_Mm", "#CCYYMM-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8 = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8", "#CCYYMMDD-DTE8", FieldType.NUMERIC, 
            8);
        pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69", "Redefines", pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8);
        pnd_Misc_Variables_Pnd_Dte8_Cc = pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69.newFieldInGroup("pnd_Misc_Variables_Pnd_Dte8_Cc", "#DTE8-CC", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Dte8_Yy = pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69.newFieldInGroup("pnd_Misc_Variables_Pnd_Dte8_Yy", "#DTE8-YY", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Dte8_Mm = pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69.newFieldInGroup("pnd_Misc_Variables_Pnd_Dte8_Mm", "#DTE8-MM", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Dte8_Dd = pnd_Misc_Variables_Pnd_Ccyymmdd_Dte8Redef69.newFieldInGroup("pnd_Misc_Variables_Pnd_Dte8_Dd", "#DTE8-DD", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Ccyy_Dte4 = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Ccyy_Dte4", "#CCYY-DTE4", FieldType.NUMERIC, 4);
        pnd_Misc_Variables_Pnd_Ccyy_Dte4Redef70 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables_Pnd_Ccyy_Dte4Redef70", "Redefines", pnd_Misc_Variables_Pnd_Ccyy_Dte4);
        pnd_Misc_Variables_Pnd_Dte4_Cc = pnd_Misc_Variables_Pnd_Ccyy_Dte4Redef70.newFieldInGroup("pnd_Misc_Variables_Pnd_Dte4_Cc", "#DTE4-CC", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Dte4_Yy = pnd_Misc_Variables_Pnd_Ccyy_Dte4Redef70.newFieldInGroup("pnd_Misc_Variables_Pnd_Dte4_Yy", "#DTE4-YY", FieldType.NUMERIC, 
            2);
        pnd_Misc_Variables_Pnd_Pay_Dte = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Pay_Dte", "#PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Misc_Variables_Pnd_Pay_DteRedef71 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables_Pnd_Pay_DteRedef71", "Redefines", pnd_Misc_Variables_Pnd_Pay_Dte);
        pnd_Misc_Variables_Pnd_Pay_Dte_Cc = pnd_Misc_Variables_Pnd_Pay_DteRedef71.newFieldInGroup("pnd_Misc_Variables_Pnd_Pay_Dte_Cc", "#PAY-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Pay_Dte_Yy = pnd_Misc_Variables_Pnd_Pay_DteRedef71.newFieldInGroup("pnd_Misc_Variables_Pnd_Pay_Dte_Yy", "#PAY-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Pay_Dte_Mm = pnd_Misc_Variables_Pnd_Pay_DteRedef71.newFieldInGroup("pnd_Misc_Variables_Pnd_Pay_Dte_Mm", "#PAY-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Pay_Dte_Dd = pnd_Misc_Variables_Pnd_Pay_DteRedef71.newFieldInGroup("pnd_Misc_Variables_Pnd_Pay_Dte_Dd", "#PAY-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte", "#HOLD-WTHDRWL-DTE", FieldType.NUMERIC, 
            6);
        pnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72", "Redefines", 
            pnd_Misc_Variables_Pnd_Hold_Wthdrwl_Dte);
        pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Cc = pnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72.newFieldInGroup("pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Cc", 
            "#WTHDRWL-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy = pnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72.newFieldInGroup("pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Yy", 
            "#WTHDRWL-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm = pnd_Misc_Variables_Pnd_Hold_Wthdrwl_DteRedef72.newFieldInGroup("pnd_Misc_Variables_Pnd_Wthdrwl_Dte_Mm", 
            "#WTHDRWL-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Indx = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Indx", "#INDX", FieldType.NUMERIC, 3);
        pnd_Misc_Variables_Pnd_Indx1 = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Indx1", "#INDX1", FieldType.NUMERIC, 3);

        pnd_Logical_Variables = newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_No_Cntrct_Rec = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_No_Cntrct_Rec", "#NO-CNTRCT-REC", FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_No_Record_Found = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_No_Record_Found", "#NO-RECORD-FOUND", 
            FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Fin_Pay_Gt_0 = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Fin_Pay_Gt_0", "#FIN-PAY-GT-0", FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Rate_Changed = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Rate_Changed", "#RATE-CHANGED", FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Issue_101_Written = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Issue_101_Written", "#ISSUE-101-WRITTEN", 
            FieldType.BOOLEAN);
        vw_iaa_Cntrct_Prtcpnt_Role.setUniquePeList();
        vw_iaa_Cpr_Trans.setUniquePeList();
        vw_iaa_Tiaa_Fund_Rcrd.setUniquePeList();
        vw_iaa_Tiaa_Fund_Trans.setUniquePeList();
        vw_iaa_Cref_Fund_Rcrd_1.setUniquePeList();
        vw_iaa_Cref_Fund_Trans_1.setUniquePeList();

        this.setRecordName("LdaIaal902b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Cref_Fund_Rcrd_1.reset();
        vw_iaa_Cref_Fund_Trans_1.reset();
    }

    // Constructor
    public LdaIaal902b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
