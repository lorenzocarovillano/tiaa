/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:49 PM
**        * FROM NATURAL LDA     : IATL420
************************************************************
**        * FILE NAME            : LdaIatl420.java
**        * CLASS NAME           : LdaIatl420
**        * INSTANCE NAME        : LdaIatl420
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIatl420 extends DbsRecord
{
    // Properties
    private DbsField pnd_Rec_Acpt;
    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_C;
    private DbsField pnd_Text_Table;
    private DbsField pnd_Units_Cnt;
    private DbsField pnd_Fund_Cd_From;
    private DbsField pnd_Fund_Cd_To;
    private DbsField pnd_To_Sub;
    private DbsField pnd_From_Sub;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;
    private DbsGroup pnd_Tiaa_Cntrct_Fund_KeyRedef1;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee;
    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd_View;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef4;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef6;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsField pnd_Sys_Date;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef7;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsField pnd_Ctl_Frm_Units;
    private DbsField pnd_Ctl_Frm_Dollars;
    private DbsField pnd_Ctl_Frm_Rcrds;
    private DbsField pnd_Ctl_Frm_Full_Out_Rcrds;
    private DbsField pnd_Ctl_Frm_Asset_Amt;
    private DbsField pnd_Ctl_Frm_Cntrctrl_Amt;
    private DbsField pnd_Ctl_Frm_Per_Divid_Amt;
    private DbsField pnd_Ctl_To_Units;
    private DbsField pnd_Ctl_To_Dollars;
    private DbsField pnd_Ctl_To_Dollars_Hold;
    private DbsField pnd_Ctl_To_Rcrds;
    private DbsField pnd_Ctl_To_New_Fund_Rcrds;
    private DbsField pnd_Ctl_To_Asset_Amt;
    private DbsField pnd_Ctl_To_Cntrctrl_Amt;
    private DbsField pnd_Ctl_To_Per_Divid_Amt;
    private DbsField pnd_Cref_To_Cref;
    private DbsField pnd_Teachers_To_Teachers;
    private DbsField pnd_Full_Transfer;
    private DbsField pnd_Partial_Transfer;
    private DbsField pnd_Teachers_Fund_Conv;

    public DbsField getPnd_Rec_Acpt() { return pnd_Rec_Acpt; }

    public DataAccessProgramView getVw_iaa_Cntrct_Prtcpnt_Role() { return vw_iaa_Cntrct_Prtcpnt_Role; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde; }

    public DbsGroup getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Acctng_Cde() { return iaa_Cntrct_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte() { return iaa_Cntrct_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Type_Cde() { return iaa_Cntrct_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Lst_Trans_Dte() { return iaa_Cntrct_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Type() { return iaa_Cntrct_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getPnd_Rec_Read() { return pnd_Rec_Read; }

    public DbsField getPnd_C() { return pnd_C; }

    public DbsField getPnd_Text_Table() { return pnd_Text_Table; }

    public DbsField getPnd_Units_Cnt() { return pnd_Units_Cnt; }

    public DbsField getPnd_Fund_Cd_From() { return pnd_Fund_Cd_From; }

    public DbsField getPnd_Fund_Cd_To() { return pnd_Fund_Cd_To; }

    public DbsField getPnd_To_Sub() { return pnd_To_Sub; }

    public DbsField getPnd_From_Sub() { return pnd_From_Sub; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key() { return pnd_Tiaa_Cntrct_Fund_Key; }

    public DbsGroup getPnd_Tiaa_Cntrct_Fund_KeyRedef1() { return pnd_Tiaa_Cntrct_Fund_KeyRedef1; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee; }

    public DbsGroup getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd_View() { return vw_iaa_Tiaa_Fund_Rcrd_View; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef4() { return pnd_Sy_Date_Yymmdd_AlphRedef4; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef6() { return pnd_Sy_Time_Hhiiss_AlphRedef6; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef7() { return iaa_Parm_Card_Pnd_Parm_DateRedef7; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsField getPnd_Ctl_Frm_Units() { return pnd_Ctl_Frm_Units; }

    public DbsField getPnd_Ctl_Frm_Dollars() { return pnd_Ctl_Frm_Dollars; }

    public DbsField getPnd_Ctl_Frm_Rcrds() { return pnd_Ctl_Frm_Rcrds; }

    public DbsField getPnd_Ctl_Frm_Full_Out_Rcrds() { return pnd_Ctl_Frm_Full_Out_Rcrds; }

    public DbsField getPnd_Ctl_Frm_Asset_Amt() { return pnd_Ctl_Frm_Asset_Amt; }

    public DbsField getPnd_Ctl_Frm_Cntrctrl_Amt() { return pnd_Ctl_Frm_Cntrctrl_Amt; }

    public DbsField getPnd_Ctl_Frm_Per_Divid_Amt() { return pnd_Ctl_Frm_Per_Divid_Amt; }

    public DbsField getPnd_Ctl_To_Units() { return pnd_Ctl_To_Units; }

    public DbsField getPnd_Ctl_To_Dollars() { return pnd_Ctl_To_Dollars; }

    public DbsField getPnd_Ctl_To_Dollars_Hold() { return pnd_Ctl_To_Dollars_Hold; }

    public DbsField getPnd_Ctl_To_Rcrds() { return pnd_Ctl_To_Rcrds; }

    public DbsField getPnd_Ctl_To_New_Fund_Rcrds() { return pnd_Ctl_To_New_Fund_Rcrds; }

    public DbsField getPnd_Ctl_To_Asset_Amt() { return pnd_Ctl_To_Asset_Amt; }

    public DbsField getPnd_Ctl_To_Cntrctrl_Amt() { return pnd_Ctl_To_Cntrctrl_Amt; }

    public DbsField getPnd_Ctl_To_Per_Divid_Amt() { return pnd_Ctl_To_Per_Divid_Amt; }

    public DbsField getPnd_Cref_To_Cref() { return pnd_Cref_To_Cref; }

    public DbsField getPnd_Teachers_To_Teachers() { return pnd_Teachers_To_Teachers; }

    public DbsField getPnd_Full_Transfer() { return pnd_Full_Transfer; }

    public DbsField getPnd_Partial_Transfer() { return pnd_Partial_Transfer; }

    public DbsField getPnd_Teachers_Fund_Conv() { return pnd_Teachers_Fund_Conv; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Rec_Acpt = newFieldInRecord("pnd_Rec_Acpt", "#REC-ACPT", FieldType.NUMERIC, 6);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");

        pnd_Rec_Read = newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 6);

        pnd_C = newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);

        pnd_Text_Table = newFieldArrayInRecord("pnd_Text_Table", "#TEXT-TABLE", FieldType.STRING, 6, new DbsArrayController(1,10));

        pnd_Units_Cnt = newFieldInRecord("pnd_Units_Cnt", "#UNITS-CNT", FieldType.DECIMAL, 9,3);

        pnd_Fund_Cd_From = newFieldInRecord("pnd_Fund_Cd_From", "#FUND-CD-FROM", FieldType.STRING, 1);

        pnd_Fund_Cd_To = newFieldInRecord("pnd_Fund_Cd_To", "#FUND-CD-TO", FieldType.STRING, 1);

        pnd_To_Sub = newFieldInRecord("pnd_To_Sub", "#TO-SUB", FieldType.NUMERIC, 2);

        pnd_From_Sub = newFieldInRecord("pnd_From_Sub", "#FROM-SUB", FieldType.NUMERIC, 2);

        pnd_Tiaa_Cntrct_Fund_Key = newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);
        pnd_Tiaa_Cntrct_Fund_KeyRedef1 = newGroupInRecord("pnd_Tiaa_Cntrct_Fund_KeyRedef1", "Redefines", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee", "#TIAA-PAYEE", 
            FieldType.STRING, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2 = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newGroupInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2", 
            "Redefines", pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N = pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N", 
            "#TIAA-PAYEE-N", FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund", "#TIAA-FUND", 
            FieldType.STRING, 3);

        vw_iaa_Tiaa_Fund_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd_View", "IAA-TIAA-FUND-RCRD-VIEW"), "IAA_TIAA_FUND_RCRD", 
            "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr", 
            "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde", 
            "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde", 
            "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3 = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde", 
            "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde", 
            "TIAA-FUND-CDE", FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt", 
            "TIAA-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt", 
            "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt", 
            "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt", 
            "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt", 
            "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp", 
            "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde", 
            "TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte", 
            "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt", 
            "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt", 
            "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt", 
            "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup", 
            "TIAA_RATE_GICMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic", 
            "TIAA-RATE-GIC", FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind", "TIAA-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund", 
            "TIAA-OLD-CMPNY-FUND", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte", 
            "TIAA-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte", 
            "TIAA-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte", 
            "TIAA-LST-XFR-OUT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key", 
            "TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef4 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef4", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef4.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5 = pnd_Sy_Date_Yymmdd_AlphRedef4.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef5.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef6 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef6", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef6.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef7 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef7", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Ctl_Frm_Units = newFieldArrayInRecord("pnd_Ctl_Frm_Units", "#CTL-FRM-UNITS", FieldType.DECIMAL, 11,3, new DbsArrayController(1,40));

        pnd_Ctl_Frm_Dollars = newFieldArrayInRecord("pnd_Ctl_Frm_Dollars", "#CTL-FRM-DOLLARS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,40));

        pnd_Ctl_Frm_Rcrds = newFieldArrayInRecord("pnd_Ctl_Frm_Rcrds", "#CTL-FRM-RCRDS", FieldType.NUMERIC, 7, new DbsArrayController(1,40));

        pnd_Ctl_Frm_Full_Out_Rcrds = newFieldArrayInRecord("pnd_Ctl_Frm_Full_Out_Rcrds", "#CTL-FRM-FULL-OUT-RCRDS", FieldType.NUMERIC, 7, new DbsArrayController(1,
            40));

        pnd_Ctl_Frm_Asset_Amt = newFieldArrayInRecord("pnd_Ctl_Frm_Asset_Amt", "#CTL-FRM-ASSET-AMT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            40));

        pnd_Ctl_Frm_Cntrctrl_Amt = newFieldArrayInRecord("pnd_Ctl_Frm_Cntrctrl_Amt", "#CTL-FRM-CNTRCTRL-AMT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            40));

        pnd_Ctl_Frm_Per_Divid_Amt = newFieldArrayInRecord("pnd_Ctl_Frm_Per_Divid_Amt", "#CTL-FRM-PER-DIVID-AMT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            40));

        pnd_Ctl_To_Units = newFieldArrayInRecord("pnd_Ctl_To_Units", "#CTL-TO-UNITS", FieldType.DECIMAL, 11,3, new DbsArrayController(1,40));

        pnd_Ctl_To_Dollars = newFieldArrayInRecord("pnd_Ctl_To_Dollars", "#CTL-TO-DOLLARS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,40));

        pnd_Ctl_To_Dollars_Hold = newFieldInRecord("pnd_Ctl_To_Dollars_Hold", "#CTL-TO-DOLLARS-HOLD", FieldType.DECIMAL, 11,2);

        pnd_Ctl_To_Rcrds = newFieldArrayInRecord("pnd_Ctl_To_Rcrds", "#CTL-TO-RCRDS", FieldType.NUMERIC, 7, new DbsArrayController(1,40));

        pnd_Ctl_To_New_Fund_Rcrds = newFieldArrayInRecord("pnd_Ctl_To_New_Fund_Rcrds", "#CTL-TO-NEW-FUND-RCRDS", FieldType.NUMERIC, 7, new DbsArrayController(1,
            40));

        pnd_Ctl_To_Asset_Amt = newFieldArrayInRecord("pnd_Ctl_To_Asset_Amt", "#CTL-TO-ASSET-AMT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,40));

        pnd_Ctl_To_Cntrctrl_Amt = newFieldArrayInRecord("pnd_Ctl_To_Cntrctrl_Amt", "#CTL-TO-CNTRCTRL-AMT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            40));

        pnd_Ctl_To_Per_Divid_Amt = newFieldArrayInRecord("pnd_Ctl_To_Per_Divid_Amt", "#CTL-TO-PER-DIVID-AMT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            40));

        pnd_Cref_To_Cref = newFieldInRecord("pnd_Cref_To_Cref", "#CREF-TO-CREF", FieldType.STRING, 1);

        pnd_Teachers_To_Teachers = newFieldInRecord("pnd_Teachers_To_Teachers", "#TEACHERS-TO-TEACHERS", FieldType.STRING, 1);

        pnd_Full_Transfer = newFieldInRecord("pnd_Full_Transfer", "#FULL-TRANSFER", FieldType.STRING, 1);

        pnd_Partial_Transfer = newFieldInRecord("pnd_Partial_Transfer", "#PARTIAL-TRANSFER", FieldType.STRING, 1);

        pnd_Teachers_Fund_Conv = newFieldInRecord("pnd_Teachers_Fund_Conv", "#TEACHERS-FUND-CONV", FieldType.STRING, 1);
        vw_iaa_Cntrct_Prtcpnt_Role.setUniquePeList();
        vw_iaa_Tiaa_Fund_Rcrd_View.setUniquePeList();

        this.setRecordName("LdaIatl420");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Tiaa_Fund_Rcrd_View.reset();
    }

    // Constructor
    public LdaIatl420() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
