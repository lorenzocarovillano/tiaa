/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:03:22 PM
**        * FROM NATURAL LDA     : IAAL915
************************************************************
**        * FILE NAME            : LdaIaal915.java
**        * CLASS NAME           : LdaIaal915
**        * INSTANCE NAME        : LdaIaal915
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal915 extends DbsRecord
{
    // Properties
    private DbsField pnd_Iaa_Parm_Card;
    private DbsGroup pnd_Iaa_Parm_CardRedef1;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Program_Id;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler1;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte;
    private DbsGroup pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef2;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N;
    private DbsGroup pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler2;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte;
    private DbsGroup pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef4;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_N;
    private DbsGroup pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Cc;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Yy;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Mm;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Dd;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler3;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte;
    private DbsGroup pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef6;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_N;
    private DbsGroup pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Cc;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Yy;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Mm;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Dd;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler4;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_User_Area;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler5;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_User_Id;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler6;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Id;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler7;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Filler8;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Totals;
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField pnd_Ws_Trans_304_Rcrd;
    private DbsGroup pnd_Ws_Trans_304_RcrdRedef8;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Dte;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cde;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde;
    private DbsField pnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Area;
    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField pnd_Check_Dte;
    private DbsGroup pnd_Check_DteRedef9;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_N;
    private DbsGroup pnd_Check_DteRedef10;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_Mm;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_Dd;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_Yy;
    private DbsField pnd_Comp_Dte;
    private DbsGroup pnd_Comp_DteRedef11;
    private DbsField pnd_Comp_Dte_Pnd_Comp_Dte_N;
    private DbsField pnd_Save_Trans_Ppcn_Nbr;
    private DbsField pnd_Save_Trans_Payee_Cde;
    private DbsField pnd_Save_Trans_Cde;
    private DbsField pnd_Cntrl_Frst_Trans_Dte_A;
    private DbsField pnd_Cntrl_Frst_Trans_Dte_T;
    private DbsField pnd_Cntrl_Rcrd_Key;
    private DbsGroup pnd_Cntrl_Rcrd_KeyRedef12;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Last_Batch_Nbr;
    private DbsField pnd_Seq_Nbr;
    private DbsField pnd_Mode;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key;
    private DbsGroup pnd_Iaa_Cntrct_Prtcpnt_KeyRedef13;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Trans_Aft_Key;
    private DbsGroup pnd_Iaa_Cpr_Trans_Aft_KeyRedef14;
    private DbsField pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Iaa_Cpr_Trans_Key;
    private DbsGroup pnd_Iaa_Cpr_Trans_KeyRedef15;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte;
    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_1st_Grp;
    private DbsField pnd_Logical_Variables_Pnd_Records_Processed;
    private DbsField pnd_Logical_Variables_Pnd_Bypass;
    private DbsGroup pnd_Packed_Variables;
    private DbsField pnd_Packed_Variables_Pnd_Records_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Records_Processed_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Final_Tot;

    public DbsField getPnd_Iaa_Parm_Card() { return pnd_Iaa_Parm_Card; }

    public DbsGroup getPnd_Iaa_Parm_CardRedef1() { return pnd_Iaa_Parm_CardRedef1; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Program_Id() { return pnd_Iaa_Parm_Card_Pnd_Program_Id; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler1() { return pnd_Iaa_Parm_Card_Pnd_Filler1; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte; }

    public DbsGroup getPnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef2() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef2; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N; }

    public DbsGroup getPnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd() { return pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler2() { return pnd_Iaa_Parm_Card_Pnd_Filler2; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte; }

    public DbsGroup getPnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef4() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef4; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_N() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_N; }

    public DbsGroup getPnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Cc() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Cc; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Yy() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Yy; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Mm() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Mm; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Dd() { return pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Dd; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler3() { return pnd_Iaa_Parm_Card_Pnd_Filler3; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte; }

    public DbsGroup getPnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef6() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef6; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_N() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_N; }

    public DbsGroup getPnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Cc() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Cc; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Yy() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Yy; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Mm() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Mm; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Dd() { return pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Dd; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler4() { return pnd_Iaa_Parm_Card_Pnd_Filler4; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_User_Area() { return pnd_Iaa_Parm_Card_Pnd_Parm_User_Area; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler5() { return pnd_Iaa_Parm_Card_Pnd_Filler5; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_User_Id() { return pnd_Iaa_Parm_Card_Pnd_Parm_User_Id; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler6() { return pnd_Iaa_Parm_Card_Pnd_Filler6; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Verify_Id() { return pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Id; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler7() { return pnd_Iaa_Parm_Card_Pnd_Filler7; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde() { return pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Filler8() { return pnd_Iaa_Parm_Card_Pnd_Filler8; }

    public DbsField getPnd_Iaa_Parm_Card_Pnd_Parm_Totals() { return pnd_Iaa_Parm_Card_Pnd_Parm_Totals; }

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Acctng_Cde() { return iaa_Cntrct_Cntrct_Acctng_Cde; }

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd() { return vw_iaa_Cntrl_Rcrd; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Check_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte; }

    public DataAccessProgramView getVw_iaa_Trans_Rcrd() { return vw_iaa_Trans_Rcrd; }

    public DbsField getIaa_Trans_Rcrd_Trans_Dte() { return iaa_Trans_Rcrd_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Invrse_Trans_Dte() { return iaa_Trans_Rcrd_Invrse_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Lst_Trans_Dte() { return iaa_Trans_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Ppcn_Nbr() { return iaa_Trans_Rcrd_Trans_Ppcn_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Payee_Cde() { return iaa_Trans_Rcrd_Trans_Payee_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Sub_Cde() { return iaa_Trans_Rcrd_Trans_Sub_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cde() { return iaa_Trans_Rcrd_Trans_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Actvty_Cde() { return iaa_Trans_Rcrd_Trans_Actvty_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Check_Dte() { return iaa_Trans_Rcrd_Trans_Check_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Area() { return iaa_Trans_Rcrd_Trans_User_Area; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Id() { return iaa_Trans_Rcrd_Trans_User_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Cde() { return iaa_Trans_Rcrd_Trans_Verify_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Id() { return iaa_Trans_Rcrd_Trans_Verify_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cmbne_Cde() { return iaa_Trans_Rcrd_Trans_Cmbne_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Effective_Dte() { return iaa_Trans_Rcrd_Trans_Effective_Dte; }

    public DbsField getPnd_Ws_Trans_304_Rcrd() { return pnd_Ws_Trans_304_Rcrd; }

    public DbsGroup getPnd_Ws_Trans_304_RcrdRedef8() { return pnd_Ws_Trans_304_RcrdRedef8; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte() { return pnd_Ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Dte() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Dte; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte() { return pnd_Ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cde() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cde; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde; }

    public DbsField getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Area() { return pnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Area; }

    public DataAccessProgramView getVw_iaa_Cntrct_Prtcpnt_Role() { return vw_iaa_Cntrct_Prtcpnt_Role; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Trans_Dte() { return iaa_Cpr_Trans_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Actvty_Cde() { return iaa_Cpr_Trans_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Mode_Ind() { return iaa_Cpr_Trans_Cntrct_Mode_Ind; }

    public DbsField getPnd_Check_Dte() { return pnd_Check_Dte; }

    public DbsGroup getPnd_Check_DteRedef9() { return pnd_Check_DteRedef9; }

    public DbsField getPnd_Check_Dte_Pnd_Check_Dte_N() { return pnd_Check_Dte_Pnd_Check_Dte_N; }

    public DbsGroup getPnd_Check_DteRedef10() { return pnd_Check_DteRedef10; }

    public DbsField getPnd_Check_Dte_Pnd_Check_Dte_Mm() { return pnd_Check_Dte_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Check_Dte_Pnd_Check_Dte_Dd() { return pnd_Check_Dte_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Check_Dte_Pnd_Check_Dte_Yy() { return pnd_Check_Dte_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Comp_Dte() { return pnd_Comp_Dte; }

    public DbsGroup getPnd_Comp_DteRedef11() { return pnd_Comp_DteRedef11; }

    public DbsField getPnd_Comp_Dte_Pnd_Comp_Dte_N() { return pnd_Comp_Dte_Pnd_Comp_Dte_N; }

    public DbsField getPnd_Save_Trans_Ppcn_Nbr() { return pnd_Save_Trans_Ppcn_Nbr; }

    public DbsField getPnd_Save_Trans_Payee_Cde() { return pnd_Save_Trans_Payee_Cde; }

    public DbsField getPnd_Save_Trans_Cde() { return pnd_Save_Trans_Cde; }

    public DbsField getPnd_Cntrl_Frst_Trans_Dte_A() { return pnd_Cntrl_Frst_Trans_Dte_A; }

    public DbsField getPnd_Cntrl_Frst_Trans_Dte_T() { return pnd_Cntrl_Frst_Trans_Dte_T; }

    public DbsField getPnd_Cntrl_Rcrd_Key() { return pnd_Cntrl_Rcrd_Key; }

    public DbsGroup getPnd_Cntrl_Rcrd_KeyRedef12() { return pnd_Cntrl_Rcrd_KeyRedef12; }

    public DbsField getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde() { return pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde; }

    public DbsField getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte() { return pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte; }

    public DbsField getPnd_Last_Batch_Nbr() { return pnd_Last_Batch_Nbr; }

    public DbsField getPnd_Seq_Nbr() { return pnd_Seq_Nbr; }

    public DbsField getPnd_Mode() { return pnd_Mode; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key() { return pnd_Iaa_Cntrct_Prtcpnt_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Prtcpnt_KeyRedef13() { return pnd_Iaa_Cntrct_Prtcpnt_KeyRedef13; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Trans_Aft_Key() { return pnd_Iaa_Cpr_Trans_Aft_Key; }

    public DbsGroup getPnd_Iaa_Cpr_Trans_Aft_KeyRedef14() { return pnd_Iaa_Cpr_Trans_Aft_KeyRedef14; }

    public DbsField getPnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Aftr_Imge_Id() { return pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Invrse_Trans_Dte() { return pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key() { return pnd_Iaa_Cpr_Trans_Key; }

    public DbsGroup getPnd_Iaa_Cpr_Trans_KeyRedef15() { return pnd_Iaa_Cpr_Trans_KeyRedef15; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Imge_Id() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Logical_Variables() { return pnd_Logical_Variables; }

    public DbsField getPnd_Logical_Variables_Pnd_1st_Grp() { return pnd_Logical_Variables_Pnd_1st_Grp; }

    public DbsField getPnd_Logical_Variables_Pnd_Records_Processed() { return pnd_Logical_Variables_Pnd_Records_Processed; }

    public DbsField getPnd_Logical_Variables_Pnd_Bypass() { return pnd_Logical_Variables_Pnd_Bypass; }

    public DbsGroup getPnd_Packed_Variables() { return pnd_Packed_Variables; }

    public DbsField getPnd_Packed_Variables_Pnd_Records_Ctr() { return pnd_Packed_Variables_Pnd_Records_Ctr; }

    public DbsField getPnd_Packed_Variables_Pnd_Records_Processed_Ctr() { return pnd_Packed_Variables_Pnd_Records_Processed_Ctr; }

    public DbsField getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr() { return pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr; }

    public DbsField getPnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr() { return pnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr; }

    public DbsField getPnd_Packed_Variables_Pnd_Final_Tot() { return pnd_Packed_Variables_Pnd_Final_Tot; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iaa_Parm_Card = newFieldInRecord("pnd_Iaa_Parm_Card", "#IAA-PARM-CARD", FieldType.STRING, 64);
        pnd_Iaa_Parm_CardRedef1 = newGroupInRecord("pnd_Iaa_Parm_CardRedef1", "Redefines", pnd_Iaa_Parm_Card);
        pnd_Iaa_Parm_Card_Pnd_Program_Id = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Program_Id", "#PROGRAM-ID", FieldType.STRING, 
            8);
        pnd_Iaa_Parm_Card_Pnd_Filler1 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", FieldType.STRING, 
            8);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef2 = pnd_Iaa_Parm_CardRedef1.newGroupInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef2", "Redefines", 
            pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N = pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N", 
            "#PARM-CHECK-DTE-N", FieldType.NUMERIC, 8);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3 = pnd_Iaa_Parm_CardRedef1.newGroupInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3", "Redefines", 
            pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc = pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc", 
            "#PARM-CHECK-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy = pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy", 
            "#PARM-CHECK-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm = pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm", 
            "#PARM-CHECK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd = pnd_Iaa_Parm_Card_Pnd_Parm_Check_DteRedef3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd", 
            "#PARM-CHECK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Filler2 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte", "#PARM-FROM-DTE", FieldType.STRING, 
            8);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef4 = pnd_Iaa_Parm_CardRedef1.newGroupInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef4", "Redefines", 
            pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_N = pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef4.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_N", "#PARM-FROM-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5 = pnd_Iaa_Parm_CardRedef1.newGroupInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5", "Redefines", 
            pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Cc = pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Cc", "#PARM-FROM-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Yy = pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Yy", "#PARM-FROM-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Mm = pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Mm", "#PARM-FROM-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Dd = pnd_Iaa_Parm_Card_Pnd_Parm_From_DteRedef5.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_From_Dte_Dd", "#PARM-FROM-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Filler3 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler3", "#FILLER3", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte", "#PARM-TO-DTE", FieldType.STRING, 
            8);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef6 = pnd_Iaa_Parm_CardRedef1.newGroupInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef6", "Redefines", pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_N = pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef6.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_N", "#PARM-TO-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7 = pnd_Iaa_Parm_CardRedef1.newGroupInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7", "Redefines", pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Cc = pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Cc", "#PARM-TO-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Yy = pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Yy", "#PARM-TO-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Mm = pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Mm", "#PARM-TO-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Dd = pnd_Iaa_Parm_Card_Pnd_Parm_To_DteRedef7.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_To_Dte_Dd", "#PARM-TO-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Filler4 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler4", "#FILLER4", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_User_Area = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_User_Area", "#PARM-USER-AREA", FieldType.STRING, 
            6);
        pnd_Iaa_Parm_Card_Pnd_Filler5 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler5", "#FILLER5", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_User_Id = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_User_Id", "#PARM-USER-ID", FieldType.STRING, 
            8);
        pnd_Iaa_Parm_Card_Pnd_Filler6 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler6", "#FILLER6", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Id = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Id", "#PARM-VERIFY-ID", FieldType.STRING, 
            8);
        pnd_Iaa_Parm_Card_Pnd_Filler7 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler7", "#FILLER7", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Verify_Cde", "#PARM-VERIFY-CDE", FieldType.STRING, 
            1);
        pnd_Iaa_Parm_Card_Pnd_Filler8 = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Filler8", "#FILLER8", FieldType.STRING, 1);
        pnd_Iaa_Parm_Card_Pnd_Parm_Totals = pnd_Iaa_Parm_CardRedef1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Totals", "#PARM-TOTALS", FieldType.STRING, 
            1);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");

        pnd_Ws_Trans_304_Rcrd = newFieldInRecord("pnd_Ws_Trans_304_Rcrd", "#WS-TRANS-304-RCRD", FieldType.STRING, 74);
        pnd_Ws_Trans_304_RcrdRedef8 = newGroupInRecord("pnd_Ws_Trans_304_RcrdRedef8", "Redefines", pnd_Ws_Trans_304_Rcrd);
        pnd_Ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte", 
            "#CNTRL-FRST-TRANS-DTE", FieldType.TIME);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Dte = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde", "#TRANS-SUB-CDE", 
            FieldType.STRING, 3);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cde = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde", "#TRANS-ACTVTY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde", "#TRANS-CMBNE-CDE", 
            FieldType.STRING, 12);
        pnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Area = pnd_Ws_Trans_304_RcrdRedef8.newFieldInGroup("pnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Area", "#TRANS-USER-AREA", 
            FieldType.STRING, 6);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");

        pnd_Check_Dte = newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 8);
        pnd_Check_DteRedef9 = newGroupInRecord("pnd_Check_DteRedef9", "Redefines", pnd_Check_Dte);
        pnd_Check_Dte_Pnd_Check_Dte_N = pnd_Check_DteRedef9.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_N", "#CHECK-DTE-N", FieldType.NUMERIC, 8);
        pnd_Check_DteRedef10 = newGroupInRecord("pnd_Check_DteRedef10", "Redefines", pnd_Check_Dte);
        pnd_Check_Dte_Pnd_Check_Dte_Mm = pnd_Check_DteRedef10.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Check_Dte_Pnd_Check_Dte_Dd = pnd_Check_DteRedef10.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Check_Dte_Pnd_Check_Dte_Yy = pnd_Check_DteRedef10.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", FieldType.NUMERIC, 2);

        pnd_Comp_Dte = newFieldInRecord("pnd_Comp_Dte", "#COMP-DTE", FieldType.STRING, 8);
        pnd_Comp_DteRedef11 = newGroupInRecord("pnd_Comp_DteRedef11", "Redefines", pnd_Comp_Dte);
        pnd_Comp_Dte_Pnd_Comp_Dte_N = pnd_Comp_DteRedef11.newFieldInGroup("pnd_Comp_Dte_Pnd_Comp_Dte_N", "#COMP-DTE-N", FieldType.NUMERIC, 8);

        pnd_Save_Trans_Ppcn_Nbr = newFieldInRecord("pnd_Save_Trans_Ppcn_Nbr", "#SAVE-TRANS-PPCN-NBR", FieldType.STRING, 10);

        pnd_Save_Trans_Payee_Cde = newFieldInRecord("pnd_Save_Trans_Payee_Cde", "#SAVE-TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Save_Trans_Cde = newFieldInRecord("pnd_Save_Trans_Cde", "#SAVE-TRANS-CDE", FieldType.NUMERIC, 3);

        pnd_Cntrl_Frst_Trans_Dte_A = newFieldInRecord("pnd_Cntrl_Frst_Trans_Dte_A", "#CNTRL-FRST-TRANS-DTE-A", FieldType.STRING, 6);

        pnd_Cntrl_Frst_Trans_Dte_T = newFieldInRecord("pnd_Cntrl_Frst_Trans_Dte_T", "#CNTRL-FRST-TRANS-DTE-T", FieldType.TIME);

        pnd_Cntrl_Rcrd_Key = newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);
        pnd_Cntrl_Rcrd_KeyRedef12 = newGroupInRecord("pnd_Cntrl_Rcrd_KeyRedef12", "Redefines", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_KeyRedef12.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_KeyRedef12.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Last_Batch_Nbr = newFieldInRecord("pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.NUMERIC, 4);

        pnd_Seq_Nbr = newFieldInRecord("pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);

        pnd_Mode = newFieldInRecord("pnd_Mode", "#MODE", FieldType.NUMERIC, 3);

        pnd_Iaa_Cntrct_Prtcpnt_Key = newFieldInRecord("pnd_Iaa_Cntrct_Prtcpnt_Key", "#IAA-CNTRCT-PRTCPNT-KEY", FieldType.STRING, 12);
        pnd_Iaa_Cntrct_Prtcpnt_KeyRedef13 = newGroupInRecord("pnd_Iaa_Cntrct_Prtcpnt_KeyRedef13", "Redefines", pnd_Iaa_Cntrct_Prtcpnt_Key);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef13.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef13.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Iaa_Cpr_Trans_Aft_Key = newFieldInRecord("pnd_Iaa_Cpr_Trans_Aft_Key", "#IAA-CPR-TRANS-AFT-KEY", FieldType.STRING, 25);
        pnd_Iaa_Cpr_Trans_Aft_KeyRedef14 = newGroupInRecord("pnd_Iaa_Cpr_Trans_Aft_KeyRedef14", "Redefines", pnd_Iaa_Cpr_Trans_Aft_Key);
        pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Aftr_Imge_Id = pnd_Iaa_Cpr_Trans_Aft_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Trans_Aft_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Trans_Aft_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Invrse_Trans_Dte = pnd_Iaa_Cpr_Trans_Aft_KeyRedef14.newFieldInGroup("pnd_Iaa_Cpr_Trans_Aft_Key_Pnd_Invrse_Trans_Dte", 
            "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);

        pnd_Iaa_Cpr_Trans_Key = newFieldInRecord("pnd_Iaa_Cpr_Trans_Key", "#IAA-CPR-TRANS-KEY", FieldType.STRING, 20);
        pnd_Iaa_Cpr_Trans_KeyRedef15 = newGroupInRecord("pnd_Iaa_Cpr_Trans_KeyRedef15", "Redefines", pnd_Iaa_Cpr_Trans_Key);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Imge_Id = pnd_Iaa_Cpr_Trans_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Trans_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Trans_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte = pnd_Iaa_Cpr_Trans_KeyRedef15.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Logical_Variables = newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_1st_Grp = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_1st_Grp", "#1ST-GRP", FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Records_Processed = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Records_Processed", "#RECORDS-PROCESSED", 
            FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Bypass = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Bypass", "#BYPASS", FieldType.BOOLEAN);

        pnd_Packed_Variables = newGroupInRecord("pnd_Packed_Variables", "#PACKED-VARIABLES");
        pnd_Packed_Variables_Pnd_Records_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Ctr", "#RECORDS-CTR", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Records_Processed_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Processed_Ctr", "#RECORDS-PROCESSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr", "#RECORDS-BYPASSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr", 
            "#304-TRANS-TO-PROCESS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Final_Tot = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Final_Tot", "#FINAL-TOT", FieldType.PACKED_DECIMAL, 
            12);

        this.setRecordName("LdaIaal915");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cpr_Trans.reset();
    }

    // Constructor
    public LdaIaal915() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
