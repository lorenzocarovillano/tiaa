/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:13 PM
**        * FROM NATURAL PDA     : FCPA105
************************************************************
**        * FILE NAME            : PdaFcpa105.java
**        * CLASS NAME           : PdaFcpa105
**        * INSTANCE NAME        : PdaFcpa105
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa105 extends PdaBase
{
    // Properties
    private DbsGroup pnd_P_Vars;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Payee_Cde;
    private DbsGroup pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N;
    private DbsField pnd_P_Vars_Pnd_Pin;
    private DbsField pnd_P_Vars_Pnd_Ph_Data_Rcd_Type;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type;
    private DbsField pnd_P_Vars_Pnd_Ssn;
    private DbsField pnd_P_Vars_Pnd_Dob_Ccyymmdd;
    private DbsField pnd_P_Vars_Pnd_Dod_Ccyymmdd;
    private DbsGroup pnd_P_Vars_Ph_Nme;
    private DbsField pnd_P_Vars_Ph_Prfx_Nme;
    private DbsField pnd_P_Vars_Ph_Last_Nme;
    private DbsGroup pnd_P_Vars_Ph_Last_NmeRedef2;
    private DbsField pnd_P_Vars_Pnd_Ph_Last_Nme_A16;
    private DbsField pnd_P_Vars_Ph_First_Nme;
    private DbsGroup pnd_P_Vars_Ph_First_NmeRedef3;
    private DbsField pnd_P_Vars_Pnd_Ph_First_Nme_A10;
    private DbsField pnd_P_Vars_Ph_Mddle_Nme;
    private DbsGroup pnd_P_Vars_Ph_Mddle_NmeRedef4;
    private DbsField pnd_P_Vars_Pnd_Ph_Mddle_Nme_A12;
    private DbsGroup pnd_P_Vars_Ph_Mddle_NmeRedef5;
    private DbsField pnd_P_Vars_Pnd_Ph_Mddle_Nme_A1;
    private DbsField pnd_P_Vars_Ph_Sffx_Nme;
    private DbsField pnd_P_Vars_Pnd_Get_Pin;
    private DbsField pnd_P_Vars_Pnd_Get_Ssn_Level_Info;

    public DbsGroup getPnd_P_Vars() { return pnd_P_Vars; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr() { return pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Payee_Cde() { return pnd_P_Vars_Pnd_Cntrct_Payee_Cde; }

    public DbsGroup getPnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1() { return pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Payee_Cde_N() { return pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N; }

    public DbsField getPnd_P_Vars_Pnd_Pin() { return pnd_P_Vars_Pnd_Pin; }

    public DbsField getPnd_P_Vars_Pnd_Ph_Data_Rcd_Type() { return pnd_P_Vars_Pnd_Ph_Data_Rcd_Type; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type() { return pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type; }

    public DbsField getPnd_P_Vars_Pnd_Ssn() { return pnd_P_Vars_Pnd_Ssn; }

    public DbsField getPnd_P_Vars_Pnd_Dob_Ccyymmdd() { return pnd_P_Vars_Pnd_Dob_Ccyymmdd; }

    public DbsField getPnd_P_Vars_Pnd_Dod_Ccyymmdd() { return pnd_P_Vars_Pnd_Dod_Ccyymmdd; }

    public DbsGroup getPnd_P_Vars_Ph_Nme() { return pnd_P_Vars_Ph_Nme; }

    public DbsField getPnd_P_Vars_Ph_Prfx_Nme() { return pnd_P_Vars_Ph_Prfx_Nme; }

    public DbsField getPnd_P_Vars_Ph_Last_Nme() { return pnd_P_Vars_Ph_Last_Nme; }

    public DbsGroup getPnd_P_Vars_Ph_Last_NmeRedef2() { return pnd_P_Vars_Ph_Last_NmeRedef2; }

    public DbsField getPnd_P_Vars_Pnd_Ph_Last_Nme_A16() { return pnd_P_Vars_Pnd_Ph_Last_Nme_A16; }

    public DbsField getPnd_P_Vars_Ph_First_Nme() { return pnd_P_Vars_Ph_First_Nme; }

    public DbsGroup getPnd_P_Vars_Ph_First_NmeRedef3() { return pnd_P_Vars_Ph_First_NmeRedef3; }

    public DbsField getPnd_P_Vars_Pnd_Ph_First_Nme_A10() { return pnd_P_Vars_Pnd_Ph_First_Nme_A10; }

    public DbsField getPnd_P_Vars_Ph_Mddle_Nme() { return pnd_P_Vars_Ph_Mddle_Nme; }

    public DbsGroup getPnd_P_Vars_Ph_Mddle_NmeRedef4() { return pnd_P_Vars_Ph_Mddle_NmeRedef4; }

    public DbsField getPnd_P_Vars_Pnd_Ph_Mddle_Nme_A12() { return pnd_P_Vars_Pnd_Ph_Mddle_Nme_A12; }

    public DbsGroup getPnd_P_Vars_Ph_Mddle_NmeRedef5() { return pnd_P_Vars_Ph_Mddle_NmeRedef5; }

    public DbsField getPnd_P_Vars_Pnd_Ph_Mddle_Nme_A1() { return pnd_P_Vars_Pnd_Ph_Mddle_Nme_A1; }

    public DbsField getPnd_P_Vars_Ph_Sffx_Nme() { return pnd_P_Vars_Ph_Sffx_Nme; }

    public DbsField getPnd_P_Vars_Pnd_Get_Pin() { return pnd_P_Vars_Pnd_Get_Pin; }

    public DbsField getPnd_P_Vars_Pnd_Get_Ssn_Level_Info() { return pnd_P_Vars_Pnd_Get_Ssn_Level_Info; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_P_Vars = dbsRecord.newGroupInRecord("pnd_P_Vars", "#P-VARS");
        pnd_P_Vars.setParameterOption(ParameterOption.ByReference);
        pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_P_Vars_Pnd_Cntrct_Payee_Cde = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1 = pnd_P_Vars.newGroupInGroup("pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1", "Redefines", pnd_P_Vars_Pnd_Cntrct_Payee_Cde);
        pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N = pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N", "#CNTRCT-PAYEE-CDE-N", 
            FieldType.NUMERIC, 2);
        pnd_P_Vars_Pnd_Pin = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_P_Vars_Pnd_Ph_Data_Rcd_Type = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Ph_Data_Rcd_Type", "#PH-DATA-RCD-TYPE", FieldType.NUMERIC, 2);
        pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type", "#CNTRCT-DATA-RCD-TYPE", FieldType.NUMERIC, 
            2);
        pnd_P_Vars_Pnd_Ssn = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 9);
        pnd_P_Vars_Pnd_Dob_Ccyymmdd = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Dob_Ccyymmdd", "#DOB-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_P_Vars_Pnd_Dod_Ccyymmdd = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Dod_Ccyymmdd", "#DOD-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_P_Vars_Ph_Nme = pnd_P_Vars.newGroupInGroup("pnd_P_Vars_Ph_Nme", "PH-NME");
        pnd_P_Vars_Ph_Prfx_Nme = pnd_P_Vars_Ph_Nme.newFieldInGroup("pnd_P_Vars_Ph_Prfx_Nme", "PH-PRFX-NME", FieldType.STRING, 8);
        pnd_P_Vars_Ph_Last_Nme = pnd_P_Vars_Ph_Nme.newFieldInGroup("pnd_P_Vars_Ph_Last_Nme", "PH-LAST-NME", FieldType.STRING, 30);
        pnd_P_Vars_Ph_Last_NmeRedef2 = pnd_P_Vars_Ph_Nme.newGroupInGroup("pnd_P_Vars_Ph_Last_NmeRedef2", "Redefines", pnd_P_Vars_Ph_Last_Nme);
        pnd_P_Vars_Pnd_Ph_Last_Nme_A16 = pnd_P_Vars_Ph_Last_NmeRedef2.newFieldInGroup("pnd_P_Vars_Pnd_Ph_Last_Nme_A16", "#PH-LAST-NME-A16", FieldType.STRING, 
            16);
        pnd_P_Vars_Ph_First_Nme = pnd_P_Vars_Ph_Nme.newFieldInGroup("pnd_P_Vars_Ph_First_Nme", "PH-FIRST-NME", FieldType.STRING, 30);
        pnd_P_Vars_Ph_First_NmeRedef3 = pnd_P_Vars_Ph_Nme.newGroupInGroup("pnd_P_Vars_Ph_First_NmeRedef3", "Redefines", pnd_P_Vars_Ph_First_Nme);
        pnd_P_Vars_Pnd_Ph_First_Nme_A10 = pnd_P_Vars_Ph_First_NmeRedef3.newFieldInGroup("pnd_P_Vars_Pnd_Ph_First_Nme_A10", "#PH-FIRST-NME-A10", FieldType.STRING, 
            10);
        pnd_P_Vars_Ph_Mddle_Nme = pnd_P_Vars_Ph_Nme.newFieldInGroup("pnd_P_Vars_Ph_Mddle_Nme", "PH-MDDLE-NME", FieldType.STRING, 30);
        pnd_P_Vars_Ph_Mddle_NmeRedef4 = pnd_P_Vars_Ph_Nme.newGroupInGroup("pnd_P_Vars_Ph_Mddle_NmeRedef4", "Redefines", pnd_P_Vars_Ph_Mddle_Nme);
        pnd_P_Vars_Pnd_Ph_Mddle_Nme_A12 = pnd_P_Vars_Ph_Mddle_NmeRedef4.newFieldInGroup("pnd_P_Vars_Pnd_Ph_Mddle_Nme_A12", "#PH-MDDLE-NME-A12", FieldType.STRING, 
            12);
        pnd_P_Vars_Ph_Mddle_NmeRedef5 = pnd_P_Vars_Ph_Nme.newGroupInGroup("pnd_P_Vars_Ph_Mddle_NmeRedef5", "Redefines", pnd_P_Vars_Ph_Mddle_Nme);
        pnd_P_Vars_Pnd_Ph_Mddle_Nme_A1 = pnd_P_Vars_Ph_Mddle_NmeRedef5.newFieldInGroup("pnd_P_Vars_Pnd_Ph_Mddle_Nme_A1", "#PH-MDDLE-NME-A1", FieldType.STRING, 
            1);
        pnd_P_Vars_Ph_Sffx_Nme = pnd_P_Vars_Ph_Nme.newFieldInGroup("pnd_P_Vars_Ph_Sffx_Nme", "PH-SFFX-NME", FieldType.STRING, 8);
        pnd_P_Vars_Pnd_Get_Pin = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Get_Pin", "#GET-PIN", FieldType.BOOLEAN);
        pnd_P_Vars_Pnd_Get_Ssn_Level_Info = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Get_Ssn_Level_Info", "#GET-SSN-LEVEL-INFO", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa105(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

