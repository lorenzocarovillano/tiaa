/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:10 PM
**        * FROM NATURAL LDA     : IAAL999F
************************************************************
**        * FILE NAME            : LdaIaal999f.java
**        * CLASS NAME           : LdaIaal999f
**        * INSTANCE NAME        : LdaIaal999f
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal999f extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tckr_Symbl;
    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde;
    private DbsGroup iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_CdeRedef2;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp;
    private DbsGroup iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Tckr_Symbl;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Mode_Ind;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef3;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Aftr_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tckr_Symbl;
    private DataAccessProgramView vw_iaa_Cref_Fund_Trans;
    private DbsField iaa_Cref_Fund_Trans_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde;
    private DbsGroup iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_CdeRedef4;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Trans_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp;
    private DbsGroup iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Cref_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte;
    private DbsField iaa_Cref_Fund_Trans_Tckr_Symbl;
    private DbsField iaa_Cref_Fund_Trans_Cref_Mode_Ind;
    private DbsField iaa_Cref_Fund_Trans_Cref_Old_Cmpny_Fund;
    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Plan_Nmbr;
    private DbsField iaa_Cntrct_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Cntrct_Nmbr;
    private DbsGroup iaa_Cntrct_Lgcy_Plan_Sub_Plan;
    private DbsField iaa_Cntrct_Lgcy_Plan_Nbr;
    private DbsField iaa_Cntrct_Lgcy_Sub_Plan_Nbr;
    private DbsField iaa_Cntrct_Lgcy_Orig_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Lgcy_Orig_Sub_Plan;
    private DbsField iaa_Cntrct_Lgcy_Pct;
    private DbsField iaa_Cntrct_Lgcy_Settlmnt_Amt;
    private DbsField iaa_Cntrct_For_Future_Use;
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Trans_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr;
    private DbsGroup iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan;
    private DbsField iaa_Cntrct_Trans_Lgcy_Plan_Nbr;
    private DbsField iaa_Cntrct_Trans_Lgcy_Sub_Plan_Nbr;
    private DbsField iaa_Cntrct_Trans_Lgcy_Orig_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_Lgcy_Orig_Sub_Plan;
    private DbsField iaa_Cntrct_Trans_Lgcy_Pct;
    private DbsField iaa_Cntrct_Trans_Lgcy_Settlmnt_Amt;
    private DbsField iaa_Cntrct_Trans_For_Future_Use;
    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Lst_Trans_Dte;
    private DbsField cpr_Prtcpnt_Ctznshp_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Sw;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Prtcpnt_Tax_Id_Typ;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Trmnte_Rsn;
    private DbsField cpr_Cntrct_Rwrttn_Ind;
    private DbsField cpr_Cntrct_Cash_Cde;
    private DbsField cpr_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsField cpr_Count_Castcntrct_Company_Data;
    private DbsGroup cpr_Cntrct_Company_Data;
    private DbsField cpr_Cntrct_Company_Cd;
    private DbsField cpr_Cntrct_Rcvry_Type_Ind;
    private DbsField cpr_Cntrct_Per_Ivc_Amt;
    private DbsField cpr_Cntrct_Resdl_Ivc_Amt;
    private DbsField cpr_Cntrct_Ivc_Amt;
    private DbsField cpr_Cntrct_Ivc_Used_Amt;
    private DbsField cpr_Cntrct_Rtb_Amt;
    private DbsField cpr_Cntrct_Rtb_Percent;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Wthdrwl_Dte;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Final_Pay_Dte;
    private DbsField cpr_Bnfcry_Xref_Ind;
    private DbsField cpr_Bnfcry_Dod_Dte;
    private DbsField cpr_Cntrct_Pend_Cde;
    private DbsField cpr_Cntrct_Hold_Cde;
    private DbsField cpr_Cntrct_Pend_Dte;
    private DbsField cpr_Cntrct_Prev_Dist_Cde;
    private DbsField cpr_Cntrct_Curr_Dist_Cde;
    private DbsField cpr_Cntrct_Cmbne_Cde;
    private DbsField cpr_Cntrct_Spirt_Cde;
    private DbsField cpr_Cntrct_Spirt_Amt;
    private DbsField cpr_Cntrct_Spirt_Srce;
    private DbsField cpr_Cntrct_Spirt_Arr_Dte;
    private DbsField cpr_Cntrct_Spirt_Prcss_Dte;
    private DbsField cpr_Cntrct_Fed_Tax_Amt;
    private DbsField cpr_Cntrct_State_Cde;
    private DbsField cpr_Cntrct_State_Tax_Amt;
    private DbsField cpr_Cntrct_Local_Cde;
    private DbsField cpr_Cntrct_Local_Tax_Amt;
    private DbsField cpr_Cntrct_Lst_Chnge_Dte;
    private DbsField cpr_Cpr_Xfr_Term_Cde;
    private DbsField cpr_Cpr_Lgl_Res_Cde;
    private DbsField cpr_Cpr_Xfr_Iss_Dte;
    private DbsField cpr_Rllvr_Cntrct_Nbr;
    private DbsField cpr_Rllvr_Ivc_Ind;
    private DbsField cpr_Rllvr_Elgble_Ind;
    private DbsGroup cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField cpr_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField cpr_Rllvr_Accptng_Irc_Cde;
    private DbsField cpr_Rllvr_Pln_Admn_Ind;
    private DbsField cpr_Roth_Dsblty_Dte;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsField iaa_Cpr_Trans_Count_Castcntrct_Company_Data;
    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cpr_Trans_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Cpr_Trans_Rllvr_Ivc_Ind;
    private DbsField iaa_Cpr_Trans_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind;
    private DbsField iaa_Cpr_Trans_Roth_Dsblty_Dte;

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd() { return vw_iaa_Tiaa_Fund_Rcrd; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tckr_Symbl() { return iaa_Tiaa_Fund_Rcrd_Tckr_Symbl; }

    public DataAccessProgramView getVw_iaa_Cref_Fund_Rcrd() { return vw_iaa_Cref_Fund_Rcrd; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr() { return iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde() { return iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde() { return iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_CdeRedef2() { return iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_CdeRedef2; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde() { return iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Fund_Cde() { return iaa_Cref_Fund_Rcrd_Cref_Fund_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt() { return iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Unit_Val() { return iaa_Cref_Fund_Rcrd_Cref_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt() { return iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val() { return iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde() { return iaa_Cref_Fund_Rcrd_Cref_Rate_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Rate_Dte() { return iaa_Cref_Fund_Rcrd_Cref_Rate_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Units_Cnt() { return iaa_Cref_Fund_Rcrd_Cref_Units_Cnt; }

    public DbsField getIaa_Cref_Fund_Rcrd_Lst_Trans_Dte() { return iaa_Cref_Fund_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte() { return iaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte() { return iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte() { return iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_Tckr_Symbl() { return iaa_Cref_Fund_Rcrd_Tckr_Symbl; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Mode_Ind() { return iaa_Cref_Fund_Rcrd_Cref_Mode_Ind; }

    public DbsField getIaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund() { return iaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Trans() { return vw_iaa_Tiaa_Fund_Trans; }

    public DbsField getIaa_Tiaa_Fund_Trans_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef3() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef3; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Trans_Trans_Check_Dte() { return iaa_Tiaa_Fund_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id() { return iaa_Tiaa_Fund_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id() { return iaa_Tiaa_Fund_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tckr_Symbl() { return iaa_Tiaa_Fund_Trans_Tckr_Symbl; }

    public DataAccessProgramView getVw_iaa_Cref_Fund_Trans() { return vw_iaa_Cref_Fund_Trans; }

    public DbsField getIaa_Cref_Fund_Trans_Trans_Dte() { return iaa_Cref_Fund_Trans_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Invrse_Trans_Dte() { return iaa_Cref_Fund_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Lst_Trans_Dte() { return iaa_Cref_Fund_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr() { return iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde() { return iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde() { return iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Cref_Fund_Trans_Cref_Cmpny_Fund_CdeRedef4() { return iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_CdeRedef4; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Cmpny_Cde() { return iaa_Cref_Fund_Trans_Cref_Cmpny_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Fund_Cde() { return iaa_Cref_Fund_Trans_Cref_Fund_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Tot_Per_Amt() { return iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Unit_Val() { return iaa_Cref_Fund_Trans_Cref_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp() { return iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getIaa_Cref_Fund_Trans_Cref_Rate_Data_Grp() { return iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Rate_Cde() { return iaa_Cref_Fund_Trans_Cref_Rate_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Rate_Dte() { return iaa_Cref_Fund_Trans_Cref_Rate_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Units_Cnt() { return iaa_Cref_Fund_Trans_Cref_Units_Cnt; }

    public DbsField getIaa_Cref_Fund_Trans_Trans_Check_Dte() { return iaa_Cref_Fund_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Bfre_Imge_Id() { return iaa_Cref_Fund_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cref_Fund_Trans_Aftr_Imge_Id() { return iaa_Cref_Fund_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Xfr_Iss_Dte() { return iaa_Cref_Fund_Trans_Cref_Xfr_Iss_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Lst_Xfr_In_Dte() { return iaa_Cref_Fund_Trans_Cref_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte() { return iaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_Tckr_Symbl() { return iaa_Cref_Fund_Trans_Tckr_Symbl; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Mode_Ind() { return iaa_Cref_Fund_Trans_Cref_Mode_Ind; }

    public DbsField getIaa_Cref_Fund_Trans_Cref_Old_Cmpny_Fund() { return iaa_Cref_Fund_Trans_Cref_Old_Cmpny_Fund; }

    public DataAccessProgramView getVw_iaa_Trans_Rcrd() { return vw_iaa_Trans_Rcrd; }

    public DbsField getIaa_Trans_Rcrd_Trans_Dte() { return iaa_Trans_Rcrd_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Invrse_Trans_Dte() { return iaa_Trans_Rcrd_Invrse_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Lst_Trans_Dte() { return iaa_Trans_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Ppcn_Nbr() { return iaa_Trans_Rcrd_Trans_Ppcn_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Payee_Cde() { return iaa_Trans_Rcrd_Trans_Payee_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Sub_Cde() { return iaa_Trans_Rcrd_Trans_Sub_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cde() { return iaa_Trans_Rcrd_Trans_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Actvty_Cde() { return iaa_Trans_Rcrd_Trans_Actvty_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Check_Dte() { return iaa_Trans_Rcrd_Trans_Check_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Todays_Dte() { return iaa_Trans_Rcrd_Trans_Todays_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Area() { return iaa_Trans_Rcrd_Trans_User_Area; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Id() { return iaa_Trans_Rcrd_Trans_User_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Cde() { return iaa_Trans_Rcrd_Trans_Verify_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Id() { return iaa_Trans_Rcrd_Trans_Verify_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cmbne_Cde() { return iaa_Trans_Rcrd_Trans_Cmbne_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Wpid() { return iaa_Trans_Rcrd_Trans_Cwf_Wpid; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Id_Nbr() { return iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Effective_Dte() { return iaa_Trans_Rcrd_Trans_Effective_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Acctng_Cde() { return iaa_Cntrct_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte() { return iaa_Cntrct_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Type_Cde() { return iaa_Cntrct_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Lst_Trans_Dte() { return iaa_Cntrct_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Type() { return iaa_Cntrct_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Roth_Ssnng_Dte() { return iaa_Cntrct_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Plan_Nmbr() { return iaa_Cntrct_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Tax_Exmpt_Ind() { return iaa_Cntrct_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dob() { return iaa_Cntrct_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dod() { return iaa_Cntrct_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Sub_Plan_Nmbr() { return iaa_Cntrct_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Orgntng_Cntrct_Nmbr; }

    public DbsGroup getIaa_Cntrct_Lgcy_Plan_Sub_Plan() { return iaa_Cntrct_Lgcy_Plan_Sub_Plan; }

    public DbsField getIaa_Cntrct_Lgcy_Plan_Nbr() { return iaa_Cntrct_Lgcy_Plan_Nbr; }

    public DbsField getIaa_Cntrct_Lgcy_Sub_Plan_Nbr() { return iaa_Cntrct_Lgcy_Sub_Plan_Nbr; }

    public DbsField getIaa_Cntrct_Lgcy_Orig_Cntrct_Nbr() { return iaa_Cntrct_Lgcy_Orig_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Lgcy_Orig_Sub_Plan() { return iaa_Cntrct_Lgcy_Orig_Sub_Plan; }

    public DbsField getIaa_Cntrct_Lgcy_Pct() { return iaa_Cntrct_Lgcy_Pct; }

    public DbsField getIaa_Cntrct_Lgcy_Settlmnt_Amt() { return iaa_Cntrct_Lgcy_Settlmnt_Amt; }

    public DbsField getIaa_Cntrct_For_Future_Use() { return iaa_Cntrct_For_Future_Use; }

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Trans_Dte() { return iaa_Cntrct_Trans_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Invrse_Trans_Dte() { return iaa_Cntrct_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Lst_Trans_Dte() { return iaa_Cntrct_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Optn_Cde() { return iaa_Cntrct_Trans_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orgn_Cde() { return iaa_Cntrct_Trans_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Acctng_Cde() { return iaa_Cntrct_Trans_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Trans_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type_Cde() { return iaa_Cntrct_Trans_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Trans_Trans_Check_Dte() { return iaa_Cntrct_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cntrct_Trans_Bfre_Imge_Id() { return iaa_Cntrct_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Aftr_Imge_Id() { return iaa_Cntrct_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type() { return iaa_Cntrct_Trans_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Trans_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Ssnng_Dte() { return iaa_Cntrct_Trans_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Plan_Nmbr() { return iaa_Cntrct_Trans_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Tax_Exmpt_Ind() { return iaa_Cntrct_Trans_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dob() { return iaa_Cntrct_Trans_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dod() { return iaa_Cntrct_Trans_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Trans_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr; }

    public DbsGroup getIaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan() { return iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan; }

    public DbsField getIaa_Cntrct_Trans_Lgcy_Plan_Nbr() { return iaa_Cntrct_Trans_Lgcy_Plan_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Lgcy_Sub_Plan_Nbr() { return iaa_Cntrct_Trans_Lgcy_Sub_Plan_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Lgcy_Orig_Cntrct_Nbr() { return iaa_Cntrct_Trans_Lgcy_Orig_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Lgcy_Orig_Sub_Plan() { return iaa_Cntrct_Trans_Lgcy_Orig_Sub_Plan; }

    public DbsField getIaa_Cntrct_Trans_Lgcy_Pct() { return iaa_Cntrct_Trans_Lgcy_Pct; }

    public DbsField getIaa_Cntrct_Trans_Lgcy_Settlmnt_Amt() { return iaa_Cntrct_Trans_Lgcy_Settlmnt_Amt; }

    public DbsField getIaa_Cntrct_Trans_For_Future_Use() { return iaa_Cntrct_Trans_For_Future_Use; }

    public DataAccessProgramView getVw_cpr() { return vw_cpr; }

    public DbsField getCpr_Cntrct_Part_Ppcn_Nbr() { return cpr_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getCpr_Cntrct_Part_Payee_Cde() { return cpr_Cntrct_Part_Payee_Cde; }

    public DbsField getCpr_Cpr_Id_Nbr() { return cpr_Cpr_Id_Nbr; }

    public DbsField getCpr_Lst_Trans_Dte() { return cpr_Lst_Trans_Dte; }

    public DbsField getCpr_Prtcpnt_Ctznshp_Cde() { return cpr_Prtcpnt_Ctznshp_Cde; }

    public DbsField getCpr_Prtcpnt_Rsdncy_Cde() { return cpr_Prtcpnt_Rsdncy_Cde; }

    public DbsField getCpr_Prtcpnt_Rsdncy_Sw() { return cpr_Prtcpnt_Rsdncy_Sw; }

    public DbsField getCpr_Prtcpnt_Tax_Id_Nbr() { return cpr_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getCpr_Prtcpnt_Tax_Id_Typ() { return cpr_Prtcpnt_Tax_Id_Typ; }

    public DbsField getCpr_Cntrct_Actvty_Cde() { return cpr_Cntrct_Actvty_Cde; }

    public DbsField getCpr_Cntrct_Trmnte_Rsn() { return cpr_Cntrct_Trmnte_Rsn; }

    public DbsField getCpr_Cntrct_Rwrttn_Ind() { return cpr_Cntrct_Rwrttn_Ind; }

    public DbsField getCpr_Cntrct_Cash_Cde() { return cpr_Cntrct_Cash_Cde; }

    public DbsField getCpr_Cntrct_Emplymnt_Trmnt_Cde() { return cpr_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsField getCpr_Count_Castcntrct_Company_Data() { return cpr_Count_Castcntrct_Company_Data; }

    public DbsGroup getCpr_Cntrct_Company_Data() { return cpr_Cntrct_Company_Data; }

    public DbsField getCpr_Cntrct_Company_Cd() { return cpr_Cntrct_Company_Cd; }

    public DbsField getCpr_Cntrct_Rcvry_Type_Ind() { return cpr_Cntrct_Rcvry_Type_Ind; }

    public DbsField getCpr_Cntrct_Per_Ivc_Amt() { return cpr_Cntrct_Per_Ivc_Amt; }

    public DbsField getCpr_Cntrct_Resdl_Ivc_Amt() { return cpr_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getCpr_Cntrct_Ivc_Amt() { return cpr_Cntrct_Ivc_Amt; }

    public DbsField getCpr_Cntrct_Ivc_Used_Amt() { return cpr_Cntrct_Ivc_Used_Amt; }

    public DbsField getCpr_Cntrct_Rtb_Amt() { return cpr_Cntrct_Rtb_Amt; }

    public DbsField getCpr_Cntrct_Rtb_Percent() { return cpr_Cntrct_Rtb_Percent; }

    public DbsField getCpr_Cntrct_Mode_Ind() { return cpr_Cntrct_Mode_Ind; }

    public DbsField getCpr_Cntrct_Wthdrwl_Dte() { return cpr_Cntrct_Wthdrwl_Dte; }

    public DbsField getCpr_Cntrct_Final_Per_Pay_Dte() { return cpr_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getCpr_Cntrct_Final_Pay_Dte() { return cpr_Cntrct_Final_Pay_Dte; }

    public DbsField getCpr_Bnfcry_Xref_Ind() { return cpr_Bnfcry_Xref_Ind; }

    public DbsField getCpr_Bnfcry_Dod_Dte() { return cpr_Bnfcry_Dod_Dte; }

    public DbsField getCpr_Cntrct_Pend_Cde() { return cpr_Cntrct_Pend_Cde; }

    public DbsField getCpr_Cntrct_Hold_Cde() { return cpr_Cntrct_Hold_Cde; }

    public DbsField getCpr_Cntrct_Pend_Dte() { return cpr_Cntrct_Pend_Dte; }

    public DbsField getCpr_Cntrct_Prev_Dist_Cde() { return cpr_Cntrct_Prev_Dist_Cde; }

    public DbsField getCpr_Cntrct_Curr_Dist_Cde() { return cpr_Cntrct_Curr_Dist_Cde; }

    public DbsField getCpr_Cntrct_Cmbne_Cde() { return cpr_Cntrct_Cmbne_Cde; }

    public DbsField getCpr_Cntrct_Spirt_Cde() { return cpr_Cntrct_Spirt_Cde; }

    public DbsField getCpr_Cntrct_Spirt_Amt() { return cpr_Cntrct_Spirt_Amt; }

    public DbsField getCpr_Cntrct_Spirt_Srce() { return cpr_Cntrct_Spirt_Srce; }

    public DbsField getCpr_Cntrct_Spirt_Arr_Dte() { return cpr_Cntrct_Spirt_Arr_Dte; }

    public DbsField getCpr_Cntrct_Spirt_Prcss_Dte() { return cpr_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getCpr_Cntrct_Fed_Tax_Amt() { return cpr_Cntrct_Fed_Tax_Amt; }

    public DbsField getCpr_Cntrct_State_Cde() { return cpr_Cntrct_State_Cde; }

    public DbsField getCpr_Cntrct_State_Tax_Amt() { return cpr_Cntrct_State_Tax_Amt; }

    public DbsField getCpr_Cntrct_Local_Cde() { return cpr_Cntrct_Local_Cde; }

    public DbsField getCpr_Cntrct_Local_Tax_Amt() { return cpr_Cntrct_Local_Tax_Amt; }

    public DbsField getCpr_Cntrct_Lst_Chnge_Dte() { return cpr_Cntrct_Lst_Chnge_Dte; }

    public DbsField getCpr_Cpr_Xfr_Term_Cde() { return cpr_Cpr_Xfr_Term_Cde; }

    public DbsField getCpr_Cpr_Lgl_Res_Cde() { return cpr_Cpr_Lgl_Res_Cde; }

    public DbsField getCpr_Cpr_Xfr_Iss_Dte() { return cpr_Cpr_Xfr_Iss_Dte; }

    public DbsField getCpr_Rllvr_Cntrct_Nbr() { return cpr_Rllvr_Cntrct_Nbr; }

    public DbsField getCpr_Rllvr_Ivc_Ind() { return cpr_Rllvr_Ivc_Ind; }

    public DbsField getCpr_Rllvr_Elgble_Ind() { return cpr_Rllvr_Elgble_Ind; }

    public DbsGroup getCpr_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getCpr_Rllvr_Dstrbtng_Irc_Cde() { return cpr_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getCpr_Rllvr_Accptng_Irc_Cde() { return cpr_Rllvr_Accptng_Irc_Cde; }

    public DbsField getCpr_Rllvr_Pln_Admn_Ind() { return cpr_Rllvr_Pln_Admn_Ind; }

    public DbsField getCpr_Roth_Dsblty_Dte() { return cpr_Roth_Dsblty_Dte; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Trans_Dte() { return iaa_Cpr_Trans_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Invrse_Trans_Dte() { return iaa_Cpr_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Lst_Trans_Dte() { return iaa_Cpr_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_Cpr_Id_Nbr() { return iaa_Cpr_Trans_Cpr_Id_Nbr; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde() { return iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde() { return iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw() { return iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr() { return iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ() { return iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Actvty_Cde() { return iaa_Cpr_Trans_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Trmnte_Rsn() { return iaa_Cpr_Trans_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rwrttn_Ind() { return iaa_Cpr_Trans_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Cash_Cde() { return iaa_Cpr_Trans_Cntrct_Cash_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsField getIaa_Cpr_Trans_Count_Castcntrct_Company_Data() { return iaa_Cpr_Trans_Count_Castcntrct_Company_Data; }

    public DbsGroup getIaa_Cpr_Trans_Cntrct_Company_Data() { return iaa_Cpr_Trans_Cntrct_Company_Data; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Company_Cd() { return iaa_Cpr_Trans_Cntrct_Company_Cd; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind() { return iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Per_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Ivc_Used_Amt() { return iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rtb_Amt() { return iaa_Cpr_Trans_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rtb_Percent() { return iaa_Cpr_Trans_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Mode_Ind() { return iaa_Cpr_Trans_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Wthdrwl_Dte() { return iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Dod_Dte() { return iaa_Cpr_Trans_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Cde() { return iaa_Cpr_Trans_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Hold_Cde() { return iaa_Cpr_Trans_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte() { return iaa_Cpr_Trans_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Prev_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Curr_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Cmbne_Cde() { return iaa_Cpr_Trans_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Cde() { return iaa_Cpr_Trans_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Amt() { return iaa_Cpr_Trans_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Srce() { return iaa_Cpr_Trans_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte() { return iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte() { return iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Fed_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_State_Cde() { return iaa_Cpr_Trans_Cntrct_State_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_State_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Local_Cde() { return iaa_Cpr_Trans_Cntrct_Local_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Local_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte() { return iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte; }

    public DbsField getIaa_Cpr_Trans_Trans_Check_Dte() { return iaa_Cpr_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cpr_Trans_Bfre_Imge_Id() { return iaa_Cpr_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Aftr_Imge_Id() { return iaa_Cpr_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Cpr_Xfr_Term_Cde() { return iaa_Cpr_Trans_Cpr_Xfr_Term_Cde; }

    public DbsField getIaa_Cpr_Trans_Cpr_Lgl_Res_Cde() { return iaa_Cpr_Trans_Cpr_Lgl_Res_Cde; }

    public DbsField getIaa_Cpr_Trans_Cpr_Xfr_Iss_Dte() { return iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Cntrct_Nbr() { return iaa_Cpr_Trans_Rllvr_Cntrct_Nbr; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Ivc_Ind() { return iaa_Cpr_Trans_Rllvr_Ivc_Ind; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Elgble_Ind() { return iaa_Cpr_Trans_Rllvr_Elgble_Ind; }

    public DbsGroup getIaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde() { return iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde() { return iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Pln_Admn_Ind() { return iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind; }

    public DbsField getIaa_Cpr_Trans_Roth_Dsblty_Dte() { return iaa_Cpr_Trans_Roth_Dsblty_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_Tckr_Symbl = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");

        vw_iaa_Cref_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd", "IAA-CREF-FUND-RCRD"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_CdeRedef2 = vw_iaa_Cref_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_CdeRedef2", 
            "Redefines", iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Rcrd_Cref_Unit_Val = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Unit_Val", "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt", "CREF-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val", "CREF-OLD-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_Cref_Rate_Cde", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Rate_Dte = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_Cref_Rate_Dte", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_Cref_Units_Cnt", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte", "CREF-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte", "CREF-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte", "CREF-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Cref_Fund_Rcrd_Tckr_Symbl = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        iaa_Cref_Fund_Rcrd_Cref_Mode_Ind = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Mode_Ind", "CREF-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund", "CREF-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef3 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef3", 
            "Redefines", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef3.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef3.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Trans_Trans_Check_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Trans_Tckr_Symbl = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");

        vw_iaa_Cref_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Trans", "IAA-CREF-FUND-TRANS"), "IAA_CREF_FUND_TRANS_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_TRANS_1"));
        iaa_Cref_Fund_Trans_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cref_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cref_Fund_Trans_Lst_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_CdeRedef4 = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_CdeRedef4", 
            "Redefines", iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Trans_Cref_Cmpny_Cde = iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_CdeRedef4.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Cref_Fund_Trans_Cref_Fund_Cde = iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_CdeRedef4.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Trans_Cref_Unit_Val = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Unit_Val", "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CREF_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Cde = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_Cref_Rate_Cde", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Dte = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_Cref_Rate_Dte", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Units_Cnt = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_Cref_Units_Cnt", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Trans_Check_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cref_Fund_Trans_Bfre_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cref_Fund_Trans_Aftr_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cref_Fund_Trans_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Xfr_Iss_Dte", "CREF-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Trans_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Lst_Xfr_In_Dte", "CREF-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte", "CREF-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Cref_Fund_Trans_Tckr_Symbl = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        iaa_Cref_Fund_Trans_Cref_Mode_Ind = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Mode_Ind", "CREF-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Cref_Fund_Trans_Cref_Old_Cmpny_Fund = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Old_Cmpny_Fund", "CREF-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT"));
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Roth_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_SSNNG_DTE");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Tax_Exmpt_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_EXMPT_IND");
        iaa_Cntrct_Orig_Ownr_Dob = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOB");
        iaa_Cntrct_Orig_Ownr_Dod = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOD");
        iaa_Cntrct_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        iaa_Cntrct_Lgcy_Plan_Sub_Plan = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Lgcy_Plan_Sub_Plan", "LGCY-PLAN-SUB-PLAN", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Plan_Nbr = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Plan_Nbr", "LGCY-PLAN-NBR", FieldType.STRING, 6, 
            new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_PLAN_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Sub_Plan_Nbr = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Sub_Plan_Nbr", "LGCY-SUB-PLAN-NBR", FieldType.STRING, 
            6, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_SUB_PLAN_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Orig_Cntrct_Nbr = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Orig_Cntrct_Nbr", "LGCY-ORIG-CNTRCT-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_ORIG_CNTRCT_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Orig_Sub_Plan = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Orig_Sub_Plan", "LGCY-ORIG-SUB-PLAN", FieldType.STRING, 
            6, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_ORIG_SUB_PLAN", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Pct = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Pct", "LGCY-PCT", FieldType.NUMERIC, 7, 4, new DbsArrayController(1,20) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_PCT", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Settlmnt_Amt = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Settlmnt_Amt", "LGCY-SETTLMNT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_SETTLMNT_AMT", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_For_Future_Use = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_For_Future_Use", "FOR-FUTURE-USE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "FOR_FUTURE_USE");

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_TRANS"));
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_Cntrct_Acctng_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_Trans_Check_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Cntrct_Type = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Trans_Roth_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_SSNNG_DTE");
        iaa_Cntrct_Trans_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Trans_Tax_Exmpt_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TAX_EXMPT_IND");
        iaa_Cntrct_Trans_Orig_Ownr_Dob = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOB");
        iaa_Cntrct_Trans_Orig_Ownr_Dod = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOD");
        iaa_Cntrct_Trans_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan", "LGCY-PLAN-SUB-PLAN", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Trans_Lgcy_Plan_Nbr = iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Trans_Lgcy_Plan_Nbr", "LGCY-PLAN-NBR", FieldType.STRING, 
            6, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_PLAN_NBR", "IA_TRANS_FILE_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Trans_Lgcy_Sub_Plan_Nbr = iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Trans_Lgcy_Sub_Plan_Nbr", "LGCY-SUB-PLAN-NBR", 
            FieldType.STRING, 6, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_SUB_PLAN_NBR", "IA_TRANS_FILE_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Trans_Lgcy_Orig_Cntrct_Nbr = iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Trans_Lgcy_Orig_Cntrct_Nbr", "LGCY-ORIG-CNTRCT-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_ORIG_CNTRCT_NBR", "IA_TRANS_FILE_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Trans_Lgcy_Orig_Sub_Plan = iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Trans_Lgcy_Orig_Sub_Plan", "LGCY-ORIG-SUB-PLAN", 
            FieldType.STRING, 6, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_ORIG_SUB_PLAN", "IA_TRANS_FILE_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Trans_Lgcy_Pct = iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Trans_Lgcy_Pct", "LGCY-PCT", FieldType.NUMERIC, 
            7, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_PCT", "IA_TRANS_FILE_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Trans_Lgcy_Settlmnt_Amt = iaa_Cntrct_Trans_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Trans_Lgcy_Settlmnt_Amt", "LGCY-SETTLMNT-AMT", 
            FieldType.NUMERIC, 10, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_SETTLMNT_AMT", "IA_TRANS_FILE_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Trans_For_Future_Use = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_For_Future_Use", "FOR-FUTURE-USE", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "FOR_FUTURE_USE");

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Lst_Trans_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        cpr_Prtcpnt_Ctznshp_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_CTZNSHP_CDE");
        cpr_Prtcpnt_Rsdncy_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr_Prtcpnt_Rsdncy_Sw = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_SW");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Prtcpnt_Tax_Id_Typ = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_TYP");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Trmnte_Rsn = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TRMNTE_RSN");
        cpr_Cntrct_Rwrttn_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_RWRTTN_IND");
        cpr_Cntrct_Cash_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_CASH_CDE");
        cpr_Cntrct_Emplymnt_Trmnt_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        cpr_Count_Castcntrct_Company_Data = vw_cpr.getRecord().newFieldInGroup("cpr_Count_Castcntrct_Company_Data", "C*CNTRCT-COMPANY-DATA", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Company_Data = vw_cpr.getRecord().newGroupInGroup("cpr_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Company_Cd = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Rcvry_Type_Ind = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Per_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Resdl_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Ivc_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Ivc_Used_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Rtb_Amt = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Rtb_Percent = cpr_Cntrct_Company_Data.newFieldArrayInGroup("cpr_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 
            7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Wthdrwl_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_WTHDRWL_DTE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Final_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_FINAL_PAY_DTE");
        cpr_Bnfcry_Xref_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        cpr_Bnfcry_Dod_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Cntrct_Hold_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        cpr_Cntrct_Pend_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        cpr_Cntrct_Prev_Dist_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PREV_DIST_CDE");
        cpr_Cntrct_Curr_Dist_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_CURR_DIST_CDE");
        cpr_Cntrct_Cmbne_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBNE_CDE");
        cpr_Cntrct_Spirt_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_CDE");
        cpr_Cntrct_Spirt_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_AMT");
        cpr_Cntrct_Spirt_Srce = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_SRCE");
        cpr_Cntrct_Spirt_Arr_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_SPIRT_ARR_DTE");
        cpr_Cntrct_Spirt_Prcss_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, 
            RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        cpr_Cntrct_Fed_Tax_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_FED_TAX_AMT");
        cpr_Cntrct_State_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_STATE_CDE");
        cpr_Cntrct_State_Tax_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        cpr_Cntrct_Local_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_LOCAL_CDE");
        cpr_Cntrct_Local_Tax_Amt = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        cpr_Cntrct_Lst_Chnge_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_LST_CHNGE_DTE");
        cpr_Cpr_Xfr_Term_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CPR_XFR_TERM_CDE");
        cpr_Cpr_Lgl_Res_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CPR_LGL_RES_CDE");
        cpr_Cpr_Xfr_Iss_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CPR_XFR_ISS_DTE");
        cpr_Rllvr_Cntrct_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RLLVR_CNTRCT_NBR");
        cpr_Rllvr_Ivc_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_IVC_IND");
        cpr_Rllvr_Elgble_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Elgble_Ind", "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_ELGBLE_IND");
        cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_cpr.getRecord().newGroupInGroup("cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup", "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        cpr_Rllvr_Dstrbtng_Irc_Cde = cpr_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("cpr_Rllvr_Dstrbtng_Irc_Cde", "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        cpr_Rllvr_Accptng_Irc_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Accptng_Irc_Cde", "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "RLLVR_ACCPTNG_IRC_CDE");
        cpr_Rllvr_Pln_Admn_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Pln_Admn_Ind", "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RLLVR_PLN_ADMN_IND");
        cpr_Roth_Dsblty_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_DSBLTY_DTE");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cpr_Trans_Count_Castcntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Count_Castcntrct_Company_Data", "C*CNTRCT-COMPANY-DATA", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        iaa_Cpr_Trans_Cpr_Xfr_Term_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cpr_Trans_Cpr_Lgl_Res_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cpr_Trans_Rllvr_Cntrct_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Cpr_Trans_Rllvr_Ivc_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        iaa_Cpr_Trans_Rllvr_Elgble_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Elgble_Ind", "RLLVR-ELGBLE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup", "RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde = iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde", "RLLVR-ACCPTNG-IRC-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind", "RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        iaa_Cpr_Trans_Roth_Dsblty_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");
        vw_iaa_Tiaa_Fund_Rcrd.setUniquePeList();
        vw_iaa_Cref_Fund_Rcrd.setUniquePeList();
        vw_iaa_Tiaa_Fund_Trans.setUniquePeList();
        vw_iaa_Cref_Fund_Trans.setUniquePeList();
        vw_iaa_Cntrct.setUniquePeList();
        vw_iaa_Cntrct_Trans.setUniquePeList();
        vw_cpr.setUniquePeList();
        vw_iaa_Cpr_Trans.setUniquePeList();

        this.setRecordName("LdaIaal999f");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cref_Fund_Rcrd.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Cref_Fund_Trans.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_cpr.reset();
        vw_iaa_Cpr_Trans.reset();
    }

    // Constructor
    public LdaIaal999f() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
