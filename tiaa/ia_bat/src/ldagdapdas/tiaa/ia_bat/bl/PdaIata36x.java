/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:50 PM
**        * FROM NATURAL PDA     : IATA36X
************************************************************
**        * FILE NAME            : PdaIata36x.java
**        * CLASS NAME           : PdaIata36x
**        * INSTANCE NAME        : PdaIata36x
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIata36x extends PdaBase
{
    // Properties
    private DbsGroup iata36x;
    private DbsField iata36x_Iata36x_Indx;
    private DbsField iata36x_Iata36x_Lngth;
    private DbsField iata36x_Iata36x_Unt_Cde_Nme;
    private DbsField iata36x_Iata36x_Nbr_Of_Unit_Ids;
    private DbsField iata36x_Iata36x_All_Valid_Unit_Cdes;
    private DbsGroup iata36x_Iata36x_Unit_Grp;
    private DbsField iata36x_Iata36x_Unit_Cde;
    private DbsGroup iata36x_Iata36x_Unit_CdeRedef1;
    private DbsField iata36x_Iata36x_Unit_Cde_7;
    private DbsField iata36x_Iata36x_Unit_Cde_Lst_1;
    private DbsField iata36x_Iata36x_Prntr_Id1;
    private DbsField iata36x_Iata36x_Prntr_Id2;
    private DbsField iata36x_Iata36x_Prntr_Id3;

    public DbsGroup getIata36x() { return iata36x; }

    public DbsField getIata36x_Iata36x_Indx() { return iata36x_Iata36x_Indx; }

    public DbsField getIata36x_Iata36x_Lngth() { return iata36x_Iata36x_Lngth; }

    public DbsField getIata36x_Iata36x_Unt_Cde_Nme() { return iata36x_Iata36x_Unt_Cde_Nme; }

    public DbsField getIata36x_Iata36x_Nbr_Of_Unit_Ids() { return iata36x_Iata36x_Nbr_Of_Unit_Ids; }

    public DbsField getIata36x_Iata36x_All_Valid_Unit_Cdes() { return iata36x_Iata36x_All_Valid_Unit_Cdes; }

    public DbsGroup getIata36x_Iata36x_Unit_Grp() { return iata36x_Iata36x_Unit_Grp; }

    public DbsField getIata36x_Iata36x_Unit_Cde() { return iata36x_Iata36x_Unit_Cde; }

    public DbsGroup getIata36x_Iata36x_Unit_CdeRedef1() { return iata36x_Iata36x_Unit_CdeRedef1; }

    public DbsField getIata36x_Iata36x_Unit_Cde_7() { return iata36x_Iata36x_Unit_Cde_7; }

    public DbsField getIata36x_Iata36x_Unit_Cde_Lst_1() { return iata36x_Iata36x_Unit_Cde_Lst_1; }

    public DbsField getIata36x_Iata36x_Prntr_Id1() { return iata36x_Iata36x_Prntr_Id1; }

    public DbsField getIata36x_Iata36x_Prntr_Id2() { return iata36x_Iata36x_Prntr_Id2; }

    public DbsField getIata36x_Iata36x_Prntr_Id3() { return iata36x_Iata36x_Prntr_Id3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iata36x = dbsRecord.newGroupInRecord("iata36x", "IATA36X");
        iata36x.setParameterOption(ParameterOption.ByReference);
        iata36x_Iata36x_Indx = iata36x.newFieldInGroup("iata36x_Iata36x_Indx", "IATA36X-INDX", FieldType.NUMERIC, 2);
        iata36x_Iata36x_Lngth = iata36x.newFieldInGroup("iata36x_Iata36x_Lngth", "IATA36X-LNGTH", FieldType.NUMERIC, 1);
        iata36x_Iata36x_Unt_Cde_Nme = iata36x.newFieldInGroup("iata36x_Iata36x_Unt_Cde_Nme", "IATA36X-UNT-CDE-NME", FieldType.STRING, 8);
        iata36x_Iata36x_Nbr_Of_Unit_Ids = iata36x.newFieldInGroup("iata36x_Iata36x_Nbr_Of_Unit_Ids", "IATA36X-NBR-OF-UNIT-IDS", FieldType.NUMERIC, 2);
        iata36x_Iata36x_All_Valid_Unit_Cdes = iata36x.newFieldArrayInGroup("iata36x_Iata36x_All_Valid_Unit_Cdes", "IATA36X-ALL-VALID-UNIT-CDES", FieldType.STRING, 
            8, new DbsArrayController(1,10));
        iata36x_Iata36x_Unit_Grp = iata36x.newGroupArrayInGroup("iata36x_Iata36x_Unit_Grp", "IATA36X-UNIT-GRP", new DbsArrayController(1,10));
        iata36x_Iata36x_Unit_Cde = iata36x_Iata36x_Unit_Grp.newFieldInGroup("iata36x_Iata36x_Unit_Cde", "IATA36X-UNIT-CDE", FieldType.STRING, 8);
        iata36x_Iata36x_Unit_CdeRedef1 = iata36x_Iata36x_Unit_Grp.newGroupInGroup("iata36x_Iata36x_Unit_CdeRedef1", "Redefines", iata36x_Iata36x_Unit_Cde);
        iata36x_Iata36x_Unit_Cde_7 = iata36x_Iata36x_Unit_CdeRedef1.newFieldInGroup("iata36x_Iata36x_Unit_Cde_7", "IATA36X-UNIT-CDE-7", FieldType.STRING, 
            7);
        iata36x_Iata36x_Unit_Cde_Lst_1 = iata36x_Iata36x_Unit_CdeRedef1.newFieldInGroup("iata36x_Iata36x_Unit_Cde_Lst_1", "IATA36X-UNIT-CDE-LST-1", FieldType.STRING, 
            1);
        iata36x_Iata36x_Prntr_Id1 = iata36x_Iata36x_Unit_Grp.newFieldInGroup("iata36x_Iata36x_Prntr_Id1", "IATA36X-PRNTR-ID1", FieldType.NUMERIC, 2);
        iata36x_Iata36x_Prntr_Id2 = iata36x_Iata36x_Unit_Grp.newFieldInGroup("iata36x_Iata36x_Prntr_Id2", "IATA36X-PRNTR-ID2", FieldType.NUMERIC, 2);
        iata36x_Iata36x_Prntr_Id3 = iata36x_Iata36x_Unit_Grp.newFieldInGroup("iata36x_Iata36x_Prntr_Id3", "IATA36X-PRNTR-ID3", FieldType.NUMERIC, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIata36x(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

