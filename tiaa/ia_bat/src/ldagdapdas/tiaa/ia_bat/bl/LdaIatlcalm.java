/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:57 PM
**        * FROM NATURAL LDA     : IATLCALM
************************************************************
**        * FILE NAME            : LdaIatlcalm.java
**        * CLASS NAME           : LdaIatlcalm
**        * INSTANCE NAME        : LdaIatlcalm
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIatlcalm extends DbsRecord
{
    // Properties
    private DbsGroup lama1001;
    private DbsField lama1001_Buffer_Pda;
    private DbsGroup lama1001_Buffer_PdaRedef1;
    private DbsGroup lama1001_Group_Pda;
    private DbsField lama1001_Result_Code;
    private DbsField lama1001_Iteration_Data;
    private DbsField lama1001_More_Data;
    private DbsField lama1001_Detail_Rec_Cnt;
    private DbsGroup lama1001_Hold_Calm_Controls;
    private DbsField lama1001_Hold_Account_Date;
    private DbsField lama1001_Hold_Sequence_No;
    private DbsField lama1001_Hold_Detail_Sequence_No;
    private DbsField lama1001_Hold_Total_Amt;
    private DbsField lama1001_Return_Cde;
    private DbsField lama1001_Return_Msg;
    private DbsField lama1001_Hdr_Journal_Date;
    private DbsField lama1001_Hdr_Origin_System;
    private DbsField lama1001_Hdr_Record_Flow_Status;
    private DbsField lama1001_Hdr_Record_Type;
    private DbsField lama1001_Hdr_Creation_Date_Stamp;
    private DbsField lama1001_Hdr_Creation_Time_Stamp;
    private DbsField lama1001_Hdr_Ctls_Received_Date_Stamp;
    private DbsField lama1001_Hdr_Ctls_Received_Time_Stamp;
    private DbsField lama1001_Hdr_Process_Date_Stamp;
    private DbsField lama1001_Hdr_Process_Time_Stamp;
    private DbsField lama1001_Hdr_Sent_To_Ps_Date_Stamp;
    private DbsField lama1001_Hdr_Sent_To_Ps_Time_Stamp;
    private DbsField lama1001_Hdr_Pymnt_Intrfce_Dte;
    private DbsField lama1001_Hdr_Total_Transaction_Amt;
    private DbsField lama1001_Hdr_Participation_Date;
    private DbsField lama1001_Hdr_Pin_Number;
    private DbsField lama1001_Hdr_State_Code;
    private DbsField lama1001_Hdr_Pits_Rqst_Id;
    private DbsField lama1001_Hdr_Bank_Code;
    private DbsField lama1001_Hdr_Contract_Type;
    private DbsField lama1001_Hdr_Pymnt_Reqst_Log_Dte_Time;
    private DbsField lama1001_Hdr_Pymnt_Corp_Wpid;
    private DbsField lama1001_Hdr_Pymnt_Dte;
    private DbsField lama1001_Hdr_Name;
    private DbsField lama1001_Hdr_Plan_Code;
    private DbsField lama1001_Hdr_Schedule_Date;
    private DbsField lama1001_Hdr_Mode_Of_Payment;
    private DbsField lama1001_Hdr_Social_Security_Number;
    private DbsField lama1001_Hdr_Cca_Id;
    private DbsField lama1001_Hdr_Reserve_For_Future_Use;
    private DbsField lama1001_Dtl_Transaction_Type;
    private DbsField lama1001_Dtl_Type_Record;
    private DbsField lama1001_Dtl_Occurrence_Nbr;
    private DbsField lama1001_Dtl_Transaction_Amt;
    private DbsField lama1001_Dtl_From_Tiaa_Contract;
    private DbsField lama1001_Dtl_From_Cref_Contract;
    private DbsField lama1001_Dtl_From_Fund_Ticker_Symbol;
    private DbsField lama1001_Dtl_From_Lob;
    private DbsField lama1001_Dtl_From_Sub_Lob;
    private DbsField lama1001_Dtl_Number_Of_From_Units;
    private DbsField lama1001_Dtl_To_Tiaa_Contract;
    private DbsField lama1001_Dtl_To_Cref_Contract;
    private DbsField lama1001_Dtl_To_Fund_Ticker_Symbol;
    private DbsField lama1001_Dtl_To_Lob;
    private DbsField lama1001_Dtl_To_Sub_Lob;
    private DbsField lama1001_Dtl_Number_Of_To_Units;
    private DbsField lama1001_Dtl_Rate_Basis;
    private DbsField lama1001_Dtl_Unledger_Indicator;
    private DbsField lama1001_Dtl_Reversal_Code;
    private DbsField lama1001_Dtl_Department_Id;
    private DbsField lama1001_Dtl_Allocation_Index;
    private DbsField lama1001_Dtl_Valuation_Basis;
    private DbsField lama1001_Dtl_Dps_Distribution_Type;
    private DbsField lama1001_Dtl_Dps_Summary_Or_Detail;
    private DbsField lama1001_Dtl_Dps_Id_Tag;
    private DbsField lama1001_Dtl_Ledger_Type;
    private DbsField lama1001_Dtl_Cntrct_Cancel_Rdrw_Actvty_Cd;
    private DbsField lama1001_Dtl_Settlement_Ind2;
    private DbsField lama1001_Dtl_Settlement_Ind;
    private DbsField lama1001_Dtl_Tax_Ind;
    private DbsField lama1001_Dtl_Net_Cash_Ind;
    private DbsField lama1001_Dtl_Installment_Date;
    private DbsField lama1001_Dtl_First_Year_Renewal_Indicator;
    private DbsField lama1001_Dtl_Sub_Plan_Number;
    private DbsField lama1001_Dtl_Omni_Transaction_Code;
    private DbsField lama1001_Dtl_Omni_Activity_Code;
    private DbsField lama1001_Dtl_Omni_Usage_Code;
    private DbsField lama1001_Dtl_Loan_Number;

    public DbsGroup getLama1001() { return lama1001; }

    public DbsField getLama1001_Buffer_Pda() { return lama1001_Buffer_Pda; }

    public DbsGroup getLama1001_Buffer_PdaRedef1() { return lama1001_Buffer_PdaRedef1; }

    public DbsGroup getLama1001_Group_Pda() { return lama1001_Group_Pda; }

    public DbsField getLama1001_Result_Code() { return lama1001_Result_Code; }

    public DbsField getLama1001_Iteration_Data() { return lama1001_Iteration_Data; }

    public DbsField getLama1001_More_Data() { return lama1001_More_Data; }

    public DbsField getLama1001_Detail_Rec_Cnt() { return lama1001_Detail_Rec_Cnt; }

    public DbsGroup getLama1001_Hold_Calm_Controls() { return lama1001_Hold_Calm_Controls; }

    public DbsField getLama1001_Hold_Account_Date() { return lama1001_Hold_Account_Date; }

    public DbsField getLama1001_Hold_Sequence_No() { return lama1001_Hold_Sequence_No; }

    public DbsField getLama1001_Hold_Detail_Sequence_No() { return lama1001_Hold_Detail_Sequence_No; }

    public DbsField getLama1001_Hold_Total_Amt() { return lama1001_Hold_Total_Amt; }

    public DbsField getLama1001_Return_Cde() { return lama1001_Return_Cde; }

    public DbsField getLama1001_Return_Msg() { return lama1001_Return_Msg; }

    public DbsField getLama1001_Hdr_Journal_Date() { return lama1001_Hdr_Journal_Date; }

    public DbsField getLama1001_Hdr_Origin_System() { return lama1001_Hdr_Origin_System; }

    public DbsField getLama1001_Hdr_Record_Flow_Status() { return lama1001_Hdr_Record_Flow_Status; }

    public DbsField getLama1001_Hdr_Record_Type() { return lama1001_Hdr_Record_Type; }

    public DbsField getLama1001_Hdr_Creation_Date_Stamp() { return lama1001_Hdr_Creation_Date_Stamp; }

    public DbsField getLama1001_Hdr_Creation_Time_Stamp() { return lama1001_Hdr_Creation_Time_Stamp; }

    public DbsField getLama1001_Hdr_Ctls_Received_Date_Stamp() { return lama1001_Hdr_Ctls_Received_Date_Stamp; }

    public DbsField getLama1001_Hdr_Ctls_Received_Time_Stamp() { return lama1001_Hdr_Ctls_Received_Time_Stamp; }

    public DbsField getLama1001_Hdr_Process_Date_Stamp() { return lama1001_Hdr_Process_Date_Stamp; }

    public DbsField getLama1001_Hdr_Process_Time_Stamp() { return lama1001_Hdr_Process_Time_Stamp; }

    public DbsField getLama1001_Hdr_Sent_To_Ps_Date_Stamp() { return lama1001_Hdr_Sent_To_Ps_Date_Stamp; }

    public DbsField getLama1001_Hdr_Sent_To_Ps_Time_Stamp() { return lama1001_Hdr_Sent_To_Ps_Time_Stamp; }

    public DbsField getLama1001_Hdr_Pymnt_Intrfce_Dte() { return lama1001_Hdr_Pymnt_Intrfce_Dte; }

    public DbsField getLama1001_Hdr_Total_Transaction_Amt() { return lama1001_Hdr_Total_Transaction_Amt; }

    public DbsField getLama1001_Hdr_Participation_Date() { return lama1001_Hdr_Participation_Date; }

    public DbsField getLama1001_Hdr_Pin_Number() { return lama1001_Hdr_Pin_Number; }

    public DbsField getLama1001_Hdr_State_Code() { return lama1001_Hdr_State_Code; }

    public DbsField getLama1001_Hdr_Pits_Rqst_Id() { return lama1001_Hdr_Pits_Rqst_Id; }

    public DbsField getLama1001_Hdr_Bank_Code() { return lama1001_Hdr_Bank_Code; }

    public DbsField getLama1001_Hdr_Contract_Type() { return lama1001_Hdr_Contract_Type; }

    public DbsField getLama1001_Hdr_Pymnt_Reqst_Log_Dte_Time() { return lama1001_Hdr_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getLama1001_Hdr_Pymnt_Corp_Wpid() { return lama1001_Hdr_Pymnt_Corp_Wpid; }

    public DbsField getLama1001_Hdr_Pymnt_Dte() { return lama1001_Hdr_Pymnt_Dte; }

    public DbsField getLama1001_Hdr_Name() { return lama1001_Hdr_Name; }

    public DbsField getLama1001_Hdr_Plan_Code() { return lama1001_Hdr_Plan_Code; }

    public DbsField getLama1001_Hdr_Schedule_Date() { return lama1001_Hdr_Schedule_Date; }

    public DbsField getLama1001_Hdr_Mode_Of_Payment() { return lama1001_Hdr_Mode_Of_Payment; }

    public DbsField getLama1001_Hdr_Social_Security_Number() { return lama1001_Hdr_Social_Security_Number; }

    public DbsField getLama1001_Hdr_Cca_Id() { return lama1001_Hdr_Cca_Id; }

    public DbsField getLama1001_Hdr_Reserve_For_Future_Use() { return lama1001_Hdr_Reserve_For_Future_Use; }

    public DbsField getLama1001_Dtl_Transaction_Type() { return lama1001_Dtl_Transaction_Type; }

    public DbsField getLama1001_Dtl_Type_Record() { return lama1001_Dtl_Type_Record; }

    public DbsField getLama1001_Dtl_Occurrence_Nbr() { return lama1001_Dtl_Occurrence_Nbr; }

    public DbsField getLama1001_Dtl_Transaction_Amt() { return lama1001_Dtl_Transaction_Amt; }

    public DbsField getLama1001_Dtl_From_Tiaa_Contract() { return lama1001_Dtl_From_Tiaa_Contract; }

    public DbsField getLama1001_Dtl_From_Cref_Contract() { return lama1001_Dtl_From_Cref_Contract; }

    public DbsField getLama1001_Dtl_From_Fund_Ticker_Symbol() { return lama1001_Dtl_From_Fund_Ticker_Symbol; }

    public DbsField getLama1001_Dtl_From_Lob() { return lama1001_Dtl_From_Lob; }

    public DbsField getLama1001_Dtl_From_Sub_Lob() { return lama1001_Dtl_From_Sub_Lob; }

    public DbsField getLama1001_Dtl_Number_Of_From_Units() { return lama1001_Dtl_Number_Of_From_Units; }

    public DbsField getLama1001_Dtl_To_Tiaa_Contract() { return lama1001_Dtl_To_Tiaa_Contract; }

    public DbsField getLama1001_Dtl_To_Cref_Contract() { return lama1001_Dtl_To_Cref_Contract; }

    public DbsField getLama1001_Dtl_To_Fund_Ticker_Symbol() { return lama1001_Dtl_To_Fund_Ticker_Symbol; }

    public DbsField getLama1001_Dtl_To_Lob() { return lama1001_Dtl_To_Lob; }

    public DbsField getLama1001_Dtl_To_Sub_Lob() { return lama1001_Dtl_To_Sub_Lob; }

    public DbsField getLama1001_Dtl_Number_Of_To_Units() { return lama1001_Dtl_Number_Of_To_Units; }

    public DbsField getLama1001_Dtl_Rate_Basis() { return lama1001_Dtl_Rate_Basis; }

    public DbsField getLama1001_Dtl_Unledger_Indicator() { return lama1001_Dtl_Unledger_Indicator; }

    public DbsField getLama1001_Dtl_Reversal_Code() { return lama1001_Dtl_Reversal_Code; }

    public DbsField getLama1001_Dtl_Department_Id() { return lama1001_Dtl_Department_Id; }

    public DbsField getLama1001_Dtl_Allocation_Index() { return lama1001_Dtl_Allocation_Index; }

    public DbsField getLama1001_Dtl_Valuation_Basis() { return lama1001_Dtl_Valuation_Basis; }

    public DbsField getLama1001_Dtl_Dps_Distribution_Type() { return lama1001_Dtl_Dps_Distribution_Type; }

    public DbsField getLama1001_Dtl_Dps_Summary_Or_Detail() { return lama1001_Dtl_Dps_Summary_Or_Detail; }

    public DbsField getLama1001_Dtl_Dps_Id_Tag() { return lama1001_Dtl_Dps_Id_Tag; }

    public DbsField getLama1001_Dtl_Ledger_Type() { return lama1001_Dtl_Ledger_Type; }

    public DbsField getLama1001_Dtl_Cntrct_Cancel_Rdrw_Actvty_Cd() { return lama1001_Dtl_Cntrct_Cancel_Rdrw_Actvty_Cd; }

    public DbsField getLama1001_Dtl_Settlement_Ind2() { return lama1001_Dtl_Settlement_Ind2; }

    public DbsField getLama1001_Dtl_Settlement_Ind() { return lama1001_Dtl_Settlement_Ind; }

    public DbsField getLama1001_Dtl_Tax_Ind() { return lama1001_Dtl_Tax_Ind; }

    public DbsField getLama1001_Dtl_Net_Cash_Ind() { return lama1001_Dtl_Net_Cash_Ind; }

    public DbsField getLama1001_Dtl_Installment_Date() { return lama1001_Dtl_Installment_Date; }

    public DbsField getLama1001_Dtl_First_Year_Renewal_Indicator() { return lama1001_Dtl_First_Year_Renewal_Indicator; }

    public DbsField getLama1001_Dtl_Sub_Plan_Number() { return lama1001_Dtl_Sub_Plan_Number; }

    public DbsField getLama1001_Dtl_Omni_Transaction_Code() { return lama1001_Dtl_Omni_Transaction_Code; }

    public DbsField getLama1001_Dtl_Omni_Activity_Code() { return lama1001_Dtl_Omni_Activity_Code; }

    public DbsField getLama1001_Dtl_Omni_Usage_Code() { return lama1001_Dtl_Omni_Usage_Code; }

    public DbsField getLama1001_Dtl_Loan_Number() { return lama1001_Dtl_Loan_Number; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        lama1001 = newGroupInRecord("lama1001", "LAMA1001");
        lama1001_Buffer_Pda = lama1001.newFieldArrayInGroup("lama1001_Buffer_Pda", "BUFFER-PDA", FieldType.STRING, 1, new DbsArrayController(1,28000));
        lama1001_Buffer_PdaRedef1 = lama1001.newGroupInGroup("lama1001_Buffer_PdaRedef1", "Redefines", lama1001_Buffer_Pda);
        lama1001_Group_Pda = lama1001_Buffer_PdaRedef1.newGroupInGroup("lama1001_Group_Pda", "GROUP-PDA");
        lama1001_Result_Code = lama1001_Group_Pda.newFieldInGroup("lama1001_Result_Code", "RESULT-CODE", FieldType.NUMERIC, 4);
        lama1001_Iteration_Data = lama1001_Group_Pda.newFieldInGroup("lama1001_Iteration_Data", "ITERATION-DATA", FieldType.STRING, 100);
        lama1001_More_Data = lama1001_Group_Pda.newFieldInGroup("lama1001_More_Data", "MORE-DATA", FieldType.STRING, 1);
        lama1001_Detail_Rec_Cnt = lama1001_Group_Pda.newFieldInGroup("lama1001_Detail_Rec_Cnt", "DETAIL-REC-CNT", FieldType.NUMERIC, 6);
        lama1001_Hold_Calm_Controls = lama1001_Group_Pda.newGroupInGroup("lama1001_Hold_Calm_Controls", "HOLD-CALM-CONTROLS");
        lama1001_Hold_Account_Date = lama1001_Hold_Calm_Controls.newFieldInGroup("lama1001_Hold_Account_Date", "HOLD-ACCOUNT-DATE", FieldType.NUMERIC, 
            8);
        lama1001_Hold_Sequence_No = lama1001_Hold_Calm_Controls.newFieldInGroup("lama1001_Hold_Sequence_No", "HOLD-SEQUENCE-NO", FieldType.NUMERIC, 9);
        lama1001_Hold_Detail_Sequence_No = lama1001_Hold_Calm_Controls.newFieldInGroup("lama1001_Hold_Detail_Sequence_No", "HOLD-DETAIL-SEQUENCE-NO", 
            FieldType.NUMERIC, 4);
        lama1001_Hold_Total_Amt = lama1001_Hold_Calm_Controls.newFieldInGroup("lama1001_Hold_Total_Amt", "HOLD-TOTAL-AMT", FieldType.DECIMAL, 16,2);
        lama1001_Return_Cde = lama1001_Group_Pda.newFieldInGroup("lama1001_Return_Cde", "RETURN-CDE", FieldType.STRING, 4);
        lama1001_Return_Msg = lama1001_Group_Pda.newFieldInGroup("lama1001_Return_Msg", "RETURN-MSG", FieldType.STRING, 79);
        lama1001_Hdr_Journal_Date = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Journal_Date", "HDR-JOURNAL-DATE", FieldType.NUMERIC, 8);
        lama1001_Hdr_Origin_System = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Origin_System", "HDR-ORIGIN-SYSTEM", FieldType.STRING, 5);
        lama1001_Hdr_Record_Flow_Status = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Record_Flow_Status", "HDR-RECORD-FLOW-STATUS", FieldType.STRING, 
            1);
        lama1001_Hdr_Record_Type = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Record_Type", "HDR-RECORD-TYPE", FieldType.STRING, 2);
        lama1001_Hdr_Creation_Date_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Creation_Date_Stamp", "HDR-CREATION-DATE-STAMP", FieldType.NUMERIC, 
            8);
        lama1001_Hdr_Creation_Time_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Creation_Time_Stamp", "HDR-CREATION-TIME-STAMP", FieldType.NUMERIC, 
            7);
        lama1001_Hdr_Ctls_Received_Date_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Ctls_Received_Date_Stamp", "HDR-CTLS-RECEIVED-DATE-STAMP", 
            FieldType.NUMERIC, 8);
        lama1001_Hdr_Ctls_Received_Time_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Ctls_Received_Time_Stamp", "HDR-CTLS-RECEIVED-TIME-STAMP", 
            FieldType.NUMERIC, 7);
        lama1001_Hdr_Process_Date_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Process_Date_Stamp", "HDR-PROCESS-DATE-STAMP", FieldType.NUMERIC, 
            8);
        lama1001_Hdr_Process_Time_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Process_Time_Stamp", "HDR-PROCESS-TIME-STAMP", FieldType.NUMERIC, 
            7);
        lama1001_Hdr_Sent_To_Ps_Date_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Sent_To_Ps_Date_Stamp", "HDR-SENT-TO-PS-DATE-STAMP", FieldType.NUMERIC, 
            8);
        lama1001_Hdr_Sent_To_Ps_Time_Stamp = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Sent_To_Ps_Time_Stamp", "HDR-SENT-TO-PS-TIME-STAMP", FieldType.NUMERIC, 
            7);
        lama1001_Hdr_Pymnt_Intrfce_Dte = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Pymnt_Intrfce_Dte", "HDR-PYMNT-INTRFCE-DTE", FieldType.NUMERIC, 
            8);
        lama1001_Hdr_Total_Transaction_Amt = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Total_Transaction_Amt", "HDR-TOTAL-TRANSACTION-AMT", FieldType.DECIMAL, 
            16,2);
        lama1001_Hdr_Participation_Date = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Participation_Date", "HDR-PARTICIPATION-DATE", FieldType.NUMERIC, 
            8);
        lama1001_Hdr_Pin_Number = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Pin_Number", "HDR-PIN-NUMBER", FieldType.STRING, 12);
        lama1001_Hdr_State_Code = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_State_Code", "HDR-STATE-CODE", FieldType.STRING, 2);
        lama1001_Hdr_Pits_Rqst_Id = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Pits_Rqst_Id", "HDR-PITS-RQST-ID", FieldType.STRING, 29);
        lama1001_Hdr_Bank_Code = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Bank_Code", "HDR-BANK-CODE", FieldType.STRING, 5);
        lama1001_Hdr_Contract_Type = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Contract_Type", "HDR-CONTRACT-TYPE", FieldType.STRING, 2);
        lama1001_Hdr_Pymnt_Reqst_Log_Dte_Time = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Pymnt_Reqst_Log_Dte_Time", "HDR-PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        lama1001_Hdr_Pymnt_Corp_Wpid = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Pymnt_Corp_Wpid", "HDR-PYMNT-CORP-WPID", FieldType.STRING, 6);
        lama1001_Hdr_Pymnt_Dte = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Pymnt_Dte", "HDR-PYMNT-DTE", FieldType.NUMERIC, 8);
        lama1001_Hdr_Name = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Name", "HDR-NAME", FieldType.STRING, 35);
        lama1001_Hdr_Plan_Code = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Plan_Code", "HDR-PLAN-CODE", FieldType.STRING, 6);
        lama1001_Hdr_Schedule_Date = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Schedule_Date", "HDR-SCHEDULE-DATE", FieldType.NUMERIC, 8);
        lama1001_Hdr_Mode_Of_Payment = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Mode_Of_Payment", "HDR-MODE-OF-PAYMENT", FieldType.STRING, 3);
        lama1001_Hdr_Social_Security_Number = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Social_Security_Number", "HDR-SOCIAL-SECURITY-NUMBER", 
            FieldType.NUMERIC, 9);
        lama1001_Hdr_Cca_Id = lama1001_Group_Pda.newFieldInGroup("lama1001_Hdr_Cca_Id", "HDR-CCA-ID", FieldType.STRING, 18);
        lama1001_Hdr_Reserve_For_Future_Use = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Hdr_Reserve_For_Future_Use", "HDR-RESERVE-FOR-FUTURE-USE", 
            FieldType.STRING, 1, new DbsArrayController(1,456));
        lama1001_Dtl_Transaction_Type = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Transaction_Type", "DTL-TRANSACTION-TYPE", FieldType.STRING, 
            2, new DbsArrayController(1,50));
        lama1001_Dtl_Type_Record = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Type_Record", "DTL-TYPE-RECORD", FieldType.STRING, 2, new DbsArrayController(1,
            50));
        lama1001_Dtl_Occurrence_Nbr = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Occurrence_Nbr", "DTL-OCCURRENCE-NBR", FieldType.NUMERIC, 
            4, new DbsArrayController(1,50));
        lama1001_Dtl_Transaction_Amt = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Transaction_Amt", "DTL-TRANSACTION-AMT", FieldType.DECIMAL, 
            16,2, new DbsArrayController(1,50));
        lama1001_Dtl_From_Tiaa_Contract = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_From_Tiaa_Contract", "DTL-FROM-TIAA-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1,50));
        lama1001_Dtl_From_Cref_Contract = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_From_Cref_Contract", "DTL-FROM-CREF-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1,50));
        lama1001_Dtl_From_Fund_Ticker_Symbol = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_From_Fund_Ticker_Symbol", "DTL-FROM-FUND-TICKER-SYMBOL", 
            FieldType.STRING, 10, new DbsArrayController(1,50));
        lama1001_Dtl_From_Lob = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_From_Lob", "DTL-FROM-LOB", FieldType.STRING, 10, new DbsArrayController(1,
            50));
        lama1001_Dtl_From_Sub_Lob = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_From_Sub_Lob", "DTL-FROM-SUB-LOB", FieldType.STRING, 12, new 
            DbsArrayController(1,50));
        lama1001_Dtl_Number_Of_From_Units = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Number_Of_From_Units", "DTL-NUMBER-OF-FROM-UNITS", FieldType.DECIMAL, 
            11,3, new DbsArrayController(1,50));
        lama1001_Dtl_To_Tiaa_Contract = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_To_Tiaa_Contract", "DTL-TO-TIAA-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1,50));
        lama1001_Dtl_To_Cref_Contract = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_To_Cref_Contract", "DTL-TO-CREF-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1,50));
        lama1001_Dtl_To_Fund_Ticker_Symbol = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_To_Fund_Ticker_Symbol", "DTL-TO-FUND-TICKER-SYMBOL", 
            FieldType.STRING, 10, new DbsArrayController(1,50));
        lama1001_Dtl_To_Lob = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_To_Lob", "DTL-TO-LOB", FieldType.STRING, 10, new DbsArrayController(1,
            50));
        lama1001_Dtl_To_Sub_Lob = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_To_Sub_Lob", "DTL-TO-SUB-LOB", FieldType.STRING, 12, new DbsArrayController(1,
            50));
        lama1001_Dtl_Number_Of_To_Units = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Number_Of_To_Units", "DTL-NUMBER-OF-TO-UNITS", FieldType.DECIMAL, 
            11,3, new DbsArrayController(1,50));
        lama1001_Dtl_Rate_Basis = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Rate_Basis", "DTL-RATE-BASIS", FieldType.STRING, 2, new DbsArrayController(1,
            50));
        lama1001_Dtl_Unledger_Indicator = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Unledger_Indicator", "DTL-UNLEDGER-INDICATOR", FieldType.STRING, 
            1, new DbsArrayController(1,50));
        lama1001_Dtl_Reversal_Code = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Reversal_Code", "DTL-REVERSAL-CODE", FieldType.STRING, 1, new 
            DbsArrayController(1,50));
        lama1001_Dtl_Department_Id = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Department_Id", "DTL-DEPARTMENT-ID", FieldType.STRING, 10, 
            new DbsArrayController(1,50));
        lama1001_Dtl_Allocation_Index = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Allocation_Index", "DTL-ALLOCATION-INDEX", FieldType.STRING, 
            6, new DbsArrayController(1,50));
        lama1001_Dtl_Valuation_Basis = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Valuation_Basis", "DTL-VALUATION-BASIS", FieldType.STRING, 
            5, new DbsArrayController(1,50));
        lama1001_Dtl_Dps_Distribution_Type = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Dps_Distribution_Type", "DTL-DPS-DISTRIBUTION-TYPE", 
            FieldType.STRING, 1, new DbsArrayController(1,50));
        lama1001_Dtl_Dps_Summary_Or_Detail = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Dps_Summary_Or_Detail", "DTL-DPS-SUMMARY-OR-DETAIL", 
            FieldType.STRING, 1, new DbsArrayController(1,50));
        lama1001_Dtl_Dps_Id_Tag = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Dps_Id_Tag", "DTL-DPS-ID-TAG", FieldType.STRING, 21, new DbsArrayController(1,
            50));
        lama1001_Dtl_Ledger_Type = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Ledger_Type", "DTL-LEDGER-TYPE", FieldType.STRING, 1, new DbsArrayController(1,
            50));
        lama1001_Dtl_Cntrct_Cancel_Rdrw_Actvty_Cd = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Cntrct_Cancel_Rdrw_Actvty_Cd", "DTL-CNTRCT-CANCEL-RDRW-ACTVTY-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,50));
        lama1001_Dtl_Settlement_Ind2 = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Settlement_Ind2", "DTL-SETTLEMENT-IND2", FieldType.STRING, 
            1, new DbsArrayController(1,50));
        lama1001_Dtl_Settlement_Ind = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Settlement_Ind", "DTL-SETTLEMENT-IND", FieldType.STRING, 1, 
            new DbsArrayController(1,50));
        lama1001_Dtl_Tax_Ind = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Tax_Ind", "DTL-TAX-IND", FieldType.STRING, 1, new DbsArrayController(1,
            50));
        lama1001_Dtl_Net_Cash_Ind = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Net_Cash_Ind", "DTL-NET-CASH-IND", FieldType.STRING, 1, new 
            DbsArrayController(1,50));
        lama1001_Dtl_Installment_Date = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Installment_Date", "DTL-INSTALLMENT-DATE", FieldType.NUMERIC, 
            8, new DbsArrayController(1,50));
        lama1001_Dtl_First_Year_Renewal_Indicator = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_First_Year_Renewal_Indicator", "DTL-FIRST-YEAR-RENEWAL-INDICATOR", 
            FieldType.STRING, 1, new DbsArrayController(1,50));
        lama1001_Dtl_Sub_Plan_Number = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Sub_Plan_Number", "DTL-SUB-PLAN-NUMBER", FieldType.STRING, 
            6, new DbsArrayController(1,50));
        lama1001_Dtl_Omni_Transaction_Code = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Omni_Transaction_Code", "DTL-OMNI-TRANSACTION-CODE", 
            FieldType.STRING, 3, new DbsArrayController(1,50));
        lama1001_Dtl_Omni_Activity_Code = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Omni_Activity_Code", "DTL-OMNI-ACTIVITY-CODE", FieldType.STRING, 
            3, new DbsArrayController(1,50));
        lama1001_Dtl_Omni_Usage_Code = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Omni_Usage_Code", "DTL-OMNI-USAGE-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,50));
        lama1001_Dtl_Loan_Number = lama1001_Group_Pda.newFieldArrayInGroup("lama1001_Dtl_Loan_Number", "DTL-LOAN-NUMBER", FieldType.STRING, 3, new DbsArrayController(1,
            50));

        this.setRecordName("LdaIatlcalm");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIatlcalm() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
