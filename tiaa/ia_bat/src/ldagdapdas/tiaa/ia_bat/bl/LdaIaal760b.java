/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:32 PM
**        * FROM NATURAL LDA     : IAAL760B
************************************************************
**        * FILE NAME            : LdaIaal760b.java
**        * CLASS NAME           : LdaIaal760b
**        * INSTANCE NAME        : LdaIaal760b
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal760b extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Esp_Data;
    private DbsField pnd_Esp_Data_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Esp_Data_Pnd_Fa;
    private DbsField pnd_Esp_Data_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Esp_Data_Pnd_Fb;
    private DbsField pnd_Esp_Data_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Esp_Data_Pnd_Fc;
    private DbsField pnd_Esp_Data_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Esp_Data_Pnd_Fd;
    private DbsField pnd_Esp_Data_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Esp_Data_Pnd_Fe;
    private DbsField pnd_Esp_Data_Pnd_Cntrct_Actvty_Cde;
    private DbsField pnd_Esp_Data_Pnd_Ff;
    private DbsField pnd_Esp_Data_Pnd_1st_Pymnt_Due_Dte;
    private DbsField pnd_Esp_Data_Pnd_Fg;
    private DbsField pnd_Esp_Data_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Esp_Data_Pnd_Fh;
    private DbsField pnd_Esp_Data_Pnd_Orig_Tiaa_Nbr;
    private DbsField pnd_Esp_Data_Pnd_Fi;
    private DbsField pnd_Esp_Data_Pnd_Orig_Cref_Nbr;
    private DbsField pnd_Esp_Data_Pnd_Fj;
    private DbsField pnd_Esp_Data_Pnd_Iaa_Tiaa_Nbr;
    private DbsField pnd_Esp_Data_Pnd_Fk;
    private DbsField pnd_Esp_Data_Pnd_Iaa_Cref_Nbr;
    private DbsField pnd_Esp_Data_Pnd_Fl;
    private DbsField pnd_Esp_Data_Pnd_1st_Annt_Ssn;
    private DbsField pnd_Esp_Data_Pnd_Fm;
    private DbsField pnd_Esp_Data_Pnd_Cpr_Pin_Nbr;
    private DbsField pnd_Esp_Data_Pnd_Fn;
    private DbsField pnd_Esp_Data_Pnd_1st_Annt_Dob_Dte;
    private DbsField pnd_Esp_Data_Pnd_Fo;
    private DbsField pnd_Esp_Data_Pnd_1st_Annt_Sex_Code;
    private DbsField pnd_Esp_Data_Pnd_Fp;
    private DbsField pnd_Esp_Data_Pnd_1st_Annt_Fname;
    private DbsField pnd_Esp_Data_Pnd_Fq;
    private DbsField pnd_Esp_Data_Pnd_1st_Annt_Mname;
    private DbsField pnd_Esp_Data_Pnd_Fr;
    private DbsField pnd_Esp_Data_Pnd_1st_Annt_Lname;
    private DbsField pnd_Esp_Data_Pnd_Fs;
    private DbsField pnd_Esp_Data_Pnd_Omni_Plan;
    private DbsField pnd_Esp_Data_Pnd_Ft;
    private DbsField pnd_Esp_Data_Pnd_Omni_Sub_Plan;
    private DbsField pnd_Esp_Data_Pnd_Fu;
    private DbsField pnd_Esp_Data_Pnd_To_Lob;
    private DbsField pnd_Esp_Data_Pnd_Fv;
    private DbsField pnd_Esp_Data_Pnd_To_Sub_Lob;
    private DbsField pnd_Esp_Data_Pnd_Fw;
    private DbsField pnd_Esp_Data_Pnd_Inv_Short_Name;
    private DbsField pnd_Esp_Data_Pnd_Fx;
    private DbsField pnd_Esp_Data_Pnd_From_Fund_Ticker;
    private DbsField pnd_Esp_Data_Pnd_Fy;
    private DbsField pnd_Esp_Data_Pnd_To_Fund_Ticker;
    private DbsField pnd_Esp_Data_Pnd_Fz;
    private DbsField pnd_Esp_Data_Pnd_Fund_Sttlmnt_Amt;
    private DbsField pnd_Esp_Data_Pnd_F1;
    private DbsField pnd_Esp_Data_Pnd_Effective_Date;
    private DbsField pnd_Esp_Data_Pnd_F2;
    private DbsField pnd_Esp_Data_Pnd_Issue_State;
    private DbsField pnd_Esp_Data_Pnd_F3;
    private DbsField pnd_Esp_Data_Pnd_Pymnt_Frequency;
    private DbsField pnd_Esp_Data_Pnd_F4;
    private DbsField pnd_Esp_Data_Pnd_Acct_Type;
    private DbsField pnd_Esp_Data_Pnd_F5;
    private DbsField pnd_Esp_Data_Pnd_Reversal_Code;
    private DbsField pnd_Esp_Data_Pnd_F6;
    private DbsField pnd_Esp_Data_Pnd_Reval_Method;
    private DbsField pnd_Esp_Data_Pnd_F7;
    private DbsField pnd_Esp_Data_Pnd_Res_State;
    private DbsField pnd_Esp_Data_Pnd_F8;
    private DbsField pnd_Esp_Data_Pnd_Extract_Timestamp;
    private DbsField pnd_Esp_Data_Pnd_F9;
    private DbsField pnd_Esp_Data_Pnd_From_Lob;

    public DbsGroup getPnd_Esp_Data() { return pnd_Esp_Data; }

    public DbsField getPnd_Esp_Data_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Esp_Data_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Esp_Data_Pnd_Fa() { return pnd_Esp_Data_Pnd_Fa; }

    public DbsField getPnd_Esp_Data_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Esp_Data_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Esp_Data_Pnd_Fb() { return pnd_Esp_Data_Pnd_Fb; }

    public DbsField getPnd_Esp_Data_Pnd_Cntrct_Optn_Cde() { return pnd_Esp_Data_Pnd_Cntrct_Optn_Cde; }

    public DbsField getPnd_Esp_Data_Pnd_Fc() { return pnd_Esp_Data_Pnd_Fc; }

    public DbsField getPnd_Esp_Data_Pnd_Cntrct_Orgn_Cde() { return pnd_Esp_Data_Pnd_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Esp_Data_Pnd_Fd() { return pnd_Esp_Data_Pnd_Fd; }

    public DbsField getPnd_Esp_Data_Pnd_Cntrct_Issue_Dte() { return pnd_Esp_Data_Pnd_Cntrct_Issue_Dte; }

    public DbsField getPnd_Esp_Data_Pnd_Fe() { return pnd_Esp_Data_Pnd_Fe; }

    public DbsField getPnd_Esp_Data_Pnd_Cntrct_Actvty_Cde() { return pnd_Esp_Data_Pnd_Cntrct_Actvty_Cde; }

    public DbsField getPnd_Esp_Data_Pnd_Ff() { return pnd_Esp_Data_Pnd_Ff; }

    public DbsField getPnd_Esp_Data_Pnd_1st_Pymnt_Due_Dte() { return pnd_Esp_Data_Pnd_1st_Pymnt_Due_Dte; }

    public DbsField getPnd_Esp_Data_Pnd_Fg() { return pnd_Esp_Data_Pnd_Fg; }

    public DbsField getPnd_Esp_Data_Pnd_Cntrct_Crrncy_Cde() { return pnd_Esp_Data_Pnd_Cntrct_Crrncy_Cde; }

    public DbsField getPnd_Esp_Data_Pnd_Fh() { return pnd_Esp_Data_Pnd_Fh; }

    public DbsField getPnd_Esp_Data_Pnd_Orig_Tiaa_Nbr() { return pnd_Esp_Data_Pnd_Orig_Tiaa_Nbr; }

    public DbsField getPnd_Esp_Data_Pnd_Fi() { return pnd_Esp_Data_Pnd_Fi; }

    public DbsField getPnd_Esp_Data_Pnd_Orig_Cref_Nbr() { return pnd_Esp_Data_Pnd_Orig_Cref_Nbr; }

    public DbsField getPnd_Esp_Data_Pnd_Fj() { return pnd_Esp_Data_Pnd_Fj; }

    public DbsField getPnd_Esp_Data_Pnd_Iaa_Tiaa_Nbr() { return pnd_Esp_Data_Pnd_Iaa_Tiaa_Nbr; }

    public DbsField getPnd_Esp_Data_Pnd_Fk() { return pnd_Esp_Data_Pnd_Fk; }

    public DbsField getPnd_Esp_Data_Pnd_Iaa_Cref_Nbr() { return pnd_Esp_Data_Pnd_Iaa_Cref_Nbr; }

    public DbsField getPnd_Esp_Data_Pnd_Fl() { return pnd_Esp_Data_Pnd_Fl; }

    public DbsField getPnd_Esp_Data_Pnd_1st_Annt_Ssn() { return pnd_Esp_Data_Pnd_1st_Annt_Ssn; }

    public DbsField getPnd_Esp_Data_Pnd_Fm() { return pnd_Esp_Data_Pnd_Fm; }

    public DbsField getPnd_Esp_Data_Pnd_Cpr_Pin_Nbr() { return pnd_Esp_Data_Pnd_Cpr_Pin_Nbr; }

    public DbsField getPnd_Esp_Data_Pnd_Fn() { return pnd_Esp_Data_Pnd_Fn; }

    public DbsField getPnd_Esp_Data_Pnd_1st_Annt_Dob_Dte() { return pnd_Esp_Data_Pnd_1st_Annt_Dob_Dte; }

    public DbsField getPnd_Esp_Data_Pnd_Fo() { return pnd_Esp_Data_Pnd_Fo; }

    public DbsField getPnd_Esp_Data_Pnd_1st_Annt_Sex_Code() { return pnd_Esp_Data_Pnd_1st_Annt_Sex_Code; }

    public DbsField getPnd_Esp_Data_Pnd_Fp() { return pnd_Esp_Data_Pnd_Fp; }

    public DbsField getPnd_Esp_Data_Pnd_1st_Annt_Fname() { return pnd_Esp_Data_Pnd_1st_Annt_Fname; }

    public DbsField getPnd_Esp_Data_Pnd_Fq() { return pnd_Esp_Data_Pnd_Fq; }

    public DbsField getPnd_Esp_Data_Pnd_1st_Annt_Mname() { return pnd_Esp_Data_Pnd_1st_Annt_Mname; }

    public DbsField getPnd_Esp_Data_Pnd_Fr() { return pnd_Esp_Data_Pnd_Fr; }

    public DbsField getPnd_Esp_Data_Pnd_1st_Annt_Lname() { return pnd_Esp_Data_Pnd_1st_Annt_Lname; }

    public DbsField getPnd_Esp_Data_Pnd_Fs() { return pnd_Esp_Data_Pnd_Fs; }

    public DbsField getPnd_Esp_Data_Pnd_Omni_Plan() { return pnd_Esp_Data_Pnd_Omni_Plan; }

    public DbsField getPnd_Esp_Data_Pnd_Ft() { return pnd_Esp_Data_Pnd_Ft; }

    public DbsField getPnd_Esp_Data_Pnd_Omni_Sub_Plan() { return pnd_Esp_Data_Pnd_Omni_Sub_Plan; }

    public DbsField getPnd_Esp_Data_Pnd_Fu() { return pnd_Esp_Data_Pnd_Fu; }

    public DbsField getPnd_Esp_Data_Pnd_To_Lob() { return pnd_Esp_Data_Pnd_To_Lob; }

    public DbsField getPnd_Esp_Data_Pnd_Fv() { return pnd_Esp_Data_Pnd_Fv; }

    public DbsField getPnd_Esp_Data_Pnd_To_Sub_Lob() { return pnd_Esp_Data_Pnd_To_Sub_Lob; }

    public DbsField getPnd_Esp_Data_Pnd_Fw() { return pnd_Esp_Data_Pnd_Fw; }

    public DbsField getPnd_Esp_Data_Pnd_Inv_Short_Name() { return pnd_Esp_Data_Pnd_Inv_Short_Name; }

    public DbsField getPnd_Esp_Data_Pnd_Fx() { return pnd_Esp_Data_Pnd_Fx; }

    public DbsField getPnd_Esp_Data_Pnd_From_Fund_Ticker() { return pnd_Esp_Data_Pnd_From_Fund_Ticker; }

    public DbsField getPnd_Esp_Data_Pnd_Fy() { return pnd_Esp_Data_Pnd_Fy; }

    public DbsField getPnd_Esp_Data_Pnd_To_Fund_Ticker() { return pnd_Esp_Data_Pnd_To_Fund_Ticker; }

    public DbsField getPnd_Esp_Data_Pnd_Fz() { return pnd_Esp_Data_Pnd_Fz; }

    public DbsField getPnd_Esp_Data_Pnd_Fund_Sttlmnt_Amt() { return pnd_Esp_Data_Pnd_Fund_Sttlmnt_Amt; }

    public DbsField getPnd_Esp_Data_Pnd_F1() { return pnd_Esp_Data_Pnd_F1; }

    public DbsField getPnd_Esp_Data_Pnd_Effective_Date() { return pnd_Esp_Data_Pnd_Effective_Date; }

    public DbsField getPnd_Esp_Data_Pnd_F2() { return pnd_Esp_Data_Pnd_F2; }

    public DbsField getPnd_Esp_Data_Pnd_Issue_State() { return pnd_Esp_Data_Pnd_Issue_State; }

    public DbsField getPnd_Esp_Data_Pnd_F3() { return pnd_Esp_Data_Pnd_F3; }

    public DbsField getPnd_Esp_Data_Pnd_Pymnt_Frequency() { return pnd_Esp_Data_Pnd_Pymnt_Frequency; }

    public DbsField getPnd_Esp_Data_Pnd_F4() { return pnd_Esp_Data_Pnd_F4; }

    public DbsField getPnd_Esp_Data_Pnd_Acct_Type() { return pnd_Esp_Data_Pnd_Acct_Type; }

    public DbsField getPnd_Esp_Data_Pnd_F5() { return pnd_Esp_Data_Pnd_F5; }

    public DbsField getPnd_Esp_Data_Pnd_Reversal_Code() { return pnd_Esp_Data_Pnd_Reversal_Code; }

    public DbsField getPnd_Esp_Data_Pnd_F6() { return pnd_Esp_Data_Pnd_F6; }

    public DbsField getPnd_Esp_Data_Pnd_Reval_Method() { return pnd_Esp_Data_Pnd_Reval_Method; }

    public DbsField getPnd_Esp_Data_Pnd_F7() { return pnd_Esp_Data_Pnd_F7; }

    public DbsField getPnd_Esp_Data_Pnd_Res_State() { return pnd_Esp_Data_Pnd_Res_State; }

    public DbsField getPnd_Esp_Data_Pnd_F8() { return pnd_Esp_Data_Pnd_F8; }

    public DbsField getPnd_Esp_Data_Pnd_Extract_Timestamp() { return pnd_Esp_Data_Pnd_Extract_Timestamp; }

    public DbsField getPnd_Esp_Data_Pnd_F9() { return pnd_Esp_Data_Pnd_F9; }

    public DbsField getPnd_Esp_Data_Pnd_From_Lob() { return pnd_Esp_Data_Pnd_From_Lob; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Esp_Data = newGroupInRecord("pnd_Esp_Data", "#ESP-DATA");
        pnd_Esp_Data_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Esp_Data_Pnd_Fa = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fa", "#FA", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Cntrct_Part_Payee_Cde = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Esp_Data_Pnd_Fb = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fb", "#FB", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Cntrct_Optn_Cde = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Esp_Data_Pnd_Fc = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fc", "#FC", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Cntrct_Orgn_Cde = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Esp_Data_Pnd_Fd = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fd", "#FD", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Cntrct_Issue_Dte = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Esp_Data_Pnd_Fe = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fe", "#FE", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Cntrct_Actvty_Cde = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cntrct_Actvty_Cde", "#CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Esp_Data_Pnd_Ff = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Ff", "#FF", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_1st_Pymnt_Due_Dte = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_1st_Pymnt_Due_Dte", "#1ST-PYMNT-DUE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Esp_Data_Pnd_Fg = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fg", "#FG", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Cntrct_Crrncy_Cde = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Esp_Data_Pnd_Fh = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fh", "#FH", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Orig_Tiaa_Nbr = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Orig_Tiaa_Nbr", "#ORIG-TIAA-NBR", FieldType.STRING, 10);
        pnd_Esp_Data_Pnd_Fi = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fi", "#FI", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Orig_Cref_Nbr = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Orig_Cref_Nbr", "#ORIG-CREF-NBR", FieldType.STRING, 10);
        pnd_Esp_Data_Pnd_Fj = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fj", "#FJ", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Iaa_Tiaa_Nbr = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Iaa_Tiaa_Nbr", "#IAA-TIAA-NBR", FieldType.STRING, 10);
        pnd_Esp_Data_Pnd_Fk = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fk", "#FK", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Iaa_Cref_Nbr = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Iaa_Cref_Nbr", "#IAA-CREF-NBR", FieldType.STRING, 10);
        pnd_Esp_Data_Pnd_Fl = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fl", "#FL", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_1st_Annt_Ssn = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_1st_Annt_Ssn", "#1ST-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Esp_Data_Pnd_Fm = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fm", "#FM", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Cpr_Pin_Nbr = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Cpr_Pin_Nbr", "#CPR-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Esp_Data_Pnd_Fn = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fn", "#FN", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_1st_Annt_Dob_Dte = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_1st_Annt_Dob_Dte", "#1ST-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Esp_Data_Pnd_Fo = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fo", "#FO", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_1st_Annt_Sex_Code = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_1st_Annt_Sex_Code", "#1ST-ANNT-SEX-CODE", FieldType.NUMERIC, 
            1);
        pnd_Esp_Data_Pnd_Fp = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fp", "#FP", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_1st_Annt_Fname = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_1st_Annt_Fname", "#1ST-ANNT-FNAME", FieldType.STRING, 35);
        pnd_Esp_Data_Pnd_Fq = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fq", "#FQ", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_1st_Annt_Mname = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_1st_Annt_Mname", "#1ST-ANNT-MNAME", FieldType.STRING, 35);
        pnd_Esp_Data_Pnd_Fr = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fr", "#FR", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_1st_Annt_Lname = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_1st_Annt_Lname", "#1ST-ANNT-LNAME", FieldType.STRING, 35);
        pnd_Esp_Data_Pnd_Fs = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fs", "#FS", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Omni_Plan = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Omni_Plan", "#OMNI-PLAN", FieldType.STRING, 6);
        pnd_Esp_Data_Pnd_Ft = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Ft", "#FT", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Omni_Sub_Plan = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Omni_Sub_Plan", "#OMNI-SUB-PLAN", FieldType.STRING, 6);
        pnd_Esp_Data_Pnd_Fu = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fu", "#FU", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_To_Lob = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_To_Lob", "#TO-LOB", FieldType.STRING, 10);
        pnd_Esp_Data_Pnd_Fv = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fv", "#FV", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_To_Sub_Lob = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_To_Sub_Lob", "#TO-SUB-LOB", FieldType.STRING, 12);
        pnd_Esp_Data_Pnd_Fw = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fw", "#FW", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Inv_Short_Name = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Inv_Short_Name", "#INV-SHORT-NAME", FieldType.STRING, 5);
        pnd_Esp_Data_Pnd_Fx = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fx", "#FX", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_From_Fund_Ticker = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_From_Fund_Ticker", "#FROM-FUND-TICKER", FieldType.STRING, 10);
        pnd_Esp_Data_Pnd_Fy = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fy", "#FY", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_To_Fund_Ticker = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_To_Fund_Ticker", "#TO-FUND-TICKER", FieldType.STRING, 10);
        pnd_Esp_Data_Pnd_Fz = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fz", "#FZ", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Fund_Sttlmnt_Amt = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Fund_Sttlmnt_Amt", "#FUND-STTLMNT-AMT", FieldType.STRING, 12);
        pnd_Esp_Data_Pnd_F1 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Effective_Date = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_Esp_Data_Pnd_F2 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Issue_State = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Issue_State", "#ISSUE-STATE", FieldType.STRING, 3);
        pnd_Esp_Data_Pnd_F3 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Pymnt_Frequency = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Pymnt_Frequency", "#PYMNT-FREQUENCY", FieldType.NUMERIC, 3);
        pnd_Esp_Data_Pnd_F4 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Acct_Type = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Acct_Type", "#ACCT-TYPE", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_F5 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Reversal_Code = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Reversal_Code", "#REVERSAL-CODE", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_F6 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F6", "#F6", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Reval_Method = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Reval_Method", "#REVAL-METHOD", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_F7 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F7", "#F7", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Res_State = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Res_State", "#RES-STATE", FieldType.STRING, 3);
        pnd_Esp_Data_Pnd_F8 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F8", "#F8", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_Extract_Timestamp = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_Extract_Timestamp", "#EXTRACT-TIMESTAMP", FieldType.STRING, 
            14);
        pnd_Esp_Data_Pnd_F9 = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_F9", "#F9", FieldType.STRING, 1);
        pnd_Esp_Data_Pnd_From_Lob = pnd_Esp_Data.newFieldInGroup("pnd_Esp_Data_Pnd_From_Lob", "#FROM-LOB", FieldType.STRING, 3);

        this.setRecordName("LdaIaal760b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal760b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
