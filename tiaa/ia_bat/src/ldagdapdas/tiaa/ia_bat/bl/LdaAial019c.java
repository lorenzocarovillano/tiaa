/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:28 PM
**        * FROM NATURAL LDA     : AIAL019C
************************************************************
**        * FILE NAME            : LdaAial019c.java
**        * CLASS NAME           : LdaAial019c
**        * INSTANCE NAME        : LdaAial019c
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial019c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Sorta_Record;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area1;
    private DbsGroup pnd_Sorta_Record_Pnd_Sa_Area1Redef1;
    private DbsField pnd_Sorta_Record_Pnd_Sa1_Contract_Payee;
    private DbsField pnd_Sorta_Record_Pnd_Sa1_Payment_Method;
    private DbsField pnd_Sorta_Record_Pnd_Sa1_Current_Record_Number;
    private DbsField pnd_Sorta_Record_Pnd_Sa1_Final_Record_Number;
    private DbsField pnd_Sorta_Record_Pnd_Sa1_Filler1;
    private DbsField pnd_Sorta_Record_Pnd_Sa1_Transfer_Effective_Date;
    private DbsField pnd_Sorta_Record_Pnd_Sa1_Filler2;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area2;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area3;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area4;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area5;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area6;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area7;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area8;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area9;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area10;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area11;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area12;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area13;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area14;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area15;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area16;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area17;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area18;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area19;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Area20;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Key_Record_Number;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number;
    private DbsField pnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key;

    public DbsGroup getPnd_Sorta_Record() { return pnd_Sorta_Record; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area1() { return pnd_Sorta_Record_Pnd_Sa_Area1; }

    public DbsGroup getPnd_Sorta_Record_Pnd_Sa_Area1Redef1() { return pnd_Sorta_Record_Pnd_Sa_Area1Redef1; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa1_Contract_Payee() { return pnd_Sorta_Record_Pnd_Sa1_Contract_Payee; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa1_Payment_Method() { return pnd_Sorta_Record_Pnd_Sa1_Payment_Method; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number() { return pnd_Sorta_Record_Pnd_Sa1_Current_Record_Number; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa1_Final_Record_Number() { return pnd_Sorta_Record_Pnd_Sa1_Final_Record_Number; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa1_Filler1() { return pnd_Sorta_Record_Pnd_Sa1_Filler1; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa1_Transfer_Effective_Date() { return pnd_Sorta_Record_Pnd_Sa1_Transfer_Effective_Date; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa1_Filler2() { return pnd_Sorta_Record_Pnd_Sa1_Filler2; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area2() { return pnd_Sorta_Record_Pnd_Sa_Area2; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area3() { return pnd_Sorta_Record_Pnd_Sa_Area3; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area4() { return pnd_Sorta_Record_Pnd_Sa_Area4; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area5() { return pnd_Sorta_Record_Pnd_Sa_Area5; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area6() { return pnd_Sorta_Record_Pnd_Sa_Area6; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area7() { return pnd_Sorta_Record_Pnd_Sa_Area7; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area8() { return pnd_Sorta_Record_Pnd_Sa_Area8; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area9() { return pnd_Sorta_Record_Pnd_Sa_Area9; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area10() { return pnd_Sorta_Record_Pnd_Sa_Area10; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area11() { return pnd_Sorta_Record_Pnd_Sa_Area11; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area12() { return pnd_Sorta_Record_Pnd_Sa_Area12; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area13() { return pnd_Sorta_Record_Pnd_Sa_Area13; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area14() { return pnd_Sorta_Record_Pnd_Sa_Area14; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area15() { return pnd_Sorta_Record_Pnd_Sa_Area15; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area16() { return pnd_Sorta_Record_Pnd_Sa_Area16; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area17() { return pnd_Sorta_Record_Pnd_Sa_Area17; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area18() { return pnd_Sorta_Record_Pnd_Sa_Area18; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area19() { return pnd_Sorta_Record_Pnd_Sa_Area19; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Area20() { return pnd_Sorta_Record_Pnd_Sa_Area20; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py() { return pnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Key_Record_Number() { return pnd_Sorta_Record_Pnd_Sa_Key_Record_Number; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte() { return pnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number() { return pnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number; }

    public DbsField getPnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key() { return pnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Sorta_Record = newGroupInRecord("pnd_Sorta_Record", "#SORTA-RECORD");
        pnd_Sorta_Record_Pnd_Sa_Area1 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area1", "#SA-AREA1", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area1Redef1 = pnd_Sorta_Record.newGroupInGroup("pnd_Sorta_Record_Pnd_Sa_Area1Redef1", "Redefines", pnd_Sorta_Record_Pnd_Sa_Area1);
        pnd_Sorta_Record_Pnd_Sa1_Contract_Payee = pnd_Sorta_Record_Pnd_Sa_Area1Redef1.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa1_Contract_Payee", "#SA1-CONTRACT-PAYEE", 
            FieldType.STRING, 10);
        pnd_Sorta_Record_Pnd_Sa1_Payment_Method = pnd_Sorta_Record_Pnd_Sa_Area1Redef1.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa1_Payment_Method", "#SA1-PAYMENT-METHOD", 
            FieldType.STRING, 1);
        pnd_Sorta_Record_Pnd_Sa1_Current_Record_Number = pnd_Sorta_Record_Pnd_Sa_Area1Redef1.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa1_Current_Record_Number", 
            "#SA1-CURRENT-RECORD-NUMBER", FieldType.NUMERIC, 2);
        pnd_Sorta_Record_Pnd_Sa1_Final_Record_Number = pnd_Sorta_Record_Pnd_Sa_Area1Redef1.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa1_Final_Record_Number", 
            "#SA1-FINAL-RECORD-NUMBER", FieldType.NUMERIC, 2);
        pnd_Sorta_Record_Pnd_Sa1_Filler1 = pnd_Sorta_Record_Pnd_Sa_Area1Redef1.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa1_Filler1", "#SA1-FILLER1", FieldType.STRING, 
            51);
        pnd_Sorta_Record_Pnd_Sa1_Transfer_Effective_Date = pnd_Sorta_Record_Pnd_Sa_Area1Redef1.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa1_Transfer_Effective_Date", 
            "#SA1-TRANSFER-EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_Sorta_Record_Pnd_Sa1_Filler2 = pnd_Sorta_Record_Pnd_Sa_Area1Redef1.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa1_Filler2", "#SA1-FILLER2", FieldType.STRING, 
            176);
        pnd_Sorta_Record_Pnd_Sa_Area2 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area2", "#SA-AREA2", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area3 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area3", "#SA-AREA3", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area4 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area4", "#SA-AREA4", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area5 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area5", "#SA-AREA5", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area6 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area6", "#SA-AREA6", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area7 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area7", "#SA-AREA7", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area8 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area8", "#SA-AREA8", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area9 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area9", "#SA-AREA9", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area10 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area10", "#SA-AREA10", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area11 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area11", "#SA-AREA11", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area12 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area12", "#SA-AREA12", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area13 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area13", "#SA-AREA13", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area14 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area14", "#SA-AREA14", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area15 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area15", "#SA-AREA15", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area16 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area16", "#SA-AREA16", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area17 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area17", "#SA-AREA17", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area18 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area18", "#SA-AREA18", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area19 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area19", "#SA-AREA19", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Area20 = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Area20", "#SA-AREA20", FieldType.STRING, 250);
        pnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py", "#SA-KEY-CNTRCT-PY", FieldType.STRING, 
            10);
        pnd_Sorta_Record_Pnd_Sa_Key_Record_Number = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Key_Record_Number", "#SA-KEY-RECORD-NUMBER", 
            FieldType.NUMERIC, 2);
        pnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte", "#SA-KEY-PRCSS-DTE", FieldType.NUMERIC, 
            8);
        pnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number", "#SA-KEY-SEQUENCE-NUMBER", 
            FieldType.NUMERIC, 2);
        pnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key = pnd_Sorta_Record.newFieldInGroup("pnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key", "#SA-KEY-LOGC-TRANSACTION-KEY", 
            FieldType.BINARY, 8);

        this.setRecordName("LdaAial019c");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAial019c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
