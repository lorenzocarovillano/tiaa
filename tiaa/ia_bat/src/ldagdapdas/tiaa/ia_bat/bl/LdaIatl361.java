/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:37 PM
**        * FROM NATURAL LDA     : IATL361
************************************************************
**        * FILE NAME            : LdaIatl361.java
**        * CLASS NAME           : LdaIatl361
**        * INSTANCE NAME        : LdaIatl361
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIatl361 extends DbsRecord
{
    // Properties
    private DbsGroup iat_Trnsfr_Rpt;
    private DbsField iat_Trnsfr_Rpt_Reval_Mthd;
    private DbsField iat_Trnsfr_Rpt_Rqst_Cntct_Mde;
    private DbsField iat_Trnsfr_Rpt_Rcrd_Type_Cde;
    private DbsField iat_Trnsfr_Rpt_Rqst_Id;
    private DbsField iat_Trnsfr_Rpt_Rqst_Effctv_Dte;
    private DbsField iat_Trnsfr_Rpt_Rqst_Entry_Dte;
    private DbsField iat_Trnsfr_Rpt_Rqst_Entry_Tme;
    private DbsField iat_Trnsfr_Rpt_Rqst_Rcvd_Dte;
    private DbsField iat_Trnsfr_Rpt_Rqst_Rcvd_Tme;
    private DbsField iat_Trnsfr_Rpt_Rqst_Opn_Clsd_Ind;
    private DbsField iat_Trnsfr_Rpt_Xfr_Stts_Cde;
    private DbsField iat_Trnsfr_Rpt_Xfr_Rjctn_Cde;
    private DbsField iat_Trnsfr_Rpt_Ia_Frm_Payee;
    private DbsField iat_Trnsfr_Rpt_Ia_Unique_Id;
    private DbsField iat_Trnsfr_Rpt_Ia_Frm_Cntrct;
    private DbsField iat_Trnsfr_Rpt_Rqst_Xfr_Type;
    private DbsField iat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde;
    private DbsField iat_Trnsfr_Rpt_Xfr_Frm_Unit_Typ;
    private DbsField iat_Trnsfr_Rpt_Xfr_Frm_Typ;
    private DbsField iat_Trnsfr_Rpt_Xfr_Frm_Qty;
    private DbsField iat_Trnsfr_Rpt_Ia_To_Cntrct;
    private DbsField iat_Trnsfr_Rpt_Xfr_To_Acct_Cde;
    private DbsField iat_Trnsfr_Rpt_Xfr_To_Unit_Typ;
    private DbsField iat_Trnsfr_Rpt_Xfr_To_Typ;
    private DbsField iat_Trnsfr_Rpt_Xfr_To_Qty;
    private DbsField iat_Trnsfr_Rpt_Prtcpnt_Nme;
    private DbsField iat_Trnsfr_Rpt_Rqst_Unit_Cde;
    private DbsGroup iat_Trnsfr_Rpt_Rqst_Unit_CdeRedef1;
    private DbsField iat_Trnsfr_Rpt_Rqst_Unit_Cde_7;
    private DbsField iat_Trnsfr_Rpt_Rqst_Unit_Cde_Lst_1;
    private DbsField iat_Trnsfr_Rpt_Rqst_New_Iss_Prt_Pull;

    public DbsGroup getIat_Trnsfr_Rpt() { return iat_Trnsfr_Rpt; }

    public DbsField getIat_Trnsfr_Rpt_Reval_Mthd() { return iat_Trnsfr_Rpt_Reval_Mthd; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Cntct_Mde() { return iat_Trnsfr_Rpt_Rqst_Cntct_Mde; }

    public DbsField getIat_Trnsfr_Rpt_Rcrd_Type_Cde() { return iat_Trnsfr_Rpt_Rcrd_Type_Cde; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Id() { return iat_Trnsfr_Rpt_Rqst_Id; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Effctv_Dte() { return iat_Trnsfr_Rpt_Rqst_Effctv_Dte; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Entry_Dte() { return iat_Trnsfr_Rpt_Rqst_Entry_Dte; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Entry_Tme() { return iat_Trnsfr_Rpt_Rqst_Entry_Tme; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Rcvd_Dte() { return iat_Trnsfr_Rpt_Rqst_Rcvd_Dte; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Rcvd_Tme() { return iat_Trnsfr_Rpt_Rqst_Rcvd_Tme; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Opn_Clsd_Ind() { return iat_Trnsfr_Rpt_Rqst_Opn_Clsd_Ind; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_Stts_Cde() { return iat_Trnsfr_Rpt_Xfr_Stts_Cde; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_Rjctn_Cde() { return iat_Trnsfr_Rpt_Xfr_Rjctn_Cde; }

    public DbsField getIat_Trnsfr_Rpt_Ia_Frm_Payee() { return iat_Trnsfr_Rpt_Ia_Frm_Payee; }

    public DbsField getIat_Trnsfr_Rpt_Ia_Unique_Id() { return iat_Trnsfr_Rpt_Ia_Unique_Id; }

    public DbsField getIat_Trnsfr_Rpt_Ia_Frm_Cntrct() { return iat_Trnsfr_Rpt_Ia_Frm_Cntrct; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Xfr_Type() { return iat_Trnsfr_Rpt_Rqst_Xfr_Type; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde() { return iat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_Frm_Unit_Typ() { return iat_Trnsfr_Rpt_Xfr_Frm_Unit_Typ; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_Frm_Typ() { return iat_Trnsfr_Rpt_Xfr_Frm_Typ; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_Frm_Qty() { return iat_Trnsfr_Rpt_Xfr_Frm_Qty; }

    public DbsField getIat_Trnsfr_Rpt_Ia_To_Cntrct() { return iat_Trnsfr_Rpt_Ia_To_Cntrct; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_To_Acct_Cde() { return iat_Trnsfr_Rpt_Xfr_To_Acct_Cde; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_To_Unit_Typ() { return iat_Trnsfr_Rpt_Xfr_To_Unit_Typ; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_To_Typ() { return iat_Trnsfr_Rpt_Xfr_To_Typ; }

    public DbsField getIat_Trnsfr_Rpt_Xfr_To_Qty() { return iat_Trnsfr_Rpt_Xfr_To_Qty; }

    public DbsField getIat_Trnsfr_Rpt_Prtcpnt_Nme() { return iat_Trnsfr_Rpt_Prtcpnt_Nme; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Unit_Cde() { return iat_Trnsfr_Rpt_Rqst_Unit_Cde; }

    public DbsGroup getIat_Trnsfr_Rpt_Rqst_Unit_CdeRedef1() { return iat_Trnsfr_Rpt_Rqst_Unit_CdeRedef1; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Unit_Cde_7() { return iat_Trnsfr_Rpt_Rqst_Unit_Cde_7; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_Unit_Cde_Lst_1() { return iat_Trnsfr_Rpt_Rqst_Unit_Cde_Lst_1; }

    public DbsField getIat_Trnsfr_Rpt_Rqst_New_Iss_Prt_Pull() { return iat_Trnsfr_Rpt_Rqst_New_Iss_Prt_Pull; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iat_Trnsfr_Rpt = newGroupInRecord("iat_Trnsfr_Rpt", "IAT-TRNSFR-RPT");
        iat_Trnsfr_Rpt_Reval_Mthd = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Reval_Mthd", "REVAL-MTHD", FieldType.STRING, 1);
        iat_Trnsfr_Rpt_Rqst_Cntct_Mde = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 1);
        iat_Trnsfr_Rpt_Rcrd_Type_Cde = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1);
        iat_Trnsfr_Rpt_Rqst_Id = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Id", "RQST-ID", FieldType.STRING, 34);
        iat_Trnsfr_Rpt_Rqst_Effctv_Dte = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE);
        iat_Trnsfr_Rpt_Rqst_Entry_Dte = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE);
        iat_Trnsfr_Rpt_Rqst_Entry_Tme = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 7);
        iat_Trnsfr_Rpt_Rqst_Rcvd_Dte = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE);
        iat_Trnsfr_Rpt_Rqst_Rcvd_Tme = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 7);
        iat_Trnsfr_Rpt_Rqst_Opn_Clsd_Ind = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", FieldType.STRING, 1);
        iat_Trnsfr_Rpt_Xfr_Stts_Cde = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2);
        iat_Trnsfr_Rpt_Xfr_Rjctn_Cde = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2);
        iat_Trnsfr_Rpt_Ia_Frm_Payee = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2);
        iat_Trnsfr_Rpt_Ia_Unique_Id = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12);
        iat_Trnsfr_Rpt_Ia_Frm_Cntrct = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10);
        iat_Trnsfr_Rpt_Rqst_Xfr_Type = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1);
        iat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        iat_Trnsfr_Rpt_Xfr_Frm_Unit_Typ = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        iat_Trnsfr_Rpt_Xfr_Frm_Typ = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1, new DbsArrayController(1,
            20));
        iat_Trnsfr_Rpt_Xfr_Frm_Qty = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 9,2, new 
            DbsArrayController(1,20));
        iat_Trnsfr_Rpt_Ia_To_Cntrct = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10);
        iat_Trnsfr_Rpt_Xfr_To_Acct_Cde = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 1, 
            new DbsArrayController(1,40));
        iat_Trnsfr_Rpt_Xfr_To_Unit_Typ = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 1, 
            new DbsArrayController(1,40));
        iat_Trnsfr_Rpt_Xfr_To_Typ = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1, new DbsArrayController(1,
            40));
        iat_Trnsfr_Rpt_Xfr_To_Qty = iat_Trnsfr_Rpt.newFieldArrayInGroup("iat_Trnsfr_Rpt_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            40));
        iat_Trnsfr_Rpt_Prtcpnt_Nme = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Prtcpnt_Nme", "PRTCPNT-NME", FieldType.STRING, 25);
        iat_Trnsfr_Rpt_Rqst_Unit_Cde = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8);
        iat_Trnsfr_Rpt_Rqst_Unit_CdeRedef1 = iat_Trnsfr_Rpt.newGroupInGroup("iat_Trnsfr_Rpt_Rqst_Unit_CdeRedef1", "Redefines", iat_Trnsfr_Rpt_Rqst_Unit_Cde);
        iat_Trnsfr_Rpt_Rqst_Unit_Cde_7 = iat_Trnsfr_Rpt_Rqst_Unit_CdeRedef1.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Unit_Cde_7", "RQST-UNIT-CDE-7", FieldType.STRING, 
            7);
        iat_Trnsfr_Rpt_Rqst_Unit_Cde_Lst_1 = iat_Trnsfr_Rpt_Rqst_Unit_CdeRedef1.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_Unit_Cde_Lst_1", "RQST-UNIT-CDE-LST-1", 
            FieldType.STRING, 1);
        iat_Trnsfr_Rpt_Rqst_New_Iss_Prt_Pull = iat_Trnsfr_Rpt.newFieldInGroup("iat_Trnsfr_Rpt_Rqst_New_Iss_Prt_Pull", "RQST-NEW-ISS-PRT-PULL", FieldType.STRING, 
            1);

        this.setRecordName("LdaIatl361");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIatl361() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
