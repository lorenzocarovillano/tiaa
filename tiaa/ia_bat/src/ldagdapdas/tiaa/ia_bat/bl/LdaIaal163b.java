/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:24 PM
**        * FROM NATURAL LDA     : IAAL163B
************************************************************
**        * FILE NAME            : LdaIaal163b.java
**        * CLASS NAME           : LdaIaal163b
**        * INSTANCE NAME        : LdaIaal163b
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal163b extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_old_Cref_Rates_S;
    private DbsField old_Cref_Rates_S_Fund_Lst_Pd_Dte;
    private DbsField old_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte;
    private DbsField old_Cref_Rates_S_Trans_Dte;
    private DbsField old_Cref_Rates_S_Trans_User_Area;
    private DbsField old_Cref_Rates_S_Trans_User_Id;
    private DbsField old_Cref_Rates_S_Trans_Verify_Id;
    private DbsField old_Cref_Rates_S_Trans_Verify_Dte;
    private DbsField old_Cref_Rates_S_Cntrct_Ppcn_Nbr;
    private DbsField old_Cref_Rates_S_Cntrct_Payee_Cde;
    private DbsField old_Cref_Rates_S_Cntrct_Mode_Ind;
    private DbsField old_Cref_Rates_S_Rcrd_Srce;
    private DbsField old_Cref_Rates_S_Rcrd_Status;
    private DbsField old_Cref_Rates_S_Cmpny_Fund_Cde;
    private DbsGroup old_Cref_Rates_S_Cmpny_Fund_CdeRedef1;
    private DbsField old_Cref_Rates_S_Cmpny_Cde;
    private DbsField old_Cref_Rates_S_Fund_Cde;
    private DbsField old_Cref_Rates_S_Cntrct_Tot_Per_Amt;
    private DbsField old_Cref_Rates_S_Cntrct_Unit_Val;
    private DbsField old_Cref_Rates_S_Cntrct_Tot_Units;
    private DbsField old_Cref_Rates_S_Lst_Trans_Dte;
    private DbsField old_Cref_Rates_S_Count_Castcref_Rate_Data_Grp;
    private DbsGroup old_Cref_Rates_S_Cref_Rate_Data_Grp;
    private DbsField old_Cref_Rates_S_Cref_Rate_Cde;
    private DbsField old_Cref_Rates_S_Cref_Rate_Dte;
    private DbsField old_Cref_Rates_S_Cref_Per_Pay_Amt;
    private DbsField old_Cref_Rates_S_Cref_No_Units;
    private DataAccessProgramView vw_old_Cref_Rates_R;
    private DbsField old_Cref_Rates_R_Fund_Invrse_Lst_Pd_Dte;
    private DbsField old_Cref_Rates_R_Cntrct_Ppcn_Nbr;
    private DbsField old_Cref_Rates_R_Cntrct_Payee_Cde;
    private DbsField old_Cref_Rates_R_Cmpny_Fund_Cde;
    private DbsGroup old_Cref_Rates_R_Cmpny_Fund_CdeRedef2;
    private DbsField old_Cref_Rates_R_Cmpny_Cde;
    private DbsField old_Cref_Rates_R_Fund_Cde;
    private DbsField old_Cref_Rates_R_Cntrct_Py_Dte_Key;
    private DataAccessProgramView vw_old_Tiaa_Rates;
    private DbsField old_Tiaa_Rates_Fund_Lst_Pd_Dte;
    private DbsField old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte;
    private DbsField old_Tiaa_Rates_Trans_Dte;
    private DbsField old_Tiaa_Rates_Trans_User_Area;
    private DbsField old_Tiaa_Rates_Trans_User_Id;
    private DbsField old_Tiaa_Rates_Trans_Verify_Id;
    private DbsField old_Tiaa_Rates_Trans_Verify_Dte;
    private DbsField old_Tiaa_Rates_Cntrct_Ppcn_Nbr;
    private DbsField old_Tiaa_Rates_Cntrct_Payee_Cde;
    private DbsField old_Tiaa_Rates_Cntrct_Mode_Ind;
    private DbsField old_Tiaa_Rates_Cmpny_Fund_Cde;
    private DbsGroup old_Tiaa_Rates_Cmpny_Fund_CdeRedef3;
    private DbsField old_Tiaa_Rates_Cmpny_Cde;
    private DbsField old_Tiaa_Rates_Fund_Cde;
    private DbsField old_Tiaa_Rates_Rcrd_Srce;
    private DbsField old_Tiaa_Rates_Rcrd_Status;
    private DbsField old_Tiaa_Rates_Cntrct_Tot_Per_Amt;
    private DbsField old_Tiaa_Rates_Cntrct_Tot_Div_Amt;
    private DbsField old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup old_Tiaa_Rates_Tiaa_Rate_Data_Grp;
    private DbsField old_Tiaa_Rates_Tiaa_Rate_Cde;
    private DbsField old_Tiaa_Rates_Tiaa_Rate_Dte;
    private DbsField old_Tiaa_Rates_Tiaa_Per_Pay_Amt;
    private DbsField old_Tiaa_Rates_Tiaa_Per_Div_Amt;
    private DbsField old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt;
    private DbsField old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt;
    private DbsField old_Tiaa_Rates_Lst_Trans_Dte;

    public DataAccessProgramView getVw_old_Cref_Rates_S() { return vw_old_Cref_Rates_S; }

    public DbsField getOld_Cref_Rates_S_Fund_Lst_Pd_Dte() { return old_Cref_Rates_S_Fund_Lst_Pd_Dte; }

    public DbsField getOld_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte() { return old_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte; }

    public DbsField getOld_Cref_Rates_S_Trans_Dte() { return old_Cref_Rates_S_Trans_Dte; }

    public DbsField getOld_Cref_Rates_S_Trans_User_Area() { return old_Cref_Rates_S_Trans_User_Area; }

    public DbsField getOld_Cref_Rates_S_Trans_User_Id() { return old_Cref_Rates_S_Trans_User_Id; }

    public DbsField getOld_Cref_Rates_S_Trans_Verify_Id() { return old_Cref_Rates_S_Trans_Verify_Id; }

    public DbsField getOld_Cref_Rates_S_Trans_Verify_Dte() { return old_Cref_Rates_S_Trans_Verify_Dte; }

    public DbsField getOld_Cref_Rates_S_Cntrct_Ppcn_Nbr() { return old_Cref_Rates_S_Cntrct_Ppcn_Nbr; }

    public DbsField getOld_Cref_Rates_S_Cntrct_Payee_Cde() { return old_Cref_Rates_S_Cntrct_Payee_Cde; }

    public DbsField getOld_Cref_Rates_S_Cntrct_Mode_Ind() { return old_Cref_Rates_S_Cntrct_Mode_Ind; }

    public DbsField getOld_Cref_Rates_S_Rcrd_Srce() { return old_Cref_Rates_S_Rcrd_Srce; }

    public DbsField getOld_Cref_Rates_S_Rcrd_Status() { return old_Cref_Rates_S_Rcrd_Status; }

    public DbsField getOld_Cref_Rates_S_Cmpny_Fund_Cde() { return old_Cref_Rates_S_Cmpny_Fund_Cde; }

    public DbsGroup getOld_Cref_Rates_S_Cmpny_Fund_CdeRedef1() { return old_Cref_Rates_S_Cmpny_Fund_CdeRedef1; }

    public DbsField getOld_Cref_Rates_S_Cmpny_Cde() { return old_Cref_Rates_S_Cmpny_Cde; }

    public DbsField getOld_Cref_Rates_S_Fund_Cde() { return old_Cref_Rates_S_Fund_Cde; }

    public DbsField getOld_Cref_Rates_S_Cntrct_Tot_Per_Amt() { return old_Cref_Rates_S_Cntrct_Tot_Per_Amt; }

    public DbsField getOld_Cref_Rates_S_Cntrct_Unit_Val() { return old_Cref_Rates_S_Cntrct_Unit_Val; }

    public DbsField getOld_Cref_Rates_S_Cntrct_Tot_Units() { return old_Cref_Rates_S_Cntrct_Tot_Units; }

    public DbsField getOld_Cref_Rates_S_Lst_Trans_Dte() { return old_Cref_Rates_S_Lst_Trans_Dte; }

    public DbsField getOld_Cref_Rates_S_Count_Castcref_Rate_Data_Grp() { return old_Cref_Rates_S_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getOld_Cref_Rates_S_Cref_Rate_Data_Grp() { return old_Cref_Rates_S_Cref_Rate_Data_Grp; }

    public DbsField getOld_Cref_Rates_S_Cref_Rate_Cde() { return old_Cref_Rates_S_Cref_Rate_Cde; }

    public DbsField getOld_Cref_Rates_S_Cref_Rate_Dte() { return old_Cref_Rates_S_Cref_Rate_Dte; }

    public DbsField getOld_Cref_Rates_S_Cref_Per_Pay_Amt() { return old_Cref_Rates_S_Cref_Per_Pay_Amt; }

    public DbsField getOld_Cref_Rates_S_Cref_No_Units() { return old_Cref_Rates_S_Cref_No_Units; }

    public DataAccessProgramView getVw_old_Cref_Rates_R() { return vw_old_Cref_Rates_R; }

    public DbsField getOld_Cref_Rates_R_Fund_Invrse_Lst_Pd_Dte() { return old_Cref_Rates_R_Fund_Invrse_Lst_Pd_Dte; }

    public DbsField getOld_Cref_Rates_R_Cntrct_Ppcn_Nbr() { return old_Cref_Rates_R_Cntrct_Ppcn_Nbr; }

    public DbsField getOld_Cref_Rates_R_Cntrct_Payee_Cde() { return old_Cref_Rates_R_Cntrct_Payee_Cde; }

    public DbsField getOld_Cref_Rates_R_Cmpny_Fund_Cde() { return old_Cref_Rates_R_Cmpny_Fund_Cde; }

    public DbsGroup getOld_Cref_Rates_R_Cmpny_Fund_CdeRedef2() { return old_Cref_Rates_R_Cmpny_Fund_CdeRedef2; }

    public DbsField getOld_Cref_Rates_R_Cmpny_Cde() { return old_Cref_Rates_R_Cmpny_Cde; }

    public DbsField getOld_Cref_Rates_R_Fund_Cde() { return old_Cref_Rates_R_Fund_Cde; }

    public DbsField getOld_Cref_Rates_R_Cntrct_Py_Dte_Key() { return old_Cref_Rates_R_Cntrct_Py_Dte_Key; }

    public DataAccessProgramView getVw_old_Tiaa_Rates() { return vw_old_Tiaa_Rates; }

    public DbsField getOld_Tiaa_Rates_Fund_Lst_Pd_Dte() { return old_Tiaa_Rates_Fund_Lst_Pd_Dte; }

    public DbsField getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte() { return old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte; }

    public DbsField getOld_Tiaa_Rates_Trans_Dte() { return old_Tiaa_Rates_Trans_Dte; }

    public DbsField getOld_Tiaa_Rates_Trans_User_Area() { return old_Tiaa_Rates_Trans_User_Area; }

    public DbsField getOld_Tiaa_Rates_Trans_User_Id() { return old_Tiaa_Rates_Trans_User_Id; }

    public DbsField getOld_Tiaa_Rates_Trans_Verify_Id() { return old_Tiaa_Rates_Trans_Verify_Id; }

    public DbsField getOld_Tiaa_Rates_Trans_Verify_Dte() { return old_Tiaa_Rates_Trans_Verify_Dte; }

    public DbsField getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr() { return old_Tiaa_Rates_Cntrct_Ppcn_Nbr; }

    public DbsField getOld_Tiaa_Rates_Cntrct_Payee_Cde() { return old_Tiaa_Rates_Cntrct_Payee_Cde; }

    public DbsField getOld_Tiaa_Rates_Cntrct_Mode_Ind() { return old_Tiaa_Rates_Cntrct_Mode_Ind; }

    public DbsField getOld_Tiaa_Rates_Cmpny_Fund_Cde() { return old_Tiaa_Rates_Cmpny_Fund_Cde; }

    public DbsGroup getOld_Tiaa_Rates_Cmpny_Fund_CdeRedef3() { return old_Tiaa_Rates_Cmpny_Fund_CdeRedef3; }

    public DbsField getOld_Tiaa_Rates_Cmpny_Cde() { return old_Tiaa_Rates_Cmpny_Cde; }

    public DbsField getOld_Tiaa_Rates_Fund_Cde() { return old_Tiaa_Rates_Fund_Cde; }

    public DbsField getOld_Tiaa_Rates_Rcrd_Srce() { return old_Tiaa_Rates_Rcrd_Srce; }

    public DbsField getOld_Tiaa_Rates_Rcrd_Status() { return old_Tiaa_Rates_Rcrd_Status; }

    public DbsField getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt() { return old_Tiaa_Rates_Cntrct_Tot_Per_Amt; }

    public DbsField getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt() { return old_Tiaa_Rates_Cntrct_Tot_Div_Amt; }

    public DbsField getOld_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp() { return old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getOld_Tiaa_Rates_Tiaa_Rate_Data_Grp() { return old_Tiaa_Rates_Tiaa_Rate_Data_Grp; }

    public DbsField getOld_Tiaa_Rates_Tiaa_Rate_Cde() { return old_Tiaa_Rates_Tiaa_Rate_Cde; }

    public DbsField getOld_Tiaa_Rates_Tiaa_Rate_Dte() { return old_Tiaa_Rates_Tiaa_Rate_Dte; }

    public DbsField getOld_Tiaa_Rates_Tiaa_Per_Pay_Amt() { return old_Tiaa_Rates_Tiaa_Per_Pay_Amt; }

    public DbsField getOld_Tiaa_Rates_Tiaa_Per_Div_Amt() { return old_Tiaa_Rates_Tiaa_Per_Div_Amt; }

    public DbsField getOld_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt() { return old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getOld_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt() { return old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getOld_Tiaa_Rates_Lst_Trans_Dte() { return old_Tiaa_Rates_Lst_Trans_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_old_Cref_Rates_S = new DataAccessProgramView(new NameInfo("vw_old_Cref_Rates_S", "OLD-CREF-RATES-S"), "IAA_OLD_CREF_RATES", "IA_OLD_RATES", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_CREF_RATES"));
        old_Cref_Rates_S_Fund_Lst_Pd_Dte = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        old_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        old_Cref_Rates_S_Trans_Dte = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        old_Cref_Rates_S_Trans_User_Area = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        old_Cref_Rates_S_Trans_User_Id = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        old_Cref_Rates_S_Trans_Verify_Id = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        old_Cref_Rates_S_Trans_Verify_Dte = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Trans_Verify_Dte", "TRANS-VERIFY-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        old_Cref_Rates_S_Cntrct_Ppcn_Nbr = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        old_Cref_Rates_S_Cntrct_Payee_Cde = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        old_Cref_Rates_S_Cntrct_Mode_Ind = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        old_Cref_Rates_S_Rcrd_Srce = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "RCRD_SRCE");
        old_Cref_Rates_S_Rcrd_Status = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_STATUS");
        old_Cref_Rates_S_Cmpny_Fund_Cde = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");
        old_Cref_Rates_S_Cmpny_Fund_CdeRedef1 = vw_old_Cref_Rates_S.getRecord().newGroupInGroup("old_Cref_Rates_S_Cmpny_Fund_CdeRedef1", "Redefines", 
            old_Cref_Rates_S_Cmpny_Fund_Cde);
        old_Cref_Rates_S_Cmpny_Cde = old_Cref_Rates_S_Cmpny_Fund_CdeRedef1.newFieldInGroup("old_Cref_Rates_S_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 
            1);
        old_Cref_Rates_S_Fund_Cde = old_Cref_Rates_S_Cmpny_Fund_CdeRedef1.newFieldInGroup("old_Cref_Rates_S_Fund_Cde", "FUND-CDE", FieldType.STRING, 2);
        old_Cref_Rates_S_Cntrct_Tot_Per_Amt = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        old_Cref_Rates_S_Cntrct_Unit_Val = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Cntrct_Unit_Val", "CNTRCT-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        old_Cref_Rates_S_Cntrct_Tot_Units = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Cntrct_Tot_Units", "CNTRCT-TOT-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3, RepeatingFieldStrategy.None, "CNTRCT_TOT_UNITS");
        old_Cref_Rates_S_Lst_Trans_Dte = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        old_Cref_Rates_S_Count_Castcref_Rate_Data_Grp = vw_old_Cref_Rates_S.getRecord().newFieldInGroup("old_Cref_Rates_S_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Cref_Rates_S_Cref_Rate_Data_Grp = vw_old_Cref_Rates_S.getRecord().newGroupInGroup("old_Cref_Rates_S_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Cref_Rates_S_Cref_Rate_Cde = old_Cref_Rates_S_Cref_Rate_Data_Grp.newFieldArrayInGroup("old_Cref_Rates_S_Cref_Rate_Cde", "CREF-RATE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Cref_Rates_S_Cref_Rate_Dte = old_Cref_Rates_S_Cref_Rate_Data_Grp.newFieldArrayInGroup("old_Cref_Rates_S_Cref_Rate_Dte", "CREF-RATE-DTE", FieldType.DATE, 
            new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Cref_Rates_S_Cref_Per_Pay_Amt = old_Cref_Rates_S_Cref_Rate_Data_Grp.newFieldArrayInGroup("old_Cref_Rates_S_Cref_Per_Pay_Amt", "CREF-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Cref_Rates_S_Cref_No_Units = old_Cref_Rates_S_Cref_Rate_Data_Grp.newFieldArrayInGroup("old_Cref_Rates_S_Cref_No_Units", "CREF-NO-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_NO_UNITS", "IA_OLD_RATES_CREF_RATE_DATA_GRP");

        vw_old_Cref_Rates_R = new DataAccessProgramView(new NameInfo("vw_old_Cref_Rates_R", "OLD-CREF-RATES-R"), "IAA_OLD_CREF_RATES", "IA_OLD_RATES");
        old_Cref_Rates_R_Fund_Invrse_Lst_Pd_Dte = vw_old_Cref_Rates_R.getRecord().newFieldInGroup("old_Cref_Rates_R_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        old_Cref_Rates_R_Cntrct_Ppcn_Nbr = vw_old_Cref_Rates_R.getRecord().newFieldInGroup("old_Cref_Rates_R_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        old_Cref_Rates_R_Cntrct_Payee_Cde = vw_old_Cref_Rates_R.getRecord().newFieldInGroup("old_Cref_Rates_R_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        old_Cref_Rates_R_Cmpny_Fund_Cde = vw_old_Cref_Rates_R.getRecord().newFieldInGroup("old_Cref_Rates_R_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");
        old_Cref_Rates_R_Cmpny_Fund_CdeRedef2 = vw_old_Cref_Rates_R.getRecord().newGroupInGroup("old_Cref_Rates_R_Cmpny_Fund_CdeRedef2", "Redefines", 
            old_Cref_Rates_R_Cmpny_Fund_Cde);
        old_Cref_Rates_R_Cmpny_Cde = old_Cref_Rates_R_Cmpny_Fund_CdeRedef2.newFieldInGroup("old_Cref_Rates_R_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 
            1);
        old_Cref_Rates_R_Fund_Cde = old_Cref_Rates_R_Cmpny_Fund_CdeRedef2.newFieldInGroup("old_Cref_Rates_R_Fund_Cde", "FUND-CDE", FieldType.STRING, 2);
        old_Cref_Rates_R_Cntrct_Py_Dte_Key = vw_old_Cref_Rates_R.getRecord().newFieldInGroup("old_Cref_Rates_R_Cntrct_Py_Dte_Key", "CNTRCT-PY-DTE-KEY", 
            FieldType.STRING, 23, RepeatingFieldStrategy.None, "CNTRCT_PY_DTE_KEY");

        vw_old_Tiaa_Rates = new DataAccessProgramView(new NameInfo("vw_old_Tiaa_Rates", "OLD-TIAA-RATES"), "IAA_OLD_TIAA_RATES", "IA_OLD_RATES", DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_TIAA_RATES"));
        old_Tiaa_Rates_Fund_Lst_Pd_Dte = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        old_Tiaa_Rates_Trans_Dte = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        old_Tiaa_Rates_Trans_User_Area = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        old_Tiaa_Rates_Trans_User_Id = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        old_Tiaa_Rates_Trans_Verify_Id = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        old_Tiaa_Rates_Trans_Verify_Dte = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Trans_Verify_Dte", "TRANS-VERIFY-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        old_Tiaa_Rates_Cntrct_Ppcn_Nbr = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        old_Tiaa_Rates_Cntrct_Payee_Cde = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        old_Tiaa_Rates_Cntrct_Mode_Ind = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        old_Tiaa_Rates_Cmpny_Fund_Cde = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");
        old_Tiaa_Rates_Cmpny_Fund_CdeRedef3 = vw_old_Tiaa_Rates.getRecord().newGroupInGroup("old_Tiaa_Rates_Cmpny_Fund_CdeRedef3", "Redefines", old_Tiaa_Rates_Cmpny_Fund_Cde);
        old_Tiaa_Rates_Cmpny_Cde = old_Tiaa_Rates_Cmpny_Fund_CdeRedef3.newFieldInGroup("old_Tiaa_Rates_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 1);
        old_Tiaa_Rates_Fund_Cde = old_Tiaa_Rates_Cmpny_Fund_CdeRedef3.newFieldInGroup("old_Tiaa_Rates_Fund_Cde", "FUND-CDE", FieldType.STRING, 2);
        old_Tiaa_Rates_Rcrd_Srce = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "RCRD_SRCE");
        old_Tiaa_Rates_Rcrd_Status = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_STATUS");
        old_Tiaa_Rates_Cntrct_Tot_Per_Amt = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        old_Tiaa_Rates_Cntrct_Tot_Div_Amt = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Cntrct_Tot_Div_Amt", "CNTRCT-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp", "C*TIAA-RATE-DATA-GRP", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_OLD_RATES_TIAA_RATE_DATA_GRP");
        old_Tiaa_Rates_Tiaa_Rate_Data_Grp = vw_old_Tiaa_Rates.getRecord().newGroupInGroup("old_Tiaa_Rates_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Tiaa_Rates_Tiaa_Rate_Cde = old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("old_Tiaa_Rates_Tiaa_Rate_Cde", "TIAA-RATE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Tiaa_Rates_Tiaa_Rate_Dte = old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("old_Tiaa_Rates_Tiaa_Rate_Dte", "TIAA-RATE-DTE", FieldType.DATE, 
            new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Tiaa_Rates_Tiaa_Per_Pay_Amt = old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("old_Tiaa_Rates_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Tiaa_Rates_Tiaa_Per_Div_Amt = old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("old_Tiaa_Rates_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt = old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt = old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt", "TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        old_Tiaa_Rates_Lst_Trans_Dte = vw_old_Tiaa_Rates.getRecord().newFieldInGroup("old_Tiaa_Rates_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        vw_old_Cref_Rates_S.setUniquePeList();
        vw_old_Tiaa_Rates.setUniquePeList();

        this.setRecordName("LdaIaal163b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_old_Cref_Rates_S.reset();
        vw_old_Cref_Rates_R.reset();
        vw_old_Tiaa_Rates.reset();
    }

    // Constructor
    public LdaIaal163b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
