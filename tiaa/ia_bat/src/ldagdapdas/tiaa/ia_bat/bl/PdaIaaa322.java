/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:39 PM
**        * FROM NATURAL PDA     : IAAA322
************************************************************
**        * FILE NAME            : PdaIaaa322.java
**        * CLASS NAME           : PdaIaaa322
**        * INSTANCE NAME        : PdaIaaa322
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIaaa322 extends PdaBase
{
    // Properties
    private DbsGroup iaaa322_Data;
    private DbsField iaaa322_Data_Iaaa322_Rec_Id;
    private DbsField iaaa322_Data_Iaaa322_Data_Lgth;
    private DbsField iaaa322_Data_Iaaa322_Data_Occurs;
    private DbsField iaaa322_Data_Iaaa322_Data_Array;
    private DbsGroup iaaa322_Data_Iaaa322_Data_ArrayRedef1;
    private DbsGroup iaaa322_Data_Psta3005_Tax;
    private DbsField iaaa322_Data_Tax_Addr_Ltr;
    private DbsField iaaa322_Data_From_State_Nme;
    private DbsField iaaa322_Data_To_State_Nme;
    private DbsField iaaa322_Data_Cntrct_Desc;
    private DbsField iaaa322_Data_C_Contract_Sets;
    private DbsGroup iaaa322_Data_Psta3005_Set_Tax;
    private DbsField iaaa322_Data_C_Contracts;
    private DbsField iaaa322_Data_Cntrct_Nbr;
    private DbsField iaaa322_Data_Cntrct_Company_Cde;
    private DbsField iaaa322_Data_Addrss_Lne;
    private DbsField iaaa322_Data_Addrss_Type_Cde;
    private DbsField iaaa322_Data_Addrss_State_Cde;
    private DbsField iaaa322_Data_Addrss_Postal_Data;
    private DbsGroup iaaa322_Data_Psta3006_Tax;
    private DbsGroup iaaa322_Data_User_Selected_Options;
    private DbsField iaaa322_Data_State_Form_Req;
    private DbsField iaaa322_Data_Prefill_State;
    private DbsGroup iaaa322_Data_Mandatory_Tax_Form_Indicators;
    private DbsField iaaa322_Data_Rollover_Ind;
    private DbsField iaaa322_Data_Periodic_Ind;

    public DbsGroup getIaaa322_Data() { return iaaa322_Data; }

    public DbsField getIaaa322_Data_Iaaa322_Rec_Id() { return iaaa322_Data_Iaaa322_Rec_Id; }

    public DbsField getIaaa322_Data_Iaaa322_Data_Lgth() { return iaaa322_Data_Iaaa322_Data_Lgth; }

    public DbsField getIaaa322_Data_Iaaa322_Data_Occurs() { return iaaa322_Data_Iaaa322_Data_Occurs; }

    public DbsField getIaaa322_Data_Iaaa322_Data_Array() { return iaaa322_Data_Iaaa322_Data_Array; }

    public DbsGroup getIaaa322_Data_Iaaa322_Data_ArrayRedef1() { return iaaa322_Data_Iaaa322_Data_ArrayRedef1; }

    public DbsGroup getIaaa322_Data_Psta3005_Tax() { return iaaa322_Data_Psta3005_Tax; }

    public DbsField getIaaa322_Data_Tax_Addr_Ltr() { return iaaa322_Data_Tax_Addr_Ltr; }

    public DbsField getIaaa322_Data_From_State_Nme() { return iaaa322_Data_From_State_Nme; }

    public DbsField getIaaa322_Data_To_State_Nme() { return iaaa322_Data_To_State_Nme; }

    public DbsField getIaaa322_Data_Cntrct_Desc() { return iaaa322_Data_Cntrct_Desc; }

    public DbsField getIaaa322_Data_C_Contract_Sets() { return iaaa322_Data_C_Contract_Sets; }

    public DbsGroup getIaaa322_Data_Psta3005_Set_Tax() { return iaaa322_Data_Psta3005_Set_Tax; }

    public DbsField getIaaa322_Data_C_Contracts() { return iaaa322_Data_C_Contracts; }

    public DbsField getIaaa322_Data_Cntrct_Nbr() { return iaaa322_Data_Cntrct_Nbr; }

    public DbsField getIaaa322_Data_Cntrct_Company_Cde() { return iaaa322_Data_Cntrct_Company_Cde; }

    public DbsField getIaaa322_Data_Addrss_Lne() { return iaaa322_Data_Addrss_Lne; }

    public DbsField getIaaa322_Data_Addrss_Type_Cde() { return iaaa322_Data_Addrss_Type_Cde; }

    public DbsField getIaaa322_Data_Addrss_State_Cde() { return iaaa322_Data_Addrss_State_Cde; }

    public DbsField getIaaa322_Data_Addrss_Postal_Data() { return iaaa322_Data_Addrss_Postal_Data; }

    public DbsGroup getIaaa322_Data_Psta3006_Tax() { return iaaa322_Data_Psta3006_Tax; }

    public DbsGroup getIaaa322_Data_User_Selected_Options() { return iaaa322_Data_User_Selected_Options; }

    public DbsField getIaaa322_Data_State_Form_Req() { return iaaa322_Data_State_Form_Req; }

    public DbsField getIaaa322_Data_Prefill_State() { return iaaa322_Data_Prefill_State; }

    public DbsGroup getIaaa322_Data_Mandatory_Tax_Form_Indicators() { return iaaa322_Data_Mandatory_Tax_Form_Indicators; }

    public DbsField getIaaa322_Data_Rollover_Ind() { return iaaa322_Data_Rollover_Ind; }

    public DbsField getIaaa322_Data_Periodic_Ind() { return iaaa322_Data_Periodic_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaaa322_Data = dbsRecord.newGroupInRecord("iaaa322_Data", "IAAA322-DATA");
        iaaa322_Data.setParameterOption(ParameterOption.ByReference);
        iaaa322_Data_Iaaa322_Rec_Id = iaaa322_Data.newFieldInGroup("iaaa322_Data_Iaaa322_Rec_Id", "IAAA322-REC-ID", FieldType.STRING, 2);
        iaaa322_Data_Iaaa322_Data_Lgth = iaaa322_Data.newFieldInGroup("iaaa322_Data_Iaaa322_Data_Lgth", "IAAA322-DATA-LGTH", FieldType.NUMERIC, 5);
        iaaa322_Data_Iaaa322_Data_Occurs = iaaa322_Data.newFieldInGroup("iaaa322_Data_Iaaa322_Data_Occurs", "IAAA322-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        iaaa322_Data_Iaaa322_Data_Array = iaaa322_Data.newFieldArrayInGroup("iaaa322_Data_Iaaa322_Data_Array", "IAAA322-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,871));
        iaaa322_Data_Iaaa322_Data_ArrayRedef1 = iaaa322_Data.newGroupInGroup("iaaa322_Data_Iaaa322_Data_ArrayRedef1", "Redefines", iaaa322_Data_Iaaa322_Data_Array);
        iaaa322_Data_Psta3005_Tax = iaaa322_Data_Iaaa322_Data_ArrayRedef1.newGroupInGroup("iaaa322_Data_Psta3005_Tax", "PSTA3005-TAX");
        iaaa322_Data_Tax_Addr_Ltr = iaaa322_Data_Psta3005_Tax.newFieldInGroup("iaaa322_Data_Tax_Addr_Ltr", "TAX-ADDR-LTR", FieldType.STRING, 1);
        iaaa322_Data_From_State_Nme = iaaa322_Data_Psta3005_Tax.newFieldInGroup("iaaa322_Data_From_State_Nme", "FROM-STATE-NME", FieldType.STRING, 14);
        iaaa322_Data_To_State_Nme = iaaa322_Data_Psta3005_Tax.newFieldInGroup("iaaa322_Data_To_State_Nme", "TO-STATE-NME", FieldType.STRING, 14);
        iaaa322_Data_Cntrct_Desc = iaaa322_Data_Psta3005_Tax.newFieldInGroup("iaaa322_Data_Cntrct_Desc", "CNTRCT-DESC", FieldType.STRING, 21);
        iaaa322_Data_C_Contract_Sets = iaaa322_Data_Psta3005_Tax.newFieldInGroup("iaaa322_Data_C_Contract_Sets", "C-CONTRACT-SETS", FieldType.NUMERIC, 
            2);
        iaaa322_Data_Psta3005_Set_Tax = iaaa322_Data_Iaaa322_Data_ArrayRedef1.newGroupInGroup("iaaa322_Data_Psta3005_Set_Tax", "PSTA3005-SET-TAX");
        iaaa322_Data_C_Contracts = iaaa322_Data_Psta3005_Set_Tax.newFieldInGroup("iaaa322_Data_C_Contracts", "C-CONTRACTS", FieldType.NUMERIC, 2);
        iaaa322_Data_Cntrct_Nbr = iaaa322_Data_Psta3005_Set_Tax.newFieldArrayInGroup("iaaa322_Data_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 9, new 
            DbsArrayController(1,10));
        iaaa322_Data_Cntrct_Company_Cde = iaaa322_Data_Psta3005_Set_Tax.newFieldArrayInGroup("iaaa322_Data_Cntrct_Company_Cde", "CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1,10));
        iaaa322_Data_Addrss_Lne = iaaa322_Data_Psta3005_Set_Tax.newFieldArrayInGroup("iaaa322_Data_Addrss_Lne", "ADDRSS-LNE", FieldType.STRING, 35, new 
            DbsArrayController(1,6));
        iaaa322_Data_Addrss_Type_Cde = iaaa322_Data_Psta3005_Set_Tax.newFieldInGroup("iaaa322_Data_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        iaaa322_Data_Addrss_State_Cde = iaaa322_Data_Psta3005_Set_Tax.newFieldInGroup("iaaa322_Data_Addrss_State_Cde", "ADDRSS-STATE-CDE", FieldType.STRING, 
            2);
        iaaa322_Data_Addrss_Postal_Data = iaaa322_Data_Psta3005_Set_Tax.newFieldInGroup("iaaa322_Data_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);
        iaaa322_Data_Psta3006_Tax = iaaa322_Data_Iaaa322_Data_ArrayRedef1.newGroupInGroup("iaaa322_Data_Psta3006_Tax", "PSTA3006-TAX");
        iaaa322_Data_User_Selected_Options = iaaa322_Data_Psta3006_Tax.newGroupInGroup("iaaa322_Data_User_Selected_Options", "USER-SELECTED-OPTIONS");
        iaaa322_Data_State_Form_Req = iaaa322_Data_User_Selected_Options.newFieldInGroup("iaaa322_Data_State_Form_Req", "STATE-FORM-REQ", FieldType.BOOLEAN);
        iaaa322_Data_Prefill_State = iaaa322_Data_User_Selected_Options.newFieldInGroup("iaaa322_Data_Prefill_State", "PREFILL-STATE", FieldType.BOOLEAN);
        iaaa322_Data_Mandatory_Tax_Form_Indicators = iaaa322_Data_Iaaa322_Data_ArrayRedef1.newGroupInGroup("iaaa322_Data_Mandatory_Tax_Form_Indicators", 
            "MANDATORY-TAX-FORM-INDICATORS");
        iaaa322_Data_Rollover_Ind = iaaa322_Data_Mandatory_Tax_Form_Indicators.newFieldInGroup("iaaa322_Data_Rollover_Ind", "ROLLOVER-IND", FieldType.STRING, 
            1);
        iaaa322_Data_Periodic_Ind = iaaa322_Data_Mandatory_Tax_Form_Indicators.newFieldInGroup("iaaa322_Data_Periodic_Ind", "PERIODIC-IND", FieldType.STRING, 
            1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIaaa322(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

