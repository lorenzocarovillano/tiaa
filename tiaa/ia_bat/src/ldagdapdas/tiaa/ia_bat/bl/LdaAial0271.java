/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:30 PM
**        * FROM NATURAL LDA     : AIAL0271
************************************************************
**        * FILE NAME            : LdaAial0271.java
**        * CLASS NAME           : LdaAial0271
**        * INSTANCE NAME        : LdaAial0271
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0271 extends DbsRecord
{
    // Properties
    private DbsGroup wtc_Tiaacref_Payout_Record;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Record_Keys;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_1;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Date;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_DateRedef2;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Century;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Year;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Month;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Day;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_2;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Da_Rate;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_3;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef3;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Ia_Rate;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_4;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_5;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_6;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_7;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef4;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Century;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Year;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Month;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Day;
    private DbsField wtc_Tiaacref_Payout_Record_Fillers_1;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_N;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_NRedef5;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Day;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_8;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef6;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Monthly_Control_Area;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Current_Ia_Rate;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_9;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef7;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Ia_Rate;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_10;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Code;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_11;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Setback;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_12;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_13;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Code;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_14;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Setback;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_15;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_16;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Standard_Total;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Code;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_17;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Setback;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_18;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_19;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Graded_Total;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Code;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_20;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Setback;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_21;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_22;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Annuity_Certain_Total;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Ac_Tiaa_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_23;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Pricipal_And_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Pi_Tiaa_Interest;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_24;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Ac_Ia_Rate;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_35;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef8;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Century;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Year;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Month;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Day;
    private DbsField wtc_Tiaacref_Payout_Record_Filler2;
    private DbsGroup wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Century;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Year;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Month;
    private DbsField wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Day;
    private DbsField wtc_Tiaacref_Payout_Record_Filler_25;

    public DbsGroup getWtc_Tiaacref_Payout_Record() { return wtc_Tiaacref_Payout_Record; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Record_Keys() { return wtc_Tiaacref_Payout_Record_Wtc_Record_Keys; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1() { return wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator() { return wtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_1() { return wtc_Tiaacref_Payout_Record_Filler_1; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Date() { return wtc_Tiaacref_Payout_Record_Wtc_Date; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_DateRedef2() { return wtc_Tiaacref_Payout_Record_Wtc_DateRedef2; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Century() { return wtc_Tiaacref_Payout_Record_Wtc_Century; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Year() { return wtc_Tiaacref_Payout_Record_Wtc_Year; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Month() { return wtc_Tiaacref_Payout_Record_Wtc_Month; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Day() { return wtc_Tiaacref_Payout_Record_Wtc_Day; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_2() { return wtc_Tiaacref_Payout_Record_Filler_2; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Da_Rate() { return wtc_Tiaacref_Payout_Record_Wtc_Da_Rate; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_3() { return wtc_Tiaacref_Payout_Record_Filler_3; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef3() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef3; 
        }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Ia_Rate() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Ia_Rate; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_4() { return wtc_Tiaacref_Payout_Record_Filler_4; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_5() { return wtc_Tiaacref_Payout_Record_Filler_5; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_6() { return wtc_Tiaacref_Payout_Record_Filler_6; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_7() { return wtc_Tiaacref_Payout_Record_Filler_7; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef4() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef4; 
        }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Century() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Century; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Year() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Year; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Month() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Month; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Day() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Day; }

    public DbsField getWtc_Tiaacref_Payout_Record_Fillers_1() { return wtc_Tiaacref_Payout_Record_Fillers_1; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_N() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_N; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_NRedef5() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_NRedef5; 
        }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Cref_End_Day() { return wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Day; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_8() { return wtc_Tiaacref_Payout_Record_Filler_8; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef6() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef6; 
        }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Monthly_Control_Area() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Monthly_Control_Area; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Current_Ia_Rate() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Current_Ia_Rate; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_9() { return wtc_Tiaacref_Payout_Record_Filler_9; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef7() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef7; 
        }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Ia_Rate() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Ia_Rate; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_10() { return wtc_Tiaacref_Payout_Record_Filler_10; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee() { return wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Code() { return wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Code; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_11() { return wtc_Tiaacref_Payout_Record_Filler_11; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Setback() { return wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Setback; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_12() { return wtc_Tiaacref_Payout_Record_Filler_12; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_13() { return wtc_Tiaacref_Payout_Record_Filler_13; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee() { return wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Code() { return wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Code; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_14() { return wtc_Tiaacref_Payout_Record_Filler_14; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Setback() { return wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Setback; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_15() { return wtc_Tiaacref_Payout_Record_Filler_15; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_16() { return wtc_Tiaacref_Payout_Record_Filler_16; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Standard_Total() { return wtc_Tiaacref_Payout_Record_Wtc_Standard_Total; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Code() { return wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Code; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_17() { return wtc_Tiaacref_Payout_Record_Filler_17; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Setback() { return wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Setback; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_18() { return wtc_Tiaacref_Payout_Record_Filler_18; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_19() { return wtc_Tiaacref_Payout_Record_Filler_19; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Graded_Total() { return wtc_Tiaacref_Payout_Record_Wtc_Graded_Total; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Code() { return wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Code; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_20() { return wtc_Tiaacref_Payout_Record_Filler_20; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Setback() { return wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Setback; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_21() { return wtc_Tiaacref_Payout_Record_Filler_21; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_22() { return wtc_Tiaacref_Payout_Record_Filler_22; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Annuity_Certain_Total() { return wtc_Tiaacref_Payout_Record_Wtc_Annuity_Certain_Total; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Ac_Tiaa_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_Ac_Tiaa_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_23() { return wtc_Tiaacref_Payout_Record_Filler_23; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Pricipal_And_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_Pricipal_And_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Pi_Tiaa_Interest() { return wtc_Tiaacref_Payout_Record_Wtc_Pi_Tiaa_Interest; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_24() { return wtc_Tiaacref_Payout_Record_Filler_24; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Ac_Ia_Rate() { return wtc_Tiaacref_Payout_Record_Wtc_Ac_Ia_Rate; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_35() { return wtc_Tiaacref_Payout_Record_Filler_35; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef8() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef8; 
        }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Century() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Century; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Year() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Year; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Month() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Month; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Day() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Day; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler2() { return wtc_Tiaacref_Payout_Record_Filler2; }

    public DbsGroup getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Century() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Century; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Year() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Year; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Month() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Month; }

    public DbsField getWtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Day() { return wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Day; }

    public DbsField getWtc_Tiaacref_Payout_Record_Filler_25() { return wtc_Tiaacref_Payout_Record_Filler_25; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        wtc_Tiaacref_Payout_Record = newGroupInRecord("wtc_Tiaacref_Payout_Record", "WTC-TIAACREF-PAYOUT-RECORD");
        wtc_Tiaacref_Payout_Record_Wtc_Record_Keys = wtc_Tiaacref_Payout_Record.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Record_Keys", "WTC-RECORD-KEYS", 
            FieldType.STRING, 13);
        wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1 = wtc_Tiaacref_Payout_Record.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Record_Keys);
        wtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator = wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Fund_Indicator", 
            "WTC-FUND-INDICATOR", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Filler_1 = wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_1", 
            "FILLER-1", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Date = wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Date", 
            "WTC-DATE", FieldType.NUMERIC, 8);
        wtc_Tiaacref_Payout_Record_Wtc_DateRedef2 = wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_DateRedef2", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Date);
        wtc_Tiaacref_Payout_Record_Wtc_Century = wtc_Tiaacref_Payout_Record_Wtc_DateRedef2.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Century", "WTC-CENTURY", 
            FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Year = wtc_Tiaacref_Payout_Record_Wtc_DateRedef2.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Year", "WTC-YEAR", 
            FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Month = wtc_Tiaacref_Payout_Record_Wtc_DateRedef2.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Month", "WTC-MONTH", 
            FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Day = wtc_Tiaacref_Payout_Record_Wtc_DateRedef2.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Day", "WTC-DAY", 
            FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_2 = wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_2", 
            "FILLER-2", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Da_Rate = wtc_Tiaacref_Payout_Record_Wtc_Record_KeysRedef1.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Da_Rate", 
            "WTC-DA-RATE", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_3 = wtc_Tiaacref_Payout_Record.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_3", "FILLER-3", FieldType.STRING, 
            1);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas = wtc_Tiaacref_Payout_Record.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas", 
            "WTC-TIAACREF-PAYOUT-AREAS", FieldType.STRING, 106);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef3 = wtc_Tiaacref_Payout_Record.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef3", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area = wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef3.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area", 
            "WTC-CREF-PAYOUT-AREA");
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Ia_Rate = wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Ia_Rate", 
            "WTC-CREF-IA-RATE", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_4 = wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_4", "FILLER-4", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code = wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Code", 
            "WTC-CREF-MORT-TABLE-CODE", FieldType.NUMERIC, 4);
        wtc_Tiaacref_Payout_Record_Filler_5 = wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_5", "FILLER-5", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback = wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Setback", 
            "WTC-CREF-MORT-TABLE-SETBACK", FieldType.DECIMAL, 7,5);
        wtc_Tiaacref_Payout_Record_Filler_6 = wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_6", "FILLER-6", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest = wtc_Tiaacref_Payout_Record_Wtc_Cref_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Mort_Table_Interest", 
            "WTC-CREF-MORT-TABLE-INTEREST", FieldType.DECIMAL, 5,5);
        wtc_Tiaacref_Payout_Record_Filler_7 = wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef3.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_7", 
            "FILLER-7", FieldType.STRING, 85);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef4 = wtc_Tiaacref_Payout_Record.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef4", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area = wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef4.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area", 
            "WTC-CREF-PRODUCT-CONTROL-AREA");
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date = wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date", 
            "WTC-CREF-SUMMARY-START-DATE");
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Century = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Century", 
            "WTC-CREF-START-CENTURY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Year = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Year", 
            "WTC-CREF-START-YEAR", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Month = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Month", 
            "WTC-CREF-START-MONTH", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Day = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Start_Day", 
            "WTC-CREF-START-DAY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Fillers_1 = wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Fillers_1", 
            "FILLERS-1", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_N = wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_N", 
            "WTC-CREF-SUMMARY-END-DATE-N", FieldType.NUMERIC, 8);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_NRedef5 = wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_NRedef5", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_N);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date_NRedef5.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date", 
            "WTC-CREF-SUMMARY-END-DATE");
        wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Century", 
            "WTC-CREF-END-CENTURY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Year", 
            "WTC-CREF-END-YEAR", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Month", 
            "WTC-CREF-END-MONTH", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Day = wtc_Tiaacref_Payout_Record_Wtc_Cref_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Cref_End_Day", 
            "WTC-CREF-END-DAY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_8 = wtc_Tiaacref_Payout_Record_Wtc_Cref_Product_Control_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_8", 
            "FILLER-8", FieldType.STRING, 89);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef6 = wtc_Tiaacref_Payout_Record.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef6", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Monthly_Control_Area = wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef6.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Monthly_Control_Area", 
            "WTC-TIAA-MONTHLY-CONTROL-AREA");
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Current_Ia_Rate = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Monthly_Control_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Current_Ia_Rate", 
            "WTC-TIAA-CURRENT-IA-RATE", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_9 = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Monthly_Control_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_9", 
            "FILLER-9", FieldType.STRING, 104);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef7 = wtc_Tiaacref_Payout_Record.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef7", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area = wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef7.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area", 
            "WTC-TIAA-PAYOUT-AREA");
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Ia_Rate = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Ia_Rate", 
            "WTC-TIAA-IA-RATE", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_10 = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_10", 
            "FILLER-10", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee", 
            "WTC-STANDARD-GUARANTEE");
        wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Code = wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Code", 
            "WTC-SG-TIAA-MORT-TBL-CODE", FieldType.NUMERIC, 4);
        wtc_Tiaacref_Payout_Record_Filler_11 = wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_11", 
            "FILLER-11", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Setback = wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Setback", 
            "WTC-SG-TIAA-MORT-TBL-SETBACK", FieldType.DECIMAL, 7,5);
        wtc_Tiaacref_Payout_Record_Filler_12 = wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_12", 
            "FILLER-12", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Interest = wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Sg_Tiaa_Mort_Tbl_Interest", 
            "WTC-SG-TIAA-MORT-TBL-INTEREST", FieldType.DECIMAL, 5,5);
        wtc_Tiaacref_Payout_Record_Filler_13 = wtc_Tiaacref_Payout_Record_Wtc_Standard_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_13", 
            "FILLER-13", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee", 
            "WTC-GRADED-GUARANTEE");
        wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Code = wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Code", 
            "WTC-GG-TIAA-MORT-TBL-CODE", FieldType.NUMERIC, 4);
        wtc_Tiaacref_Payout_Record_Filler_14 = wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_14", 
            "FILLER-14", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Setback = wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Setback", 
            "WTC-GG-TIAA-MORT-TBL-SETBACK", FieldType.DECIMAL, 7,5);
        wtc_Tiaacref_Payout_Record_Filler_15 = wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_15", 
            "FILLER-15", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Interest = wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Gg_Tiaa_Mort_Tbl_Interest", 
            "WTC-GG-TIAA-MORT-TBL-INTEREST", FieldType.DECIMAL, 5,5);
        wtc_Tiaacref_Payout_Record_Filler_16 = wtc_Tiaacref_Payout_Record_Wtc_Graded_Guarantee.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_16", 
            "FILLER-16", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Standard_Total = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Standard_Total", 
            "WTC-STANDARD-TOTAL");
        wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Code = wtc_Tiaacref_Payout_Record_Wtc_Standard_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Code", 
            "WTC-ST-TIAA-MORT-TBL-CODE", FieldType.NUMERIC, 4);
        wtc_Tiaacref_Payout_Record_Filler_17 = wtc_Tiaacref_Payout_Record_Wtc_Standard_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_17", "FILLER-17", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Setback = wtc_Tiaacref_Payout_Record_Wtc_Standard_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Setback", 
            "WTC-ST-TIAA-MORT-TBL-SETBACK", FieldType.DECIMAL, 7,5);
        wtc_Tiaacref_Payout_Record_Filler_18 = wtc_Tiaacref_Payout_Record_Wtc_Standard_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_18", "FILLER-18", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Interest = wtc_Tiaacref_Payout_Record_Wtc_Standard_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_St_Tiaa_Mort_Tbl_Interest", 
            "WTC-ST-TIAA-MORT-TBL-INTEREST", FieldType.DECIMAL, 5,5);
        wtc_Tiaacref_Payout_Record_Filler_19 = wtc_Tiaacref_Payout_Record_Wtc_Standard_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_19", "FILLER-19", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Graded_Total = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Graded_Total", 
            "WTC-GRADED-TOTAL");
        wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Code = wtc_Tiaacref_Payout_Record_Wtc_Graded_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Code", 
            "WTC-GT-TIAA-MORT-TBL-CODE", FieldType.NUMERIC, 4);
        wtc_Tiaacref_Payout_Record_Filler_20 = wtc_Tiaacref_Payout_Record_Wtc_Graded_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_20", "FILLER-20", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Setback = wtc_Tiaacref_Payout_Record_Wtc_Graded_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Setback", 
            "WTC-GT-TIAA-MORT-TBL-SETBACK", FieldType.DECIMAL, 7,5);
        wtc_Tiaacref_Payout_Record_Filler_21 = wtc_Tiaacref_Payout_Record_Wtc_Graded_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_21", "FILLER-21", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Interest = wtc_Tiaacref_Payout_Record_Wtc_Graded_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Gt_Tiaa_Mort_Tbl_Interest", 
            "WTC-GT-TIAA-MORT-TBL-INTEREST", FieldType.DECIMAL, 5,5);
        wtc_Tiaacref_Payout_Record_Filler_22 = wtc_Tiaacref_Payout_Record_Wtc_Graded_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_22", "FILLER-22", 
            FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Annuity_Certain_Total = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Annuity_Certain_Total", 
            "WTC-ANNUITY-CERTAIN-TOTAL");
        wtc_Tiaacref_Payout_Record_Wtc_Ac_Tiaa_Interest = wtc_Tiaacref_Payout_Record_Wtc_Annuity_Certain_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Ac_Tiaa_Interest", 
            "WTC-AC-TIAA-INTEREST", FieldType.DECIMAL, 5,5);
        wtc_Tiaacref_Payout_Record_Filler_23 = wtc_Tiaacref_Payout_Record_Wtc_Annuity_Certain_Total.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_23", 
            "FILLER-23", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Pricipal_And_Interest = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Pricipal_And_Interest", 
            "WTC-PRICIPAL-AND-INTEREST");
        wtc_Tiaacref_Payout_Record_Wtc_Pi_Tiaa_Interest = wtc_Tiaacref_Payout_Record_Wtc_Pricipal_And_Interest.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Pi_Tiaa_Interest", 
            "WTC-PI-TIAA-INTEREST", FieldType.DECIMAL, 5,5);
        wtc_Tiaacref_Payout_Record_Filler_24 = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_24", 
            "FILLER-24", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Ac_Ia_Rate = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Ac_Ia_Rate", 
            "WTC-AC-IA-RATE", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_35 = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Payout_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_35", 
            "FILLER-35", FieldType.STRING, 13);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef8 = wtc_Tiaacref_Payout_Record.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef8", 
            "Redefines", wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_Areas);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area = wtc_Tiaacref_Payout_Record_Wtc_Tiaacref_Payout_AreasRedef8.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area", 
            "WTC-TIAA-PRODUCT-CONTROL-AREA");
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date", 
            "WTC-TIAA-SUMMARY-START-DATE");
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Century = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Century", 
            "WTC-TIAA-START-CENTURY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Year = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Year", 
            "WTC-TIAA-START-YEAR", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Month = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Month", 
            "WTC-TIAA-START-MONTH", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Day = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_Start_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Start_Day", 
            "WTC-TIAA-START-DAY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler2 = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler2", 
            "FILLER2", FieldType.STRING, 1);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area.newGroupInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date", 
            "WTC-TIAA-SUMMARY-END-DATE");
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Century = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Century", 
            "WTC-TIAA-END-CENTURY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Year = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Year", 
            "WTC-TIAA-END-YEAR", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Month = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Month", 
            "WTC-TIAA-END-MONTH", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Day = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Summary_End_Date.newFieldInGroup("wtc_Tiaacref_Payout_Record_Wtc_Tiaa_End_Day", 
            "WTC-TIAA-END-DAY", FieldType.NUMERIC, 2);
        wtc_Tiaacref_Payout_Record_Filler_25 = wtc_Tiaacref_Payout_Record_Wtc_Tiaa_Product_Control_Area.newFieldInGroup("wtc_Tiaacref_Payout_Record_Filler_25", 
            "FILLER-25", FieldType.STRING, 89);

        this.setRecordName("LdaAial0271");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAial0271() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
