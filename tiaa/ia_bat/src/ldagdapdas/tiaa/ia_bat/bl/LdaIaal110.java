/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:13 PM
**        * FROM NATURAL LDA     : IAAL110
************************************************************
**        * FILE NAME            : LdaIaal110.java
**        * CLASS NAME           : LdaIaal110
**        * INSTANCE NAME        : LdaIaal110
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal110 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot;
    private DbsGroup pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde;
    private DbsGroup pnd_Input_Record_Pnd_I_Tiaa_Fund_CdeRedef2;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr;
    private DbsGroup pnd_Input_Record_Pnd_I_Cnt_NbrRedef3;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr_8;
    private DbsGroup pnd_Input_Record_Pnd_I_Cnt_NbrRedef4;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr_2;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Payee_Cde;
    private DbsGroup pnd_Input_Record_Pnd_I_Cnt_Payee_CdeRedef5;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Actvty_Cde;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Issue_Dte;
    private DbsGroup pnd_Input_Record_Pnd_I_Cnt_Issue_DteRedef6;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Mode_Ind;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde;
    private DbsField pnd_Input_Record_Pnd_I_Ddctn_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Cntrct_Optn_Code;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_T;
    private DbsField pnd_Cnt_Payee_Field;
    private DbsField pnd_Final_Pay_Flag;
    private DbsField pnd_Fmt3_Mode_Sub;
    private DbsField pnd_Fmt4_Cnt_Amt_Tot;
    private DbsField pnd_Fmt4_Per_Div_Tot;
    private DbsField pnd_Fmt3_Tiaa_Table_Tot;
    private DbsField pnd_Fmt3_Tiaa_Table;
    private DbsField pnd_Fmt5_Tiaa_Table_Tot;
    private DbsField pnd_Fmt5_Tiaa_Table;
    private DbsField pnd_Fmt6_Tiaa_Table_Tot;
    private DbsField pnd_Fmt6_Tiaa_Table;
    private DbsField pnd_Fmt7_Tiaa_Table_Tot;
    private DbsField pnd_Fmt7_Tiaa_Table;
    private DbsField pnd_Fmt8_Tiaa_Table_Tot;
    private DbsField pnd_Fmt8_Tiaa_Table;
    private DbsField pnd_Fmt9_Tiaa_Table_Tot;
    private DbsField pnd_Fmt9_Tiaa_Table;
    private DbsField pnd_Fmt10_Tiaa_Table_Tot;
    private DbsField pnd_Fmt10_Tiaa_Table;
    private DbsField pnd_Fmt11_Tiaa_Table_Tot;
    private DbsField pnd_Fmt11_Tiaa_Table;
    private DbsField pnd_Fmt3_Units_Tot;
    private DbsField pnd_Fmt3_Units;
    private DbsField pnd_Fmt3_Dollars;
    private DbsField pnd_Fmt3_Dollars_Tot;
    private DbsField pnd_Fmt3_Deductions;
    private DbsField pnd_Fmt5_Units_Tot;
    private DbsField pnd_Fmt5_Units;
    private DbsField pnd_Fmt5_Dollars;
    private DbsField pnd_Fmt5_Dollars_Tot;
    private DbsField pnd_Fmt5_Deductions;
    private DbsField pnd_Fmt6_Units_Tot;
    private DbsField pnd_Fmt6_Units;
    private DbsField pnd_Fmt6_Dollars;
    private DbsField pnd_Fmt6_Dollars_Tot;
    private DbsField pnd_Fmt6_Deductions;
    private DbsField pnd_Fmt7_Units_Tot;
    private DbsField pnd_Fmt7_Units;
    private DbsField pnd_Fmt7_Dollars;
    private DbsField pnd_Fmt7_Dollars_Tot;
    private DbsField pnd_Fmt7_Deductions;
    private DbsField pnd_Generic_Unit_Value;
    private DbsField pnd_Generic_Dollar_Amount;
    private DbsField pnd_Fund_Types;
    private DbsField pnd_Tiaa_Title;
    private DbsField pnd_Cref_Title;
    private DbsField pnd_Tiaa_Title_Tab;
    private DbsField pnd_Cref_Title_Tab;
    private DbsField pnd_Mode_1;
    private DbsField pnd_Cnt_Type_Sub;
    private DbsField pnd_Fund_Cde_Hold_Tot;
    private DbsGroup pnd_Fund_Cde_Hold_TotRedef7;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold;
    private DbsGroup pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_HoldRedef8;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2;
    private DbsField pnd_Tiaa_Table_1;
    private DbsField pnd_Tiaa_Table_2;
    private DbsField pnd_Tiaa_Table_3;
    private DbsField pnd_Tiaa_Table_4;
    private DbsField pnd_Tiaa_Table_5;
    private DbsField pnd_Tiaa_Table_6;
    private DbsField pnd_Tiaa_Table_7;
    private DbsField pnd_Tiaa_Table_Tot;
    private DbsField pnd_Tiaa_Table_Tot_4;
    private DbsField pnd_Tiaa_Table_Tot_5;
    private DbsField pnd_Tiaa_Table_Tot_6;
    private DbsField pnd_Tiaa_Table_Tot_7;
    private DbsField pnd_Tiaa_Table_Tot_G;
    private DbsField pnd_Cref_Units_1;
    private DbsField pnd_Cref_Units_2;
    private DbsField pnd_Cref_Units_3;
    private DbsField pnd_Cref_Units_Total;
    private DbsField pnd_Cref_Dollars_1;
    private DbsField pnd_Cref_Dollars_2;
    private DbsField pnd_Cref_Dollars_3;
    private DbsField pnd_Cref_Dollars_Total;
    private DbsField pnd_Cref_Deduct_1;
    private DbsField pnd_Cref_Deduct_2;
    private DbsField pnd_Cref_Deduct_3;
    private DbsField pnd_Cref_Deduct_Total;
    private DbsField pnd_Cref_Units_Tot;
    private DbsField pnd_Cref_Dollars_Tot;
    private DbsField pnd_Cref_Deduct_Tot;
    private DbsField pnd_Mode_Table_2;
    private DbsField pnd_Unit_Text_Table;
    private DbsField pnd_Dollars_Text_Table;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_4;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_5;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_6;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_7;
    private DbsField pnd_Tiaa_Per_Ded_Tot;
    private DbsField pnd_Tiaa_Per_Ded_Tot_4;
    private DbsField pnd_Tiaa_Per_Ded_Tot_5;
    private DbsField pnd_Tiaa_Per_Ded_Tot_6;
    private DbsField pnd_Tiaa_Per_Ded_Tot_7;
    private DbsField pnd_Tiaa_Fin_Pay_Tot;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_4;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_5;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_6;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_7;
    private DbsField pnd_Tiaa_Per_Div_Tot;
    private DbsField pnd_Tiaa_Per_Div_Tot_4;
    private DbsField pnd_Tiaa_Per_Div_Tot_5;
    private DbsField pnd_Tiaa_Per_Div_Tot_6;
    private DbsField pnd_Tiaa_Per_Div_Tot_7;
    private DbsField pnd_Tiaa_Fin_Div_Tot;
    private DbsField pnd_Tiaa_Fin_Div_Tot_4;
    private DbsField pnd_Tiaa_Fin_Div_Tot_5;
    private DbsField pnd_Tiaa_Fin_Div_Tot_6;
    private DbsField pnd_Tiaa_Fin_Div_Tot_7;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_G;
    private DbsField pnd_Tiaa_Per_Ded_Tot_G;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_G;
    private DbsField pnd_Tiaa_Per_Div_Tot_G;
    private DbsField pnd_Tiaa_Fin_Div_Tot_G;
    private DbsField pnd_Pay_Mode_Table;
    private DbsField pnd_W_Rec_3;
    private DbsGroup pnd_W_Rec_3Redef9;
    private DbsField pnd_W_Rec_3_Pnd_W_Rec_3_A;

    public DbsGroup getPnd_Input_Record() { return pnd_Input_Record; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot() { return pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot; }

    public DbsGroup getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1() { return pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde() { return pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde() { return pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde; }

    public DbsGroup getPnd_Input_Record_Pnd_I_Tiaa_Fund_CdeRedef2() { return pnd_Input_Record_Pnd_I_Tiaa_Fund_CdeRedef2; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1() { return pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2() { return pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Nbr() { return pnd_Input_Record_Pnd_I_Cnt_Nbr; }

    public DbsGroup getPnd_Input_Record_Pnd_I_Cnt_NbrRedef3() { return pnd_Input_Record_Pnd_I_Cnt_NbrRedef3; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Nbr_8() { return pnd_Input_Record_Pnd_I_Cnt_Nbr_8; }

    public DbsGroup getPnd_Input_Record_Pnd_I_Cnt_NbrRedef4() { return pnd_Input_Record_Pnd_I_Cnt_NbrRedef4; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Nbr_2() { return pnd_Input_Record_Pnd_I_Cnt_Nbr_2; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Nbr_3_7() { return pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Payee_Cde() { return pnd_Input_Record_Pnd_I_Cnt_Payee_Cde; }

    public DbsGroup getPnd_Input_Record_Pnd_I_Cnt_Payee_CdeRedef5() { return pnd_Input_Record_Pnd_I_Cnt_Payee_CdeRedef5; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A() { return pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Actvty_Cde() { return pnd_Input_Record_Pnd_I_Cnt_Actvty_Cde; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Issue_Dte() { return pnd_Input_Record_Pnd_I_Cnt_Issue_Dte; }

    public DbsGroup getPnd_Input_Record_Pnd_I_Cnt_Issue_DteRedef6() { return pnd_Input_Record_Pnd_I_Cnt_Issue_DteRedef6; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm() { return pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd() { return pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind() { return pnd_Input_Record_Pnd_I_Cnt_Mode_Ind; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte() { return pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte; }

    public DbsField getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde() { return pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde; }

    public DbsField getPnd_Input_Record_Pnd_I_Ddctn_Per_Amt() { return pnd_Input_Record_Pnd_I_Ddctn_Per_Amt; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt() { return pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt() { return pnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt() { return pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt() { return pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt() { return pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt() { return pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt() { return pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_I_Cntrct_Optn_Code() { return pnd_Input_Record_Pnd_I_Cntrct_Optn_Code; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_J() { return pnd_J; }

    public DbsField getPnd_T() { return pnd_T; }

    public DbsField getPnd_Cnt_Payee_Field() { return pnd_Cnt_Payee_Field; }

    public DbsField getPnd_Final_Pay_Flag() { return pnd_Final_Pay_Flag; }

    public DbsField getPnd_Fmt3_Mode_Sub() { return pnd_Fmt3_Mode_Sub; }

    public DbsField getPnd_Fmt4_Cnt_Amt_Tot() { return pnd_Fmt4_Cnt_Amt_Tot; }

    public DbsField getPnd_Fmt4_Per_Div_Tot() { return pnd_Fmt4_Per_Div_Tot; }

    public DbsField getPnd_Fmt3_Tiaa_Table_Tot() { return pnd_Fmt3_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt3_Tiaa_Table() { return pnd_Fmt3_Tiaa_Table; }

    public DbsField getPnd_Fmt5_Tiaa_Table_Tot() { return pnd_Fmt5_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt5_Tiaa_Table() { return pnd_Fmt5_Tiaa_Table; }

    public DbsField getPnd_Fmt6_Tiaa_Table_Tot() { return pnd_Fmt6_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt6_Tiaa_Table() { return pnd_Fmt6_Tiaa_Table; }

    public DbsField getPnd_Fmt7_Tiaa_Table_Tot() { return pnd_Fmt7_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt7_Tiaa_Table() { return pnd_Fmt7_Tiaa_Table; }

    public DbsField getPnd_Fmt8_Tiaa_Table_Tot() { return pnd_Fmt8_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt8_Tiaa_Table() { return pnd_Fmt8_Tiaa_Table; }

    public DbsField getPnd_Fmt9_Tiaa_Table_Tot() { return pnd_Fmt9_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt9_Tiaa_Table() { return pnd_Fmt9_Tiaa_Table; }

    public DbsField getPnd_Fmt10_Tiaa_Table_Tot() { return pnd_Fmt10_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt10_Tiaa_Table() { return pnd_Fmt10_Tiaa_Table; }

    public DbsField getPnd_Fmt11_Tiaa_Table_Tot() { return pnd_Fmt11_Tiaa_Table_Tot; }

    public DbsField getPnd_Fmt11_Tiaa_Table() { return pnd_Fmt11_Tiaa_Table; }

    public DbsField getPnd_Fmt3_Units_Tot() { return pnd_Fmt3_Units_Tot; }

    public DbsField getPnd_Fmt3_Units() { return pnd_Fmt3_Units; }

    public DbsField getPnd_Fmt3_Dollars() { return pnd_Fmt3_Dollars; }

    public DbsField getPnd_Fmt3_Dollars_Tot() { return pnd_Fmt3_Dollars_Tot; }

    public DbsField getPnd_Fmt3_Deductions() { return pnd_Fmt3_Deductions; }

    public DbsField getPnd_Fmt5_Units_Tot() { return pnd_Fmt5_Units_Tot; }

    public DbsField getPnd_Fmt5_Units() { return pnd_Fmt5_Units; }

    public DbsField getPnd_Fmt5_Dollars() { return pnd_Fmt5_Dollars; }

    public DbsField getPnd_Fmt5_Dollars_Tot() { return pnd_Fmt5_Dollars_Tot; }

    public DbsField getPnd_Fmt5_Deductions() { return pnd_Fmt5_Deductions; }

    public DbsField getPnd_Fmt6_Units_Tot() { return pnd_Fmt6_Units_Tot; }

    public DbsField getPnd_Fmt6_Units() { return pnd_Fmt6_Units; }

    public DbsField getPnd_Fmt6_Dollars() { return pnd_Fmt6_Dollars; }

    public DbsField getPnd_Fmt6_Dollars_Tot() { return pnd_Fmt6_Dollars_Tot; }

    public DbsField getPnd_Fmt6_Deductions() { return pnd_Fmt6_Deductions; }

    public DbsField getPnd_Fmt7_Units_Tot() { return pnd_Fmt7_Units_Tot; }

    public DbsField getPnd_Fmt7_Units() { return pnd_Fmt7_Units; }

    public DbsField getPnd_Fmt7_Dollars() { return pnd_Fmt7_Dollars; }

    public DbsField getPnd_Fmt7_Dollars_Tot() { return pnd_Fmt7_Dollars_Tot; }

    public DbsField getPnd_Fmt7_Deductions() { return pnd_Fmt7_Deductions; }

    public DbsField getPnd_Generic_Unit_Value() { return pnd_Generic_Unit_Value; }

    public DbsField getPnd_Generic_Dollar_Amount() { return pnd_Generic_Dollar_Amount; }

    public DbsField getPnd_Fund_Types() { return pnd_Fund_Types; }

    public DbsField getPnd_Tiaa_Title() { return pnd_Tiaa_Title; }

    public DbsField getPnd_Cref_Title() { return pnd_Cref_Title; }

    public DbsField getPnd_Tiaa_Title_Tab() { return pnd_Tiaa_Title_Tab; }

    public DbsField getPnd_Cref_Title_Tab() { return pnd_Cref_Title_Tab; }

    public DbsField getPnd_Mode_1() { return pnd_Mode_1; }

    public DbsField getPnd_Cnt_Type_Sub() { return pnd_Cnt_Type_Sub; }

    public DbsField getPnd_Fund_Cde_Hold_Tot() { return pnd_Fund_Cde_Hold_Tot; }

    public DbsGroup getPnd_Fund_Cde_Hold_TotRedef7() { return pnd_Fund_Cde_Hold_TotRedef7; }

    public DbsField getPnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold() { return pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold; }

    public DbsField getPnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold() { return pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold; }

    public DbsGroup getPnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_HoldRedef8() { return pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_HoldRedef8; }

    public DbsField getPnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1() { return pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1; }

    public DbsField getPnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2() { return pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2; }

    public DbsField getPnd_Tiaa_Table_1() { return pnd_Tiaa_Table_1; }

    public DbsField getPnd_Tiaa_Table_2() { return pnd_Tiaa_Table_2; }

    public DbsField getPnd_Tiaa_Table_3() { return pnd_Tiaa_Table_3; }

    public DbsField getPnd_Tiaa_Table_4() { return pnd_Tiaa_Table_4; }

    public DbsField getPnd_Tiaa_Table_5() { return pnd_Tiaa_Table_5; }

    public DbsField getPnd_Tiaa_Table_6() { return pnd_Tiaa_Table_6; }

    public DbsField getPnd_Tiaa_Table_7() { return pnd_Tiaa_Table_7; }

    public DbsField getPnd_Tiaa_Table_Tot() { return pnd_Tiaa_Table_Tot; }

    public DbsField getPnd_Tiaa_Table_Tot_4() { return pnd_Tiaa_Table_Tot_4; }

    public DbsField getPnd_Tiaa_Table_Tot_5() { return pnd_Tiaa_Table_Tot_5; }

    public DbsField getPnd_Tiaa_Table_Tot_6() { return pnd_Tiaa_Table_Tot_6; }

    public DbsField getPnd_Tiaa_Table_Tot_7() { return pnd_Tiaa_Table_Tot_7; }

    public DbsField getPnd_Tiaa_Table_Tot_G() { return pnd_Tiaa_Table_Tot_G; }

    public DbsField getPnd_Cref_Units_1() { return pnd_Cref_Units_1; }

    public DbsField getPnd_Cref_Units_2() { return pnd_Cref_Units_2; }

    public DbsField getPnd_Cref_Units_3() { return pnd_Cref_Units_3; }

    public DbsField getPnd_Cref_Units_Total() { return pnd_Cref_Units_Total; }

    public DbsField getPnd_Cref_Dollars_1() { return pnd_Cref_Dollars_1; }

    public DbsField getPnd_Cref_Dollars_2() { return pnd_Cref_Dollars_2; }

    public DbsField getPnd_Cref_Dollars_3() { return pnd_Cref_Dollars_3; }

    public DbsField getPnd_Cref_Dollars_Total() { return pnd_Cref_Dollars_Total; }

    public DbsField getPnd_Cref_Deduct_1() { return pnd_Cref_Deduct_1; }

    public DbsField getPnd_Cref_Deduct_2() { return pnd_Cref_Deduct_2; }

    public DbsField getPnd_Cref_Deduct_3() { return pnd_Cref_Deduct_3; }

    public DbsField getPnd_Cref_Deduct_Total() { return pnd_Cref_Deduct_Total; }

    public DbsField getPnd_Cref_Units_Tot() { return pnd_Cref_Units_Tot; }

    public DbsField getPnd_Cref_Dollars_Tot() { return pnd_Cref_Dollars_Tot; }

    public DbsField getPnd_Cref_Deduct_Tot() { return pnd_Cref_Deduct_Tot; }

    public DbsField getPnd_Mode_Table_2() { return pnd_Mode_Table_2; }

    public DbsField getPnd_Unit_Text_Table() { return pnd_Unit_Text_Table; }

    public DbsField getPnd_Dollars_Text_Table() { return pnd_Dollars_Text_Table; }

    public DbsField getPnd_Tiaa_Cnt_Amt_Tot() { return pnd_Tiaa_Cnt_Amt_Tot; }

    public DbsField getPnd_Tiaa_Cnt_Amt_Tot_4() { return pnd_Tiaa_Cnt_Amt_Tot_4; }

    public DbsField getPnd_Tiaa_Cnt_Amt_Tot_5() { return pnd_Tiaa_Cnt_Amt_Tot_5; }

    public DbsField getPnd_Tiaa_Cnt_Amt_Tot_6() { return pnd_Tiaa_Cnt_Amt_Tot_6; }

    public DbsField getPnd_Tiaa_Cnt_Amt_Tot_7() { return pnd_Tiaa_Cnt_Amt_Tot_7; }

    public DbsField getPnd_Tiaa_Per_Ded_Tot() { return pnd_Tiaa_Per_Ded_Tot; }

    public DbsField getPnd_Tiaa_Per_Ded_Tot_4() { return pnd_Tiaa_Per_Ded_Tot_4; }

    public DbsField getPnd_Tiaa_Per_Ded_Tot_5() { return pnd_Tiaa_Per_Ded_Tot_5; }

    public DbsField getPnd_Tiaa_Per_Ded_Tot_6() { return pnd_Tiaa_Per_Ded_Tot_6; }

    public DbsField getPnd_Tiaa_Per_Ded_Tot_7() { return pnd_Tiaa_Per_Ded_Tot_7; }

    public DbsField getPnd_Tiaa_Fin_Pay_Tot() { return pnd_Tiaa_Fin_Pay_Tot; }

    public DbsField getPnd_Tiaa_Fin_Pay_Tot_4() { return pnd_Tiaa_Fin_Pay_Tot_4; }

    public DbsField getPnd_Tiaa_Fin_Pay_Tot_5() { return pnd_Tiaa_Fin_Pay_Tot_5; }

    public DbsField getPnd_Tiaa_Fin_Pay_Tot_6() { return pnd_Tiaa_Fin_Pay_Tot_6; }

    public DbsField getPnd_Tiaa_Fin_Pay_Tot_7() { return pnd_Tiaa_Fin_Pay_Tot_7; }

    public DbsField getPnd_Tiaa_Per_Div_Tot() { return pnd_Tiaa_Per_Div_Tot; }

    public DbsField getPnd_Tiaa_Per_Div_Tot_4() { return pnd_Tiaa_Per_Div_Tot_4; }

    public DbsField getPnd_Tiaa_Per_Div_Tot_5() { return pnd_Tiaa_Per_Div_Tot_5; }

    public DbsField getPnd_Tiaa_Per_Div_Tot_6() { return pnd_Tiaa_Per_Div_Tot_6; }

    public DbsField getPnd_Tiaa_Per_Div_Tot_7() { return pnd_Tiaa_Per_Div_Tot_7; }

    public DbsField getPnd_Tiaa_Fin_Div_Tot() { return pnd_Tiaa_Fin_Div_Tot; }

    public DbsField getPnd_Tiaa_Fin_Div_Tot_4() { return pnd_Tiaa_Fin_Div_Tot_4; }

    public DbsField getPnd_Tiaa_Fin_Div_Tot_5() { return pnd_Tiaa_Fin_Div_Tot_5; }

    public DbsField getPnd_Tiaa_Fin_Div_Tot_6() { return pnd_Tiaa_Fin_Div_Tot_6; }

    public DbsField getPnd_Tiaa_Fin_Div_Tot_7() { return pnd_Tiaa_Fin_Div_Tot_7; }

    public DbsField getPnd_Tiaa_Cnt_Amt_Tot_G() { return pnd_Tiaa_Cnt_Amt_Tot_G; }

    public DbsField getPnd_Tiaa_Per_Ded_Tot_G() { return pnd_Tiaa_Per_Ded_Tot_G; }

    public DbsField getPnd_Tiaa_Fin_Pay_Tot_G() { return pnd_Tiaa_Fin_Pay_Tot_G; }

    public DbsField getPnd_Tiaa_Per_Div_Tot_G() { return pnd_Tiaa_Per_Div_Tot_G; }

    public DbsField getPnd_Tiaa_Fin_Div_Tot_G() { return pnd_Tiaa_Fin_Div_Tot_G; }

    public DbsField getPnd_Pay_Mode_Table() { return pnd_Pay_Mode_Table; }

    public DbsField getPnd_W_Rec_3() { return pnd_W_Rec_3; }

    public DbsGroup getPnd_W_Rec_3Redef9() { return pnd_W_Rec_3Redef9; }

    public DbsField getPnd_W_Rec_3_Pnd_W_Rec_3_A() { return pnd_W_Rec_3_Pnd_W_Rec_3_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Input_Record = newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot", "#I-TIAA-FUND-CDE-TOT", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1", "Redefines", 
            pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot);
        pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde = pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde", 
            "#I-TIAA-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde = pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde", 
            "#I-TIAA-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_CdeRedef2 = pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotRedef1.newGroupInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_CdeRedef2", 
            "Redefines", pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1 = pnd_Input_Record_Pnd_I_Tiaa_Fund_CdeRedef2.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1", 
            "#I-TIAA-FUND-CDE-1", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2 = pnd_Input_Record_Pnd_I_Tiaa_Fund_CdeRedef2.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2", 
            "#I-TIAA-FUND-CDE-2", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Cnt_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr", "#I-CNT-NBR", FieldType.STRING, 10);
        pnd_Input_Record_Pnd_I_Cnt_NbrRedef3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_I_Cnt_NbrRedef3", "Redefines", pnd_Input_Record_Pnd_I_Cnt_Nbr);
        pnd_Input_Record_Pnd_I_Cnt_Nbr_8 = pnd_Input_Record_Pnd_I_Cnt_NbrRedef3.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr_8", "#I-CNT-NBR-8", FieldType.STRING, 
            8);
        pnd_Input_Record_Pnd_I_Cnt_NbrRedef4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_I_Cnt_NbrRedef4", "Redefines", pnd_Input_Record_Pnd_I_Cnt_Nbr);
        pnd_Input_Record_Pnd_I_Cnt_Nbr_2 = pnd_Input_Record_Pnd_I_Cnt_NbrRedef4.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr_2", "#I-CNT-NBR-2", FieldType.STRING, 
            2);
        pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7 = pnd_Input_Record_Pnd_I_Cnt_NbrRedef4.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7", "#I-CNT-NBR-3-7", 
            FieldType.NUMERIC, 5);
        pnd_Input_Record_Pnd_I_Cnt_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Payee_Cde", "#I-CNT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_I_Cnt_Payee_CdeRedef5 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_I_Cnt_Payee_CdeRedef5", "Redefines", pnd_Input_Record_Pnd_I_Cnt_Payee_Cde);
        pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A = pnd_Input_Record_Pnd_I_Cnt_Payee_CdeRedef5.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A", 
            "#I-CNT-PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Record_Pnd_I_Cnt_Actvty_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Actvty_Cde", "#I-CNT-ACTVTY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Record_Pnd_I_Cnt_Issue_Dte = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Issue_Dte", "#I-CNT-ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Record_Pnd_I_Cnt_Issue_DteRedef6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_I_Cnt_Issue_DteRedef6", "Redefines", pnd_Input_Record_Pnd_I_Cnt_Issue_Dte);
        pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm = pnd_Input_Record_Pnd_I_Cnt_Issue_DteRedef6.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm", 
            "#I-CNT-ISSUE-DTE-YYMM", FieldType.NUMERIC, 4);
        pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd = pnd_Input_Record_Pnd_I_Cnt_Issue_DteRedef6.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd", 
            "#I-CNT-ISSUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_I_Cnt_Mode_Ind = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Mode_Ind", "#I-CNT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte", "#I-CNT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde", "#I-CNT-CURR-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Input_Record_Pnd_I_Ddctn_Per_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Ddctn_Per_Amt", "#I-DDCTN-PER-AMT", FieldType.NUMERIC, 
            5);
        pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt", "#I-TIAA-TOT-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt", "#I-TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt", "#I-TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt", "#I-TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt", "#I-TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt", "#I-TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt", "#I-TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_I_Cntrct_Optn_Code = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cntrct_Optn_Code", "#I-CNTRCT-OPTN-CODE", FieldType.NUMERIC, 
            2);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        pnd_J = newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);

        pnd_T = newFieldInRecord("pnd_T", "#T", FieldType.NUMERIC, 3);

        pnd_Cnt_Payee_Field = newFieldInRecord("pnd_Cnt_Payee_Field", "#CNT-PAYEE-FIELD", FieldType.STRING, 11);

        pnd_Final_Pay_Flag = newFieldInRecord("pnd_Final_Pay_Flag", "#FINAL-PAY-FLAG", FieldType.STRING, 1);

        pnd_Fmt3_Mode_Sub = newFieldInRecord("pnd_Fmt3_Mode_Sub", "#FMT3-MODE-SUB", FieldType.NUMERIC, 1);

        pnd_Fmt4_Cnt_Amt_Tot = newFieldInRecord("pnd_Fmt4_Cnt_Amt_Tot", "#FMT4-CNT-AMT-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt4_Per_Div_Tot = newFieldInRecord("pnd_Fmt4_Per_Div_Tot", "#FMT4-PER-DIV-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt3_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt3_Tiaa_Table_Tot", "#FMT3-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt3_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt3_Tiaa_Table", "#FMT3-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,4));

        pnd_Fmt5_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt5_Tiaa_Table_Tot", "#FMT5-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt5_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt5_Tiaa_Table", "#FMT5-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,4));

        pnd_Fmt6_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt6_Tiaa_Table_Tot", "#FMT6-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt6_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt6_Tiaa_Table", "#FMT6-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,4));

        pnd_Fmt7_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt7_Tiaa_Table_Tot", "#FMT7-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt7_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt7_Tiaa_Table", "#FMT7-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,4));

        pnd_Fmt8_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt8_Tiaa_Table_Tot", "#FMT8-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt8_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt8_Tiaa_Table", "#FMT8-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,4));

        pnd_Fmt9_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt9_Tiaa_Table_Tot", "#FMT9-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt9_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt9_Tiaa_Table", "#FMT9-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,4));

        pnd_Fmt10_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt10_Tiaa_Table_Tot", "#FMT10-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt10_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt10_Tiaa_Table", "#FMT10-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,
            1,4));

        pnd_Fmt11_Tiaa_Table_Tot = newFieldInRecord("pnd_Fmt11_Tiaa_Table_Tot", "#FMT11-TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt11_Tiaa_Table = newFieldArrayInRecord("pnd_Fmt11_Tiaa_Table", "#FMT11-TIAA-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,
            1,4));

        pnd_Fmt3_Units_Tot = newFieldInRecord("pnd_Fmt3_Units_Tot", "#FMT3-UNITS-TOT", FieldType.DECIMAL, 11,3);

        pnd_Fmt3_Units = newFieldArrayInRecord("pnd_Fmt3_Units", "#FMT3-UNITS", FieldType.DECIMAL, 11,3, new DbsArrayController(1,20,1,4));

        pnd_Fmt3_Dollars = newFieldArrayInRecord("pnd_Fmt3_Dollars", "#FMT3-DOLLARS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,20,1,4));

        pnd_Fmt3_Dollars_Tot = newFieldInRecord("pnd_Fmt3_Dollars_Tot", "#FMT3-DOLLARS-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt3_Deductions = newFieldArrayInRecord("pnd_Fmt3_Deductions", "#FMT3-DEDUCTIONS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,4));

        pnd_Fmt5_Units_Tot = newFieldInRecord("pnd_Fmt5_Units_Tot", "#FMT5-UNITS-TOT", FieldType.DECIMAL, 11,3);

        pnd_Fmt5_Units = newFieldArrayInRecord("pnd_Fmt5_Units", "#FMT5-UNITS", FieldType.DECIMAL, 11,3, new DbsArrayController(1,20,1,4));

        pnd_Fmt5_Dollars = newFieldArrayInRecord("pnd_Fmt5_Dollars", "#FMT5-DOLLARS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,20,1,4));

        pnd_Fmt5_Dollars_Tot = newFieldInRecord("pnd_Fmt5_Dollars_Tot", "#FMT5-DOLLARS-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt5_Deductions = newFieldArrayInRecord("pnd_Fmt5_Deductions", "#FMT5-DEDUCTIONS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,4));

        pnd_Fmt6_Units_Tot = newFieldInRecord("pnd_Fmt6_Units_Tot", "#FMT6-UNITS-TOT", FieldType.DECIMAL, 11,3);

        pnd_Fmt6_Units = newFieldArrayInRecord("pnd_Fmt6_Units", "#FMT6-UNITS", FieldType.DECIMAL, 11,3, new DbsArrayController(1,20,1,4));

        pnd_Fmt6_Dollars = newFieldArrayInRecord("pnd_Fmt6_Dollars", "#FMT6-DOLLARS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,20,1,4));

        pnd_Fmt6_Dollars_Tot = newFieldInRecord("pnd_Fmt6_Dollars_Tot", "#FMT6-DOLLARS-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt6_Deductions = newFieldArrayInRecord("pnd_Fmt6_Deductions", "#FMT6-DEDUCTIONS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,4));

        pnd_Fmt7_Units_Tot = newFieldInRecord("pnd_Fmt7_Units_Tot", "#FMT7-UNITS-TOT", FieldType.DECIMAL, 11,3);

        pnd_Fmt7_Units = newFieldArrayInRecord("pnd_Fmt7_Units", "#FMT7-UNITS", FieldType.DECIMAL, 11,3, new DbsArrayController(1,20,1,4));

        pnd_Fmt7_Dollars = newFieldArrayInRecord("pnd_Fmt7_Dollars", "#FMT7-DOLLARS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,20,1,4));

        pnd_Fmt7_Dollars_Tot = newFieldInRecord("pnd_Fmt7_Dollars_Tot", "#FMT7-DOLLARS-TOT", FieldType.DECIMAL, 11,2);

        pnd_Fmt7_Deductions = newFieldArrayInRecord("pnd_Fmt7_Deductions", "#FMT7-DEDUCTIONS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,4));

        pnd_Generic_Unit_Value = newFieldInRecord("pnd_Generic_Unit_Value", "#GENERIC-UNIT-VALUE", FieldType.DECIMAL, 5,2);

        pnd_Generic_Dollar_Amount = newFieldInRecord("pnd_Generic_Dollar_Amount", "#GENERIC-DOLLAR-AMOUNT", FieldType.DECIMAL, 11,2);

        pnd_Fund_Types = newFieldInRecord("pnd_Fund_Types", "#FUND-TYPES", FieldType.NUMERIC, 1);

        pnd_Tiaa_Title = newFieldInRecord("pnd_Tiaa_Title", "#TIAA-TITLE", FieldType.STRING, 30);

        pnd_Cref_Title = newFieldInRecord("pnd_Cref_Title", "#CREF-TITLE", FieldType.STRING, 30);

        pnd_Tiaa_Title_Tab = newFieldArrayInRecord("pnd_Tiaa_Title_Tab", "#TIAA-TITLE-TAB", FieldType.STRING, 30, new DbsArrayController(1,4));

        pnd_Cref_Title_Tab = newFieldArrayInRecord("pnd_Cref_Title_Tab", "#CREF-TITLE-TAB", FieldType.STRING, 30, new DbsArrayController(1,4));

        pnd_Mode_1 = newFieldInRecord("pnd_Mode_1", "#MODE-1", FieldType.NUMERIC, 2);

        pnd_Cnt_Type_Sub = newFieldInRecord("pnd_Cnt_Type_Sub", "#CNT-TYPE-SUB", FieldType.NUMERIC, 2);

        pnd_Fund_Cde_Hold_Tot = newFieldInRecord("pnd_Fund_Cde_Hold_Tot", "#FUND-CDE-HOLD-TOT", FieldType.STRING, 3);
        pnd_Fund_Cde_Hold_TotRedef7 = newGroupInRecord("pnd_Fund_Cde_Hold_TotRedef7", "Redefines", pnd_Fund_Cde_Hold_Tot);
        pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold = pnd_Fund_Cde_Hold_TotRedef7.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold", "#CMPNY-CDE-HOLD", 
            FieldType.STRING, 1);
        pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold = pnd_Fund_Cde_Hold_TotRedef7.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold", "#FUND-CDE-HOLD", 
            FieldType.STRING, 2);
        pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_HoldRedef8 = pnd_Fund_Cde_Hold_TotRedef7.newGroupInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_HoldRedef8", "Redefines", 
            pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold);
        pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1 = pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_HoldRedef8.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1", 
            "#FUND-CDE-HOLD-1", FieldType.STRING, 1);
        pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2 = pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_HoldRedef8.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2", 
            "#FUND-CDE-HOLD-2", FieldType.STRING, 1);

        pnd_Tiaa_Table_1 = newFieldArrayInRecord("pnd_Tiaa_Table_1", "#TIAA-TABLE-1", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_2 = newFieldArrayInRecord("pnd_Tiaa_Table_2", "#TIAA-TABLE-2", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_3 = newFieldArrayInRecord("pnd_Tiaa_Table_3", "#TIAA-TABLE-3", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_4 = newFieldArrayInRecord("pnd_Tiaa_Table_4", "#TIAA-TABLE-4", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_5 = newFieldArrayInRecord("pnd_Tiaa_Table_5", "#TIAA-TABLE-5", FieldType.DECIMAL, 13,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_6 = newFieldArrayInRecord("pnd_Tiaa_Table_6", "#TIAA-TABLE-6", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_7 = newFieldArrayInRecord("pnd_Tiaa_Table_7", "#TIAA-TABLE-7", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_Tot = newFieldArrayInRecord("pnd_Tiaa_Table_Tot", "#TIAA-TABLE-TOT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,1,4));

        pnd_Tiaa_Table_Tot_4 = newFieldArrayInRecord("pnd_Tiaa_Table_Tot_4", "#TIAA-TABLE-TOT-4", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,
            1,4));

        pnd_Tiaa_Table_Tot_5 = newFieldArrayInRecord("pnd_Tiaa_Table_Tot_5", "#TIAA-TABLE-TOT-5", FieldType.DECIMAL, 13,2, new DbsArrayController(1,22,
            1,4));

        pnd_Tiaa_Table_Tot_6 = newFieldArrayInRecord("pnd_Tiaa_Table_Tot_6", "#TIAA-TABLE-TOT-6", FieldType.DECIMAL, 13,2, new DbsArrayController(1,22,
            1,4));

        pnd_Tiaa_Table_Tot_7 = newFieldArrayInRecord("pnd_Tiaa_Table_Tot_7", "#TIAA-TABLE-TOT-7", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22,
            1,4));

        pnd_Tiaa_Table_Tot_G = newFieldArrayInRecord("pnd_Tiaa_Table_Tot_G", "#TIAA-TABLE-TOT-G", FieldType.DECIMAL, 13,2, new DbsArrayController(1,22,
            1,4));

        pnd_Cref_Units_1 = newFieldArrayInRecord("pnd_Cref_Units_1", "#CREF-UNITS-1", FieldType.DECIMAL, 11,3, new DbsArrayController(1,22));

        pnd_Cref_Units_2 = newFieldArrayInRecord("pnd_Cref_Units_2", "#CREF-UNITS-2", FieldType.DECIMAL, 11,3, new DbsArrayController(1,22));

        pnd_Cref_Units_3 = newFieldArrayInRecord("pnd_Cref_Units_3", "#CREF-UNITS-3", FieldType.DECIMAL, 11,3, new DbsArrayController(1,22));

        pnd_Cref_Units_Total = newFieldInRecord("pnd_Cref_Units_Total", "#CREF-UNITS-TOTAL", FieldType.DECIMAL, 11,3);

        pnd_Cref_Dollars_1 = newFieldArrayInRecord("pnd_Cref_Dollars_1", "#CREF-DOLLARS-1", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Cref_Dollars_2 = newFieldArrayInRecord("pnd_Cref_Dollars_2", "#CREF-DOLLARS-2", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Cref_Dollars_3 = newFieldArrayInRecord("pnd_Cref_Dollars_3", "#CREF-DOLLARS-3", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Cref_Dollars_Total = newFieldInRecord("pnd_Cref_Dollars_Total", "#CREF-DOLLARS-TOTAL", FieldType.DECIMAL, 11,2);

        pnd_Cref_Deduct_1 = newFieldArrayInRecord("pnd_Cref_Deduct_1", "#CREF-DEDUCT-1", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Cref_Deduct_2 = newFieldArrayInRecord("pnd_Cref_Deduct_2", "#CREF-DEDUCT-2", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Cref_Deduct_3 = newFieldArrayInRecord("pnd_Cref_Deduct_3", "#CREF-DEDUCT-3", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Cref_Deduct_Total = newFieldInRecord("pnd_Cref_Deduct_Total", "#CREF-DEDUCT-TOTAL", FieldType.DECIMAL, 11,2);

        pnd_Cref_Units_Tot = newFieldArrayInRecord("pnd_Cref_Units_Tot", "#CREF-UNITS-TOT", FieldType.DECIMAL, 11,3, new DbsArrayController(1,22));

        pnd_Cref_Dollars_Tot = newFieldArrayInRecord("pnd_Cref_Dollars_Tot", "#CREF-DOLLARS-TOT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Cref_Deduct_Tot = newFieldArrayInRecord("pnd_Cref_Deduct_Tot", "#CREF-DEDUCT-TOT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,22));

        pnd_Mode_Table_2 = newFieldArrayInRecord("pnd_Mode_Table_2", "#MODE-TABLE-2", FieldType.STRING, 3, new DbsArrayController(1,22));

        pnd_Unit_Text_Table = newFieldArrayInRecord("pnd_Unit_Text_Table", "#UNIT-TEXT-TABLE", FieldType.STRING, 24, new DbsArrayController(1,20));

        pnd_Dollars_Text_Table = newFieldArrayInRecord("pnd_Dollars_Text_Table", "#DOLLARS-TEXT-TABLE", FieldType.STRING, 24, new DbsArrayController(1,
            20));

        pnd_Tiaa_Cnt_Amt_Tot = newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot", "#TIAA-CNT-AMT-TOT", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Cnt_Amt_Tot_4 = newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_4", "#TIAA-CNT-AMT-TOT-4", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Cnt_Amt_Tot_5 = newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_5", "#TIAA-CNT-AMT-TOT-5", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Cnt_Amt_Tot_6 = newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_6", "#TIAA-CNT-AMT-TOT-6", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Cnt_Amt_Tot_7 = newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_7", "#TIAA-CNT-AMT-TOT-7", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Ded_Tot = newFieldInRecord("pnd_Tiaa_Per_Ded_Tot", "#TIAA-PER-DED-TOT", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Ded_Tot_4 = newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_4", "#TIAA-PER-DED-TOT-4", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Ded_Tot_5 = newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_5", "#TIAA-PER-DED-TOT-5", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Ded_Tot_6 = newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_6", "#TIAA-PER-DED-TOT-6", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Ded_Tot_7 = newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_7", "#TIAA-PER-DED-TOT-7", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Pay_Tot = newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot", "#TIAA-FIN-PAY-TOT", FieldType.DECIMAL, 13,2);

        pnd_Tiaa_Fin_Pay_Tot_4 = newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_4", "#TIAA-FIN-PAY-TOT-4", FieldType.DECIMAL, 13,2);

        pnd_Tiaa_Fin_Pay_Tot_5 = newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_5", "#TIAA-FIN-PAY-TOT-5", FieldType.DECIMAL, 13,2);

        pnd_Tiaa_Fin_Pay_Tot_6 = newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_6", "#TIAA-FIN-PAY-TOT-6", FieldType.DECIMAL, 13,2);

        pnd_Tiaa_Fin_Pay_Tot_7 = newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_7", "#TIAA-FIN-PAY-TOT-7", FieldType.DECIMAL, 13,2);

        pnd_Tiaa_Per_Div_Tot = newFieldInRecord("pnd_Tiaa_Per_Div_Tot", "#TIAA-PER-DIV-TOT", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Div_Tot_4 = newFieldInRecord("pnd_Tiaa_Per_Div_Tot_4", "#TIAA-PER-DIV-TOT-4", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Div_Tot_5 = newFieldInRecord("pnd_Tiaa_Per_Div_Tot_5", "#TIAA-PER-DIV-TOT-5", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Div_Tot_6 = newFieldInRecord("pnd_Tiaa_Per_Div_Tot_6", "#TIAA-PER-DIV-TOT-6", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Per_Div_Tot_7 = newFieldInRecord("pnd_Tiaa_Per_Div_Tot_7", "#TIAA-PER-DIV-TOT-7", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Div_Tot = newFieldInRecord("pnd_Tiaa_Fin_Div_Tot", "#TIAA-FIN-DIV-TOT", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Div_Tot_4 = newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_4", "#TIAA-FIN-DIV-TOT-4", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Div_Tot_5 = newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_5", "#TIAA-FIN-DIV-TOT-5", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Div_Tot_6 = newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_6", "#TIAA-FIN-DIV-TOT-6", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Div_Tot_7 = newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_7", "#TIAA-FIN-DIV-TOT-7", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Cnt_Amt_Tot_G = newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_G", "#TIAA-CNT-AMT-TOT-G", FieldType.DECIMAL, 13,2);

        pnd_Tiaa_Per_Ded_Tot_G = newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_G", "#TIAA-PER-DED-TOT-G", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Pay_Tot_G = newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_G", "#TIAA-FIN-PAY-TOT-G", FieldType.DECIMAL, 13,2);

        pnd_Tiaa_Per_Div_Tot_G = newFieldInRecord("pnd_Tiaa_Per_Div_Tot_G", "#TIAA-PER-DIV-TOT-G", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Fin_Div_Tot_G = newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_G", "#TIAA-FIN-DIV-TOT-G", FieldType.DECIMAL, 11,2);

        pnd_Pay_Mode_Table = newFieldArrayInRecord("pnd_Pay_Mode_Table", "#PAY-MODE-TABLE", FieldType.NUMERIC, 3, new DbsArrayController(1,4));

        pnd_W_Rec_3 = newFieldInRecord("pnd_W_Rec_3", "#W-REC-3", FieldType.NUMERIC, 8);
        pnd_W_Rec_3Redef9 = newGroupInRecord("pnd_W_Rec_3Redef9", "Redefines", pnd_W_Rec_3);
        pnd_W_Rec_3_Pnd_W_Rec_3_A = pnd_W_Rec_3Redef9.newFieldInGroup("pnd_W_Rec_3_Pnd_W_Rec_3_A", "#W-REC-3-A", FieldType.STRING, 8);

        this.setRecordName("LdaIaal110");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal110() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
