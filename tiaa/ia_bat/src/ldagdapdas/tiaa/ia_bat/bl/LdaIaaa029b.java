/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:08 PM
**        * FROM NATURAL LDA     : IAAA029B
************************************************************
**        * FILE NAME            : LdaIaaa029b.java
**        * CLASS NAME           : LdaIaaa029b
**        * INSTANCE NAME        : LdaIaaa029b
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaaa029b extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Netchange_Record;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Pin_Nbr;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Contract_Nbr;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Payee_Code;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Contract_Type;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Hold_Code;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Hold_CodeRedef1;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Hold_1;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Hold_2;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Rqst_Id;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Origin;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Fil;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Name_Address;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Full_Name;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Address_1;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Check_Date;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Check_Mm;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Check_Dd;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Check_Cc;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Check_Yy;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Check_Date;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Check_Mm;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Check_Dd;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Check_Cc;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Check_Yy;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Postnet_Zip;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Zip_Code_5;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Zip_Code_5Redef5;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Zip_Code_3;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Zip_Code_2;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Deduction_Count;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Deductions;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Deduction_Code;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Deduction_Desc;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Ded_Chg_Flag;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_St_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Prev_Total_Deduction;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_St_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Curr_Total_Deduction;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Count;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Funds;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Detail;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Valuation_Period;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Code;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Desc;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Type;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_ZeroRedef7;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_ValueRedef8;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Zero;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Messages_Ctr;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Messages;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Message_Number;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Message_NumberRedef9;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Message_Number_N;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Message_Date_1;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_1;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_1;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Message_Date_2;
    private DbsGroup pnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_2;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_2;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Message_Percent;
    private DbsField pnd_Netchange_Record_Pnd_Nc_Phone_Number;

    public DbsGroup getPnd_Netchange_Record() { return pnd_Netchange_Record; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract() { return pnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr() { return pnd_Netchange_Record_Pnd_Nc_Pin_Nbr; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Contract_Nbr() { return pnd_Netchange_Record_Pnd_Nc_Contract_Nbr; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Payee_Code() { return pnd_Netchange_Record_Pnd_Nc_Payee_Code; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Contract_Type() { return pnd_Netchange_Record_Pnd_Nc_Contract_Type; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Hold_Code() { return pnd_Netchange_Record_Pnd_Nc_Hold_Code; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Hold_CodeRedef1() { return pnd_Netchange_Record_Pnd_Nc_Hold_CodeRedef1; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Hold_1() { return pnd_Netchange_Record_Pnd_Nc_Hold_1; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Hold_2() { return pnd_Netchange_Record_Pnd_Nc_Hold_2; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind() { return pnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Rqst_Id() { return pnd_Netchange_Record_Pnd_Nc_Rqst_Id; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2() { return pnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Origin() { return pnd_Netchange_Record_Pnd_Nc_Origin; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr() { return pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Fil() { return pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Fil; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code() { return pnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Name_Address() { return pnd_Netchange_Record_Pnd_Nc_Name_Address; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Full_Name() { return pnd_Netchange_Record_Pnd_Nc_Full_Name; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Address_1() { return pnd_Netchange_Record_Pnd_Nc_Address_1; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Date() { return pnd_Netchange_Record_Pnd_Nc_Prev_Check_Date; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3() { return pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Mm() { return pnd_Netchange_Record_Pnd_Nc_Prev_Check_Mm; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Dd() { return pnd_Netchange_Record_Pnd_Nc_Prev_Check_Dd; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Cc() { return pnd_Netchange_Record_Pnd_Nc_Prev_Check_Cc; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Yy() { return pnd_Netchange_Record_Pnd_Nc_Prev_Check_Yy; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Date() { return pnd_Netchange_Record_Pnd_Nc_Curr_Check_Date; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4() { return pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Mm() { return pnd_Netchange_Record_Pnd_Nc_Curr_Check_Mm; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Dd() { return pnd_Netchange_Record_Pnd_Nc_Curr_Check_Dd; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Cc() { return pnd_Netchange_Record_Pnd_Nc_Curr_Check_Cc; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Yy() { return pnd_Netchange_Record_Pnd_Nc_Curr_Check_Yy; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2() { return pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3() { return pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4() { return pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5() { return pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Postnet_Zip() { return pnd_Netchange_Record_Pnd_Nc_Postnet_Zip; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Zip_Code_5() { return pnd_Netchange_Record_Pnd_Nc_Zip_Code_5; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Zip_Code_5Redef5() { return pnd_Netchange_Record_Pnd_Nc_Zip_Code_5Redef5; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Zip_Code_3() { return pnd_Netchange_Record_Pnd_Nc_Zip_Code_3; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Zip_Code_2() { return pnd_Netchange_Record_Pnd_Nc_Zip_Code_2; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Deduction_Count() { return pnd_Netchange_Record_Pnd_Nc_Deduction_Count; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Deductions() { return pnd_Netchange_Record_Pnd_Nc_Deductions; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Deduction_Code() { return pnd_Netchange_Record_Pnd_Nc_Deduction_Code; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc() { return pnd_Netchange_Record_Pnd_Nc_Deduction_Desc; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt() { return pnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt() { return pnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Ded_Chg_Flag() { return pnd_Netchange_Record_Pnd_Nc_Ded_Chg_Flag; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt() { return pnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_St_Amt() { return pnd_Netchange_Record_Pnd_Nc_Prev_St_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt() { return pnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Prev_Total_Deduction() { return pnd_Netchange_Record_Pnd_Nc_Prev_Total_Deduction; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt() { return pnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_St_Amt() { return pnd_Netchange_Record_Pnd_Nc_Curr_St_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt() { return pnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Curr_Total_Deduction() { return pnd_Netchange_Record_Pnd_Nc_Curr_Total_Deduction; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Count() { return pnd_Netchange_Record_Pnd_Nc_Fund_Count; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Funds() { return pnd_Netchange_Record_Pnd_Nc_Funds; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Detail() { return pnd_Netchange_Record_Pnd_Nc_Fund_Detail; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6() { return pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Valuation_Period() { return pnd_Netchange_Record_Pnd_Nc_Valuation_Period; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Code() { return pnd_Netchange_Record_Pnd_Nc_Fund_Code; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Desc() { return pnd_Netchange_Record_Pnd_Nc_Fund_Desc; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Type() { return pnd_Netchange_Record_Pnd_Nc_Fund_Type; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method() { return pnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_ZeroRedef7() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_ZeroRedef7; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_ValueRedef8() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_ValueRedef8; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Zero() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Zero; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt() { return pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Messages_Ctr() { return pnd_Netchange_Record_Pnd_Nc_Messages_Ctr; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Messages() { return pnd_Netchange_Record_Pnd_Nc_Messages; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Message_Number() { return pnd_Netchange_Record_Pnd_Nc_Message_Number; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Message_NumberRedef9() { return pnd_Netchange_Record_Pnd_Nc_Message_NumberRedef9; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Message_Number_N() { return pnd_Netchange_Record_Pnd_Nc_Message_Number_N; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Message_Date_1() { return pnd_Netchange_Record_Pnd_Nc_Message_Date_1; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10() { return pnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_1() { return pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_1; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_1() { return pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_1; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1() { return pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Message_Date_2() { return pnd_Netchange_Record_Pnd_Nc_Message_Date_2; }

    public DbsGroup getPnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11() { return pnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_2() { return pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_2; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_2() { return pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_2; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2() { return pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Message_Percent() { return pnd_Netchange_Record_Pnd_Nc_Message_Percent; }

    public DbsField getPnd_Netchange_Record_Pnd_Nc_Phone_Number() { return pnd_Netchange_Record_Pnd_Nc_Phone_Number; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Netchange_Record = newGroupInRecord("pnd_Netchange_Record", "#NETCHANGE-RECORD");
        pnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract", "#NC-NBR-OF-CONTRACT", 
            FieldType.NUMERIC, 3);
        pnd_Netchange_Record_Pnd_Nc_Pin_Nbr = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Pin_Nbr", "#NC-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Netchange_Record_Pnd_Nc_Contract_Nbr = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Contract_Nbr", "#NC-CONTRACT-NBR", 
            FieldType.STRING, 10);
        pnd_Netchange_Record_Pnd_Nc_Payee_Code = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Payee_Code", "#NC-PAYEE-CODE", FieldType.STRING, 
            4);
        pnd_Netchange_Record_Pnd_Nc_Contract_Type = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Contract_Type", "#NC-CONTRACT-TYPE", 
            FieldType.STRING, 1);
        pnd_Netchange_Record_Pnd_Nc_Hold_Code = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Hold_Code", "#NC-HOLD-CODE", FieldType.STRING, 
            4);
        pnd_Netchange_Record_Pnd_Nc_Hold_CodeRedef1 = pnd_Netchange_Record.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Hold_CodeRedef1", "Redefines", 
            pnd_Netchange_Record_Pnd_Nc_Hold_Code);
        pnd_Netchange_Record_Pnd_Nc_Hold_1 = pnd_Netchange_Record_Pnd_Nc_Hold_CodeRedef1.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Hold_1", "#NC-HOLD-1", 
            FieldType.STRING, 1);
        pnd_Netchange_Record_Pnd_Nc_Hold_2 = pnd_Netchange_Record_Pnd_Nc_Hold_CodeRedef1.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Hold_2", "#NC-HOLD-2", 
            FieldType.STRING, 3);
        pnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind", "#NC-LABEL-HOLD-IND", 
            FieldType.STRING, 1);
        pnd_Netchange_Record_Pnd_Nc_Rqst_Id = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Rqst_Id", "#NC-RQST-ID", FieldType.STRING, 
            14);
        pnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2 = pnd_Netchange_Record.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2", "Redefines", pnd_Netchange_Record_Pnd_Nc_Rqst_Id);
        pnd_Netchange_Record_Pnd_Nc_Origin = pnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Origin", "#NC-ORIGIN", 
            FieldType.STRING, 5);
        pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr = pnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr", 
            "#NC-RQST-ID-NBR", FieldType.STRING, 6);
        pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Fil = pnd_Netchange_Record_Pnd_Nc_Rqst_IdRedef2.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Rqst_Id_Fil", 
            "#NC-RQST-ID-FIL", FieldType.STRING, 3);
        pnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code = pnd_Netchange_Record.newFieldArrayInGroup("pnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code", "#NC-GTN-REPORT-CODE", 
            FieldType.NUMERIC, 2, new DbsArrayController(1,10));
        pnd_Netchange_Record_Pnd_Nc_Name_Address = pnd_Netchange_Record.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Name_Address", "#NC-NAME-ADDRESS");
        pnd_Netchange_Record_Pnd_Nc_Full_Name = pnd_Netchange_Record_Pnd_Nc_Name_Address.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Full_Name", "#NC-FULL-NAME", 
            FieldType.STRING, 35);
        pnd_Netchange_Record_Pnd_Nc_Address_1 = pnd_Netchange_Record_Pnd_Nc_Name_Address.newFieldArrayInGroup("pnd_Netchange_Record_Pnd_Nc_Address_1", 
            "#NC-ADDRESS-1", FieldType.STRING, 35, new DbsArrayController(1,6));
        pnd_Netchange_Record_Pnd_Nc_Prev_Check_Date = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Check_Date", "#NC-PREV-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3 = pnd_Netchange_Record.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Prev_Check_Date);
        pnd_Netchange_Record_Pnd_Nc_Prev_Check_Mm = pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Check_Mm", 
            "#NC-PREV-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Prev_Check_Dd = pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Check_Dd", 
            "#NC-PREV-CHECK-DD", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Prev_Check_Cc = pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Check_Cc", 
            "#NC-PREV-CHECK-CC", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Prev_Check_Yy = pnd_Netchange_Record_Pnd_Nc_Prev_Check_DateRedef3.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Check_Yy", 
            "#NC-PREV-CHECK-YY", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Check_Date = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Check_Date", "#NC-CURR-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4 = pnd_Netchange_Record.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Curr_Check_Date);
        pnd_Netchange_Record_Pnd_Nc_Curr_Check_Mm = pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Check_Mm", 
            "#NC-CURR-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Check_Dd = pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Check_Dd", 
            "#NC-CURR-CHECK-DD", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Check_Cc = pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Check_Cc", 
            "#NC-CURR-CHECK-CC", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Check_Yy = pnd_Netchange_Record_Pnd_Nc_Curr_Check_DateRedef4.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Check_Yy", 
            "#NC-CURR-CHECK-YY", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2 = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2", "#NC-BIN-NBR-2", FieldType.STRING, 
            1);
        pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3 = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3", "#NC-BIN-NBR-3", FieldType.STRING, 
            1);
        pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4 = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4", "#NC-BIN-NBR-4", FieldType.STRING, 
            1);
        pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5 = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5", "#NC-BIN-NBR-5", FieldType.STRING, 
            1);
        pnd_Netchange_Record_Pnd_Nc_Postnet_Zip = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Postnet_Zip", "#NC-POSTNET-ZIP", FieldType.STRING, 
            14);
        pnd_Netchange_Record_Pnd_Nc_Zip_Code_5 = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Zip_Code_5", "#NC-ZIP-CODE-5", FieldType.STRING, 
            5);
        pnd_Netchange_Record_Pnd_Nc_Zip_Code_5Redef5 = pnd_Netchange_Record.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Zip_Code_5Redef5", "Redefines", 
            pnd_Netchange_Record_Pnd_Nc_Zip_Code_5);
        pnd_Netchange_Record_Pnd_Nc_Zip_Code_3 = pnd_Netchange_Record_Pnd_Nc_Zip_Code_5Redef5.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Zip_Code_3", 
            "#NC-ZIP-CODE-3", FieldType.STRING, 3);
        pnd_Netchange_Record_Pnd_Nc_Zip_Code_2 = pnd_Netchange_Record_Pnd_Nc_Zip_Code_5Redef5.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Zip_Code_2", 
            "#NC-ZIP-CODE-2", FieldType.STRING, 2);
        pnd_Netchange_Record_Pnd_Nc_Deduction_Count = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Deduction_Count", "#NC-DEDUCTION-COUNT", 
            FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Deductions = pnd_Netchange_Record.newGroupArrayInGroup("pnd_Netchange_Record_Pnd_Nc_Deductions", "#NC-DEDUCTIONS", 
            new DbsArrayController(1,10));
        pnd_Netchange_Record_Pnd_Nc_Deduction_Code = pnd_Netchange_Record_Pnd_Nc_Deductions.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Deduction_Code", 
            "#NC-DEDUCTION-CODE", FieldType.NUMERIC, 3);
        pnd_Netchange_Record_Pnd_Nc_Deduction_Desc = pnd_Netchange_Record_Pnd_Nc_Deductions.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Deduction_Desc", 
            "#NC-DEDUCTION-DESC", FieldType.STRING, 30);
        pnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt = pnd_Netchange_Record_Pnd_Nc_Deductions.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt", 
            "#NC-PREV-DED-AMT", FieldType.DECIMAL, 7,2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt = pnd_Netchange_Record_Pnd_Nc_Deductions.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt", 
            "#NC-CURR-DED-AMT", FieldType.DECIMAL, 7,2);
        pnd_Netchange_Record_Pnd_Nc_Ded_Chg_Flag = pnd_Netchange_Record_Pnd_Nc_Deductions.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Ded_Chg_Flag", 
            "#NC-DED-CHG-FLAG", FieldType.STRING, 1);
        pnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt", "#NC-PREV-FED-AMT", 
            FieldType.DECIMAL, 7,2);
        pnd_Netchange_Record_Pnd_Nc_Prev_St_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_St_Amt", "#NC-PREV-ST-AMT", FieldType.DECIMAL, 
            7,2);
        pnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt", "#NC-PREV-LOC-AMT", 
            FieldType.DECIMAL, 7,2);
        pnd_Netchange_Record_Pnd_Nc_Prev_Total_Deduction = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Prev_Total_Deduction", "#NC-PREV-TOTAL-DEDUCTION", 
            FieldType.DECIMAL, 9,2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt", "#NC-CURR-FED-AMT", 
            FieldType.DECIMAL, 7,2);
        pnd_Netchange_Record_Pnd_Nc_Curr_St_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_St_Amt", "#NC-CURR-ST-AMT", FieldType.DECIMAL, 
            7,2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt", "#NC-CURR-LOC-AMT", 
            FieldType.DECIMAL, 7,2);
        pnd_Netchange_Record_Pnd_Nc_Curr_Total_Deduction = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Curr_Total_Deduction", "#NC-CURR-TOTAL-DEDUCTION", 
            FieldType.DECIMAL, 9,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt", "#NC-FUND-PREV-NET-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt", "#NC-FUND-CURR-NET-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Count = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Count", "#NC-FUND-COUNT", FieldType.NUMERIC, 
            3);
        pnd_Netchange_Record_Pnd_Nc_Funds = pnd_Netchange_Record.newGroupArrayInGroup("pnd_Netchange_Record_Pnd_Nc_Funds", "#NC-FUNDS", new DbsArrayController(1,
            40));
        pnd_Netchange_Record_Pnd_Nc_Fund_Detail = pnd_Netchange_Record_Pnd_Nc_Funds.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Detail", "#NC-FUND-DETAIL", 
            FieldType.STRING, 161);
        pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6 = pnd_Netchange_Record_Pnd_Nc_Funds.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Fund_Detail);
        pnd_Netchange_Record_Pnd_Nc_Valuation_Period = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Valuation_Period", 
            "#NC-VALUATION-PERIOD", FieldType.STRING, 1);
        pnd_Netchange_Record_Pnd_Nc_Fund_Code = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Code", 
            "#NC-FUND-CODE", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Desc = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Desc", 
            "#NC-FUND-DESC", FieldType.STRING, 40);
        pnd_Netchange_Record_Pnd_Nc_Fund_Type = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Type", 
            "#NC-FUND-TYPE", FieldType.STRING, 1);
        pnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method", 
            "#NC-FUND-PAYMENT-METHOD", FieldType.STRING, 1);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units", 
            "#NC-FUND-PREV-UNITS", FieldType.DECIMAL, 9,3);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero", 
            "#NC-FUND-PREV-UNIT-ZERO", FieldType.STRING, 9);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_ZeroRedef7 = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_ZeroRedef7", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value = pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_ZeroRedef7.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value", 
            "#NC-FUND-PREV-UNIT-VALUE", FieldType.DECIMAL, 9,4);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt", 
            "#NC-FUND-PREV-CONTRACT-AMT", FieldType.DECIMAL, 9,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend", 
            "#NC-FUND-PREV-DIVIDEND", FieldType.DECIMAL, 9,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt", 
            "#NC-FUND-PREV-TOTAL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt", 
            "#NC-FUND-PREV-GROSS-AMT", FieldType.DECIMAL, 11,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units", 
            "#NC-FUND-CURR-UNITS", FieldType.DECIMAL, 9,3);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value", 
            "#NC-FUND-CURR-UNIT-VALUE", FieldType.DECIMAL, 9,4);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_ValueRedef8 = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_ValueRedef8", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Zero = pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_ValueRedef8.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Zero", 
            "#NC-FUND-CURR-UNIT-ZERO", FieldType.STRING, 9);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt", 
            "#NC-FUND-CURR-CONTRACT-AMT", FieldType.DECIMAL, 9,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend", 
            "#NC-FUND-CURR-DIVIDEND", FieldType.DECIMAL, 9,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt", 
            "#NC-FUND-CURR-TOTAL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt = pnd_Netchange_Record_Pnd_Nc_Fund_DetailRedef6.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt", 
            "#NC-FUND-CURR-GROSS-AMT", FieldType.DECIMAL, 11,2);
        pnd_Netchange_Record_Pnd_Nc_Messages_Ctr = pnd_Netchange_Record.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Messages_Ctr", "#NC-MESSAGES-CTR", 
            FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Messages = pnd_Netchange_Record.newGroupArrayInGroup("pnd_Netchange_Record_Pnd_Nc_Messages", "#NC-MESSAGES", new DbsArrayController(1,
            20));
        pnd_Netchange_Record_Pnd_Nc_Message_Number = pnd_Netchange_Record_Pnd_Nc_Messages.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Message_Number", 
            "#NC-MESSAGE-NUMBER", FieldType.STRING, 2);
        pnd_Netchange_Record_Pnd_Nc_Message_NumberRedef9 = pnd_Netchange_Record_Pnd_Nc_Messages.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Message_NumberRedef9", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Message_Number);
        pnd_Netchange_Record_Pnd_Nc_Message_Number_N = pnd_Netchange_Record_Pnd_Nc_Message_NumberRedef9.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Message_Number_N", 
            "#NC-MESSAGE-NUMBER-N", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Message_Date_1 = pnd_Netchange_Record_Pnd_Nc_Messages.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Message_Date_1", 
            "#NC-MESSAGE-DATE-1", FieldType.NUMERIC, 8);
        pnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10 = pnd_Netchange_Record_Pnd_Nc_Messages.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Message_Date_1);
        pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_1 = pnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_1", 
            "#NC-MSG-DATE-MM-1", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_1 = pnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_1", 
            "#NC-MSG-DATE-DD-1", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1 = pnd_Netchange_Record_Pnd_Nc_Message_Date_1Redef10.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_1", 
            "#NC-MSG-DATE-CCYY-1", FieldType.NUMERIC, 4);
        pnd_Netchange_Record_Pnd_Nc_Message_Date_2 = pnd_Netchange_Record_Pnd_Nc_Messages.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Message_Date_2", 
            "#NC-MESSAGE-DATE-2", FieldType.NUMERIC, 8);
        pnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11 = pnd_Netchange_Record_Pnd_Nc_Messages.newGroupInGroup("pnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11", 
            "Redefines", pnd_Netchange_Record_Pnd_Nc_Message_Date_2);
        pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_2 = pnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Msg_Date_Mm_2", 
            "#NC-MSG-DATE-MM-2", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_2 = pnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Msg_Date_Dd_2", 
            "#NC-MSG-DATE-DD-2", FieldType.NUMERIC, 2);
        pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2 = pnd_Netchange_Record_Pnd_Nc_Message_Date_2Redef11.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Msg_Date_Ccyy_2", 
            "#NC-MSG-DATE-CCYY-2", FieldType.NUMERIC, 4);
        pnd_Netchange_Record_Pnd_Nc_Message_Percent = pnd_Netchange_Record_Pnd_Nc_Messages.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Message_Percent", 
            "#NC-MESSAGE-PERCENT", FieldType.DECIMAL, 5,3);
        pnd_Netchange_Record_Pnd_Nc_Phone_Number = pnd_Netchange_Record_Pnd_Nc_Messages.newFieldInGroup("pnd_Netchange_Record_Pnd_Nc_Phone_Number", "#NC-PHONE-NUMBER", 
            FieldType.STRING, 14);

        this.setRecordName("LdaIaaa029b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaaa029b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
