/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:14 PM
**        * FROM NATURAL LDA     : IAAL140
************************************************************
**        * FILE NAME            : LdaIaal140.java
**        * CLASS NAME           : LdaIaal140
**        * INSTANCE NAME        : LdaIaal140
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal140 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr;
    private DbsGroup pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Record_Cde;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef2;
    private DbsField pnd_Input_Record_Pnd_W_Check_Date;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Input_Record_Pnd_Filler_1z;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill2;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dob;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dob;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill5;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef4;
    private DbsField pnd_Input_Record_Pnd_Filler_1a;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Actvty_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_3a;
    private DbsGroup pnd_Input_Record_Pnd_Cpr_Ivc_Info;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Company_Cd;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Percent;
    private DbsGroup pnd_Input_Record_Pnd_Cpr_Ivc_InfoRedef5;
    private DbsField pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Mode_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_Bnfcry_Xref;
    private DbsField pnd_Input_Record_Pnd_Bnfcry_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Pend_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Hold_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Pend_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef6;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Cde;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Ytd_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Tot_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Intent_Code;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Stp_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Final_Dte;
    private DbsGroup pnd_Input_Record_Pnd_Rest_Of_RecordRedef7;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Fund_Cde;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rtb_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Old_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Old_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Filler_1c;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Units_Cnt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record_2;
    private DbsGroup pnd_Output_Record;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr;
    private DbsGroup pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Ddctn_Per_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde;
    private DbsField pnd_W_Rec_3;

    public DbsGroup getPnd_Input_Record() { return pnd_Input_Record; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsGroup getPnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9() { return pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Payee_Cde() { return pnd_Input_Record_Pnd_Cntrct_Payee_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Record_Cde() { return pnd_Input_Record_Pnd_Record_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Rest_Of_Record() { return pnd_Input_Record_Pnd_Rest_Of_Record; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef2() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef2; }

    public DbsField getPnd_Input_Record_Pnd_W_Check_Date() { return pnd_Input_Record_Pnd_W_Check_Date; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef3() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef3; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Optn_Cde() { return pnd_Input_Record_Pnd_Cntrct_Optn_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1() { return pnd_Input_Record_Pnd_Filler_1; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Issue_Dte() { return pnd_Input_Record_Pnd_Cntrct_Issue_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1z() { return pnd_Input_Record_Pnd_Filler_1z; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Crrncy_Cde() { return pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Fill1() { return pnd_Input_Record_Pnd_Cntrct_Fill1; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I() { return pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Fill2() { return pnd_Input_Record_Pnd_Cntrct_Fill2; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_1st_Xref() { return pnd_Input_Record_Pnd_Cntrct_1st_Xref; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_1st_Dob() { return pnd_Input_Record_Pnd_Cntrct_1st_Dob; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Fill3() { return pnd_Input_Record_Pnd_Cntrct_Fill3; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex() { return pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt() { return pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_1st_Dod() { return pnd_Input_Record_Pnd_Cntrct_1st_Dod; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_2nd_Xref() { return pnd_Input_Record_Pnd_Cntrct_2nd_Xref; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_2nd_Dob() { return pnd_Input_Record_Pnd_Cntrct_2nd_Dob; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Fill5() { return pnd_Input_Record_Pnd_Cntrct_Fill5; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex() { return pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt() { return pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_2nd_Dod() { return pnd_Input_Record_Pnd_Cntrct_2nd_Dod; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn() { return pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef4() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef4; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1a() { return pnd_Input_Record_Pnd_Filler_1a; }

    public DbsField getPnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde() { return pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw() { return pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw; }

    public DbsField getPnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr() { return pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getPnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ() { return pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde() { return pnd_Input_Record_Pnd_Cntrct_Actvty_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Filler_3a() { return pnd_Input_Record_Pnd_Filler_3a; }

    public DbsGroup getPnd_Input_Record_Pnd_Cpr_Ivc_Info() { return pnd_Input_Record_Pnd_Cpr_Ivc_Info; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Company_Cd() { return pnd_Input_Record_Pnd_Cntrct_Company_Cd; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind() { return pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt() { return pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt() { return pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ivc_Amt() { return pnd_Input_Record_Pnd_Cntrct_Ivc_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt() { return pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Rtb_Amt() { return pnd_Input_Record_Pnd_Cntrct_Rtb_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Rtb_Percent() { return pnd_Input_Record_Pnd_Cntrct_Rtb_Percent; }

    public DbsGroup getPnd_Input_Record_Pnd_Cpr_Ivc_InfoRedef5() { return pnd_Input_Record_Pnd_Cpr_Ivc_InfoRedef5; }

    public DbsField getPnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha() { return pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Mode_Ind() { return pnd_Input_Record_Pnd_Cntrct_Mode_Ind; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte() { return pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Final_Per_Dte() { return pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte() { return pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Bnfcry_Xref() { return pnd_Input_Record_Pnd_Bnfcry_Xref; }

    public DbsField getPnd_Input_Record_Pnd_Bnfcry_Dod() { return pnd_Input_Record_Pnd_Bnfcry_Dod; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Pend_Cde() { return pnd_Input_Record_Pnd_Cntrct_Pend_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Hold_Cde() { return pnd_Input_Record_Pnd_Cntrct_Hold_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Pend_Dte() { return pnd_Input_Record_Pnd_Cntrct_Pend_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde() { return pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde() { return pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef6() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef6; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Id_Nbr() { return pnd_Input_Record_Pnd_Ddctn_Id_Nbr; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Cde() { return pnd_Input_Record_Pnd_Ddctn_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Seq_Nbr() { return pnd_Input_Record_Pnd_Ddctn_Seq_Nbr; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Payee() { return pnd_Input_Record_Pnd_Ddctn_Payee; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Per_Amt() { return pnd_Input_Record_Pnd_Ddctn_Per_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Ytd_Amt() { return pnd_Input_Record_Pnd_Ddctn_Ytd_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Pd_To_Dte() { return pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Tot_Amt() { return pnd_Input_Record_Pnd_Ddctn_Tot_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Intent_Code() { return pnd_Input_Record_Pnd_Ddctn_Intent_Code; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Strt_Dte() { return pnd_Input_Record_Pnd_Ddctn_Strt_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Stp_Dte() { return pnd_Input_Record_Pnd_Ddctn_Stp_Dte; }

    public DbsField getPnd_Input_Record_Pnd_Ddctn_Final_Dte() { return pnd_Input_Record_Pnd_Ddctn_Final_Dte; }

    public DbsGroup getPnd_Input_Record_Pnd_Rest_Of_RecordRedef7() { return pnd_Input_Record_Pnd_Rest_Of_RecordRedef7; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde() { return pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Fund_Cde() { return pnd_Input_Record_Pnd_Tiaa_Fund_Cde; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt() { return pnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Rtb_Amt() { return pnd_Input_Record_Pnd_Tiaa_Rtb_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt() { return pnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt() { return pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Old_Per_Amt() { return pnd_Input_Record_Pnd_Tiaa_Old_Per_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Old_Div_Amt() { return pnd_Input_Record_Pnd_Tiaa_Old_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Filler_1c() { return pnd_Input_Record_Pnd_Filler_1c; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt() { return pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt() { return pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Units_Cnt() { return pnd_Input_Record_Pnd_Tiaa_Units_Cnt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt() { return pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt() { return pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getPnd_Input_Record_Pnd_Rest_Of_Record_2() { return pnd_Input_Record_Pnd_Rest_Of_Record_2; }

    public DbsGroup getPnd_Output_Record() { return pnd_Output_Record; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Fund_Cde() { return pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr; }

    public DbsGroup getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6() { return pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Payee_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Issue_Dte() { return pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Mode_Ind() { return pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte() { return pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt() { return pnd_Output_Record_Pnd_O_Ddctn_Per_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde() { return pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Units_Cnt() { return pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt() { return pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getPnd_Output_Record_Pnd_O_Cntrct_Optn_Cde() { return pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde; }

    public DbsField getPnd_W_Rec_3() { return pnd_W_Rec_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Input_Record = newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1", "Redefines", pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1 = pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1", 
            "#CNTRCT-PPCN-NBR-1", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9 = pnd_Input_Record_Pnd_Cntrct_Ppcn_NbrRedef1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9", 
            "#CNTRCT-PPCN-NBR-9", FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_Record_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Record_Cde", "#RECORD-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            251);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef2 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef2", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_W_Check_Date = pnd_Input_Record_Pnd_Rest_Of_RecordRedef2.newFieldInGroup("pnd_Input_Record_Pnd_W_Check_Date", "#W-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef3", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Cntrct_Optn_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Filler_1 = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 
            4);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Filler_1z = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1z", "#FILLER-1Z", FieldType.STRING, 
            12);
        pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_Fill1 = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill1", "#CNTRCT-FILL1", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I", 
            "#CNTRCT-JOINT-CNVRT-RCRCD-I", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Fill2 = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill2", "#CNTRCT-FILL2", 
            FieldType.STRING, 11);
        pnd_Input_Record_Pnd_Cntrct_1st_Xref = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Xref", "#CNTRCT-1ST-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_1st_Dob = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dob", "#CNTRCT-1ST-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Cntrct_Fill3 = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill3", "#CNTRCT-FILL3", 
            FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex", 
            "#CNTRCT-1ST-ANNT-SEX", FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt", 
            "#CNTRCT-1ST-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_1st_Dod = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dod", "#CNTRCT-1ST-DOD", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Record_Pnd_Cntrct_2nd_Xref = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Xref", "#CNTRCT-2ND-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dob = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dob", "#CNTRCT-2ND-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Cntrct_Fill5 = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill5", "#CNTRCT-FILL5", 
            FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex", 
            "#CNTRCT-2ND-ANNT-SEX", FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt", 
            "#CNTRCT-2ND-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dod = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dod", "#CNTRCT-2ND-DOD", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn = pnd_Input_Record_Pnd_Rest_Of_RecordRedef3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn", 
            "#CNTRCT-2ND-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef4", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Filler_1a = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1a", "#FILLER-1A", FieldType.STRING, 
            22);
        pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde", 
            "#PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw", "#PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr", 
            "#PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ", 
            "#PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Actvty_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Actvty_Cde", "#CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Filler_3a = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_3a", "#FILLER-3A", FieldType.STRING, 
            5);
        pnd_Input_Record_Pnd_Cpr_Ivc_Info = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newGroupInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_Info", "#CPR-IVC-INFO");
        pnd_Input_Record_Pnd_Cntrct_Company_Cd = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Company_Cd", "#CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind", 
            "#CNTRCT-RCVRY-TYPE-IND", FieldType.STRING, 1, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt", 
            "#CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt", 
            "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Amt", "#CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Percent = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Percent", "#CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7,4, new DbsArrayController(1,5));
        pnd_Input_Record_Pnd_Cpr_Ivc_InfoRedef5 = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newGroupInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_InfoRedef5", 
            "Redefines", pnd_Input_Record_Pnd_Cpr_Ivc_Info);
        pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha = pnd_Input_Record_Pnd_Cpr_Ivc_InfoRedef5.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha", "#CPR-IVC-INFO-ALPHA", 
            FieldType.STRING, 155);
        pnd_Input_Record_Pnd_Cntrct_Mode_Ind = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Mode_Ind", "#CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3);
        pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte", 
            "#CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte", 
            "#CNTRCT-FINAL-PER-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte", 
            "#CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Bnfcry_Xref = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Bnfcry_Xref", "#BNFCRY-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Bnfcry_Dod = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Bnfcry_Dod", "#BNFCRY-DOD", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Pend_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Pend_Cde", "#CNTRCT-PEND-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Hold_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Hold_Cde", "#CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Pend_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Pend_Dte", "#CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde", 
            "#CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde", 
            "#CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef6", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Ddctn_Id_Nbr = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Id_Nbr", "#DDCTN-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Ddctn_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_Ddctn_Seq_Nbr = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Input_Record_Pnd_Ddctn_Payee = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", 
            FieldType.STRING, 5);
        pnd_Input_Record_Pnd_Ddctn_Per_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", 
            FieldType.DECIMAL, 7,2);
        pnd_Input_Record_Pnd_Ddctn_Ytd_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Ytd_Amt", "#DDCTN-YTD-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte", "#DDCTN-PD-TO-DTE", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Ddctn_Tot_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Tot_Amt", "#DDCTN-TOT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Ddctn_Intent_Code = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Intent_Code", "#DDCTN-INTENT-CODE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Ddctn_Strt_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Ddctn_Stp_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Ddctn_Final_Dte = pnd_Input_Record_Pnd_Rest_Of_RecordRedef6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Final_Dte", "#DDCTN-FINAL-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Rest_Of_RecordRedef7 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record_Pnd_Rest_Of_RecordRedef7", "Redefines", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde", "#TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Tiaa_Fund_Cde = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Fund_Cde", "#TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt", "#TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Rtb_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rtb_Amt", "#TIAA-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt", "#TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt", "#TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Old_Per_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Old_Per_Amt", "#TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Old_Div_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Old_Div_Amt", "#TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Filler_1c = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1c", "#FILLER-1C", FieldType.STRING, 
            6);
        pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt", "#TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt", "#TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Units_Cnt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Units_Cnt", "#TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9,3);
        pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt", 
            "#TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt = pnd_Input_Record_Pnd_Rest_Of_RecordRedef7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt", 
            "#TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Record_Pnd_Rest_Of_Record_2 = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Rest_Of_Record_2", "#REST-OF-RECORD-2", FieldType.STRING, 
            63);

        pnd_Output_Record = newGroupInRecord("pnd_Output_Record", "#OUTPUT-RECORD");
        pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Fund_Cde", "#O-TIAA-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr", "#O-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8 = pnd_Output_Record.newGroupInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8", "Redefines", 
            pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3 = pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3", 
            "#O-CNTRCT-PPCN-NBR-1-3", FieldType.STRING, 3);
        pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6 = pnd_Output_Record_Pnd_O_Cntrct_Ppcn_NbrRedef8.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_6", 
            "#O-CNTRCT-PPCN-NBR-6", FieldType.STRING, 6);
        pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Payee_Cde", "#O-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde", "#O-CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Issue_Dte", "#O-CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Mode_Ind", "#O-CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte", "#O-CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde", "#O-CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Output_Record_Pnd_O_Ddctn_Per_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Ddctn_Per_Amt", "#O-DDCTN-PER-AMT", FieldType.NUMERIC, 
            5);
        pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde", "#O-TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt", "#O-TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt", "#O-TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Units_Cnt", "#O-TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt", "#O-TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt", "#O-TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_O_Cntrct_Optn_Cde", "#O-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2);

        pnd_W_Rec_3 = newFieldInRecord("pnd_W_Rec_3", "#W-REC-3", FieldType.NUMERIC, 8);

        this.setRecordName("LdaIaal140");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaal140() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
