/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:36 PM
**        * FROM NATURAL LDA     : IATL300
************************************************************
**        * FILE NAME            : LdaIatl300.java
**        * CLASS NAME           : LdaIatl300
**        * INSTANCE NAME        : LdaIatl300
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIatl300 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Status_1;
    private DbsField pnd_Status_1_Pnd_Awaiting_Factor;
    private DbsField pnd_Status_1_Pnd_Complete;
    private DbsField pnd_Status_1_Pnd_Delayed;
    private DbsField pnd_Status_1_Pnd_Deleted;
    private DbsField pnd_Status_1_Pnd_Pending_Appl;
    private DbsField pnd_Status_1_Pnd_Rejected;
    private DbsField pnd_Status_1_Pnd_Retro_Active;
    private DbsField pnd_Status_1_Pnd_Supvr_Ovrd;

    public DbsGroup getPnd_Status_1() { return pnd_Status_1; }

    public DbsField getPnd_Status_1_Pnd_Awaiting_Factor() { return pnd_Status_1_Pnd_Awaiting_Factor; }

    public DbsField getPnd_Status_1_Pnd_Complete() { return pnd_Status_1_Pnd_Complete; }

    public DbsField getPnd_Status_1_Pnd_Delayed() { return pnd_Status_1_Pnd_Delayed; }

    public DbsField getPnd_Status_1_Pnd_Deleted() { return pnd_Status_1_Pnd_Deleted; }

    public DbsField getPnd_Status_1_Pnd_Pending_Appl() { return pnd_Status_1_Pnd_Pending_Appl; }

    public DbsField getPnd_Status_1_Pnd_Rejected() { return pnd_Status_1_Pnd_Rejected; }

    public DbsField getPnd_Status_1_Pnd_Retro_Active() { return pnd_Status_1_Pnd_Retro_Active; }

    public DbsField getPnd_Status_1_Pnd_Supvr_Ovrd() { return pnd_Status_1_Pnd_Supvr_Ovrd; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Status_1 = newGroupInRecord("pnd_Status_1", "#STATUS-1");
        pnd_Status_1_Pnd_Awaiting_Factor = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Awaiting_Factor", "#AWAITING-FACTOR", FieldType.STRING, 1);
        pnd_Status_1_Pnd_Complete = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Complete", "#COMPLETE", FieldType.STRING, 1);
        pnd_Status_1_Pnd_Delayed = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Delayed", "#DELAYED", FieldType.STRING, 1);
        pnd_Status_1_Pnd_Deleted = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Deleted", "#DELETED", FieldType.STRING, 1);
        pnd_Status_1_Pnd_Pending_Appl = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Pending_Appl", "#PENDING-APPL", FieldType.STRING, 1);
        pnd_Status_1_Pnd_Rejected = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Rejected", "#REJECTED", FieldType.STRING, 1);
        pnd_Status_1_Pnd_Retro_Active = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Retro_Active", "#RETRO-ACTIVE", FieldType.STRING, 1);
        pnd_Status_1_Pnd_Supvr_Ovrd = pnd_Status_1.newFieldInGroup("pnd_Status_1_Pnd_Supvr_Ovrd", "#SUPVR-OVRD", FieldType.STRING, 1);

        this.setRecordName("LdaIatl300");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Status_1_Pnd_Awaiting_Factor.setInitialValue("F");
        pnd_Status_1_Pnd_Complete.setInitialValue("M");
        pnd_Status_1_Pnd_Delayed.setInitialValue("E");
        pnd_Status_1_Pnd_Deleted.setInitialValue("N");
        pnd_Status_1_Pnd_Pending_Appl.setInitialValue("C");
        pnd_Status_1_Pnd_Rejected.setInitialValue("P");
        pnd_Status_1_Pnd_Retro_Active.setInitialValue("R");
        pnd_Status_1_Pnd_Supvr_Ovrd.setInitialValue("B");
    }

    // Constructor
    public LdaIatl300() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
