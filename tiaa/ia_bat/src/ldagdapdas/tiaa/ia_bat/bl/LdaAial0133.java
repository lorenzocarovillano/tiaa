/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:25 PM
**        * FROM NATURAL LDA     : AIAL0133
************************************************************
**        * FILE NAME            : LdaAial0133.java
**        * CLASS NAME           : LdaAial0133
**        * INSTANCE NAME        : LdaAial0133
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0133 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Cref_Rea_Indicator_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Reval_Switch_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Contract_Payee_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Mode_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Option_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Issue_Date_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Final_Per_Pay_Date_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dob_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Sex_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dod_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dob_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Sex_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dod_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Effective_Date_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Type_Of_Run_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Revaluation_Indicator_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Processing_Date_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Illustration_Eff_Date_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Units_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler6;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Origin_S1_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_OutRedef2;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S2_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S2_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S2_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler7;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S3_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S3_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S3_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Next_Pymnt_Dte_S3_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Return_Code_S3_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler8;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_OutRedef4;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S4_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S4_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S4_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S4_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler9;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_OutRedef5;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S5_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S5_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S5_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S5_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler10;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S6_Out;
    private DbsGroup pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmt_Method_Code_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Rate_Code_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Gtd_Pmt_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Dvd_Pmt_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Tiaa_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Total_Transfer_Amt_Out_S6_Out;
    private DbsField pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler11;
    private DbsField pnd_Iaxfr_Actrl_Prcss_Dte_Out;

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out; }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Cref_Rea_Indicator_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Cref_Rea_Indicator_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Reval_Switch_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Reval_Switch_S1_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Contract_Payee_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Contract_Payee_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Mode_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Mode_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Option_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Option_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Issue_Date_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Issue_Date_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Final_Per_Pay_Date_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Final_Per_Pay_Date_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dob_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dob_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Sex_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Sex_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dod_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dod_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dob_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dob_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Sex_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Sex_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dod_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dod_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Effective_Date_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Effective_Date_S1_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Type_Of_Run_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Type_Of_Run_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Revaluation_Indicator_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Revaluation_Indicator_S1_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Processing_Date_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Processing_Date_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Illustration_Eff_Date_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Illustration_Eff_Date_S1_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Units_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Units_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler6() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler6; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Origin_S1_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Origin_S1_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out; }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_OutRedef2() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_OutRedef2; 
        }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S2_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S2_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S2_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S2_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S2_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S2_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler7() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler7; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out; }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3; 
        }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S3_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S3_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S3_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Next_Pymnt_Dte_S3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Next_Pymnt_Dte_S3_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Return_Code_S3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Return_Code_S3_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler8() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler8; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out; }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_OutRedef4() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_OutRedef4; 
        }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S4_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S4_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S4_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S4_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S4_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S4_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S4_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S4_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler9() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler9; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out; }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_OutRedef5() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_OutRedef5; 
        }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S5_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S5_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S5_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S5_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S5_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S5_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S5_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S5_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler10() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler10; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out; }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6; 
        }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S6_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S6_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S6_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S6_Out; 
        }

    public DbsGroup getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmt_Method_Code_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmt_Method_Code_Out_S6_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Rate_Code_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Rate_Code_Out_S6_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Gtd_Pmt_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Gtd_Pmt_Out_S6_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Dvd_Pmt_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Dvd_Pmt_Out_S6_Out; }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Tiaa_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Tiaa_S6_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Total_Transfer_Amt_Out_S6_Out() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Total_Transfer_Amt_Out_S6_Out; 
        }

    public DbsField getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler11() { return pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler11; }

    public DbsField getPnd_Iaxfr_Actrl_Prcss_Dte_Out() { return pnd_Iaxfr_Actrl_Prcss_Dte_Out; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iaa_Xfr_Actrl_Rcrd_Out = newGroupInRecord("pnd_Iaa_Xfr_Actrl_Rcrd_Out", "#IAA-XFR-ACTRL-RCRD-OUT");
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out", 
            "#IAXFR-ACTRL-FLD-1-OUT", FieldType.STRING, 250);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1 = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newGroupInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1", 
            "Redefines", pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Cref_Rea_Indicator_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Cref_Rea_Indicator_S1_Out", 
            "#CREF-REA-INDICATOR-S1-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Reval_Switch_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Reval_Switch_S1_Out", 
            "#TRANSFER-REVAL-SWITCH-S1-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Contract_Payee_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Contract_Payee_S1_Out", 
            "#CONTRACT-PAYEE-S1-OUT", FieldType.STRING, 10);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_S1_Out", 
            "#FUND-CODE-S1-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Mode_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Mode_S1_Out", 
            "#MODE-S1-OUT", FieldType.NUMERIC, 3);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Option_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Option_S1_Out", 
            "#OPTION-S1-OUT", FieldType.NUMERIC, 2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Issue_Date_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Issue_Date_S1_Out", 
            "#ISSUE-DATE-S1-OUT", FieldType.NUMERIC, 6);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Final_Per_Pay_Date_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Final_Per_Pay_Date_S1_Out", 
            "#FINAL-PER-PAY-DATE-S1-OUT", FieldType.NUMERIC, 6);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dob_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dob_S1_Out", 
            "#FIRST-ANN-DOB-S1-OUT", FieldType.NUMERIC, 8);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Sex_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Sex_S1_Out", 
            "#FIRST-ANN-SEX-S1-OUT", FieldType.NUMERIC, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dod_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_First_Ann_Dod_S1_Out", 
            "#FIRST-ANN-DOD-S1-OUT", FieldType.NUMERIC, 6);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dob_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dob_S1_Out", 
            "#SECOND-ANN-DOB-S1-OUT", FieldType.NUMERIC, 8);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Sex_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Sex_S1_Out", 
            "#SECOND-ANN-SEX-S1-OUT", FieldType.NUMERIC, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dod_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Second_Ann_Dod_S1_Out", 
            "#SECOND-ANN-DOD-S1-OUT", FieldType.NUMERIC, 6);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Effective_Date_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Effective_Date_S1_Out", 
            "#TRANSFER-EFFECTIVE-DATE-S1-OUT", FieldType.NUMERIC, 8);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Type_Of_Run_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Type_Of_Run_S1_Out", 
            "#TYPE-OF-RUN-S1-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Revaluation_Indicator_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Revaluation_Indicator_S1_Out", 
            "#REVALUATION-INDICATOR-S1-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Processing_Date_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Processing_Date_S1_Out", 
            "#PROCESSING-DATE-S1-OUT", FieldType.NUMERIC, 8);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Illustration_Eff_Date_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Illustration_Eff_Date_S1_Out", 
            "#ILLUSTRATION-EFF-DATE-S1-OUT", FieldType.NUMERIC, 8);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Units_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Units_S1_Out", 
            "#TRANSFER-UNITS-S1-OUT", FieldType.DECIMAL, 9,3);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler6 = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler6", 
            "#FILLER6", FieldType.STRING, 153);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Origin_S1_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_OutRedef1.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Origin_S1_Out", 
            "#ORIGIN-S1-OUT", FieldType.NUMERIC, 2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out", 
            "#IAXFR-ACTRL-FLD-2-OUT", FieldType.STRING, 253);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_OutRedef2 = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newGroupInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_OutRedef2", 
            "Redefines", pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_OutRedef2.newGroupArrayInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out", 
            "#TRANSFER-TO-ARRAYS-S2-OUT", new DbsArrayController(1,11));
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S2_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S2_Out", 
            "#FUND-TO-RECEIVE-S2-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S2_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S2_Out", 
            "#UNITS-TO-RECEIVE-S2-OUT", FieldType.DECIMAL, 9,3);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S2_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S2_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S2_Out", 
            "#PMTS-TO-RECEIVE-S2-OUT", FieldType.DECIMAL, 11,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler7 = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_OutRedef2.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler7", 
            "#FILLER7", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out", 
            "#IAXFR-ACTRL-FLD-3-OUT", FieldType.STRING, 250);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3 = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newGroupInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3", 
            "Redefines", pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3.newGroupArrayInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out", 
            "#TRANSFER-TO-ARRAYS-S3-OUT", new DbsArrayController(1,10));
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_To_Receive_S3_Out", 
            "#FUND-TO-RECEIVE-S3-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_To_Receive_S3_Out", 
            "#UNITS-TO-RECEIVE-S3-OUT", FieldType.DECIMAL, 9,3);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_To_Arrays_S3_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmts_To_Receive_S3_Out", 
            "#PMTS-TO-RECEIVE-S3-OUT", FieldType.DECIMAL, 11,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Next_Pymnt_Dte_S3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Next_Pymnt_Dte_S3_Out", 
            "#NEXT-PYMNT-DTE-S3-OUT", FieldType.DATE);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Return_Code_S3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Return_Code_S3_Out", 
            "#RETURN-CODE-S3-OUT", FieldType.NUMERIC, 2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out", 
            "#TRANSFER-EFF-DATE-OUT-S3-OUT", FieldType.NUMERIC, 8);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler8 = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_OutRedef3.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler8", 
            "#FILLER8", FieldType.STRING, 26);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out", 
            "#IAXFR-ACTRL-FLD-4-OUT", FieldType.STRING, 250);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_OutRedef4 = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newGroupInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_OutRedef4", 
            "Redefines", pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_OutRedef4.newGroupArrayInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out", 
            "#TRANSFER-OUT-CREF-ARRAYS-S4-OUT", new DbsArrayController(1,8));
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S4_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S4_Out", 
            "#FUND-CODE-OUT-S4-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S4_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S4_Out", 
            "#UNITS-OUT-S4-OUT", FieldType.DECIMAL, 9,3);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S4_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S4_Out", 
            "#AUV-OUT-S4-OUT", FieldType.DECIMAL, 8,4);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S4_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S4_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S4_Out", 
            "#TRANSFER-AMT-OUT-CREF-S4-OUT", FieldType.DECIMAL, 13,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler9 = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_OutRedef4.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler9", 
            "#FILLER9", FieldType.STRING, 2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out", 
            "#IAXFR-ACTRL-FLD-5-OUT", FieldType.STRING, 250);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_OutRedef5 = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newGroupInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_OutRedef5", 
            "Redefines", pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_OutRedef5.newGroupArrayInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out", 
            "#TRANSFER-OUT-CREF-ARRAYS-S5-OUT", new DbsArrayController(1,8));
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S5_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S5_Out", 
            "#FUND-CODE-OUT-S5-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S5_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S5_Out", 
            "#UNITS-OUT-S5-OUT", FieldType.DECIMAL, 9,3);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S5_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S5_Out", 
            "#AUV-OUT-S5-OUT", FieldType.DECIMAL, 8,4);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S5_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S5_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S5_Out", 
            "#TRANSFER-AMT-OUT-CREF-S5-OUT", FieldType.DECIMAL, 13,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler10 = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_OutRedef5.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler10", 
            "#FILLER10", FieldType.STRING, 2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out", 
            "#IAXFR-ACTRL-FLD-6-OUT", FieldType.STRING, 250);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6 = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newGroupInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6", 
            "Redefines", pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6.newGroupArrayInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out", 
            "#TRANSFER-OUT-CREF-ARRAYS-S6-OUT", new DbsArrayController(1,3));
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Fund_Code_Out_S6_Out", 
            "#FUND-CODE-OUT-S6-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Units_Out_S6_Out", 
            "#UNITS-OUT-S6-OUT", FieldType.DECIMAL, 9,3);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Auv_Out_S6_Out", 
            "#AUV-OUT-S6-OUT", FieldType.DECIMAL, 8,4);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Cref_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Cref_S6_Out", 
            "#TRANSFER-AMT-OUT-CREF-S6-OUT", FieldType.DECIMAL, 13,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6.newGroupArrayInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out", 
            "#TRANSFER-OUT-TIAA-ARRAYS-S6-OUT", new DbsArrayController(1,2));
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmt_Method_Code_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Pmt_Method_Code_Out_S6_Out", 
            "#PMT-METHOD-CODE-OUT-S6-OUT", FieldType.STRING, 1);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Rate_Code_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Rate_Code_Out_S6_Out", 
            "#RATE-CODE-OUT-S6-OUT", FieldType.STRING, 2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Gtd_Pmt_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Gtd_Pmt_Out_S6_Out", 
            "#GTD-PMT-OUT-S6-OUT", FieldType.DECIMAL, 9,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Dvd_Pmt_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Out_Tiaa_Arrays_S6_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Dvd_Pmt_Out_S6_Out", 
            "#DVD-PMT-OUT-S6-OUT", FieldType.DECIMAL, 9,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Tiaa_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_OutRedef6.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Amt_Out_Tiaa_S6_Out", 
            "#TRANSFER-AMT-OUT-TIAA-S6-OUT", FieldType.DECIMAL, 13,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Total_Transfer_Amt_Out_S6_Out = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Total_Transfer_Amt_Out_S6_Out", 
            "#TOTAL-TRANSFER-AMT-OUT-S6-OUT", FieldType.DECIMAL, 14,2);
        pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler11 = pnd_Iaa_Xfr_Actrl_Rcrd_Out.newFieldInGroup("pnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Filler11", "#FILLER11", FieldType.STRING, 
            75);

        pnd_Iaxfr_Actrl_Prcss_Dte_Out = newFieldInRecord("pnd_Iaxfr_Actrl_Prcss_Dte_Out", "#IAXFR-ACTRL-PRCSS-DTE-OUT", FieldType.NUMERIC, 8);

        this.setRecordName("LdaAial0133");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAial0133() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
