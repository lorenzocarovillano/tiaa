/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:03:48 PM
**        * FROM NATURAL LDA     : IAAL920
************************************************************
**        * FILE NAME            : LdaIaal920.java
**        * CLASS NAME           : LdaIaal920
**        * INSTANCE NAME        : LdaIaal920
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal920 extends DbsRecord
{
    // Properties
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Program_Id;
    private DbsField iaa_Parm_Card_Pnd_Filler1;
    private DbsField iaa_Parm_Card_Pnd_Parm_Check_Dte;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Check_DteRedef1;
    private DbsField iaa_Parm_Card_Pnd_Parm_Check_Dte_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Check_DteRedef2;
    private DbsField iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd;
    private DbsField iaa_Parm_Card_Pnd_Filler2;
    private DbsField iaa_Parm_Card_Pnd_Parm_From_Dte;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_From_DteRedef3;
    private DbsField iaa_Parm_Card_Pnd_Parm_From_Dte_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_From_DteRedef4;
    private DbsField iaa_Parm_Card_Pnd_Parm_From_Dte_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_From_Dte_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_From_Dte_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_From_Dte_Dd;
    private DbsField iaa_Parm_Card_Pnd_Filler3;
    private DbsField iaa_Parm_Card_Pnd_Parm_To_Dte;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_To_DteRedef5;
    private DbsField iaa_Parm_Card_Pnd_Parm_To_Dte_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_To_DteRedef6;
    private DbsField iaa_Parm_Card_Pnd_Parm_To_Dte_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_To_Dte_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_To_Dte_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_To_Dte_Dd;
    private DbsField iaa_Parm_Card_Pnd_Filler4;
    private DbsField iaa_Parm_Card_Pnd_Parm_User_Area;
    private DbsField iaa_Parm_Card_Pnd_Filler5;
    private DbsField iaa_Parm_Card_Pnd_Parm_User_Id;
    private DbsField iaa_Parm_Card_Pnd_Filler6;
    private DbsField iaa_Parm_Card_Pnd_Parm_Verify_Id;
    private DbsField iaa_Parm_Card_Pnd_Filler7;
    private DbsField iaa_Parm_Card_Pnd_Parm_Verify_Cde;
    private DbsField iaa_Parm_Card_Pnd_Filler8;
    private DbsField iaa_Parm_Card_Pnd_Parm_Totals;
    private DbsField pnd_Input_Rec;
    private DbsGroup pnd_Input_RecRedef7;
    private DbsField pnd_Input_Rec_Pnd_Check_Dte;
    private DbsField pnd_Input_Rec_Pnd_User_Area;
    private DbsField pnd_Input_Rec_Pnd_User_Id;
    private DbsField pnd_Input_Rec_Pnd_Trans_Dte;
    private DbsField pnd_Input_Rec_Pnd_Trans_Cde;
    private DbsGroup pnd_Input_Rec_Pnd_Trans_CdeRedef8;
    private DbsField pnd_Input_Rec_Pnd_Trans_Cde_N;
    private DbsField pnd_Input_Rec_Pnd_Contract_Nbr;
    private DbsField pnd_Input_Rec_Pnd_Payee;
    private DbsField pnd_Input_Rec_Pnd_Xref;
    private DbsField pnd_Input_Rec_Pnd_Record_Nbr;
    private DbsField pnd_Input_Rec_Pnd_Rest_Of_Input;
    private DbsField pnd_Input_Rec_Pnd_Multiple;
    private DbsField pnd_Input_Rec_Pnd_Seq_Nbr;
    private DbsField pnd_Input_Rec_Pnd_Verify;
    private DbsField pnd_Input_Rec_Pnd_Mode;
    private DbsField pnd_Input_Rec_Pnd_Filler1;
    private DbsField pnd_Output_Rec;
    private DbsGroup pnd_Output_RecRedef9;
    private DbsField pnd_Output_Rec_Pnd_Check_Dte;
    private DbsField pnd_Output_Rec_Pnd_User_Area;
    private DbsField pnd_Output_Rec_Pnd_User_Id;
    private DbsField pnd_Output_Rec_Pnd_Trans_Dte;
    private DbsField pnd_Output_Rec_Pnd_Trans_Cde;
    private DbsGroup pnd_Output_Rec_Pnd_Trans_CdeRedef10;
    private DbsField pnd_Output_Rec_Pnd_Trans_Cde_N;
    private DbsField pnd_Output_Rec_Pnd_Contract_Nbr;
    private DbsField pnd_Output_Rec_Pnd_Payee;
    private DbsField pnd_Output_Rec_Pnd_Xref;
    private DbsField pnd_Output_Rec_Pnd_Record_Nbr;
    private DbsField pnd_Output_Rec_Pnd_Rest_Of_Output;
    private DbsField pnd_Output_Rec_Pnd_Multiple;
    private DbsField pnd_Output_Rec_Pnd_Seq_Nbr;
    private DbsField pnd_Output_Rec_Pnd_Verify;
    private DbsField pnd_Output_Rec_Pnd_Mode;
    private DbsField pnd_Output_Rec_Pnd_New_Trans;
    private DbsField pnd_Misc_Non_Tax_Trans;
    private DbsGroup pnd_Misc_Non_Tax_TransRedef11;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_User_Area;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_User_Id;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_Trans_DteRedef13;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_N;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Cc;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Dd;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Record_Status;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef15;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef17;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef18;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef20;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef21;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Filler1;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Multiple;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Seq_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Verify;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Filler2;
    private DbsField pnd_Selection_Rcrd;
    private DbsGroup pnd_Selection_RcrdRedef22;
    private DbsField pnd_Selection_Rcrd_Pnd_Manual_New_Issue1;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte1;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Check_Dte1Redef24;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Mm1;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Dd1;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Yy1;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Area1;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Id1;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte1;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte1_Cc;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte1_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte1_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte1_Dd;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef26;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte1_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Nbr1;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1Redef27;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N1;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N7;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Status1;
    private DbsField pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1Redef28;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Type1;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Nbr1;
    private DbsField pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29;
    private DbsField pnd_Selection_Rcrd_Pnd_Product;
    private DbsField pnd_Selection_Rcrd_Pnd_Currency;
    private DbsGroup pnd_Selection_Rcrd_Pnd_CurrencyRedef30;
    private DbsField pnd_Selection_Rcrd_Pnd_Currency_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Mode;
    private DbsGroup pnd_Selection_Rcrd_Pnd_ModeRedef31;
    private DbsField pnd_Selection_Rcrd_Pnd_Mode_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Pend_Code;
    private DbsField pnd_Selection_Rcrd_Pnd_Hold_Check_Cde;
    private DbsField pnd_Selection_Rcrd_Pnd_Pend_Dte;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Pend_DteRedef32;
    private DbsField pnd_Selection_Rcrd_Pnd_Pend_Dte_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Pend_Dte_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Option;
    private DbsGroup pnd_Selection_Rcrd_Pnd_OptionRedef33;
    private DbsField pnd_Selection_Rcrd_Pnd_Option_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Origin;
    private DbsField pnd_Selection_Rcrd_Pnd_Iss_Dte;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Iss_DteRedef34;
    private DbsField pnd_Selection_Rcrd_Pnd_Iss_Dte_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Iss_Dte_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte;
    private DbsGroup pnd_Selection_Rcrd_Pnd_1st_Pay_Due_DteRedef35;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_DteRedef36;
    private DbsField pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_DteRedef37;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Wdrawal_Dte;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Wdrawal_DteRedef39;
    private DbsField pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Filler1;
    private DbsField pnd_Selection_Rcrd_Pnd_Multiple1;
    private DbsField pnd_Selection_Rcrd_Pnd_Seq_Nbr1;
    private DbsField pnd_Selection_Rcrd_Pnd_Verify1;
    private DbsField pnd_Selection_Rcrd_Pnd_Mode1;
    private DbsField pnd_Selection_Rcrd_Pnd_New_Trans;
    private DbsGroup pnd_Selection_RcrdRedef40;
    private DbsField pnd_Selection_Rcrd_Pnd_Manual_New_Issue2;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte2;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Check_Dte2Redef42;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Mm2;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Dd2;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Yy2;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Area2;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Id2;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte2;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Trans_Dte2Redef43;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte2_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Nbr2;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Nbr2;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Status2;
    private DbsField pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr2;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Type2;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Nbr2;
    private DbsField pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44;
    private DbsField pnd_Selection_Rcrd_Pnd_Citizen;
    private DbsField pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss;
    private DbsField pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Res;
    private DbsField pnd_Selection_Rcrd_Pnd_Coll_Iss;
    private DbsField pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_AmtRedef45;
    private DbsField pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt_A;
    private DbsField pnd_Selection_Rcrd_Pnd_Ssn;
    private DbsField pnd_Selection_Rcrd_Pnd_Joint_Cnvrt;
    private DbsField pnd_Selection_Rcrd_Pnd_Spirt;
    private DbsField pnd_Selection_Rcrd_Pnd_Pen_Pln_Cde;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Type;
    private DbsGroup pnd_Selection_RcrdRedef46;
    private DbsField pnd_Selection_Rcrd_Pnd_Manual_New_Issue3;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte3;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Check_Dte3Redef48;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Mm3;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Dd3;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Yy3;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Area3;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Id3;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte3;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Trans_Dte3Redef49;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte3_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Nbr3;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Nbr3;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Status3;
    private DbsField pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr3;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Type3;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Nbr3;
    private DbsField pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref;
    private DbsGroup pnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Last_Name;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_First_Init;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Mid_Init;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Sex;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Dob;
    private DbsGroup pnd_Selection_Rcrd_Pnd_1st_DobRedef52;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Dob_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Dob_Dd;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Dob_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Dod;
    private DbsGroup pnd_Selection_Rcrd_Pnd_1st_DodRedef53;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Dod_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_1st_Dod_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Mort_Yob3;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Mort_Yob3Redef54;
    private DbsField pnd_Selection_Rcrd_Pnd_Mort_Yob3_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Life_Cnt3;
    private DbsField pnd_Selection_Rcrd_Pnd_Div_Payee;
    private DbsField pnd_Selection_Rcrd_Pnd_Coll_Cde;
    private DbsField pnd_Selection_Rcrd_Pnd_Orig_Cntrct;
    private DbsField pnd_Selection_Rcrd_Pnd_Cash_Cde;
    private DbsField pnd_Selection_Rcrd_Pnd_Emp_Term;
    private DbsGroup pnd_Selection_RcrdRedef55;
    private DbsField pnd_Selection_Rcrd_Pnd_Manual_New_Issue4;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte4;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Check_Dte4Redef57;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Mm4;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Dd4;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Yy4;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Area4;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Id4;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte4;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Trans_Dte4Redef58;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte4_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Nbr4;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Nbr4;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Status4;
    private DbsField pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr4;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Type4;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Nbr4;
    private DbsField pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref;
    private DbsGroup pnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Last_Name;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_First_Init;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Mid_Init;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Sex;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Dob;
    private DbsGroup pnd_Selection_Rcrd_Pnd_2nd_DobRedef61;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Dob_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Dob_Dd;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Dob_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Dod;
    private DbsGroup pnd_Selection_Rcrd_Pnd_2nd_DodRedef62;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Dod_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_2nd_Dod_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Mort_Yob4;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Mort_Yob4Redef63;
    private DbsField pnd_Selection_Rcrd_Pnd_Mort_Yob4_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Life_Cnt4;
    private DbsField pnd_Selection_Rcrd_Pnd_Ben_Xref;
    private DbsField pnd_Selection_Rcrd_Pnd_Ben_Dod;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Ben_DodRedef64;
    private DbsField pnd_Selection_Rcrd_Pnd_Ben_Dod_A;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Ben_DodRedef65;
    private DbsField pnd_Selection_Rcrd_Pnd_Ben_Dod_Mm;
    private DbsField pnd_Selection_Rcrd_Pnd_Ben_Dod_Yy;
    private DbsField pnd_Selection_Rcrd_Pnd_Dest_Prev;
    private DbsField pnd_Selection_Rcrd_Pnd_Dest_Curr;
    private DbsGroup pnd_Selection_RcrdRedef66;
    private DbsField pnd_Selection_Rcrd_Pnd_Manual_New_Issue5;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte5;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Check_Dte5Redef68;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Mm5;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Dd5;
    private DbsField pnd_Selection_Rcrd_Pnd_Check_Dte_Yy5;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Area5;
    private DbsField pnd_Selection_Rcrd_Pnd_User_Id5;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte5;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Trans_Dte5Redef69;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Dte5_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Trans_Nbr5;
    private DbsField pnd_Selection_Rcrd_Pnd_Cntrct_Nbr5;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Status5;
    private DbsField pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr5;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Type5;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Nbr5;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Record_Nbr5Redef70;
    private DbsField pnd_Selection_Rcrd_Pnd_Record_Nbr5_N;
    private DbsField pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71;
    private DbsField pnd_Selection_Rcrd_Pnd_Rate;
    private DbsField pnd_Selection_Rcrd_Pnd_Per_Pay;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Per_PayRedef72;
    private DbsField pnd_Selection_Rcrd_Pnd_Per_Pay_A;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Per_PayRedef73;
    private DbsField pnd_Selection_Rcrd_Pnd_Cref_Units;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Cref_UnitsRedef74;
    private DbsField pnd_Selection_Rcrd_Pnd_Cref_Units_A;
    private DbsField pnd_Selection_Rcrd_Pnd_Per_Div;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Per_DivRedef75;
    private DbsField pnd_Selection_Rcrd_Pnd_Per_Div_A;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Pay;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Fin_PayRedef76;
    private DbsField pnd_Selection_Rcrd_Pnd_Fin_Pay_A;
    private DbsField pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Tot_Old_Per_DivRedef77;
    private DbsField pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div_A;
    private DbsField pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef78;
    private DbsField pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay_A;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef79;
    private DbsField pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units;
    private DbsGroup pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_UnitsRedef80;
    private DbsField pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units_A;
    private DbsField pnd_Tax;
    private DbsGroup pnd_TaxRedef81;
    private DbsField pnd_Tax_Pnd_Check_Dte;
    private DbsGroup pnd_Tax_Pnd_Check_DteRedef82;
    private DbsField pnd_Tax_Pnd_Check_Dte_Mm;
    private DbsField pnd_Tax_Pnd_Check_Dte_Dd;
    private DbsField pnd_Tax_Pnd_Check_Dte_Yy;
    private DbsField pnd_Tax_Pnd_User_Area;
    private DbsField pnd_Tax_Pnd_User_Id;
    private DbsField pnd_Tax_Pnd_Trans_Dte;
    private DbsGroup pnd_Tax_Pnd_Trans_DteRedef83;
    private DbsField pnd_Tax_Pnd_Trans_Dte_Cc;
    private DbsField pnd_Tax_Pnd_Trans_Dte_Yy;
    private DbsField pnd_Tax_Pnd_Trans_Dte_Mm;
    private DbsField pnd_Tax_Pnd_Trans_Dte_Dd;
    private DbsGroup pnd_Tax_Pnd_Trans_DteRedef84;
    private DbsField pnd_Tax_Pnd_Trans_Dte_N;
    private DbsField pnd_Tax_Pnd_Trans_Nbr;
    private DbsField pnd_Tax_Pnd_Cntrct_Nbr;
    private DbsField pnd_Tax_Pnd_Record_Status;
    private DbsField pnd_Tax_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Tax_Pnd_Intent_Code;
    private DbsField pnd_Tax_Pnd_Filler1;
    private DbsField pnd_Tax_Pnd_Invest_In_Cntrct_A;
    private DbsGroup pnd_Tax_Pnd_Invest_In_Cntrct_ARedef85;
    private DbsField pnd_Tax_Pnd_Invest_In_Cntrct_N;
    private DbsField pnd_Tax_Pnd_Filler2;
    private DbsField pnd_Tax_Pnd_Multiple;
    private DbsField pnd_Tax_Pnd_Seq_Nbr;
    private DbsField pnd_Tax_Pnd_Verify;
    private DbsField pnd_Tax_Pnd_Filler3;
    private DbsField pnd_Comb_Chk_Trans;
    private DbsGroup pnd_Comb_Chk_TransRedef86;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Check_DteRedef87;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy;
    private DbsField pnd_Comb_Chk_Trans_Pnd_User_Area;
    private DbsField pnd_Comb_Chk_Trans_Pnd_User_Id;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Cc;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Yy;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Mm;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Dd;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef89;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Trans_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Rcrd_Status;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Intent_Code;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Currency;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_AnntRedef90;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Ssn;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_AnntRedef91;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef92;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Sex;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy;
    private DbsField pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef94;
    private DbsField pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct;
    private DbsField pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef95;
    private DbsField pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef96;
    private DbsField pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct;
    private DbsField pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr;
    private DbsGroup pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef97;
    private DbsField pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct;
    private DbsField pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Filler1;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Multiple;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Seq_Nbr;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Verify;
    private DbsField pnd_Comb_Chk_Trans_Pnd_Filler2;
    private DbsField pnd_Ddctn_From_Net;
    private DbsGroup pnd_Ddctn_From_NetRedef98;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Check_DteRedef99;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd;
    private DbsField pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy;
    private DbsField pnd_Ddctn_From_Net_Pnd_User_Area;
    private DbsField pnd_Ddctn_From_Net_Pnd_User_Id;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte_Cc;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte_Yy;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte_Mm;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte_Dd;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Trans_DteRedef101;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Dte_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Trans_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Record_Status;
    private DbsField pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Intent_Code;
    private DbsField pnd_Ddctn_From_Net_Pnd_Sequence_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Ddctn_Cde;
    private DbsField pnd_Ddctn_From_Net_Pnd_Ddctn_Payee;
    private DbsField pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef102;
    private DbsField pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Tot_Ddctn;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef103;
    private DbsField pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N;
    private DbsField pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte;
    private DbsGroup pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef104;
    private DbsField pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm;
    private DbsField pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy;
    private DbsField pnd_Ddctn_From_Net_Pnd_Filler;
    private DbsField pnd_Ddctn_From_Net_Pnd_Multiple;
    private DbsField pnd_Ddctn_From_Net_Pnd_Seq_Nbr;
    private DbsField pnd_Ddctn_From_Net_Pnd_Verify;
    private DbsField pnd_Ddctn_From_Net_Pnd_Filler1;
    private DbsField pnd_His_Ytd_Ddctn;
    private DbsGroup pnd_His_Ytd_DdctnRedef105;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte;
    private DbsGroup pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_User_Area;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_User_Id;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte;
    private DbsGroup pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Cc;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Yy;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Mm;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Dd;
    private DbsGroup pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef108;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Record_Status;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Intent_Code;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt;
    private DbsGroup pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef109;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Filler;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Multiple;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Verify;
    private DbsField pnd_His_Ytd_Ddctn_Pnd_Filler1;
    private DbsField pnd_Page_Ctr;
    private DbsField pnd_Page_Ctr2;
    private DbsField pnd_Display_Dte_Trans;
    private DbsField pnd_Display_Dte_Dob1;
    private DbsField pnd_Display_Dte_Dob2;
    private DbsField pnd_Display_Dte_Dod1;
    private DbsField pnd_Display_Dte_Dod2;
    private DbsField pnd_Display_Dte_Res;
    private DbsField pnd_Display_Dte_Pend;
    private DbsField pnd_Display_Dte_1st_Pay_Due;
    private DbsField pnd_Display_Dte_Iss;
    private DbsField pnd_Display_Dte_Lst_Man_Chk;
    private DbsField pnd_Display_Dte_Fin_Per_Pay;
    private DbsField pnd_Display_Dte_Fin_Pymt;
    private DbsField pnd_Display_Dte_Wdrawal;
    private DbsField pnd_Display_Heading;
    private DbsField pnd_Break_User_Area;
    private DbsField pnd_L_Newpage;
    private DbsField pnd_Ctr;
    private DbsField pnd_Total_Trans;
    private DbsField pnd_User_Area_Trans;
    private DbsField pnd_Userid_Trans;
    private DbsField pnd_Total_Per_Pay;
    private DbsField pnd_Total_Per_Div;
    private DbsField pnd_Total_Fin_Pay;
    private DbsField pnd_Total_Cref_Units;
    private DbsField pnd_Total_Per_Ded;
    private DbsField pnd_Total_Total_Ded;
    private DbsField pnd_Total_Hist_Ded;
    private DbsField pnd_Total_Ivc_Amt;
    private DbsField pnd_Report;
    private DbsField pnd_Save_User_Area;
    private DbsField pnd_Display_User_Area;
    private DbsField pnd_Save_Userid;
    private DbsField pnd_Display_Userid;
    private DbsField pnd_Save_Mode;
    private DbsField pnd_Display_Mode;
    private DbsField pnd_New_Trans_L;
    private DbsField pnd_First;
    private DbsField pnd_Wrote_Disp_050;
    private DbsField pnd_Have_33;
    private DbsField pnd_Have_50;
    private DbsField pnd_Indx;
    private DbsField pnd_Indx1;
    private DbsField pnd_Indx2;
    private DbsField pnd_Disp_Dte;
    private DbsGroup pnd_Disp_DteRedef110;
    private DbsField pnd_Disp_Dte_Pnd_Disp_Dte_Mm;
    private DbsField pnd_Disp_Dte_Pnd_Disp_Dte_S1;
    private DbsField pnd_Disp_Dte_Pnd_Disp_Dte_Dd;
    private DbsField pnd_Disp_Dte_Pnd_Disp_Dte_S2;
    private DbsField pnd_Disp_Dte_Pnd_Disp_Dte_Yy;
    private DbsField pnd_Parm_Check_Dte_Disp;
    private DbsField pnd_Parm_From_Dte_Disp;
    private DbsField pnd_Parm_To_Dte_Disp;

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Program_Id() { return iaa_Parm_Card_Pnd_Program_Id; }

    public DbsField getIaa_Parm_Card_Pnd_Filler1() { return iaa_Parm_Card_Pnd_Filler1; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Check_Dte() { return iaa_Parm_Card_Pnd_Parm_Check_Dte; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Check_DteRedef1() { return iaa_Parm_Card_Pnd_Parm_Check_DteRedef1; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Check_Dte_N() { return iaa_Parm_Card_Pnd_Parm_Check_Dte_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Check_DteRedef2() { return iaa_Parm_Card_Pnd_Parm_Check_DteRedef2; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Check_Dte_Cc() { return iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Check_Dte_Yy() { return iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Check_Dte_Mm() { return iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Check_Dte_Dd() { return iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd; }

    public DbsField getIaa_Parm_Card_Pnd_Filler2() { return iaa_Parm_Card_Pnd_Filler2; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_From_Dte() { return iaa_Parm_Card_Pnd_Parm_From_Dte; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_From_DteRedef3() { return iaa_Parm_Card_Pnd_Parm_From_DteRedef3; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_From_Dte_N() { return iaa_Parm_Card_Pnd_Parm_From_Dte_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_From_DteRedef4() { return iaa_Parm_Card_Pnd_Parm_From_DteRedef4; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_From_Dte_Cc() { return iaa_Parm_Card_Pnd_Parm_From_Dte_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_From_Dte_Yy() { return iaa_Parm_Card_Pnd_Parm_From_Dte_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_From_Dte_Mm() { return iaa_Parm_Card_Pnd_Parm_From_Dte_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_From_Dte_Dd() { return iaa_Parm_Card_Pnd_Parm_From_Dte_Dd; }

    public DbsField getIaa_Parm_Card_Pnd_Filler3() { return iaa_Parm_Card_Pnd_Filler3; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_To_Dte() { return iaa_Parm_Card_Pnd_Parm_To_Dte; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_To_DteRedef5() { return iaa_Parm_Card_Pnd_Parm_To_DteRedef5; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_To_Dte_N() { return iaa_Parm_Card_Pnd_Parm_To_Dte_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_To_DteRedef6() { return iaa_Parm_Card_Pnd_Parm_To_DteRedef6; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_To_Dte_Cc() { return iaa_Parm_Card_Pnd_Parm_To_Dte_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_To_Dte_Yy() { return iaa_Parm_Card_Pnd_Parm_To_Dte_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_To_Dte_Mm() { return iaa_Parm_Card_Pnd_Parm_To_Dte_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_To_Dte_Dd() { return iaa_Parm_Card_Pnd_Parm_To_Dte_Dd; }

    public DbsField getIaa_Parm_Card_Pnd_Filler4() { return iaa_Parm_Card_Pnd_Filler4; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_User_Area() { return iaa_Parm_Card_Pnd_Parm_User_Area; }

    public DbsField getIaa_Parm_Card_Pnd_Filler5() { return iaa_Parm_Card_Pnd_Filler5; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_User_Id() { return iaa_Parm_Card_Pnd_Parm_User_Id; }

    public DbsField getIaa_Parm_Card_Pnd_Filler6() { return iaa_Parm_Card_Pnd_Filler6; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Verify_Id() { return iaa_Parm_Card_Pnd_Parm_Verify_Id; }

    public DbsField getIaa_Parm_Card_Pnd_Filler7() { return iaa_Parm_Card_Pnd_Filler7; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Verify_Cde() { return iaa_Parm_Card_Pnd_Parm_Verify_Cde; }

    public DbsField getIaa_Parm_Card_Pnd_Filler8() { return iaa_Parm_Card_Pnd_Filler8; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Totals() { return iaa_Parm_Card_Pnd_Parm_Totals; }

    public DbsField getPnd_Input_Rec() { return pnd_Input_Rec; }

    public DbsGroup getPnd_Input_RecRedef7() { return pnd_Input_RecRedef7; }

    public DbsField getPnd_Input_Rec_Pnd_Check_Dte() { return pnd_Input_Rec_Pnd_Check_Dte; }

    public DbsField getPnd_Input_Rec_Pnd_User_Area() { return pnd_Input_Rec_Pnd_User_Area; }

    public DbsField getPnd_Input_Rec_Pnd_User_Id() { return pnd_Input_Rec_Pnd_User_Id; }

    public DbsField getPnd_Input_Rec_Pnd_Trans_Dte() { return pnd_Input_Rec_Pnd_Trans_Dte; }

    public DbsField getPnd_Input_Rec_Pnd_Trans_Cde() { return pnd_Input_Rec_Pnd_Trans_Cde; }

    public DbsGroup getPnd_Input_Rec_Pnd_Trans_CdeRedef8() { return pnd_Input_Rec_Pnd_Trans_CdeRedef8; }

    public DbsField getPnd_Input_Rec_Pnd_Trans_Cde_N() { return pnd_Input_Rec_Pnd_Trans_Cde_N; }

    public DbsField getPnd_Input_Rec_Pnd_Contract_Nbr() { return pnd_Input_Rec_Pnd_Contract_Nbr; }

    public DbsField getPnd_Input_Rec_Pnd_Payee() { return pnd_Input_Rec_Pnd_Payee; }

    public DbsField getPnd_Input_Rec_Pnd_Xref() { return pnd_Input_Rec_Pnd_Xref; }

    public DbsField getPnd_Input_Rec_Pnd_Record_Nbr() { return pnd_Input_Rec_Pnd_Record_Nbr; }

    public DbsField getPnd_Input_Rec_Pnd_Rest_Of_Input() { return pnd_Input_Rec_Pnd_Rest_Of_Input; }

    public DbsField getPnd_Input_Rec_Pnd_Multiple() { return pnd_Input_Rec_Pnd_Multiple; }

    public DbsField getPnd_Input_Rec_Pnd_Seq_Nbr() { return pnd_Input_Rec_Pnd_Seq_Nbr; }

    public DbsField getPnd_Input_Rec_Pnd_Verify() { return pnd_Input_Rec_Pnd_Verify; }

    public DbsField getPnd_Input_Rec_Pnd_Mode() { return pnd_Input_Rec_Pnd_Mode; }

    public DbsField getPnd_Input_Rec_Pnd_Filler1() { return pnd_Input_Rec_Pnd_Filler1; }

    public DbsField getPnd_Output_Rec() { return pnd_Output_Rec; }

    public DbsGroup getPnd_Output_RecRedef9() { return pnd_Output_RecRedef9; }

    public DbsField getPnd_Output_Rec_Pnd_Check_Dte() { return pnd_Output_Rec_Pnd_Check_Dte; }

    public DbsField getPnd_Output_Rec_Pnd_User_Area() { return pnd_Output_Rec_Pnd_User_Area; }

    public DbsField getPnd_Output_Rec_Pnd_User_Id() { return pnd_Output_Rec_Pnd_User_Id; }

    public DbsField getPnd_Output_Rec_Pnd_Trans_Dte() { return pnd_Output_Rec_Pnd_Trans_Dte; }

    public DbsField getPnd_Output_Rec_Pnd_Trans_Cde() { return pnd_Output_Rec_Pnd_Trans_Cde; }

    public DbsGroup getPnd_Output_Rec_Pnd_Trans_CdeRedef10() { return pnd_Output_Rec_Pnd_Trans_CdeRedef10; }

    public DbsField getPnd_Output_Rec_Pnd_Trans_Cde_N() { return pnd_Output_Rec_Pnd_Trans_Cde_N; }

    public DbsField getPnd_Output_Rec_Pnd_Contract_Nbr() { return pnd_Output_Rec_Pnd_Contract_Nbr; }

    public DbsField getPnd_Output_Rec_Pnd_Payee() { return pnd_Output_Rec_Pnd_Payee; }

    public DbsField getPnd_Output_Rec_Pnd_Xref() { return pnd_Output_Rec_Pnd_Xref; }

    public DbsField getPnd_Output_Rec_Pnd_Record_Nbr() { return pnd_Output_Rec_Pnd_Record_Nbr; }

    public DbsField getPnd_Output_Rec_Pnd_Rest_Of_Output() { return pnd_Output_Rec_Pnd_Rest_Of_Output; }

    public DbsField getPnd_Output_Rec_Pnd_Multiple() { return pnd_Output_Rec_Pnd_Multiple; }

    public DbsField getPnd_Output_Rec_Pnd_Seq_Nbr() { return pnd_Output_Rec_Pnd_Seq_Nbr; }

    public DbsField getPnd_Output_Rec_Pnd_Verify() { return pnd_Output_Rec_Pnd_Verify; }

    public DbsField getPnd_Output_Rec_Pnd_Mode() { return pnd_Output_Rec_Pnd_Mode; }

    public DbsField getPnd_Output_Rec_Pnd_New_Trans() { return pnd_Output_Rec_Pnd_New_Trans; }

    public DbsField getPnd_Misc_Non_Tax_Trans() { return pnd_Misc_Non_Tax_Trans; }

    public DbsGroup getPnd_Misc_Non_Tax_TransRedef11() { return pnd_Misc_Non_Tax_TransRedef11; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_User_Area() { return pnd_Misc_Non_Tax_Trans_Pnd_User_Area; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_User_Id() { return pnd_Misc_Non_Tax_Trans_Pnd_User_Id; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_Trans_DteRedef13() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_DteRedef13; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_N() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_N; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Cc() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Cc; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Dd() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Dd; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Record_Status() { return pnd_Misc_Non_Tax_Trans_Pnd_Record_Status; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code() { return pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde() { return pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde() { return pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res() { return pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef15() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef15; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef17() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef17; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef18() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef18; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef20() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef20; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef21() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef21; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Filler1() { return pnd_Misc_Non_Tax_Trans_Pnd_Filler1; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Multiple() { return pnd_Misc_Non_Tax_Trans_Pnd_Multiple; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Seq_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Seq_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Verify() { return pnd_Misc_Non_Tax_Trans_Pnd_Verify; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Filler2() { return pnd_Misc_Non_Tax_Trans_Pnd_Filler2; }

    public DbsField getPnd_Selection_Rcrd() { return pnd_Selection_Rcrd; }

    public DbsGroup getPnd_Selection_RcrdRedef22() { return pnd_Selection_RcrdRedef22; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue1; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte1() { return pnd_Selection_Rcrd_Pnd_Check_Dte1; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Check_Dte1Redef24() { return pnd_Selection_Rcrd_Pnd_Check_Dte1Redef24; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Mm1() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Mm1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Dd1() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Dd1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Yy1() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Yy1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Area1() { return pnd_Selection_Rcrd_Pnd_User_Area1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Id1() { return pnd_Selection_Rcrd_Pnd_User_Id1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte1() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte1_Cc() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1_Cc; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte1_Yy() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte1_Mm() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte1_Dd() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1_Dd; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Trans_Dte1Redef26() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef26; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte1_N() { return pnd_Selection_Rcrd_Pnd_Trans_Dte1_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Nbr1() { return pnd_Selection_Rcrd_Pnd_Trans_Nbr1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1Redef27() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1Redef27; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N1() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N7() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N7; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Status1() { return pnd_Selection_Rcrd_Pnd_Record_Status1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1() { return pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1() { return pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1Redef28() { return pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1Redef28; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Type1() { return pnd_Selection_Rcrd_Pnd_Record_Type1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Nbr1() { return pnd_Selection_Rcrd_Pnd_Record_Nbr1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue1() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Product() { return pnd_Selection_Rcrd_Pnd_Product; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Currency() { return pnd_Selection_Rcrd_Pnd_Currency; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_CurrencyRedef30() { return pnd_Selection_Rcrd_Pnd_CurrencyRedef30; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Currency_N() { return pnd_Selection_Rcrd_Pnd_Currency_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Mode() { return pnd_Selection_Rcrd_Pnd_Mode; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_ModeRedef31() { return pnd_Selection_Rcrd_Pnd_ModeRedef31; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Mode_N() { return pnd_Selection_Rcrd_Pnd_Mode_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Pend_Code() { return pnd_Selection_Rcrd_Pnd_Pend_Code; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Hold_Check_Cde() { return pnd_Selection_Rcrd_Pnd_Hold_Check_Cde; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Pend_Dte() { return pnd_Selection_Rcrd_Pnd_Pend_Dte; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Pend_DteRedef32() { return pnd_Selection_Rcrd_Pnd_Pend_DteRedef32; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Pend_Dte_Mm() { return pnd_Selection_Rcrd_Pnd_Pend_Dte_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Pend_Dte_Yy() { return pnd_Selection_Rcrd_Pnd_Pend_Dte_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Option() { return pnd_Selection_Rcrd_Pnd_Option; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_OptionRedef33() { return pnd_Selection_Rcrd_Pnd_OptionRedef33; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Option_N() { return pnd_Selection_Rcrd_Pnd_Option_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Origin() { return pnd_Selection_Rcrd_Pnd_Origin; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Iss_Dte() { return pnd_Selection_Rcrd_Pnd_Iss_Dte; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Iss_DteRedef34() { return pnd_Selection_Rcrd_Pnd_Iss_DteRedef34; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Iss_Dte_Mm() { return pnd_Selection_Rcrd_Pnd_Iss_Dte_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Iss_Dte_Yy() { return pnd_Selection_Rcrd_Pnd_Iss_Dte_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte() { return pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_DteRedef35() { return pnd_Selection_Rcrd_Pnd_1st_Pay_Due_DteRedef35; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm() { return pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy() { return pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte() { return pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_DteRedef36() { return pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_DteRedef36; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm() { return pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy() { return pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte() { return pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_DteRedef37() { return pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_DteRedef37; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm() { return pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy() { return pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte() { return pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38() { return pnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm() { return pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd() { return pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy() { return pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte() { return pnd_Selection_Rcrd_Pnd_Wdrawal_Dte; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Wdrawal_DteRedef39() { return pnd_Selection_Rcrd_Pnd_Wdrawal_DteRedef39; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm() { return pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy() { return pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Filler1() { return pnd_Selection_Rcrd_Pnd_Filler1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Multiple1() { return pnd_Selection_Rcrd_Pnd_Multiple1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Seq_Nbr1() { return pnd_Selection_Rcrd_Pnd_Seq_Nbr1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Verify1() { return pnd_Selection_Rcrd_Pnd_Verify1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Mode1() { return pnd_Selection_Rcrd_Pnd_Mode1; }

    public DbsField getPnd_Selection_Rcrd_Pnd_New_Trans() { return pnd_Selection_Rcrd_Pnd_New_Trans; }

    public DbsGroup getPnd_Selection_RcrdRedef40() { return pnd_Selection_RcrdRedef40; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Manual_New_Issue2() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue2; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte2() { return pnd_Selection_Rcrd_Pnd_Check_Dte2; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Check_Dte2Redef42() { return pnd_Selection_Rcrd_Pnd_Check_Dte2Redef42; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Mm2() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Mm2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Dd2() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Dd2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Yy2() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Yy2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Area2() { return pnd_Selection_Rcrd_Pnd_User_Area2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Id2() { return pnd_Selection_Rcrd_Pnd_User_Id2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte2() { return pnd_Selection_Rcrd_Pnd_Trans_Dte2; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Trans_Dte2Redef43() { return pnd_Selection_Rcrd_Pnd_Trans_Dte2Redef43; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte2_N() { return pnd_Selection_Rcrd_Pnd_Trans_Dte2_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Nbr2() { return pnd_Selection_Rcrd_Pnd_Trans_Nbr2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr2() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Status2() { return pnd_Selection_Rcrd_Pnd_Record_Status2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr2() { return pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Type2() { return pnd_Selection_Rcrd_Pnd_Record_Type2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Nbr2() { return pnd_Selection_Rcrd_Pnd_Record_Nbr2; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue2() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Citizen() { return pnd_Selection_Rcrd_Pnd_Citizen; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss() { return pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Res() { return pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Res; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Coll_Iss() { return pnd_Selection_Rcrd_Pnd_Coll_Iss; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt() { return pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_AmtRedef45() { return pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_AmtRedef45; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt_A() { return pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt_A; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Ssn() { return pnd_Selection_Rcrd_Pnd_Ssn; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Joint_Cnvrt() { return pnd_Selection_Rcrd_Pnd_Joint_Cnvrt; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Spirt() { return pnd_Selection_Rcrd_Pnd_Spirt; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Pen_Pln_Cde() { return pnd_Selection_Rcrd_Pnd_Pen_Pln_Cde; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Type() { return pnd_Selection_Rcrd_Pnd_Cntrct_Type; }

    public DbsGroup getPnd_Selection_RcrdRedef46() { return pnd_Selection_RcrdRedef46; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Manual_New_Issue3() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue3; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte3() { return pnd_Selection_Rcrd_Pnd_Check_Dte3; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Check_Dte3Redef48() { return pnd_Selection_Rcrd_Pnd_Check_Dte3Redef48; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Mm3() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Mm3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Dd3() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Dd3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Yy3() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Yy3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Area3() { return pnd_Selection_Rcrd_Pnd_User_Area3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Id3() { return pnd_Selection_Rcrd_Pnd_User_Id3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte3() { return pnd_Selection_Rcrd_Pnd_Trans_Dte3; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Trans_Dte3Redef49() { return pnd_Selection_Rcrd_Pnd_Trans_Dte3Redef49; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte3_N() { return pnd_Selection_Rcrd_Pnd_Trans_Dte3_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Nbr3() { return pnd_Selection_Rcrd_Pnd_Trans_Nbr3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr3() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Status3() { return pnd_Selection_Rcrd_Pnd_Record_Status3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr3() { return pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Type3() { return pnd_Selection_Rcrd_Pnd_Record_Type3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Nbr3() { return pnd_Selection_Rcrd_Pnd_Record_Nbr3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue3() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref() { return pnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51() { return pnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Last_Name() { return pnd_Selection_Rcrd_Pnd_1st_Last_Name; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_First_Init() { return pnd_Selection_Rcrd_Pnd_1st_First_Init; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Mid_Init() { return pnd_Selection_Rcrd_Pnd_1st_Mid_Init; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Sex() { return pnd_Selection_Rcrd_Pnd_1st_Sex; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Dob() { return pnd_Selection_Rcrd_Pnd_1st_Dob; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_1st_DobRedef52() { return pnd_Selection_Rcrd_Pnd_1st_DobRedef52; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Dob_Mm() { return pnd_Selection_Rcrd_Pnd_1st_Dob_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Dob_Dd() { return pnd_Selection_Rcrd_Pnd_1st_Dob_Dd; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Dob_Yy() { return pnd_Selection_Rcrd_Pnd_1st_Dob_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Dod() { return pnd_Selection_Rcrd_Pnd_1st_Dod; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_1st_DodRedef53() { return pnd_Selection_Rcrd_Pnd_1st_DodRedef53; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Dod_Mm() { return pnd_Selection_Rcrd_Pnd_1st_Dod_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_1st_Dod_Yy() { return pnd_Selection_Rcrd_Pnd_1st_Dod_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Mort_Yob3() { return pnd_Selection_Rcrd_Pnd_Mort_Yob3; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Mort_Yob3Redef54() { return pnd_Selection_Rcrd_Pnd_Mort_Yob3Redef54; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Mort_Yob3_Yy() { return pnd_Selection_Rcrd_Pnd_Mort_Yob3_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Life_Cnt3() { return pnd_Selection_Rcrd_Pnd_Life_Cnt3; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Div_Payee() { return pnd_Selection_Rcrd_Pnd_Div_Payee; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Coll_Cde() { return pnd_Selection_Rcrd_Pnd_Coll_Cde; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Orig_Cntrct() { return pnd_Selection_Rcrd_Pnd_Orig_Cntrct; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cash_Cde() { return pnd_Selection_Rcrd_Pnd_Cash_Cde; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Emp_Term() { return pnd_Selection_Rcrd_Pnd_Emp_Term; }

    public DbsGroup getPnd_Selection_RcrdRedef55() { return pnd_Selection_RcrdRedef55; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Manual_New_Issue4() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue4; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte4() { return pnd_Selection_Rcrd_Pnd_Check_Dte4; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Check_Dte4Redef57() { return pnd_Selection_Rcrd_Pnd_Check_Dte4Redef57; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Mm4() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Mm4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Dd4() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Dd4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Yy4() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Yy4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Area4() { return pnd_Selection_Rcrd_Pnd_User_Area4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Id4() { return pnd_Selection_Rcrd_Pnd_User_Id4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte4() { return pnd_Selection_Rcrd_Pnd_Trans_Dte4; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Trans_Dte4Redef58() { return pnd_Selection_Rcrd_Pnd_Trans_Dte4Redef58; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte4_N() { return pnd_Selection_Rcrd_Pnd_Trans_Dte4_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Nbr4() { return pnd_Selection_Rcrd_Pnd_Trans_Nbr4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr4() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Status4() { return pnd_Selection_Rcrd_Pnd_Record_Status4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr4() { return pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Type4() { return pnd_Selection_Rcrd_Pnd_Record_Type4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Nbr4() { return pnd_Selection_Rcrd_Pnd_Record_Nbr4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue4() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref() { return pnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60() { return pnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Last_Name() { return pnd_Selection_Rcrd_Pnd_2nd_Last_Name; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_First_Init() { return pnd_Selection_Rcrd_Pnd_2nd_First_Init; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Mid_Init() { return pnd_Selection_Rcrd_Pnd_2nd_Mid_Init; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Sex() { return pnd_Selection_Rcrd_Pnd_2nd_Sex; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Dob() { return pnd_Selection_Rcrd_Pnd_2nd_Dob; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_2nd_DobRedef61() { return pnd_Selection_Rcrd_Pnd_2nd_DobRedef61; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Dob_Mm() { return pnd_Selection_Rcrd_Pnd_2nd_Dob_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Dob_Dd() { return pnd_Selection_Rcrd_Pnd_2nd_Dob_Dd; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Dob_Yy() { return pnd_Selection_Rcrd_Pnd_2nd_Dob_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Dod() { return pnd_Selection_Rcrd_Pnd_2nd_Dod; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_2nd_DodRedef62() { return pnd_Selection_Rcrd_Pnd_2nd_DodRedef62; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Dod_Mm() { return pnd_Selection_Rcrd_Pnd_2nd_Dod_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_2nd_Dod_Yy() { return pnd_Selection_Rcrd_Pnd_2nd_Dod_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Mort_Yob4() { return pnd_Selection_Rcrd_Pnd_Mort_Yob4; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Mort_Yob4Redef63() { return pnd_Selection_Rcrd_Pnd_Mort_Yob4Redef63; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Mort_Yob4_Yy() { return pnd_Selection_Rcrd_Pnd_Mort_Yob4_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Life_Cnt4() { return pnd_Selection_Rcrd_Pnd_Life_Cnt4; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Ben_Xref() { return pnd_Selection_Rcrd_Pnd_Ben_Xref; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Ben_Dod() { return pnd_Selection_Rcrd_Pnd_Ben_Dod; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Ben_DodRedef64() { return pnd_Selection_Rcrd_Pnd_Ben_DodRedef64; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Ben_Dod_A() { return pnd_Selection_Rcrd_Pnd_Ben_Dod_A; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Ben_DodRedef65() { return pnd_Selection_Rcrd_Pnd_Ben_DodRedef65; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Ben_Dod_Mm() { return pnd_Selection_Rcrd_Pnd_Ben_Dod_Mm; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Ben_Dod_Yy() { return pnd_Selection_Rcrd_Pnd_Ben_Dod_Yy; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Dest_Prev() { return pnd_Selection_Rcrd_Pnd_Dest_Prev; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Dest_Curr() { return pnd_Selection_Rcrd_Pnd_Dest_Curr; }

    public DbsGroup getPnd_Selection_RcrdRedef66() { return pnd_Selection_RcrdRedef66; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue5; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67() { return pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte5() { return pnd_Selection_Rcrd_Pnd_Check_Dte5; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Check_Dte5Redef68() { return pnd_Selection_Rcrd_Pnd_Check_Dte5Redef68; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Mm5() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Mm5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Dd5() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Dd5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Check_Dte_Yy5() { return pnd_Selection_Rcrd_Pnd_Check_Dte_Yy5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Area5() { return pnd_Selection_Rcrd_Pnd_User_Area5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_User_Id5() { return pnd_Selection_Rcrd_Pnd_User_Id5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte5() { return pnd_Selection_Rcrd_Pnd_Trans_Dte5; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Trans_Dte5Redef69() { return pnd_Selection_Rcrd_Pnd_Trans_Dte5Redef69; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Dte5_N() { return pnd_Selection_Rcrd_Pnd_Trans_Dte5_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Trans_Nbr5() { return pnd_Selection_Rcrd_Pnd_Trans_Nbr5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr5() { return pnd_Selection_Rcrd_Pnd_Cntrct_Nbr5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Status5() { return pnd_Selection_Rcrd_Pnd_Record_Status5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr5() { return pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Type5() { return pnd_Selection_Rcrd_Pnd_Record_Type5; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Nbr5() { return pnd_Selection_Rcrd_Pnd_Record_Nbr5; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Record_Nbr5Redef70() { return pnd_Selection_Rcrd_Pnd_Record_Nbr5Redef70; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Record_Nbr5_N() { return pnd_Selection_Rcrd_Pnd_Record_Nbr5_N; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue5() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71() { return pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Rate() { return pnd_Selection_Rcrd_Pnd_Rate; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Per_Pay() { return pnd_Selection_Rcrd_Pnd_Per_Pay; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Per_PayRedef72() { return pnd_Selection_Rcrd_Pnd_Per_PayRedef72; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Per_Pay_A() { return pnd_Selection_Rcrd_Pnd_Per_Pay_A; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Per_PayRedef73() { return pnd_Selection_Rcrd_Pnd_Per_PayRedef73; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cref_Units() { return pnd_Selection_Rcrd_Pnd_Cref_Units; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Cref_UnitsRedef74() { return pnd_Selection_Rcrd_Pnd_Cref_UnitsRedef74; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Cref_Units_A() { return pnd_Selection_Rcrd_Pnd_Cref_Units_A; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Per_Div() { return pnd_Selection_Rcrd_Pnd_Per_Div; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Per_DivRedef75() { return pnd_Selection_Rcrd_Pnd_Per_DivRedef75; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Per_Div_A() { return pnd_Selection_Rcrd_Pnd_Per_Div_A; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Pay() { return pnd_Selection_Rcrd_Pnd_Fin_Pay; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Fin_PayRedef76() { return pnd_Selection_Rcrd_Pnd_Fin_PayRedef76; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Fin_Pay_A() { return pnd_Selection_Rcrd_Pnd_Fin_Pay_A; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_DivRedef77() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Per_DivRedef77; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div_A() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div_A; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef78() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef78; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay_A() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay_A; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef79() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef79; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units; }

    public DbsGroup getPnd_Selection_Rcrd_Pnd_Tot_Old_Cref_UnitsRedef80() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_UnitsRedef80; }

    public DbsField getPnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units_A() { return pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units_A; }

    public DbsField getPnd_Tax() { return pnd_Tax; }

    public DbsGroup getPnd_TaxRedef81() { return pnd_TaxRedef81; }

    public DbsField getPnd_Tax_Pnd_Check_Dte() { return pnd_Tax_Pnd_Check_Dte; }

    public DbsGroup getPnd_Tax_Pnd_Check_DteRedef82() { return pnd_Tax_Pnd_Check_DteRedef82; }

    public DbsField getPnd_Tax_Pnd_Check_Dte_Mm() { return pnd_Tax_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Tax_Pnd_Check_Dte_Dd() { return pnd_Tax_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Tax_Pnd_Check_Dte_Yy() { return pnd_Tax_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Tax_Pnd_User_Area() { return pnd_Tax_Pnd_User_Area; }

    public DbsField getPnd_Tax_Pnd_User_Id() { return pnd_Tax_Pnd_User_Id; }

    public DbsField getPnd_Tax_Pnd_Trans_Dte() { return pnd_Tax_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Tax_Pnd_Trans_DteRedef83() { return pnd_Tax_Pnd_Trans_DteRedef83; }

    public DbsField getPnd_Tax_Pnd_Trans_Dte_Cc() { return pnd_Tax_Pnd_Trans_Dte_Cc; }

    public DbsField getPnd_Tax_Pnd_Trans_Dte_Yy() { return pnd_Tax_Pnd_Trans_Dte_Yy; }

    public DbsField getPnd_Tax_Pnd_Trans_Dte_Mm() { return pnd_Tax_Pnd_Trans_Dte_Mm; }

    public DbsField getPnd_Tax_Pnd_Trans_Dte_Dd() { return pnd_Tax_Pnd_Trans_Dte_Dd; }

    public DbsGroup getPnd_Tax_Pnd_Trans_DteRedef84() { return pnd_Tax_Pnd_Trans_DteRedef84; }

    public DbsField getPnd_Tax_Pnd_Trans_Dte_N() { return pnd_Tax_Pnd_Trans_Dte_N; }

    public DbsField getPnd_Tax_Pnd_Trans_Nbr() { return pnd_Tax_Pnd_Trans_Nbr; }

    public DbsField getPnd_Tax_Pnd_Cntrct_Nbr() { return pnd_Tax_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Tax_Pnd_Record_Status() { return pnd_Tax_Pnd_Record_Status; }

    public DbsField getPnd_Tax_Pnd_Cross_Ref_Nbr() { return pnd_Tax_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Tax_Pnd_Intent_Code() { return pnd_Tax_Pnd_Intent_Code; }

    public DbsField getPnd_Tax_Pnd_Filler1() { return pnd_Tax_Pnd_Filler1; }

    public DbsField getPnd_Tax_Pnd_Invest_In_Cntrct_A() { return pnd_Tax_Pnd_Invest_In_Cntrct_A; }

    public DbsGroup getPnd_Tax_Pnd_Invest_In_Cntrct_ARedef85() { return pnd_Tax_Pnd_Invest_In_Cntrct_ARedef85; }

    public DbsField getPnd_Tax_Pnd_Invest_In_Cntrct_N() { return pnd_Tax_Pnd_Invest_In_Cntrct_N; }

    public DbsField getPnd_Tax_Pnd_Filler2() { return pnd_Tax_Pnd_Filler2; }

    public DbsField getPnd_Tax_Pnd_Multiple() { return pnd_Tax_Pnd_Multiple; }

    public DbsField getPnd_Tax_Pnd_Seq_Nbr() { return pnd_Tax_Pnd_Seq_Nbr; }

    public DbsField getPnd_Tax_Pnd_Verify() { return pnd_Tax_Pnd_Verify; }

    public DbsField getPnd_Tax_Pnd_Filler3() { return pnd_Tax_Pnd_Filler3; }

    public DbsField getPnd_Comb_Chk_Trans() { return pnd_Comb_Chk_Trans; }

    public DbsGroup getPnd_Comb_Chk_TransRedef86() { return pnd_Comb_Chk_TransRedef86; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Check_DteRedef87() { return pnd_Comb_Chk_Trans_Pnd_Check_DteRedef87; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy() { return pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_User_Area() { return pnd_Comb_Chk_Trans_Pnd_User_Area; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_User_Id() { return pnd_Comb_Chk_Trans_Pnd_User_Id; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88() { return pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_Cc() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Cc; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_Yy() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Yy; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_Mm() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Mm; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_Dd() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Dd; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Trans_DteRedef89() { return pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef89; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_N() { return pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Trans_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Trans_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status() { return pnd_Comb_Chk_Trans_Pnd_Rcrd_Status; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Intent_Code() { return pnd_Comb_Chk_Trans_Pnd_Intent_Code; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Currency() { return pnd_Comb_Chk_Trans_Pnd_Currency; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt() { return pnd_Comb_Chk_Trans_Pnd_Annt; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_AnntRedef90() { return pnd_Comb_Chk_Trans_Pnd_AnntRedef90; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Ssn() { return pnd_Comb_Chk_Trans_Pnd_Ssn; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_AnntRedef91() { return pnd_Comb_Chk_Trans_Pnd_AnntRedef91; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_A() { return pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef92() { return pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef92; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Sex() { return pnd_Comb_Chk_Trans_Pnd_Annt_Sex; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_A() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy() { return pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef94() { return pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef94; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef95() { return pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef95; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef96() { return pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef96; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr; }

    public DbsGroup getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef97() { return pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef97; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct() { return pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr() { return pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Filler1() { return pnd_Comb_Chk_Trans_Pnd_Filler1; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Multiple() { return pnd_Comb_Chk_Trans_Pnd_Multiple; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Seq_Nbr() { return pnd_Comb_Chk_Trans_Pnd_Seq_Nbr; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Verify() { return pnd_Comb_Chk_Trans_Pnd_Verify; }

    public DbsField getPnd_Comb_Chk_Trans_Pnd_Filler2() { return pnd_Comb_Chk_Trans_Pnd_Filler2; }

    public DbsField getPnd_Ddctn_From_Net() { return pnd_Ddctn_From_Net; }

    public DbsGroup getPnd_Ddctn_From_NetRedef98() { return pnd_Ddctn_From_NetRedef98; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte() { return pnd_Ddctn_From_Net_Pnd_Check_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Check_DteRedef99() { return pnd_Ddctn_From_Net_Pnd_Check_DteRedef99; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Mm() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Dd() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Check_Dte_Yy() { return pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_User_Area() { return pnd_Ddctn_From_Net_Pnd_User_Area; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_User_Id() { return pnd_Ddctn_From_Net_Pnd_User_Id; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Trans_DteRedef100() { return pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte_Cc() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte_Cc; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte_Yy() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte_Yy; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte_Mm() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte_Mm; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte_Dd() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte_Dd; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Trans_DteRedef101() { return pnd_Ddctn_From_Net_Pnd_Trans_DteRedef101; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Dte_N() { return pnd_Ddctn_From_Net_Pnd_Trans_Dte_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Trans_Nbr() { return pnd_Ddctn_From_Net_Pnd_Trans_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Cntrct_Nbr() { return pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Record_Status() { return pnd_Ddctn_From_Net_Pnd_Record_Status; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr() { return pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Intent_Code() { return pnd_Ddctn_From_Net_Pnd_Intent_Code; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Sequence_Nbr() { return pnd_Ddctn_From_Net_Pnd_Sequence_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Ddctn_Cde() { return pnd_Ddctn_From_Net_Pnd_Ddctn_Cde; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Ddctn_Payee() { return pnd_Ddctn_From_Net_Pnd_Ddctn_Payee; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt() { return pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef102() { return pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef102; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N() { return pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn() { return pnd_Ddctn_From_Net_Pnd_Tot_Ddctn; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef103() { return pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef103; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N() { return pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte; }

    public DbsGroup getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef104() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef104; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy() { return pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Filler() { return pnd_Ddctn_From_Net_Pnd_Filler; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Multiple() { return pnd_Ddctn_From_Net_Pnd_Multiple; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Seq_Nbr() { return pnd_Ddctn_From_Net_Pnd_Seq_Nbr; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Verify() { return pnd_Ddctn_From_Net_Pnd_Verify; }

    public DbsField getPnd_Ddctn_From_Net_Pnd_Filler1() { return pnd_Ddctn_From_Net_Pnd_Filler1; }

    public DbsField getPnd_His_Ytd_Ddctn() { return pnd_His_Ytd_Ddctn; }

    public DbsGroup getPnd_His_Ytd_DdctnRedef105() { return pnd_His_Ytd_DdctnRedef105; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte; }

    public DbsGroup getPnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106() { return pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy() { return pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_User_Area() { return pnd_His_Ytd_Ddctn_Pnd_User_Area; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_User_Id() { return pnd_His_Ytd_Ddctn_Pnd_User_Id; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte; }

    public DbsGroup getPnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107() { return pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Cc() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Cc; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Yy() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Yy; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Mm() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Mm; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Dd() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Dd; }

    public DbsGroup getPnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef108() { return pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef108; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Trans_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Record_Status() { return pnd_His_Ytd_Ddctn_Pnd_Record_Status; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Intent_Code() { return pnd_His_Ytd_Ddctn_Pnd_Intent_Code; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde() { return pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee() { return pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt() { return pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt; }

    public DbsGroup getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef109() { return pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef109; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N() { return pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Filler() { return pnd_His_Ytd_Ddctn_Pnd_Filler; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Multiple() { return pnd_His_Ytd_Ddctn_Pnd_Multiple; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Seq_Nbr() { return pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Verify() { return pnd_His_Ytd_Ddctn_Pnd_Verify; }

    public DbsField getPnd_His_Ytd_Ddctn_Pnd_Filler1() { return pnd_His_Ytd_Ddctn_Pnd_Filler1; }

    public DbsField getPnd_Page_Ctr() { return pnd_Page_Ctr; }

    public DbsField getPnd_Page_Ctr2() { return pnd_Page_Ctr2; }

    public DbsField getPnd_Display_Dte_Trans() { return pnd_Display_Dte_Trans; }

    public DbsField getPnd_Display_Dte_Dob1() { return pnd_Display_Dte_Dob1; }

    public DbsField getPnd_Display_Dte_Dob2() { return pnd_Display_Dte_Dob2; }

    public DbsField getPnd_Display_Dte_Dod1() { return pnd_Display_Dte_Dod1; }

    public DbsField getPnd_Display_Dte_Dod2() { return pnd_Display_Dte_Dod2; }

    public DbsField getPnd_Display_Dte_Res() { return pnd_Display_Dte_Res; }

    public DbsField getPnd_Display_Dte_Pend() { return pnd_Display_Dte_Pend; }

    public DbsField getPnd_Display_Dte_1st_Pay_Due() { return pnd_Display_Dte_1st_Pay_Due; }

    public DbsField getPnd_Display_Dte_Iss() { return pnd_Display_Dte_Iss; }

    public DbsField getPnd_Display_Dte_Lst_Man_Chk() { return pnd_Display_Dte_Lst_Man_Chk; }

    public DbsField getPnd_Display_Dte_Fin_Per_Pay() { return pnd_Display_Dte_Fin_Per_Pay; }

    public DbsField getPnd_Display_Dte_Fin_Pymt() { return pnd_Display_Dte_Fin_Pymt; }

    public DbsField getPnd_Display_Dte_Wdrawal() { return pnd_Display_Dte_Wdrawal; }

    public DbsField getPnd_Display_Heading() { return pnd_Display_Heading; }

    public DbsField getPnd_Break_User_Area() { return pnd_Break_User_Area; }

    public DbsField getPnd_L_Newpage() { return pnd_L_Newpage; }

    public DbsField getPnd_Ctr() { return pnd_Ctr; }

    public DbsField getPnd_Total_Trans() { return pnd_Total_Trans; }

    public DbsField getPnd_User_Area_Trans() { return pnd_User_Area_Trans; }

    public DbsField getPnd_Userid_Trans() { return pnd_Userid_Trans; }

    public DbsField getPnd_Total_Per_Pay() { return pnd_Total_Per_Pay; }

    public DbsField getPnd_Total_Per_Div() { return pnd_Total_Per_Div; }

    public DbsField getPnd_Total_Fin_Pay() { return pnd_Total_Fin_Pay; }

    public DbsField getPnd_Total_Cref_Units() { return pnd_Total_Cref_Units; }

    public DbsField getPnd_Total_Per_Ded() { return pnd_Total_Per_Ded; }

    public DbsField getPnd_Total_Total_Ded() { return pnd_Total_Total_Ded; }

    public DbsField getPnd_Total_Hist_Ded() { return pnd_Total_Hist_Ded; }

    public DbsField getPnd_Total_Ivc_Amt() { return pnd_Total_Ivc_Amt; }

    public DbsField getPnd_Report() { return pnd_Report; }

    public DbsField getPnd_Save_User_Area() { return pnd_Save_User_Area; }

    public DbsField getPnd_Display_User_Area() { return pnd_Display_User_Area; }

    public DbsField getPnd_Save_Userid() { return pnd_Save_Userid; }

    public DbsField getPnd_Display_Userid() { return pnd_Display_Userid; }

    public DbsField getPnd_Save_Mode() { return pnd_Save_Mode; }

    public DbsField getPnd_Display_Mode() { return pnd_Display_Mode; }

    public DbsField getPnd_New_Trans_L() { return pnd_New_Trans_L; }

    public DbsField getPnd_First() { return pnd_First; }

    public DbsField getPnd_Wrote_Disp_050() { return pnd_Wrote_Disp_050; }

    public DbsField getPnd_Have_33() { return pnd_Have_33; }

    public DbsField getPnd_Have_50() { return pnd_Have_50; }

    public DbsField getPnd_Indx() { return pnd_Indx; }

    public DbsField getPnd_Indx1() { return pnd_Indx1; }

    public DbsField getPnd_Indx2() { return pnd_Indx2; }

    public DbsField getPnd_Disp_Dte() { return pnd_Disp_Dte; }

    public DbsGroup getPnd_Disp_DteRedef110() { return pnd_Disp_DteRedef110; }

    public DbsField getPnd_Disp_Dte_Pnd_Disp_Dte_Mm() { return pnd_Disp_Dte_Pnd_Disp_Dte_Mm; }

    public DbsField getPnd_Disp_Dte_Pnd_Disp_Dte_S1() { return pnd_Disp_Dte_Pnd_Disp_Dte_S1; }

    public DbsField getPnd_Disp_Dte_Pnd_Disp_Dte_Dd() { return pnd_Disp_Dte_Pnd_Disp_Dte_Dd; }

    public DbsField getPnd_Disp_Dte_Pnd_Disp_Dte_S2() { return pnd_Disp_Dte_Pnd_Disp_Dte_S2; }

    public DbsField getPnd_Disp_Dte_Pnd_Disp_Dte_Yy() { return pnd_Disp_Dte_Pnd_Disp_Dte_Yy; }

    public DbsField getPnd_Parm_Check_Dte_Disp() { return pnd_Parm_Check_Dte_Disp; }

    public DbsField getPnd_Parm_From_Dte_Disp() { return pnd_Parm_From_Dte_Disp; }

    public DbsField getPnd_Parm_To_Dte_Disp() { return pnd_Parm_To_Dte_Disp; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Program_Id = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Program_Id", "#PROGRAM-ID", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Filler1 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_Check_Dte = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_Check_DteRedef1 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Check_DteRedef1", "Redefines", iaa_Parm_Card_Pnd_Parm_Check_Dte);
        iaa_Parm_Card_Pnd_Parm_Check_Dte_N = iaa_Parm_Card_Pnd_Parm_Check_DteRedef1.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Check_Dte_N", "#PARM-CHECK-DTE-N", 
            FieldType.NUMERIC, 8);
        iaa_Parm_Card_Pnd_Parm_Check_DteRedef2 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Check_DteRedef2", "Redefines", iaa_Parm_Card_Pnd_Parm_Check_Dte);
        iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc = iaa_Parm_Card_Pnd_Parm_Check_DteRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc", "#PARM-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy = iaa_Parm_Card_Pnd_Parm_Check_DteRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy", "#PARM-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm = iaa_Parm_Card_Pnd_Parm_Check_DteRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm", "#PARM-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd = iaa_Parm_Card_Pnd_Parm_Check_DteRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd", "#PARM-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Filler2 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_From_Dte = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_From_Dte", "#PARM-FROM-DTE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_From_DteRedef3 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_From_DteRedef3", "Redefines", iaa_Parm_Card_Pnd_Parm_From_Dte);
        iaa_Parm_Card_Pnd_Parm_From_Dte_N = iaa_Parm_Card_Pnd_Parm_From_DteRedef3.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_From_Dte_N", "#PARM-FROM-DTE-N", 
            FieldType.NUMERIC, 8);
        iaa_Parm_Card_Pnd_Parm_From_DteRedef4 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_From_DteRedef4", "Redefines", iaa_Parm_Card_Pnd_Parm_From_Dte);
        iaa_Parm_Card_Pnd_Parm_From_Dte_Cc = iaa_Parm_Card_Pnd_Parm_From_DteRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_From_Dte_Cc", "#PARM-FROM-DTE-CC", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_From_Dte_Yy = iaa_Parm_Card_Pnd_Parm_From_DteRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_From_Dte_Yy", "#PARM-FROM-DTE-YY", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_From_Dte_Mm = iaa_Parm_Card_Pnd_Parm_From_DteRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_From_Dte_Mm", "#PARM-FROM-DTE-MM", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_From_Dte_Dd = iaa_Parm_Card_Pnd_Parm_From_DteRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_From_Dte_Dd", "#PARM-FROM-DTE-DD", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Filler3 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler3", "#FILLER3", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_To_Dte = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_To_Dte", "#PARM-TO-DTE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_To_DteRedef5 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_To_DteRedef5", "Redefines", iaa_Parm_Card_Pnd_Parm_To_Dte);
        iaa_Parm_Card_Pnd_Parm_To_Dte_N = iaa_Parm_Card_Pnd_Parm_To_DteRedef5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_To_Dte_N", "#PARM-TO-DTE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_To_DteRedef6 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_To_DteRedef6", "Redefines", iaa_Parm_Card_Pnd_Parm_To_Dte);
        iaa_Parm_Card_Pnd_Parm_To_Dte_Cc = iaa_Parm_Card_Pnd_Parm_To_DteRedef6.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_To_Dte_Cc", "#PARM-TO-DTE-CC", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_To_Dte_Yy = iaa_Parm_Card_Pnd_Parm_To_DteRedef6.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_To_Dte_Yy", "#PARM-TO-DTE-YY", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_To_Dte_Mm = iaa_Parm_Card_Pnd_Parm_To_DteRedef6.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_To_Dte_Mm", "#PARM-TO-DTE-MM", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Parm_To_Dte_Dd = iaa_Parm_Card_Pnd_Parm_To_DteRedef6.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_To_Dte_Dd", "#PARM-TO-DTE-DD", 
            FieldType.NUMERIC, 2);
        iaa_Parm_Card_Pnd_Filler4 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler4", "#FILLER4", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_User_Area = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_User_Area", "#PARM-USER-AREA", FieldType.STRING, 6);
        iaa_Parm_Card_Pnd_Filler5 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler5", "#FILLER5", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_User_Id = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_User_Id", "#PARM-USER-ID", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Filler6 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler6", "#FILLER6", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_Verify_Id = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Verify_Id", "#PARM-VERIFY-ID", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Filler7 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler7", "#FILLER7", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_Verify_Cde = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Verify_Cde", "#PARM-VERIFY-CDE", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Filler8 = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Filler8", "#FILLER8", FieldType.STRING, 1);
        iaa_Parm_Card_Pnd_Parm_Totals = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Totals", "#PARM-TOTALS", FieldType.STRING, 1);

        pnd_Input_Rec = newFieldInRecord("pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 120);
        pnd_Input_RecRedef7 = newGroupInRecord("pnd_Input_RecRedef7", "Redefines", pnd_Input_Rec);
        pnd_Input_Rec_Pnd_Check_Dte = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 6);
        pnd_Input_Rec_Pnd_User_Area = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 6);
        pnd_Input_Rec_Pnd_User_Id = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Input_Rec_Pnd_Trans_Dte = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Rec_Pnd_Trans_Cde = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Trans_CdeRedef8 = pnd_Input_RecRedef7.newGroupInGroup("pnd_Input_Rec_Pnd_Trans_CdeRedef8", "Redefines", pnd_Input_Rec_Pnd_Trans_Cde);
        pnd_Input_Rec_Pnd_Trans_Cde_N = pnd_Input_Rec_Pnd_Trans_CdeRedef8.newFieldInGroup("pnd_Input_Rec_Pnd_Trans_Cde_N", "#TRANS-CDE-N", FieldType.NUMERIC, 
            3);
        pnd_Input_Rec_Pnd_Contract_Nbr = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Input_Rec_Pnd_Payee = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Payee", "#PAYEE", FieldType.NUMERIC, 2);
        pnd_Input_Rec_Pnd_Xref = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Xref", "#XREF", FieldType.STRING, 9);
        pnd_Input_Rec_Pnd_Record_Nbr = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Record_Nbr", "#RECORD-NBR", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Rest_Of_Input = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Rest_Of_Input", "#REST-OF-INPUT", FieldType.STRING, 57);
        pnd_Input_Rec_Pnd_Multiple = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_Input_Rec_Pnd_Seq_Nbr = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Input_Rec_Pnd_Verify = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Input_Rec_Pnd_Mode = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        pnd_Input_Rec_Pnd_Filler1 = pnd_Input_RecRedef7.newFieldInGroup("pnd_Input_Rec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);

        pnd_Output_Rec = newFieldInRecord("pnd_Output_Rec", "#OUTPUT-REC", FieldType.STRING, 120);
        pnd_Output_RecRedef9 = newGroupInRecord("pnd_Output_RecRedef9", "Redefines", pnd_Output_Rec);
        pnd_Output_Rec_Pnd_Check_Dte = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 6);
        pnd_Output_Rec_Pnd_User_Area = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 6);
        pnd_Output_Rec_Pnd_User_Id = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Output_Rec_Pnd_Trans_Dte = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.NUMERIC, 8);
        pnd_Output_Rec_Pnd_Trans_Cde = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.STRING, 3);
        pnd_Output_Rec_Pnd_Trans_CdeRedef10 = pnd_Output_RecRedef9.newGroupInGroup("pnd_Output_Rec_Pnd_Trans_CdeRedef10", "Redefines", pnd_Output_Rec_Pnd_Trans_Cde);
        pnd_Output_Rec_Pnd_Trans_Cde_N = pnd_Output_Rec_Pnd_Trans_CdeRedef10.newFieldInGroup("pnd_Output_Rec_Pnd_Trans_Cde_N", "#TRANS-CDE-N", FieldType.NUMERIC, 
            3);
        pnd_Output_Rec_Pnd_Contract_Nbr = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Output_Rec_Pnd_Payee = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Payee", "#PAYEE", FieldType.NUMERIC, 2);
        pnd_Output_Rec_Pnd_Xref = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Xref", "#XREF", FieldType.STRING, 9);
        pnd_Output_Rec_Pnd_Record_Nbr = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Record_Nbr", "#RECORD-NBR", FieldType.STRING, 3);
        pnd_Output_Rec_Pnd_Rest_Of_Output = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Rest_Of_Output", "#REST-OF-OUTPUT", FieldType.STRING, 
            57);
        pnd_Output_Rec_Pnd_Multiple = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_Output_Rec_Pnd_Seq_Nbr = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Output_Rec_Pnd_Verify = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Output_Rec_Pnd_Mode = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        pnd_Output_Rec_Pnd_New_Trans = pnd_Output_RecRedef9.newFieldInGroup("pnd_Output_Rec_Pnd_New_Trans", "#NEW-TRANS", FieldType.STRING, 1);

        pnd_Misc_Non_Tax_Trans = newFieldInRecord("pnd_Misc_Non_Tax_Trans", "#MISC-NON-TAX-TRANS", FieldType.STRING, 120);
        pnd_Misc_Non_Tax_TransRedef11 = newGroupInRecord("pnd_Misc_Non_Tax_TransRedef11", "Redefines", pnd_Misc_Non_Tax_Trans);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 
            6);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12", "Redefines", 
            pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm = pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm", 
            "#CHECK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd = pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd", 
            "#CHECK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy = pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef12.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy", 
            "#CHECK-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_User_Area = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 
            6);
        pnd_Misc_Non_Tax_Trans_Pnd_User_Id = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_User_Id", "#USER-ID", FieldType.STRING, 
            8);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 
            8);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_DteRedef13 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_DteRedef13", "Redefines", 
            pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_N = pnd_Misc_Non_Tax_Trans_Pnd_Trans_DteRedef13.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_N", 
            "#TRANS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14 = pnd_Misc_Non_Tax_Trans_Pnd_Trans_DteRedef13.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_N);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Cc = pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Cc", 
            "#TRANS-DTE-CC", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Yy = pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Yy", 
            "#TRANS-DTE-YY", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Mm = pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Mm", 
            "#TRANS-DTE-MM", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Dd = pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_NRedef14.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Dd", 
            "#TRANS-DTE-DD", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 
            3);
        pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Misc_Non_Tax_Trans_Pnd_Record_Status = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Record_Status", "#RECORD-STATUS", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", 
            FieldType.STRING, 9);
        pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code", "#INTENT-CODE", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde", 
            "#HOLD-CHECK-RSN-CDE", FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde", "#SUS-PYMNT-RSN-CDE", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res", "#STATE-CNTRY-RES", 
            FieldType.STRING, 3);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A", "#1ST-ANNT-SEX-A", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef15 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef15", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef15.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex", 
            "#1ST-ANNT-SEX", FieldType.NUMERIC, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A", "#1ST-ANNT-DOB-A", 
            FieldType.STRING, 6);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm", 
            "#1ST-ANNT-DOB-MM", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd", 
            "#1ST-ANNT-DOB-DD", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef16.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy", 
            "#1ST-ANNT-DOB-YY", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A", "#1ST-ANNT-DOD-A", 
            FieldType.STRING, 4);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef17 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef17", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef17.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm", 
            "#1ST-ANNT-DOD-MM", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef17.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy", 
            "#1ST-ANNT-DOD-YY", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A", "#2ND-ANNT-SEX-A", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef18 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef18", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef18.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex", 
            "#2ND-ANNT-SEX", FieldType.NUMERIC, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A", "#2ND-ANNT-DOB-A", 
            FieldType.STRING, 6);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm", 
            "#2ND-ANNT-DOB-MM", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd", 
            "#2ND-ANNT-DOB-DD", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef19.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy", 
            "#2ND-ANNT-DOB-YY", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A", "#2ND-ANNT-DOD-A", 
            FieldType.STRING, 4);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef20 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef20", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef20.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm", 
            "#2ND-ANNT-DOD-MM", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef20.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy", 
            "#2ND-ANNT-DOD-YY", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A", 
            "#EFF-DTE-CHNG-RES-A", FieldType.STRING, 4);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef21 = pnd_Misc_Non_Tax_TransRedef11.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef21", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm = pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef21.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm", 
            "#EFF-DTE-CHNG-RES-MM", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy = pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef21.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy", 
            "#EFF-DTE-CHNG-RES-YY", FieldType.STRING, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Filler1 = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            28);
        pnd_Misc_Non_Tax_Trans_Pnd_Multiple = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 
            1);
        pnd_Misc_Non_Tax_Trans_Pnd_Seq_Nbr = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 
            4);
        pnd_Misc_Non_Tax_Trans_Pnd_Verify = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Verify", "#VERIFY", FieldType.STRING, 
            1);
        pnd_Misc_Non_Tax_Trans_Pnd_Filler2 = pnd_Misc_Non_Tax_TransRedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            4);

        pnd_Selection_Rcrd = newFieldInRecord("pnd_Selection_Rcrd", "#SELECTION-RCRD", FieldType.STRING, 120);
        pnd_Selection_RcrdRedef22 = newGroupInRecord("pnd_Selection_RcrdRedef22", "Redefines", pnd_Selection_Rcrd);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue1 = pnd_Selection_RcrdRedef22.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue1", "#MANUAL-NEW-ISSUE1", 
            FieldType.STRING, 120);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23 = pnd_Selection_RcrdRedef22.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Manual_New_Issue1);
        pnd_Selection_Rcrd_Pnd_Check_Dte1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte1", "#CHECK-DTE1", 
            FieldType.NUMERIC, 6);
        pnd_Selection_Rcrd_Pnd_Check_Dte1Redef24 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte1Redef24", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Check_Dte1);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Mm1 = pnd_Selection_Rcrd_Pnd_Check_Dte1Redef24.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Mm1", "#CHECK-DTE-MM1", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Dd1 = pnd_Selection_Rcrd_Pnd_Check_Dte1Redef24.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Dd1", "#CHECK-DTE-DD1", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Yy1 = pnd_Selection_Rcrd_Pnd_Check_Dte1Redef24.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Yy1", "#CHECK-DTE-YY1", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_User_Area1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Area1", "#USER-AREA1", 
            FieldType.STRING, 6);
        pnd_Selection_Rcrd_Pnd_User_Id1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Id1", "#USER-ID1", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1", "#TRANS-DTE1", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Trans_Dte1);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1_Cc = pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1_Cc", "#TRANS-DTE1-CC", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1_Yy = pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1_Yy", "#TRANS-DTE1-YY", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1_Mm = pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1_Mm", "#TRANS-DTE1-MM", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1_Dd = pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef25.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1_Dd", "#TRANS-DTE1-DD", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef26 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef26", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Trans_Dte1);
        pnd_Selection_Rcrd_Pnd_Trans_Dte1_N = pnd_Selection_Rcrd_Pnd_Trans_Dte1Redef26.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte1_N", "#TRANS-DTE1-N", 
            FieldType.NUMERIC, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Nbr1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Nbr1", "#TRANS-NBR1", 
            FieldType.NUMERIC, 3);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1", "#CNTRCT-NBR1", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1Redef27 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1Redef27", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N1 = pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1Redef27.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N1", "#CNTRCT-NBR-N1", 
            FieldType.NUMERIC, 1);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N7 = pnd_Selection_Rcrd_Pnd_Cntrct_Nbr1Redef27.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N7", "#CNTRCT-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Selection_Rcrd_Pnd_Record_Status1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Status1", 
            "#RECORD-STATUS1", FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1", 
            "#CROSS-REF-NBR1", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1", 
            "#RECORD-TYPE-NBR1", FieldType.NUMERIC, 3);
        pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1Redef28 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1Redef28", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1);
        pnd_Selection_Rcrd_Pnd_Record_Type1 = pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1Redef28.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Type1", "#RECORD-TYPE1", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Record_Nbr1 = pnd_Selection_Rcrd_Pnd_Record_Type_Nbr1Redef28.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Nbr1", "#RECORD-NBR1", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1", 
            "#REST-OF-ISSUE1", FieldType.STRING, 45);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1);
        pnd_Selection_Rcrd_Pnd_Product = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Product", "#PRODUCT", FieldType.STRING, 
            1);
        pnd_Selection_Rcrd_Pnd_Currency = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Currency", "#CURRENCY", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_CurrencyRedef30 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_CurrencyRedef30", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Currency);
        pnd_Selection_Rcrd_Pnd_Currency_N = pnd_Selection_Rcrd_Pnd_CurrencyRedef30.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Currency_N", "#CURRENCY-N", 
            FieldType.NUMERIC, 1);
        pnd_Selection_Rcrd_Pnd_Mode = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Mode", "#MODE", FieldType.STRING, 
            3);
        pnd_Selection_Rcrd_Pnd_ModeRedef31 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_ModeRedef31", "Redefines", 
            pnd_Selection_Rcrd_Pnd_Mode);
        pnd_Selection_Rcrd_Pnd_Mode_N = pnd_Selection_Rcrd_Pnd_ModeRedef31.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Mode_N", "#MODE-N", FieldType.NUMERIC, 
            3);
        pnd_Selection_Rcrd_Pnd_Pend_Code = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Pend_Code", "#PEND-CODE", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Hold_Check_Cde = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Hold_Check_Cde", 
            "#HOLD-CHECK-CDE", FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Pend_Dte = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Pend_Dte", "#PEND-DTE", 
            FieldType.STRING, 4);
        pnd_Selection_Rcrd_Pnd_Pend_DteRedef32 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Pend_DteRedef32", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Pend_Dte);
        pnd_Selection_Rcrd_Pnd_Pend_Dte_Mm = pnd_Selection_Rcrd_Pnd_Pend_DteRedef32.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Pend_Dte_Mm", "#PEND-DTE-MM", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Pend_Dte_Yy = pnd_Selection_Rcrd_Pnd_Pend_DteRedef32.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Pend_Dte_Yy", "#PEND-DTE-YY", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Option = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Option", "#OPTION", FieldType.STRING, 
            2);
        pnd_Selection_Rcrd_Pnd_OptionRedef33 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_OptionRedef33", "Redefines", 
            pnd_Selection_Rcrd_Pnd_Option);
        pnd_Selection_Rcrd_Pnd_Option_N = pnd_Selection_Rcrd_Pnd_OptionRedef33.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Option_N", "#OPTION-N", FieldType.NUMERIC, 
            2);
        pnd_Selection_Rcrd_Pnd_Origin = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Origin", "#ORIGIN", FieldType.STRING, 
            2);
        pnd_Selection_Rcrd_Pnd_Iss_Dte = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Iss_Dte", "#ISS-DTE", FieldType.STRING, 
            4);
        pnd_Selection_Rcrd_Pnd_Iss_DteRedef34 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Iss_DteRedef34", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Iss_Dte);
        pnd_Selection_Rcrd_Pnd_Iss_Dte_Mm = pnd_Selection_Rcrd_Pnd_Iss_DteRedef34.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Iss_Dte_Mm", "#ISS-DTE-MM", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Iss_Dte_Yy = pnd_Selection_Rcrd_Pnd_Iss_DteRedef34.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Iss_Dte_Yy", "#ISS-DTE-YY", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte", 
            "#1ST-PAY-DUE-DTE", FieldType.STRING, 4);
        pnd_Selection_Rcrd_Pnd_1st_Pay_Due_DteRedef35 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_1st_Pay_Due_DteRedef35", 
            "Redefines", pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte);
        pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm = pnd_Selection_Rcrd_Pnd_1st_Pay_Due_DteRedef35.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm", 
            "#1ST-PAY-DUE-DTE-MM", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy = pnd_Selection_Rcrd_Pnd_1st_Pay_Due_DteRedef35.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy", 
            "#1ST-PAY-DUE-DTE-YY", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte", 
            "#LST-MAN-CHK-DTE", FieldType.STRING, 4);
        pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_DteRedef36 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_DteRedef36", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte);
        pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm = pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_DteRedef36.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm", 
            "#LST-MAN-CHK-DTE-MM", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy = pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_DteRedef36.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy", 
            "#LST-MAN-CHK-DTE-YY", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte", 
            "#FIN-PER-PAY-DTE", FieldType.STRING, 4);
        pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_DteRedef37 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_DteRedef37", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte);
        pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm = pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_DteRedef37.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm", 
            "#FIN-PER-PAY-DTE-MM", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy = pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_DteRedef37.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy", 
            "#FIN-PER-PAY-DTE-YY", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte", "#FIN-PYMT-DTE", 
            FieldType.STRING, 6);
        pnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte);
        pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm = pnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm", 
            "#FIN-PYMT-DTE-MM", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd = pnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd", 
            "#FIN-PYMT-DTE-DD", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy = pnd_Selection_Rcrd_Pnd_Fin_Pymt_DteRedef38.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy", 
            "#FIN-PYMT-DTE-YY", FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Wdrawal_Dte = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Wdrawal_Dte", "#WDRAWAL-DTE", 
            FieldType.STRING, 4);
        pnd_Selection_Rcrd_Pnd_Wdrawal_DteRedef39 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue1Redef29.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Wdrawal_DteRedef39", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Wdrawal_Dte);
        pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm = pnd_Selection_Rcrd_Pnd_Wdrawal_DteRedef39.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm", "#WDRAWAL-DTE-MM", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy = pnd_Selection_Rcrd_Pnd_Wdrawal_DteRedef39.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy", "#WDRAWAL-DTE-YY", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Filler1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Filler1", "#FILLER1", 
            FieldType.STRING, 12);
        pnd_Selection_Rcrd_Pnd_Multiple1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Multiple1", "#MULTIPLE1", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Seq_Nbr1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Seq_Nbr1", "#SEQ-NBR1", 
            FieldType.NUMERIC, 4);
        pnd_Selection_Rcrd_Pnd_Verify1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Verify1", "#VERIFY1", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Mode1 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Mode1", "#MODE1", FieldType.NUMERIC, 
            3);
        pnd_Selection_Rcrd_Pnd_New_Trans = pnd_Selection_Rcrd_Pnd_Manual_New_Issue1Redef23.newFieldInGroup("pnd_Selection_Rcrd_Pnd_New_Trans", "#NEW-TRANS", 
            FieldType.STRING, 1);
        pnd_Selection_RcrdRedef40 = newGroupInRecord("pnd_Selection_RcrdRedef40", "Redefines", pnd_Selection_Rcrd);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue2 = pnd_Selection_RcrdRedef40.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue2", "#MANUAL-NEW-ISSUE2", 
            FieldType.STRING, 120);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41 = pnd_Selection_RcrdRedef40.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Manual_New_Issue2);
        pnd_Selection_Rcrd_Pnd_Check_Dte2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte2", "#CHECK-DTE2", 
            FieldType.NUMERIC, 6);
        pnd_Selection_Rcrd_Pnd_Check_Dte2Redef42 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte2Redef42", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Check_Dte2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Mm2 = pnd_Selection_Rcrd_Pnd_Check_Dte2Redef42.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Mm2", "#CHECK-DTE-MM2", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Dd2 = pnd_Selection_Rcrd_Pnd_Check_Dte2Redef42.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Dd2", "#CHECK-DTE-DD2", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Yy2 = pnd_Selection_Rcrd_Pnd_Check_Dte2Redef42.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Yy2", "#CHECK-DTE-YY2", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_User_Area2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Area2", "#USER-AREA2", 
            FieldType.STRING, 6);
        pnd_Selection_Rcrd_Pnd_User_Id2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Id2", "#USER-ID2", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte2", "#TRANS-DTE2", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte2Redef43 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte2Redef43", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Trans_Dte2);
        pnd_Selection_Rcrd_Pnd_Trans_Dte2_N = pnd_Selection_Rcrd_Pnd_Trans_Dte2Redef43.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte2_N", "#TRANS-DTE2-N", 
            FieldType.NUMERIC, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Nbr2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Nbr2", "#TRANS-NBR2", 
            FieldType.NUMERIC, 3);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr2", "#CNTRCT-NBR2", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Record_Status2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Status2", 
            "#RECORD-STATUS2", FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr2", 
            "#CROSS-REF-NBR2", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Record_Type2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Type2", "#RECORD-TYPE2", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Record_Nbr2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Nbr2", "#RECORD-NBR2", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2", 
            "#REST-OF-ISSUE2", FieldType.STRING, 45);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue2Redef41.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2);
        pnd_Selection_Rcrd_Pnd_Citizen = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Citizen", "#CITIZEN", FieldType.NUMERIC, 
            3);
        pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss", 
            "#ST/CNTRY-ISS", FieldType.STRING, 3);
        pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Res = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Res", 
            "#ST/CNTRY-RES", FieldType.STRING, 3);
        pnd_Selection_Rcrd_Pnd_Coll_Iss = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Coll_Iss", "#COLL-ISS", 
            FieldType.STRING, 5);
        pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt", 
            "#RTB/TTB-AMT", FieldType.DECIMAL, 9,2);
        pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_AmtRedef45 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_AmtRedef45", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt);
        pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt_A = pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_AmtRedef45.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt_A", 
            "#RTB/TTB-AMT-A", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Ssn = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 
            9);
        pnd_Selection_Rcrd_Pnd_Joint_Cnvrt = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Joint_Cnvrt", "#JOINT-CNVRT", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Spirt = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Spirt", "#SPIRT", FieldType.STRING, 
            1);
        pnd_Selection_Rcrd_Pnd_Pen_Pln_Cde = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Pen_Pln_Cde", "#PEN-PLN-CDE", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Cntrct_Type = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue2Redef44.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Type", "#CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Selection_RcrdRedef46 = newGroupInRecord("pnd_Selection_RcrdRedef46", "Redefines", pnd_Selection_Rcrd);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue3 = pnd_Selection_RcrdRedef46.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue3", "#MANUAL-NEW-ISSUE3", 
            FieldType.STRING, 120);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47 = pnd_Selection_RcrdRedef46.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Manual_New_Issue3);
        pnd_Selection_Rcrd_Pnd_Check_Dte3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte3", "#CHECK-DTE3", 
            FieldType.NUMERIC, 6);
        pnd_Selection_Rcrd_Pnd_Check_Dte3Redef48 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte3Redef48", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Check_Dte3);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Mm3 = pnd_Selection_Rcrd_Pnd_Check_Dte3Redef48.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Mm3", "#CHECK-DTE-MM3", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Dd3 = pnd_Selection_Rcrd_Pnd_Check_Dte3Redef48.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Dd3", "#CHECK-DTE-DD3", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Yy3 = pnd_Selection_Rcrd_Pnd_Check_Dte3Redef48.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Yy3", "#CHECK-DTE-YY3", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_User_Area3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Area3", "#USER-AREA3", 
            FieldType.STRING, 6);
        pnd_Selection_Rcrd_Pnd_User_Id3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Id3", "#USER-ID3", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte3", "#TRANS-DTE3", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte3Redef49 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte3Redef49", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Trans_Dte3);
        pnd_Selection_Rcrd_Pnd_Trans_Dte3_N = pnd_Selection_Rcrd_Pnd_Trans_Dte3Redef49.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte3_N", "#TRANS-DTE3-N", 
            FieldType.NUMERIC, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Nbr3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Nbr3", "#TRANS-NBR3", 
            FieldType.NUMERIC, 3);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr3", "#CNTRCT-NBR3", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Record_Status3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Status3", 
            "#RECORD-STATUS3", FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr3", 
            "#CROSS-REF-NBR3", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Record_Type3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Type3", "#RECORD-TYPE3", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Record_Nbr3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Nbr3", "#RECORD-NBR3", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3", 
            "#REST-OF-ISSUE3", FieldType.STRING, 45);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue3Redef47.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3);
        pnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref", 
            "#1ST-ANNT-X-REF", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newGroupInGroup("pnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51", 
            "Redefines", pnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref);
        pnd_Selection_Rcrd_Pnd_1st_Last_Name = pnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Last_Name", "#1ST-LAST-NAME", 
            FieldType.STRING, 7);
        pnd_Selection_Rcrd_Pnd_1st_First_Init = pnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_First_Init", 
            "#1ST-FIRST-INIT", FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_1st_Mid_Init = pnd_Selection_Rcrd_Pnd_1st_Annt_X_RefRedef51.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Mid_Init", "#1ST-MID-INIT", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_1st_Sex = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Sex", "#1ST-SEX", FieldType.STRING, 
            1);
        pnd_Selection_Rcrd_Pnd_1st_Dob = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Dob", "#1ST-DOB", FieldType.NUMERIC, 
            6);
        pnd_Selection_Rcrd_Pnd_1st_DobRedef52 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newGroupInGroup("pnd_Selection_Rcrd_Pnd_1st_DobRedef52", 
            "Redefines", pnd_Selection_Rcrd_Pnd_1st_Dob);
        pnd_Selection_Rcrd_Pnd_1st_Dob_Mm = pnd_Selection_Rcrd_Pnd_1st_DobRedef52.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Dob_Mm", "#1ST-DOB-MM", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_1st_Dob_Dd = pnd_Selection_Rcrd_Pnd_1st_DobRedef52.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Dob_Dd", "#1ST-DOB-DD", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_1st_Dob_Yy = pnd_Selection_Rcrd_Pnd_1st_DobRedef52.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Dob_Yy", "#1ST-DOB-YY", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_1st_Dod = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Dod", "#1ST-DOD", FieldType.NUMERIC, 
            4);
        pnd_Selection_Rcrd_Pnd_1st_DodRedef53 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newGroupInGroup("pnd_Selection_Rcrd_Pnd_1st_DodRedef53", 
            "Redefines", pnd_Selection_Rcrd_Pnd_1st_Dod);
        pnd_Selection_Rcrd_Pnd_1st_Dod_Mm = pnd_Selection_Rcrd_Pnd_1st_DodRedef53.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Dod_Mm", "#1ST-DOD-MM", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_1st_Dod_Yy = pnd_Selection_Rcrd_Pnd_1st_DodRedef53.newFieldInGroup("pnd_Selection_Rcrd_Pnd_1st_Dod_Yy", "#1ST-DOD-YY", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Mort_Yob3 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Mort_Yob3", "#MORT-YOB3", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Mort_Yob3Redef54 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Mort_Yob3Redef54", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Mort_Yob3);
        pnd_Selection_Rcrd_Pnd_Mort_Yob3_Yy = pnd_Selection_Rcrd_Pnd_Mort_Yob3Redef54.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Mort_Yob3_Yy", "#MORT-YOB3-YY", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Life_Cnt3 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Life_Cnt3", "#LIFE-CNT3", 
            FieldType.NUMERIC, 1);
        pnd_Selection_Rcrd_Pnd_Div_Payee = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Div_Payee", "#DIV-PAYEE", 
            FieldType.NUMERIC, 1);
        pnd_Selection_Rcrd_Pnd_Coll_Cde = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Coll_Cde", "#COLL-CDE", 
            FieldType.STRING, 5);
        pnd_Selection_Rcrd_Pnd_Orig_Cntrct = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Orig_Cntrct", "#ORIG-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Cash_Cde = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cash_Cde", "#CASH-CDE", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Emp_Term = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue3Redef50.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Emp_Term", "#EMP-TERM", 
            FieldType.STRING, 1);
        pnd_Selection_RcrdRedef55 = newGroupInRecord("pnd_Selection_RcrdRedef55", "Redefines", pnd_Selection_Rcrd);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue4 = pnd_Selection_RcrdRedef55.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue4", "#MANUAL-NEW-ISSUE4", 
            FieldType.STRING, 120);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56 = pnd_Selection_RcrdRedef55.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Manual_New_Issue4);
        pnd_Selection_Rcrd_Pnd_Check_Dte4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte4", "#CHECK-DTE4", 
            FieldType.NUMERIC, 6);
        pnd_Selection_Rcrd_Pnd_Check_Dte4Redef57 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte4Redef57", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Check_Dte4);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Mm4 = pnd_Selection_Rcrd_Pnd_Check_Dte4Redef57.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Mm4", "#CHECK-DTE-MM4", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Dd4 = pnd_Selection_Rcrd_Pnd_Check_Dte4Redef57.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Dd4", "#CHECK-DTE-DD4", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Yy4 = pnd_Selection_Rcrd_Pnd_Check_Dte4Redef57.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Yy4", "#CHECK-DTE-YY4", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_User_Area4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Area4", "#USER-AREA4", 
            FieldType.STRING, 6);
        pnd_Selection_Rcrd_Pnd_User_Id4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Id4", "#USER-ID4", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte4", "#TRANS-DTE4", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte4Redef58 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte4Redef58", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Trans_Dte4);
        pnd_Selection_Rcrd_Pnd_Trans_Dte4_N = pnd_Selection_Rcrd_Pnd_Trans_Dte4Redef58.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte4_N", "#TRANS-DTE4-N", 
            FieldType.NUMERIC, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Nbr4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Nbr4", "#TRANS-NBR4", 
            FieldType.NUMERIC, 3);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr4", "#CNTRCT-NBR4", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Record_Status4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Status4", 
            "#RECORD-STATUS4", FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr4", 
            "#CROSS-REF-NBR4", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Record_Type4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Type4", "#RECORD-TYPE4", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Record_Nbr4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Nbr4", "#RECORD-NBR4", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4", 
            "#REST-OF-ISSUE4", FieldType.STRING, 45);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue4Redef56.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4);
        pnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref", 
            "#2ND-ANNT-X-REF", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newGroupInGroup("pnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60", 
            "Redefines", pnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref);
        pnd_Selection_Rcrd_Pnd_2nd_Last_Name = pnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Last_Name", "#2ND-LAST-NAME", 
            FieldType.STRING, 7);
        pnd_Selection_Rcrd_Pnd_2nd_First_Init = pnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_First_Init", 
            "#2ND-FIRST-INIT", FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_2nd_Mid_Init = pnd_Selection_Rcrd_Pnd_2nd_Annt_X_RefRedef60.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Mid_Init", "#2ND-MID-INIT", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_2nd_Sex = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Sex", "#2ND-SEX", FieldType.STRING, 
            1);
        pnd_Selection_Rcrd_Pnd_2nd_Dob = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Dob", "#2ND-DOB", FieldType.NUMERIC, 
            6);
        pnd_Selection_Rcrd_Pnd_2nd_DobRedef61 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newGroupInGroup("pnd_Selection_Rcrd_Pnd_2nd_DobRedef61", 
            "Redefines", pnd_Selection_Rcrd_Pnd_2nd_Dob);
        pnd_Selection_Rcrd_Pnd_2nd_Dob_Mm = pnd_Selection_Rcrd_Pnd_2nd_DobRedef61.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Dob_Mm", "#2ND-DOB-MM", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_2nd_Dob_Dd = pnd_Selection_Rcrd_Pnd_2nd_DobRedef61.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Dob_Dd", "#2ND-DOB-DD", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_2nd_Dob_Yy = pnd_Selection_Rcrd_Pnd_2nd_DobRedef61.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Dob_Yy", "#2ND-DOB-YY", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_2nd_Dod = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Dod", "#2ND-DOD", FieldType.NUMERIC, 
            4);
        pnd_Selection_Rcrd_Pnd_2nd_DodRedef62 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newGroupInGroup("pnd_Selection_Rcrd_Pnd_2nd_DodRedef62", 
            "Redefines", pnd_Selection_Rcrd_Pnd_2nd_Dod);
        pnd_Selection_Rcrd_Pnd_2nd_Dod_Mm = pnd_Selection_Rcrd_Pnd_2nd_DodRedef62.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Dod_Mm", "#2ND-DOD-MM", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_2nd_Dod_Yy = pnd_Selection_Rcrd_Pnd_2nd_DodRedef62.newFieldInGroup("pnd_Selection_Rcrd_Pnd_2nd_Dod_Yy", "#2ND-DOD-YY", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Mort_Yob4 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Mort_Yob4", "#MORT-YOB4", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Mort_Yob4Redef63 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Mort_Yob4Redef63", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Mort_Yob4);
        pnd_Selection_Rcrd_Pnd_Mort_Yob4_Yy = pnd_Selection_Rcrd_Pnd_Mort_Yob4Redef63.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Mort_Yob4_Yy", "#MORT-YOB4-YY", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Life_Cnt4 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Life_Cnt4", "#LIFE-CNT4", 
            FieldType.NUMERIC, 1);
        pnd_Selection_Rcrd_Pnd_Ben_Xref = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Ben_Xref", "#BEN-XREF", 
            FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Ben_Dod = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Ben_Dod", "#BEN-DOD", FieldType.NUMERIC, 
            4);
        pnd_Selection_Rcrd_Pnd_Ben_DodRedef64 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Ben_DodRedef64", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Ben_Dod);
        pnd_Selection_Rcrd_Pnd_Ben_Dod_A = pnd_Selection_Rcrd_Pnd_Ben_DodRedef64.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Ben_Dod_A", "#BEN-DOD-A", FieldType.STRING, 
            4);
        pnd_Selection_Rcrd_Pnd_Ben_DodRedef65 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Ben_DodRedef65", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Ben_Dod);
        pnd_Selection_Rcrd_Pnd_Ben_Dod_Mm = pnd_Selection_Rcrd_Pnd_Ben_DodRedef65.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Ben_Dod_Mm", "#BEN-DOD-MM", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Ben_Dod_Yy = pnd_Selection_Rcrd_Pnd_Ben_DodRedef65.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Ben_Dod_Yy", "#BEN-DOD-YY", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Dest_Prev = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Dest_Prev", "#DEST-PREV", 
            FieldType.STRING, 4);
        pnd_Selection_Rcrd_Pnd_Dest_Curr = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue4Redef59.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Dest_Curr", "#DEST-CURR", 
            FieldType.STRING, 4);
        pnd_Selection_RcrdRedef66 = newGroupInRecord("pnd_Selection_RcrdRedef66", "Redefines", pnd_Selection_Rcrd);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue5 = pnd_Selection_RcrdRedef66.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue5", "#MANUAL-NEW-ISSUE5", 
            FieldType.STRING, 120);
        pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67 = pnd_Selection_RcrdRedef66.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Manual_New_Issue5);
        pnd_Selection_Rcrd_Pnd_Check_Dte5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte5", "#CHECK-DTE5", 
            FieldType.NUMERIC, 6);
        pnd_Selection_Rcrd_Pnd_Check_Dte5Redef68 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte5Redef68", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Check_Dte5);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Mm5 = pnd_Selection_Rcrd_Pnd_Check_Dte5Redef68.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Mm5", "#CHECK-DTE-MM5", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Dd5 = pnd_Selection_Rcrd_Pnd_Check_Dte5Redef68.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Dd5", "#CHECK-DTE-DD5", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Check_Dte_Yy5 = pnd_Selection_Rcrd_Pnd_Check_Dte5Redef68.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Check_Dte_Yy5", "#CHECK-DTE-YY5", 
            FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_User_Area5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Area5", "#USER-AREA5", 
            FieldType.STRING, 6);
        pnd_Selection_Rcrd_Pnd_User_Id5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_User_Id5", "#USER-ID5", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte5", "#TRANS-DTE5", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Dte5Redef69 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte5Redef69", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Trans_Dte5);
        pnd_Selection_Rcrd_Pnd_Trans_Dte5_N = pnd_Selection_Rcrd_Pnd_Trans_Dte5Redef69.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Dte5_N", "#TRANS-DTE5-N", 
            FieldType.NUMERIC, 8);
        pnd_Selection_Rcrd_Pnd_Trans_Nbr5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Trans_Nbr5", "#TRANS-NBR5", 
            FieldType.NUMERIC, 3);
        pnd_Selection_Rcrd_Pnd_Cntrct_Nbr5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cntrct_Nbr5", "#CNTRCT-NBR5", 
            FieldType.STRING, 8);
        pnd_Selection_Rcrd_Pnd_Record_Status5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Status5", 
            "#RECORD-STATUS5", FieldType.NUMERIC, 2);
        pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr5", 
            "#CROSS-REF-NBR5", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Record_Type5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Type5", "#RECORD-TYPE5", 
            FieldType.STRING, 2);
        pnd_Selection_Rcrd_Pnd_Record_Nbr5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Nbr5", "#RECORD-NBR5", 
            FieldType.STRING, 1);
        pnd_Selection_Rcrd_Pnd_Record_Nbr5Redef70 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Record_Nbr5Redef70", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Record_Nbr5);
        pnd_Selection_Rcrd_Pnd_Record_Nbr5_N = pnd_Selection_Rcrd_Pnd_Record_Nbr5Redef70.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Record_Nbr5_N", "#RECORD-NBR5-N", 
            FieldType.NUMERIC, 1);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5", 
            "#REST-OF-ISSUE5", FieldType.STRING, 53);
        pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71 = pnd_Selection_Rcrd_Pnd_Manual_New_Issue5Redef67.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5);
        pnd_Selection_Rcrd_Pnd_Rate = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Rate", "#RATE", FieldType.STRING, 
            2);
        pnd_Selection_Rcrd_Pnd_Per_Pay = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Per_Pay", "#PER-PAY", FieldType.PACKED_DECIMAL, 
            13,2);
        pnd_Selection_Rcrd_Pnd_Per_PayRedef72 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Per_PayRedef72", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Per_Pay);
        pnd_Selection_Rcrd_Pnd_Per_Pay_A = pnd_Selection_Rcrd_Pnd_Per_PayRedef72.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Per_Pay_A", "#PER-PAY-A", FieldType.STRING, 
            7);
        pnd_Selection_Rcrd_Pnd_Per_PayRedef73 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Per_PayRedef73", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Per_Pay);
        pnd_Selection_Rcrd_Pnd_Cref_Units = pnd_Selection_Rcrd_Pnd_Per_PayRedef73.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cref_Units", "#CREF-UNITS", 
            FieldType.PACKED_DECIMAL, 13,3);
        pnd_Selection_Rcrd_Pnd_Cref_UnitsRedef74 = pnd_Selection_Rcrd_Pnd_Per_PayRedef73.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Cref_UnitsRedef74", "Redefines", 
            pnd_Selection_Rcrd_Pnd_Cref_Units);
        pnd_Selection_Rcrd_Pnd_Cref_Units_A = pnd_Selection_Rcrd_Pnd_Cref_UnitsRedef74.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Cref_Units_A", "#CREF-UNITS-A", 
            FieldType.STRING, 7);
        pnd_Selection_Rcrd_Pnd_Per_Div = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Per_Div", "#PER-DIV", FieldType.DECIMAL, 
            9,2);
        pnd_Selection_Rcrd_Pnd_Per_DivRedef75 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Per_DivRedef75", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Per_Div);
        pnd_Selection_Rcrd_Pnd_Per_Div_A = pnd_Selection_Rcrd_Pnd_Per_DivRedef75.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Per_Div_A", "#PER-DIV-A", FieldType.STRING, 
            7);
        pnd_Selection_Rcrd_Pnd_Fin_Pay = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Pay", "#FIN-PAY", FieldType.DECIMAL, 
            9,2);
        pnd_Selection_Rcrd_Pnd_Fin_PayRedef76 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Fin_PayRedef76", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Fin_Pay);
        pnd_Selection_Rcrd_Pnd_Fin_Pay_A = pnd_Selection_Rcrd_Pnd_Fin_PayRedef76.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Fin_Pay_A", "#FIN-PAY-A", FieldType.STRING, 
            9);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div", 
            "#TOT-OLD-PER-DIV", FieldType.DECIMAL, 9,2);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Per_DivRedef77 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Per_DivRedef77", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div_A = pnd_Selection_Rcrd_Pnd_Tot_Old_Per_DivRedef77.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div_A", 
            "#TOT-OLD-PER-DIV-A", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay", 
            "#TOT-OLD-PER-PAY", FieldType.DECIMAL, 9,2);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef78 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef78", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay_A = pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef78.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay_A", 
            "#TOT-OLD-PER-PAY-A", FieldType.STRING, 9);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef79 = pnd_Selection_Rcrd_Pnd_Rest_Of_Issue5Redef71.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef79", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units = pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef79.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units", 
            "#TOT-OLD-CREF-UNITS", FieldType.DECIMAL, 9,3);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_UnitsRedef80 = pnd_Selection_Rcrd_Pnd_Tot_Old_Per_PayRedef79.newGroupInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_UnitsRedef80", 
            "Redefines", pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units);
        pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units_A = pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_UnitsRedef80.newFieldInGroup("pnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units_A", 
            "#TOT-OLD-CREF-UNITS-A", FieldType.STRING, 9);

        pnd_Tax = newFieldInRecord("pnd_Tax", "#TAX", FieldType.STRING, 120);
        pnd_TaxRedef81 = newGroupInRecord("pnd_TaxRedef81", "Redefines", pnd_Tax);
        pnd_Tax_Pnd_Check_Dte = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 6);
        pnd_Tax_Pnd_Check_DteRedef82 = pnd_TaxRedef81.newGroupInGroup("pnd_Tax_Pnd_Check_DteRedef82", "Redefines", pnd_Tax_Pnd_Check_Dte);
        pnd_Tax_Pnd_Check_Dte_Mm = pnd_Tax_Pnd_Check_DteRedef82.newFieldInGroup("pnd_Tax_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_Check_Dte_Dd = pnd_Tax_Pnd_Check_DteRedef82.newFieldInGroup("pnd_Tax_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_Check_Dte_Yy = pnd_Tax_Pnd_Check_DteRedef82.newFieldInGroup("pnd_Tax_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_User_Area = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 6);
        pnd_Tax_Pnd_User_Id = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Tax_Pnd_Trans_Dte = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Tax_Pnd_Trans_DteRedef83 = pnd_TaxRedef81.newGroupInGroup("pnd_Tax_Pnd_Trans_DteRedef83", "Redefines", pnd_Tax_Pnd_Trans_Dte);
        pnd_Tax_Pnd_Trans_Dte_Cc = pnd_Tax_Pnd_Trans_DteRedef83.newFieldInGroup("pnd_Tax_Pnd_Trans_Dte_Cc", "#TRANS-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_Trans_Dte_Yy = pnd_Tax_Pnd_Trans_DteRedef83.newFieldInGroup("pnd_Tax_Pnd_Trans_Dte_Yy", "#TRANS-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_Trans_Dte_Mm = pnd_Tax_Pnd_Trans_DteRedef83.newFieldInGroup("pnd_Tax_Pnd_Trans_Dte_Mm", "#TRANS-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_Trans_Dte_Dd = pnd_Tax_Pnd_Trans_DteRedef83.newFieldInGroup("pnd_Tax_Pnd_Trans_Dte_Dd", "#TRANS-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_Trans_DteRedef84 = pnd_TaxRedef81.newGroupInGroup("pnd_Tax_Pnd_Trans_DteRedef84", "Redefines", pnd_Tax_Pnd_Trans_Dte);
        pnd_Tax_Pnd_Trans_Dte_N = pnd_Tax_Pnd_Trans_DteRedef84.newFieldInGroup("pnd_Tax_Pnd_Trans_Dte_N", "#TRANS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Tax_Pnd_Trans_Nbr = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 3);
        pnd_Tax_Pnd_Cntrct_Nbr = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Tax_Pnd_Record_Status = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Record_Status", "#RECORD-STATUS", FieldType.NUMERIC, 2);
        pnd_Tax_Pnd_Cross_Ref_Nbr = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 9);
        pnd_Tax_Pnd_Intent_Code = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 1);
        pnd_Tax_Pnd_Filler1 = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Filler1", "#FILLER1", FieldType.STRING, 30);
        pnd_Tax_Pnd_Invest_In_Cntrct_A = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Invest_In_Cntrct_A", "#INVEST-IN-CNTRCT-A", FieldType.STRING, 9);
        pnd_Tax_Pnd_Invest_In_Cntrct_ARedef85 = pnd_TaxRedef81.newGroupInGroup("pnd_Tax_Pnd_Invest_In_Cntrct_ARedef85", "Redefines", pnd_Tax_Pnd_Invest_In_Cntrct_A);
        pnd_Tax_Pnd_Invest_In_Cntrct_N = pnd_Tax_Pnd_Invest_In_Cntrct_ARedef85.newFieldInGroup("pnd_Tax_Pnd_Invest_In_Cntrct_N", "#INVEST-IN-CNTRCT-N", 
            FieldType.DECIMAL, 9,2);
        pnd_Tax_Pnd_Filler2 = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Filler2", "#FILLER2", FieldType.STRING, 20);
        pnd_Tax_Pnd_Multiple = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_Tax_Pnd_Seq_Nbr = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Tax_Pnd_Verify = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Tax_Pnd_Filler3 = pnd_TaxRedef81.newFieldInGroup("pnd_Tax_Pnd_Filler3", "#FILLER3", FieldType.STRING, 4);

        pnd_Comb_Chk_Trans = newFieldInRecord("pnd_Comb_Chk_Trans", "#COMB-CHK-TRANS", FieldType.STRING, 120);
        pnd_Comb_Chk_TransRedef86 = newGroupInRecord("pnd_Comb_Chk_TransRedef86", "Redefines", pnd_Comb_Chk_Trans);
        pnd_Comb_Chk_Trans_Pnd_Check_Dte = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 
            6);
        pnd_Comb_Chk_Trans_Pnd_Check_DteRedef87 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Check_DteRedef87", "Redefines", pnd_Comb_Chk_Trans_Pnd_Check_Dte);
        pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm = pnd_Comb_Chk_Trans_Pnd_Check_DteRedef87.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd = pnd_Comb_Chk_Trans_Pnd_Check_DteRedef87.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy = pnd_Comb_Chk_Trans_Pnd_Check_DteRedef87.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_User_Area = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 
            6);
        pnd_Comb_Chk_Trans_Pnd_User_Id = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 
            8);
        pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88", "Redefines", pnd_Comb_Chk_Trans_Pnd_Trans_Dte);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Cc = pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Cc", "#TRANS-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Yy = pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Yy", "#TRANS-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Mm = pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Mm", "#TRANS-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Dd = pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef88.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte_Dd", "#TRANS-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef89 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef89", "Redefines", pnd_Comb_Chk_Trans_Pnd_Trans_Dte);
        pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N = pnd_Comb_Chk_Trans_Pnd_Trans_DteRedef89.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Dte_N", "#TRANS-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Comb_Chk_Trans_Pnd_Trans_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 
            3);
        pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Comb_Chk_Trans_Pnd_Rcrd_Status = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Rcrd_Status", "#RCRD-STATUS", FieldType.NUMERIC, 
            2);
        pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_Comb_Chk_Trans_Pnd_Intent_Code = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Rcrd_Nbr", "#RCRD-NBR", FieldType.STRING, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Currency = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Currency", "#CURRENCY", FieldType.NUMERIC, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Annt = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt", "#ANNT", FieldType.STRING, 9);
        pnd_Comb_Chk_Trans_Pnd_AnntRedef90 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_AnntRedef90", "Redefines", pnd_Comb_Chk_Trans_Pnd_Annt);
        pnd_Comb_Chk_Trans_Pnd_Ssn = pnd_Comb_Chk_Trans_Pnd_AnntRedef90.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Comb_Chk_Trans_Pnd_AnntRedef91 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_AnntRedef91", "Redefines", pnd_Comb_Chk_Trans_Pnd_Annt);
        pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A = pnd_Comb_Chk_Trans_Pnd_AnntRedef91.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A", "#ANNT-SEX-A", FieldType.STRING, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef92 = pnd_Comb_Chk_Trans_Pnd_AnntRedef91.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef92", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_Annt_Sex_A);
        pnd_Comb_Chk_Trans_Pnd_Annt_Sex = pnd_Comb_Chk_Trans_Pnd_Annt_Sex_ARedef92.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Sex", "#ANNT-SEX", FieldType.NUMERIC, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A = pnd_Comb_Chk_Trans_Pnd_AnntRedef91.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A", "#ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93 = pnd_Comb_Chk_Trans_Pnd_AnntRedef91.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_Annt_Dob_A);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm = pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm", "#ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd = pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd", "#ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy = pnd_Comb_Chk_Trans_Pnd_Annt_Dob_ARedef93.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy", "#ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct", "#2ND-COMB-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef94 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef94", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct);
        pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct = pnd_Comb_Chk_Trans_Pnd_2nd_Comb_CntrctRedef94.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct", 
            "#5TH-COMB-CNTRCT", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr", "#2ND-STA-NBR", FieldType.STRING, 
            2);
        pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef95 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef95", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr);
        pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr = pnd_Comb_Chk_Trans_Pnd_2nd_Sta_NbrRedef95.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr", "#5TH-STA-NBR", 
            FieldType.STRING, 2);
        pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct", "#3RD-COMB-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef96 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef96", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct);
        pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct = pnd_Comb_Chk_Trans_Pnd_3rd_Comb_CntrctRedef96.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct", 
            "#6TH-COMB-CNTRCT", FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr", "#3RD-STA-NBR", FieldType.STRING, 
            2);
        pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef97 = pnd_Comb_Chk_TransRedef86.newGroupInGroup("pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef97", "Redefines", 
            pnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr);
        pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr = pnd_Comb_Chk_Trans_Pnd_3rd_Sta_NbrRedef97.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr", "#6TH-STA-NBR", 
            FieldType.STRING, 2);
        pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct", "#4TH-COMB-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr", "#4TH-STA-NBR", FieldType.STRING, 
            2);
        pnd_Comb_Chk_Trans_Pnd_Filler1 = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Filler1", "#FILLER1", FieldType.STRING, 18);
        pnd_Comb_Chk_Trans_Pnd_Multiple = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 
            1);
        pnd_Comb_Chk_Trans_Pnd_Seq_Nbr = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Comb_Chk_Trans_Pnd_Verify = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Comb_Chk_Trans_Pnd_Filler2 = pnd_Comb_Chk_TransRedef86.newFieldInGroup("pnd_Comb_Chk_Trans_Pnd_Filler2", "#FILLER2", FieldType.STRING, 4);

        pnd_Ddctn_From_Net = newFieldInRecord("pnd_Ddctn_From_Net", "#DDCTN-FROM-NET", FieldType.STRING, 120);
        pnd_Ddctn_From_NetRedef98 = newGroupInRecord("pnd_Ddctn_From_NetRedef98", "Redefines", pnd_Ddctn_From_Net);
        pnd_Ddctn_From_Net_Pnd_Check_Dte = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 
            6);
        pnd_Ddctn_From_Net_Pnd_Check_DteRedef99 = pnd_Ddctn_From_NetRedef98.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Check_DteRedef99", "Redefines", pnd_Ddctn_From_Net_Pnd_Check_Dte);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm = pnd_Ddctn_From_Net_Pnd_Check_DteRedef99.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd = pnd_Ddctn_From_Net_Pnd_Check_DteRedef99.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy = pnd_Ddctn_From_Net_Pnd_Check_DteRedef99.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_User_Area = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 
            6);
        pnd_Ddctn_From_Net_Pnd_User_Id = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 
            8);
        pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100 = pnd_Ddctn_From_NetRedef98.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Trans_Dte);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte_Cc = pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte_Cc", "#TRANS-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte_Yy = pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte_Yy", "#TRANS-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte_Mm = pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte_Mm", "#TRANS-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte_Dd = pnd_Ddctn_From_Net_Pnd_Trans_DteRedef100.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte_Dd", "#TRANS-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Trans_DteRedef101 = pnd_Ddctn_From_NetRedef98.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Trans_DteRedef101", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Trans_Dte);
        pnd_Ddctn_From_Net_Pnd_Trans_Dte_N = pnd_Ddctn_From_Net_Pnd_Trans_DteRedef101.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Dte_N", "#TRANS-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Ddctn_From_Net_Pnd_Trans_Nbr = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 
            3);
        pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Ddctn_From_Net_Pnd_Record_Status = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Record_Status", "#RECORD-STATUS", FieldType.NUMERIC, 
            2);
        pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_Ddctn_From_Net_Pnd_Intent_Code = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_Ddctn_From_Net_Pnd_Sequence_Nbr = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Sequence_Nbr", "#SEQUENCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Ddctn_From_Net_Pnd_Ddctn_Cde = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_Ddctn_From_Net_Pnd_Ddctn_Payee = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt", "#PER-DDCTN-AMT", FieldType.STRING, 
            7);
        pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef102 = pnd_Ddctn_From_NetRedef98.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef102", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt);
        pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N = pnd_Ddctn_From_Net_Pnd_Per_Ddctn_AmtRedef102.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N", 
            "#PER-DDCTN-AMT-N", FieldType.DECIMAL, 7,2);
        pnd_Ddctn_From_Net_Pnd_Tot_Ddctn = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Tot_Ddctn", "#TOT-DDCTN", FieldType.STRING, 
            9);
        pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef103 = pnd_Ddctn_From_NetRedef98.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef103", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Tot_Ddctn);
        pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N = pnd_Ddctn_From_Net_Pnd_Tot_DdctnRedef103.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N", "#TOT-DDCTN-N", 
            FieldType.DECIMAL, 9,2);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte", "#FINAL-DDCTN-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef104 = pnd_Ddctn_From_NetRedef98.newGroupInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef104", "Redefines", 
            pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm = pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef104.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Mm", 
            "#FINAL-DDCTN-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy = pnd_Ddctn_From_Net_Pnd_Final_Ddctn_DteRedef104.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte_Yy", 
            "#FINAL-DDCTN-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Ddctn_From_Net_Pnd_Filler = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Filler", "#FILLER", FieldType.STRING, 28);
        pnd_Ddctn_From_Net_Pnd_Multiple = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 
            1);
        pnd_Ddctn_From_Net_Pnd_Seq_Nbr = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_Ddctn_From_Net_Pnd_Verify = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_Ddctn_From_Net_Pnd_Filler1 = pnd_Ddctn_From_NetRedef98.newFieldInGroup("pnd_Ddctn_From_Net_Pnd_Filler1", "#FILLER1", FieldType.STRING, 4);

        pnd_His_Ytd_Ddctn = newFieldInRecord("pnd_His_Ytd_Ddctn", "#HIS-YTD-DDCTN", FieldType.STRING, 120);
        pnd_His_Ytd_DdctnRedef105 = newGroupInRecord("pnd_His_Ytd_DdctnRedef105", "Redefines", pnd_His_Ytd_Ddctn);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 
            6);
        pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106 = pnd_His_Ytd_DdctnRedef105.newGroupInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106", "Redefines", pnd_His_Ytd_Ddctn_Pnd_Check_Dte);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm = pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd = pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy = pnd_His_Ytd_Ddctn_Pnd_Check_DteRedef106.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_User_Area = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 
            6);
        pnd_His_Ytd_Ddctn_Pnd_User_Id = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 
            8);
        pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107 = pnd_His_Ytd_DdctnRedef105.newGroupInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107", "Redefines", pnd_His_Ytd_Ddctn_Pnd_Trans_Dte);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Cc = pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Cc", "#TRANS-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Yy = pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Yy", "#TRANS-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Mm = pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Mm", "#TRANS-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Dd = pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef107.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Dd", "#TRANS-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef108 = pnd_His_Ytd_DdctnRedef105.newGroupInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef108", "Redefines", pnd_His_Ytd_Ddctn_Pnd_Trans_Dte);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N = pnd_His_Ytd_Ddctn_Pnd_Trans_DteRedef108.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Dte_N", "#TRANS-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 
            3);
        pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_His_Ytd_Ddctn_Pnd_Record_Status = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Record_Status", "#RECORD-STATUS", FieldType.NUMERIC, 
            2);
        pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", FieldType.STRING, 
            9);
        pnd_His_Ytd_Ddctn_Pnd_Intent_Code = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr", "#SEQUENCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt", "#HIS-DDCTN-AMT", FieldType.STRING, 
            9);
        pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef109 = pnd_His_Ytd_DdctnRedef105.newGroupInGroup("pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef109", "Redefines", 
            pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt);
        pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N = pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_AmtRedef109.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N", "#HIS-DDCTN-AMT-N", 
            FieldType.DECIMAL, 9,2);
        pnd_His_Ytd_Ddctn_Pnd_Filler = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Filler", "#FILLER", FieldType.STRING, 39);
        pnd_His_Ytd_Ddctn_Pnd_Multiple = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Multiple", "#MULTIPLE", FieldType.STRING, 1);
        pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 4);
        pnd_His_Ytd_Ddctn_Pnd_Verify = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Verify", "#VERIFY", FieldType.STRING, 1);
        pnd_His_Ytd_Ddctn_Pnd_Filler1 = pnd_His_Ytd_DdctnRedef105.newFieldInGroup("pnd_His_Ytd_Ddctn_Pnd_Filler1", "#FILLER1", FieldType.STRING, 4);

        pnd_Page_Ctr = newFieldInRecord("pnd_Page_Ctr", "#PAGE-CTR", FieldType.NUMERIC, 4);

        pnd_Page_Ctr2 = newFieldInRecord("pnd_Page_Ctr2", "#PAGE-CTR2", FieldType.NUMERIC, 4);

        pnd_Display_Dte_Trans = newFieldInRecord("pnd_Display_Dte_Trans", "#DISPLAY-DTE-TRANS", FieldType.STRING, 8);

        pnd_Display_Dte_Dob1 = newFieldInRecord("pnd_Display_Dte_Dob1", "#DISPLAY-DTE-DOB1", FieldType.STRING, 8);

        pnd_Display_Dte_Dob2 = newFieldInRecord("pnd_Display_Dte_Dob2", "#DISPLAY-DTE-DOB2", FieldType.STRING, 8);

        pnd_Display_Dte_Dod1 = newFieldInRecord("pnd_Display_Dte_Dod1", "#DISPLAY-DTE-DOD1", FieldType.STRING, 5);

        pnd_Display_Dte_Dod2 = newFieldInRecord("pnd_Display_Dte_Dod2", "#DISPLAY-DTE-DOD2", FieldType.STRING, 5);

        pnd_Display_Dte_Res = newFieldInRecord("pnd_Display_Dte_Res", "#DISPLAY-DTE-RES", FieldType.STRING, 5);

        pnd_Display_Dte_Pend = newFieldInRecord("pnd_Display_Dte_Pend", "#DISPLAY-DTE-PEND", FieldType.STRING, 5);

        pnd_Display_Dte_1st_Pay_Due = newFieldInRecord("pnd_Display_Dte_1st_Pay_Due", "#DISPLAY-DTE-1ST-PAY-DUE", FieldType.STRING, 5);

        pnd_Display_Dte_Iss = newFieldInRecord("pnd_Display_Dte_Iss", "#DISPLAY-DTE-ISS", FieldType.STRING, 5);

        pnd_Display_Dte_Lst_Man_Chk = newFieldInRecord("pnd_Display_Dte_Lst_Man_Chk", "#DISPLAY-DTE-LST-MAN-CHK", FieldType.STRING, 5);

        pnd_Display_Dte_Fin_Per_Pay = newFieldInRecord("pnd_Display_Dte_Fin_Per_Pay", "#DISPLAY-DTE-FIN-PER-PAY", FieldType.STRING, 5);

        pnd_Display_Dte_Fin_Pymt = newFieldInRecord("pnd_Display_Dte_Fin_Pymt", "#DISPLAY-DTE-FIN-PYMT", FieldType.STRING, 8);

        pnd_Display_Dte_Wdrawal = newFieldInRecord("pnd_Display_Dte_Wdrawal", "#DISPLAY-DTE-WDRAWAL", FieldType.STRING, 5);

        pnd_Display_Heading = newFieldInRecord("pnd_Display_Heading", "#DISPLAY-HEADING", FieldType.STRING, 27);

        pnd_Break_User_Area = newFieldInRecord("pnd_Break_User_Area", "#BREAK-USER-AREA", FieldType.STRING, 6);

        pnd_L_Newpage = newFieldInRecord("pnd_L_Newpage", "#L-NEWPAGE", FieldType.BOOLEAN);

        pnd_Ctr = newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 5);

        pnd_Total_Trans = newFieldArrayInRecord("pnd_Total_Trans", "#TOTAL-TRANS", FieldType.NUMERIC, 6, new DbsArrayController(1,17));

        pnd_User_Area_Trans = newFieldArrayInRecord("pnd_User_Area_Trans", "#USER-AREA-TRANS", FieldType.NUMERIC, 6, new DbsArrayController(1,17));

        pnd_Userid_Trans = newFieldArrayInRecord("pnd_Userid_Trans", "#USERID-TRANS", FieldType.NUMERIC, 6, new DbsArrayController(1,17));

        pnd_Total_Per_Pay = newFieldArrayInRecord("pnd_Total_Per_Pay", "#TOTAL-PER-PAY", FieldType.PACKED_DECIMAL, 14,2, new DbsArrayController(1,3));

        pnd_Total_Per_Div = newFieldArrayInRecord("pnd_Total_Per_Div", "#TOTAL-PER-DIV", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,3));

        pnd_Total_Fin_Pay = newFieldArrayInRecord("pnd_Total_Fin_Pay", "#TOTAL-FIN-PAY", FieldType.PACKED_DECIMAL, 14,2, new DbsArrayController(1,3));

        pnd_Total_Cref_Units = newFieldArrayInRecord("pnd_Total_Cref_Units", "#TOTAL-CREF-UNITS", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,
            3));

        pnd_Total_Per_Ded = newFieldArrayInRecord("pnd_Total_Per_Ded", "#TOTAL-PER-DED", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,2));

        pnd_Total_Total_Ded = newFieldArrayInRecord("pnd_Total_Total_Ded", "#TOTAL-TOTAL-DED", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,
            2));

        pnd_Total_Hist_Ded = newFieldArrayInRecord("pnd_Total_Hist_Ded", "#TOTAL-HIST-DED", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,2));

        pnd_Total_Ivc_Amt = newFieldArrayInRecord("pnd_Total_Ivc_Amt", "#TOTAL-IVC-AMT", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,2));

        pnd_Report = newFieldInRecord("pnd_Report", "#REPORT", FieldType.STRING, 1);

        pnd_Save_User_Area = newFieldInRecord("pnd_Save_User_Area", "#SAVE-USER-AREA", FieldType.STRING, 6);

        pnd_Display_User_Area = newFieldInRecord("pnd_Display_User_Area", "#DISPLAY-USER-AREA", FieldType.STRING, 6);

        pnd_Save_Userid = newFieldInRecord("pnd_Save_Userid", "#SAVE-USERID", FieldType.STRING, 8);

        pnd_Display_Userid = newFieldInRecord("pnd_Display_Userid", "#DISPLAY-USERID", FieldType.STRING, 8);

        pnd_Save_Mode = newFieldInRecord("pnd_Save_Mode", "#SAVE-MODE", FieldType.NUMERIC, 3);

        pnd_Display_Mode = newFieldInRecord("pnd_Display_Mode", "#DISPLAY-MODE", FieldType.NUMERIC, 3);

        pnd_New_Trans_L = newFieldInRecord("pnd_New_Trans_L", "#NEW-TRANS-L", FieldType.BOOLEAN);

        pnd_First = newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN);

        pnd_Wrote_Disp_050 = newFieldInRecord("pnd_Wrote_Disp_050", "#WROTE-DISP-050", FieldType.BOOLEAN);

        pnd_Have_33 = newFieldInRecord("pnd_Have_33", "#HAVE-33", FieldType.BOOLEAN);

        pnd_Have_50 = newFieldInRecord("pnd_Have_50", "#HAVE-50", FieldType.BOOLEAN);

        pnd_Indx = newFieldInRecord("pnd_Indx", "#INDX", FieldType.NUMERIC, 4);

        pnd_Indx1 = newFieldInRecord("pnd_Indx1", "#INDX1", FieldType.NUMERIC, 4);

        pnd_Indx2 = newFieldInRecord("pnd_Indx2", "#INDX2", FieldType.NUMERIC, 4);

        pnd_Disp_Dte = newFieldInRecord("pnd_Disp_Dte", "#DISP-DTE", FieldType.STRING, 8);
        pnd_Disp_DteRedef110 = newGroupInRecord("pnd_Disp_DteRedef110", "Redefines", pnd_Disp_Dte);
        pnd_Disp_Dte_Pnd_Disp_Dte_Mm = pnd_Disp_DteRedef110.newFieldInGroup("pnd_Disp_Dte_Pnd_Disp_Dte_Mm", "#DISP-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Disp_Dte_Pnd_Disp_Dte_S1 = pnd_Disp_DteRedef110.newFieldInGroup("pnd_Disp_Dte_Pnd_Disp_Dte_S1", "#DISP-DTE-S1", FieldType.STRING, 1);
        pnd_Disp_Dte_Pnd_Disp_Dte_Dd = pnd_Disp_DteRedef110.newFieldInGroup("pnd_Disp_Dte_Pnd_Disp_Dte_Dd", "#DISP-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Disp_Dte_Pnd_Disp_Dte_S2 = pnd_Disp_DteRedef110.newFieldInGroup("pnd_Disp_Dte_Pnd_Disp_Dte_S2", "#DISP-DTE-S2", FieldType.STRING, 1);
        pnd_Disp_Dte_Pnd_Disp_Dte_Yy = pnd_Disp_DteRedef110.newFieldInGroup("pnd_Disp_Dte_Pnd_Disp_Dte_Yy", "#DISP-DTE-YY", FieldType.NUMERIC, 2);

        pnd_Parm_Check_Dte_Disp = newFieldInRecord("pnd_Parm_Check_Dte_Disp", "#PARM-CHECK-DTE-DISP", FieldType.STRING, 8);

        pnd_Parm_From_Dte_Disp = newFieldInRecord("pnd_Parm_From_Dte_Disp", "#PARM-FROM-DTE-DISP", FieldType.STRING, 8);

        pnd_Parm_To_Dte_Disp = newFieldInRecord("pnd_Parm_To_Dte_Disp", "#PARM-TO-DTE-DISP", FieldType.STRING, 8);

        this.setRecordName("LdaIaal920");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Total_Trans.getValue(1).setInitialValue(0);
        pnd_User_Area_Trans.getValue(1).setInitialValue(0);
        pnd_Userid_Trans.getValue(1).setInitialValue(0);
        pnd_Total_Per_Pay.getValue(1).setInitialValue(0);
        pnd_Total_Per_Div.getValue(1).setInitialValue(0);
        pnd_Total_Fin_Pay.getValue(1).setInitialValue(0);
        pnd_Total_Cref_Units.getValue(1).setInitialValue(0);
        pnd_Total_Per_Ded.getValue(1).setInitialValue(0);
        pnd_Total_Total_Ded.getValue(1).setInitialValue(0);
        pnd_Total_Hist_Ded.getValue(1).setInitialValue(0);
        pnd_Total_Ivc_Amt.getValue(1).setInitialValue(0);
    }

    // Constructor
    public LdaIaal920() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
