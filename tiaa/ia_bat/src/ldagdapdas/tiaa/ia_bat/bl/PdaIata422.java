/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:50 PM
**        * FROM NATURAL PDA     : IATA422
************************************************************
**        * FILE NAME            : PdaIata422.java
**        * CLASS NAME           : PdaIata422
**        * INSTANCE NAME        : PdaIata422
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIata422 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Iata422;
    private DbsField pnd_Iata422_Pnd_Iata422_Rqst_Id;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_Accntng_Dte;
    private DbsField pnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr;
    private DbsField pnd_Iata422_Pnd_Iata422_From_Payee_Cde;
    private DbsField pnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr;
    private DbsField pnd_Iata422_Pnd_Iata422_To_Pye_Cde;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde;
    private DbsField pnd_Iata422_Pnd_Iata422_Switch_Ind;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde;
    private DbsField pnd_Iata422_Pnd_Iata422_Ldgr_Amt;
    private DbsField pnd_Iata422_Pnd_Iata422_Lst_Chnge_Dte;
    private DbsField pnd_Iata422_Pnd_Iata422_Return_Cd;

    public DbsGroup getPnd_Iata422() { return pnd_Iata422; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Rqst_Id() { return pnd_Iata422_Pnd_Iata422_Rqst_Id; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte() { return pnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte() { return pnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_Accntng_Dte() { return pnd_Iata422_Pnd_Iata422_Ldgr_Accntng_Dte; }

    public DbsField getPnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr() { return pnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr; }

    public DbsField getPnd_Iata422_Pnd_Iata422_From_Payee_Cde() { return pnd_Iata422_Pnd_Iata422_From_Payee_Cde; }

    public DbsField getPnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr() { return pnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr; }

    public DbsField getPnd_Iata422_Pnd_Iata422_To_Pye_Cde() { return pnd_Iata422_Pnd_Iata422_To_Pye_Cde; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde() { return pnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Switch_Ind() { return pnd_Iata422_Pnd_Iata422_Switch_Ind; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd() { return pnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ() { return pnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde() { return pnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Ldgr_Amt() { return pnd_Iata422_Pnd_Iata422_Ldgr_Amt; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Lst_Chnge_Dte() { return pnd_Iata422_Pnd_Iata422_Lst_Chnge_Dte; }

    public DbsField getPnd_Iata422_Pnd_Iata422_Return_Cd() { return pnd_Iata422_Pnd_Iata422_Return_Cd; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iata422 = dbsRecord.newGroupInRecord("pnd_Iata422", "#IATA422");
        pnd_Iata422.setParameterOption(ParameterOption.ByReference);
        pnd_Iata422_Pnd_Iata422_Rqst_Id = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Rqst_Id", "#IATA422-RQST-ID", FieldType.STRING, 34);
        pnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_Cycle_Dte", "#IATA422-LDGR-CYCLE-DTE", FieldType.DATE);
        pnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_Effctv_Dte", "#IATA422-LDGR-EFFCTV-DTE", FieldType.DATE);
        pnd_Iata422_Pnd_Iata422_Ldgr_Accntng_Dte = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_Accntng_Dte", "#IATA422-LDGR-ACCNTNG-DTE", 
            FieldType.DATE);
        pnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_From_Ppcn_Nbr", "#IATA422-FROM-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Iata422_Pnd_Iata422_From_Payee_Cde = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_From_Payee_Cde", "#IATA422-FROM-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_To_Ppcn_Nbr", "#IATA422-TO-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Iata422_Pnd_Iata422_To_Pye_Cde = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_To_Pye_Cde", "#IATA422-TO-PYE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_From_Fund_Cde", "#IATA422-LDGR-FROM-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Iata422_Pnd_Iata422_Switch_Ind = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Switch_Ind", "#IATA422-SWITCH-IND", FieldType.STRING, 
            1);
        pnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_Acct_Cd", "#IATA422-LDGR-ACCT-CD", FieldType.STRING, 
            1);
        pnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ = pnd_Iata422.newFieldArrayInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_To_Unit_Typ", "#IATA422-LDGR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde = pnd_Iata422.newFieldArrayInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_To_Fund_Cde", "#IATA422-LDGR-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,20));
        pnd_Iata422_Pnd_Iata422_Ldgr_Amt = pnd_Iata422.newFieldArrayInGroup("pnd_Iata422_Pnd_Iata422_Ldgr_Amt", "#IATA422-LDGR-AMT", FieldType.PACKED_DECIMAL, 
            11,2, new DbsArrayController(1,20));
        pnd_Iata422_Pnd_Iata422_Lst_Chnge_Dte = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Lst_Chnge_Dte", "#IATA422-LST-CHNGE-DTE", FieldType.TIME);
        pnd_Iata422_Pnd_Iata422_Return_Cd = pnd_Iata422.newFieldInGroup("pnd_Iata422_Pnd_Iata422_Return_Cd", "#IATA422-RETURN-CD", FieldType.STRING, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIata422(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

