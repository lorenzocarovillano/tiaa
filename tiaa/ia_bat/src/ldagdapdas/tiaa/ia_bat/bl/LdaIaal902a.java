/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:35 PM
**        * FROM NATURAL LDA     : IAAL902A
************************************************************
**        * FILE NAME            : LdaIaal902a.java
**        * CLASS NAME           : LdaIaal902a
**        * INSTANCE NAME        : LdaIaal902a
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal902a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1;
    private DbsField iaa_Cntrct_Filler1;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Yy;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Mm;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dd;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3;
    private DbsField iaa_Cntrct_Filler2;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Cc;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Yy;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Mm;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsGroup iaa_Cpr_Trans_Cntrct_Pend_DteRedef7;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte_Cc;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte_Yy;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte_Mm;
    private DbsGroup pnd_Misc_Non_Tax_Trans;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Record_Status;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Filler1;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef9;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef11;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Filler2;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef12;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef14;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Filler3;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A;
    private DbsGroup pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef15;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy;
    private DbsField pnd_Misc_Non_Tax_Trans_Pnd_User_Area;
    private DbsField pnd_Cntrct_Bfre_Key;
    private DbsGroup pnd_Cntrct_Bfre_KeyRedef16;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Cntrct_Aftr_Key;
    private DbsGroup pnd_Cntrct_Aftr_KeyRedef17;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Cntrct_Payee_Key;
    private DbsGroup pnd_Cntrct_Payee_KeyRedef18;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Bfre_Key;
    private DbsGroup pnd_Cpr_Bfre_KeyRedef19;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Trans_Dte;
    private DbsField pnd_Iaa_Cntrct_Key;
    private DbsGroup pnd_Iaa_Cntrct_KeyRedef20;
    private DbsField pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key;
    private DbsGroup pnd_Iaa_Cntrct_Prtcpnt_KeyRedef21;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cntrct_Trans_Key;
    private DbsGroup pnd_Iaa_Cntrct_Trans_KeyRedef22;
    private DbsField pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id;
    private DbsField pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte;
    private DbsField pnd_Iaa_Cpr_Trans_Key;
    private DbsGroup pnd_Iaa_Cpr_Trans_KeyRedef23;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Image_Id;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte;
    private DbsField pnd_Bnfcry_Dod;
    private DbsGroup pnd_Bnfcry_DodRedef24;
    private DbsField pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Cc;
    private DbsField pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Yy;
    private DbsField pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Mm;
    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_No_Cntrct_Rec;

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Acctng_Cde() { return iaa_Cntrct_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte() { return iaa_Cntrct_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Type_Cde() { return iaa_Cntrct_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1() { return iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1; }

    public DbsField getIaa_Cntrct_Filler1() { return iaa_Cntrct_Filler1; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Yy() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Mm() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dd() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2() { return iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Cc() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Yy() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Mm() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3; }

    public DbsField getIaa_Cntrct_Filler2() { return iaa_Cntrct_Filler2; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Lst_Trans_Dte() { return iaa_Cntrct_Lst_Trans_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct_Prtcpnt_Role() { return vw_iaa_Cntrct_Prtcpnt_Role; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte() { return iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind() { return iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte() { return iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Lst_Trans_Dte() { return iaa_Cntrct_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Invrse_Trans_Dte() { return iaa_Cntrct_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Cc() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Yy() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Mm() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsGroup getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Cc() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Cc; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Yy() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Yy; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Mm() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Mm; }

    public DbsField getIaa_Cntrct_Trans_Aftr_Imge_Id() { return iaa_Cntrct_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Bfre_Imge_Id() { return iaa_Cntrct_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Trans_Dte() { return iaa_Cpr_Trans_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Invrse_Trans_Dte() { return iaa_Cpr_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Lst_Trans_Dte() { return iaa_Cpr_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Cde() { return iaa_Cpr_Trans_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Hold_Cde() { return iaa_Cpr_Trans_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cpr_Trans_Bfre_Imge_Id() { return iaa_Cpr_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Aftr_Imge_Id() { return iaa_Cpr_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte() { return iaa_Cpr_Trans_Cntrct_Pend_Dte; }

    public DbsGroup getIaa_Cpr_Trans_Cntrct_Pend_DteRedef7() { return iaa_Cpr_Trans_Cntrct_Pend_DteRedef7; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte_Cc() { return iaa_Cpr_Trans_Cntrct_Pend_Dte_Cc; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte_Yy() { return iaa_Cpr_Trans_Cntrct_Pend_Dte_Yy; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte_Mm() { return iaa_Cpr_Trans_Cntrct_Pend_Dte_Mm; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans() { return pnd_Misc_Non_Tax_Trans; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Record_Status() { return pnd_Misc_Non_Tax_Trans_Pnd_Record_Status; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr() { return pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code() { return pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Filler1() { return pnd_Misc_Non_Tax_Trans_Pnd_Filler1; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde() { return pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde() { return pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res() { return pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef9() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef9; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef11() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef11; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Filler2() { return pnd_Misc_Non_Tax_Trans_Pnd_Filler2; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef12() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef12; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef14() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef14; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Filler3() { return pnd_Misc_Non_Tax_Trans_Pnd_Filler3; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A; }

    public DbsGroup getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef15() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef15; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy() { return pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy; }

    public DbsField getPnd_Misc_Non_Tax_Trans_Pnd_User_Area() { return pnd_Misc_Non_Tax_Trans_Pnd_User_Area; }

    public DbsField getPnd_Cntrct_Bfre_Key() { return pnd_Cntrct_Bfre_Key; }

    public DbsGroup getPnd_Cntrct_Bfre_KeyRedef16() { return pnd_Cntrct_Bfre_KeyRedef16; }

    public DbsField getPnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Bfre_Key_Pnd_Trans_Dte() { return pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Cntrct_Aftr_Key() { return pnd_Cntrct_Aftr_Key; }

    public DbsGroup getPnd_Cntrct_Aftr_KeyRedef17() { return pnd_Cntrct_Aftr_KeyRedef17; }

    public DbsField getPnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id() { return pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte() { return pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte; }

    public DbsField getPnd_Cntrct_Payee_Key() { return pnd_Cntrct_Payee_Key; }

    public DbsGroup getPnd_Cntrct_Payee_KeyRedef18() { return pnd_Cntrct_Payee_KeyRedef18; }

    public DbsField getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Cpr_Bfre_Key() { return pnd_Cpr_Bfre_Key; }

    public DbsGroup getPnd_Cpr_Bfre_KeyRedef19() { return pnd_Cpr_Bfre_KeyRedef19; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id() { return pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Cpr_Bfre_Key_Pnd_Trans_Dte() { return pnd_Cpr_Bfre_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Iaa_Cntrct_Key() { return pnd_Iaa_Cntrct_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_KeyRedef20() { return pnd_Iaa_Cntrct_KeyRedef20; }

    public DbsField getPnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte() { return pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key() { return pnd_Iaa_Cntrct_Prtcpnt_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Prtcpnt_KeyRedef21() { return pnd_Iaa_Cntrct_Prtcpnt_KeyRedef21; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key() { return pnd_Iaa_Cntrct_Trans_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Trans_KeyRedef22() { return pnd_Iaa_Cntrct_Trans_KeyRedef22; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id() { return pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte() { return pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key() { return pnd_Iaa_Cpr_Trans_Key; }

    public DbsGroup getPnd_Iaa_Cpr_Trans_KeyRedef23() { return pnd_Iaa_Cpr_Trans_KeyRedef23; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Image_Id() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Image_Id; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte; }

    public DbsField getPnd_Bnfcry_Dod() { return pnd_Bnfcry_Dod; }

    public DbsGroup getPnd_Bnfcry_DodRedef24() { return pnd_Bnfcry_DodRedef24; }

    public DbsField getPnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Cc() { return pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Cc; }

    public DbsField getPnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Yy() { return pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Yy; }

    public DbsField getPnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Mm() { return pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Mm; }

    public DbsGroup getPnd_Logical_Variables() { return pnd_Logical_Variables; }

    public DbsField getPnd_Logical_Variables_Pnd_No_Cntrct_Rec() { return pnd_Logical_Variables_Pnd_No_Cntrct_Rec; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1", "Redefines", 
            iaa_Cntrct_Cntrct_First_Annt_Dob_Dte);
        iaa_Cntrct_Filler1 = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Filler1", "FILLER1", FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Yy = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Yy", "CNTRCT-FIRST-ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Mm = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Mm", "CNTRCT-FIRST-ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dd = iaa_Cntrct_Cntrct_First_Annt_Dob_DteRedef1.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dd", "CNTRCT-FIRST-ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2", "Redefines", 
            iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Cc = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Cc", "CNTRCT-FIRST-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Yy = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Yy", "CNTRCT-FIRST-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_First_Annt_Dod_Mm = iaa_Cntrct_Cntrct_First_Annt_Dod_DteRedef2.newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Mm", "CNTRCT-FIRST-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3", "Redefines", 
            iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte);
        iaa_Cntrct_Filler2 = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Filler2", "FILLER2", FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Yy", "CNTRCT-SCND-ANNT-DOB-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Mm", "CNTRCT-SCND-ANNT-DOB-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd = iaa_Cntrct_Cntrct_Scnd_Annt_Dob_DteRedef3.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dd", "CNTRCT-SCND-ANNT-DOB-DD", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4", "Redefines", 
            iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Cc", "CNTRCT-SCND-ANNT-DOD-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Yy", "CNTRCT-SCND-ANNT-DOD-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm = iaa_Cntrct_Cntrct_Scnd_Annt_Dod_DteRedef4.newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Mm", "CNTRCT-SCND-ANNT-DOD-MM", 
            FieldType.NUMERIC, 2);
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5 = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5", 
            "Redefines", iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte);
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Cc = iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5.newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Cc", 
            "CNTRCT-FIRST-ANNT-DOD-CC", FieldType.NUMERIC, 2);
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Yy = iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5.newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Yy", 
            "CNTRCT-FIRST-ANNT-DOD-YY", FieldType.NUMERIC, 2);
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Mm = iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_DteRedef5.newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Mm", 
            "CNTRCT-FIRST-ANNT-DOD-MM", FieldType.NUMERIC, 2);
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6 = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6", 
            "Redefines", iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte);
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Cc = iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6.newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Cc", 
            "CNTRCT-SCND-ANNT-DOD-CC", FieldType.NUMERIC, 2);
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Yy = iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6.newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Yy", 
            "CNTRCT-SCND-ANNT-DOD-YY", FieldType.NUMERIC, 2);
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Mm = iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_DteRedef6.newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Mm", 
            "CNTRCT-SCND-ANNT-DOD-MM", FieldType.NUMERIC, 2);
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_DteRedef7 = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Pend_DteRedef7", "Redefines", iaa_Cpr_Trans_Cntrct_Pend_Dte);
        iaa_Cpr_Trans_Cntrct_Pend_Dte_Cc = iaa_Cpr_Trans_Cntrct_Pend_DteRedef7.newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte_Cc", "CNTRCT-PEND-DTE-CC", 
            FieldType.NUMERIC, 2);
        iaa_Cpr_Trans_Cntrct_Pend_Dte_Yy = iaa_Cpr_Trans_Cntrct_Pend_DteRedef7.newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte_Yy", "CNTRCT-PEND-DTE-YY", 
            FieldType.NUMERIC, 2);
        iaa_Cpr_Trans_Cntrct_Pend_Dte_Mm = iaa_Cpr_Trans_Cntrct_Pend_DteRedef7.newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte_Mm", "CNTRCT-PEND-DTE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Misc_Non_Tax_Trans = newGroupInRecord("pnd_Misc_Non_Tax_Trans", "#MISC-NON-TAX-TRANS");
        pnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr", "#BATCH-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 
            6);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8", "Redefines", 
            pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm = pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Mm", 
            "#CHECK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd = pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Dd", 
            "#CHECK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy = pnd_Misc_Non_Tax_Trans_Pnd_Check_DteRedef8.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte_Yy", 
            "#CHECK-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Misc_Non_Tax_Trans_Pnd_Record_Status = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Record_Status", "#RECORD-STATUS", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr", "#CROSS-REF-NBR", 
            FieldType.STRING, 9);
        pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr", "#TRANS-NBR", FieldType.NUMERIC, 
            3);
        pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 
            1);
        pnd_Misc_Non_Tax_Trans_Pnd_Filler1 = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            6);
        pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde", "#HOLD-CHECK-RSN-CDE", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde", "#SUS-PYMNT-RSN-CDE", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res", "#STATE-CNTRY-RES", 
            FieldType.STRING, 3);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A", "#1ST-ANNT-SEX-A", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef9 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef9", "Redefines", 
            pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_ARedef9.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex", 
            "#1ST-ANNT-SEX", FieldType.NUMERIC, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A", "#1ST-ANNT-DOB-A", 
            FieldType.STRING, 6);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_A);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm", 
            "#1ST-ANNT-DOB-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd", 
            "#1ST-ANNT-DOB-DD", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_ARedef10.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy", 
            "#1ST-ANNT-DOB-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A", "#1ST-ANNT-DOD-A", 
            FieldType.STRING, 4);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef11 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef11", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_A);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm", 
            "#1ST-ANNT-DOD-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy = pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_ARedef11.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy", 
            "#1ST-ANNT-DOD-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Filler2 = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A", "#2ND-ANNT-SEX-A", 
            FieldType.STRING, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef12 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef12", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_ARedef12.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex", 
            "#2ND-ANNT-SEX", FieldType.NUMERIC, 1);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A", "#2ND-ANNT-DOB-A", 
            FieldType.STRING, 6);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_A);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm", 
            "#2ND-ANNT-DOB-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd", 
            "#2ND-ANNT-DOB-DD", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_ARedef13.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy", 
            "#2ND-ANNT-DOB-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A", "#2ND-ANNT-DOD-A", 
            FieldType.STRING, 4);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef14 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef14", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_A);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef14.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm", 
            "#2ND-ANNT-DOD-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy = pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_ARedef14.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy", 
            "#2ND-ANNT-DOD-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Filler3 = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Filler3", "#FILLER3", FieldType.STRING, 
            8);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A", "#EFF-DTE-CHNG-RES-A", 
            FieldType.STRING, 4);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef15 = pnd_Misc_Non_Tax_Trans.newGroupInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef15", 
            "Redefines", pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_A);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm = pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef15.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm", 
            "#EFF-DTE-CHNG-RES-MM", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy = pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_ARedef15.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy", 
            "#EFF-DTE-CHNG-RES-YY", FieldType.NUMERIC, 2);
        pnd_Misc_Non_Tax_Trans_Pnd_User_Area = pnd_Misc_Non_Tax_Trans.newFieldInGroup("pnd_Misc_Non_Tax_Trans_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 
            6);

        pnd_Cntrct_Bfre_Key = newFieldInRecord("pnd_Cntrct_Bfre_Key", "#CNTRCT-BFRE-KEY", FieldType.STRING, 18);
        pnd_Cntrct_Bfre_KeyRedef16 = newGroupInRecord("pnd_Cntrct_Bfre_KeyRedef16", "Redefines", pnd_Cntrct_Bfre_Key);
        pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cntrct_Bfre_KeyRedef16.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Bfre_KeyRedef16.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte = pnd_Cntrct_Bfre_KeyRedef16.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Cntrct_Aftr_Key = newFieldInRecord("pnd_Cntrct_Aftr_Key", "#CNTRCT-AFTR-KEY", FieldType.STRING, 23);
        pnd_Cntrct_Aftr_KeyRedef17 = newGroupInRecord("pnd_Cntrct_Aftr_KeyRedef17", "Redefines", pnd_Cntrct_Aftr_Key);
        pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cntrct_Aftr_KeyRedef17.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Aftr_KeyRedef17.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Cntrct_Aftr_KeyRedef17.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        pnd_Cntrct_Payee_Key = newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);
        pnd_Cntrct_Payee_KeyRedef18 = newGroupInRecord("pnd_Cntrct_Payee_KeyRedef18", "Redefines", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_KeyRedef18.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_KeyRedef18.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Cpr_Bfre_Key = newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);
        pnd_Cpr_Bfre_KeyRedef19 = newGroupInRecord("pnd_Cpr_Bfre_KeyRedef19", "Redefines", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cpr_Bfre_KeyRedef19.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Bfre_KeyRedef19.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Bfre_KeyRedef19.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_Trans_Dte = pnd_Cpr_Bfre_KeyRedef19.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Iaa_Cntrct_Key = newFieldInRecord("pnd_Iaa_Cntrct_Key", "#IAA-CNTRCT-KEY", FieldType.STRING, 17);
        pnd_Iaa_Cntrct_KeyRedef20 = newGroupInRecord("pnd_Iaa_Cntrct_KeyRedef20", "Redefines", pnd_Iaa_Cntrct_Key);
        pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_KeyRedef20.newFieldInGroup("pnd_Iaa_Cntrct_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte = pnd_Iaa_Cntrct_KeyRedef20.newFieldInGroup("pnd_Iaa_Cntrct_Key_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.PACKED_DECIMAL, 
            7);

        pnd_Iaa_Cntrct_Prtcpnt_Key = newFieldInRecord("pnd_Iaa_Cntrct_Prtcpnt_Key", "#IAA-CNTRCT-PRTCPNT-KEY", FieldType.STRING, 12);
        pnd_Iaa_Cntrct_Prtcpnt_KeyRedef21 = newGroupInRecord("pnd_Iaa_Cntrct_Prtcpnt_KeyRedef21", "Redefines", pnd_Iaa_Cntrct_Prtcpnt_Key);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef21.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef21.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Iaa_Cntrct_Trans_Key = newFieldInRecord("pnd_Iaa_Cntrct_Trans_Key", "#IAA-CNTRCT-TRANS-KEY", FieldType.STRING, 18);
        pnd_Iaa_Cntrct_Trans_KeyRedef22 = newGroupInRecord("pnd_Iaa_Cntrct_Trans_KeyRedef22", "Redefines", pnd_Iaa_Cntrct_Trans_Key);
        pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id = pnd_Iaa_Cntrct_Trans_KeyRedef22.newFieldInGroup("pnd_Iaa_Cntrct_Trans_Key_Pnd_Bfre_Imgr_Id", "#BFRE-IMGR-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Iaa_Cntrct_Trans_KeyRedef22.newFieldInGroup("pnd_Iaa_Cntrct_Trans_Key_Pnd_Cntrct_Ppcn_Nbr", 
            "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte = pnd_Iaa_Cntrct_Trans_KeyRedef22.newFieldInGroup("pnd_Iaa_Cntrct_Trans_Key_Pnd_Trans_Dte", "#TRANS-DTE", 
            FieldType.TIME);

        pnd_Iaa_Cpr_Trans_Key = newFieldInRecord("pnd_Iaa_Cpr_Trans_Key", "#IAA-CPR-TRANS-KEY", FieldType.STRING, 20);
        pnd_Iaa_Cpr_Trans_KeyRedef23 = newGroupInRecord("pnd_Iaa_Cpr_Trans_KeyRedef23", "Redefines", pnd_Iaa_Cpr_Trans_Key);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Image_Id = pnd_Iaa_Cpr_Trans_KeyRedef23.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Image_Id", "#BFRE-IMAGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Trans_KeyRedef23.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Trans_KeyRedef23.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte = pnd_Iaa_Cpr_Trans_KeyRedef23.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Bnfcry_Dod = newFieldInRecord("pnd_Bnfcry_Dod", "#BNFCRY-DOD", FieldType.NUMERIC, 6);
        pnd_Bnfcry_DodRedef24 = newGroupInRecord("pnd_Bnfcry_DodRedef24", "Redefines", pnd_Bnfcry_Dod);
        pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Cc = pnd_Bnfcry_DodRedef24.newFieldInGroup("pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Cc", "#BNFCRY-DOD-CC", FieldType.NUMERIC, 
            2);
        pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Yy = pnd_Bnfcry_DodRedef24.newFieldInGroup("pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Yy", "#BNFCRY-DOD-YY", FieldType.NUMERIC, 
            2);
        pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Mm = pnd_Bnfcry_DodRedef24.newFieldInGroup("pnd_Bnfcry_Dod_Pnd_Bnfcry_Dod_Mm", "#BNFCRY-DOD-MM", FieldType.NUMERIC, 
            2);

        pnd_Logical_Variables = newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_No_Cntrct_Rec = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_No_Cntrct_Rec", "#NO-CNTRCT-REC", FieldType.BOOLEAN);
        vw_iaa_Cntrct_Prtcpnt_Role.setUniquePeList();

        this.setRecordName("LdaIaal902a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();
        pnd_Misc_Non_Tax_Trans_Pnd_Batch_Nbr.setInitialValue(0);
        pnd_Misc_Non_Tax_Trans_Pnd_Check_Dte.setInitialValue(0);
        pnd_Misc_Non_Tax_Trans_Pnd_Record_Status.setInitialValue(0);
    }

    // Constructor
    public LdaIaal902a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
