/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:09 PM
**        * FROM NATURAL LDA     : IAAA090A
************************************************************
**        * FILE NAME            : LdaIaaa090a.java
**        * CLASS NAME           : LdaIaaa090a
**        * INSTANCE NAME        : LdaIaaa090a
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaaa090a extends DbsRecord
{
    // Properties
    private DbsGroup iaaa090;
    private DbsField iaaa090_Cntrl_Cde;
    private DbsField iaaa090_Cntrl_Tiaa_Payees;
    private DbsField iaaa090_Cntrl_Per_Pay_Amt;
    private DbsField iaaa090_Cntrl_Per_Div_Amt;
    private DbsField iaaa090_Cntrl_Final_Pay_Amt;
    private DbsField iaaa090_Cntrl_Final_Div_Amt;
    private DbsGroup iaaa090_Cntrl_Fund_Cnts;
    private DbsField iaaa090_Cntrl_Fund_Cde;
    private DbsField iaaa090_Cntrl_Fund_Payees;
    private DbsField iaaa090_Cntrl_Units;
    private DbsField iaaa090_Cntrl_Amt;
    private DbsField iaaa090_Cntrl_Ddctn_Cnt;
    private DbsField iaaa090_Cntrl_Ddctn_Amt;
    private DbsField iaaa090_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaaa090_Cntrl_Actve_Cref_Pys;
    private DbsField iaaa090_Cntrl_Inactve_Payees;

    public DbsGroup getIaaa090() { return iaaa090; }

    public DbsField getIaaa090_Cntrl_Cde() { return iaaa090_Cntrl_Cde; }

    public DbsField getIaaa090_Cntrl_Tiaa_Payees() { return iaaa090_Cntrl_Tiaa_Payees; }

    public DbsField getIaaa090_Cntrl_Per_Pay_Amt() { return iaaa090_Cntrl_Per_Pay_Amt; }

    public DbsField getIaaa090_Cntrl_Per_Div_Amt() { return iaaa090_Cntrl_Per_Div_Amt; }

    public DbsField getIaaa090_Cntrl_Final_Pay_Amt() { return iaaa090_Cntrl_Final_Pay_Amt; }

    public DbsField getIaaa090_Cntrl_Final_Div_Amt() { return iaaa090_Cntrl_Final_Div_Amt; }

    public DbsGroup getIaaa090_Cntrl_Fund_Cnts() { return iaaa090_Cntrl_Fund_Cnts; }

    public DbsField getIaaa090_Cntrl_Fund_Cde() { return iaaa090_Cntrl_Fund_Cde; }

    public DbsField getIaaa090_Cntrl_Fund_Payees() { return iaaa090_Cntrl_Fund_Payees; }

    public DbsField getIaaa090_Cntrl_Units() { return iaaa090_Cntrl_Units; }

    public DbsField getIaaa090_Cntrl_Amt() { return iaaa090_Cntrl_Amt; }

    public DbsField getIaaa090_Cntrl_Ddctn_Cnt() { return iaaa090_Cntrl_Ddctn_Cnt; }

    public DbsField getIaaa090_Cntrl_Ddctn_Amt() { return iaaa090_Cntrl_Ddctn_Amt; }

    public DbsField getIaaa090_Cntrl_Actve_Tiaa_Pys() { return iaaa090_Cntrl_Actve_Tiaa_Pys; }

    public DbsField getIaaa090_Cntrl_Actve_Cref_Pys() { return iaaa090_Cntrl_Actve_Cref_Pys; }

    public DbsField getIaaa090_Cntrl_Inactve_Payees() { return iaaa090_Cntrl_Inactve_Payees; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaaa090 = newGroupInRecord("iaaa090", "IAAA090");
        iaaa090_Cntrl_Cde = iaaa090.newFieldInGroup("iaaa090_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2);
        iaaa090_Cntrl_Tiaa_Payees = iaaa090.newFieldInGroup("iaaa090_Cntrl_Tiaa_Payees", "CNTRL-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        iaaa090_Cntrl_Per_Pay_Amt = iaaa090.newFieldInGroup("iaaa090_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090_Cntrl_Per_Div_Amt = iaaa090.newFieldInGroup("iaaa090_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090_Cntrl_Final_Pay_Amt = iaaa090.newFieldInGroup("iaaa090_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090_Cntrl_Final_Div_Amt = iaaa090.newFieldInGroup("iaaa090_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090_Cntrl_Fund_Cnts = iaaa090.newGroupArrayInGroup("iaaa090_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", new DbsArrayController(1,80));
        iaaa090_Cntrl_Fund_Cde = iaaa090_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 3);
        iaaa090_Cntrl_Fund_Payees = iaaa090_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", FieldType.PACKED_DECIMAL, 
            9);
        iaaa090_Cntrl_Units = iaaa090_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 13,3);
        iaaa090_Cntrl_Amt = iaaa090_Cntrl_Fund_Cnts.newFieldInGroup("iaaa090_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090_Cntrl_Ddctn_Cnt = iaaa090.newFieldInGroup("iaaa090_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 9);
        iaaa090_Cntrl_Ddctn_Amt = iaaa090.newFieldInGroup("iaaa090_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 13,2);
        iaaa090_Cntrl_Actve_Tiaa_Pys = iaaa090.newFieldInGroup("iaaa090_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", FieldType.PACKED_DECIMAL, 9);
        iaaa090_Cntrl_Actve_Cref_Pys = iaaa090.newFieldInGroup("iaaa090_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", FieldType.PACKED_DECIMAL, 9);
        iaaa090_Cntrl_Inactve_Payees = iaaa090.newFieldInGroup("iaaa090_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", FieldType.PACKED_DECIMAL, 9);

        this.setRecordName("LdaIaaa090a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaaa090a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
