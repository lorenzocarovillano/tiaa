/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:37 PM
**        * FROM NATURAL LDA     : IATL406
************************************************************
**        * FILE NAME            : LdaIatl406.java
**        * CLASS NAME           : LdaIatl406
**        * INSTANCE NAME        : LdaIatl406
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIatl406 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cis_Prtcpnt_File;
    private DbsField cis_Prtcpnt_File_Cis_Pin_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Id_Key;
    private DbsField cis_Prtcpnt_File_Cis_Tiaa_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Tiaa_Doi;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Doi;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Ssn;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Prfx;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Sffx;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Dob;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Sex_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Frst_Annt_Calc_Method;
    private DbsField cis_Prtcpnt_File_Cis_Opn_Clsd_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Status_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Cntrct_Type;
    private DbsField cis_Prtcpnt_File_Cis_Cntrct_Apprvl_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Id;
    private DbsField cis_Prtcpnt_File_Cis_Hold_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cntrct_Print_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Extract_Date;
    private DbsGroup cis_Prtcpnt_File_Cis_Mit_Request;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Work_Process_Id;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Step_Id;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Status_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Appl_Rcvd_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Appl_Rcvd_User_Id;
    private DbsField cis_Prtcpnt_File_Cis_Appl_Entry_User_Id;
    private DbsField cis_Prtcpnt_File_Cis_Annty_Option;
    private DbsField cis_Prtcpnt_File_Cis_Pymnt_Mode;
    private DbsField cis_Prtcpnt_File_Cis_Annty_Start_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Annty_End_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Grnted_Period_Yrs;
    private DbsField cis_Prtcpnt_File_Cis_Grnted_Period_Dys;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Prfx;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Sffx;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Ssn;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Dob;
    private DbsField cis_Prtcpnt_File_Cis_Scnd_Annt_Sex_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Pin_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Prfx;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Frst_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Mid_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Lst_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Sffx;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Birth_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Death_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Ssn;
    private DbsField cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Rltn_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Grnted_Grd_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Grnted_Std_Amt;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info;
    private DbsGroup cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info;
    private DbsField cis_Prtcpnt_File_Cis_Comut_Grnted_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Comut_Int_Rate;
    private DbsField cis_Prtcpnt_File_Cis_Comut_Pymt_Method;
    private DbsField cis_Prtcpnt_File_Cis_Grnted_Int_Rate;
    private DbsField cis_Prtcpnt_File_Cis_Surv_Redct_Amt;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Da_Tiaa_Cntrcts_Rqst;
    private DbsGroup cis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_File_Cis_Da_Tiaa_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt;
    private DbsGroup cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Flag;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Acct_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Pymnt_Mode;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Units;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_From_Company;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_To_Company;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Tiaa_Rea_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Rea_Units;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Da_Cref_Cntrcts_Rqst;
    private DbsGroup cis_Prtcpnt_File_Cis_Da_Cref_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_File_Cis_Da_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Cref_Annty_Pymnt;
    private DbsGroup cis_Prtcpnt_File_Cis_Cref_Annty_Pymnt;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Acct_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Annty_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Rea_Annty_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_Cis_Rea_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_Cis_Rea_Surv_Nbr_Units;
    private DbsField cis_Prtcpnt_File_Cis_Surrender_Chg;
    private DbsField cis_Prtcpnt_File_Cis_Contingencies_Chg;
    private DbsField cis_Prtcpnt_File_Cis_Mdo_Contract_Cash_Status;
    private DbsField cis_Prtcpnt_File_Cis_Mdo_Contract_Type;
    private DbsField cis_Prtcpnt_File_Cis_Mdo_Traditional_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Mdo_Tiaa_Int_Pymnt_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Mdo_Cref_Int_Pymnt_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Mdo_Tiaa_Excluded_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Mdo_Cref_Excluded_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Tiaa_Personal_Annuity;
    private DbsField cis_Prtcpnt_File_Cis_Orig_Issue_State;
    private DbsField cis_Prtcpnt_File_Cis_Issue_State_Name;
    private DbsField cis_Prtcpnt_File_Cis_Issue_State_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Ppg_Code;
    private DbsField cis_Prtcpnt_File_Cis_Region_Code;
    private DbsField cis_Prtcpnt_File_Cis_Ownership_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Tax_Witholding;
    private DbsField cis_Prtcpnt_File_Cis_Bill_Code;
    private DbsField cis_Prtcpnt_File_Cis_Lob;
    private DbsField cis_Prtcpnt_File_Cis_Lob_Type;
    private DbsField cis_Prtcpnt_File_Cis_Mail_Instructions;
    private DbsField cis_Prtcpnt_File_Cis_Premium_Paying;
    private DbsField cis_Prtcpnt_File_Cis_Cashability_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Transferability_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Addr_Syn_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Addr_Process_Env;
    private DbsGroup cis_Prtcpnt_File_Cis_Address_Info;
    private DbsField cis_Prtcpnt_File_Cis_Address_Chg_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Address_Dest_Name;
    private DbsGroup cis_Prtcpnt_File_Cis_Address_TxtMuGroup;
    private DbsField cis_Prtcpnt_File_Cis_Address_Txt;
    private DbsField cis_Prtcpnt_File_Cis_Zip_Code;
    private DbsField cis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField cis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField cis_Prtcpnt_File_Cis_Stndrd_Trn_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Finalist_Reason_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Addr_Usage_Code;
    private DbsField cis_Prtcpnt_File_Cis_Checking_Saving_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Addr_Stndrd_Code;
    private DbsField cis_Prtcpnt_File_Cis_Stndrd_Overide;
    private DbsField cis_Prtcpnt_File_Cis_Postal_Data_Fields;
    private DbsField cis_Prtcpnt_File_Cis_Geographic_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Pct;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Amt;
    private DbsGroup cis_Prtcpnt_File_Cis_Rtb_Dest_Data;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Dest_Pct;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Dest_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Dest_Nme;
    private DbsGroup cis_Prtcpnt_File_Cis_Rtb_Dest_AddrMuGroup;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Dest_Addr;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Bank_Accnt_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Trnst_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Dest_Zip;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Dest_Type_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_File_Cis_Corp_Sync_Ind;
    private DbsGroup cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Function;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Process_Env;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Error_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Error_Msg;
    private DbsGroup cis_Prtcpnt_File_Cis_Non_Premium_Dta;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Sync_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Process_Env;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Effective_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Product_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Contract_Retr_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Rate_Accum_Adj_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Currency;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Tiaa_Age_First_Pymnt;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Cref_Age_First_Pymnt;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Deletion_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Vesting_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate38_Accum_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate42_Accum_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Error_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Nonp_Error_Msg;
    private DbsGroup cis_Prtcpnt_File_Cis_Allocation_Dta;
    private DbsField cis_Prtcpnt_File_Cis_Alloc_Sync_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Alloc_Process_Env;
    private DbsField cis_Prtcpnt_File_Cis_Alloc_Effective_Dte;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Alloc_Percentage;
    private DbsGroup cis_Prtcpnt_File_Cis_Alloc_PercentageMuGroup;
    private DbsField cis_Prtcpnt_File_Cis_Alloc_Percentage;
    private DbsField cis_Prtcpnt_File_Cis_Alloc_Error_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Alloc_Error_Msg;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Sync_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Process_Env;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Rcd_Typ_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Neg_Election_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Occupnt_Nme;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Xref_Pin;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Active_Vip_Cnt;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Actve_Vip_Grp;
    private DbsGroup cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Grp;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Area_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Mail_Table_Cnt;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Mail_Grp;
    private DbsGroup cis_Prtcpnt_File_Cis_Cor_Ph_Mail_Grp;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Phmail_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Phmail_Area_Of_Origin;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Media_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Cntnts_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Cntrct_Table_Cnt;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Error_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Cor_Error_Msg;
    private DbsField cis_Prtcpnt_File_Cis_Pull_Code;
    private DbsField cis_Prtcpnt_File_Cis_Annual_Required_Dist_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Required_Begin_Date;
    private DbsField cis_Prtcpnt_File_Cis_Rea_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Rea_Doi;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Issue_State;
    private DbsField cis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type;
    private DbsField cis_Prtcpnt_File_Cis_Rea_Cntrct_Type;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Cntrct_Type;
    private DbsField cis_Prtcpnt_File_Cis_Financial_Data_1;
    private DbsField cis_Prtcpnt_File_Cis_Financial_Data_2;
    private DbsField cis_Prtcpnt_File_Cis_Financial_Data_3;
    private DbsField cis_Prtcpnt_File_Cis_Financial_Data_4;
    private DbsField cis_Prtcpnt_File_Cis_Financial_Data_5;
    private DbsField cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info_2;
    private DbsGroup cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2;
    private DbsField cis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2;
    private DbsField cis_Prtcpnt_File_Cis_Comut_Int_Rate_2;
    private DbsField cis_Prtcpnt_File_Cis_Comut_Pymt_Method_2;
    private DbsField cis_Prtcpnt_File_Cis_Comut_Mortality_Basis;
    private DbsField cis_Prtcpnt_File_Cis_Grnted_Period_Mnts;
    private DbsGroup cis_Prtcpnt_File_Cis_Sg_Fund_Identifier;
    private DbsField cis_Prtcpnt_File_Cis_Sg_Fund_Ticker;
    private DbsField cis_Prtcpnt_File_Cis_Sg_Fund_Allocation;
    private DbsField cis_Prtcpnt_File_Cis_Sg_Fund_Amt;
    private DbsField cis_Prtcpnt_File_Cis_Sg_Text_Udf_1;
    private DbsField cis_Prtcpnt_File_Cis_Sg_Text_Udf_2;
    private DbsField cis_Prtcpnt_File_Cis_Sg_Text_Udf_3;
    private DbsField cis_Prtcpnt_File_Cis_Tacc_Annty_Amt;
    private DbsGroup cis_Prtcpnt_File_Cis_Tacc_Fund_Info;
    private DbsField cis_Prtcpnt_File_Cis_Tacc_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Tacc_Account;
    private DbsField cis_Prtcpnt_File_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_Cis_Tacc_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_Cis_Institution_Name;
    private DbsField cis_Prtcpnt_File_Cis_Four_Fifty_Seven_Ind;
    private DbsGroup cis_Prtcpnt_File_Cis_Trnsf_Info;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Ticker;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Reval_Type;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Cref_Units;
    private DbsField cis_Prtcpnt_File_Cis_Trnsf_Rece_Company;

    public DataAccessProgramView getVw_cis_Prtcpnt_File() { return vw_cis_Prtcpnt_File; }

    public DbsField getCis_Prtcpnt_File_Cis_Pin_Nbr() { return cis_Prtcpnt_File_Cis_Pin_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Id_Key() { return cis_Prtcpnt_File_Cis_Rqst_Id_Key; }

    public DbsField getCis_Prtcpnt_File_Cis_Tiaa_Nbr() { return cis_Prtcpnt_File_Cis_Tiaa_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Cert_Nbr() { return cis_Prtcpnt_File_Cis_Cert_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Tiaa_Doi() { return cis_Prtcpnt_File_Cis_Tiaa_Doi; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Doi() { return cis_Prtcpnt_File_Cis_Cref_Doi; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn() { return cis_Prtcpnt_File_Cis_Frst_Annt_Ssn; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Prfx() { return cis_Prtcpnt_File_Cis_Frst_Annt_Prfx; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme() { return cis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme() { return cis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme() { return cis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Sffx() { return cis_Prtcpnt_File_Cis_Frst_Annt_Sffx; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd() { return cis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde() { return cis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Dob() { return cis_Prtcpnt_File_Cis_Frst_Annt_Dob; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Sex_Cde() { return cis_Prtcpnt_File_Cis_Frst_Annt_Sex_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Frst_Annt_Calc_Method() { return cis_Prtcpnt_File_Cis_Frst_Annt_Calc_Method; }

    public DbsField getCis_Prtcpnt_File_Cis_Opn_Clsd_Ind() { return cis_Prtcpnt_File_Cis_Opn_Clsd_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Status_Cd() { return cis_Prtcpnt_File_Cis_Status_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Cntrct_Type() { return cis_Prtcpnt_File_Cis_Cntrct_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Cntrct_Apprvl_Ind() { return cis_Prtcpnt_File_Cis_Cntrct_Apprvl_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Id() { return cis_Prtcpnt_File_Cis_Rqst_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Hold_Cde() { return cis_Prtcpnt_File_Cis_Hold_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cntrct_Print_Dte() { return cis_Prtcpnt_File_Cis_Cntrct_Print_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Extract_Date() { return cis_Prtcpnt_File_Cis_Extract_Date; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Mit_Request() { return cis_Prtcpnt_File_Cis_Mit_Request; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme() { return cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde() { return cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde() { return cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Work_Process_Id() { return cis_Prtcpnt_File_Cis_Mit_Work_Process_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Step_Id() { return cis_Prtcpnt_File_Cis_Mit_Step_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Status_Cde() { return cis_Prtcpnt_File_Cis_Mit_Status_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Appl_Rcvd_Dte() { return cis_Prtcpnt_File_Cis_Appl_Rcvd_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Appl_Rcvd_User_Id() { return cis_Prtcpnt_File_Cis_Appl_Rcvd_User_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Appl_Entry_User_Id() { return cis_Prtcpnt_File_Cis_Appl_Entry_User_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Annty_Option() { return cis_Prtcpnt_File_Cis_Annty_Option; }

    public DbsField getCis_Prtcpnt_File_Cis_Pymnt_Mode() { return cis_Prtcpnt_File_Cis_Pymnt_Mode; }

    public DbsField getCis_Prtcpnt_File_Cis_Annty_Start_Dte() { return cis_Prtcpnt_File_Cis_Annty_Start_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Annty_End_Dte() { return cis_Prtcpnt_File_Cis_Annty_End_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs() { return cis_Prtcpnt_File_Cis_Grnted_Period_Yrs; }

    public DbsField getCis_Prtcpnt_File_Cis_Grnted_Period_Dys() { return cis_Prtcpnt_File_Cis_Grnted_Period_Dys; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Prfx() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Prfx; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Sffx; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Ssn; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Dob; }

    public DbsField getCis_Prtcpnt_File_Cis_Scnd_Annt_Sex_Cde() { return cis_Prtcpnt_File_Cis_Scnd_Annt_Sex_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Pin_Nbr() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Pin_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Prfx() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Prfx; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Frst_Nme() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Frst_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Mid_Nme() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Mid_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Lst_Nme() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Lst_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Sffx() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Sffx; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Birth_Dte() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Birth_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Death_Dte() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Death_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Ssn() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Ssn; }

    public DbsField getCis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Rltn_Cde() { return cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Rltn_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Grnted_Grd_Amt() { return cis_Prtcpnt_File_Cis_Grnted_Grd_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Grnted_Std_Amt() { return cis_Prtcpnt_File_Cis_Grnted_Std_Amt; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info() { return cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Tiaa_Commuted_Info() { return cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info; }

    public DbsField getCis_Prtcpnt_File_Cis_Comut_Grnted_Amt() { return cis_Prtcpnt_File_Cis_Comut_Grnted_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Comut_Int_Rate() { return cis_Prtcpnt_File_Cis_Comut_Int_Rate; }

    public DbsField getCis_Prtcpnt_File_Cis_Comut_Pymt_Method() { return cis_Prtcpnt_File_Cis_Comut_Pymt_Method; }

    public DbsField getCis_Prtcpnt_File_Cis_Grnted_Int_Rate() { return cis_Prtcpnt_File_Cis_Grnted_Int_Rate; }

    public DbsField getCis_Prtcpnt_File_Cis_Surv_Redct_Amt() { return cis_Prtcpnt_File_Cis_Surv_Redct_Amt; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Da_Tiaa_Cntrcts_Rqst() { return cis_Prtcpnt_File_Count_Castcis_Da_Tiaa_Cntrcts_Rqst; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst() { return cis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst; }

    public DbsField getCis_Prtcpnt_File_Cis_Da_Tiaa_Nbr() { return cis_Prtcpnt_File_Cis_Da_Tiaa_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt() { return cis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt() { return cis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Post_Ret_Trnsfer() { return cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Flag() { return cis_Prtcpnt_File_Cis_Trnsf_Flag; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Acct_Cde() { return cis_Prtcpnt_File_Cis_Trnsf_Acct_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Pymnt_Mode() { return cis_Prtcpnt_File_Cis_Trnsf_Pymnt_Mode; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Units() { return cis_Prtcpnt_File_Cis_Trnsf_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Cert_Nbr() { return cis_Prtcpnt_File_Cis_Trnsf_Cert_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_From_Company() { return cis_Prtcpnt_File_Cis_Trnsf_From_Company; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_To_Company() { return cis_Prtcpnt_File_Cis_Trnsf_To_Company; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Tiaa_Rea_Nbr() { return cis_Prtcpnt_File_Cis_Trnsf_Tiaa_Rea_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Rea_Units() { return cis_Prtcpnt_File_Cis_Trnsf_Rea_Units; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Da_Cref_Cntrcts_Rqst() { return cis_Prtcpnt_File_Count_Castcis_Da_Cref_Cntrcts_Rqst; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Da_Cref_Cntrcts_Rqst() { return cis_Prtcpnt_File_Cis_Da_Cref_Cntrcts_Rqst; }

    public DbsField getCis_Prtcpnt_File_Cis_Da_Cert_Nbr() { return cis_Prtcpnt_File_Cis_Da_Cert_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt() { return cis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Cref_Annty_Pymnt() { return cis_Prtcpnt_File_Count_Castcis_Cref_Annty_Pymnt; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Cref_Annty_Pymnt() { return cis_Prtcpnt_File_Cis_Cref_Annty_Pymnt; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Acct_Cde() { return cis_Prtcpnt_File_Cis_Cref_Acct_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units() { return cis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units() { return cis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Annty_Amt() { return cis_Prtcpnt_File_Cis_Cref_Annty_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Rea_Annty_Amt() { return cis_Prtcpnt_File_Cis_Rea_Annty_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Rea_Mnthly_Nbr_Units() { return cis_Prtcpnt_File_Cis_Rea_Mnthly_Nbr_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Rea_Annual_Nbr_Units() { return cis_Prtcpnt_File_Cis_Rea_Annual_Nbr_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Rea_Surv_Nbr_Units() { return cis_Prtcpnt_File_Cis_Rea_Surv_Nbr_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Surrender_Chg() { return cis_Prtcpnt_File_Cis_Surrender_Chg; }

    public DbsField getCis_Prtcpnt_File_Cis_Contingencies_Chg() { return cis_Prtcpnt_File_Cis_Contingencies_Chg; }

    public DbsField getCis_Prtcpnt_File_Cis_Mdo_Contract_Cash_Status() { return cis_Prtcpnt_File_Cis_Mdo_Contract_Cash_Status; }

    public DbsField getCis_Prtcpnt_File_Cis_Mdo_Contract_Type() { return cis_Prtcpnt_File_Cis_Mdo_Contract_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Mdo_Traditional_Amt() { return cis_Prtcpnt_File_Cis_Mdo_Traditional_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Mdo_Tiaa_Int_Pymnt_Amt() { return cis_Prtcpnt_File_Cis_Mdo_Tiaa_Int_Pymnt_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Mdo_Cref_Int_Pymnt_Amt() { return cis_Prtcpnt_File_Cis_Mdo_Cref_Int_Pymnt_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Mdo_Tiaa_Excluded_Amt() { return cis_Prtcpnt_File_Cis_Mdo_Tiaa_Excluded_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Mdo_Cref_Excluded_Amt() { return cis_Prtcpnt_File_Cis_Mdo_Cref_Excluded_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Tiaa_Personal_Annuity() { return cis_Prtcpnt_File_Cis_Tiaa_Personal_Annuity; }

    public DbsField getCis_Prtcpnt_File_Cis_Orig_Issue_State() { return cis_Prtcpnt_File_Cis_Orig_Issue_State; }

    public DbsField getCis_Prtcpnt_File_Cis_Issue_State_Name() { return cis_Prtcpnt_File_Cis_Issue_State_Name; }

    public DbsField getCis_Prtcpnt_File_Cis_Issue_State_Cd() { return cis_Prtcpnt_File_Cis_Issue_State_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Ppg_Code() { return cis_Prtcpnt_File_Cis_Ppg_Code; }

    public DbsField getCis_Prtcpnt_File_Cis_Region_Code() { return cis_Prtcpnt_File_Cis_Region_Code; }

    public DbsField getCis_Prtcpnt_File_Cis_Ownership_Cd() { return cis_Prtcpnt_File_Cis_Ownership_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Tax_Witholding() { return cis_Prtcpnt_File_Cis_Tax_Witholding; }

    public DbsField getCis_Prtcpnt_File_Cis_Bill_Code() { return cis_Prtcpnt_File_Cis_Bill_Code; }

    public DbsField getCis_Prtcpnt_File_Cis_Lob() { return cis_Prtcpnt_File_Cis_Lob; }

    public DbsField getCis_Prtcpnt_File_Cis_Lob_Type() { return cis_Prtcpnt_File_Cis_Lob_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Mail_Instructions() { return cis_Prtcpnt_File_Cis_Mail_Instructions; }

    public DbsField getCis_Prtcpnt_File_Cis_Premium_Paying() { return cis_Prtcpnt_File_Cis_Premium_Paying; }

    public DbsField getCis_Prtcpnt_File_Cis_Cashability_Ind() { return cis_Prtcpnt_File_Cis_Cashability_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Transferability_Ind() { return cis_Prtcpnt_File_Cis_Transferability_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Addr_Syn_Ind() { return cis_Prtcpnt_File_Cis_Addr_Syn_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Addr_Process_Env() { return cis_Prtcpnt_File_Cis_Addr_Process_Env; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Address_Info() { return cis_Prtcpnt_File_Cis_Address_Info; }

    public DbsField getCis_Prtcpnt_File_Cis_Address_Chg_Ind() { return cis_Prtcpnt_File_Cis_Address_Chg_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Address_Dest_Name() { return cis_Prtcpnt_File_Cis_Address_Dest_Name; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Address_TxtMuGroup() { return cis_Prtcpnt_File_Cis_Address_TxtMuGroup; }

    public DbsField getCis_Prtcpnt_File_Cis_Address_Txt() { return cis_Prtcpnt_File_Cis_Address_Txt; }

    public DbsField getCis_Prtcpnt_File_Cis_Zip_Code() { return cis_Prtcpnt_File_Cis_Zip_Code; }

    public DbsField getCis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr() { return cis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr() { return cis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Stndrd_Trn_Cd() { return cis_Prtcpnt_File_Cis_Stndrd_Trn_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Finalist_Reason_Cd() { return cis_Prtcpnt_File_Cis_Finalist_Reason_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Addr_Usage_Code() { return cis_Prtcpnt_File_Cis_Addr_Usage_Code; }

    public DbsField getCis_Prtcpnt_File_Cis_Checking_Saving_Cd() { return cis_Prtcpnt_File_Cis_Checking_Saving_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Addr_Stndrd_Code() { return cis_Prtcpnt_File_Cis_Addr_Stndrd_Code; }

    public DbsField getCis_Prtcpnt_File_Cis_Stndrd_Overide() { return cis_Prtcpnt_File_Cis_Stndrd_Overide; }

    public DbsField getCis_Prtcpnt_File_Cis_Postal_Data_Fields() { return cis_Prtcpnt_File_Cis_Postal_Data_Fields; }

    public DbsField getCis_Prtcpnt_File_Cis_Geographic_Cd() { return cis_Prtcpnt_File_Cis_Geographic_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Pct() { return cis_Prtcpnt_File_Cis_Rtb_Pct; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Amt() { return cis_Prtcpnt_File_Cis_Rtb_Amt; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Rtb_Dest_Data() { return cis_Prtcpnt_File_Cis_Rtb_Dest_Data; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Dest_Pct() { return cis_Prtcpnt_File_Cis_Rtb_Dest_Pct; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Dest_Amt() { return cis_Prtcpnt_File_Cis_Rtb_Dest_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Dest_Nme() { return cis_Prtcpnt_File_Cis_Rtb_Dest_Nme; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Rtb_Dest_AddrMuGroup() { return cis_Prtcpnt_File_Cis_Rtb_Dest_AddrMuGroup; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Dest_Addr() { return cis_Prtcpnt_File_Cis_Rtb_Dest_Addr; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Bank_Accnt_Nbr() { return cis_Prtcpnt_File_Cis_Rtb_Bank_Accnt_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Trnst_Nbr() { return cis_Prtcpnt_File_Cis_Rtb_Trnst_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Dest_Zip() { return cis_Prtcpnt_File_Cis_Rtb_Dest_Zip; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Dest_Type_Cd() { return cis_Prtcpnt_File_Cis_Rtb_Dest_Type_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Pct() { return cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Pct; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Desc() { return cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Desc; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Pct() { return cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Pct; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Desc() { return cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Desc; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Pct() { return cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Pct; }

    public DbsField getCis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Desc() { return cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Desc; }

    public DbsField getCis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Pct() { return cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Pct; }

    public DbsField getCis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Desc() { return cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Desc; }

    public DbsField getCis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Pct() { return cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Pct; }

    public DbsField getCis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Desc() { return cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Desc; }

    public DbsField getCis_Prtcpnt_File_Cis_Corp_Sync_Ind() { return cis_Prtcpnt_File_Cis_Corp_Sync_Ind; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta() { return cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind() { return cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme() { return cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Function() { return cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Function; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Process_Env() { return cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Process_Env; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Error_Cde() { return cis_Prtcpnt_File_Cis_Mit_Error_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Error_Msg() { return cis_Prtcpnt_File_Cis_Mit_Error_Msg; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Non_Premium_Dta() { return cis_Prtcpnt_File_Cis_Non_Premium_Dta; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind() { return cis_Prtcpnt_File_Cis_Nonp_Sync_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Process_Env() { return cis_Prtcpnt_File_Cis_Nonp_Process_Env; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Effective_Dte() { return cis_Prtcpnt_File_Cis_Nonp_Effective_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Product_Cde() { return cis_Prtcpnt_File_Cis_Nonp_Product_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Contract_Retr_Dte() { return cis_Prtcpnt_File_Cis_Nonp_Contract_Retr_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Rate_Accum_Adj_Amt() { return cis_Prtcpnt_File_Cis_Nonp_Rate_Accum_Adj_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Currency() { return cis_Prtcpnt_File_Cis_Nonp_Currency; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Tiaa_Age_First_Pymnt() { return cis_Prtcpnt_File_Cis_Nonp_Tiaa_Age_First_Pymnt; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Cref_Age_First_Pymnt() { return cis_Prtcpnt_File_Cis_Nonp_Cref_Age_First_Pymnt; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Deletion_Cde() { return cis_Prtcpnt_File_Cis_Nonp_Deletion_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Vesting_Dte() { return cis_Prtcpnt_File_Cis_Nonp_Vesting_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Cntr_Rate38_Accum_Amt() { return cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate38_Accum_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Cntr_Rate42_Accum_Amt() { return cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate42_Accum_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Error_Cde() { return cis_Prtcpnt_File_Cis_Nonp_Error_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Nonp_Error_Msg() { return cis_Prtcpnt_File_Cis_Nonp_Error_Msg; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Allocation_Dta() { return cis_Prtcpnt_File_Cis_Allocation_Dta; }

    public DbsField getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind() { return cis_Prtcpnt_File_Cis_Alloc_Sync_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Alloc_Process_Env() { return cis_Prtcpnt_File_Cis_Alloc_Process_Env; }

    public DbsField getCis_Prtcpnt_File_Cis_Alloc_Effective_Dte() { return cis_Prtcpnt_File_Cis_Alloc_Effective_Dte; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Alloc_Percentage() { return cis_Prtcpnt_File_Count_Castcis_Alloc_Percentage; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Alloc_PercentageMuGroup() { return cis_Prtcpnt_File_Cis_Alloc_PercentageMuGroup; }

    public DbsField getCis_Prtcpnt_File_Cis_Alloc_Percentage() { return cis_Prtcpnt_File_Cis_Alloc_Percentage; }

    public DbsField getCis_Prtcpnt_File_Cis_Alloc_Error_Cde() { return cis_Prtcpnt_File_Cis_Alloc_Error_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Alloc_Error_Msg() { return cis_Prtcpnt_File_Cis_Alloc_Error_Msg; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Sync_Ind() { return cis_Prtcpnt_File_Cis_Cor_Sync_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Process_Env() { return cis_Prtcpnt_File_Cis_Cor_Process_Env; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Rcd_Typ_Cde() { return cis_Prtcpnt_File_Cis_Cor_Ph_Rcd_Typ_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Neg_Election_Cde() { return cis_Prtcpnt_File_Cis_Cor_Ph_Neg_Election_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Occupnt_Nme() { return cis_Prtcpnt_File_Cis_Cor_Ph_Occupnt_Nme; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Xref_Pin() { return cis_Prtcpnt_File_Cis_Cor_Ph_Xref_Pin; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Active_Vip_Cnt() { return cis_Prtcpnt_File_Cis_Cor_Ph_Active_Vip_Cnt; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Cor_Ph_Actve_Vip_Grp() { return cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Actve_Vip_Grp; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Grp() { return cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Grp; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Cde() { return cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Area_Cde() { return cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Area_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Mail_Table_Cnt() { return cis_Prtcpnt_File_Cis_Cor_Mail_Table_Cnt; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Cor_Ph_Mail_Grp() { return cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Mail_Grp; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Cor_Ph_Mail_Grp() { return cis_Prtcpnt_File_Cis_Cor_Ph_Mail_Grp; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Phmail_Cde() { return cis_Prtcpnt_File_Cis_Cor_Phmail_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Phmail_Area_Of_Origin() { return cis_Prtcpnt_File_Cis_Cor_Phmail_Area_Of_Origin; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte() { return cis_Prtcpnt_File_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Nbr() { return cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Ind() { return cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind() { return cis_Prtcpnt_File_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde() { return cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Media_Ind() { return cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Media_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Cntnts_Cde() { return cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Cntnts_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Cntrct_Table_Cnt() { return cis_Prtcpnt_File_Cis_Cor_Cntrct_Table_Cnt; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Cde() { return cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Yr_Dte() { return cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Yr_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Cntrct_Payee_Cde() { return cis_Prtcpnt_File_Cis_Cor_Cntrct_Payee_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Error_Cde() { return cis_Prtcpnt_File_Cis_Cor_Error_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Cor_Error_Msg() { return cis_Prtcpnt_File_Cis_Cor_Error_Msg; }

    public DbsField getCis_Prtcpnt_File_Cis_Pull_Code() { return cis_Prtcpnt_File_Cis_Pull_Code; }

    public DbsField getCis_Prtcpnt_File_Cis_Annual_Required_Dist_Amt() { return cis_Prtcpnt_File_Cis_Annual_Required_Dist_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Required_Begin_Date() { return cis_Prtcpnt_File_Cis_Required_Begin_Date; }

    public DbsField getCis_Prtcpnt_File_Cis_Rea_Nbr() { return cis_Prtcpnt_File_Cis_Rea_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Rea_Doi() { return cis_Prtcpnt_File_Cis_Rea_Doi; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Issue_State() { return cis_Prtcpnt_File_Cis_Cref_Issue_State; }

    public DbsField getCis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type() { return cis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Rea_Cntrct_Type() { return cis_Prtcpnt_File_Cis_Rea_Cntrct_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Cntrct_Type() { return cis_Prtcpnt_File_Cis_Cref_Cntrct_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Financial_Data_1() { return cis_Prtcpnt_File_Cis_Financial_Data_1; }

    public DbsField getCis_Prtcpnt_File_Cis_Financial_Data_2() { return cis_Prtcpnt_File_Cis_Financial_Data_2; }

    public DbsField getCis_Prtcpnt_File_Cis_Financial_Data_3() { return cis_Prtcpnt_File_Cis_Financial_Data_3; }

    public DbsField getCis_Prtcpnt_File_Cis_Financial_Data_4() { return cis_Prtcpnt_File_Cis_Financial_Data_4; }

    public DbsField getCis_Prtcpnt_File_Cis_Financial_Data_5() { return cis_Prtcpnt_File_Cis_Financial_Data_5; }

    public DbsField getCis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info_2() { return cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info_2; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2() { return cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2; }

    public DbsField getCis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2() { return cis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2; }

    public DbsField getCis_Prtcpnt_File_Cis_Comut_Int_Rate_2() { return cis_Prtcpnt_File_Cis_Comut_Int_Rate_2; }

    public DbsField getCis_Prtcpnt_File_Cis_Comut_Pymt_Method_2() { return cis_Prtcpnt_File_Cis_Comut_Pymt_Method_2; }

    public DbsField getCis_Prtcpnt_File_Cis_Comut_Mortality_Basis() { return cis_Prtcpnt_File_Cis_Comut_Mortality_Basis; }

    public DbsField getCis_Prtcpnt_File_Cis_Grnted_Period_Mnts() { return cis_Prtcpnt_File_Cis_Grnted_Period_Mnts; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Sg_Fund_Identifier() { return cis_Prtcpnt_File_Cis_Sg_Fund_Identifier; }

    public DbsField getCis_Prtcpnt_File_Cis_Sg_Fund_Ticker() { return cis_Prtcpnt_File_Cis_Sg_Fund_Ticker; }

    public DbsField getCis_Prtcpnt_File_Cis_Sg_Fund_Allocation() { return cis_Prtcpnt_File_Cis_Sg_Fund_Allocation; }

    public DbsField getCis_Prtcpnt_File_Cis_Sg_Fund_Amt() { return cis_Prtcpnt_File_Cis_Sg_Fund_Amt; }

    public DbsField getCis_Prtcpnt_File_Cis_Sg_Text_Udf_1() { return cis_Prtcpnt_File_Cis_Sg_Text_Udf_1; }

    public DbsField getCis_Prtcpnt_File_Cis_Sg_Text_Udf_2() { return cis_Prtcpnt_File_Cis_Sg_Text_Udf_2; }

    public DbsField getCis_Prtcpnt_File_Cis_Sg_Text_Udf_3() { return cis_Prtcpnt_File_Cis_Sg_Text_Udf_3; }

    public DbsField getCis_Prtcpnt_File_Cis_Tacc_Annty_Amt() { return cis_Prtcpnt_File_Cis_Tacc_Annty_Amt; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Tacc_Fund_Info() { return cis_Prtcpnt_File_Cis_Tacc_Fund_Info; }

    public DbsField getCis_Prtcpnt_File_Cis_Tacc_Ind() { return cis_Prtcpnt_File_Cis_Tacc_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Tacc_Account() { return cis_Prtcpnt_File_Cis_Tacc_Account; }

    public DbsField getCis_Prtcpnt_File_Cis_Tacc_Mnthly_Nbr_Units() { return cis_Prtcpnt_File_Cis_Tacc_Mnthly_Nbr_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Tacc_Annual_Nbr_Units() { return cis_Prtcpnt_File_Cis_Tacc_Annual_Nbr_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Institution_Name() { return cis_Prtcpnt_File_Cis_Institution_Name; }

    public DbsField getCis_Prtcpnt_File_Cis_Four_Fifty_Seven_Ind() { return cis_Prtcpnt_File_Cis_Four_Fifty_Seven_Ind; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Trnsf_Info() { return cis_Prtcpnt_File_Cis_Trnsf_Info; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Ticker() { return cis_Prtcpnt_File_Cis_Trnsf_Ticker; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Reval_Type() { return cis_Prtcpnt_File_Cis_Trnsf_Reval_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Cref_Units() { return cis_Prtcpnt_File_Cis_Trnsf_Cref_Units; }

    public DbsField getCis_Prtcpnt_File_Cis_Trnsf_Rece_Company() { return cis_Prtcpnt_File_Cis_Trnsf_Rece_Company; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cis_Prtcpnt_File = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt_File", "CIS-PRTCPNT-FILE"), "CIS_PRTCPNT_FILE_12", "CIS_PRTCPNT_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE_12"));
        cis_Prtcpnt_File_Cis_Pin_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Prtcpnt_File_Cis_Rqst_Id_Key = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "CIS_RQST_ID_KEY");
        cis_Prtcpnt_File_Cis_Tiaa_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_TIAA_NBR");
        cis_Prtcpnt_File_Cis_Cert_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_CERT_NBR");
        cis_Prtcpnt_File_Cis_Tiaa_Doi = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_TIAA_DOI");
        cis_Prtcpnt_File_Cis_Cref_Doi = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_CREF_DOI");
        cis_Prtcpnt_File_Cis_Frst_Annt_Ssn = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_SSN");
        cis_Prtcpnt_File_Cis_Frst_Annt_Prfx = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_PRFX");
        cis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_FRST_NME");
        cis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_MID_NME");
        cis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_LST_NME");
        cis_Prtcpnt_File_Cis_Frst_Annt_Sffx = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_SFFX");
        cis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd", "CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_CTZNSHP_CD");
        cis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde", "CIS-FRST-ANNT-RSDNC-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_RSDNC_CDE");
        cis_Prtcpnt_File_Cis_Frst_Annt_Dob = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_DOB");
        cis_Prtcpnt_File_Cis_Frst_Annt_Sex_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_SEX_CDE");
        cis_Prtcpnt_File_Cis_Frst_Annt_Calc_Method = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Frst_Annt_Calc_Method", "CIS-FRST-ANNT-CALC-METHOD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_CALC_METHOD");
        cis_Prtcpnt_File_Cis_Opn_Clsd_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_OPN_CLSD_IND");
        cis_Prtcpnt_File_Cis_Status_Cd = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_STATUS_CD");
        cis_Prtcpnt_File_Cis_Cntrct_Type = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_CNTRCT_TYPE");
        cis_Prtcpnt_File_Cis_Cntrct_Apprvl_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTRCT_APPRVL_IND");
        cis_Prtcpnt_File_Cis_Rqst_Id = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_RQST_ID");
        cis_Prtcpnt_File_Cis_Hold_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Hold_Cde", "CIS-HOLD-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_HOLD_CDE");
        cis_Prtcpnt_File_Cis_Cntrct_Print_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CNTRCT_PRINT_DTE");
        cis_Prtcpnt_File_Cis_Extract_Date = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Extract_Date", "CIS-EXTRACT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_EXTRACT_DATE");
        cis_Prtcpnt_File_Cis_Mit_Request = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Mit_Request", "CIS-MIT-REQUEST");
        cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme", "CIS-MIT-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_MIT_RQST_LOG_DTE_TME");
        cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde", 
            "CIS-MIT-RQST-LOG-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_MIT_RQST_LOG_OPRTR_CDE");
        cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde", "CIS-MIT-ORIGINAL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_MIT_ORIGINAL_UNIT_CDE");
        cis_Prtcpnt_File_Cis_Mit_Work_Process_Id = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Work_Process_Id", "CIS-MIT-WORK-PROCESS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "CIS_MIT_WORK_PROCESS_ID");
        cis_Prtcpnt_File_Cis_Mit_Step_Id = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Step_Id", "CIS-MIT-STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "CIS_MIT_STEP_ID");
        cis_Prtcpnt_File_Cis_Mit_Status_Cde = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Status_Cde", "CIS-MIT-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_MIT_STATUS_CDE");
        cis_Prtcpnt_File_Cis_Appl_Rcvd_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_APPL_RCVD_DTE");
        cis_Prtcpnt_File_Cis_Appl_Rcvd_User_Id = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Appl_Rcvd_User_Id", "CIS-APPL-RCVD-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_APPL_RCVD_USER_ID");
        cis_Prtcpnt_File_Cis_Appl_Entry_User_Id = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Appl_Entry_User_Id", "CIS-APPL-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_APPL_ENTRY_USER_ID");
        cis_Prtcpnt_File_Cis_Annty_Option = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_ANNTY_OPTION");
        cis_Prtcpnt_File_Cis_Pymnt_Mode = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_PYMNT_MODE");
        cis_Prtcpnt_File_Cis_Annty_Start_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Annty_Start_Dte", "CIS-ANNTY-START-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ANNTY_START_DTE");
        cis_Prtcpnt_File_Cis_Annty_End_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ANNTY_END_DTE");
        cis_Prtcpnt_File_Cis_Grnted_Period_Yrs = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Grnted_Period_Yrs", "CIS-GRNTED-PERIOD-YRS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_YRS");
        cis_Prtcpnt_File_Cis_Grnted_Period_Dys = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Grnted_Period_Dys", "CIS-GRNTED-PERIOD-DYS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_DYS");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Prfx = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_PRFX");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_FRST_NME");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_MID_NME");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_LST_NME");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Sffx = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SFFX");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Ssn = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SSN");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Dob = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_DOB");
        cis_Prtcpnt_File_Cis_Scnd_Annt_Sex_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SEX_CDE");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Pin_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Pin_Nbr", "CIS-ORGNL-PRTCPNT-PIN-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_PIN_NBR");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Prfx = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Prfx", "CIS-ORGNL-PRTCPNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_PRFX");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Frst_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Frst_Nme", "CIS-ORGNL-PRTCPNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_FRST_NME");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Mid_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Mid_Nme", "CIS-ORGNL-PRTCPNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_MID_NME");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Lst_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Lst_Nme", "CIS-ORGNL-PRTCPNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_LST_NME");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Sffx = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Sffx", "CIS-ORGNL-PRTCPNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_SFFX");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Birth_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Birth_Dte", 
            "CIS-ORGNL-PRTCPNT-BIRTH-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_BIRTH_DTE");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Death_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Death_Dte", 
            "CIS-ORGNL-PRTCPNT-DEATH-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_DEATH_DTE");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Ssn = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Ssn", "CIS-ORGNL-PRTCPNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_SSN");
        cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Rltn_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orgnl_Prtcpnt_Rltn_Cde", "CIS-ORGNL-PRTCPNT-RLTN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_RLTN_CDE");
        cis_Prtcpnt_File_Cis_Grnted_Grd_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Grnted_Grd_Amt", "CIS-GRNTED-GRD-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_GRD_AMT");
        cis_Prtcpnt_File_Cis_Grnted_Std_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Grnted_Std_Amt", "CIS-GRNTED-STD-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_STD_AMT");
        cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info", 
            "C*CIS-TIAA-COMMUTED-INFO", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info", "CIS-TIAA-COMMUTED-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_Cis_Comut_Grnted_Amt = cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Comut_Grnted_Amt", 
            "CIS-COMUT-GRNTED-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_Cis_Comut_Int_Rate = cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Comut_Int_Rate", "CIS-COMUT-INT-RATE", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_Cis_Comut_Pymt_Method = cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Comut_Pymt_Method", 
            "CIS-COMUT-PYMT-METHOD", FieldType.STRING, 8, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_Cis_Grnted_Int_Rate = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Grnted_Int_Rate", "CIS-GRNTED-INT-RATE", 
            FieldType.NUMERIC, 5, 3, RepeatingFieldStrategy.None, "CIS_GRNTED_INT_RATE");
        cis_Prtcpnt_File_Cis_Surv_Redct_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Surv_Redct_Amt", "CIS-SURV-REDCT-AMT", 
            FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "CIS_SURV_REDCT_AMT");
        cis_Prtcpnt_File_Count_Castcis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Da_Tiaa_Cntrcts_Rqst", 
            "C*CIS-DA-TIAA-CNTRCTS-RQST", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst", "CIS-DA-TIAA-CNTRCTS-RQST", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Da_Tiaa_Nbr = cis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Da_Tiaa_Nbr", "CIS-DA-TIAA-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_NBR", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt = cis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt", 
            "CIS-DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt = cis_Prtcpnt_File_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt", 
            "CIS-DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_REA_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer", "CIS-POST-RET-TRNSFER");
        cis_Prtcpnt_File_Cis_Trnsf_Flag = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_TRNSF_FLAG");
        cis_Prtcpnt_File_Cis_Trnsf_Acct_Cde = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_Acct_Cde", "CIS-TRNSF-ACCT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TRNSF_ACCT_CDE");
        cis_Prtcpnt_File_Cis_Trnsf_Pymnt_Mode = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_Pymnt_Mode", "CIS-TRNSF-PYMNT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TRNSF_PYMNT_MODE");
        cis_Prtcpnt_File_Cis_Trnsf_Units = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_Units", "CIS-TRNSF-UNITS", 
            FieldType.NUMERIC, 8, 3, RepeatingFieldStrategy.None, "CIS_TRNSF_UNITS");
        cis_Prtcpnt_File_Cis_Trnsf_Cert_Nbr = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_Cert_Nbr", "CIS-TRNSF-CERT-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TRNSF_CERT_NBR");
        cis_Prtcpnt_File_Cis_Trnsf_From_Company = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_From_Company", "CIS-TRNSF-FROM-COMPANY", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_TRNSF_FROM_COMPANY");
        cis_Prtcpnt_File_Cis_Trnsf_To_Company = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_To_Company", "CIS-TRNSF-TO-COMPANY", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_TRNSF_TO_COMPANY");
        cis_Prtcpnt_File_Cis_Trnsf_Tiaa_Rea_Nbr = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_Tiaa_Rea_Nbr", "CIS-TRNSF-TIAA-REA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TRNSF_TIAA_REA_NBR");
        cis_Prtcpnt_File_Cis_Trnsf_Rea_Units = cis_Prtcpnt_File_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_File_Cis_Trnsf_Rea_Units", "CIS-TRNSF-REA-UNITS", 
            FieldType.NUMERIC, 8, 3, RepeatingFieldStrategy.None, "CIS_TRNSF_REA_UNITS");
        cis_Prtcpnt_File_Count_Castcis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Da_Cref_Cntrcts_Rqst", 
            "C*CIS-DA-CREF-CNTRCTS-RQST", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Da_Cref_Cntrcts_Rqst", "CIS-DA-CREF-CNTRCTS-RQST", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Da_Cert_Nbr = cis_Prtcpnt_File_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Da_Cert_Nbr", "CIS-DA-CERT-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CERT_NBR", "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt = cis_Prtcpnt_File_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt", 
            "CIS-DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CREF_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_Count_Castcis_Cref_Annty_Pymnt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Cref_Annty_Pymnt", 
            "C*CIS-CREF-ANNTY-PYMNT", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_Cis_Cref_Annty_Pymnt = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_Cis_Cref_Acct_Cde = cis_Prtcpnt_File_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ACCT_CDE", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units = cis_Prtcpnt_File_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units", 
            "CIS-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_MNTHLY_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units = cis_Prtcpnt_File_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units", 
            "CIS-CREF-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ANNUAL_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_Cis_Cref_Annty_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cref_Annty_Amt", "CIS-CREF-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_CREF_ANNTY_AMT");
        cis_Prtcpnt_File_Cis_Rea_Annty_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rea_Annty_Amt", "CIS-REA-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_REA_ANNTY_AMT");
        cis_Prtcpnt_File_Cis_Rea_Mnthly_Nbr_Units = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rea_Mnthly_Nbr_Units", "CIS-REA-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_MNTHLY_NBR_UNITS");
        cis_Prtcpnt_File_Cis_Rea_Annual_Nbr_Units = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rea_Annual_Nbr_Units", "CIS-REA-ANNUAL-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_ANNUAL_NBR_UNITS");
        cis_Prtcpnt_File_Cis_Rea_Surv_Nbr_Units = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rea_Surv_Nbr_Units", "CIS-REA-SURV-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_SURV_NBR_UNITS");
        cis_Prtcpnt_File_Cis_Surrender_Chg = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Surrender_Chg", "CIS-SURRENDER-CHG", 
            FieldType.NUMERIC, 4, 2, RepeatingFieldStrategy.None, "CIS_SURRENDER_CHG");
        cis_Prtcpnt_File_Cis_Contingencies_Chg = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Contingencies_Chg", "CIS-CONTINGENCIES-CHG", 
            FieldType.NUMERIC, 4, 2, RepeatingFieldStrategy.None, "CIS_CONTINGENCIES_CHG");
        cis_Prtcpnt_File_Cis_Mdo_Contract_Cash_Status = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mdo_Contract_Cash_Status", 
            "CIS-MDO-CONTRACT-CASH-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_MDO_CONTRACT_CASH_STATUS");
        cis_Prtcpnt_File_Cis_Mdo_Contract_Type = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_MDO_CONTRACT_TYPE");
        cis_Prtcpnt_File_Cis_Mdo_Traditional_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mdo_Traditional_Amt", "CIS-MDO-TRADITIONAL-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TRADITIONAL_AMT");
        cis_Prtcpnt_File_Cis_Mdo_Tiaa_Int_Pymnt_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mdo_Tiaa_Int_Pymnt_Amt", "CIS-MDO-TIAA-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_INT_PYMNT_AMT");
        cis_Prtcpnt_File_Cis_Mdo_Cref_Int_Pymnt_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mdo_Cref_Int_Pymnt_Amt", "CIS-MDO-CREF-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_INT_PYMNT_AMT");
        cis_Prtcpnt_File_Cis_Mdo_Tiaa_Excluded_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mdo_Tiaa_Excluded_Amt", "CIS-MDO-TIAA-EXCLUDED-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_EXCLUDED_AMT");
        cis_Prtcpnt_File_Cis_Mdo_Cref_Excluded_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mdo_Cref_Excluded_Amt", "CIS-MDO-CREF-EXCLUDED-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_EXCLUDED_AMT");
        cis_Prtcpnt_File_Cis_Tiaa_Personal_Annuity = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tiaa_Personal_Annuity", "CIS-TIAA-PERSONAL-ANNUITY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TIAA_PERSONAL_ANNUITY");
        cis_Prtcpnt_File_Cis_Orig_Issue_State = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Orig_Issue_State", "CIS-ORIG-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_ORIG_ISSUE_STATE");
        cis_Prtcpnt_File_Cis_Issue_State_Name = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Issue_State_Name", "CIS-ISSUE-STATE-NAME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_ISSUE_STATE_NAME");
        cis_Prtcpnt_File_Cis_Issue_State_Cd = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_ISSUE_STATE_CD");
        cis_Prtcpnt_File_Cis_Ppg_Code = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Ppg_Code", "CIS-PPG-CODE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "CIS_PPG_CODE");
        cis_Prtcpnt_File_Cis_Region_Code = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Region_Code", "CIS-REGION-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CIS_REGION_CODE");
        cis_Prtcpnt_File_Cis_Ownership_Cd = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CIS_OWNERSHIP_CD");
        cis_Prtcpnt_File_Cis_Tax_Witholding = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tax_Witholding", "CIS-TAX-WITHOLDING", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_TAX_WITHOLDING");
        cis_Prtcpnt_File_Cis_Bill_Code = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Bill_Code", "CIS-BILL-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BILL_CODE");
        cis_Prtcpnt_File_Cis_Lob = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Lob", "CIS-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_LOB");
        cis_Prtcpnt_File_Cis_Lob_Type = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Lob_Type", "CIS-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_LOB_TYPE");
        cis_Prtcpnt_File_Cis_Mail_Instructions = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Mail_Instructions", "CIS-MAIL-INSTRUCTIONS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_MAIL_INSTRUCTIONS");
        cis_Prtcpnt_File_Cis_Premium_Paying = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Premium_Paying", "CIS-PREMIUM-PAYING", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_PREMIUM_PAYING");
        cis_Prtcpnt_File_Cis_Cashability_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cashability_Ind", "CIS-CASHABILITY-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_CASHABILITY_IND");
        cis_Prtcpnt_File_Cis_Transferability_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Transferability_Ind", "CIS-TRANSFERABILITY-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_TRANSFERABILITY_IND");
        cis_Prtcpnt_File_Cis_Addr_Syn_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Addr_Syn_Ind", "CIS-ADDR-SYN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_ADDR_SYN_IND");
        cis_Prtcpnt_File_Cis_Addr_Process_Env = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Addr_Process_Env", "CIS-ADDR-PROCESS-ENV", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_ADDR_PROCESS_ENV");
        cis_Prtcpnt_File_Cis_Address_Info = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Address_Info", "CIS-ADDRESS-INFO", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Address_Chg_Ind = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Address_Chg_Ind", "CIS-ADDRESS-CHG-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_CHG_IND", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Address_Dest_Name = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Address_Dest_Name", "CIS-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_DEST_NAME", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Address_TxtMuGroup = cis_Prtcpnt_File_Cis_Address_Info.newGroupInGroup("cis_Prtcpnt_File_Cis_Address_TxtMuGroup", "CIS_ADDRESS_TXTMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_ADDRESS_TXT", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Address_Txt = cis_Prtcpnt_File_Cis_Address_TxtMuGroup.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Address_Txt", "CIS-ADDRESS-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,3,1,5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_ADDRESS_TXT", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Zip_Code = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Zip_Code", "CIS-ZIP-CODE", FieldType.STRING, 
            9, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ZIP_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr", 
            "CIS-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_PYMNT_ACCT_NMBR", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr", "CIS-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_ABA_ACCT_NMBR", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Stndrd_Trn_Cd = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Stndrd_Trn_Cd", "CIS-STNDRD-TRN-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_TRN_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Finalist_Reason_Cd = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Finalist_Reason_Cd", "CIS-FINALIST-REASON-CD", 
            FieldType.STRING, 10, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_FINALIST_REASON_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Addr_Usage_Code = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Addr_Usage_Code", "CIS-ADDR-USAGE-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_USAGE_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Checking_Saving_Cd = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Checking_Saving_Cd", "CIS-CHECKING-SAVING-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CHECKING_SAVING_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Addr_Stndrd_Code = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Addr_Stndrd_Code", "CIS-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_STNDRD_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Stndrd_Overide = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Stndrd_Overide", "CIS-STNDRD-OVERIDE", 
            FieldType.STRING, 1, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_OVERIDE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Postal_Data_Fields = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Postal_Data_Fields", "CIS-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_POSTAL_DATA_FIELDS", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Geographic_Cd = cis_Prtcpnt_File_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Geographic_Cd", "CIS-GEOGRAPHIC-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_GEOGRAPHIC_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_Cis_Rtb_Pct = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_Pct", "CIS-RTB-PCT", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CIS_RTB_PCT");
        cis_Prtcpnt_File_Cis_Rtb_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_Amt", "CIS-RTB-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_RTB_AMT");
        cis_Prtcpnt_File_Cis_Rtb_Dest_Data = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_Data", "CIS-RTB-DEST-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Dest_Pct = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_Pct", "CIS-RTB-DEST-PCT", 
            FieldType.NUMERIC, 3, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_PCT", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Dest_Amt = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_Amt", "CIS-RTB-DEST-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_AMT", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Dest_Nme = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_Nme", "CIS-RTB-DEST-NME", 
            FieldType.STRING, 35, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_NME", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Dest_AddrMuGroup = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newGroupInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_AddrMuGroup", "CIS_RTB_DEST_ADDRMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_RTB_DEST_ADDR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Dest_Addr = cis_Prtcpnt_File_Cis_Rtb_Dest_AddrMuGroup.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_Addr", "CIS-RTB-DEST-ADDR", 
            FieldType.STRING, 35, new DbsArrayController(1,3,1,5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_RTB_DEST_ADDR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Bank_Accnt_Nbr = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Bank_Accnt_Nbr", "CIS-RTB-BANK-ACCNT-NBR", 
            FieldType.STRING, 21, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_BANK_ACCNT_NBR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Trnst_Nbr = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Trnst_Nbr", "CIS-RTB-TRNST-NBR", 
            FieldType.NUMERIC, 9, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_TRNST_NBR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Dest_Zip = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_Zip", "CIS-RTB-DEST-ZIP", 
            FieldType.STRING, 9, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_ZIP", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Dest_Type_Cd = cis_Prtcpnt_File_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Rtb_Dest_Type_Cd", "CIS-RTB-DEST-TYPE-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_TYPE_CD", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Pct = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Pct", "CIS-RTB-FED-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_RTB_FED_TAX_WH_PCT");
        cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Desc = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_Fed_Tax_Wh_Desc", "CIS-RTB-FED-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_RTB_FED_TAX_WH_DESC");
        cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Pct = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Pct", "CIS-RTB-ST-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_RTB_ST_TAX_WH_PCT");
        cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Desc = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_St_Tax_Wh_Desc", "CIS-RTB-ST-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_RTB_ST_TAX_WH_DESC");
        cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Pct = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Pct", "CIS-RTB-NON-RES-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_RTB_NON_RES_TAX_WH_PCT");
        cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Desc = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rtb_Non_Res_Tax_Wh_Desc", 
            "CIS-RTB-NON-RES-TAX-WH-DESC", FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_RTB_NON_RES_TAX_WH_DESC");
        cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Pct = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Pct", "CIS-PE-PYMT-FED-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_PE_PYMT_FED_TAX_WH_PCT");
        cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Desc = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pe_Pymt_Fed_Tax_Wh_Desc", 
            "CIS-PE-PYMT-FED-TAX-WH-DESC", FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_PE_PYMT_FED_TAX_WH_DESC");
        cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Pct = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Pct", "CIS-PE-PYMT-ST-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_PE_PYMT_ST_TAX_WH_PCT");
        cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Desc = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pe_Pymt_St_Tax_Wh_Desc", "CIS-PE-PYMT-ST-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_PE_PYMT_ST_TAX_WH_DESC");
        cis_Prtcpnt_File_Cis_Corp_Sync_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Corp_Sync_Ind", "CIS-CORP-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CORP_SYNC_IND");
        cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta", "CIS-RQST-SYSTEM-MIT-DTA");
        cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind = cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind", 
            "CIS-RQST-SYS-MIT-SYNC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_SYNC_IND");
        cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme = cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme", 
            "CIS-RQST-SYS-MIT-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_LOG_DTE_TME");
        cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Function = cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Function", 
            "CIS-RQST-SYS-MIT-FUNCTION", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_FUNCTION");
        cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Process_Env = cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Process_Env", 
            "CIS-RQST-SYS-MIT-PROCESS-ENV", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_PROCESS_ENV");
        cis_Prtcpnt_File_Cis_Mit_Error_Cde = cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Error_Cde", "CIS-MIT-ERROR-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_MIT_ERROR_CDE");
        cis_Prtcpnt_File_Cis_Mit_Error_Msg = cis_Prtcpnt_File_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Error_Msg", "CIS-MIT-ERROR-MSG", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "CIS_MIT_ERROR_MSG");
        cis_Prtcpnt_File_Cis_Non_Premium_Dta = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Non_Premium_Dta", "CIS-NON-PREMIUM-DTA");
        cis_Prtcpnt_File_Cis_Nonp_Sync_Ind = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Sync_Ind", "CIS-NONP-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_NONP_SYNC_IND");
        cis_Prtcpnt_File_Cis_Nonp_Process_Env = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Process_Env", "CIS-NONP-PROCESS-ENV", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_NONP_PROCESS_ENV");
        cis_Prtcpnt_File_Cis_Nonp_Effective_Dte = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Effective_Dte", "CIS-NONP-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_NONP_EFFECTIVE_DTE");
        cis_Prtcpnt_File_Cis_Nonp_Product_Cde = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Product_Cde", "CIS-NONP-PRODUCT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_NONP_PRODUCT_CDE");
        cis_Prtcpnt_File_Cis_Nonp_Contract_Retr_Dte = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Contract_Retr_Dte", 
            "CIS-NONP-CONTRACT-RETR-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_NONP_CONTRACT_RETR_DTE");
        cis_Prtcpnt_File_Cis_Nonp_Rate_Accum_Adj_Amt = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Rate_Accum_Adj_Amt", 
            "CIS-NONP-RATE-ACCUM-ADJ-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "CIS_NONP_RATE_ACCUM_ADJ_AMT");
        cis_Prtcpnt_File_Cis_Nonp_Currency = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Currency", "CIS-NONP-CURRENCY", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CIS_NONP_CURRENCY");
        cis_Prtcpnt_File_Cis_Nonp_Tiaa_Age_First_Pymnt = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Tiaa_Age_First_Pymnt", 
            "CIS-NONP-TIAA-AGE-FIRST-PYMNT", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_NONP_TIAA_AGE_FIRST_PYMNT");
        cis_Prtcpnt_File_Cis_Nonp_Cref_Age_First_Pymnt = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Cref_Age_First_Pymnt", 
            "CIS-NONP-CREF-AGE-FIRST-PYMNT", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_NONP_CREF_AGE_FIRST_PYMNT");
        cis_Prtcpnt_File_Cis_Nonp_Deletion_Cde = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Deletion_Cde", "CIS-NONP-DELETION-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_NONP_DELETION_CDE");
        cis_Prtcpnt_File_Cis_Nonp_Vesting_Dte = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Vesting_Dte", "CIS-NONP-VESTING-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_NONP_VESTING_DTE");
        cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate38_Accum_Amt = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate38_Accum_Amt", 
            "CIS-NONP-CNTR-RATE38-ACCUM-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "CIS_NONP_CNTR_RATE38_ACCUM_AMT");
        cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate42_Accum_Amt = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Cntr_Rate42_Accum_Amt", 
            "CIS-NONP-CNTR-RATE42-ACCUM-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "CIS_NONP_CNTR_RATE42_ACCUM_AMT");
        cis_Prtcpnt_File_Cis_Nonp_Error_Cde = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Error_Cde", "CIS-NONP-ERROR-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_NONP_ERROR_CDE");
        cis_Prtcpnt_File_Cis_Nonp_Error_Msg = cis_Prtcpnt_File_Cis_Non_Premium_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Nonp_Error_Msg", "CIS-NONP-ERROR-MSG", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "CIS_NONP_ERROR_MSG");
        cis_Prtcpnt_File_Cis_Allocation_Dta = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Allocation_Dta", "CIS-ALLOCATION-DTA");
        cis_Prtcpnt_File_Cis_Alloc_Sync_Ind = cis_Prtcpnt_File_Cis_Allocation_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Alloc_Sync_Ind", "CIS-ALLOC-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_ALLOC_SYNC_IND");
        cis_Prtcpnt_File_Cis_Alloc_Process_Env = cis_Prtcpnt_File_Cis_Allocation_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Alloc_Process_Env", "CIS-ALLOC-PROCESS-ENV", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_ALLOC_PROCESS_ENV");
        cis_Prtcpnt_File_Cis_Alloc_Effective_Dte = cis_Prtcpnt_File_Cis_Allocation_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Alloc_Effective_Dte", "CIS-ALLOC-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ALLOC_EFFECTIVE_DTE");
        cis_Prtcpnt_File_Count_Castcis_Alloc_Percentage = cis_Prtcpnt_File_Cis_Allocation_Dta.newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Alloc_Percentage", 
            "C*CIS-ALLOC-PERCENTAGE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_ALLOC_PERCENTAGE");
        cis_Prtcpnt_File_Cis_Alloc_PercentageMuGroup = cis_Prtcpnt_File_Cis_Allocation_Dta.newGroupInGroup("cis_Prtcpnt_File_Cis_Alloc_PercentageMuGroup", 
            "CIS_ALLOC_PERCENTAGEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_ALLOC_PERCENTAGE");
        cis_Prtcpnt_File_Cis_Alloc_Percentage = cis_Prtcpnt_File_Cis_Alloc_PercentageMuGroup.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Alloc_Percentage", 
            "CIS-ALLOC-PERCENTAGE", FieldType.NUMERIC, 3, new DbsArrayController(1,20), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CIS_ALLOC_PERCENTAGE");
        cis_Prtcpnt_File_Cis_Alloc_Error_Cde = cis_Prtcpnt_File_Cis_Allocation_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Alloc_Error_Cde", "CIS-ALLOC-ERROR-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_ALLOC_ERROR_CDE");
        cis_Prtcpnt_File_Cis_Alloc_Error_Msg = cis_Prtcpnt_File_Cis_Allocation_Dta.newFieldInGroup("cis_Prtcpnt_File_Cis_Alloc_Error_Msg", "CIS-ALLOC-ERROR-MSG", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "CIS_ALLOC_ERROR_MSG");
        cis_Prtcpnt_File_Cis_Cor_Sync_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Sync_Ind", "CIS-COR-SYNC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_COR_SYNC_IND");
        cis_Prtcpnt_File_Cis_Cor_Process_Env = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Process_Env", "CIS-COR-PROCESS-ENV", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_COR_PROCESS_ENV");
        cis_Prtcpnt_File_Cis_Cor_Ph_Rcd_Typ_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Rcd_Typ_Cde", "CIS-COR-PH-RCD-TYP-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_PH_RCD_TYP_CDE");
        cis_Prtcpnt_File_Cis_Cor_Ph_Neg_Election_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Neg_Election_Cde", 
            "CIS-COR-PH-NEG-ELECTION-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CIS_COR_PH_NEG_ELECTION_CDE");
        cis_Prtcpnt_File_Cis_Cor_Ph_Occupnt_Nme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Occupnt_Nme", "CIS-COR-PH-OCCUPNT-NME", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_COR_PH_OCCUPNT_NME");
        cis_Prtcpnt_File_Cis_Cor_Ph_Xref_Pin = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Xref_Pin", "CIS-COR-PH-XREF-PIN", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_COR_PH_XREF_PIN");
        cis_Prtcpnt_File_Cis_Cor_Ph_Active_Vip_Cnt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Active_Vip_Cnt", "CIS-COR-PH-ACTIVE-VIP-CNT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_PH_ACTIVE_VIP_CNT");
        cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Actve_Vip_Grp = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Actve_Vip_Grp", 
            "C*CIS-COR-PH-ACTVE-VIP-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");
        cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Grp = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Grp", "CIS-COR-PH-ACTVE-VIP-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");
        cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Cde = cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Grp.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Cde", 
            "CIS-COR-PH-ACTVE-VIP-CDE", FieldType.STRING, 2, new DbsArrayController(1,8) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PH_ACTVE_VIP_CDE", 
            "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");
        cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Area_Cde = cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Grp.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Actve_Vip_Area_Cde", 
            "CIS-COR-PH-ACTVE-VIP-AREA-CDE", FieldType.STRING, 3, new DbsArrayController(1,8) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PH_ACTVE_VIP_AREA_CDE", 
            "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");
        cis_Prtcpnt_File_Cis_Cor_Mail_Table_Cnt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Mail_Table_Cnt", "CIS-COR-MAIL-TABLE-CNT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_MAIL_TABLE_CNT");
        cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Mail_Grp = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Cor_Ph_Mail_Grp", 
            "C*CIS-COR-PH-MAIL-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");
        cis_Prtcpnt_File_Cis_Cor_Ph_Mail_Grp = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Mail_Grp", "CIS-COR-PH-MAIL-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");
        cis_Prtcpnt_File_Cis_Cor_Phmail_Cde = cis_Prtcpnt_File_Cis_Cor_Ph_Mail_Grp.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Cor_Phmail_Cde", "CIS-COR-PHMAIL-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,25) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PHMAIL_CDE", "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");
        cis_Prtcpnt_File_Cis_Cor_Phmail_Area_Of_Origin = cis_Prtcpnt_File_Cis_Cor_Ph_Mail_Grp.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Cor_Phmail_Area_Of_Origin", 
            "CIS-COR-PHMAIL-AREA-OF-ORIGIN", FieldType.STRING, 3, new DbsArrayController(1,25) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PHMAIL_AREA_OF_ORIGIN", 
            "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");
        cis_Prtcpnt_File_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte", 
            "CIS-COR-INST-PH-RMTTNG-INSTN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_COR_INST_PH_RMTTNG_INSTN_DTE");
        cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Nbr", 
            "CIS-COR-PH-RMTTNG-INSTN-NBR", FieldType.NUMERIC, 5, RepeatingFieldStrategy.None, "CIS_COR_PH_RMTTNG_INSTN_NBR");
        cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Instn_Ind", 
            "CIS-COR-PH-RMTTNG-INSTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_COR_PH_RMTTNG_INSTN_IND");
        cis_Prtcpnt_File_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind", 
            "CIS-COR-PH-RMMTNG-INSTN-PDUP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_COR_PH_RMMTNG_INSTN_PDUP_IND");
        cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde", 
            "CIS-COR-PH-RMTTNG-NMBR-RANGE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_COR_PH_RMTTNG_NMBR_RANGE_CDE");
        cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Media_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Media_Ind", 
            "CIS-COR-PH-MCRJCK-MEDIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_COR_PH_MCRJCK_MEDIA_IND");
        cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Cntnts_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Ph_Mcrjck_Cntnts_Cde", 
            "CIS-COR-PH-MCRJCK-CNTNTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_COR_PH_MCRJCK_CNTNTS_CDE");
        cis_Prtcpnt_File_Cis_Cor_Cntrct_Table_Cnt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Cntrct_Table_Cnt", "CIS-COR-CNTRCT-TABLE-CNT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_TABLE_CNT");
        cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_STATUS_CDE");
        cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Yr_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Cntrct_Status_Yr_Dte", 
            "CIS-COR-CNTRCT-STATUS-YR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_STATUS_YR_DTE");
        cis_Prtcpnt_File_Cis_Cor_Cntrct_Payee_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_PAYEE_CDE");
        cis_Prtcpnt_File_Cis_Cor_Error_Cde = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Error_Cde", "CIS-COR-ERROR-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_COR_ERROR_CDE");
        cis_Prtcpnt_File_Cis_Cor_Error_Msg = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cor_Error_Msg", "CIS-COR-ERROR-MSG", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "CIS_COR_ERROR_MSG");
        cis_Prtcpnt_File_Cis_Pull_Code = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pull_Code", "CIS-PULL-CODE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CIS_PULL_CODE");
        cis_Prtcpnt_File_Cis_Annual_Required_Dist_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Annual_Required_Dist_Amt", 
            "CIS-ANNUAL-REQUIRED-DIST-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_ANNUAL_REQUIRED_DIST_AMT");
        cis_Prtcpnt_File_Cis_Required_Begin_Date = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Required_Begin_Date", "CIS-REQUIRED-BEGIN-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_REQUIRED_BEGIN_DATE");
        cis_Prtcpnt_File_Cis_Rea_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rea_Nbr", "CIS-REA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_REA_NBR");
        cis_Prtcpnt_File_Cis_Rea_Doi = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rea_Doi", "CIS-REA-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_REA_DOI");
        cis_Prtcpnt_File_Cis_Cref_Issue_State = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cref_Issue_State", "CIS-CREF-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_CREF_ISSUE_STATE");
        cis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type", "CIS-TIAA-CNTRCT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TIAA_CNTRCT_TYPE");
        cis_Prtcpnt_File_Cis_Rea_Cntrct_Type = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rea_Cntrct_Type", "CIS-REA-CNTRCT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_REA_CNTRCT_TYPE");
        cis_Prtcpnt_File_Cis_Cref_Cntrct_Type = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cref_Cntrct_Type", "CIS-CREF-CNTRCT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CREF_CNTRCT_TYPE");
        cis_Prtcpnt_File_Cis_Financial_Data_1 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Financial_Data_1", "CIS-FINANCIAL-DATA-1", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_FINANCIAL_DATA_1");
        cis_Prtcpnt_File_Cis_Financial_Data_2 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Financial_Data_2", "CIS-FINANCIAL-DATA-2", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_FINANCIAL_DATA_2");
        cis_Prtcpnt_File_Cis_Financial_Data_3 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Financial_Data_3", "CIS-FINANCIAL-DATA-3", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_FINANCIAL_DATA_3");
        cis_Prtcpnt_File_Cis_Financial_Data_4 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Financial_Data_4", "CIS-FINANCIAL-DATA-4", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_FINANCIAL_DATA_4");
        cis_Prtcpnt_File_Cis_Financial_Data_5 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Financial_Data_5", "CIS-FINANCIAL-DATA-5", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_FINANCIAL_DATA_5");
        cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info_2 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Count_Castcis_Tiaa_Commuted_Info_2", 
            "C*CIS-TIAA-COMMUTED-INFO-2", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2 = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2", "CIS-TIAA-COMMUTED-INFO-2", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2 = cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2", 
            "CIS-COMUT-GRNTED-AMT-2", FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_Cis_Comut_Int_Rate_2 = cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Comut_Int_Rate_2", 
            "CIS-COMUT-INT-RATE-2", FieldType.NUMERIC, 5, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_Cis_Comut_Pymt_Method_2 = cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Comut_Pymt_Method_2", 
            "CIS-COMUT-PYMT-METHOD-2", FieldType.STRING, 8, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_Cis_Comut_Mortality_Basis = cis_Prtcpnt_File_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Comut_Mortality_Basis", 
            "CIS-COMUT-MORTALITY-BASIS", FieldType.STRING, 30, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_MORTALITY_BASIS", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_Cis_Grnted_Period_Mnts = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Grnted_Period_Mnts", "CIS-GRNTED-PERIOD-MNTS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_MNTS");
        cis_Prtcpnt_File_Cis_Sg_Fund_Identifier = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Sg_Fund_Identifier", "CIS-SG-FUND-IDENTIFIER", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_File_Cis_Sg_Fund_Ticker = cis_Prtcpnt_File_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Sg_Fund_Ticker", "CIS-SG-FUND-TICKER", 
            FieldType.STRING, 10, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_TICKER", "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_File_Cis_Sg_Fund_Allocation = cis_Prtcpnt_File_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Sg_Fund_Allocation", 
            "CIS-SG-FUND-ALLOCATION", FieldType.NUMERIC, 3, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_ALLOCATION", 
            "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_File_Cis_Sg_Fund_Amt = cis_Prtcpnt_File_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Sg_Fund_Amt", "CIS-SG-FUND-AMT", 
            FieldType.NUMERIC, 13, 4, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_AMT", "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_File_Cis_Sg_Text_Udf_1 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Sg_Text_Udf_1", "CIS-SG-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_1");
        cis_Prtcpnt_File_Cis_Sg_Text_Udf_2 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Sg_Text_Udf_2", "CIS-SG-TEXT-UDF-2", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_2");
        cis_Prtcpnt_File_Cis_Sg_Text_Udf_3 = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_3");
        cis_Prtcpnt_File_Cis_Tacc_Annty_Amt = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tacc_Annty_Amt", "CIS-TACC-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_TACC_ANNTY_AMT");
        cis_Prtcpnt_File_Cis_Tacc_Fund_Info = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Tacc_Fund_Info", "CIS-TACC-FUND-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_Cis_Tacc_Ind = cis_Prtcpnt_File_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Tacc_Ind", "CIS-TACC-IND", FieldType.STRING, 
            4, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_IND", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_Cis_Tacc_Account = cis_Prtcpnt_File_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Tacc_Account", "CIS-TACC-ACCOUNT", 
            FieldType.STRING, 10, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ACCOUNT", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_Cis_Tacc_Mnthly_Nbr_Units = cis_Prtcpnt_File_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Tacc_Mnthly_Nbr_Units", 
            "CIS-TACC-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_MNTHLY_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_Cis_Tacc_Annual_Nbr_Units = cis_Prtcpnt_File_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Tacc_Annual_Nbr_Units", 
            "CIS-TACC-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ANNUAL_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_Cis_Institution_Name = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Institution_Name", "CIS-INSTITUTION-NAME", 
            FieldType.STRING, 72, RepeatingFieldStrategy.None, "CIS_INSTITUTION_NAME");
        cis_Prtcpnt_File_Cis_Four_Fifty_Seven_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Four_Fifty_Seven_Ind", "CIS-FOUR-FIFTY-SEVEN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_FOUR_FIFTY_SEVEN_IND");
        cis_Prtcpnt_File_Cis_Trnsf_Info = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Trnsf_Info", "CIS-TRNSF-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_PRTCPNT_FILE_CIS_TRNSF_INFO");
        cis_Prtcpnt_File_Cis_Trnsf_Ticker = cis_Prtcpnt_File_Cis_Trnsf_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Trnsf_Ticker", "CIS-TRNSF-TICKER", 
            FieldType.STRING, 10, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TRNSF_TICKER", "CIS_PRTCPNT_FILE_CIS_TRNSF_INFO");
        cis_Prtcpnt_File_Cis_Trnsf_Reval_Type = cis_Prtcpnt_File_Cis_Trnsf_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Trnsf_Reval_Type", "CIS-TRNSF-REVAL-TYPE", 
            FieldType.STRING, 1, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TRNSF_REVAL_TYPE", "CIS_PRTCPNT_FILE_CIS_TRNSF_INFO");
        cis_Prtcpnt_File_Cis_Trnsf_Cref_Units = cis_Prtcpnt_File_Cis_Trnsf_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Trnsf_Cref_Units", "CIS-TRNSF-CREF-UNITS", 
            FieldType.NUMERIC, 8, 3, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TRNSF_CREF_UNITS", "CIS_PRTCPNT_FILE_CIS_TRNSF_INFO");
        cis_Prtcpnt_File_Cis_Trnsf_Rece_Company = cis_Prtcpnt_File_Cis_Trnsf_Info.newFieldArrayInGroup("cis_Prtcpnt_File_Cis_Trnsf_Rece_Company", "CIS-TRNSF-RECE-COMPANY", 
            FieldType.STRING, 4, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TRNSF_RECE_COMPANY", "CIS_PRTCPNT_FILE_CIS_TRNSF_INFO");
        vw_cis_Prtcpnt_File.setUniquePeList();

        this.setRecordName("LdaIatl406");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cis_Prtcpnt_File.reset();
    }

    // Constructor
    public LdaIatl406() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
