/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:01:56 PM
**        * FROM NATURAL LDA     : IAAL580A
************************************************************
**        * FILE NAME            : LdaIaal580a.java
**        * CLASS NAME           : LdaIaal580a
**        * INSTANCE NAME        : LdaIaal580a
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal580a extends DbsRecord
{
    // Properties
    private DbsField pnd_Page_Ctr;
    private DbsField pnd_Lines_To_Print;
    private DbsField pnd_Lines_Left;
    private DbsField pnd_Other_Lines;
    private DbsField pnd_Flag_02;
    private DbsField pnd_Flag_03_99;
    private DbsField pnd_Count_02;
    private DbsField pnd_Count_03_99;
    private DbsField pnd_Count_Contracts;
    private DbsField pnd_Work_Records_Read;
    private DbsField pnd_Corr_Count;
    private DbsField pnd_Paym_Count;
    private DbsField pnd_Date_Ccyymmdd;
    private DbsGroup pnd_Date_CcyymmddRedef1;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Cc;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Dd;
    private DbsField pnd_Line_1;
    private DbsGroup pnd_Line_1Redef2;
    private DbsField pnd_Line_1_Pnd_Ln1_Operator_Id;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler1;
    private DbsField pnd_Line_1_Pnd_Ln1_Corr_Last_Name;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler2;
    private DbsField pnd_Line_1_Pnd_Ln1_Corr_First_Name;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler3;
    private DbsField pnd_Line_1_Pnd_Ln1_Corr_Middle_Name;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler4;
    private DbsField pnd_Line_1_Pnd_Ln1_Orgn;
    private DbsField pnd_Line_1_Pnd_Ln1_Fillerx;
    private DbsField pnd_Line_1_Pnd_Ln1_Date;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler5;
    private DbsField pnd_Line_1_Pnd_Ln1_Ssn;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler6;
    private DbsField pnd_Line_1_Pnd_Ln1_Gender_Code;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler6a;
    private DbsField pnd_Line_1_Pnd_Ln1_Opt_Cde;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler7;
    private DbsField pnd_Line_1_Pnd_Ln1_Contract;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler8;
    private DbsField pnd_Line_1_Pnd_Ln1_Payee_Code;
    private DbsField pnd_Line_1_Pnd_Ln1_Filler9;
    private DbsField pnd_Line_1_Pnd_Ln1_Payment_Name;
    private DbsField pnd_Line_2;
    private DbsGroup pnd_Line_2Redef3;
    private DbsField pnd_Line_2_Pnd_Ln2_Operator_Unit;
    private DbsField pnd_Line_2_Pnd_Ln2_Filler1;
    private DbsField pnd_Line_2_Pnd_Ln2_Corr_Address;
    private DbsField pnd_Line_2_Pnd_Ln2_Filler2;
    private DbsField pnd_Line_2_Pnd_Ln2_Paym_Address;
    private DbsGroup pnd_Table_Other_Lines;
    private DbsGroup pnd_Table_Other_Lines_Pnd_Table_Group;
    private DbsField pnd_Table_Other_Lines_Pnd_Tab_Filler1;
    private DbsField pnd_Table_Other_Lines_Pnd_Tab_Corr_Address;
    private DbsField pnd_Table_Other_Lines_Pnd_Tab_Filler2;
    private DbsField pnd_Table_Other_Lines_Pnd_Tab_Paym_Address;
    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr;
    private DbsGroup pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4;
    private DbsField pnd_Work_Record_Pnd_Ssn_1_3;
    private DbsField pnd_Work_Record_Pnd_Ssn_4_5;
    private DbsField pnd_Work_Record_Pnd_Ssn_6_9;
    private DbsField pnd_Work_Record_Pnd_Paudit_Payee_Code;
    private DbsField pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr;
    private DbsGroup pnd_Work_Record_Pnd_Paudit_Ppcn_NbrRedef5;
    private DbsField pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_1_7;
    private DbsField pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_8;
    private DbsField pnd_Work_Record_Pnd_Paudit_Dob_Dte;
    private DbsField pnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr;
    private DbsField pnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind;
    private DbsField pnd_Work_Record_Pnd_Paudit_Eft_Transit_Id;
    private DbsField pnd_Work_Record_Pnd_Paudit_Ph_Name_First;
    private DbsField pnd_Work_Record_Pnd_Paudit_Ph_Name_Middle;
    private DbsField pnd_Work_Record_Pnd_Paudit_Ph_Name_Last;
    private DbsField pnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num;
    private DbsGroup pnd_Work_Record_Pnd_Paudit_Name_Address_Grp;
    private DbsField pnd_Work_Record_Pnd_Paudit_Name;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_Lne_1;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_Lne_2;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_Lne_3;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_Lne_4;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_Lne_5;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_Lne_6;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_City;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_State;
    private DbsField pnd_Work_Record_Pnd_Paudit_Addrss_Zip;
    private DbsField pnd_Work_Record_Pnd_Paudit_Oper_Id;
    private DbsField pnd_Work_Record_Pnd_Paudit_Oper_User_Grp;
    private DbsField pnd_Work_Record_Pnd_Paudit_Sex_Code;
    private DbsField pnd_Work_Record_Pnd_Paudit_Type_Ind;
    private DbsField pnd_Work_Record_Pnd_Paudit_Type_Req_Ind;
    private DbsField pnd_Work_Record_Pnd_New_Acct_Id;
    private DbsField pnd_City_State;
    private DbsField pnd_Total_Zip;
    private DbsField pnd_Zip_Breakdown;
    private DbsGroup pnd_Zip_BreakdownRedef6;
    private DbsField pnd_Zip_Breakdown_Pnd_Zip_1_5;
    private DbsField pnd_Zip_Breakdown_Pnd_Zip_6_9;
    private DbsField pnd_I;
    private DbsField pnd_Hold_Variable_1;
    private DbsField pnd_Hold_Variable_2;
    private DbsField pnd_W_Parm_Date;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef7;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsGroup iaa_Cntrct_Cntrct_Optn_CdeRedef8;
    private DbsField iaa_Cntrct_Pnd_Cntrct_Optn_Cde_A;

    public DbsField getPnd_Page_Ctr() { return pnd_Page_Ctr; }

    public DbsField getPnd_Lines_To_Print() { return pnd_Lines_To_Print; }

    public DbsField getPnd_Lines_Left() { return pnd_Lines_Left; }

    public DbsField getPnd_Other_Lines() { return pnd_Other_Lines; }

    public DbsField getPnd_Flag_02() { return pnd_Flag_02; }

    public DbsField getPnd_Flag_03_99() { return pnd_Flag_03_99; }

    public DbsField getPnd_Count_02() { return pnd_Count_02; }

    public DbsField getPnd_Count_03_99() { return pnd_Count_03_99; }

    public DbsField getPnd_Count_Contracts() { return pnd_Count_Contracts; }

    public DbsField getPnd_Work_Records_Read() { return pnd_Work_Records_Read; }

    public DbsField getPnd_Corr_Count() { return pnd_Corr_Count; }

    public DbsField getPnd_Paym_Count() { return pnd_Paym_Count; }

    public DbsField getPnd_Date_Ccyymmdd() { return pnd_Date_Ccyymmdd; }

    public DbsGroup getPnd_Date_CcyymmddRedef1() { return pnd_Date_CcyymmddRedef1; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Cc() { return pnd_Date_Ccyymmdd_Pnd_Date_Cc; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Yy() { return pnd_Date_Ccyymmdd_Pnd_Date_Yy; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Mm() { return pnd_Date_Ccyymmdd_Pnd_Date_Mm; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Dd() { return pnd_Date_Ccyymmdd_Pnd_Date_Dd; }

    public DbsField getPnd_Line_1() { return pnd_Line_1; }

    public DbsGroup getPnd_Line_1Redef2() { return pnd_Line_1Redef2; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Operator_Id() { return pnd_Line_1_Pnd_Ln1_Operator_Id; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler1() { return pnd_Line_1_Pnd_Ln1_Filler1; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Corr_Last_Name() { return pnd_Line_1_Pnd_Ln1_Corr_Last_Name; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler2() { return pnd_Line_1_Pnd_Ln1_Filler2; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Corr_First_Name() { return pnd_Line_1_Pnd_Ln1_Corr_First_Name; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler3() { return pnd_Line_1_Pnd_Ln1_Filler3; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Corr_Middle_Name() { return pnd_Line_1_Pnd_Ln1_Corr_Middle_Name; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler4() { return pnd_Line_1_Pnd_Ln1_Filler4; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Orgn() { return pnd_Line_1_Pnd_Ln1_Orgn; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Fillerx() { return pnd_Line_1_Pnd_Ln1_Fillerx; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Date() { return pnd_Line_1_Pnd_Ln1_Date; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler5() { return pnd_Line_1_Pnd_Ln1_Filler5; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Ssn() { return pnd_Line_1_Pnd_Ln1_Ssn; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler6() { return pnd_Line_1_Pnd_Ln1_Filler6; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Gender_Code() { return pnd_Line_1_Pnd_Ln1_Gender_Code; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler6a() { return pnd_Line_1_Pnd_Ln1_Filler6a; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Opt_Cde() { return pnd_Line_1_Pnd_Ln1_Opt_Cde; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler7() { return pnd_Line_1_Pnd_Ln1_Filler7; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Contract() { return pnd_Line_1_Pnd_Ln1_Contract; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler8() { return pnd_Line_1_Pnd_Ln1_Filler8; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Payee_Code() { return pnd_Line_1_Pnd_Ln1_Payee_Code; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Filler9() { return pnd_Line_1_Pnd_Ln1_Filler9; }

    public DbsField getPnd_Line_1_Pnd_Ln1_Payment_Name() { return pnd_Line_1_Pnd_Ln1_Payment_Name; }

    public DbsField getPnd_Line_2() { return pnd_Line_2; }

    public DbsGroup getPnd_Line_2Redef3() { return pnd_Line_2Redef3; }

    public DbsField getPnd_Line_2_Pnd_Ln2_Operator_Unit() { return pnd_Line_2_Pnd_Ln2_Operator_Unit; }

    public DbsField getPnd_Line_2_Pnd_Ln2_Filler1() { return pnd_Line_2_Pnd_Ln2_Filler1; }

    public DbsField getPnd_Line_2_Pnd_Ln2_Corr_Address() { return pnd_Line_2_Pnd_Ln2_Corr_Address; }

    public DbsField getPnd_Line_2_Pnd_Ln2_Filler2() { return pnd_Line_2_Pnd_Ln2_Filler2; }

    public DbsField getPnd_Line_2_Pnd_Ln2_Paym_Address() { return pnd_Line_2_Pnd_Ln2_Paym_Address; }

    public DbsGroup getPnd_Table_Other_Lines() { return pnd_Table_Other_Lines; }

    public DbsGroup getPnd_Table_Other_Lines_Pnd_Table_Group() { return pnd_Table_Other_Lines_Pnd_Table_Group; }

    public DbsField getPnd_Table_Other_Lines_Pnd_Tab_Filler1() { return pnd_Table_Other_Lines_Pnd_Tab_Filler1; }

    public DbsField getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address() { return pnd_Table_Other_Lines_Pnd_Tab_Corr_Address; }

    public DbsField getPnd_Table_Other_Lines_Pnd_Tab_Filler2() { return pnd_Table_Other_Lines_Pnd_Tab_Filler2; }

    public DbsField getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address() { return pnd_Table_Other_Lines_Pnd_Tab_Paym_Address; }

    public DbsGroup getPnd_Work_Record() { return pnd_Work_Record; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr() { return pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr; }

    public DbsGroup getPnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4() { return pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_1_3() { return pnd_Work_Record_Pnd_Ssn_1_3; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_4_5() { return pnd_Work_Record_Pnd_Ssn_4_5; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_6_9() { return pnd_Work_Record_Pnd_Ssn_6_9; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Payee_Code() { return pnd_Work_Record_Pnd_Paudit_Payee_Code; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr() { return pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr; }

    public DbsGroup getPnd_Work_Record_Pnd_Paudit_Ppcn_NbrRedef5() { return pnd_Work_Record_Pnd_Paudit_Ppcn_NbrRedef5; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_1_7() { return pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_1_7; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_8() { return pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_8; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Dob_Dte() { return pnd_Work_Record_Pnd_Paudit_Dob_Dte; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr() { return pnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind() { return pnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Eft_Transit_Id() { return pnd_Work_Record_Pnd_Paudit_Eft_Transit_Id; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Ph_Name_First() { return pnd_Work_Record_Pnd_Paudit_Ph_Name_First; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Ph_Name_Middle() { return pnd_Work_Record_Pnd_Paudit_Ph_Name_Middle; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Ph_Name_Last() { return pnd_Work_Record_Pnd_Paudit_Ph_Name_Last; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num() { return pnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num; }

    public DbsGroup getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp() { return pnd_Work_Record_Pnd_Paudit_Name_Address_Grp; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Name() { return pnd_Work_Record_Pnd_Paudit_Name; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_1() { return pnd_Work_Record_Pnd_Paudit_Addrss_Lne_1; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2() { return pnd_Work_Record_Pnd_Paudit_Addrss_Lne_2; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3() { return pnd_Work_Record_Pnd_Paudit_Addrss_Lne_3; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4() { return pnd_Work_Record_Pnd_Paudit_Addrss_Lne_4; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_5() { return pnd_Work_Record_Pnd_Paudit_Addrss_Lne_5; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_6() { return pnd_Work_Record_Pnd_Paudit_Addrss_Lne_6; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_City() { return pnd_Work_Record_Pnd_Paudit_Addrss_City; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_State() { return pnd_Work_Record_Pnd_Paudit_Addrss_State; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Addrss_Zip() { return pnd_Work_Record_Pnd_Paudit_Addrss_Zip; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Oper_Id() { return pnd_Work_Record_Pnd_Paudit_Oper_Id; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Oper_User_Grp() { return pnd_Work_Record_Pnd_Paudit_Oper_User_Grp; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Sex_Code() { return pnd_Work_Record_Pnd_Paudit_Sex_Code; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Type_Ind() { return pnd_Work_Record_Pnd_Paudit_Type_Ind; }

    public DbsField getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind() { return pnd_Work_Record_Pnd_Paudit_Type_Req_Ind; }

    public DbsField getPnd_Work_Record_Pnd_New_Acct_Id() { return pnd_Work_Record_Pnd_New_Acct_Id; }

    public DbsField getPnd_City_State() { return pnd_City_State; }

    public DbsField getPnd_Total_Zip() { return pnd_Total_Zip; }

    public DbsField getPnd_Zip_Breakdown() { return pnd_Zip_Breakdown; }

    public DbsGroup getPnd_Zip_BreakdownRedef6() { return pnd_Zip_BreakdownRedef6; }

    public DbsField getPnd_Zip_Breakdown_Pnd_Zip_1_5() { return pnd_Zip_Breakdown_Pnd_Zip_1_5; }

    public DbsField getPnd_Zip_Breakdown_Pnd_Zip_6_9() { return pnd_Zip_Breakdown_Pnd_Zip_6_9; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_Hold_Variable_1() { return pnd_Hold_Variable_1; }

    public DbsField getPnd_Hold_Variable_2() { return pnd_Hold_Variable_2; }

    public DbsField getPnd_W_Parm_Date() { return pnd_W_Parm_Date; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef7() { return iaa_Parm_Card_Pnd_Parm_DateRedef7; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsGroup getIaa_Cntrct_Cntrct_Optn_CdeRedef8() { return iaa_Cntrct_Cntrct_Optn_CdeRedef8; }

    public DbsField getIaa_Cntrct_Pnd_Cntrct_Optn_Cde_A() { return iaa_Cntrct_Pnd_Cntrct_Optn_Cde_A; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Page_Ctr = newFieldInRecord("pnd_Page_Ctr", "#PAGE-CTR", FieldType.NUMERIC, 4);

        pnd_Lines_To_Print = newFieldInRecord("pnd_Lines_To_Print", "#LINES-TO-PRINT", FieldType.NUMERIC, 2);

        pnd_Lines_Left = newFieldInRecord("pnd_Lines_Left", "#LINES-LEFT", FieldType.NUMERIC, 2);

        pnd_Other_Lines = newFieldInRecord("pnd_Other_Lines", "#OTHER-LINES", FieldType.NUMERIC, 2);

        pnd_Flag_02 = newFieldInRecord("pnd_Flag_02", "#FLAG-02", FieldType.STRING, 1);

        pnd_Flag_03_99 = newFieldInRecord("pnd_Flag_03_99", "#FLAG-03-99", FieldType.STRING, 1);

        pnd_Count_02 = newFieldInRecord("pnd_Count_02", "#COUNT-02", FieldType.NUMERIC, 4);

        pnd_Count_03_99 = newFieldInRecord("pnd_Count_03_99", "#COUNT-03-99", FieldType.NUMERIC, 4);

        pnd_Count_Contracts = newFieldInRecord("pnd_Count_Contracts", "#COUNT-CONTRACTS", FieldType.NUMERIC, 4);

        pnd_Work_Records_Read = newFieldInRecord("pnd_Work_Records_Read", "#WORK-RECORDS-READ", FieldType.NUMERIC, 4);

        pnd_Corr_Count = newFieldInRecord("pnd_Corr_Count", "#CORR-COUNT", FieldType.NUMERIC, 1);

        pnd_Paym_Count = newFieldInRecord("pnd_Paym_Count", "#PAYM-COUNT", FieldType.NUMERIC, 1);

        pnd_Date_Ccyymmdd = newFieldInRecord("pnd_Date_Ccyymmdd", "#DATE-CCYYMMDD", FieldType.STRING, 8);
        pnd_Date_CcyymmddRedef1 = newGroupInRecord("pnd_Date_CcyymmddRedef1", "Redefines", pnd_Date_Ccyymmdd);
        pnd_Date_Ccyymmdd_Pnd_Date_Cc = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Cc", "#DATE-CC", FieldType.STRING, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Yy = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.STRING, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Mm = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.STRING, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Dd = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.STRING, 2);

        pnd_Line_1 = newFieldInRecord("pnd_Line_1", "#LINE-1", FieldType.STRING, 132);
        pnd_Line_1Redef2 = newGroupInRecord("pnd_Line_1Redef2", "Redefines", pnd_Line_1);
        pnd_Line_1_Pnd_Ln1_Operator_Id = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Operator_Id", "#LN1-OPERATOR-ID", FieldType.STRING, 8);
        pnd_Line_1_Pnd_Ln1_Filler1 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler1", "#LN1-FILLER1", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Corr_Last_Name = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Corr_Last_Name", "#LN1-CORR-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Line_1_Pnd_Ln1_Filler2 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler2", "#LN1-FILLER2", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Corr_First_Name = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Corr_First_Name", "#LN1-CORR-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Line_1_Pnd_Ln1_Filler3 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler3", "#LN1-FILLER3", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Corr_Middle_Name = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Corr_Middle_Name", "#LN1-CORR-MIDDLE-NAME", FieldType.STRING, 
            2);
        pnd_Line_1_Pnd_Ln1_Filler4 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler4", "#LN1-FILLER4", FieldType.STRING, 8);
        pnd_Line_1_Pnd_Ln1_Orgn = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Orgn", "#LN1-ORGN", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Fillerx = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Fillerx", "#LN1-FILLERX", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Date = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Date", "#LN1-DATE", FieldType.STRING, 8);
        pnd_Line_1_Pnd_Ln1_Filler5 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler5", "#LN1-FILLER5", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Ssn = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Ssn", "#LN1-SSN", FieldType.STRING, 11);
        pnd_Line_1_Pnd_Ln1_Filler6 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler6", "#LN1-FILLER6", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Gender_Code = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Gender_Code", "#LN1-GENDER-CODE", FieldType.STRING, 1);
        pnd_Line_1_Pnd_Ln1_Filler6a = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler6a", "#LN1-FILLER6A", FieldType.STRING, 1);
        pnd_Line_1_Pnd_Ln1_Opt_Cde = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Opt_Cde", "#LN1-OPT-CDE", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Filler7 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler7", "#LN1-FILLER7", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Contract = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Contract", "#LN1-CONTRACT", FieldType.STRING, 9);
        pnd_Line_1_Pnd_Ln1_Filler8 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler8", "#LN1-FILLER8", FieldType.STRING, 1);
        pnd_Line_1_Pnd_Ln1_Payee_Code = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Payee_Code", "#LN1-PAYEE-CODE", FieldType.NUMERIC, 2);
        pnd_Line_1_Pnd_Ln1_Filler9 = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Filler9", "#LN1-FILLER9", FieldType.STRING, 2);
        pnd_Line_1_Pnd_Ln1_Payment_Name = pnd_Line_1Redef2.newFieldInGroup("pnd_Line_1_Pnd_Ln1_Payment_Name", "#LN1-PAYMENT-NAME", FieldType.STRING, 35);

        pnd_Line_2 = newFieldInRecord("pnd_Line_2", "#LINE-2", FieldType.STRING, 132);
        pnd_Line_2Redef3 = newGroupInRecord("pnd_Line_2Redef3", "Redefines", pnd_Line_2);
        pnd_Line_2_Pnd_Ln2_Operator_Unit = pnd_Line_2Redef3.newFieldInGroup("pnd_Line_2_Pnd_Ln2_Operator_Unit", "#LN2-OPERATOR-UNIT", FieldType.STRING, 
            6);
        pnd_Line_2_Pnd_Ln2_Filler1 = pnd_Line_2Redef3.newFieldInGroup("pnd_Line_2_Pnd_Ln2_Filler1", "#LN2-FILLER1", FieldType.STRING, 4);
        pnd_Line_2_Pnd_Ln2_Corr_Address = pnd_Line_2Redef3.newFieldInGroup("pnd_Line_2_Pnd_Ln2_Corr_Address", "#LN2-CORR-ADDRESS", FieldType.STRING, 35);
        pnd_Line_2_Pnd_Ln2_Filler2 = pnd_Line_2Redef3.newFieldInGroup("pnd_Line_2_Pnd_Ln2_Filler2", "#LN2-FILLER2", FieldType.STRING, 52);
        pnd_Line_2_Pnd_Ln2_Paym_Address = pnd_Line_2Redef3.newFieldInGroup("pnd_Line_2_Pnd_Ln2_Paym_Address", "#LN2-PAYM-ADDRESS", FieldType.STRING, 35);

        pnd_Table_Other_Lines = newGroupInRecord("pnd_Table_Other_Lines", "#TABLE-OTHER-LINES");
        pnd_Table_Other_Lines_Pnd_Table_Group = pnd_Table_Other_Lines.newGroupArrayInGroup("pnd_Table_Other_Lines_Pnd_Table_Group", "#TABLE-GROUP", new 
            DbsArrayController(1,9));
        pnd_Table_Other_Lines_Pnd_Tab_Filler1 = pnd_Table_Other_Lines_Pnd_Table_Group.newFieldInGroup("pnd_Table_Other_Lines_Pnd_Tab_Filler1", "#TAB-FILLER1", 
            FieldType.STRING, 9);
        pnd_Table_Other_Lines_Pnd_Tab_Corr_Address = pnd_Table_Other_Lines_Pnd_Table_Group.newFieldInGroup("pnd_Table_Other_Lines_Pnd_Tab_Corr_Address", 
            "#TAB-CORR-ADDRESS", FieldType.STRING, 35);
        pnd_Table_Other_Lines_Pnd_Tab_Filler2 = pnd_Table_Other_Lines_Pnd_Table_Group.newFieldInGroup("pnd_Table_Other_Lines_Pnd_Tab_Filler2", "#TAB-FILLER2", 
            FieldType.STRING, 50);
        pnd_Table_Other_Lines_Pnd_Tab_Paym_Address = pnd_Table_Other_Lines_Pnd_Table_Group.newFieldInGroup("pnd_Table_Other_Lines_Pnd_Tab_Paym_Address", 
            "#TAB-PAYM-ADDRESS", FieldType.STRING, 35);

        pnd_Work_Record = newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr", "#PAUDIT-PYEE-TAX-ID-NBR", 
            FieldType.STRING, 9);
        pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4", "Redefines", 
            pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr);
        pnd_Work_Record_Pnd_Ssn_1_3 = pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_1_3", "#SSN-1-3", FieldType.STRING, 
            3);
        pnd_Work_Record_Pnd_Ssn_4_5 = pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_4_5", "#SSN-4-5", FieldType.STRING, 
            2);
        pnd_Work_Record_Pnd_Ssn_6_9 = pnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrRedef4.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_6_9", "#SSN-6-9", FieldType.STRING, 
            4);
        pnd_Work_Record_Pnd_Paudit_Payee_Code = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Payee_Code", "#PAUDIT-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr", "#PAUDIT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Record_Pnd_Paudit_Ppcn_NbrRedef5 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_Paudit_Ppcn_NbrRedef5", "Redefines", pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr);
        pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_1_7 = pnd_Work_Record_Pnd_Paudit_Ppcn_NbrRedef5.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_1_7", 
            "#PAUDIT-PPCN-NBR-1-7", FieldType.STRING, 7);
        pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_8 = pnd_Work_Record_Pnd_Paudit_Ppcn_NbrRedef5.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_8", "#PAUDIT-PPCN-NBR-8", 
            FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Paudit_Dob_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Dob_Dte", "#PAUDIT-DOB-DTE", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr", "#PAUDIT-EFT-ACCT-NBR", FieldType.STRING, 
            21);
        pnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind", "#PAUDIT-CHK-SAV-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Paudit_Eft_Transit_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Eft_Transit_Id", "#PAUDIT-EFT-TRANSIT-ID", 
            FieldType.STRING, 9);
        pnd_Work_Record_Pnd_Paudit_Ph_Name_First = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Ph_Name_First", "#PAUDIT-PH-NAME-FIRST", 
            FieldType.STRING, 30);
        pnd_Work_Record_Pnd_Paudit_Ph_Name_Middle = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Ph_Name_Middle", "#PAUDIT-PH-NAME-MIDDLE", 
            FieldType.STRING, 30);
        pnd_Work_Record_Pnd_Paudit_Ph_Name_Last = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Ph_Name_Last", "#PAUDIT-PH-NAME-LAST", FieldType.STRING, 
            30);
        pnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num", "#PAUDIT-NAME-ADDRESS-GRP-NUM", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Paudit_Name_Address_Grp = pnd_Work_Record.newGroupArrayInGroup("pnd_Work_Record_Pnd_Paudit_Name_Address_Grp", "#PAUDIT-NAME-ADDRESS-GRP", 
            new DbsArrayController(1,2));
        pnd_Work_Record_Pnd_Paudit_Name = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Name", "#PAUDIT-NAME", 
            FieldType.STRING, 35);
        pnd_Work_Record_Pnd_Paudit_Addrss_Lne_1 = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_Lne_1", 
            "#PAUDIT-ADDRSS-LNE-1", FieldType.STRING, 35);
        pnd_Work_Record_Pnd_Paudit_Addrss_Lne_2 = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_Lne_2", 
            "#PAUDIT-ADDRSS-LNE-2", FieldType.STRING, 35);
        pnd_Work_Record_Pnd_Paudit_Addrss_Lne_3 = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_Lne_3", 
            "#PAUDIT-ADDRSS-LNE-3", FieldType.STRING, 35);
        pnd_Work_Record_Pnd_Paudit_Addrss_Lne_4 = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_Lne_4", 
            "#PAUDIT-ADDRSS-LNE-4", FieldType.STRING, 35);
        pnd_Work_Record_Pnd_Paudit_Addrss_Lne_5 = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_Lne_5", 
            "#PAUDIT-ADDRSS-LNE-5", FieldType.STRING, 35);
        pnd_Work_Record_Pnd_Paudit_Addrss_Lne_6 = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_Lne_6", 
            "#PAUDIT-ADDRSS-LNE-6", FieldType.STRING, 35);
        pnd_Work_Record_Pnd_Paudit_Addrss_City = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_City", 
            "#PAUDIT-ADDRSS-CITY", FieldType.STRING, 21);
        pnd_Work_Record_Pnd_Paudit_Addrss_State = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_State", 
            "#PAUDIT-ADDRSS-STATE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Paudit_Addrss_Zip = pnd_Work_Record_Pnd_Paudit_Name_Address_Grp.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Addrss_Zip", "#PAUDIT-ADDRSS-ZIP", 
            FieldType.STRING, 9);
        pnd_Work_Record_Pnd_Paudit_Oper_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Oper_Id", "#PAUDIT-OPER-ID", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Paudit_Oper_User_Grp = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Oper_User_Grp", "#PAUDIT-OPER-USER-GRP", 
            FieldType.STRING, 6);
        pnd_Work_Record_Pnd_Paudit_Sex_Code = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Sex_Code", "#PAUDIT-SEX-CODE", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Paudit_Type_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Type_Ind", "#PAUDIT-TYPE-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Paudit_Type_Req_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Paudit_Type_Req_Ind", "#PAUDIT-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_Pnd_New_Acct_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_New_Acct_Id", "#NEW-ACCT-ID", FieldType.STRING, 1);

        pnd_City_State = newFieldInRecord("pnd_City_State", "#CITY-STATE", FieldType.STRING, 24);

        pnd_Total_Zip = newFieldInRecord("pnd_Total_Zip", "#TOTAL-ZIP", FieldType.STRING, 10);

        pnd_Zip_Breakdown = newFieldInRecord("pnd_Zip_Breakdown", "#ZIP-BREAKDOWN", FieldType.STRING, 9);
        pnd_Zip_BreakdownRedef6 = newGroupInRecord("pnd_Zip_BreakdownRedef6", "Redefines", pnd_Zip_Breakdown);
        pnd_Zip_Breakdown_Pnd_Zip_1_5 = pnd_Zip_BreakdownRedef6.newFieldInGroup("pnd_Zip_Breakdown_Pnd_Zip_1_5", "#ZIP-1-5", FieldType.STRING, 5);
        pnd_Zip_Breakdown_Pnd_Zip_6_9 = pnd_Zip_BreakdownRedef6.newFieldInGroup("pnd_Zip_Breakdown_Pnd_Zip_6_9", "#ZIP-6-9", FieldType.STRING, 4);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        pnd_Hold_Variable_1 = newFieldInRecord("pnd_Hold_Variable_1", "#HOLD-VARIABLE-1", FieldType.STRING, 14);

        pnd_Hold_Variable_2 = newFieldInRecord("pnd_Hold_Variable_2", "#HOLD-VARIABLE-2", FieldType.STRING, 6);

        pnd_W_Parm_Date = newFieldInRecord("pnd_W_Parm_Date", "#W-PARM-DATE", FieldType.STRING, 8);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef7 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef7", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_DateRedef7.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.STRING, 
            2);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Optn_CdeRedef8 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Optn_CdeRedef8", "Redefines", iaa_Cntrct_Cntrct_Optn_Cde);
        iaa_Cntrct_Pnd_Cntrct_Optn_Cde_A = iaa_Cntrct_Cntrct_Optn_CdeRedef8.newFieldInGroup("iaa_Cntrct_Pnd_Cntrct_Optn_Cde_A", "#CNTRCT-OPTN-CDE-A", 
            FieldType.STRING, 2);

        this.setRecordName("LdaIaal580a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct.reset();
    }

    // Constructor
    public LdaIaal580a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
