/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:48:48 PM
**        * FROM NATURAL GDA     : IAAG588R
************************************************************
**        * FILE NAME            : GdaIaag588r.java
**        * CLASS NAME           : GdaIaag588r
**        * INSTANCE NAME        : GdaIaag588r
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaIaag588r extends DbsRecord
{
    private static ThreadLocal<List<GdaIaag588r>> _instance = new ThreadLocal<List<GdaIaag588r>>();

    // Properties
    private DbsGroup pnd_Work_Record2;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Id_Nbr;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Tax_Id_Nbr;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Payee_Cde;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Req_Seq_Nbr;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Timestamp;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Status_Cde;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Status_Timestamp;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Nbr;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Typ;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Dob_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Dod_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Proof_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Lst_Pymnt_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Notify_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Cycle_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Acctg_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Pymnt_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Type_Req_Ind;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Type_Ind;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Roll_Dest_Cde;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Eft_Acct_Nbr;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Eft_Transit_Id;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Chk_Sav_Ind;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Rate_Basis;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Per_Pymnt;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Per_Dvdnd;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Per_Units;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnts;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dte;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Typ;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Guar;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Divd;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ivc;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Units;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Instllmnt_Futr_Ind;
    private DbsField pnd_Work_Record2_Pnd_Wk2_New_Acct_Ind;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Ctznshp_Cde;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Rsdncy_Cde;
    private DbsField pnd_Work_Record2_Pnd_Wk2_Locality_Cde;
    private DbsField pnd_Table_Guar;
    private DbsField pnd_Table_Divd;
    private DbsField pnd_Table_Dci;
    private DbsField pnd_Table_Ovrpy;

    public DbsGroup getPnd_Work_Record2() { return pnd_Work_Record2; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Id_Nbr() { return pnd_Work_Record2_Pnd_Wk2_Id_Nbr; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Tax_Id_Nbr() { return pnd_Work_Record2_Pnd_Wk2_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr() { return pnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Payee_Cde() { return pnd_Work_Record2_Pnd_Wk2_Payee_Cde; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Req_Seq_Nbr() { return pnd_Work_Record2_Pnd_Wk2_Req_Seq_Nbr; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Timestamp() { return pnd_Work_Record2_Pnd_Wk2_Timestamp; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Status_Cde() { return pnd_Work_Record2_Pnd_Wk2_Status_Cde; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Status_Timestamp() { return pnd_Work_Record2_Pnd_Wk2_Status_Timestamp; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Nbr() { return pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Typ() { return pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Typ; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Dob_Dte() { return pnd_Work_Record2_Pnd_Wk2_Dob_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Dod_Dte() { return pnd_Work_Record2_Pnd_Wk2_Dod_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Proof_Dte() { return pnd_Work_Record2_Pnd_Wk2_Proof_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Lst_Pymnt_Dte() { return pnd_Work_Record2_Pnd_Wk2_Lst_Pymnt_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Notify_Dte() { return pnd_Work_Record2_Pnd_Wk2_Notify_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Cycle_Dte() { return pnd_Work_Record2_Pnd_Wk2_Cycle_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Acctg_Dte() { return pnd_Work_Record2_Pnd_Wk2_Acctg_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte() { return pnd_Work_Record2_Pnd_Wk2_Pymnt_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Type_Req_Ind() { return pnd_Work_Record2_Pnd_Wk2_Type_Req_Ind; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Type_Ind() { return pnd_Work_Record2_Pnd_Wk2_Type_Ind; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Roll_Dest_Cde() { return pnd_Work_Record2_Pnd_Wk2_Roll_Dest_Cde; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Eft_Acct_Nbr() { return pnd_Work_Record2_Pnd_Wk2_Eft_Acct_Nbr; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Eft_Transit_Id() { return pnd_Work_Record2_Pnd_Wk2_Eft_Transit_Id; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Chk_Sav_Ind() { return pnd_Work_Record2_Pnd_Wk2_Chk_Sav_Ind; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Rate_Basis() { return pnd_Work_Record2_Pnd_Wk2_Rate_Basis; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt() { return pnd_Work_Record2_Pnd_Wk2_Per_Pymnt; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Per_Dvdnd() { return pnd_Work_Record2_Pnd_Wk2_Per_Dvdnd; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Per_Units() { return pnd_Work_Record2_Pnd_Wk2_Per_Units; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnts() { return pnd_Work_Record2_Pnd_Wk2_Instllmnts; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dte() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dte; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Typ() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Typ; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Guar() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Guar; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Divd() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Divd; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Ivc() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ivc; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Units() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Units; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Futr_Ind() { return pnd_Work_Record2_Pnd_Wk2_Instllmnt_Futr_Ind; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_New_Acct_Ind() { return pnd_Work_Record2_Pnd_Wk2_New_Acct_Ind; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Ctznshp_Cde() { return pnd_Work_Record2_Pnd_Wk2_Ctznshp_Cde; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Rsdncy_Cde() { return pnd_Work_Record2_Pnd_Wk2_Rsdncy_Cde; }

    public DbsField getPnd_Work_Record2_Pnd_Wk2_Locality_Cde() { return pnd_Work_Record2_Pnd_Wk2_Locality_Cde; }

    public DbsField getPnd_Table_Guar() { return pnd_Table_Guar; }

    public DbsField getPnd_Table_Divd() { return pnd_Table_Divd; }

    public DbsField getPnd_Table_Dci() { return pnd_Table_Dci; }

    public DbsField getPnd_Table_Ovrpy() { return pnd_Table_Ovrpy; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Work_Record2 = newGroupInRecord("pnd_Work_Record2", "#WORK-RECORD2");
        pnd_Work_Record2_Pnd_Wk2_Id_Nbr = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Id_Nbr", "#WK2-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Work_Record2_Pnd_Wk2_Tax_Id_Nbr = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Tax_Id_Nbr", "#WK2-TAX-ID-NBR", FieldType.NUMERIC, 
            9);
        pnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr", "#WK2-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record2_Pnd_Wk2_Payee_Cde = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Payee_Cde", "#WK2-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record2_Pnd_Wk2_Req_Seq_Nbr = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Req_Seq_Nbr", "#WK2-REQ-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Work_Record2_Pnd_Wk2_Timestamp = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Timestamp", "#WK2-TIMESTAMP", FieldType.STRING, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Status_Cde = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Status_Cde", "#WK2-STATUS-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record2_Pnd_Wk2_Status_Timestamp = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Status_Timestamp", "#WK2-STATUS-TIMESTAMP", 
            FieldType.STRING, 8);
        pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Nbr = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Nbr", "#WK2-PYEE-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Typ = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Typ", "#WK2-PYEE-TAX-ID-TYP", 
            FieldType.STRING, 1);
        pnd_Work_Record2_Pnd_Wk2_Dob_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Dob_Dte", "#WK2-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record2_Pnd_Wk2_Dod_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Dod_Dte", "#WK2-DOD-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record2_Pnd_Wk2_Proof_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Proof_Dte", "#WK2-PROOF-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Lst_Pymnt_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Lst_Pymnt_Dte", "#WK2-LST-PYMNT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Notify_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Notify_Dte", "#WK2-NOTIFY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Cycle_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Cycle_Dte", "#WK2-CYCLE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Acctg_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Acctg_Dte", "#WK2-ACCTG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Pymnt_Dte = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Pymnt_Dte", "#WK2-PYMNT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Type_Req_Ind = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Type_Req_Ind", "#WK2-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        pnd_Work_Record2_Pnd_Wk2_Type_Ind = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Type_Ind", "#WK2-TYPE-IND", FieldType.STRING, 1);
        pnd_Work_Record2_Pnd_Wk2_Roll_Dest_Cde = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Roll_Dest_Cde", "#WK2-ROLL-DEST-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record2_Pnd_Wk2_Eft_Acct_Nbr = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Eft_Acct_Nbr", "#WK2-EFT-ACCT-NBR", FieldType.STRING, 
            21);
        pnd_Work_Record2_Pnd_Wk2_Eft_Transit_Id = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Eft_Transit_Id", "#WK2-EFT-TRANSIT-ID", FieldType.NUMERIC, 
            9);
        pnd_Work_Record2_Pnd_Wk2_Chk_Sav_Ind = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Chk_Sav_Ind", "#WK2-CHK-SAV-IND", FieldType.STRING, 
            1);
        pnd_Work_Record2_Pnd_Wk2_Rate_Basis = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Rate_Basis", "#WK2-RATE-BASIS", FieldType.NUMERIC, 
            2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Per_Pymnt = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Per_Pymnt", "#WK2-PER-PYMNT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Per_Dvdnd = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Per_Dvdnd", "#WK2-PER-DVDND", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Per_Units = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Per_Units", "#WK2-PER-UNITS", FieldType.DECIMAL, 
            11,4, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnts = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnts", "#WK2-INSTLLMNTS", FieldType.NUMERIC, 
            8);
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dte = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dte", "#WK2-INSTLLMNT-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Typ = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Typ", "#WK2-INSTLLMNT-TYP", 
            FieldType.STRING, 4, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross", "#WK2-INSTLLMNT-GROSS", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Guar = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Guar", "#WK2-INSTLLMNT-GUAR", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Divd = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Divd", "#WK2-INSTLLMNT-DIVD", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ivc = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ivc", "#WK2-INSTLLMNT-IVC", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci", "#WK2-INSTLLMNT-DCI", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Units = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Units", "#WK2-INSTLLMNT-UNITS", 
            FieldType.DECIMAL, 11,4, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt", "#WK2-INSTLLMNT-OVRPYMNT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_Instllmnt_Futr_Ind = pnd_Work_Record2.newFieldArrayInGroup("pnd_Work_Record2_Pnd_Wk2_Instllmnt_Futr_Ind", "#WK2-INSTLLMNT-FUTR-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,99));
        pnd_Work_Record2_Pnd_Wk2_New_Acct_Ind = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_New_Acct_Ind", "#WK2-NEW-ACCT-IND", FieldType.STRING, 
            1);
        pnd_Work_Record2_Pnd_Wk2_Ctznshp_Cde = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Ctznshp_Cde", "#WK2-CTZNSHP-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record2_Pnd_Wk2_Rsdncy_Cde = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Rsdncy_Cde", "#WK2-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record2_Pnd_Wk2_Locality_Cde = pnd_Work_Record2.newFieldInGroup("pnd_Work_Record2_Pnd_Wk2_Locality_Cde", "#WK2-LOCALITY-CDE", FieldType.STRING, 
            2);

        pnd_Table_Guar = newFieldArrayInRecord("pnd_Table_Guar", "#TABLE-GUAR", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,99));

        pnd_Table_Divd = newFieldArrayInRecord("pnd_Table_Divd", "#TABLE-DIVD", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,99));

        pnd_Table_Dci = newFieldArrayInRecord("pnd_Table_Dci", "#TABLE-DCI", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,99));

        pnd_Table_Ovrpy = newFieldArrayInRecord("pnd_Table_Ovrpy", "#TABLE-OVRPY", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,99));

        this.setRecordName("GdaIaag588r");
    }
    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    private GdaIaag588r() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaIaag588r getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaIaag588r>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaIaag588r());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

