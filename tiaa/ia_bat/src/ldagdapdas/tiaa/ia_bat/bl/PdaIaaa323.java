/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:39 PM
**        * FROM NATURAL PDA     : IAAA323
************************************************************
**        * FILE NAME            : PdaIaaa323.java
**        * CLASS NAME           : PdaIaaa323
**        * INSTANCE NAME        : PdaIaaa323
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIaaa323 extends PdaBase
{
    // Properties
    private DbsGroup iaaa320;
    private DbsField iaaa320_Pin_Nbr;
    private DbsField iaaa320_Contract_Nbr;
    private DbsField iaaa320_Payee_Cde;
    private DbsField iaaa320_Ssn_Nbr;
    private DbsField iaaa320_Zip_Cde;
    private DbsField iaaa320_Company_Cde;
    private DbsField iaaa320_Mailing_Label_Ind;
    private DbsField iaaa320_Hold_Ind;
    private DbsField iaaa320_Hold_Cde;
    private DbsField iaaa320_Hold_Grp_Cde;
    private DbsField iaaa320_Mode_Cde;
    private DbsField iaaa320_Off_Mode_Ind;
    private DbsField iaaa320_Transfer_Ind;
    private DbsField iaaa320_Combined_Pymnt_Ind;
    private DbsField iaaa320_Gtn_Return_Cde;
    private DbsField iaaa320_Gtn_Report_Cde;
    private DbsField iaaa320_Prior_State_Rsdncy_Cde;
    private DbsField iaaa320_Curr_State_Rsdncy_Cde;
    private DbsField iaaa320_State_Rsdncy_Chg_Flag;
    private DbsField iaaa320_Ph_Last_Nme;
    private DbsField iaaa320_Ph_First_Nme;
    private DbsField iaaa320_Ph_Middle_Nme;
    private DbsField iaaa320_Ph_Mailing_Nme;
    private DbsField iaaa320_Ph_Address_Line_1;
    private DbsField iaaa320_Ph_Address_Line_2;
    private DbsField iaaa320_Ph_Address_Line_3;
    private DbsField iaaa320_Ph_Address_Line_4;
    private DbsField iaaa320_Ph_Address_Line_5;
    private DbsField iaaa320_Ph_Address_Line_6;
    private DbsField iaaa320_Postal_Data;
    private DbsGroup iaaa320_Postal_DataRedef1;
    private DbsField iaaa320_Zip_Code_5;
    private DbsField iaaa320_Zip_Code_4;
    private DbsField iaaa320_Carr_Walk_Rte;
    private DbsField iaaa320_Prior_Check_Dte;
    private DbsField iaaa320_Curr_Check_Dte;
    private DbsField iaaa320_Prior_Cycle_Dte;
    private DbsField iaaa320_Curr_Cycle_Dte;
    private DbsField iaaa320_Future_Pymnt_Dte;
    private DbsField iaaa320_Cntrct_Annty_Ins_Type;
    private DbsField iaaa320_Cntrct_Annty_Type_Cde;
    private DbsField iaaa320_Cntrct_Insurance_Option;
    private DbsField iaaa320_Cntrct_Life_Contingency;
    private DbsField iaaa320_Cntrct_Option_Cde;
    private DbsField filler01;
    private DbsField iaaa320_Deduction_Cnt;
    private DbsGroup iaaa320_Deductions;
    private DbsField iaaa320_Ded_Detail;
    private DbsGroup iaaa320_Ded_DetailRedef2;
    private DbsField iaaa320_Ded_Cde;
    private DbsGroup iaaa320_Ded_CdeRedef3;
    private DbsField iaaa320_Ded_Cde_A;
    private DbsField iaaa320_Ded_Prior_Amt;
    private DbsField iaaa320_Ded_Curr_Amt;
    private DbsField iaaa320_Ded_Amt_Chg_Flag;
    private DbsField iaaa320_Tax_Prior_Fed_Amt;
    private DbsField iaaa320_Tax_Prior_St_Amt;
    private DbsField iaaa320_Tax_Prior_Loc_Amt;
    private DbsField iaaa320_Tax_Curr_Fed_Amt;
    private DbsField iaaa320_Tax_Curr_St_Amt;
    private DbsField iaaa320_Tax_Curr_Loc_Amt;
    private DbsField iaaa320_Tax_Fed_Chg_Flag;
    private DbsField iaaa320_Tax_St_Chg_Flag;
    private DbsField iaaa320_Tax_Loc_Chg_Flag;
    private DbsField iaaa320_Fund_Cnt;
    private DbsGroup iaaa320_Fund_Grp;
    private DbsField iaaa320_Fund_Detail;
    private DbsGroup iaaa320_Fund_DetailRedef4;
    private DbsField iaaa320_Fund_Valuat_Fund;
    private DbsGroup iaaa320_Fund_Valuat_FundRedef5;
    private DbsField iaaa320_Fund_Valuat_Period;
    private DbsField iaaa320_Fund_Account_Cde_A;
    private DbsField iaaa320_Fund_Account_Cde_N;
    private DbsField iaaa320_Fund_Prior_Unit_Qty;
    private DbsField iaaa320_Fund_Prior_Unit_Value;
    private DbsField iaaa320_Fund_Prior_Settlement_Amt;
    private DbsField iaaa320_Fund_Prior_Contract_Amt;
    private DbsField iaaa320_Fund_Prior_Dividend_Amt;
    private DbsField iaaa320_Fund_Prior_Net_Pymnt_Amt;
    private DbsField iaaa320_Fund_Curr_Unit_Qty;
    private DbsField iaaa320_Fund_Curr_Unit_Value;
    private DbsField iaaa320_Fund_Curr_Settlement_Amt;
    private DbsField iaaa320_Fund_Curr_Contract_Amt;
    private DbsField iaaa320_Fund_Curr_Dividend_Amt;
    private DbsField iaaa320_Fund_Curr_Net_Pymnt_Amt;
    private DbsField iaaa320_Fund_Unit_Qty_Chg_Flag;
    private DbsField iaaa320_Fund_Unit_Value_Chg_Flag;
    private DbsField iaaa320_Fund_Settlement_Chg_Flag;
    private DbsField iaaa320_Fund_Contract_Chg_Flag;
    private DbsField iaaa320_Fund_Dividend_Chg_Flag;
    private DbsField iaaa320_Fund_Net_Pymnt_Chg_Flag;

    public DbsGroup getIaaa320() { return iaaa320; }

    public DbsField getIaaa320_Pin_Nbr() { return iaaa320_Pin_Nbr; }

    public DbsField getIaaa320_Contract_Nbr() { return iaaa320_Contract_Nbr; }

    public DbsField getIaaa320_Payee_Cde() { return iaaa320_Payee_Cde; }

    public DbsField getIaaa320_Ssn_Nbr() { return iaaa320_Ssn_Nbr; }

    public DbsField getIaaa320_Zip_Cde() { return iaaa320_Zip_Cde; }

    public DbsField getIaaa320_Company_Cde() { return iaaa320_Company_Cde; }

    public DbsField getIaaa320_Mailing_Label_Ind() { return iaaa320_Mailing_Label_Ind; }

    public DbsField getIaaa320_Hold_Ind() { return iaaa320_Hold_Ind; }

    public DbsField getIaaa320_Hold_Cde() { return iaaa320_Hold_Cde; }

    public DbsField getIaaa320_Hold_Grp_Cde() { return iaaa320_Hold_Grp_Cde; }

    public DbsField getIaaa320_Mode_Cde() { return iaaa320_Mode_Cde; }

    public DbsField getIaaa320_Off_Mode_Ind() { return iaaa320_Off_Mode_Ind; }

    public DbsField getIaaa320_Transfer_Ind() { return iaaa320_Transfer_Ind; }

    public DbsField getIaaa320_Combined_Pymnt_Ind() { return iaaa320_Combined_Pymnt_Ind; }

    public DbsField getIaaa320_Gtn_Return_Cde() { return iaaa320_Gtn_Return_Cde; }

    public DbsField getIaaa320_Gtn_Report_Cde() { return iaaa320_Gtn_Report_Cde; }

    public DbsField getIaaa320_Prior_State_Rsdncy_Cde() { return iaaa320_Prior_State_Rsdncy_Cde; }

    public DbsField getIaaa320_Curr_State_Rsdncy_Cde() { return iaaa320_Curr_State_Rsdncy_Cde; }

    public DbsField getIaaa320_State_Rsdncy_Chg_Flag() { return iaaa320_State_Rsdncy_Chg_Flag; }

    public DbsField getIaaa320_Ph_Last_Nme() { return iaaa320_Ph_Last_Nme; }

    public DbsField getIaaa320_Ph_First_Nme() { return iaaa320_Ph_First_Nme; }

    public DbsField getIaaa320_Ph_Middle_Nme() { return iaaa320_Ph_Middle_Nme; }

    public DbsField getIaaa320_Ph_Mailing_Nme() { return iaaa320_Ph_Mailing_Nme; }

    public DbsField getIaaa320_Ph_Address_Line_1() { return iaaa320_Ph_Address_Line_1; }

    public DbsField getIaaa320_Ph_Address_Line_2() { return iaaa320_Ph_Address_Line_2; }

    public DbsField getIaaa320_Ph_Address_Line_3() { return iaaa320_Ph_Address_Line_3; }

    public DbsField getIaaa320_Ph_Address_Line_4() { return iaaa320_Ph_Address_Line_4; }

    public DbsField getIaaa320_Ph_Address_Line_5() { return iaaa320_Ph_Address_Line_5; }

    public DbsField getIaaa320_Ph_Address_Line_6() { return iaaa320_Ph_Address_Line_6; }

    public DbsField getIaaa320_Postal_Data() { return iaaa320_Postal_Data; }

    public DbsGroup getIaaa320_Postal_DataRedef1() { return iaaa320_Postal_DataRedef1; }

    public DbsField getIaaa320_Zip_Code_5() { return iaaa320_Zip_Code_5; }

    public DbsField getIaaa320_Zip_Code_4() { return iaaa320_Zip_Code_4; }

    public DbsField getIaaa320_Carr_Walk_Rte() { return iaaa320_Carr_Walk_Rte; }

    public DbsField getIaaa320_Prior_Check_Dte() { return iaaa320_Prior_Check_Dte; }

    public DbsField getIaaa320_Curr_Check_Dte() { return iaaa320_Curr_Check_Dte; }

    public DbsField getIaaa320_Prior_Cycle_Dte() { return iaaa320_Prior_Cycle_Dte; }

    public DbsField getIaaa320_Curr_Cycle_Dte() { return iaaa320_Curr_Cycle_Dte; }

    public DbsField getIaaa320_Future_Pymnt_Dte() { return iaaa320_Future_Pymnt_Dte; }

    public DbsField getIaaa320_Cntrct_Annty_Ins_Type() { return iaaa320_Cntrct_Annty_Ins_Type; }

    public DbsField getIaaa320_Cntrct_Annty_Type_Cde() { return iaaa320_Cntrct_Annty_Type_Cde; }

    public DbsField getIaaa320_Cntrct_Insurance_Option() { return iaaa320_Cntrct_Insurance_Option; }

    public DbsField getIaaa320_Cntrct_Life_Contingency() { return iaaa320_Cntrct_Life_Contingency; }

    public DbsField getIaaa320_Cntrct_Option_Cde() { return iaaa320_Cntrct_Option_Cde; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getIaaa320_Deduction_Cnt() { return iaaa320_Deduction_Cnt; }

    public DbsGroup getIaaa320_Deductions() { return iaaa320_Deductions; }

    public DbsField getIaaa320_Ded_Detail() { return iaaa320_Ded_Detail; }

    public DbsGroup getIaaa320_Ded_DetailRedef2() { return iaaa320_Ded_DetailRedef2; }

    public DbsField getIaaa320_Ded_Cde() { return iaaa320_Ded_Cde; }

    public DbsGroup getIaaa320_Ded_CdeRedef3() { return iaaa320_Ded_CdeRedef3; }

    public DbsField getIaaa320_Ded_Cde_A() { return iaaa320_Ded_Cde_A; }

    public DbsField getIaaa320_Ded_Prior_Amt() { return iaaa320_Ded_Prior_Amt; }

    public DbsField getIaaa320_Ded_Curr_Amt() { return iaaa320_Ded_Curr_Amt; }

    public DbsField getIaaa320_Ded_Amt_Chg_Flag() { return iaaa320_Ded_Amt_Chg_Flag; }

    public DbsField getIaaa320_Tax_Prior_Fed_Amt() { return iaaa320_Tax_Prior_Fed_Amt; }

    public DbsField getIaaa320_Tax_Prior_St_Amt() { return iaaa320_Tax_Prior_St_Amt; }

    public DbsField getIaaa320_Tax_Prior_Loc_Amt() { return iaaa320_Tax_Prior_Loc_Amt; }

    public DbsField getIaaa320_Tax_Curr_Fed_Amt() { return iaaa320_Tax_Curr_Fed_Amt; }

    public DbsField getIaaa320_Tax_Curr_St_Amt() { return iaaa320_Tax_Curr_St_Amt; }

    public DbsField getIaaa320_Tax_Curr_Loc_Amt() { return iaaa320_Tax_Curr_Loc_Amt; }

    public DbsField getIaaa320_Tax_Fed_Chg_Flag() { return iaaa320_Tax_Fed_Chg_Flag; }

    public DbsField getIaaa320_Tax_St_Chg_Flag() { return iaaa320_Tax_St_Chg_Flag; }

    public DbsField getIaaa320_Tax_Loc_Chg_Flag() { return iaaa320_Tax_Loc_Chg_Flag; }

    public DbsField getIaaa320_Fund_Cnt() { return iaaa320_Fund_Cnt; }

    public DbsGroup getIaaa320_Fund_Grp() { return iaaa320_Fund_Grp; }

    public DbsField getIaaa320_Fund_Detail() { return iaaa320_Fund_Detail; }

    public DbsGroup getIaaa320_Fund_DetailRedef4() { return iaaa320_Fund_DetailRedef4; }

    public DbsField getIaaa320_Fund_Valuat_Fund() { return iaaa320_Fund_Valuat_Fund; }

    public DbsGroup getIaaa320_Fund_Valuat_FundRedef5() { return iaaa320_Fund_Valuat_FundRedef5; }

    public DbsField getIaaa320_Fund_Valuat_Period() { return iaaa320_Fund_Valuat_Period; }

    public DbsField getIaaa320_Fund_Account_Cde_A() { return iaaa320_Fund_Account_Cde_A; }

    public DbsField getIaaa320_Fund_Account_Cde_N() { return iaaa320_Fund_Account_Cde_N; }

    public DbsField getIaaa320_Fund_Prior_Unit_Qty() { return iaaa320_Fund_Prior_Unit_Qty; }

    public DbsField getIaaa320_Fund_Prior_Unit_Value() { return iaaa320_Fund_Prior_Unit_Value; }

    public DbsField getIaaa320_Fund_Prior_Settlement_Amt() { return iaaa320_Fund_Prior_Settlement_Amt; }

    public DbsField getIaaa320_Fund_Prior_Contract_Amt() { return iaaa320_Fund_Prior_Contract_Amt; }

    public DbsField getIaaa320_Fund_Prior_Dividend_Amt() { return iaaa320_Fund_Prior_Dividend_Amt; }

    public DbsField getIaaa320_Fund_Prior_Net_Pymnt_Amt() { return iaaa320_Fund_Prior_Net_Pymnt_Amt; }

    public DbsField getIaaa320_Fund_Curr_Unit_Qty() { return iaaa320_Fund_Curr_Unit_Qty; }

    public DbsField getIaaa320_Fund_Curr_Unit_Value() { return iaaa320_Fund_Curr_Unit_Value; }

    public DbsField getIaaa320_Fund_Curr_Settlement_Amt() { return iaaa320_Fund_Curr_Settlement_Amt; }

    public DbsField getIaaa320_Fund_Curr_Contract_Amt() { return iaaa320_Fund_Curr_Contract_Amt; }

    public DbsField getIaaa320_Fund_Curr_Dividend_Amt() { return iaaa320_Fund_Curr_Dividend_Amt; }

    public DbsField getIaaa320_Fund_Curr_Net_Pymnt_Amt() { return iaaa320_Fund_Curr_Net_Pymnt_Amt; }

    public DbsField getIaaa320_Fund_Unit_Qty_Chg_Flag() { return iaaa320_Fund_Unit_Qty_Chg_Flag; }

    public DbsField getIaaa320_Fund_Unit_Value_Chg_Flag() { return iaaa320_Fund_Unit_Value_Chg_Flag; }

    public DbsField getIaaa320_Fund_Settlement_Chg_Flag() { return iaaa320_Fund_Settlement_Chg_Flag; }

    public DbsField getIaaa320_Fund_Contract_Chg_Flag() { return iaaa320_Fund_Contract_Chg_Flag; }

    public DbsField getIaaa320_Fund_Dividend_Chg_Flag() { return iaaa320_Fund_Dividend_Chg_Flag; }

    public DbsField getIaaa320_Fund_Net_Pymnt_Chg_Flag() { return iaaa320_Fund_Net_Pymnt_Chg_Flag; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaaa320 = dbsRecord.newGroupInRecord("iaaa320", "IAAA320");
        iaaa320.setParameterOption(ParameterOption.ByReference);
        iaaa320_Pin_Nbr = iaaa320.newFieldInGroup("iaaa320_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        iaaa320_Contract_Nbr = iaaa320.newFieldInGroup("iaaa320_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10);
        iaaa320_Payee_Cde = iaaa320.newFieldInGroup("iaaa320_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 4);
        iaaa320_Ssn_Nbr = iaaa320.newFieldInGroup("iaaa320_Ssn_Nbr", "SSN-NBR", FieldType.NUMERIC, 9);
        iaaa320_Zip_Cde = iaaa320.newFieldInGroup("iaaa320_Zip_Cde", "ZIP-CDE", FieldType.STRING, 9);
        iaaa320_Company_Cde = iaaa320.newFieldInGroup("iaaa320_Company_Cde", "COMPANY-CDE", FieldType.STRING, 1);
        iaaa320_Mailing_Label_Ind = iaaa320.newFieldInGroup("iaaa320_Mailing_Label_Ind", "MAILING-LABEL-IND", FieldType.STRING, 1);
        iaaa320_Hold_Ind = iaaa320.newFieldInGroup("iaaa320_Hold_Ind", "HOLD-IND", FieldType.STRING, 1);
        iaaa320_Hold_Cde = iaaa320.newFieldInGroup("iaaa320_Hold_Cde", "HOLD-CDE", FieldType.STRING, 4);
        iaaa320_Hold_Grp_Cde = iaaa320.newFieldInGroup("iaaa320_Hold_Grp_Cde", "HOLD-GRP-CDE", FieldType.STRING, 3);
        iaaa320_Mode_Cde = iaaa320.newFieldInGroup("iaaa320_Mode_Cde", "MODE-CDE", FieldType.NUMERIC, 3);
        iaaa320_Off_Mode_Ind = iaaa320.newFieldInGroup("iaaa320_Off_Mode_Ind", "OFF-MODE-IND", FieldType.NUMERIC, 1);
        iaaa320_Transfer_Ind = iaaa320.newFieldInGroup("iaaa320_Transfer_Ind", "TRANSFER-IND", FieldType.STRING, 1);
        iaaa320_Combined_Pymnt_Ind = iaaa320.newFieldInGroup("iaaa320_Combined_Pymnt_Ind", "COMBINED-PYMNT-IND", FieldType.STRING, 1);
        iaaa320_Gtn_Return_Cde = iaaa320.newFieldInGroup("iaaa320_Gtn_Return_Cde", "GTN-RETURN-CDE", FieldType.STRING, 4);
        iaaa320_Gtn_Report_Cde = iaaa320.newFieldArrayInGroup("iaaa320_Gtn_Report_Cde", "GTN-REPORT-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1,
            10));
        iaaa320_Prior_State_Rsdncy_Cde = iaaa320.newFieldInGroup("iaaa320_Prior_State_Rsdncy_Cde", "PRIOR-STATE-RSDNCY-CDE", FieldType.STRING, 2);
        iaaa320_Curr_State_Rsdncy_Cde = iaaa320.newFieldInGroup("iaaa320_Curr_State_Rsdncy_Cde", "CURR-STATE-RSDNCY-CDE", FieldType.STRING, 2);
        iaaa320_State_Rsdncy_Chg_Flag = iaaa320.newFieldInGroup("iaaa320_State_Rsdncy_Chg_Flag", "STATE-RSDNCY-CHG-FLAG", FieldType.NUMERIC, 1);
        iaaa320_Ph_Last_Nme = iaaa320.newFieldInGroup("iaaa320_Ph_Last_Nme", "PH-LAST-NME", FieldType.STRING, 16);
        iaaa320_Ph_First_Nme = iaaa320.newFieldInGroup("iaaa320_Ph_First_Nme", "PH-FIRST-NME", FieldType.STRING, 10);
        iaaa320_Ph_Middle_Nme = iaaa320.newFieldInGroup("iaaa320_Ph_Middle_Nme", "PH-MIDDLE-NME", FieldType.STRING, 12);
        iaaa320_Ph_Mailing_Nme = iaaa320.newFieldInGroup("iaaa320_Ph_Mailing_Nme", "PH-MAILING-NME", FieldType.STRING, 38);
        iaaa320_Ph_Address_Line_1 = iaaa320.newFieldInGroup("iaaa320_Ph_Address_Line_1", "PH-ADDRESS-LINE-1", FieldType.STRING, 35);
        iaaa320_Ph_Address_Line_2 = iaaa320.newFieldInGroup("iaaa320_Ph_Address_Line_2", "PH-ADDRESS-LINE-2", FieldType.STRING, 35);
        iaaa320_Ph_Address_Line_3 = iaaa320.newFieldInGroup("iaaa320_Ph_Address_Line_3", "PH-ADDRESS-LINE-3", FieldType.STRING, 35);
        iaaa320_Ph_Address_Line_4 = iaaa320.newFieldInGroup("iaaa320_Ph_Address_Line_4", "PH-ADDRESS-LINE-4", FieldType.STRING, 35);
        iaaa320_Ph_Address_Line_5 = iaaa320.newFieldInGroup("iaaa320_Ph_Address_Line_5", "PH-ADDRESS-LINE-5", FieldType.STRING, 35);
        iaaa320_Ph_Address_Line_6 = iaaa320.newFieldInGroup("iaaa320_Ph_Address_Line_6", "PH-ADDRESS-LINE-6", FieldType.STRING, 35);
        iaaa320_Postal_Data = iaaa320.newFieldInGroup("iaaa320_Postal_Data", "POSTAL-DATA", FieldType.STRING, 32);
        iaaa320_Postal_DataRedef1 = iaaa320.newGroupInGroup("iaaa320_Postal_DataRedef1", "Redefines", iaaa320_Postal_Data);
        iaaa320_Zip_Code_5 = iaaa320_Postal_DataRedef1.newFieldInGroup("iaaa320_Zip_Code_5", "ZIP-CODE-5", FieldType.STRING, 5);
        iaaa320_Zip_Code_4 = iaaa320_Postal_DataRedef1.newFieldInGroup("iaaa320_Zip_Code_4", "ZIP-CODE-4", FieldType.STRING, 4);
        iaaa320_Carr_Walk_Rte = iaaa320_Postal_DataRedef1.newFieldInGroup("iaaa320_Carr_Walk_Rte", "CARR-WALK-RTE", FieldType.STRING, 23);
        iaaa320_Prior_Check_Dte = iaaa320.newFieldInGroup("iaaa320_Prior_Check_Dte", "PRIOR-CHECK-DTE", FieldType.NUMERIC, 8);
        iaaa320_Curr_Check_Dte = iaaa320.newFieldInGroup("iaaa320_Curr_Check_Dte", "CURR-CHECK-DTE", FieldType.NUMERIC, 8);
        iaaa320_Prior_Cycle_Dte = iaaa320.newFieldInGroup("iaaa320_Prior_Cycle_Dte", "PRIOR-CYCLE-DTE", FieldType.NUMERIC, 8);
        iaaa320_Curr_Cycle_Dte = iaaa320.newFieldInGroup("iaaa320_Curr_Cycle_Dte", "CURR-CYCLE-DTE", FieldType.NUMERIC, 8);
        iaaa320_Future_Pymnt_Dte = iaaa320.newFieldInGroup("iaaa320_Future_Pymnt_Dte", "FUTURE-PYMNT-DTE", FieldType.NUMERIC, 8);
        iaaa320_Cntrct_Annty_Ins_Type = iaaa320.newFieldInGroup("iaaa320_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        iaaa320_Cntrct_Annty_Type_Cde = iaaa320.newFieldInGroup("iaaa320_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        iaaa320_Cntrct_Insurance_Option = iaaa320.newFieldInGroup("iaaa320_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 1);
        iaaa320_Cntrct_Life_Contingency = iaaa320.newFieldInGroup("iaaa320_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 1);
        iaaa320_Cntrct_Option_Cde = iaaa320.newFieldInGroup("iaaa320_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        filler01 = iaaa320.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 15);
        iaaa320_Deduction_Cnt = iaaa320.newFieldInGroup("iaaa320_Deduction_Cnt", "DEDUCTION-CNT", FieldType.NUMERIC, 2);
        iaaa320_Deductions = iaaa320.newGroupArrayInGroup("iaaa320_Deductions", "DEDUCTIONS", new DbsArrayController(1,10));
        iaaa320_Ded_Detail = iaaa320_Deductions.newFieldInGroup("iaaa320_Ded_Detail", "DED-DETAIL", FieldType.STRING, 22);
        iaaa320_Ded_DetailRedef2 = iaaa320_Deductions.newGroupInGroup("iaaa320_Ded_DetailRedef2", "Redefines", iaaa320_Ded_Detail);
        iaaa320_Ded_Cde = iaaa320_Ded_DetailRedef2.newFieldInGroup("iaaa320_Ded_Cde", "DED-CDE", FieldType.NUMERIC, 3);
        iaaa320_Ded_CdeRedef3 = iaaa320_Ded_DetailRedef2.newGroupInGroup("iaaa320_Ded_CdeRedef3", "Redefines", iaaa320_Ded_Cde);
        iaaa320_Ded_Cde_A = iaaa320_Ded_CdeRedef3.newFieldInGroup("iaaa320_Ded_Cde_A", "DED-CDE-A", FieldType.STRING, 3);
        iaaa320_Ded_Prior_Amt = iaaa320_Ded_DetailRedef2.newFieldInGroup("iaaa320_Ded_Prior_Amt", "DED-PRIOR-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Ded_Curr_Amt = iaaa320_Ded_DetailRedef2.newFieldInGroup("iaaa320_Ded_Curr_Amt", "DED-CURR-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Ded_Amt_Chg_Flag = iaaa320_Ded_DetailRedef2.newFieldInGroup("iaaa320_Ded_Amt_Chg_Flag", "DED-AMT-CHG-FLAG", FieldType.NUMERIC, 1);
        iaaa320_Tax_Prior_Fed_Amt = iaaa320.newFieldInGroup("iaaa320_Tax_Prior_Fed_Amt", "TAX-PRIOR-FED-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Tax_Prior_St_Amt = iaaa320.newFieldInGroup("iaaa320_Tax_Prior_St_Amt", "TAX-PRIOR-ST-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Tax_Prior_Loc_Amt = iaaa320.newFieldInGroup("iaaa320_Tax_Prior_Loc_Amt", "TAX-PRIOR-LOC-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Tax_Curr_Fed_Amt = iaaa320.newFieldInGroup("iaaa320_Tax_Curr_Fed_Amt", "TAX-CURR-FED-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Tax_Curr_St_Amt = iaaa320.newFieldInGroup("iaaa320_Tax_Curr_St_Amt", "TAX-CURR-ST-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Tax_Curr_Loc_Amt = iaaa320.newFieldInGroup("iaaa320_Tax_Curr_Loc_Amt", "TAX-CURR-LOC-AMT", FieldType.DECIMAL, 9,2);
        iaaa320_Tax_Fed_Chg_Flag = iaaa320.newFieldInGroup("iaaa320_Tax_Fed_Chg_Flag", "TAX-FED-CHG-FLAG", FieldType.NUMERIC, 1);
        iaaa320_Tax_St_Chg_Flag = iaaa320.newFieldInGroup("iaaa320_Tax_St_Chg_Flag", "TAX-ST-CHG-FLAG", FieldType.NUMERIC, 1);
        iaaa320_Tax_Loc_Chg_Flag = iaaa320.newFieldInGroup("iaaa320_Tax_Loc_Chg_Flag", "TAX-LOC-CHG-FLAG", FieldType.NUMERIC, 1);
        iaaa320_Fund_Cnt = iaaa320.newFieldInGroup("iaaa320_Fund_Cnt", "FUND-CNT", FieldType.NUMERIC, 2);
        iaaa320_Fund_Grp = iaaa320.newGroupArrayInGroup("iaaa320_Fund_Grp", "FUND-GRP", new DbsArrayController(1,40));
        iaaa320_Fund_Detail = iaaa320_Fund_Grp.newFieldInGroup("iaaa320_Fund_Detail", "FUND-DETAIL", FieldType.STRING, 123);
        iaaa320_Fund_DetailRedef4 = iaaa320_Fund_Grp.newGroupInGroup("iaaa320_Fund_DetailRedef4", "Redefines", iaaa320_Fund_Detail);
        iaaa320_Fund_Valuat_Fund = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Valuat_Fund", "FUND-VALUAT-FUND", FieldType.STRING, 3);
        iaaa320_Fund_Valuat_FundRedef5 = iaaa320_Fund_DetailRedef4.newGroupInGroup("iaaa320_Fund_Valuat_FundRedef5", "Redefines", iaaa320_Fund_Valuat_Fund);
        iaaa320_Fund_Valuat_Period = iaaa320_Fund_Valuat_FundRedef5.newFieldInGroup("iaaa320_Fund_Valuat_Period", "FUND-VALUAT-PERIOD", FieldType.STRING, 
            1);
        iaaa320_Fund_Account_Cde_A = iaaa320_Fund_Valuat_FundRedef5.newFieldInGroup("iaaa320_Fund_Account_Cde_A", "FUND-ACCOUNT-CDE-A", FieldType.STRING, 
            2);
        iaaa320_Fund_Account_Cde_N = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Account_Cde_N", "FUND-ACCOUNT-CDE-N", FieldType.NUMERIC, 
            2);
        iaaa320_Fund_Prior_Unit_Qty = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Prior_Unit_Qty", "FUND-PRIOR-UNIT-QTY", FieldType.DECIMAL, 
            9,3);
        iaaa320_Fund_Prior_Unit_Value = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Prior_Unit_Value", "FUND-PRIOR-UNIT-VALUE", FieldType.DECIMAL, 
            9,4);
        iaaa320_Fund_Prior_Settlement_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Prior_Settlement_Amt", "FUND-PRIOR-SETTLEMENT-AMT", 
            FieldType.DECIMAL, 11,2);
        iaaa320_Fund_Prior_Contract_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Prior_Contract_Amt", "FUND-PRIOR-CONTRACT-AMT", FieldType.DECIMAL, 
            9,2);
        iaaa320_Fund_Prior_Dividend_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Prior_Dividend_Amt", "FUND-PRIOR-DIVIDEND-AMT", FieldType.DECIMAL, 
            9,2);
        iaaa320_Fund_Prior_Net_Pymnt_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Prior_Net_Pymnt_Amt", "FUND-PRIOR-NET-PYMNT-AMT", FieldType.DECIMAL, 
            9,2);
        iaaa320_Fund_Curr_Unit_Qty = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Curr_Unit_Qty", "FUND-CURR-UNIT-QTY", FieldType.DECIMAL, 
            9,3);
        iaaa320_Fund_Curr_Unit_Value = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Curr_Unit_Value", "FUND-CURR-UNIT-VALUE", FieldType.DECIMAL, 
            9,4);
        iaaa320_Fund_Curr_Settlement_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Curr_Settlement_Amt", "FUND-CURR-SETTLEMENT-AMT", FieldType.DECIMAL, 
            11,2);
        iaaa320_Fund_Curr_Contract_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Curr_Contract_Amt", "FUND-CURR-CONTRACT-AMT", FieldType.DECIMAL, 
            9,2);
        iaaa320_Fund_Curr_Dividend_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Curr_Dividend_Amt", "FUND-CURR-DIVIDEND-AMT", FieldType.DECIMAL, 
            9,2);
        iaaa320_Fund_Curr_Net_Pymnt_Amt = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Curr_Net_Pymnt_Amt", "FUND-CURR-NET-PYMNT-AMT", FieldType.DECIMAL, 
            9,2);
        iaaa320_Fund_Unit_Qty_Chg_Flag = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Unit_Qty_Chg_Flag", "FUND-UNIT-QTY-CHG-FLAG", FieldType.NUMERIC, 
            1);
        iaaa320_Fund_Unit_Value_Chg_Flag = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Unit_Value_Chg_Flag", "FUND-UNIT-VALUE-CHG-FLAG", FieldType.NUMERIC, 
            1);
        iaaa320_Fund_Settlement_Chg_Flag = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Settlement_Chg_Flag", "FUND-SETTLEMENT-CHG-FLAG", FieldType.NUMERIC, 
            1);
        iaaa320_Fund_Contract_Chg_Flag = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Contract_Chg_Flag", "FUND-CONTRACT-CHG-FLAG", FieldType.NUMERIC, 
            1);
        iaaa320_Fund_Dividend_Chg_Flag = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Dividend_Chg_Flag", "FUND-DIVIDEND-CHG-FLAG", FieldType.NUMERIC, 
            1);
        iaaa320_Fund_Net_Pymnt_Chg_Flag = iaaa320_Fund_DetailRedef4.newFieldInGroup("iaaa320_Fund_Net_Pymnt_Chg_Flag", "FUND-NET-PYMNT-CHG-FLAG", FieldType.NUMERIC, 
            1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIaaa323(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

