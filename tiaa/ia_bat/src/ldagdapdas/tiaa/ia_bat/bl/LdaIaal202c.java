/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:40 PM
**        * FROM NATURAL LDA     : IAAL202C
************************************************************
**        * FILE NAME            : LdaIaal202c.java
**        * CLASS NAME           : LdaIaal202c
**        * INSTANCE NAME        : LdaIaal202c
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal202c extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Ddctn_Trans;
    private DbsField iaa_Ddctn_Trans_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Lst_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Id_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Cde;
    private DbsGroup iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee;
    private DbsField iaa_Ddctn_Trans_Ddctn_Per_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ytd_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Pd_To_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Tot_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Intent_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Strt_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Stp_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Final_Dte;
    private DbsField iaa_Ddctn_Trans_Trans_Check_Dte;
    private DbsField iaa_Ddctn_Trans_Bfre_Imge_Id;
    private DbsField iaa_Ddctn_Trans_Aftr_Imge_Id;

    public DataAccessProgramView getVw_iaa_Ddctn_Trans() { return vw_iaa_Ddctn_Trans; }

    public DbsField getIaa_Ddctn_Trans_Trans_Dte() { return iaa_Ddctn_Trans_Trans_Dte; }

    public DbsField getIaa_Ddctn_Trans_Invrse_Trans_Dte() { return iaa_Ddctn_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Ddctn_Trans_Lst_Trans_Dte() { return iaa_Ddctn_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Ppcn_Nbr() { return iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Payee_Cde() { return iaa_Ddctn_Trans_Ddctn_Payee_Cde; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Id_Nbr() { return iaa_Ddctn_Trans_Ddctn_Id_Nbr; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Seq_Cde() { return iaa_Ddctn_Trans_Ddctn_Seq_Cde; }

    public DbsGroup getIaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1() { return iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Seq_Nbr() { return iaa_Ddctn_Trans_Ddctn_Seq_Nbr; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Cde() { return iaa_Ddctn_Trans_Ddctn_Cde; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Payee() { return iaa_Ddctn_Trans_Ddctn_Payee; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Per_Amt() { return iaa_Ddctn_Trans_Ddctn_Per_Amt; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Ytd_Amt() { return iaa_Ddctn_Trans_Ddctn_Ytd_Amt; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Pd_To_Dte() { return iaa_Ddctn_Trans_Ddctn_Pd_To_Dte; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Tot_Amt() { return iaa_Ddctn_Trans_Ddctn_Tot_Amt; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Intent_Cde() { return iaa_Ddctn_Trans_Ddctn_Intent_Cde; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Strt_Dte() { return iaa_Ddctn_Trans_Ddctn_Strt_Dte; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Stp_Dte() { return iaa_Ddctn_Trans_Ddctn_Stp_Dte; }

    public DbsField getIaa_Ddctn_Trans_Ddctn_Final_Dte() { return iaa_Ddctn_Trans_Ddctn_Final_Dte; }

    public DbsField getIaa_Ddctn_Trans_Trans_Check_Dte() { return iaa_Ddctn_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Ddctn_Trans_Bfre_Imge_Id() { return iaa_Ddctn_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Ddctn_Trans_Aftr_Imge_Id() { return iaa_Ddctn_Trans_Aftr_Imge_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Ddctn_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Ddctn_Trans", "IAA-DDCTN-TRANS"), "IAA_DDCTN_TRANS", "IA_TRANS_FILE");
        iaa_Ddctn_Trans_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Ddctn_Trans_Invrse_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Ddctn_Trans_Lst_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Ddctn_Trans_Ddctn_Payee_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Ddctn_Trans_Ddctn_Id_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Ddctn_Trans_Ddctn_Seq_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Cde", "DDCTN-SEQ-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "DDCTN_SEQ_CDE");
        iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1 = vw_iaa_Ddctn_Trans.getRecord().newGroupInGroup("iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1", "Redefines", iaa_Ddctn_Trans_Ddctn_Seq_Cde);
        iaa_Ddctn_Trans_Ddctn_Seq_Nbr = iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        iaa_Ddctn_Trans_Ddctn_Cde = iaa_Ddctn_Trans_Ddctn_Seq_CdeRedef1.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3);
        iaa_Ddctn_Trans_Ddctn_Payee = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Ddctn_Trans_Ddctn_Per_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Ddctn_Trans_Ddctn_Ytd_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Ddctn_Trans_Ddctn_Pd_To_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Ddctn_Trans_Ddctn_Tot_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Ddctn_Trans_Ddctn_Intent_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Ddctn_Trans_Ddctn_Strt_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Ddctn_Trans_Ddctn_Stp_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Ddctn_Trans_Ddctn_Final_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Ddctn_Trans_Trans_Check_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Ddctn_Trans_Bfre_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Ddctn_Trans_Aftr_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");

        this.setRecordName("LdaIaal202c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Ddctn_Trans.reset();
    }

    // Constructor
    public LdaIaal202c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
