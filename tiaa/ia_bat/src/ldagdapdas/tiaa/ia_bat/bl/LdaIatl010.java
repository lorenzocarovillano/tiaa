/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:28 PM
**        * FROM NATURAL LDA     : IATL010
************************************************************
**        * FILE NAME            : LdaIatl010.java
**        * CLASS NAME           : LdaIatl010
**        * INSTANCE NAME        : LdaIatl010
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIatl010 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iatl010;
    private DbsField iatl010_Rcrd_Type_Cde;
    private DbsField iatl010_Rqst_Id;
    private DbsField iatl010_Rqst_Effctv_Dte;
    private DbsField iatl010_Rqst_Entry_Dte;
    private DbsField iatl010_Rqst_Entry_Tme;
    private DbsField iatl010_Rqst_Lst_Actvty_Dte;
    private DbsField iatl010_Rqst_Rcvd_Dte;
    private DbsField iatl010_Rqst_Rcvd_Tme;
    private DbsField iatl010_Rqst_Rcvd_User_Id;
    private DbsField iatl010_Rqst_Entry_User_Id;
    private DbsField iatl010_Rqst_Cntct_Mde;
    private DbsField iatl010_Rqst_Srce;
    private DbsField iatl010_Rqst_Rep_Nme;
    private DbsField iatl010_Rqst_Sttmnt_Ind;
    private DbsField iatl010_Rqst_Opn_Clsd_Ind;
    private DbsField iatl010_Rqst_Rcprcl_Dte;
    private DbsField iatl010_Rqst_Ssn;
    private DbsField iatl010_Xfr_Work_Prcss_Id;
    private DbsField iatl010_Xfr_Mit_Log_Dte_Tme;
    private DbsField iatl010_Xfr_Stts_Cde;
    private DbsField iatl010_Xfr_Rjctn_Cde;
    private DbsField iatl010_Xfr_In_Progress_Ind;
    private DbsField iatl010_Xfr_Retry_Cnt;
    private DbsField iatl010_Ia_Frm_Cntrct;
    private DbsField iatl010_Ia_Frm_Payee;
    private DbsField iatl010_Ia_Unique_Id;
    private DbsField iatl010_Ia_Hold_Cde;
    private DbsField iatl010_Count_Castxfr_Frm_Acct_Dta;
    private DbsGroup iatl010_Xfr_Frm_Acct_Dta;
    private DbsField iatl010_Xfr_Frm_Acct_Cde;
    private DbsField iatl010_Xfr_Frm_Unit_Typ;
    private DbsField iatl010_Xfr_Frm_Typ;
    private DbsField iatl010_Xfr_Frm_Qty;
    private DbsField iatl010_Xfr_Frm_Est_Amt;
    private DbsField iatl010_Xfr_Frm_Asset_Amt;
    private DbsField iatl010_Count_Castxfr_To_Acct_Dta;
    private DbsGroup iatl010_Xfr_To_Acct_Dta;
    private DbsField iatl010_Xfr_To_Acct_Cde;
    private DbsField iatl010_Xfr_To_Unit_Typ;
    private DbsField iatl010_Xfr_To_Typ;
    private DbsField iatl010_Xfr_To_Qty;
    private DbsField iatl010_Xfr_To_Est_Amt;
    private DbsField iatl010_Xfr_To_Asset_Amt;
    private DbsField iatl010_Ia_To_Cntrct;
    private DbsField iatl010_Ia_To_Payee;
    private DbsField iatl010_Ia_Appl_Rcvd_Dte;
    private DbsField iatl010_Ia_New_Issue;
    private DbsField iatl010_Ia_Rsn_For_Ovrrde;
    private DbsField iatl010_Ia_Ovrrde_User_Id;
    private DbsField iatl010_Ia_Gnrl_Pend_Cde;
    private DbsField iatl010_Rqst_Xfr_Type;
    private DbsField iatl010_Rqst_Unit_Cde;
    private DbsField iatl010_Ia_New_Iss_Prt_Pull;

    public DataAccessProgramView getVw_iatl010() { return vw_iatl010; }

    public DbsField getIatl010_Rcrd_Type_Cde() { return iatl010_Rcrd_Type_Cde; }

    public DbsField getIatl010_Rqst_Id() { return iatl010_Rqst_Id; }

    public DbsField getIatl010_Rqst_Effctv_Dte() { return iatl010_Rqst_Effctv_Dte; }

    public DbsField getIatl010_Rqst_Entry_Dte() { return iatl010_Rqst_Entry_Dte; }

    public DbsField getIatl010_Rqst_Entry_Tme() { return iatl010_Rqst_Entry_Tme; }

    public DbsField getIatl010_Rqst_Lst_Actvty_Dte() { return iatl010_Rqst_Lst_Actvty_Dte; }

    public DbsField getIatl010_Rqst_Rcvd_Dte() { return iatl010_Rqst_Rcvd_Dte; }

    public DbsField getIatl010_Rqst_Rcvd_Tme() { return iatl010_Rqst_Rcvd_Tme; }

    public DbsField getIatl010_Rqst_Rcvd_User_Id() { return iatl010_Rqst_Rcvd_User_Id; }

    public DbsField getIatl010_Rqst_Entry_User_Id() { return iatl010_Rqst_Entry_User_Id; }

    public DbsField getIatl010_Rqst_Cntct_Mde() { return iatl010_Rqst_Cntct_Mde; }

    public DbsField getIatl010_Rqst_Srce() { return iatl010_Rqst_Srce; }

    public DbsField getIatl010_Rqst_Rep_Nme() { return iatl010_Rqst_Rep_Nme; }

    public DbsField getIatl010_Rqst_Sttmnt_Ind() { return iatl010_Rqst_Sttmnt_Ind; }

    public DbsField getIatl010_Rqst_Opn_Clsd_Ind() { return iatl010_Rqst_Opn_Clsd_Ind; }

    public DbsField getIatl010_Rqst_Rcprcl_Dte() { return iatl010_Rqst_Rcprcl_Dte; }

    public DbsField getIatl010_Rqst_Ssn() { return iatl010_Rqst_Ssn; }

    public DbsField getIatl010_Xfr_Work_Prcss_Id() { return iatl010_Xfr_Work_Prcss_Id; }

    public DbsField getIatl010_Xfr_Mit_Log_Dte_Tme() { return iatl010_Xfr_Mit_Log_Dte_Tme; }

    public DbsField getIatl010_Xfr_Stts_Cde() { return iatl010_Xfr_Stts_Cde; }

    public DbsField getIatl010_Xfr_Rjctn_Cde() { return iatl010_Xfr_Rjctn_Cde; }

    public DbsField getIatl010_Xfr_In_Progress_Ind() { return iatl010_Xfr_In_Progress_Ind; }

    public DbsField getIatl010_Xfr_Retry_Cnt() { return iatl010_Xfr_Retry_Cnt; }

    public DbsField getIatl010_Ia_Frm_Cntrct() { return iatl010_Ia_Frm_Cntrct; }

    public DbsField getIatl010_Ia_Frm_Payee() { return iatl010_Ia_Frm_Payee; }

    public DbsField getIatl010_Ia_Unique_Id() { return iatl010_Ia_Unique_Id; }

    public DbsField getIatl010_Ia_Hold_Cde() { return iatl010_Ia_Hold_Cde; }

    public DbsField getIatl010_Count_Castxfr_Frm_Acct_Dta() { return iatl010_Count_Castxfr_Frm_Acct_Dta; }

    public DbsGroup getIatl010_Xfr_Frm_Acct_Dta() { return iatl010_Xfr_Frm_Acct_Dta; }

    public DbsField getIatl010_Xfr_Frm_Acct_Cde() { return iatl010_Xfr_Frm_Acct_Cde; }

    public DbsField getIatl010_Xfr_Frm_Unit_Typ() { return iatl010_Xfr_Frm_Unit_Typ; }

    public DbsField getIatl010_Xfr_Frm_Typ() { return iatl010_Xfr_Frm_Typ; }

    public DbsField getIatl010_Xfr_Frm_Qty() { return iatl010_Xfr_Frm_Qty; }

    public DbsField getIatl010_Xfr_Frm_Est_Amt() { return iatl010_Xfr_Frm_Est_Amt; }

    public DbsField getIatl010_Xfr_Frm_Asset_Amt() { return iatl010_Xfr_Frm_Asset_Amt; }

    public DbsField getIatl010_Count_Castxfr_To_Acct_Dta() { return iatl010_Count_Castxfr_To_Acct_Dta; }

    public DbsGroup getIatl010_Xfr_To_Acct_Dta() { return iatl010_Xfr_To_Acct_Dta; }

    public DbsField getIatl010_Xfr_To_Acct_Cde() { return iatl010_Xfr_To_Acct_Cde; }

    public DbsField getIatl010_Xfr_To_Unit_Typ() { return iatl010_Xfr_To_Unit_Typ; }

    public DbsField getIatl010_Xfr_To_Typ() { return iatl010_Xfr_To_Typ; }

    public DbsField getIatl010_Xfr_To_Qty() { return iatl010_Xfr_To_Qty; }

    public DbsField getIatl010_Xfr_To_Est_Amt() { return iatl010_Xfr_To_Est_Amt; }

    public DbsField getIatl010_Xfr_To_Asset_Amt() { return iatl010_Xfr_To_Asset_Amt; }

    public DbsField getIatl010_Ia_To_Cntrct() { return iatl010_Ia_To_Cntrct; }

    public DbsField getIatl010_Ia_To_Payee() { return iatl010_Ia_To_Payee; }

    public DbsField getIatl010_Ia_Appl_Rcvd_Dte() { return iatl010_Ia_Appl_Rcvd_Dte; }

    public DbsField getIatl010_Ia_New_Issue() { return iatl010_Ia_New_Issue; }

    public DbsField getIatl010_Ia_Rsn_For_Ovrrde() { return iatl010_Ia_Rsn_For_Ovrrde; }

    public DbsField getIatl010_Ia_Ovrrde_User_Id() { return iatl010_Ia_Ovrrde_User_Id; }

    public DbsField getIatl010_Ia_Gnrl_Pend_Cde() { return iatl010_Ia_Gnrl_Pend_Cde; }

    public DbsField getIatl010_Rqst_Xfr_Type() { return iatl010_Rqst_Xfr_Type; }

    public DbsField getIatl010_Rqst_Unit_Cde() { return iatl010_Rqst_Unit_Cde; }

    public DbsField getIatl010_Ia_New_Iss_Prt_Pull() { return iatl010_Ia_New_Iss_Prt_Pull; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iatl010 = new DataAccessProgramView(new NameInfo("vw_iatl010", "IATL010"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        iatl010_Rcrd_Type_Cde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        iatl010_Rqst_Id = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, "RQST_ID");
        iatl010_Rqst_Effctv_Dte = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_EFFCTV_DTE");
        iatl010_Rqst_Entry_Dte = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_DTE");
        iatl010_Rqst_Entry_Tme = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_TME");
        iatl010_Rqst_Lst_Actvty_Dte = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_LST_ACTVTY_DTE");
        iatl010_Rqst_Rcvd_Dte = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_RCVD_DTE");
        iatl010_Rqst_Rcvd_Tme = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RQST_RCVD_TME");
        iatl010_Rqst_Rcvd_User_Id = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_RCVD_USER_ID");
        iatl010_Rqst_Entry_User_Id = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_USER_ID");
        iatl010_Rqst_Cntct_Mde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_CNTCT_MDE");
        iatl010_Rqst_Srce = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_SRCE");
        iatl010_Rqst_Rep_Nme = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RQST_REP_NME");
        iatl010_Rqst_Sttmnt_Ind = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_STTMNT_IND");
        iatl010_Rqst_Opn_Clsd_Ind = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_OPN_CLSD_IND");
        iatl010_Rqst_Rcprcl_Dte = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RQST_RCPRCL_DTE");
        iatl010_Rqst_Ssn = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "RQST_SSN");
        iatl010_Xfr_Work_Prcss_Id = vw_iatl010.getRecord().newFieldInGroup("iatl010_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "XFR_WORK_PRCSS_ID");
        iatl010_Xfr_Mit_Log_Dte_Tme = vw_iatl010.getRecord().newFieldInGroup("iatl010_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        iatl010_Xfr_Stts_Cde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_STTS_CDE");
        iatl010_Xfr_Rjctn_Cde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_RJCTN_CDE");
        iatl010_Xfr_In_Progress_Ind = vw_iatl010.getRecord().newFieldInGroup("iatl010_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", FieldType.BOOLEAN, 
            1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        iatl010_Xfr_Retry_Cnt = vw_iatl010.getRecord().newFieldInGroup("iatl010_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "XFR_RETRY_CNT");
        iatl010_Ia_Frm_Cntrct = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_FRM_CNTRCT");
        iatl010_Ia_Frm_Payee = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_FRM_PAYEE");
        iatl010_Ia_Unique_Id = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "IA_UNIQUE_ID");
        iatl010_Ia_Hold_Cde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IA_HOLD_CDE");
        iatl010_Count_Castxfr_Frm_Acct_Dta = vw_iatl010.getRecord().newFieldInGroup("iatl010_Count_Castxfr_Frm_Acct_Dta", "C*XFR-FRM-ACCT-DTA", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Xfr_Frm_Acct_Dta = vw_iatl010.getRecord().newGroupInGroup("iatl010_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Xfr_Frm_Acct_Cde = iatl010_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 1, 
            new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Xfr_Frm_Unit_Typ = iatl010_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 1, 
            new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Xfr_Frm_Typ = iatl010_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1, new DbsArrayController(1,20) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Xfr_Frm_Qty = iatl010_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,20) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Xfr_Frm_Est_Amt = iatl010_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Xfr_Frm_Asset_Amt = iatl010_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iatl010_Count_Castxfr_To_Acct_Dta = vw_iatl010.getRecord().newFieldInGroup("iatl010_Count_Castxfr_To_Acct_Dta", "C*XFR-TO-ACCT-DTA", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Xfr_To_Acct_Dta = vw_iatl010.getRecord().newGroupInGroup("iatl010_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Xfr_To_Acct_Cde = iatl010_Xfr_To_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 1, new 
            DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Xfr_To_Unit_Typ = iatl010_Xfr_To_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 1, new 
            DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Xfr_To_Typ = iatl010_Xfr_To_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Xfr_To_Qty = iatl010_Xfr_To_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Xfr_To_Est_Amt = iatl010_Xfr_To_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Xfr_To_Asset_Amt = iatl010_Xfr_To_Acct_Dta.newFieldArrayInGroup("iatl010_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iatl010_Ia_To_Cntrct = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_TO_CNTRCT");
        iatl010_Ia_To_Payee = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_TO_PAYEE");
        iatl010_Ia_Appl_Rcvd_Dte = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "IA_APPL_RCVD_DTE");
        iatl010_Ia_New_Issue = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "IA_NEW_ISSUE");
        iatl010_Ia_Rsn_For_Ovrrde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_RSN_FOR_OVRRDE");
        iatl010_Ia_Ovrrde_User_Id = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IA_OVRRDE_USER_ID");
        iatl010_Ia_Gnrl_Pend_Cde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "IA_GNRL_PEND_CDE");
        iatl010_Rqst_Xfr_Type = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_XFR_TYPE");
        iatl010_Rqst_Unit_Cde = vw_iatl010.getRecord().newFieldInGroup("iatl010_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_UNIT_CDE");
        iatl010_Ia_New_Iss_Prt_Pull = vw_iatl010.getRecord().newFieldInGroup("iatl010_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        vw_iatl010.setUniquePeList();

        this.setRecordName("LdaIatl010");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iatl010.reset();
    }

    // Constructor
    public LdaIatl010() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
