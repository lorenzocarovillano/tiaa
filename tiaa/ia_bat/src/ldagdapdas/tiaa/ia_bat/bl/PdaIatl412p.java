/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:51 PM
**        * FROM NATURAL PDA     : IATL412P
************************************************************
**        * FILE NAME            : PdaIatl412p.java
**        * CLASS NAME           : PdaIatl412p
**        * INSTANCE NAME        : PdaIatl412p
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIatl412p extends PdaBase
{
    // Properties
    private DbsGroup pnd_Iatn412_In;
    private DbsField pnd_Iatn412_In_Pnd_Printer_Id;
    private DbsField pnd_Iatn412_In_Rqst_Id;
    private DbsGroup pnd_Iatn412_Out;
    private DbsField pnd_Iatn412_Out_Pnd_Return_Code;
    private DbsField pnd_Iatn412_Out_Pnd_Msg;

    public DbsGroup getPnd_Iatn412_In() { return pnd_Iatn412_In; }

    public DbsField getPnd_Iatn412_In_Pnd_Printer_Id() { return pnd_Iatn412_In_Pnd_Printer_Id; }

    public DbsField getPnd_Iatn412_In_Rqst_Id() { return pnd_Iatn412_In_Rqst_Id; }

    public DbsGroup getPnd_Iatn412_Out() { return pnd_Iatn412_Out; }

    public DbsField getPnd_Iatn412_Out_Pnd_Return_Code() { return pnd_Iatn412_Out_Pnd_Return_Code; }

    public DbsField getPnd_Iatn412_Out_Pnd_Msg() { return pnd_Iatn412_Out_Pnd_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iatn412_In = dbsRecord.newGroupInRecord("pnd_Iatn412_In", "#IATN412-IN");
        pnd_Iatn412_In.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn412_In_Pnd_Printer_Id = pnd_Iatn412_In.newFieldInGroup("pnd_Iatn412_In_Pnd_Printer_Id", "#PRINTER-ID", FieldType.STRING, 4);
        pnd_Iatn412_In_Rqst_Id = pnd_Iatn412_In.newFieldInGroup("pnd_Iatn412_In_Rqst_Id", "RQST-ID", FieldType.STRING, 34);

        pnd_Iatn412_Out = dbsRecord.newGroupInRecord("pnd_Iatn412_Out", "#IATN412-OUT");
        pnd_Iatn412_Out.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn412_Out_Pnd_Return_Code = pnd_Iatn412_Out.newFieldInGroup("pnd_Iatn412_Out_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Iatn412_Out_Pnd_Msg = pnd_Iatn412_Out.newFieldInGroup("pnd_Iatn412_Out_Pnd_Msg", "#MSG", FieldType.STRING, 79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIatl412p(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

