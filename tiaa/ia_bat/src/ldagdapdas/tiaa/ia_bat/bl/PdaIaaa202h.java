/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:38 PM
**        * FROM NATURAL PDA     : IAAA202H
************************************************************
**        * FILE NAME            : PdaIaaa202h.java
**        * CLASS NAME           : PdaIaaa202h
**        * INSTANCE NAME        : PdaIaaa202h
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIaaa202h extends PdaBase
{
    // Properties
    private DbsGroup pnd_Iaa_Cntrl_Rcrd_1;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cpnd_Cntrl_Fund_Cnts;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;
    private DbsField pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte;

    public DbsGroup getPnd_Iaa_Cntrl_Rcrd_1() { return pnd_Iaa_Cntrl_Rcrd_1; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Cde() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Cde; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cpnd_Cntrl_Fund_Cnts() { return pnd_Iaa_Cntrl_Rcrd_1_Cpnd_Cntrl_Fund_Cnts; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Amt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Amt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees; }

    public DbsField getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte() { return pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iaa_Cntrl_Rcrd_1 = dbsRecord.newGroupInRecord("pnd_Iaa_Cntrl_Rcrd_1", "#IAA-CNTRL-RCRD-1");
        pnd_Iaa_Cntrl_Rcrd_1.setParameterOption(ParameterOption.ByReference);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Cde = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees", "CNTRL-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            13,2);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13,2);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13,3);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13,2);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13,2);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE);
        pnd_Iaa_Cntrl_Rcrd_1_Cpnd_Cntrl_Fund_Cnts = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cpnd_Cntrl_Fund_Cnts", "C#CNTRL-FUND-CNTS", 
            FieldType.NUMERIC, 3);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = pnd_Iaa_Cntrl_Rcrd_1.newFieldArrayInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1,80));
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = pnd_Iaa_Cntrl_Rcrd_1.newFieldArrayInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1,80));
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units = pnd_Iaa_Cntrl_Rcrd_1.newFieldArrayInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13,3, new DbsArrayController(1,80));
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Amt = pnd_Iaa_Cntrl_Rcrd_1.newFieldArrayInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,80));
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13,2);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte = pnd_Iaa_Cntrl_Rcrd_1.newFieldInGroup("pnd_Iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", 
            FieldType.DATE);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIaaa202h(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

