/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:51 PM
**        * FROM NATURAL PDA     : IATL403P
************************************************************
**        * FILE NAME            : PdaIatl403p.java
**        * CLASS NAME           : PdaIatl403p
**        * INSTANCE NAME        : PdaIatl403p
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIatl403p extends PdaBase
{
    // Properties
    private DbsGroup pnd_Iatn403_In;
    private DbsField pnd_Iatn403_In_Pnd_Transfer_Switch;
    private DbsField pnd_Iatn403_In_Rqst_Effctv_Dte;
    private DbsField pnd_Iatn403_In_Ia_Frm_Cntrct;
    private DbsField pnd_Iatn403_In_Ia_Frm_Payee;
    private DbsField pnd_Iatn403_In_Ia_To_Cntrct;
    private DbsField pnd_Iatn403_In_Ia_To_Payee;
    private DbsField pnd_Iatn403_In_Xfr_Frm_Acct_Cde;
    private DbsField pnd_Iatn403_In_Xfr_To_Acct_Cde;
    private DbsGroup pnd_Iatn403_Out;
    private DbsField pnd_Iatn403_Out_Pnd_State_Approved;
    private DbsField pnd_Iatn403_Out_Pnd_Residence_State;
    private DbsField pnd_Iatn403_Out_Pnd_Issue_State;
    private DbsField pnd_Iatn403_Out_Pnd_To_Issue_State;
    private DbsField pnd_Iatn403_Out_Pnd_Return_Code;
    private DbsField pnd_Iatn403_Out_Pnd_Msg;

    public DbsGroup getPnd_Iatn403_In() { return pnd_Iatn403_In; }

    public DbsField getPnd_Iatn403_In_Pnd_Transfer_Switch() { return pnd_Iatn403_In_Pnd_Transfer_Switch; }

    public DbsField getPnd_Iatn403_In_Rqst_Effctv_Dte() { return pnd_Iatn403_In_Rqst_Effctv_Dte; }

    public DbsField getPnd_Iatn403_In_Ia_Frm_Cntrct() { return pnd_Iatn403_In_Ia_Frm_Cntrct; }

    public DbsField getPnd_Iatn403_In_Ia_Frm_Payee() { return pnd_Iatn403_In_Ia_Frm_Payee; }

    public DbsField getPnd_Iatn403_In_Ia_To_Cntrct() { return pnd_Iatn403_In_Ia_To_Cntrct; }

    public DbsField getPnd_Iatn403_In_Ia_To_Payee() { return pnd_Iatn403_In_Ia_To_Payee; }

    public DbsField getPnd_Iatn403_In_Xfr_Frm_Acct_Cde() { return pnd_Iatn403_In_Xfr_Frm_Acct_Cde; }

    public DbsField getPnd_Iatn403_In_Xfr_To_Acct_Cde() { return pnd_Iatn403_In_Xfr_To_Acct_Cde; }

    public DbsGroup getPnd_Iatn403_Out() { return pnd_Iatn403_Out; }

    public DbsField getPnd_Iatn403_Out_Pnd_State_Approved() { return pnd_Iatn403_Out_Pnd_State_Approved; }

    public DbsField getPnd_Iatn403_Out_Pnd_Residence_State() { return pnd_Iatn403_Out_Pnd_Residence_State; }

    public DbsField getPnd_Iatn403_Out_Pnd_Issue_State() { return pnd_Iatn403_Out_Pnd_Issue_State; }

    public DbsField getPnd_Iatn403_Out_Pnd_To_Issue_State() { return pnd_Iatn403_Out_Pnd_To_Issue_State; }

    public DbsField getPnd_Iatn403_Out_Pnd_Return_Code() { return pnd_Iatn403_Out_Pnd_Return_Code; }

    public DbsField getPnd_Iatn403_Out_Pnd_Msg() { return pnd_Iatn403_Out_Pnd_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iatn403_In = dbsRecord.newGroupInRecord("pnd_Iatn403_In", "#IATN403-IN");
        pnd_Iatn403_In.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn403_In_Pnd_Transfer_Switch = pnd_Iatn403_In.newFieldInGroup("pnd_Iatn403_In_Pnd_Transfer_Switch", "#TRANSFER-SWITCH", FieldType.STRING, 
            1);
        pnd_Iatn403_In_Rqst_Effctv_Dte = pnd_Iatn403_In.newFieldInGroup("pnd_Iatn403_In_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE);
        pnd_Iatn403_In_Ia_Frm_Cntrct = pnd_Iatn403_In.newFieldInGroup("pnd_Iatn403_In_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_Iatn403_In_Ia_Frm_Payee = pnd_Iatn403_In.newFieldInGroup("pnd_Iatn403_In_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2);
        pnd_Iatn403_In_Ia_To_Cntrct = pnd_Iatn403_In.newFieldInGroup("pnd_Iatn403_In_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10);
        pnd_Iatn403_In_Ia_To_Payee = pnd_Iatn403_In.newFieldInGroup("pnd_Iatn403_In_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2);
        pnd_Iatn403_In_Xfr_Frm_Acct_Cde = pnd_Iatn403_In.newFieldArrayInGroup("pnd_Iatn403_In_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        pnd_Iatn403_In_Xfr_To_Acct_Cde = pnd_Iatn403_In.newFieldArrayInGroup("pnd_Iatn403_In_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 1, 
            new DbsArrayController(1,20));

        pnd_Iatn403_Out = dbsRecord.newGroupInRecord("pnd_Iatn403_Out", "#IATN403-OUT");
        pnd_Iatn403_Out.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn403_Out_Pnd_State_Approved = pnd_Iatn403_Out.newFieldInGroup("pnd_Iatn403_Out_Pnd_State_Approved", "#STATE-APPROVED", FieldType.BOOLEAN);
        pnd_Iatn403_Out_Pnd_Residence_State = pnd_Iatn403_Out.newFieldInGroup("pnd_Iatn403_Out_Pnd_Residence_State", "#RESIDENCE-STATE", FieldType.STRING, 
            2);
        pnd_Iatn403_Out_Pnd_Issue_State = pnd_Iatn403_Out.newFieldInGroup("pnd_Iatn403_Out_Pnd_Issue_State", "#ISSUE-STATE", FieldType.STRING, 2);
        pnd_Iatn403_Out_Pnd_To_Issue_State = pnd_Iatn403_Out.newFieldInGroup("pnd_Iatn403_Out_Pnd_To_Issue_State", "#TO-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_Iatn403_Out_Pnd_Return_Code = pnd_Iatn403_Out.newFieldInGroup("pnd_Iatn403_Out_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Iatn403_Out_Pnd_Msg = pnd_Iatn403_Out.newFieldInGroup("pnd_Iatn403_Out_Pnd_Msg", "#MSG", FieldType.STRING, 79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIatl403p(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

