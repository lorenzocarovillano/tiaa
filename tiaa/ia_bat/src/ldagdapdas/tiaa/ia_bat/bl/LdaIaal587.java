/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:19 PM
**        * FROM NATURAL LDA     : IAAL587
************************************************************
**        * FILE NAME            : LdaIaal587.java
**        * CLASS NAME           : LdaIaal587
**        * INSTANCE NAME        : LdaIaal587
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal587 extends DbsRecord
{
    // Properties
    private DbsField pnd_Audit_Reads;
    private DataAccessProgramView vw_audit_View;
    private DbsField audit_View_Paudit_Id_Nbr;
    private DbsField audit_View_Paudit_Tax_Id_Nbr;
    private DbsField audit_View_Paudit_Ppcn_Nbr;
    private DbsField audit_View_Paudit_Payee_Cde;
    private DbsField audit_View_Paudit_Req_Seq_Nbr;
    private DbsField audit_View_Paudit_Timestamp;
    private DbsField audit_View_Paudit_Status_Cde;
    private DbsField audit_View_Paudit_Status_Timestamp;
    private DbsField audit_View_Paudit_Pyee_Tax_Id_Nbr;
    private DbsField audit_View_Paudit_Pyee_Tax_Id_Typ;
    private DbsField audit_View_Paudit_Ctznshp_Cde;
    private DbsField audit_View_Paudit_Rsdncy_Cde;
    private DbsField audit_View_Paudit_Locality_Cde;
    private DbsField audit_View_Paudit_Ph_Name_First;
    private DbsField audit_View_Paudit_Ph_Name_Middle;
    private DbsField audit_View_Paudit_Ph_Name_Last;
    private DbsField audit_View_Paudit_Decedent_Name;
    private DbsField audit_View_Paudit_Dob_Dte;
    private DbsField audit_View_Paudit_Dod_Dte;
    private DbsField audit_View_Paudit_Proof_Dte;
    private DbsField audit_View_Paudit_Lst_Pymnt_Dte;
    private DbsField audit_View_Paudit_Notify_Dte;
    private DbsField audit_View_Paudit_Cycle_Dte;
    private DbsField audit_View_Paudit_Acctg_Dte;
    private DbsField audit_View_Paudit_Pymnt_Dte;
    private DbsField audit_View_Paudit_Intrfce_Dte;
    private DbsField audit_View_Paudit_Type_Req_Ind;
    private DbsField audit_View_Paudit_Type_Ind;
    private DbsField audit_View_Paudit_Roll_Dest_Cde;
    private DbsField audit_View_Paudit_Eft_Acct_Nbr;
    private DbsField audit_View_Paudit_Eft_Transit_Id;
    private DbsField audit_View_Paudit_Chk_Sav_Ind;
    private DbsField audit_View_Paudit_Hold_Cde;
    private DbsField audit_View_Count_Castpaudit_Rate_Tbl;
    private DbsGroup audit_View_Paudit_Rate_Tbl;
    private DbsField audit_View_Paudit_Rate_Basis;
    private DbsField audit_View_Paudit_Per_Pymnt;
    private DbsField audit_View_Paudit_Per_Dvdnd;
    private DbsField audit_View_Paudit_Per_Units;
    private DbsField audit_View_Count_Castpaudit_Installments;
    private DbsGroup audit_View_Paudit_Installments;
    private DbsField audit_View_Paudit_Instllmnt_Dte;
    private DbsField audit_View_Paudit_Instllmnt_Typ;
    private DbsField audit_View_Paudit_Instllmnt_Gross;
    private DbsField audit_View_Paudit_Instllmnt_Guar;
    private DbsField audit_View_Paudit_Instllmnt_Divd;
    private DbsField audit_View_Paudit_Instllmnt_Ivc;
    private DbsField audit_View_Paudit_Instllmnt_Dci;
    private DbsField audit_View_Paudit_Instllmnt_Units;
    private DbsField audit_View_Paudit_Instllmnt_Ovrpymnt;
    private DbsField audit_View_Paudit_Instllmnt_Futr_Ind;
    private DbsField audit_View_Paudit_Oper_Timestamp;
    private DbsField audit_View_Paudit_Inv_Acct_Cde;
    private DbsField audit_View_Paudit_Surv_Bene_Sex_Cde;
    private DbsField audit_View_Paudit_New_Acct_Ind;
    private DbsField audit_View_Paudit_Ovr_Prcnt_This_Pye;
    private DbsField audit_View_Paudit_Ovr_Amt_This_Pye;
    private DbsField audit_View_Paudit_Ovr_Method_Of_Rcvry_Cde;
    private DbsField audit_View_Paudit_Ovr_Pymnt_Rcvry_Per_Amt;
    private DbsField audit_View_Count_Castpaudit_Fund_Data;
    private DbsGroup audit_View_Paudit_Fund_Data;
    private DbsField audit_View_Paudit_Fund_Cde;
    private DbsField pnd_Audit_Time_Selects;
    private DbsField pnd_Payment_Records;
    private DbsField pnd_Diff_Accounting_Date;
    private DbsField pnd_M;
    private DbsField pnd_N;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Num;
    private DbsField pnd_W_Title_Variable1;
    private DbsField pnd_W_Title_Variable2;
    private DbsField pnd_Account_Error;
    private DbsField pnd_Eft_Hold;
    private DbsField pnd_Eft;
    private DbsField pnd_Check;
    private DbsField pnd_Tab_Total_Num_Of_Pay;
    private DbsField pnd_Tab_Total_Gross_Amt;
    private DbsField pnd_Tab_Cref_Gross;
    private DbsField pnd_Grand_Total_Num_Of_Pay;
    private DbsField pnd_Grand_Total_Gross_Amt;
    private DbsField pnd_Grand_Total_Tiaa_Gross_Amt;
    private DbsField pnd_Grand_Total_Cref_Gross_Amt;
    private DbsField pnd_Grand_Total_Real_Est_Gross_Amt;
    private DbsField pnd_Title_Variable1;
    private DbsField pnd_Title_Variable2;
    private DbsField pnd_Tab_Divd;
    private DbsField pnd_Tab_Guar;
    private DbsField pnd_Tab_Gross;
    private DbsField pnd_Tab_Gross_Eft_Hold;
    private DbsField pnd_Tab_Gross_Eft;
    private DbsField pnd_Tab_Gross_Chk;
    private DbsField pnd_Tab_Dci;
    private DbsField pnd_Tab_Ovrpy;
    private DbsField pnd_Tab_Num_Paym;
    private DbsField pnd_Tab_Num_Eft;
    private DbsField pnd_Tab_Num_Eft_Hold;
    private DbsField pnd_Tab_Num_Check;
    private DbsField pnd_Tab_Header;
    private DbsField pnd_Date_Ccyymmdd;
    private DbsGroup pnd_Date_CcyymmddRedef1;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Cc;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Ccyymmdd_Pnd_Date_Dd;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef2;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Date_NRedef3;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_AlphRedef4;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd;
    private DbsField pnd_Fl_Time_Hhiiss_Alph;
    private DbsGroup pnd_Fl_Time_Hhiiss_AlphRedef6;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num;
    private DbsGroup pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef8;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef10;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsGroup pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;

    public DbsField getPnd_Audit_Reads() { return pnd_Audit_Reads; }

    public DataAccessProgramView getVw_audit_View() { return vw_audit_View; }

    public DbsField getAudit_View_Paudit_Id_Nbr() { return audit_View_Paudit_Id_Nbr; }

    public DbsField getAudit_View_Paudit_Tax_Id_Nbr() { return audit_View_Paudit_Tax_Id_Nbr; }

    public DbsField getAudit_View_Paudit_Ppcn_Nbr() { return audit_View_Paudit_Ppcn_Nbr; }

    public DbsField getAudit_View_Paudit_Payee_Cde() { return audit_View_Paudit_Payee_Cde; }

    public DbsField getAudit_View_Paudit_Req_Seq_Nbr() { return audit_View_Paudit_Req_Seq_Nbr; }

    public DbsField getAudit_View_Paudit_Timestamp() { return audit_View_Paudit_Timestamp; }

    public DbsField getAudit_View_Paudit_Status_Cde() { return audit_View_Paudit_Status_Cde; }

    public DbsField getAudit_View_Paudit_Status_Timestamp() { return audit_View_Paudit_Status_Timestamp; }

    public DbsField getAudit_View_Paudit_Pyee_Tax_Id_Nbr() { return audit_View_Paudit_Pyee_Tax_Id_Nbr; }

    public DbsField getAudit_View_Paudit_Pyee_Tax_Id_Typ() { return audit_View_Paudit_Pyee_Tax_Id_Typ; }

    public DbsField getAudit_View_Paudit_Ctznshp_Cde() { return audit_View_Paudit_Ctznshp_Cde; }

    public DbsField getAudit_View_Paudit_Rsdncy_Cde() { return audit_View_Paudit_Rsdncy_Cde; }

    public DbsField getAudit_View_Paudit_Locality_Cde() { return audit_View_Paudit_Locality_Cde; }

    public DbsField getAudit_View_Paudit_Ph_Name_First() { return audit_View_Paudit_Ph_Name_First; }

    public DbsField getAudit_View_Paudit_Ph_Name_Middle() { return audit_View_Paudit_Ph_Name_Middle; }

    public DbsField getAudit_View_Paudit_Ph_Name_Last() { return audit_View_Paudit_Ph_Name_Last; }

    public DbsField getAudit_View_Paudit_Decedent_Name() { return audit_View_Paudit_Decedent_Name; }

    public DbsField getAudit_View_Paudit_Dob_Dte() { return audit_View_Paudit_Dob_Dte; }

    public DbsField getAudit_View_Paudit_Dod_Dte() { return audit_View_Paudit_Dod_Dte; }

    public DbsField getAudit_View_Paudit_Proof_Dte() { return audit_View_Paudit_Proof_Dte; }

    public DbsField getAudit_View_Paudit_Lst_Pymnt_Dte() { return audit_View_Paudit_Lst_Pymnt_Dte; }

    public DbsField getAudit_View_Paudit_Notify_Dte() { return audit_View_Paudit_Notify_Dte; }

    public DbsField getAudit_View_Paudit_Cycle_Dte() { return audit_View_Paudit_Cycle_Dte; }

    public DbsField getAudit_View_Paudit_Acctg_Dte() { return audit_View_Paudit_Acctg_Dte; }

    public DbsField getAudit_View_Paudit_Pymnt_Dte() { return audit_View_Paudit_Pymnt_Dte; }

    public DbsField getAudit_View_Paudit_Intrfce_Dte() { return audit_View_Paudit_Intrfce_Dte; }

    public DbsField getAudit_View_Paudit_Type_Req_Ind() { return audit_View_Paudit_Type_Req_Ind; }

    public DbsField getAudit_View_Paudit_Type_Ind() { return audit_View_Paudit_Type_Ind; }

    public DbsField getAudit_View_Paudit_Roll_Dest_Cde() { return audit_View_Paudit_Roll_Dest_Cde; }

    public DbsField getAudit_View_Paudit_Eft_Acct_Nbr() { return audit_View_Paudit_Eft_Acct_Nbr; }

    public DbsField getAudit_View_Paudit_Eft_Transit_Id() { return audit_View_Paudit_Eft_Transit_Id; }

    public DbsField getAudit_View_Paudit_Chk_Sav_Ind() { return audit_View_Paudit_Chk_Sav_Ind; }

    public DbsField getAudit_View_Paudit_Hold_Cde() { return audit_View_Paudit_Hold_Cde; }

    public DbsField getAudit_View_Count_Castpaudit_Rate_Tbl() { return audit_View_Count_Castpaudit_Rate_Tbl; }

    public DbsGroup getAudit_View_Paudit_Rate_Tbl() { return audit_View_Paudit_Rate_Tbl; }

    public DbsField getAudit_View_Paudit_Rate_Basis() { return audit_View_Paudit_Rate_Basis; }

    public DbsField getAudit_View_Paudit_Per_Pymnt() { return audit_View_Paudit_Per_Pymnt; }

    public DbsField getAudit_View_Paudit_Per_Dvdnd() { return audit_View_Paudit_Per_Dvdnd; }

    public DbsField getAudit_View_Paudit_Per_Units() { return audit_View_Paudit_Per_Units; }

    public DbsField getAudit_View_Count_Castpaudit_Installments() { return audit_View_Count_Castpaudit_Installments; }

    public DbsGroup getAudit_View_Paudit_Installments() { return audit_View_Paudit_Installments; }

    public DbsField getAudit_View_Paudit_Instllmnt_Dte() { return audit_View_Paudit_Instllmnt_Dte; }

    public DbsField getAudit_View_Paudit_Instllmnt_Typ() { return audit_View_Paudit_Instllmnt_Typ; }

    public DbsField getAudit_View_Paudit_Instllmnt_Gross() { return audit_View_Paudit_Instllmnt_Gross; }

    public DbsField getAudit_View_Paudit_Instllmnt_Guar() { return audit_View_Paudit_Instllmnt_Guar; }

    public DbsField getAudit_View_Paudit_Instllmnt_Divd() { return audit_View_Paudit_Instllmnt_Divd; }

    public DbsField getAudit_View_Paudit_Instllmnt_Ivc() { return audit_View_Paudit_Instllmnt_Ivc; }

    public DbsField getAudit_View_Paudit_Instllmnt_Dci() { return audit_View_Paudit_Instllmnt_Dci; }

    public DbsField getAudit_View_Paudit_Instllmnt_Units() { return audit_View_Paudit_Instllmnt_Units; }

    public DbsField getAudit_View_Paudit_Instllmnt_Ovrpymnt() { return audit_View_Paudit_Instllmnt_Ovrpymnt; }

    public DbsField getAudit_View_Paudit_Instllmnt_Futr_Ind() { return audit_View_Paudit_Instllmnt_Futr_Ind; }

    public DbsField getAudit_View_Paudit_Oper_Timestamp() { return audit_View_Paudit_Oper_Timestamp; }

    public DbsField getAudit_View_Paudit_Inv_Acct_Cde() { return audit_View_Paudit_Inv_Acct_Cde; }

    public DbsField getAudit_View_Paudit_Surv_Bene_Sex_Cde() { return audit_View_Paudit_Surv_Bene_Sex_Cde; }

    public DbsField getAudit_View_Paudit_New_Acct_Ind() { return audit_View_Paudit_New_Acct_Ind; }

    public DbsField getAudit_View_Paudit_Ovr_Prcnt_This_Pye() { return audit_View_Paudit_Ovr_Prcnt_This_Pye; }

    public DbsField getAudit_View_Paudit_Ovr_Amt_This_Pye() { return audit_View_Paudit_Ovr_Amt_This_Pye; }

    public DbsField getAudit_View_Paudit_Ovr_Method_Of_Rcvry_Cde() { return audit_View_Paudit_Ovr_Method_Of_Rcvry_Cde; }

    public DbsField getAudit_View_Paudit_Ovr_Pymnt_Rcvry_Per_Amt() { return audit_View_Paudit_Ovr_Pymnt_Rcvry_Per_Amt; }

    public DbsField getAudit_View_Count_Castpaudit_Fund_Data() { return audit_View_Count_Castpaudit_Fund_Data; }

    public DbsGroup getAudit_View_Paudit_Fund_Data() { return audit_View_Paudit_Fund_Data; }

    public DbsField getAudit_View_Paudit_Fund_Cde() { return audit_View_Paudit_Fund_Cde; }

    public DbsField getPnd_Audit_Time_Selects() { return pnd_Audit_Time_Selects; }

    public DbsField getPnd_Payment_Records() { return pnd_Payment_Records; }

    public DbsField getPnd_Diff_Accounting_Date() { return pnd_Diff_Accounting_Date; }

    public DbsField getPnd_M() { return pnd_M; }

    public DbsField getPnd_N() { return pnd_N; }

    public DbsField getPnd_I() { return pnd_I; }

    public DbsField getPnd_J() { return pnd_J; }

    public DbsField getPnd_Num() { return pnd_Num; }

    public DbsField getPnd_W_Title_Variable1() { return pnd_W_Title_Variable1; }

    public DbsField getPnd_W_Title_Variable2() { return pnd_W_Title_Variable2; }

    public DbsField getPnd_Account_Error() { return pnd_Account_Error; }

    public DbsField getPnd_Eft_Hold() { return pnd_Eft_Hold; }

    public DbsField getPnd_Eft() { return pnd_Eft; }

    public DbsField getPnd_Check() { return pnd_Check; }

    public DbsField getPnd_Tab_Total_Num_Of_Pay() { return pnd_Tab_Total_Num_Of_Pay; }

    public DbsField getPnd_Tab_Total_Gross_Amt() { return pnd_Tab_Total_Gross_Amt; }

    public DbsField getPnd_Tab_Cref_Gross() { return pnd_Tab_Cref_Gross; }

    public DbsField getPnd_Grand_Total_Num_Of_Pay() { return pnd_Grand_Total_Num_Of_Pay; }

    public DbsField getPnd_Grand_Total_Gross_Amt() { return pnd_Grand_Total_Gross_Amt; }

    public DbsField getPnd_Grand_Total_Tiaa_Gross_Amt() { return pnd_Grand_Total_Tiaa_Gross_Amt; }

    public DbsField getPnd_Grand_Total_Cref_Gross_Amt() { return pnd_Grand_Total_Cref_Gross_Amt; }

    public DbsField getPnd_Grand_Total_Real_Est_Gross_Amt() { return pnd_Grand_Total_Real_Est_Gross_Amt; }

    public DbsField getPnd_Title_Variable1() { return pnd_Title_Variable1; }

    public DbsField getPnd_Title_Variable2() { return pnd_Title_Variable2; }

    public DbsField getPnd_Tab_Divd() { return pnd_Tab_Divd; }

    public DbsField getPnd_Tab_Guar() { return pnd_Tab_Guar; }

    public DbsField getPnd_Tab_Gross() { return pnd_Tab_Gross; }

    public DbsField getPnd_Tab_Gross_Eft_Hold() { return pnd_Tab_Gross_Eft_Hold; }

    public DbsField getPnd_Tab_Gross_Eft() { return pnd_Tab_Gross_Eft; }

    public DbsField getPnd_Tab_Gross_Chk() { return pnd_Tab_Gross_Chk; }

    public DbsField getPnd_Tab_Dci() { return pnd_Tab_Dci; }

    public DbsField getPnd_Tab_Ovrpy() { return pnd_Tab_Ovrpy; }

    public DbsField getPnd_Tab_Num_Paym() { return pnd_Tab_Num_Paym; }

    public DbsField getPnd_Tab_Num_Eft() { return pnd_Tab_Num_Eft; }

    public DbsField getPnd_Tab_Num_Eft_Hold() { return pnd_Tab_Num_Eft_Hold; }

    public DbsField getPnd_Tab_Num_Check() { return pnd_Tab_Num_Check; }

    public DbsField getPnd_Tab_Header() { return pnd_Tab_Header; }

    public DbsField getPnd_Date_Ccyymmdd() { return pnd_Date_Ccyymmdd; }

    public DbsGroup getPnd_Date_CcyymmddRedef1() { return pnd_Date_CcyymmddRedef1; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Cc() { return pnd_Date_Ccyymmdd_Pnd_Date_Cc; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Yy() { return pnd_Date_Ccyymmdd_Pnd_Date_Yy; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Mm() { return pnd_Date_Ccyymmdd_Pnd_Date_Mm; }

    public DbsField getPnd_Date_Ccyymmdd_Pnd_Date_Dd() { return pnd_Date_Ccyymmdd_Pnd_Date_Dd; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef2() { return iaa_Parm_Card_Pnd_Parm_DateRedef2; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Date_NRedef3() { return iaa_Parm_Card_Pnd_Parm_Date_NRedef3; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph() { return pnd_Fl_Date_Yyyymmdd_Alph; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_AlphRedef4() { return pnd_Fl_Date_Yyyymmdd_AlphRedef4; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph() { return pnd_Fl_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_AlphRedef6() { return pnd_Fl_Time_Hhiiss_AlphRedef6; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef8() { return pnd_Sy_Date_Yymmdd_AlphRedef8; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef10() { return pnd_Sy_Time_Hhiiss_AlphRedef10; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsField getPnd_Sys_Time() { return pnd_Sys_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Audit_Reads = newFieldInRecord("pnd_Audit_Reads", "#AUDIT-READS", FieldType.NUMERIC, 9);

        vw_audit_View = new DataAccessProgramView(new NameInfo("vw_audit_View", "AUDIT-VIEW"), "IAA_DC_PMT_AUDIT", "IA_DEATH_CLAIMS", DdmPeriodicGroups.getInstance().getGroups("IAA_DC_PMT_AUDIT"));
        audit_View_Paudit_Id_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Id_Nbr", "PAUDIT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PAUDIT_ID_NBR");
        audit_View_Paudit_Tax_Id_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Tax_Id_Nbr", "PAUDIT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PAUDIT_TAX_ID_NBR");
        audit_View_Paudit_Ppcn_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ppcn_Nbr", "PAUDIT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "PAUDIT_PPCN_NBR");
        audit_View_Paudit_Payee_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Payee_Cde", "PAUDIT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PAUDIT_PAYEE_CDE");
        audit_View_Paudit_Req_Seq_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Req_Seq_Nbr", "PAUDIT-REQ-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PAUDIT_REQ_SEQ_NBR");
        audit_View_Paudit_Timestamp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Timestamp", "PAUDIT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "PAUDIT_TIMESTAMP");
        audit_View_Paudit_Status_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Status_Cde", "PAUDIT-STATUS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "PAUDIT_STATUS_CDE");
        audit_View_Paudit_Status_Timestamp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Status_Timestamp", "PAUDIT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PAUDIT_STATUS_TIMESTAMP");
        audit_View_Paudit_Pyee_Tax_Id_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Pyee_Tax_Id_Nbr", "PAUDIT-PYEE-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PAUDIT_PYEE_TAX_ID_NBR");
        audit_View_Paudit_Pyee_Tax_Id_Typ = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Pyee_Tax_Id_Typ", "PAUDIT-PYEE-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_PYEE_TAX_ID_TYP");
        audit_View_Paudit_Ctznshp_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ctznshp_Cde", "PAUDIT-CTZNSHP-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "PAUDIT_CTZNSHP_CDE");
        audit_View_Paudit_Rsdncy_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Rsdncy_Cde", "PAUDIT-RSDNCY-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "PAUDIT_RSDNCY_CDE");
        audit_View_Paudit_Locality_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Locality_Cde", "PAUDIT-LOCALITY-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "PAUDIT_LOCALITY_CDE");
        audit_View_Paudit_Ph_Name_First = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ph_Name_First", "PAUDIT-PH-NAME-FIRST", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_FIRST");
        audit_View_Paudit_Ph_Name_Middle = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ph_Name_Middle", "PAUDIT-PH-NAME-MIDDLE", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_MIDDLE");
        audit_View_Paudit_Ph_Name_Last = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ph_Name_Last", "PAUDIT-PH-NAME-LAST", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_LAST");
        audit_View_Paudit_Decedent_Name = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Decedent_Name", "PAUDIT-DECEDENT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "PAUDIT_DECEDENT_NAME");
        audit_View_Paudit_Dob_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Dob_Dte", "PAUDIT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "PAUDIT_DOB_DTE");
        audit_View_Paudit_Dod_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Dod_Dte", "PAUDIT-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "PAUDIT_DOD_DTE");
        audit_View_Paudit_Proof_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Proof_Dte", "PAUDIT-PROOF-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_PROOF_DTE");
        audit_View_Paudit_Lst_Pymnt_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Lst_Pymnt_Dte", "PAUDIT-LST-PYMNT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_LST_PYMNT_DTE");
        audit_View_Paudit_Notify_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Notify_Dte", "PAUDIT-NOTIFY-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_NOTIFY_DTE");
        audit_View_Paudit_Cycle_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Cycle_Dte", "PAUDIT-CYCLE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_CYCLE_DTE");
        audit_View_Paudit_Acctg_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Acctg_Dte", "PAUDIT-ACCTG-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_ACCTG_DTE");
        audit_View_Paudit_Pymnt_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Pymnt_Dte", "PAUDIT-PYMNT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_PYMNT_DTE");
        audit_View_Paudit_Intrfce_Dte = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Intrfce_Dte", "PAUDIT-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_INTRFCE_DTE");
        audit_View_Paudit_Type_Req_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Type_Req_Ind", "PAUDIT-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PAUDIT_TYPE_REQ_IND");
        audit_View_Paudit_Type_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Type_Ind", "PAUDIT-TYPE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PAUDIT_TYPE_IND");
        audit_View_Paudit_Roll_Dest_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Roll_Dest_Cde", "PAUDIT-ROLL-DEST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PAUDIT_ROLL_DEST_CDE");
        audit_View_Paudit_Eft_Acct_Nbr = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Eft_Acct_Nbr", "PAUDIT-EFT-ACCT-NBR", FieldType.STRING, 
            21, RepeatingFieldStrategy.None, "PAUDIT_EFT_ACCT_NBR");
        audit_View_Paudit_Eft_Transit_Id = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Eft_Transit_Id", "PAUDIT-EFT-TRANSIT-ID", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PAUDIT_EFT_TRANSIT_ID");
        audit_View_Paudit_Chk_Sav_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Chk_Sav_Ind", "PAUDIT-CHK-SAV-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_CHK_SAV_IND");
        audit_View_Paudit_Hold_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Hold_Cde", "PAUDIT-HOLD-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "PAUDIT_HOLD_CDE");
        audit_View_Count_Castpaudit_Rate_Tbl = vw_audit_View.getRecord().newFieldInGroup("audit_View_Count_Castpaudit_Rate_Tbl", "C*PAUDIT-RATE-TBL", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Paudit_Rate_Tbl = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Rate_Tbl", "PAUDIT-RATE-TBL", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Paudit_Rate_Basis = audit_View_Paudit_Rate_Tbl.newFieldArrayInGroup("audit_View_Paudit_Rate_Basis", "PAUDIT-RATE-BASIS", FieldType.NUMERIC, 
            2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_RATE_BASIS", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Paudit_Per_Pymnt = audit_View_Paudit_Rate_Tbl.newFieldArrayInGroup("audit_View_Paudit_Per_Pymnt", "PAUDIT-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_PYMNT", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Paudit_Per_Dvdnd = audit_View_Paudit_Rate_Tbl.newFieldArrayInGroup("audit_View_Paudit_Per_Dvdnd", "PAUDIT-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_DVDND", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Paudit_Per_Units = audit_View_Paudit_Rate_Tbl.newFieldArrayInGroup("audit_View_Paudit_Per_Units", "PAUDIT-PER-UNITS", FieldType.PACKED_DECIMAL, 
            11, 4, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_UNITS", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        audit_View_Count_Castpaudit_Installments = vw_audit_View.getRecord().newFieldInGroup("audit_View_Count_Castpaudit_Installments", "C*PAUDIT-INSTALLMENTS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Installments = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Installments", "PAUDIT-INSTALLMENTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Dte = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Dte", "PAUDIT-INSTLLMNT-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DTE", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Typ = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Typ", "PAUDIT-INSTLLMNT-TYP", 
            FieldType.STRING, 4, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_TYP", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Gross = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Gross", "PAUDIT-INSTLLMNT-GROSS", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_GROSS", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Guar = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Guar", "PAUDIT-INSTLLMNT-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_GUAR", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Divd = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Divd", "PAUDIT-INSTLLMNT-DIVD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DIVD", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Ivc = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Ivc", "PAUDIT-INSTLLMNT-IVC", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_IVC", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Dci = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Dci", "PAUDIT-INSTLLMNT-DCI", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DCI", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Units = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Units", "PAUDIT-INSTLLMNT-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_UNITS", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Ovrpymnt = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Ovrpymnt", "PAUDIT-INSTLLMNT-OVRPYMNT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_OVRPYMNT", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Instllmnt_Futr_Ind = audit_View_Paudit_Installments.newFieldArrayInGroup("audit_View_Paudit_Instllmnt_Futr_Ind", "PAUDIT-INSTLLMNT-FUTR-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_FUTR_IND", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        audit_View_Paudit_Oper_Timestamp = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Oper_Timestamp", "PAUDIT-OPER-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "FETV_OPER_TIMESTAMP");
        audit_View_Paudit_Inv_Acct_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Inv_Acct_Cde", "PAUDIT-INV-ACCT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_INV_ACCT_CDE");
        audit_View_Paudit_Surv_Bene_Sex_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Surv_Bene_Sex_Cde", "PAUDIT-SURV-BENE-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "PAYMNT_SURV_BENE_SEX_CDE");
        audit_View_Paudit_New_Acct_Ind = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_New_Acct_Ind", "PAUDIT-NEW-ACCT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_NEW_ACCT_IND");
        audit_View_Paudit_Ovr_Prcnt_This_Pye = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ovr_Prcnt_This_Pye", "PAUDIT-OVR-PRCNT-THIS-PYE", 
            FieldType.NUMERIC, 7, 4, RepeatingFieldStrategy.None, "PAUDIT_OVR_PRCNT_THIS_PYE");
        audit_View_Paudit_Ovr_Amt_This_Pye = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ovr_Amt_This_Pye", "PAUDIT-OVR-AMT-THIS-PYE", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "PAUDIT_OVR_AMT_THIS_PYE");
        audit_View_Paudit_Ovr_Method_Of_Rcvry_Cde = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ovr_Method_Of_Rcvry_Cde", "PAUDIT-OVR-METHOD-OF-RCVRY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PAUDIT_OVR_METHOD_OF_RCVRY_CDE");
        audit_View_Paudit_Ovr_Pymnt_Rcvry_Per_Amt = vw_audit_View.getRecord().newFieldInGroup("audit_View_Paudit_Ovr_Pymnt_Rcvry_Per_Amt", "PAUDIT-OVR-PYMNT-RCVRY-PER-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "PAUDIT_OVR_PYMNT_RCVRY_PER_AMT");
        audit_View_Count_Castpaudit_Fund_Data = vw_audit_View.getRecord().newFieldInGroup("audit_View_Count_Castpaudit_Fund_Data", "C*PAUDIT-FUND-DATA", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_PAUDIT_FUND_DATA");
        audit_View_Paudit_Fund_Data = vw_audit_View.getRecord().newGroupInGroup("audit_View_Paudit_Fund_Data", "PAUDIT-FUND-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_DEATH_CLAIMS_PAUDIT_FUND_DATA");
        audit_View_Paudit_Fund_Cde = audit_View_Paudit_Fund_Data.newFieldArrayInGroup("audit_View_Paudit_Fund_Cde", "PAUDIT-FUND-CDE", FieldType.STRING, 
            1, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_FUND_CDE", "IA_DEATH_CLAIMS_PAUDIT_FUND_DATA");

        pnd_Audit_Time_Selects = newFieldInRecord("pnd_Audit_Time_Selects", "#AUDIT-TIME-SELECTS", FieldType.NUMERIC, 9);

        pnd_Payment_Records = newFieldInRecord("pnd_Payment_Records", "#PAYMENT-RECORDS", FieldType.NUMERIC, 9);

        pnd_Diff_Accounting_Date = newFieldInRecord("pnd_Diff_Accounting_Date", "#DIFF-ACCOUNTING-DATE", FieldType.STRING, 1);

        pnd_M = newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 2);

        pnd_N = newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 2);

        pnd_I = newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        pnd_J = newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);

        pnd_Num = newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 2);

        pnd_W_Title_Variable1 = newFieldInRecord("pnd_W_Title_Variable1", "#W-TITLE-VARIABLE1", FieldType.STRING, 8);

        pnd_W_Title_Variable2 = newFieldInRecord("pnd_W_Title_Variable2", "#W-TITLE-VARIABLE2", FieldType.STRING, 8);

        pnd_Account_Error = newFieldInRecord("pnd_Account_Error", "#ACCOUNT-ERROR", FieldType.STRING, 1);

        pnd_Eft_Hold = newFieldInRecord("pnd_Eft_Hold", "#EFT-HOLD", FieldType.STRING, 1);

        pnd_Eft = newFieldInRecord("pnd_Eft", "#EFT", FieldType.STRING, 1);

        pnd_Check = newFieldInRecord("pnd_Check", "#CHECK", FieldType.STRING, 1);

        pnd_Tab_Total_Num_Of_Pay = newFieldArrayInRecord("pnd_Tab_Total_Num_Of_Pay", "#TAB-TOTAL-NUM-OF-PAY", FieldType.NUMERIC, 4, new DbsArrayController(1,
            2));

        pnd_Tab_Total_Gross_Amt = newFieldArrayInRecord("pnd_Tab_Total_Gross_Amt", "#TAB-TOTAL-GROSS-AMT", FieldType.DECIMAL, 12,2, new DbsArrayController(1,
            2));

        pnd_Tab_Cref_Gross = newFieldArrayInRecord("pnd_Tab_Cref_Gross", "#TAB-CREF-GROSS", FieldType.DECIMAL, 12,2, new DbsArrayController(1,2));

        pnd_Grand_Total_Num_Of_Pay = newFieldInRecord("pnd_Grand_Total_Num_Of_Pay", "#GRAND-TOTAL-NUM-OF-PAY", FieldType.NUMERIC, 4);

        pnd_Grand_Total_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Gross_Amt", "#GRAND-TOTAL-GROSS-AMT", FieldType.DECIMAL, 12,2);

        pnd_Grand_Total_Tiaa_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Tiaa_Gross_Amt", "#GRAND-TOTAL-TIAA-GROSS-AMT", FieldType.DECIMAL, 12,2);

        pnd_Grand_Total_Cref_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Cref_Gross_Amt", "#GRAND-TOTAL-CREF-GROSS-AMT", FieldType.DECIMAL, 12,2);

        pnd_Grand_Total_Real_Est_Gross_Amt = newFieldInRecord("pnd_Grand_Total_Real_Est_Gross_Amt", "#GRAND-TOTAL-REAL-EST-GROSS-AMT", FieldType.DECIMAL, 
            12,2);

        pnd_Title_Variable1 = newFieldArrayInRecord("pnd_Title_Variable1", "#TITLE-VARIABLE1", FieldType.NUMERIC, 8, new DbsArrayController(1,2));

        pnd_Title_Variable2 = newFieldArrayInRecord("pnd_Title_Variable2", "#TITLE-VARIABLE2", FieldType.NUMERIC, 8, new DbsArrayController(1,2));

        pnd_Tab_Divd = newFieldArrayInRecord("pnd_Tab_Divd", "#TAB-DIVD", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Guar = newFieldArrayInRecord("pnd_Tab_Guar", "#TAB-GUAR", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Gross = newFieldArrayInRecord("pnd_Tab_Gross", "#TAB-GROSS", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Gross_Eft_Hold = newFieldArrayInRecord("pnd_Tab_Gross_Eft_Hold", "#TAB-GROSS-EFT-HOLD", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            2,1,60));

        pnd_Tab_Gross_Eft = newFieldArrayInRecord("pnd_Tab_Gross_Eft", "#TAB-GROSS-EFT", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Gross_Chk = newFieldArrayInRecord("pnd_Tab_Gross_Chk", "#TAB-GROSS-CHK", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Dci = newFieldArrayInRecord("pnd_Tab_Dci", "#TAB-DCI", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Ovrpy = newFieldArrayInRecord("pnd_Tab_Ovrpy", "#TAB-OVRPY", FieldType.DECIMAL, 11,2, new DbsArrayController(1,2,1,60));

        pnd_Tab_Num_Paym = newFieldArrayInRecord("pnd_Tab_Num_Paym", "#TAB-NUM-PAYM", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Num_Eft = newFieldArrayInRecord("pnd_Tab_Num_Eft", "#TAB-NUM-EFT", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Num_Eft_Hold = newFieldArrayInRecord("pnd_Tab_Num_Eft_Hold", "#TAB-NUM-EFT-HOLD", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Num_Check = newFieldArrayInRecord("pnd_Tab_Num_Check", "#TAB-NUM-CHECK", FieldType.NUMERIC, 3, new DbsArrayController(1,2));

        pnd_Tab_Header = newFieldArrayInRecord("pnd_Tab_Header", "#TAB-HEADER", FieldType.STRING, 12, new DbsArrayController(1,12));

        pnd_Date_Ccyymmdd = newFieldInRecord("pnd_Date_Ccyymmdd", "#DATE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Date_CcyymmddRedef1 = newGroupInRecord("pnd_Date_CcyymmddRedef1", "Redefines", pnd_Date_Ccyymmdd);
        pnd_Date_Ccyymmdd_Pnd_Date_Cc = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Yy = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Mm = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Ccyymmdd_Pnd_Date_Dd = pnd_Date_CcyymmddRedef1.newFieldInGroup("pnd_Date_Ccyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef2 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef2", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_Date_NRedef3 = iaa_Parm_Card_Pnd_Parm_DateRedef2.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Date_NRedef3", "Redefines", iaa_Parm_Card_Pnd_Parm_Date_N);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_Date_NRedef3.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_Date_NRedef3.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_Date_NRedef3.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_Date_NRedef3.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Fl_Date_Yyyymmdd_Alph = newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Fl_Date_Yyyymmdd_AlphRedef4 = newGroupInRecord("pnd_Fl_Date_Yyyymmdd_AlphRedef4", "Redefines", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_AlphRedef4.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5 = pnd_Fl_Date_Yyyymmdd_AlphRedef4.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5", 
            "Redefines", pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef5.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd", 
            "#FL-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Fl_Time_Hhiiss_Alph = newFieldInRecord("pnd_Fl_Time_Hhiiss_Alph", "#FL-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Fl_Time_Hhiiss_AlphRedef6 = newGroupInRecord("pnd_Fl_Time_Hhiiss_AlphRedef6", "Redefines", pnd_Fl_Time_Hhiiss_Alph);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num = pnd_Fl_Time_Hhiiss_AlphRedef6.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num", 
            "#FL-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7 = pnd_Fl_Time_Hhiiss_AlphRedef6.newGroupInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7", 
            "Redefines", pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh", 
            "#FL-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii", 
            "#FL-TIME-II", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef7.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss", 
            "#FL-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef8 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef8", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef8.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9 = pnd_Sy_Date_Yymmdd_AlphRedef8.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef9.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef10 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef10", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef10.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11 = pnd_Sy_Time_Hhiiss_AlphRedef10.newGroupInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11", 
            "Redefines", pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh", 
            "#SY-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii", 
            "#SY-TIME-II", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef11.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss", 
            "#SY-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        pnd_Sys_Time = newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        vw_audit_View.setUniquePeList();

        this.setRecordName("LdaIaal587");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_audit_View.reset();
    }

    // Constructor
    public LdaIaal587() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
