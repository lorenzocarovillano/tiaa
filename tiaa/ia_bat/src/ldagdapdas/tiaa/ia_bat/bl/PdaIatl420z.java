/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:52 PM
**        * FROM NATURAL PDA     : IATL420Z
************************************************************
**        * FILE NAME            : PdaIatl420z.java
**        * CLASS NAME           : PdaIatl420z
**        * INSTANCE NAME        : PdaIatl420z
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaIatl420z extends PdaBase
{
    // Properties
    private DbsGroup iatl010;
    private DbsField iatl010_Pnd_Sys_Time;
    private DbsField iatl010_Pnd_Todays_Dte;
    private DbsField iatl010_Pnd_Next_Bus_Dte;
    private DbsField iatl010_Pnd_Check_Dte;
    private DbsGroup iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst;
    private DbsField iatl010_Rcrd_Type_Cde;
    private DbsField iatl010_Rqst_Id;
    private DbsField iatl010_Rqst_Effctv_Dte;
    private DbsField iatl010_Rqst_Entry_Dte;
    private DbsField iatl010_Rqst_Entry_Tme;
    private DbsField iatl010_Rqst_Lst_Actvty_Dte;
    private DbsField iatl010_Rqst_Rcvd_Dte;
    private DbsField iatl010_Rqst_Rcvd_Tme;
    private DbsField iatl010_Rqst_Rcvd_User_Id;
    private DbsField iatl010_Rqst_Entry_User_Id;
    private DbsField iatl010_Rqst_Cntct_Mde;
    private DbsField iatl010_Rqst_Srce;
    private DbsField iatl010_Rqst_Rep_Nme;
    private DbsField iatl010_Rqst_Sttmnt_Ind;
    private DbsField iatl010_Rqst_Opn_Clsd_Ind;
    private DbsField iatl010_Rqst_Rcprcl_Dte;
    private DbsField iatl010_Rqst_Ssn;
    private DbsField iatl010_Xfr_Work_Prcss_Id;
    private DbsField iatl010_Xfr_Mit_Log_Dte_Tme;
    private DbsField iatl010_Xfr_Stts_Cde;
    private DbsField iatl010_Xfr_Rjctn_Cde;
    private DbsField iatl010_Xfr_In_Progress_Ind;
    private DbsField iatl010_Xfr_Retry_Cnt;
    private DbsField iatl010_Ia_Frm_Cntrct;
    private DbsField iatl010_Ia_Frm_Payee;
    private DbsGroup iatl010_Ia_Frm_PayeeRedef1;
    private DbsField iatl010_Pnd_Ia_Frm_Payee_N;
    private DbsField iatl010_Ia_Unique_Id;
    private DbsField iatl010_Ia_Hold_Cde;
    private DbsField iatl010_Cpnd_Xfr_Frm_Acct_Dta;
    private DbsField iatl010_Xfr_Frm_Acct_Cde;
    private DbsField iatl010_Xfr_Frm_Unit_Typ;
    private DbsField iatl010_Xfr_Frm_Typ;
    private DbsField iatl010_Xfr_Frm_Qty;
    private DbsField iatl010_Xfr_Frm_Est_Amt;
    private DbsField iatl010_Xfr_Frm_Asset_Amt;
    private DbsField iatl010_Cpnd_Xfr_To_Acct_Dta;
    private DbsField iatl010_Xfr_To_Acct_Cde;
    private DbsField iatl010_Xfr_To_Unit_Typ;
    private DbsField iatl010_Xfr_To_Typ;
    private DbsField iatl010_Xfr_To_Qty;
    private DbsField iatl010_Xfr_To_Est_Amt;
    private DbsField iatl010_Xfr_To_Asset_Amt;
    private DbsField iatl010_Ia_To_Cntrct;
    private DbsField iatl010_Ia_To_Payee;
    private DbsGroup iatl010_Ia_To_PayeeRedef2;
    private DbsField iatl010_Pnd_Ia_To_Payee_N;
    private DbsField iatl010_Ia_Appl_Rcvd_Dte;
    private DbsField iatl010_Ia_New_Issue;
    private DbsField iatl010_Ia_Rsn_For_Ovrrde;
    private DbsField iatl010_Ia_Ovrrde_User_Id;
    private DbsField iatl010_Ia_Gnrl_Pend_Cde;
    private DbsField iatl010_Rqst_Xfr_Type;
    private DbsField iatl010_Rqst_Unit_Cde;
    private DbsField iatl010_Ia_New_Iss_Prt_Pull;
    private DbsGroup pnd_Iatn420_Out;
    private DbsField pnd_Iatn420_Out_Pnd_Return_Code;
    private DbsField pnd_Iatn420_Out_Pnd_Msg;

    public DbsGroup getIatl010() { return iatl010; }

    public DbsField getIatl010_Pnd_Sys_Time() { return iatl010_Pnd_Sys_Time; }

    public DbsField getIatl010_Pnd_Todays_Dte() { return iatl010_Pnd_Todays_Dte; }

    public DbsField getIatl010_Pnd_Next_Bus_Dte() { return iatl010_Pnd_Next_Bus_Dte; }

    public DbsField getIatl010_Pnd_Check_Dte() { return iatl010_Pnd_Check_Dte; }

    public DbsGroup getIatl010_Pnd_Iaa_Trnsfr_Sw_Rqst() { return iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst; }

    public DbsField getIatl010_Rcrd_Type_Cde() { return iatl010_Rcrd_Type_Cde; }

    public DbsField getIatl010_Rqst_Id() { return iatl010_Rqst_Id; }

    public DbsField getIatl010_Rqst_Effctv_Dte() { return iatl010_Rqst_Effctv_Dte; }

    public DbsField getIatl010_Rqst_Entry_Dte() { return iatl010_Rqst_Entry_Dte; }

    public DbsField getIatl010_Rqst_Entry_Tme() { return iatl010_Rqst_Entry_Tme; }

    public DbsField getIatl010_Rqst_Lst_Actvty_Dte() { return iatl010_Rqst_Lst_Actvty_Dte; }

    public DbsField getIatl010_Rqst_Rcvd_Dte() { return iatl010_Rqst_Rcvd_Dte; }

    public DbsField getIatl010_Rqst_Rcvd_Tme() { return iatl010_Rqst_Rcvd_Tme; }

    public DbsField getIatl010_Rqst_Rcvd_User_Id() { return iatl010_Rqst_Rcvd_User_Id; }

    public DbsField getIatl010_Rqst_Entry_User_Id() { return iatl010_Rqst_Entry_User_Id; }

    public DbsField getIatl010_Rqst_Cntct_Mde() { return iatl010_Rqst_Cntct_Mde; }

    public DbsField getIatl010_Rqst_Srce() { return iatl010_Rqst_Srce; }

    public DbsField getIatl010_Rqst_Rep_Nme() { return iatl010_Rqst_Rep_Nme; }

    public DbsField getIatl010_Rqst_Sttmnt_Ind() { return iatl010_Rqst_Sttmnt_Ind; }

    public DbsField getIatl010_Rqst_Opn_Clsd_Ind() { return iatl010_Rqst_Opn_Clsd_Ind; }

    public DbsField getIatl010_Rqst_Rcprcl_Dte() { return iatl010_Rqst_Rcprcl_Dte; }

    public DbsField getIatl010_Rqst_Ssn() { return iatl010_Rqst_Ssn; }

    public DbsField getIatl010_Xfr_Work_Prcss_Id() { return iatl010_Xfr_Work_Prcss_Id; }

    public DbsField getIatl010_Xfr_Mit_Log_Dte_Tme() { return iatl010_Xfr_Mit_Log_Dte_Tme; }

    public DbsField getIatl010_Xfr_Stts_Cde() { return iatl010_Xfr_Stts_Cde; }

    public DbsField getIatl010_Xfr_Rjctn_Cde() { return iatl010_Xfr_Rjctn_Cde; }

    public DbsField getIatl010_Xfr_In_Progress_Ind() { return iatl010_Xfr_In_Progress_Ind; }

    public DbsField getIatl010_Xfr_Retry_Cnt() { return iatl010_Xfr_Retry_Cnt; }

    public DbsField getIatl010_Ia_Frm_Cntrct() { return iatl010_Ia_Frm_Cntrct; }

    public DbsField getIatl010_Ia_Frm_Payee() { return iatl010_Ia_Frm_Payee; }

    public DbsGroup getIatl010_Ia_Frm_PayeeRedef1() { return iatl010_Ia_Frm_PayeeRedef1; }

    public DbsField getIatl010_Pnd_Ia_Frm_Payee_N() { return iatl010_Pnd_Ia_Frm_Payee_N; }

    public DbsField getIatl010_Ia_Unique_Id() { return iatl010_Ia_Unique_Id; }

    public DbsField getIatl010_Ia_Hold_Cde() { return iatl010_Ia_Hold_Cde; }

    public DbsField getIatl010_Cpnd_Xfr_Frm_Acct_Dta() { return iatl010_Cpnd_Xfr_Frm_Acct_Dta; }

    public DbsField getIatl010_Xfr_Frm_Acct_Cde() { return iatl010_Xfr_Frm_Acct_Cde; }

    public DbsField getIatl010_Xfr_Frm_Unit_Typ() { return iatl010_Xfr_Frm_Unit_Typ; }

    public DbsField getIatl010_Xfr_Frm_Typ() { return iatl010_Xfr_Frm_Typ; }

    public DbsField getIatl010_Xfr_Frm_Qty() { return iatl010_Xfr_Frm_Qty; }

    public DbsField getIatl010_Xfr_Frm_Est_Amt() { return iatl010_Xfr_Frm_Est_Amt; }

    public DbsField getIatl010_Xfr_Frm_Asset_Amt() { return iatl010_Xfr_Frm_Asset_Amt; }

    public DbsField getIatl010_Cpnd_Xfr_To_Acct_Dta() { return iatl010_Cpnd_Xfr_To_Acct_Dta; }

    public DbsField getIatl010_Xfr_To_Acct_Cde() { return iatl010_Xfr_To_Acct_Cde; }

    public DbsField getIatl010_Xfr_To_Unit_Typ() { return iatl010_Xfr_To_Unit_Typ; }

    public DbsField getIatl010_Xfr_To_Typ() { return iatl010_Xfr_To_Typ; }

    public DbsField getIatl010_Xfr_To_Qty() { return iatl010_Xfr_To_Qty; }

    public DbsField getIatl010_Xfr_To_Est_Amt() { return iatl010_Xfr_To_Est_Amt; }

    public DbsField getIatl010_Xfr_To_Asset_Amt() { return iatl010_Xfr_To_Asset_Amt; }

    public DbsField getIatl010_Ia_To_Cntrct() { return iatl010_Ia_To_Cntrct; }

    public DbsField getIatl010_Ia_To_Payee() { return iatl010_Ia_To_Payee; }

    public DbsGroup getIatl010_Ia_To_PayeeRedef2() { return iatl010_Ia_To_PayeeRedef2; }

    public DbsField getIatl010_Pnd_Ia_To_Payee_N() { return iatl010_Pnd_Ia_To_Payee_N; }

    public DbsField getIatl010_Ia_Appl_Rcvd_Dte() { return iatl010_Ia_Appl_Rcvd_Dte; }

    public DbsField getIatl010_Ia_New_Issue() { return iatl010_Ia_New_Issue; }

    public DbsField getIatl010_Ia_Rsn_For_Ovrrde() { return iatl010_Ia_Rsn_For_Ovrrde; }

    public DbsField getIatl010_Ia_Ovrrde_User_Id() { return iatl010_Ia_Ovrrde_User_Id; }

    public DbsField getIatl010_Ia_Gnrl_Pend_Cde() { return iatl010_Ia_Gnrl_Pend_Cde; }

    public DbsField getIatl010_Rqst_Xfr_Type() { return iatl010_Rqst_Xfr_Type; }

    public DbsField getIatl010_Rqst_Unit_Cde() { return iatl010_Rqst_Unit_Cde; }

    public DbsField getIatl010_Ia_New_Iss_Prt_Pull() { return iatl010_Ia_New_Iss_Prt_Pull; }

    public DbsGroup getPnd_Iatn420_Out() { return pnd_Iatn420_Out; }

    public DbsField getPnd_Iatn420_Out_Pnd_Return_Code() { return pnd_Iatn420_Out_Pnd_Return_Code; }

    public DbsField getPnd_Iatn420_Out_Pnd_Msg() { return pnd_Iatn420_Out_Pnd_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iatl010 = dbsRecord.newGroupInRecord("iatl010", "IATL010");
        iatl010.setParameterOption(ParameterOption.ByReference);
        iatl010_Pnd_Sys_Time = iatl010.newFieldInGroup("iatl010_Pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        iatl010_Pnd_Todays_Dte = iatl010.newFieldInGroup("iatl010_Pnd_Todays_Dte", "#TODAYS-DTE", FieldType.DATE);
        iatl010_Pnd_Next_Bus_Dte = iatl010.newFieldInGroup("iatl010_Pnd_Next_Bus_Dte", "#NEXT-BUS-DTE", FieldType.DATE);
        iatl010_Pnd_Check_Dte = iatl010.newFieldInGroup("iatl010_Pnd_Check_Dte", "#CHECK-DTE", FieldType.DATE);
        iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst = iatl010.newGroupInGroup("iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst", "#IAA-TRNSFR-SW-RQST");
        iatl010_Rcrd_Type_Cde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1);
        iatl010_Rqst_Id = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Id", "RQST-ID", FieldType.STRING, 34);
        iatl010_Rqst_Effctv_Dte = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE);
        iatl010_Rqst_Entry_Dte = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE);
        iatl010_Rqst_Entry_Tme = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 7);
        iatl010_Rqst_Lst_Actvty_Dte = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", FieldType.DATE);
        iatl010_Rqst_Rcvd_Dte = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE);
        iatl010_Rqst_Rcvd_Tme = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 7);
        iatl010_Rqst_Rcvd_User_Id = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", FieldType.STRING, 
            8);
        iatl010_Rqst_Entry_User_Id = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 
            8);
        iatl010_Rqst_Cntct_Mde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 1);
        iatl010_Rqst_Srce = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 1);
        iatl010_Rqst_Rep_Nme = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 40);
        iatl010_Rqst_Sttmnt_Ind = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", FieldType.STRING, 1);
        iatl010_Rqst_Opn_Clsd_Ind = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", FieldType.STRING, 
            1);
        iatl010_Rqst_Rcprcl_Dte = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", FieldType.NUMERIC, 8);
        iatl010_Rqst_Ssn = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 9);
        iatl010_Xfr_Work_Prcss_Id = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        iatl010_Xfr_Mit_Log_Dte_Tme = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 
            15);
        iatl010_Xfr_Stts_Cde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2);
        iatl010_Xfr_Rjctn_Cde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2);
        iatl010_Xfr_In_Progress_Ind = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", FieldType.BOOLEAN);
        iatl010_Xfr_Retry_Cnt = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 1);
        iatl010_Ia_Frm_Cntrct = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10);
        iatl010_Ia_Frm_Payee = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2);
        iatl010_Ia_Frm_PayeeRedef1 = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newGroupInGroup("iatl010_Ia_Frm_PayeeRedef1", "Redefines", iatl010_Ia_Frm_Payee);
        iatl010_Pnd_Ia_Frm_Payee_N = iatl010_Ia_Frm_PayeeRedef1.newFieldInGroup("iatl010_Pnd_Ia_Frm_Payee_N", "#IA-FRM-PAYEE-N", FieldType.NUMERIC, 2);
        iatl010_Ia_Unique_Id = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12);
        iatl010_Ia_Hold_Cde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 8);
        iatl010_Cpnd_Xfr_Frm_Acct_Dta = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Cpnd_Xfr_Frm_Acct_Dta", "C#XFR-FRM-ACCT-DTA", FieldType.NUMERIC, 
            3);
        iatl010_Xfr_Frm_Acct_Cde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        iatl010_Xfr_Frm_Unit_Typ = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        iatl010_Xfr_Frm_Typ = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1, new DbsArrayController(1,
            20));
        iatl010_Xfr_Frm_Qty = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 9,2, 
            new DbsArrayController(1,20));
        iatl010_Xfr_Frm_Est_Amt = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        iatl010_Xfr_Frm_Asset_Amt = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11,2, new DbsArrayController(1,20));
        iatl010_Cpnd_Xfr_To_Acct_Dta = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Cpnd_Xfr_To_Acct_Dta", "C#XFR-TO-ACCT-DTA", FieldType.NUMERIC, 
            3);
        iatl010_Xfr_To_Acct_Cde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        iatl010_Xfr_To_Unit_Typ = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        iatl010_Xfr_To_Typ = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1, new DbsArrayController(1,
            20));
        iatl010_Xfr_To_Qty = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9,2, new 
            DbsArrayController(1,20));
        iatl010_Xfr_To_Est_Amt = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        iatl010_Xfr_To_Asset_Amt = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldArrayInGroup("iatl010_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11,2, new DbsArrayController(1,20));
        iatl010_Ia_To_Cntrct = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10);
        iatl010_Ia_To_Payee = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2);
        iatl010_Ia_To_PayeeRedef2 = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newGroupInGroup("iatl010_Ia_To_PayeeRedef2", "Redefines", iatl010_Ia_To_Payee);
        iatl010_Pnd_Ia_To_Payee_N = iatl010_Ia_To_PayeeRedef2.newFieldInGroup("iatl010_Pnd_Ia_To_Payee_N", "#IA-TO-PAYEE-N", FieldType.NUMERIC, 2);
        iatl010_Ia_Appl_Rcvd_Dte = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", FieldType.DATE);
        iatl010_Ia_New_Issue = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 1);
        iatl010_Ia_Rsn_For_Ovrrde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", FieldType.STRING, 
            2);
        iatl010_Ia_Ovrrde_User_Id = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", FieldType.STRING, 
            8);
        iatl010_Ia_Gnrl_Pend_Cde = iatl010_Pnd_Iaa_Trnsfr_Sw_Rqst.newFieldInGroup("iatl010_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", FieldType.STRING, 1);
        iatl010_Rqst_Xfr_Type = iatl010.newFieldInGroup("iatl010_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1);
        iatl010_Rqst_Unit_Cde = iatl010.newFieldInGroup("iatl010_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8);
        iatl010_Ia_New_Iss_Prt_Pull = iatl010.newFieldInGroup("iatl010_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 1);

        pnd_Iatn420_Out = dbsRecord.newGroupInRecord("pnd_Iatn420_Out", "#IATN420-OUT");
        pnd_Iatn420_Out.setParameterOption(ParameterOption.ByReference);
        pnd_Iatn420_Out_Pnd_Return_Code = pnd_Iatn420_Out.newFieldInGroup("pnd_Iatn420_Out_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Iatn420_Out_Pnd_Msg = pnd_Iatn420_Out.newFieldInGroup("pnd_Iatn420_Out_Pnd_Msg", "#MSG", FieldType.STRING, 79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaIatl420z(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

