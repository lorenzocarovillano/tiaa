/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:26 PM
**        * FROM NATURAL LDA     : AIAL013B
************************************************************
**        * FILE NAME            : LdaAial013b.java
**        * CLASS NAME           : LdaAial013b
**        * INSTANCE NAME        : LdaAial013b
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial013b extends DbsRecord
{
    // Properties
    private DbsField pnd_Store_Actrl_Fld_1;
    private DbsGroup pnd_Store_Actrl_Fld_1Redef1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Contract_Payee_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Fund_Code_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Mode_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Option_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Issue_Date_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Final_Per_Pay_Date_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dob_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Sex_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dod_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dob_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Sex_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dod_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Transfer_Effective_Date_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Type_Of_Run_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Processing_Date_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Illustration_Eff_Date_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1;
    private DbsGroup pnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Fund_To_Receive_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Units_To_Receive_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Pmts_To_Receive_S1;
    private DbsField pnd_Store_Actrl_Fld_1_Pnd_Filler6;
    private DbsField pnd_Store_Actrl_Fld_2;
    private DbsGroup pnd_Store_Actrl_Fld_2Redef2;
    private DbsGroup pnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2;
    private DbsField pnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2;
    private DbsField pnd_Store_Actrl_Fld_2_Pnd_Units_To_Receive_S2;
    private DbsField pnd_Store_Actrl_Fld_2_Pnd_Pmts_To_Receive_S2;
    private DbsField pnd_Store_Actrl_Fld_2_Pnd_Next_Pymnt_Dte_S2;
    private DbsField pnd_Store_Actrl_Fld_2_Pnd_Return_Code_S2;
    private DbsField pnd_Store_Actrl_Fld_2_Pnd_Transfer_Eff_Date_Out_S2;
    private DbsField pnd_Store_Actrl_Fld_2_Pnd_Filler7;
    private DbsField pnd_Store_Actrl_Fld_3;
    private DbsGroup pnd_Store_Actrl_Fld_3Redef3;
    private DbsGroup pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3;
    private DbsField pnd_Store_Actrl_Fld_3_Pnd_Fund_Code_Out_S3;
    private DbsField pnd_Store_Actrl_Fld_3_Pnd_Units_Out_S3;
    private DbsField pnd_Store_Actrl_Fld_3_Pnd_Auv_Out_S3;
    private DbsField pnd_Store_Actrl_Fld_3_Pnd_Transfer_Amt_Out_Cref_S3;
    private DbsField pnd_Store_Actrl_Fld_3_Pnd_Filler8;
    private DbsField pnd_Store_Actrl_Fld_4;
    private DbsGroup pnd_Store_Actrl_Fld_4Redef4;
    private DbsGroup pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4;
    private DbsField pnd_Store_Actrl_Fld_4_Pnd_Fund_Code_Out_S4;
    private DbsField pnd_Store_Actrl_Fld_4_Pnd_Units_Out_S4;
    private DbsField pnd_Store_Actrl_Fld_4_Pnd_Auv_Out_S4;
    private DbsField pnd_Store_Actrl_Fld_4_Pnd_Transfer_Amt_Out_Cref_S4;
    private DbsField pnd_Store_Actrl_Fld_4_Pnd_Filler9;
    private DbsField pnd_Store_Actrl_Fld_5;
    private DbsGroup pnd_Store_Actrl_Fld_5Redef5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Fund_Code_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Units_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Auv_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Cref_S5;
    private DbsGroup pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Pmt_Method_Code_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Rate_Code_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Gtd_Pmt_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Dvd_Pmt_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Tiaa_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Total_Transfer_Amt_Out_S5;
    private DbsField pnd_Store_Actrl_Fld_5_Pnd_Filler10;

    public DbsField getPnd_Store_Actrl_Fld_1() { return pnd_Store_Actrl_Fld_1; }

    public DbsGroup getPnd_Store_Actrl_Fld_1Redef1() { return pnd_Store_Actrl_Fld_1Redef1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Contract_Payee_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Contract_Payee_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Fund_Code_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Fund_Code_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Mode_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Mode_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Option_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Option_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Issue_Date_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Issue_Date_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Final_Per_Pay_Date_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Final_Per_Pay_Date_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dob_S1() { return pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dob_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_First_Ann_Sex_S1() { return pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Sex_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dod_S1() { return pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dod_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dob_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dob_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Sex_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Sex_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dod_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dod_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Effective_Date_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Transfer_Effective_Date_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Type_Of_Run_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Type_Of_Run_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Processing_Date_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Processing_Date_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Illustration_Eff_Date_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Illustration_Eff_Date_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1; }

    public DbsGroup getPnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Fund_To_Receive_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Fund_To_Receive_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Units_To_Receive_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Units_To_Receive_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Pmts_To_Receive_S1() { return pnd_Store_Actrl_Fld_1_Pnd_Pmts_To_Receive_S1; }

    public DbsField getPnd_Store_Actrl_Fld_1_Pnd_Filler6() { return pnd_Store_Actrl_Fld_1_Pnd_Filler6; }

    public DbsField getPnd_Store_Actrl_Fld_2() { return pnd_Store_Actrl_Fld_2; }

    public DbsGroup getPnd_Store_Actrl_Fld_2Redef2() { return pnd_Store_Actrl_Fld_2Redef2; }

    public DbsGroup getPnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2() { return pnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2; }

    public DbsField getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2() { return pnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2; }

    public DbsField getPnd_Store_Actrl_Fld_2_Pnd_Units_To_Receive_S2() { return pnd_Store_Actrl_Fld_2_Pnd_Units_To_Receive_S2; }

    public DbsField getPnd_Store_Actrl_Fld_2_Pnd_Pmts_To_Receive_S2() { return pnd_Store_Actrl_Fld_2_Pnd_Pmts_To_Receive_S2; }

    public DbsField getPnd_Store_Actrl_Fld_2_Pnd_Next_Pymnt_Dte_S2() { return pnd_Store_Actrl_Fld_2_Pnd_Next_Pymnt_Dte_S2; }

    public DbsField getPnd_Store_Actrl_Fld_2_Pnd_Return_Code_S2() { return pnd_Store_Actrl_Fld_2_Pnd_Return_Code_S2; }

    public DbsField getPnd_Store_Actrl_Fld_2_Pnd_Transfer_Eff_Date_Out_S2() { return pnd_Store_Actrl_Fld_2_Pnd_Transfer_Eff_Date_Out_S2; }

    public DbsField getPnd_Store_Actrl_Fld_2_Pnd_Filler7() { return pnd_Store_Actrl_Fld_2_Pnd_Filler7; }

    public DbsField getPnd_Store_Actrl_Fld_3() { return pnd_Store_Actrl_Fld_3; }

    public DbsGroup getPnd_Store_Actrl_Fld_3Redef3() { return pnd_Store_Actrl_Fld_3Redef3; }

    public DbsGroup getPnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3() { return pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3; }

    public DbsField getPnd_Store_Actrl_Fld_3_Pnd_Fund_Code_Out_S3() { return pnd_Store_Actrl_Fld_3_Pnd_Fund_Code_Out_S3; }

    public DbsField getPnd_Store_Actrl_Fld_3_Pnd_Units_Out_S3() { return pnd_Store_Actrl_Fld_3_Pnd_Units_Out_S3; }

    public DbsField getPnd_Store_Actrl_Fld_3_Pnd_Auv_Out_S3() { return pnd_Store_Actrl_Fld_3_Pnd_Auv_Out_S3; }

    public DbsField getPnd_Store_Actrl_Fld_3_Pnd_Transfer_Amt_Out_Cref_S3() { return pnd_Store_Actrl_Fld_3_Pnd_Transfer_Amt_Out_Cref_S3; }

    public DbsField getPnd_Store_Actrl_Fld_3_Pnd_Filler8() { return pnd_Store_Actrl_Fld_3_Pnd_Filler8; }

    public DbsField getPnd_Store_Actrl_Fld_4() { return pnd_Store_Actrl_Fld_4; }

    public DbsGroup getPnd_Store_Actrl_Fld_4Redef4() { return pnd_Store_Actrl_Fld_4Redef4; }

    public DbsGroup getPnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4() { return pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4; }

    public DbsField getPnd_Store_Actrl_Fld_4_Pnd_Fund_Code_Out_S4() { return pnd_Store_Actrl_Fld_4_Pnd_Fund_Code_Out_S4; }

    public DbsField getPnd_Store_Actrl_Fld_4_Pnd_Units_Out_S4() { return pnd_Store_Actrl_Fld_4_Pnd_Units_Out_S4; }

    public DbsField getPnd_Store_Actrl_Fld_4_Pnd_Auv_Out_S4() { return pnd_Store_Actrl_Fld_4_Pnd_Auv_Out_S4; }

    public DbsField getPnd_Store_Actrl_Fld_4_Pnd_Transfer_Amt_Out_Cref_S4() { return pnd_Store_Actrl_Fld_4_Pnd_Transfer_Amt_Out_Cref_S4; }

    public DbsField getPnd_Store_Actrl_Fld_4_Pnd_Filler9() { return pnd_Store_Actrl_Fld_4_Pnd_Filler9; }

    public DbsField getPnd_Store_Actrl_Fld_5() { return pnd_Store_Actrl_Fld_5; }

    public DbsGroup getPnd_Store_Actrl_Fld_5Redef5() { return pnd_Store_Actrl_Fld_5Redef5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Fund_Code_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Fund_Code_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Units_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Units_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Auv_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Auv_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Cref_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Cref_S5; }

    public DbsGroup getPnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Pmt_Method_Code_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Pmt_Method_Code_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Rate_Code_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Rate_Code_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Gtd_Pmt_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Gtd_Pmt_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Dvd_Pmt_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Dvd_Pmt_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Tiaa_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Tiaa_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Total_Transfer_Amt_Out_S5() { return pnd_Store_Actrl_Fld_5_Pnd_Total_Transfer_Amt_Out_S5; }

    public DbsField getPnd_Store_Actrl_Fld_5_Pnd_Filler10() { return pnd_Store_Actrl_Fld_5_Pnd_Filler10; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Store_Actrl_Fld_1 = newFieldInRecord("pnd_Store_Actrl_Fld_1", "#STORE-ACTRL-FLD-1", FieldType.STRING, 250);
        pnd_Store_Actrl_Fld_1Redef1 = newGroupInRecord("pnd_Store_Actrl_Fld_1Redef1", "Redefines", pnd_Store_Actrl_Fld_1);
        pnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1", 
            "#CREF-REA-INDICATOR-S1", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1", 
            "#TRANSFER-REVAL-SWITCH-S1", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_1_Pnd_Contract_Payee_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Contract_Payee_S1", "#CONTRACT-PAYEE-S1", 
            FieldType.STRING, 10);
        pnd_Store_Actrl_Fld_1_Pnd_Fund_Code_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Fund_Code_S1", "#FUND-CODE-S1", 
            FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_1_Pnd_Mode_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Mode_S1", "#MODE-S1", FieldType.NUMERIC, 
            3);
        pnd_Store_Actrl_Fld_1_Pnd_Option_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Option_S1", "#OPTION-S1", FieldType.NUMERIC, 
            2);
        pnd_Store_Actrl_Fld_1_Pnd_Issue_Date_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Issue_Date_S1", "#ISSUE-DATE-S1", 
            FieldType.NUMERIC, 6);
        pnd_Store_Actrl_Fld_1_Pnd_Final_Per_Pay_Date_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Final_Per_Pay_Date_S1", 
            "#FINAL-PER-PAY-DATE-S1", FieldType.NUMERIC, 6);
        pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dob_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dob_S1", "#FIRST-ANN-DOB-S1", 
            FieldType.NUMERIC, 8);
        pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Sex_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Sex_S1", "#FIRST-ANN-SEX-S1", 
            FieldType.NUMERIC, 1);
        pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dod_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_First_Ann_Dod_S1", "#FIRST-ANN-DOD-S1", 
            FieldType.NUMERIC, 6);
        pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dob_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dob_S1", "#SECOND-ANN-DOB-S1", 
            FieldType.NUMERIC, 8);
        pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Sex_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Sex_S1", "#SECOND-ANN-SEX-S1", 
            FieldType.NUMERIC, 1);
        pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dod_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Second_Ann_Dod_S1", "#SECOND-ANN-DOD-S1", 
            FieldType.NUMERIC, 6);
        pnd_Store_Actrl_Fld_1_Pnd_Transfer_Effective_Date_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Transfer_Effective_Date_S1", 
            "#TRANSFER-EFFECTIVE-DATE-S1", FieldType.NUMERIC, 8);
        pnd_Store_Actrl_Fld_1_Pnd_Type_Of_Run_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Type_Of_Run_S1", "#TYPE-OF-RUN-S1", 
            FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1", 
            "#REVALUATION-INDICATOR-S1", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_1_Pnd_Processing_Date_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Processing_Date_S1", "#PROCESSING-DATE-S1", 
            FieldType.NUMERIC, 8);
        pnd_Store_Actrl_Fld_1_Pnd_Illustration_Eff_Date_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Illustration_Eff_Date_S1", 
            "#ILLUSTRATION-EFF-DATE-S1", FieldType.NUMERIC, 8);
        pnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1", "#TRANSFER-UNITS-S1", 
            FieldType.DECIMAL, 8,3);
        pnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1 = pnd_Store_Actrl_Fld_1Redef1.newGroupArrayInGroup("pnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1", 
            "#TRANSFER-TO-ARRAYS-S1", new DbsArrayController(1,8));
        pnd_Store_Actrl_Fld_1_Pnd_Fund_To_Receive_S1 = pnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Fund_To_Receive_S1", 
            "#FUND-TO-RECEIVE-S1", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_1_Pnd_Units_To_Receive_S1 = pnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Units_To_Receive_S1", 
            "#UNITS-TO-RECEIVE-S1", FieldType.DECIMAL, 8,3);
        pnd_Store_Actrl_Fld_1_Pnd_Pmts_To_Receive_S1 = pnd_Store_Actrl_Fld_1_Pnd_Transfer_To_Arrays_S1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Pmts_To_Receive_S1", 
            "#PMTS-TO-RECEIVE-S1", FieldType.DECIMAL, 9,2);
        pnd_Store_Actrl_Fld_1_Pnd_Filler6 = pnd_Store_Actrl_Fld_1Redef1.newFieldInGroup("pnd_Store_Actrl_Fld_1_Pnd_Filler6", "#FILLER6", FieldType.STRING, 
            12);

        pnd_Store_Actrl_Fld_2 = newFieldInRecord("pnd_Store_Actrl_Fld_2", "#STORE-ACTRL-FLD-2", FieldType.STRING, 250);
        pnd_Store_Actrl_Fld_2Redef2 = newGroupInRecord("pnd_Store_Actrl_Fld_2Redef2", "Redefines", pnd_Store_Actrl_Fld_2);
        pnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2 = pnd_Store_Actrl_Fld_2Redef2.newGroupArrayInGroup("pnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2", 
            "#TRANSFER-TO-ARRAYS-S2", new DbsArrayController(1,13));
        pnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2 = pnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2.newFieldInGroup("pnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2", 
            "#FUND-TO-RECEIVE-S2", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_2_Pnd_Units_To_Receive_S2 = pnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2.newFieldInGroup("pnd_Store_Actrl_Fld_2_Pnd_Units_To_Receive_S2", 
            "#UNITS-TO-RECEIVE-S2", FieldType.DECIMAL, 8,3);
        pnd_Store_Actrl_Fld_2_Pnd_Pmts_To_Receive_S2 = pnd_Store_Actrl_Fld_2_Pnd_Transfer_To_Arrays_S2.newFieldInGroup("pnd_Store_Actrl_Fld_2_Pnd_Pmts_To_Receive_S2", 
            "#PMTS-TO-RECEIVE-S2", FieldType.DECIMAL, 9,2);
        pnd_Store_Actrl_Fld_2_Pnd_Next_Pymnt_Dte_S2 = pnd_Store_Actrl_Fld_2Redef2.newFieldInGroup("pnd_Store_Actrl_Fld_2_Pnd_Next_Pymnt_Dte_S2", "#NEXT-PYMNT-DTE-S2", 
            FieldType.DATE);
        pnd_Store_Actrl_Fld_2_Pnd_Return_Code_S2 = pnd_Store_Actrl_Fld_2Redef2.newFieldInGroup("pnd_Store_Actrl_Fld_2_Pnd_Return_Code_S2", "#RETURN-CODE-S2", 
            FieldType.NUMERIC, 2);
        pnd_Store_Actrl_Fld_2_Pnd_Transfer_Eff_Date_Out_S2 = pnd_Store_Actrl_Fld_2Redef2.newFieldInGroup("pnd_Store_Actrl_Fld_2_Pnd_Transfer_Eff_Date_Out_S2", 
            "#TRANSFER-EFF-DATE-OUT-S2", FieldType.NUMERIC, 8);
        pnd_Store_Actrl_Fld_2_Pnd_Filler7 = pnd_Store_Actrl_Fld_2Redef2.newFieldInGroup("pnd_Store_Actrl_Fld_2_Pnd_Filler7", "#FILLER7", FieldType.STRING, 
            2);

        pnd_Store_Actrl_Fld_3 = newFieldInRecord("pnd_Store_Actrl_Fld_3", "#STORE-ACTRL-FLD-3", FieldType.STRING, 250);
        pnd_Store_Actrl_Fld_3Redef3 = newGroupInRecord("pnd_Store_Actrl_Fld_3Redef3", "Redefines", pnd_Store_Actrl_Fld_3);
        pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3 = pnd_Store_Actrl_Fld_3Redef3.newGroupArrayInGroup("pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3", 
            "#TRANSFER-OUT-CREF-ARRAYS-S3", new DbsArrayController(1,9));
        pnd_Store_Actrl_Fld_3_Pnd_Fund_Code_Out_S3 = pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3.newFieldInGroup("pnd_Store_Actrl_Fld_3_Pnd_Fund_Code_Out_S3", 
            "#FUND-CODE-OUT-S3", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_3_Pnd_Units_Out_S3 = pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3.newFieldInGroup("pnd_Store_Actrl_Fld_3_Pnd_Units_Out_S3", 
            "#UNITS-OUT-S3", FieldType.DECIMAL, 8,3);
        pnd_Store_Actrl_Fld_3_Pnd_Auv_Out_S3 = pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3.newFieldInGroup("pnd_Store_Actrl_Fld_3_Pnd_Auv_Out_S3", 
            "#AUV-OUT-S3", FieldType.DECIMAL, 8,4);
        pnd_Store_Actrl_Fld_3_Pnd_Transfer_Amt_Out_Cref_S3 = pnd_Store_Actrl_Fld_3_Pnd_Transfer_Out_Cref_Arrays_S3.newFieldInGroup("pnd_Store_Actrl_Fld_3_Pnd_Transfer_Amt_Out_Cref_S3", 
            "#TRANSFER-AMT-OUT-CREF-S3", FieldType.DECIMAL, 10,2);
        pnd_Store_Actrl_Fld_3_Pnd_Filler8 = pnd_Store_Actrl_Fld_3Redef3.newFieldInGroup("pnd_Store_Actrl_Fld_3_Pnd_Filler8", "#FILLER8", FieldType.STRING, 
            7);

        pnd_Store_Actrl_Fld_4 = newFieldInRecord("pnd_Store_Actrl_Fld_4", "#STORE-ACTRL-FLD-4", FieldType.STRING, 250);
        pnd_Store_Actrl_Fld_4Redef4 = newGroupInRecord("pnd_Store_Actrl_Fld_4Redef4", "Redefines", pnd_Store_Actrl_Fld_4);
        pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4 = pnd_Store_Actrl_Fld_4Redef4.newGroupArrayInGroup("pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4", 
            "#TRANSFER-OUT-CREF-ARRAYS-S4", new DbsArrayController(1,9));
        pnd_Store_Actrl_Fld_4_Pnd_Fund_Code_Out_S4 = pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4.newFieldInGroup("pnd_Store_Actrl_Fld_4_Pnd_Fund_Code_Out_S4", 
            "#FUND-CODE-OUT-S4", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_4_Pnd_Units_Out_S4 = pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4.newFieldInGroup("pnd_Store_Actrl_Fld_4_Pnd_Units_Out_S4", 
            "#UNITS-OUT-S4", FieldType.DECIMAL, 8,3);
        pnd_Store_Actrl_Fld_4_Pnd_Auv_Out_S4 = pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4.newFieldInGroup("pnd_Store_Actrl_Fld_4_Pnd_Auv_Out_S4", 
            "#AUV-OUT-S4", FieldType.DECIMAL, 8,4);
        pnd_Store_Actrl_Fld_4_Pnd_Transfer_Amt_Out_Cref_S4 = pnd_Store_Actrl_Fld_4_Pnd_Transfer_Out_Cref_Arrays_S4.newFieldInGroup("pnd_Store_Actrl_Fld_4_Pnd_Transfer_Amt_Out_Cref_S4", 
            "#TRANSFER-AMT-OUT-CREF-S4", FieldType.DECIMAL, 10,2);
        pnd_Store_Actrl_Fld_4_Pnd_Filler9 = pnd_Store_Actrl_Fld_4Redef4.newFieldInGroup("pnd_Store_Actrl_Fld_4_Pnd_Filler9", "#FILLER9", FieldType.STRING, 
            7);

        pnd_Store_Actrl_Fld_5 = newFieldInRecord("pnd_Store_Actrl_Fld_5", "#STORE-ACTRL-FLD-5", FieldType.STRING, 250);
        pnd_Store_Actrl_Fld_5Redef5 = newGroupInRecord("pnd_Store_Actrl_Fld_5Redef5", "Redefines", pnd_Store_Actrl_Fld_5);
        pnd_Store_Actrl_Fld_5_Pnd_Fund_Code_Out_S5 = pnd_Store_Actrl_Fld_5Redef5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Fund_Code_Out_S5", "#FUND-CODE-OUT-S5", 
            FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_5_Pnd_Units_Out_S5 = pnd_Store_Actrl_Fld_5Redef5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Units_Out_S5", "#UNITS-OUT-S5", 
            FieldType.DECIMAL, 8,3);
        pnd_Store_Actrl_Fld_5_Pnd_Auv_Out_S5 = pnd_Store_Actrl_Fld_5Redef5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Auv_Out_S5", "#AUV-OUT-S5", FieldType.DECIMAL, 
            8,4);
        pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Cref_S5 = pnd_Store_Actrl_Fld_5Redef5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Cref_S5", 
            "#TRANSFER-AMT-OUT-CREF-S5", FieldType.DECIMAL, 10,2);
        pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5 = pnd_Store_Actrl_Fld_5Redef5.newGroupArrayInGroup("pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5", 
            "#TRANSFER-OUT-TIAA-ARRAYS-S5", new DbsArrayController(1,2));
        pnd_Store_Actrl_Fld_5_Pnd_Pmt_Method_Code_Out_S5 = pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Pmt_Method_Code_Out_S5", 
            "#PMT-METHOD-CODE-OUT-S5", FieldType.STRING, 1);
        pnd_Store_Actrl_Fld_5_Pnd_Rate_Code_Out_S5 = pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Rate_Code_Out_S5", 
            "#RATE-CODE-OUT-S5", FieldType.STRING, 2);
        pnd_Store_Actrl_Fld_5_Pnd_Gtd_Pmt_Out_S5 = pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Gtd_Pmt_Out_S5", 
            "#GTD-PMT-OUT-S5", FieldType.DECIMAL, 9,2);
        pnd_Store_Actrl_Fld_5_Pnd_Dvd_Pmt_Out_S5 = pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Dvd_Pmt_Out_S5", 
            "#DVD-PMT-OUT-S5", FieldType.DECIMAL, 9,2);
        pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Tiaa_S5 = pnd_Store_Actrl_Fld_5_Pnd_Transfer_Out_Tiaa_Arrays_S5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Tiaa_S5", 
            "#TRANSFER-AMT-OUT-TIAA-S5", FieldType.DECIMAL, 10,2);
        pnd_Store_Actrl_Fld_5_Pnd_Total_Transfer_Amt_Out_S5 = pnd_Store_Actrl_Fld_5Redef5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Total_Transfer_Amt_Out_S5", 
            "#TOTAL-TRANSFER-AMT-OUT-S5", FieldType.DECIMAL, 11,2);
        pnd_Store_Actrl_Fld_5_Pnd_Filler10 = pnd_Store_Actrl_Fld_5Redef5.newFieldInGroup("pnd_Store_Actrl_Fld_5_Pnd_Filler10", "#FILLER10", FieldType.STRING, 
            150);

        this.setRecordName("LdaAial013b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAial013b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
