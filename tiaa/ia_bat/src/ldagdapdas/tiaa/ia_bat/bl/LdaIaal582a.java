/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:04 PM
**        * FROM NATURAL LDA     : IAAL582A
************************************************************
**        * FILE NAME            : LdaIaal582a.java
**        * CLASS NAME           : LdaIaal582a
**        * INSTANCE NAME        : LdaIaal582a
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal582a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Pnd_W_Name;
    private DbsGroup pnd_Work_Record_Pnd_W_NameRedef1;
    private DbsField pnd_Work_Record_Pnd_W_Last_Name;
    private DbsField pnd_Work_Record_Pnd_W_First_Name;
    private DbsField pnd_Work_Record_Pnd_W_Middle_Name;
    private DbsField pnd_Work_Record_Pnd_W_Cont_Stat;
    private DbsGroup pnd_Work_Record_Pnd_W_Cont_StatRedef2;
    private DbsField pnd_Work_Record_Pnd_W_Contract;
    private DbsField pnd_Work_Record_Pnd_W_Dash;
    private DbsField pnd_Work_Record_Pnd_W_Cont_Pyee;
    private DbsField pnd_Work_Record_Pnd_W_Pin;
    private DbsField pnd_Work_Record_Pnd_W_Ssn;
    private DbsGroup pnd_Work_Record_Pnd_W_SsnRedef3;
    private DbsField pnd_Work_Record_Pnd_Ssn_1_3;
    private DbsField pnd_Work_Record_Pnd_Ssn_4_5;
    private DbsField pnd_Work_Record_Pnd_Ssn_6_9;
    private DbsField pnd_Work_Record_Pnd_W_Annu_Type;
    private DbsField pnd_Work_Record_Pnd_W_Contract_Mode;
    private DbsField pnd_Work_Record_Pnd_W_Dod_Dte;
    private DbsField pnd_Work_Record_Pnd_W_Notify_Name;
    private DbsField pnd_Work_Record_Pnd_W_Notify_Addr1;
    private DbsField pnd_Work_Record_Pnd_W_Notify_Addr2;
    private DbsField pnd_Work_Record_Pnd_W_Notify_Addr3;
    private DbsField pnd_Work_Record_Pnd_W_Notify_Addr4;
    private DbsField pnd_Work_Record_Pnd_W_Notify_City;
    private DbsField pnd_Work_Record_Pnd_W_Notify_State;
    private DbsField pnd_Work_Record_Pnd_W_Notify_Zip;
    private DbsField pnd_Work_Record_Pnd_W_User_Area;
    private DbsField pnd_Work_Record_Pnd_W_User_Id;
    private DbsField pnd_Work_Record_Pnd_W_Nfp;
    private DbsField pnd_W_Parm_Date;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef4;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd() { return vw_iaa_Cntrl_Rcrd; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Check_Dte; }

    public DbsGroup getPnd_Work_Record() { return pnd_Work_Record; }

    public DbsField getPnd_Work_Record_Pnd_W_Name() { return pnd_Work_Record_Pnd_W_Name; }

    public DbsGroup getPnd_Work_Record_Pnd_W_NameRedef1() { return pnd_Work_Record_Pnd_W_NameRedef1; }

    public DbsField getPnd_Work_Record_Pnd_W_Last_Name() { return pnd_Work_Record_Pnd_W_Last_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_First_Name() { return pnd_Work_Record_Pnd_W_First_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_Middle_Name() { return pnd_Work_Record_Pnd_W_Middle_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_Cont_Stat() { return pnd_Work_Record_Pnd_W_Cont_Stat; }

    public DbsGroup getPnd_Work_Record_Pnd_W_Cont_StatRedef2() { return pnd_Work_Record_Pnd_W_Cont_StatRedef2; }

    public DbsField getPnd_Work_Record_Pnd_W_Contract() { return pnd_Work_Record_Pnd_W_Contract; }

    public DbsField getPnd_Work_Record_Pnd_W_Dash() { return pnd_Work_Record_Pnd_W_Dash; }

    public DbsField getPnd_Work_Record_Pnd_W_Cont_Pyee() { return pnd_Work_Record_Pnd_W_Cont_Pyee; }

    public DbsField getPnd_Work_Record_Pnd_W_Pin() { return pnd_Work_Record_Pnd_W_Pin; }

    public DbsField getPnd_Work_Record_Pnd_W_Ssn() { return pnd_Work_Record_Pnd_W_Ssn; }

    public DbsGroup getPnd_Work_Record_Pnd_W_SsnRedef3() { return pnd_Work_Record_Pnd_W_SsnRedef3; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_1_3() { return pnd_Work_Record_Pnd_Ssn_1_3; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_4_5() { return pnd_Work_Record_Pnd_Ssn_4_5; }

    public DbsField getPnd_Work_Record_Pnd_Ssn_6_9() { return pnd_Work_Record_Pnd_Ssn_6_9; }

    public DbsField getPnd_Work_Record_Pnd_W_Annu_Type() { return pnd_Work_Record_Pnd_W_Annu_Type; }

    public DbsField getPnd_Work_Record_Pnd_W_Contract_Mode() { return pnd_Work_Record_Pnd_W_Contract_Mode; }

    public DbsField getPnd_Work_Record_Pnd_W_Dod_Dte() { return pnd_Work_Record_Pnd_W_Dod_Dte; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_Name() { return pnd_Work_Record_Pnd_W_Notify_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_Addr1() { return pnd_Work_Record_Pnd_W_Notify_Addr1; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_Addr2() { return pnd_Work_Record_Pnd_W_Notify_Addr2; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_Addr3() { return pnd_Work_Record_Pnd_W_Notify_Addr3; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_Addr4() { return pnd_Work_Record_Pnd_W_Notify_Addr4; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_City() { return pnd_Work_Record_Pnd_W_Notify_City; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_State() { return pnd_Work_Record_Pnd_W_Notify_State; }

    public DbsField getPnd_Work_Record_Pnd_W_Notify_Zip() { return pnd_Work_Record_Pnd_W_Notify_Zip; }

    public DbsField getPnd_Work_Record_Pnd_W_User_Area() { return pnd_Work_Record_Pnd_W_User_Area; }

    public DbsField getPnd_Work_Record_Pnd_W_User_Id() { return pnd_Work_Record_Pnd_W_User_Id; }

    public DbsField getPnd_Work_Record_Pnd_W_Nfp() { return pnd_Work_Record_Pnd_W_Nfp; }

    public DbsField getPnd_W_Parm_Date() { return pnd_W_Parm_Date; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef4() { return iaa_Parm_Card_Pnd_Parm_DateRedef4; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");

        pnd_Work_Record = newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Pnd_W_Name = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Name", "#W-NAME", FieldType.STRING, 38);
        pnd_Work_Record_Pnd_W_NameRedef1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_W_NameRedef1", "Redefines", pnd_Work_Record_Pnd_W_Name);
        pnd_Work_Record_Pnd_W_Last_Name = pnd_Work_Record_Pnd_W_NameRedef1.newFieldInGroup("pnd_Work_Record_Pnd_W_Last_Name", "#W-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Work_Record_Pnd_W_First_Name = pnd_Work_Record_Pnd_W_NameRedef1.newFieldInGroup("pnd_Work_Record_Pnd_W_First_Name", "#W-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Work_Record_Pnd_W_Middle_Name = pnd_Work_Record_Pnd_W_NameRedef1.newFieldInGroup("pnd_Work_Record_Pnd_W_Middle_Name", "#W-MIDDLE-NAME", FieldType.STRING, 
            12);
        pnd_Work_Record_Pnd_W_Cont_Stat = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cont_Stat", "#W-CONT-STAT", FieldType.STRING, 11);
        pnd_Work_Record_Pnd_W_Cont_StatRedef2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_W_Cont_StatRedef2", "Redefines", pnd_Work_Record_Pnd_W_Cont_Stat);
        pnd_Work_Record_Pnd_W_Contract = pnd_Work_Record_Pnd_W_Cont_StatRedef2.newFieldInGroup("pnd_Work_Record_Pnd_W_Contract", "#W-CONTRACT", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_W_Dash = pnd_Work_Record_Pnd_W_Cont_StatRedef2.newFieldInGroup("pnd_Work_Record_Pnd_W_Dash", "#W-DASH", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_W_Cont_Pyee = pnd_Work_Record_Pnd_W_Cont_StatRedef2.newFieldInGroup("pnd_Work_Record_Pnd_W_Cont_Pyee", "#W-CONT-PYEE", FieldType.STRING, 
            2);
        pnd_Work_Record_Pnd_W_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Pnd_W_Ssn = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Ssn", "#W-SSN", FieldType.STRING, 9);
        pnd_Work_Record_Pnd_W_SsnRedef3 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_W_SsnRedef3", "Redefines", pnd_Work_Record_Pnd_W_Ssn);
        pnd_Work_Record_Pnd_Ssn_1_3 = pnd_Work_Record_Pnd_W_SsnRedef3.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_1_3", "#SSN-1-3", FieldType.STRING, 3);
        pnd_Work_Record_Pnd_Ssn_4_5 = pnd_Work_Record_Pnd_W_SsnRedef3.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_4_5", "#SSN-4-5", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Ssn_6_9 = pnd_Work_Record_Pnd_W_SsnRedef3.newFieldInGroup("pnd_Work_Record_Pnd_Ssn_6_9", "#SSN-6-9", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_W_Annu_Type = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Annu_Type", "#W-ANNU-TYPE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_W_Contract_Mode = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Contract_Mode", "#W-CONTRACT-MODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Pnd_W_Dod_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Dod_Dte", "#W-DOD-DTE", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_W_Notify_Name = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_Name", "#W-NOTIFY-NAME", FieldType.STRING, 35);
        pnd_Work_Record_Pnd_W_Notify_Addr1 = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_Addr1", "#W-NOTIFY-ADDR1", FieldType.STRING, 
            35);
        pnd_Work_Record_Pnd_W_Notify_Addr2 = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_Addr2", "#W-NOTIFY-ADDR2", FieldType.STRING, 
            35);
        pnd_Work_Record_Pnd_W_Notify_Addr3 = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_Addr3", "#W-NOTIFY-ADDR3", FieldType.STRING, 
            35);
        pnd_Work_Record_Pnd_W_Notify_Addr4 = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_Addr4", "#W-NOTIFY-ADDR4", FieldType.STRING, 
            35);
        pnd_Work_Record_Pnd_W_Notify_City = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_City", "#W-NOTIFY-CITY", FieldType.STRING, 21);
        pnd_Work_Record_Pnd_W_Notify_State = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_State", "#W-NOTIFY-STATE", FieldType.STRING, 
            2);
        pnd_Work_Record_Pnd_W_Notify_Zip = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Notify_Zip", "#W-NOTIFY-ZIP", FieldType.STRING, 9);
        pnd_Work_Record_Pnd_W_User_Area = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_User_Area", "#W-USER-AREA", FieldType.STRING, 6);
        pnd_Work_Record_Pnd_W_User_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_User_Id", "#W-USER-ID", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_W_Nfp = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Nfp", "#W-NFP", FieldType.STRING, 1);

        pnd_W_Parm_Date = newFieldInRecord("pnd_W_Parm_Date", "#W-PARM-DATE", FieldType.STRING, 8);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef4 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef4", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.STRING, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_DateRedef4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.STRING, 
            2);

        this.setRecordName("LdaIaal582a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrl_Rcrd.reset();
    }

    // Constructor
    public LdaIaal582a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
