/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:30 PM
**        * FROM NATURAL LDA     : IATL100
************************************************************
**        * FILE NAME            : LdaIatl100.java
**        * CLASS NAME           : LdaIatl100
**        * INSTANCE NAME        : LdaIatl100
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIatl100 extends DbsRecord
{
    // Properties
    private DbsField pnd_Cnv_Table;
    private DbsField pnd_To_From_Table;
    private DbsField pnd_To_From_Funds;
    private DbsField pnd_To_From_Table_Retro;
    private DbsField pnd_To_From_Table_Retro_Funds;
    private DbsField pnd_To_From_Table_Future;
    private DbsField pnd_To_From_Table_Future_Funds;
    private DbsField pnd_To_From_Table_Total;
    private DbsField pnd_Tot_Across_Cnv;
    private DbsField pnd_Tot_Down_Cnv;
    private DbsField pnd_Sub_Tot_Down_Across;
    private DbsField pnd_Sub_Tot_Down_Across_Tot;
    private DbsField pnd_Tot_Down_Tot;
    private DbsField pnd_Rec_Acpt;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_C;
    private DbsField pnd_Text_Table;
    private DbsField pnd_Tiaa_Units_Cnt;
    private DbsField pnd_Fund_Cd_From;
    private DbsField pnd_Fund_Cd_To;
    private DbsField pnd_Tiaa_Fund_Cd_To;
    private DbsField pnd_To_Sub;
    private DbsField pnd_From_Sub;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;
    private DbsGroup pnd_Tiaa_Cntrct_Fund_KeyRedef1;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee;
    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd_View;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key;

    public DbsField getPnd_Cnv_Table() { return pnd_Cnv_Table; }

    public DbsField getPnd_To_From_Table() { return pnd_To_From_Table; }

    public DbsField getPnd_To_From_Funds() { return pnd_To_From_Funds; }

    public DbsField getPnd_To_From_Table_Retro() { return pnd_To_From_Table_Retro; }

    public DbsField getPnd_To_From_Table_Retro_Funds() { return pnd_To_From_Table_Retro_Funds; }

    public DbsField getPnd_To_From_Table_Future() { return pnd_To_From_Table_Future; }

    public DbsField getPnd_To_From_Table_Future_Funds() { return pnd_To_From_Table_Future_Funds; }

    public DbsField getPnd_To_From_Table_Total() { return pnd_To_From_Table_Total; }

    public DbsField getPnd_Tot_Across_Cnv() { return pnd_Tot_Across_Cnv; }

    public DbsField getPnd_Tot_Down_Cnv() { return pnd_Tot_Down_Cnv; }

    public DbsField getPnd_Sub_Tot_Down_Across() { return pnd_Sub_Tot_Down_Across; }

    public DbsField getPnd_Sub_Tot_Down_Across_Tot() { return pnd_Sub_Tot_Down_Across_Tot; }

    public DbsField getPnd_Tot_Down_Tot() { return pnd_Tot_Down_Tot; }

    public DbsField getPnd_Rec_Acpt() { return pnd_Rec_Acpt; }

    public DbsField getPnd_Rec_Read() { return pnd_Rec_Read; }

    public DbsField getPnd_C() { return pnd_C; }

    public DbsField getPnd_Text_Table() { return pnd_Text_Table; }

    public DbsField getPnd_Tiaa_Units_Cnt() { return pnd_Tiaa_Units_Cnt; }

    public DbsField getPnd_Fund_Cd_From() { return pnd_Fund_Cd_From; }

    public DbsField getPnd_Fund_Cd_To() { return pnd_Fund_Cd_To; }

    public DbsField getPnd_Tiaa_Fund_Cd_To() { return pnd_Tiaa_Fund_Cd_To; }

    public DbsField getPnd_To_Sub() { return pnd_To_Sub; }

    public DbsField getPnd_From_Sub() { return pnd_From_Sub; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key() { return pnd_Tiaa_Cntrct_Fund_Key; }

    public DbsGroup getPnd_Tiaa_Cntrct_Fund_KeyRedef1() { return pnd_Tiaa_Cntrct_Fund_KeyRedef1; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee; }

    public DbsGroup getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N; }

    public DbsField getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund() { return pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd_View() { return vw_iaa_Tiaa_Fund_Rcrd_View; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key() { return iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Cnv_Table = newFieldArrayInRecord("pnd_Cnv_Table", "#CNV-TABLE", FieldType.NUMERIC, 7, new DbsArrayController(1,20,1,20));

        pnd_To_From_Table = newFieldArrayInRecord("pnd_To_From_Table", "#TO-FROM-TABLE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,20,1,20));

        pnd_To_From_Funds = newFieldArrayInRecord("pnd_To_From_Funds", "#TO-FROM-FUNDS", FieldType.STRING, 2, new DbsArrayController(1,20,1,20));

        pnd_To_From_Table_Retro = newFieldArrayInRecord("pnd_To_From_Table_Retro", "#TO-FROM-TABLE-RETRO", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            20,1,20));

        pnd_To_From_Table_Retro_Funds = newFieldArrayInRecord("pnd_To_From_Table_Retro_Funds", "#TO-FROM-TABLE-RETRO-FUNDS", FieldType.STRING, 2, new 
            DbsArrayController(1,20,1,20));

        pnd_To_From_Table_Future = newFieldArrayInRecord("pnd_To_From_Table_Future", "#TO-FROM-TABLE-FUTURE", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            20,1,20));

        pnd_To_From_Table_Future_Funds = newFieldArrayInRecord("pnd_To_From_Table_Future_Funds", "#TO-FROM-TABLE-FUTURE-FUNDS", FieldType.STRING, 2, new 
            DbsArrayController(1,20,1,20));

        pnd_To_From_Table_Total = newFieldArrayInRecord("pnd_To_From_Table_Total", "#TO-FROM-TABLE-TOTAL", FieldType.DECIMAL, 11,2, new DbsArrayController(1,
            20,1,20));

        pnd_Tot_Across_Cnv = newFieldArrayInRecord("pnd_Tot_Across_Cnv", "#TOT-ACROSS-CNV", FieldType.NUMERIC, 10, new DbsArrayController(1,20));

        pnd_Tot_Down_Cnv = newFieldArrayInRecord("pnd_Tot_Down_Cnv", "#TOT-DOWN-CNV", FieldType.NUMERIC, 10, new DbsArrayController(1,20));

        pnd_Sub_Tot_Down_Across = newFieldArrayInRecord("pnd_Sub_Tot_Down_Across", "#SUB-TOT-DOWN-ACROSS", FieldType.NUMERIC, 10, new DbsArrayController(1,
            20));

        pnd_Sub_Tot_Down_Across_Tot = newFieldInRecord("pnd_Sub_Tot_Down_Across_Tot", "#SUB-TOT-DOWN-ACROSS-TOT", FieldType.NUMERIC, 10);

        pnd_Tot_Down_Tot = newFieldInRecord("pnd_Tot_Down_Tot", "#TOT-DOWN-TOT", FieldType.NUMERIC, 10);

        pnd_Rec_Acpt = newFieldInRecord("pnd_Rec_Acpt", "#REC-ACPT", FieldType.NUMERIC, 6);

        pnd_Rec_Read = newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 6);

        pnd_C = newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);

        pnd_Text_Table = newFieldArrayInRecord("pnd_Text_Table", "#TEXT-TABLE", FieldType.STRING, 6, new DbsArrayController(1,20));

        pnd_Tiaa_Units_Cnt = newFieldInRecord("pnd_Tiaa_Units_Cnt", "#TIAA-UNITS-CNT", FieldType.DECIMAL, 9,3);

        pnd_Fund_Cd_From = newFieldInRecord("pnd_Fund_Cd_From", "#FUND-CD-FROM", FieldType.STRING, 1);

        pnd_Fund_Cd_To = newFieldInRecord("pnd_Fund_Cd_To", "#FUND-CD-TO", FieldType.STRING, 1);

        pnd_Tiaa_Fund_Cd_To = newFieldInRecord("pnd_Tiaa_Fund_Cd_To", "#TIAA-FUND-CD-TO", FieldType.STRING, 1);

        pnd_To_Sub = newFieldInRecord("pnd_To_Sub", "#TO-SUB", FieldType.NUMERIC, 2);

        pnd_From_Sub = newFieldInRecord("pnd_From_Sub", "#FROM-SUB", FieldType.NUMERIC, 2);

        pnd_Tiaa_Cntrct_Fund_Key = newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);
        pnd_Tiaa_Cntrct_Fund_KeyRedef1 = newGroupInRecord("pnd_Tiaa_Cntrct_Fund_KeyRedef1", "Redefines", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee", "#TIAA-PAYEE", 
            FieldType.STRING, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2 = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newGroupInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2", 
            "Redefines", pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N = pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_PayeeRedef2.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N", 
            "#TIAA-PAYEE-N", FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund = pnd_Tiaa_Cntrct_Fund_KeyRedef1.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund", "#TIAA-FUND", 
            FieldType.STRING, 3);

        vw_iaa_Tiaa_Fund_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd_View", "IAA-TIAA-FUND-RCRD-VIEW"), "IAA_TIAA_FUND_RCRD", 
            "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr", 
            "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde", 
            "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde", 
            "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3 = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde", 
            "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_CdeRedef3.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde", 
            "TIAA-FUND-CDE", FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt", 
            "TIAA-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt", 
            "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt", 
            "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt", 
            "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt", 
            "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp", 
            "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde", 
            "TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte", 
            "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt", 
            "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt", 
            "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt", 
            "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup", 
            "TIAA_RATE_GICMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic", 
            "TIAA-RATE-GIC", FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Mode_Ind", "TIAA-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Cmpny_Fund", 
            "TIAA-OLD-CMPNY-FUND", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Xfr_Iss_Dte", 
            "TIAA-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte", 
            "TIAA-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_Out_Dte", 
            "TIAA-LST-XFR-OUT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key = vw_iaa_Tiaa_Fund_Rcrd_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Fund_Key", 
            "TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");
        vw_iaa_Tiaa_Fund_Rcrd_View.setUniquePeList();

        this.setRecordName("LdaIatl100");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Tiaa_Fund_Rcrd_View.reset();
    }

    // Constructor
    public LdaIatl100() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
