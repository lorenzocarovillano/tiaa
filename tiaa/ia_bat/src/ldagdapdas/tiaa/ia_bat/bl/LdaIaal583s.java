/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:02:04 PM
**        * FROM NATURAL LDA     : IAAL583S
************************************************************
**        * FILE NAME            : LdaIaal583s.java
**        * CLASS NAME           : LdaIaal583s
**        * INSTANCE NAME        : LdaIaal583s
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal583s extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role_View;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_CdeRedef1;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Pnd_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Payee_Key;
    private DataAccessProgramView vw_iaa_Sttlmnt_View;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Id_Nbr;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Tax_Id_Nbr;
    private DbsGroup iaa_Sttlmnt_View_Sttlmnt_Tax_Id_NbrRedef2;
    private DbsField iaa_Sttlmnt_View_Pnd_Sttlmnt_Tax_Id_Nbr_A;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Req_Seq_Nbr;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Process_Type;
    private DbsGroup iaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3;
    private DbsField iaa_Sttlmnt_View_Pnd_Sttlmnt_Id;
    private DbsField iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type;
    private DbsField iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Filler;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Timestamp;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Status_Cde;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Status_Timestamp;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Cwf_Wpid;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Decedent_Name;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Decedent_Type;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Dod_Dte;
    private DbsGroup iaa_Sttlmnt_View_Sttlmnt_Dod_DteRedef4;
    private DbsField iaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_Dte;
    private DbsGroup iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_DteRedef5;
    private DbsField iaa_Sttlmnt_View_Pnd_Sttlmnt_2nd_Dod_Dte;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Name;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr1;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr2;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr3;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr4;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_City;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_State;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Zip;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Phone;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notify_Date;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Notifier_Rltnshp;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Cntrct_Count;
    private DbsField iaa_Sttlmnt_View_Oper_Id;
    private DbsField iaa_Sttlmnt_View_Oper_Timestamp;
    private DbsField iaa_Sttlmnt_View_Oper_User_Grp;
    private DbsField iaa_Sttlmnt_View_Verf_Id;
    private DbsField iaa_Sttlmnt_View_Verf_Timestamp;
    private DbsField iaa_Sttlmnt_View_Verf_User_Grp;
    private DbsField iaa_Sttlmnt_View_Sttlmnt_Coding_Dte;
    private DbsField iaa_Sttlmnt_View_Pin_Taxid_Seq_Key;
    private DataAccessProgramView vw_iaa_Dc_Cntrct_View;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Id_Nbr;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Tax_Id_Nbr;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Process_Type;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Req_Seq_Nbr;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Timestamp;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Payee_Cde;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Status_Cde;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Status_Timestamp;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Error_Msg;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Annt_Type;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Optn_Cde;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Issue_Dte;
    private DbsField iaa_Dc_Cntrct_View_Count_Castcntrct_Sttlmnt_Info_Cde;
    private DbsGroup iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_CdeMuGroup;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Xref_Ind;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Dod_Dte;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Nmbr;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Seq;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Pmt_Data_Nmbr;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Name;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Spirt_Code;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Delete_Code;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Brkdwn_Pct;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Lump_Sum_Avail;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Cont_Pmt_Avail;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Pymnt_Mode;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Needed_Ind;
    private DbsField iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Entered_Ind;
    private DbsField iaa_Dc_Cntrct_View_Pin_Taxid_Seq_Cntrct_Key;
    private DbsField pnd_Cntrct_Payee_Key;
    private DbsGroup pnd_Cntrct_Payee_KeyRedef6;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee;
    private DbsGroup pnd_Cntrct_Payee_Key_Pnd_Cpk_PayeeRedef7;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A;
    private DbsField pnd_Contract_1_8;
    private DbsField pnd_Sttlmnt_Reads;
    private DbsField pnd_Sttlmnt_Time_Selects;
    private DbsField pnd_Sttlmnt_Id_Complete_Selects;
    private DbsField pnd_Contract_Reads;
    private DbsField pnd_Ph_Info_Reads;
    private DbsField pnd_Contract_Partic_Reads;
    private DbsField pnd_Tax_03_Deced;
    private DbsField pnd_Sttlmnt_Id_Nbr;
    private DbsField pnd_Sttlmnt_Seq_Nbr;
    private DbsField pnd_Sttlmnt_Decedent_Type;
    private DbsField pnd_Sttlmnt_Ssn;
    private DbsField pnd_Sttlmnt_Dod_Dte_1;
    private DbsField pnd_Sttlmnt_Dod_Dte_2;
    private DbsField pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Work_Record_Writes_3;
    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Pnd_W_Name;
    private DbsGroup pnd_Work_Record_Pnd_W_NameRedef8;
    private DbsField pnd_Work_Record_Pnd_W_Last_Name;
    private DbsField pnd_Work_Record_Pnd_W_First_Name;
    private DbsField pnd_Work_Record_Pnd_W_Middle_Name;
    private DbsField pnd_Work_Record_Pnd_W_Cont_Stat;
    private DbsField pnd_Work_Record_Pnd_W_Pin;
    private DbsField pnd_Work_Record_Pnd_W_Ssn;
    private DbsField pnd_Work_Record_Pnd_W_Annu_Type;
    private DbsField pnd_Work_Record_Pnd_W_Contract_Mode;
    private DbsField pnd_Work_Record_Pnd_W_Dod_Dte;
    private DbsField pnd_Work_Record_Pnd_W_Nfp;
    private DbsField pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte;
    private DbsField pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde;
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_DateRedef9;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsGroup iaa_Parm_Card_Pnd_Parm_Date_NRedef10;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Cc;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S;
    private DbsGroup pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E;
    private DbsGroup pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_AlphRedef13;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd;
    private DbsField pnd_Fl_Time_Hhiiss_Alph;
    private DbsGroup pnd_Fl_Time_Hhiiss_AlphRedef15;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num;
    private DbsGroup pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii;
    private DbsField pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss;
    private DbsField pnd_Sy_Date_Yymmdd_Alph;
    private DbsGroup pnd_Sy_Date_Yymmdd_AlphRedef17;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num;
    private DbsGroup pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm;
    private DbsField pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd;
    private DbsField pnd_Sy_Time_Hhiiss_Alph;
    private DbsGroup pnd_Sy_Time_Hhiiss_AlphRedef19;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num;
    private DbsGroup pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii;
    private DbsField pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;
    private DbsField pnd_Num;
    private DbsField pnd_G;
    private DbsField pnd_Sttl_Comp;

    public DataAccessProgramView getVw_iaa_Cntrct_Prtcpnt_Role_View() { return vw_iaa_Cntrct_Prtcpnt_Role_View; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr() { return iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde() { return iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde() { return iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde; }

    public DbsGroup getIaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_CdeRedef1() { return iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_CdeRedef1; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_View_Pnd_Prtcpnt_Ctznshp_Cde() { return iaa_Cntrct_Prtcpnt_Role_View_Pnd_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde() { return iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind() { return iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_View_Cntrct_Payee_Key() { return iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Payee_Key; }

    public DataAccessProgramView getVw_iaa_Sttlmnt_View() { return vw_iaa_Sttlmnt_View; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Id_Nbr() { return iaa_Sttlmnt_View_Sttlmnt_Id_Nbr; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Tax_Id_Nbr() { return iaa_Sttlmnt_View_Sttlmnt_Tax_Id_Nbr; }

    public DbsGroup getIaa_Sttlmnt_View_Sttlmnt_Tax_Id_NbrRedef2() { return iaa_Sttlmnt_View_Sttlmnt_Tax_Id_NbrRedef2; }

    public DbsField getIaa_Sttlmnt_View_Pnd_Sttlmnt_Tax_Id_Nbr_A() { return iaa_Sttlmnt_View_Pnd_Sttlmnt_Tax_Id_Nbr_A; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Req_Seq_Nbr() { return iaa_Sttlmnt_View_Sttlmnt_Req_Seq_Nbr; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Process_Type() { return iaa_Sttlmnt_View_Sttlmnt_Process_Type; }

    public DbsGroup getIaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3() { return iaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3; }

    public DbsField getIaa_Sttlmnt_View_Pnd_Sttlmnt_Id() { return iaa_Sttlmnt_View_Pnd_Sttlmnt_Id; }

    public DbsField getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type() { return iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type; }

    public DbsField getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Filler() { return iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Filler; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Timestamp() { return iaa_Sttlmnt_View_Sttlmnt_Timestamp; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Status_Cde() { return iaa_Sttlmnt_View_Sttlmnt_Status_Cde; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Status_Timestamp() { return iaa_Sttlmnt_View_Sttlmnt_Status_Timestamp; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Cwf_Wpid() { return iaa_Sttlmnt_View_Sttlmnt_Cwf_Wpid; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Decedent_Name() { return iaa_Sttlmnt_View_Sttlmnt_Decedent_Name; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Decedent_Type() { return iaa_Sttlmnt_View_Sttlmnt_Decedent_Type; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Dod_Dte() { return iaa_Sttlmnt_View_Sttlmnt_Dod_Dte; }

    public DbsGroup getIaa_Sttlmnt_View_Sttlmnt_Dod_DteRedef4() { return iaa_Sttlmnt_View_Sttlmnt_Dod_DteRedef4; }

    public DbsField getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte() { return iaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_2nd_Dod_Dte() { return iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_Dte; }

    public DbsGroup getIaa_Sttlmnt_View_Sttlmnt_2nd_Dod_DteRedef5() { return iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_DteRedef5; }

    public DbsField getIaa_Sttlmnt_View_Pnd_Sttlmnt_2nd_Dod_Dte() { return iaa_Sttlmnt_View_Pnd_Sttlmnt_2nd_Dod_Dte; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Name() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Name; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr1() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr1; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr2() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr2; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr3() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr3; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr4() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr4; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_City() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_City; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_State() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_State; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Zip() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Zip; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Phone() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Phone; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notify_Date() { return iaa_Sttlmnt_View_Sttlmnt_Notify_Date; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Notifier_Rltnshp() { return iaa_Sttlmnt_View_Sttlmnt_Notifier_Rltnshp; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Cntrct_Count() { return iaa_Sttlmnt_View_Sttlmnt_Cntrct_Count; }

    public DbsField getIaa_Sttlmnt_View_Oper_Id() { return iaa_Sttlmnt_View_Oper_Id; }

    public DbsField getIaa_Sttlmnt_View_Oper_Timestamp() { return iaa_Sttlmnt_View_Oper_Timestamp; }

    public DbsField getIaa_Sttlmnt_View_Oper_User_Grp() { return iaa_Sttlmnt_View_Oper_User_Grp; }

    public DbsField getIaa_Sttlmnt_View_Verf_Id() { return iaa_Sttlmnt_View_Verf_Id; }

    public DbsField getIaa_Sttlmnt_View_Verf_Timestamp() { return iaa_Sttlmnt_View_Verf_Timestamp; }

    public DbsField getIaa_Sttlmnt_View_Verf_User_Grp() { return iaa_Sttlmnt_View_Verf_User_Grp; }

    public DbsField getIaa_Sttlmnt_View_Sttlmnt_Coding_Dte() { return iaa_Sttlmnt_View_Sttlmnt_Coding_Dte; }

    public DbsField getIaa_Sttlmnt_View_Pin_Taxid_Seq_Key() { return iaa_Sttlmnt_View_Pin_Taxid_Seq_Key; }

    public DataAccessProgramView getVw_iaa_Dc_Cntrct_View() { return vw_iaa_Dc_Cntrct_View; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Id_Nbr() { return iaa_Dc_Cntrct_View_Cntrct_Id_Nbr; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Tax_Id_Nbr() { return iaa_Dc_Cntrct_View_Cntrct_Tax_Id_Nbr; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Process_Type() { return iaa_Dc_Cntrct_View_Cntrct_Process_Type; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Req_Seq_Nbr() { return iaa_Dc_Cntrct_View_Cntrct_Req_Seq_Nbr; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Timestamp() { return iaa_Dc_Cntrct_View_Cntrct_Timestamp; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Ppcn_Nbr() { return iaa_Dc_Cntrct_View_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Payee_Cde() { return iaa_Dc_Cntrct_View_Cntrct_Payee_Cde; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Status_Cde() { return iaa_Dc_Cntrct_View_Cntrct_Status_Cde; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Status_Timestamp() { return iaa_Dc_Cntrct_View_Cntrct_Status_Timestamp; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Error_Msg() { return iaa_Dc_Cntrct_View_Cntrct_Error_Msg; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Annt_Type() { return iaa_Dc_Cntrct_View_Cntrct_Annt_Type; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Optn_Cde() { return iaa_Dc_Cntrct_View_Cntrct_Optn_Cde; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Issue_Dte() { return iaa_Dc_Cntrct_View_Cntrct_Issue_Dte; }

    public DbsField getIaa_Dc_Cntrct_View_Count_Castcntrct_Sttlmnt_Info_Cde() { return iaa_Dc_Cntrct_View_Count_Castcntrct_Sttlmnt_Info_Cde; }

    public DbsGroup getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_CdeMuGroup() { return iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_CdeMuGroup; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde() { return iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_First_Annt_Xref_Ind() { return iaa_Dc_Cntrct_View_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_First_Annt_Dod_Dte() { return iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_First_Annt_Dob_Dte() { return iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_First_Annt_Sex_Cde() { return iaa_Dc_Cntrct_View_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Bnfcry_Xref_Ind() { return iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Bnfcry_Dod_Dte() { return iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Nmbr() { return iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Nmbr; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Seq() { return iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Seq; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Pmt_Data_Nmbr() { return iaa_Dc_Cntrct_View_Cntrct_Pmt_Data_Nmbr; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Bnfcry_Name() { return iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Name; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Spirt_Code() { return iaa_Dc_Cntrct_View_Cntrct_Spirt_Code; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Delete_Code() { return iaa_Dc_Cntrct_View_Cntrct_Delete_Code; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Rwrttn_Ind() { return iaa_Dc_Cntrct_View_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Brkdwn_Pct() { return iaa_Dc_Cntrct_View_Cntrct_Brkdwn_Pct; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Lump_Sum_Avail() { return iaa_Dc_Cntrct_View_Cntrct_Lump_Sum_Avail; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Cont_Pmt_Avail() { return iaa_Dc_Cntrct_View_Cntrct_Cont_Pmt_Avail; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Pymnt_Mode() { return iaa_Dc_Cntrct_View_Cntrct_Pymnt_Mode; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Needed_Ind() { return iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Needed_Ind; }

    public DbsField getIaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Entered_Ind() { return iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Entered_Ind; }

    public DbsField getIaa_Dc_Cntrct_View_Pin_Taxid_Seq_Cntrct_Key() { return iaa_Dc_Cntrct_View_Pin_Taxid_Seq_Cntrct_Key; }

    public DbsField getPnd_Cntrct_Payee_Key() { return pnd_Cntrct_Payee_Key; }

    public DbsGroup getPnd_Cntrct_Payee_KeyRedef6() { return pnd_Cntrct_Payee_KeyRedef6; }

    public DbsField getPnd_Cntrct_Payee_Key_Pnd_Cpk_Contract() { return pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract; }

    public DbsField getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee() { return pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee; }

    public DbsGroup getPnd_Cntrct_Payee_Key_Pnd_Cpk_PayeeRedef7() { return pnd_Cntrct_Payee_Key_Pnd_Cpk_PayeeRedef7; }

    public DbsField getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A() { return pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A; }

    public DbsField getPnd_Contract_1_8() { return pnd_Contract_1_8; }

    public DbsField getPnd_Sttlmnt_Reads() { return pnd_Sttlmnt_Reads; }

    public DbsField getPnd_Sttlmnt_Time_Selects() { return pnd_Sttlmnt_Time_Selects; }

    public DbsField getPnd_Sttlmnt_Id_Complete_Selects() { return pnd_Sttlmnt_Id_Complete_Selects; }

    public DbsField getPnd_Contract_Reads() { return pnd_Contract_Reads; }

    public DbsField getPnd_Ph_Info_Reads() { return pnd_Ph_Info_Reads; }

    public DbsField getPnd_Contract_Partic_Reads() { return pnd_Contract_Partic_Reads; }

    public DbsField getPnd_Tax_03_Deced() { return pnd_Tax_03_Deced; }

    public DbsField getPnd_Sttlmnt_Id_Nbr() { return pnd_Sttlmnt_Id_Nbr; }

    public DbsField getPnd_Sttlmnt_Seq_Nbr() { return pnd_Sttlmnt_Seq_Nbr; }

    public DbsField getPnd_Sttlmnt_Decedent_Type() { return pnd_Sttlmnt_Decedent_Type; }

    public DbsField getPnd_Sttlmnt_Ssn() { return pnd_Sttlmnt_Ssn; }

    public DbsField getPnd_Sttlmnt_Dod_Dte_1() { return pnd_Sttlmnt_Dod_Dte_1; }

    public DbsField getPnd_Sttlmnt_Dod_Dte_2() { return pnd_Sttlmnt_Dod_Dte_2; }

    public DbsField getPnd_Cntrct_Optn_Cde() { return pnd_Cntrct_Optn_Cde; }

    public DbsField getPnd_Work_Record_Writes_3() { return pnd_Work_Record_Writes_3; }

    public DbsGroup getPnd_Work_Record() { return pnd_Work_Record; }

    public DbsField getPnd_Work_Record_Pnd_W_Name() { return pnd_Work_Record_Pnd_W_Name; }

    public DbsGroup getPnd_Work_Record_Pnd_W_NameRedef8() { return pnd_Work_Record_Pnd_W_NameRedef8; }

    public DbsField getPnd_Work_Record_Pnd_W_Last_Name() { return pnd_Work_Record_Pnd_W_Last_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_First_Name() { return pnd_Work_Record_Pnd_W_First_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_Middle_Name() { return pnd_Work_Record_Pnd_W_Middle_Name; }

    public DbsField getPnd_Work_Record_Pnd_W_Cont_Stat() { return pnd_Work_Record_Pnd_W_Cont_Stat; }

    public DbsField getPnd_Work_Record_Pnd_W_Pin() { return pnd_Work_Record_Pnd_W_Pin; }

    public DbsField getPnd_Work_Record_Pnd_W_Ssn() { return pnd_Work_Record_Pnd_W_Ssn; }

    public DbsField getPnd_Work_Record_Pnd_W_Annu_Type() { return pnd_Work_Record_Pnd_W_Annu_Type; }

    public DbsField getPnd_Work_Record_Pnd_W_Contract_Mode() { return pnd_Work_Record_Pnd_W_Contract_Mode; }

    public DbsField getPnd_Work_Record_Pnd_W_Dod_Dte() { return pnd_Work_Record_Pnd_W_Dod_Dte; }

    public DbsField getPnd_Work_Record_Pnd_W_Nfp() { return pnd_Work_Record_Pnd_W_Nfp; }

    public DbsField getPnd_Work_Record_Pnd_W_Cntrct_Issue_Dte() { return pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte; }

    public DbsField getPnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde() { return pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde; }

    public DbsField getPnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde() { return pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde; }

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date() { return iaa_Parm_Card_Pnd_Parm_Date; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_DateRedef9() { return iaa_Parm_Card_Pnd_Parm_DateRedef9; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_N() { return iaa_Parm_Card_Pnd_Parm_Date_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Parm_Date_NRedef10() { return iaa_Parm_Card_Pnd_Parm_Date_NRedef10; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Cc() { return iaa_Parm_Card_Pnd_Parm_Date_Cc; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Yy() { return iaa_Parm_Card_Pnd_Parm_Date_Yy; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Mm() { return iaa_Parm_Card_Pnd_Parm_Date_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Parm_Date_Dd() { return iaa_Parm_Card_Pnd_Parm_Date_Dd; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_S() { return pnd_Pin_Taxid_Seq_Cntrct_Key_S; }

    public DbsGroup getPnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11() { return pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S() { return pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S() { return pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S() { return pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S() { return pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S() { return pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_E() { return pnd_Pin_Taxid_Seq_Cntrct_Key_E; }

    public DbsGroup getPnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12() { return pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E() { return pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E() { return pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E() { return pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E() { return pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E; }

    public DbsField getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E() { return pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph() { return pnd_Fl_Date_Yyyymmdd_Alph; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_AlphRedef13() { return pnd_Fl_Date_Yyyymmdd_AlphRedef13; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num; }

    public DbsGroup getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14; 
        }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm; }

    public DbsField getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd() { return pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph() { return pnd_Fl_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_AlphRedef15() { return pnd_Fl_Time_Hhiiss_AlphRedef15; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii; }

    public DbsField getPnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss() { return pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph() { return pnd_Sy_Date_Yymmdd_Alph; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_AlphRedef17() { return pnd_Sy_Date_Yymmdd_AlphRedef17; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num; }

    public DbsGroup getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm; }

    public DbsField getPnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd() { return pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph() { return pnd_Sy_Time_Hhiiss_Alph; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_AlphRedef19() { return pnd_Sy_Time_Hhiiss_AlphRedef19; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num; }

    public DbsGroup getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii; }

    public DbsField getPnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss() { return pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss; }

    public DbsField getPnd_Sys_Date() { return pnd_Sys_Date; }

    public DbsField getPnd_Sys_Time() { return pnd_Sys_Time; }

    public DbsField getPnd_Num() { return pnd_Num; }

    public DbsField getPnd_G() { return pnd_G; }

    public DbsField getPnd_Sttl_Comp() { return pnd_Sttl_Comp; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cntrct_Prtcpnt_Role_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role_View", "IAA-CNTRCT-PRTCPNT-ROLE-VIEW"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_CdeRedef1 = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_CdeRedef1", 
            "Redefines", iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde);
        iaa_Cntrct_Prtcpnt_Role_View_Pnd_Prtcpnt_Ctznshp_Cde = iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_CdeRedef1.newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Pnd_Prtcpnt_Ctznshp_Cde", 
            "#PRTCPNT-CTZNSHP-CDE", FieldType.STRING, 3);
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind", 
            "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Payee_Key = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Payee_Key", 
            "CNTRCT-PAYEE-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_KEY");

        vw_iaa_Sttlmnt_View = new DataAccessProgramView(new NameInfo("vw_iaa_Sttlmnt_View", "IAA-STTLMNT-VIEW"), "IAA_DC_STTLMNT_REQ", "IA_DEATH_CLAIMS");
        iaa_Sttlmnt_View_Sttlmnt_Id_Nbr = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Id_Nbr", "STTLMNT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "STTLMNT_ID_NBR");
        iaa_Sttlmnt_View_Sttlmnt_Tax_Id_Nbr = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Tax_Id_Nbr", "STTLMNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "STTLMNT_TAX_ID_NBR");
        iaa_Sttlmnt_View_Sttlmnt_Tax_Id_NbrRedef2 = vw_iaa_Sttlmnt_View.getRecord().newGroupInGroup("iaa_Sttlmnt_View_Sttlmnt_Tax_Id_NbrRedef2", "Redefines", 
            iaa_Sttlmnt_View_Sttlmnt_Tax_Id_Nbr);
        iaa_Sttlmnt_View_Pnd_Sttlmnt_Tax_Id_Nbr_A = iaa_Sttlmnt_View_Sttlmnt_Tax_Id_NbrRedef2.newFieldInGroup("iaa_Sttlmnt_View_Pnd_Sttlmnt_Tax_Id_Nbr_A", 
            "#STTLMNT-TAX-ID-NBR-A", FieldType.STRING, 9);
        iaa_Sttlmnt_View_Sttlmnt_Req_Seq_Nbr = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Req_Seq_Nbr", "STTLMNT-REQ-SEQ-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STTLMNT_REQ_SEQ_NBR");
        iaa_Sttlmnt_View_Sttlmnt_Process_Type = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Process_Type", "STTLMNT-PROCESS-TYPE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "STTLMNT_PROCESS_TYPE");
        iaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3 = vw_iaa_Sttlmnt_View.getRecord().newGroupInGroup("iaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3", "Redefines", 
            iaa_Sttlmnt_View_Sttlmnt_Process_Type);
        iaa_Sttlmnt_View_Pnd_Sttlmnt_Id = iaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3.newFieldInGroup("iaa_Sttlmnt_View_Pnd_Sttlmnt_Id", "#STTLMNT-ID", 
            FieldType.STRING, 2);
        iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type = iaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3.newFieldInGroup("iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type", 
            "#STTLMNT-DCDNT-TYPE", FieldType.STRING, 1);
        iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Filler = iaa_Sttlmnt_View_Sttlmnt_Process_TypeRedef3.newFieldInGroup("iaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Filler", 
            "#STTLMNT-DCDNT-FILLER", FieldType.STRING, 1);
        iaa_Sttlmnt_View_Sttlmnt_Timestamp = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Timestamp", "STTLMNT-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STTLMNT_TIMESTAMP");
        iaa_Sttlmnt_View_Sttlmnt_Status_Cde = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Status_Cde", "STTLMNT-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "STTLMNT_STATUS_CDE");
        iaa_Sttlmnt_View_Sttlmnt_Status_Timestamp = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Status_Timestamp", "STTLMNT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STTLMNT_STATUS_TIMESTAMP");
        iaa_Sttlmnt_View_Sttlmnt_Cwf_Wpid = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Cwf_Wpid", "STTLMNT-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STTLMNT_CWF_WPID");
        iaa_Sttlmnt_View_Sttlmnt_Decedent_Name = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Decedent_Name", "STTLMNT-DECEDENT-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_NAME");
        iaa_Sttlmnt_View_Sttlmnt_Decedent_Type = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Decedent_Type", "STTLMNT-DECEDENT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_TYPE");
        iaa_Sttlmnt_View_Sttlmnt_Dod_Dte = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Dod_Dte", "STTLMNT-DOD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "STTLMNT_DOD_DTE");
        iaa_Sttlmnt_View_Sttlmnt_Dod_DteRedef4 = vw_iaa_Sttlmnt_View.getRecord().newGroupInGroup("iaa_Sttlmnt_View_Sttlmnt_Dod_DteRedef4", "Redefines", 
            iaa_Sttlmnt_View_Sttlmnt_Dod_Dte);
        iaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte = iaa_Sttlmnt_View_Sttlmnt_Dod_DteRedef4.newFieldInGroup("iaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte", "#STTLMNT-DOD-DTE", 
            FieldType.STRING, 8);
        iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_Dte = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_Dte", "STTLMNT-2ND-DOD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "STTLMNT_2ND_DOD_DTE");
        iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_DteRedef5 = vw_iaa_Sttlmnt_View.getRecord().newGroupInGroup("iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_DteRedef5", "Redefines", 
            iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_Dte);
        iaa_Sttlmnt_View_Pnd_Sttlmnt_2nd_Dod_Dte = iaa_Sttlmnt_View_Sttlmnt_2nd_Dod_DteRedef5.newFieldInGroup("iaa_Sttlmnt_View_Pnd_Sttlmnt_2nd_Dod_Dte", 
            "#STTLMNT-2ND-DOD-DTE", FieldType.STRING, 8);
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Name = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Name", "STTLMNT-NOTIFIER-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_NAME");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr1 = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr1", "STTLMNT-NOTIFIER-ADDR1", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR1");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr2 = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr2", "STTLMNT-NOTIFIER-ADDR2", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR2");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr3 = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr3", "STTLMNT-NOTIFIER-ADDR3", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR3");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr4 = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Addr4", "STTLMNT-NOTIFIER-ADDR4", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ADDR4");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_City = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_City", "STTLMNT-NOTIFIER-CITY", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_CITY");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_State = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_State", "STTLMNT-NOTIFIER-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_STATE");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Zip = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Zip", "STTLMNT-NOTIFIER-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_ZIP");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Phone = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Phone", "STTLMNT-NOTIFIER-PHONE", 
            FieldType.STRING, 14, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_PHONE");
        iaa_Sttlmnt_View_Sttlmnt_Notify_Date = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notify_Date", "STTLMNT-NOTIFY-DATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "STTLMNT_NOTIFY_DATE");
        iaa_Sttlmnt_View_Sttlmnt_Notifier_Rltnshp = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Notifier_Rltnshp", "STTLMNT-NOTIFIER-RLTNSHP", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "STTLMNT_NOTIFIER_RLTNSHP");
        iaa_Sttlmnt_View_Sttlmnt_Cntrct_Count = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Cntrct_Count", "STTLMNT-CNTRCT-COUNT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "STTLMNT_CNTRCT_COUNT");
        iaa_Sttlmnt_View_Oper_Id = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Oper_Id", "OPER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OVRPYMNT_OPER_ID");
        iaa_Sttlmnt_View_Oper_Timestamp = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Oper_Timestamp", "OPER-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "FETV_OPER_TIMESTAMP");
        iaa_Sttlmnt_View_Oper_User_Grp = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Oper_User_Grp", "OPER-USER-GRP", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "OVRPYMNT_OPER_USER_GRP");
        iaa_Sttlmnt_View_Verf_Id = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Verf_Id", "VERF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "FETV_VERF_ID");
        iaa_Sttlmnt_View_Verf_Timestamp = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Verf_Timestamp", "VERF-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "BNFCRY_VERF_TIMESTAMP");
        iaa_Sttlmnt_View_Verf_User_Grp = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Verf_User_Grp", "VERF-USER-GRP", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "FETV_VERF_USER_GRP");
        iaa_Sttlmnt_View_Sttlmnt_Coding_Dte = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Sttlmnt_Coding_Dte", "STTLMNT-CODING-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "STTLMNT_CODING_DTE");
        iaa_Sttlmnt_View_Pin_Taxid_Seq_Key = vw_iaa_Sttlmnt_View.getRecord().newFieldInGroup("iaa_Sttlmnt_View_Pin_Taxid_Seq_Key", "PIN-TAXID-SEQ-KEY", 
            FieldType.BINARY, 24, RepeatingFieldStrategy.None, "PIN_TAXID_SEQ_KEY");

        vw_iaa_Dc_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_iaa_Dc_Cntrct_View", "IAA-DC-CNTRCT-VIEW"), "IAA_DC_CNTRCT", "IA_DEATH_CLAIMS");
        iaa_Dc_Cntrct_View_Cntrct_Id_Nbr = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Id_Nbr", "CNTRCT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_ID_NBR");
        iaa_Dc_Cntrct_View_Cntrct_Tax_Id_Nbr = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Tax_Id_Nbr", "CNTRCT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_TAX_ID_NBR");
        iaa_Dc_Cntrct_View_Cntrct_Process_Type = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Process_Type", "CNTRCT-PROCESS-TYPE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PROCESS_TYPE");
        iaa_Dc_Cntrct_View_Cntrct_Req_Seq_Nbr = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Req_Seq_Nbr", "CNTRCT-REQ-SEQ-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_REQ_SEQ_NBR");
        iaa_Dc_Cntrct_View_Cntrct_Timestamp = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Timestamp", "CNTRCT-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRCT_TIMESTAMP");
        iaa_Dc_Cntrct_View_Cntrct_Ppcn_Nbr = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Dc_Cntrct_View_Cntrct_Payee_Cde = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Dc_Cntrct_View_Cntrct_Status_Cde = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Status_Cde", "CNTRCT-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_STATUS_CDE");
        iaa_Dc_Cntrct_View_Cntrct_Status_Timestamp = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Status_Timestamp", "CNTRCT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRCT_STATUS_TIMESTAMP");
        iaa_Dc_Cntrct_View_Cntrct_Error_Msg = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Error_Msg", "CNTRCT-ERROR-MSG", 
            FieldType.STRING, 40, RepeatingFieldStrategy.None, "CNTRCT_ERROR_MSG");
        iaa_Dc_Cntrct_View_Cntrct_Annt_Type = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Annt_Type", "CNTRCT-ANNT-TYPE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_ANNT_TYPE");
        iaa_Dc_Cntrct_View_Cntrct_Optn_Cde = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Dc_Cntrct_View_Cntrct_Issue_Dte = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Dc_Cntrct_View_Count_Castcntrct_Sttlmnt_Info_Cde = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Count_Castcntrct_Sttlmnt_Info_Cde", 
            "C*CNTRCT-STTLMNT-INFO-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_CNTRCT_STTLMNT_INFO_CDE");
        iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_CdeMuGroup = vw_iaa_Dc_Cntrct_View.getRecord().newGroupInGroup("iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_CdeMuGroup", 
            "CNTRCT_STTLMNT_INFO_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_DEATH_CLAIMS_CNTRCT_STTLMNT_INFO_CDE");
        iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde = iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_CdeMuGroup.newFieldArrayInGroup("iaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde", 
            "CNTRCT-STTLMNT-INFO-CDE", FieldType.STRING, 4, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_STTLMNT_INFO_CDE");
        iaa_Dc_Cntrct_View_Cntrct_First_Annt_Xref_Ind = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_First_Annt_Xref_Ind", 
            "CNTRCT-FIRST-ANNT-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dod_Dte = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dod_Dte", 
            "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dob_Dte = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_First_Annt_Dob_Dte", 
            "CNTRCT-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Dc_Cntrct_View_Cntrct_First_Annt_Sex_Cde = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_First_Annt_Sex_Cde", 
            "CNTRCT-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind", 
            "CNTRCT-SCND-ANNT-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte", 
            "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte", 
            "CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde", 
            "CNTRCT-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Xref_Ind = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Xref_Ind", "CNTRCT-BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_XREF_IND");
        iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Dod_Dte = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Dod_Dte", "CNTRCT-BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_DOD_DTE");
        iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Nmbr = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Nmbr", "CNTRCT-BNFCRY-GRP-NMBR", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_GRP_NMBR");
        iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Seq = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Grp_Seq", "CNTRCT-BNFCRY-GRP-SEQ", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_GRP_SEQ");
        iaa_Dc_Cntrct_View_Cntrct_Pmt_Data_Nmbr = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Pmt_Data_Nmbr", "CNTRCT-PMT-DATA-NMBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_PMT_DATA_NMBR");
        iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Name = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Bnfcry_Name", "CNTRCT-BNFCRY-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CNTRCT_BNFCRY_NAME");
        iaa_Dc_Cntrct_View_Cntrct_Spirt_Code = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Spirt_Code", "CNTRCT-SPIRT-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CODE");
        iaa_Dc_Cntrct_View_Cntrct_Delete_Code = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Delete_Code", "CNTRCT-DELETE-CODE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_DELETE_CODE");
        iaa_Dc_Cntrct_View_Cntrct_Rwrttn_Ind = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Dc_Cntrct_View_Cntrct_Brkdwn_Pct = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Brkdwn_Pct", "CNTRCT-BRKDWN-PCT", 
            FieldType.NUMERIC, 7, 4, RepeatingFieldStrategy.None, "CNTRCT_BRKDWN_PCT");
        iaa_Dc_Cntrct_View_Cntrct_Lump_Sum_Avail = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Lump_Sum_Avail", "CNTRCT-LUMP-SUM-AVAIL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_LUMP_SUM_AVAIL");
        iaa_Dc_Cntrct_View_Cntrct_Cont_Pmt_Avail = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Cont_Pmt_Avail", "CNTRCT-CONT-PMT-AVAIL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CONT_PMT_AVAIL");
        iaa_Dc_Cntrct_View_Cntrct_Pymnt_Mode = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Pymnt_Mode", "CNTRCT-PYMNT-MODE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MODE");
        iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Needed_Ind = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Needed_Ind", 
            "CNTRCT-HIST-RATE-NEEDED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HIST_RATE_NEEDED_IND");
        iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Entered_Ind = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Cntrct_Hist_Rate_Entered_Ind", 
            "CNTRCT-HIST-RATE-ENTERED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HIST_RATE_ENTERED_IND");
        iaa_Dc_Cntrct_View_Pin_Taxid_Seq_Cntrct_Key = vw_iaa_Dc_Cntrct_View.getRecord().newFieldInGroup("iaa_Dc_Cntrct_View_Pin_Taxid_Seq_Cntrct_Key", 
            "PIN-TAXID-SEQ-CNTRCT-KEY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "PIN_TAXID_SEQ_CNTRCT_KEY");

        pnd_Cntrct_Payee_Key = newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);
        pnd_Cntrct_Payee_KeyRedef6 = newGroupInRecord("pnd_Cntrct_Payee_KeyRedef6", "Redefines", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract = pnd_Cntrct_Payee_KeyRedef6.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cpk_Contract", "#CPK-CONTRACT", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee = pnd_Cntrct_Payee_KeyRedef6.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee", "#CPK-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Payee_Key_Pnd_Cpk_PayeeRedef7 = pnd_Cntrct_Payee_KeyRedef6.newGroupInGroup("pnd_Cntrct_Payee_Key_Pnd_Cpk_PayeeRedef7", "Redefines", 
            pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee);
        pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A = pnd_Cntrct_Payee_Key_Pnd_Cpk_PayeeRedef7.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A", "#CPK-PAYEE-A", 
            FieldType.STRING, 2);

        pnd_Contract_1_8 = newFieldInRecord("pnd_Contract_1_8", "#CONTRACT-1-8", FieldType.STRING, 8);

        pnd_Sttlmnt_Reads = newFieldInRecord("pnd_Sttlmnt_Reads", "#STTLMNT-READS", FieldType.NUMERIC, 9);

        pnd_Sttlmnt_Time_Selects = newFieldInRecord("pnd_Sttlmnt_Time_Selects", "#STTLMNT-TIME-SELECTS", FieldType.NUMERIC, 9);

        pnd_Sttlmnt_Id_Complete_Selects = newFieldInRecord("pnd_Sttlmnt_Id_Complete_Selects", "#STTLMNT-ID-COMPLETE-SELECTS", FieldType.NUMERIC, 9);

        pnd_Contract_Reads = newFieldInRecord("pnd_Contract_Reads", "#CONTRACT-READS", FieldType.NUMERIC, 9);

        pnd_Ph_Info_Reads = newFieldInRecord("pnd_Ph_Info_Reads", "#PH-INFO-READS", FieldType.NUMERIC, 9);

        pnd_Contract_Partic_Reads = newFieldInRecord("pnd_Contract_Partic_Reads", "#CONTRACT-PARTIC-READS", FieldType.NUMERIC, 9);

        pnd_Tax_03_Deced = newFieldInRecord("pnd_Tax_03_Deced", "#TAX-03-DECED", FieldType.NUMERIC, 9);

        pnd_Sttlmnt_Id_Nbr = newFieldInRecord("pnd_Sttlmnt_Id_Nbr", "#STTLMNT-ID-NBR", FieldType.NUMERIC, 12);

        pnd_Sttlmnt_Seq_Nbr = newFieldInRecord("pnd_Sttlmnt_Seq_Nbr", "#STTLMNT-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Sttlmnt_Decedent_Type = newFieldInRecord("pnd_Sttlmnt_Decedent_Type", "#STTLMNT-DECEDENT-TYPE", FieldType.STRING, 2);

        pnd_Sttlmnt_Ssn = newFieldInRecord("pnd_Sttlmnt_Ssn", "#STTLMNT-SSN", FieldType.NUMERIC, 9);

        pnd_Sttlmnt_Dod_Dte_1 = newFieldInRecord("pnd_Sttlmnt_Dod_Dte_1", "#STTLMNT-DOD-DTE-1", FieldType.STRING, 8);

        pnd_Sttlmnt_Dod_Dte_2 = newFieldInRecord("pnd_Sttlmnt_Dod_Dte_2", "#STTLMNT-DOD-DTE-2", FieldType.STRING, 8);

        pnd_Cntrct_Optn_Cde = newFieldInRecord("pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);

        pnd_Work_Record_Writes_3 = newFieldInRecord("pnd_Work_Record_Writes_3", "#WORK-RECORD-WRITES-3", FieldType.NUMERIC, 9);

        pnd_Work_Record = newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Pnd_W_Name = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Name", "#W-NAME", FieldType.STRING, 38);
        pnd_Work_Record_Pnd_W_NameRedef8 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record_Pnd_W_NameRedef8", "Redefines", pnd_Work_Record_Pnd_W_Name);
        pnd_Work_Record_Pnd_W_Last_Name = pnd_Work_Record_Pnd_W_NameRedef8.newFieldInGroup("pnd_Work_Record_Pnd_W_Last_Name", "#W-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Work_Record_Pnd_W_First_Name = pnd_Work_Record_Pnd_W_NameRedef8.newFieldInGroup("pnd_Work_Record_Pnd_W_First_Name", "#W-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Work_Record_Pnd_W_Middle_Name = pnd_Work_Record_Pnd_W_NameRedef8.newFieldInGroup("pnd_Work_Record_Pnd_W_Middle_Name", "#W-MIDDLE-NAME", FieldType.STRING, 
            12);
        pnd_Work_Record_Pnd_W_Cont_Stat = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cont_Stat", "#W-CONT-STAT", FieldType.STRING, 11);
        pnd_Work_Record_Pnd_W_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Pnd_W_Ssn = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Ssn", "#W-SSN", FieldType.STRING, 9);
        pnd_Work_Record_Pnd_W_Annu_Type = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Annu_Type", "#W-ANNU-TYPE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_W_Contract_Mode = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Contract_Mode", "#W-CONTRACT-MODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Pnd_W_Dod_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Dod_Dte", "#W-DOD-DTE", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_W_Nfp = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Nfp", "#W-NFP", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Cntrct_Issue_Dte", "#W-CNTRCT-ISSUE-DTE", FieldType.STRING, 
            6);
        pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde", "#W-PRTCPNT-CTZNSHP-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde", "#W-PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3);

        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);
        iaa_Parm_Card_Pnd_Parm_DateRedef9 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_DateRedef9", "Redefines", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card_Pnd_Parm_DateRedef9.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 
            8);
        iaa_Parm_Card_Pnd_Parm_Date_NRedef10 = iaa_Parm_Card_Pnd_Parm_DateRedef9.newGroupInGroup("iaa_Parm_Card_Pnd_Parm_Date_NRedef10", "Redefines", 
            iaa_Parm_Card_Pnd_Parm_Date_N);
        iaa_Parm_Card_Pnd_Parm_Date_Cc = iaa_Parm_Card_Pnd_Parm_Date_NRedef10.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Cc", "#PARM-DATE-CC", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Yy = iaa_Parm_Card_Pnd_Parm_Date_NRedef10.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yy", "#PARM-DATE-YY", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card_Pnd_Parm_Date_NRedef10.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card_Pnd_Parm_Date_NRedef10.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Pin_Taxid_Seq_Cntrct_Key_S = newFieldInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_S", "#PIN-TAXID-SEQ-CNTRCT-KEY-S", FieldType.STRING, 37);
        pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11 = newGroupInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11", "Redefines", pnd_Pin_Taxid_Seq_Cntrct_Key_S);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S", 
            "#PTSCK-ID-NBR-S", FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S", 
            "#PTSCK-TAX-ID-NBR-S", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S", 
            "#PTSCK-REQ-SEQ-NBR-S", FieldType.NUMERIC, 3);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S = pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S", 
            "#PTSCK-PPCN-NBR-S", FieldType.STRING, 11);
        pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S = pnd_Pin_Taxid_Seq_Cntrct_Key_SRedef11.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S", 
            "#PTSCK-PAYEE-CDE-S", FieldType.NUMERIC, 2);

        pnd_Pin_Taxid_Seq_Cntrct_Key_E = newFieldInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_E", "#PIN-TAXID-SEQ-CNTRCT-KEY-E", FieldType.STRING, 37);
        pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12 = newGroupInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12", "Redefines", pnd_Pin_Taxid_Seq_Cntrct_Key_E);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E", 
            "#PTSCK-ID-NBR-E", FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E", 
            "#PTSCK-TAX-ID-NBR-E", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E", 
            "#PTSCK-REQ-SEQ-NBR-E", FieldType.NUMERIC, 3);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E = pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E", 
            "#PTSCK-PPCN-NBR-E", FieldType.STRING, 11);
        pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E = pnd_Pin_Taxid_Seq_Cntrct_Key_ERedef12.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E", 
            "#PTSCK-PAYEE-CDE-E", FieldType.NUMERIC, 2);

        pnd_Fl_Date_Yyyymmdd_Alph = newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Fl_Date_Yyyymmdd_AlphRedef13 = newGroupInRecord("pnd_Fl_Date_Yyyymmdd_AlphRedef13", "Redefines", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_AlphRedef13.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14 = pnd_Fl_Date_Yyyymmdd_AlphRedef13.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14", 
            "Redefines", pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd = pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_NumRedef14.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Dd", 
            "#FL-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Fl_Time_Hhiiss_Alph = newFieldInRecord("pnd_Fl_Time_Hhiiss_Alph", "#FL-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Fl_Time_Hhiiss_AlphRedef15 = newGroupInRecord("pnd_Fl_Time_Hhiiss_AlphRedef15", "Redefines", pnd_Fl_Time_Hhiiss_Alph);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num = pnd_Fl_Time_Hhiiss_AlphRedef15.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num", 
            "#FL-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16 = pnd_Fl_Time_Hhiiss_AlphRedef15.newGroupInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16", 
            "Redefines", pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_Num);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hh", 
            "#FL-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ii", 
            "#FL-TIME-II", FieldType.NUMERIC, 2);
        pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss = pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Hhiiss_NumRedef16.newFieldInGroup("pnd_Fl_Time_Hhiiss_Alph_Pnd_Fl_Time_Ss", 
            "#FL-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sy_Date_Yymmdd_Alph = newFieldInRecord("pnd_Sy_Date_Yymmdd_Alph", "#SY-DATE-YYMMDD-ALPH", FieldType.STRING, 8);
        pnd_Sy_Date_Yymmdd_AlphRedef17 = newGroupInRecord("pnd_Sy_Date_Yymmdd_AlphRedef17", "Redefines", pnd_Sy_Date_Yymmdd_Alph);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num = pnd_Sy_Date_Yymmdd_AlphRedef17.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num", 
            "#SY-DATE-YYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18 = pnd_Sy_Date_Yymmdd_AlphRedef17.newGroupInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18", 
            "Redefines", pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_Num);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Cc", 
            "#SY-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yy", 
            "#SY-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Mm", 
            "#SY-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd = pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Yymmdd_NumRedef18.newFieldInGroup("pnd_Sy_Date_Yymmdd_Alph_Pnd_Sy_Date_Dd", 
            "#SY-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Sy_Time_Hhiiss_Alph = newFieldInRecord("pnd_Sy_Time_Hhiiss_Alph", "#SY-TIME-HHIISS-ALPH", FieldType.STRING, 6);
        pnd_Sy_Time_Hhiiss_AlphRedef19 = newGroupInRecord("pnd_Sy_Time_Hhiiss_AlphRedef19", "Redefines", pnd_Sy_Time_Hhiiss_Alph);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num = pnd_Sy_Time_Hhiiss_AlphRedef19.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num", 
            "#SY-TIME-HHIISS-NUM", FieldType.NUMERIC, 6);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20 = pnd_Sy_Time_Hhiiss_AlphRedef19.newGroupInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20", 
            "Redefines", pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_Num);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hh", 
            "#SY-TIME-HH", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ii", 
            "#SY-TIME-II", FieldType.NUMERIC, 2);
        pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss = pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Hhiiss_NumRedef20.newFieldInGroup("pnd_Sy_Time_Hhiiss_Alph_Pnd_Sy_Time_Ss", 
            "#SY-TIME-SS", FieldType.NUMERIC, 2);

        pnd_Sys_Date = newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);

        pnd_Sys_Time = newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);

        pnd_Num = newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 3);

        pnd_G = newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 3);

        pnd_Sttl_Comp = newFieldInRecord("pnd_Sttl_Comp", "#STTL-COMP", FieldType.STRING, 1);

        this.setRecordName("LdaIaal583s");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrct_Prtcpnt_Role_View.reset();
        vw_iaa_Sttlmnt_View.reset();
        vw_iaa_Dc_Cntrct_View.reset();
    }

    // Constructor
    public LdaIaal583s() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
