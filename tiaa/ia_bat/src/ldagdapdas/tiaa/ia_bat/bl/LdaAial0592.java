/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:35 PM
**        * FROM NATURAL LDA     : AIAL0592
************************************************************
**        * FILE NAME            : LdaAial0592.java
**        * CLASS NAME           : LdaAial0592
**        * INSTANCE NAME        : LdaAial0592
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0592 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans_View;
    private DbsField iaa_Tiaa_Fund_Trans_View_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_CdeRedef1;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_View_Trans_Check_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_View_Aftr_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tckr_Symbl;
    private DbsGroup iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key_2;
    private DbsField iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key_2;

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Trans_View() { return vw_iaa_Tiaa_Fund_Trans_View; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Trans_Dte() { return iaa_Tiaa_Fund_Trans_View_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Invrse_Trans_Dte() { return iaa_Tiaa_Fund_Trans_View_Invrse_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Trans_View_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_CdeRedef1() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_CdeRedef1; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_View_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Trans_Check_Dte() { return iaa_Tiaa_Fund_Trans_View_Trans_Check_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Bfre_Imge_Id() { return iaa_Tiaa_Fund_Trans_View_Bfre_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Aftr_Imge_Id() { return iaa_Tiaa_Fund_Trans_View_Aftr_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tckr_Symbl() { return iaa_Tiaa_Fund_Trans_View_Tckr_Symbl; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_View_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Cmpny_Fund; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key_2() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key_2; }

    public DbsField getIaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key_2() { return iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Tiaa_Fund_Trans_View = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans_View", "IAA-TIAA-FUND-TRANS-VIEW"), "IAA_TIAA_FUND_TRANS", 
            "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_View_Trans_Dte = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Trans_Dte", "TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_View_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Invrse_Trans_Dte", 
            "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_View_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Ppcn_Nbr", 
            "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Payee_Cde", 
            "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde", 
            "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_CdeRedef1 = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_CdeRedef1", 
            "Redefines", iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Cde", 
            "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Cde", 
            "TIAA-FUND-CDE", FieldType.STRING, 2);
        iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Per_Amt", 
            "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Div_Amt", 
            "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Per_Amt", 
            "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Div_Amt", 
            "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_View_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp", 
            "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Cde", 
            "TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Dte", 
            "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Pay_Amt", 
            "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Per_Div_Amt", 
            "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Units_Cnt", 
            "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_View_Trans_Check_Dte = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Trans_Check_Dte", 
            "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Tiaa_Fund_Trans_View_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Bfre_Imge_Id", "BFRE-IMGE-ID", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_View_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Aftr_Imge_Id", "AFTR-IMGE-ID", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Xfr_Iss_Dte", 
            "TIAA-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_In_Dte", 
            "TIAA-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Lst_Xfr_Out_Dte", 
            "TIAA-LST-XFR-OUT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Trans_View_Tckr_Symbl = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tckr_Symbl", "TCKR-SYMBL", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_GicMuGroup", 
            "TIAA_RATE_GICMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Rate_Gic", 
            "TIAA-RATE-GIC", FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Mode_Ind", "TIAA-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Old_Cmpny_Fund", 
            "TIAA-OLD-CMPNY-FUND", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key", 
            "TIAA-FUND-BFRE-KEY", FieldType.BINARY, 22, RepeatingFieldStrategy.None, "CREF_FUND_BFRE_KEY");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key", 
            "TIAA-FUND-AFTR-KEY", FieldType.STRING, 27, RepeatingFieldStrategy.None, "TIAA_FUND_AFTR_KEY");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key_2 = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Bfre_Key_2", 
            "TIAA-FUND-BFRE-KEY-2", FieldType.BINARY, 23, RepeatingFieldStrategy.None, "CREF_FUND_BFRE_KEY_2");
        iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key_2 = vw_iaa_Tiaa_Fund_Trans_View.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Aftr_Key_2", 
            "TIAA-FUND-AFTR-KEY-2", FieldType.STRING, 28, RepeatingFieldStrategy.None, "CREF_FUND_AFTR_KEY_2");
        vw_iaa_Tiaa_Fund_Trans_View.setUniquePeList();

        this.setRecordName("LdaAial0592");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Tiaa_Fund_Trans_View.reset();
    }

    // Constructor
    public LdaAial0592() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
