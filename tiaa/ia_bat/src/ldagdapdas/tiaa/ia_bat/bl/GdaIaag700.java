/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:48:48 PM
**        * FROM NATURAL GDA     : IAAG700
************************************************************
**        * FILE NAME            : GdaIaag700.java
**        * CLASS NAME           : GdaIaag700
**        * INSTANCE NAME        : GdaIaag700
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaIaag700 extends DbsRecord
{
    private static ThreadLocal<List<GdaIaag700>> _instance = new ThreadLocal<List<GdaIaag700>>();

    // Properties
    private DbsGroup pnd_Hld;
    private DbsField pnd_Hld_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Hld_Cntrct_Payee;
    private DbsGroup pnd_Hld_Cntrct_PayeeRedef1;
    private DbsField pnd_Hld_Cntrct_Nmbr;
    private DbsField pnd_Hld_Cntrct_Payee_Cde;
    private DbsField pnd_Hld_Pnd_Cntrct_Name_Add_K;
    private DbsGroup pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2;
    private DbsField pnd_Hld_Cntrct_Name_Free_K;
    private DbsField pnd_Hld_Addrss_Lne_1_K;
    private DbsField pnd_Hld_Addrss_Lne_2_K;
    private DbsField pnd_Hld_Addrss_Lne_3_K;
    private DbsField pnd_Hld_Addrss_Lne_4_K;
    private DbsField pnd_Hld_Addrss_Lne_5_K;
    private DbsField pnd_Hld_Addrss_Lne_6_K;
    private DbsField pnd_Hld_Addrss_Postal_Data_K;
    private DbsGroup pnd_Hld_Addrss_Postal_Data_KRedef3;
    private DbsField pnd_Hld_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Hld_Addrss_Carrier_Rte_K;
    private DbsField pnd_Hld_Addrss_Walk_Rte_K;
    private DbsField pnd_Hld_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Hld_Pnd_Rest_Of_Record_K;
    private DbsGroup pnd_Hld_Pnd_Rest_Of_Record_KRedef4;
    private DbsField pnd_Hld_Addrss_Type_Cde_K;
    private DbsField pnd_Hld_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Hld_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Hld_Permanent_Addrss_Ind_K;
    private DbsField pnd_Hld_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Hld_Bank_Aba_Acct_Nmbr_KRedef5;
    private DbsField pnd_Hld_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Hld_Eft_Status_Ind_K;
    private DbsField pnd_Hld_Checking_Saving_Cde_K;
    private DbsField pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Hld_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Hld_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Hld_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Hld_Addrss_Geographic_Cde_K;
    private DbsField pnd_Hld_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Hld_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Hld_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Hld_Pnd_Cntrct_Name_Add_R;
    private DbsGroup pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6;
    private DbsField pnd_Hld_Cntrct_Name_Free_R;
    private DbsField pnd_Hld_Addrss_Lne_1_R;
    private DbsField pnd_Hld_Addrss_Lne_2_R;
    private DbsField pnd_Hld_Addrss_Lne_3_R;
    private DbsField pnd_Hld_Addrss_Lne_4_R;
    private DbsField pnd_Hld_Addrss_Lne_5_R;
    private DbsField pnd_Hld_Addrss_Lne_6_R;
    private DbsField pnd_Hld_Addrss_Postal_Data_R;
    private DbsGroup pnd_Hld_Addrss_Postal_Data_RRedef7;
    private DbsField pnd_Hld_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Hld_Addrss_Carrier_Rte_R;
    private DbsField pnd_Hld_Addrss_Walk_Rte_R;
    private DbsField pnd_Hld_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Hld_Pnd_Rest_Of_Record_R;
    private DbsGroup pnd_Hld_Pnd_Rest_Of_Record_RRedef8;
    private DbsField pnd_Hld_Addrss_Type_Cde_R;
    private DbsField pnd_Hld_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Hld_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Hld_Permanent_Addrss_Ind_R;
    private DbsField pnd_Hld_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Hld_Bank_Aba_Acct_Nmbr_RRedef9;
    private DbsField pnd_Hld_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Hld_Eft_Status_Ind_R;
    private DbsField pnd_Hld_Checking_Saving_Cde_R;
    private DbsField pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Hld_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Hld_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Hld_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Hld_Addrss_Geographic_Cde_R;
    private DbsField pnd_Hld_Pnd_V_Cntrct_Name_Add_K;
    private DbsGroup pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10;
    private DbsField pnd_Hld_V_Cntrct_Name_Free_K;
    private DbsField pnd_Hld_V_Addrss_Lne_1_K;
    private DbsField pnd_Hld_V_Addrss_Lne_2_K;
    private DbsField pnd_Hld_V_Addrss_Lne_3_K;
    private DbsField pnd_Hld_V_Addrss_Lne_4_K;
    private DbsField pnd_Hld_V_Addrss_Lne_5_K;
    private DbsField pnd_Hld_V_Addrss_Lne_6_K;
    private DbsField pnd_Hld_V_Addrss_Postal_Data_K;
    private DbsGroup pnd_Hld_V_Addrss_Postal_Data_KRedef11;
    private DbsField pnd_Hld_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Hld_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Hld_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Hld_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Hld_Pnd_V_Rest_Of_Record_K;
    private DbsGroup pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12;
    private DbsField pnd_Hld_V_Addrss_Type_Cde_K;
    private DbsField pnd_Hld_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Hld_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Hld_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Hld_V_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Hld_V_Bank_Aba_Acct_Nmbr_KRedef13;
    private DbsField pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Hld_V_Eft_Status_Ind_K;
    private DbsField pnd_Hld_V_Checking_Saving_Cde_K;
    private DbsField pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Hld_V_Pnding_Addrss_Chnge_Dte_K;
    private DbsField pnd_Hld_V_Pnding_Addrss_Restore_Dte_K;
    private DbsField pnd_Hld_V_Pnding_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Hld_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Hld_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Hld_V_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Hld_V_Iintl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Hld_V_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Hld_Pnd_V_Cntrct_Name_Add_R;
    private DbsGroup pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14;
    private DbsField pnd_Hld_V_Cntrct_Name_Free_R;
    private DbsField pnd_Hld_V_Addrss_Lne_1_R;
    private DbsField pnd_Hld_V_Addrss_Lne_2_R;
    private DbsField pnd_Hld_V_Addrss_Lne_3_R;
    private DbsField pnd_Hld_V_Addrss_Lne_4_R;
    private DbsField pnd_Hld_V_Addrss_Lne_5_R;
    private DbsField pnd_Hld_V_Addrss_Lne_6_R;
    private DbsField pnd_Hld_V_Addrss_Postal_Data_R;
    private DbsGroup pnd_Hld_V_Addrss_Postal_Data_RRedef15;
    private DbsField pnd_Hld_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Hld_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Hld_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Hld_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Hld_Pnd_V_Rest_Of_Record_R;
    private DbsGroup pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16;
    private DbsField pnd_Hld_V_Addrss_Type_Cde_R;
    private DbsField pnd_Hld_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Hld_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Hld_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Hld_V_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Hld_V_Bank_Aba_Acct_Nmbr_RRedef17;
    private DbsField pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Hld_V_Eft_Status_Ind_R;
    private DbsField pnd_Hld_V_Checking_Saving_Cde_R;
    private DbsField pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Hld_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Hld_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Hld_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Hld_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Hld_V_Addrss_Geographic_Cde_R;
    private DbsField pnd_Hld_Global_Indicator;
    private DbsField pnd_Hld_Global_Country;
    private DbsField pnd_Hld_Legal_State;
    private DbsField pnd_Core;
    private DbsGroup pnd_CoreRedef18;
    private DbsField pnd_Core_Cntrct_Payee;
    private DbsGroup pnd_Core_Cntrct_PayeeRedef19;
    private DbsField pnd_Core_Cntrct_Nbr;
    private DbsField pnd_Core_Cntrct_Payee_Cde;
    private DbsField pnd_Core_Ph_Unique_Id_Nbr;
    private DbsField pnd_Core_Ph_Rcd_Type_Cde;
    private DbsField pnd_Core_Ph_Social_Security_No;
    private DbsField pnd_Core_Ph_Dob_Dte;
    private DbsField pnd_Core_Ph_Dod_Dte;
    private DbsGroup pnd_Core_Ph_Nme;
    private DbsField pnd_Core_Ph_Last_Nme;
    private DbsGroup pnd_Core_Ph_Last_NmeRedef20;
    private DbsField pnd_Core_Ph_Last_Nme_16;
    private DbsField pnd_Core_Ph_First_Nme;
    private DbsGroup pnd_Core_Ph_First_NmeRedef21;
    private DbsField pnd_Core_Ph_First_Nme_10;
    private DbsField pnd_Core_Ph_Mddle_Nme;
    private DbsGroup pnd_Core_Ph_Mddle_NmeRedef22;
    private DbsField pnd_Core_Ph_Mddle_Nme_12;
    private DbsField pnd_Core_Cntrct_Status_Cde;
    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;
    private DbsGroup pnd_Input_Pnd_Cntrct_PayeeRedef23;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;
    private DbsGroup pnd_Input_Pnd_Payee_CdeRedef24;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;
    private DbsGroup pnd_Input_Pnd_Rest_Of_Record_353Redef25;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;
    private DbsGroup pnd_Input_Pnd_Rest_Of_Record_353Redef26;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;
    private DbsGroup pnd_Input_Pnd_Issue_DteRedef27;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;
    private DbsGroup pnd_Input_Pnd_Crrncy_CdeRedef28;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_F5;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input_Pnd_F6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_F7;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Type;
    private DbsField pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Pnd_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;
    private DbsField pnd_Input_Pnd_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Cntrct_Nmbr;
    private DbsGroup pnd_Input_Pnd_Rest_Of_Record_353Redef29;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;
    private DbsGroup pnd_Input_Pnd_Ddctn_CdeRedef30;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;
    private DbsGroup pnd_Input_Pnd_Ddctn_Seq_NbrRedef31;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;
    private DbsGroup pnd_Input_Pnd_Rest_Of_Record_353Redef32;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;
    private DbsGroup pnd_Input_Pnd_Summ_Per_DvdndRedef33;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;
    private DbsGroup pnd_Input_Pnd_Rest_Of_Record_353Redef34;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_F17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_F20;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;
    private DbsGroup pnd_Input_Pnd_Pend_DteRedef35;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_F22;
    private DbsField pnd_Input_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Input_Pnd_F23;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;
    private DbsGroup gtn_Pda_E;
    private DbsField gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time;
    private DbsField gtn_Pda_E_Cntrct_Unq_Id_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Ppcn_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Cmbn_Nbr;
    private DbsGroup gtn_Pda_E_Cntrct_Cmbn_NbrRedef36;
    private DbsField gtn_Pda_E_Cntrct_Cmbn_Nbr_A10;
    private DbsField gtn_Pda_E_Cntrct_Cmbn_Nbr_Payee;
    private DbsField gtn_Pda_E_Annt_Soc_Sec_Ind;
    private DbsField gtn_Pda_E_Annt_Soc_Sec_Nbr;
    private DbsField gtn_Pda_E_Annt_Ctznshp_Cde;
    private DbsField gtn_Pda_E_Annt_Rsdncy_Cde;
    private DbsField gtn_Pda_E_Annt_Locality_Cde;
    private DbsGroup gtn_Pda_E_Ph_Name;
    private DbsField gtn_Pda_E_Ph_Last_Name;
    private DbsField gtn_Pda_E_Ph_Middle_Name;
    private DbsField gtn_Pda_E_Ph_First_Name;
    private DbsField gtn_Pda_E_Pymnt_Dob;
    private DbsGroup gtn_Pda_E_Pymnt_Nme_And_Addr_Grp;
    private DbsField gtn_Pda_E_Pymnt_Nme;
    private DbsField gtn_Pda_E_Pymnt_Addr_Lines;
    private DbsGroup gtn_Pda_E_Pymnt_Addr_LinesRedef37;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line_Txt;
    private DbsGroup gtn_Pda_E_Pymnt_Addr_LinesRedef38;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line1_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line2_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line3_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line4_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line5_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line6_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Zip_Cde;
    private DbsField gtn_Pda_E_Pymnt_Postl_Data;
    private DbsField gtn_Pda_E_Pymnt_Addr_Type_Ind;
    private DbsField gtn_Pda_E_Pymnt_Foreign_Cde;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme;
    private DbsField gtn_Pda_E_Pymnt_Addr_Chg_Ind;
    private DbsField gtn_Pda_E_Pymnt_Settl_Ivc_Ind;
    private DbsGroup gtn_Pda_E_Pnd_Pnd_Fund_Payee;
    private DbsField gtn_Pda_E_Inv_Acct_Cde;
    private DbsField gtn_Pda_E_Inv_Acct_Settl_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Cntrct_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Dvdnd_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Unit_Value;
    private DbsField gtn_Pda_E_Inv_Acct_Unit_Qty;
    private DbsField gtn_Pda_E_Inv_Acct_Ivc_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Ivc_Ind;
    private DbsField gtn_Pda_E_Inv_Acct_Valuat_Period;
    private DbsGroup gtn_Pda_E_Pnd_Pnd_Deductions;
    private DbsField gtn_Pda_E_Pymnt_Ded_Cde;
    private DbsField gtn_Pda_E_Pymnt_Ded_Amt;
    private DbsField gtn_Pda_E_Pymnt_Ded_Payee_Cde;
    private DbsGroup gtn_Pda_E_Pymnt_Ded_Payee_CdeRedef39;
    private DbsField gtn_Pda_E_Pymnt_Ded_Payee_Cde_5;
    private DbsField gtn_Pda_E_Pymnt_Ded_Payee_Cde_3;
    private DbsField gtn_Pda_E_Pymnt_Payee_Tax_Ind;
    private DbsField gtn_Pda_E_Pymnt_Settlmnt_Dte;
    private DbsField gtn_Pda_E_Pymnt_Check_Dte;
    private DbsField gtn_Pda_E_Pymnt_Cycle_Dte;
    private DbsField gtn_Pda_E_Pymnt_Acctg_Dte;
    private DbsField gtn_Pda_E_Pymnt_Ia_Issue_Dte;
    private DbsField gtn_Pda_E_Pymnt_Intrfce_Dte;
    private DbsField gtn_Pda_E_Cntrct_Check_Crrncy_Cde;
    private DbsField gtn_Pda_E_Cntrct_Orgn_Cde;
    private DbsField gtn_Pda_E_Cntrct_Type_Cde;
    private DbsField gtn_Pda_E_Cntrct_Payee_Cde;
    private DbsField gtn_Pda_E_Cntrct_Option_Cde;
    private DbsField gtn_Pda_E_Cntrct_Ac_Lt_10yrs;
    private DbsField gtn_Pda_E_Cntrct_Mode_Cde;
    private DbsField gtn_Pda_E_Pymnt_Pay_Type_Req_Ind;
    private DbsField gtn_Pda_E_Pymnt_Spouse_Pay_Stats;
    private DbsField gtn_Pda_E_Cntrct_Pymnt_Type_Ind;
    private DbsField gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind;
    private DbsField gtn_Pda_E_Cntrct_Pymnt_Dest_Cde;
    private DbsField gtn_Pda_E_Cntrct_Roll_Dest_Cde;
    private DbsField gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde;
    private DbsField gtn_Pda_E_Pymnt_Eft_Acct_Nbr;
    private DbsField gtn_Pda_E_Pymnt_Eft_Transit_Id_A;
    private DbsGroup gtn_Pda_E_Pymnt_Eft_Transit_Id_ARedef40;
    private DbsField gtn_Pda_E_Pymnt_Eft_Transit_Id;
    private DbsField gtn_Pda_E_Pymnt_Chk_Sav_Ind;
    private DbsField gtn_Pda_E_Cntrct_Hold_Cde;
    private DbsField gtn_Pda_E_Cntrct_Hold_Ind;
    private DbsField gtn_Pda_E_Cntrct_Hold_Grp;
    private DbsField gtn_Pda_E_Cntrct_Hold_User_Id;
    private DbsField gtn_Pda_E_Pymnt_Tax_Exempt_Ind;
    private DbsField gtn_Pda_E_Cntrct_Qlfied_Cde;
    private DbsField gtn_Pda_E_Cntrct_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Sub_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Ia_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Annty_Ins_Type;
    private DbsField gtn_Pda_E_Cntrct_Annty_Type_Cde;
    private DbsField gtn_Pda_E_Cntrct_Insurance_Option;
    private DbsField gtn_Pda_E_Cntrct_Life_Contingency;
    private DbsField gtn_Pda_E_Pymnt_Suspend_Cde;
    private DbsField gtn_Pda_E_Pymnt_Suspend_Dte;
    private DbsField gtn_Pda_E_Pnd_Pnd_This_Pymnt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts;
    private DbsField gtn_Pda_E_Gtn_Ret_Code;
    private DbsGroup gtn_Pda_E_Gtn_Ret_CodeRedef41;
    private DbsField gtn_Pda_E_Global_Country;
    private DbsField gtn_Pda_E_Temp_Pend_Cde;
    private DbsField gtn_Pda_E_Error_Code_1;
    private DbsField gtn_Pda_E_Error_Code_2;
    private DbsField gtn_Pda_E_Current_Mode;
    private DbsField gtn_Pda_E_Egtrra_Eligibility_Ind;
    private DbsField gtn_Pda_E_Originating_Irs_Cde;
    private DbsGroup gtn_Pda_E_Originating_Irs_CdeRedef42;
    private DbsField gtn_Pda_E_Distributing_Irc_Cde;
    private DbsField gtn_Pda_E_Receiving_Irc_Cde;
    private DbsField gtn_Pda_E_Cntrct_Money_Source;
    private DbsField gtn_Pda_E_Roth_Dob;
    private DbsField gtn_Pda_E_Roth_First_Contrib_Dte;
    private DbsField gtn_Pda_E_Roth_Death_Dte;
    private DbsGroup gtn_Pda_E_Roth_Death_DteRedef43;
    private DbsField gtn_Pda_E_Roth_Death_Dte_N6;
    private DbsField gtn_Pda_E_Roth_Death_Dte_Dd;
    private DbsField gtn_Pda_E_Roth_Disability_Dte;
    private DbsField gtn_Pda_E_Roth_Money_Source;
    private DbsField gtn_Pda_E_Roth_Qual_Non_Qual_Distrib;
    private DbsField gtn_Pda_E_Ia_Orgn_Cde;
    private DbsField gtn_Pda_E_Plan_Number;
    private DbsField gtn_Pda_E_Sub_Plan;
    private DbsField gtn_Pda_E_Orig_Cntrct_Nbr;
    private DbsField gtn_Pda_E_Orig_Sub_Plan;
    private DbsGroup pnd_Name_01;
    private DbsField pnd_Name_01_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_01_Cntrct_Payee;
    private DbsGroup pnd_Name_01_Cntrct_PayeeRedef44;
    private DbsField pnd_Name_01_Cntrct_Nmbr;
    private DbsField pnd_Name_01_Cntrct_Payee_Cde;
    private DbsField pnd_Name_01_Pnd_Cntrct_Name_Add_K;
    private DbsGroup pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45;
    private DbsField pnd_Name_01_Cntrct_Name_Free_K;
    private DbsField pnd_Name_01_Addrss_Lne_1_K;
    private DbsField pnd_Name_01_Addrss_Lne_2_K;
    private DbsField pnd_Name_01_Addrss_Lne_3_K;
    private DbsField pnd_Name_01_Addrss_Lne_4_K;
    private DbsField pnd_Name_01_Addrss_Lne_5_K;
    private DbsField pnd_Name_01_Addrss_Lne_6_K;
    private DbsField pnd_Name_01_Addrss_Postal_Data_K;
    private DbsGroup pnd_Name_01_Addrss_Postal_Data_KRedef46;
    private DbsField pnd_Name_01_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_01_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_01_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_01_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_01_Pnd_Rest_Of_Record_K;
    private DbsGroup pnd_Name_01_Pnd_Rest_Of_Record_KRedef47;
    private DbsField pnd_Name_01_Addrss_Type_Cde_K;
    private DbsField pnd_Name_01_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_01_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_01_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_01_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Name_01_Bank_Aba_Acct_Nmbr_KRedef48;
    private DbsField pnd_Name_01_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Name_01_Eft_Status_Ind_K;
    private DbsField pnd_Name_01_Checking_Saving_Cde_K;
    private DbsField pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_01_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_01_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_01_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_01_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_01_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_01_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_01_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_01_Pnd_Cntrct_Name_Add_R;
    private DbsGroup pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49;
    private DbsField pnd_Name_01_Cntrct_Name_Free_R;
    private DbsField pnd_Name_01_Addrss_Lne_1_R;
    private DbsField pnd_Name_01_Addrss_Lne_2_R;
    private DbsField pnd_Name_01_Addrss_Lne_3_R;
    private DbsField pnd_Name_01_Addrss_Lne_4_R;
    private DbsField pnd_Name_01_Addrss_Lne_5_R;
    private DbsField pnd_Name_01_Addrss_Lne_6_R;
    private DbsField pnd_Name_01_Addrss_Postal_Data_R;
    private DbsGroup pnd_Name_01_Addrss_Postal_Data_RRedef50;
    private DbsField pnd_Name_01_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_01_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_01_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_01_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_01_Pnd_Rest_Of_Record_R;
    private DbsGroup pnd_Name_01_Pnd_Rest_Of_Record_RRedef51;
    private DbsField pnd_Name_01_Addrss_Type_Cde_R;
    private DbsField pnd_Name_01_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_01_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_01_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_01_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Name_01_Bank_Aba_Acct_Nmbr_RRedef52;
    private DbsField pnd_Name_01_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Name_01_Eft_Status_Ind_R;
    private DbsField pnd_Name_01_Checking_Saving_Cde_R;
    private DbsField pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_01_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_01_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_01_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_01_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_01_Pnd_V_Cntrct_Name_Add_K;
    private DbsGroup pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53;
    private DbsField pnd_Name_01_V_Cntrct_Name_Free_K;
    private DbsField pnd_Name_01_V_Addrss_Lne_1_K;
    private DbsField pnd_Name_01_V_Addrss_Lne_2_K;
    private DbsField pnd_Name_01_V_Addrss_Lne_3_K;
    private DbsField pnd_Name_01_V_Addrss_Lne_4_K;
    private DbsField pnd_Name_01_V_Addrss_Lne_5_K;
    private DbsField pnd_Name_01_V_Addrss_Lne_6_K;
    private DbsField pnd_Name_01_V_Addrss_Postal_Data_K;
    private DbsGroup pnd_Name_01_V_Addrss_Postal_Data_KRedef54;
    private DbsField pnd_Name_01_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_01_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_01_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_01_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_01_Pnd_V_Rest_Of_Record_K;
    private DbsGroup pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55;
    private DbsField pnd_Name_01_V_Addrss_Type_Cde_K;
    private DbsField pnd_Name_01_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_01_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_01_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_01_V_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Name_01_V_Bank_Aba_Acct_Nmbr_KRedef56;
    private DbsField pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Name_01_V_Eft_Status_Ind_K;
    private DbsField pnd_Name_01_V_Checking_Saving_Cde_K;
    private DbsField pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_01_V_Pnding_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_01_V_Pnding_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_01_V_Pnding_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_01_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_01_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_01_V_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_01_V_Iintl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_01_V_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_01_Pnd_V_Cntrct_Name_Add_R;
    private DbsGroup pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57;
    private DbsField pnd_Name_01_V_Cntrct_Name_Free_R;
    private DbsField pnd_Name_01_V_Addrss_Lne_1_R;
    private DbsField pnd_Name_01_V_Addrss_Lne_2_R;
    private DbsField pnd_Name_01_V_Addrss_Lne_3_R;
    private DbsField pnd_Name_01_V_Addrss_Lne_4_R;
    private DbsField pnd_Name_01_V_Addrss_Lne_5_R;
    private DbsField pnd_Name_01_V_Addrss_Lne_6_R;
    private DbsField pnd_Name_01_V_Addrss_Postal_Data_R;
    private DbsGroup pnd_Name_01_V_Addrss_Postal_Data_RRedef58;
    private DbsField pnd_Name_01_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_01_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_01_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_01_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_01_Pnd_V_Rest_Of_Record_R;
    private DbsGroup pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59;
    private DbsField pnd_Name_01_V_Addrss_Type_Cde_R;
    private DbsField pnd_Name_01_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_01_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_01_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_01_V_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Name_01_V_Bank_Aba_Acct_Nmbr_RRedef60;
    private DbsField pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Name_01_V_Eft_Status_Ind_R;
    private DbsField pnd_Name_01_V_Checking_Saving_Cde_R;
    private DbsField pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_01_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_01_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_01_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Name_01_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_01_V_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_01_Global_Indicator;
    private DbsField pnd_Name_01_Global_Country;
    private DbsField pnd_Name_01_Legal_State;
    private DbsGroup pnd_Name_Save;
    private DbsField pnd_Name_Save_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Save_Cntrct_Payee;
    private DbsGroup pnd_Name_Save_Cntrct_PayeeRedef61;
    private DbsField pnd_Name_Save_Cntrct_Nmbr;
    private DbsField pnd_Name_Save_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Save_Pnd_Cntrct_Name_Add_K;
    private DbsGroup pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62;
    private DbsField pnd_Name_Save_Cntrct_Name_Free_K;
    private DbsField pnd_Name_Save_Addrss_Lne_1_K;
    private DbsField pnd_Name_Save_Addrss_Lne_2_K;
    private DbsField pnd_Name_Save_Addrss_Lne_3_K;
    private DbsField pnd_Name_Save_Addrss_Lne_4_K;
    private DbsField pnd_Name_Save_Addrss_Lne_5_K;
    private DbsField pnd_Name_Save_Addrss_Lne_6_K;
    private DbsField pnd_Name_Save_Addrss_Postal_Data_K;
    private DbsGroup pnd_Name_Save_Addrss_Postal_Data_KRedef63;
    private DbsField pnd_Name_Save_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_Save_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_Save_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_Save_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Save_Pnd_Rest_Of_Record_K;
    private DbsGroup pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64;
    private DbsField pnd_Name_Save_Addrss_Type_Cde_K;
    private DbsField pnd_Name_Save_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_Save_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_Save_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_Save_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Name_Save_Bank_Aba_Acct_Nmbr_KRedef65;
    private DbsField pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Name_Save_Eft_Status_Ind_K;
    private DbsField pnd_Name_Save_Checking_Saving_Cde_K;
    private DbsField pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_Save_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Save_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Save_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_Save_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Save_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Save_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Save_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Save_Pnd_Cntrct_Name_Add_R;
    private DbsGroup pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66;
    private DbsField pnd_Name_Save_Cntrct_Name_Free_R;
    private DbsField pnd_Name_Save_Addrss_Lne_1_R;
    private DbsField pnd_Name_Save_Addrss_Lne_2_R;
    private DbsField pnd_Name_Save_Addrss_Lne_3_R;
    private DbsField pnd_Name_Save_Addrss_Lne_4_R;
    private DbsField pnd_Name_Save_Addrss_Lne_5_R;
    private DbsField pnd_Name_Save_Addrss_Lne_6_R;
    private DbsField pnd_Name_Save_Addrss_Postal_Data_R;
    private DbsGroup pnd_Name_Save_Addrss_Postal_Data_RRedef67;
    private DbsField pnd_Name_Save_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_Save_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_Save_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_Save_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Save_Pnd_Rest_Of_Record_R;
    private DbsGroup pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68;
    private DbsField pnd_Name_Save_Addrss_Type_Cde_R;
    private DbsField pnd_Name_Save_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_Save_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_Save_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_Save_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Name_Save_Bank_Aba_Acct_Nmbr_RRedef69;
    private DbsField pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Name_Save_Eft_Status_Ind_R;
    private DbsField pnd_Name_Save_Checking_Saving_Cde_R;
    private DbsField pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_Save_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Save_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Save_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_Save_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Save_Pnd_V_Cntrct_Name_Add_K;
    private DbsGroup pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70;
    private DbsField pnd_Name_Save_V_Cntrct_Name_Free_K;
    private DbsField pnd_Name_Save_V_Addrss_Lne_1_K;
    private DbsField pnd_Name_Save_V_Addrss_Lne_2_K;
    private DbsField pnd_Name_Save_V_Addrss_Lne_3_K;
    private DbsField pnd_Name_Save_V_Addrss_Lne_4_K;
    private DbsField pnd_Name_Save_V_Addrss_Lne_5_K;
    private DbsField pnd_Name_Save_V_Addrss_Lne_6_K;
    private DbsField pnd_Name_Save_V_Addrss_Postal_Data_K;
    private DbsGroup pnd_Name_Save_V_Addrss_Postal_Data_KRedef71;
    private DbsField pnd_Name_Save_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_Save_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_Save_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_Save_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Save_Pnd_V_Rest_Of_Record_K;
    private DbsGroup pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72;
    private DbsField pnd_Name_Save_V_Addrss_Type_Cde_K;
    private DbsField pnd_Name_Save_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_Save_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_Save_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_KRedef73;
    private DbsField pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Name_Save_V_Eft_Status_Ind_K;
    private DbsField pnd_Name_Save_V_Checking_Saving_Cde_K;
    private DbsField pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_Save_V_Pnding_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Save_V_Pnding_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_Save_V_Pnding_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Save_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_Save_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Save_V_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Save_V_Iintl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Save_V_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Save_Pnd_V_Cntrct_Name_Add_R;
    private DbsGroup pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74;
    private DbsField pnd_Name_Save_V_Cntrct_Name_Free_R;
    private DbsField pnd_Name_Save_V_Addrss_Lne_1_R;
    private DbsField pnd_Name_Save_V_Addrss_Lne_2_R;
    private DbsField pnd_Name_Save_V_Addrss_Lne_3_R;
    private DbsField pnd_Name_Save_V_Addrss_Lne_4_R;
    private DbsField pnd_Name_Save_V_Addrss_Lne_5_R;
    private DbsField pnd_Name_Save_V_Addrss_Lne_6_R;
    private DbsField pnd_Name_Save_V_Addrss_Postal_Data_R;
    private DbsGroup pnd_Name_Save_V_Addrss_Postal_Data_RRedef75;
    private DbsField pnd_Name_Save_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_Save_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_Save_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_Save_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Save_Pnd_V_Rest_Of_Record_R;
    private DbsGroup pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76;
    private DbsField pnd_Name_Save_V_Addrss_Type_Cde_R;
    private DbsField pnd_Name_Save_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_Save_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_Save_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_RRedef77;
    private DbsField pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Name_Save_V_Eft_Status_Ind_R;
    private DbsField pnd_Name_Save_V_Checking_Saving_Cde_R;
    private DbsField pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_Save_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Save_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_Save_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Name_Save_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_Save_V_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Save_Global_Indicator;
    private DbsField pnd_Name_Save_Global_Country;
    private DbsField pnd_Name_Save_Legal_State;
    private DbsGroup pnd_Name;
    private DbsField pnd_Name_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee;
    private DbsGroup pnd_Name_Cntrct_PayeeRedef78;
    private DbsField pnd_Name_Cntrct_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_K;
    private DbsGroup pnd_Name_Pnd_Cntrct_Name_Add_KRedef79;
    private DbsField pnd_Name_Cntrct_Name_Free_K;
    private DbsField pnd_Name_Addrss_Lne_1_K;
    private DbsField pnd_Name_Addrss_Lne_2_K;
    private DbsField pnd_Name_Addrss_Lne_3_K;
    private DbsField pnd_Name_Addrss_Lne_4_K;
    private DbsField pnd_Name_Addrss_Lne_5_K;
    private DbsField pnd_Name_Addrss_Lne_6_K;
    private DbsField pnd_Name_Addrss_Postal_Data_K;
    private DbsGroup pnd_Name_Addrss_Postal_Data_KRedef80;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_K;
    private DbsGroup pnd_Name_Pnd_Rest_Of_Record_KRedef81;
    private DbsField pnd_Name_Addrss_Type_Cde_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Name_Bank_Aba_Acct_Nmbr_KRedef82;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Name_Eft_Status_Ind_K;
    private DbsField pnd_Name_Checking_Saving_Cde_K;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_R;
    private DbsGroup pnd_Name_Pnd_Cntrct_Name_Add_RRedef83;
    private DbsField pnd_Name_Cntrct_Name_Free_R;
    private DbsField pnd_Name_Addrss_Lne_1_R;
    private DbsField pnd_Name_Addrss_Lne_2_R;
    private DbsField pnd_Name_Addrss_Lne_3_R;
    private DbsField pnd_Name_Addrss_Lne_4_R;
    private DbsField pnd_Name_Addrss_Lne_5_R;
    private DbsField pnd_Name_Addrss_Lne_6_R;
    private DbsField pnd_Name_Addrss_Postal_Data_R;
    private DbsGroup pnd_Name_Addrss_Postal_Data_RRedef84;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_R;
    private DbsGroup pnd_Name_Pnd_Rest_Of_Record_RRedef85;
    private DbsField pnd_Name_Addrss_Type_Cde_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Name_Bank_Aba_Acct_Nmbr_RRedef86;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Name_Eft_Status_Ind_R;
    private DbsField pnd_Name_Checking_Saving_Cde_R;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Pnd_V_Cntrct_Name_Add_K;
    private DbsGroup pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87;
    private DbsField pnd_Name_V_Cntrct_Name_Free_K;
    private DbsField pnd_Name_V_Addrss_Lne_1_K;
    private DbsField pnd_Name_V_Addrss_Lne_2_K;
    private DbsField pnd_Name_V_Addrss_Lne_3_K;
    private DbsField pnd_Name_V_Addrss_Lne_4_K;
    private DbsField pnd_Name_V_Addrss_Lne_5_K;
    private DbsField pnd_Name_V_Addrss_Lne_6_K;
    private DbsField pnd_Name_V_Addrss_Postal_Data_K;
    private DbsGroup pnd_Name_V_Addrss_Postal_Data_KRedef88;
    private DbsField pnd_Name_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Pnd_V_Rest_Of_Record_K;
    private DbsGroup pnd_Name_Pnd_V_Rest_Of_Record_KRedef89;
    private DbsField pnd_Name_V_Addrss_Type_Cde_K;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_K;
    private DbsGroup pnd_Name_V_Bank_Aba_Acct_Nmbr_KRedef90;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_N_K;
    private DbsField pnd_Name_V_Eft_Status_Ind_K;
    private DbsField pnd_Name_V_Checking_Saving_Cde_K;
    private DbsField pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_V_Pnding_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_V_Pnding_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_V_Pnding_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_V_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_V_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Pnd_V_Cntrct_Name_Add_R;
    private DbsGroup pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91;
    private DbsField pnd_Name_V_Cntrct_Name_Free_R;
    private DbsField pnd_Name_V_Addrss_Lne_1_R;
    private DbsField pnd_Name_V_Addrss_Lne_2_R;
    private DbsField pnd_Name_V_Addrss_Lne_3_R;
    private DbsField pnd_Name_V_Addrss_Lne_4_R;
    private DbsField pnd_Name_V_Addrss_Lne_5_R;
    private DbsField pnd_Name_V_Addrss_Lne_6_R;
    private DbsField pnd_Name_V_Addrss_Postal_Data_R;
    private DbsGroup pnd_Name_V_Addrss_Postal_Data_RRedef92;
    private DbsField pnd_Name_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Pnd_V_Rest_Of_Record_R;
    private DbsGroup pnd_Name_Pnd_V_Rest_Of_Record_RRedef93;
    private DbsField pnd_Name_V_Addrss_Type_Cde_R;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_R;
    private DbsGroup pnd_Name_V_Bank_Aba_Acct_Nmbr_RRedef94;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_N_R;
    private DbsField pnd_Name_V_Eft_Status_Ind_R;
    private DbsField pnd_Name_V_Checking_Saving_Cde_R;
    private DbsField pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Name_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_V_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Global_Indicator;
    private DbsField pnd_Name_Global_Country;
    private DbsField pnd_Name_Legal_State;

    public DbsGroup getPnd_Hld() { return pnd_Hld; }

    public DbsField getPnd_Hld_Ph_Unque_Id_Nmbr() { return pnd_Hld_Ph_Unque_Id_Nmbr; }

    public DbsField getPnd_Hld_Cntrct_Payee() { return pnd_Hld_Cntrct_Payee; }

    public DbsGroup getPnd_Hld_Cntrct_PayeeRedef1() { return pnd_Hld_Cntrct_PayeeRedef1; }

    public DbsField getPnd_Hld_Cntrct_Nmbr() { return pnd_Hld_Cntrct_Nmbr; }

    public DbsField getPnd_Hld_Cntrct_Payee_Cde() { return pnd_Hld_Cntrct_Payee_Cde; }

    public DbsField getPnd_Hld_Pnd_Cntrct_Name_Add_K() { return pnd_Hld_Pnd_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Hld_Pnd_Cntrct_Name_Add_KRedef2() { return pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2; }

    public DbsField getPnd_Hld_Cntrct_Name_Free_K() { return pnd_Hld_Cntrct_Name_Free_K; }

    public DbsField getPnd_Hld_Addrss_Lne_1_K() { return pnd_Hld_Addrss_Lne_1_K; }

    public DbsField getPnd_Hld_Addrss_Lne_2_K() { return pnd_Hld_Addrss_Lne_2_K; }

    public DbsField getPnd_Hld_Addrss_Lne_3_K() { return pnd_Hld_Addrss_Lne_3_K; }

    public DbsField getPnd_Hld_Addrss_Lne_4_K() { return pnd_Hld_Addrss_Lne_4_K; }

    public DbsField getPnd_Hld_Addrss_Lne_5_K() { return pnd_Hld_Addrss_Lne_5_K; }

    public DbsField getPnd_Hld_Addrss_Lne_6_K() { return pnd_Hld_Addrss_Lne_6_K; }

    public DbsField getPnd_Hld_Addrss_Postal_Data_K() { return pnd_Hld_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Hld_Addrss_Postal_Data_KRedef3() { return pnd_Hld_Addrss_Postal_Data_KRedef3; }

    public DbsField getPnd_Hld_Addrss_Zip_Plus_4_K() { return pnd_Hld_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Hld_Addrss_Carrier_Rte_K() { return pnd_Hld_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Hld_Addrss_Walk_Rte_K() { return pnd_Hld_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Hld_Addrss_Usps_Future_Use_K() { return pnd_Hld_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Hld_Pnd_Rest_Of_Record_K() { return pnd_Hld_Pnd_Rest_Of_Record_K; }

    public DbsGroup getPnd_Hld_Pnd_Rest_Of_Record_KRedef4() { return pnd_Hld_Pnd_Rest_Of_Record_KRedef4; }

    public DbsField getPnd_Hld_Addrss_Type_Cde_K() { return pnd_Hld_Addrss_Type_Cde_K; }

    public DbsField getPnd_Hld_Addrss_Last_Chnge_Dte_K() { return pnd_Hld_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Hld_Addrss_Last_Chnge_Time_K() { return pnd_Hld_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Hld_Permanent_Addrss_Ind_K() { return pnd_Hld_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Hld_Bank_Aba_Acct_Nmbr_K() { return pnd_Hld_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Hld_Bank_Aba_Acct_Nmbr_KRedef5() { return pnd_Hld_Bank_Aba_Acct_Nmbr_KRedef5; }

    public DbsField getPnd_Hld_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Hld_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Hld_Eft_Status_Ind_K() { return pnd_Hld_Eft_Status_Ind_K; }

    public DbsField getPnd_Hld_Checking_Saving_Cde_K() { return pnd_Hld_Checking_Saving_Cde_K; }

    public DbsField getPnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Hld_Pending_Addrss_Chnge_Dte_K() { return pnd_Hld_Pending_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Hld_Pending_Addrss_Restore_Dte_K() { return pnd_Hld_Pending_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Hld_Pending_Perm_Addrss_Chnge_Dte_K() { return pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Hld_Check_Mailing_Addrss_Ind_K() { return pnd_Hld_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Hld_Addrss_Geographic_Cde_K() { return pnd_Hld_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Hld_Intl_Eft_Pay_Type_Cde() { return pnd_Hld_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Hld_Intl_Bank_Pymnt_Eft_Nmbr() { return pnd_Hld_Intl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Hld_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Hld_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Hld_Pnd_Cntrct_Name_Add_R() { return pnd_Hld_Pnd_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Hld_Pnd_Cntrct_Name_Add_RRedef6() { return pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6; }

    public DbsField getPnd_Hld_Cntrct_Name_Free_R() { return pnd_Hld_Cntrct_Name_Free_R; }

    public DbsField getPnd_Hld_Addrss_Lne_1_R() { return pnd_Hld_Addrss_Lne_1_R; }

    public DbsField getPnd_Hld_Addrss_Lne_2_R() { return pnd_Hld_Addrss_Lne_2_R; }

    public DbsField getPnd_Hld_Addrss_Lne_3_R() { return pnd_Hld_Addrss_Lne_3_R; }

    public DbsField getPnd_Hld_Addrss_Lne_4_R() { return pnd_Hld_Addrss_Lne_4_R; }

    public DbsField getPnd_Hld_Addrss_Lne_5_R() { return pnd_Hld_Addrss_Lne_5_R; }

    public DbsField getPnd_Hld_Addrss_Lne_6_R() { return pnd_Hld_Addrss_Lne_6_R; }

    public DbsField getPnd_Hld_Addrss_Postal_Data_R() { return pnd_Hld_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Hld_Addrss_Postal_Data_RRedef7() { return pnd_Hld_Addrss_Postal_Data_RRedef7; }

    public DbsField getPnd_Hld_Addrss_Zip_Plus_4_R() { return pnd_Hld_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Hld_Addrss_Carrier_Rte_R() { return pnd_Hld_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Hld_Addrss_Walk_Rte_R() { return pnd_Hld_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Hld_Addrss_Usps_Future_Use_R() { return pnd_Hld_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Hld_Pnd_Rest_Of_Record_R() { return pnd_Hld_Pnd_Rest_Of_Record_R; }

    public DbsGroup getPnd_Hld_Pnd_Rest_Of_Record_RRedef8() { return pnd_Hld_Pnd_Rest_Of_Record_RRedef8; }

    public DbsField getPnd_Hld_Addrss_Type_Cde_R() { return pnd_Hld_Addrss_Type_Cde_R; }

    public DbsField getPnd_Hld_Addrss_Last_Chnge_Dte_R() { return pnd_Hld_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Hld_Addrss_Last_Chnge_Time_R() { return pnd_Hld_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Hld_Permanent_Addrss_Ind_R() { return pnd_Hld_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Hld_Bank_Aba_Acct_Nmbr_R() { return pnd_Hld_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Hld_Bank_Aba_Acct_Nmbr_RRedef9() { return pnd_Hld_Bank_Aba_Acct_Nmbr_RRedef9; }

    public DbsField getPnd_Hld_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Hld_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Hld_Eft_Status_Ind_R() { return pnd_Hld_Eft_Status_Ind_R; }

    public DbsField getPnd_Hld_Checking_Saving_Cde_R() { return pnd_Hld_Checking_Saving_Cde_R; }

    public DbsField getPnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Hld_Pending_Addrss_Chnge_Dte_R() { return pnd_Hld_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Hld_Pending_Addrss_Restore_Dte_R() { return pnd_Hld_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Hld_Pending_Perm_Addrss_Chnge_Dte_R() { return pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Hld_Correspondence_Addrss_Ind_R() { return pnd_Hld_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Hld_Addrss_Geographic_Cde_R() { return pnd_Hld_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Hld_Pnd_V_Cntrct_Name_Add_K() { return pnd_Hld_Pnd_V_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10() { return pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10; }

    public DbsField getPnd_Hld_V_Cntrct_Name_Free_K() { return pnd_Hld_V_Cntrct_Name_Free_K; }

    public DbsField getPnd_Hld_V_Addrss_Lne_1_K() { return pnd_Hld_V_Addrss_Lne_1_K; }

    public DbsField getPnd_Hld_V_Addrss_Lne_2_K() { return pnd_Hld_V_Addrss_Lne_2_K; }

    public DbsField getPnd_Hld_V_Addrss_Lne_3_K() { return pnd_Hld_V_Addrss_Lne_3_K; }

    public DbsField getPnd_Hld_V_Addrss_Lne_4_K() { return pnd_Hld_V_Addrss_Lne_4_K; }

    public DbsField getPnd_Hld_V_Addrss_Lne_5_K() { return pnd_Hld_V_Addrss_Lne_5_K; }

    public DbsField getPnd_Hld_V_Addrss_Lne_6_K() { return pnd_Hld_V_Addrss_Lne_6_K; }

    public DbsField getPnd_Hld_V_Addrss_Postal_Data_K() { return pnd_Hld_V_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Hld_V_Addrss_Postal_Data_KRedef11() { return pnd_Hld_V_Addrss_Postal_Data_KRedef11; }

    public DbsField getPnd_Hld_V_Addrss_Zip_Plus_4_K() { return pnd_Hld_V_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Hld_V_Addrss_Carrier_Rte_K() { return pnd_Hld_V_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Hld_V_Addrss_Walk_Rte_K() { return pnd_Hld_V_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Hld_V_Addrss_Usps_Future_Use_K() { return pnd_Hld_V_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Hld_Pnd_V_Rest_Of_Record_K() { return pnd_Hld_Pnd_V_Rest_Of_Record_K; }

    public DbsGroup getPnd_Hld_Pnd_V_Rest_Of_Record_KRedef12() { return pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12; }

    public DbsField getPnd_Hld_V_Addrss_Type_Cde_K() { return pnd_Hld_V_Addrss_Type_Cde_K; }

    public DbsField getPnd_Hld_V_Addrss_Last_Chnge_Dte_K() { return pnd_Hld_V_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Hld_V_Addrss_Last_Chnge_Time_K() { return pnd_Hld_V_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Hld_V_Permanent_Addrss_Ind_K() { return pnd_Hld_V_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Hld_V_Bank_Aba_Acct_Nmbr_K() { return pnd_Hld_V_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Hld_V_Bank_Aba_Acct_Nmbr_KRedef13() { return pnd_Hld_V_Bank_Aba_Acct_Nmbr_KRedef13; }

    public DbsField getPnd_Hld_V_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Hld_V_Eft_Status_Ind_K() { return pnd_Hld_V_Eft_Status_Ind_K; }

    public DbsField getPnd_Hld_V_Checking_Saving_Cde_K() { return pnd_Hld_V_Checking_Saving_Cde_K; }

    public DbsField getPnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Hld_V_Pnding_Addrss_Chnge_Dte_K() { return pnd_Hld_V_Pnding_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Hld_V_Pnding_Addrss_Restore_Dte_K() { return pnd_Hld_V_Pnding_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Hld_V_Pnding_Perm_Addrss_Chnge_Dte_K() { return pnd_Hld_V_Pnding_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Hld_V_Check_Mailing_Addrss_Ind_K() { return pnd_Hld_V_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Hld_V_Addrss_Geographic_Cde_K() { return pnd_Hld_V_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Hld_V_Intl_Eft_Pay_Type_Cde() { return pnd_Hld_V_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Hld_V_Iintl_Bank_Pymnt_Eft_Nmbr() { return pnd_Hld_V_Iintl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Hld_V_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Hld_V_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Hld_Pnd_V_Cntrct_Name_Add_R() { return pnd_Hld_Pnd_V_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14() { return pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14; }

    public DbsField getPnd_Hld_V_Cntrct_Name_Free_R() { return pnd_Hld_V_Cntrct_Name_Free_R; }

    public DbsField getPnd_Hld_V_Addrss_Lne_1_R() { return pnd_Hld_V_Addrss_Lne_1_R; }

    public DbsField getPnd_Hld_V_Addrss_Lne_2_R() { return pnd_Hld_V_Addrss_Lne_2_R; }

    public DbsField getPnd_Hld_V_Addrss_Lne_3_R() { return pnd_Hld_V_Addrss_Lne_3_R; }

    public DbsField getPnd_Hld_V_Addrss_Lne_4_R() { return pnd_Hld_V_Addrss_Lne_4_R; }

    public DbsField getPnd_Hld_V_Addrss_Lne_5_R() { return pnd_Hld_V_Addrss_Lne_5_R; }

    public DbsField getPnd_Hld_V_Addrss_Lne_6_R() { return pnd_Hld_V_Addrss_Lne_6_R; }

    public DbsField getPnd_Hld_V_Addrss_Postal_Data_R() { return pnd_Hld_V_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Hld_V_Addrss_Postal_Data_RRedef15() { return pnd_Hld_V_Addrss_Postal_Data_RRedef15; }

    public DbsField getPnd_Hld_V_Addrss_Zip_Plus_4_R() { return pnd_Hld_V_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Hld_V_Addrss_Carrier_Rte_R() { return pnd_Hld_V_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Hld_V_Addrss_Walk_Rte_R() { return pnd_Hld_V_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Hld_V_Addrss_Usps_Future_Use_R() { return pnd_Hld_V_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Hld_Pnd_V_Rest_Of_Record_R() { return pnd_Hld_Pnd_V_Rest_Of_Record_R; }

    public DbsGroup getPnd_Hld_Pnd_V_Rest_Of_Record_RRedef16() { return pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16; }

    public DbsField getPnd_Hld_V_Addrss_Type_Cde_R() { return pnd_Hld_V_Addrss_Type_Cde_R; }

    public DbsField getPnd_Hld_V_Addrss_Last_Chnge_Dte_R() { return pnd_Hld_V_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Hld_V_Addrss_Last_Chnge_Time_R() { return pnd_Hld_V_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Hld_V_Permanent_Addrss_Ind_R() { return pnd_Hld_V_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Hld_V_Bank_Aba_Acct_Nmbr_R() { return pnd_Hld_V_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Hld_V_Bank_Aba_Acct_Nmbr_RRedef17() { return pnd_Hld_V_Bank_Aba_Acct_Nmbr_RRedef17; }

    public DbsField getPnd_Hld_V_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Hld_V_Eft_Status_Ind_R() { return pnd_Hld_V_Eft_Status_Ind_R; }

    public DbsField getPnd_Hld_V_Checking_Saving_Cde_R() { return pnd_Hld_V_Checking_Saving_Cde_R; }

    public DbsField getPnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Hld_V_Pending_Addrss_Chnge_Dte_R() { return pnd_Hld_V_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Hld_V_Pending_Addrss_Restore_Dte_R() { return pnd_Hld_V_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Hld_V_Pending_Perm_Add_Chnge_Dte_R() { return pnd_Hld_V_Pending_Perm_Add_Chnge_Dte_R; }

    public DbsField getPnd_Hld_V_Correspondence_Addrss_Ind_R() { return pnd_Hld_V_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Hld_V_Addrss_Geographic_Cde_R() { return pnd_Hld_V_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Hld_Global_Indicator() { return pnd_Hld_Global_Indicator; }

    public DbsField getPnd_Hld_Global_Country() { return pnd_Hld_Global_Country; }

    public DbsField getPnd_Hld_Legal_State() { return pnd_Hld_Legal_State; }

    public DbsField getPnd_Core() { return pnd_Core; }

    public DbsGroup getPnd_CoreRedef18() { return pnd_CoreRedef18; }

    public DbsField getPnd_Core_Cntrct_Payee() { return pnd_Core_Cntrct_Payee; }

    public DbsGroup getPnd_Core_Cntrct_PayeeRedef19() { return pnd_Core_Cntrct_PayeeRedef19; }

    public DbsField getPnd_Core_Cntrct_Nbr() { return pnd_Core_Cntrct_Nbr; }

    public DbsField getPnd_Core_Cntrct_Payee_Cde() { return pnd_Core_Cntrct_Payee_Cde; }

    public DbsField getPnd_Core_Ph_Unique_Id_Nbr() { return pnd_Core_Ph_Unique_Id_Nbr; }

    public DbsField getPnd_Core_Ph_Rcd_Type_Cde() { return pnd_Core_Ph_Rcd_Type_Cde; }

    public DbsField getPnd_Core_Ph_Social_Security_No() { return pnd_Core_Ph_Social_Security_No; }

    public DbsField getPnd_Core_Ph_Dob_Dte() { return pnd_Core_Ph_Dob_Dte; }

    public DbsField getPnd_Core_Ph_Dod_Dte() { return pnd_Core_Ph_Dod_Dte; }

    public DbsGroup getPnd_Core_Ph_Nme() { return pnd_Core_Ph_Nme; }

    public DbsField getPnd_Core_Ph_Last_Nme() { return pnd_Core_Ph_Last_Nme; }

    public DbsGroup getPnd_Core_Ph_Last_NmeRedef20() { return pnd_Core_Ph_Last_NmeRedef20; }

    public DbsField getPnd_Core_Ph_Last_Nme_16() { return pnd_Core_Ph_Last_Nme_16; }

    public DbsField getPnd_Core_Ph_First_Nme() { return pnd_Core_Ph_First_Nme; }

    public DbsGroup getPnd_Core_Ph_First_NmeRedef21() { return pnd_Core_Ph_First_NmeRedef21; }

    public DbsField getPnd_Core_Ph_First_Nme_10() { return pnd_Core_Ph_First_Nme_10; }

    public DbsField getPnd_Core_Ph_Mddle_Nme() { return pnd_Core_Ph_Mddle_Nme; }

    public DbsGroup getPnd_Core_Ph_Mddle_NmeRedef22() { return pnd_Core_Ph_Mddle_NmeRedef22; }

    public DbsField getPnd_Core_Ph_Mddle_Nme_12() { return pnd_Core_Ph_Mddle_Nme_12; }

    public DbsField getPnd_Core_Cntrct_Status_Cde() { return pnd_Core_Cntrct_Status_Cde; }

    public DbsGroup getPnd_Input() { return pnd_Input; }

    public DbsField getPnd_Input_Pnd_Cntrct_Payee() { return pnd_Input_Pnd_Cntrct_Payee; }

    public DbsGroup getPnd_Input_Pnd_Cntrct_PayeeRedef23() { return pnd_Input_Pnd_Cntrct_PayeeRedef23; }

    public DbsField getPnd_Input_Pnd_Ppcn_Nbr() { return pnd_Input_Pnd_Ppcn_Nbr; }

    public DbsField getPnd_Input_Pnd_Payee_Cde() { return pnd_Input_Pnd_Payee_Cde; }

    public DbsGroup getPnd_Input_Pnd_Payee_CdeRedef24() { return pnd_Input_Pnd_Payee_CdeRedef24; }

    public DbsField getPnd_Input_Pnd_Payee_Cde_A() { return pnd_Input_Pnd_Payee_Cde_A; }

    public DbsField getPnd_Input_Pnd_Record_Code() { return pnd_Input_Pnd_Record_Code; }

    public DbsField getPnd_Input_Pnd_Rest_Of_Record_353() { return pnd_Input_Pnd_Rest_Of_Record_353; }

    public DbsGroup getPnd_Input_Pnd_Rest_Of_Record_353Redef25() { return pnd_Input_Pnd_Rest_Of_Record_353Redef25; }

    public DbsField getPnd_Input_Pnd_Header_Chk_Dte() { return pnd_Input_Pnd_Header_Chk_Dte; }

    public DbsGroup getPnd_Input_Pnd_Rest_Of_Record_353Redef26() { return pnd_Input_Pnd_Rest_Of_Record_353Redef26; }

    public DbsField getPnd_Input_Pnd_Optn_Cde() { return pnd_Input_Pnd_Optn_Cde; }

    public DbsField getPnd_Input_Pnd_Orgn_Cde() { return pnd_Input_Pnd_Orgn_Cde; }

    public DbsField getPnd_Input_Pnd_F1() { return pnd_Input_Pnd_F1; }

    public DbsField getPnd_Input_Pnd_Issue_Dte() { return pnd_Input_Pnd_Issue_Dte; }

    public DbsGroup getPnd_Input_Pnd_Issue_DteRedef27() { return pnd_Input_Pnd_Issue_DteRedef27; }

    public DbsField getPnd_Input_Pnd_Issue_Dte_A() { return pnd_Input_Pnd_Issue_Dte_A; }

    public DbsField getPnd_Input_Pnd_1st_Due_Dte() { return pnd_Input_Pnd_1st_Due_Dte; }

    public DbsField getPnd_Input_Pnd_1st_Pd_Dte() { return pnd_Input_Pnd_1st_Pd_Dte; }

    public DbsField getPnd_Input_Pnd_Crrncy_Cde() { return pnd_Input_Pnd_Crrncy_Cde; }

    public DbsGroup getPnd_Input_Pnd_Crrncy_CdeRedef28() { return pnd_Input_Pnd_Crrncy_CdeRedef28; }

    public DbsField getPnd_Input_Pnd_Crrncy_Cde_A() { return pnd_Input_Pnd_Crrncy_Cde_A; }

    public DbsField getPnd_Input_Pnd_Type_Cde() { return pnd_Input_Pnd_Type_Cde; }

    public DbsField getPnd_Input_Pnd_F2() { return pnd_Input_Pnd_F2; }

    public DbsField getPnd_Input_Pnd_Pnsn_Pln_Cde() { return pnd_Input_Pnd_Pnsn_Pln_Cde; }

    public DbsField getPnd_Input_Pnd_F3() { return pnd_Input_Pnd_F3; }

    public DbsField getPnd_Input_Pnd_First_Ann_Dob() { return pnd_Input_Pnd_First_Ann_Dob; }

    public DbsField getPnd_Input_Pnd_F4() { return pnd_Input_Pnd_F4; }

    public DbsField getPnd_Input_Pnd_First_Ann_Dod() { return pnd_Input_Pnd_First_Ann_Dod; }

    public DbsField getPnd_Input_Pnd_F5() { return pnd_Input_Pnd_F5; }

    public DbsField getPnd_Input_Pnd_Scnd_Ann_Dob() { return pnd_Input_Pnd_Scnd_Ann_Dob; }

    public DbsField getPnd_Input_Pnd_F6() { return pnd_Input_Pnd_F6; }

    public DbsField getPnd_Input_Pnd_Scnd_Ann_Dod() { return pnd_Input_Pnd_Scnd_Ann_Dod; }

    public DbsField getPnd_Input_Pnd_F7() { return pnd_Input_Pnd_F7; }

    public DbsField getPnd_Input_Pnd_Div_Coll_Cde() { return pnd_Input_Pnd_Div_Coll_Cde; }

    public DbsField getPnd_Input_Pnd_Ppg_Cde() { return pnd_Input_Pnd_Ppg_Cde; }

    public DbsField getPnd_Input_Pnd_Lst_Trans_Dte() { return pnd_Input_Pnd_Lst_Trans_Dte; }

    public DbsField getPnd_Input_Pnd_Cntrct_Type() { return pnd_Input_Pnd_Cntrct_Type; }

    public DbsField getPnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re() { return pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsField getPnd_Input_Pnd_Cntrct_Fnl_Prm_Dte() { return pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte; }

    public DbsField getPnd_Input_Pnd_Cntrct_Mtch_Ppcn() { return pnd_Input_Pnd_Cntrct_Mtch_Ppcn; }

    public DbsField getPnd_Input_Pnd_Cntrct_Annty_Strt_Dte() { return pnd_Input_Pnd_Cntrct_Annty_Strt_Dte; }

    public DbsField getPnd_Input_Pnd_Cntrct_Issue_Dte_Dd() { return pnd_Input_Pnd_Cntrct_Issue_Dte_Dd; }

    public DbsField getPnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd() { return pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getPnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd() { return pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getPnd_Input_Pnd_Roth_Frst_Cntrb_Dte() { return pnd_Input_Pnd_Roth_Frst_Cntrb_Dte; }

    public DbsField getPnd_Input_Pnd_Roth_Ssnng_Dte() { return pnd_Input_Pnd_Roth_Ssnng_Dte; }

    public DbsField getPnd_Input_Pnd_Cntrct_Ssnng_Dte() { return pnd_Input_Pnd_Cntrct_Ssnng_Dte; }

    public DbsField getPnd_Input_Pnd_Plan_Nmbr() { return pnd_Input_Pnd_Plan_Nmbr; }

    public DbsField getPnd_Input_Pnd_Tax_Exmpt_Ind() { return pnd_Input_Pnd_Tax_Exmpt_Ind; }

    public DbsField getPnd_Input_Pnd_Orig_Ownr_Dob() { return pnd_Input_Pnd_Orig_Ownr_Dob; }

    public DbsField getPnd_Input_Pnd_Orig_Ownr_Dod() { return pnd_Input_Pnd_Orig_Ownr_Dod; }

    public DbsField getPnd_Input_Pnd_Sub_Plan_Nmbr() { return pnd_Input_Pnd_Sub_Plan_Nmbr; }

    public DbsField getPnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr() { return pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getPnd_Input_Pnd_Orgntng_Cntrct_Nmbr() { return pnd_Input_Pnd_Orgntng_Cntrct_Nmbr; }

    public DbsGroup getPnd_Input_Pnd_Rest_Of_Record_353Redef29() { return pnd_Input_Pnd_Rest_Of_Record_353Redef29; }

    public DbsField getPnd_Input_Pnd_F9() { return pnd_Input_Pnd_F9; }

    public DbsField getPnd_Input_Pnd_Ddctn_Cde() { return pnd_Input_Pnd_Ddctn_Cde; }

    public DbsGroup getPnd_Input_Pnd_Ddctn_CdeRedef30() { return pnd_Input_Pnd_Ddctn_CdeRedef30; }

    public DbsField getPnd_Input_Pnd_Ddctn_Cde_N() { return pnd_Input_Pnd_Ddctn_Cde_N; }

    public DbsField getPnd_Input_Pnd_Ddctn_Seq_Nbr() { return pnd_Input_Pnd_Ddctn_Seq_Nbr; }

    public DbsGroup getPnd_Input_Pnd_Ddctn_Seq_NbrRedef31() { return pnd_Input_Pnd_Ddctn_Seq_NbrRedef31; }

    public DbsField getPnd_Input_Pnd_Ddctn_Seq_Nbr_A() { return pnd_Input_Pnd_Ddctn_Seq_Nbr_A; }

    public DbsField getPnd_Input_Pnd_Ddctn_Payee() { return pnd_Input_Pnd_Ddctn_Payee; }

    public DbsField getPnd_Input_Pnd_Ddctn_Per_Amt() { return pnd_Input_Pnd_Ddctn_Per_Amt; }

    public DbsField getPnd_Input_Pnd_F10() { return pnd_Input_Pnd_F10; }

    public DbsField getPnd_Input_Pnd_Ddctn_Stp_Dte() { return pnd_Input_Pnd_Ddctn_Stp_Dte; }

    public DbsGroup getPnd_Input_Pnd_Rest_Of_Record_353Redef32() { return pnd_Input_Pnd_Rest_Of_Record_353Redef32; }

    public DbsField getPnd_Input_Pnd_Summ_Cmpny_Cde() { return pnd_Input_Pnd_Summ_Cmpny_Cde; }

    public DbsField getPnd_Input_Pnd_Summ_Fund_Cde() { return pnd_Input_Pnd_Summ_Fund_Cde; }

    public DbsField getPnd_Input_Pnd_Summ_Per_Ivc_Amt() { return pnd_Input_Pnd_Summ_Per_Ivc_Amt; }

    public DbsField getPnd_Input_Pnd_F11() { return pnd_Input_Pnd_F11; }

    public DbsField getPnd_Input_Pnd_Summ_Per_Pymnt() { return pnd_Input_Pnd_Summ_Per_Pymnt; }

    public DbsField getPnd_Input_Pnd_Summ_Per_Dvdnd() { return pnd_Input_Pnd_Summ_Per_Dvdnd; }

    public DbsGroup getPnd_Input_Pnd_Summ_Per_DvdndRedef33() { return pnd_Input_Pnd_Summ_Per_DvdndRedef33; }

    public DbsField getPnd_Input_Pnd_Summ_Per_Dvdnd_R() { return pnd_Input_Pnd_Summ_Per_Dvdnd_R; }

    public DbsField getPnd_Input_Pnd_F12() { return pnd_Input_Pnd_F12; }

    public DbsField getPnd_Input_Pnd_Summ_Units() { return pnd_Input_Pnd_Summ_Units; }

    public DbsField getPnd_Input_Pnd_Summ_Fin_Pymnt() { return pnd_Input_Pnd_Summ_Fin_Pymnt; }

    public DbsField getPnd_Input_Pnd_Summ_Fin_Dvdnd() { return pnd_Input_Pnd_Summ_Fin_Dvdnd; }

    public DbsGroup getPnd_Input_Pnd_Rest_Of_Record_353Redef34() { return pnd_Input_Pnd_Rest_Of_Record_353Redef34; }

    public DbsField getPnd_Input_Pnd_Cpr_Id_Nbr() { return pnd_Input_Pnd_Cpr_Id_Nbr; }

    public DbsField getPnd_Input_Pnd_F13() { return pnd_Input_Pnd_F13; }

    public DbsField getPnd_Input_Pnd_Ctznshp_Cde() { return pnd_Input_Pnd_Ctznshp_Cde; }

    public DbsField getPnd_Input_Pnd_Rsdncy_Cde() { return pnd_Input_Pnd_Rsdncy_Cde; }

    public DbsField getPnd_Input_Pnd_F14() { return pnd_Input_Pnd_F14; }

    public DbsField getPnd_Input_Pnd_Tax_Id_Nbr() { return pnd_Input_Pnd_Tax_Id_Nbr; }

    public DbsField getPnd_Input_Pnd_F15() { return pnd_Input_Pnd_F15; }

    public DbsField getPnd_Input_Pnd_Status_Cde() { return pnd_Input_Pnd_Status_Cde; }

    public DbsField getPnd_Input_Pnd_F16() { return pnd_Input_Pnd_F16; }

    public DbsField getPnd_Input_Pnd_Cash_Cde() { return pnd_Input_Pnd_Cash_Cde; }

    public DbsField getPnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde() { return pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde; }

    public DbsField getPnd_Input_Pnd_F17() { return pnd_Input_Pnd_F17; }

    public DbsField getPnd_Input_Pnd_Rcvry_Type_Ind() { return pnd_Input_Pnd_Rcvry_Type_Ind; }

    public DbsField getPnd_Input_Pnd_Cntrct_Per_Ivc_Amt() { return pnd_Input_Pnd_Cntrct_Per_Ivc_Amt; }

    public DbsField getPnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt() { return pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getPnd_Input_Pnd_Cntrct_Ivc_Amt() { return pnd_Input_Pnd_Cntrct_Ivc_Amt; }

    public DbsField getPnd_Input_Pnd_Cntrct_Ivc_Used_Amt() { return pnd_Input_Pnd_Cntrct_Ivc_Used_Amt; }

    public DbsField getPnd_Input_Pnd_F18() { return pnd_Input_Pnd_F18; }

    public DbsField getPnd_Input_Pnd_Mode_Ind() { return pnd_Input_Pnd_Mode_Ind; }

    public DbsField getPnd_Input_Pnd_F19() { return pnd_Input_Pnd_F19; }

    public DbsField getPnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte() { return pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte; }

    public DbsField getPnd_Input_Pnd_F20() { return pnd_Input_Pnd_F20; }

    public DbsField getPnd_Input_Pnd_Pend_Cde() { return pnd_Input_Pnd_Pend_Cde; }

    public DbsField getPnd_Input_Pnd_Hold_Cde() { return pnd_Input_Pnd_Hold_Cde; }

    public DbsField getPnd_Input_Pnd_Pend_Dte() { return pnd_Input_Pnd_Pend_Dte; }

    public DbsGroup getPnd_Input_Pnd_Pend_DteRedef35() { return pnd_Input_Pnd_Pend_DteRedef35; }

    public DbsField getPnd_Input_Pnd_Pend_Dte_A() { return pnd_Input_Pnd_Pend_Dte_A; }

    public DbsField getPnd_Input_Pnd_F21() { return pnd_Input_Pnd_F21; }

    public DbsField getPnd_Input_Pnd_Curr_Dist_Cde() { return pnd_Input_Pnd_Curr_Dist_Cde; }

    public DbsField getPnd_Input_Pnd_Cmbne_Cde() { return pnd_Input_Pnd_Cmbne_Cde; }

    public DbsField getPnd_Input_Pnd_F22() { return pnd_Input_Pnd_F22; }

    public DbsField getPnd_Input_Pnd_Cntrct_Local_Cde() { return pnd_Input_Pnd_Cntrct_Local_Cde; }

    public DbsField getPnd_Input_Pnd_F23() { return pnd_Input_Pnd_F23; }

    public DbsField getPnd_Input_Pnd_Rllvr_Cntrct_Nbr() { return pnd_Input_Pnd_Rllvr_Cntrct_Nbr; }

    public DbsField getPnd_Input_Pnd_Rllvr_Ivc_Ind() { return pnd_Input_Pnd_Rllvr_Ivc_Ind; }

    public DbsField getPnd_Input_Pnd_Rllvr_Elgble_Ind() { return pnd_Input_Pnd_Rllvr_Elgble_Ind; }

    public DbsField getPnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde() { return pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getPnd_Input_Pnd_Rllvr_Accptng_Irc_Cde() { return pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde; }

    public DbsField getPnd_Input_Pnd_Rllvr_Pln_Admn_Ind() { return pnd_Input_Pnd_Rllvr_Pln_Admn_Ind; }

    public DbsField getPnd_Input_Pnd_F24() { return pnd_Input_Pnd_F24; }

    public DbsField getPnd_Input_Pnd_Roth_Dsblty_Dte() { return pnd_Input_Pnd_Roth_Dsblty_Dte; }

    public DbsGroup getGtn_Pda_E() { return gtn_Pda_E; }

    public DbsField getGtn_Pda_E_Pymnt_Reqst_Log_Dte_Time() { return gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getGtn_Pda_E_Cntrct_Unq_Id_Nbr() { return gtn_Pda_E_Cntrct_Unq_Id_Nbr; }

    public DbsField getGtn_Pda_E_Cntrct_Ppcn_Nbr() { return gtn_Pda_E_Cntrct_Ppcn_Nbr; }

    public DbsField getGtn_Pda_E_Cntrct_Cmbn_Nbr() { return gtn_Pda_E_Cntrct_Cmbn_Nbr; }

    public DbsGroup getGtn_Pda_E_Cntrct_Cmbn_NbrRedef36() { return gtn_Pda_E_Cntrct_Cmbn_NbrRedef36; }

    public DbsField getGtn_Pda_E_Cntrct_Cmbn_Nbr_A10() { return gtn_Pda_E_Cntrct_Cmbn_Nbr_A10; }

    public DbsField getGtn_Pda_E_Cntrct_Cmbn_Nbr_Payee() { return gtn_Pda_E_Cntrct_Cmbn_Nbr_Payee; }

    public DbsField getGtn_Pda_E_Annt_Soc_Sec_Ind() { return gtn_Pda_E_Annt_Soc_Sec_Ind; }

    public DbsField getGtn_Pda_E_Annt_Soc_Sec_Nbr() { return gtn_Pda_E_Annt_Soc_Sec_Nbr; }

    public DbsField getGtn_Pda_E_Annt_Ctznshp_Cde() { return gtn_Pda_E_Annt_Ctznshp_Cde; }

    public DbsField getGtn_Pda_E_Annt_Rsdncy_Cde() { return gtn_Pda_E_Annt_Rsdncy_Cde; }

    public DbsField getGtn_Pda_E_Annt_Locality_Cde() { return gtn_Pda_E_Annt_Locality_Cde; }

    public DbsGroup getGtn_Pda_E_Ph_Name() { return gtn_Pda_E_Ph_Name; }

    public DbsField getGtn_Pda_E_Ph_Last_Name() { return gtn_Pda_E_Ph_Last_Name; }

    public DbsField getGtn_Pda_E_Ph_Middle_Name() { return gtn_Pda_E_Ph_Middle_Name; }

    public DbsField getGtn_Pda_E_Ph_First_Name() { return gtn_Pda_E_Ph_First_Name; }

    public DbsField getGtn_Pda_E_Pymnt_Dob() { return gtn_Pda_E_Pymnt_Dob; }

    public DbsGroup getGtn_Pda_E_Pymnt_Nme_And_Addr_Grp() { return gtn_Pda_E_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getGtn_Pda_E_Pymnt_Nme() { return gtn_Pda_E_Pymnt_Nme; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Lines() { return gtn_Pda_E_Pymnt_Addr_Lines; }

    public DbsGroup getGtn_Pda_E_Pymnt_Addr_LinesRedef37() { return gtn_Pda_E_Pymnt_Addr_LinesRedef37; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Line_Txt() { return gtn_Pda_E_Pymnt_Addr_Line_Txt; }

    public DbsGroup getGtn_Pda_E_Pymnt_Addr_LinesRedef38() { return gtn_Pda_E_Pymnt_Addr_LinesRedef38; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Line1_Txt() { return gtn_Pda_E_Pymnt_Addr_Line1_Txt; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Line2_Txt() { return gtn_Pda_E_Pymnt_Addr_Line2_Txt; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Line3_Txt() { return gtn_Pda_E_Pymnt_Addr_Line3_Txt; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Line4_Txt() { return gtn_Pda_E_Pymnt_Addr_Line4_Txt; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Line5_Txt() { return gtn_Pda_E_Pymnt_Addr_Line5_Txt; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Line6_Txt() { return gtn_Pda_E_Pymnt_Addr_Line6_Txt; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Zip_Cde() { return gtn_Pda_E_Pymnt_Addr_Zip_Cde; }

    public DbsField getGtn_Pda_E_Pymnt_Postl_Data() { return gtn_Pda_E_Pymnt_Postl_Data; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Type_Ind() { return gtn_Pda_E_Pymnt_Addr_Type_Ind; }

    public DbsField getGtn_Pda_E_Pymnt_Foreign_Cde() { return gtn_Pda_E_Pymnt_Foreign_Cde; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte() { return gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme() { return gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme; }

    public DbsField getGtn_Pda_E_Pymnt_Addr_Chg_Ind() { return gtn_Pda_E_Pymnt_Addr_Chg_Ind; }

    public DbsField getGtn_Pda_E_Pymnt_Settl_Ivc_Ind() { return gtn_Pda_E_Pymnt_Settl_Ivc_Ind; }

    public DbsGroup getGtn_Pda_E_Pnd_Pnd_Fund_Payee() { return gtn_Pda_E_Pnd_Pnd_Fund_Payee; }

    public DbsField getGtn_Pda_E_Inv_Acct_Cde() { return gtn_Pda_E_Inv_Acct_Cde; }

    public DbsField getGtn_Pda_E_Inv_Acct_Settl_Amt() { return gtn_Pda_E_Inv_Acct_Settl_Amt; }

    public DbsField getGtn_Pda_E_Inv_Acct_Cntrct_Amt() { return gtn_Pda_E_Inv_Acct_Cntrct_Amt; }

    public DbsField getGtn_Pda_E_Inv_Acct_Dvdnd_Amt() { return gtn_Pda_E_Inv_Acct_Dvdnd_Amt; }

    public DbsField getGtn_Pda_E_Inv_Acct_Unit_Value() { return gtn_Pda_E_Inv_Acct_Unit_Value; }

    public DbsField getGtn_Pda_E_Inv_Acct_Unit_Qty() { return gtn_Pda_E_Inv_Acct_Unit_Qty; }

    public DbsField getGtn_Pda_E_Inv_Acct_Ivc_Amt() { return gtn_Pda_E_Inv_Acct_Ivc_Amt; }

    public DbsField getGtn_Pda_E_Inv_Acct_Ivc_Ind() { return gtn_Pda_E_Inv_Acct_Ivc_Ind; }

    public DbsField getGtn_Pda_E_Inv_Acct_Valuat_Period() { return gtn_Pda_E_Inv_Acct_Valuat_Period; }

    public DbsGroup getGtn_Pda_E_Pnd_Pnd_Deductions() { return gtn_Pda_E_Pnd_Pnd_Deductions; }

    public DbsField getGtn_Pda_E_Pymnt_Ded_Cde() { return gtn_Pda_E_Pymnt_Ded_Cde; }

    public DbsField getGtn_Pda_E_Pymnt_Ded_Amt() { return gtn_Pda_E_Pymnt_Ded_Amt; }

    public DbsField getGtn_Pda_E_Pymnt_Ded_Payee_Cde() { return gtn_Pda_E_Pymnt_Ded_Payee_Cde; }

    public DbsGroup getGtn_Pda_E_Pymnt_Ded_Payee_CdeRedef39() { return gtn_Pda_E_Pymnt_Ded_Payee_CdeRedef39; }

    public DbsField getGtn_Pda_E_Pymnt_Ded_Payee_Cde_5() { return gtn_Pda_E_Pymnt_Ded_Payee_Cde_5; }

    public DbsField getGtn_Pda_E_Pymnt_Ded_Payee_Cde_3() { return gtn_Pda_E_Pymnt_Ded_Payee_Cde_3; }

    public DbsField getGtn_Pda_E_Pymnt_Payee_Tax_Ind() { return gtn_Pda_E_Pymnt_Payee_Tax_Ind; }

    public DbsField getGtn_Pda_E_Pymnt_Settlmnt_Dte() { return gtn_Pda_E_Pymnt_Settlmnt_Dte; }

    public DbsField getGtn_Pda_E_Pymnt_Check_Dte() { return gtn_Pda_E_Pymnt_Check_Dte; }

    public DbsField getGtn_Pda_E_Pymnt_Cycle_Dte() { return gtn_Pda_E_Pymnt_Cycle_Dte; }

    public DbsField getGtn_Pda_E_Pymnt_Acctg_Dte() { return gtn_Pda_E_Pymnt_Acctg_Dte; }

    public DbsField getGtn_Pda_E_Pymnt_Ia_Issue_Dte() { return gtn_Pda_E_Pymnt_Ia_Issue_Dte; }

    public DbsField getGtn_Pda_E_Pymnt_Intrfce_Dte() { return gtn_Pda_E_Pymnt_Intrfce_Dte; }

    public DbsField getGtn_Pda_E_Cntrct_Check_Crrncy_Cde() { return gtn_Pda_E_Cntrct_Check_Crrncy_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Orgn_Cde() { return gtn_Pda_E_Cntrct_Orgn_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Type_Cde() { return gtn_Pda_E_Cntrct_Type_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Payee_Cde() { return gtn_Pda_E_Cntrct_Payee_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Option_Cde() { return gtn_Pda_E_Cntrct_Option_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Ac_Lt_10yrs() { return gtn_Pda_E_Cntrct_Ac_Lt_10yrs; }

    public DbsField getGtn_Pda_E_Cntrct_Mode_Cde() { return gtn_Pda_E_Cntrct_Mode_Cde; }

    public DbsField getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind() { return gtn_Pda_E_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getGtn_Pda_E_Pymnt_Spouse_Pay_Stats() { return gtn_Pda_E_Pymnt_Spouse_Pay_Stats; }

    public DbsField getGtn_Pda_E_Cntrct_Pymnt_Type_Ind() { return gtn_Pda_E_Cntrct_Pymnt_Type_Ind; }

    public DbsField getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind() { return gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getGtn_Pda_E_Cntrct_Pymnt_Dest_Cde() { return gtn_Pda_E_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Roll_Dest_Cde() { return gtn_Pda_E_Cntrct_Roll_Dest_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Dvdnd_Payee_Cde() { return gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getGtn_Pda_E_Pymnt_Eft_Acct_Nbr() { return gtn_Pda_E_Pymnt_Eft_Acct_Nbr; }

    public DbsField getGtn_Pda_E_Pymnt_Eft_Transit_Id_A() { return gtn_Pda_E_Pymnt_Eft_Transit_Id_A; }

    public DbsGroup getGtn_Pda_E_Pymnt_Eft_Transit_Id_ARedef40() { return gtn_Pda_E_Pymnt_Eft_Transit_Id_ARedef40; }

    public DbsField getGtn_Pda_E_Pymnt_Eft_Transit_Id() { return gtn_Pda_E_Pymnt_Eft_Transit_Id; }

    public DbsField getGtn_Pda_E_Pymnt_Chk_Sav_Ind() { return gtn_Pda_E_Pymnt_Chk_Sav_Ind; }

    public DbsField getGtn_Pda_E_Cntrct_Hold_Cde() { return gtn_Pda_E_Cntrct_Hold_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Hold_Ind() { return gtn_Pda_E_Cntrct_Hold_Ind; }

    public DbsField getGtn_Pda_E_Cntrct_Hold_Grp() { return gtn_Pda_E_Cntrct_Hold_Grp; }

    public DbsField getGtn_Pda_E_Cntrct_Hold_User_Id() { return gtn_Pda_E_Cntrct_Hold_User_Id; }

    public DbsField getGtn_Pda_E_Pymnt_Tax_Exempt_Ind() { return gtn_Pda_E_Pymnt_Tax_Exempt_Ind; }

    public DbsField getGtn_Pda_E_Cntrct_Qlfied_Cde() { return gtn_Pda_E_Cntrct_Qlfied_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Lob_Cde() { return gtn_Pda_E_Cntrct_Lob_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Sub_Lob_Cde() { return gtn_Pda_E_Cntrct_Sub_Lob_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Ia_Lob_Cde() { return gtn_Pda_E_Cntrct_Ia_Lob_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Annty_Ins_Type() { return gtn_Pda_E_Cntrct_Annty_Ins_Type; }

    public DbsField getGtn_Pda_E_Cntrct_Annty_Type_Cde() { return gtn_Pda_E_Cntrct_Annty_Type_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Insurance_Option() { return gtn_Pda_E_Cntrct_Insurance_Option; }

    public DbsField getGtn_Pda_E_Cntrct_Life_Contingency() { return gtn_Pda_E_Cntrct_Life_Contingency; }

    public DbsField getGtn_Pda_E_Pymnt_Suspend_Cde() { return gtn_Pda_E_Pymnt_Suspend_Cde; }

    public DbsField getGtn_Pda_E_Pymnt_Suspend_Dte() { return gtn_Pda_E_Pymnt_Suspend_Dte; }

    public DbsField getGtn_Pda_E_Pnd_Pnd_This_Pymnt() { return gtn_Pda_E_Pnd_Pnd_This_Pymnt; }

    public DbsField getGtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts() { return gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts; }

    public DbsField getGtn_Pda_E_Gtn_Ret_Code() { return gtn_Pda_E_Gtn_Ret_Code; }

    public DbsGroup getGtn_Pda_E_Gtn_Ret_CodeRedef41() { return gtn_Pda_E_Gtn_Ret_CodeRedef41; }

    public DbsField getGtn_Pda_E_Global_Country() { return gtn_Pda_E_Global_Country; }

    public DbsField getGtn_Pda_E_Temp_Pend_Cde() { return gtn_Pda_E_Temp_Pend_Cde; }

    public DbsField getGtn_Pda_E_Error_Code_1() { return gtn_Pda_E_Error_Code_1; }

    public DbsField getGtn_Pda_E_Error_Code_2() { return gtn_Pda_E_Error_Code_2; }

    public DbsField getGtn_Pda_E_Current_Mode() { return gtn_Pda_E_Current_Mode; }

    public DbsField getGtn_Pda_E_Egtrra_Eligibility_Ind() { return gtn_Pda_E_Egtrra_Eligibility_Ind; }

    public DbsField getGtn_Pda_E_Originating_Irs_Cde() { return gtn_Pda_E_Originating_Irs_Cde; }

    public DbsGroup getGtn_Pda_E_Originating_Irs_CdeRedef42() { return gtn_Pda_E_Originating_Irs_CdeRedef42; }

    public DbsField getGtn_Pda_E_Distributing_Irc_Cde() { return gtn_Pda_E_Distributing_Irc_Cde; }

    public DbsField getGtn_Pda_E_Receiving_Irc_Cde() { return gtn_Pda_E_Receiving_Irc_Cde; }

    public DbsField getGtn_Pda_E_Cntrct_Money_Source() { return gtn_Pda_E_Cntrct_Money_Source; }

    public DbsField getGtn_Pda_E_Roth_Dob() { return gtn_Pda_E_Roth_Dob; }

    public DbsField getGtn_Pda_E_Roth_First_Contrib_Dte() { return gtn_Pda_E_Roth_First_Contrib_Dte; }

    public DbsField getGtn_Pda_E_Roth_Death_Dte() { return gtn_Pda_E_Roth_Death_Dte; }

    public DbsGroup getGtn_Pda_E_Roth_Death_DteRedef43() { return gtn_Pda_E_Roth_Death_DteRedef43; }

    public DbsField getGtn_Pda_E_Roth_Death_Dte_N6() { return gtn_Pda_E_Roth_Death_Dte_N6; }

    public DbsField getGtn_Pda_E_Roth_Death_Dte_Dd() { return gtn_Pda_E_Roth_Death_Dte_Dd; }

    public DbsField getGtn_Pda_E_Roth_Disability_Dte() { return gtn_Pda_E_Roth_Disability_Dte; }

    public DbsField getGtn_Pda_E_Roth_Money_Source() { return gtn_Pda_E_Roth_Money_Source; }

    public DbsField getGtn_Pda_E_Roth_Qual_Non_Qual_Distrib() { return gtn_Pda_E_Roth_Qual_Non_Qual_Distrib; }

    public DbsField getGtn_Pda_E_Ia_Orgn_Cde() { return gtn_Pda_E_Ia_Orgn_Cde; }

    public DbsField getGtn_Pda_E_Plan_Number() { return gtn_Pda_E_Plan_Number; }

    public DbsField getGtn_Pda_E_Sub_Plan() { return gtn_Pda_E_Sub_Plan; }

    public DbsField getGtn_Pda_E_Orig_Cntrct_Nbr() { return gtn_Pda_E_Orig_Cntrct_Nbr; }

    public DbsField getGtn_Pda_E_Orig_Sub_Plan() { return gtn_Pda_E_Orig_Sub_Plan; }

    public DbsGroup getPnd_Name_01() { return pnd_Name_01; }

    public DbsField getPnd_Name_01_Ph_Unque_Id_Nmbr() { return pnd_Name_01_Ph_Unque_Id_Nmbr; }

    public DbsField getPnd_Name_01_Cntrct_Payee() { return pnd_Name_01_Cntrct_Payee; }

    public DbsGroup getPnd_Name_01_Cntrct_PayeeRedef44() { return pnd_Name_01_Cntrct_PayeeRedef44; }

    public DbsField getPnd_Name_01_Cntrct_Nmbr() { return pnd_Name_01_Cntrct_Nmbr; }

    public DbsField getPnd_Name_01_Cntrct_Payee_Cde() { return pnd_Name_01_Cntrct_Payee_Cde; }

    public DbsField getPnd_Name_01_Pnd_Cntrct_Name_Add_K() { return pnd_Name_01_Pnd_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45() { return pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45; }

    public DbsField getPnd_Name_01_Cntrct_Name_Free_K() { return pnd_Name_01_Cntrct_Name_Free_K; }

    public DbsField getPnd_Name_01_Addrss_Lne_1_K() { return pnd_Name_01_Addrss_Lne_1_K; }

    public DbsField getPnd_Name_01_Addrss_Lne_2_K() { return pnd_Name_01_Addrss_Lne_2_K; }

    public DbsField getPnd_Name_01_Addrss_Lne_3_K() { return pnd_Name_01_Addrss_Lne_3_K; }

    public DbsField getPnd_Name_01_Addrss_Lne_4_K() { return pnd_Name_01_Addrss_Lne_4_K; }

    public DbsField getPnd_Name_01_Addrss_Lne_5_K() { return pnd_Name_01_Addrss_Lne_5_K; }

    public DbsField getPnd_Name_01_Addrss_Lne_6_K() { return pnd_Name_01_Addrss_Lne_6_K; }

    public DbsField getPnd_Name_01_Addrss_Postal_Data_K() { return pnd_Name_01_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Name_01_Addrss_Postal_Data_KRedef46() { return pnd_Name_01_Addrss_Postal_Data_KRedef46; }

    public DbsField getPnd_Name_01_Addrss_Zip_Plus_4_K() { return pnd_Name_01_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Name_01_Addrss_Carrier_Rte_K() { return pnd_Name_01_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Name_01_Addrss_Walk_Rte_K() { return pnd_Name_01_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Name_01_Addrss_Usps_Future_Use_K() { return pnd_Name_01_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Name_01_Pnd_Rest_Of_Record_K() { return pnd_Name_01_Pnd_Rest_Of_Record_K; }

    public DbsGroup getPnd_Name_01_Pnd_Rest_Of_Record_KRedef47() { return pnd_Name_01_Pnd_Rest_Of_Record_KRedef47; }

    public DbsField getPnd_Name_01_Addrss_Type_Cde_K() { return pnd_Name_01_Addrss_Type_Cde_K; }

    public DbsField getPnd_Name_01_Addrss_Last_Chnge_Dte_K() { return pnd_Name_01_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Name_01_Addrss_Last_Chnge_Time_K() { return pnd_Name_01_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Name_01_Permanent_Addrss_Ind_K() { return pnd_Name_01_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Name_01_Bank_Aba_Acct_Nmbr_K() { return pnd_Name_01_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Name_01_Bank_Aba_Acct_Nmbr_KRedef48() { return pnd_Name_01_Bank_Aba_Acct_Nmbr_KRedef48; }

    public DbsField getPnd_Name_01_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Name_01_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Name_01_Eft_Status_Ind_K() { return pnd_Name_01_Eft_Status_Ind_K; }

    public DbsField getPnd_Name_01_Checking_Saving_Cde_K() { return pnd_Name_01_Checking_Saving_Cde_K; }

    public DbsField getPnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Name_01_Pending_Addrss_Chnge_Dte_K() { return pnd_Name_01_Pending_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_01_Pending_Addrss_Restore_Dte_K() { return pnd_Name_01_Pending_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_K() { return pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_01_Check_Mailing_Addrss_Ind_K() { return pnd_Name_01_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Name_01_Addrss_Geographic_Cde_K() { return pnd_Name_01_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Name_01_Intl_Eft_Pay_Type_Cde() { return pnd_Name_01_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Name_01_Intl_Bank_Pymnt_Eft_Nmbr() { return pnd_Name_01_Intl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Name_01_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Name_01_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Name_01_Pnd_Cntrct_Name_Add_R() { return pnd_Name_01_Pnd_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49() { return pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49; }

    public DbsField getPnd_Name_01_Cntrct_Name_Free_R() { return pnd_Name_01_Cntrct_Name_Free_R; }

    public DbsField getPnd_Name_01_Addrss_Lne_1_R() { return pnd_Name_01_Addrss_Lne_1_R; }

    public DbsField getPnd_Name_01_Addrss_Lne_2_R() { return pnd_Name_01_Addrss_Lne_2_R; }

    public DbsField getPnd_Name_01_Addrss_Lne_3_R() { return pnd_Name_01_Addrss_Lne_3_R; }

    public DbsField getPnd_Name_01_Addrss_Lne_4_R() { return pnd_Name_01_Addrss_Lne_4_R; }

    public DbsField getPnd_Name_01_Addrss_Lne_5_R() { return pnd_Name_01_Addrss_Lne_5_R; }

    public DbsField getPnd_Name_01_Addrss_Lne_6_R() { return pnd_Name_01_Addrss_Lne_6_R; }

    public DbsField getPnd_Name_01_Addrss_Postal_Data_R() { return pnd_Name_01_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Name_01_Addrss_Postal_Data_RRedef50() { return pnd_Name_01_Addrss_Postal_Data_RRedef50; }

    public DbsField getPnd_Name_01_Addrss_Zip_Plus_4_R() { return pnd_Name_01_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Name_01_Addrss_Carrier_Rte_R() { return pnd_Name_01_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Name_01_Addrss_Walk_Rte_R() { return pnd_Name_01_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Name_01_Addrss_Usps_Future_Use_R() { return pnd_Name_01_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Name_01_Pnd_Rest_Of_Record_R() { return pnd_Name_01_Pnd_Rest_Of_Record_R; }

    public DbsGroup getPnd_Name_01_Pnd_Rest_Of_Record_RRedef51() { return pnd_Name_01_Pnd_Rest_Of_Record_RRedef51; }

    public DbsField getPnd_Name_01_Addrss_Type_Cde_R() { return pnd_Name_01_Addrss_Type_Cde_R; }

    public DbsField getPnd_Name_01_Addrss_Last_Chnge_Dte_R() { return pnd_Name_01_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Name_01_Addrss_Last_Chnge_Time_R() { return pnd_Name_01_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Name_01_Permanent_Addrss_Ind_R() { return pnd_Name_01_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Name_01_Bank_Aba_Acct_Nmbr_R() { return pnd_Name_01_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Name_01_Bank_Aba_Acct_Nmbr_RRedef52() { return pnd_Name_01_Bank_Aba_Acct_Nmbr_RRedef52; }

    public DbsField getPnd_Name_01_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Name_01_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Name_01_Eft_Status_Ind_R() { return pnd_Name_01_Eft_Status_Ind_R; }

    public DbsField getPnd_Name_01_Checking_Saving_Cde_R() { return pnd_Name_01_Checking_Saving_Cde_R; }

    public DbsField getPnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Name_01_Pending_Addrss_Chnge_Dte_R() { return pnd_Name_01_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_01_Pending_Addrss_Restore_Dte_R() { return pnd_Name_01_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_R() { return pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_01_Correspondence_Addrss_Ind_R() { return pnd_Name_01_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Name_01_Addrss_Geographic_Cde_R() { return pnd_Name_01_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Name_01_Pnd_V_Cntrct_Name_Add_K() { return pnd_Name_01_Pnd_V_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53() { return pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53; }

    public DbsField getPnd_Name_01_V_Cntrct_Name_Free_K() { return pnd_Name_01_V_Cntrct_Name_Free_K; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_1_K() { return pnd_Name_01_V_Addrss_Lne_1_K; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_2_K() { return pnd_Name_01_V_Addrss_Lne_2_K; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_3_K() { return pnd_Name_01_V_Addrss_Lne_3_K; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_4_K() { return pnd_Name_01_V_Addrss_Lne_4_K; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_5_K() { return pnd_Name_01_V_Addrss_Lne_5_K; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_6_K() { return pnd_Name_01_V_Addrss_Lne_6_K; }

    public DbsField getPnd_Name_01_V_Addrss_Postal_Data_K() { return pnd_Name_01_V_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Name_01_V_Addrss_Postal_Data_KRedef54() { return pnd_Name_01_V_Addrss_Postal_Data_KRedef54; }

    public DbsField getPnd_Name_01_V_Addrss_Zip_Plus_4_K() { return pnd_Name_01_V_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Name_01_V_Addrss_Carrier_Rte_K() { return pnd_Name_01_V_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Name_01_V_Addrss_Walk_Rte_K() { return pnd_Name_01_V_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Name_01_V_Addrss_Usps_Future_Use_K() { return pnd_Name_01_V_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Name_01_Pnd_V_Rest_Of_Record_K() { return pnd_Name_01_Pnd_V_Rest_Of_Record_K; }

    public DbsGroup getPnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55() { return pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55; }

    public DbsField getPnd_Name_01_V_Addrss_Type_Cde_K() { return pnd_Name_01_V_Addrss_Type_Cde_K; }

    public DbsField getPnd_Name_01_V_Addrss_Last_Chnge_Dte_K() { return pnd_Name_01_V_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Name_01_V_Addrss_Last_Chnge_Time_K() { return pnd_Name_01_V_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Name_01_V_Permanent_Addrss_Ind_K() { return pnd_Name_01_V_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Name_01_V_Bank_Aba_Acct_Nmbr_K() { return pnd_Name_01_V_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Name_01_V_Bank_Aba_Acct_Nmbr_KRedef56() { return pnd_Name_01_V_Bank_Aba_Acct_Nmbr_KRedef56; }

    public DbsField getPnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Name_01_V_Eft_Status_Ind_K() { return pnd_Name_01_V_Eft_Status_Ind_K; }

    public DbsField getPnd_Name_01_V_Checking_Saving_Cde_K() { return pnd_Name_01_V_Checking_Saving_Cde_K; }

    public DbsField getPnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Name_01_V_Pnding_Addrss_Chnge_Dte_K() { return pnd_Name_01_V_Pnding_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_01_V_Pnding_Addrss_Restore_Dte_K() { return pnd_Name_01_V_Pnding_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Name_01_V_Pnding_Perm_Addrss_Chnge_Dte_K() { return pnd_Name_01_V_Pnding_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_01_V_Check_Mailing_Addrss_Ind_K() { return pnd_Name_01_V_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Name_01_V_Addrss_Geographic_Cde_K() { return pnd_Name_01_V_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Name_01_V_Intl_Eft_Pay_Type_Cde() { return pnd_Name_01_V_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Name_01_V_Iintl_Bank_Pymnt_Eft_Nmbr() { return pnd_Name_01_V_Iintl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Name_01_V_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Name_01_V_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Name_01_Pnd_V_Cntrct_Name_Add_R() { return pnd_Name_01_Pnd_V_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57() { return pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57; }

    public DbsField getPnd_Name_01_V_Cntrct_Name_Free_R() { return pnd_Name_01_V_Cntrct_Name_Free_R; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_1_R() { return pnd_Name_01_V_Addrss_Lne_1_R; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_2_R() { return pnd_Name_01_V_Addrss_Lne_2_R; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_3_R() { return pnd_Name_01_V_Addrss_Lne_3_R; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_4_R() { return pnd_Name_01_V_Addrss_Lne_4_R; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_5_R() { return pnd_Name_01_V_Addrss_Lne_5_R; }

    public DbsField getPnd_Name_01_V_Addrss_Lne_6_R() { return pnd_Name_01_V_Addrss_Lne_6_R; }

    public DbsField getPnd_Name_01_V_Addrss_Postal_Data_R() { return pnd_Name_01_V_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Name_01_V_Addrss_Postal_Data_RRedef58() { return pnd_Name_01_V_Addrss_Postal_Data_RRedef58; }

    public DbsField getPnd_Name_01_V_Addrss_Zip_Plus_4_R() { return pnd_Name_01_V_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Name_01_V_Addrss_Carrier_Rte_R() { return pnd_Name_01_V_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Name_01_V_Addrss_Walk_Rte_R() { return pnd_Name_01_V_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Name_01_V_Addrss_Usps_Future_Use_R() { return pnd_Name_01_V_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Name_01_Pnd_V_Rest_Of_Record_R() { return pnd_Name_01_Pnd_V_Rest_Of_Record_R; }

    public DbsGroup getPnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59() { return pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59; }

    public DbsField getPnd_Name_01_V_Addrss_Type_Cde_R() { return pnd_Name_01_V_Addrss_Type_Cde_R; }

    public DbsField getPnd_Name_01_V_Addrss_Last_Chnge_Dte_R() { return pnd_Name_01_V_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Name_01_V_Addrss_Last_Chnge_Time_R() { return pnd_Name_01_V_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Name_01_V_Permanent_Addrss_Ind_R() { return pnd_Name_01_V_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Name_01_V_Bank_Aba_Acct_Nmbr_R() { return pnd_Name_01_V_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Name_01_V_Bank_Aba_Acct_Nmbr_RRedef60() { return pnd_Name_01_V_Bank_Aba_Acct_Nmbr_RRedef60; }

    public DbsField getPnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Name_01_V_Eft_Status_Ind_R() { return pnd_Name_01_V_Eft_Status_Ind_R; }

    public DbsField getPnd_Name_01_V_Checking_Saving_Cde_R() { return pnd_Name_01_V_Checking_Saving_Cde_R; }

    public DbsField getPnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Name_01_V_Pending_Addrss_Chnge_Dte_R() { return pnd_Name_01_V_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_01_V_Pending_Addrss_Restore_Dte_R() { return pnd_Name_01_V_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Name_01_V_Pending_Perm_Add_Chnge_Dte_R() { return pnd_Name_01_V_Pending_Perm_Add_Chnge_Dte_R; }

    public DbsField getPnd_Name_01_V_Correspondence_Addrss_Ind_R() { return pnd_Name_01_V_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Name_01_V_Addrss_Geographic_Cde_R() { return pnd_Name_01_V_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Name_01_Global_Indicator() { return pnd_Name_01_Global_Indicator; }

    public DbsField getPnd_Name_01_Global_Country() { return pnd_Name_01_Global_Country; }

    public DbsField getPnd_Name_01_Legal_State() { return pnd_Name_01_Legal_State; }

    public DbsGroup getPnd_Name_Save() { return pnd_Name_Save; }

    public DbsField getPnd_Name_Save_Ph_Unque_Id_Nmbr() { return pnd_Name_Save_Ph_Unque_Id_Nmbr; }

    public DbsField getPnd_Name_Save_Cntrct_Payee() { return pnd_Name_Save_Cntrct_Payee; }

    public DbsGroup getPnd_Name_Save_Cntrct_PayeeRedef61() { return pnd_Name_Save_Cntrct_PayeeRedef61; }

    public DbsField getPnd_Name_Save_Cntrct_Nmbr() { return pnd_Name_Save_Cntrct_Nmbr; }

    public DbsField getPnd_Name_Save_Cntrct_Payee_Cde() { return pnd_Name_Save_Cntrct_Payee_Cde; }

    public DbsField getPnd_Name_Save_Pnd_Cntrct_Name_Add_K() { return pnd_Name_Save_Pnd_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62() { return pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62; }

    public DbsField getPnd_Name_Save_Cntrct_Name_Free_K() { return pnd_Name_Save_Cntrct_Name_Free_K; }

    public DbsField getPnd_Name_Save_Addrss_Lne_1_K() { return pnd_Name_Save_Addrss_Lne_1_K; }

    public DbsField getPnd_Name_Save_Addrss_Lne_2_K() { return pnd_Name_Save_Addrss_Lne_2_K; }

    public DbsField getPnd_Name_Save_Addrss_Lne_3_K() { return pnd_Name_Save_Addrss_Lne_3_K; }

    public DbsField getPnd_Name_Save_Addrss_Lne_4_K() { return pnd_Name_Save_Addrss_Lne_4_K; }

    public DbsField getPnd_Name_Save_Addrss_Lne_5_K() { return pnd_Name_Save_Addrss_Lne_5_K; }

    public DbsField getPnd_Name_Save_Addrss_Lne_6_K() { return pnd_Name_Save_Addrss_Lne_6_K; }

    public DbsField getPnd_Name_Save_Addrss_Postal_Data_K() { return pnd_Name_Save_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Name_Save_Addrss_Postal_Data_KRedef63() { return pnd_Name_Save_Addrss_Postal_Data_KRedef63; }

    public DbsField getPnd_Name_Save_Addrss_Zip_Plus_4_K() { return pnd_Name_Save_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Name_Save_Addrss_Carrier_Rte_K() { return pnd_Name_Save_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Name_Save_Addrss_Walk_Rte_K() { return pnd_Name_Save_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Name_Save_Addrss_Usps_Future_Use_K() { return pnd_Name_Save_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Name_Save_Pnd_Rest_Of_Record_K() { return pnd_Name_Save_Pnd_Rest_Of_Record_K; }

    public DbsGroup getPnd_Name_Save_Pnd_Rest_Of_Record_KRedef64() { return pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64; }

    public DbsField getPnd_Name_Save_Addrss_Type_Cde_K() { return pnd_Name_Save_Addrss_Type_Cde_K; }

    public DbsField getPnd_Name_Save_Addrss_Last_Chnge_Dte_K() { return pnd_Name_Save_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Name_Save_Addrss_Last_Chnge_Time_K() { return pnd_Name_Save_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Name_Save_Permanent_Addrss_Ind_K() { return pnd_Name_Save_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Name_Save_Bank_Aba_Acct_Nmbr_K() { return pnd_Name_Save_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Name_Save_Bank_Aba_Acct_Nmbr_KRedef65() { return pnd_Name_Save_Bank_Aba_Acct_Nmbr_KRedef65; }

    public DbsField getPnd_Name_Save_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Name_Save_Eft_Status_Ind_K() { return pnd_Name_Save_Eft_Status_Ind_K; }

    public DbsField getPnd_Name_Save_Checking_Saving_Cde_K() { return pnd_Name_Save_Checking_Saving_Cde_K; }

    public DbsField getPnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Name_Save_Pending_Addrss_Chnge_Dte_K() { return pnd_Name_Save_Pending_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_Save_Pending_Addrss_Restore_Dte_K() { return pnd_Name_Save_Pending_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_K() { return pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_Save_Check_Mailing_Addrss_Ind_K() { return pnd_Name_Save_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Name_Save_Addrss_Geographic_Cde_K() { return pnd_Name_Save_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Name_Save_Intl_Eft_Pay_Type_Cde() { return pnd_Name_Save_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Name_Save_Intl_Bank_Pymnt_Eft_Nmbr() { return pnd_Name_Save_Intl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Name_Save_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Name_Save_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Name_Save_Pnd_Cntrct_Name_Add_R() { return pnd_Name_Save_Pnd_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66() { return pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66; }

    public DbsField getPnd_Name_Save_Cntrct_Name_Free_R() { return pnd_Name_Save_Cntrct_Name_Free_R; }

    public DbsField getPnd_Name_Save_Addrss_Lne_1_R() { return pnd_Name_Save_Addrss_Lne_1_R; }

    public DbsField getPnd_Name_Save_Addrss_Lne_2_R() { return pnd_Name_Save_Addrss_Lne_2_R; }

    public DbsField getPnd_Name_Save_Addrss_Lne_3_R() { return pnd_Name_Save_Addrss_Lne_3_R; }

    public DbsField getPnd_Name_Save_Addrss_Lne_4_R() { return pnd_Name_Save_Addrss_Lne_4_R; }

    public DbsField getPnd_Name_Save_Addrss_Lne_5_R() { return pnd_Name_Save_Addrss_Lne_5_R; }

    public DbsField getPnd_Name_Save_Addrss_Lne_6_R() { return pnd_Name_Save_Addrss_Lne_6_R; }

    public DbsField getPnd_Name_Save_Addrss_Postal_Data_R() { return pnd_Name_Save_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Name_Save_Addrss_Postal_Data_RRedef67() { return pnd_Name_Save_Addrss_Postal_Data_RRedef67; }

    public DbsField getPnd_Name_Save_Addrss_Zip_Plus_4_R() { return pnd_Name_Save_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Name_Save_Addrss_Carrier_Rte_R() { return pnd_Name_Save_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Name_Save_Addrss_Walk_Rte_R() { return pnd_Name_Save_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Name_Save_Addrss_Usps_Future_Use_R() { return pnd_Name_Save_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Name_Save_Pnd_Rest_Of_Record_R() { return pnd_Name_Save_Pnd_Rest_Of_Record_R; }

    public DbsGroup getPnd_Name_Save_Pnd_Rest_Of_Record_RRedef68() { return pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68; }

    public DbsField getPnd_Name_Save_Addrss_Type_Cde_R() { return pnd_Name_Save_Addrss_Type_Cde_R; }

    public DbsField getPnd_Name_Save_Addrss_Last_Chnge_Dte_R() { return pnd_Name_Save_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Name_Save_Addrss_Last_Chnge_Time_R() { return pnd_Name_Save_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Name_Save_Permanent_Addrss_Ind_R() { return pnd_Name_Save_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Name_Save_Bank_Aba_Acct_Nmbr_R() { return pnd_Name_Save_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Name_Save_Bank_Aba_Acct_Nmbr_RRedef69() { return pnd_Name_Save_Bank_Aba_Acct_Nmbr_RRedef69; }

    public DbsField getPnd_Name_Save_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Name_Save_Eft_Status_Ind_R() { return pnd_Name_Save_Eft_Status_Ind_R; }

    public DbsField getPnd_Name_Save_Checking_Saving_Cde_R() { return pnd_Name_Save_Checking_Saving_Cde_R; }

    public DbsField getPnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Name_Save_Pending_Addrss_Chnge_Dte_R() { return pnd_Name_Save_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_Save_Pending_Addrss_Restore_Dte_R() { return pnd_Name_Save_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_R() { return pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_Save_Correspondence_Addrss_Ind_R() { return pnd_Name_Save_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Name_Save_Addrss_Geographic_Cde_R() { return pnd_Name_Save_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Name_Save_Pnd_V_Cntrct_Name_Add_K() { return pnd_Name_Save_Pnd_V_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70() { return pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70; }

    public DbsField getPnd_Name_Save_V_Cntrct_Name_Free_K() { return pnd_Name_Save_V_Cntrct_Name_Free_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_1_K() { return pnd_Name_Save_V_Addrss_Lne_1_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_2_K() { return pnd_Name_Save_V_Addrss_Lne_2_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_3_K() { return pnd_Name_Save_V_Addrss_Lne_3_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_4_K() { return pnd_Name_Save_V_Addrss_Lne_4_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_5_K() { return pnd_Name_Save_V_Addrss_Lne_5_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_6_K() { return pnd_Name_Save_V_Addrss_Lne_6_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Postal_Data_K() { return pnd_Name_Save_V_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Name_Save_V_Addrss_Postal_Data_KRedef71() { return pnd_Name_Save_V_Addrss_Postal_Data_KRedef71; }

    public DbsField getPnd_Name_Save_V_Addrss_Zip_Plus_4_K() { return pnd_Name_Save_V_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Carrier_Rte_K() { return pnd_Name_Save_V_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Walk_Rte_K() { return pnd_Name_Save_V_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Usps_Future_Use_K() { return pnd_Name_Save_V_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Name_Save_Pnd_V_Rest_Of_Record_K() { return pnd_Name_Save_Pnd_V_Rest_Of_Record_K; }

    public DbsGroup getPnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72() { return pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72; }

    public DbsField getPnd_Name_Save_V_Addrss_Type_Cde_K() { return pnd_Name_Save_V_Addrss_Type_Cde_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Last_Chnge_Dte_K() { return pnd_Name_Save_V_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Last_Chnge_Time_K() { return pnd_Name_Save_V_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Name_Save_V_Permanent_Addrss_Ind_K() { return pnd_Name_Save_V_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Name_Save_V_Bank_Aba_Acct_Nmbr_K() { return pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Name_Save_V_Bank_Aba_Acct_Nmbr_KRedef73() { return pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_KRedef73; }

    public DbsField getPnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Name_Save_V_Eft_Status_Ind_K() { return pnd_Name_Save_V_Eft_Status_Ind_K; }

    public DbsField getPnd_Name_Save_V_Checking_Saving_Cde_K() { return pnd_Name_Save_V_Checking_Saving_Cde_K; }

    public DbsField getPnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Name_Save_V_Pnding_Addrss_Chnge_Dte_K() { return pnd_Name_Save_V_Pnding_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_Save_V_Pnding_Addrss_Restore_Dte_K() { return pnd_Name_Save_V_Pnding_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Name_Save_V_Pnding_Perm_Addrss_Chnge_Dte_K() { return pnd_Name_Save_V_Pnding_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_Save_V_Check_Mailing_Addrss_Ind_K() { return pnd_Name_Save_V_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Name_Save_V_Addrss_Geographic_Cde_K() { return pnd_Name_Save_V_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Name_Save_V_Intl_Eft_Pay_Type_Cde() { return pnd_Name_Save_V_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Name_Save_V_Iintl_Bank_Pymnt_Eft_Nmbr() { return pnd_Name_Save_V_Iintl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Name_Save_V_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Name_Save_V_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Name_Save_Pnd_V_Cntrct_Name_Add_R() { return pnd_Name_Save_Pnd_V_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74() { return pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74; }

    public DbsField getPnd_Name_Save_V_Cntrct_Name_Free_R() { return pnd_Name_Save_V_Cntrct_Name_Free_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_1_R() { return pnd_Name_Save_V_Addrss_Lne_1_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_2_R() { return pnd_Name_Save_V_Addrss_Lne_2_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_3_R() { return pnd_Name_Save_V_Addrss_Lne_3_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_4_R() { return pnd_Name_Save_V_Addrss_Lne_4_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_5_R() { return pnd_Name_Save_V_Addrss_Lne_5_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Lne_6_R() { return pnd_Name_Save_V_Addrss_Lne_6_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Postal_Data_R() { return pnd_Name_Save_V_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Name_Save_V_Addrss_Postal_Data_RRedef75() { return pnd_Name_Save_V_Addrss_Postal_Data_RRedef75; }

    public DbsField getPnd_Name_Save_V_Addrss_Zip_Plus_4_R() { return pnd_Name_Save_V_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Carrier_Rte_R() { return pnd_Name_Save_V_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Walk_Rte_R() { return pnd_Name_Save_V_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Usps_Future_Use_R() { return pnd_Name_Save_V_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Name_Save_Pnd_V_Rest_Of_Record_R() { return pnd_Name_Save_Pnd_V_Rest_Of_Record_R; }

    public DbsGroup getPnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76() { return pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76; }

    public DbsField getPnd_Name_Save_V_Addrss_Type_Cde_R() { return pnd_Name_Save_V_Addrss_Type_Cde_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Last_Chnge_Dte_R() { return pnd_Name_Save_V_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Last_Chnge_Time_R() { return pnd_Name_Save_V_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Name_Save_V_Permanent_Addrss_Ind_R() { return pnd_Name_Save_V_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Name_Save_V_Bank_Aba_Acct_Nmbr_R() { return pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Name_Save_V_Bank_Aba_Acct_Nmbr_RRedef77() { return pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_RRedef77; }

    public DbsField getPnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Name_Save_V_Eft_Status_Ind_R() { return pnd_Name_Save_V_Eft_Status_Ind_R; }

    public DbsField getPnd_Name_Save_V_Checking_Saving_Cde_R() { return pnd_Name_Save_V_Checking_Saving_Cde_R; }

    public DbsField getPnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Name_Save_V_Pending_Addrss_Chnge_Dte_R() { return pnd_Name_Save_V_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_Save_V_Pending_Addrss_Restore_Dte_R() { return pnd_Name_Save_V_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Name_Save_V_Pending_Perm_Add_Chnge_Dte_R() { return pnd_Name_Save_V_Pending_Perm_Add_Chnge_Dte_R; }

    public DbsField getPnd_Name_Save_V_Correspondence_Addrss_Ind_R() { return pnd_Name_Save_V_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Name_Save_V_Addrss_Geographic_Cde_R() { return pnd_Name_Save_V_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Name_Save_Global_Indicator() { return pnd_Name_Save_Global_Indicator; }

    public DbsField getPnd_Name_Save_Global_Country() { return pnd_Name_Save_Global_Country; }

    public DbsField getPnd_Name_Save_Legal_State() { return pnd_Name_Save_Legal_State; }

    public DbsGroup getPnd_Name() { return pnd_Name; }

    public DbsField getPnd_Name_Ph_Unque_Id_Nmbr() { return pnd_Name_Ph_Unque_Id_Nmbr; }

    public DbsField getPnd_Name_Cntrct_Payee() { return pnd_Name_Cntrct_Payee; }

    public DbsGroup getPnd_Name_Cntrct_PayeeRedef78() { return pnd_Name_Cntrct_PayeeRedef78; }

    public DbsField getPnd_Name_Cntrct_Nmbr() { return pnd_Name_Cntrct_Nmbr; }

    public DbsField getPnd_Name_Cntrct_Payee_Cde() { return pnd_Name_Cntrct_Payee_Cde; }

    public DbsField getPnd_Name_Pnd_Cntrct_Name_Add_K() { return pnd_Name_Pnd_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Name_Pnd_Cntrct_Name_Add_KRedef79() { return pnd_Name_Pnd_Cntrct_Name_Add_KRedef79; }

    public DbsField getPnd_Name_Cntrct_Name_Free_K() { return pnd_Name_Cntrct_Name_Free_K; }

    public DbsField getPnd_Name_Addrss_Lne_1_K() { return pnd_Name_Addrss_Lne_1_K; }

    public DbsField getPnd_Name_Addrss_Lne_2_K() { return pnd_Name_Addrss_Lne_2_K; }

    public DbsField getPnd_Name_Addrss_Lne_3_K() { return pnd_Name_Addrss_Lne_3_K; }

    public DbsField getPnd_Name_Addrss_Lne_4_K() { return pnd_Name_Addrss_Lne_4_K; }

    public DbsField getPnd_Name_Addrss_Lne_5_K() { return pnd_Name_Addrss_Lne_5_K; }

    public DbsField getPnd_Name_Addrss_Lne_6_K() { return pnd_Name_Addrss_Lne_6_K; }

    public DbsField getPnd_Name_Addrss_Postal_Data_K() { return pnd_Name_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Name_Addrss_Postal_Data_KRedef80() { return pnd_Name_Addrss_Postal_Data_KRedef80; }

    public DbsField getPnd_Name_Addrss_Zip_Plus_4_K() { return pnd_Name_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Name_Addrss_Carrier_Rte_K() { return pnd_Name_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Name_Addrss_Walk_Rte_K() { return pnd_Name_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Name_Addrss_Usps_Future_Use_K() { return pnd_Name_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Name_Pnd_Rest_Of_Record_K() { return pnd_Name_Pnd_Rest_Of_Record_K; }

    public DbsGroup getPnd_Name_Pnd_Rest_Of_Record_KRedef81() { return pnd_Name_Pnd_Rest_Of_Record_KRedef81; }

    public DbsField getPnd_Name_Addrss_Type_Cde_K() { return pnd_Name_Addrss_Type_Cde_K; }

    public DbsField getPnd_Name_Addrss_Last_Chnge_Dte_K() { return pnd_Name_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Name_Addrss_Last_Chnge_Time_K() { return pnd_Name_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Name_Permanent_Addrss_Ind_K() { return pnd_Name_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Name_Bank_Aba_Acct_Nmbr_K() { return pnd_Name_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Name_Bank_Aba_Acct_Nmbr_KRedef82() { return pnd_Name_Bank_Aba_Acct_Nmbr_KRedef82; }

    public DbsField getPnd_Name_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Name_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Name_Eft_Status_Ind_K() { return pnd_Name_Eft_Status_Ind_K; }

    public DbsField getPnd_Name_Checking_Saving_Cde_K() { return pnd_Name_Checking_Saving_Cde_K; }

    public DbsField getPnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Name_Pending_Addrss_Chnge_Dte_K() { return pnd_Name_Pending_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_Pending_Addrss_Restore_Dte_K() { return pnd_Name_Pending_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Name_Pending_Perm_Addrss_Chnge_Dte_K() { return pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_Check_Mailing_Addrss_Ind_K() { return pnd_Name_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Name_Addrss_Geographic_Cde_K() { return pnd_Name_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Name_Intl_Eft_Pay_Type_Cde() { return pnd_Name_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Name_Intl_Bank_Pymnt_Eft_Nmbr() { return pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Name_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Name_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Name_Pnd_Cntrct_Name_Add_R() { return pnd_Name_Pnd_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Name_Pnd_Cntrct_Name_Add_RRedef83() { return pnd_Name_Pnd_Cntrct_Name_Add_RRedef83; }

    public DbsField getPnd_Name_Cntrct_Name_Free_R() { return pnd_Name_Cntrct_Name_Free_R; }

    public DbsField getPnd_Name_Addrss_Lne_1_R() { return pnd_Name_Addrss_Lne_1_R; }

    public DbsField getPnd_Name_Addrss_Lne_2_R() { return pnd_Name_Addrss_Lne_2_R; }

    public DbsField getPnd_Name_Addrss_Lne_3_R() { return pnd_Name_Addrss_Lne_3_R; }

    public DbsField getPnd_Name_Addrss_Lne_4_R() { return pnd_Name_Addrss_Lne_4_R; }

    public DbsField getPnd_Name_Addrss_Lne_5_R() { return pnd_Name_Addrss_Lne_5_R; }

    public DbsField getPnd_Name_Addrss_Lne_6_R() { return pnd_Name_Addrss_Lne_6_R; }

    public DbsField getPnd_Name_Addrss_Postal_Data_R() { return pnd_Name_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Name_Addrss_Postal_Data_RRedef84() { return pnd_Name_Addrss_Postal_Data_RRedef84; }

    public DbsField getPnd_Name_Addrss_Zip_Plus_4_R() { return pnd_Name_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Name_Addrss_Carrier_Rte_R() { return pnd_Name_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Name_Addrss_Walk_Rte_R() { return pnd_Name_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Name_Addrss_Usps_Future_Use_R() { return pnd_Name_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Name_Pnd_Rest_Of_Record_R() { return pnd_Name_Pnd_Rest_Of_Record_R; }

    public DbsGroup getPnd_Name_Pnd_Rest_Of_Record_RRedef85() { return pnd_Name_Pnd_Rest_Of_Record_RRedef85; }

    public DbsField getPnd_Name_Addrss_Type_Cde_R() { return pnd_Name_Addrss_Type_Cde_R; }

    public DbsField getPnd_Name_Addrss_Last_Chnge_Dte_R() { return pnd_Name_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Name_Addrss_Last_Chnge_Time_R() { return pnd_Name_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Name_Permanent_Addrss_Ind_R() { return pnd_Name_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Name_Bank_Aba_Acct_Nmbr_R() { return pnd_Name_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Name_Bank_Aba_Acct_Nmbr_RRedef86() { return pnd_Name_Bank_Aba_Acct_Nmbr_RRedef86; }

    public DbsField getPnd_Name_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Name_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Name_Eft_Status_Ind_R() { return pnd_Name_Eft_Status_Ind_R; }

    public DbsField getPnd_Name_Checking_Saving_Cde_R() { return pnd_Name_Checking_Saving_Cde_R; }

    public DbsField getPnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Name_Pending_Addrss_Chnge_Dte_R() { return pnd_Name_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_Pending_Addrss_Restore_Dte_R() { return pnd_Name_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Name_Pending_Perm_Addrss_Chnge_Dte_R() { return pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_Correspondence_Addrss_Ind_R() { return pnd_Name_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Name_Addrss_Geographic_Cde_R() { return pnd_Name_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Name_Pnd_V_Cntrct_Name_Add_K() { return pnd_Name_Pnd_V_Cntrct_Name_Add_K; }

    public DbsGroup getPnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87() { return pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87; }

    public DbsField getPnd_Name_V_Cntrct_Name_Free_K() { return pnd_Name_V_Cntrct_Name_Free_K; }

    public DbsField getPnd_Name_V_Addrss_Lne_1_K() { return pnd_Name_V_Addrss_Lne_1_K; }

    public DbsField getPnd_Name_V_Addrss_Lne_2_K() { return pnd_Name_V_Addrss_Lne_2_K; }

    public DbsField getPnd_Name_V_Addrss_Lne_3_K() { return pnd_Name_V_Addrss_Lne_3_K; }

    public DbsField getPnd_Name_V_Addrss_Lne_4_K() { return pnd_Name_V_Addrss_Lne_4_K; }

    public DbsField getPnd_Name_V_Addrss_Lne_5_K() { return pnd_Name_V_Addrss_Lne_5_K; }

    public DbsField getPnd_Name_V_Addrss_Lne_6_K() { return pnd_Name_V_Addrss_Lne_6_K; }

    public DbsField getPnd_Name_V_Addrss_Postal_Data_K() { return pnd_Name_V_Addrss_Postal_Data_K; }

    public DbsGroup getPnd_Name_V_Addrss_Postal_Data_KRedef88() { return pnd_Name_V_Addrss_Postal_Data_KRedef88; }

    public DbsField getPnd_Name_V_Addrss_Zip_Plus_4_K() { return pnd_Name_V_Addrss_Zip_Plus_4_K; }

    public DbsField getPnd_Name_V_Addrss_Carrier_Rte_K() { return pnd_Name_V_Addrss_Carrier_Rte_K; }

    public DbsField getPnd_Name_V_Addrss_Walk_Rte_K() { return pnd_Name_V_Addrss_Walk_Rte_K; }

    public DbsField getPnd_Name_V_Addrss_Usps_Future_Use_K() { return pnd_Name_V_Addrss_Usps_Future_Use_K; }

    public DbsField getPnd_Name_Pnd_V_Rest_Of_Record_K() { return pnd_Name_Pnd_V_Rest_Of_Record_K; }

    public DbsGroup getPnd_Name_Pnd_V_Rest_Of_Record_KRedef89() { return pnd_Name_Pnd_V_Rest_Of_Record_KRedef89; }

    public DbsField getPnd_Name_V_Addrss_Type_Cde_K() { return pnd_Name_V_Addrss_Type_Cde_K; }

    public DbsField getPnd_Name_V_Addrss_Last_Chnge_Dte_K() { return pnd_Name_V_Addrss_Last_Chnge_Dte_K; }

    public DbsField getPnd_Name_V_Addrss_Last_Chnge_Time_K() { return pnd_Name_V_Addrss_Last_Chnge_Time_K; }

    public DbsField getPnd_Name_V_Permanent_Addrss_Ind_K() { return pnd_Name_V_Permanent_Addrss_Ind_K; }

    public DbsField getPnd_Name_V_Bank_Aba_Acct_Nmbr_K() { return pnd_Name_V_Bank_Aba_Acct_Nmbr_K; }

    public DbsGroup getPnd_Name_V_Bank_Aba_Acct_Nmbr_KRedef90() { return pnd_Name_V_Bank_Aba_Acct_Nmbr_KRedef90; }

    public DbsField getPnd_Name_V_Bank_Aba_Acct_Nmbr_N_K() { return pnd_Name_V_Bank_Aba_Acct_Nmbr_N_K; }

    public DbsField getPnd_Name_V_Eft_Status_Ind_K() { return pnd_Name_V_Eft_Status_Ind_K; }

    public DbsField getPnd_Name_V_Checking_Saving_Cde_K() { return pnd_Name_V_Checking_Saving_Cde_K; }

    public DbsField getPnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K() { return pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K; }

    public DbsField getPnd_Name_V_Pnding_Addrss_Chnge_Dte_K() { return pnd_Name_V_Pnding_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_V_Pnding_Addrss_Restore_Dte_K() { return pnd_Name_V_Pnding_Addrss_Restore_Dte_K; }

    public DbsField getPnd_Name_V_Pnding_Perm_Addrss_Chnge_Dte_K() { return pnd_Name_V_Pnding_Perm_Addrss_Chnge_Dte_K; }

    public DbsField getPnd_Name_V_Check_Mailing_Addrss_Ind_K() { return pnd_Name_V_Check_Mailing_Addrss_Ind_K; }

    public DbsField getPnd_Name_V_Addrss_Geographic_Cde_K() { return pnd_Name_V_Addrss_Geographic_Cde_K; }

    public DbsField getPnd_Name_V_Intl_Eft_Pay_Type_Cde() { return pnd_Name_V_Intl_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr() { return pnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr; }

    public DbsField getPnd_Name_V_Intl_Bank_Pymnt_Acct_Nmbr() { return pnd_Name_V_Intl_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Name_Pnd_V_Cntrct_Name_Add_R() { return pnd_Name_Pnd_V_Cntrct_Name_Add_R; }

    public DbsGroup getPnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91() { return pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91; }

    public DbsField getPnd_Name_V_Cntrct_Name_Free_R() { return pnd_Name_V_Cntrct_Name_Free_R; }

    public DbsField getPnd_Name_V_Addrss_Lne_1_R() { return pnd_Name_V_Addrss_Lne_1_R; }

    public DbsField getPnd_Name_V_Addrss_Lne_2_R() { return pnd_Name_V_Addrss_Lne_2_R; }

    public DbsField getPnd_Name_V_Addrss_Lne_3_R() { return pnd_Name_V_Addrss_Lne_3_R; }

    public DbsField getPnd_Name_V_Addrss_Lne_4_R() { return pnd_Name_V_Addrss_Lne_4_R; }

    public DbsField getPnd_Name_V_Addrss_Lne_5_R() { return pnd_Name_V_Addrss_Lne_5_R; }

    public DbsField getPnd_Name_V_Addrss_Lne_6_R() { return pnd_Name_V_Addrss_Lne_6_R; }

    public DbsField getPnd_Name_V_Addrss_Postal_Data_R() { return pnd_Name_V_Addrss_Postal_Data_R; }

    public DbsGroup getPnd_Name_V_Addrss_Postal_Data_RRedef92() { return pnd_Name_V_Addrss_Postal_Data_RRedef92; }

    public DbsField getPnd_Name_V_Addrss_Zip_Plus_4_R() { return pnd_Name_V_Addrss_Zip_Plus_4_R; }

    public DbsField getPnd_Name_V_Addrss_Carrier_Rte_R() { return pnd_Name_V_Addrss_Carrier_Rte_R; }

    public DbsField getPnd_Name_V_Addrss_Walk_Rte_R() { return pnd_Name_V_Addrss_Walk_Rte_R; }

    public DbsField getPnd_Name_V_Addrss_Usps_Future_Use_R() { return pnd_Name_V_Addrss_Usps_Future_Use_R; }

    public DbsField getPnd_Name_Pnd_V_Rest_Of_Record_R() { return pnd_Name_Pnd_V_Rest_Of_Record_R; }

    public DbsGroup getPnd_Name_Pnd_V_Rest_Of_Record_RRedef93() { return pnd_Name_Pnd_V_Rest_Of_Record_RRedef93; }

    public DbsField getPnd_Name_V_Addrss_Type_Cde_R() { return pnd_Name_V_Addrss_Type_Cde_R; }

    public DbsField getPnd_Name_V_Addrss_Last_Chnge_Dte_R() { return pnd_Name_V_Addrss_Last_Chnge_Dte_R; }

    public DbsField getPnd_Name_V_Addrss_Last_Chnge_Time_R() { return pnd_Name_V_Addrss_Last_Chnge_Time_R; }

    public DbsField getPnd_Name_V_Permanent_Addrss_Ind_R() { return pnd_Name_V_Permanent_Addrss_Ind_R; }

    public DbsField getPnd_Name_V_Bank_Aba_Acct_Nmbr_R() { return pnd_Name_V_Bank_Aba_Acct_Nmbr_R; }

    public DbsGroup getPnd_Name_V_Bank_Aba_Acct_Nmbr_RRedef94() { return pnd_Name_V_Bank_Aba_Acct_Nmbr_RRedef94; }

    public DbsField getPnd_Name_V_Bank_Aba_Acct_Nmbr_N_R() { return pnd_Name_V_Bank_Aba_Acct_Nmbr_N_R; }

    public DbsField getPnd_Name_V_Eft_Status_Ind_R() { return pnd_Name_V_Eft_Status_Ind_R; }

    public DbsField getPnd_Name_V_Checking_Saving_Cde_R() { return pnd_Name_V_Checking_Saving_Cde_R; }

    public DbsField getPnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R() { return pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R; }

    public DbsField getPnd_Name_V_Pending_Addrss_Chnge_Dte_R() { return pnd_Name_V_Pending_Addrss_Chnge_Dte_R; }

    public DbsField getPnd_Name_V_Pending_Addrss_Restore_Dte_R() { return pnd_Name_V_Pending_Addrss_Restore_Dte_R; }

    public DbsField getPnd_Name_V_Pending_Perm_Add_Chnge_Dte_R() { return pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R; }

    public DbsField getPnd_Name_V_Correspondence_Addrss_Ind_R() { return pnd_Name_V_Correspondence_Addrss_Ind_R; }

    public DbsField getPnd_Name_V_Addrss_Geographic_Cde_R() { return pnd_Name_V_Addrss_Geographic_Cde_R; }

    public DbsField getPnd_Name_Global_Indicator() { return pnd_Name_Global_Indicator; }

    public DbsField getPnd_Name_Global_Country() { return pnd_Name_Global_Country; }

    public DbsField getPnd_Name_Legal_State() { return pnd_Name_Legal_State; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Hld = newGroupInRecord("pnd_Hld", "#HLD");
        pnd_Hld_Ph_Unque_Id_Nmbr = pnd_Hld.newFieldInGroup("pnd_Hld_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Hld_Cntrct_Payee = pnd_Hld.newFieldInGroup("pnd_Hld_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Hld_Cntrct_PayeeRedef1 = pnd_Hld.newGroupInGroup("pnd_Hld_Cntrct_PayeeRedef1", "Redefines", pnd_Hld_Cntrct_Payee);
        pnd_Hld_Cntrct_Nmbr = pnd_Hld_Cntrct_PayeeRedef1.newFieldInGroup("pnd_Hld_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Hld_Cntrct_Payee_Cde = pnd_Hld_Cntrct_PayeeRedef1.newFieldInGroup("pnd_Hld_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Hld_Pnd_Cntrct_Name_Add_K = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 245);
        pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2", "Redefines", pnd_Hld_Pnd_Cntrct_Name_Add_K);
        pnd_Hld_Cntrct_Name_Free_K = pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2.newFieldInGroup("pnd_Hld_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", FieldType.STRING, 
            35);
        pnd_Hld_Addrss_Lne_1_K = pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2.newFieldInGroup("pnd_Hld_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_2_K = pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2.newFieldInGroup("pnd_Hld_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_3_K = pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2.newFieldInGroup("pnd_Hld_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_4_K = pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2.newFieldInGroup("pnd_Hld_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_5_K = pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2.newFieldInGroup("pnd_Hld_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_6_K = pnd_Hld_Pnd_Cntrct_Name_Add_KRedef2.newFieldInGroup("pnd_Hld_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 35);
        pnd_Hld_Addrss_Postal_Data_K = pnd_Hld.newFieldInGroup("pnd_Hld_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);
        pnd_Hld_Addrss_Postal_Data_KRedef3 = pnd_Hld.newGroupInGroup("pnd_Hld_Addrss_Postal_Data_KRedef3", "Redefines", pnd_Hld_Addrss_Postal_Data_K);
        pnd_Hld_Addrss_Zip_Plus_4_K = pnd_Hld_Addrss_Postal_Data_KRedef3.newFieldInGroup("pnd_Hld_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 
            9);
        pnd_Hld_Addrss_Carrier_Rte_K = pnd_Hld_Addrss_Postal_Data_KRedef3.newFieldInGroup("pnd_Hld_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", FieldType.STRING, 
            4);
        pnd_Hld_Addrss_Walk_Rte_K = pnd_Hld_Addrss_Postal_Data_KRedef3.newFieldInGroup("pnd_Hld_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", FieldType.STRING, 
            4);
        pnd_Hld_Addrss_Usps_Future_Use_K = pnd_Hld_Addrss_Postal_Data_KRedef3.newFieldInGroup("pnd_Hld_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Hld_Pnd_Rest_Of_Record_K = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 148);
        pnd_Hld_Pnd_Rest_Of_Record_KRedef4 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_Rest_Of_Record_KRedef4", "Redefines", pnd_Hld_Pnd_Rest_Of_Record_K);
        pnd_Hld_Addrss_Type_Cde_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", FieldType.STRING, 
            1);
        pnd_Hld_Addrss_Last_Chnge_Dte_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Addrss_Last_Chnge_Time_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Hld_Permanent_Addrss_Ind_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Hld_Bank_Aba_Acct_Nmbr_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", FieldType.STRING, 
            9);
        pnd_Hld_Bank_Aba_Acct_Nmbr_KRedef5 = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newGroupInGroup("pnd_Hld_Bank_Aba_Acct_Nmbr_KRedef5", "Redefines", pnd_Hld_Bank_Aba_Acct_Nmbr_K);
        pnd_Hld_Bank_Aba_Acct_Nmbr_N_K = pnd_Hld_Bank_Aba_Acct_Nmbr_KRedef5.newFieldInGroup("pnd_Hld_Bank_Aba_Acct_Nmbr_N_K", "BANK-ABA-ACCT-NMBR-N-K", 
            FieldType.NUMERIC, 9);
        pnd_Hld_Eft_Status_Ind_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Eft_Status_Ind_K", "EFT-STATUS-IND-K", FieldType.STRING, 
            1);
        pnd_Hld_Checking_Saving_Cde_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", FieldType.STRING, 
            1);
        pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_K", "PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Hld_Pending_Addrss_Chnge_Dte_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Pending_Addrss_Chnge_Dte_K", "PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Pending_Addrss_Restore_Dte_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Pending_Addrss_Restore_Dte_K", "PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_K", "PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Check_Mailing_Addrss_Ind_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Check_Mailing_Addrss_Ind_K", "CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Hld_Addrss_Geographic_Cde_K = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Hld_Intl_Eft_Pay_Type_Cde = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Hld_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Hld_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Hld_Pnd_Rest_Of_Record_KRedef4.newFieldInGroup("pnd_Hld_Intl_Bank_Pymnt_Acct_Nmbr", "INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Hld_Pnd_Cntrct_Name_Add_R = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 245);
        pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6", "Redefines", pnd_Hld_Pnd_Cntrct_Name_Add_R);
        pnd_Hld_Cntrct_Name_Free_R = pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6.newFieldInGroup("pnd_Hld_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", FieldType.STRING, 
            35);
        pnd_Hld_Addrss_Lne_1_R = pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6.newFieldInGroup("pnd_Hld_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_2_R = pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6.newFieldInGroup("pnd_Hld_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_3_R = pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6.newFieldInGroup("pnd_Hld_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_4_R = pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6.newFieldInGroup("pnd_Hld_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_5_R = pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6.newFieldInGroup("pnd_Hld_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 35);
        pnd_Hld_Addrss_Lne_6_R = pnd_Hld_Pnd_Cntrct_Name_Add_RRedef6.newFieldInGroup("pnd_Hld_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 35);
        pnd_Hld_Addrss_Postal_Data_R = pnd_Hld.newFieldInGroup("pnd_Hld_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);
        pnd_Hld_Addrss_Postal_Data_RRedef7 = pnd_Hld.newGroupInGroup("pnd_Hld_Addrss_Postal_Data_RRedef7", "Redefines", pnd_Hld_Addrss_Postal_Data_R);
        pnd_Hld_Addrss_Zip_Plus_4_R = pnd_Hld_Addrss_Postal_Data_RRedef7.newFieldInGroup("pnd_Hld_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 
            9);
        pnd_Hld_Addrss_Carrier_Rte_R = pnd_Hld_Addrss_Postal_Data_RRedef7.newFieldInGroup("pnd_Hld_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", FieldType.STRING, 
            4);
        pnd_Hld_Addrss_Walk_Rte_R = pnd_Hld_Addrss_Postal_Data_RRedef7.newFieldInGroup("pnd_Hld_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", FieldType.STRING, 
            4);
        pnd_Hld_Addrss_Usps_Future_Use_R = pnd_Hld_Addrss_Postal_Data_RRedef7.newFieldInGroup("pnd_Hld_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Hld_Pnd_Rest_Of_Record_R = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 76);
        pnd_Hld_Pnd_Rest_Of_Record_RRedef8 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_Rest_Of_Record_RRedef8", "Redefines", pnd_Hld_Pnd_Rest_Of_Record_R);
        pnd_Hld_Addrss_Type_Cde_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", FieldType.STRING, 
            1);
        pnd_Hld_Addrss_Last_Chnge_Dte_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Addrss_Last_Chnge_Time_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Hld_Permanent_Addrss_Ind_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Hld_Bank_Aba_Acct_Nmbr_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", FieldType.STRING, 
            9);
        pnd_Hld_Bank_Aba_Acct_Nmbr_RRedef9 = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newGroupInGroup("pnd_Hld_Bank_Aba_Acct_Nmbr_RRedef9", "Redefines", pnd_Hld_Bank_Aba_Acct_Nmbr_R);
        pnd_Hld_Bank_Aba_Acct_Nmbr_N_R = pnd_Hld_Bank_Aba_Acct_Nmbr_RRedef9.newFieldInGroup("pnd_Hld_Bank_Aba_Acct_Nmbr_N_R", "BANK-ABA-ACCT-NMBR-N-R", 
            FieldType.NUMERIC, 9);
        pnd_Hld_Eft_Status_Ind_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Eft_Status_Ind_R", "EFT-STATUS-IND-R", FieldType.STRING, 
            1);
        pnd_Hld_Checking_Saving_Cde_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", FieldType.STRING, 
            1);
        pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_R", "PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Hld_Pending_Addrss_Chnge_Dte_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Pending_Addrss_Chnge_Dte_R", "PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Pending_Addrss_Restore_Dte_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Pending_Addrss_Restore_Dte_R", "PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Pending_Perm_Addrss_Chnge_Dte_R", "PENDING-PERM-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_Correspondence_Addrss_Ind_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Correspondence_Addrss_Ind_R", "CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Hld_Addrss_Geographic_Cde_R = pnd_Hld_Pnd_Rest_Of_Record_RRedef8.newFieldInGroup("pnd_Hld_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Hld_Pnd_V_Cntrct_Name_Add_K = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", FieldType.STRING, 245);
        pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10", "Redefines", pnd_Hld_Pnd_V_Cntrct_Name_Add_K);
        pnd_Hld_V_Cntrct_Name_Free_K = pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10.newFieldInGroup("pnd_Hld_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", 
            FieldType.STRING, 35);
        pnd_Hld_V_Addrss_Lne_1_K = pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10.newFieldInGroup("pnd_Hld_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_2_K = pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10.newFieldInGroup("pnd_Hld_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_3_K = pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10.newFieldInGroup("pnd_Hld_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_4_K = pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10.newFieldInGroup("pnd_Hld_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_5_K = pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10.newFieldInGroup("pnd_Hld_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_6_K = pnd_Hld_Pnd_V_Cntrct_Name_Add_KRedef10.newFieldInGroup("pnd_Hld_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Postal_Data_K = pnd_Hld.newFieldInGroup("pnd_Hld_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);
        pnd_Hld_V_Addrss_Postal_Data_KRedef11 = pnd_Hld.newGroupInGroup("pnd_Hld_V_Addrss_Postal_Data_KRedef11", "Redefines", pnd_Hld_V_Addrss_Postal_Data_K);
        pnd_Hld_V_Addrss_Zip_Plus_4_K = pnd_Hld_V_Addrss_Postal_Data_KRedef11.newFieldInGroup("pnd_Hld_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", 
            FieldType.STRING, 9);
        pnd_Hld_V_Addrss_Carrier_Rte_K = pnd_Hld_V_Addrss_Postal_Data_KRedef11.newFieldInGroup("pnd_Hld_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Hld_V_Addrss_Walk_Rte_K = pnd_Hld_V_Addrss_Postal_Data_KRedef11.newFieldInGroup("pnd_Hld_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", FieldType.STRING, 
            4);
        pnd_Hld_V_Addrss_Usps_Future_Use_K = pnd_Hld_V_Addrss_Postal_Data_KRedef11.newFieldInGroup("pnd_Hld_V_Addrss_Usps_Future_Use_K", "V-ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Hld_Pnd_V_Rest_Of_Record_K = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 148);
        pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12", "Redefines", pnd_Hld_Pnd_V_Rest_Of_Record_K);
        pnd_Hld_V_Addrss_Type_Cde_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", FieldType.STRING, 
            1);
        pnd_Hld_V_Addrss_Last_Chnge_Dte_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Addrss_Last_Chnge_Dte_K", "V-ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Addrss_Last_Chnge_Time_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Addrss_Last_Chnge_Time_K", "V-ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Hld_V_Permanent_Addrss_Ind_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Permanent_Addrss_Ind_K", "V-PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Hld_V_Bank_Aba_Acct_Nmbr_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Hld_V_Bank_Aba_Acct_Nmbr_KRedef13 = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newGroupInGroup("pnd_Hld_V_Bank_Aba_Acct_Nmbr_KRedef13", "Redefines", 
            pnd_Hld_V_Bank_Aba_Acct_Nmbr_K);
        pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_K = pnd_Hld_V_Bank_Aba_Acct_Nmbr_KRedef13.newFieldInGroup("pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_K", "V-BANK-ABA-ACCT-NMBR-N-K", 
            FieldType.NUMERIC, 9);
        pnd_Hld_V_Eft_Status_Ind_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", FieldType.STRING, 
            1);
        pnd_Hld_V_Checking_Saving_Cde_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_K", "V-PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Hld_V_Pnding_Addrss_Chnge_Dte_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Pnding_Addrss_Chnge_Dte_K", "V-PNDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Pnding_Addrss_Restore_Dte_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Pnding_Addrss_Restore_Dte_K", "V-PNDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Pnding_Perm_Addrss_Chnge_Dte_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Pnding_Perm_Addrss_Chnge_Dte_K", "V-PNDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Check_Mailing_Addrss_Ind_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Check_Mailing_Addrss_Ind_K", "V-CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Hld_V_Addrss_Geographic_Cde_K = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Addrss_Geographic_Cde_K", "V-ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Hld_V_Intl_Eft_Pay_Type_Cde = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Intl_Eft_Pay_Type_Cde", "V-INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Hld_V_Iintl_Bank_Pymnt_Eft_Nmbr = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Iintl_Bank_Pymnt_Eft_Nmbr", "V-IINTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Hld_V_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Hld_Pnd_V_Rest_Of_Record_KRedef12.newFieldInGroup("pnd_Hld_V_Intl_Bank_Pymnt_Acct_Nmbr", "V-INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Hld_Pnd_V_Cntrct_Name_Add_R = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", FieldType.STRING, 245);
        pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14", "Redefines", pnd_Hld_Pnd_V_Cntrct_Name_Add_R);
        pnd_Hld_V_Cntrct_Name_Free_R = pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14.newFieldInGroup("pnd_Hld_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", 
            FieldType.STRING, 35);
        pnd_Hld_V_Addrss_Lne_1_R = pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14.newFieldInGroup("pnd_Hld_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_2_R = pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14.newFieldInGroup("pnd_Hld_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_3_R = pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14.newFieldInGroup("pnd_Hld_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_4_R = pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14.newFieldInGroup("pnd_Hld_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_5_R = pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14.newFieldInGroup("pnd_Hld_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Lne_6_R = pnd_Hld_Pnd_V_Cntrct_Name_Add_RRedef14.newFieldInGroup("pnd_Hld_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", FieldType.STRING, 
            35);
        pnd_Hld_V_Addrss_Postal_Data_R = pnd_Hld.newFieldInGroup("pnd_Hld_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);
        pnd_Hld_V_Addrss_Postal_Data_RRedef15 = pnd_Hld.newGroupInGroup("pnd_Hld_V_Addrss_Postal_Data_RRedef15", "Redefines", pnd_Hld_V_Addrss_Postal_Data_R);
        pnd_Hld_V_Addrss_Zip_Plus_4_R = pnd_Hld_V_Addrss_Postal_Data_RRedef15.newFieldInGroup("pnd_Hld_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", 
            FieldType.STRING, 9);
        pnd_Hld_V_Addrss_Carrier_Rte_R = pnd_Hld_V_Addrss_Postal_Data_RRedef15.newFieldInGroup("pnd_Hld_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Hld_V_Addrss_Walk_Rte_R = pnd_Hld_V_Addrss_Postal_Data_RRedef15.newFieldInGroup("pnd_Hld_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", FieldType.STRING, 
            4);
        pnd_Hld_V_Addrss_Usps_Future_Use_R = pnd_Hld_V_Addrss_Postal_Data_RRedef15.newFieldInGroup("pnd_Hld_V_Addrss_Usps_Future_Use_R", "V-ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Hld_Pnd_V_Rest_Of_Record_R = pnd_Hld.newFieldInGroup("pnd_Hld_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 76);
        pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16 = pnd_Hld.newGroupInGroup("pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16", "Redefines", pnd_Hld_Pnd_V_Rest_Of_Record_R);
        pnd_Hld_V_Addrss_Type_Cde_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", FieldType.STRING, 
            1);
        pnd_Hld_V_Addrss_Last_Chnge_Dte_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Addrss_Last_Chnge_Dte_R", "V-ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Addrss_Last_Chnge_Time_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Addrss_Last_Chnge_Time_R", "V-ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Hld_V_Permanent_Addrss_Ind_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Permanent_Addrss_Ind_R", "V-PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Hld_V_Bank_Aba_Acct_Nmbr_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Hld_V_Bank_Aba_Acct_Nmbr_RRedef17 = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newGroupInGroup("pnd_Hld_V_Bank_Aba_Acct_Nmbr_RRedef17", "Redefines", 
            pnd_Hld_V_Bank_Aba_Acct_Nmbr_R);
        pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_R = pnd_Hld_V_Bank_Aba_Acct_Nmbr_RRedef17.newFieldInGroup("pnd_Hld_V_Bank_Aba_Acct_Nmbr_N_R", "V-BANK-ABA-ACCT-NMBR-N-R", 
            FieldType.NUMERIC, 9);
        pnd_Hld_V_Eft_Status_Ind_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", FieldType.STRING, 
            1);
        pnd_Hld_V_Checking_Saving_Cde_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_R", "V-PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Hld_V_Pending_Addrss_Chnge_Dte_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Pending_Addrss_Chnge_Dte_R", "V-PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Pending_Addrss_Restore_Dte_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Pending_Addrss_Restore_Dte_R", "V-PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Pending_Perm_Add_Chnge_Dte_R", "V-PENDING-PERM-ADD-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Hld_V_Correspondence_Addrss_Ind_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Correspondence_Addrss_Ind_R", "V-CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Hld_V_Addrss_Geographic_Cde_R = pnd_Hld_Pnd_V_Rest_Of_Record_RRedef16.newFieldInGroup("pnd_Hld_V_Addrss_Geographic_Cde_R", "V-ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Hld_Global_Indicator = pnd_Hld.newFieldInGroup("pnd_Hld_Global_Indicator", "GLOBAL-INDICATOR", FieldType.STRING, 1);
        pnd_Hld_Global_Country = pnd_Hld.newFieldInGroup("pnd_Hld_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Hld_Legal_State = pnd_Hld.newFieldInGroup("pnd_Hld_Legal_State", "LEGAL-STATE", FieldType.STRING, 2);

        pnd_Core = newFieldInRecord("pnd_Core", "#CORE", FieldType.STRING, 150);
        pnd_CoreRedef18 = newGroupInRecord("pnd_CoreRedef18", "Redefines", pnd_Core);
        pnd_Core_Cntrct_Payee = pnd_CoreRedef18.newFieldInGroup("pnd_Core_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Core_Cntrct_PayeeRedef19 = pnd_CoreRedef18.newGroupInGroup("pnd_Core_Cntrct_PayeeRedef19", "Redefines", pnd_Core_Cntrct_Payee);
        pnd_Core_Cntrct_Nbr = pnd_Core_Cntrct_PayeeRedef19.newFieldInGroup("pnd_Core_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Core_Cntrct_Payee_Cde = pnd_Core_Cntrct_PayeeRedef19.newFieldInGroup("pnd_Core_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Core_Ph_Unique_Id_Nbr = pnd_CoreRedef18.newFieldInGroup("pnd_Core_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Core_Ph_Rcd_Type_Cde = pnd_CoreRedef18.newFieldInGroup("pnd_Core_Ph_Rcd_Type_Cde", "PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Core_Ph_Social_Security_No = pnd_CoreRedef18.newFieldInGroup("pnd_Core_Ph_Social_Security_No", "PH-SOCIAL-SECURITY-NO", FieldType.NUMERIC, 
            9);
        pnd_Core_Ph_Dob_Dte = pnd_CoreRedef18.newFieldInGroup("pnd_Core_Ph_Dob_Dte", "PH-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Core_Ph_Dod_Dte = pnd_CoreRedef18.newFieldInGroup("pnd_Core_Ph_Dod_Dte", "PH-DOD-DTE", FieldType.NUMERIC, 8);
        pnd_Core_Ph_Nme = pnd_CoreRedef18.newGroupInGroup("pnd_Core_Ph_Nme", "PH-NME");
        pnd_Core_Ph_Last_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Last_Nme", "PH-LAST-NME", FieldType.STRING, 30);
        pnd_Core_Ph_Last_NmeRedef20 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core_Ph_Last_NmeRedef20", "Redefines", pnd_Core_Ph_Last_Nme);
        pnd_Core_Ph_Last_Nme_16 = pnd_Core_Ph_Last_NmeRedef20.newFieldInGroup("pnd_Core_Ph_Last_Nme_16", "PH-LAST-NME-16", FieldType.STRING, 16);
        pnd_Core_Ph_First_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_First_Nme", "PH-FIRST-NME", FieldType.STRING, 30);
        pnd_Core_Ph_First_NmeRedef21 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core_Ph_First_NmeRedef21", "Redefines", pnd_Core_Ph_First_Nme);
        pnd_Core_Ph_First_Nme_10 = pnd_Core_Ph_First_NmeRedef21.newFieldInGroup("pnd_Core_Ph_First_Nme_10", "PH-FIRST-NME-10", FieldType.STRING, 10);
        pnd_Core_Ph_Mddle_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Mddle_Nme", "PH-MDDLE-NME", FieldType.STRING, 30);
        pnd_Core_Ph_Mddle_NmeRedef22 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core_Ph_Mddle_NmeRedef22", "Redefines", pnd_Core_Ph_Mddle_Nme);
        pnd_Core_Ph_Mddle_Nme_12 = pnd_Core_Ph_Mddle_NmeRedef22.newFieldInGroup("pnd_Core_Ph_Mddle_Nme_12", "PH-MDDLE-NME-12", FieldType.STRING, 12);
        pnd_Core_Cntrct_Status_Cde = pnd_CoreRedef18.newFieldInGroup("pnd_Core_Cntrct_Status_Cde", "CNTRCT-STATUS-CDE", FieldType.STRING, 1);

        pnd_Input = newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Input_Pnd_Cntrct_PayeeRedef23 = pnd_Input.newGroupInGroup("pnd_Input_Pnd_Cntrct_PayeeRedef23", "Redefines", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input_Pnd_Cntrct_PayeeRedef23.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input_Pnd_Cntrct_PayeeRedef23.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Payee_CdeRedef24 = pnd_Input_Pnd_Cntrct_PayeeRedef23.newGroupInGroup("pnd_Input_Pnd_Payee_CdeRedef24", "Redefines", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input_Pnd_Payee_CdeRedef24.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1,353));
        pnd_Input_Pnd_Rest_Of_Record_353Redef25 = pnd_Input.newGroupInGroup("pnd_Input_Pnd_Rest_Of_Record_353Redef25", "Redefines", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef25.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Rest_Of_Record_353Redef26 = pnd_Input.newGroupInGroup("pnd_Input_Pnd_Rest_Of_Record_353Redef26", "Redefines", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_F1 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Issue_DteRedef27 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newGroupInGroup("pnd_Input_Pnd_Issue_DteRedef27", "Redefines", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input_Pnd_Issue_DteRedef27.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_Crrncy_CdeRedef28 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newGroupInGroup("pnd_Input_Pnd_Crrncy_CdeRedef28", "Redefines", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input_Pnd_Crrncy_CdeRedef28.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Type_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F3 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 21);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_F4 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Pnd_F5 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_F5", "#F5", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_F6 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Pnd_F7 = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_F7", "#F7", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 
            5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Lst_Trans_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Pnd_Cntrct_Type = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re", "#CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3);
        pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte", "#CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5));
        pnd_Input_Pnd_Cntrct_Mtch_Ppcn = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", 
            FieldType.STRING, 10);
        pnd_Input_Pnd_Cntrct_Annty_Strt_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Annty_Strt_Dte", "#CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd", "#CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd", "#CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Sub_Plan_Nmbr = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Sub_Plan_Nmbr", "#SUB-PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr", "#ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6);
        pnd_Input_Pnd_Orgntng_Cntrct_Nmbr = pnd_Input_Pnd_Rest_Of_Record_353Redef26.newFieldInGroup("pnd_Input_Pnd_Orgntng_Cntrct_Nmbr", "#ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10);
        pnd_Input_Pnd_Rest_Of_Record_353Redef29 = pnd_Input.newGroupInGroup("pnd_Input_Pnd_Rest_Of_Record_353Redef29", "Redefines", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_CdeRedef30 = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newGroupInGroup("pnd_Input_Pnd_Ddctn_CdeRedef30", "Redefines", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input_Pnd_Ddctn_CdeRedef30.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Input_Pnd_Ddctn_Seq_NbrRedef31 = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newGroupInGroup("pnd_Input_Pnd_Ddctn_Seq_NbrRedef31", "Redefines", 
            pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input_Pnd_Ddctn_Seq_NbrRedef31.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 
            3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.DECIMAL, 
            7,2);
        pnd_Input_Pnd_F10 = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef29.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Rest_Of_Record_353Redef32 = pnd_Input.newGroupInGroup("pnd_Input_Pnd_Rest_Of_Record_353Redef32", "Redefines", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Input_Pnd_F11 = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Pnd_Summ_Per_DvdndRedef33 = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newGroupInGroup("pnd_Input_Pnd_Summ_Per_DvdndRedef33", "Redefines", 
            pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input_Pnd_Summ_Per_DvdndRedef33.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9,4);
        pnd_Input_Pnd_F12 = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input_Pnd_Rest_Of_Record_353Redef32.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Input_Pnd_Rest_Of_Record_353Redef34 = pnd_Input.newGroupInGroup("pnd_Input_Pnd_Rest_Of_Record_353Redef34", "Redefines", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Input_Pnd_F13 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 
            3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_F14 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 
            9);
        pnd_Input_Pnd_F15 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_F16 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 3);
        pnd_Input_Pnd_Cash_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Pnd_F17 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1,5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Input_Pnd_F18 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_F20 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F20", "#F20", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Pend_DteRedef35 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newGroupInGroup("pnd_Input_Pnd_Pend_DteRedef35", "Redefines", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input_Pnd_Pend_DteRedef35.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_F22 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F22", "#F22", FieldType.STRING, 29);
        pnd_Input_Pnd_Cntrct_Local_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Pnd_F23 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F23", "#F23", FieldType.STRING, 19);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", 
            FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", 
            FieldType.STRING, 1);
        pnd_Input_Pnd_F24 = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input_Pnd_Rest_Of_Record_353Redef34.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);

        gtn_Pda_E = newGroupInRecord("gtn_Pda_E", "GTN-PDA-E");
        gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 
            15);
        gtn_Pda_E_Cntrct_Unq_Id_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        gtn_Pda_E_Cntrct_Ppcn_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Cntrct_Cmbn_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        gtn_Pda_E_Cntrct_Cmbn_NbrRedef36 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Cntrct_Cmbn_NbrRedef36", "Redefines", gtn_Pda_E_Cntrct_Cmbn_Nbr);
        gtn_Pda_E_Cntrct_Cmbn_Nbr_A10 = gtn_Pda_E_Cntrct_Cmbn_NbrRedef36.newFieldInGroup("gtn_Pda_E_Cntrct_Cmbn_Nbr_A10", "CNTRCT-CMBN-NBR-A10", FieldType.STRING, 
            10);
        gtn_Pda_E_Cntrct_Cmbn_Nbr_Payee = gtn_Pda_E_Cntrct_Cmbn_NbrRedef36.newFieldInGroup("gtn_Pda_E_Cntrct_Cmbn_Nbr_Payee", "CNTRCT-CMBN-NBR-PAYEE", 
            FieldType.STRING, 4);
        gtn_Pda_E_Annt_Soc_Sec_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        gtn_Pda_E_Annt_Soc_Sec_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        gtn_Pda_E_Annt_Ctznshp_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_E_Annt_Rsdncy_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Annt_Locality_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Ph_Name = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Ph_Name", "PH-NAME");
        gtn_Pda_E_Ph_Last_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        gtn_Pda_E_Ph_Middle_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        gtn_Pda_E_Ph_First_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        gtn_Pda_E_Pymnt_Dob = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);
        gtn_Pda_E_Pymnt_Nme_And_Addr_Grp = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1,
            2));
        gtn_Pda_E_Pymnt_Nme = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        gtn_Pda_E_Pymnt_Addr_Lines = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Lines", "PYMNT-ADDR-LINES", FieldType.STRING, 
            210);
        gtn_Pda_E_Pymnt_Addr_LinesRedef37 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E_Pymnt_Addr_LinesRedef37", "Redefines", gtn_Pda_E_Pymnt_Addr_Lines);
        gtn_Pda_E_Pymnt_Addr_Line_Txt = gtn_Pda_E_Pymnt_Addr_LinesRedef37.newFieldArrayInGroup("gtn_Pda_E_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        gtn_Pda_E_Pymnt_Addr_LinesRedef38 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E_Pymnt_Addr_LinesRedef38", "Redefines", gtn_Pda_E_Pymnt_Addr_Lines);
        gtn_Pda_E_Pymnt_Addr_Line1_Txt = gtn_Pda_E_Pymnt_Addr_LinesRedef38.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line2_Txt = gtn_Pda_E_Pymnt_Addr_LinesRedef38.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line3_Txt = gtn_Pda_E_Pymnt_Addr_LinesRedef38.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line4_Txt = gtn_Pda_E_Pymnt_Addr_LinesRedef38.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line5_Txt = gtn_Pda_E_Pymnt_Addr_LinesRedef38.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line6_Txt = gtn_Pda_E_Pymnt_Addr_LinesRedef38.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Zip_Cde = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 
            9);
        gtn_Pda_E_Pymnt_Postl_Data = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 
            32);
        gtn_Pda_E_Pymnt_Addr_Type_Ind = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Foreign_Cde = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Foreign_Cde", "PYMNT-FOREIGN-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", 
            FieldType.NUMERIC, 8);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", 
            FieldType.NUMERIC, 7);
        gtn_Pda_E_Pymnt_Addr_Chg_Ind = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Settl_Ivc_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1);
        gtn_Pda_E_Pnd_Pnd_Fund_Payee = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Payee", "##FUND-PAYEE", new DbsArrayController(1,40));
        gtn_Pda_E_Inv_Acct_Cde = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Inv_Acct_Settl_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        gtn_Pda_E_Inv_Acct_Cntrct_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Cntrct_Amt", "INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        gtn_Pda_E_Inv_Acct_Dvdnd_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        gtn_Pda_E_Inv_Acct_Unit_Value = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9,4);
        gtn_Pda_E_Inv_Acct_Unit_Qty = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9,3);
        gtn_Pda_E_Inv_Acct_Ivc_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        gtn_Pda_E_Inv_Acct_Ivc_Ind = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Inv_Acct_Valuat_Period = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        gtn_Pda_E_Pnd_Pnd_Deductions = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Deductions", "##DEDUCTIONS", new DbsArrayController(1,10));
        gtn_Pda_E_Pymnt_Ded_Cde = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_E_Pymnt_Ded_Amt = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9,
            2);
        gtn_Pda_E_Pymnt_Ded_Payee_Cde = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 
            8);
        gtn_Pda_E_Pymnt_Ded_Payee_CdeRedef39 = gtn_Pda_E_Pnd_Pnd_Deductions.newGroupInGroup("gtn_Pda_E_Pymnt_Ded_Payee_CdeRedef39", "Redefines", gtn_Pda_E_Pymnt_Ded_Payee_Cde);
        gtn_Pda_E_Pymnt_Ded_Payee_Cde_5 = gtn_Pda_E_Pymnt_Ded_Payee_CdeRedef39.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Payee_Cde_5", "PYMNT-DED-PAYEE-CDE-5", 
            FieldType.STRING, 5);
        gtn_Pda_E_Pymnt_Ded_Payee_Cde_3 = gtn_Pda_E_Pymnt_Ded_Payee_CdeRedef39.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Payee_Cde_3", "PYMNT-DED-PAYEE-CDE-3", 
            FieldType.STRING, 3);
        gtn_Pda_E_Pymnt_Payee_Tax_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Payee_Tax_Ind", "PYMNT-PAYEE-TAX-IND", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Settlmnt_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Check_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Cycle_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Acctg_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Ia_Issue_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Intrfce_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        gtn_Pda_E_Cntrct_Check_Crrncy_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Orgn_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Type_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Payee_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Option_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_E_Cntrct_Ac_Lt_10yrs = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN);
        gtn_Pda_E_Cntrct_Mode_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_E_Pymnt_Pay_Type_Req_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        gtn_Pda_E_Pymnt_Spouse_Pay_Stats = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Pymnt_Type_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Pymnt_Dest_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Roll_Dest_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        gtn_Pda_E_Pymnt_Eft_Acct_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        gtn_Pda_E_Pymnt_Eft_Transit_Id_A = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Transit_Id_A", "PYMNT-EFT-TRANSIT-ID-A", FieldType.STRING, 9);
        gtn_Pda_E_Pymnt_Eft_Transit_Id_ARedef40 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Pymnt_Eft_Transit_Id_ARedef40", "Redefines", gtn_Pda_E_Pymnt_Eft_Transit_Id_A);
        gtn_Pda_E_Pymnt_Eft_Transit_Id = gtn_Pda_E_Pymnt_Eft_Transit_Id_ARedef40.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        gtn_Pda_E_Pymnt_Chk_Sav_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Hold_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Hold_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Hold_Grp = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        gtn_Pda_E_Cntrct_Hold_User_Id = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", FieldType.STRING, 3);
        gtn_Pda_E_Pymnt_Tax_Exempt_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Qlfied_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Sub_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Ia_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Annty_Ins_Type = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Annty_Type_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Insurance_Option = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Life_Contingency = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Suspend_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Suspend_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Suspend_Dte", "PYMNT-SUSPEND-DTE", FieldType.DATE);
        gtn_Pda_E_Pnd_Pnd_This_Pymnt = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_This_Pymnt", "##THIS-PYMNT", FieldType.NUMERIC, 2);
        gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts", "##NBR-OF-PYMNTS", FieldType.NUMERIC, 2);
        gtn_Pda_E_Gtn_Ret_Code = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Gtn_Ret_Code", "GTN-RET-CODE", FieldType.STRING, 4);
        gtn_Pda_E_Gtn_Ret_CodeRedef41 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Gtn_Ret_CodeRedef41", "Redefines", gtn_Pda_E_Gtn_Ret_Code);
        gtn_Pda_E_Global_Country = gtn_Pda_E_Gtn_Ret_CodeRedef41.newFieldInGroup("gtn_Pda_E_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        gtn_Pda_E_Temp_Pend_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Temp_Pend_Cde", "TEMP-PEND-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Error_Code_1 = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Error_Code_1", "ERROR-CODE-1", FieldType.STRING, 1);
        gtn_Pda_E_Error_Code_2 = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Error_Code_2", "ERROR-CODE-2", FieldType.STRING, 1);
        gtn_Pda_E_Current_Mode = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Current_Mode", "CURRENT-MODE", FieldType.STRING, 1);
        gtn_Pda_E_Egtrra_Eligibility_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 1);
        gtn_Pda_E_Originating_Irs_Cde = gtn_Pda_E.newFieldArrayInGroup("gtn_Pda_E_Originating_Irs_Cde", "ORIGINATING-IRS-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1,8));
        gtn_Pda_E_Originating_Irs_CdeRedef42 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Originating_Irs_CdeRedef42", "Redefines", gtn_Pda_E_Originating_Irs_Cde);
        gtn_Pda_E_Distributing_Irc_Cde = gtn_Pda_E_Originating_Irs_CdeRedef42.newFieldInGroup("gtn_Pda_E_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", 
            FieldType.STRING, 16);
        gtn_Pda_E_Receiving_Irc_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Money_Source = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Money_Source", "CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_E_Roth_Dob = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Dob", "ROTH-DOB", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_First_Contrib_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_First_Contrib_Dte", "ROTH-FIRST-CONTRIB-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_E_Roth_Death_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Death_Dte", "ROTH-DEATH-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_Death_DteRedef43 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Roth_Death_DteRedef43", "Redefines", gtn_Pda_E_Roth_Death_Dte);
        gtn_Pda_E_Roth_Death_Dte_N6 = gtn_Pda_E_Roth_Death_DteRedef43.newFieldInGroup("gtn_Pda_E_Roth_Death_Dte_N6", "ROTH-DEATH-DTE-N6", FieldType.NUMERIC, 
            6);
        gtn_Pda_E_Roth_Death_Dte_Dd = gtn_Pda_E_Roth_Death_DteRedef43.newFieldInGroup("gtn_Pda_E_Roth_Death_Dte_Dd", "ROTH-DEATH-DTE-DD", FieldType.STRING, 
            2);
        gtn_Pda_E_Roth_Disability_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Disability_Dte", "ROTH-DISABILITY-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_Money_Source = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_E_Roth_Qual_Non_Qual_Distrib = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Qual_Non_Qual_Distrib", "ROTH-QUAL-NON-QUAL-DISTRIB", FieldType.STRING, 
            1);
        gtn_Pda_E_Ia_Orgn_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Ia_Orgn_Cde", "IA-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Plan_Number = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Plan_Number", "PLAN-NUMBER", FieldType.STRING, 6);
        gtn_Pda_E_Sub_Plan = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Sub_Plan", "SUB-PLAN", FieldType.STRING, 6);
        gtn_Pda_E_Orig_Cntrct_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Orig_Cntrct_Nbr", "ORIG-CNTRCT-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Orig_Sub_Plan = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Orig_Sub_Plan", "ORIG-SUB-PLAN", FieldType.STRING, 6);

        pnd_Name_01 = newGroupInRecord("pnd_Name_01", "#NAME-01");
        pnd_Name_01_Ph_Unque_Id_Nmbr = pnd_Name_01.newFieldInGroup("pnd_Name_01_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_01_Cntrct_Payee = pnd_Name_01.newFieldInGroup("pnd_Name_01_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Name_01_Cntrct_PayeeRedef44 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Cntrct_PayeeRedef44", "Redefines", pnd_Name_01_Cntrct_Payee);
        pnd_Name_01_Cntrct_Nmbr = pnd_Name_01_Cntrct_PayeeRedef44.newFieldInGroup("pnd_Name_01_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_01_Cntrct_Payee_Cde = pnd_Name_01_Cntrct_PayeeRedef44.newFieldInGroup("pnd_Name_01_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Name_01_Pnd_Cntrct_Name_Add_K = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 245);
        pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45", "Redefines", pnd_Name_01_Pnd_Cntrct_Name_Add_K);
        pnd_Name_01_Cntrct_Name_Free_K = pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45.newFieldInGroup("pnd_Name_01_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", 
            FieldType.STRING, 35);
        pnd_Name_01_Addrss_Lne_1_K = pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45.newFieldInGroup("pnd_Name_01_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_2_K = pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45.newFieldInGroup("pnd_Name_01_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_3_K = pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45.newFieldInGroup("pnd_Name_01_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_4_K = pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45.newFieldInGroup("pnd_Name_01_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_5_K = pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45.newFieldInGroup("pnd_Name_01_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_6_K = pnd_Name_01_Pnd_Cntrct_Name_Add_KRedef45.newFieldInGroup("pnd_Name_01_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Postal_Data_K = pnd_Name_01.newFieldInGroup("pnd_Name_01_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);
        pnd_Name_01_Addrss_Postal_Data_KRedef46 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Addrss_Postal_Data_KRedef46", "Redefines", pnd_Name_01_Addrss_Postal_Data_K);
        pnd_Name_01_Addrss_Zip_Plus_4_K = pnd_Name_01_Addrss_Postal_Data_KRedef46.newFieldInGroup("pnd_Name_01_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", 
            FieldType.STRING, 9);
        pnd_Name_01_Addrss_Carrier_Rte_K = pnd_Name_01_Addrss_Postal_Data_KRedef46.newFieldInGroup("pnd_Name_01_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_01_Addrss_Walk_Rte_K = pnd_Name_01_Addrss_Postal_Data_KRedef46.newFieldInGroup("pnd_Name_01_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_01_Addrss_Usps_Future_Use_K = pnd_Name_01_Addrss_Postal_Data_KRedef46.newFieldInGroup("pnd_Name_01_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_01_Pnd_Rest_Of_Record_K = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 148);
        pnd_Name_01_Pnd_Rest_Of_Record_KRedef47 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_Rest_Of_Record_KRedef47", "Redefines", pnd_Name_01_Pnd_Rest_Of_Record_K);
        pnd_Name_01_Addrss_Type_Cde_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_01_Addrss_Last_Chnge_Dte_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_01_Addrss_Last_Chnge_Time_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_01_Permanent_Addrss_Ind_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_01_Bank_Aba_Acct_Nmbr_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Name_01_Bank_Aba_Acct_Nmbr_KRedef48 = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newGroupInGroup("pnd_Name_01_Bank_Aba_Acct_Nmbr_KRedef48", "Redefines", 
            pnd_Name_01_Bank_Aba_Acct_Nmbr_K);
        pnd_Name_01_Bank_Aba_Acct_Nmbr_N_K = pnd_Name_01_Bank_Aba_Acct_Nmbr_KRedef48.newFieldInGroup("pnd_Name_01_Bank_Aba_Acct_Nmbr_N_K", "BANK-ABA-ACCT-NMBR-N-K", 
            FieldType.NUMERIC, 9);
        pnd_Name_01_Eft_Status_Ind_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Eft_Status_Ind_K", "EFT-STATUS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_01_Checking_Saving_Cde_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_K", "PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Name_01_Pending_Addrss_Chnge_Dte_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Pending_Addrss_Chnge_Dte_K", "PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_01_Pending_Addrss_Restore_Dte_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Pending_Addrss_Restore_Dte_K", 
            "PENDING-ADDRSS-RESTORE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_K", 
            "PENDING-PERM-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_01_Check_Mailing_Addrss_Ind_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Check_Mailing_Addrss_Ind_K", "CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_01_Addrss_Geographic_Cde_K = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Name_01_Intl_Eft_Pay_Type_Cde = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_01_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_01_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_01_Pnd_Rest_Of_Record_KRedef47.newFieldInGroup("pnd_Name_01_Intl_Bank_Pymnt_Acct_Nmbr", "INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_01_Pnd_Cntrct_Name_Add_R = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 245);
        pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49", "Redefines", pnd_Name_01_Pnd_Cntrct_Name_Add_R);
        pnd_Name_01_Cntrct_Name_Free_R = pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49.newFieldInGroup("pnd_Name_01_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", 
            FieldType.STRING, 35);
        pnd_Name_01_Addrss_Lne_1_R = pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49.newFieldInGroup("pnd_Name_01_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_2_R = pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49.newFieldInGroup("pnd_Name_01_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_3_R = pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49.newFieldInGroup("pnd_Name_01_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_4_R = pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49.newFieldInGroup("pnd_Name_01_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_5_R = pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49.newFieldInGroup("pnd_Name_01_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Lne_6_R = pnd_Name_01_Pnd_Cntrct_Name_Add_RRedef49.newFieldInGroup("pnd_Name_01_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 
            35);
        pnd_Name_01_Addrss_Postal_Data_R = pnd_Name_01.newFieldInGroup("pnd_Name_01_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);
        pnd_Name_01_Addrss_Postal_Data_RRedef50 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Addrss_Postal_Data_RRedef50", "Redefines", pnd_Name_01_Addrss_Postal_Data_R);
        pnd_Name_01_Addrss_Zip_Plus_4_R = pnd_Name_01_Addrss_Postal_Data_RRedef50.newFieldInGroup("pnd_Name_01_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", 
            FieldType.STRING, 9);
        pnd_Name_01_Addrss_Carrier_Rte_R = pnd_Name_01_Addrss_Postal_Data_RRedef50.newFieldInGroup("pnd_Name_01_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_01_Addrss_Walk_Rte_R = pnd_Name_01_Addrss_Postal_Data_RRedef50.newFieldInGroup("pnd_Name_01_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_01_Addrss_Usps_Future_Use_R = pnd_Name_01_Addrss_Postal_Data_RRedef50.newFieldInGroup("pnd_Name_01_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_01_Pnd_Rest_Of_Record_R = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 76);
        pnd_Name_01_Pnd_Rest_Of_Record_RRedef51 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_Rest_Of_Record_RRedef51", "Redefines", pnd_Name_01_Pnd_Rest_Of_Record_R);
        pnd_Name_01_Addrss_Type_Cde_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_01_Addrss_Last_Chnge_Dte_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_01_Addrss_Last_Chnge_Time_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_01_Permanent_Addrss_Ind_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_01_Bank_Aba_Acct_Nmbr_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Name_01_Bank_Aba_Acct_Nmbr_RRedef52 = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newGroupInGroup("pnd_Name_01_Bank_Aba_Acct_Nmbr_RRedef52", "Redefines", 
            pnd_Name_01_Bank_Aba_Acct_Nmbr_R);
        pnd_Name_01_Bank_Aba_Acct_Nmbr_N_R = pnd_Name_01_Bank_Aba_Acct_Nmbr_RRedef52.newFieldInGroup("pnd_Name_01_Bank_Aba_Acct_Nmbr_N_R", "BANK-ABA-ACCT-NMBR-N-R", 
            FieldType.NUMERIC, 9);
        pnd_Name_01_Eft_Status_Ind_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Eft_Status_Ind_R", "EFT-STATUS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_01_Checking_Saving_Cde_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Ph_Bank_Pymnt_Acct_Nmbr_R", "PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Name_01_Pending_Addrss_Chnge_Dte_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Pending_Addrss_Chnge_Dte_R", "PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_01_Pending_Addrss_Restore_Dte_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Pending_Addrss_Restore_Dte_R", 
            "PENDING-ADDRSS-RESTORE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Pending_Perm_Addrss_Chnge_Dte_R", 
            "PENDING-PERM-ADDRSS-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_01_Correspondence_Addrss_Ind_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Correspondence_Addrss_Ind_R", "CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_01_Addrss_Geographic_Cde_R = pnd_Name_01_Pnd_Rest_Of_Record_RRedef51.newFieldInGroup("pnd_Name_01_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Name_01_Pnd_V_Cntrct_Name_Add_K = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", FieldType.STRING, 
            245);
        pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53", "Redefines", pnd_Name_01_Pnd_V_Cntrct_Name_Add_K);
        pnd_Name_01_V_Cntrct_Name_Free_K = pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53.newFieldInGroup("pnd_Name_01_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_1_K = pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_2_K = pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_3_K = pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_4_K = pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_5_K = pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_6_K = pnd_Name_01_Pnd_V_Cntrct_Name_Add_KRedef53.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Postal_Data_K = pnd_Name_01.newFieldInGroup("pnd_Name_01_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", FieldType.STRING, 
            32);
        pnd_Name_01_V_Addrss_Postal_Data_KRedef54 = pnd_Name_01.newGroupInGroup("pnd_Name_01_V_Addrss_Postal_Data_KRedef54", "Redefines", pnd_Name_01_V_Addrss_Postal_Data_K);
        pnd_Name_01_V_Addrss_Zip_Plus_4_K = pnd_Name_01_V_Addrss_Postal_Data_KRedef54.newFieldInGroup("pnd_Name_01_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", 
            FieldType.STRING, 9);
        pnd_Name_01_V_Addrss_Carrier_Rte_K = pnd_Name_01_V_Addrss_Postal_Data_KRedef54.newFieldInGroup("pnd_Name_01_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_01_V_Addrss_Walk_Rte_K = pnd_Name_01_V_Addrss_Postal_Data_KRedef54.newFieldInGroup("pnd_Name_01_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_01_V_Addrss_Usps_Future_Use_K = pnd_Name_01_V_Addrss_Postal_Data_KRedef54.newFieldInGroup("pnd_Name_01_V_Addrss_Usps_Future_Use_K", "V-ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_01_Pnd_V_Rest_Of_Record_K = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 
            148);
        pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55", "Redefines", pnd_Name_01_Pnd_V_Rest_Of_Record_K);
        pnd_Name_01_V_Addrss_Type_Cde_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Addrss_Last_Chnge_Dte_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Addrss_Last_Chnge_Dte_K", "V-ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_01_V_Addrss_Last_Chnge_Time_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Addrss_Last_Chnge_Time_K", "V-ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_01_V_Permanent_Addrss_Ind_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Permanent_Addrss_Ind_K", "V-PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Bank_Aba_Acct_Nmbr_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Name_01_V_Bank_Aba_Acct_Nmbr_KRedef56 = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newGroupInGroup("pnd_Name_01_V_Bank_Aba_Acct_Nmbr_KRedef56", 
            "Redefines", pnd_Name_01_V_Bank_Aba_Acct_Nmbr_K);
        pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_K = pnd_Name_01_V_Bank_Aba_Acct_Nmbr_KRedef56.newFieldInGroup("pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_K", "V-BANK-ABA-ACCT-NMBR-N-K", 
            FieldType.NUMERIC, 9);
        pnd_Name_01_V_Eft_Status_Ind_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Checking_Saving_Cde_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_K", 
            "V-PH-BANK-PYMNT-ACCT-NMBR-K", FieldType.STRING, 21);
        pnd_Name_01_V_Pnding_Addrss_Chnge_Dte_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Pnding_Addrss_Chnge_Dte_K", 
            "V-PNDING-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_01_V_Pnding_Addrss_Restore_Dte_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Pnding_Addrss_Restore_Dte_K", 
            "V-PNDING-ADDRSS-RESTORE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_01_V_Pnding_Perm_Addrss_Chnge_Dte_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Pnding_Perm_Addrss_Chnge_Dte_K", 
            "V-PNDING-PERM-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_01_V_Check_Mailing_Addrss_Ind_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Check_Mailing_Addrss_Ind_K", 
            "V-CHECK-MAILING-ADDRSS-IND-K", FieldType.STRING, 1);
        pnd_Name_01_V_Addrss_Geographic_Cde_K = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Addrss_Geographic_Cde_K", "V-ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Name_01_V_Intl_Eft_Pay_Type_Cde = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Intl_Eft_Pay_Type_Cde", "V-INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_01_V_Iintl_Bank_Pymnt_Eft_Nmbr = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Iintl_Bank_Pymnt_Eft_Nmbr", 
            "V-IINTL-BANK-PYMNT-EFT-NMBR", FieldType.STRING, 35);
        pnd_Name_01_V_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_01_Pnd_V_Rest_Of_Record_KRedef55.newFieldInGroup("pnd_Name_01_V_Intl_Bank_Pymnt_Acct_Nmbr", 
            "V-INTL-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 35);
        pnd_Name_01_Pnd_V_Cntrct_Name_Add_R = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", FieldType.STRING, 
            245);
        pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57", "Redefines", pnd_Name_01_Pnd_V_Cntrct_Name_Add_R);
        pnd_Name_01_V_Cntrct_Name_Free_R = pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57.newFieldInGroup("pnd_Name_01_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_1_R = pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_2_R = pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_3_R = pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_4_R = pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_5_R = pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Lne_6_R = pnd_Name_01_Pnd_V_Cntrct_Name_Add_RRedef57.newFieldInGroup("pnd_Name_01_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", 
            FieldType.STRING, 35);
        pnd_Name_01_V_Addrss_Postal_Data_R = pnd_Name_01.newFieldInGroup("pnd_Name_01_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", FieldType.STRING, 
            32);
        pnd_Name_01_V_Addrss_Postal_Data_RRedef58 = pnd_Name_01.newGroupInGroup("pnd_Name_01_V_Addrss_Postal_Data_RRedef58", "Redefines", pnd_Name_01_V_Addrss_Postal_Data_R);
        pnd_Name_01_V_Addrss_Zip_Plus_4_R = pnd_Name_01_V_Addrss_Postal_Data_RRedef58.newFieldInGroup("pnd_Name_01_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", 
            FieldType.STRING, 9);
        pnd_Name_01_V_Addrss_Carrier_Rte_R = pnd_Name_01_V_Addrss_Postal_Data_RRedef58.newFieldInGroup("pnd_Name_01_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_01_V_Addrss_Walk_Rte_R = pnd_Name_01_V_Addrss_Postal_Data_RRedef58.newFieldInGroup("pnd_Name_01_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_01_V_Addrss_Usps_Future_Use_R = pnd_Name_01_V_Addrss_Postal_Data_RRedef58.newFieldInGroup("pnd_Name_01_V_Addrss_Usps_Future_Use_R", "V-ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_01_Pnd_V_Rest_Of_Record_R = pnd_Name_01.newFieldInGroup("pnd_Name_01_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 
            76);
        pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59 = pnd_Name_01.newGroupInGroup("pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59", "Redefines", pnd_Name_01_Pnd_V_Rest_Of_Record_R);
        pnd_Name_01_V_Addrss_Type_Cde_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Addrss_Last_Chnge_Dte_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Addrss_Last_Chnge_Dte_R", "V-ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_01_V_Addrss_Last_Chnge_Time_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Addrss_Last_Chnge_Time_R", "V-ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_01_V_Permanent_Addrss_Ind_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Permanent_Addrss_Ind_R", "V-PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Bank_Aba_Acct_Nmbr_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Name_01_V_Bank_Aba_Acct_Nmbr_RRedef60 = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newGroupInGroup("pnd_Name_01_V_Bank_Aba_Acct_Nmbr_RRedef60", 
            "Redefines", pnd_Name_01_V_Bank_Aba_Acct_Nmbr_R);
        pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_R = pnd_Name_01_V_Bank_Aba_Acct_Nmbr_RRedef60.newFieldInGroup("pnd_Name_01_V_Bank_Aba_Acct_Nmbr_N_R", "V-BANK-ABA-ACCT-NMBR-N-R", 
            FieldType.NUMERIC, 9);
        pnd_Name_01_V_Eft_Status_Ind_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Checking_Saving_Cde_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Ph_Bank_Pymnt_Acct_Nmbr_R", 
            "V-PH-BANK-PYMNT-ACCT-NMBR-R", FieldType.STRING, 21);
        pnd_Name_01_V_Pending_Addrss_Chnge_Dte_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Pending_Addrss_Chnge_Dte_R", 
            "V-PENDING-ADDRSS-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_01_V_Pending_Addrss_Restore_Dte_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Pending_Addrss_Restore_Dte_R", 
            "V-PENDING-ADDRSS-RESTORE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_01_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Pending_Perm_Add_Chnge_Dte_R", 
            "V-PENDING-PERM-ADD-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_01_V_Correspondence_Addrss_Ind_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Correspondence_Addrss_Ind_R", 
            "V-CORRESPONDENCE-ADDRSS-IND-R", FieldType.STRING, 1);
        pnd_Name_01_V_Addrss_Geographic_Cde_R = pnd_Name_01_Pnd_V_Rest_Of_Record_RRedef59.newFieldInGroup("pnd_Name_01_V_Addrss_Geographic_Cde_R", "V-ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Name_01_Global_Indicator = pnd_Name_01.newFieldInGroup("pnd_Name_01_Global_Indicator", "GLOBAL-INDICATOR", FieldType.STRING, 1);
        pnd_Name_01_Global_Country = pnd_Name_01.newFieldInGroup("pnd_Name_01_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Name_01_Legal_State = pnd_Name_01.newFieldInGroup("pnd_Name_01_Legal_State", "LEGAL-STATE", FieldType.STRING, 2);

        pnd_Name_Save = newGroupInRecord("pnd_Name_Save", "#NAME-SAVE");
        pnd_Name_Save_Ph_Unque_Id_Nmbr = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Save_Cntrct_Payee = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Name_Save_Cntrct_PayeeRedef61 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Cntrct_PayeeRedef61", "Redefines", pnd_Name_Save_Cntrct_Payee);
        pnd_Name_Save_Cntrct_Nmbr = pnd_Name_Save_Cntrct_PayeeRedef61.newFieldInGroup("pnd_Name_Save_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Save_Cntrct_Payee_Cde = pnd_Name_Save_Cntrct_PayeeRedef61.newFieldInGroup("pnd_Name_Save_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Name_Save_Pnd_Cntrct_Name_Add_K = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 
            245);
        pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62", "Redefines", pnd_Name_Save_Pnd_Cntrct_Name_Add_K);
        pnd_Name_Save_Cntrct_Name_Free_K = pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62.newFieldInGroup("pnd_Name_Save_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_Addrss_Lne_1_K = pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62.newFieldInGroup("pnd_Name_Save_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_2_K = pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62.newFieldInGroup("pnd_Name_Save_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_3_K = pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62.newFieldInGroup("pnd_Name_Save_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_4_K = pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62.newFieldInGroup("pnd_Name_Save_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_5_K = pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62.newFieldInGroup("pnd_Name_Save_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_6_K = pnd_Name_Save_Pnd_Cntrct_Name_Add_KRedef62.newFieldInGroup("pnd_Name_Save_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Postal_Data_K = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 
            32);
        pnd_Name_Save_Addrss_Postal_Data_KRedef63 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Addrss_Postal_Data_KRedef63", "Redefines", pnd_Name_Save_Addrss_Postal_Data_K);
        pnd_Name_Save_Addrss_Zip_Plus_4_K = pnd_Name_Save_Addrss_Postal_Data_KRedef63.newFieldInGroup("pnd_Name_Save_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", 
            FieldType.STRING, 9);
        pnd_Name_Save_Addrss_Carrier_Rte_K = pnd_Name_Save_Addrss_Postal_Data_KRedef63.newFieldInGroup("pnd_Name_Save_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_Save_Addrss_Walk_Rte_K = pnd_Name_Save_Addrss_Postal_Data_KRedef63.newFieldInGroup("pnd_Name_Save_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_Save_Addrss_Usps_Future_Use_K = pnd_Name_Save_Addrss_Postal_Data_KRedef63.newFieldInGroup("pnd_Name_Save_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_Save_Pnd_Rest_Of_Record_K = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 
            148);
        pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64", "Redefines", pnd_Name_Save_Pnd_Rest_Of_Record_K);
        pnd_Name_Save_Addrss_Type_Cde_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_Save_Addrss_Last_Chnge_Dte_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Save_Addrss_Last_Chnge_Time_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_Save_Permanent_Addrss_Ind_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Save_Bank_Aba_Acct_Nmbr_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Name_Save_Bank_Aba_Acct_Nmbr_KRedef65 = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newGroupInGroup("pnd_Name_Save_Bank_Aba_Acct_Nmbr_KRedef65", 
            "Redefines", pnd_Name_Save_Bank_Aba_Acct_Nmbr_K);
        pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_K = pnd_Name_Save_Bank_Aba_Acct_Nmbr_KRedef65.newFieldInGroup("pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_K", "BANK-ABA-ACCT-NMBR-N-K", 
            FieldType.NUMERIC, 9);
        pnd_Name_Save_Eft_Status_Ind_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Eft_Status_Ind_K", "EFT-STATUS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Save_Checking_Saving_Cde_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_K", 
            "PH-BANK-PYMNT-ACCT-NMBR-K", FieldType.STRING, 21);
        pnd_Name_Save_Pending_Addrss_Chnge_Dte_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Pending_Addrss_Chnge_Dte_K", 
            "PENDING-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_Save_Pending_Addrss_Restore_Dte_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Pending_Addrss_Restore_Dte_K", 
            "PENDING-ADDRSS-RESTORE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_K", 
            "PENDING-PERM-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_Save_Check_Mailing_Addrss_Ind_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Check_Mailing_Addrss_Ind_K", 
            "CHECK-MAILING-ADDRSS-IND-K", FieldType.STRING, 1);
        pnd_Name_Save_Addrss_Geographic_Cde_K = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Name_Save_Intl_Eft_Pay_Type_Cde = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Save_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Save_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Save_Pnd_Rest_Of_Record_KRedef64.newFieldInGroup("pnd_Name_Save_Intl_Bank_Pymnt_Acct_Nmbr", 
            "INTL-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 35);
        pnd_Name_Save_Pnd_Cntrct_Name_Add_R = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 
            245);
        pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66", "Redefines", pnd_Name_Save_Pnd_Cntrct_Name_Add_R);
        pnd_Name_Save_Cntrct_Name_Free_R = pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66.newFieldInGroup("pnd_Name_Save_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_Addrss_Lne_1_R = pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66.newFieldInGroup("pnd_Name_Save_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_2_R = pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66.newFieldInGroup("pnd_Name_Save_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_3_R = pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66.newFieldInGroup("pnd_Name_Save_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_4_R = pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66.newFieldInGroup("pnd_Name_Save_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_5_R = pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66.newFieldInGroup("pnd_Name_Save_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Lne_6_R = pnd_Name_Save_Pnd_Cntrct_Name_Add_RRedef66.newFieldInGroup("pnd_Name_Save_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 
            35);
        pnd_Name_Save_Addrss_Postal_Data_R = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 
            32);
        pnd_Name_Save_Addrss_Postal_Data_RRedef67 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Addrss_Postal_Data_RRedef67", "Redefines", pnd_Name_Save_Addrss_Postal_Data_R);
        pnd_Name_Save_Addrss_Zip_Plus_4_R = pnd_Name_Save_Addrss_Postal_Data_RRedef67.newFieldInGroup("pnd_Name_Save_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", 
            FieldType.STRING, 9);
        pnd_Name_Save_Addrss_Carrier_Rte_R = pnd_Name_Save_Addrss_Postal_Data_RRedef67.newFieldInGroup("pnd_Name_Save_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_Save_Addrss_Walk_Rte_R = pnd_Name_Save_Addrss_Postal_Data_RRedef67.newFieldInGroup("pnd_Name_Save_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_Save_Addrss_Usps_Future_Use_R = pnd_Name_Save_Addrss_Postal_Data_RRedef67.newFieldInGroup("pnd_Name_Save_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_Save_Pnd_Rest_Of_Record_R = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 
            76);
        pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68", "Redefines", pnd_Name_Save_Pnd_Rest_Of_Record_R);
        pnd_Name_Save_Addrss_Type_Cde_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_Save_Addrss_Last_Chnge_Dte_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Save_Addrss_Last_Chnge_Time_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_Save_Permanent_Addrss_Ind_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Save_Bank_Aba_Acct_Nmbr_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Name_Save_Bank_Aba_Acct_Nmbr_RRedef69 = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newGroupInGroup("pnd_Name_Save_Bank_Aba_Acct_Nmbr_RRedef69", 
            "Redefines", pnd_Name_Save_Bank_Aba_Acct_Nmbr_R);
        pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_R = pnd_Name_Save_Bank_Aba_Acct_Nmbr_RRedef69.newFieldInGroup("pnd_Name_Save_Bank_Aba_Acct_Nmbr_N_R", "BANK-ABA-ACCT-NMBR-N-R", 
            FieldType.NUMERIC, 9);
        pnd_Name_Save_Eft_Status_Ind_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Eft_Status_Ind_R", "EFT-STATUS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Save_Checking_Saving_Cde_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Ph_Bank_Pymnt_Acct_Nmbr_R", 
            "PH-BANK-PYMNT-ACCT-NMBR-R", FieldType.STRING, 21);
        pnd_Name_Save_Pending_Addrss_Chnge_Dte_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Pending_Addrss_Chnge_Dte_R", 
            "PENDING-ADDRSS-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_Save_Pending_Addrss_Restore_Dte_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Pending_Addrss_Restore_Dte_R", 
            "PENDING-ADDRSS-RESTORE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Pending_Perm_Addrss_Chnge_Dte_R", 
            "PENDING-PERM-ADDRSS-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_Save_Correspondence_Addrss_Ind_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Correspondence_Addrss_Ind_R", 
            "CORRESPONDENCE-ADDRSS-IND-R", FieldType.STRING, 1);
        pnd_Name_Save_Addrss_Geographic_Cde_R = pnd_Name_Save_Pnd_Rest_Of_Record_RRedef68.newFieldInGroup("pnd_Name_Save_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Name_Save_Pnd_V_Cntrct_Name_Add_K = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", FieldType.STRING, 
            245);
        pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70", "Redefines", pnd_Name_Save_Pnd_V_Cntrct_Name_Add_K);
        pnd_Name_Save_V_Cntrct_Name_Free_K = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70.newFieldInGroup("pnd_Name_Save_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_1_K = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_2_K = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_3_K = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_4_K = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_5_K = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_6_K = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_KRedef70.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Postal_Data_K = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", FieldType.STRING, 
            32);
        pnd_Name_Save_V_Addrss_Postal_Data_KRedef71 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_V_Addrss_Postal_Data_KRedef71", "Redefines", pnd_Name_Save_V_Addrss_Postal_Data_K);
        pnd_Name_Save_V_Addrss_Zip_Plus_4_K = pnd_Name_Save_V_Addrss_Postal_Data_KRedef71.newFieldInGroup("pnd_Name_Save_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", 
            FieldType.STRING, 9);
        pnd_Name_Save_V_Addrss_Carrier_Rte_K = pnd_Name_Save_V_Addrss_Postal_Data_KRedef71.newFieldInGroup("pnd_Name_Save_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_Save_V_Addrss_Walk_Rte_K = pnd_Name_Save_V_Addrss_Postal_Data_KRedef71.newFieldInGroup("pnd_Name_Save_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_Save_V_Addrss_Usps_Future_Use_K = pnd_Name_Save_V_Addrss_Postal_Data_KRedef71.newFieldInGroup("pnd_Name_Save_V_Addrss_Usps_Future_Use_K", 
            "V-ADDRSS-USPS-FUTURE-USE-K", FieldType.STRING, 15);
        pnd_Name_Save_Pnd_V_Rest_Of_Record_K = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 
            148);
        pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72", "Redefines", pnd_Name_Save_Pnd_V_Rest_Of_Record_K);
        pnd_Name_Save_V_Addrss_Type_Cde_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_Save_V_Addrss_Last_Chnge_Dte_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Addrss_Last_Chnge_Dte_K", 
            "V-ADDRSS-LAST-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Addrss_Last_Chnge_Time_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Addrss_Last_Chnge_Time_K", 
            "V-ADDRSS-LAST-CHNGE-TIME-K", FieldType.NUMERIC, 7);
        pnd_Name_Save_V_Permanent_Addrss_Ind_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Permanent_Addrss_Ind_K", 
            "V-PERMANENT-ADDRSS-IND-K", FieldType.STRING, 1);
        pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_KRedef73 = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newGroupInGroup("pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_KRedef73", 
            "Redefines", pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_K);
        pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_K = pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_KRedef73.newFieldInGroup("pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_K", 
            "V-BANK-ABA-ACCT-NMBR-N-K", FieldType.NUMERIC, 9);
        pnd_Name_Save_V_Eft_Status_Ind_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Save_V_Checking_Saving_Cde_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_K", 
            "V-PH-BANK-PYMNT-ACCT-NMBR-K", FieldType.STRING, 21);
        pnd_Name_Save_V_Pnding_Addrss_Chnge_Dte_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Pnding_Addrss_Chnge_Dte_K", 
            "V-PNDING-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Pnding_Addrss_Restore_Dte_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Pnding_Addrss_Restore_Dte_K", 
            "V-PNDING-ADDRSS-RESTORE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Pnding_Perm_Addrss_Chnge_Dte_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Pnding_Perm_Addrss_Chnge_Dte_K", 
            "V-PNDING-PERM-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Check_Mailing_Addrss_Ind_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Check_Mailing_Addrss_Ind_K", 
            "V-CHECK-MAILING-ADDRSS-IND-K", FieldType.STRING, 1);
        pnd_Name_Save_V_Addrss_Geographic_Cde_K = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Addrss_Geographic_Cde_K", 
            "V-ADDRSS-GEOGRAPHIC-CDE-K", FieldType.STRING, 2);
        pnd_Name_Save_V_Intl_Eft_Pay_Type_Cde = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Intl_Eft_Pay_Type_Cde", "V-INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Save_V_Iintl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Iintl_Bank_Pymnt_Eft_Nmbr", 
            "V-IINTL-BANK-PYMNT-EFT-NMBR", FieldType.STRING, 35);
        pnd_Name_Save_V_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Save_Pnd_V_Rest_Of_Record_KRedef72.newFieldInGroup("pnd_Name_Save_V_Intl_Bank_Pymnt_Acct_Nmbr", 
            "V-INTL-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 35);
        pnd_Name_Save_Pnd_V_Cntrct_Name_Add_R = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", FieldType.STRING, 
            245);
        pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74", "Redefines", pnd_Name_Save_Pnd_V_Cntrct_Name_Add_R);
        pnd_Name_Save_V_Cntrct_Name_Free_R = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74.newFieldInGroup("pnd_Name_Save_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_1_R = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_2_R = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_3_R = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_4_R = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_5_R = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Lne_6_R = pnd_Name_Save_Pnd_V_Cntrct_Name_Add_RRedef74.newFieldInGroup("pnd_Name_Save_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", 
            FieldType.STRING, 35);
        pnd_Name_Save_V_Addrss_Postal_Data_R = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", FieldType.STRING, 
            32);
        pnd_Name_Save_V_Addrss_Postal_Data_RRedef75 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_V_Addrss_Postal_Data_RRedef75", "Redefines", pnd_Name_Save_V_Addrss_Postal_Data_R);
        pnd_Name_Save_V_Addrss_Zip_Plus_4_R = pnd_Name_Save_V_Addrss_Postal_Data_RRedef75.newFieldInGroup("pnd_Name_Save_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", 
            FieldType.STRING, 9);
        pnd_Name_Save_V_Addrss_Carrier_Rte_R = pnd_Name_Save_V_Addrss_Postal_Data_RRedef75.newFieldInGroup("pnd_Name_Save_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_Save_V_Addrss_Walk_Rte_R = pnd_Name_Save_V_Addrss_Postal_Data_RRedef75.newFieldInGroup("pnd_Name_Save_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_Save_V_Addrss_Usps_Future_Use_R = pnd_Name_Save_V_Addrss_Postal_Data_RRedef75.newFieldInGroup("pnd_Name_Save_V_Addrss_Usps_Future_Use_R", 
            "V-ADDRSS-USPS-FUTURE-USE-R", FieldType.STRING, 15);
        pnd_Name_Save_Pnd_V_Rest_Of_Record_R = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 
            76);
        pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76 = pnd_Name_Save.newGroupInGroup("pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76", "Redefines", pnd_Name_Save_Pnd_V_Rest_Of_Record_R);
        pnd_Name_Save_V_Addrss_Type_Cde_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_Save_V_Addrss_Last_Chnge_Dte_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Addrss_Last_Chnge_Dte_R", 
            "V-ADDRSS-LAST-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Addrss_Last_Chnge_Time_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Addrss_Last_Chnge_Time_R", 
            "V-ADDRSS-LAST-CHNGE-TIME-R", FieldType.NUMERIC, 7);
        pnd_Name_Save_V_Permanent_Addrss_Ind_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Permanent_Addrss_Ind_R", 
            "V-PERMANENT-ADDRSS-IND-R", FieldType.STRING, 1);
        pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_RRedef77 = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newGroupInGroup("pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_RRedef77", 
            "Redefines", pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_R);
        pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_R = pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_RRedef77.newFieldInGroup("pnd_Name_Save_V_Bank_Aba_Acct_Nmbr_N_R", 
            "V-BANK-ABA-ACCT-NMBR-N-R", FieldType.NUMERIC, 9);
        pnd_Name_Save_V_Eft_Status_Ind_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Save_V_Checking_Saving_Cde_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Ph_Bank_Pymnt_Acct_Nmbr_R", 
            "V-PH-BANK-PYMNT-ACCT-NMBR-R", FieldType.STRING, 21);
        pnd_Name_Save_V_Pending_Addrss_Chnge_Dte_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Pending_Addrss_Chnge_Dte_R", 
            "V-PENDING-ADDRSS-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Pending_Addrss_Restore_Dte_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Pending_Addrss_Restore_Dte_R", 
            "V-PENDING-ADDRSS-RESTORE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Pending_Perm_Add_Chnge_Dte_R", 
            "V-PENDING-PERM-ADD-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Name_Save_V_Correspondence_Addrss_Ind_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Correspondence_Addrss_Ind_R", 
            "V-CORRESPONDENCE-ADDRSS-IND-R", FieldType.STRING, 1);
        pnd_Name_Save_V_Addrss_Geographic_Cde_R = pnd_Name_Save_Pnd_V_Rest_Of_Record_RRedef76.newFieldInGroup("pnd_Name_Save_V_Addrss_Geographic_Cde_R", 
            "V-ADDRSS-GEOGRAPHIC-CDE-R", FieldType.STRING, 2);
        pnd_Name_Save_Global_Indicator = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Global_Indicator", "GLOBAL-INDICATOR", FieldType.STRING, 1);
        pnd_Name_Save_Global_Country = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Name_Save_Legal_State = pnd_Name_Save.newFieldInGroup("pnd_Name_Save_Legal_State", "LEGAL-STATE", FieldType.STRING, 2);

        pnd_Name = newGroupInRecord("pnd_Name", "#NAME");
        pnd_Name_Ph_Unque_Id_Nmbr = pnd_Name.newFieldInGroup("pnd_Name_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Cntrct_Payee = pnd_Name.newFieldInGroup("pnd_Name_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Name_Cntrct_PayeeRedef78 = pnd_Name.newGroupInGroup("pnd_Name_Cntrct_PayeeRedef78", "Redefines", pnd_Name_Cntrct_Payee);
        pnd_Name_Cntrct_Nmbr = pnd_Name_Cntrct_PayeeRedef78.newFieldInGroup("pnd_Name_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Cntrct_Payee_Cde = pnd_Name_Cntrct_PayeeRedef78.newFieldInGroup("pnd_Name_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Name_Pnd_Cntrct_Name_Add_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 245);
        pnd_Name_Pnd_Cntrct_Name_Add_KRedef79 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_Cntrct_Name_Add_KRedef79", "Redefines", pnd_Name_Pnd_Cntrct_Name_Add_K);
        pnd_Name_Cntrct_Name_Free_K = pnd_Name_Pnd_Cntrct_Name_Add_KRedef79.newFieldInGroup("pnd_Name_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_1_K = pnd_Name_Pnd_Cntrct_Name_Add_KRedef79.newFieldInGroup("pnd_Name_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_2_K = pnd_Name_Pnd_Cntrct_Name_Add_KRedef79.newFieldInGroup("pnd_Name_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_3_K = pnd_Name_Pnd_Cntrct_Name_Add_KRedef79.newFieldInGroup("pnd_Name_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_4_K = pnd_Name_Pnd_Cntrct_Name_Add_KRedef79.newFieldInGroup("pnd_Name_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_5_K = pnd_Name_Pnd_Cntrct_Name_Add_KRedef79.newFieldInGroup("pnd_Name_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_6_K = pnd_Name_Pnd_Cntrct_Name_Add_KRedef79.newFieldInGroup("pnd_Name_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Postal_Data_K = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);
        pnd_Name_Addrss_Postal_Data_KRedef80 = pnd_Name.newGroupInGroup("pnd_Name_Addrss_Postal_Data_KRedef80", "Redefines", pnd_Name_Addrss_Postal_Data_K);
        pnd_Name_Addrss_Zip_Plus_4_K = pnd_Name_Addrss_Postal_Data_KRedef80.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 
            9);
        pnd_Name_Addrss_Carrier_Rte_K = pnd_Name_Addrss_Postal_Data_KRedef80.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_Addrss_Walk_Rte_K = pnd_Name_Addrss_Postal_Data_KRedef80.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Usps_Future_Use_K = pnd_Name_Addrss_Postal_Data_KRedef80.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_Rest_Of_Record_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 148);
        pnd_Name_Pnd_Rest_Of_Record_KRedef81 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_Rest_Of_Record_KRedef81", "Redefines", pnd_Name_Pnd_Rest_Of_Record_K);
        pnd_Name_Addrss_Type_Cde_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_Addrss_Last_Chnge_Dte_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Addrss_Last_Chnge_Time_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_Permanent_Addrss_Ind_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Bank_Aba_Acct_Nmbr_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Name_Bank_Aba_Acct_Nmbr_KRedef82 = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newGroupInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_KRedef82", "Redefines", 
            pnd_Name_Bank_Aba_Acct_Nmbr_K);
        pnd_Name_Bank_Aba_Acct_Nmbr_N_K = pnd_Name_Bank_Aba_Acct_Nmbr_KRedef82.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_N_K", "BANK-ABA-ACCT-NMBR-N-K", 
            FieldType.NUMERIC, 9);
        pnd_Name_Eft_Status_Ind_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Eft_Status_Ind_K", "EFT-STATUS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_Checking_Saving_Cde_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K", "PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Name_Pending_Addrss_Chnge_Dte_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_K", "PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_K", "PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K", "PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Mailing_Addrss_Ind_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Check_Mailing_Addrss_Ind_K", "CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_K = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Name_Intl_Eft_Pay_Type_Cde = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Pnd_Rest_Of_Record_KRedef81.newFieldInGroup("pnd_Name_Intl_Bank_Pymnt_Acct_Nmbr", "INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Pnd_Cntrct_Name_Add_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 245);
        pnd_Name_Pnd_Cntrct_Name_Add_RRedef83 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_Cntrct_Name_Add_RRedef83", "Redefines", pnd_Name_Pnd_Cntrct_Name_Add_R);
        pnd_Name_Cntrct_Name_Free_R = pnd_Name_Pnd_Cntrct_Name_Add_RRedef83.newFieldInGroup("pnd_Name_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_1_R = pnd_Name_Pnd_Cntrct_Name_Add_RRedef83.newFieldInGroup("pnd_Name_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_2_R = pnd_Name_Pnd_Cntrct_Name_Add_RRedef83.newFieldInGroup("pnd_Name_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_3_R = pnd_Name_Pnd_Cntrct_Name_Add_RRedef83.newFieldInGroup("pnd_Name_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_4_R = pnd_Name_Pnd_Cntrct_Name_Add_RRedef83.newFieldInGroup("pnd_Name_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_5_R = pnd_Name_Pnd_Cntrct_Name_Add_RRedef83.newFieldInGroup("pnd_Name_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Lne_6_R = pnd_Name_Pnd_Cntrct_Name_Add_RRedef83.newFieldInGroup("pnd_Name_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 
            35);
        pnd_Name_Addrss_Postal_Data_R = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);
        pnd_Name_Addrss_Postal_Data_RRedef84 = pnd_Name.newGroupInGroup("pnd_Name_Addrss_Postal_Data_RRedef84", "Redefines", pnd_Name_Addrss_Postal_Data_R);
        pnd_Name_Addrss_Zip_Plus_4_R = pnd_Name_Addrss_Postal_Data_RRedef84.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 
            9);
        pnd_Name_Addrss_Carrier_Rte_R = pnd_Name_Addrss_Postal_Data_RRedef84.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_Addrss_Walk_Rte_R = pnd_Name_Addrss_Postal_Data_RRedef84.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Usps_Future_Use_R = pnd_Name_Addrss_Postal_Data_RRedef84.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_Rest_Of_Record_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 76);
        pnd_Name_Pnd_Rest_Of_Record_RRedef85 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_Rest_Of_Record_RRedef85", "Redefines", pnd_Name_Pnd_Rest_Of_Record_R);
        pnd_Name_Addrss_Type_Cde_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_Addrss_Last_Chnge_Dte_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Addrss_Last_Chnge_Time_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_Permanent_Addrss_Ind_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Bank_Aba_Acct_Nmbr_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Name_Bank_Aba_Acct_Nmbr_RRedef86 = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newGroupInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_RRedef86", "Redefines", 
            pnd_Name_Bank_Aba_Acct_Nmbr_R);
        pnd_Name_Bank_Aba_Acct_Nmbr_N_R = pnd_Name_Bank_Aba_Acct_Nmbr_RRedef86.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_N_R", "BANK-ABA-ACCT-NMBR-N-R", 
            FieldType.NUMERIC, 9);
        pnd_Name_Eft_Status_Ind_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Eft_Status_Ind_R", "EFT-STATUS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_Checking_Saving_Cde_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R", "PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Name_Pending_Addrss_Chnge_Dte_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_R", "PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_R", "PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R", "PENDING-PERM-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Correspondence_Addrss_Ind_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Correspondence_Addrss_Ind_R", "CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_R = pnd_Name_Pnd_Rest_Of_Record_RRedef85.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Name_Pnd_V_Cntrct_Name_Add_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", FieldType.STRING, 245);
        pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87", "Redefines", pnd_Name_Pnd_V_Cntrct_Name_Add_K);
        pnd_Name_V_Cntrct_Name_Free_K = pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87.newFieldInGroup("pnd_Name_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", 
            FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_1_K = pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87.newFieldInGroup("pnd_Name_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_2_K = pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87.newFieldInGroup("pnd_Name_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_3_K = pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87.newFieldInGroup("pnd_Name_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_4_K = pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87.newFieldInGroup("pnd_Name_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_5_K = pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87.newFieldInGroup("pnd_Name_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_6_K = pnd_Name_Pnd_V_Cntrct_Name_Add_KRedef87.newFieldInGroup("pnd_Name_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Postal_Data_K = pnd_Name.newFieldInGroup("pnd_Name_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);
        pnd_Name_V_Addrss_Postal_Data_KRedef88 = pnd_Name.newGroupInGroup("pnd_Name_V_Addrss_Postal_Data_KRedef88", "Redefines", pnd_Name_V_Addrss_Postal_Data_K);
        pnd_Name_V_Addrss_Zip_Plus_4_K = pnd_Name_V_Addrss_Postal_Data_KRedef88.newFieldInGroup("pnd_Name_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", 
            FieldType.STRING, 9);
        pnd_Name_V_Addrss_Carrier_Rte_K = pnd_Name_V_Addrss_Postal_Data_KRedef88.newFieldInGroup("pnd_Name_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Name_V_Addrss_Walk_Rte_K = pnd_Name_V_Addrss_Postal_Data_KRedef88.newFieldInGroup("pnd_Name_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_V_Addrss_Usps_Future_Use_K = pnd_Name_V_Addrss_Postal_Data_KRedef88.newFieldInGroup("pnd_Name_V_Addrss_Usps_Future_Use_K", "V-ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_V_Rest_Of_Record_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 148);
        pnd_Name_Pnd_V_Rest_Of_Record_KRedef89 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_V_Rest_Of_Record_KRedef89", "Redefines", pnd_Name_Pnd_V_Rest_Of_Record_K);
        pnd_Name_V_Addrss_Type_Cde_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_V_Addrss_Last_Chnge_Dte_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Dte_K", "V-ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Addrss_Last_Chnge_Time_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Time_K", "V-ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_V_Permanent_Addrss_Ind_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Permanent_Addrss_Ind_K", "V-PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_KRedef90 = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newGroupInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_KRedef90", "Redefines", 
            pnd_Name_V_Bank_Aba_Acct_Nmbr_K);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_N_K = pnd_Name_V_Bank_Aba_Acct_Nmbr_KRedef90.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_N_K", "V-BANK-ABA-ACCT-NMBR-N-K", 
            FieldType.NUMERIC, 9);
        pnd_Name_V_Eft_Status_Ind_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_V_Checking_Saving_Cde_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K", "V-PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Name_V_Pnding_Addrss_Chnge_Dte_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Pnding_Addrss_Chnge_Dte_K", "V-PNDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pnding_Addrss_Restore_Dte_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Pnding_Addrss_Restore_Dte_K", "V-PNDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pnding_Perm_Addrss_Chnge_Dte_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Pnding_Perm_Addrss_Chnge_Dte_K", 
            "V-PNDING-PERM-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Name_V_Check_Mailing_Addrss_Ind_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Check_Mailing_Addrss_Ind_K", "V-CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_V_Addrss_Geographic_Cde_K = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Addrss_Geographic_Cde_K", "V-ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Name_V_Intl_Eft_Pay_Type_Cde = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Intl_Eft_Pay_Type_Cde", "V-INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr", "V-IINTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_V_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Pnd_V_Rest_Of_Record_KRedef89.newFieldInGroup("pnd_Name_V_Intl_Bank_Pymnt_Acct_Nmbr", "V-INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Pnd_V_Cntrct_Name_Add_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", FieldType.STRING, 245);
        pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91", "Redefines", pnd_Name_Pnd_V_Cntrct_Name_Add_R);
        pnd_Name_V_Cntrct_Name_Free_R = pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91.newFieldInGroup("pnd_Name_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", 
            FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_1_R = pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91.newFieldInGroup("pnd_Name_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_2_R = pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91.newFieldInGroup("pnd_Name_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_3_R = pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91.newFieldInGroup("pnd_Name_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_4_R = pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91.newFieldInGroup("pnd_Name_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_5_R = pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91.newFieldInGroup("pnd_Name_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_6_R = pnd_Name_Pnd_V_Cntrct_Name_Add_RRedef91.newFieldInGroup("pnd_Name_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Postal_Data_R = pnd_Name.newFieldInGroup("pnd_Name_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);
        pnd_Name_V_Addrss_Postal_Data_RRedef92 = pnd_Name.newGroupInGroup("pnd_Name_V_Addrss_Postal_Data_RRedef92", "Redefines", pnd_Name_V_Addrss_Postal_Data_R);
        pnd_Name_V_Addrss_Zip_Plus_4_R = pnd_Name_V_Addrss_Postal_Data_RRedef92.newFieldInGroup("pnd_Name_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", 
            FieldType.STRING, 9);
        pnd_Name_V_Addrss_Carrier_Rte_R = pnd_Name_V_Addrss_Postal_Data_RRedef92.newFieldInGroup("pnd_Name_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Name_V_Addrss_Walk_Rte_R = pnd_Name_V_Addrss_Postal_Data_RRedef92.newFieldInGroup("pnd_Name_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_V_Addrss_Usps_Future_Use_R = pnd_Name_V_Addrss_Postal_Data_RRedef92.newFieldInGroup("pnd_Name_V_Addrss_Usps_Future_Use_R", "V-ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_V_Rest_Of_Record_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 76);
        pnd_Name_Pnd_V_Rest_Of_Record_RRedef93 = pnd_Name.newGroupInGroup("pnd_Name_Pnd_V_Rest_Of_Record_RRedef93", "Redefines", pnd_Name_Pnd_V_Rest_Of_Record_R);
        pnd_Name_V_Addrss_Type_Cde_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_V_Addrss_Last_Chnge_Dte_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Dte_R", "V-ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Addrss_Last_Chnge_Time_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Time_R", "V-ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_V_Permanent_Addrss_Ind_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Permanent_Addrss_Ind_R", "V-PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_RRedef94 = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newGroupInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_RRedef94", "Redefines", 
            pnd_Name_V_Bank_Aba_Acct_Nmbr_R);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_N_R = pnd_Name_V_Bank_Aba_Acct_Nmbr_RRedef94.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_N_R", "V-BANK-ABA-ACCT-NMBR-N-R", 
            FieldType.NUMERIC, 9);
        pnd_Name_V_Eft_Status_Ind_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_V_Checking_Saving_Cde_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R", "V-PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Name_V_Pending_Addrss_Chnge_Dte_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Pending_Addrss_Chnge_Dte_R", "V-PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Addrss_Restore_Dte_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Pending_Addrss_Restore_Dte_R", "V-PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R", "V-PENDING-PERM-ADD-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Correspondence_Addrss_Ind_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Correspondence_Addrss_Ind_R", "V-CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_V_Addrss_Geographic_Cde_R = pnd_Name_Pnd_V_Rest_Of_Record_RRedef93.newFieldInGroup("pnd_Name_V_Addrss_Geographic_Cde_R", "V-ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Name_Global_Indicator = pnd_Name.newFieldInGroup("pnd_Name_Global_Indicator", "GLOBAL-INDICATOR", FieldType.STRING, 1);
        pnd_Name_Global_Country = pnd_Name.newFieldInGroup("pnd_Name_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Name_Legal_State = pnd_Name.newFieldInGroup("pnd_Name_Legal_State", "LEGAL-STATE", FieldType.STRING, 2);

        this.setRecordName("GdaIaag700");
    }
    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    private GdaIaag700() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaIaag700 getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaIaag700>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaIaag700());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

