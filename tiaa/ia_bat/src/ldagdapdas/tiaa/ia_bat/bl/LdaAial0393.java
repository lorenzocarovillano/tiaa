/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:32 PM
**        * FROM NATURAL LDA     : AIAL0393
************************************************************
**        * FILE NAME            : LdaAial0393.java
**        * CLASS NAME           : LdaAial0393
**        * INSTANCE NAME        : LdaAial0393
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0393 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd_1_View;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde;
    private DbsGroup iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_CdeRedef1;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp;
    private DbsGroup iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_Out_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Fund_Key;

    public DataAccessProgramView getVw_iaa_Cref_Fund_Rcrd_1_View() { return vw_iaa_Cref_Fund_Rcrd_1_View; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_CdeRedef1() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_CdeRedef1; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Tot_Per_Amt() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Tot_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Unit_Val() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Old_Per_Amt() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Old_Unit_Val() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Cde() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Dte() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte() { return iaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Xfr_Iss_Dte() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Xfr_Iss_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_In_Dte() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_Out_Dte() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Fund_Key() { return iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Fund_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_iaa_Cref_Fund_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd_1_View", "IAA-CREF-FUND-RCRD-1-VIEW"), "IAA_CREF_FUND_RCRD_1", 
            "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr", 
            "CREF-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde", 
            "CREF-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_CdeRedef1 = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_CdeRedef1", 
            "Redefines", iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde", 
            "CREF-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde", 
            "CREF-FUND-CDE", FieldType.STRING, 2);
        iaa_Cref_Fund_Rcrd_1_View_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Tot_Per_Amt", 
            "CREF-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Unit_Val = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Unit_Val", 
            "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Per_Amt = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Per_Amt", 
            "CREF-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Unit_Val = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Old_Unit_Val", 
            "CREF-OLD-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp", 
            "CREF-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Cde", 
            "CREF-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Dte = iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Dte", 
            "CREF-RATE-DTE", FieldType.DATE, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt", 
            "CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte", 
            "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Xfr_Iss_Dte", 
            "CREF-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_In_Dte", 
            "CREF-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Lst_Xfr_Out_Dte", 
            "CREF-LST-XFR-OUT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Fund_Key = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Fund_Key", 
            "CREF-CNTRCT-FUND-KEY", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");
        vw_iaa_Cref_Fund_Rcrd_1_View.setUniquePeList();

        this.setRecordName("LdaAial0393");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cref_Fund_Rcrd_1_View.reset();
    }

    // Constructor
    public LdaAial0393() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
