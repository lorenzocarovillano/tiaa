/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:04:27 PM
**        * FROM NATURAL LDA     : IAALPSGM
************************************************************
**        * FILE NAME            : LdaIaalpsgm.java
**        * CLASS NAME           : LdaIaalpsgm
**        * INSTANCE NAME        : LdaIaalpsgm
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaalpsgm extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Psgm001_Common_Area;
    private DbsGroup pnd_Psgm001_Common_Area_Pnd_Psgm001_Input;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Function;
    private DbsGroup pnd_Psgm001_Common_Area_Pnd_In_Data;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Type;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn;
    private DbsGroup pnd_Psgm001_Common_Area_Pnd_Rt_Area;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code;
    private DbsGroup pnd_Psgm001_Common_Area_Pnd_Rt_Record;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Input_String;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Pref;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_First_Name;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_City;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Country;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status;

    public DbsGroup getPnd_Psgm001_Common_Area() { return pnd_Psgm001_Common_Area; }

    public DbsGroup getPnd_Psgm001_Common_Area_Pnd_Psgm001_Input() { return pnd_Psgm001_Common_Area_Pnd_Psgm001_Input; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_In_Function() { return pnd_Psgm001_Common_Area_Pnd_In_Function; }

    public DbsGroup getPnd_Psgm001_Common_Area_Pnd_In_Data() { return pnd_Psgm001_Common_Area_Pnd_In_Data; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ() { return pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_In_Type() { return pnd_Psgm001_Common_Area_Pnd_In_Type; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn() { return pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn; }

    public DbsGroup getPnd_Psgm001_Common_Area_Pnd_Rt_Area() { return pnd_Psgm001_Common_Area_Pnd_Rt_Area; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code() { return pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code; }

    public DbsGroup getPnd_Psgm001_Common_Area_Pnd_Rt_Record() { return pnd_Psgm001_Common_Area_Pnd_Rt_Record; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Input_String() { return pnd_Psgm001_Common_Area_Pnd_Rt_Input_String; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin() { return pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn() { return pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date() { return pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date() { return pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date() { return pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code() { return pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Pref() { return pnd_Psgm001_Common_Area_Pnd_Rt_Pref; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name() { return pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name() { return pnd_Psgm001_Common_Area_Pnd_Rt_First_Name; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name() { return pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc() { return pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd() { return pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_1() { return pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_2() { return pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_3() { return pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_4() { return pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_5() { return pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_City() { return pnd_Psgm001_Common_Area_Pnd_Rt_City; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code() { return pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code() { return pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Country() { return pnd_Psgm001_Common_Area_Pnd_Rt_Country; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code() { return pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm() { return pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_State_Nm() { return pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code() { return pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd() { return pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd; }

    public DbsField getPnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status() { return pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Psgm001_Common_Area = newGroupInRecord("pnd_Psgm001_Common_Area", "#PSGM001-COMMON-AREA");
        pnd_Psgm001_Common_Area_Pnd_Psgm001_Input = pnd_Psgm001_Common_Area.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_Psgm001_Input", "#PSGM001-INPUT");
        pnd_Psgm001_Common_Area_Pnd_In_Function = pnd_Psgm001_Common_Area_Pnd_Psgm001_Input.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Function", 
            "#IN-FUNCTION", FieldType.STRING, 2);
        pnd_Psgm001_Common_Area_Pnd_In_Data = pnd_Psgm001_Common_Area_Pnd_Psgm001_Input.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_In_Data", "#IN-DATA");
        pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ = pnd_Psgm001_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ", 
            "#IN-ADDR-USG-TYP", FieldType.STRING, 2);
        pnd_Psgm001_Common_Area_Pnd_In_Type = pnd_Psgm001_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Type", "#IN-TYPE", FieldType.STRING, 
            3);
        pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn = pnd_Psgm001_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn", "#IN-PIN-SSN", 
            FieldType.STRING, 15);
        pnd_Psgm001_Common_Area_Pnd_Rt_Area = pnd_Psgm001_Common_Area.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Area", "#RT-AREA");
        pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Area.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code", "#RT-RET-CODE", 
            FieldType.STRING, 2);
        pnd_Psgm001_Common_Area_Pnd_Rt_Record = pnd_Psgm001_Common_Area_Pnd_Rt_Area.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Record", "#RT-RECORD");
        pnd_Psgm001_Common_Area_Pnd_Rt_Input_String = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Input_String", 
            "#RT-INPUT-STRING", FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin", "#RT-OUT-PIN", 
            FieldType.STRING, 25);
        pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn", "#RT-OUT-SSN", 
            FieldType.STRING, 25);
        pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date", 
            "#RT-INACT-DATE", FieldType.STRING, 8);
        pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date", 
            "#RT-BIRTH-DATE", FieldType.STRING, 8);
        pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date", "#RT-DEC-DATE", 
            FieldType.STRING, 8);
        pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code", 
            "#RT-GENDER-CODE", FieldType.STRING, 1);
        pnd_Psgm001_Common_Area_Pnd_Rt_Pref = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Pref", "#RT-PREF", 
            FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name", "#RT-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Psgm001_Common_Area_Pnd_Rt_First_Name = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_First_Name", 
            "#RT-FIRST-NAME", FieldType.STRING, 30);
        pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name", 
            "#RT-SECOND-NAME", FieldType.STRING, 30);
        pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc", 
            "#RT-SUFFIX-DESC", FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd", 
            "#RT-PERSON-ORG-CD", FieldType.STRING, 1);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1", "#RT-ADDR-1", 
            FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2", "#RT-ADDR-2", 
            FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3", "#RT-ADDR-3", 
            FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4", "#RT-ADDR-4", 
            FieldType.STRING, 250);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5", "#RT-ADDR-5", 
            FieldType.STRING, 250);
        pnd_Psgm001_Common_Area_Pnd_Rt_City = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_City", "#RT-CITY", 
            FieldType.STRING, 50);
        pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code", 
            "#RT-POSTAL-CODE", FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code", "#RT-ISO-CODE", 
            FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Country = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Country", "#RT-COUNTRY", 
            FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code", "#RT-IRS-CODE", 
            FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm", 
            "#RT-IRS-COUNTRY-NM", FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm", "#RT-STATE-NM", 
            FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code", "#RT-GEO-CODE", 
            FieldType.STRING, 2);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd", 
            "#RT-ADDR-TYPE-CD", FieldType.STRING, 1);
        pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status", 
            "#RT-RCRD-STATUS", FieldType.STRING, 1);

        this.setRecordName("LdaIaalpsgm");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaIaalpsgm() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
