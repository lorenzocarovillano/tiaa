/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:03:51 PM
**        * FROM NATURAL LDA     : IAAL950
************************************************************
**        * FILE NAME            : LdaIaal950.java
**        * CLASS NAME           : LdaIaal950
**        * INSTANCE NAME        : LdaIaal950
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaIaal950 extends DbsRecord
{
    // Properties
    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Check_Dte;
    private DbsGroup iaa_Parm_Card_Pnd_Check_DteRedef1;
    private DbsField iaa_Parm_Card_Pnd_Check_Dte_N;
    private DbsGroup iaa_Parm_Card_Pnd_Check_DteRedef2;
    private DbsField iaa_Parm_Card_Pnd_Check_Dte_Mm;
    private DbsField iaa_Parm_Card_Pnd_Check_Dte_Dd;
    private DbsField iaa_Parm_Card_Pnd_Check_Dte_Yy;
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField pnd_Final_Tot;
    private DbsField pnd_Cntrl_Check_Dte;
    private DbsField pnd_Parm_Check_Dte_D;
    private DbsField pnd_Parm_Check_Dte_N;
    private DbsGroup pnd_Parm_Check_Dte_NRedef3;
    private DbsField pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Cc;
    private DbsField pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Yy;
    private DbsField pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Mm;
    private DbsField pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Dd;
    private DbsField pnd_Save_Trans_Ppcn_Nbr;
    private DbsField pnd_Save_Trans_Payee_Cde;
    private DbsField pnd_Save_Trans_Cde;
    private DbsField pnd_Cntrl_Frst_Trans_Dte_A;
    private DbsField pnd_Cntrl_Frst_Trans_Dte_T;
    private DbsField pnd_Cntrl_Rcrd_Key;
    private DbsGroup pnd_Cntrl_Rcrd_KeyRedef4;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Last_Batch_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key;
    private DbsGroup pnd_Iaa_Cntrct_Prtcpnt_KeyRedef5;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Trans_Key;
    private DbsGroup pnd_Iaa_Cpr_Trans_KeyRedef6;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte;
    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_1st_Grp;
    private DbsField pnd_Logical_Variables_Pnd_Records_Processed;
    private DbsGroup pnd_Packed_Variables;
    private DbsField pnd_Packed_Variables_Pnd_Records_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Records_Processed_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr;

    public DbsGroup getIaa_Parm_Card() { return iaa_Parm_Card; }

    public DbsField getIaa_Parm_Card_Pnd_Check_Dte() { return iaa_Parm_Card_Pnd_Check_Dte; }

    public DbsGroup getIaa_Parm_Card_Pnd_Check_DteRedef1() { return iaa_Parm_Card_Pnd_Check_DteRedef1; }

    public DbsField getIaa_Parm_Card_Pnd_Check_Dte_N() { return iaa_Parm_Card_Pnd_Check_Dte_N; }

    public DbsGroup getIaa_Parm_Card_Pnd_Check_DteRedef2() { return iaa_Parm_Card_Pnd_Check_DteRedef2; }

    public DbsField getIaa_Parm_Card_Pnd_Check_Dte_Mm() { return iaa_Parm_Card_Pnd_Check_Dte_Mm; }

    public DbsField getIaa_Parm_Card_Pnd_Check_Dte_Dd() { return iaa_Parm_Card_Pnd_Check_Dte_Dd; }

    public DbsField getIaa_Parm_Card_Pnd_Check_Dte_Yy() { return iaa_Parm_Card_Pnd_Check_Dte_Yy; }

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd() { return vw_iaa_Cntrl_Rcrd; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Check_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte() { return iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte; }

    public DataAccessProgramView getVw_iaa_Trans_Rcrd() { return vw_iaa_Trans_Rcrd; }

    public DbsField getIaa_Trans_Rcrd_Trans_Dte() { return iaa_Trans_Rcrd_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Invrse_Trans_Dte() { return iaa_Trans_Rcrd_Invrse_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Lst_Trans_Dte() { return iaa_Trans_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Ppcn_Nbr() { return iaa_Trans_Rcrd_Trans_Ppcn_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Payee_Cde() { return iaa_Trans_Rcrd_Trans_Payee_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Sub_Cde() { return iaa_Trans_Rcrd_Trans_Sub_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cde() { return iaa_Trans_Rcrd_Trans_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Actvty_Cde() { return iaa_Trans_Rcrd_Trans_Actvty_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Check_Dte() { return iaa_Trans_Rcrd_Trans_Check_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Todays_Dte() { return iaa_Trans_Rcrd_Trans_Todays_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Area() { return iaa_Trans_Rcrd_Trans_User_Area; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Id() { return iaa_Trans_Rcrd_Trans_User_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Cde() { return iaa_Trans_Rcrd_Trans_Verify_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Id() { return iaa_Trans_Rcrd_Trans_Verify_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cmbne_Cde() { return iaa_Trans_Rcrd_Trans_Cmbne_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Wpid() { return iaa_Trans_Rcrd_Trans_Cwf_Wpid; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Id_Nbr() { return iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Trans_Dte() { return iaa_Cpr_Trans_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Invrse_Trans_Dte() { return iaa_Cpr_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Lst_Trans_Dte() { return iaa_Cpr_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_Cpr_Id_Nbr() { return iaa_Cpr_Trans_Cpr_Id_Nbr; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde() { return iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde() { return iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw() { return iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr() { return iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ() { return iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Actvty_Cde() { return iaa_Cpr_Trans_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Trmnte_Rsn() { return iaa_Cpr_Trans_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rwrttn_Ind() { return iaa_Cpr_Trans_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Cash_Cde() { return iaa_Cpr_Trans_Cntrct_Cash_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getIaa_Cpr_Trans_Cntrct_Company_Data() { return iaa_Cpr_Trans_Cntrct_Company_Data; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Company_Cd() { return iaa_Cpr_Trans_Cntrct_Company_Cd; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind() { return iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Per_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Ivc_Used_Amt() { return iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rtb_Amt() { return iaa_Cpr_Trans_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rtb_Percent() { return iaa_Cpr_Trans_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Mode_Ind() { return iaa_Cpr_Trans_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Wthdrwl_Dte() { return iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Dod_Dte() { return iaa_Cpr_Trans_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Cde() { return iaa_Cpr_Trans_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Hold_Cde() { return iaa_Cpr_Trans_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte() { return iaa_Cpr_Trans_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Prev_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Curr_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Cmbne_Cde() { return iaa_Cpr_Trans_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Cde() { return iaa_Cpr_Trans_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Amt() { return iaa_Cpr_Trans_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Srce() { return iaa_Cpr_Trans_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte() { return iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte() { return iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Fed_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_State_Cde() { return iaa_Cpr_Trans_Cntrct_State_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_State_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Local_Cde() { return iaa_Cpr_Trans_Cntrct_Local_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Local_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte() { return iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte; }

    public DbsField getIaa_Cpr_Trans_Trans_Check_Dte() { return iaa_Cpr_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cpr_Trans_Bfre_Imge_Id() { return iaa_Cpr_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Aftr_Imge_Id() { return iaa_Cpr_Trans_Aftr_Imge_Id; }

    public DbsField getPnd_Final_Tot() { return pnd_Final_Tot; }

    public DbsField getPnd_Cntrl_Check_Dte() { return pnd_Cntrl_Check_Dte; }

    public DbsField getPnd_Parm_Check_Dte_D() { return pnd_Parm_Check_Dte_D; }

    public DbsField getPnd_Parm_Check_Dte_N() { return pnd_Parm_Check_Dte_N; }

    public DbsGroup getPnd_Parm_Check_Dte_NRedef3() { return pnd_Parm_Check_Dte_NRedef3; }

    public DbsField getPnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Cc() { return pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Cc; }

    public DbsField getPnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Yy() { return pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Yy; }

    public DbsField getPnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Mm() { return pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Mm; }

    public DbsField getPnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Dd() { return pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Dd; }

    public DbsField getPnd_Save_Trans_Ppcn_Nbr() { return pnd_Save_Trans_Ppcn_Nbr; }

    public DbsField getPnd_Save_Trans_Payee_Cde() { return pnd_Save_Trans_Payee_Cde; }

    public DbsField getPnd_Save_Trans_Cde() { return pnd_Save_Trans_Cde; }

    public DbsField getPnd_Cntrl_Frst_Trans_Dte_A() { return pnd_Cntrl_Frst_Trans_Dte_A; }

    public DbsField getPnd_Cntrl_Frst_Trans_Dte_T() { return pnd_Cntrl_Frst_Trans_Dte_T; }

    public DbsField getPnd_Cntrl_Rcrd_Key() { return pnd_Cntrl_Rcrd_Key; }

    public DbsGroup getPnd_Cntrl_Rcrd_KeyRedef4() { return pnd_Cntrl_Rcrd_KeyRedef4; }

    public DbsField getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde() { return pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde; }

    public DbsField getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte() { return pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte; }

    public DbsField getPnd_Last_Batch_Nbr() { return pnd_Last_Batch_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key() { return pnd_Iaa_Cntrct_Prtcpnt_Key; }

    public DbsGroup getPnd_Iaa_Cntrct_Prtcpnt_KeyRedef5() { return pnd_Iaa_Cntrct_Prtcpnt_KeyRedef5; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key() { return pnd_Iaa_Cpr_Trans_Key; }

    public DbsGroup getPnd_Iaa_Cpr_Trans_KeyRedef6() { return pnd_Iaa_Cpr_Trans_KeyRedef6; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde; }

    public DbsField getPnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte() { return pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte; }

    public DbsGroup getPnd_Logical_Variables() { return pnd_Logical_Variables; }

    public DbsField getPnd_Logical_Variables_Pnd_1st_Grp() { return pnd_Logical_Variables_Pnd_1st_Grp; }

    public DbsField getPnd_Logical_Variables_Pnd_Records_Processed() { return pnd_Logical_Variables_Pnd_Records_Processed; }

    public DbsGroup getPnd_Packed_Variables() { return pnd_Packed_Variables; }

    public DbsField getPnd_Packed_Variables_Pnd_Records_Ctr() { return pnd_Packed_Variables_Pnd_Records_Ctr; }

    public DbsField getPnd_Packed_Variables_Pnd_Records_Processed_Ctr() { return pnd_Packed_Variables_Pnd_Records_Processed_Ctr; }

    public DbsField getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr() { return pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        iaa_Parm_Card = newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Check_Dte = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 6);
        iaa_Parm_Card_Pnd_Check_DteRedef1 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Check_DteRedef1", "Redefines", iaa_Parm_Card_Pnd_Check_Dte);
        iaa_Parm_Card_Pnd_Check_Dte_N = iaa_Parm_Card_Pnd_Check_DteRedef1.newFieldInGroup("iaa_Parm_Card_Pnd_Check_Dte_N", "#CHECK-DTE-N", FieldType.NUMERIC, 
            6);
        iaa_Parm_Card_Pnd_Check_DteRedef2 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card_Pnd_Check_DteRedef2", "Redefines", iaa_Parm_Card_Pnd_Check_Dte);
        iaa_Parm_Card_Pnd_Check_Dte_Mm = iaa_Parm_Card_Pnd_Check_DteRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Check_Dte_Mm", "#CHECK-DTE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Check_Dte_Dd = iaa_Parm_Card_Pnd_Check_DteRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Check_Dte_Dd", "#CHECK-DTE-DD", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Check_Dte_Yy = iaa_Parm_Card_Pnd_Check_DteRedef2.newFieldInGroup("iaa_Parm_Card_Pnd_Check_Dte_Yy", "#CHECK-DTE-YY", FieldType.NUMERIC, 
            2);

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");

        pnd_Final_Tot = newFieldInRecord("pnd_Final_Tot", "#FINAL-TOT", FieldType.PACKED_DECIMAL, 12);

        pnd_Cntrl_Check_Dte = newFieldInRecord("pnd_Cntrl_Check_Dte", "#CNTRL-CHECK-DTE", FieldType.STRING, 6);

        pnd_Parm_Check_Dte_D = newFieldInRecord("pnd_Parm_Check_Dte_D", "#PARM-CHECK-DTE-D", FieldType.DATE);

        pnd_Parm_Check_Dte_N = newFieldInRecord("pnd_Parm_Check_Dte_N", "#PARM-CHECK-DTE-N", FieldType.NUMERIC, 8);
        pnd_Parm_Check_Dte_NRedef3 = newGroupInRecord("pnd_Parm_Check_Dte_NRedef3", "Redefines", pnd_Parm_Check_Dte_N);
        pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Cc = pnd_Parm_Check_Dte_NRedef3.newFieldInGroup("pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Cc", "#PARM-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Yy = pnd_Parm_Check_Dte_NRedef3.newFieldInGroup("pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Yy", "#PARM-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Mm = pnd_Parm_Check_Dte_NRedef3.newFieldInGroup("pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Mm", "#PARM-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Dd = pnd_Parm_Check_Dte_NRedef3.newFieldInGroup("pnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_Dd", "#PARM-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Save_Trans_Ppcn_Nbr = newFieldInRecord("pnd_Save_Trans_Ppcn_Nbr", "#SAVE-TRANS-PPCN-NBR", FieldType.STRING, 10);

        pnd_Save_Trans_Payee_Cde = newFieldInRecord("pnd_Save_Trans_Payee_Cde", "#SAVE-TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Save_Trans_Cde = newFieldInRecord("pnd_Save_Trans_Cde", "#SAVE-TRANS-CDE", FieldType.NUMERIC, 3);

        pnd_Cntrl_Frst_Trans_Dte_A = newFieldInRecord("pnd_Cntrl_Frst_Trans_Dte_A", "#CNTRL-FRST-TRANS-DTE-A", FieldType.STRING, 6);

        pnd_Cntrl_Frst_Trans_Dte_T = newFieldInRecord("pnd_Cntrl_Frst_Trans_Dte_T", "#CNTRL-FRST-TRANS-DTE-T", FieldType.TIME);

        pnd_Cntrl_Rcrd_Key = newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);
        pnd_Cntrl_Rcrd_KeyRedef4 = newGroupInRecord("pnd_Cntrl_Rcrd_KeyRedef4", "Redefines", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_KeyRedef4.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_KeyRedef4.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Last_Batch_Nbr = newFieldInRecord("pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.NUMERIC, 4);

        pnd_Iaa_Cntrct_Prtcpnt_Key = newFieldInRecord("pnd_Iaa_Cntrct_Prtcpnt_Key", "#IAA-CNTRCT-PRTCPNT-KEY", FieldType.STRING, 12);
        pnd_Iaa_Cntrct_Prtcpnt_KeyRedef5 = newGroupInRecord("pnd_Iaa_Cntrct_Prtcpnt_KeyRedef5", "Redefines", pnd_Iaa_Cntrct_Prtcpnt_Key);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef5.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cntrct_Prtcpnt_KeyRedef5.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Iaa_Cpr_Trans_Key = newFieldInRecord("pnd_Iaa_Cpr_Trans_Key", "#IAA-CPR-TRANS-KEY", FieldType.STRING, 25);
        pnd_Iaa_Cpr_Trans_KeyRedef6 = newGroupInRecord("pnd_Iaa_Cpr_Trans_KeyRedef6", "Redefines", pnd_Iaa_Cpr_Trans_Key);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id = pnd_Iaa_Cpr_Trans_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Trans_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Trans_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte = pnd_Iaa_Cpr_Trans_KeyRedef6.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        pnd_Logical_Variables = newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_1st_Grp = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_1st_Grp", "#1ST-GRP", FieldType.BOOLEAN);
        pnd_Logical_Variables_Pnd_Records_Processed = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Records_Processed", "#RECORDS-PROCESSED", 
            FieldType.BOOLEAN);

        pnd_Packed_Variables = newGroupInRecord("pnd_Packed_Variables", "#PACKED-VARIABLES");
        pnd_Packed_Variables_Pnd_Records_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Ctr", "#RECORDS-CTR", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Records_Processed_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Processed_Ctr", "#RECORDS-PROCESSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr", "#RECORDS-BYPASSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        vw_iaa_Cpr_Trans.setUniquePeList();

        this.setRecordName("LdaIaal950");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cpr_Trans.reset();
        iaa_Parm_Card_Pnd_Check_Dte.setInitialValue("MM01YY");
        pnd_Parm_Check_Dte_N.setInitialValue(19000001);
    }

    // Constructor
    public LdaIaal950() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
