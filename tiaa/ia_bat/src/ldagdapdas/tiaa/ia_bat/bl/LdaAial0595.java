/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:37 PM
**        * FROM NATURAL LDA     : AIAL0595
************************************************************
**        * FILE NAME            : LdaAial0595.java
**        * CLASS NAME           : LdaAial0595
**        * INSTANCE NAME        : LdaAial0595
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial0595 extends DbsRecord
{
    // Properties
    private DbsField pnd_Ia_Ledger_Rec;
    private DbsGroup pnd_Ia_Ledger_RecRedef1;
    private DbsField pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Key;
    private DbsField pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Record;
    private DbsField pnd_Ia_Record;
    private DbsGroup pnd_Ia_RecordRedef2;
    private DbsField pnd_Ia_Record_Pnd_Ia_Key;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler1;
    private DbsField pnd_Ia_Record_Pnd_Ia_Contract;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler_1;
    private DbsField pnd_Ia_Record_Pnd_Ia_Payee_Code;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Payee_CodeRedef3;
    private DbsField pnd_Ia_Record_Pnd_Ia_Pay_Code;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler2;
    private DbsField pnd_Ia_Record_Pnd_Ia_Da_Contract;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler3;
    private DbsField pnd_Ia_Record_Pnd_Ia_Transaction_Code;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Transaction_CodeRedef4;
    private DbsField pnd_Ia_Record_Pnd_Ia_Transaction;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler4;
    private DbsField pnd_Ia_Record_Pnd_Ia_Transaction_Date;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Transaction_DateRedef5;
    private DbsField pnd_Ia_Record_Pnd_Ia_Transaction_Date_N;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler5;
    private DbsField pnd_Ia_Record_Pnd_Ia_Effective_Date;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Effective_DateRedef6;
    private DbsField pnd_Ia_Record_Pnd_Ia_Effective_Date_N;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7;
    private DbsField pnd_Ia_Record_Pnd_Ia_Effective_Date_C;
    private DbsField pnd_Ia_Record_Pnd_Ia_Effective_Date_Y;
    private DbsField pnd_Ia_Record_Pnd_Ia_Effective_Date_M;
    private DbsField pnd_Ia_Record_Pnd_Ia_Effective_Date_D;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler6;
    private DbsField pnd_Ia_Record_Pnd_Ia_Check_Date;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler7;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sequence_Code;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler8;
    private DbsField pnd_Ia_Record_Pnd_Ia_Fund_Code;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler81;
    private DbsField pnd_Ia_Record_Pnd_Ia_Rate_Basis;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler9;
    private DbsField pnd_Ia_Record_Pnd_Ia_Reval_Ind;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler10;
    private DbsField pnd_Ia_Record_Pnd_Ia_Per_Units;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler11;
    private DbsField pnd_Ia_Record_Pnd_Ia_Per_Payment;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler12;
    private DbsField pnd_Ia_Record_Pnd_Ia_Option;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler13;
    private DbsField pnd_Ia_Record_Pnd_Ia_Mode;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler14;
    private DbsField pnd_Ia_Record_Pnd_Ia_Origin;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler15;
    private DbsField pnd_Ia_Record_Pnd_Ia_Issue_Date;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Issue_DateRedef8;
    private DbsField pnd_Ia_Record_Pnd_Ia_Issue_Date_N;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler16;
    private DbsField pnd_Ia_Record_Pnd_Ia_Final_Pay_Date;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Final_Pay_DateRedef9;
    private DbsField pnd_Ia_Record_Pnd_Ia_Final_Pay_Date_N;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler17;
    private DbsField pnd_Ia_Record_Pnd_Ia_First_Ann_Xref;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler18;
    private DbsField pnd_Ia_Record_Pnd_Ia_First_Ann_Dob;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_First_Ann_DobRedef10;
    private DbsField pnd_Ia_Record_Pnd_Ia_First_Ann_Dob_N;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler19;
    private DbsField pnd_Ia_Record_Pnd_Ia_First_Ann_Sex;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler20;
    private DbsField pnd_Ia_Record_Pnd_Ia_First_Ann_Dod;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_First_Ann_DodRedef11;
    private DbsField pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Yymm;
    private DbsField pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Dd;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler21;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sec_Ann_Xref;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler22;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Sec_Ann_DobRedef12;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob_N;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler23;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler24;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod;
    private DbsGroup pnd_Ia_Record_Pnd_Ia_Sec_Ann_DodRedef13;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Yymm;
    private DbsField pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Dd;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler25;
    private DbsField pnd_Ia_Record_Pnd_Ia_Beneficiary_Xref;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler26;
    private DbsField pnd_Ia_Record_Pnd_Ia_Mode_Change_Ind;
    private DbsField pnd_Ia_Record_Pnd_Ia_Filler27;
    private DbsField pnd_Ledger_Record;
    private DbsGroup pnd_Ledger_RecordRedef14;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Key;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler1;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Ia_Contract;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler2;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Account_Number;
    private DbsGroup pnd_Ledger_Record_Pnd_Ledger_Account_NumberRedef15;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Acc_Number;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Acc_Valuation;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler4;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Dr_Or_Cr;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler5;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Transaction_Date;
    private DbsGroup pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef16;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_N;
    private DbsGroup pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_C;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_Y;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_M;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_D;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler6;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Effective_Date;
    private DbsGroup pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef18;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Effective_Date_N;
    private DbsGroup pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Effective_Date_C;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Effective_Date_Y;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Effective_Date_M;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Effective_Date_D;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler7;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Sequence_Number;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler8;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Fund_Code;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler81;
    private DbsField pnd_Ledger_Record_Pnd_L_Rate_Code;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler9;
    private DbsField pnd_Ledger_Record_Pnd_Ledger_Reval_Ind;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler10;
    private DbsField pnd_Ledger_Record_Pnd_L_Transaction_Amount;
    private DbsField pnd_Ledger_Record_Pnd_L_Filler11;

    public DbsField getPnd_Ia_Ledger_Rec() { return pnd_Ia_Ledger_Rec; }

    public DbsGroup getPnd_Ia_Ledger_RecRedef1() { return pnd_Ia_Ledger_RecRedef1; }

    public DbsField getPnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Key() { return pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Key; }

    public DbsField getPnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Record() { return pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Record; }

    public DbsField getPnd_Ia_Record() { return pnd_Ia_Record; }

    public DbsGroup getPnd_Ia_RecordRedef2() { return pnd_Ia_RecordRedef2; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Key() { return pnd_Ia_Record_Pnd_Ia_Key; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler1() { return pnd_Ia_Record_Pnd_Ia_Filler1; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Contract() { return pnd_Ia_Record_Pnd_Ia_Contract; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler_1() { return pnd_Ia_Record_Pnd_Ia_Filler_1; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Payee_Code() { return pnd_Ia_Record_Pnd_Ia_Payee_Code; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Payee_CodeRedef3() { return pnd_Ia_Record_Pnd_Ia_Payee_CodeRedef3; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Pay_Code() { return pnd_Ia_Record_Pnd_Ia_Pay_Code; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler2() { return pnd_Ia_Record_Pnd_Ia_Filler2; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Da_Contract() { return pnd_Ia_Record_Pnd_Ia_Da_Contract; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler3() { return pnd_Ia_Record_Pnd_Ia_Filler3; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Transaction_Code() { return pnd_Ia_Record_Pnd_Ia_Transaction_Code; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Transaction_CodeRedef4() { return pnd_Ia_Record_Pnd_Ia_Transaction_CodeRedef4; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Transaction() { return pnd_Ia_Record_Pnd_Ia_Transaction; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler4() { return pnd_Ia_Record_Pnd_Ia_Filler4; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Transaction_Date() { return pnd_Ia_Record_Pnd_Ia_Transaction_Date; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Transaction_DateRedef5() { return pnd_Ia_Record_Pnd_Ia_Transaction_DateRedef5; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Transaction_Date_N() { return pnd_Ia_Record_Pnd_Ia_Transaction_Date_N; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler5() { return pnd_Ia_Record_Pnd_Ia_Filler5; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Effective_Date() { return pnd_Ia_Record_Pnd_Ia_Effective_Date; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Effective_DateRedef6() { return pnd_Ia_Record_Pnd_Ia_Effective_DateRedef6; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Effective_Date_N() { return pnd_Ia_Record_Pnd_Ia_Effective_Date_N; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Effective_DateRedef7() { return pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Effective_Date_C() { return pnd_Ia_Record_Pnd_Ia_Effective_Date_C; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Effective_Date_Y() { return pnd_Ia_Record_Pnd_Ia_Effective_Date_Y; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Effective_Date_M() { return pnd_Ia_Record_Pnd_Ia_Effective_Date_M; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Effective_Date_D() { return pnd_Ia_Record_Pnd_Ia_Effective_Date_D; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler6() { return pnd_Ia_Record_Pnd_Ia_Filler6; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Check_Date() { return pnd_Ia_Record_Pnd_Ia_Check_Date; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler7() { return pnd_Ia_Record_Pnd_Ia_Filler7; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sequence_Code() { return pnd_Ia_Record_Pnd_Ia_Sequence_Code; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler8() { return pnd_Ia_Record_Pnd_Ia_Filler8; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Fund_Code() { return pnd_Ia_Record_Pnd_Ia_Fund_Code; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler81() { return pnd_Ia_Record_Pnd_Ia_Filler81; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Rate_Basis() { return pnd_Ia_Record_Pnd_Ia_Rate_Basis; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler9() { return pnd_Ia_Record_Pnd_Ia_Filler9; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Reval_Ind() { return pnd_Ia_Record_Pnd_Ia_Reval_Ind; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler10() { return pnd_Ia_Record_Pnd_Ia_Filler10; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Per_Units() { return pnd_Ia_Record_Pnd_Ia_Per_Units; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler11() { return pnd_Ia_Record_Pnd_Ia_Filler11; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Per_Payment() { return pnd_Ia_Record_Pnd_Ia_Per_Payment; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler12() { return pnd_Ia_Record_Pnd_Ia_Filler12; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Option() { return pnd_Ia_Record_Pnd_Ia_Option; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler13() { return pnd_Ia_Record_Pnd_Ia_Filler13; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Mode() { return pnd_Ia_Record_Pnd_Ia_Mode; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler14() { return pnd_Ia_Record_Pnd_Ia_Filler14; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Origin() { return pnd_Ia_Record_Pnd_Ia_Origin; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler15() { return pnd_Ia_Record_Pnd_Ia_Filler15; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Issue_Date() { return pnd_Ia_Record_Pnd_Ia_Issue_Date; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Issue_DateRedef8() { return pnd_Ia_Record_Pnd_Ia_Issue_DateRedef8; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Issue_Date_N() { return pnd_Ia_Record_Pnd_Ia_Issue_Date_N; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler16() { return pnd_Ia_Record_Pnd_Ia_Filler16; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Final_Pay_Date() { return pnd_Ia_Record_Pnd_Ia_Final_Pay_Date; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Final_Pay_DateRedef9() { return pnd_Ia_Record_Pnd_Ia_Final_Pay_DateRedef9; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Final_Pay_Date_N() { return pnd_Ia_Record_Pnd_Ia_Final_Pay_Date_N; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler17() { return pnd_Ia_Record_Pnd_Ia_Filler17; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_First_Ann_Xref() { return pnd_Ia_Record_Pnd_Ia_First_Ann_Xref; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler18() { return pnd_Ia_Record_Pnd_Ia_Filler18; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_First_Ann_Dob() { return pnd_Ia_Record_Pnd_Ia_First_Ann_Dob; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_First_Ann_DobRedef10() { return pnd_Ia_Record_Pnd_Ia_First_Ann_DobRedef10; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_First_Ann_Dob_N() { return pnd_Ia_Record_Pnd_Ia_First_Ann_Dob_N; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler19() { return pnd_Ia_Record_Pnd_Ia_Filler19; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_First_Ann_Sex() { return pnd_Ia_Record_Pnd_Ia_First_Ann_Sex; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler20() { return pnd_Ia_Record_Pnd_Ia_Filler20; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_First_Ann_Dod() { return pnd_Ia_Record_Pnd_Ia_First_Ann_Dod; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_First_Ann_DodRedef11() { return pnd_Ia_Record_Pnd_Ia_First_Ann_DodRedef11; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Yymm() { return pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Yymm; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Dd() { return pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Dd; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler21() { return pnd_Ia_Record_Pnd_Ia_Filler21; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Xref() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_Xref; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler22() { return pnd_Ia_Record_Pnd_Ia_Filler22; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Sec_Ann_DobRedef12() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_DobRedef12; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob_N() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob_N; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler23() { return pnd_Ia_Record_Pnd_Ia_Filler23; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler24() { return pnd_Ia_Record_Pnd_Ia_Filler24; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod; }

    public DbsGroup getPnd_Ia_Record_Pnd_Ia_Sec_Ann_DodRedef13() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_DodRedef13; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Yymm() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Yymm; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Dd() { return pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Dd; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler25() { return pnd_Ia_Record_Pnd_Ia_Filler25; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Beneficiary_Xref() { return pnd_Ia_Record_Pnd_Ia_Beneficiary_Xref; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler26() { return pnd_Ia_Record_Pnd_Ia_Filler26; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Mode_Change_Ind() { return pnd_Ia_Record_Pnd_Ia_Mode_Change_Ind; }

    public DbsField getPnd_Ia_Record_Pnd_Ia_Filler27() { return pnd_Ia_Record_Pnd_Ia_Filler27; }

    public DbsField getPnd_Ledger_Record() { return pnd_Ledger_Record; }

    public DbsGroup getPnd_Ledger_RecordRedef14() { return pnd_Ledger_RecordRedef14; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Key() { return pnd_Ledger_Record_Pnd_Ledger_Key; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler1() { return pnd_Ledger_Record_Pnd_L_Filler1; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract() { return pnd_Ledger_Record_Pnd_Ledger_Ia_Contract; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler2() { return pnd_Ledger_Record_Pnd_L_Filler2; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Account_Number() { return pnd_Ledger_Record_Pnd_Ledger_Account_Number; }

    public DbsGroup getPnd_Ledger_Record_Pnd_Ledger_Account_NumberRedef15() { return pnd_Ledger_Record_Pnd_Ledger_Account_NumberRedef15; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Acc_Number() { return pnd_Ledger_Record_Pnd_Ledger_Acc_Number; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Acc_Valuation() { return pnd_Ledger_Record_Pnd_Ledger_Acc_Valuation; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler4() { return pnd_Ledger_Record_Pnd_L_Filler4; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Dr_Or_Cr() { return pnd_Ledger_Record_Pnd_Ledger_Dr_Or_Cr; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler5() { return pnd_Ledger_Record_Pnd_L_Filler5; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_Date; }

    public DbsGroup getPnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef16() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef16; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_N() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_N; }

    public DbsGroup getPnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_C() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_C; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_Y() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_Y; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_M() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_M; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_D() { return pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_D; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler6() { return pnd_Ledger_Record_Pnd_L_Filler6; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Effective_Date() { return pnd_Ledger_Record_Pnd_Ledger_Effective_Date; }

    public DbsGroup getPnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef18() { return pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef18; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_N() { return pnd_Ledger_Record_Pnd_Ledger_Effective_Date_N; }

    public DbsGroup getPnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19() { return pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_C() { return pnd_Ledger_Record_Pnd_Ledger_Effective_Date_C; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_Y() { return pnd_Ledger_Record_Pnd_Ledger_Effective_Date_Y; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_M() { return pnd_Ledger_Record_Pnd_Ledger_Effective_Date_M; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_D() { return pnd_Ledger_Record_Pnd_Ledger_Effective_Date_D; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler7() { return pnd_Ledger_Record_Pnd_L_Filler7; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Sequence_Number() { return pnd_Ledger_Record_Pnd_Ledger_Sequence_Number; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler8() { return pnd_Ledger_Record_Pnd_L_Filler8; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Fund_Code() { return pnd_Ledger_Record_Pnd_Ledger_Fund_Code; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler81() { return pnd_Ledger_Record_Pnd_L_Filler81; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Rate_Code() { return pnd_Ledger_Record_Pnd_L_Rate_Code; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler9() { return pnd_Ledger_Record_Pnd_L_Filler9; }

    public DbsField getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind() { return pnd_Ledger_Record_Pnd_Ledger_Reval_Ind; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler10() { return pnd_Ledger_Record_Pnd_L_Filler10; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Transaction_Amount() { return pnd_Ledger_Record_Pnd_L_Transaction_Amount; }

    public DbsField getPnd_Ledger_Record_Pnd_L_Filler11() { return pnd_Ledger_Record_Pnd_L_Filler11; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ia_Ledger_Rec = newFieldInRecord("pnd_Ia_Ledger_Rec", "#IA-LEDGER-REC", FieldType.STRING, 228);
        pnd_Ia_Ledger_RecRedef1 = newGroupInRecord("pnd_Ia_Ledger_RecRedef1", "Redefines", pnd_Ia_Ledger_Rec);
        pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Key = pnd_Ia_Ledger_RecRedef1.newFieldInGroup("pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Key", "#IA-LEDGER-KEY", FieldType.STRING, 
            1);
        pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Record = pnd_Ia_Ledger_RecRedef1.newFieldInGroup("pnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Record", "#IA-LEDGER-RECORD", 
            FieldType.STRING, 227);

        pnd_Ia_Record = newFieldInRecord("pnd_Ia_Record", "#IA-RECORD", FieldType.STRING, 228);
        pnd_Ia_RecordRedef2 = newGroupInRecord("pnd_Ia_RecordRedef2", "Redefines", pnd_Ia_Record);
        pnd_Ia_Record_Pnd_Ia_Key = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Key", "#IA-KEY", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Filler1 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler1", "#IA-FILLER1", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Contract = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Contract", "#IA-CONTRACT", FieldType.STRING, 8);
        pnd_Ia_Record_Pnd_Ia_Filler_1 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler_1", "#IA-FILLER-1", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Payee_Code = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Payee_Code", "#IA-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Ia_Record_Pnd_Ia_Payee_CodeRedef3 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Payee_CodeRedef3", "Redefines", pnd_Ia_Record_Pnd_Ia_Payee_Code);
        pnd_Ia_Record_Pnd_Ia_Pay_Code = pnd_Ia_Record_Pnd_Ia_Payee_CodeRedef3.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Pay_Code", "#IA-PAY-CODE", FieldType.STRING, 
            2);
        pnd_Ia_Record_Pnd_Ia_Filler2 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler2", "#IA-FILLER2", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Da_Contract = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Da_Contract", "#IA-DA-CONTRACT", FieldType.STRING, 
            8);
        pnd_Ia_Record_Pnd_Ia_Filler3 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler3", "#IA-FILLER3", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Transaction_Code = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Transaction_Code", "#IA-TRANSACTION-CODE", FieldType.NUMERIC, 
            2);
        pnd_Ia_Record_Pnd_Ia_Transaction_CodeRedef4 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Transaction_CodeRedef4", "Redefines", 
            pnd_Ia_Record_Pnd_Ia_Transaction_Code);
        pnd_Ia_Record_Pnd_Ia_Transaction = pnd_Ia_Record_Pnd_Ia_Transaction_CodeRedef4.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Transaction", "#IA-TRANSACTION", 
            FieldType.NUMERIC, 2);
        pnd_Ia_Record_Pnd_Ia_Filler4 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler4", "#IA-FILLER4", FieldType.STRING, 13);
        pnd_Ia_Record_Pnd_Ia_Transaction_Date = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Transaction_Date", "#IA-TRANSACTION-DATE", FieldType.STRING, 
            8);
        pnd_Ia_Record_Pnd_Ia_Transaction_DateRedef5 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Transaction_DateRedef5", "Redefines", 
            pnd_Ia_Record_Pnd_Ia_Transaction_Date);
        pnd_Ia_Record_Pnd_Ia_Transaction_Date_N = pnd_Ia_Record_Pnd_Ia_Transaction_DateRedef5.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Transaction_Date_N", 
            "#IA-TRANSACTION-DATE-N", FieldType.NUMERIC, 8);
        pnd_Ia_Record_Pnd_Ia_Filler5 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler5", "#IA-FILLER5", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Effective_Date = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Effective_Date", "#IA-EFFECTIVE-DATE", FieldType.STRING, 
            8);
        pnd_Ia_Record_Pnd_Ia_Effective_DateRedef6 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Effective_DateRedef6", "Redefines", pnd_Ia_Record_Pnd_Ia_Effective_Date);
        pnd_Ia_Record_Pnd_Ia_Effective_Date_N = pnd_Ia_Record_Pnd_Ia_Effective_DateRedef6.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Effective_Date_N", "#IA-EFFECTIVE-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7", "Redefines", pnd_Ia_Record_Pnd_Ia_Effective_Date);
        pnd_Ia_Record_Pnd_Ia_Effective_Date_C = pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Effective_Date_C", "#IA-EFFECTIVE-DATE-C", 
            FieldType.STRING, 2);
        pnd_Ia_Record_Pnd_Ia_Effective_Date_Y = pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Effective_Date_Y", "#IA-EFFECTIVE-DATE-Y", 
            FieldType.STRING, 2);
        pnd_Ia_Record_Pnd_Ia_Effective_Date_M = pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Effective_Date_M", "#IA-EFFECTIVE-DATE-M", 
            FieldType.STRING, 2);
        pnd_Ia_Record_Pnd_Ia_Effective_Date_D = pnd_Ia_Record_Pnd_Ia_Effective_DateRedef7.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Effective_Date_D", "#IA-EFFECTIVE-DATE-D", 
            FieldType.STRING, 2);
        pnd_Ia_Record_Pnd_Ia_Filler6 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler6", "#IA-FILLER6", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Check_Date = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Check_Date", "#IA-CHECK-DATE", FieldType.STRING, 8);
        pnd_Ia_Record_Pnd_Ia_Filler7 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler7", "#IA-FILLER7", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Sequence_Code = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sequence_Code", "#IA-SEQUENCE-CODE", FieldType.STRING, 
            2);
        pnd_Ia_Record_Pnd_Ia_Filler8 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler8", "#IA-FILLER8", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Fund_Code = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Filler81 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler81", "#IA-FILLER81", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Rate_Basis = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Rate_Basis", "#IA-RATE-BASIS", FieldType.NUMERIC, 
            2);
        pnd_Ia_Record_Pnd_Ia_Filler9 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler9", "#IA-FILLER9", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Reval_Ind = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Reval_Ind", "#IA-REVAL-IND", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Filler10 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler10", "#IA-FILLER10", FieldType.STRING, 15);
        pnd_Ia_Record_Pnd_Ia_Per_Units = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Per_Units", "#IA-PER-UNITS", FieldType.DECIMAL, 10,
            3);
        pnd_Ia_Record_Pnd_Ia_Filler11 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler11", "#IA-FILLER11", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Per_Payment = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Per_Payment", "#IA-PER-PAYMENT", FieldType.DECIMAL, 
            9,2);
        pnd_Ia_Record_Pnd_Ia_Filler12 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler12", "#IA-FILLER12", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Option = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Option", "#IA-OPTION", FieldType.NUMERIC, 2);
        pnd_Ia_Record_Pnd_Ia_Filler13 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler13", "#IA-FILLER13", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Mode = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Mode", "#IA-MODE", FieldType.NUMERIC, 3);
        pnd_Ia_Record_Pnd_Ia_Filler14 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler14", "#IA-FILLER14", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Origin = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Origin", "#IA-ORIGIN", FieldType.NUMERIC, 2);
        pnd_Ia_Record_Pnd_Ia_Filler15 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler15", "#IA-FILLER15", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Issue_Date = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Issue_Date", "#IA-ISSUE-DATE", FieldType.STRING, 8);
        pnd_Ia_Record_Pnd_Ia_Issue_DateRedef8 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Issue_DateRedef8", "Redefines", pnd_Ia_Record_Pnd_Ia_Issue_Date);
        pnd_Ia_Record_Pnd_Ia_Issue_Date_N = pnd_Ia_Record_Pnd_Ia_Issue_DateRedef8.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Issue_Date_N", "#IA-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Record_Pnd_Ia_Filler16 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler16", "#IA-FILLER16", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Final_Pay_Date = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Final_Pay_Date", "#IA-FINAL-PAY-DATE", FieldType.STRING, 
            6);
        pnd_Ia_Record_Pnd_Ia_Final_Pay_DateRedef9 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Final_Pay_DateRedef9", "Redefines", pnd_Ia_Record_Pnd_Ia_Final_Pay_Date);
        pnd_Ia_Record_Pnd_Ia_Final_Pay_Date_N = pnd_Ia_Record_Pnd_Ia_Final_Pay_DateRedef9.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Final_Pay_Date_N", "#IA-FINAL-PAY-DATE-N", 
            FieldType.NUMERIC, 6);
        pnd_Ia_Record_Pnd_Ia_Filler17 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler17", "#IA-FILLER17", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_First_Ann_Xref = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_Xref", "#IA-FIRST-ANN-XREF", FieldType.STRING, 
            9);
        pnd_Ia_Record_Pnd_Ia_Filler18 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler18", "#IA-FILLER18", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_First_Ann_Dob = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_Dob", "#IA-FIRST-ANN-DOB", FieldType.STRING, 
            8);
        pnd_Ia_Record_Pnd_Ia_First_Ann_DobRedef10 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_DobRedef10", "Redefines", pnd_Ia_Record_Pnd_Ia_First_Ann_Dob);
        pnd_Ia_Record_Pnd_Ia_First_Ann_Dob_N = pnd_Ia_Record_Pnd_Ia_First_Ann_DobRedef10.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_Dob_N", "#IA-FIRST-ANN-DOB-N", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Record_Pnd_Ia_Filler19 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler19", "#IA-FILLER19", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_First_Ann_Sex = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_Sex", "#IA-FIRST-ANN-SEX", FieldType.NUMERIC, 
            1);
        pnd_Ia_Record_Pnd_Ia_Filler20 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler20", "#IA-FILLER20", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_First_Ann_Dod = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_Dod", "#IA-FIRST-ANN-DOD", FieldType.STRING, 
            8);
        pnd_Ia_Record_Pnd_Ia_First_Ann_DodRedef11 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_DodRedef11", "Redefines", pnd_Ia_Record_Pnd_Ia_First_Ann_Dod);
        pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Yymm = pnd_Ia_Record_Pnd_Ia_First_Ann_DodRedef11.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Yymm", 
            "#IA-FIRST-ANN-DOD-YYMM", FieldType.NUMERIC, 6);
        pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Dd = pnd_Ia_Record_Pnd_Ia_First_Ann_DodRedef11.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Dd", "#IA-FIRST-ANN-DOD-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ia_Record_Pnd_Ia_Filler21 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler21", "#IA-FILLER21", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_Xref = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_Xref", "#IA-SEC-ANN-XREF", FieldType.STRING, 
            9);
        pnd_Ia_Record_Pnd_Ia_Filler22 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler22", "#IA-FILLER22", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob", "#IA-SEC-ANN-DOB", FieldType.STRING, 
            8);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_DobRedef12 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_DobRedef12", "Redefines", pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob_N = pnd_Ia_Record_Pnd_Ia_Sec_Ann_DobRedef12.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob_N", "#IA-SEC-ANN-DOB-N", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Record_Pnd_Ia_Filler23 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler23", "#IA-FILLER23", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex", "#IA-SEC-ANN-SEX", FieldType.NUMERIC, 
            1);
        pnd_Ia_Record_Pnd_Ia_Filler24 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler24", "#IA-FILLER24", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod", "#IA-SEC-ANN-DOD", FieldType.STRING, 
            8);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_DodRedef13 = pnd_Ia_RecordRedef2.newGroupInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_DodRedef13", "Redefines", pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Yymm = pnd_Ia_Record_Pnd_Ia_Sec_Ann_DodRedef13.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Yymm", "#IA-SEC-ANN-DOD-YYMM", 
            FieldType.NUMERIC, 6);
        pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Dd = pnd_Ia_Record_Pnd_Ia_Sec_Ann_DodRedef13.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Dd", "#IA-SEC-ANN-DOD-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ia_Record_Pnd_Ia_Filler25 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler25", "#IA-FILLER25", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Beneficiary_Xref = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Beneficiary_Xref", "#IA-BENEFICIARY-XREF", FieldType.STRING, 
            9);
        pnd_Ia_Record_Pnd_Ia_Filler26 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler26", "#IA-FILLER26", FieldType.STRING, 1);
        pnd_Ia_Record_Pnd_Ia_Mode_Change_Ind = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Mode_Change_Ind", "#IA-MODE-CHANGE-IND", FieldType.STRING, 
            1);
        pnd_Ia_Record_Pnd_Ia_Filler27 = pnd_Ia_RecordRedef2.newFieldInGroup("pnd_Ia_Record_Pnd_Ia_Filler27", "#IA-FILLER27", FieldType.STRING, 21);

        pnd_Ledger_Record = newFieldInRecord("pnd_Ledger_Record", "#LEDGER-RECORD", FieldType.STRING, 178);
        pnd_Ledger_RecordRedef14 = newGroupInRecord("pnd_Ledger_RecordRedef14", "Redefines", pnd_Ledger_Record);
        pnd_Ledger_Record_Pnd_Ledger_Key = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Key", "#LEDGER-KEY", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_L_Filler1 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler1", "#L-FILLER1", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_Ledger_Ia_Contract = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Ia_Contract", "#LEDGER-IA-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Ledger_Record_Pnd_L_Filler2 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler2", "#L-FILLER2", FieldType.STRING, 
            16);
        pnd_Ledger_Record_Pnd_Ledger_Account_Number = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Account_Number", "#LEDGER-ACCOUNT-NUMBER", 
            FieldType.NUMERIC, 8);
        pnd_Ledger_Record_Pnd_Ledger_Account_NumberRedef15 = pnd_Ledger_RecordRedef14.newGroupInGroup("pnd_Ledger_Record_Pnd_Ledger_Account_NumberRedef15", 
            "Redefines", pnd_Ledger_Record_Pnd_Ledger_Account_Number);
        pnd_Ledger_Record_Pnd_Ledger_Acc_Number = pnd_Ledger_Record_Pnd_Ledger_Account_NumberRedef15.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Acc_Number", 
            "#LEDGER-ACC-NUMBER", FieldType.NUMERIC, 6);
        pnd_Ledger_Record_Pnd_Ledger_Acc_Valuation = pnd_Ledger_Record_Pnd_Ledger_Account_NumberRedef15.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Acc_Valuation", 
            "#LEDGER-ACC-VALUATION", FieldType.NUMERIC, 2);
        pnd_Ledger_Record_Pnd_L_Filler4 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler4", "#L-FILLER4", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_Ledger_Dr_Or_Cr = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Dr_Or_Cr", "#LEDGER-DR-OR-CR", 
            FieldType.STRING, 1);
        pnd_Ledger_Record_Pnd_L_Filler5 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler5", "#L-FILLER5", FieldType.STRING, 
            2);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_Date = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_Date", "#LEDGER-TRANSACTION-DATE", 
            FieldType.STRING, 8);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef16 = pnd_Ledger_RecordRedef14.newGroupInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef16", 
            "Redefines", pnd_Ledger_Record_Pnd_Ledger_Transaction_Date);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_N = pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef16.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_N", 
            "#LEDGER-TRANSACTION-DATE-N", FieldType.NUMERIC, 8);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17 = pnd_Ledger_RecordRedef14.newGroupInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17", 
            "Redefines", pnd_Ledger_Record_Pnd_Ledger_Transaction_Date);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_C = pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_C", 
            "#LEDGER-TRANSACTION-DATE-C", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_Y = pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_Y", 
            "#LEDGER-TRANSACTION-DATE-Y", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_M = pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_M", 
            "#LEDGER-TRANSACTION-DATE-M", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_D = pnd_Ledger_Record_Pnd_Ledger_Transaction_DateRedef17.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Transaction_Date_D", 
            "#LEDGER-TRANSACTION-DATE-D", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_L_Filler6 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler6", "#L-FILLER6", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_Ledger_Effective_Date = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_Date", "#LEDGER-EFFECTIVE-DATE", 
            FieldType.STRING, 8);
        pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef18 = pnd_Ledger_RecordRedef14.newGroupInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef18", 
            "Redefines", pnd_Ledger_Record_Pnd_Ledger_Effective_Date);
        pnd_Ledger_Record_Pnd_Ledger_Effective_Date_N = pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef18.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_Date_N", 
            "#LEDGER-EFFECTIVE-DATE-N", FieldType.NUMERIC, 8);
        pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19 = pnd_Ledger_RecordRedef14.newGroupInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19", 
            "Redefines", pnd_Ledger_Record_Pnd_Ledger_Effective_Date);
        pnd_Ledger_Record_Pnd_Ledger_Effective_Date_C = pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_Date_C", 
            "#LEDGER-EFFECTIVE-DATE-C", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_Ledger_Effective_Date_Y = pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_Date_Y", 
            "#LEDGER-EFFECTIVE-DATE-Y", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_Ledger_Effective_Date_M = pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_Date_M", 
            "#LEDGER-EFFECTIVE-DATE-M", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_Ledger_Effective_Date_D = pnd_Ledger_Record_Pnd_Ledger_Effective_DateRedef19.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Effective_Date_D", 
            "#LEDGER-EFFECTIVE-DATE-D", FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_L_Filler7 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler7", "#L-FILLER7", FieldType.STRING, 
            10);
        pnd_Ledger_Record_Pnd_Ledger_Sequence_Number = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Sequence_Number", "#LEDGER-SEQUENCE-NUMBER", 
            FieldType.STRING, 2);
        pnd_Ledger_Record_Pnd_L_Filler8 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler8", "#L-FILLER8", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_Ledger_Fund_Code = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Fund_Code", "#LEDGER-FUND-CODE", 
            FieldType.STRING, 1);
        pnd_Ledger_Record_Pnd_L_Filler81 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler81", "#L-FILLER81", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_L_Rate_Code = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Rate_Code", "#L-RATE-CODE", FieldType.STRING, 
            2);
        pnd_Ledger_Record_Pnd_L_Filler9 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler9", "#L-FILLER9", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_Ledger_Reval_Ind = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_Ledger_Reval_Ind", "#LEDGER-REVAL-IND", 
            FieldType.STRING, 1);
        pnd_Ledger_Record_Pnd_L_Filler10 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler10", "#L-FILLER10", FieldType.STRING, 
            1);
        pnd_Ledger_Record_Pnd_L_Transaction_Amount = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Transaction_Amount", "#L-TRANSACTION-AMOUNT", 
            FieldType.DECIMAL, 13,2);
        pnd_Ledger_Record_Pnd_L_Filler11 = pnd_Ledger_RecordRedef14.newFieldInGroup("pnd_Ledger_Record_Pnd_L_Filler11", "#L-FILLER11", FieldType.STRING, 
            89);

        this.setRecordName("LdaAial0595");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAial0595() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
