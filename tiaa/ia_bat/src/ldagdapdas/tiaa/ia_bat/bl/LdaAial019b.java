/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:28 PM
**        * FROM NATURAL LDA     : AIAL019B
************************************************************
**        * FILE NAME            : LdaAial019b.java
**        * CLASS NAME           : LdaAial019b
**        * INSTANCE NAME        : LdaAial019b
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAial019b extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Sortb_Record;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1;
    private DbsGroup pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Cpm_Key;
    private DbsGroup pnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Contract_Payee;
    private DbsGroup pnd_Sortb_Record_Pnd_Sbc1_Contract_PayeeRedef3;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Contract_Number;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Payee_Code;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Payment_Method;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Final_Record_Number;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Mode;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Origin;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Option;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Issue_Date;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Final_Per_Pay_Date;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dob;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Sex;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dod;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dob;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Sex;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dod;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Processing_Date;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Type_Of_Run;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Next_Pymnt_Dte;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Pct;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Rate;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Prorate_Ind;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Return_Code;
    private DbsField pnd_Sortb_Record_Pnd_Sbc1_Filler;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2;
    private DbsGroup pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4;
    private DbsField pnd_Sortb_Record_Pnd_Sbc2_Contract_Payee;
    private DbsField pnd_Sortb_Record_Pnd_Sbc2_Payment_Method;
    private DbsField pnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number;
    private DbsField pnd_Sortb_Record_Pnd_Sbc2_Final_Record_Number;
    private DbsField pnd_Sortb_Record_Pnd_Sbc2_Cref_Fund_In;
    private DbsField pnd_Sortb_Record_Pnd_Sbc2_Cref_Reval_Method;
    private DbsField pnd_Sortb_Record_Pnd_Sbc2_Filler;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area1;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area2;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area3;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area4;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area5;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area6;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area7;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area8;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area9;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area10;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area11;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area12;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area13;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area14;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area15;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area16;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area17;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area18;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Array_Area19;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Key_Record_Number;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number;
    private DbsField pnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key;

    public DbsGroup getPnd_Sortb_Record() { return pnd_Sortb_Record; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area1() { return pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1; }

    public DbsGroup getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1() { return pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Cpm_Key() { return pnd_Sortb_Record_Pnd_Sbc1_Cpm_Key; }

    public DbsGroup getPnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2() { return pnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Contract_Payee() { return pnd_Sortb_Record_Pnd_Sbc1_Contract_Payee; }

    public DbsGroup getPnd_Sortb_Record_Pnd_Sbc1_Contract_PayeeRedef3() { return pnd_Sortb_Record_Pnd_Sbc1_Contract_PayeeRedef3; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Contract_Number() { return pnd_Sortb_Record_Pnd_Sbc1_Contract_Number; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Payee_Code() { return pnd_Sortb_Record_Pnd_Sbc1_Payee_Code; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Payment_Method() { return pnd_Sortb_Record_Pnd_Sbc1_Payment_Method; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number() { return pnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Final_Record_Number() { return pnd_Sortb_Record_Pnd_Sbc1_Final_Record_Number; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Mode() { return pnd_Sortb_Record_Pnd_Sbc1_Mode; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Origin() { return pnd_Sortb_Record_Pnd_Sbc1_Origin; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Option() { return pnd_Sortb_Record_Pnd_Sbc1_Option; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Issue_Date() { return pnd_Sortb_Record_Pnd_Sbc1_Issue_Date; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Final_Per_Pay_Date() { return pnd_Sortb_Record_Pnd_Sbc1_Final_Per_Pay_Date; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dob() { return pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dob; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_First_Ann_Sex() { return pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Sex; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dod() { return pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dod; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dob() { return pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dob; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Sex() { return pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Sex; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dod() { return pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dod; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date() { return pnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Processing_Date() { return pnd_Sortb_Record_Pnd_Sbc1_Processing_Date; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Type_Of_Run() { return pnd_Sortb_Record_Pnd_Sbc1_Type_Of_Run; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Next_Pymnt_Dte() { return pnd_Sortb_Record_Pnd_Sbc1_Next_Pymnt_Dte; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Gbpm_Pct() { return pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Pct; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Gbpm_Rate() { return pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Rate; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Gbpm_Prorate_Ind() { return pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Prorate_Ind; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Return_Code() { return pnd_Sortb_Record_Pnd_Sbc1_Return_Code; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc1_Filler() { return pnd_Sortb_Record_Pnd_Sbc1_Filler; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area2() { return pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2; }

    public DbsGroup getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4() { return pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc2_Contract_Payee() { return pnd_Sortb_Record_Pnd_Sbc2_Contract_Payee; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc2_Payment_Method() { return pnd_Sortb_Record_Pnd_Sbc2_Payment_Method; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number() { return pnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc2_Final_Record_Number() { return pnd_Sortb_Record_Pnd_Sbc2_Final_Record_Number; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc2_Cref_Fund_In() { return pnd_Sortb_Record_Pnd_Sbc2_Cref_Fund_In; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc2_Cref_Reval_Method() { return pnd_Sortb_Record_Pnd_Sbc2_Cref_Reval_Method; }

    public DbsField getPnd_Sortb_Record_Pnd_Sbc2_Filler() { return pnd_Sortb_Record_Pnd_Sbc2_Filler; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area1() { return pnd_Sortb_Record_Pnd_Sb_Array_Area1; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area2() { return pnd_Sortb_Record_Pnd_Sb_Array_Area2; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area3() { return pnd_Sortb_Record_Pnd_Sb_Array_Area3; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area4() { return pnd_Sortb_Record_Pnd_Sb_Array_Area4; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area5() { return pnd_Sortb_Record_Pnd_Sb_Array_Area5; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area6() { return pnd_Sortb_Record_Pnd_Sb_Array_Area6; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area7() { return pnd_Sortb_Record_Pnd_Sb_Array_Area7; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area8() { return pnd_Sortb_Record_Pnd_Sb_Array_Area8; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area9() { return pnd_Sortb_Record_Pnd_Sb_Array_Area9; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area10() { return pnd_Sortb_Record_Pnd_Sb_Array_Area10; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area11() { return pnd_Sortb_Record_Pnd_Sb_Array_Area11; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area12() { return pnd_Sortb_Record_Pnd_Sb_Array_Area12; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area13() { return pnd_Sortb_Record_Pnd_Sb_Array_Area13; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area14() { return pnd_Sortb_Record_Pnd_Sb_Array_Area14; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area15() { return pnd_Sortb_Record_Pnd_Sb_Array_Area15; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area16() { return pnd_Sortb_Record_Pnd_Sb_Array_Area16; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area17() { return pnd_Sortb_Record_Pnd_Sb_Array_Area17; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area18() { return pnd_Sortb_Record_Pnd_Sb_Array_Area18; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Array_Area19() { return pnd_Sortb_Record_Pnd_Sb_Array_Area19; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py() { return pnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Key_Record_Number() { return pnd_Sortb_Record_Pnd_Sb_Key_Record_Number; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte() { return pnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number() { return pnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number; }

    public DbsField getPnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key() { return pnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Sortb_Record = newGroupInRecord("pnd_Sortb_Record", "#SORTB-RECORD");
        pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1", "#SB-COMMON-DATA-AREA1", 
            FieldType.STRING, 250);
        pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1 = pnd_Sortb_Record.newGroupInGroup("pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1", "Redefines", 
            pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1);
        pnd_Sortb_Record_Pnd_Sbc1_Cpm_Key = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Cpm_Key", "#SBC1-CPM-KEY", 
            FieldType.STRING, 11);
        pnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2 = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newGroupInGroup("pnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2", 
            "Redefines", pnd_Sortb_Record_Pnd_Sbc1_Cpm_Key);
        pnd_Sortb_Record_Pnd_Sbc1_Contract_Payee = pnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Contract_Payee", 
            "#SBC1-CONTRACT-PAYEE", FieldType.STRING, 10);
        pnd_Sortb_Record_Pnd_Sbc1_Contract_PayeeRedef3 = pnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2.newGroupInGroup("pnd_Sortb_Record_Pnd_Sbc1_Contract_PayeeRedef3", 
            "Redefines", pnd_Sortb_Record_Pnd_Sbc1_Contract_Payee);
        pnd_Sortb_Record_Pnd_Sbc1_Contract_Number = pnd_Sortb_Record_Pnd_Sbc1_Contract_PayeeRedef3.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Contract_Number", 
            "#SBC1-CONTRACT-NUMBER", FieldType.STRING, 8);
        pnd_Sortb_Record_Pnd_Sbc1_Payee_Code = pnd_Sortb_Record_Pnd_Sbc1_Contract_PayeeRedef3.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Payee_Code", 
            "#SBC1-PAYEE-CODE", FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc1_Payment_Method = pnd_Sortb_Record_Pnd_Sbc1_Cpm_KeyRedef2.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Payment_Method", 
            "#SBC1-PAYMENT-METHOD", FieldType.STRING, 1);
        pnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number", 
            "#SBC1-CURRENT-RECORD-NUMBER", FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc1_Final_Record_Number = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Final_Record_Number", 
            "#SBC1-FINAL-RECORD-NUMBER", FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc1_Mode = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Mode", "#SBC1-MODE", 
            FieldType.NUMERIC, 3);
        pnd_Sortb_Record_Pnd_Sbc1_Origin = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Origin", "#SBC1-ORIGIN", 
            FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc1_Option = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Option", "#SBC1-OPTION", 
            FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc1_Issue_Date = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Issue_Date", 
            "#SBC1-ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_Sortb_Record_Pnd_Sbc1_Final_Per_Pay_Date = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Final_Per_Pay_Date", 
            "#SBC1-FINAL-PER-PAY-DATE", FieldType.NUMERIC, 6);
        pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dob = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dob", 
            "#SBC1-FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Sex = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Sex", 
            "#SBC1-FIRST-ANN-SEX", FieldType.NUMERIC, 1);
        pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dod = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_First_Ann_Dod", 
            "#SBC1-FIRST-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dob = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dob", 
            "#SBC1-SECOND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Sex = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Sex", 
            "#SBC1-SECOND-ANN-SEX", FieldType.NUMERIC, 1);
        pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dod = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Second_Ann_Dod", 
            "#SBC1-SECOND-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date", 
            "#SBC1-TRANSFER-EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_Sortb_Record_Pnd_Sbc1_Processing_Date = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Processing_Date", 
            "#SBC1-PROCESSING-DATE", FieldType.NUMERIC, 8);
        pnd_Sortb_Record_Pnd_Sbc1_Type_Of_Run = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Type_Of_Run", 
            "#SBC1-TYPE-OF-RUN", FieldType.STRING, 1);
        pnd_Sortb_Record_Pnd_Sbc1_Next_Pymnt_Dte = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Next_Pymnt_Dte", 
            "#SBC1-NEXT-PYMNT-DTE", FieldType.DATE);
        pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Pct = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Pct", "#SBC1-GBPM-PCT", 
            FieldType.DECIMAL, 4,2);
        pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Rate = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Rate", "#SBC1-GBPM-RATE", 
            FieldType.DECIMAL, 4,2);
        pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Prorate_Ind = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Gbpm_Prorate_Ind", 
            "#SBC1-GBPM-PRORATE-IND", FieldType.STRING, 1);
        pnd_Sortb_Record_Pnd_Sbc1_Return_Code = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Return_Code", 
            "#SBC1-RETURN-CODE", FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc1_Filler = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area1Redef1.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc1_Filler", "#SBC1-FILLER", 
            FieldType.STRING, 152);
        pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2", "#SB-COMMON-DATA-AREA2", 
            FieldType.STRING, 250);
        pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4 = pnd_Sortb_Record.newGroupInGroup("pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4", "Redefines", 
            pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2);
        pnd_Sortb_Record_Pnd_Sbc2_Contract_Payee = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc2_Contract_Payee", 
            "#SBC2-CONTRACT-PAYEE", FieldType.STRING, 10);
        pnd_Sortb_Record_Pnd_Sbc2_Payment_Method = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc2_Payment_Method", 
            "#SBC2-PAYMENT-METHOD", FieldType.STRING, 1);
        pnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number", 
            "#SBC2-CURRENT-RECORD-NUMBER", FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc2_Final_Record_Number = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc2_Final_Record_Number", 
            "#SBC2-FINAL-RECORD-NUMBER", FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sbc2_Cref_Fund_In = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc2_Cref_Fund_In", 
            "#SBC2-CREF-FUND-IN", FieldType.STRING, 1);
        pnd_Sortb_Record_Pnd_Sbc2_Cref_Reval_Method = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc2_Cref_Reval_Method", 
            "#SBC2-CREF-REVAL-METHOD", FieldType.STRING, 1);
        pnd_Sortb_Record_Pnd_Sbc2_Filler = pnd_Sortb_Record_Pnd_Sb_Common_Data_Area2Redef4.newFieldInGroup("pnd_Sortb_Record_Pnd_Sbc2_Filler", "#SBC2-FILLER", 
            FieldType.STRING, 233);
        pnd_Sortb_Record_Pnd_Sb_Array_Area1 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area1", "#SB-ARRAY-AREA1", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area2 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area2", "#SB-ARRAY-AREA2", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area3 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area3", "#SB-ARRAY-AREA3", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area4 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area4", "#SB-ARRAY-AREA4", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area5 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area5", "#SB-ARRAY-AREA5", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area6 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area6", "#SB-ARRAY-AREA6", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area7 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area7", "#SB-ARRAY-AREA7", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area8 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area8", "#SB-ARRAY-AREA8", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area9 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area9", "#SB-ARRAY-AREA9", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area10 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area10", "#SB-ARRAY-AREA10", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area11 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area11", "#SB-ARRAY-AREA11", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area12 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area12", "#SB-ARRAY-AREA12", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area13 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area13", "#SB-ARRAY-AREA13", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area14 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area14", "#SB-ARRAY-AREA14", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area15 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area15", "#SB-ARRAY-AREA15", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area16 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area16", "#SB-ARRAY-AREA16", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area17 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area17", "#SB-ARRAY-AREA17", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area18 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area18", "#SB-ARRAY-AREA18", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Array_Area19 = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Array_Area19", "#SB-ARRAY-AREA19", FieldType.STRING, 
            250);
        pnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py", "#SB-KEY-CNTRCT-PY", FieldType.STRING, 
            10);
        pnd_Sortb_Record_Pnd_Sb_Key_Record_Number = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Key_Record_Number", "#SB-KEY-RECORD-NUMBER", 
            FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte", "#SB-KEY-PRCSS-DTE", FieldType.NUMERIC, 
            8);
        pnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number", "#SB-KEY-SEQUENCE-NUMBER", 
            FieldType.NUMERIC, 2);
        pnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key = pnd_Sortb_Record.newFieldInGroup("pnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key", "#SB-KEY-LOGC-TRANSACTION-KEY", 
            FieldType.BINARY, 8);

        this.setRecordName("LdaAial019b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAial019b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
