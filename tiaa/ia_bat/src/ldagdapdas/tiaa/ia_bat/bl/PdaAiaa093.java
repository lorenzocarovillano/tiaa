/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:42 PM
**        * FROM NATURAL PDA     : AIAA093
************************************************************
**        * FILE NAME            : PdaAiaa093.java
**        * CLASS NAME           : PdaAiaa093
**        * INSTANCE NAME        : PdaAiaa093
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAiaa093 extends PdaBase
{
    // Properties
    private DbsGroup aian093_Linkage;
    private DbsGroup aian093_Linkage_Fund_Array;
    private DbsField aian093_Linkage_Fund_Code;
    private DbsField aian093_Linkage_Fund_Annual_Annlzd_Units;
    private DbsField aian093_Linkage_Fund_Annual_Units;
    private DbsField aian093_Linkage_Fund_Annual_Annlzd_Payts;
    private DbsField aian093_Linkage_Fund_Annual_Payments;
    private DbsField aian093_Linkage_Fund_Monthly_Units;
    private DbsField aian093_Linkage_Fund_Monthly_Payments;
    private DbsField aian093_Linkage_Aian093_Return_Code;
    private DbsGroup aian093_Linkage_Aian093_Return_CodeRedef1;
    private DbsField aian093_Linkage_Aian093_Return_Code_Pgm;
    private DbsField aian093_Linkage_Aian093_Return_Code_Nbr;

    public DbsGroup getAian093_Linkage() { return aian093_Linkage; }

    public DbsGroup getAian093_Linkage_Fund_Array() { return aian093_Linkage_Fund_Array; }

    public DbsField getAian093_Linkage_Fund_Code() { return aian093_Linkage_Fund_Code; }

    public DbsField getAian093_Linkage_Fund_Annual_Annlzd_Units() { return aian093_Linkage_Fund_Annual_Annlzd_Units; }

    public DbsField getAian093_Linkage_Fund_Annual_Units() { return aian093_Linkage_Fund_Annual_Units; }

    public DbsField getAian093_Linkage_Fund_Annual_Annlzd_Payts() { return aian093_Linkage_Fund_Annual_Annlzd_Payts; }

    public DbsField getAian093_Linkage_Fund_Annual_Payments() { return aian093_Linkage_Fund_Annual_Payments; }

    public DbsField getAian093_Linkage_Fund_Monthly_Units() { return aian093_Linkage_Fund_Monthly_Units; }

    public DbsField getAian093_Linkage_Fund_Monthly_Payments() { return aian093_Linkage_Fund_Monthly_Payments; }

    public DbsField getAian093_Linkage_Aian093_Return_Code() { return aian093_Linkage_Aian093_Return_Code; }

    public DbsGroup getAian093_Linkage_Aian093_Return_CodeRedef1() { return aian093_Linkage_Aian093_Return_CodeRedef1; }

    public DbsField getAian093_Linkage_Aian093_Return_Code_Pgm() { return aian093_Linkage_Aian093_Return_Code_Pgm; }

    public DbsField getAian093_Linkage_Aian093_Return_Code_Nbr() { return aian093_Linkage_Aian093_Return_Code_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        aian093_Linkage = dbsRecord.newGroupInRecord("aian093_Linkage", "AIAN093-LINKAGE");
        aian093_Linkage.setParameterOption(ParameterOption.ByReference);
        aian093_Linkage_Fund_Array = aian093_Linkage.newGroupArrayInGroup("aian093_Linkage_Fund_Array", "FUND-ARRAY", new DbsArrayController(1,20));
        aian093_Linkage_Fund_Code = aian093_Linkage_Fund_Array.newFieldInGroup("aian093_Linkage_Fund_Code", "FUND-CODE", FieldType.STRING, 1);
        aian093_Linkage_Fund_Annual_Annlzd_Units = aian093_Linkage_Fund_Array.newFieldInGroup("aian093_Linkage_Fund_Annual_Annlzd_Units", "FUND-ANNUAL-ANNLZD-UNITS", 
            FieldType.DECIMAL, 13,3);
        aian093_Linkage_Fund_Annual_Units = aian093_Linkage_Fund_Array.newFieldInGroup("aian093_Linkage_Fund_Annual_Units", "FUND-ANNUAL-UNITS", FieldType.DECIMAL, 
            13,3);
        aian093_Linkage_Fund_Annual_Annlzd_Payts = aian093_Linkage_Fund_Array.newFieldInGroup("aian093_Linkage_Fund_Annual_Annlzd_Payts", "FUND-ANNUAL-ANNLZD-PAYTS", 
            FieldType.DECIMAL, 13,2);
        aian093_Linkage_Fund_Annual_Payments = aian093_Linkage_Fund_Array.newFieldInGroup("aian093_Linkage_Fund_Annual_Payments", "FUND-ANNUAL-PAYMENTS", 
            FieldType.DECIMAL, 13,2);
        aian093_Linkage_Fund_Monthly_Units = aian093_Linkage_Fund_Array.newFieldInGroup("aian093_Linkage_Fund_Monthly_Units", "FUND-MONTHLY-UNITS", FieldType.DECIMAL, 
            13,3);
        aian093_Linkage_Fund_Monthly_Payments = aian093_Linkage_Fund_Array.newFieldInGroup("aian093_Linkage_Fund_Monthly_Payments", "FUND-MONTHLY-PAYMENTS", 
            FieldType.DECIMAL, 13,2);
        aian093_Linkage_Aian093_Return_Code = aian093_Linkage.newFieldInGroup("aian093_Linkage_Aian093_Return_Code", "AIAN093-RETURN-CODE", FieldType.STRING, 
            11);
        aian093_Linkage_Aian093_Return_CodeRedef1 = aian093_Linkage.newGroupInGroup("aian093_Linkage_Aian093_Return_CodeRedef1", "Redefines", aian093_Linkage_Aian093_Return_Code);
        aian093_Linkage_Aian093_Return_Code_Pgm = aian093_Linkage_Aian093_Return_CodeRedef1.newFieldInGroup("aian093_Linkage_Aian093_Return_Code_Pgm", 
            "AIAN093-RETURN-CODE-PGM", FieldType.STRING, 8);
        aian093_Linkage_Aian093_Return_Code_Nbr = aian093_Linkage_Aian093_Return_CodeRedef1.newFieldInGroup("aian093_Linkage_Aian093_Return_Code_Nbr", 
            "AIAN093-RETURN-CODE-NBR", FieldType.NUMERIC, 3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAiaa093(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

