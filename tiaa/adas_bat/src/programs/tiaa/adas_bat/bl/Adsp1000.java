/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:41 PM
**        * FROM NATURAL PROGRAM : Adsp1000
************************************************************
**        * FILE NAME            : Adsp1000.java
**        * CLASS NAME           : Adsp1000
**        * INSTANCE NAME        : Adsp1000
************************************************************
************************************************************************
* PROGRAM  : ADSP1000
* GENERATED: APRIL 14, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : ANNUITIZATION SYSTEM PAYMENT SUMMARY
*            DAILY ACTIVITY REPORT - FINAL PREMIUM ACCUMULATION
*            PROGRAM WILL READ WORK FILE AND REPORT THE DA TO IA
*            FINAL PREMIUM ACCUMULATIONS IN EFFECTIVE DATE SEQUENCE.
*
* REMARKS  : CLONED FROM NAZP770 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY        DESCRIPTION OF CHANGES
* 01-02-01  C SCHNEIDER ADDED OPTN-CDE TO WORK FILE TO SUPPORT REPORT
*                       CHANGES
* 01-26-04  O SOTTO     99 RATES CHANGES. SCAN FOR 012604.
* 04-14-04  C. AVE      MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 12/05/08 R.SACHARNY   PROCESS "TIAA Stable Return" AND "CREF Access"
*                       FUNDS (RS0)
* 7/19/2010 C. MASON  RESTOW DUE TO LDA CHANGES
* 03/01/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030112.
* 09/2013   O. SOTTO  CREF REA CHANGES MARKED WITH OS-092013.
* 02/24/2017 R.CARREON PIN EXTANSION 02242017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp1000 extends BLNatBase
{
    // Data Areas
    private LdaAdsl1000 ldaAdsl1000;
    private LdaAdsl401a ldaAdsl401a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cnvrt_Annty_Optn;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table;
    private DbsField pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_All_Indx_Pnd_Loop;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_Cntl_Isn;
    private DbsField pnd_I;
    private DbsField pnd_Total_Number_Rec;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Cref_Prod_Found;
    private DbsField pnd_Stbl_Fund;
    private DbsField pnd_Accs_Fund;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Work_Business_Date;

    private DbsGroup pnd_Work_Business_Date__R_Field_3;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Cc;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Yy;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Mm;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Dd;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd;

    private DbsGroup pnd_Check_Cycle_Date_Yyyymmdd__R_Field_4;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Cc;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Yy;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Mm;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dd;
    private DbsField pnd_Today_Business_Date;

    private DbsGroup pnd_Today_Business_Date__R_Field_5;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Cc;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_6;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd;
    private DbsField pnd_Work_Accounting_Dte_Mmddccyy;

    private DbsGroup pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7;
    private DbsField pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Mm;
    private DbsField pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Slash_1;
    private DbsField pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Dd;
    private DbsField pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Slash_2;
    private DbsField pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Cc;
    private DbsField pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Yy;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy;

    private DbsGroup pnd_Today_Accounting_Dte_Mmddccyy__R_Field_8;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Mm;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Dd;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Cc;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Yy;
    private DbsField pnd_Prev_Effctv_Dte;

    private DbsGroup pnd_Prev_Effctv_Dte__R_Field_9;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd;
    private DbsField pnd_Compare_Prev_Effctv_Dte_A;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Adp_Effctv_Date;
    private DbsField pnd_Adp_Effctv_Date_A;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_10;
    private DbsField pnd_Date_A_Pnd_Date_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl1000 = new LdaAdsl1000();
        registerRecord(ldaAdsl1000);
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cnvrt_Annty_Optn = localVariables.newFieldInRecord("pnd_Cnvrt_Annty_Optn", "#CNVRT-ANNTY-OPTN", FieldType.STRING, 1);
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);

        pnd_Pas_Ext_Cntrl_02_Work_Area = localVariables.newGroupInRecord("pnd_Pas_Ext_Cntrl_02_Work_Area", "#PAS-EXT-CNTRL-02-WORK-AREA");

        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table = pnd_Pas_Ext_Cntrl_02_Work_Area.newGroupArrayInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table", 
            "#PEC-ACCT-TABLE", new DbsArrayController(1, 20));
        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1 = pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table.newFieldInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1", 
            "#PEC-ACCT-NAME-1", FieldType.STRING, 1);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte", "ADS-FNL-PRM-ACCUM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_FNL_PRM_ACCUM_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Loop = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Loop", "#LOOP", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Number_Rec = localVariables.newFieldInRecord("pnd_Total_Number_Rec", "#TOTAL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Cref_Prod_Found = localVariables.newFieldInRecord("pnd_Cref_Prod_Found", "#CREF-PROD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Stbl_Fund = localVariables.newFieldInRecord("pnd_Stbl_Fund", "#STBL-FUND", FieldType.BOOLEAN, 1);
        pnd_Accs_Fund = localVariables.newFieldInRecord("pnd_Accs_Fund", "#ACCS-FUND", FieldType.BOOLEAN, 1);
        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Work_Business_Date = localVariables.newFieldInRecord("pnd_Work_Business_Date", "#WORK-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Work_Business_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Business_Date__R_Field_3", "REDEFINE", pnd_Work_Business_Date);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Cc = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Cc", 
            "#WORK-BUSINESS-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Yy = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Yy", 
            "#WORK-BUSINESS-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Mm = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Mm", 
            "#WORK-BUSINESS-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Dd = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Dd", 
            "#WORK-BUSINESS-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Check_Cycle_Date_Yyyymmdd", "#CHECK-CYCLE-DATE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Check_Cycle_Date_Yyyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Check_Cycle_Date_Yyyymmdd__R_Field_4", "REDEFINE", pnd_Check_Cycle_Date_Yyyymmdd);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Cc = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Cc", 
            "#CHECK-CYCLE-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Yy = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Yy", 
            "#CHECK-CYCLE-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Mm = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Mm", 
            "#CHECK-CYCLE-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dd = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dd", 
            "#CHECK-CYCLE-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date = localVariables.newFieldInRecord("pnd_Today_Business_Date", "#TODAY-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Today_Business_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Today_Business_Date__R_Field_5", "REDEFINE", pnd_Today_Business_Date);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm = pnd_Today_Business_Date__R_Field_5.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm", 
            "#TODAY-BUSINESS-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd = pnd_Today_Business_Date__R_Field_5.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd", 
            "#TODAY-BUSINESS-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Cc = pnd_Today_Business_Date__R_Field_5.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Cc", 
            "#TODAY-BUSINESS-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy = pnd_Today_Business_Date__R_Field_5.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy", 
            "#TODAY-BUSINESS-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd", "#PREV-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_6", "REDEFINE", pnd_Prev_Accounting_Dte_Yyyymmdd);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc", 
            "#PREV-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy", 
            "#PREV-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm", 
            "#PREV-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd", 
            "#PREV-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Accounting_Dte_Mmddccyy = localVariables.newFieldInRecord("pnd_Work_Accounting_Dte_Mmddccyy", "#WORK-ACCOUNTING-DTE-MMDDCCYY", FieldType.STRING, 
            10);

        pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7", "REDEFINE", pnd_Work_Accounting_Dte_Mmddccyy);
        pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Mm = pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Mm", 
            "#WORK-ACCOUNTING-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Slash_1 = pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Slash_1", 
            "#WORK-ACCOUNTING-DTE-SLASH-1", FieldType.STRING, 1);
        pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Dd = pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Dd", 
            "#WORK-ACCOUNTING-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Slash_2 = pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Slash_2", 
            "#WORK-ACCOUNTING-DTE-SLASH-2", FieldType.STRING, 1);
        pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Cc = pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Cc", 
            "#WORK-ACCOUNTING-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Yy = pnd_Work_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Yy", 
            "#WORK-ACCOUNTING-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Today_Accounting_Dte_Mmddccyy = localVariables.newFieldInRecord("pnd_Today_Accounting_Dte_Mmddccyy", "#TODAY-ACCOUNTING-DTE-MMDDCCYY", FieldType.STRING, 
            8);

        pnd_Today_Accounting_Dte_Mmddccyy__R_Field_8 = localVariables.newGroupInRecord("pnd_Today_Accounting_Dte_Mmddccyy__R_Field_8", "REDEFINE", pnd_Today_Accounting_Dte_Mmddccyy);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Mm = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_8.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Mm", 
            "#TODAY-ACCOUNTING-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Dd = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_8.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Dd", 
            "#TODAY-ACCOUNTING-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Cc = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_8.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Cc", 
            "#TODAY-ACCOUNTING-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Yy = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_8.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Yy", 
            "#TODAY-ACCOUNTING-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte = localVariables.newFieldInRecord("pnd_Prev_Effctv_Dte", "#PREV-EFFCTV-DTE", FieldType.STRING, 8);

        pnd_Prev_Effctv_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Prev_Effctv_Dte__R_Field_9", "REDEFINE", pnd_Prev_Effctv_Dte);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc = pnd_Prev_Effctv_Dte__R_Field_9.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc", "#PREV-EFFCTV-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy = pnd_Prev_Effctv_Dte__R_Field_9.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy", "#PREV-EFFCTV-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm = pnd_Prev_Effctv_Dte__R_Field_9.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm", "#PREV-EFFCTV-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd = pnd_Prev_Effctv_Dte__R_Field_9.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd", "#PREV-EFFCTV-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Compare_Prev_Effctv_Dte_A = localVariables.newFieldInRecord("pnd_Compare_Prev_Effctv_Dte_A", "#COMPARE-PREV-EFFCTV-DTE-A", FieldType.STRING, 
            8);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Adp_Effctv_Date = localVariables.newFieldInRecord("pnd_Adp_Effctv_Date", "#ADP-EFFCTV-DATE", FieldType.DATE);
        pnd_Adp_Effctv_Date_A = localVariables.newFieldInRecord("pnd_Adp_Effctv_Date_A", "#ADP-EFFCTV-DATE-A", FieldType.STRING, 8);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_10", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_10.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();
        vw_iaa_Cntrl_Rcrd_1_View.reset();

        ldaAdsl1000.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Loop.setInitialValue(0);
        pnd_Max_Rate.setInitialValue(250);
        pnd_Cntl_Isn.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_Total_Number_Rec.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Cref_Prod_Found.setInitialValue(true);
        pnd_Stbl_Fund.setInitialValue(false);
        pnd_Accs_Fund.setInitialValue(false);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp1000() throws Exception
    {
        super("Adsp1000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  OS-092013 START                                                                                                                                              //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 79 PS = 60;//Natural: FORMAT ( 2 ) LS = 79 PS = 60
        //* *CALLNAT 'ADSN888'    /* NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        //* *  #PAS-EXT-CNTRL-02-WORK-AREA
        //* *  #PEC-NBR-ACTIVE-ACCT
        DbsUtil.callnat(Adsn889.class , getCurrentProcessState(), pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue("*"));                                      //Natural: CALLNAT 'ADSN889' #PEC-ACCT-NAME-1 ( * )
        if (condition(Global.isEscape())) return;
        //*  OS-092013 END
                                                                                                                                                                          //Natural: PERFORM ADS-CONTROL-RECORD
        sub_Ads_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IAA-CONTROL-RECORD
        sub_Iaa_Control_Record();
        if (condition(Global.isEscape())) {return;}
        //* ****************************************************************
        //*                  MAIN SECTION
        //* ****************************************************************
        PND_PND_L1840:                                                                                                                                                    //Natural: READ WORK FILE 1 ADS-CNTRCT-VIEW #CNVRT-ANNTY-OPTN #ADP-EFFCTV-DATE
        while (condition(getWorkFiles().read(1, ldaAdsl401a.getVw_ads_Cntrct_View(), pnd_Cnvrt_Annty_Optn, pnd_Adp_Effctv_Date)))
        {
            //*                                                                                                                                                           //Natural: AT END OF DATA
            pnd_Work_Accounting_Dte_Mmddccyy.setValueEdited(ldaAdsl401a.getAds_Cntrct_View_Adc_Lst_Actvty_Dte(),new ReportEditMask("MM/DD/YYYY"));                        //Natural: MOVE EDITED ADC-LST-ACTVTY-DTE ( EM = MM/DD/YYYY ) TO #WORK-ACCOUNTING-DTE-MMDDCCYY
            pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Mm.setValue(pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Mm);                          //Natural: MOVE #WORK-ACCOUNTING-DTE-MM TO #TODAY-ACCOUNTING-DTE-MM
            pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Dd.setValue(pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Dd);                          //Natural: MOVE #WORK-ACCOUNTING-DTE-DD TO #TODAY-ACCOUNTING-DTE-DD
            pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Cc.setValue(pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Cc);                          //Natural: MOVE #WORK-ACCOUNTING-DTE-CC TO #TODAY-ACCOUNTING-DTE-CC
            pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Yy.setValue(pnd_Work_Accounting_Dte_Mmddccyy_Pnd_Work_Accounting_Dte_Yy);                          //Natural: MOVE #WORK-ACCOUNTING-DTE-YY TO #TODAY-ACCOUNTING-DTE-YY
            if (condition(!(pnd_Today_Accounting_Dte_Mmddccyy.equals(pnd_Today_Business_Date))))                                                                          //Natural: ACCEPT IF #TODAY-ACCOUNTING-DTE-MMDDCCYY = #TODAY-BUSINESS-DATE
            {
                continue;
            }
            pnd_Adp_Effctv_Date_A.setValueEdited(pnd_Adp_Effctv_Date,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED #ADP-EFFCTV-DATE ( EM = YYYYMMDD ) TO #ADP-EFFCTV-DATE-A
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Prev_Effctv_Dte.setValueEdited(pnd_Adp_Effctv_Date,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED #ADP-EFFCTV-DATE ( EM = YYYYMMDD ) TO #PREV-EFFCTV-DTE
                pnd_Compare_Prev_Effctv_Dte_A.setValue(pnd_Adp_Effctv_Date_A);                                                                                            //Natural: MOVE #ADP-EFFCTV-DATE-A TO #COMPARE-PREV-EFFCTV-DTE-A
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-TIME
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Adp_Effctv_Date_A.notEquals(pnd_Compare_Prev_Effctv_Dte_A)))                                                                                //Natural: IF #ADP-EFFCTV-DATE-A NE #COMPARE-PREV-EFFCTV-DTE-A
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-EFFECTIVE-DATE
                sub_Break_Of_Effective_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1840"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1840"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Effctv_Dte.setValueEdited(pnd_Adp_Effctv_Date,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED #ADP-EFFCTV-DATE ( EM = YYYYMMDD ) TO #PREV-EFFCTV-DTE
                pnd_Compare_Prev_Effctv_Dte_A.setValue(pnd_Adp_Effctv_Date_A);                                                                                            //Natural: MOVE #ADP-EFFCTV-DATE-A TO #COMPARE-PREV-EFFCTV-DTE-A
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_A_Indx.reset();                                                                                                                              //Natural: RESET #A-INDX
            FOR01:                                                                                                                                                        //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
            {
                if (condition(pnd_All_Indx_Pnd_A_Indx.greater(pnd_Pec_Nbr_Active_Acct)))                                                                                  //Natural: IF #A-INDX > #PEC-NBR-ACTIVE-ACCT
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(" ")))                                            //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE ' '
                {
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y")))                                           //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                    {
                        pnd_Stbl_Fund.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #STBL-FUND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Stbl_Fund.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #STBL-FUND
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'T' OR ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                    {
                        pnd_Total_Number_Rec.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                        sub_Calculate_Tiaa_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Total_Number_Rec.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-PRODUCT-ACCUMULATION
                        sub_Calculate_Cref_Product_Accumulation();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1840"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1840"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT TOP OF PAGE ( 1 );//Natural: END-WORK
        PND_PND_L1840_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "pnd_pnd_L1840");                                                                             //Natural: ESCAPE BOTTOM ( ##L1840. )
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-EFFECTIVE-DATE
        sub_Break_Of_Effective_Date();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-ACCOUNTING-DATE-RTN
        sub_Break_Of_Accounting_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADS-CONTROL-RECORD
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CONTROL-RECORD
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-PRODUCT-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-ACCOUNTING-DATE-RTN
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-EFFECTIVE-DATE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    private void sub_Ads_Control_Record() throws Exception                                                                                                                //Natural: ADS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "PND_PND_L2480",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        PND_PND_L2480:
        while (condition(vw_ads_Cntl_View.readNextRow("PND_PND_L2480")))
        {
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER ( ##L2480. ) = 2
            {
                //*     MOVE *ISN                      TO  #CNTL-ISN
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Bsnss_Dte_N.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                 //Natural: MOVE #DATE-N TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                      //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Bsnss_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A);                                            //Natural: MOVE EDITED #PREV-BSNSS-DTE-A TO #PREV-BSNSS-DTE-D ( EM = YYYYMMDD )
                pnd_Work_Business_Date.setValueEdited(pnd_Prev_Bsnss_Dte_D,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED #PREV-BSNSS-DTE-D ( EM = YYYYMMDD ) TO #WORK-BUSINESS-DATE
                pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm.setValue(pnd_Work_Business_Date_Pnd_Work_Business_Date_Mm);                                            //Natural: MOVE #WORK-BUSINESS-DATE-MM TO #TODAY-BUSINESS-DATE-MM
                pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd.setValue(pnd_Work_Business_Date_Pnd_Work_Business_Date_Dd);                                            //Natural: MOVE #WORK-BUSINESS-DATE-DD TO #TODAY-BUSINESS-DATE-DD
                pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy.setValue(pnd_Work_Business_Date_Pnd_Work_Business_Date_Yy);                                            //Natural: MOVE #WORK-BUSINESS-DATE-YY TO #TODAY-BUSINESS-DATE-YY
                pnd_Today_Business_Date_Pnd_Today_Business_Date_Cc.setValue(pnd_Work_Business_Date_Pnd_Work_Business_Date_Cc);                                            //Natural: MOVE #WORK-BUSINESS-DATE-CC TO #TODAY-BUSINESS-DATE-CC
                //* *  MOVE   ADS-CNTL-BSNSS-TMRRW-DTE               TO
                //* *    #PREV-ACCOUNTING-DTE-YYYYMMDD
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                     //Natural: MOVE #DATE-N TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
                ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
                ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
                ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Control_Record() throws Exception                                                                                                                //Natural: IAA-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        vw_iaa_Cntrl_Rcrd_1_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("READ01")))
        {
            pnd_Check_Cycle_Date_Yyyymmdd.setValueEdited(iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-CYCLE-DATE-YYYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Tiaa_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-TIAA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  012604
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATE
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rate)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                          //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                          //Natural: IF ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                //*  TPA
                if (condition(pnd_Cnvrt_Annty_Optn.equals("1")))                                                                                                          //Natural: IF #CNVRT-ANNTY-OPTN = '1'
                {
                    ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt().nadd(1);                                                                     //Natural: ADD 1 TO #TIAA-TPA-RATE-STD-MATURITY-CNT
                    ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #TIAA-TPA-GRN-STD-MATURITY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(pnd_Cnvrt_Annty_Optn.equals("2")))                                                                                                      //Natural: IF #CNVRT-ANNTY-OPTN = '2'
                    {
                        ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #TIAA-IPRO-RATE-STD-MATURITY-CNT
                        ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                       //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
                        ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                        ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt().nadd(1);                                                                 //Natural: ADD 1 TO #TIAA-IPRO-GRN-STD-MATURITY-CNT
                        //*  RS0 STABLE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                        //Natural: IF #STBL-FUND
                        {
                            ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt().nadd(1);                                                                 //Natural: ADD 1 TO #TIAA-STBL-STD-MATURITY-CNT
                            ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt().nadd(1);                                                             //Natural: ADD 1 TO #TIAA-STBL-GRN-STD-MATURITY-CNT
                            //*  TIAA TOTAL 780
                            ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                            //*  TIAA TOTAL 781
                            ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt().nadd(1);                                                                 //Natural: ADD 1 TO #TIAA-RATE-STD-MATURITY-CNT
                            ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
                            ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                            ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt().nadd(1);                                                                  //Natural: ADD 1 TO #TIAA-GRN-STD-MATURITY-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  TOTAL M781
                ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                       //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
                ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                       //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                //*  TPA
                if (condition(pnd_Cnvrt_Annty_Optn.equals("1")))                                                                                                          //Natural: IF #CNVRT-ANNTY-OPTN = '1'
                {
                    ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-TPA-STD-MATURITY-AMT
                    ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-TPA-GRN-STD-MATURITY-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(pnd_Cnvrt_Annty_Optn.equals("2")))                                                                                                      //Natural: IF #CNVRT-ANNTY-OPTN = '2'
                    {
                        ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-IPRO-STD-MATURITY-AMT
                        //*  TIAA TOTAL M781
                        ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-MATURITY-AMT
                        ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                        ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-IPRO-GRN-STD-MATURITY-AMT
                        //*  RS0 STABLE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                        //Natural: IF #STBL-FUND
                        {
                            ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-STD-MATURITY-AMT
                            ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-GRN-STD-MATURITY-AMT
                            //*  TIAA TOTAL M780
                            ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                            //*  TIAA TOTAL M781
                            ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-MATURITY-AMT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STD-MATURITY-AMT
                            //*  TIAA TOTAL M781
                            ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-MATURITY-AMT
                            ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                            ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STD-MATURITY-AMT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  TOTAL M781
                ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-MATURITY-AMT
                ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                             //Natural: IF ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                //*  RS0
                if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                //Natural: IF #STBL-FUND
                {
                    ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt().nadd(1);                                                                         //Natural: ADD 1 TO #TIAA-STBL-GRD-MATURITY-CNT
                    ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt().nadd(1);                                                                     //Natural: ADD 1 TO #TIAA-STBL-GRN-GRD-MATURITY-CNT
                    //*  TIAA TOTAL M781
                    ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
                    //*  TOTAL M781
                    ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt().nadd(1);                                                                         //Natural: ADD 1 TO #TIAA-RATE-GRD-MATURITY-CNT
                    //*  TIAA TOTAL M781
                    ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
                    //*  TOTAL M781
                    ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
                    ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt().nadd(1);                                                                          //Natural: ADD 1 TO #TIAA-GRN-GRD-MATURITY-CNT
                    ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                        //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                       //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                //*  RS0
                if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                //Natural: IF #STBL-FUND
                {
                    ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-GRD-MATURITY-AMT
                    ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-GRN-GRD-MATURITY-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl1000.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRD-MATURITY-AMT
                    ldaAdsl1000.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-GRD-MATURITY-AMT
                    ldaAdsl1000.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-MATURITY-AMT
                    ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
                //*  TOTAL M781
                ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-MATURITY-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Cref_Product_Accumulation() throws Exception                                                                                               //Natural: CALCULATE-CREF-PRODUCT-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                              //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Accs_Fund.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #ACCS-FUND
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ADC-ACCT-CDE ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                           //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl1000.getPnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                         //Natural: ADD 1 TO #CREF-MTH-MAT-CNT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1000.getPnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt().nadd(1);                                                     //Natural: ADD 1 TO #CREF-SUB-RATE-MTH-MATURITY-CNT
                        //*  CREF MTH TOTAL
                        ldaAdsl1000.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt().nadd(1);                                                      //Natural: ADD 1 TO #CREF-SUB-GRN-MTH-MATURITY-CNT
                        ldaAdsl1000.getPnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-MTH-MATURITY-AMT
                        //*  CREF MTH TOTAL
                        ldaAdsl1000.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-MTH-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TOTAL M781
                    ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
                    ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl1000.getPnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-MTH-MAT-AMT ( #B-INDX )
                    //*  TOTAL M781
                    ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #TOTAL-MATURITY-AMT
                    ldaAdsl1000.getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-MTH-MAT-AMT ( #B-INDX )
                    ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                    //*  ADC-GRD-MNTHLY-ACTL-AMT(#A-INDX) NE 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                          //Natural: IF ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl1000.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                        //Natural: ADD 1 TO #CREF-ANN-MAT-CNT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1000.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt().nadd(1);                                                    //Natural: ADD 1 TO #CREF-SUB-RATE-ANN-MATURITY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TOTAL M781
                    ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
                    ldaAdsl1000.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                    //Natural: ADD 1 TO #CREF-GRN-ANN-MAT-CNT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        //*  CREF TOTAL LINE
                        ldaAdsl1000.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt().nadd(1);                                                     //Natural: ADD 1 TO #CREF-SUB-GRN-ANN-MATURITY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl1000.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-ANN-MAT-AMT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1000.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-ANN-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TOTAL M781
                    ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #TOTAL-MATURITY-AMT
                    //*  RS0
                    //*  TOTAL M780
                    ldaAdsl1000.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-ANN-MAT-AMT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        //*  CREF TOTAL LINE
                        ldaAdsl1000.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-ANN-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                    //*  ADC-STNDRD-ANNL-ACTL-AMT (#A-INDX) NE 0
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Break_Of_Accounting_Date_Rtn() throws Exception                                                                                                      //Natural: BREAK-OF-ACCOUNTING-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm780.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM780'
        pnd_Prev_Accounting_Dte_Yyyymmdd.setValueEdited(ldaAdsl401a.getAds_Cntrct_View_Adc_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED ADC-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #PREV-ACCOUNTING-DTE-YYYYMMDD
        ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                          //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
        ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                          //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
        ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                          //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
        ldaAdsl1000.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                          //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
        ldaAdsl1000.getPnd_Tiaa_Grand_Accum().reset();                                                                                                                    //Natural: RESET #TIAA-GRAND-ACCUM #TIAA-SUB-GRN-ACCUM #CREF-MONTHLY-GRAND-ACCUM ( * ) #CREF-ANNUALLY-GRAND-ACCUM ( * ) #CREF-MONTHLY-SUB-GRAND-ACCUM #CREF-ANNUALLY-SUB-GRAND-ACCUM #CREF-AND-TIAA-GRAND-ACCUM
        ldaAdsl1000.getPnd_Tiaa_Sub_Grn_Accum().reset();
        ldaAdsl1000.getPnd_Cref_Monthly_Grand_Accum().getValue("*").reset();
        ldaAdsl1000.getPnd_Cref_Annually_Grand_Accum().getValue("*").reset();
        ldaAdsl1000.getPnd_Cref_Monthly_Sub_Grand_Accum().reset();
        ldaAdsl1000.getPnd_Cref_Annually_Sub_Grand_Accum().reset();
        ldaAdsl1000.getPnd_Cref_And_Tiaa_Grand_Accum().reset();
    }
    private void sub_Break_Of_Effective_Date() throws Exception                                                                                                           //Natural: BREAK-OF-EFFECTIVE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl1000.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm().setValue(pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm);                                              //Natural: MOVE #PREV-EFFCTV-DTE-MM TO #EFFECTIVE-DATE-MCDY-MM
        ldaAdsl1000.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd().setValue(pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd);                                              //Natural: MOVE #PREV-EFFCTV-DTE-DD TO #EFFECTIVE-DATE-MCDY-DD
        ldaAdsl1000.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc().setValue(pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc);                                              //Natural: MOVE #PREV-EFFCTV-DTE-CC TO #EFFECTIVE-DATE-MCDY-CC
        ldaAdsl1000.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy().setValue(pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy);                                              //Natural: MOVE #PREV-EFFCTV-DTE-YY TO #EFFECTIVE-DATE-MCDY-YY
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm781.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM781'
        ldaAdsl1000.getPnd_Tiaa_Minor_Accum().reset();                                                                                                                    //Natural: RESET #TIAA-MINOR-ACCUM #TIAA-SUB-ACCUM #CREF-MONTHLY-MINOR-ACCUM ( * ) #CREF-ANNUALLY-MINOR-ACCUM ( * ) #CREF-MONTHLY-SUB-TOTAL-ACCUM #CREF-ANNUALLY-SUB-TOTAL-ACCUM #TOTAL-CREF-AND-TIAA-ACCUM
        ldaAdsl1000.getPnd_Tiaa_Sub_Accum().reset();
        ldaAdsl1000.getPnd_Cref_Monthly_Minor_Accum().getValue("*").reset();
        ldaAdsl1000.getPnd_Cref_Annually_Minor_Accum().getValue("*").reset();
        ldaAdsl1000.getPnd_Cref_Monthly_Sub_Total_Accum().reset();
        ldaAdsl1000.getPnd_Cref_Annually_Sub_Total_Accum().reset();
        ldaAdsl1000.getPnd_Total_Cref_And_Tiaa_Accum().reset();
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().write(1, ReportOption.NOTITLE,"*******************************************************");                                                            //Natural: WRITE ( 1 ) '*******************************************************'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"*     FINAL PREMIUM ACCUMULATION - JOB RUN ENDED      *");                                                            //Natural: WRITE ( 1 ) '*     FINAL PREMIUM ACCUMULATION - JOB RUN ENDED      *'
        if (Global.isEscape()) return;
        if (condition(pnd_Total_Number_Rec.equals(getZero())))                                                                                                            //Natural: IF #TOTAL-NUMBER-REC = 0
        {
            getReports().write(1, ReportOption.NOTITLE,"*          NO TRANSACTIONS PROCESSED TODAY            *");                                                        //Natural: WRITE ( 1 ) '*          NO TRANSACTIONS PROCESSED TODAY            *'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,"* NUMBER OF TRANSACTIONS PROCESSED TODAY :",pnd_Total_Number_Rec, new ReportEditMask ("ZZ,ZZZ,ZZ9"),              //Natural: WRITE ( 1 ) '* NUMBER OF TRANSACTIONS PROCESSED TODAY :' #TOTAL-NUMBER-REC ( EM = ZZ,ZZZ,ZZ9 ) ' *'
                " *");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,"*******************************************************");                                                            //Natural: WRITE ( 1 ) '*******************************************************'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm779.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM779'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=79 PS=60");
        Global.format(2, "LS=79 PS=60");
    }
}
